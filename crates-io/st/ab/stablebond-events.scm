(define-module (crates-io st ab stablebond-events) #:use-module (crates-io))

(define-public crate-stablebond-events-0.2.21 (c (n "stablebond-events") (v "0.2.21") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "045i6rzfwkhi0i623ip7zqav8inplxpb1c8lkya6ll31h02zzyvx") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.22 (c (n "stablebond-events") (v "0.2.22") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "160fpdggxqn7yhdgl947p0fam412fm2rvivv3zfdir597yw1l9s9") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.23 (c (n "stablebond-events") (v "0.2.23") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0v96dbghl9220w46k14wchjfdn7yxlfzp6bdzv454rz42ggpwg4c") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.24 (c (n "stablebond-events") (v "0.2.24") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0pisyyahv2y03ac0bmazdx3gaa2kn72iswmi0pv1gsspkqynhrzl") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.25 (c (n "stablebond-events") (v "0.2.25") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "1l1ly4n7g2lxj9i3cfxprbn54kzgylm5y0z4j82r26ha2g3crrk0") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.26 (c (n "stablebond-events") (v "0.2.26") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0yhl9hkxaawbdim247r912ipf8ylfxz97b2h11i5h4805q15yfkv") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.27 (c (n "stablebond-events") (v "0.2.27") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0ayf9mr2y99f0ki37drbcdyd6yqfaajglvx1la9pa9d6cykv1m0f") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.28 (c (n "stablebond-events") (v "0.2.28") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0lfgsqm9np3aggv1bqp6qc6xs647crijj5w8j82n6gjxp4rk50vw") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.29 (c (n "stablebond-events") (v "0.2.29") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "01khdcfszhvy17r6w946cq5p67fbw746hfv86lvv0x2ag0n2xnaq") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.30 (c (n "stablebond-events") (v "0.2.30") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0vjqg3fdcwjjj5f38nhba9h1afz6g58hfgzvvap51s167ggsbscg") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.31 (c (n "stablebond-events") (v "0.2.31") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "04628gzkdvhdf7rg909bxqc5k89334xxkckyyfrglq6ipafm4bp1") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.32 (c (n "stablebond-events") (v "0.2.32") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "1yhvq3fqlmhpmw5m0h9krrrzc22hhj8cpd8d4qgqv83n4zaf8bkp") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.33 (c (n "stablebond-events") (v "0.2.33") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0frfrq3q2l1r33pyghyxgywhb4f61wd37zcmbs1skh43jl8xl1r9") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.34 (c (n "stablebond-events") (v "0.2.34") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "05y1yaghbx7m7xf59q3wcs0ps34lmrm8mcldwh5rq9z4g9rjfvny") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.35 (c (n "stablebond-events") (v "0.2.35") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "021fq3229inh87qmdd6vm6ygn4r9bipwhb3rjxd8g9d3z0qnir4q") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.36 (c (n "stablebond-events") (v "0.2.36") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "18jay2icr75sndzwsyrh3sym79p6bwyv93c95g228kjpf8aq3zp1") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.37 (c (n "stablebond-events") (v "0.2.37") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0421sw1fgx1yfvfkhj60qmf29hwsv891rb67h34zz4pmgnhqqwhw") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.38 (c (n "stablebond-events") (v "0.2.38") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "12mlqswj15bk8gggb8hiqdkrbvwsgldq92hd2hvam6cclzsi0gp5") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.39 (c (n "stablebond-events") (v "0.2.39") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0wv5nk95zbmpb0xw6chk148rxn73pjaiwn8si7iyh0nvpfs9l1cl") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.40 (c (n "stablebond-events") (v "0.2.40") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "1vgrws0a4031r34w0m94ywjs2h1s35vqkmbgnaxjl9iivisplv0b") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.41 (c (n "stablebond-events") (v "0.2.41") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "1r1ava9dyp170imqyssf0rx5qrz9ansbai15n4n3877zd2zyjb82") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.42 (c (n "stablebond-events") (v "0.2.42") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "00rf90ysn18qhk6rcksicalbwh2jj71xw40lrjw2cp3srp41g4d0") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.43 (c (n "stablebond-events") (v "0.2.43") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "007c3zyhbmj5sqz5hvjqny638fknr9vbvciif7pxk2vb5c4whixf") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.44 (c (n "stablebond-events") (v "0.2.44") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0zckrjrmvsbh23jm7g9rr6k7hwpcqalp8c0j1lh8grsfbrh457bm") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.46 (c (n "stablebond-events") (v "0.2.46") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "1m7pl72kkja2qch74gz1yk3qm2c2lbpv8v2fkk56k5q0lnw8fps8") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.47 (c (n "stablebond-events") (v "0.2.47") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "0ih163453zzz5n078cx81hnlfmdvzn0ps64c07dqshz5g15w4cah") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.48 (c (n "stablebond-events") (v "0.2.48") (d (list (d (n "anchor-lang") (r ">=0.29.0") (d #t) (k 0)))) (h "15g958njhaq74jjrzv11ggcnf39npyyn92qmxls6wnbjhwswwv3r") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.49 (c (n "stablebond-events") (v "0.2.49") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0gg22wb63636n02ym41bp36z6aasanakjwas9wm9p9vyv3kqjbpd") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.50 (c (n "stablebond-events") (v "0.2.50") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0dxgc7wyjrn4cdm7860gpflnvr0rghxqajchfm6gs54c4nbz88v6") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.51 (c (n "stablebond-events") (v "0.2.51") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1na0fshhxvxzhf5zawi6d2316arlwimfdgs0mv18crrdrvas4rh1") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.52 (c (n "stablebond-events") (v "0.2.52") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1xd75hbp8wzh5s0zq06zy8rxzkizl56j8zwa5cp1hyy6hllqrj4y") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.53 (c (n "stablebond-events") (v "0.2.53") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "141gid7jcsgginmd62vllwkbgcx10rl7lqfhncfz8jf8p6v9di5z") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.54 (c (n "stablebond-events") (v "0.2.54") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0c7wds7rvs7fskic8ivh8f2m821cx5ryzs04irmfwg07x04biy5f") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.55 (c (n "stablebond-events") (v "0.2.55") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "15lm7fqg1w6yzsj4kld5zr67akmzrfdkvlbk521nsgcnafglwwcv") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.56 (c (n "stablebond-events") (v "0.2.56") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "040hymdz5k1shf80yhp1ll3phm38lhd7fzvq8rpfv7pbgw84iy2l") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.57 (c (n "stablebond-events") (v "0.2.57") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0l711rdi07ky1mll60xqrbrgcdzqjwxwmpn2snwa0agc4zlfgs49") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.58 (c (n "stablebond-events") (v "0.2.58") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1i7wxg0q0i1pvgjq614l9bs15j7pdd501famdp6arcbvl8f66qmm") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.59 (c (n "stablebond-events") (v "0.2.59") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0hylqkpgjwwkricdiwn1h1ni1nbq1515vg3cmmyfblyk2xbc6wxy") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.61 (c (n "stablebond-events") (v "0.2.61") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0v00kmrkxg2vz93js8zyw2x5vgwm3ha546avfali18j910dzhi2v") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.62 (c (n "stablebond-events") (v "0.2.62") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1c8vfqhz85wi81k4c1kpmfl101pfhh4jrlincfzfb5pibvg740ag") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.63 (c (n "stablebond-events") (v "0.2.63") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1via88md86r3axzzaxbf27766i40pbxraw5bz5amkv5awjh15br1") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.2.65 (c (n "stablebond-events") (v "0.2.65") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1vdd61l03n3yd330b2qdk0lrcxvkygb4xd3zmbdkhyfmsgs0jg3a") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.0 (c (n "stablebond-events") (v "0.3.0") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "14r3zryrm7gx2cd17z6zv2i0wp6fz6fpsbwvmzlmlcfjwgrya2nq") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.1 (c (n "stablebond-events") (v "0.3.1") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1ipmmfcxxj3n4pgxgbfdhxb194s3rdyr0gg0d0xv7ry71plv7bbx") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.2 (c (n "stablebond-events") (v "0.3.2") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "15ayp1hars2895b9xkm9ma7hqd44gma4jx9wirqfpgx3qrbgqhv3") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.3 (c (n "stablebond-events") (v "0.3.3") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "1mp666snb0slrnflpv0l3cb8akclb2fplisk4h2zibc5zm8k2x43") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.4 (c (n "stablebond-events") (v "0.3.4") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0cb5dzqb04km85sxq59dqk3xql27plrs026wqzdnyxki9iyrnxhv") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.5 (c (n "stablebond-events") (v "0.3.5") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0aciq95qh7fppaqbn1mbcy726j8w96gkzkmrv7cpzas28ykzks3n") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.6 (c (n "stablebond-events") (v "0.3.6") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0172mkryv34xapxjixihhp8wxqxqqan0lm2l4q2jyl4ah0ik6605") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.7 (c (n "stablebond-events") (v "0.3.7") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0qph0mznasszkycmi7mzm2a32ny9zh4bcf9rp3zqks9fi43srib2") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.8 (c (n "stablebond-events") (v "0.3.8") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "12gim3xhfvq33y14j06gw6w474kxk7q4f43hbrmsfpjp7a31p84i") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.9 (c (n "stablebond-events") (v "0.3.9") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0xfc3ayv26g6zxdw030i9065xkhi83mf9jzsb7kpl4lqvr5w6ki6") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-stablebond-events-0.3.10 (c (n "stablebond-events") (v "0.3.10") (d (list (d (n "anchor-lang") (r "=0.30.0") (d #t) (k 0)))) (h "0f8rb1pnklib7s3h7kvm0mr3aw5fhmchp6sr4wjiy5c518n6f0ri") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

