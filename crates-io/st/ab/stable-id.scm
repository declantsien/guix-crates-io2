(define-module (crates-io st ab stable-id) #:use-module (crates-io))

(define-public crate-stable-id-0.1.0 (c (n "stable-id") (v "0.1.0") (h "1li2yc1ns9nrfv7xxb040x9b71pg216jnaq74d3ai7z9nh1yswv3")))

(define-public crate-stable-id-0.1.1 (c (n "stable-id") (v "0.1.1") (h "0xbbllq82ald55yp9h2b4bkm9fg19nq8sm5dxs8pd7g3s0fxy2vi")))

(define-public crate-stable-id-0.2.0 (c (n "stable-id") (v "0.2.0") (d (list (d (n "derive-stable-id") (r "^0.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.1.1") (d #t) (k 0)))) (h "0pwvi8yxi6m8s6a3g14z0m7skc9974c5jpwy4rja7xdwj015p33z")))

(define-public crate-stable-id-0.2.1 (c (n "stable-id") (v "0.2.1") (d (list (d (n "derive-stable-id") (r "^0.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.1.1") (d #t) (k 0)))) (h "0rlcrwby0srrjnxjq912b0gkjy0yf9c6sw15sjdn8hq9b14aj1dw")))

(define-public crate-stable-id-0.3.0 (c (n "stable-id") (v "0.3.0") (d (list (d (n "derive-stable-id") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0jaljfzjjz4y9ylwgfcc1d6fhhz2b5d2cvl2w8xq365bz4v6sff0")))

(define-public crate-stable-id-0.3.1 (c (n "stable-id") (v "0.3.1") (d (list (d (n "derive-stable-id") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.2.0") (d #t) (k 0)))) (h "03qxxnpflzvpmlvhqx3lsw7n9v6yryrzhqw1r4p0f6qxjwrr3sbh")))

(define-public crate-stable-id-0.3.2 (c (n "stable-id") (v "0.3.2") (d (list (d (n "derive-stable-id") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.2.0") (d #t) (k 0)))) (h "1d9zgcb5s6rnhda6rgf7cmsp96zglksssyk2m4ff61cg9nf71d8j")))

(define-public crate-stable-id-0.3.3 (c (n "stable-id") (v "0.3.3") (d (list (d (n "derive-stable-id") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.2.0") (d #t) (k 0)))) (h "06hjnvw9lyhf5l6jhbwyaz5fwav1k8rs3hh6ng60003w916djapc")))

(define-public crate-stable-id-0.3.4 (c (n "stable-id") (v "0.3.4") (d (list (d (n "derive-stable-id") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.2.0") (d #t) (k 0)))) (h "1s3gg7624xyjy37bg896cdkz3nd2faq8va4q5aa3i1rkmliya6mh")))

(define-public crate-stable-id-0.4.0 (c (n "stable-id") (v "0.4.0") (d (list (d (n "derive-stable-id") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.2.0") (d #t) (k 0)))) (h "0mdxzsj4skqvlxdgrhhw52fbdz0ijzy9f7vrzq4iz3aim6pdbdj1")))

(define-public crate-stable-id-0.4.1 (c (n "stable-id") (v "0.4.1") (d (list (d (n "derive-stable-id") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "stable-id-traits") (r "^0.2.0") (d #t) (k 0)))) (h "14ssp58agyad9mwm6yn65rm0w1v3pd9mc8hl034zh1p2yiqsb0v0")))

