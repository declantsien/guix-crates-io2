(define-module (crates-io st ab stable-vec) #:use-module (crates-io))

(define-public crate-stable-vec-0.1.0 (c (n "stable-vec") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0vw0mdhlckfibj9hc97jdqmlkmq4gk2qzdx8288ig727yri1wgl0")))

(define-public crate-stable-vec-0.1.1 (c (n "stable-vec") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "128q8l5wpi7vyxx859gffqzxvqqd14w975sr9yfgsl6w5m72w3cb")))

(define-public crate-stable-vec-0.1.2 (c (n "stable-vec") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "07khgji60inlkqbk6h5mjlmlj579fvqsf52hqhr1gxryyvxm0p94")))

(define-public crate-stable-vec-0.2.0 (c (n "stable-vec") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "07d5kxkd61s5w2x5ir4z0lvm68w9wvkiq569zlxckpb48mbfqv8b") (y #t)))

(define-public crate-stable-vec-0.2.1 (c (n "stable-vec") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "11qlrgazc388gdq2yz0af2gbp3f6a70jg3hjc2g6igmar5d722pn") (y #t)))

(define-public crate-stable-vec-0.2.2 (c (n "stable-vec") (v "0.2.2") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1100pjxi0dj518bg2ix5i45b8vrmk4hkfzyx1sdyjpllr1s4rsp8") (y #t)))

(define-public crate-stable-vec-0.3.0 (c (n "stable-vec") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1zmy71v9sy15m74ip06313p1wdb89kabnkndwv1qvs2k1g1v11fz") (y #t)))

(define-public crate-stable-vec-0.3.1 (c (n "stable-vec") (v "0.3.1") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "14535sm898jgg2y810gpi59nii95kmbv8c26bmfnf1dnr7ny466p") (f (quote (("nightly-bench" "criterion/real_blackbox"))))))

(define-public crate-stable-vec-0.3.2 (c (n "stable-vec") (v "0.3.2") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "17vjii12j4rnyxsx6y555001cs7c7vc58z639n16xiq22kcvxcdv") (f (quote (("nightly-bench" "criterion/real_blackbox"))))))

(define-public crate-stable-vec-0.4.0-beta (c (n "stable-vec") (v "0.4.0-beta") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "17f36gx2p7i24jz8811brjs4b90v4x653kkprj34amash0dznhyc") (f (quote (("nightly-bench" "criterion/real_blackbox")))) (y #t)))

(define-public crate-stable-vec-0.4.0 (c (n "stable-vec") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1jnqj3ssj3k1f60bwm1iahwgjcrvcffk94v7ar8gmlpk52xvgpw0") (f (quote (("nightly-bench" "criterion/real_blackbox")))) (y #t)))

(define-public crate-stable-vec-0.4.1 (c (n "stable-vec") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1j4yvhqkb16h3aab4q471mv8in7bgh11k147xhxji1z05hmg7pyi") (f (quote (("nightly-bench" "criterion/real_blackbox"))))))

