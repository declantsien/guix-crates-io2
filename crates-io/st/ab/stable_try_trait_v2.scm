(define-module (crates-io st ab stable_try_trait_v2) #:use-module (crates-io))

(define-public crate-stable_try_trait_v2-1.75.0 (c (n "stable_try_trait_v2") (v "1.75.0") (h "0di4h9pckbspnx0yrgs9kv4zn5655gszfmza01l6w8kydcnxw9xz") (y #t)))

(define-public crate-stable_try_trait_v2-1.75.1 (c (n "stable_try_trait_v2") (v "1.75.1") (h "04qdpq4x3n0pg4aalmfl4x1ay8dq7dzbdgvh0k5crf2d3x0lhkhw")))

