(define-module (crates-io st ab stable_node_set) #:use-module (crates-io))

(define-public crate-stable_node_set-0.1.0 (c (n "stable_node_set") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1x7ndx7ciad2a1fwgaw4nz32vs3is1958ki94zq9z8w1w01qmixr")))

