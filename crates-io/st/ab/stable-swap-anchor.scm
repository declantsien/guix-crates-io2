(define-module (crates-io st ab stable-swap-anchor) #:use-module (crates-io))

(define-public crate-stable-swap-anchor-1.2.0 (c (n "stable-swap-anchor") (v "1.2.0") (d (list (d (n "anchor-lang") (r "^0.13.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.13.2") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.0.0") (d #t) (k 0)))) (h "12y8vyxr8m2wiq9z4i8zwibybwz4y9fvf1wm9gpz0hlks6nrvmx4")))

(define-public crate-stable-swap-anchor-1.4.0 (c (n "stable-swap-anchor") (v "1.4.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "0knz4y4pn90ram664lrrszx811pj5r8vzw2zi5nixqb8hrx1j7p4")))

(define-public crate-stable-swap-anchor-1.4.1 (c (n "stable-swap-anchor") (v "1.4.1") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "0j4r8y7wwa237y5f16rqnc4m6lrhah6559g7ss5mpdia4p58dcja")))

(define-public crate-stable-swap-anchor-1.4.2 (c (n "stable-swap-anchor") (v "1.4.2") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "10q9yx184xhlwjad339r36piy1ihri5pvvhr0sw1biaymr798a8k")))

(define-public crate-stable-swap-anchor-1.4.3 (c (n "stable-swap-anchor") (v "1.4.3") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "1w51c4mjap3a560bggk70rx1xhwqjcaa85b3w4icn9s9b411bmc2")))

(define-public crate-stable-swap-anchor-1.4.4 (c (n "stable-swap-anchor") (v "1.4.4") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "06n82q8l6mka2676jswh4f0i8pvsz66p4ki4nqgivvcknaqa96pf")))

(define-public crate-stable-swap-anchor-1.5.0 (c (n "stable-swap-anchor") (v "1.5.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "0icbba10ywrqi4733b5r5v5p62qnpjwl7dzg0rhsva9xs18yb13s")))

(define-public crate-stable-swap-anchor-1.5.1 (c (n "stable-swap-anchor") (v "1.5.1") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "1xdv1g3z0qs1vzsf37cjz80m67c547hbif4inm2m29bwpipqn5fb")))

(define-public crate-stable-swap-anchor-1.5.2 (c (n "stable-swap-anchor") (v "1.5.2") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "197rrf7z3yixn281ix5viv85sm0yr4w7kw31p3gm4lv0a7icwcgc")))

(define-public crate-stable-swap-anchor-1.5.4 (c (n "stable-swap-anchor") (v "1.5.4") (d (list (d (n "anchor-lang") (r "^0.18.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.18.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.0") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.2.0") (d #t) (k 0)))) (h "1pg914ps645wz69y5ci49bs0qk3vcy7wckvi8hxawgsrvzkdhjij")))

(define-public crate-stable-swap-anchor-1.5.5 (c (n "stable-swap-anchor") (v "1.5.5") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "1zd7mrirbd4sxmz5qnp2gvslkka7gn1ppc4kkk7bbwsqww4yiiag")))

(define-public crate-stable-swap-anchor-1.5.6 (c (n "stable-swap-anchor") (v "1.5.6") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "0hs3dirkqgsly69kkh38svcygzdp3kn64p3x44v187ps9ds5dr6g")))

(define-public crate-stable-swap-anchor-1.5.7 (c (n "stable-swap-anchor") (v "1.5.7") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "1rw50c1vxm31ak3fc1knvcpc8h9ry09baqyypsk3rggj1a1012sr")))

(define-public crate-stable-swap-anchor-1.6.0 (c (n "stable-swap-anchor") (v "1.6.0") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "1zbicqjn3dvyjl2kw87ldd9mnvz6d8i9sxslv09pnrhf0rafq81i")))

(define-public crate-stable-swap-anchor-1.6.6 (c (n "stable-swap-anchor") (v "1.6.6") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "0gccvsnk1g5inzigj0knqmhi8dj0yms78kq2r4h9haiggpwgbv60")))

(define-public crate-stable-swap-anchor-1.6.7 (c (n "stable-swap-anchor") (v "1.6.7") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "1i7mdfljlqh5irn7ygdyibs2chlbkb7dv6i1fxn6ljpz844cf4gk")))

(define-public crate-stable-swap-anchor-1.6.8 (c (n "stable-swap-anchor") (v "1.6.8") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "1zq27g4cv41sbd0gpr5p1hpsq4s0lay0ccmk7i2ckn3fabxdl5yw")))

(define-public crate-stable-swap-anchor-1.7.0 (c (n "stable-swap-anchor") (v "1.7.0") (d (list (d (n "anchor-lang") (r ">=0.22.0") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "11b6z2abvfzajv8iclvjn29ip23jxfgq5ndix4gp88hcwn6amyla")))

(define-public crate-stable-swap-anchor-1.7.3 (c (n "stable-swap-anchor") (v "1.7.3") (d (list (d (n "anchor-lang") (r ">=0.22.0") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "0i0m5hixsl0cspnv7yryrgis9k7yamzx317flmylik5fj2ldpkqn")))

(define-public crate-stable-swap-anchor-1.8.0 (c (n "stable-swap-anchor") (v "1.8.0") (d (list (d (n "anchor-lang") (r ">=0.22.0") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "1hlv1slbqhs0l0b3pjqanygi85gmsdfk329k6lmxgpzp2icpayb6")))

(define-public crate-stable-swap-anchor-1.8.1 (c (n "stable-swap-anchor") (v "1.8.1") (d (list (d (n "anchor-lang") (r ">=0.22.0") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1") (d #t) (k 0)))) (h "1i1js7bwrhmm2b056ri3dcfnzvp4ad7b5sx42hsqjik9nvvv14lh")))

