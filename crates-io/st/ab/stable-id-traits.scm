(define-module (crates-io st ab stable-id-traits) #:use-module (crates-io))

(define-public crate-stable-id-traits-0.1.0 (c (n "stable-id-traits") (v "0.1.0") (h "0c7fjs27axc5y5xriyy3plq92984mckbhaajhpfbmak33gs10j8s") (y #t)))

(define-public crate-stable-id-traits-0.1.1 (c (n "stable-id-traits") (v "0.1.1") (h "1m6080lvg1bd4i7c4k7qcavk0xhm86c9bfrafwa88h04qpd8d3jr")))

(define-public crate-stable-id-traits-0.2.0 (c (n "stable-id-traits") (v "0.2.0") (h "1kcxm47dmncxk2a5r7cn5w0k367p0cf81m1i9rw04k88siqwfsva")))

