(define-module (crates-io st ab stable-curve-math) #:use-module (crates-io))

(define-public crate-stable-curve-math-0.1.0 (c (n "stable-curve-math") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.5") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.8.1") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "0g88fq64gn0fq98vpilvc7ad3vjr3v0cfaax525cah5sq5jz7s7b") (y #t)))

(define-public crate-stable-curve-math-0.1.1 (c (n "stable-curve-math") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.5") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.8.1") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "13jlydlc5hw9vzrxmxlb7y79rlgigvky9lqpg9p4l63qndkf3iy3") (y #t)))

(define-public crate-stable-curve-math-0.1.2 (c (n "stable-curve-math") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.5") (d #t) (k 0)) (d (n "stable-swap-client") (r "^1.8.1") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "0gkanqs4qd3hlr55h5hcb209kcfh4l4zyiq1md62cyqx9ldf7anl") (y #t)))

(define-public crate-stable-curve-math-0.1.3 (c (n "stable-curve-math") (v "0.1.3") (h "0j1hlg2pj4qp7j27x5rqp6cnv9s8dgnsz464nipaf8yc4dqqavm8") (y #t)))

