(define-module (crates-io st ab stable-pattern) #:use-module (crates-io))

(define-public crate-stable-pattern-0.1.0 (c (n "stable-pattern") (v "0.1.0") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "0i8hq82vm82mqj02qqcsd7caibrih7x5w3a1xpm8hpv30261cr25") (f (quote (("std" "memchr/std") ("default" "std"))))))

