(define-module (crates-io st ab stable-inline-python) #:use-module (crates-io))

(define-public crate-stable-inline-python-0.1.0 (c (n "stable-inline-python") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (k 0)))) (h "1z2niiiwxl14cj7dwhvp812wndbfv2ig14jsrjm1sbrm212rqazj")))

(define-public crate-stable-inline-python-0.1.1 (c (n "stable-inline-python") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (k 0)))) (h "0y18wcs5rd5bgh50aqsb4kdfkzjf4hc6l334nv7sc9hv3b5864s0")))

(define-public crate-stable-inline-python-0.1.2 (c (n "stable-inline-python") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (k 0)))) (h "0hg6mwnm14mqskhwfghml2j0f3wg1vxcd7xd15l1ibs3xil8cp73")))

