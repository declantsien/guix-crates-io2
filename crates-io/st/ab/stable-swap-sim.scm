(define-module (crates-io st ab stable-swap-sim) #:use-module (crates-io))

(define-public crate-stable-swap-sim-0.1.0 (c (n "stable-swap-sim") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.13.2") (d #t) (k 0)))) (h "0d2ccb01v7h5rqn1yks7dmfd5drb2j98bpyh0phwszg3pqbd4y69")))

(define-public crate-stable-swap-sim-0.1.2 (c (n "stable-swap-sim") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0d94qzfmah4sq0qb7lwalhs6fs1jxpjnb0ir0r5803f37vsag2ff")))

(define-public crate-stable-swap-sim-0.1.3 (c (n "stable-swap-sim") (v "0.1.3") (d (list (d (n "pyo3") (r "^0.16.1") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1k4028mg1idpkm41bfcp3xyjzds55jcs4xy29iz39dgjjsjxjigk")))

(define-public crate-stable-swap-sim-0.1.4 (c (n "stable-swap-sim") (v "0.1.4") (d (list (d (n "pyo3") (r "^0.16.4") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0b0hg2f9imylhxqgy9ra261iniyrmgpj43bz1c6iss03hn8hlfvy")))

