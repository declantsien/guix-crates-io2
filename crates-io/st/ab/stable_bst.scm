(define-module (crates-io st ab stable_bst) #:use-module (crates-io))

(define-public crate-stable_bst-0.0.1 (c (n "stable_bst") (v "0.0.1") (d (list (d (n "compare") (r "^0.0") (d #t) (k 0)) (d (n "ordered_iter") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0m7q74gn11aqkdwk75cdm6nhkxy9q9hnd1qy2nq0nbyxyq3wf13c") (f (quote (("default" "ordered_iter"))))))

(define-public crate-stable_bst-0.0.2 (c (n "stable_bst") (v "0.0.2") (d (list (d (n "compare") (r "^0.0") (d #t) (k 0)) (d (n "ordered_iter") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1l48r2c1062rgjvfjh3iayd0d40ni0w3h55px4fadi07li6hq2il") (f (quote (("default" "ordered_iter"))))))

(define-public crate-stable_bst-0.1.0 (c (n "stable_bst") (v "0.1.0") (d (list (d (n "compare") (r "^0.0") (d #t) (k 0)) (d (n "ordered_iter") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "03qxd6ngbw0fjiw7f2ll3k9i9alcj8d0rbwpd96qaar7d8lrg9ga") (f (quote (("default" "ordered_iter"))))))

(define-public crate-stable_bst-0.2.0 (c (n "stable_bst") (v "0.2.0") (d (list (d (n "compare") (r "^0.0") (d #t) (k 0)) (d (n "ordered_iter") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1bcnvcsrn74ql2fqk24lmjlh385nf9s6k0lfldsxp34pr5qzj7dc") (f (quote (("default" "ordered_iter"))))))

