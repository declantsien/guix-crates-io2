(define-module (crates-io st ab stable-fs) #:use-module (crates-io))

(define-public crate-stable-fs-0.1.9 (c (n "stable-fs") (v "0.1.9") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.8") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "034w8mx72yxjfgxvdr23rss1gmyh2drj367k5h2fwy5lvr7m0zlb")))

(define-public crate-stable-fs-0.1.10 (c (n "stable-fs") (v "0.1.10") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.8") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "09dbvpqz8n1rzkqlwcaqvzx22cyxf638q560hzb1p0k9qisqh07z")))

(define-public crate-stable-fs-0.1.11 (c (n "stable-fs") (v "0.1.11") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "0ck1xqjk2ssvdfx1z7prdccyyw4nha3acr4grjx3avjipslp8giw")))

(define-public crate-stable-fs-0.1.12 (c (n "stable-fs") (v "0.1.12") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "02h2q16dpbk6ncy501lvismb0xd3vkmra2220f030a38cr38ks45")))

(define-public crate-stable-fs-0.1.13 (c (n "stable-fs") (v "0.1.13") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1qvfvfvvhiyssysix9agws6bj6qvqzc2biy038vnmkw8dxs4s65i") (y #t)))

(define-public crate-stable-fs-0.2.0 (c (n "stable-fs") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "17k6mz4vfskrbpm4jwmq9i6ih1nva16113mypsda16crpkjv5ggk")))

(define-public crate-stable-fs-0.3.0 (c (n "stable-fs") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.12.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1d2afc5rklrqqcgi5r1cqab9k516dsrzlrb2pd3zyffmrl1jhp1v")))

(define-public crate-stable-fs-0.3.1 (c (n "stable-fs") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.13.1") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)))) (h "1ivvx33czl2z9bj07dvbffggbcjwfxnn8kn6bvgcg8s6crv7bzpy")))

