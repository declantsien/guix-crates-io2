(define-module (crates-io st ab stable-step) #:use-module (crates-io))

(define-public crate-stable-step-0.1.1 (c (n "stable-step") (v "0.1.1") (d (list (d (n "stable-step-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0bbmgkk3i7xybsgl6fxca6b4k76xbn3q0n0nkc92wc2zj1m16mh6") (f (quote (("derive" "stable-step-derive") ("default"))))))

