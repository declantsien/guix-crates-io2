(define-module (crates-io st ab stable-bloom-filter) #:use-module (crates-io))

(define-public crate-stable-bloom-filter-0.1.0 (c (n "stable-bloom-filter") (v "0.1.0") (h "0xrp4gqzbrmqj9mfv1c9y01gj311f0nxy9l3naxvwzpbcv6wf08g")))

(define-public crate-stable-bloom-filter-0.2.0 (c (n "stable-bloom-filter") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0nyc77sym583h0k2i09caqai50yn3dikhrlrrbqlz3k3plsr8lvw")))

(define-public crate-stable-bloom-filter-0.3.0 (c (n "stable-bloom-filter") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "float-cmp") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0ssh62pb2ra9pyjxaksykfs4js6shkwpq602xzjib5nvfdwg30kj")))

