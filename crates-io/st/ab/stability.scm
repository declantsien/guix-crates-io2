(define-module (crates-io st ab stability) #:use-module (crates-io))

(define-public crate-stability-0.1.0 (c (n "stability") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0arklz5b9w6f3bjyajcbbdzawzl5xdabjrdw2v93knirgq99g1w3")))

(define-public crate-stability-0.1.1 (c (n "stability") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1kn3vcicmpg8bnyalp15i2j0dbv6c0wc62022bcs58jdi5vv3lgb")))

(define-public crate-stability-0.2.0 (c (n "stability") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "0ykqc6q16vj1pz6s98fpgxqcr1psvz9vdn154f0cii6yagwfmy9g") (r "1.60")))

