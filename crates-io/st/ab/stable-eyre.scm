(define-module (crates-io st ab stable-eyre) #:use-module (crates-io))

(define-public crate-stable-eyre-0.1.0 (c (n "stable-eyre") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.46") (d #t) (k 0)) (d (n "eyre") (r "^0.3.8") (d #t) (k 0)) (d (n "indenter") (r "^0.1.3") (d #t) (k 0)))) (h "1k78frbyhk0b0nkcd7p5mh1krl4j0idgbs4javfsf1hgs859vdbr")))

(define-public crate-stable-eyre-0.1.1 (c (n "stable-eyre") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.46") (d #t) (k 0)) (d (n "eyre") (r "^0.3.8") (d #t) (k 0)) (d (n "indenter") (r "^0.1.3") (d #t) (k 0)))) (h "1zsbl6n7rc88qivh27fzcqp11i41i88r15ij3bpdb9q73b6jlz0k")))

(define-public crate-stable-eyre-0.1.2 (c (n "stable-eyre") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.46") (d #t) (k 0)) (d (n "eyre") (r "^0.3.8") (d #t) (k 0)) (d (n "indenter") (r "^0.1.3") (d #t) (k 0)))) (h "1d7zfiiap7zq00rlxf795lfwkhg8zbpga459yf361xr3xhfvqcz7")))

(define-public crate-stable-eyre-0.1.3 (c (n "stable-eyre") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3.46") (d #t) (k 0)) (d (n "eyre") (r "^0.3.8") (d #t) (k 0)) (d (n "indenter") (r "^0.1.3") (d #t) (k 0)))) (h "1f1jxlaj3c9js7l4mak651jgl5qrc6nvj04i5vg4hbbjyj161ikx")))

(define-public crate-stable-eyre-0.2.0 (c (n "stable-eyre") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3.48") (f (quote ("gimli-symbolize"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "indenter") (r "^0.3.0") (d #t) (k 0)))) (h "0wgw57sn26aig1bnjwgpx39b24jdqbycplswlazyppxs2qfjsdcf")))

(define-public crate-stable-eyre-0.2.1 (c (n "stable-eyre") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3.48") (f (quote ("gimli-symbolize"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "indenter") (r "^0.3.0") (d #t) (k 0)))) (h "07f2zfygg3lppi2m04rrmpx0z3xxyma0b5d1kk01m1f7gsp0x77y")))

(define-public crate-stable-eyre-0.2.2 (c (n "stable-eyre") (v "0.2.2") (d (list (d (n "backtrace") (r "^0.3.48") (f (quote ("gimli-symbolize"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "indenter") (r "^0.3.0") (d #t) (k 0)))) (h "1i2wd742406zx6vrgw3zaqp6azndm3w8vn0nbyvp0k535n6fqvsm")))

