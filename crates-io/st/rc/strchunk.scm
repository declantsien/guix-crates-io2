(define-module (crates-io st rc strchunk) #:use-module (crates-io))

(define-public crate-strchunk-0.1.0-beta.1 (c (n "strchunk") (v "0.1.0-beta.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "range-split") (r "^0.1.0-beta.1") (f (quote ("bytes"))) (d #t) (k 0)))) (h "0wxb1n52a8mfzahj0ibi5jkaydpmxvy2wvs9r56ww646avpdazrg") (f (quote (("specialization"))))))

(define-public crate-strchunk-0.1.0 (c (n "strchunk") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "range-split") (r "^0.1") (f (quote ("bytes"))) (d #t) (k 0)))) (h "1f02zlrl9qnbjh9fmkxaqwqpib8ad0i8j5q1lfsamvn28dwpkjg2") (f (quote (("specialization"))))))

(define-public crate-strchunk-0.2.0-beta.1 (c (n "strchunk") (v "0.2.0-beta.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "range-split") (r "^0.2.0-beta.2") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util"))) (d #t) (k 2)))) (h "0y94c8pbniswy2jpw6apvynwvih10s2mpxqhvxfdca49ajbmkyvw") (f (quote (("specialization"))))))

(define-public crate-strchunk-0.2.0 (c (n "strchunk") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "range-split") (r "^0.2") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util"))) (d #t) (k 2)))) (h "17snc3c71fxlvdwbylj79y2hzvpqs7dg354a514q45nk7hzfnq89") (f (quote (("specialization"))))))

(define-public crate-strchunk-0.3.0 (c (n "strchunk") (v "0.3.0") (d (list (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "range-split") (r "^0.3") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-std" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1r3066h9lapcpcl8g4phylgzychcfhzhpnfvr6isi9jxwyhb126p") (f (quote (("unstable" "specialization") ("specialization"))))))

(define-public crate-strchunk-0.4.0 (c (n "strchunk") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "range-split") (r "^0.4") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("io-std" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1hmlijnwr4x88yfszk5zr2cbxs0lzpwh7qmqpr1klcd58pm71n48") (f (quote (("unstable" "specialization") ("specialization"))))))

(define-public crate-strchunk-0.4.1 (c (n "strchunk") (v "0.4.1") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "range-split") (r "^0.4") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("io-std" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0jgl06sxrmj58xrzcqjbirifvb0yfis97l1cs8bsb5pas94gjg5f") (f (quote (("unstable" "specialization") ("specialization"))))))

