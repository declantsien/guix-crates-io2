(define-module (crates-io st rc strcursor) #:use-module (crates-io))

(define-public crate-strcursor-0.1.0 (c (n "strcursor") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^0.1.0") (d #t) (k 0)))) (h "0q20awdciz3abmk6ghm6dvwki8fkyw52axwmb061h2f89jk9yhqm")))

(define-public crate-strcursor-0.2.0 (c (n "strcursor") (v "0.2.0") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1.0") (d #t) (k 0)))) (h "1hmhrwcdx1vcb6xka6k2fk517dvpflwcd8kgyvsyc9in5ypgaxzg")))

(define-public crate-strcursor-0.2.1 (c (n "strcursor") (v "0.2.1") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1.0") (d #t) (k 0)))) (h "10dd3i4kir0i7mq477csl8j2b57x8r873i4h7nzxf6z0l045b28k")))

(define-public crate-strcursor-0.2.2 (c (n "strcursor") (v "0.2.2") (d (list (d (n "debug_unreachable") (r "^0.0.6") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^0.1.0") (d #t) (k 0)))) (h "0qn50fqh3bndyfidbp7ip1vqfipd0gh86yqb2l20c1w3ras91xw2")))

(define-public crate-strcursor-0.2.3 (c (n "strcursor") (v "0.2.3") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^0.1.0") (d #t) (k 0)))) (h "1gnk9zn318xgz7zp4s2gprgcbsnvcnp567d2kzskcc0vmmq5qc38")))

(define-public crate-strcursor-0.2.4 (c (n "strcursor") (v "0.2.4") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^0.1.0, < 0.1.3") (d #t) (k 0)))) (h "1gd2pchxrcbxh98zdfig8bim7c45smmv3bcs4isnnidy0q4413cl")))

(define-public crate-strcursor-0.2.5 (c (n "strcursor") (v "0.2.5") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^0.1.0, < 0.1.3") (d #t) (k 0)))) (h "0nj6kfka15za5yigdn7xi149mv6v1rvxgqln2axdpl7nw4hvfg55")))

