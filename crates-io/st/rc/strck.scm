(define-module (crates-io st rc strck) #:use-module (crates-io))

(define-public crate-strck-0.1.0 (c (n "strck") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 2)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 2)))) (h "0hrmwxn7i7nb8lk0assz9bwqs0jrsixgga0gp55s4hkcbhbc6v2a")))

(define-public crate-strck-0.1.1 (c (n "strck") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smol_str") (r "^0.1") (d #t) (k 2)))) (h "1balb9585hm4h3zqswi3qpnlqcxaxrxf8n45m63dxp7majq9pfa2")))

(define-public crate-strck-0.1.2 (c (n "strck") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smol_str") (r "^0.1") (d #t) (k 2)))) (h "05z981r28f3s7a29cwkb1fg7cznk89rpf8g9kyfrg3wxxl6hk4dy")))

