(define-module (crates-io st rc strck_ident) #:use-module (crates-io))

(define-public crate-strck_ident-0.1.0 (c (n "strck_ident") (v "0.1.0") (d (list (d (n "strck") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0pj9cvdd8mb7avvayjs9d0lqmg38rh0qx9078zcnxg6vjszvrin1") (f (quote (("serde" "strck/serde") ("rust"))))))

(define-public crate-strck_ident-0.1.1 (c (n "strck_ident") (v "0.1.1") (d (list (d (n "strck") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0yzjiz7sman2kwka51wjnyywqqlcwlrcr9grw5b167hny4ld3wks") (f (quote (("serde" "strck/serde") ("rust"))))))

(define-public crate-strck_ident-0.1.2 (c (n "strck_ident") (v "0.1.2") (d (list (d (n "strck") (r "^0.1.2") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "00kjyy5avywpzhkajvlk35h6w4xkl34j3wk78sj5hf4v2qmq1hz1") (f (quote (("serde" "strck/serde") ("rust"))))))

