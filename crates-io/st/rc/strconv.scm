(define-module (crates-io st rc strconv) #:use-module (crates-io))

(define-public crate-strconv-0.1.0 (c (n "strconv") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1i7qjl8wd8b8qa93q3y48iy8zpsjyk76hiz2qhdw7hpf107z2bgk")))

(define-public crate-strconv-0.1.1 (c (n "strconv") (v "0.1.1") (d (list (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1w73qxpfkjpsy4wy06xfzahxl8saip6h0yym2fcp677dlbylzlza") (y #t)))

