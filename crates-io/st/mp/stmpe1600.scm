(define-module (crates-io st mp stmpe1600) #:use-module (crates-io))

(define-public crate-stmpe1600-0.1.0 (c (n "stmpe1600") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1iicg5b3ksci620j6cdq51lf2iqijb4bq8b5nyvn13j99ahc72p3") (y #t)))

(define-public crate-stmpe1600-0.2.0 (c (n "stmpe1600") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "08l3w7bwhv63yzbxxg526c4zvyd474vvk6nzlna31ln2ymw1h5y4")))

(define-public crate-stmpe1600-1.0.0 (c (n "stmpe1600") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1k2mlli315nln0d9jawbj3mv3yiyg6ij60w4crnd4j93g4q0z4x2")))

(define-public crate-stmpe1600-1.0.1 (c (n "stmpe1600") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "03crly04avghfsi8li7ww1jkz52kbzx3izn0wfjj54kqasbicr2i")))

(define-public crate-stmpe1600-1.1.0 (c (n "stmpe1600") (v "1.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "06kw48f20q1klqp0kym4hv5yc03gn9pl6nn74l3cs9iblzj7phkx")))

(define-public crate-stmpe1600-1.1.1 (c (n "stmpe1600") (v "1.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "1zykp5an84mrnm24ig5s927ywzlli9m1l9nvs47hicjmgab2y0rh")))

(define-public crate-stmpe1600-1.1.2 (c (n "stmpe1600") (v "1.1.2") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "1cvl2pw8iry8i43dwsv6hcbrqarjmq59wagz2mlfb4imm3n9kgvi")))

(define-public crate-stmpe1600-2.0.0 (c (n "stmpe1600") (v "2.0.0") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "1pgvw4q2xv0ww3i4crcf9hsnivizxqkm6hmva18fpknas42jv3l2")))

