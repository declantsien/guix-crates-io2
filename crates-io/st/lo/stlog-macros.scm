(define-module (crates-io st lo stlog-macros) #:use-module (crates-io))

(define-public crate-stlog-macros-0.1.0 (c (n "stlog-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (f (quote ("full"))) (d #t) (k 0)))) (h "0w1dlrlyjh9a7dgd7bbp7d2ngl6rjjllcjaz7s243r76ndw6nbaq")))

(define-public crate-stlog-macros-0.1.1 (c (n "stlog-macros") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (f (quote ("full"))) (d #t) (k 0)))) (h "03wrvxnf3y58z4r7rszsyziyvqgbadqlyf5d6ndn15kx21zc2ypb")))

(define-public crate-stlog-macros-0.1.2 (c (n "stlog-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jc82jnkfncfr1bh52brczs1rzi9879mn2f17fmmy0mpfmgmzh4l") (f (quote (("spanned"))))))

