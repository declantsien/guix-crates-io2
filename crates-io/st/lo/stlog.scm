(define-module (crates-io st lo stlog) #:use-module (crates-io))

(define-public crate-stlog-0.1.0 (c (n "stlog") (v "0.1.0") (h "0dyxr8098yhikglnhizws4rbgxscq7x7nfp6fdi3f2028d28qbpz")))

(define-public crate-stlog-0.2.0 (c (n "stlog") (v "0.2.0") (h "00g3y6fwbl2i274frcjqlbj0vfncbf71z8pbkh2kkhnwcbjcnqrz")))

(define-public crate-stlog-0.3.0 (c (n "stlog") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 1)) (d (n "stlog-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1vni668ppayrpwgvsd0qx5gdzj68014iapf1brib87kq8knqmphk") (f (quote (("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug")))) (y #t)))

(define-public crate-stlog-0.3.1 (c (n "stlog") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 1)) (d (n "stlog-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1068v1qgbn9hvn59jn0my3vj7fmhr7vnck1kpmz8jw6vrc36kbj5") (f (quote (("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug")))) (y #t)))

(define-public crate-stlog-0.3.2 (c (n "stlog") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 1)) (d (n "stlog-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1hkkpnxz08clvkl6rgg3aa0hdhxjlk3klg30xc5w819ljiwk097w") (f (quote (("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug"))))))

(define-public crate-stlog-0.3.3 (c (n "stlog") (v "0.3.3") (d (list (d (n "stlog-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1m7f7dvzzrjvi7l0rdygr342j0kkcnxiwg0nb5nqpbg1mq0axyq5") (f (quote (("spanned" "stlog-macros/spanned") ("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug"))))))

