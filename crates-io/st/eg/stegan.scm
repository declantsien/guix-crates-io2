(define-module (crates-io st eg stegan) #:use-module (crates-io))

(define-public crate-stegan-0.0.1 (c (n "stegan") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.14") (d #t) (k 0)) (d (n "steganography") (r "^1.0.1") (d #t) (k 0)))) (h "1vgm0mldz3mnk04il5ljv0hk535p5nqcxk89bxm3l4dllj9n86a7")))

(define-public crate-stegan-0.0.5 (c (n "stegan") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.14") (d #t) (k 0)) (d (n "steganography") (r "^1.0.1") (d #t) (k 0)))) (h "12plyz0ri2phm4kp7r98scvpc7hl1fyd67ig18vc9j579mby66hi")))

