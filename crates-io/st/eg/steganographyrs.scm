(define-module (crates-io st eg steganographyrs) #:use-module (crates-io))

(define-public crate-steganographyrs-0.1.0 (c (n "steganographyrs") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)))) (h "0qaq6giyaxjhkr56jcc4xq1ngqj1wipaln0hzvprvz5rv5f7gn2a")))

(define-public crate-steganographyrs-0.1.1 (c (n "steganographyrs") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.12") (d #t) (k 0)))) (h "01h2jn49j0h5q3vkpyd1snjcf7qds51p4vlzjzg6gfngsj4g1c7v")))

