(define-module (crates-io st eg stego) #:use-module (crates-io))

(define-public crate-stego-0.1.0 (c (n "stego") (v "0.1.0") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1dnxnm4xr1im4xg4by03rc1x3q2rnwva5z4ina601rp6c4rglay9")))

(define-public crate-stego-0.1.2 (c (n "stego") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1lcrd5rm9wglvqlrg8vxgl5q6cbxyzc0zkfc2502k3v6xn4v3cij")))

(define-public crate-stego-0.1.3 (c (n "stego") (v "0.1.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1zq56yfgw78dfxvccmljxfxzdbz9ci8vydgr2nbh66dd05p4l89c")))

(define-public crate-stego-0.1.4 (c (n "stego") (v "0.1.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "07h8b63b39nkjjjwgxaplqrx4slz6kk9n66n3x2590rvc1la8bk5")))

