(define-module (crates-io st eg steggy) #:use-module (crates-io))

(define-public crate-steggy-0.1.0 (c (n "steggy") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1rjx6ygjv6h8lnhgkjhd24qx4av4inm4in6lqx3s5jy3gx42dgsh")))

