(define-module (crates-io st eg steganocrypt) #:use-module (crates-io))

(define-public crate-steganocrypt-0.1.0 (c (n "steganocrypt") (v "0.1.0") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "168ibg36cjfmwkpbla7hw08j61hmsn92rd9bblfd69y3jvppjyad") (y #t)))

(define-public crate-steganocrypt-0.1.1 (c (n "steganocrypt") (v "0.1.1") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "1a7fclsni7ppg3x3dsz7pixlw7s4p2f12nikxqpjb90lxl472i1a") (y #t)))

(define-public crate-steganocrypt-0.1.2 (c (n "steganocrypt") (v "0.1.2") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "0an0r7cfxkr9irczbrpq0saw3rlc60wk027ijzrkq3vq0lz30az8") (y #t)))

(define-public crate-steganocrypt-0.1.3 (c (n "steganocrypt") (v "0.1.3") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "1ci20hwspnsvp2xs40dmplr957g951hk4ppjfciqpxw8cknrrkzn") (y #t)))

(define-public crate-steganocrypt-0.1.4 (c (n "steganocrypt") (v "0.1.4") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "0iz44pvs8am7ss3n0m2rn8l34wi6hg0d7v50nky7j76cp63li2k3") (y #t)))

(define-public crate-steganocrypt-0.1.5 (c (n "steganocrypt") (v "0.1.5") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "19cp9cdrqsa1aprjnshh11y37qzbgsvc4dpary2j20xqsvq76zaj") (y #t)))

(define-public crate-steganocrypt-0.1.6 (c (n "steganocrypt") (v "0.1.6") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "0470qgs1wbiwxd7qh8s6h5avp6ap76siw7766kbvyq7q6jrsidzk") (y #t)))

(define-public crate-steganocrypt-0.1.7 (c (n "steganocrypt") (v "0.1.7") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "1kymk1fgfmqdx89xhnzxbs536dbcvvan5vl9cw4vng7jnqsycq2y") (y #t)))

(define-public crate-steganocrypt-0.1.8 (c (n "steganocrypt") (v "0.1.8") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "1wdjsbz8p5y63rv1xss4wlxw6fnwmgw2z76v99cgysvlms31ph7i") (y #t)))

(define-public crate-steganocrypt-0.1.9 (c (n "steganocrypt") (v "0.1.9") (d (list (d (n "steganography") (r "^1.0.2") (d #t) (k 0)))) (h "1rnl5pk3yxa7398xp7ff44z546yrfbhw04jiwpfxjv3d6v362ajg") (y #t)))

