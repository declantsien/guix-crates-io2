(define-module (crates-io st eg steganography) #:use-module (crates-io))

(define-public crate-steganography-0.1.0 (c (n "steganography") (v "0.1.0") (d (list (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "09vfbaw05g26h30nxrpa5njyc6wh8634ylsa8nmccjh00sp57ksx")))

(define-public crate-steganography-0.2.0 (c (n "steganography") (v "0.2.0") (d (list (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "0cw9psa3wy05cvid86ghmmi4ipmri5mrdajyb688lc6130c6d6f4")))

(define-public crate-steganography-0.2.1 (c (n "steganography") (v "0.2.1") (d (list (d (n "image") (r "^0.12.3") (d #t) (k 0)))) (h "1ki54kkgkm77rdxvg72g1kf48qf485xdzhsja2rdm9i85bkfkbl0")))

(define-public crate-steganography-0.2.2 (c (n "steganography") (v "0.2.2") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)))) (h "1cx99aygzm6pck1sr0n3kfrb6lgd00vx5ji1v2xryvqcqyrbnk7j")))

(define-public crate-steganography-0.2.3 (c (n "steganography") (v "0.2.3") (d (list (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1p3s38dhgafamagrdmyvanvp5m1z6vk6ka1vk0b0vc4idkgvwj9a")))

(define-public crate-steganography-1.0.0 (c (n "steganography") (v "1.0.0") (d (list (d (n "image") (r "^0.15.0") (d #t) (k 0)))) (h "06i43m2n9ms7ppygf12gjzm8nv2v0lg187snb3h4k06fvzvssw56")))

(define-public crate-steganography-1.0.1 (c (n "steganography") (v "1.0.1") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1advdp444h0qg9rbg5kxk43hn0vmbg01dz1iadc1k2z9zwbmkj58")))

(define-public crate-steganography-1.0.2 (c (n "steganography") (v "1.0.2") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "0azc2whacb98v1n2v7qc8nkw6mk6l6ckk4k9wyb3xy4z95r70bjm")))

