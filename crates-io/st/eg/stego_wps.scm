(define-module (crates-io st eg stego_wps) #:use-module (crates-io))

(define-public crate-stego_wps-1.0.0 (c (n "stego_wps") (v "1.0.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "14laj0wcb7l47kcqr8gl0xg09yhnpacwhxh9zx5yxv36q1frj9iv")))

(define-public crate-stego_wps-1.0.1 (c (n "stego_wps") (v "1.0.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0m7njr8ybbw3bbw8rx6rlg9v3ya63j9mvall8ccj80f55fyk3hsn")))

(define-public crate-stego_wps-1.0.8 (c (n "stego_wps") (v "1.0.8") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1r1k4x3z1if25ckydfzpragsspaihff8gy5jg5c6q8amjsam7pgb")))

(define-public crate-stego_wps-1.0.9 (c (n "stego_wps") (v "1.0.9") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "18j5baanhjhcynpkjhzidrhawjmw5rq7ji52vsx4ysfal6fh89jq")))

(define-public crate-stego_wps-1.1.9 (c (n "stego_wps") (v "1.1.9") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0qi854z3cwvmdx1zhajaylm87pcimg7kavp8wjv2xg040sa92vnd")))

