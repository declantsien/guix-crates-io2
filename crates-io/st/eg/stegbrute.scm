(define-module (crates-io st eg stegbrute) #:use-module (crates-io))

(define-public crate-stegbrute-0.1.0 (c (n "stegbrute") (v "0.1.0") (d (list (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "05ymwi09k8khs3li0ylr1313y0a4idja7xd41c0id2g3jjkc9635")))

(define-public crate-stegbrute-0.1.1 (c (n "stegbrute") (v "0.1.1") (d (list (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "1wn07mqdkljrighh0kchg7a67i07nwrzmbr24kf256azhw6wfza4")))

