(define-module (crates-io st eg stegomage) #:use-module (crates-io))

(define-public crate-stegomage-0.1.0 (c (n "stegomage") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1c83krp6y7nwpr4v256v2rqwagq5l29wm3dmcqr9m18ansbgnmx5")))

