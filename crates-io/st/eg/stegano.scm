(define-module (crates-io st eg stegano) #:use-module (crates-io))

(define-public crate-stegano-0.0.1 (c (n "stegano") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.3") (d #t) (k 0)))) (h "0w7ssb7sdljvrca064nrzkvp0zg99wm8c29anx01qkif9bqmr0sx")))

(define-public crate-stegano-0.0.2 (c (n "stegano") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.4") (d #t) (k 0)))) (h "0fdpbfhk89mz7nrkj1llzllxshd2n5b5g7j2440fswmshkfij4j2")))

(define-public crate-stegano-0.0.3 (c (n "stegano") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.4") (d #t) (k 0)))) (h "0wfk9c2dwzhahpyhivqbcbqwabjjd5n8zj38hslchjxqm7w20jnr")))

(define-public crate-stegano-0.0.4 (c (n "stegano") (v "0.0.4") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.4") (d #t) (k 0)))) (h "0pw75faif74sh7d6dkcyq9zwi29rmbkqzhn75dk16c3xs4w95l4r")))

(define-public crate-stegano-0.0.5 (c (n "stegano") (v "0.0.5") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.4") (d #t) (k 0)))) (h "0dhfclrc9q9fsbq7190jp6drk9f97mc64yk67fj1z4kaalzav27c")))

(define-public crate-stegano-0.0.6 (c (n "stegano") (v "0.0.6") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.4") (d #t) (k 0)))) (h "161yb4pqj14k01jrgbajppwaw2zp015q2vrbqs5346zad75rqkhz")))

(define-public crate-stegano-0.1.0 (c (n "stegano") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.4") (d #t) (k 0)))) (h "1ph4mxzap3bdap1hcjlbb7lclbgwz2yydsar9asfh1gi178sqwg7")))

(define-public crate-stegano-0.1.1 (c (n "stegano") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32-v2") (r "^0.0.4") (d #t) (k 0)))) (h "07955i9s8sgrzhdps7ak8l0dqlzzcbsl9ah16q1qyslnq3nrxlkn")))

