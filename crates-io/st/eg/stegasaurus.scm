(define-module (crates-io st eg stegasaurus) #:use-module (crates-io))

(define-public crate-stegasaurus-0.1.0 (c (n "stegasaurus") (v "0.1.0") (d (list (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1gxxpd8x3dpl3kj93i4bxmz2x1x5ji3ffij1ks1fnlb74ryhxy0g")))

(define-public crate-stegasaurus-0.1.1 (c (n "stegasaurus") (v "0.1.1") (d (list (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0rs9jc3gxw37wg5g168gn048xvndg0gxlvd7kq26x9shmbhpiy7l")))

