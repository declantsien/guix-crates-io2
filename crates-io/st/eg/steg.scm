(define-module (crates-io st eg steg) #:use-module (crates-io))

(define-public crate-steg-0.1.0 (c (n "steg") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lsb_png_steganography") (r "^0.1.0") (d #t) (k 0)))) (h "18kdc3yjx5ihgd2d9336xnnks94c2p20r84zk09mbi748dfzkyxy")))

(define-public crate-steg-0.1.1 (c (n "steg") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lsb_png_steganography") (r "^0.1.0") (d #t) (k 0)))) (h "1apd7cmq7m157m7rl75yn1vy8gjs6w9pa4vn8i7dxlls45i88n8z")))

(define-public crate-steg-0.1.2 (c (n "steg") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lsb_png_steganography") (r "^0.1.0") (d #t) (k 0)) (d (n "whitespace_text_steganography") (r "^0.2.0") (d #t) (k 0)))) (h "18lg9lxsdw4hip3qyxs1hr87zbvh20ggv1akf9a2d1wbg5dfxgh5")))

(define-public crate-steg-0.1.3 (c (n "steg") (v "0.1.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lsb_png_steganography") (r "^0.1.0") (d #t) (k 0)) (d (n "whitespace_text_steganography") (r "^0.2.0") (d #t) (k 0)))) (h "0m75k3xrvimpx58dn0dffkjxks4iq04isddpdc458hfk6gykfvwj")))

(define-public crate-steg-0.1.4 (c (n "steg") (v "0.1.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lsb_png_steganography") (r "^0.1.0") (d #t) (k 0)) (d (n "whitespace_text_steganography") (r "^0.2.1") (d #t) (k 0)))) (h "0k4crkz5q93n0sv1rbwsjfwy3cdimx6iw4b5jxwwf265w3mmr84i")))

(define-public crate-steg-0.1.5 (c (n "steg") (v "0.1.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lsb_png_steganography") (r "^0.1.0") (d #t) (k 0)) (d (n "lsb_text_png_steganography") (r "^0.1.1") (d #t) (k 0)) (d (n "whitespace_text_steganography") (r "^0.2.1") (d #t) (k 0)))) (h "1izx4d3vfbr0jjpcb43glhz0qpy85k9n2x49bzfplr1xfpaw255b")))

(define-public crate-steg-0.1.6 (c (n "steg") (v "0.1.6") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lsb_png_steganography") (r "^0.1.0") (d #t) (k 0)) (d (n "lsb_text_png_steganography") (r "^0.1.1") (d #t) (k 0)) (d (n "whitespace_text_steganography") (r "^0.2.1") (d #t) (k 0)))) (h "1ndvsz44fhrxksq2fsx0vlv9ccs069a2iygss0zjjfv6c3j9i156")))

