(define-module (crates-io st eg stegano-cli) #:use-module (crates-io))

(define-public crate-stegano-cli-0.3.1-beta.1 (c (n "stegano-cli") (v "0.3.1-beta.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "stegano-core") (r "^0.3.1-beta.1") (d #t) (k 0)))) (h "1x6m4d57idyslaa97sbkyqb7ddg18yvijilwf7lbpqna72qhv2gk")))

(define-public crate-stegano-cli-0.3.1-beta.2 (c (n "stegano-cli") (v "0.3.1-beta.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "stegano-core") (r "^0.3.1-beta.1") (d #t) (k 0)))) (h "1i5dkycm381ab387jck2a4fr640hc6xkfjylx2y9h5ycs7apys4i")))

(define-public crate-stegano-cli-0.3.1-beta.3 (c (n "stegano-cli") (v "0.3.1-beta.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "stegano-core") (r "^0.3.1-beta.2") (d #t) (k 0)))) (h "1r86jz72abpaf93j2rv0h6hvk0yybf6cmpdia40x3y4gbkjr5p30")))

(define-public crate-stegano-cli-0.3.1-beta.4 (c (n "stegano-cli") (v "0.3.1-beta.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "stegano-core") (r "^0.3.1-beta.3") (d #t) (k 0)))) (h "1y0b5i9m3vi8psnxi6p0rb5kfb3xm35hqabb4lpq9rivarm2drv9")))

(define-public crate-stegano-cli-0.3.1 (c (n "stegano-cli") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.3.1") (d #t) (k 0)))) (h "1bciv4xbmja2z7r37xwskvbkd82i3wp5jj0csbrsq80w6hc2a4nk")))

(define-public crate-stegano-cli-0.3.2 (c (n "stegano-cli") (v "0.3.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.3.2") (d #t) (k 0)))) (h "1yr8z2g88ny9jxxsyhnzakwmpvzcpa6nnp04154aqrqqc390d7xj")))

(define-public crate-stegano-cli-0.4.0 (c (n "stegano-cli") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.0") (d #t) (k 0)))) (h "1in8jylsa4r2zcn9i33ssnac8x34z54fq15b2mjqci3miy2pphrw") (y #t)))

(define-public crate-stegano-cli-0.4.1 (c (n "stegano-cli") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.1") (d #t) (k 0)))) (h "1y7fj0mdi00z9a3zj7lzsj5dbnb45ipc2bwnym8ii1v99i05a07b")))

(define-public crate-stegano-cli-0.4.5 (c (n "stegano-cli") (v "0.4.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.5") (d #t) (k 0)))) (h "0vwqkfgm87afvnm2asvpnidz63dbqhzxziwqrr8vwkw6lnf8k0c4")))

(define-public crate-stegano-cli-0.4.6 (c (n "stegano-cli") (v "0.4.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.6") (d #t) (k 0)))) (h "1h6v3nk4fs4m5vj5fxnzg34d0z6l6mymfchdr3dsqyiihda2qmvi")))

(define-public crate-stegano-cli-0.4.7 (c (n "stegano-cli") (v "0.4.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.7") (d #t) (k 0)))) (h "1rxj93hfg489zyzayyb2spgy0hvffv4kcbrr6l0sd73yp2ymgkck")))

(define-public crate-stegano-cli-0.4.8 (c (n "stegano-cli") (v "0.4.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.8") (d #t) (k 0)))) (h "0l4glh6sjcfwrlf467rfk51sbyz1wz5dgp3vah7gpzzzh4xrrlns")))

(define-public crate-stegano-cli-0.4.9 (c (n "stegano-cli") (v "0.4.9") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.9") (d #t) (k 0)))) (h "0rsq80mwhv911ayssz3hz5wzrw924kbqirnkrz75hvj885vbww9m")))

(define-public crate-stegano-cli-0.4.10 (c (n "stegano-cli") (v "0.4.10") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "stegano-core") (r "^0.4.10") (d #t) (k 0)))) (h "12paqfsa780lk5f7qrzlxvamqgb5fjr5fdgzj529n1mi9ic6sji7")))

(define-public crate-stegano-cli-0.5.0 (c (n "stegano-cli") (v "0.5.0") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "stegano-core") (r "^0.5.0") (d #t) (k 0)))) (h "00afkic0qrc93k5yqrnvzc3f0lmrfh8s3f1g6w0661rxk4xf6bg3")))

(define-public crate-stegano-cli-0.5.1 (c (n "stegano-cli") (v "0.5.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "stegano-core") (r "^0.5.1") (d #t) (k 0)))) (h "12i7mlj6vf5kkfif6mq0dzxxv91gifcyv59ymm4xrhj38w8lmnc0")))

(define-public crate-stegano-cli-0.5.2 (c (n "stegano-cli") (v "0.5.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "stegano-core") (r "^0.5.2") (d #t) (k 0)))) (h "0hgsgpsmf5h8s01gsf3z53fjkq9sv8knisa72jp3mgrq3vh8qr4f")))

(define-public crate-stegano-cli-0.5.3 (c (n "stegano-cli") (v "0.5.3") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "stegano-core") (r "^0.5.3") (d #t) (k 0)))) (h "09avhm667nd0i9cq8bpa2zrwrdxnzb83jlfbrd551d2hqy1kyhp7")))

