(define-module (crates-io st dw stdweb-internal-test-macro) #:use-module (crates-io))

(define-public crate-stdweb-internal-test-macro-0.1.0 (c (n "stdweb-internal-test-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "12rrm7p77xnm3xacgn3rgniiyyjb4gq7902wpbljsvbx045z69l2")))

(define-public crate-stdweb-internal-test-macro-0.1.1 (c (n "stdweb-internal-test-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0wx3jlm98qrg1pdw149fprzs9x3x3igqkm5ll23jv2v62yddfrjf")))

