(define-module (crates-io st dw stdweb-internal-runtime) #:use-module (crates-io))

(define-public crate-stdweb-internal-runtime-0.1.0 (c (n "stdweb-internal-runtime") (v "0.1.0") (h "1isihqbksp25a3wgr9gx8paar1ly43cghb48df9c3i05wanf74qf")))

(define-public crate-stdweb-internal-runtime-0.1.2 (c (n "stdweb-internal-runtime") (v "0.1.2") (h "1dcpha88d8qzjpx25hpvd5vwpwr7bywrimzlhwcyj1qd9kgfjkxp") (f (quote (("docs-rs") ("default"))))))

(define-public crate-stdweb-internal-runtime-0.1.3 (c (n "stdweb-internal-runtime") (v "0.1.3") (h "1nnbjsc2n5a8kgl960y7hgirm66zpl5n60m3s6r3fqsmxfig98m2") (f (quote (("docs-rs") ("default"))))))

(define-public crate-stdweb-internal-runtime-0.1.4 (c (n "stdweb-internal-runtime") (v "0.1.4") (h "1nhpyra7glbwcpakhpj5a3d7h7kx1ynif473nzshmk226m91f8ym") (f (quote (("docs-rs") ("default"))))))

(define-public crate-stdweb-internal-runtime-0.1.5 (c (n "stdweb-internal-runtime") (v "0.1.5") (h "1h0nkppb4r8dbrbms2hw9n5xdcs392m0r5hj3b6lsx3h6fx02dr1") (f (quote (("docs-rs") ("default"))))))

