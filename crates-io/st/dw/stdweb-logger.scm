(define-module (crates-io st dw stdweb-logger) #:use-module (crates-io))

(define-public crate-stdweb-logger-0.1.0 (c (n "stdweb-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)))) (h "0ysb6liya8h4ab9cr3riy32zjn7ds19dbqq3xcvrj833pggn654j")))

(define-public crate-stdweb-logger-0.1.1 (c (n "stdweb-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)))) (h "1p4sk5yv92hfg5clh6qs5164z5qvkhsfgqm5n4pyza4yi6aqf3n0")))

