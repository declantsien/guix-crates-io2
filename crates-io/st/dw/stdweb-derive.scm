(define-module (crates-io st dw stdweb-derive) #:use-module (crates-io))

(define-public crate-stdweb-derive-0.4.0 (c (n "stdweb-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0jxxn6vc0wba7fxh0mjghgmy7b46lhsxprks6a58l0pa72dnx93a")))

(define-public crate-stdweb-derive-0.5.0 (c (n "stdweb-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "02862lng655xc2zgxl2p0h7v0cp0jglc9j80xnq5fzphca3vm2q2")))

(define-public crate-stdweb-derive-0.5.1 (c (n "stdweb-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0c1rxx6rqcc4iic5hx320ki3vshpi8k58m5600iqzq4x2zcyn88f")))

(define-public crate-stdweb-derive-0.5.2 (c (n "stdweb-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "06qv17pp8myab8lqna2v082mrx5m900h79543kmn197fvwj67snv")))

(define-public crate-stdweb-derive-0.5.3 (c (n "stdweb-derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1vsh7g0gaxn4kxqq3knhymdn02p2pfxmnd2j0vplpj6c1yj60yn8")))

