(define-module (crates-io st us stusb4500) #:use-module (crates-io))

(define-public crate-stusb4500-0.1.0-beta (c (n "stusb4500") (v "0.1.0-beta") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1wn6vjvyfpr6vpbll6m6ndpkrws6hd29jib75zlbg7h1l5l9r7gk")))

(define-public crate-stusb4500-0.1.0 (c (n "stusb4500") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0xhmsw6f23yv03d58yhr6m2hqi2rm353mzv07i2ybai6pn0jjf01")))

