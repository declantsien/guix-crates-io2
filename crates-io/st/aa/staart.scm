(define-module (crates-io st aa staart) #:use-module (crates-io))

(define-public crate-staart-0.1.4 (c (n "staart") (v "0.1.4") (h "0bxnj02gzicqv00wv427r2sc5h8zw4pjkj1yhrc3g943qxz5laqz")))

(define-public crate-staart-0.1.5 (c (n "staart") (v "0.1.5") (h "0x4gxlyrs6jsm7zw64bsjrj13m8jxc75nicgm0y5d41s06mkzavy")))

(define-public crate-staart-0.1.6 (c (n "staart") (v "0.1.6") (h "0h2slh88z6fvkxpcfkpm4kqzqdn82nvif79wlj64pfls5rr6s0s2")))

(define-public crate-staart-0.1.7 (c (n "staart") (v "0.1.7") (h "06a0bxj6grwih6kcf1r84ykdzmj408xmdvbknr34xy72mzdw7ljf")))

(define-public crate-staart-0.1.9 (c (n "staart") (v "0.1.9") (h "10hy3p0l614ipbf05f5ph78d4vkjd7fzkff6fr5s62666i0l41y1")))

(define-public crate-staart-0.2.0 (c (n "staart") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gg3dgb00dnfzf62ids5hcqs7i5rnv0v5ph1742kzw45m6ix0r1z") (y #t)))

(define-public crate-staart-0.2.1 (c (n "staart") (v "0.2.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "02gda7p7bjarhf71n4vrhxh82mwjfjhm2q9d3qyvv684hgpf3cg0")))

(define-public crate-staart-0.3.0 (c (n "staart") (v "0.3.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1dpsd000q1g0kylabmw18xl7xyvsva0vk82yqxbw9gv89ypwzmx2")))

(define-public crate-staart-0.3.1 (c (n "staart") (v "0.3.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1w3w4fwqlbhlm177dk3x1w9w1mbkh62pjyiq0zi2m7fwnzhwyk4r")))

(define-public crate-staart-0.4.0 (c (n "staart") (v "0.4.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1vfpanpdv57nx4q4psy1q4al7szvi4k4az161nqzad9gc6avx8ra")))

(define-public crate-staart-0.5.0 (c (n "staart") (v "0.5.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12w0dr37lz32ck57gvg9x4vcmdh6ffjfks99k7mabgyw8xa87g6l")))

(define-public crate-staart-0.6.0 (c (n "staart") (v "0.6.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "139l02zklbclnnx34vxl2065wlw7nk1zi24mzz95fgnhxny1gl0g")))

(define-public crate-staart-0.7.0 (c (n "staart") (v "0.7.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13nnvpdxqrl0m5b0ziklyp419qvxwbriyvrkqrq0ih5fgwxhmwnk")))

(define-public crate-staart-0.7.1 (c (n "staart") (v "0.7.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1hzww9nlx2hr80m2v3gjvd8jdjgj6kiwa89wad6yiacf7piz2alp")))

(define-public crate-staart-0.7.2 (c (n "staart") (v "0.7.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1l25mm879qqfrjy147fpmawi0s15563l2ac257iqpx2ijg4blrb6")))

