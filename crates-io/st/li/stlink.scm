(define-module (crates-io st li stlink) #:use-module (crates-io))

(define-public crate-stlink-0.1.0 (c (n "stlink") (v "0.1.0") (d (list (d (n "arm-memory") (r "^0.1.0") (d #t) (k 0)) (d (n "coresight") (r "^0.1.0") (d #t) (k 0)) (d (n "debug-probe") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rental") (r "^0.5.4") (d #t) (k 0)) (d (n "rusb") (r "^0.5.1") (d #t) (k 0)) (d (n "scroll") (r "^0.9.2") (d #t) (k 0)))) (h "1hs7589wg0brc15jq81gdxanala5s41j5ma9dqfdwjdgmd84kh99") (y #t)))

