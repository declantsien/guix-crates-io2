(define-module (crates-io st li stlink-tool-rs) #:use-module (crates-io))

(define-public crate-stlink-tool-rs-0.1.0 (c (n "stlink-tool-rs") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ecb") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)))) (h "0h7bgkn92h78nik4csr7mw1g7ydzl90zs4yn2rg4j8a3ykwlzgji")))

(define-public crate-stlink-tool-rs-0.2.0 (c (n "stlink-tool-rs") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ecb") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)))) (h "062513iwhm5w2hdjf67g7zv7lqc5jxs9q9cizp79dvigscw8npj9")))

