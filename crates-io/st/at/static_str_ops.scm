(define-module (crates-io st at static_str_ops) #:use-module (crates-io))

(define-public crate-static_str_ops-0.1.0 (c (n "static_str_ops") (v "0.1.0") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "03dmmbq2j7a3g6sv2v0nr36dgcd20jajg63xisrrzyp8kbgcb601")))

(define-public crate-static_str_ops-0.1.1 (c (n "static_str_ops") (v "0.1.1") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0w9dccvh6ia393lk561wy12zvyg4z4dgrbfar50dij3mb736qy3r")))

(define-public crate-static_str_ops-0.1.2 (c (n "static_str_ops") (v "0.1.2") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1m277aqz7irxyxff6kz2hg8kril4wzjjgw82cf1h6kdwh293iwpf")))

