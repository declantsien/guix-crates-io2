(define-module (crates-io st at statics) #:use-module (crates-io))

(define-public crate-statics-0.1.0 (c (n "statics") (v "0.1.0") (d (list (d (n "cargo-watch") (r "^6") (d #t) (k 2)))) (h "1g3ngw4ksla55rhyga8lim27ppg0s7d2wnf5fkviqjhphhyf8yqr")))

(define-public crate-statics-0.1.1 (c (n "statics") (v "0.1.1") (d (list (d (n "cargo-watch") (r "^6") (d #t) (k 2)))) (h "1p4vkrd59x6iy25wd3s5bpb600si9x0nrp2j4i26lkfzw84k7gpz")))

(define-public crate-statics-0.2.0 (c (n "statics") (v "0.2.0") (d (list (d (n "cargo-watch") (r "^6") (d #t) (k 2)) (d (n "extra-asserts") (r "^0.1") (d #t) (k 2)))) (h "02s3s3s05dy1bjcifsipcrahws9k30pdsjfr98wbanylm2m0lql3")))

