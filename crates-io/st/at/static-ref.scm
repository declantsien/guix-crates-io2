(define-module (crates-io st at static-ref) #:use-module (crates-io))

(define-public crate-static-ref-0.1.0 (c (n "static-ref") (v "0.1.0") (h "0mfaqn61szrckp8xqc9aakqd6cnjxxqjnzw3fh9r62l7wf7bcpj8")))

(define-public crate-static-ref-0.1.1 (c (n "static-ref") (v "0.1.1") (h "1l1ld4ndw08m63f7lsgljhdbf9qcbh7k0kwmvmqbpg1szm4k7hfh")))

(define-public crate-static-ref-0.2.0 (c (n "static-ref") (v "0.2.0") (h "1n7aibdmc617i0z327alklbw4psn3gf1ihar2q79nmfsgzg4yf7x")))

(define-public crate-static-ref-0.2.1 (c (n "static-ref") (v "0.2.1") (h "0jj27paiwgpk9hmdj1jrki3kqfjw7ids9wzmapyq0zsvc5mc8gwr")))

