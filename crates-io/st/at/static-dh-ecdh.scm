(define-module (crates-io st at static-dh-ecdh) #:use-module (crates-io))

(define-public crate-static-dh-ecdh-0.1.0 (c (n "static-dh-ecdh") (v "0.1.0") (d (list (d (n "elliptic-curve") (r "^0.8.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "num-bigint-dig") (r "^0.6.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "p256") (r "^0.7.1") (d #t) (k 0)) (d (n "p384") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)))) (h "0n3g109dnkczvdndr80dg83322pw1a2nyzyiwdhhw3mwgm7lmcnv")))

(define-public crate-static-dh-ecdh-0.1.1 (c (n "static-dh-ecdh") (v "0.1.1") (d (list (d (n "elliptic-curve") (r "^0.8.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "num-bigint-dig") (r "^0.6.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "p256") (r "^0.7.1") (f (quote ("ecdsa"))) (d #t) (k 0)) (d (n "p384") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0d7a8zlpnhig510ywqjpzx79nxl70qr0x03yp9rw676arrv7jzxk")))

