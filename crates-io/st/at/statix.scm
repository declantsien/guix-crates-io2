(define-module (crates-io st at statix) #:use-module (crates-io))

(define-public crate-statix-0.1.0 (c (n "statix") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "12agd1irnxmvkwzly4zclyy3flgqa3qhglvi8xl0q0djlkcsjgxk")))

(define-public crate-statix-0.1.1 (c (n "statix") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "07gfwrk33h6zqrwb8rdm8dmkcnmlirf2c448wjxzzh6cxw48qhnc")))

(define-public crate-statix-0.1.2 (c (n "statix") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "044zai5smbcxbknfpih0ga7z4i273wrh11r3wcw2k5fmwnj7km5l")))

(define-public crate-statix-1.0.0 (c (n "statix") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "057np0giq6k1dkjxz2pxrgc2z8xczq99jzhkawgn072wwicm2pfy")))

(define-public crate-statix-2.0.0 (c (n "statix") (v "2.0.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "1sf020mvf94r9wkkwammdp4pc6x52v5mfvd875yfl22ijs7gazcw")))

(define-public crate-statix-3.0.0 (c (n "statix") (v "3.0.0") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "0ir9nq782nxlnvsi16pngcmkawl2ni2qgldrgv2y54rz8nzj37zi")))

(define-public crate-statix-4.0.0 (c (n "statix") (v "4.0.0") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "1gpipmdnypxvri7ksf8kfnn03hj0f0h7mp5j0py5fkw68h0jlsnm")))

(define-public crate-statix-4.0.1 (c (n "statix") (v "4.0.1") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "09m4rswfymyd1l3i7lmldskbn7i42jw2ls8n6pcn1305awrriysq")))

(define-public crate-statix-4.1.3 (c (n "statix") (v "4.1.3") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "1hy13ss21ifkyd4q1cqncsvwaqma8x4xm6xkmky17wa2b957gq7s")))

(define-public crate-statix-4.2.3 (c (n "statix") (v "4.2.3") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "0zi1r0ayhf0dp4yr8iff15h9rxqc09685l85kdwpkq45c4qkids7")))

(define-public crate-statix-4.3.3 (c (n "statix") (v "4.3.3") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "0gkm3jsbrhx981y3bhj6pd9d9k5062gfll70ggmlaij5hzac0riw")))

(define-public crate-statix-4.3.4 (c (n "statix") (v "4.3.4") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "1g8l27rycr7sx122m1qgga35sawk81dbpkkiwrv02bvykpk4v0i7")))

(define-public crate-statix-4.4.0 (c (n "statix") (v "4.4.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "0sqkc577qj0rwvk8bwjxancyhpv3gqjizq6zhiidx1jak6y4893n")))

(define-public crate-statix-4.5.0 (c (n "statix") (v "4.5.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "1ggjf0d20zdn5wjn9p5laik7mi8bmwsx52naf9jyb3p3k6s5nrrx")))

(define-public crate-statix-4.5.1 (c (n "statix") (v "4.5.1") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)))) (h "15wc34y326vz3hy5wygwpa10g6mvaq9iqm40bh8v7p14x1f59rsd")))

(define-public crate-statix-4.6.0 (c (n "statix") (v "4.6.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.6.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "0hwyfhnbmza5wmgppyi7cdjn0ghixflbhgv5qp8g105927qf9ff4")))

(define-public crate-statix-4.6.1 (c (n "statix") (v "4.6.1") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.6.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "1ln4wpbpj6rpgyc0bphhifc1757786ryf9xkqk2imcp01l9bbahb")))

(define-public crate-statix-4.6.2 (c (n "statix") (v "4.6.2") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.6.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "1ijnjwzf3a8y4j57gkyr1fijicj38jd8vkcfw1lvm0v7a3bfqn4h")))

(define-public crate-statix-4.7.0 (c (n "statix") (v "4.7.0") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.6.0") (d #t) (k 0)) (d (n "istor") (r "^0.1.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "173wrkm0rscniv0y3l4r3y5daxqqdz3scgrd521lnn00khnr8yi2")))

(define-public crate-statix-4.7.1 (c (n "statix") (v "4.7.1") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.6.0") (d #t) (k 0)) (d (n "istor") (r "^0.1.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "1dw8kj7s0vwrc416alvzlbivn591v7jkiyrisl0dz7d13x7vi8dl")))

(define-public crate-statix-4.7.2 (c (n "statix") (v "4.7.2") (d (list (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "infer") (r "^0.6.0") (d #t) (k 0)) (d (n "istor") (r "^0.1.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "1a8ddps7fl71v12hcr0p6wr0i54r3iy2x2raxcjpr1hvrj35kl2s")))

(define-public crate-statix-4.8.0 (c (n "statix") (v "4.8.0") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "infer") (r "^0.6.0") (d #t) (k 0)) (d (n "istor") (r "^0.1.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "1zqi2pkdqbclq6mcrn5slzy4kbqlwdgcx5vyj0bz3bryga0xyk7f")))

(define-public crate-statix-4.8.1 (c (n "statix") (v "4.8.1") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "infer") (r "^0.7.0") (d #t) (k 0)) (d (n "istor") (r "^0.1.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "087iffk5xvv7v2pkivxmq4bhl1znz25rf81k43md8d5w9n2wr280")))

(define-public crate-statix-4.8.2 (c (n "statix") (v "4.8.2") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "infer") (r "^0.7.0") (d #t) (k 0)) (d (n "istor") (r "^0.2.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.10") (d #t) (k 0)))) (h "1h035zxrszdhr4zrjsd2fd4zhnqw7f02d4wmvksd5z5r7bpbqrm8")))

(define-public crate-statix-4.8.3 (c (n "statix") (v "4.8.3") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "infer") (r "^0.7.0") (d #t) (k 0)) (d (n "istor") (r "^0.2.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11") (d #t) (k 0)))) (h "01xwdkihv5yjlnx1g6jplly09jmmrnhbl1vvzv5mmpld5viva0bf")))

