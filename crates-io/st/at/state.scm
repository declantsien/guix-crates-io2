(define-module (crates-io st at state) #:use-module (crates-io))

(define-public crate-state-0.1.0 (c (n "state") (v "0.1.0") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1f03dyzyz9idiyhijn94i0zl663xj6h5f4yz125yxi2lzy8f0b93") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.1.1 (c (n "state") (v "0.1.1") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1rp8g80d76pcr51a517jxszf1qa1626jghkc44d8c5kj774drng9") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.2.0 (c (n "state") (v "0.2.0") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "057krhwj835z12cacpamkb25njr3m16ch7m0zidfnhvi3ajwsbk7") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.2.1 (c (n "state") (v "0.2.1") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "02kaax0iczgay2jz2mlpi9b010acizs3phnc8qayiifm9b5xij3h") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.2.2 (c (n "state") (v "0.2.2") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "03gs971lhxd8p1a1nx5c334g50wmm5djqbg3nqk1vpx4gxcad9wr") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.3.0 (c (n "state") (v "0.3.0") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1nh4k0p3zjsrgzwcl0n5vwc22czwj8bdmwraz17f6ff5scv0hwnz") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.3.1 (c (n "state") (v "0.3.1") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "04lhaywl5d2xj74ggdy6rpag0cjc1i1jl9hprfmm31y6jgqxs19j") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.3.2 (c (n "state") (v "0.3.2") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "069rlx0a8p74k14556gwynns280p7zl0myldzpy1la3a28llxixc") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.3.3 (c (n "state") (v "0.3.3") (d (list (d (n "thread_local") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1nbqyz55kvp41i6xyyjykszkdayd5clx9c9yr3fpi1snamq2kzp2") (f (quote (("tls" "thread_local"))))))

(define-public crate-state-0.4.0 (c (n "state") (v "0.4.0") (d (list (d (n "thread_local") (r "<= 0.3.3") (o #t) (d #t) (k 0)))) (h "1hhl2w642svphgvmj3dq7l3gawz7ihllwsyg3jd3vzl5jp2jlmnm") (f (quote (("tls" "thread_local") ("const_fn"))))))

(define-public crate-state-0.4.1 (c (n "state") (v "0.4.1") (d (list (d (n "thread_local") (r "<= 0.3.3") (o #t) (d #t) (k 0)))) (h "0a4h14dfw65lik55524bzh23dc2yl685k9q3s7dzy8ggs5qwjibk") (f (quote (("tls" "thread_local") ("const_fn"))))))

(define-public crate-state-0.4.2 (c (n "state") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)))) (h "10v4k9bgjryc9m40c8nnhyrby2ngkhpx841p3k4halgxlp8af59h") (f (quote (("tls" "lazy_static") ("const_fn"))))))

(define-public crate-state-0.5.0 (c (n "state") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0294ipdgzbsg5na6485wcw0kblvf9f4ipjsg5nmxjn7zadd0r5fa") (f (quote (("tls" "lazy_static"))))))

(define-public crate-state-0.5.1 (c (n "state") (v "0.5.1") (d (list (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0av3dnazx9haf0mbjpdd16czmhfnkvxf2j7fb55bhk8rcclw4m0b") (f (quote (("tls"))))))

(define-public crate-state-0.5.2 (c (n "state") (v "0.5.2") (d (list (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1rbd5zg3zsj95di88h4my346llaiyj89cp1nbr5h9lz6d59lzkw7") (f (quote (("tls"))))))

(define-public crate-state-0.5.3 (c (n "state") (v "0.5.3") (d (list (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0fzji31ijbkimbzdy4dln9mp5xp7lm1a0dnqxv4n10hywphnds6v") (f (quote (("tls"))))))

(define-public crate-state-0.6.0 (c (n "state") (v "0.6.0") (d (list (d (n "loom") (r "^0.5") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1n3n2h324h1y5zhaajh6kplvzfvg1l6hsr8siggmf4yq8m24m31b") (f (quote (("tls"))))))

