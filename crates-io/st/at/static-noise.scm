(define-module (crates-io st at static-noise) #:use-module (crates-io))

(define-public crate-static-noise-0.1.0 (c (n "static-noise") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "05zwdxxavg7n3ca61q64chflwazmv191akff0d6zvhng2zinykqx")))

(define-public crate-static-noise-0.1.1 (c (n "static-noise") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hblbmda83453sxki8sm5lvpakmyalrxb982mzwkwr9mkz84yi70")))

