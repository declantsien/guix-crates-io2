(define-module (crates-io st at static-rc) #:use-module (crates-io))

(define-public crate-static-rc-0.1.0 (c (n "static-rc") (v "0.1.0") (h "0p58migmkfslcaajn27201ydmzj8wldwqv9nmh2giskwx4fizxld") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("compile-time-ratio"))))))

(define-public crate-static-rc-0.2.0 (c (n "static-rc") (v "0.2.0") (h "12ssjfnwzn74i6wh6gil3r0na1qa3394l44aj4bminfxhsl70mzc") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.2.1 (c (n "static-rc") (v "0.2.1") (h "0zfw2w1n0ghwaz78bxvr43mz3383msf0bj5908kfpr21bcg70nxs") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.2.2 (c (n "static-rc") (v "0.2.2") (h "10404kznsa6wyk982zaqj095cv4rdwhi76xxc730m8dx3m2996mr") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.3.0 (c (n "static-rc") (v "0.3.0") (h "1jyrhv2qghmz0c6616ng2qdj6wvbsazc4h12lvn9dr33fw8q0dsk") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("experimental-lift") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.3.1 (c (n "static-rc") (v "0.3.1") (h "1gl542fb1wcjvd4kvp3m952fc82fv01sd3jf84236dbd3nn1w38r") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("experimental-lift") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.4.0 (c (n "static-rc") (v "0.4.0") (h "137hvxrm55c9dmgpqyw9ci9i91nczwazmhi336yqvls4rxsncmwq") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("experimental-lift") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.4.1 (c (n "static-rc") (v "0.4.1") (h "0g7k28y456kcaq1kh66n7q061mccs4mfn06s7k3n6jbc6ns99mw0") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("experimental-lift") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.5.0 (c (n "static-rc") (v "0.5.0") (h "1zz8aklx256gdgjm7idwjfww32n2w49jbbwpw0az2nm0jbkxidw3") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-stream") ("experimental-lift") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.6.0 (c (n "static-rc") (v "0.6.0") (h "1ahwfqzyhc6b62p1kan0rxqi0ivzskpjvnwih3az8snvzgm6q7ma") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-iterator") ("experimental-lift") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

(define-public crate-static-rc-0.6.1 (c (n "static-rc") (v "0.6.1") (h "0m76629zbf7zm8isn6mzq0d12w154a1rz49h9fifv2mjlw2027dr") (f (quote (("nightly-generator-trait") ("nightly-dispatch-from-dyn") ("nightly-coerce-unsized") ("nightly-async-iterator") ("experimental-lift") ("default" "alloc") ("compile-time-ratio") ("alloc"))))))

