(define-module (crates-io st at stateroom) #:use-module (crates-io))

(define-public crate-stateroom-0.2.0 (c (n "stateroom") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qbc7cfajvyp77q8wzbh7aa76gaza7qv8ljb2p4f417n4y117h39") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.1 (c (n "stateroom") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ws2kjapy9745k1zwckz479qnzx4i0ww3g5g0y1asqy83cnw9f0g") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.2 (c (n "stateroom") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08mjwcr4qsmccvhay6r8h354zx67j6krwyjcdyg4gkal7ra8d2wx") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.3 (c (n "stateroom") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "107fhvppbqrrhwaywbq8wc4kbm3prbg76sjzwn5l5019ihb8cris") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.4 (c (n "stateroom") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09hgxgfqcdarkni40yhnh8c1avxb130kkr2sb6kffm3bypb542xw") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.5 (c (n "stateroom") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p1p39hrdh2j2wysds9kd1qxv1q1cry71lwjpbc20da6n8yza7bg") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.6 (c (n "stateroom") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sr7maby6ng8jl2sigmsxl2zmmrak179wvx55qpvv8nv199blx5b") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.7 (c (n "stateroom") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n9xbc9qb926px711pjhkwl9pdyh2h99ycjx8xcdllpkmcym7zn8") (f (quote (("default"))))))

(define-public crate-stateroom-0.2.8 (c (n "stateroom") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z7l3s3vrfdj5jwi42y9zqz6ih2wfndfyswxf1hqmiqn9yb3lqhs") (f (quote (("default"))))))

(define-public crate-stateroom-0.4.0 (c (n "stateroom") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ajmsv0gg3061sbz6akscvbi9sga2xxczljjlv56l2f6hz35pcg3") (f (quote (("default"))))))

(define-public crate-stateroom-0.4.1 (c (n "stateroom") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06mkvy2jfv6y813j23gws7glb3h36vmw70qm6g9jp33acbvdvmv0") (f (quote (("default"))))))

(define-public crate-stateroom-0.4.2 (c (n "stateroom") (v "0.4.2") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00hx9jm5d9qfxp7dd46ky7i8rjxfc0a29989k6j6bg1dvw8xpcql") (f (quote (("default"))))))

(define-public crate-stateroom-0.4.3 (c (n "stateroom") (v "0.4.3") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vrh7ajwcfyyn5avc6j84w6bj4k0smz6nfhdwqlqzb3ggiikynll") (f (quote (("default"))))))

