(define-module (crates-io st at statuses) #:use-module (crates-io))

(define-public crate-statuses-0.1.0 (c (n "statuses") (v "0.1.0") (d (list (d (n "gjson") (r "^0.8") (d #t) (k 0)))) (h "1xixglymla4v739sz357cpwfw9pz6hr53d8gvbwbh5sxas26i7yf")))

(define-public crate-statuses-0.1.1 (c (n "statuses") (v "0.1.1") (d (list (d (n "gjson") (r "^0.8") (d #t) (k 0)))) (h "1zhhig3881630i2s68iz882akria21x1ga2npm2dln2lrfr9d275")))

(define-public crate-statuses-0.1.2 (c (n "statuses") (v "0.1.2") (d (list (d (n "gjson") (r "^0.8") (d #t) (k 0)))) (h "1jivzs3g8kpv8a8k96605hawx7g480gsvhh9p6nda3grbk0z0pzq")))

