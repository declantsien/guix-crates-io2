(define-module (crates-io st at static_map) #:use-module (crates-io))

(define-public crate-static_map-0.1.0 (c (n "static_map") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.1") (d #t) (k 0)))) (h "17j6axf3b68967fmlbvv4hwwn40prky1ngm0kr7vzd78p8lhpll8")))

(define-public crate-static_map-0.1.1 (c (n "static_map") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.1") (d #t) (k 0)))) (h "06qakjf6b45wg39g6zl8vjqcfsziz8wrdhc62d8cpvk2alr1i83v")))

(define-public crate-static_map-0.2.0-alpha (c (n "static_map") (v "0.2.0-alpha") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "1i9sga29hkyyhda4akakwrfp482g6a6wbj582bpzp55382ny203p") (y #t)))

(define-public crate-static_map-0.2.0-beta (c (n "static_map") (v "0.2.0-beta") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1amlbn399f67c4wgqih3sxj218lwfni015ir6jlc866k31sn1pl5")))

