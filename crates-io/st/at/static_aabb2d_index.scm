(define-module (crates-io st at static_aabb2d_index) #:use-module (crates-io))

(define-public crate-static_aabb2d_index-0.1.0 (c (n "static_aabb2d_index") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0ilc617qivjbzv2r6z0fdwmw7x0qhwd36ic0i7rjav7wx0zqbab7") (f (quote (("allow_unsafe"))))))

(define-public crate-static_aabb2d_index-0.2.0 (c (n "static_aabb2d_index") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0nc8c8yysf1bz1c10xx4bk8b5bsvjrl3s0vwnwk08365izfcgqdd") (f (quote (("allow_unsafe"))))))

(define-public crate-static_aabb2d_index-0.3.0 (c (n "static_aabb2d_index") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0byg01nmprz5hmh6mixy502naf5mb2q4sgd31m9i5dj87sgd6wq8") (f (quote (("allow_unsafe"))))))

(define-public crate-static_aabb2d_index-0.4.0 (c (n "static_aabb2d_index") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "19pvmj5pzw2xwxwm8016imw8gks816bn6j7ifvpccsp0ps7620cd") (f (quote (("allow_unsafe"))))))

(define-public crate-static_aabb2d_index-0.5.0 (c (n "static_aabb2d_index") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1abcv35y98cr3b68bqprz65k21mpxpx66pg4zbsgkpv2wm0svbmn") (f (quote (("allow_unsafe"))))))

(define-public crate-static_aabb2d_index-0.5.1 (c (n "static_aabb2d_index") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "17wv75clzp17ykr3xh352l5gm55wg9wjhwk0cs2y5k113r686hil") (f (quote (("allow_unsafe"))))))

(define-public crate-static_aabb2d_index-0.6.0 (c (n "static_aabb2d_index") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xk6lznkgbbb6ylhgagz2nm5ysz03ds7kndncp1h35h1hapk51ya") (f (quote (("allow_unsafe"))))))

(define-public crate-static_aabb2d_index-0.7.0 (c (n "static_aabb2d_index") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1f1jhb7fi39bvsydzqww3lz3whsary58w9jc64anibz9z3qqvsnz") (f (quote (("unsafe_optimizations"))))))

(define-public crate-static_aabb2d_index-0.7.1 (c (n "static_aabb2d_index") (v "0.7.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zwaxmfn9fsf32mwj947nm2hzkqbndcrx8m4skdn4g8dkwxa61bj") (f (quote (("unsafe_optimizations"))))))

(define-public crate-static_aabb2d_index-1.0.0 (c (n "static_aabb2d_index") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hri23mmy55qgpsggxb6knczk26bkrilj98nsh4n49q1984qf7z5") (f (quote (("unsafe_optimizations"))))))

(define-public crate-static_aabb2d_index-1.1.0 (c (n "static_aabb2d_index") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1swakq1dl97mrmlzsk1hfpbm5x09h8pfkbd8xwrd5f8pwb3hxam8") (f (quote (("unsafe_optimizations"))))))

(define-public crate-static_aabb2d_index-2.0.0 (c (n "static_aabb2d_index") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jcq4zjkkbccczs2mlvva8cd797am8hxjyqm1p35s8m2awqbs10j") (f (quote (("unsafe_optimizations"))))))

