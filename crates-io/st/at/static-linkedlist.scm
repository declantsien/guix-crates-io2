(define-module (crates-io st at static-linkedlist) #:use-module (crates-io))

(define-public crate-static-linkedlist-0.1.0 (c (n "static-linkedlist") (v "0.1.0") (h "0mhr47xsqfqpmrgqyrnw6xhxl18inwy67l8d82a9nj5b0b8j811s")))

(define-public crate-static-linkedlist-0.1.1 (c (n "static-linkedlist") (v "0.1.1") (h "1p4qpsj76wyyriwfb8csp6p8wizkz93qg8661jcwx8wcca2hc6fk")))

(define-public crate-static-linkedlist-0.1.2 (c (n "static-linkedlist") (v "0.1.2") (h "0z2h9zh86ms1ab8b0ak9bqvwsd1nhcgfivjd63mank0c9xaqbj68")))

