(define-module (crates-io st at static-buffer) #:use-module (crates-io))

(define-public crate-static-buffer-0.1.0 (c (n "static-buffer") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.2.0") (d #t) (k 0)) (d (n "typenum") (r "^1.2.0") (d #t) (k 0)))) (h "105g7ximnmknifrxmzm3r6ylfan8973iql6xxbkwpgs4wv3gvs4r")))

(define-public crate-static-buffer-0.2.0 (c (n "static-buffer") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.2.0") (d #t) (k 0)) (d (n "typenum") (r "^1.2.0") (d #t) (k 0)))) (h "0dvlfg81r1x5panwdi4s6lscjmxb570w5plhxp9p5cyl8xiak9kd")))

