(define-module (crates-io st at static-context) #:use-module (crates-io))

(define-public crate-static-context-0.1.0 (c (n "static-context") (v "0.1.0") (h "1xan3l6vmcs06v7zxy4s2b15dm0vjv35y222igblrwwcza3ci5hl")))

(define-public crate-static-context-0.2.0 (c (n "static-context") (v "0.2.0") (h "0ix40dq7yzd9r6j4msaj300davd94mp3xifi6x394pfsfynvzq50")))

(define-public crate-static-context-0.3.0 (c (n "static-context") (v "0.3.0") (h "0lvnbkn92725vpch4cm60p2wydn1rbj8aj3z5vxfdnsc86s5g0qr")))

(define-public crate-static-context-0.4.0 (c (n "static-context") (v "0.4.0") (h "0jgr2x99fhlhk4c65cva6swm8lj8lml36np06qjal9d3nnyl4h7y")))

