(define-module (crates-io st at status) #:use-module (crates-io))

(define-public crate-status-0.0.1 (c (n "status") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1bj0zk33qsd3wf3zsjw67ywf4cazqi9402vx95sjfzia6i7xxckk") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.2 (c (n "status") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "15g7sn9klw8gvk7rv37iqyba45ql9ny3cp6kg1yk2lmaid8ilx84") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.3 (c (n "status") (v "0.0.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "001dh210i47n22mazfn2bavhqrkcvaz37vzfr7bpydazx62shjw8") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.4 (c (n "status") (v "0.0.4") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0bwljgpkz7yfrp5p7m8m7d1m413hpmymc0m80xqbx8pxyzz12wkm") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.5 (c (n "status") (v "0.0.5") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1mvyi9ah0dc6jrq6cagl8khlq3ai1gnb804mhks3l7fsr5rbpq0s") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.6 (c (n "status") (v "0.0.6") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "11zhv64r1qkhb0sg08zsrmksdr0c7z5ff7719qawyqaarz66navj") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.7 (c (n "status") (v "0.0.7") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1hs7h7jd5ksvbdhr6kyf7jz1asxnjdwzq33krzc7bz9a7kk0cv4g") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.8 (c (n "status") (v "0.0.8") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1757xvll3s9vfsbnhwpch623ccxw3mpjvg61fjjlaqv5kf4wmf7d") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.9 (c (n "status") (v "0.0.9") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1fmlq2z01p72n1bc27pixi0341a5gw9j53w30z06gnwkbd4x9hw4") (f (quote (("std") ("send_sync") ("default" "std"))))))

(define-public crate-status-0.0.10 (c (n "status") (v "0.0.10") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1yclqmv2260iajfp98b5d3bs5vgzmbfv08i7k94rxasq7pdydxrn") (f (quote (("std") ("send_sync") ("default" "std"))))))

