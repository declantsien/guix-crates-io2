(define-module (crates-io st at static-map-macro) #:use-module (crates-io))

(define-public crate-static-map-macro-0.1.0 (c (n "static-map-macro") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w4s1frgqn7gfgkhgzhrswhm31gpvgs3xih3hjl8lkmd3x8l86kd")))

(define-public crate-static-map-macro-0.2.0 (c (n "static-map-macro") (v "0.2.0") (d (list (d (n "pmutil") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a4d5kvpfy645lqwl90drfydbj95d36md0g2mrp2r9gfs0hywckf")))

(define-public crate-static-map-macro-0.2.1 (c (n "static-map-macro") (v "0.2.1") (d (list (d (n "pmutil") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zcm28d46dggdpbn06xlpa274z25l228cmzxpc8qh8s8y43kwl6m")))

(define-public crate-static-map-macro-0.2.2 (c (n "static-map-macro") (v "0.2.2") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10hvckpa178rvzxsf5h3y4f8zqr9r9wcbrv8j6cvz38nyvarw18y") (y #t)))

(define-public crate-static-map-macro-0.2.3 (c (n "static-map-macro") (v "0.2.3") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0symhzz5kdliqdy60m9pmd9c6n0p76n4fpf5q7dpz4yqkkg689bm")))

(define-public crate-static-map-macro-0.2.4 (c (n "static-map-macro") (v "0.2.4") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rpaalzardzh2agsm6zps85x5bk54ypx2rlwi4w8pxl7dqq3ba3w")))

(define-public crate-static-map-macro-0.2.5 (c (n "static-map-macro") (v "0.2.5") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16c5xw3krap14d9mhb3z8vkh3x9k2kkf543q05dhixy9zfcdaqmq")))

(define-public crate-static-map-macro-0.3.0 (c (n "static-map-macro") (v "0.3.0") (d (list (d (n "pmutil") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1myh4k89xgp46igk4090i4z3didir2jl3nh38ggd6m95v45swa3n")))

(define-public crate-static-map-macro-0.3.1 (c (n "static-map-macro") (v "0.3.1") (d (list (d (n "pmutil") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0a36p68wja8i8lyllsi823knl6rnbszsjhds4w71c37g7l7isfkz")))

(define-public crate-static-map-macro-0.3.2 (c (n "static-map-macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wjw92xlbyd344gi389jiaaj0wgycljn6rlkbmbrgbp0p54lrl84")))

(define-public crate-static-map-macro-0.3.3 (c (n "static-map-macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0m44m5ny5p9rd0mkxkgsfynljla22ic8k1qnp78h6fhfgvm87x4w")))

