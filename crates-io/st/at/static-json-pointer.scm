(define-module (crates-io st at static-json-pointer) #:use-module (crates-io))

(define-public crate-static-json-pointer-0.1.0 (c (n "static-json-pointer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)))) (h "17yjnf2pqp6z6d30mmc1xn3r190ss2na2hikgyah37lasm547g3b")))

