(define-module (crates-io st at static_locks) #:use-module (crates-io))

(define-public crate-static_locks-0.1.0 (c (n "static_locks") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "scopeguard") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (k 0)))) (h "0a1fghiimwy0qkdvs494ih37dzbpbpb87a36h7minzbk1jznp2df") (f (quote (("serde_support" "parking_lot/serde" "serde") ("owning_ref_support" "owning_ref" "parking_lot/owning_ref") ("nightly" "parking_lot/nightly") ("default") ("deadlock_detection" "parking_lot/deadlock_detection"))))))

