(define-module (crates-io st at statemachine-macro) #:use-module (crates-io))

(define-public crate-statemachine-macro-0.1.0 (c (n "statemachine-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zf4217qfhag63npx9z89d55i0dy9ja292zxr04wc9ldsbymkhf4")))

(define-public crate-statemachine-macro-0.1.1 (c (n "statemachine-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dlvi0zbgng6k0r7dvsjdz989vf1ccnahl6s4830mb6gjcc6q15c")))

