(define-module (crates-io st at static-atom-build) #:use-module (crates-io))

(define-public crate-static-atom-build-0.1.1 (c (n "static-atom-build") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "1fxiawxnasxa2578naqaf7wymkv9rk537h77cpjqj6276lyl1j8y") (f (quote (("default" "serde"))))))

(define-public crate-static-atom-build-0.1.2 (c (n "static-atom-build") (v "0.1.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "0q1dbhgf75plq2bga1j2zcyi402vvfy0rkak7kj833f0dvafwq9c") (f (quote (("default" "serde"))))))

