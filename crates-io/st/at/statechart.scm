(define-module (crates-io st at statechart) #:use-module (crates-io))

(define-public crate-statechart-0.0.0 (c (n "statechart") (v "0.0.0") (h "00yncm3130fkyv2nrmxnkm2v1kb3lac707pzyzfi0ichv52mn6sf")))

(define-public crate-statechart-0.0.1 (c (n "statechart") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)))) (h "029wrj0q6mxsl67x6rxr4jkkqvgwwpssv7pkxdz501p398dr9z08")))

(define-public crate-statechart-0.0.2 (c (n "statechart") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)))) (h "1gy6ixvndhshgrzc8xkpsp8k8cgaw42v8gdn79zaxapv3b1q5lf6")))

(define-public crate-statechart-0.0.3 (c (n "statechart") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)))) (h "109krjd8xpfd8ks3z8p5fr363y0y6p5qcnz2hkbpqlk6lw9wn04m")))

(define-public crate-statechart-0.0.4 (c (n "statechart") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)))) (h "16xkf7wfvj84pcsz0pm9y7zx31s8z64flw4q1r1nzbfddl9wwn91")))

(define-public crate-statechart-0.0.5 (c (n "statechart") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1m1dix2v441gmc6072ba8ngl3hll2lp9vd2gkd87xl89kppbiwqf")))

(define-public crate-statechart-0.0.6 (c (n "statechart") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0s1q2fsvnlmgxnbw1f86kzb03skxyfk5fvnmir17100p01sc9lk8")))

(define-public crate-statechart-0.0.7 (c (n "statechart") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "04x7ixkkwxn7rmg6f9g1zx59h80vxdir3a4j6y1px8ryv42flm61")))

(define-public crate-statechart-0.0.8 (c (n "statechart") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1mvrzf46z037krinybgqzzdkxml26x3dh4b88gv176aw73ccgnbb")))

