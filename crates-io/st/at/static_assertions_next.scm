(define-module (crates-io st at static_assertions_next) #:use-module (crates-io))

(define-public crate-static_assertions_next-1.1.1 (c (n "static_assertions_next") (v "1.1.1") (d (list (d (n "proc_static_assertions_next") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0yjxy2k1p7b6vphc9cg3a6l3hcw1drkpjkxq3dff683c8rakww01") (f (quote (("proc" "proc_static_assertions_next") ("nightly"))))))

(define-public crate-static_assertions_next-1.1.2 (c (n "static_assertions_next") (v "1.1.2") (d (list (d (n "proc_static_assertions_next") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0rn7c362606jj1lp3ff3vsicjmpr2x1qra9zd25rlpjrh98sxgnp") (f (quote (("proc" "proc_static_assertions_next") ("nightly"))))))

