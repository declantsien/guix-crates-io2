(define-module (crates-io st at static-self-derive) #:use-module (crates-io))

(define-public crate-static-self-derive-0.1.0 (c (n "static-self-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "056czvwiq2cma4d6mqrcgp605c5lizv7si96pnxdjcllny0c0lxw")))

(define-public crate-static-self-derive-0.1.1 (c (n "static-self-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m95jhdlc7jm5n7isphscmcvbiyn48jlkn2jka55az4h9dnwjs2j")))

