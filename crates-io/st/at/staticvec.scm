(define-module (crates-io st at staticvec) #:use-module (crates-io))

(define-public crate-staticvec-0.1.0 (c (n "staticvec") (v "0.1.0") (h "1yal63pkk8bbl50lwpwa984wna2v4dcixdzz53cah352ds7a789y") (y #t)))

(define-public crate-staticvec-0.1.1 (c (n "staticvec") (v "0.1.1") (h "1b3mhppnlc7i6cihv0h5amziwpd1pkjwldv86202ndq1giwhljds") (y #t)))

(define-public crate-staticvec-0.1.2 (c (n "staticvec") (v "0.1.2") (h "0x6n0ra8jazxfp85bn11l0nh8b4hfphmalqqiqa0q96vqr21rr6d") (y #t)))

(define-public crate-staticvec-0.1.3 (c (n "staticvec") (v "0.1.3") (h "0rp62wf6prshzxn9d0fc0724n4bs0ynhafx1x8lwcghwcfjdrfnl") (y #t)))

(define-public crate-staticvec-0.1.4 (c (n "staticvec") (v "0.1.4") (h "08262hv97qipzr44as7k9w7r2ssafkm4jrr2cjzx99hddj5245k8") (y #t)))

(define-public crate-staticvec-0.1.5 (c (n "staticvec") (v "0.1.5") (h "03mc9c1p36w80hxgb3p4pxh2lmcrc9lncw66rxnczql1yjl43jm7") (y #t)))

(define-public crate-staticvec-0.1.6 (c (n "staticvec") (v "0.1.6") (h "0sv941nqjfd11nb5drsaclq6vlj6gbdxha9fxsm1zbapmcv4glqc") (y #t)))

(define-public crate-staticvec-0.1.7 (c (n "staticvec") (v "0.1.7") (h "1kh4k6h1crl0ndqapdy57rnj7s6g3gijcwqgp5xzndaj7jlmbczg") (y #t)))

(define-public crate-staticvec-0.1.8 (c (n "staticvec") (v "0.1.8") (h "1s005h7cp8sc3fis6zb06q669qa5k13h21clshia07wvb3iswf3x") (y #t)))

(define-public crate-staticvec-0.1.9 (c (n "staticvec") (v "0.1.9") (h "1f7warjhvbf459h1ryw05jlhsqs64dsg6pwcmdxpsbv7sfbsvhr6") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.1.10 (c (n "staticvec") (v "0.1.10") (h "1vfv1lvg7pv6p65psfz2h2nkh97w6mgx6msrsz0wfysml7bk369i") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.1.11 (c (n "staticvec") (v "0.1.11") (h "0hif3qrvhwkypmj92i1ckr4nsdlika8nairh0gfk6b4lcjbbyjcg") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.2.0 (c (n "staticvec") (v "0.2.0") (h "13w1ffsdpiaapl7fih7fc69vqxfrx3rbl99wrr4wv55v8wzsdrcc") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.1 (c (n "staticvec") (v "0.2.1") (h "0hm7pi5kg3j3dzizcfr97zfzg8rvq2a199jrjv5p9rmlikh6dhc9") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.2 (c (n "staticvec") (v "0.2.2") (h "0j0j8lcbcb17bxzrrh39bg30a990d98q85bpsbccrhadpafmq3p7") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.3 (c (n "staticvec") (v "0.2.3") (h "1jg4nns5cgvpk2zm9dym6hc49h2qm525fa6n11hmgzpckl3nqwwk") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.4 (c (n "staticvec") (v "0.2.4") (h "1vfig4s6rybjlhgzjbhkiy5j8mnx1x0wpm8cjba0m2brghshq80a") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.5 (c (n "staticvec") (v "0.2.5") (h "12qvgp78smzzj6xkpydvdvzycs327ay8rbh0qql67kgyr23yxdzh") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.6 (c (n "staticvec") (v "0.2.6") (h "166s04bzdg9y9hsi01ksqpl9mw1xnxgsvkl0w3f4mp61y27sfsh8") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.2.61 (c (n "staticvec") (v "0.2.61") (h "08hni0c182vskbmdc530y2r8qi47vjfl8nw6x73x7ydacvnzn09f") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.75 (c (n "staticvec") (v "0.2.75") (h "0mcid9yw7xlwqvigkbl2506hllg607xz5v4jjpr1vv3vgdialil0") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.76 (c (n "staticvec") (v "0.2.76") (h "1wqbgahsgj0sxyg2b7indyfn9995xzv4dmp3mhcdhbgskcma4jhg") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.2.80 (c (n "staticvec") (v "0.2.80") (h "00l4vyd5ghcsybix3wzi7gmb81qb7j9r2hjh1d00mah69psiii9s") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.3.0 (c (n "staticvec") (v "0.3.0") (h "19idkaqmqngfy5302cywqbdjy9l1yswc18j5id6qmm140mf4781l") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.3.0-docfix (c (n "staticvec") (v "0.3.0-docfix") (h "19ai1kncmb0b3fjjs0jjpr5yl5nnx49k26bx37gzgvd4nrcv54ij") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.3.1 (c (n "staticvec") (v "0.3.1") (h "1n37bb1hylnlpswvwm5prhzb3nja1kzyglahbk99496arcninp7n") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.3.2 (c (n "staticvec") (v "0.3.2") (h "0nlw16hhzi83hh3vkgfwl8jd9fxkr9bql3dcc0qfxc1bh6pw0gqs") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.3.3 (c (n "staticvec") (v "0.3.3") (h "1hnr2daca9jah4qw6gpk6q170g2cygn40k60alkiyr6qrxivydrb") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.3.4 (c (n "staticvec") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1gdy63libdir82hs5dkzi59150szg2js93q0kwrvjhdv45fjarks") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.3.5 (c (n "staticvec") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1d2xiviwj50430zrimdjsr9va8yrkq5m4jrv8k8lr9y2q4y48y8d") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.3.6 (c (n "staticvec") (v "0.3.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "023aj2vg03vd4b62vp938sl7bb2kx3s13395r06s8qxxlpgmpyb8") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.4.0 (c (n "staticvec") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00r0m3pvp46r5c0hycjng8qngacp4258f5rdw2fz0fm4gppwdnkx") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.4.1 (c (n "staticvec") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0n99hbidw71x7n4ihpws9xv27dqd2zkldlqwjbxk2zrmpsvz7fj3") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.4.2 (c (n "staticvec") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1nsap5x1bkh6lj05l212zjbzqg5b8nk81c7b9q1dl5bidxn546i8") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("deref_to_slice") ("default" "std"))))))

(define-public crate-staticvec-0.4.3 (c (n "staticvec") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "150sbskzv19pw43v7pq2r81y49ps51cvlccgf9qv08vvngyaq2nr") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("deref_to_slice") ("default" "std"))))))

(define-public crate-staticvec-0.4.4 (c (n "staticvec") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0z2kin9s4sqwhi1nk66mq9plql48mjkg1v5gcd27ipmich8q7kqz") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("deref_to_slice") ("default" "std"))))))

(define-public crate-staticvec-0.4.5 (c (n "staticvec") (v "0.4.5") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pdzmzk2p7fmpdz4ny6a5nzknb573cgai8wj1cjs8svvigaxblnq") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("deref_to_slice") ("default" "std"))))))

(define-public crate-staticvec-0.4.6 (c (n "staticvec") (v "0.4.6") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "197z1skvah0mln4ash0pb9dwx82mcs7dx0f2gfgxhi2c95f5ldzb") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.4.7 (c (n "staticvec") (v "0.4.7") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "132jhfwqi1ygcb7ihl8q4njwfjqlcq4nhrp3qaigkl2wl48394fx") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.4.7-docfix (c (n "staticvec") (v "0.4.7-docfix") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yjw0pd0px2rwxkg9jjcpcdqxm5wrm8qg2hf4w6hsv9nfsj8cg4w") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.4.8 (c (n "staticvec") (v "0.4.8") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01al6155zi201nrspz9pcrc08h8qy6hm3dk95lqchggyyyvdc8sk") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.4.9 (c (n "staticvec") (v "0.4.9") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1dfybr8ykd82cpvi0vhslwz6zwrss7s6jwl43byrj95zgx41915z") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.0 (c (n "staticvec") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fdwyb11y6ign9wsd6c82722lcm61dw6xcma7x2yygivncnpg26i") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.1 (c (n "staticvec") (v "0.5.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "071pn375wjx4w90dj1hxq5x9bv9xqfqpqp1hfljgzvc2pb5hpf5d") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.2 (c (n "staticvec") (v "0.5.2") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11v16q768bka51fzyc8jwpx42q47x9liz24c3w6yagik5ipnm89n") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.3 (c (n "staticvec") (v "0.5.3") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1cxbkigr5qamvx1xpjsl80i24z5m2xf7j1mv2yg5ifn1bq8hzl3g") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.4 (c (n "staticvec") (v "0.5.4") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0z8rpkh05vpbliq02m56dykd8f0kgh8i81rxci94rjl3im12zcg2") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.5 (c (n "staticvec") (v "0.5.5") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1rayn2n4vhwa7hynrc8das7drpha2kx9f2gqs9mzpm22430vslda") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.6 (c (n "staticvec") (v "0.5.6") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bx9xq7mzi38b5i0mi2vjw2s5njy6a5n54j0sllj5a46v4hxsmk1") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.5.7 (c (n "staticvec") (v "0.5.7") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14ic087zir16skk0pkiv9ibh7sfd8mdj0bfd7xlabr1vrmgmblcr") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.6.0 (c (n "staticvec") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0sf64zalagf7vwc10l8mr1mrdgqa4cvvrcsl0pna5k49pzmblk7k") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("repr_c") ("default" "std"))))))

(define-public crate-staticvec-0.6.1 (c (n "staticvec") (v "0.6.1") (d (list (d (n "arrayvec") (r "^0.5.1") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1v6k4n28zg3iiigvms8a1yfbcw89i5zlzx10sxf3c36kjjicz114") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("repr_c") ("default" "std"))))))

(define-public crate-staticvec-0.6.2 (c (n "staticvec") (v "0.6.2") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "127grd2wj0nia3whlhv1xq60qaphipwxywa6rhfwif3flrd4192y") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("repr_c") ("default" "std"))))))

(define-public crate-staticvec-0.6.3 (c (n "staticvec") (v "0.6.3") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18c7pv5jngdb44dyq1v68fs9iv7g3s27ka8gx21jz29grxlfh529") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("repr_c") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.6.4 (c (n "staticvec") (v "0.6.4") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "06vsg1p4gzhb23ry4bagl5qfadyyjrgia2jin4ym0fydbdc2msml") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("repr_c") ("default" "std"))))))

(define-public crate-staticvec-0.6.5 (c (n "staticvec") (v "0.6.5") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rgqxnlrq5r4m15awlqs1bmkazm26q6f0dr0cw3mgi5g816w95v1") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.6.6 (c (n "staticvec") (v "0.6.6") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lq1i66m33x9bqlqr58s1db3ficifhp2ycpq0mq93d6hx5l108ic") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.6.7 (c (n "staticvec") (v "0.6.7") (d (list (d (n "arrayvec") (r "^0.5") (f (quote ("array-sizes-33-128" "array-sizes-129-255"))) (d #t) (k 2)) (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1") (d #t) (k 0)))) (h "1ibxfasa13506yyxy7rvm8553y1rmdfrw1msf5wbfn7x9685hqmr") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.6.8 (c (n "staticvec") (v "0.6.8") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1") (d #t) (k 0)))) (h "0f64va28wb3lai6wrlmglwnwsshadarx8m2r6n8332ig4vlhw5k1") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.6.9 (c (n "staticvec") (v "0.6.9") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "1n6pqs40bcaxqhjpgyljldgbfrz4mm7p6rakjzp07hfdrhn8k6v0") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.6.91 (c (n "staticvec") (v "0.6.91") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "0vsvq81rkm3dmv0c2h915bhyx9q1jc85mqahdjfi3c0m6j2bbgvc") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.7.0 (c (n "staticvec") (v "0.7.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "0z1kamv3a6lv2xhp77561q04yw9z0cdy9f8qdmshvqpyrayn6jnr") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.7.5 (c (n "staticvec") (v "0.7.5") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "04y695jbjkdmjgxxrwipnr9w7bz0kl52q75adp3mrri2yjcr1zwm") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.7.6 (c (n "staticvec") (v "0.7.6") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "0vbpcz43kcbpd3jhc23yrf7vkfad832w0z2yzq481ckzc3sy89nj") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.7.7 (c (n "staticvec") (v "0.7.7") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "14dyv3y56h50172dk61c3lp0wwyp0chrl49zng1spl0xd4x3x4ks") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.8.0 (c (n "staticvec") (v "0.8.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "0ijyqfzr15hqjxii5mmclw8hfrg3iv4dvgnpzfwy2m53jg0c3sdv") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.8.1 (c (n "staticvec") (v "0.8.1") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "0cqif1g4bdgb8d7sl1ghrlb4ifyx1pm13rj9aj902a1sml9793n2") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.8.2 (c (n "staticvec") (v "0.8.2") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "1gh9dwgq6va36dhd9q0595i0mcfrxdwp11pf1iyjncp57ryacvgm") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.8.5 (c (n "staticvec") (v "0.8.5") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "1hapk0p0w7c1zlcdrdfm64bmjywg7c6lbjl8bqrp0yflq1f34vkr") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.8.6 (c (n "staticvec") (v "0.8.6") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "0z5xhimgccqz8l3hzhlqhambvhksg6vwjzgajl2f31ym83l9vbqy") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.8.7 (c (n "staticvec") (v "0.8.7") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "staticsort") (r "^0.1.4") (d #t) (k 0)))) (h "1q1zp6hm3abp88m3gr6jqih9amq7mhwx3hh5dnfg77w61pfwc739") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.8.8 (c (n "staticvec") (v "0.8.8") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13yxnzh40wh1baf5bll5fcyys166l6pv4hpiw79b90y75l7fjaa1") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.8.9 (c (n "staticvec") (v "0.8.9") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1brj46lkh8y3v930c5fk4c0pjhkzzl1cdbq4l5njg66qfl38p1dd") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.9.0 (c (n "staticvec") (v "0.9.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14g0a8cvvlhb5z2cb6qfx7zfj0yi14w6id9c7fx2cq06f5pi1735") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.9.1 (c (n "staticvec") (v "0.9.1") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0j0qm9h6s4fmphzi8r00jqrvn1xz1ihdafp2r3h7dg7vmpa4yh56") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.9.2 (c (n "staticvec") (v "0.9.2") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "19kh3bbhdlfimggxz5ppzrlc4bh9nr0if1432bm5q1d2cy5smzrk") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.9.3 (c (n "staticvec") (v "0.9.3") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0m8ldp180glz8fkqzgyanvjndfibvdm4ycncx9df55q64d4fhk7n") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.10.0 (c (n "staticvec") (v "0.10.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1bff2f89qzlqzl46yigd88cpmwm6bjfac75yh0c0db21hzanh2cc") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.10.1 (c (n "staticvec") (v "0.10.1") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14r04k5mipykd6bcqng1cfi02lm73wcqq5lrmnfiqqzz5jg8f0ms") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.10.2 (c (n "staticvec") (v "0.10.2") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b3x2dj0rykacplrkzj38yh6pfnc9dg9sf9k3jchcbixhxjqrgag") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.10.3 (c (n "staticvec") (v "0.10.3") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lmaqq5qvbd60j3ky99cyr802ns4n9cdg9h9f6b98gvqcjz9k1c8") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std")))) (y #t)))

(define-public crate-staticvec-0.10.4 (c (n "staticvec") (v "0.10.4") (d (list (d (n "cool_asserts") (r "^1.0.2") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "162xaqpffr91v5i9mmz1kjdkydk6s0p7x9761ifrnc59xq8cwlpz") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.10.5 (c (n "staticvec") (v "0.10.5") (d (list (d (n "cool_asserts") (r "^1.0.2") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fbh9phawr52kklb2ly421hbmn2sf64cl51qgwar95nq1yzg91rw") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.10.6 (c (n "staticvec") (v "0.10.6") (d (list (d (n "cool_asserts") (r "^1.0.3") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10qa8y7hdbyi4i2yxvrjaf2h3m8w7hi66k3hkjnf9f1kdfnxc6j9") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.10.7 (c (n "staticvec") (v "0.10.7") (d (list (d (n "cool_asserts") (r "^1.0.3") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ck2k5bwpnhk7v8hp82ksv2h1lh8f51n6xca81xr9asbj3rrm5cg") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.10.8 (c (n "staticvec") (v "0.10.8") (d (list (d (n "cool_asserts") (r "^1.0.3") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02by17kq2y730qsdps6mrrs2qby78ys3ykrn2g78nwn7jrkagdsy") (f (quote (("std") ("serde_support" "serde") ("serde_json_support" "serde_json") ("default" "std"))))))

(define-public crate-staticvec-0.11.0 (c (n "staticvec") (v "0.11.0") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1x7i1a3px3b2b7y4m06lw4zqlb82k6rsb7mkwn4anrx1pxzr949s") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.1 (c (n "staticvec") (v "0.11.1") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qhlagryhzff4vq9bdisl754kbkndm139lyd547fkp0fwz8hrkkf") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.2 (c (n "staticvec") (v "0.11.2") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1h0wvl88ggf9b0h7c26j32gbdd9b77xrfnnvbxj5aq1p4zp8nc6g") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.3 (c (n "staticvec") (v "0.11.3") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gn6qiahdzc8684mvqznakxwi1w2rh0y4fnmiggmqyss2c8l8l71") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.4 (c (n "staticvec") (v "0.11.4") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0h24x1iwbbqv2awxvrw3vp0zvg3d9a0z9vg8j5z565px9wq4fyfk") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.5 (c (n "staticvec") (v "0.11.5") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1whvggbmir6fpbx4pf5qklwb5kj7z1spbk3xhxidyv7xq6ml6gmf") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.6 (c (n "staticvec") (v "0.11.6") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jm9ypni4w39i2z98hvjcvwnl719wfxaa08z87qks30x78kjvfc2") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.7 (c (n "staticvec") (v "0.11.7") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jydaxmvsypyjryc993vhfbwk0vnjrmb15j9xza0gfqfa1kbwgmf") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.8 (c (n "staticvec") (v "0.11.8") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wr4cxyqn74n73kid2cqcghsg6p7pkwfk74ph0a0770sxx6n2zqj") (f (quote (("std") ("default" "std"))))))

(define-public crate-staticvec-0.11.9 (c (n "staticvec") (v "0.11.9") (d (list (d (n "cool_asserts") (r "^1.1.1") (d #t) (k 2)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12yhqg219mrr0ir6z9278lg9zz2vm7diwmlmd1cphgf0x11kj676") (f (quote (("std") ("default" "std"))))))

