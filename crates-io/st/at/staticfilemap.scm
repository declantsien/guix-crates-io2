(define-module (crates-io st at staticfilemap) #:use-module (crates-io))

(define-public crate-staticfilemap-0.1.0 (c (n "staticfilemap") (v "0.1.0") (d (list (d (n "minilz4") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0m7gymn5rpx19ckww5gc3wv92w1fz1jil309fqgdp57p5pzbqznr")))

(define-public crate-staticfilemap-0.1.1 (c (n "staticfilemap") (v "0.1.1") (d (list (d (n "minilz4") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0wf94nwp8hrhqqz4wmmsz8y9yh2jri96g298dhsv4ixwn2gjp3z1")))

(define-public crate-staticfilemap-0.1.2 (c (n "staticfilemap") (v "0.1.2") (d (list (d (n "minilz4") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1xnkhd1r140gyi5v739hkw998dzfrk54wmb2h0fimy8lydrnpl1s")))

(define-public crate-staticfilemap-0.1.3 (c (n "staticfilemap") (v "0.1.3") (d (list (d (n "minilz4") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.5") (d #t) (k 0)))) (h "099fm3vis7cyiwvfn6x9zwhk80da5lpj2y2jblbzrz17n6v1dkks")))

(define-public crate-staticfilemap-0.1.4 (c (n "staticfilemap") (v "0.1.4") (d (list (d (n "minilz4") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1q9v9awscmmjpszhyxq7pfg64fj23zw2xs3s58j4in8a3l17abgf") (f (quote (("lz4" "minilz4") ("default" "lz4"))))))

(define-public crate-staticfilemap-0.1.5 (c (n "staticfilemap") (v "0.1.5") (d (list (d (n "minilz4") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0gzh1imgpzjcv0g1p8337a6ivsx5cd5hb396rmk54vvry0gry894") (f (quote (("lz4" "minilz4") ("default" "lz4"))))))

(define-public crate-staticfilemap-0.2.0 (c (n "staticfilemap") (v "0.2.0") (d (list (d (n "minilz4") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1pkhi76yjx6hxagndswdjpbgwfyz5vp1nbkawwf27fdswd3in5r1") (f (quote (("lz4" "minilz4") ("default" "lz4"))))))

(define-public crate-staticfilemap-0.3.0 (c (n "staticfilemap") (v "0.3.0") (d (list (d (n "minilz4") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.10") (o #t) (d #t) (k 0)))) (h "07rfzsg8ji363k7b47r68nmlhlr7nbm5jibdvdy8x36gw49i9jn3") (f (quote (("lz4" "minilz4") ("default" "lz4"))))))

(define-public crate-staticfilemap-0.4.0 (c (n "staticfilemap") (v "0.4.0") (d (list (d (n "minilz4") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.11") (o #t) (d #t) (k 0)))) (h "070ryzlri5zv4f6ckff2pyvb89xpc0d3kc34qsdbkflcfa5lar1i") (f (quote (("lz4" "minilz4") ("default" "lz4"))))))

(define-public crate-staticfilemap-0.4.1 (c (n "staticfilemap") (v "0.4.1") (d (list (d (n "minilz4") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.11") (f (quote ("arrays"))) (o #t) (k 0)))) (h "1bmjqlp9zkhz00jmhiy4v03144mk8mgk881s8d8l614llabagabk") (f (quote (("multithread" "zstd/zstdmt") ("lz4" "minilz4") ("default" "lz4"))))))

(define-public crate-staticfilemap-0.5.0 (c (n "staticfilemap") (v "0.5.0") (d (list (d (n "minilz4") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12") (f (quote ("arrays"))) (o #t) (k 0)))) (h "0mjb813lplhz6xrhi2zvfmq69l1nhlnfwqi72ya3v0yd0yd3nply") (f (quote (("default" "zstd")))) (s 2) (e (quote (("zstd" "dep:zstd") ("multithread" "zstd?/zstdmt") ("lz4" "dep:minilz4"))))))

(define-public crate-staticfilemap-0.6.0 (c (n "staticfilemap") (v "0.6.0") (d (list (d (n "minilz4") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12") (f (quote ("arrays"))) (o #t) (k 0)))) (h "0wmn94k033zxyq1z8qyb4brml1rvz88965ckjqkrbkkf67zygp7m") (f (quote (("default" "zstd")))) (s 2) (e (quote (("zstd" "dep:zstd") ("multithread" "zstd?/zstdmt") ("lz4" "dep:minilz4"))))))

(define-public crate-staticfilemap-0.6.1 (c (n "staticfilemap") (v "0.6.1") (d (list (d (n "minilz4") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12") (f (quote ("arrays"))) (o #t) (k 0)))) (h "0i4qbckv019y719dch12k73zy71liyw7r7sb6cy8nmhg1c4nwd4y") (f (quote (("default" "zstd")))) (s 2) (e (quote (("zstd" "dep:zstd") ("multithread" "zstd?/zstdmt") ("lz4" "dep:minilz4"))))))

(define-public crate-staticfilemap-0.7.0 (c (n "staticfilemap") (v "0.7.0") (d (list (d (n "minilz4") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "zstd") (r "^0.13") (f (quote ("arrays"))) (o #t) (k 0)))) (h "0avk0n1yva6ahgrcszdb4fmx07v4957vyzxasvw4isgy73r94pq3") (f (quote (("default" "zstd")))) (s 2) (e (quote (("zstd" "dep:zstd") ("multithread" "zstd?/zstdmt") ("lz4" "dep:minilz4"))))))

