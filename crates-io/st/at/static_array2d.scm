(define-module (crates-io st at static_array2d) #:use-module (crates-io))

(define-public crate-static_array2d-0.1.0 (c (n "static_array2d") (v "0.1.0") (h "1fjfdymgy9s7nbld2hg37higm3cwaykn9qhabnz3s8s73qwm630d")))

(define-public crate-static_array2d-0.2.0 (c (n "static_array2d") (v "0.2.0") (h "01i2ln1v8phjfpcdbzqhbmxmk0rfh4zd9330y1n68pknasgf9k6v")))

