(define-module (crates-io st at stateful_async_worker) #:use-module (crates-io))

(define-public crate-stateful_async_worker-0.1.0 (c (n "stateful_async_worker") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^0.1") (d #t) (k 0)))) (h "1jvq91fnl37k0q17pl66djanwxxn6hvg4kjwj1q3xcv4c21bss0h")))

(define-public crate-stateful_async_worker-0.1.1 (c (n "stateful_async_worker") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^0.1") (d #t) (k 0)))) (h "0wix1bz25ijl5012666s06sb1dsgi4sx1v814n4qfasjabwi8vka")))

(define-public crate-stateful_async_worker-0.1.2 (c (n "stateful_async_worker") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^0.1") (d #t) (k 0)))) (h "01jhnlwxxhjraqhdh6qzgxhavzah8sq6fpxsh794dwqxkwnfigqg")))

