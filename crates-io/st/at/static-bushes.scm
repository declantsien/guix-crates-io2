(define-module (crates-io st at static-bushes) #:use-module (crates-io))

(define-public crate-static-bushes-0.1.1 (c (n "static-bushes") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1iqnnwdj2s2d4hg6mw3pnqfmq04r0gqqc6f0xgwzxxj87kiinnhv")))

(define-public crate-static-bushes-0.1.2 (c (n "static-bushes") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1p5l17skccbfp8dhagrx3956kygap3x1p38spnsacsvq58l6nzs6")))

