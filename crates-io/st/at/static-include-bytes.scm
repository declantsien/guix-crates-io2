(define-module (crates-io st at static-include-bytes) #:use-module (crates-io))

(define-public crate-static-include-bytes-1.0.0 (c (n "static-include-bytes") (v "1.0.0") (h "15s2a6m4xi195ixxz4ba8dx4dpj3yf4s5iill0gdnhjhv4mdlylh") (y #t)))

(define-public crate-static-include-bytes-1.0.1 (c (n "static-include-bytes") (v "1.0.1") (h "19d37f91nmyxwxv0r9xrapr2la0r1dcqfg29h6vvcdd9ldvbz1d1")))

(define-public crate-static-include-bytes-2.0.0 (c (n "static-include-bytes") (v "2.0.0") (h "1l47nfs7fs3jjdj3x9wk3y8q567wh2a3p4dysnjlmk0jcx0yf38i")))

