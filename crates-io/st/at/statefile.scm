(define-module (crates-io st at statefile) #:use-module (crates-io))

(define-public crate-statefile-0.1.0 (c (n "statefile") (v "0.1.0") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("sync" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1ihbnhprm22m74l9ckrmi5k23zzq2zj9dvf2l73rls1haz5fqpr4")))

