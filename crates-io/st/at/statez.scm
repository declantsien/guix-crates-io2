(define-module (crates-io st at statez) #:use-module (crates-io))

(define-public crate-statez-0.1.0 (c (n "statez") (v "0.1.0") (d (list (d (n "ggez") (r "^0.4") (d #t) (k 0)))) (h "0lif4iwdsbfdjx2g849zdljdxv6z1q87p1478zz182bvmivm985d")))

(define-public crate-statez-0.1.1 (c (n "statez") (v "0.1.1") (d (list (d (n "ggez") (r "^0.4") (d #t) (k 0)))) (h "1ji7hpcpyy7j1wxwz4gk7xsmw4y7fjgr29h8434zqj5k3ixj8kxr")))

(define-public crate-statez-0.1.2 (c (n "statez") (v "0.1.2") (d (list (d (n "ggez") (r "^0.4") (d #t) (k 0)))) (h "0g2cwyjmji30zl24p8gfkfm44fb6lsgg9z7nzflkbqky75k412bj")))

