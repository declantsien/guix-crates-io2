(define-module (crates-io st at stats_traits) #:use-module (crates-io))

(define-public crate-stats_traits-0.1.0 (c (n "stats_traits") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "11nag5y3s10y73dc07s9hpzys6hhj275sw9fw01785jg9fp1p7np")))

