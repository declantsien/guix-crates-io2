(define-module (crates-io st at statig) #:use-module (crates-io))

(define-public crate-statig-0.1.0-beta.0 (c (n "statig") (v "0.1.0-beta.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)) (d (n "statig_macro") (r "^0.1.0-beta") (o #t) (d #t) (k 0)))) (h "1ggks0qc3slylg9kddazyv1cd3mkc6svcny51bp5g08nabwpc8z0") (f (quote (("macro" "statig_macro") ("default" "macro"))))))

(define-public crate-statig-0.1.0-beta.1 (c (n "statig") (v "0.1.0-beta.1") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)) (d (n "statig_macro") (r "^0.1.0-beta") (o #t) (d #t) (k 0)))) (h "1mif30q3q36w7d70hg5by23d55mgldpks5mmpbch1sk4saqg2h42") (f (quote (("macro" "statig_macro") ("default" "macro"))))))

(define-public crate-statig-0.1.0 (c (n "statig") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 2)) (d (n "statig_macro") (r "^0.1.0-beta") (o #t) (d #t) (k 0)))) (h "06bjhdxzsm4q7cbllyra8war39dp838ky0cxhwgdsiv600lrzfip") (f (quote (("macro" "statig_macro") ("default" "macro")))) (r "1.65")))

(define-public crate-statig-0.2.0-beta.0 (c (n "statig") (v "0.2.0-beta.0") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 2)) (d (n "statig_macro") (r "^0.2.0-beta") (o #t) (d #t) (k 0)))) (h "0s8w4vhv9k89z66siv5mi8hlna20x3dw9bwvndql07rvv98rxw25") (f (quote (("macro" "statig_macro") ("default" "macro")))) (r "1.65")))

(define-public crate-statig-0.2.0 (c (n "statig") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 2)) (d (n "statig_macro") (r "^0.2.0-beta") (o #t) (d #t) (k 0)))) (h "087pbgcphyvxajcsj77zmvh0iq0pzk8ghmghdrs4idzsagmbf8n7") (f (quote (("macro" "statig_macro") ("default" "macro")))) (r "1.66")))

(define-public crate-statig-0.3.0 (c (n "statig") (v "0.3.0") (d (list (d (n "bevy_ecs") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "statig_macro") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0qy633b41yabh2gc4470yckdjc4w98dmn8mqf2zkjik6b766gi22") (f (quote (("std") ("macro" "statig_macro") ("default" "macro") ("async" "std")))) (s 2) (e (quote (("serde" "dep:serde") ("bevy" "dep:bevy_ecs")))) (r "1.66")))

