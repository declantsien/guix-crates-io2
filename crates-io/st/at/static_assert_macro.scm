(define-module (crates-io st at static_assert_macro) #:use-module (crates-io))

(define-public crate-static_assert_macro-1.0.0 (c (n "static_assert_macro") (v "1.0.0") (h "1ddnw0migb2scgjb5z8bv7p0svlw5lw2msjp94gcrqznrwmgpgf9")))

(define-public crate-static_assert_macro-1.0.1 (c (n "static_assert_macro") (v "1.0.1") (h "1icgh2rx12mcdrf3l61xgyvfibd48d5bmdwpy7bdcgncqnhkwhd3")))

(define-public crate-static_assert_macro-1.0.2 (c (n "static_assert_macro") (v "1.0.2") (h "0x2gml5v3p6138mxndn0n3lad75br89zj07zlgm0zfsr6farp0v6")))

(define-public crate-static_assert_macro-1.0.3 (c (n "static_assert_macro") (v "1.0.3") (h "0r6789bwgfxf9n4axl2smmkhpzf03hfmdckqmf4a83sdn4cc8fb4")))

(define-public crate-static_assert_macro-1.0.4 (c (n "static_assert_macro") (v "1.0.4") (h "0yjlfqgwd00fylbgkn8kjrjygjlj9ljdmy97i737zi7zjs9nn772")))

(define-public crate-static_assert_macro-1.1.0 (c (n "static_assert_macro") (v "1.1.0") (h "1nzy8a8zg1aais1gi2mkyz051fjv46cyczkl5hjlx490z3hrz8ca")))

