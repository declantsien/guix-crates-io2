(define-module (crates-io st at static_res) #:use-module (crates-io))

(define-public crate-static_res-0.1.0 (c (n "static_res") (v "0.1.0") (d (list (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "1q4hy08csvq8llk0nwj7h0xv2q8vid92fqrjhrxwib47nsmx1ckq")))

(define-public crate-static_res-0.1.1 (c (n "static_res") (v "0.1.1") (d (list (d (n "globwalk") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lvsl194q5xyjiqmcnrsnmgmlk6i3pykx2g0h4s3lhl4w7kpb1gk")))

