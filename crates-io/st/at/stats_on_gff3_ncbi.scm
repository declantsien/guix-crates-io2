(define-module (crates-io st at stats_on_gff3_ncbi) #:use-module (crates-io))

(define-public crate-stats_on_gff3_ncbi-0.1.22 (c (n "stats_on_gff3_ncbi") (v "0.1.22") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "09pm0d7cvb106b3ki23g4w4a54qlw4pnvpmzlhfr63b7qpnm1pd3")))

(define-public crate-stats_on_gff3_ncbi-0.1.23 (c (n "stats_on_gff3_ncbi") (v "0.1.23") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kzd12g1m2bvr7snqdy47rcr3ksd2anca32w627l1jjckk6l9326")))

(define-public crate-stats_on_gff3_ncbi-0.1.24 (c (n "stats_on_gff3_ncbi") (v "0.1.24") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1azry0qa98sv0mwx35z821pvc6k6sx6m5f82bhvs6m479jskfdbm")))

(define-public crate-stats_on_gff3_ncbi-0.1.25 (c (n "stats_on_gff3_ncbi") (v "0.1.25") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0941injiqdi35pvm5gzabna9jrpgg8imy6y4zrakfgx7bjs8sljz")))

(define-public crate-stats_on_gff3_ncbi-0.1.26 (c (n "stats_on_gff3_ncbi") (v "0.1.26") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f6vimj1zz270vrsqga4m26pbcw6r57004nrff03a607x8hwy53p")))

(define-public crate-stats_on_gff3_ncbi-0.1.27 (c (n "stats_on_gff3_ncbi") (v "0.1.27") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "18j0qzfh7km56j0lv4iqvzb16i6bzrdm9y0spgnw601ysws6w51n")))

(define-public crate-stats_on_gff3_ncbi-0.1.28 (c (n "stats_on_gff3_ncbi") (v "0.1.28") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "18scwcl3pwfnck16akmwl1rnzs5grpm556407yxafqzpdpwia7jq")))

(define-public crate-stats_on_gff3_ncbi-0.1.29 (c (n "stats_on_gff3_ncbi") (v "0.1.29") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "00vfjid5sc9ghh28mw0pallw0b1lbc5j75d8wlsd632rx00lrz6c")))

(define-public crate-stats_on_gff3_ncbi-0.1.30 (c (n "stats_on_gff3_ncbi") (v "0.1.30") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fhx0fblmkj3br97psy7l7vh7hdkj870aawpqq64zdj6263vawm8")))

(define-public crate-stats_on_gff3_ncbi-0.1.31 (c (n "stats_on_gff3_ncbi") (v "0.1.31") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "180lglgdxvhgg76sm02xviz5624gs00ffrchmxm6rqrfj6xnc480")))

(define-public crate-stats_on_gff3_ncbi-0.1.34 (c (n "stats_on_gff3_ncbi") (v "0.1.34") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "18c2p4vzzn0b5hni2gazgpy9xyvznfaav0qww7q8kaaidi3aa8gj")))

(define-public crate-stats_on_gff3_ncbi-0.1.35 (c (n "stats_on_gff3_ncbi") (v "0.1.35") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0frc2h4rxdary30wiv2kd75qqkqkw4nxbh3sr91m8driyr561kya")))

(define-public crate-stats_on_gff3_ncbi-0.1.36 (c (n "stats_on_gff3_ncbi") (v "0.1.36") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "04z6qv685dslhqxsn8awcwff7wi38fcni6dz3qlc7h7k49namabm")))

(define-public crate-stats_on_gff3_ncbi-0.1.37 (c (n "stats_on_gff3_ncbi") (v "0.1.37") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ma9ybzssy7ga3fhh59dgiybksgs0amslr7za7fq7h0a7w2w61a3")))

(define-public crate-stats_on_gff3_ncbi-0.1.38 (c (n "stats_on_gff3_ncbi") (v "0.1.38") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rcqav8rssc4fydwhfnxnhg3v7vxhr8b31pqkpxbqsnsw3piwala")))

(define-public crate-stats_on_gff3_ncbi-0.1.39 (c (n "stats_on_gff3_ncbi") (v "0.1.39") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "12cziava942ybla0dzfz5w6pjkx9bvq5hdgxavr6sxl9dhsnv9kl")))

(define-public crate-stats_on_gff3_ncbi-0.1.40 (c (n "stats_on_gff3_ncbi") (v "0.1.40") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1as5zgksgz94vh7zknnpgm5d77rw8fmwppslz1l0qrprfq56dsgf")))

(define-public crate-stats_on_gff3_ncbi-0.1.41 (c (n "stats_on_gff3_ncbi") (v "0.1.41") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cwc32fzgyk5p43h5vm8js4x7iiqrlcf52bhvwa5563lyz24k8j6")))

(define-public crate-stats_on_gff3_ncbi-0.1.42 (c (n "stats_on_gff3_ncbi") (v "0.1.42") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jj6hdfdw5yw9q9ybjd3xjyy1brk5r4vp909gkrwy1q500glyx77")))

(define-public crate-stats_on_gff3_ncbi-0.1.43 (c (n "stats_on_gff3_ncbi") (v "0.1.43") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1giawl3ls12kvdxa08jrxilb75v5s0d9fk3k40jxnf62z84vysh5")))

(define-public crate-stats_on_gff3_ncbi-0.1.44 (c (n "stats_on_gff3_ncbi") (v "0.1.44") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gisqzc86837jndyqj1l83ymkkcgp4799b8916fmv3jq4iih39d9")))

(define-public crate-stats_on_gff3_ncbi-0.1.45 (c (n "stats_on_gff3_ncbi") (v "0.1.45") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cbjwlkc3jg5v5ilb1pfcc151jd8hwblh77d6bpz3hzy67snkqbj")))

(define-public crate-stats_on_gff3_ncbi-0.1.46 (c (n "stats_on_gff3_ncbi") (v "0.1.46") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zl4plh32k2msa2jlpb2xl5ar8p13qdxnpmhk27qfzag5gymyghl")))

(define-public crate-stats_on_gff3_ncbi-0.1.47 (c (n "stats_on_gff3_ncbi") (v "0.1.47") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rhal53kllnhh4p38dbbxd33swflbp2wy0nbvnax83p57p4sz7zs")))

(define-public crate-stats_on_gff3_ncbi-0.1.48 (c (n "stats_on_gff3_ncbi") (v "0.1.48") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cm6hzahvdy058gnqgjnj9ds82rvkxzpvlznc59kaazpmrx2r6ry")))

(define-public crate-stats_on_gff3_ncbi-0.1.49 (c (n "stats_on_gff3_ncbi") (v "0.1.49") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "11jff77q4qmkzr8kx9pc9ihnnfp7qiifr0yj66fvd1vr278s140y")))

(define-public crate-stats_on_gff3_ncbi-0.1.50 (c (n "stats_on_gff3_ncbi") (v "0.1.50") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "09hs32vkzg2bqbxylxbhspb0z2dycc3p1r9p2ynqw0fjphxdgirs")))

(define-public crate-stats_on_gff3_ncbi-0.1.51 (c (n "stats_on_gff3_ncbi") (v "0.1.51") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "13rwdd1726vv7arskbj16x6zvvjfbn2nfdbdi6vqp8778mg370gk")))

(define-public crate-stats_on_gff3_ncbi-0.1.52 (c (n "stats_on_gff3_ncbi") (v "0.1.52") (d (list (d (n "bio") (r "^1.5.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "08y90jahvp9hkkacqvvd1gykv60qzdxz8x7dpax994ab11hnsv58")))

