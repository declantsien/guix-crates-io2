(define-module (crates-io st at stateroom-wasm-macro) #:use-module (crates-io))

(define-public crate-stateroom-wasm-macro-0.2.0 (c (n "stateroom-wasm-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0fxwlkvffmzbqsqr6r10nri8449nx03sarq4n4fk2cpsd1zhplmy")))

(define-public crate-stateroom-wasm-macro-0.2.3 (c (n "stateroom-wasm-macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0c9xqph7mg1clyqfcrjwvlsfa288wkn01ynzaax7h3d0vfy7i9gy")))

(define-public crate-stateroom-wasm-macro-0.2.4 (c (n "stateroom-wasm-macro") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7kwvblkqvc14x540ypj7nq6mb79hd6i5p6avbgrlxb4iwmzfcb")))

(define-public crate-stateroom-wasm-macro-0.2.5 (c (n "stateroom-wasm-macro") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "17v8cl5c2f71idqqg9sv4gryn7mdhy0arfzhvlg4cwdhim5ifpj8")))

(define-public crate-stateroom-wasm-macro-0.2.6 (c (n "stateroom-wasm-macro") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "035ippw26k96xjgdgsi02scni45cj91j3bcn8f9rm57m1fqmlq79")))

(define-public crate-stateroom-wasm-macro-0.2.7 (c (n "stateroom-wasm-macro") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0alxhiia742vzhqhx8s56hn5hgpy32mvd22l5r0xld1pnzgd2qym")))

(define-public crate-stateroom-wasm-macro-0.2.8 (c (n "stateroom-wasm-macro") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1ibhc0rmqwav2pvi50wmy7s7ybc4fzjwv41bgably4spchrq0r3n")))

(define-public crate-stateroom-wasm-macro-0.2.9 (c (n "stateroom-wasm-macro") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0p1rgyii7f8w33ra46li6nms2y9gl9ryhr8i0vy4pmzjxm6h17zx")))

(define-public crate-stateroom-wasm-macro-0.4.0 (c (n "stateroom-wasm-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0pg2xsxs06zxwp0rqzbv0kvml4l1yyl982rbl1v9l6imqk7slras")))

