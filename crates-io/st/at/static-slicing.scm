(define-module (crates-io st at static-slicing) #:use-module (crates-io))

(define-public crate-static-slicing-0.1.0 (c (n "static-slicing") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1g5rhv3l4siy8lfp7cd91m6s4gqx8r1nxqqx0q2y8wakr8f7qg04") (f (quote (("std") ("default" "std")))) (r "1.57")))

(define-public crate-static-slicing-0.1.1 (c (n "static-slicing") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0ii5igb7jm62yki8a8lr4vm25kkis61i4gfy9ip1zbj53vc9va63") (f (quote (("std") ("default" "std")))) (r "1.57")))

(define-public crate-static-slicing-0.2.0 (c (n "static-slicing") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0qi9vhwy1397rm7jf327mbv25w19ivnc6lc7yvnbszv7k9bs49mk") (f (quote (("std") ("default" "std")))) (r "1.59")))

