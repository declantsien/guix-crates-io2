(define-module (crates-io st at static-instance) #:use-module (crates-io))

(define-public crate-static-instance-0.1.0 (c (n "static-instance") (v "0.1.0") (h "0xd39j25fzb0yd5ka38n56a53f3mla7qp7wdb9rmdb2hk32hkh00")))

(define-public crate-static-instance-0.1.1 (c (n "static-instance") (v "0.1.1") (h "1afrz14y1226j2j9vmfbxx53xf8s34ykhw5m6zsyq99x435n57ab")))

