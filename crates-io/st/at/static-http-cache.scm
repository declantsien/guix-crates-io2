(define-module (crates-io st at static-http-cache) #:use-module (crates-io))

(define-public crate-static-http-cache-0.1.0 (c (n "static-http-cache") (v "0.1.0") (d (list (d (n "crypto-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.3") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "sqlite") (r "^0.23.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1f2zhp2yjrjfsrc0kwk4sh5pbxskrf9b8plk6xns0vx8gz93bvik")))

(define-public crate-static-http-cache-0.2.0 (c (n "static-http-cache") (v "0.2.0") (d (list (d (n "crypto-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.3") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "sqlite") (r "^0.23.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1xf630ilr3bd5q20grb8lcqc2mgl39rh5w7ldrk1x37f3fcmr8w0")))

(define-public crate-static-http-cache-0.3.0 (c (n "static-http-cache") (v "0.3.0") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sqlite") (r "^0.30.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "01cnpvnc4fm0hlxs958d3591rq7i763dncsa99gcndf068rbxw0d")))

