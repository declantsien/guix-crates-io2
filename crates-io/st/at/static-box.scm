(define-module (crates-io st at static-box) #:use-module (crates-io))

(define-public crate-static-box-0.0.1 (c (n "static-box") (v "0.0.1") (h "0jbhacbsgamyvp0dns6pf59bvqvcvl6gfqf8fx5fdl4x8jwsmsci")))

(define-public crate-static-box-0.0.2 (c (n "static-box") (v "0.0.2") (h "0z34a8alxghgdnc2pkdmj145sa81ffv8rcwld718pih2238i9sgv")))

(define-public crate-static-box-0.1.0 (c (n "static-box") (v "0.1.0") (h "1zbiak17g4m9lb28s2igymsqnkk04k2mfd4821zdrbw39zrnnrb8")))

(define-public crate-static-box-0.2.0 (c (n "static-box") (v "0.2.0") (h "0w9jgzg0zj21n3g9p5q0yw15z17cn71ynkqvvjj787xh9qwavqcr")))

