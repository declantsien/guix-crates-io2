(define-module (crates-io st at statslib) #:use-module (crates-io))

(define-public crate-statslib-0.1.0 (c (n "statslib") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "argmin") (r "^0.4.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1iw3vhi6wqwjdq6k5rbrnjdrzg482ik69spkjzw7dxcnny0qd7qm")))

