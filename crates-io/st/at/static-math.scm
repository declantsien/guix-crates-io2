(define-module (crates-io st at static-math) #:use-module (crates-io))

(define-public crate-static-math-0.1.0 (c (n "static-math") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0x33lqqnp0spqqzhba6w8410dknb1hzh1hmdarg360iq6zdz4f1r")))

(define-public crate-static-math-0.1.1 (c (n "static-math") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0rwxnq996rqqf06vkajlnh3wk9nd4dk7rmh19jb96x49fpz5jb4n")))

(define-public crate-static-math-0.1.2 (c (n "static-math") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1llr23xjjm5mkb7a5b9s318dy1wb3crf3yl05v7qwvlk9flyfbm2")))

(define-public crate-static-math-0.1.3 (c (n "static-math") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "07lgz20qkrg9ccjp2xvw3pp9hqhxn52lj2v00li63v0fvc2zzv32")))

(define-public crate-static-math-0.1.4 (c (n "static-math") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "1np2qmp73v1j4g5xz35bdqqs388v8xy2s0x3256n0cagzr3v8ri2")))

(define-public crate-static-math-0.1.5 (c (n "static-math") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "1l9f3vfff0kxvwj6av1646w0245dflb3ay59d3m0glhm6kk87x32")))

(define-public crate-static-math-0.1.6 (c (n "static-math") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "0m6ny56qcscwisi70wkra8bx0j5isj87kfqgm827v743kj731ap9")))

(define-public crate-static-math-0.1.7 (c (n "static-math") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3.0") (d #t) (k 0)))) (h "1y2zckm2v3j3adizrjf999wzvqd7zzmb2fhf8qyd1myg9wbmblhn")))

(define-public crate-static-math-0.1.8 (c (n "static-math") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0l97kszbjmrxlznv14f28lhbcq7bc35cg768ici69kgd1ag6pr9x")))

(define-public crate-static-math-0.1.9 (c (n "static-math") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "00is8qy4kdjn49h47g15zb6aw0zicy01ihavvf2y2yxxh26rg2im")))

(define-public crate-static-math-0.2.0 (c (n "static-math") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)))) (h "0awdhz2j3h4qpc5mak2v07xagsign634dcjvgs41v4jcx3137lv5") (f (quote (("no-std" "num/libm") ("default" "num/std"))))))

(define-public crate-static-math-0.2.1 (c (n "static-math") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)))) (h "1kx138qx46sbj6c9kq5pzj3qicfrkxkr711ifc8kvjvn3z6myw72") (f (quote (("no-std" "num/libm") ("default" "num/std"))))))

(define-public crate-static-math-0.2.2 (c (n "static-math") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)))) (h "140g5srxwbz2ri25iyvgxwgm0w3p5qp953rlw7iphpjfj9ikf5y8") (f (quote (("no-std" "num/libm") ("default" "num/std"))))))

(define-public crate-static-math-0.2.3 (c (n "static-math") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4") (k 0)))) (h "0bimr77ap1g7dqny85zk8m4gf5k5f8vav2b2dylmcf2vh9lss5wx") (f (quote (("no-std" "num/libm") ("default" "num/std"))))))

