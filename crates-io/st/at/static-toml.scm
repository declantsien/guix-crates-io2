(define-module (crates-io st at static-toml) #:use-module (crates-io))

(define-public crate-static-toml-1.0.0 (c (n "static-toml") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "13m1x1x595f5cf9a790zqa6kh4wgqwy127h490cc2fflwyy7j1k8")))

(define-public crate-static-toml-1.0.1 (c (n "static-toml") (v "1.0.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "16r0ljfxfm0vb34qxwq1dnfbff1z3rd2hhbjvmm8hys86j5i35z8")))

(define-public crate-static-toml-1.1.0 (c (n "static-toml") (v "1.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0rd9rw84l4j3g2jka0c4r2s333h6na04wbka6jyzxsgrc7mxg90d")))

(define-public crate-static-toml-1.2.0 (c (n "static-toml") (v "1.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "034m0r1x07nn7z9nv3h2mjj9i0zjj4kfpzplxggay750ndv7mx1w")))

