(define-module (crates-io st at status-return) #:use-module (crates-io))

(define-public crate-status-return-0.1.0 (c (n "status-return") (v "0.1.0") (h "1cif242zx4dqizh471sbnjsz7llbri7ka3a14ywxg8d2v4774zrk")))

(define-public crate-status-return-0.1.1 (c (n "status-return") (v "0.1.1") (h "1dq9hsacqhrhk88x708a1ml0bn5p89qld0kafd0g4xwrrzmkfdbh")))

