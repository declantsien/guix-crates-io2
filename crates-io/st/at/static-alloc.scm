(define-module (crates-io st at static-alloc) #:use-module (crates-io))

(define-public crate-static-alloc-0.0.1 (c (n "static-alloc") (v "0.0.1") (h "0qk5qzw6016jf20mp0yfxp6gfzh6xrnk9nj1k893l9d5g4l4b7x7") (f (quote (("try_reserve"))))))

(define-public crate-static-alloc-0.0.2 (c (n "static-alloc") (v "0.0.2") (h "0b5ica81gchgd3w7nc1qd6axyydgjkd44ywkqgv8dm3azqghxdai") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.0.3 (c (n "static-alloc") (v "0.0.3") (h "04cba283vccz8mihncx8qgw9b0jiivgr1ifj84yij146jkag20sw") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.0.4 (c (n "static-alloc") (v "0.0.4") (h "041bgms7pqb702wpr47grbryqjw32ycn2x8f89zp9q0n5v4sczxa") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.0.5 (c (n "static-alloc") (v "0.0.5") (h "085lj2a9ybcmf79a6n7iyswfaaj9pcl970sh8y9f4sh5rqq6gqbz") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.0.6 (c (n "static-alloc") (v "0.0.6") (h "1g4bin5k5mrywpfd96ashh74a0d5gfgsmxsva95zsljgrzypjxrj") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.0.7 (c (n "static-alloc") (v "0.0.7") (h "0hfigmwxknrpbfjszzck795ih6087rxvq1wvhf0a0q4scmmshfp1") (f (quote (("nightly_try_reserve")))) (y #t)))

(define-public crate-static-alloc-0.1.0 (c (n "static-alloc") (v "0.1.0") (h "0i1p9plf7a6wwc98j1nlfw7bk1iax766qwf3jc7ybf1alrhakfma") (f (quote (("nightly_try_reserve")))) (y #t)))

(define-public crate-static-alloc-0.1.1 (c (n "static-alloc") (v "0.1.1") (h "0rlanpyak3kq8jp6nrdzd7mdd3ys388pc91xi8dkn5dl0b4y33ba") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.1.2 (c (n "static-alloc") (v "0.1.2") (h "1n113c7mgbknz2jr9px6szdcfsyiaynsnd57n3dbgyniwl9b97cy") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.2.0 (c (n "static-alloc") (v "0.2.0") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)))) (h "065gln12xby75xrz6dq8mcdgkas9yxsna3xzwpwsg6zp920qfcry") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.2.1 (c (n "static-alloc") (v "0.2.1") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)))) (h "14gwc0q9vxybqx1x4lprj7zjs2k2iqc3dirzkxnzfh1glmy9f8jv") (f (quote (("nightly_try_reserve"))))))

(define-public crate-static-alloc-0.2.2 (c (n "static-alloc") (v "0.2.2") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)))) (h "1llfcic23nxf2pscardxbkaw3s2m35j0b7ds1p6m2m2ysl5was96") (f (quote (("nightly_try_reserve") ("nightly_chain" "alloc") ("alloc"))))))

(define-public crate-static-alloc-0.2.3 (c (n "static-alloc") (v "0.2.3") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0dcfbd4glaivs93qfv05943mfrvmz3fwpprbpx235wa838g2d9vv") (f (quote (("polyfill" "atomic-polyfill") ("nightly_try_reserve") ("nightly_chain" "alloc") ("alloc"))))))

(define-public crate-static-alloc-0.2.4 (c (n "static-alloc") (v "0.2.4") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0zlx3qbprn3zfkr85z0n4c03f5g222jbnsmjqn09zyfx1a27w2sp") (f (quote (("polyfill" "atomic-polyfill") ("nightly_try_reserve") ("nightly_chain" "alloc") ("alloc"))))))

(define-public crate-static-alloc-0.2.5 (c (n "static-alloc") (v "0.2.5") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^1") (o #t) (d #t) (k 0)))) (h "07p6s9njqc1v6jpr0vlw55ps4v32wp3df27fxjg565nf6ph7aacb") (f (quote (("polyfill" "atomic-polyfill") ("nightly_try_reserve") ("nightly_chain" "alloc") ("alloc"))))))

