(define-module (crates-io st at static-bytes) #:use-module (crates-io))

(define-public crate-static-bytes-0.1.0 (c (n "static-bytes") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (k 0)))) (h "1n1z8knws3cdqnvpciir58x73y1apbsmbmm7daj7dl7i7023jp8p")))

(define-public crate-static-bytes-0.2.0 (c (n "static-bytes") (v "0.2.0") (d (list (d (n "bytes") (r "^0.6") (k 0)))) (h "1x445n9ypqawj94gijgymd56iv6lh6mglccw5wkx7ivyfxmbj650")))

(define-public crate-static-bytes-0.3.0 (c (n "static-bytes") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (k 0)))) (h "0s6a8iwwmpx9cpmq5l8pa6j4i7b3717bbydg06c3ckmc52yi1avz")))

