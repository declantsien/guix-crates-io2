(define-module (crates-io st at statenum) #:use-module (crates-io))

(define-public crate-statenum-1.0.0 (c (n "statenum") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ldlkz0facn5j054bngqjy6rzf8n52z0rpxgc1wwizkqllgwfmvb")))

