(define-module (crates-io st at statefun-sdk) #:use-module (crates-io))

(define-public crate-statefun-sdk-0.1.0 (c (n "statefun-sdk") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.15") (d #t) (k 0)) (d (n "statefun-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ffd6dwpfaf4lhjkn3m7nlhqpxk1yk6qms2bjrsgpm0lkdz8gpvy") (y #t)))

