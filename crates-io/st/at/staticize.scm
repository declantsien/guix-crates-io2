(define-module (crates-io st at staticize) #:use-module (crates-io))

(define-public crate-staticize-0.1.0 (c (n "staticize") (v "0.1.0") (h "0hs1wngz8cc7zwxrlbvzac03f8qlxwm60iig09j7f622rm26lqzw")))

(define-public crate-staticize-0.1.1 (c (n "staticize") (v "0.1.1") (h "06hkx4n4vg2dasm1n0zl144kbvc6fp55qvcwh7j5i9ryl5gm7hwv") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-staticize-0.1.2 (c (n "staticize") (v "0.1.2") (h "018w098xdqmbxpx5yfnj8h3z1ylcxqyi24k0343m20wy2nh4vr8d") (f (quote (("std") ("default") ("alloc"))))))

