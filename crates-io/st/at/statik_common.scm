(define-module (crates-io st at statik_common) #:use-module (crates-io))

(define-public crate-statik_common-0.2.0 (c (n "statik_common") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0gmbb9yk24armvnfpvsldnv11a5bd66i375bmlcw48pm0vghcdch")))

(define-public crate-statik_common-0.2.1 (c (n "statik_common") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0l3nv90cjwr8303vbk9lj7mr7r3cagg83f2mm7kfsr1zgln7nxym")))

