(define-module (crates-io st at statx-sys) #:use-module (crates-io))

(define-public crate-statx-sys-0.1.0 (c (n "statx-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 2)))) (h "155gg1nj3lilh7z9vv5bbwalz3w5a1n6qb6li9ixnc4r1s0bknrd")))

(define-public crate-statx-sys-0.1.1 (c (n "statx-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 2)))) (h "0djiwbg4iibdjy4r13306b94c5lba6xb9kkm931p5x55zcfps7rv")))

(define-public crate-statx-sys-0.2.0 (c (n "statx-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 2)))) (h "1gbznqxf0i87gbz5j8kqixzdv3c9mspzzxc3cq03kamqcf8a4kl7")))

(define-public crate-statx-sys-0.2.1 (c (n "statx-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.51") (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 2)))) (h "0janmrxvwx38mf198aivapz88nf9plc5wi0gdsbf98kjh5750nlx")))

(define-public crate-statx-sys-0.3.0 (c (n "statx-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.3") (k 0)) (d (n "memoffset") (r "^0.5.1") (d #t) (k 2)))) (h "1bbb5jsfwcb89kykf8f82hnqvhwk349w6jq7j2d2s5cdwyl3bdx1")))

(define-public crate-statx-sys-0.4.0 (c (n "statx-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.3") (k 0)) (d (n "memoffset") (r "^0.5.1") (d #t) (k 2)))) (h "11highsd0k7lfjzd2hxzkmp9d9a80pcn8h2h2jhz7g5fwrbirmcn")))

(define-public crate-statx-sys-0.4.1 (c (n "statx-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.3") (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)))) (h "11zy17sgfkkv4xrx618gydip6jjjk6wypw47zdk7lnvhdzs2bhv9")))

