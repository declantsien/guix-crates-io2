(define-module (crates-io st at static_slice) #:use-module (crates-io))

(define-public crate-static_slice-0.0.1 (c (n "static_slice") (v "0.0.1") (h "1464snxny6bgjq307by76d5if1zfcaglklyvghkafiylzp1f6lpn")))

(define-public crate-static_slice-0.0.2 (c (n "static_slice") (v "0.0.2") (h "0i3jd9chqcm1rwcnyrrzhvyrp0849ix123i3r6m8diacrbad1kf5")))

(define-public crate-static_slice-0.0.3 (c (n "static_slice") (v "0.0.3") (h "0qhkjds16jf4ac9ib2s06r07pcdv8yws2qqfps7jxdfzwg2y19wj")))

