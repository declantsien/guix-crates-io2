(define-module (crates-io st at static_test) #:use-module (crates-io))

(define-public crate-static_test-0.1.0 (c (n "static_test") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04wqkfky71dm0df9f8p2blya1ajyxkrhy7cbdfis91zyjjgwywjm") (f (quote (("external_doc") ("default"))))))

