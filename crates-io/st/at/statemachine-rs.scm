(define-module (crates-io st at statemachine-rs) #:use-module (crates-io))

(define-public crate-statemachine-rs-0.1.0 (c (n "statemachine-rs") (v "0.1.0") (h "1ldqjck8hxwilh65bgc7364ymnlidlgymg6w40c9ndsjmmm1wafp")))

(define-public crate-statemachine-rs-0.2.0 (c (n "statemachine-rs") (v "0.2.0") (h "132rfl2xddpvsyzyj5mj66lc2smynzx4ma1aivsm6vw5k66kk80z")))

