(define-module (crates-io st at stationeers-mips) #:use-module (crates-io))

(define-public crate-stationeers-mips-0.1.0 (c (n "stationeers-mips") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1b5ws1lrps91nmfpdhhpkmxcazryzyf1f0cnygh6jkpn593cczpa")))

(define-public crate-stationeers-mips-0.2.0 (c (n "stationeers-mips") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "088hhnc591h3bnwggibir9jkd6gl1xhmrn7gjy2854p2bzxx0plz")))

