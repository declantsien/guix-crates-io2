(define-module (crates-io st at static_linear_algebra) #:use-module (crates-io))

(define-public crate-static_linear_algebra-0.1.0 (c (n "static_linear_algebra") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0ncqsrnas6lzihgsmrbsr2bzb5xaz9p6g1dkmv146zikpwwr6saj")))

(define-public crate-static_linear_algebra-0.2.0 (c (n "static_linear_algebra") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1dpsh1in1k0r9vv68vyggkgzljc1rmads9bgm62g7fvq4jq5pzcv")))

