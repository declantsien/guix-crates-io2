(define-module (crates-io st at statik) #:use-module (crates-io))

(define-public crate-statik-0.1.0 (c (n "statik") (v "0.1.0") (h "00yprkqi6yqcw32cd9h5k63xgwhw5m16axfy21xkgdslwj5nv7wc")))

(define-public crate-statik-0.2.0 (c (n "statik") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "statik_common") (r "^0.2.0") (d #t) (k 0) (p "statik_common")) (d (n "statik_server") (r "^0.2.0") (d #t) (k 0) (p "statik_server")) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "1i66nb57ds2n2w05dfwl2fka07xgbcqicn6v5fxx8ndz52hxx0b1")))

(define-public crate-statik-0.2.1 (c (n "statik") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "statik_common") (r "^0.2.0") (d #t) (k 0) (p "statik_common")) (d (n "statik_server") (r "^0.2.0") (d #t) (k 0) (p "statik_server")) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "00qy8c41j533mpa77yq9xqa1j23my3jm05pdcsr6pncvim5pysn9")))

