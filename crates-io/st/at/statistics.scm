(define-module (crates-io st at statistics) #:use-module (crates-io))

(define-public crate-statistics-0.1.0 (c (n "statistics") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "1260xdy4lnd2sdfksajnphvbxccc8d8ncjb9xl76gsk2j1c56ffk")))

(define-public crate-statistics-0.2.0 (c (n "statistics") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "0hkghd0yk7mjwwpfqnjc2qbsqdb14rzdml3liqsrd9ia5r68im2k")))

(define-public crate-statistics-0.2.1 (c (n "statistics") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "0wnwziypf72ahy3h3y5z742ldmi146i351cdks8r3cx1p6zri1s4")))

(define-public crate-statistics-0.2.2 (c (n "statistics") (v "0.2.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "044jay06iskq37r9w8f28789v8zhsw3s2w7w38h1sh0yf9q9pnh7")))

(define-public crate-statistics-0.3.0 (c (n "statistics") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "1g7nlp1250kw5djw7r6gifdadpvj7yq52x9g01kdgxfl50yq0bgs")))

(define-public crate-statistics-0.3.1 (c (n "statistics") (v "0.3.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "0q1qdi61yfqj54r7hlx2ciynn8gdv4bw6wxssjfvz7z478sds09g")))

(define-public crate-statistics-0.3.2 (c (n "statistics") (v "0.3.2") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)))) (h "16zpywx76pml35d5nncgjndvny3kaz6n5b1lxrcig5yk8zgj30vi")))

(define-public crate-statistics-0.4.0 (c (n "statistics") (v "0.4.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)))) (h "1klddq8w10yc3klyfpihyx2312ms64l9fgwsdjzaafdn5w137hvc")))

(define-public crate-statistics-0.4.1 (c (n "statistics") (v "0.4.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)))) (h "1krs0x9fki7n6gb79f6bg21k4za4lmiqhzgj42vnhi4m0z42hiiz")))

