(define-module (crates-io st at statsd) #:use-module (crates-io))

(define-public crate-statsd-0.1.1 (c (n "statsd") (v "0.1.1") (h "0fxxx511ihir3jna9nn4ibi9w1y9mnhcad8kkv83byjk08a4z2qy")))

(define-public crate-statsd-0.1.2 (c (n "statsd") (v "0.1.2") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1x4wa7f4q4kxb4hnpqavbw4hjq16r8civvkm0divrcsnxy8i4dm1")))

(define-public crate-statsd-0.2.0 (c (n "statsd") (v "0.2.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0vr45mhmw9xg5a3gxr4ykygajznhi2bcg6q6j9pw7ykyr5qxlg6q")))

(define-public crate-statsd-0.3.0 (c (n "statsd") (v "0.3.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0bc7lmjli29axw1ciy6vnqxpfzlf8pbn07251x63x496392hlzkb")))

(define-public crate-statsd-0.3.1 (c (n "statsd") (v "0.3.1") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "15mc73jg706prr7mgxrh5fx0w2halq0x2hfm2aiqhnkgipfj8q2i")))

(define-public crate-statsd-0.4.1 (c (n "statsd") (v "0.4.1") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "13303iwwb84c7b1y2a0rkgr11sl1i186vs2mdbrjl4ghqcrgmfj3")))

(define-public crate-statsd-0.4.2 (c (n "statsd") (v "0.4.2") (d (list (d (n "clock_ticks") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0vqhkrfbh0zifv6659g8x1alxzgwq8rc7773hb4lcd8mzmb2jkn7")))

(define-public crate-statsd-0.5.0 (c (n "statsd") (v "0.5.0") (d (list (d (n "clock_ticks") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "029ywhm53pjk4k318r7c9mb5zgghsmdka1wpq8xrmw3fnzrmxyy9")))

(define-public crate-statsd-0.6.0 (c (n "statsd") (v "0.6.0") (d (list (d (n "clock_ticks") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06gqzf9cfg7byrk729q6wlv82v3hs1la63l2wp0z3jy3l06jdpz0")))

(define-public crate-statsd-0.7.0 (c (n "statsd") (v "0.7.0") (d (list (d (n "clock_ticks") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "11b0jydjddkibr3sz2sc7kkqpabaxc696y0grcb339msn59vdrk5")))

(define-public crate-statsd-0.8.0 (c (n "statsd") (v "0.8.0") (d (list (d (n "clock_ticks") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "12pzd7bp8wbn9rblznsyzdy9jp1fclr1rbcz5ic7fnrqqsbgqkxp")))

(define-public crate-statsd-0.9.0 (c (n "statsd") (v "0.9.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "051fyqnv7czayiyy48nmnhvah7zyk6778xs4iyd6xmglwng4rsij")))

(define-public crate-statsd-0.9.1 (c (n "statsd") (v "0.9.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1bnx918gf2jccgn8ggdls122wc094j8cw74nxd1mp7akqlxk2w8q")))

(define-public crate-statsd-0.10.0 (c (n "statsd") (v "0.10.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0h9laskrzgxc814qpz51r8j03npa7rk3nl6hwnjy19vlip9y0zpd")))

(define-public crate-statsd-0.11.0 (c (n "statsd") (v "0.11.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1j7kb2v3rr4hzpf7nk4q1ldrsadrgnrrijxl2acmgbkb8can0fcn")))

(define-public crate-statsd-0.13.0 (c (n "statsd") (v "0.13.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10g3wcz2r98lb9sjlzhpjhvz4cyb5ppwa1fqgfj3gscy1ddpmklw")))

(define-public crate-statsd-0.13.1 (c (n "statsd") (v "0.13.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1d6h2x1nszr2qhmf6jnnbx04pyr4hv9fa9qhv46ccda1rjra2vyx")))

(define-public crate-statsd-0.14.0 (c (n "statsd") (v "0.14.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "016xi9jwc0i3i7pzrpqfvbk7qyzk7vvmnyxf5dsvkvwnskmnvssh")))

(define-public crate-statsd-0.14.1 (c (n "statsd") (v "0.14.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nmxam0vbfgbmnylrwfvixzm7dam0lf62rh8yxv9yfwzjqbzi0ai")))

(define-public crate-statsd-0.15.0 (c (n "statsd") (v "0.15.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0c536ja2jcj1pqpdynv0fl9yvfrvlj2l4dclxkmvbh7j9gmzq7nz")))

(define-public crate-statsd-0.16.0 (c (n "statsd") (v "0.16.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "12vgmxskn4l494qb1kv2gqdzhknjgmmgzi7c30f5a9plqwkfwcwh")))

