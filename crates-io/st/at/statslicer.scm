(define-module (crates-io st at statslicer) #:use-module (crates-io))

(define-public crate-statslicer-0.1.0 (c (n "statslicer") (v "0.1.0") (d (list (d (n "arrrg") (r "^0.3") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "guacamole") (r "^0.6") (d #t) (k 2)) (d (n "sig_fig_histogram") (r "^0.2") (d #t) (k 0)))) (h "0p35pc0gi7qxma704iavcxixzgccw7gibpl0lfmdybchn0mp2va5") (f (quote (("default" "binaries") ("binaries"))))))

(define-public crate-statslicer-0.2.0 (c (n "statslicer") (v "0.2.0") (d (list (d (n "arrrg") (r "^0.3") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "guacamole") (r "^0.6") (d #t) (k 2)) (d (n "sig_fig_histogram") (r "^0.2") (d #t) (k 0)))) (h "0s3v23zywrxhgl4g69py76nczdzks7q5s68jwqw853j507fwfiqn") (f (quote (("default" "binaries") ("binaries"))))))

