(define-module (crates-io st at staticfraction) #:use-module (crates-io))

(define-public crate-staticfraction-0.0.1 (c (n "staticfraction") (v "0.0.1") (d (list (d (n "fraction") (r ">= 0.3") (o #t) (d #t) (k 0)) (d (n "num") (r ">= 0.1") (d #t) (k 0)))) (h "0n68wk3l8s5v9qg2ixx8f67srlan4kbpgsfpsiqsc3wb480qf2mh") (f (quote (("to_fraction" "fraction"))))))

