(define-module (crates-io st at static_table) #:use-module (crates-io))

(define-public crate-static_table-0.1.0 (c (n "static_table") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "tabled") (r "^0.11") (f (quote ("std"))) (k 0)))) (h "1g90nf95c9zgab03d9c4b9ifrzd2qw9i3q8m2a1f874jvvrk6n1x") (f (quote (("color" "tabled/color"))))))

(define-public crate-static_table-0.2.0 (c (n "static_table") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "tabled") (r "^0.12") (f (quote ("std"))) (k 0)))) (h "0wcxz7zbnzw502nm2w8cplhs362im3r6bjxh8ik6l2csyd2b5wfc") (f (quote (("color" "tabled/color"))))))

(define-public crate-static_table-0.2.1 (c (n "static_table") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "tabled") (r "^0.12") (f (quote ("std"))) (k 0)))) (h "1sf0npzyw8prry34h2d4yivi9n5rhbqfwkg29xp1dabhbzv0fl9n") (f (quote (("color" "tabled/color"))))))

(define-public crate-static_table-0.3.0 (c (n "static_table") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "tabled") (r "^0.15") (f (quote ("std"))) (k 0)))) (h "0lhd3wlw2k8zdd5gd8l06w429rp35vj269j44kfpkhrndvy5cr53") (f (quote (("macros" "tabled/macros") ("derive" "tabled/derive") ("ansi" "tabled/ansi"))))))

