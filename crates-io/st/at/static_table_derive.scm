(define-module (crates-io st at static_table_derive) #:use-module (crates-io))

(define-public crate-static_table_derive-0.1.0 (c (n "static_table_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "1m4rwy6n3kcp7iwbn9wlv86jywzrd6cwz7z156yddq5s7svxcz7v")))

(define-public crate-static_table_derive-0.1.1 (c (n "static_table_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (d #t) (k 0)))) (h "0jdd83m9idwsi3ar2dc2v9nnsll7v50vfanmkqwj8a5zk3b09gja")))

(define-public crate-static_table_derive-0.1.2 (c (n "static_table_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.22") (d #t) (k 0)))) (h "0kbgvqf6fw44vab6cnn3srhb108p2bgw0b4hvvrxigbh77q2y2d5")))

(define-public crate-static_table_derive-0.1.3 (c (n "static_table_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.30") (d #t) (k 0)))) (h "02nqc79ib6778hj1wb3fz48bbszga7cg9jdn40qp4k4pbqd9xxrc")))

(define-public crate-static_table_derive-0.1.4 (c (n "static_table_derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.31") (d #t) (k 0)))) (h "0p4a21prlmvyql1pj9mljnr73s3ww6yc3d13gx0f3cy8lh2qg9fy")))

(define-public crate-static_table_derive-0.1.5 (c (n "static_table_derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1qhdwjradk2yybpd4k300s1vckkfffjkxkgsys1j512d0zvpnggr")))

(define-public crate-static_table_derive-0.1.6 (c (n "static_table_derive") (v "0.1.6") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (d #t) (k 0)))) (h "103z82ikvg2fmn758vi4vglgg1y1y6s7jnnib0xg8cdnpdlsgdpr")))

(define-public crate-static_table_derive-0.1.7 (c (n "static_table_derive") (v "0.1.7") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (d #t) (k 0)))) (h "1h7748y3s75c4sl546b3i7qdl176zdg26x68nhk7nymqwzcivh3c")))

(define-public crate-static_table_derive-0.1.8 (c (n "static_table_derive") (v "0.1.8") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "112zsaa192ci0yb87x438mam949m7hrz25f0n78xv5nji7waic4r")))

(define-public crate-static_table_derive-0.1.9 (c (n "static_table_derive") (v "0.1.9") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "1vr45vzsy863m5im0xxwhnknqg3rwcvavxjnis05xzg93i54nzgw")))

(define-public crate-static_table_derive-0.1.10 (c (n "static_table_derive") (v "0.1.10") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.22") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "1amrk7cm77mqicsl8pmqwmz278iihskbgw9r6s7qmgbscxmjydhn")))

(define-public crate-static_table_derive-0.1.11 (c (n "static_table_derive") (v "0.1.11") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.43") (d #t) (k 0)))) (h "0ylm7wgxzyjq0pfbd3zrkkcjm18hcm8m0mpf52d1n891jjbkik37")))

(define-public crate-static_table_derive-0.1.12 (c (n "static_table_derive") (v "0.1.12") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (d #t) (k 0)))) (h "0z62b9xrsbacrawygvm5p5p47h7yxr58n65qw57vnbdp1r6fw63k")))

(define-public crate-static_table_derive-0.1.13 (c (n "static_table_derive") (v "0.1.13") (d (list (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (d #t) (k 0)))) (h "0j5i1q7ikg1k9b4v4xpvbambzf4x4id644r9n4hly85jq1hkl5pf")))

(define-public crate-static_table_derive-0.1.14 (c (n "static_table_derive") (v "0.1.14") (d (list (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0hg4q78gf1lryx40x6y8265iqj2qnqcvciva5w6i58aw2dbvxyxa")))

(define-public crate-static_table_derive-0.1.15 (c (n "static_table_derive") (v "0.1.15") (d (list (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "1gjn459b0l65bchx1kkh48wjm9srps1qck3qzbq7n5kwk846vvpz")))

(define-public crate-static_table_derive-0.1.16 (c (n "static_table_derive") (v "0.1.16") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "11f0h3fk941yh9pfz1h22lmz1rv9nygbd91i7j41vw0pm8kxs431")))

(define-public crate-static_table_derive-0.1.17 (c (n "static_table_derive") (v "0.1.17") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.59") (d #t) (k 0)))) (h "1g9v7zvx428f1zjiza641csz1g5p78ls8j6qwxmqsyqcj2bp3r66")))

(define-public crate-static_table_derive-0.1.18 (c (n "static_table_derive") (v "0.1.18") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1fp98529d4769l3b36pn18pvbz17jqlz7z4b9g9hfwya7jr7y0bd")))

(define-public crate-static_table_derive-0.1.19 (c (n "static_table_derive") (v "0.1.19") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "0lhck5vx22m3np9y04105xpxi4n0djcsh6b0hfv5hah9mpvlhzdr")))

(define-public crate-static_table_derive-0.1.20 (c (n "static_table_derive") (v "0.1.20") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)))) (h "1vjx2gyb7di97fjb31zabjbaykqpn0nd1nk6pxbq1wagwxsvq4jf")))

(define-public crate-static_table_derive-0.1.21 (c (n "static_table_derive") (v "0.1.21") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (d #t) (k 0)))) (h "09q025v4bk1113srq10qp237bw2m9dgs8gsypxvv8wiz0m4l6jr2")))

(define-public crate-static_table_derive-0.1.22 (c (n "static_table_derive") (v "0.1.22") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.65") (d #t) (k 0)))) (h "11ff1qsh3ydpcbs097yhs2zdmdwgwf09val53fs0hm60ryyjp3yp")))

(define-public crate-static_table_derive-0.1.23 (c (n "static_table_derive") (v "0.1.23") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "05cqy5m27vcv1xd4v07ng51lhhalfq06kmjk2870dwk4v0p9ngga")))

(define-public crate-static_table_derive-0.1.24 (c (n "static_table_derive") (v "0.1.24") (d (list (d (n "darling") (r "^0.12.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "1p0d36kw19vv8rckzsd0qim5ijcndr9s2gw90gr5n4pd6g2xddc1")))

(define-public crate-static_table_derive-0.1.25 (c (n "static_table_derive") (v "0.1.25") (d (list (d (n "darling") (r "^0.12.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (d #t) (k 0)))) (h "006kahi5k7cm47iy4hdayvjc4w16z74zvma6h10wz2wfn53djljb")))

(define-public crate-static_table_derive-0.1.26 (c (n "static_table_derive") (v "0.1.26") (d (list (d (n "darling") (r "^0.12.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (d #t) (k 0)))) (h "0z3vgyg5l0k1r1rnw81lccb4c1b7xwfmahi9pz7rc5wdz4kigb7c")))

(define-public crate-static_table_derive-0.1.27 (c (n "static_table_derive") (v "0.1.27") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1sa49vf09s7jln815fqcyd1mwdsqqxs9r9yzb4pimh9rhdy73pkc")))

(define-public crate-static_table_derive-0.1.28 (c (n "static_table_derive") (v "0.1.28") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0w0b00qxhjysp5vrwvipkn6c8z9wdfr8fscg1iizrbirwaxq7mzi")))

(define-public crate-static_table_derive-0.1.29 (c (n "static_table_derive") (v "0.1.29") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0gyq6axhv94np1c4qhkxrwj80cb0scsrg2xqvvgyb0bf2n3mzv18")))

(define-public crate-static_table_derive-0.1.30 (c (n "static_table_derive") (v "0.1.30") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "03c79704ahhm24mickjg45qpf79f1fb9dc9lyhlgl1hr26lq4cwq")))

(define-public crate-static_table_derive-0.1.31 (c (n "static_table_derive") (v "0.1.31") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)))) (h "1xisxmb5l7blx8wnylm7jlbr3ffad4hp2sjxahhcdk32989nbz5k")))

(define-public crate-static_table_derive-0.1.33 (c (n "static_table_derive") (v "0.1.33") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "15ydrv7bd750062fs6hil4zv2zqnwlq3a12dprzg3pcfs60x2sa3")))

(define-public crate-static_table_derive-0.1.34 (c (n "static_table_derive") (v "0.1.34") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0h8lhfn676khw97chxy6c72pxc0k6k4gm7k0jm42h3cds3vg0psv")))

(define-public crate-static_table_derive-0.1.35 (c (n "static_table_derive") (v "0.1.35") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "0867a55kdpim7b0rxr4g0pywzh3yq95kqpv0lx4kmz07xp0kfjcq")))

(define-public crate-static_table_derive-0.1.36 (c (n "static_table_derive") (v "0.1.36") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "17fk1x4wz4l67qvzr39bmd4pqmhqhwhhkigmcd4jr2p0lka2g6d6")))

(define-public crate-static_table_derive-0.1.37 (c (n "static_table_derive") (v "0.1.37") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "0sdwcipcaism5vzkl1ljq6hxiy237nwdp720kc0yhcj6953abfz8")))

(define-public crate-static_table_derive-0.1.38 (c (n "static_table_derive") (v "0.1.38") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1avg7bg07km1b8r9b20c183ffyyh8vx4k0jlqy73jwgyc3iw1jzz")))

(define-public crate-static_table_derive-0.1.39 (c (n "static_table_derive") (v "0.1.39") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0wlcq6v7nff57lcq3r00vb2bbalxqpkh2djd6hp5w05mkywqns58")))

(define-public crate-static_table_derive-0.1.40 (c (n "static_table_derive") (v "0.1.40") (d (list (d (n "darling") (r "^0.13.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)))) (h "0xkn1387jdixsph3rj8yk19s6a0vg3xw6ikn7wkh2g42f15zdz0m")))

(define-public crate-static_table_derive-0.1.41 (c (n "static_table_derive") (v "0.1.41") (d (list (d (n "darling") (r "^0.13.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1yyrydgq87j0ik6jdcy58cgfag4x9p8zq0gk4nk7q3wq69639pb3")))

(define-public crate-static_table_derive-0.1.42 (c (n "static_table_derive") (v "0.1.42") (d (list (d (n "darling") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "07qrwbhf26sm6lb3bq5wv478j59vs1yg98f5iwdlm4zbb59304mr")))

(define-public crate-static_table_derive-0.1.43 (c (n "static_table_derive") (v "0.1.43") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0fc3clha6fhlwk92bz2ncx4ys932va98d2zl3n3ki85974x8zk56")))

(define-public crate-static_table_derive-0.1.44 (c (n "static_table_derive") (v "0.1.44") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1gxfj04462l6kvb1jjk7mi8vc7w0rh2izwb33vfld7ljav5wqa6a")))

(define-public crate-static_table_derive-0.1.45 (c (n "static_table_derive") (v "0.1.45") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "15nj641panqv0akrkpp3j26ypm0qc2nqmf5fvi8my4v64xc76xxw")))

(define-public crate-static_table_derive-0.1.46 (c (n "static_table_derive") (v "0.1.46") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "05ywq8n29dn5jkqxv3xvbnqa3q75d5hzgxvm1z0vi977wq6viixc")))

(define-public crate-static_table_derive-0.1.47 (c (n "static_table_derive") (v "0.1.47") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1dn3asslc93z6p32ncl0b49cp29i8k0dc5ga9zgykcqic1c116w6")))

(define-public crate-static_table_derive-0.1.48 (c (n "static_table_derive") (v "0.1.48") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0377s9ipdhqliy9617fia8wb7m3qf11wh30qkmr8h5q0prdfg797")))

(define-public crate-static_table_derive-0.1.49 (c (n "static_table_derive") (v "0.1.49") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1ll4738zd3vqf6nqz0g1a4y77qfh5x2pl86jkpw4crabif7knbdx")))

(define-public crate-static_table_derive-0.1.50 (c (n "static_table_derive") (v "0.1.50") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.48") (d #t) (k 0)) (d (n "quote") (r "^1.0.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.106") (d #t) (k 0)))) (h "08dilys5476z803nqx0d844piwkwyfvwsfmh9kbv8cngi2nqci57")))

(define-public crate-static_table_derive-0.1.51 (c (n "static_table_derive") (v "0.1.51") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1v267ns6bcdsxbkjv9ziyx7miqwxlydpawvfqfp31ikpcpd1wins")))

(define-public crate-static_table_derive-0.1.52 (c (n "static_table_derive") (v "0.1.52") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "16izsbsgrkhkxryly3wl7zy8hsyy35nr6k88kkmafkw5bvsrnfch")))

(define-public crate-static_table_derive-0.1.53 (c (n "static_table_derive") (v "0.1.53") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "17p4whdz7bdymxahnk8kin0vlagfhrg1p394824k4hcr0vkfifn2")))

(define-public crate-static_table_derive-0.1.54 (c (n "static_table_derive") (v "0.1.54") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1z96as53vwqxqkbmvvv2ppcyhcfwba4mcq704cqypv25fgib41w8")))

(define-public crate-static_table_derive-0.1.55 (c (n "static_table_derive") (v "0.1.55") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1jny2gzn9523mff74bzv3fhh4y6arxr28war2qn1iw5n423ji0cs")))

(define-public crate-static_table_derive-0.1.56 (c (n "static_table_derive") (v "0.1.56") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "10kcdpfr13dqzcf5p2a4r0ksxnha4ghmkilfamzjv8cf42wjsir2")))

(define-public crate-static_table_derive-0.1.57 (c (n "static_table_derive") (v "0.1.57") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0y3nh17md4050bprms1hmy5vi5nnislvjjywi9nnj4sz3brfidx1")))

(define-public crate-static_table_derive-0.1.58 (c (n "static_table_derive") (v "0.1.58") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (d #t) (k 0)))) (h "0brdqyzvn29f0ii9gcr8sn9rmc8vx1bd2139a9cmhzpj0cb1m50b")))

(define-public crate-static_table_derive-0.1.59 (c (n "static_table_derive") (v "0.1.59") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "0rvpki86brklpbqg18mm7bp62mhrm7xw15dhara2rzd90nrbxmpa")))

(define-public crate-static_table_derive-0.1.60 (c (n "static_table_derive") (v "0.1.60") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0aci43pk748r1rlbxfvlbrkv45nqd0yb2akazycx2kkqmn99fzm7")))

(define-public crate-static_table_derive-0.1.61 (c (n "static_table_derive") (v "0.1.61") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "07kvk40w0wjgmycy6srajmpmpb8vmha2n5w2lwv6lca0kh17930r")))

(define-public crate-static_table_derive-0.1.62 (c (n "static_table_derive") (v "0.1.62") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0s10y0r6kqfx2vfb98cz267ni9lq90kxidrbiq0spsf4kvv0dv1a")))

(define-public crate-static_table_derive-0.1.63 (c (n "static_table_derive") (v "0.1.63") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0jd3899nr63vfcsjkr1q6wgppsavcgaiw5ndxic0waqx67ya6ihv")))

(define-public crate-static_table_derive-0.1.64 (c (n "static_table_derive") (v "0.1.64") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0v1rxzdvw6kfb4c85v9rm61kgbynnsaby22msa4hn3zi8dg48b28")))

(define-public crate-static_table_derive-0.1.65 (c (n "static_table_derive") (v "0.1.65") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1pm38jpg6i2x8l0bbi3vbyfpar2jmpf94f95z3vm2fwcz5jl1lcl")))

(define-public crate-static_table_derive-0.1.66 (c (n "static_table_derive") (v "0.1.66") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1m7s6s59zi0qcvgn1b0kl77hr1hfyyia998dmlmcfzdm1lil8ha1")))

