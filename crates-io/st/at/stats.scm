(define-module (crates-io st at stats) #:use-module (crates-io))

(define-public crate-stats-0.0.1 (c (n "stats") (v "0.0.1") (h "0nixsjw1zv8fx93i8g3rdhblblan3k0gzs5w78kiprjp7b8bbvj1")))

(define-public crate-stats-0.0.2 (c (n "stats") (v "0.0.2") (h "02kligzyv2j55vk98hzsj0qrz73r4kgy42f8pla1frl4hx7sbd0p") (y #t)))

