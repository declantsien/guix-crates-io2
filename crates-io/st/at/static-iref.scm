(define-module (crates-io st at static-iref) #:use-module (crates-io))

(define-public crate-static-iref-1.0.0 (c (n "static-iref") (v "1.0.0") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)))) (h "0l9v8lc7zs81g0bascy24rp76rqll1ssw6wb5qwxks2pqqyhas7y")))

(define-public crate-static-iref-2.0.0 (c (n "static-iref") (v "2.0.0") (d (list (d (n "iref") (r "^2.1") (d #t) (k 0)))) (h "1rqwnxsgy1113v9cr8j9c5s781qggrnq4yn5hpz26m4yir5nsy6r")))

(define-public crate-static-iref-3.0.0 (c (n "static-iref") (v "3.0.0") (d (list (d (n "iref") (r "^3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0jamiv1dvamz12rnz1c25jps2lr1vkf8ci8p85nqjhxfjy20di1w")))

