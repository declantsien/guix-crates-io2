(define-module (crates-io st at static_format) #:use-module (crates-io))

(define-public crate-static_format-0.0.1 (c (n "static_format") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "130snn3i7mcgq98bpij56pfp0gmzfx0pzw8v05zfiwdhy58rz7hz")))

(define-public crate-static_format-0.0.2 (c (n "static_format") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0xzywnjvdhlil9qqq4ysr1zk3mgkk4wzrg3x8852xjln9pyg8q8v")))

(define-public crate-static_format-0.0.3 (c (n "static_format") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "052714qyd48b0v8v0ck1hamfy5pihchl2530nlims5w1nrn3ks8q")))

