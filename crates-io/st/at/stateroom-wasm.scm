(define-module (crates-io st at stateroom-wasm) #:use-module (crates-io))

(define-public crate-stateroom-wasm-0.2.0 (c (n "stateroom-wasm") (v "0.2.0") (d (list (d (n "stateroom") (r "^0.2.0") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0xs5b00lkjd3rkqy32a88ijmj21v4jc2i4mx7ibhnwqcrckcy0fa")))

(define-public crate-stateroom-wasm-0.2.2 (c (n "stateroom-wasm") (v "0.2.2") (d (list (d (n "stateroom") (r "^0.2.2") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.0") (d #t) (k 0)))) (h "189wikyfnwiysk5abyvyw85m1v6cnxhzbqwqam0v2p13blq47zkx")))

(define-public crate-stateroom-wasm-0.2.3 (c (n "stateroom-wasm") (v "0.2.3") (d (list (d (n "stateroom") (r "^0.2.3") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.3") (d #t) (k 0)))) (h "06pj6s919jag6h834c3a8knyfwl5rzxmpw8zq63nq7sqgw65fd4c")))

(define-public crate-stateroom-wasm-0.2.4 (c (n "stateroom-wasm") (v "0.2.4") (d (list (d (n "stateroom") (r "^0.2.4") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.4") (d #t) (k 0)))) (h "10kbkgbfs3jy6nmizki62s70i7kx3rfq53q83sh8jf24ij4kkghc")))

(define-public crate-stateroom-wasm-0.2.5 (c (n "stateroom-wasm") (v "0.2.5") (d (list (d (n "stateroom") (r "^0.2.4") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.4") (d #t) (k 0)))) (h "16l9xbr4plaz5bzbaab1mp6x21h199sihpwc903zsl1z53fqavpy")))

(define-public crate-stateroom-wasm-0.2.6 (c (n "stateroom-wasm") (v "0.2.6") (d (list (d (n "stateroom") (r "^0.2.6") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.6") (d #t) (k 0)))) (h "1cpgc18zrijkkfy3xmkgm4kn3py02ylvpjipm5md00vqjj66bmq9")))

(define-public crate-stateroom-wasm-0.2.7 (c (n "stateroom-wasm") (v "0.2.7") (d (list (d (n "stateroom") (r "^0.2.6") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.6") (d #t) (k 0)))) (h "1mnpbq78zpr2ag1gp715si62bp2kh5w9wbmkbsy1vaagmwhqmfsr")))

(define-public crate-stateroom-wasm-0.2.8 (c (n "stateroom-wasm") (v "0.2.8") (d (list (d (n "stateroom") (r "^0.2.8") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.8") (d #t) (k 0)))) (h "0ysgv6h21c20v5czdj945wxsrnr4y0hcxlv4siq4ywm8ay51gjwy")))

(define-public crate-stateroom-wasm-0.2.9 (c (n "stateroom-wasm") (v "0.2.9") (d (list (d (n "stateroom") (r "^0.2.8") (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.2.8") (d #t) (k 0)))) (h "0iimv53k3c1zps4mi3ynaqzvzgpkkx147vhm07pp9vsapfl8ciqr")))

(define-public crate-stateroom-wasm-0.4.0 (c (n "stateroom-wasm") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "stateroom") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "stateroom-wasm-macro") (r "^0.4.0") (d #t) (k 0)))) (h "05ax138jdvhqxx18dx5dns3qaa5sgf1s6bh0pi43c1i1v5rn9x45")))

