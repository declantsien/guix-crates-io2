(define-module (crates-io st at statsdproto) #:use-module (crates-io))

(define-public crate-statsdproto-0.1.0 (c (n "statsdproto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0pvigs44ffhjdiclzxr9hg11zaainzhqn0xhjxm5r28kacsjrxym")))

(define-public crate-statsdproto-0.1.1 (c (n "statsdproto") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0p02wms2h9azrmv5jjw793746z6sdq985qgi8z7ppa9r7lsrjrlw")))

