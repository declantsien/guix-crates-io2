(define-module (crates-io st at static_type_map) #:use-module (crates-io))

(define-public crate-static_type_map-0.1.0 (c (n "static_type_map") (v "0.1.0") (h "0h2di1vnvx4j3qsr1za2snzh9hk57qq887zazlmhah0fqdc27hq0")))

(define-public crate-static_type_map-0.1.1 (c (n "static_type_map") (v "0.1.1") (h "1mraxi0173rld8sljvjsrn0r18s4srprnc1s0mq3pky4adxh685f")))

(define-public crate-static_type_map-0.1.2 (c (n "static_type_map") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.9") (o #t) (d #t) (k 0)))) (h "026pda1b1vs981m4h508wisf3n31q8f5gqcl4ljk3b5agd2a9ic1") (f (quote (("no_std" "hashbrown"))))))

(define-public crate-static_type_map-0.2.0 (c (n "static_type_map") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1gasqa3cll45yayddbbmf2k17jlb7c2flf9pkf8dwlfqdy090m0a") (f (quote (("no_std" "hashbrown"))))))

(define-public crate-static_type_map-0.3.0 (c (n "static_type_map") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0cj7wdb4zyqmjik87792q9nqfh9glrpk1bf09pxqnpkffm9667gl") (f (quote (("no_std" "hashbrown"))))))

(define-public crate-static_type_map-0.4.0 (c (n "static_type_map") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1gas8201jw9rl6lyvpiar0gj7wpbpsjwddp5rfn54cjn9dr60zgc") (f (quote (("sync") ("send") ("no_std" "hashbrown") ("default" "send" "sync"))))))

(define-public crate-static_type_map-0.4.1 (c (n "static_type_map") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "0qr9zbpj10kib5q93nkxdv7jbrn7vm3f7c3bsjpz1vczyc5h08bx") (f (quote (("sync") ("send") ("no_std" "hashbrown") ("default" "send" "sync"))))))

(define-public crate-static_type_map-0.5.0 (c (n "static_type_map") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1g2x8q1p9g8mffa11j0j180bzl008nyhfw2wig88h1ysv0m1prvz") (f (quote (("sync") ("send") ("default" "send" "sync"))))))

(define-public crate-static_type_map-0.5.1 (c (n "static_type_map") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "0lhx3jgjfq9wk89yw041lnmbf3bi9cn4dhm35drav8hwa90y5nwg") (f (quote (("sync") ("send") ("default" "send" "sync"))))))

(define-public crate-static_type_map-0.5.2 (c (n "static_type_map") (v "0.5.2") (d (list (d (n "hashbrown") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "0jvpv6abpx1jlmjj6k4ki8nmdcikqcczrhl9cagdg0r1qqir57ax") (f (quote (("sync") ("send") ("default" "send" "sync"))))))

