(define-module (crates-io st at state-governor) #:use-module (crates-io))

(define-public crate-state-governor-0.0.1 (c (n "state-governor") (v "0.0.1") (h "0g23d8aapll91gn3v2sqczk61a55si123wknd4p7my61pm68kk75")))

(define-public crate-state-governor-0.0.2 (c (n "state-governor") (v "0.0.2") (d (list (d (n "heapless") (r "^0.7.4") (d #t) (k 0)))) (h "1d2ja33d1yclcq7v9w6f2sj8vgfd34vahb839id4jjncj3j3i2vx")))

