(define-module (crates-io st at static-valorant-api) #:use-module (crates-io))

(define-public crate-static-valorant-api-0.1.0 (c (n "static-valorant-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0kaq4vgv8njsd4far33h255sci5jjg8caj4kjg8vy8vc2385ncqk")))

