(define-module (crates-io st at static-files) #:use-module (crates-io))

(define-public crate-static-files-0.0.1 (c (n "static-files") (v "0.0.1") (h "0m83fs1mmm1nm6lg9sj8xdir3m722265f91jj3n6l7p2hvzn8lcv")))

(define-public crate-static-files-0.1.0 (c (n "static-files") (v "0.1.0") (d (list (d (n "change-detection") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "change-detection") (r "^1.1") (o #t) (d #t) (k 1)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 1)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 1)))) (h "1cjdbywlgkvx92pi4q6y1s9q20l3chzai9ynrzyj16y17y59s36i") (f (quote (("default" "change-detection"))))))

(define-public crate-static-files-0.2.0 (c (n "static-files") (v "0.2.0") (d (list (d (n "change-detection") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "change-detection") (r "^1.1") (o #t) (d #t) (k 1)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 1)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 1)))) (h "0cqcqjzhcxffvpbi6r6dv2r7ix2p65f6vdd834jr0xspsrayq0qq") (f (quote (("default" "change-detection"))))))

(define-public crate-static-files-0.2.1 (c (n "static-files") (v "0.2.1") (d (list (d (n "change-detection") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "change-detection") (r "^1.2") (o #t) (d #t) (k 1)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 1)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 1)))) (h "17fam74nhyna3n1a4nl0xdc2f5adr1ymcm9bnam604b99plgbhhy") (f (quote (("default" "change-detection"))))))

(define-public crate-static-files-0.2.2 (c (n "static-files") (v "0.2.2") (d (list (d (n "change-detection") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "change-detection") (r "^1.2") (o #t) (d #t) (k 1)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 1)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 1)))) (h "12hgm25il16sl5rqys33p8qg8wxh87vddyc31fnnjs09a16dmb0s") (f (quote (("default" "change-detection"))))))

(define-public crate-static-files-0.2.3 (c (n "static-files") (v "0.2.3") (d (list (d (n "change-detection") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "change-detection") (r "^1.2") (o #t) (d #t) (k 1)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 1)) (d (n "path-slash") (r "^0.1") (d #t) (k 0)) (d (n "path-slash") (r "^0.1") (d #t) (k 1)))) (h "0sx0yvhql95xdv6930fkbfms5bq5l8mqf1cn3l702h71wfhjwwb4") (f (quote (("default" "change-detection"))))))

