(define-module (crates-io st at staticbitset) #:use-module (crates-io))

(define-public crate-staticbitset-0.1.1 (c (n "staticbitset") (v "0.1.1") (h "0c558lka29b0fxnsnizkz470ip0pzn623xwy7i0c9w2qg3b5a6zw")))

(define-public crate-staticbitset-0.5.1 (c (n "staticbitset") (v "0.5.1") (h "11kplvqr0r7lj337qrh4qdwwmcapj8clmrqdlh8b7y2p2li55h29")))

(define-public crate-staticbitset-0.5.2 (c (n "staticbitset") (v "0.5.2") (h "02kl7d2sldsljff0506lv3h8qb1jfiv39wr2sv611d4cgzyfxgbx")))

