(define-module (crates-io st at static_assets) #:use-module (crates-io))

(define-public crate-static_assets-0.1.0 (c (n "static_assets") (v "0.1.0") (h "1sk2jhxaknl522d8iv6iwq4g9xis803s8b225fv1ch53s7kvj6w9") (f (quote (("force-static") ("force-dynamic") ("default"))))))

(define-public crate-static_assets-0.1.1 (c (n "static_assets") (v "0.1.1") (h "0c6yh1ph5saidqzr88a0h1wphizvb9fmjwcbcgx8agkqgb2vhf1n") (f (quote (("force-static") ("force-dynamic") ("default"))))))

