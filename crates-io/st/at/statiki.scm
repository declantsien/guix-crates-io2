(define-module (crates-io st at statiki) #:use-module (crates-io))

(define-public crate-statiki-0.1.0 (c (n "statiki") (v "0.1.0") (h "0z1wdz0pkg7jzprm7b2rmlghz2g4147y6qqfzy694bcav5yh7pa8") (f (quote (("docs") ("default"))))))

(define-public crate-statiki-0.2.0 (c (n "statiki") (v "0.2.0") (h "16756b50zmjih034xjjlpckpg4vv034bx4lri543zsvqk8f7ymm9") (f (quote (("docs") ("default"))))))

(define-public crate-statiki-0.2.1 (c (n "statiki") (v "0.2.1") (h "1ph02lns0h8db8jbwp77hwr4y1382gwaxm0j9dzj9yg9h3pms4yi") (f (quote (("docs") ("default"))))))

(define-public crate-statiki-0.2.2 (c (n "statiki") (v "0.2.2") (h "1a5zdspgl7q6m1h91nq8zxhxpq1cj684mv7xq895mwm9psps66fs") (f (quote (("std") ("docs") ("default"))))))

(define-public crate-statiki-0.3.0 (c (n "statiki") (v "0.3.0") (h "1jv4804ylxiwranzzi12sw94qvkpqa3nxqzw3wjwdiqybf4w7kq6") (f (quote (("std") ("docs") ("default"))))))

(define-public crate-statiki-0.4.0 (c (n "statiki") (v "0.4.0") (h "00y0cwgmrq8qimb8n0snsx9g7kajqr4lz7glq7jn1ag0yr0k6wpk") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-statiki-0.4.1 (c (n "statiki") (v "0.4.1") (h "0hsn16sns14zi5rymqbkgphxs8k7gq3b6djd3149x09kj8h2klfx") (f (quote (("std") ("default"))))))

(define-public crate-statiki-0.4.2 (c (n "statiki") (v "0.4.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1nji3nnnnsq8vrnx4j2zsyn3dmznp6zh0ql2np59wnsbz0lm3cy4") (f (quote (("std") ("default"))))))

(define-public crate-statiki-0.4.3 (c (n "statiki") (v "0.4.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "04c3i16n0x21cyxz7xqrm7gfcc6adi9daml6qcjraxx4r3lqfqkz") (f (quote (("std") ("default"))))))

(define-public crate-statiki-0.5.0 (c (n "statiki") (v "0.5.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "16llqj3aijkfqm57hgz6cjq59bdymzx027kyxkgff4b30vhsf7ql") (f (quote (("std") ("default"))))))

(define-public crate-statiki-0.5.1 (c (n "statiki") (v "0.5.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)))) (h "10xbfbxfh435v8vhml7226lvzh8g2p869slq491iqfdcgx26f72a") (f (quote (("std") ("default"))))))

