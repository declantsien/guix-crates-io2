(define-module (crates-io st at static-reflect) #:use-module (crates-io))

(define-public crate-static-reflect-0.1.0 (c (n "static-reflect") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0x315dwa11crcgcgnh12i6ni09wnzfnmankp4wgi5pymrlii45ca") (f (quote (("never") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.1 (c (n "static-reflect") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)))) (h "0gw6w9z3az33vfcb42m5sack0lpicwa7y4x9an28w97p1a3fz3nk") (f (quote (("never") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.2 (c (n "static-reflect") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)))) (h "1y43mnrdi2mcfc2dx9vshikcnj3bf4ijkjlkz6njzkmbc64hdds4") (f (quote (("never") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.3 (c (n "static-reflect") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)))) (h "031lbk5bfp32qz2vfw9gl0wzcs8djl2wcgndxyzfb6zy68wy1sbd") (f (quote (("never") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.4 (c (n "static-reflect") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)))) (h "16sm9qzszw4zyndvydsj7c95nvbhbgqjc6h5nq478rqcwrb7j9l2") (f (quote (("never") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.5 (c (n "static-reflect") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)))) (h "115bxd01mkpq8gvgy57izxbjyw6vdlrdxlk9vj6darmlqvyhb769") (f (quote (("never") ("docs-rs") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.6 (c (n "static-reflect") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.2") (o #t) (d #t) (k 0)))) (h "16n8jbwmwr2sg56vkr4mjjyw8pnl2s87mlvc9v46i8fl0clg733x") (f (quote (("never") ("docs-rs") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.7 (c (n "static-reflect") (v "0.1.7") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.3") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.3") (o #t) (d #t) (k 0)))) (h "0jxabpykbkbj8jag8s1pmhpkf5sh461aqwsg8si0szpjcaf4bikd") (f (quote (("never") ("docs-rs") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.8 (c (n "static-reflect") (v "0.1.8") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)))) (h "1ndra5sw23jdhm9dlh8fdnwzcdg9b9w4azz6hrv9wsr9ahmfjsdy") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("docs-rs") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.1.9 (c (n "static-reflect") (v "0.1.9") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)))) (h "16q7bzfm25zj5c0f1a237lma9c8j7v542lhl3l9lz3adm7r96wa2") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.2.0-alpha.1 (c (n "static-reflect") (v "0.2.0-alpha.1") (d (list (d (n "zerogc") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)))) (h "03mngac3i3jssc59rs1q7v52bp4k7cdkx4aq8c5s69gr6irwhn51") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.2.0-alpha.2 (c (n "static-reflect") (v "0.2.0-alpha.2") (d (list (d (n "zerogc") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)))) (h "1bs1zp2iwc28iaa57lakjlqjfjchh6kriyqpp8dj3wn3dlqph8yb") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.2.0-alpha.3 (c (n "static-reflect") (v "0.2.0-alpha.3") (d (list (d (n "zerogc") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)))) (h "143yv0v172x1bk2bl1zysfhkr8g4vlxj7c3d9nwpcp5c9wi1xyqw") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.2.0-alpha.4 (c (n "static-reflect") (v "0.2.0-alpha.4") (d (list (d (n "zerogc") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.4") (o #t) (d #t) (k 0)))) (h "0cgbyg6x9c7nb4179c427cgp2ppsp86r7w58w6ffj44wn9c8zpys") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.2.0-alpha.5 (c (n "static-reflect") (v "0.2.0-alpha.5") (d (list (d (n "zerogc") (r "^0.2.0-alpha.5") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.5") (o #t) (d #t) (k 0)))) (h "1q4imchjxcf1yq39xb5dqh530fax1w4n09krzgq57fazyrc9pfhd") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("default" "never" "builtins") ("builtins"))))))

(define-public crate-static-reflect-0.2.0-alpha.6 (c (n "static-reflect") (v "0.2.0-alpha.6") (d (list (d (n "zerogc") (r "^0.2.0-alpha.6") (o #t) (d #t) (k 0)) (d (n "zerogc-derive") (r "^0.2.0-alpha.6") (o #t) (d #t) (k 0)))) (h "0l2zpb4pc5dmc15w79if4n1ylwalcm2hl0p9wd5ijgrc6bjayf56") (f (quote (("never") ("gc" "zerogc" "zerogc-derive") ("default" "never" "builtins") ("builtins"))))))

