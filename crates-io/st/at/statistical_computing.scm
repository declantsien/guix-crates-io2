(define-module (crates-io st at statistical_computing) #:use-module (crates-io))

(define-public crate-statistical_computing-0.0.1 (c (n "statistical_computing") (v "0.0.1") (h "1lrprfy0n7lagc3vdcpd0svr18kpvb45clf0i76frhm98hcsqkcp") (y #t)))

(define-public crate-statistical_computing-0.0.1-alpha.1 (c (n "statistical_computing") (v "0.0.1-alpha.1") (h "0cix9jq6gbf6zi1722kzd20lcfr9yp1r76n5ni20ylj8hfzilayi")))

(define-public crate-statistical_computing-0.0.1-alpha.2 (c (n "statistical_computing") (v "0.0.1-alpha.2") (h "1h2d45hjifs45k814kgzp6rnzz884526pfal63zmmwmv5zwlxlsp")))

