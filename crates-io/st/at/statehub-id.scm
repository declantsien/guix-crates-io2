(define-module (crates-io st at statehub-id) #:use-module (crates-io))

(define-public crate-statehub-id-0.1.0 (c (n "statehub-id") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "nanoid-dictionary") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.7") (d #t) (k 0)))) (h "0qg4vyi3sih3g3ryvym17xg79gqfnq2rsnagrpiyg5wxqcz00qa2") (r "1.56.0")))

(define-public crate-statehub-id-0.1.1 (c (n "statehub-id") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "nanoid-dictionary") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_with") (r "^1.7") (d #t) (k 0)))) (h "0c08ac47p4cnvkdgqwna0005xnz06qf5xdia0fzliqs97d0dvk1w") (r "1.56.0")))

(define-public crate-statehub-id-0.1.2 (c (n "statehub-id") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "nanoid-dictionary") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^1.7") (d #t) (k 0)))) (h "0l9w678yxx2s1d1rb43wcfzvda3b91q120ma960qdx97blksaljm") (r "1.56.0")))

(define-public crate-statehub-id-0.1.3 (c (n "statehub-id") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "nanoid-dictionary") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^1.7") (d #t) (k 0)))) (h "1lxif7v00v9k2flnmjkb24j2ls635zfgabycdc7qn799i4zkqlvr") (r "1.56.0")))

(define-public crate-statehub-id-0.1.4 (c (n "statehub-id") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "nanoid-dictionary") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^1.7") (d #t) (k 0)))) (h "0jc5f2jwcph3i422hry1c9wvih5c3a7bbwc4734dp610y22sx9pn") (r "1.56.0")))

(define-public crate-statehub-id-0.1.5 (c (n "statehub-id") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "nanoid-dictionary") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^1.7") (d #t) (k 0)))) (h "1izw0wy7n9zrlwjxbjzac7q5ryzmrz2fa95wn4cvd83aav1c57qg") (r "1.56.0")))

(define-public crate-statehub-id-0.1.6 (c (n "statehub-id") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "nanoid-dictionary") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^1.7") (d #t) (k 0)))) (h "0h7rlpp6ifkhyh67lgj89fn2wi42zh23izjwzvvvpmb1j66817k4") (r "1.56.0")))

