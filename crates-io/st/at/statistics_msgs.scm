(define-module (crates-io st at statistics_msgs) #:use-module (crates-io))

(define-public crate-statistics_msgs-1.2.1 (c (n "statistics_msgs") (v "1.2.1") (d (list (d (n "builtin_interfaces") (r "^1.2.1") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0jlp8r8ryka3ncwbafzj23ba2iyr5nnzxv53ljx63nkg55rnkk50") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "builtin_interfaces/serde"))))))

