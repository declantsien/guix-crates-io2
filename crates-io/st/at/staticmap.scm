(define-module (crates-io st at staticmap) #:use-module (crates-io))

(define-public crate-staticmap-0.1.0 (c (n "staticmap") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.15") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (f (quote ("png"))) (k 0)) (d (n "raqote") (r "^0.8.0") (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)))) (h "0m7v64m19gc3ad2sh4r25jiqajinfywqc5yr0a1bflqx3ibphhig")))

(define-public crate-staticmap-0.1.1 (c (n "staticmap") (v "0.1.1") (d (list (d (n "attohttpc") (r "^0.15") (f (quote ("tls-rustls"))) (k 0)) (d (n "image") (r "^0.23.10") (f (quote ("png"))) (k 0)) (d (n "raqote") (r "^0.8.0") (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)))) (h "1p7v8z9fd4fhljhpy7x39r7zpzp0lcgngakvjl57qqd7jk3w91dj")))

(define-public crate-staticmap-0.2.0 (c (n "staticmap") (v "0.2.0") (d (list (d (n "attohttpc") (r "^0.15") (f (quote ("tls-rustls"))) (k 0)) (d (n "image") (r "^0.23.10") (f (quote ("png"))) (k 0)) (d (n "raqote") (r "^0.8.0") (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)))) (h "0pnkr78v7ls2v7wh3p3i0z8fbr7xba4m7a00jlhvb09ix8fqms0n")))

(define-public crate-staticmap-0.3.0 (c (n "staticmap") (v "0.3.0") (d (list (d (n "attohttpc") (r "^0.16") (f (quote ("tls-rustls"))) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "png") (r "^0.16") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5") (d #t) (k 0)))) (h "1krp5khz0cxggp34a3xjrsg2lad16vnq4f1qqr4d5mffjvairil2")))

(define-public crate-staticmap-0.3.1 (c (n "staticmap") (v "0.3.1") (d (list (d (n "attohttpc") (r "^0.16") (f (quote ("tls-rustls"))) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "png") (r "^0.16") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5") (d #t) (k 0)))) (h "1n8y341wi39mh21006zsk0gv3gwirbaxp4r5pj8n9qllswn02plm")))

(define-public crate-staticmap-0.3.2 (c (n "staticmap") (v "0.3.2") (d (list (d (n "attohttpc") (r "^0.17") (f (quote ("tls-rustls"))) (k 0)) (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "png") (r "^0.16") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5") (d #t) (k 0)))) (h "0b2mly09qhxda8s4l40gbi0541p1j2gpvccwhlibrnanbvz8g75n")))

(define-public crate-staticmap-0.4.0 (c (n "staticmap") (v "0.4.0") (d (list (d (n "attohttpc") (r "^0.17") (f (quote ("tls-rustls"))) (k 0)) (d (n "png") (r "^0.16") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5") (d #t) (k 0)))) (h "06gx1xjajsqklglixq0l2q4937w1qcfr96mr6zlfzc07ap7wyiba")))

(define-public crate-staticmap-0.4.1 (c (n "staticmap") (v "0.4.1") (d (list (d (n "attohttpc") (r "^0.19") (f (quote ("tls-rustls"))) (k 0)) (d (n "png") (r "^0.17") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.6") (d #t) (k 0)))) (h "101iq7s3k7q3bk89p6k4qc604hyc3qrzbm8xhfvwcpclcbar8fi8")))

(define-public crate-staticmap-0.4.2 (c (n "staticmap") (v "0.4.2") (d (list (d (n "attohttpc") (r "^0.27") (f (quote ("tls-rustls"))) (k 0)) (d (n "png") (r "^0.17") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 0)))) (h "1xqmz0bxpdig0i2d3qa4f6i50n1khhk8yn78xl5zc8qla6f78pix")))

