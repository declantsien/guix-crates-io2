(define-module (crates-io st at static_leak) #:use-module (crates-io))

(define-public crate-static_leak-0.1.0 (c (n "static_leak") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1pjh53n21127sd41vi6nyjqzjiq12yfq3i9rinrmmr0iq2jcw46w") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("spin" "dep:spin") ("async-std" "dep:async-std"))))))

