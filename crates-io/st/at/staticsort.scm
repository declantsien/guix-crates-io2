(define-module (crates-io st at staticsort) #:use-module (crates-io))

(define-public crate-staticsort-0.1.0 (c (n "staticsort") (v "0.1.0") (h "0gzq7602wdks8a7m99946lli982b0a6a6w59jr7z234jkvcwfkyf") (y #t)))

(define-public crate-staticsort-0.1.1 (c (n "staticsort") (v "0.1.1") (h "1dq4z0fgkhc0jvzbr5vjlhqjbafc7qnv1n1chsjrm9fchywx7n41") (y #t)))

(define-public crate-staticsort-0.1.2 (c (n "staticsort") (v "0.1.2") (h "1bk52saq33arrd2jirxxag5kj5dv77kajx20a71gkxvrw0byiiw1") (y #t)))

(define-public crate-staticsort-0.1.3 (c (n "staticsort") (v "0.1.3") (h "01ax6m7hkz4z2g68612zzyj4wxd8n9if7fnzfph2avh1scv0k847") (y #t)))

(define-public crate-staticsort-0.1.4 (c (n "staticsort") (v "0.1.4") (h "1rhaba6d3w0bd5bngyymn6l0g9n8dshspkfqarj8kiinhp0444y5")))

(define-public crate-staticsort-0.1.5 (c (n "staticsort") (v "0.1.5") (h "076mq19dwp9j9z9vjd1csxh5spq413ba739rhy3cpbf4734w54dx")))

(define-public crate-staticsort-0.2.0 (c (n "staticsort") (v "0.2.0") (h "05vflc7v3dq21dhawrzarjx5g15kjnc5l4wvp4qv3pc5xxfidyxx")))

(define-public crate-staticsort-0.3.0 (c (n "staticsort") (v "0.3.0") (h "0353z3xl28mp9ixpppamzqwvnsq009a5sx20pa4kkjd9lhhh1fiz")))

(define-public crate-staticsort-0.4.0 (c (n "staticsort") (v "0.4.0") (h "1fk10vmrrb8f98i3zgj1fpc37ndrb3z9ymd15ivp5p3jhzkrgsxb")))

(define-public crate-staticsort-0.4.1 (c (n "staticsort") (v "0.4.1") (h "0ka1fnh03fs6ii63ig4rkk1082if0anxxzxykhnkzwq31052xk4w")))

(define-public crate-staticsort-0.4.2 (c (n "staticsort") (v "0.4.2") (h "1cmw3ycp61zwd6vbhjh87qrv11i016ky6hwcizwiniq23s4hz3v8")))

