(define-module (crates-io st at static_vcruntime) #:use-module (crates-io))

(define-public crate-static_vcruntime-1.0.0 (c (n "static_vcruntime") (v "1.0.0") (h "0wiy0srwaa8qi1x2d0wgpj6g9p5r9w3lh8h18rbmkxd8ssc8cfzl")))

(define-public crate-static_vcruntime-1.0.1 (c (n "static_vcruntime") (v "1.0.1") (h "1pr71yakvw85fdq83ick92xvivc39gcm9h08qm4fbpbrg58fc4g7")))

(define-public crate-static_vcruntime-1.1.0 (c (n "static_vcruntime") (v "1.1.0") (h "0b6mm3xiys20l414slwaz7kqbr0khrvbb4dnkh7xskw9a30261k0")))

(define-public crate-static_vcruntime-1.2.0 (c (n "static_vcruntime") (v "1.2.0") (h "1pmpwzwwbmxlv2ff7fnsllnlpli4pm43s0x0vjw73imk6vwrra3s")))

(define-public crate-static_vcruntime-1.3.0 (c (n "static_vcruntime") (v "1.3.0") (h "1013jsyg3pmm81xp7mn9y1kqgk93ri1rkcm725gy8vkgg08lx328")))

(define-public crate-static_vcruntime-1.4.0 (c (n "static_vcruntime") (v "1.4.0") (h "14y5k0gn6zpin3lz70mmys8mipkh7ygq47bjvp169gwljdyk3sph")))

(define-public crate-static_vcruntime-1.4.1 (c (n "static_vcruntime") (v "1.4.1") (h "0gkbjhyic9il8sspvrc3hf4sr26d80sdjdx32rak01i1y1sw3hic")))

(define-public crate-static_vcruntime-1.5.0 (c (n "static_vcruntime") (v "1.5.0") (h "0sirh83vlqr5pqpk888qdfmylwi7s6cscwcpl7bdbvynwg2nm9l3")))

(define-public crate-static_vcruntime-1.5.1 (c (n "static_vcruntime") (v "1.5.1") (h "1ylqq3rb5qbpqgcvcc0niqca1pjzc5akidbqqn0fl43jzvb1b358")))

(define-public crate-static_vcruntime-2.0.0 (c (n "static_vcruntime") (v "2.0.0") (h "16y8klkf1sijf83p6y5r1pbmria7043g8nq78vfgkph3g23kwklm")))

