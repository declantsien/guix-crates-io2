(define-module (crates-io st at static-address) #:use-module (crates-io))

(define-public crate-static-address-0.1.1 (c (n "static-address") (v "0.1.1") (d (list (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 2)) (d (n "static-address-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mmxcs6m86cjkwyhf0pciq8m6sakna9cai7y45l8yhl3v1mwfq58") (f (quote (("address32" "static-address-parser/address32") ("address20" "static-address-parser/address20"))))))

(define-public crate-static-address-0.1.2 (c (n "static-address") (v "0.1.2") (d (list (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 2)) (d (n "static-address-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dfl5sif369bqrp0016qhl2idxw7hwmbxgv9nrc69d7rkrhripcl") (f (quote (("default") ("address32" "static-address-parser/address32") ("address20" "static-address-parser/address20"))))))

(define-public crate-static-address-0.2.0 (c (n "static-address") (v "0.2.0") (d (list (d (n "static-address-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04xmk3gd00flkb0lmkvip27zbbcfcwkdsjz4hsx0i6mlqrz551kf") (f (quote (("default") ("address32" "static-address-parser/address32") ("address20" "static-address-parser/address20"))))))

(define-public crate-static-address-0.2.2 (c (n "static-address") (v "0.2.2") (d (list (d (n "account-address") (r "^0.2.0") (d #t) (k 0)) (d (n "static-address-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0dp0vg6pixifx8gmdsf6iqgf8wffsk1dhpcq4y8rir7bfsxpqkcb") (f (quote (("default") ("address32" "account-address/address32" "static-address-macro/address32") ("address20" "account-address/address20" "static-address-macro/address20"))))))

