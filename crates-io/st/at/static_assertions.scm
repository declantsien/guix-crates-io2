(define-module (crates-io st at static_assertions) #:use-module (crates-io))

(define-public crate-static_assertions-0.1.0 (c (n "static_assertions") (v "0.1.0") (h "0bsw08s0gxvj7r4gmcqqrd98cxw31mvp8y0s48c0rc2mv066la77") (f (quote (("nightly" "const_fn") ("failure") ("const_fn"))))))

(define-public crate-static_assertions-0.1.1 (c (n "static_assertions") (v "0.1.1") (h "19w93xx2hg531r3gk7p7a5s8pib430blsyzx3zhrcryvd3pdc1pl") (f (quote (("nightly" "const_fn") ("failure") ("const_fn"))))))

(define-public crate-static_assertions-0.2.0 (c (n "static_assertions") (v "0.2.0") (h "05b7yviaf58yzfa9q2rfh1s908qw1dzl00kwjzj8vk9fmr0l53dw") (f (quote (("nightly" "const_fn") ("failure") ("const_fn"))))))

(define-public crate-static_assertions-0.2.1 (c (n "static_assertions") (v "0.2.1") (h "0i7grkgizhlniz43y53c5wxk9rlx1ss01gx3w0ac9f3k4nsj17r3") (f (quote (("nightly" "const_fn") ("failure") ("const_fn"))))))

(define-public crate-static_assertions-0.2.2 (c (n "static_assertions") (v "0.2.2") (h "1wb59xn8pqym0ixj234bpsh850rgnwhbq6zplvr4iyinjs5b2hgl") (f (quote (("nightly" "const_fn") ("failure") ("const_fn"))))))

(define-public crate-static_assertions-0.2.3 (c (n "static_assertions") (v "0.2.3") (h "05p93kd3y183888ix60gl4v42ll1qmyrmvc6689raqjlqyghqs8s") (f (quote (("nightly" "const_fn") ("failure") ("const_fn"))))))

(define-public crate-static_assertions-0.2.4 (c (n "static_assertions") (v "0.2.4") (h "0isj50dhbhwnk0gnqnh84makqfznb26n07v1vlbqc8q3sl85qn8i") (f (quote (("nightly" "const_fn") ("failure") ("const_fn"))))))

(define-public crate-static_assertions-0.2.5 (c (n "static_assertions") (v "0.2.5") (h "1mcrj7kc4h89d4njbic346dpz3vh9b9h3r937b5n2n214qqy56y1")))

(define-public crate-static_assertions-0.3.0 (c (n "static_assertions") (v "0.3.0") (h "0cz4wy9s1klrq75vbqcsll27r8qr89db4513672zkfl7i3hnx0xb") (f (quote (("nightly"))))))

(define-public crate-static_assertions-0.3.1 (c (n "static_assertions") (v "0.3.1") (h "1lb5zh2l7iqrl3darc2kn2py3i96y7ccnyb4xpdngwi4yisy971q") (f (quote (("nightly"))))))

(define-public crate-static_assertions-0.3.2 (c (n "static_assertions") (v "0.3.2") (h "1bw9cxxrdixd0xh3909j3glv7p1w4x9ralk3yh2k00zj36yjqg7c") (f (quote (("nightly"))))))

(define-public crate-static_assertions-0.3.3 (c (n "static_assertions") (v "0.3.3") (h "0hdj6cxknz53vxnf7szpifs16rk4l3x0n0i4bzmm6li1v8vdxy5l") (f (quote (("nightly"))))))

(define-public crate-static_assertions-0.3.4 (c (n "static_assertions") (v "0.3.4") (h "1lw33i89888yb3x29c6dv4mrkg3534n0rlg3r7qzh4p58xmv6gkz") (f (quote (("nightly"))))))

(define-public crate-static_assertions-1.0.0 (c (n "static_assertions") (v "1.0.0") (h "0my6vsxnqrgsliczrw5gg3mfvnv2i8vir6v3d1dnd22n6l9kd88g") (f (quote (("nightly"))))))

(define-public crate-static_assertions-1.1.0 (c (n "static_assertions") (v "1.1.0") (h "0gsl6xmw10gvn3zs1rv99laj5ig7ylffnh71f9l34js4nr4r7sx2") (f (quote (("nightly"))))))

