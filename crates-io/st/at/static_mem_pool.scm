(define-module (crates-io st at static_mem_pool) #:use-module (crates-io))

(define-public crate-static_mem_pool-0.1.0 (c (n "static_mem_pool") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("real_blackbox" "cargo_bench_support"))) (k 2)))) (h "16k56ggziwzmhvaf6ciz6fda1spgyyd5m64mcxwz9k1j2mwxp0sf") (y #t)))

(define-public crate-static_mem_pool-0.2.0 (c (n "static_mem_pool") (v "0.2.0") (d (list (d (n "const_collections") (r "^0.1.0") (d #t) (k 0)))) (h "1yvz7dgjysdidnhn24i86wk5gsnmap9dxk4lvmqa859zq6r93kxx") (y #t)))

(define-public crate-static_mem_pool-0.3.0 (c (n "static_mem_pool") (v "0.3.0") (h "065nfzdxhaqq50rcg1vpifsccks7vsm1ngg9zyav0dip0vxsrb20") (y #t)))

