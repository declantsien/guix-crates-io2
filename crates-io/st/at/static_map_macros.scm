(define-module (crates-io st at static_map_macros) #:use-module (crates-io))

(define-public crate-static_map_macros-0.1.0 (c (n "static_map_macros") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 2)) (d (n "phf_macros") (r "^0.7") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 2)) (d (n "static_map") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1rgh1wxvci7pzvk5cli1f0cihzn6lmhybymxwrlhhidb8xaw3jky")))

(define-public crate-static_map_macros-0.1.1 (c (n "static_map_macros") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 2)) (d (n "phf_macros") (r "^0.7") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 2)) (d (n "static_map") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1zac0jg907sdhqxcihad17lhga6vkqp5k52a3dwl2psqgv7gyrk2")))

(define-public crate-static_map_macros-0.2.0-alpha (c (n "static_map_macros") (v "0.2.0-alpha") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 2)) (d (n "phf_macros") (r "^0.7") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 2)) (d (n "static_map") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0w854hjl3wbbd3ff2r4sn0g8gdnjsd4mppxzvg5ymym989gkbxlg") (y #t)))

(define-public crate-static_map_macros-0.2.0-beta (c (n "static_map_macros") (v "0.2.0-beta") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 2)) (d (n "phf_macros") (r "^0.7") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 2)) (d (n "static_map") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0mxa3af5anz3iw8gmxzq5375gjmjj935iv0z80vpfnvq522g201k")))

