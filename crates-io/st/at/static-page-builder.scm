(define-module (crates-io st at static-page-builder) #:use-module (crates-io))

(define-public crate-static-page-builder-0.1.0 (c (n "static-page-builder") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "maud") (r "^0.21.0") (f (quote ("rocket"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.3.0") (d #t) (k 0)))) (h "0wlc1z670wqg4mcvn8wpdsqi6lsz9xl1wcdwm8w0l5llid69i6bf")))

(define-public crate-static-page-builder-0.2.0 (c (n "static-page-builder") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "maud") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typed-builder") (r "^0.3.0") (d #t) (k 0)))) (h "1n05j4lyw2pvk2p5m7p0182xxl5rzsnfb34srzknqlcf5hnly6jx") (f (quote (("rocket" "maud/rocket"))))))

