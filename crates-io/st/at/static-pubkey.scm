(define-module (crates-io st at static-pubkey) #:use-module (crates-io))

(define-public crate-static-pubkey-1.0.0 (c (n "static-pubkey") (v "1.0.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1gcbfdfx9p347sqh5rfqr3n48jz5ps9zxmxb41lwryhizqzmn858")))

(define-public crate-static-pubkey-1.0.1 (c (n "static-pubkey") (v "1.0.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1sn71cm2s3x11v20n3g6mvkxa141ci12lmy6dgfdlhxhg9dqgmi3")))

(define-public crate-static-pubkey-1.0.2 (c (n "static-pubkey") (v "1.0.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04c09b43syw69qw5h3y7ayrmnxw5dkq46iss4q16rz8hb7ky3473")))

(define-public crate-static-pubkey-1.0.3 (c (n "static-pubkey") (v "1.0.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "144fkaxly78w5jr4axk8y9hqam6hlg8nfamz30dmxxckbjadaiq5")))

