(define-module (crates-io st at stationary-cam) #:use-module (crates-io))

(define-public crate-stationary-cam-0.2.1 (c (n "stationary-cam") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0qcviibl0hmga6yl6zy4wjnsmjr12ck64xkz3a29j2aa7l51pn9y")))

(define-public crate-stationary-cam-0.2.2 (c (n "stationary-cam") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0i2199czsywqjqi4bzyfa9gjwp9a09i0s62h5z21xaw0yzylb1xk")))

(define-public crate-stationary-cam-0.2.3 (c (n "stationary-cam") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "0rmby58dajlb9bgm06ah4vdgcy0l7phcxpzx462s09vdggscdny3")))

(define-public crate-stationary-cam-0.2.4 (c (n "stationary-cam") (v "0.2.4") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "15d6h4y4ygmgp0pvj3m4lhihvl16i295jnhs0ylgkjvi9fjwmz7a")))

