(define-module (crates-io st at static) #:use-module (crates-io))

(define-public crate-static-0.0.1 (c (n "static") (v "0.0.1") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "iron-test") (r "*") (d #t) (k 0)) (d (n "mount") (r "*") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1rk5dlzrj00fqvqmg1y6rrvn4ajpb2qn9r9asicf4sgw3mzf3dh4")))

(define-public crate-static-0.0.2 (c (n "static") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "iron-test") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mount") (r "*") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0g8phfw5x620d0r52j9z8sll5d50i421fgba6hqzyajm4c699y2a")))

(define-public crate-static-0.0.3 (c (n "static") (v "0.0.3") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "iron-test") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mount") (r "*") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1qr7aqmycsgpx10zyrf2jygalr00f42zbf7dch4gsycca6fkb3q2")))

(define-public crate-static-0.0.4 (c (n "static") (v "0.0.4") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "iron-test") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mount") (r "*") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "19k0x2n9hcy1p547qbr1dkfzkps0l07l9m7yrli6w8crm6m7hxwh")))

