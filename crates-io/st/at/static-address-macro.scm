(define-module (crates-io st at static-address-macro) #:use-module (crates-io))

(define-public crate-static-address-macro-0.2.2 (c (n "static-address-macro") (v "0.2.2") (d (list (d (n "static-address-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15wrnsalvccgbjjz177h0rhs0al57bzjamli4rs00dwx09aflsmr") (f (quote (("default") ("address32" "static-address-parser/address32") ("address20" "static-address-parser/address20"))))))

