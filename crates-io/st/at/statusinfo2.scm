(define-module (crates-io st at statusinfo2) #:use-module (crates-io))

(define-public crate-statusinfo2-0.0.1 (c (n "statusinfo2") (v "0.0.1") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0avw9qr70xa5jpazvmna46gjzx7bqmgkw5rajldsbadk9vslvksx")))

(define-public crate-statusinfo2-0.0.2 (c (n "statusinfo2") (v "0.0.2") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wih6m08sz2gpnv8vy6gf5xddz6yi1jw25dc73cf5cnkpmcdhspm")))

(define-public crate-statusinfo2-0.0.3 (c (n "statusinfo2") (v "0.0.3") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0yjddyxk2hzhsw59h6bgk7vg0yybyngas309mypz8rv3bn73y8ra")))

(define-public crate-statusinfo2-0.0.4 (c (n "statusinfo2") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0b4wf714jkn50nf14qbmgsddiw6vvq9c3nh6hwq65kjxsr58mdrs")))

(define-public crate-statusinfo2-0.0.5 (c (n "statusinfo2") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0cizp5s9r4i68hgphpmf3isz2niyr16h9442hhlqir6ngwfj8cpj")))

