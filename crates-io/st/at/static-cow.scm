(define-module (crates-io st at static-cow) #:use-module (crates-io))

(define-public crate-static-cow-0.1.0 (c (n "static-cow") (v "0.1.0") (h "1z1hn4dvb0lmhf8mxadd72xw6aimf4z9gicgkhc0yngzzv5hqw1h")))

(define-public crate-static-cow-0.1.1 (c (n "static-cow") (v "0.1.1") (h "1q9nmzlc900adhgj67r90rfn45w4gjzsnrq4rz2xmqj5mbrmkshn")))

(define-public crate-static-cow-0.2.0 (c (n "static-cow") (v "0.2.0") (h "054xxq8v3ijk2hradgl4f6b6905vnizxph2aksa8xp67103g4d7x")))

