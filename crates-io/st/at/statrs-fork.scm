(define-module (crates-io st at statrs-fork) #:use-module (crates-io))

(define-public crate-statrs-fork-0.17.0 (c (n "statrs-fork") (v "0.17.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1b04yf3liwh8nzlp9hpnzs1ajj1jlmryzabw1fn0ny6s7gv82dal") (f (quote (("nightly"))))))

