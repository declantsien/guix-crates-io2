(define-module (crates-io st at state_machine) #:use-module (crates-io))

(define-public crate-state_machine-0.1.0 (c (n "state_machine") (v "0.1.0") (h "17fdqwg98mx7rp9ng553m45wsh27p08q0vcqk1g7nzp9v0xs1jw2")))

(define-public crate-state_machine-0.1.1 (c (n "state_machine") (v "0.1.1") (h "067pl1s2ydc79hil5j6l8x1gfmf7rzqv9q3ny369c1ssj4czsw24")))

(define-public crate-state_machine-0.1.12 (c (n "state_machine") (v "0.1.12") (h "0xpfhx784whvn0b99lzqjc5iipa1yh7s1qsg3i968v0i4bbj3lh0")))

(define-public crate-state_machine-0.1.13 (c (n "state_machine") (v "0.1.13") (h "1z4im1hjzl8gqfgsd71r5c1n5991xc2ghni99rzkaxd34y327yc8")))

(define-public crate-state_machine-0.1.14 (c (n "state_machine") (v "0.1.14") (h "0lbnw6ld7ihykk6966fgkmcrwpczv2vhbdcpa5razkyzrl5x0kh1")))

