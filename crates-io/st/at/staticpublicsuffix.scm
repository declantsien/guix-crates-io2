(define-module (crates-io st at staticpublicsuffix) #:use-module (crates-io))

(define-public crate-staticpublicsuffix-0.1.0 (c (n "staticpublicsuffix") (v "0.1.0") (d (list (d (n "lazy_static") (r "< 2") (d #t) (k 0)) (d (n "publicsuffix") (r "^1.5.0") (d #t) (k 0)))) (h "1k0pdaiib0kxbndi5m08cmvqhag3ryvzb448pqfswxf3hkirybrq")))

