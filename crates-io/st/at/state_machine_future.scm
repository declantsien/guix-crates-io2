(define-module (crates-io st at state_machine_future) #:use-module (crates-io))

(define-public crate-state_machine_future-0.1.0 (c (n "state_machine_future") (v "0.1.0") (d (list (d (n "derive_state_machine_future") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "1ysar82gfnsxk5n8v29z0lw7bw7r95d2n9ybz0yrvg1jia9aba8g") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.1 (c (n "state_machine_future") (v "0.1.1") (d (list (d (n "derive_state_machine_future") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "1wwpszd7x8w7cclz23czgggmal9bwgyjx6jypyfk5vhaqx3cb6c1") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.2 (c (n "state_machine_future") (v "0.1.2") (d (list (d (n "derive_state_machine_future") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "09ybn6jsnb2xbp0ljwmz8c45ypza06znx4hvv7y70cb8289hsh55") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.3 (c (n "state_machine_future") (v "0.1.3") (d (list (d (n "derive_state_machine_future") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "0wd09khschsf44whkydsk2s3vybhcjcd4bfv25h6llmfrrnhnm42") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.4 (c (n "state_machine_future") (v "0.1.4") (d (list (d (n "derive_state_machine_future") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "03p9212qhzdr4yymk9zk8z5m6902z4la65n1ff3y4ddpwyxy40qh") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.5 (c (n "state_machine_future") (v "0.1.5") (d (list (d (n "derive_state_machine_future") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "1m1va98g7lgw8309r333vcxjmhlxbhxcgnk0742nyfslfndqxfw1") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.6 (c (n "state_machine_future") (v "0.1.6") (d (list (d (n "derive_state_machine_future") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "1nkr06ihy953430r35mnxp4mcxw3yr556fkzf84kwhfs9mbvpbza") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.7 (c (n "state_machine_future") (v "0.1.7") (d (list (d (n "derive_state_machine_future") (r "^0.1.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "06zhnsg8p6299rmky5rljk3qdw8bb8lp624fs04v0rncybz45j11") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.1.8 (c (n "state_machine_future") (v "0.1.8") (d (list (d (n "derive_state_machine_future") (r "= 0.1.8") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "065r1r1s76jzhhwmgkzn0q4m23mmkzv53jdjwgggi065dry4ara3") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

(define-public crate-state_machine_future-0.2.0 (c (n "state_machine_future") (v "0.2.0") (d (list (d (n "derive_state_machine_future") (r "= 0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "rent_to_own") (r "^0.1.0") (d #t) (k 0)))) (h "0gdi4idfsklpa5ks3s3f6i9s5bvanyn4frifw6y8br5a9di1s3jk") (f (quote (("debug_code_generation" "derive_state_machine_future/debug_code_generation"))))))

