(define-module (crates-io st at static-reflect-derive-internals) #:use-module (crates-io))

(define-public crate-static-reflect-derive-internals-0.1.0 (c (n "static-reflect-derive-internals") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12npcgy908rszlsafc7l42y9m6h74dkxr0qzxs7d1xv098l2qllj")))

(define-public crate-static-reflect-derive-internals-0.2.0-alpha.1 (c (n "static-reflect-derive-internals") (v "0.2.0-alpha.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13q2y35qimfk0smz4v1ja5j6qj6ymi6c5xrivhp10ka4nn0j1w58")))

