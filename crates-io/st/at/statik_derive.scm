(define-module (crates-io st at statik_derive) #:use-module (crates-io))

(define-public crate-statik_derive-0.2.0 (c (n "statik_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1sjxpz76n71rh5nh491k9hd4swznzxbhykp46p9s43mvkjhwd96q")))

(define-public crate-statik_derive-0.2.1 (c (n "statik_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "04vg0vxf08h4x7cckqvm13qr32r3as4mc0h9li47kkw4bsqvmcx9")))

