(define-module (crates-io st at static-atom) #:use-module (crates-io))

(define-public crate-static-atom-0.1.0 (c (n "static-atom") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "0y6nc0p1l9d7lqxq1k6k5223alj94zrbrzbp0wh49nx0qi4c1kgl") (f (quote (("default" "serde"))))))

(define-public crate-static-atom-0.1.1 (c (n "static-atom") (v "0.1.1") (d (list (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "16l2fyrq0rkj8jx9ia6zk4cxrvv36b7zrvx08bcbb1rbvxqm0sx9")))

(define-public crate-static-atom-0.1.2 (c (n "static-atom") (v "0.1.2") (d (list (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "185zjlsiq59yn5ldacq29ldmc7pkbnbkviglkzwwj17pgw71n389")))

