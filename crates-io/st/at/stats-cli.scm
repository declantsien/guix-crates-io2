(define-module (crates-io st at stats-cli) #:use-module (crates-io))

(define-public crate-stats-cli-0.0.0 (c (n "stats-cli") (v "0.0.0") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "05pv5i0y15crqwpyyyxfs5gsnyx6xh7qf84i8lrszzj6d31xyl5l")))

(define-public crate-stats-cli-0.0.1 (c (n "stats-cli") (v "0.0.1") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "1116z2akxahkc98x1xk4552crn7cag7dwqnqhcx4zk79f7mdp8lv")))

(define-public crate-stats-cli-0.0.2 (c (n "stats-cli") (v "0.0.2") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "01grsyh3k08rlfdyn8nx9722jwpj8kpapqcd39hlr740nki59899")))

(define-public crate-stats-cli-0.0.3 (c (n "stats-cli") (v "0.0.3") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "01fnrydkljmx2xf8lm9lvab8q7b6z5l8v0smv7hdi62hahsm8krj")))

(define-public crate-stats-cli-0.0.4 (c (n "stats-cli") (v "0.0.4") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "1qji9b17nxxqvbkxv8imssnvyfxpcgpj7617f69r1lmbhn5f75l7")))

(define-public crate-stats-cli-0.0.5 (c (n "stats-cli") (v "0.0.5") (d (list (d (n "clap") (r "^2.21") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "1pb8fc5lxlhw3p3jbzsj5p92bzhsxzhsz6l5l8nshzxd1ghs7jnk")))

(define-public crate-stats-cli-1.0.0 (c (n "stats-cli") (v "1.0.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "03h65c5ifa5y9kb94h8sffaghpz141fzrh3w79m35i6xcrp67l86")))

(define-public crate-stats-cli-2.0.0 (c (n "stats-cli") (v "2.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "order-stat") (r "^0.1") (d #t) (k 0)))) (h "1v7xkax36dfzpci9qr2zjfn3ivf1vz826d7calr98pm9lw9rhpiz")))

(define-public crate-stats-cli-3.0.0 (c (n "stats-cli") (v "3.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ywhbj1rb9k8083f2v5ha38pqy0z3pya149xvgh588gwn6j8ibds")))

(define-public crate-stats-cli-3.0.1 (c (n "stats-cli") (v "3.0.1") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wf8y5ygbbar3m6s2jzj26cdrr3r7x0k7v8av7yc86x9r17nqy68")))

