(define-module (crates-io st at stat_common) #:use-module (crates-io))

(define-public crate-stat_common-1.1.4 (c (n "stat_common") (v "1.1.4") (d (list (d (n "bytes") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "chrono") (r "^0.4") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "protobuf-src") (r "^1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)) (d (n "tonic") (r "^0.11") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (d #t) (k 1)) (d (n "tonic-build") (r "^0.11") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "1hmp86yr4h2crdzyyq7q27sbln995vyvz2nd4a2a2l07wngfk08h")))

