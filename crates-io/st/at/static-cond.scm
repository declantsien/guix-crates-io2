(define-module (crates-io st at static-cond) #:use-module (crates-io))

(define-public crate-static-cond-0.1.0 (c (n "static-cond") (v "0.1.0") (h "0v0j7lwh6h9zfh1w6c4in3r7nkdp76z7j6wk7x97inbm2q1m0ns0")))

(define-public crate-static-cond-0.1.1 (c (n "static-cond") (v "0.1.1") (h "02p3d6iqd3yc3s8q0d78v9clb3x58fpm5jg00x0yx2nyb1ac9rl1")))

(define-public crate-static-cond-0.2.0 (c (n "static-cond") (v "0.2.0") (h "1ng950r0416w1wvmr4qivswcdsznpqcz2jyhn7k3i9qrj6y0f041")))

(define-public crate-static-cond-0.2.1 (c (n "static-cond") (v "0.2.1") (h "1q1i4nk0h6ia58k48v9n5imksfjbma99i3s9487dmbh01q86iv4s")))

(define-public crate-static-cond-0.3.0 (c (n "static-cond") (v "0.3.0") (h "1mhj5xjywxbll5y8gfihvsx9yb340frbm02f0cwmgc545vbys2b5")))

