(define-module (crates-io st at stats_utils) #:use-module (crates-io))

(define-public crate-stats_utils-0.1.0 (c (n "stats_utils") (v "0.1.0") (h "1sps310ljv3gmp7irxmqnwha8ba3a3dpklzsnp1vsjc5l1lwl9mq")))

(define-public crate-stats_utils-0.1.1 (c (n "stats_utils") (v "0.1.1") (h "0bf98x96kd8wwhnzch9khzx3f5bdxp40277pxgy5vdr8c1a1vx7h")))

(define-public crate-stats_utils-0.1.2 (c (n "stats_utils") (v "0.1.2") (h "1hknws5b347rargmh984m53fhg2k665sjiskmra83kxwwsk6k127")))

(define-public crate-stats_utils-0.1.3 (c (n "stats_utils") (v "0.1.3") (h "0xxqkzinj2xd4m22snsg49j1hqc5n5lzxc83b054ss9n99ribmmj")))

