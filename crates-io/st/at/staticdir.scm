(define-module (crates-io st at staticdir) #:use-module (crates-io))

(define-public crate-staticdir-0.3.0 (c (n "staticdir") (v "0.3.0") (d (list (d (n "filetime") (r "^0.1.9") (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "iron-test") (r "^0.2") (d #t) (k 2)) (d (n "mount") (r "^0") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "staticfile") (r "^0.1") (d #t) (k 2)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1i7jwira6qfvi2gr3ng1d72fap1gak58j9d8ci2661av1yr51nl7")))

(define-public crate-staticdir-0.3.1 (c (n "staticdir") (v "0.3.1") (d (list (d (n "filetime") (r "^0.1.9") (d #t) (k 0)) (d (n "iron") (r "^0.2") (d #t) (k 0)) (d (n "iron-test") (r "^0.2") (d #t) (k 2)) (d (n "mount") (r "^0") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "staticfile") (r "^0.1") (d #t) (k 2)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0ypmw7hb5rpr7fz0y85fa617lbvjjadn4283pxkhb6fzcs58xx31")))

