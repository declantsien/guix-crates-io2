(define-module (crates-io st at static-events) #:use-module (crates-io))

(define-public crate-static-events-0.1.0 (c (n "static-events") (v "0.1.0") (h "0zbmj3giz6g9bcn4llagr6n15ad4ar8yn2brz2zxkxfa5gi0a8ai")))

(define-public crate-static-events-0.1.1 (c (n "static-events") (v "0.1.1") (h "1mpc01vbp51y0by3kncj28jpl633i3gpnh54ar7v9sji73akwxwn")))

(define-public crate-static-events-0.1.2 (c (n "static-events") (v "0.1.2") (d (list (d (n "unhygienic") (r "^0.1") (d #t) (k 0)))) (h "0f8fvik97xndvrndwsxbxj351033grnr326nazf7xlcaxf1qgf81")))

(define-public crate-static-events-0.1.3 (c (n "static-events") (v "0.1.3") (d (list (d (n "unhygienic") (r "^0.1") (d #t) (k 0)))) (h "03pjalznrcbgsccnjd73xfwi5w1z1h0w7gasx5h8dhihizq6fagh")))

