(define-module (crates-io st at statue) #:use-module (crates-io))

(define-public crate-statue-0.1.0 (c (n "statue") (v "0.1.0") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0gsw8ms8yriknfnlkdnyl86k23l3zg08mvsz4krifv6zpcnvabhd") (r "1.65.0")))

(define-public crate-statue-0.1.1 (c (n "statue") (v "0.1.1") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0gwva1m4k7hpdq5ppikrnnk7xphiz94mkprgcd1j7vc0gw8afrx7") (r "1.65.0")))

(define-public crate-statue-0.1.2 (c (n "statue") (v "0.1.2") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "03d1wdgcwzm1m2wfj3vh3z7w7w1c1jmbbv4gcakcv5075c1b9z4b") (r "1.65.0")))

(define-public crate-statue-0.1.3 (c (n "statue") (v "0.1.3") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1qix56p1g210ny7g2gy2q830byjv1zy777l2kj2dph78217pqvqs") (r "1.65.0")))

(define-public crate-statue-0.1.4 (c (n "statue") (v "0.1.4") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0w2m40zxw2calmivxyl5qpx2r2q4m5aifn6w51qnmcr3ln4iazrk") (r "1.65.0")))

(define-public crate-statue-0.1.5 (c (n "statue") (v "0.1.5") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "135vnl3wrap3czgqw1g5m1zzwxwxf2fbr5hxbxrjhiy8q1wsa4jd") (r "1.65.0")))

(define-public crate-statue-0.1.6 (c (n "statue") (v "0.1.6") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "187c9vnjp9q4bdjzy29k10xq4f57j5azq9albk39gjxrl59sv6g1") (r "1.65.0")))

(define-public crate-statue-0.1.7 (c (n "statue") (v "0.1.7") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "14d95g5nq17pb0jmsqzdpzckj661ny2n70hapzgirwc0h6cpda45") (r "1.65.0")))

(define-public crate-statue-0.1.8 (c (n "statue") (v "0.1.8") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0lh15vria1magfpxgbm737xixqcw185sij3vd4skmvja3mwmd2qs") (r "1.65.0")))

(define-public crate-statue-0.1.9 (c (n "statue") (v "0.1.9") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1ng3bpzq9ay2bj8l7ah5ispnl2sbjaagfbxa1y3qw2a9apx1wknr") (r "1.65.0")))

(define-public crate-statue-0.1.10 (c (n "statue") (v "0.1.10") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1vxlzdjcyqqa1zlp7h9agyi5agj8i3dr46r449lbqj2lfy439r8q") (r "1.65.0")))

(define-public crate-statue-0.2.0 (c (n "statue") (v "0.2.0") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "00y605v3rcxbhn47zf4rspl1ikll9gg8028c4h10f1ix0d4s4gcm") (r "1.65.0")))

(define-public crate-statue-0.3.0 (c (n "statue") (v "0.3.0") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "1j7yb8w344fm99d7czdpjq0ijkvbk30ndlzwhw7rhi0ap2jf4x73") (r "1.65.0")))

(define-public crate-statue-0.3.1 (c (n "statue") (v "0.3.1") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "03iqjfpfgwpk97xpimqs83czk8cgri2qqj9ccmm18ki5dfmdb8nw") (r "1.65.0")))

