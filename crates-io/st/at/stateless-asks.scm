(define-module (crates-io st at stateless-asks) #:use-module (crates-io))

(define-public crate-stateless-asks-0.1.0 (c (n "stateless-asks") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "metaplex-token-metadata") (r "^0.0.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.8.1") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.8.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.8.1") (d #t) (k 2)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aic225g5iyq8slb5c91q5a0q72n06zm3ldkkq18nkgavh90fwwz") (f (quote (("test-bpf") ("no-entrypoint"))))))

