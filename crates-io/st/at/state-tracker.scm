(define-module (crates-io st at state-tracker) #:use-module (crates-io))

(define-public crate-state-tracker-0.1.0 (c (n "state-tracker") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i122bp8dlk0rqm2wns09ic120grc1j0apnc1w4281nbxwv2n0ly")))

