(define-module (crates-io st at static_cell) #:use-module (crates-io))

(define-public crate-static_cell-0.0.0 (c (n "static_cell") (v "0.0.0") (d (list (d (n "atomic-polyfill") (r "^1.0.1") (d #t) (k 0)))) (h "0ybziwlykb04mqz180a971dqlswfpbf4mkm9nljnmhj3mcy9l6z2")))

(define-public crate-static_cell-1.0.0-alpha.1 (c (n "static_cell") (v "1.0.0-alpha.1") (d (list (d (n "atomic-polyfill") (r "^1.0.1") (d #t) (k 0)))) (h "0xigfyb7fxch83z54mb8n79rg200m6ak4r5mqk3q5v34553avrhc")))

(define-public crate-static_cell-1.0.0 (c (n "static_cell") (v "1.0.0") (d (list (d (n "atomic-polyfill") (r "^1.0.1") (d #t) (k 0)))) (h "1kn2bim9aqirkr2czlm82713jrby3m0faxjy2skkzx911ljprhz4")))

(define-public crate-static_cell-1.1.0 (c (n "static_cell") (v "1.1.0") (d (list (d (n "atomic-polyfill") (r "^1.0.1") (d #t) (k 0)))) (h "18p869lmrnb6m5p7l9l8d5jbic3db9afh95d5cmibkh0khg8aspd") (f (quote (("nightly"))))))

(define-public crate-static_cell-1.2.0 (c (n "static_cell") (v "1.2.0") (d (list (d (n "atomic-polyfill") (r "^1.0.1") (d #t) (k 0)))) (h "1h2i2s7rzwxw4gnhid652sbry292ss0xfy7f0gwk9d8yq8zk5ka9") (f (quote (("nightly"))))))

(define-public crate-static_cell-1.3.0 (c (n "static_cell") (v "1.3.0") (d (list (d (n "portable-atomic") (r "^1.5.1") (f (quote ("critical-section"))) (d #t) (k 0)))) (h "1rx383n505xld5rb31zhaxhf01srfs2cv5gd58vdw764qrczblrc") (f (quote (("nightly"))))))

(define-public crate-static_cell-2.0.0 (c (n "static_cell") (v "2.0.0") (d (list (d (n "portable-atomic") (r "^1.5.1") (d #t) (k 0)))) (h "181w5ln427i3kcjl4x1mbv5iy2hag5gfm67h4pmx705zhg7s8szs") (f (quote (("nightly"))))))

(define-public crate-static_cell-2.1.0 (c (n "static_cell") (v "2.1.0") (d (list (d (n "portable-atomic") (r "^1.5.1") (d #t) (k 0)))) (h "07jcw5k9nlb42r51zzlnd7zw5brl7wsf9c9d50qk922ai220d6yq") (f (quote (("nightly"))))))

