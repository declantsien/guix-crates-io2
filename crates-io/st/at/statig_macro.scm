(define-module (crates-io st at statig_macro) #:use-module (crates-io))

(define-public crate-statig_macro-0.1.0-beta.0 (c (n "statig_macro") (v "0.1.0-beta.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "quote" "extra-traits"))) (d #t) (k 0)))) (h "02qrnv4a72y60xfj7mzwaniz1fvckpcl9n0515vzqw29n34inblk")))

(define-public crate-statig_macro-0.1.0-beta.1 (c (n "statig_macro") (v "0.1.0-beta.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "quote" "extra-traits"))) (d #t) (k 0)))) (h "0lvpnl74basjvwf50wlyvks5mn7k9i93i965gfmz9gp9p9ixl5is")))

(define-public crate-statig_macro-0.1.0 (c (n "statig_macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "quote" "extra-traits"))) (d #t) (k 0)))) (h "1q7nnqdcxp6isl5n8j53lpkxlj7ip5di7z0kqk6ch16b9v1kwzx7") (r "1.65")))

(define-public crate-statig_macro-0.2.0-beta.0 (c (n "statig_macro") (v "0.2.0-beta.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.104") (f (quote ("full" "quote" "extra-traits"))) (d #t) (k 0)))) (h "0x95xklhxdlmbaa387x4v8b04j4ddyy64mv6d2q8s4ynijsaf42i") (r "1.65")))

(define-public crate-statig_macro-0.2.0 (c (n "statig_macro") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.104") (f (quote ("full" "quote" "extra-traits"))) (d #t) (k 0)))) (h "1fic6jaakz6z4ryqmgpg1vy655c49xk7cgh52ia2bi7n0dd4zs1s") (r "1.66")))

(define-public crate-statig_macro-0.3.0 (c (n "statig_macro") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "quote" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1pjzr65s5bcgfyq1gzj8sfjwdj192fx3zkpf5i2nxpv87db62k5z") (r "1.66")))

