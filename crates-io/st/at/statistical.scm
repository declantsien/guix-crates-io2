(define-module (crates-io st at statistical) #:use-module (crates-io))

(define-public crate-statistical-0.0.1 (c (n "statistical") (v "0.0.1") (h "0zcbysjgv6kslj2pnw88q6wmzwmhiq8a1m39p8sbbzj676dm3cav")))

(define-public crate-statistical-0.1.1 (c (n "statistical") (v "0.1.1") (d (list (d (n "num") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "0a62ir6ys8f2gzpxmnbbclig2aa66zxwv8i0hjr56v6r8qpr8ff1")))

(define-public crate-statistical-1.0.0 (c (n "statistical") (v "1.0.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0wm35q6p6jcq1r1darczv4if7qss460kd391nlw5x3hjpc17kma9")))

