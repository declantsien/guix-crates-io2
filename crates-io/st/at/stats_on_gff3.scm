(define-module (crates-io st at stats_on_gff3) #:use-module (crates-io))

(define-public crate-stats_on_gff3-0.1.0 (c (n "stats_on_gff3") (v "0.1.0") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iyg7rg1rhd6dkwy68gp2qrymnbvlsnbgb65ia9lkk1hl2nkw5bl")))

(define-public crate-stats_on_gff3-0.1.1 (c (n "stats_on_gff3") (v "0.1.1") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nyiivjwfmc7z9ndc9f7y93zhdbh8bxnh6b30fkwzrqgaidwizci")))

(define-public crate-stats_on_gff3-0.1.2 (c (n "stats_on_gff3") (v "0.1.2") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qxk79j1siwvzg6d1xagqc5p58j2fgf11na0yq2q7s846i81g5j8")))

(define-public crate-stats_on_gff3-0.1.3 (c (n "stats_on_gff3") (v "0.1.3") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m53ppqv79ffyyrk8cn1cvbqhk80cv97gdvkps94ayq6qcavipgs")))

(define-public crate-stats_on_gff3-0.1.4 (c (n "stats_on_gff3") (v "0.1.4") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y3nsy7b7isb285h97nd07vsbgahcvx80qhh41bmm7c1qa0j5fgi")))

(define-public crate-stats_on_gff3-0.1.5 (c (n "stats_on_gff3") (v "0.1.5") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gfr1w4sayzmj202wcd06amwdm31k8c4b0g740vaa00414zwrz2y")))

(define-public crate-stats_on_gff3-0.1.6 (c (n "stats_on_gff3") (v "0.1.6") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nnlzwzr3k42c4bbq7nqzb2admnm4q5q8ykslvzxb4yjfrkfni75")))

(define-public crate-stats_on_gff3-0.1.7 (c (n "stats_on_gff3") (v "0.1.7") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m4flbbclvxhy8ffjmm25cgxfn7l372hvrb9yr3f0l7zpyj1i7rv")))

(define-public crate-stats_on_gff3-0.1.8 (c (n "stats_on_gff3") (v "0.1.8") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "137snn481qrgbycfzxgc5x7l6cb17f7wq4fmhn5i7vb47v52pihx")))

(define-public crate-stats_on_gff3-0.1.9 (c (n "stats_on_gff3") (v "0.1.9") (d (list (d (n "bio") (r "^1.0.0") (d #t) (k 0)) (d (n "bio-types") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "10rgl4q3a8ak8f33vnkh4v5a2d6i5fyg1ipjarg8s23wvgzm80vc")))

(define-public crate-stats_on_gff3-0.1.10 (c (n "stats_on_gff3") (v "0.1.10") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ih40inmgbcfmfp2crn39rkbrv3inhjhkam6g5h9nv1jcfcbv5c2")))

(define-public crate-stats_on_gff3-0.1.11 (c (n "stats_on_gff3") (v "0.1.11") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "13a4npqg5qrmcq10s7inahy8dgsmx6570a4db46l8mwnn2i9ir93")))

(define-public crate-stats_on_gff3-0.1.12 (c (n "stats_on_gff3") (v "0.1.12") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "17kcmxmh5aag7i1ph42dh24b083da89xyil92g9h6sp0plbfl3az")))

(define-public crate-stats_on_gff3-0.1.13 (c (n "stats_on_gff3") (v "0.1.13") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zva1mxwh4sf7hz72lsq73sdsvpxgmnh4jvn86wfzp7ivvqx00i4")))

(define-public crate-stats_on_gff3-0.1.15 (c (n "stats_on_gff3") (v "0.1.15") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p9z8cii80wn1fd6v41jfw20f9c4mcp17xrzkcazgs6jb5yw7whn")))

(define-public crate-stats_on_gff3-0.1.16 (c (n "stats_on_gff3") (v "0.1.16") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bpaax7wvlc3f34na9vh1zp9q1q14r8a2iifqwfd344rcqnhgm50")))

(define-public crate-stats_on_gff3-0.1.17 (c (n "stats_on_gff3") (v "0.1.17") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m387d0icdn1f6669cibdxnvxpd5lxlnfb3n7yv8a0vq7wf8cprb")))

(define-public crate-stats_on_gff3-0.1.18 (c (n "stats_on_gff3") (v "0.1.18") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g0mmjq1185is33y1s85j2lrnhx2a7pb2axhf7jhrllpx9ppp739")))

(define-public crate-stats_on_gff3-0.1.19 (c (n "stats_on_gff3") (v "0.1.19") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "07n8fq6xxmn04pfikfyzdk62lv3ghvws2l6y0s4kf1xzi0zg2221")))

(define-public crate-stats_on_gff3-0.1.20 (c (n "stats_on_gff3") (v "0.1.20") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1in8hnxv04vghln2j98szr2kyivckjbb67af0jwsbypvjg9nmkby")))

(define-public crate-stats_on_gff3-0.1.21 (c (n "stats_on_gff3") (v "0.1.21") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "012ankw3c8kjv98ynh2bb88j8gikqk7ndri1cjp13b4abg6y2yvp")))

(define-public crate-stats_on_gff3-0.1.22 (c (n "stats_on_gff3") (v "0.1.22") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kh6dhx7y5cv4p59y9ah16ncwmr5bqalvimxzrcp1p5129brnzkz")))

(define-public crate-stats_on_gff3-0.1.23 (c (n "stats_on_gff3") (v "0.1.23") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h06cx4ajggn8ri0pmj30zciimfig7hvv4hmfz0nwm0crv6mvpsn")))

(define-public crate-stats_on_gff3-0.1.24 (c (n "stats_on_gff3") (v "0.1.24") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kqbl0najxqapj1rrqcnl67cnry2jzsd7klzd6qapbyj8cv802zq")))

(define-public crate-stats_on_gff3-0.1.25 (c (n "stats_on_gff3") (v "0.1.25") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1762zkc3kxck2md2q09lwn3na99fn29ycvsxf4qjviamhacd8ixh")))

(define-public crate-stats_on_gff3-0.1.26 (c (n "stats_on_gff3") (v "0.1.26") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "bio-types") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cf64c1v9icxfwziflqmypinlbji0xqcid9gg7p6jbyjg3n55dcp")))

