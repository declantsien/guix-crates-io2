(define-module (crates-io st at stateful_faas_sim) #:use-module (crates-io))

(define-public crate-stateful_faas_sim-0.1.0 (c (n "stateful_faas_sim") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "histogram-sampler") (r "^0.5.0") (d #t) (k 0)) (d (n "incr_stats") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ra3vfzsyy5lc1l1sjykb517jy9r9gagp8s5lb85i6dss63cv6f6")))

