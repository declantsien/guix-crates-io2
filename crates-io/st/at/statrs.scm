(define-module (crates-io st at statrs) #:use-module (crates-io))

(define-public crate-statrs-0.1.0 (c (n "statrs") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kjp0201xdc6chbh5j5cbw6fk358921bbmp4h3w609m0ildzl7bc")))

(define-public crate-statrs-0.2.0 (c (n "statrs") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0js34f17xy990q760lq9casxicwxqrj20mac0dbf9i4mzyrk61x7")))

(define-public crate-statrs-0.3.0 (c (n "statrs") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dkda5146z49pm5iaxa8ismpdwya7qrg9rpil55k1h1q2n7jmdjh")))

(define-public crate-statrs-0.3.1 (c (n "statrs") (v "0.3.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1g75d6cmmgfdl6gap9jd04r97yv33l4zkg446hkqagk10f8h6yz4")))

(define-public crate-statrs-0.3.2 (c (n "statrs") (v "0.3.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jnnmj15r73vr10jp5mik3g23n8dvsviln7xjg4876p7fqbvxrsf")))

(define-public crate-statrs-0.4.0 (c (n "statrs") (v "0.4.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ncqjnsvnvqwryi47gb4m0g0xzizr08h19vdjv8nhsadp4lk3x36")))

(define-public crate-statrs-0.5.0 (c (n "statrs") (v "0.5.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0wwixg0s49m8yv47x98ia7hrzfid50pq170rkakmx9xva77smwgv")))

(define-public crate-statrs-0.5.1 (c (n "statrs") (v "0.5.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vfxhcplam521zl14vqfp8f33wjwf8mx9zv03bpkpz9ddqwkkkxx")))

(define-public crate-statrs-0.6.0 (c (n "statrs") (v "0.6.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "025y50r3nzr5fxrw03chccb77gbgpwh8a5awj5xdar4q1fs08abf")))

(define-public crate-statrs-0.7.0 (c (n "statrs") (v "0.7.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0625k2zxh9ss7vjn4kdwgfx1wi1rcvpc9q256d4fwq9id6bkpn2x")))

(define-public crate-statrs-0.8.0 (c (n "statrs") (v "0.8.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1p1xda25svskxrgix4raj9iks74qr525vnqaf04brwqi4ggh9lhq")))

(define-public crate-statrs-0.9.0 (c (n "statrs") (v "0.9.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ixym4c9crnlwa0b005xc1s8ndpi6aazvxybg02ilzc6wdh8d33x")))

(define-public crate-statrs-0.10.0 (c (n "statrs") (v "0.10.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1s454spijvbyls3bd6l72lpdliw6pa0nzhmg7wmxndaysp42l40h")))

(define-public crate-statrs-0.11.0 (c (n "statrs") (v "0.11.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0dj6crkwrc5z14amq9sz1f1b1qzml5sb5z2c47cj8dcb6rlicnl3")))

(define-public crate-statrs-0.12.0 (c (n "statrs") (v "0.12.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "10hk9194ig21w08947yavf4l97g0106ph4xxlzn8ps2kwrnnzqfc")))

(define-public crate-statrs-0.13.0 (c (n "statrs") (v "0.13.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0r00b60zlsn6srb6m6bzbw3w5cyihcy4w2rfjav64x4viy5bad0y")))

(define-public crate-statrs-0.14.0 (c (n "statrs") (v "0.14.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bdff4rsghp9hj5i5ynl6iw3pyzprd65cbf8ihmgvyv190a1y30y") (f (quote (("nightly"))))))

(define-public crate-statrs-0.15.0 (c (n "statrs") (v "0.15.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.27.1") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "01bggaq9n09ch20r4yq9s2c4y54367nd71asg22nl8bq9s7bpg85") (f (quote (("nightly"))))))

(define-public crate-statrs-0.16.0 (c (n "statrs") (v "0.16.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vk9wlimhy6wx9a1hhy2n3knpd2gj8b8pnl1q8y734l1fkhya21d") (f (quote (("nightly"))))))

