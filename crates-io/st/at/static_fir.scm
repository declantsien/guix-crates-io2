(define-module (crates-io st at static_fir) #:use-module (crates-io))

(define-public crate-static_fir-0.1.0 (c (n "static_fir") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.34") (d #t) (k 0)))) (h "1s90sqv214qsgchivvsd0g0rbby3xcqigg0q359hks0qmh8k77a7")))

(define-public crate-static_fir-0.2.0 (c (n "static_fir") (v "0.2.0") (h "1j0gdfp8vwrvpkn6ma9zdh4p6rrp24y4shlzcl0pnc1xqhfp9f35")))

