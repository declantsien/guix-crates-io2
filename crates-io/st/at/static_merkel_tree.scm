(define-module (crates-io st at static_merkel_tree) #:use-module (crates-io))

(define-public crate-static_merkel_tree-0.1.0 (c (n "static_merkel_tree") (v "0.1.0") (h "18iiclzkmy55qbg3q2fkjq78mgc3lg8dkawwryi57hdvj4za36wz")))

(define-public crate-static_merkel_tree-0.1.1 (c (n "static_merkel_tree") (v "0.1.1") (h "1zisv7q1haszki5i6ndi0xmjlaxhl9s7j1mnvridf2v4pjcfklp5")))

(define-public crate-static_merkel_tree-1.0.0 (c (n "static_merkel_tree") (v "1.0.0") (h "0cy0364psldb539jkwccn9mk3bcywqplbb2ry4fynvpzkjf5navc")))

