(define-module (crates-io st at stats_on_genomes) #:use-module (crates-io))

(define-public crate-stats_on_genomes-0.1.0 (c (n "stats_on_genomes") (v "0.1.0") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0knpmhjadgsfwyr0ai3mj1rz8a6z85iqymly54maxgikcppwn0kn")))

(define-public crate-stats_on_genomes-0.1.1 (c (n "stats_on_genomes") (v "0.1.1") (d (list (d (n "bio") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "14jndy5bg4v0r7dzvjk6bjhy268xr0s6ds3aw9njh25irnd6ppdn")))

