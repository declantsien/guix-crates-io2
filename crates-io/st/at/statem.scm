(define-module (crates-io st at statem) #:use-module (crates-io))

(define-public crate-statem-0.1.0 (c (n "statem") (v "0.1.0") (h "06a5sivz208ixyyq2wffw2k312z04m98j3xsyrfnyrnpp1gqyfzv") (y #t)))

(define-public crate-statem-0.0.0-reserved (c (n "statem") (v "0.0.0-reserved") (h "1q4km97dqff2wws7n9y55hvmldm0pdjvvd3hak02299chvjlx6k2")))

