(define-module (crates-io st at statc) #:use-module (crates-io))

(define-public crate-statc-0.0.1 (c (n "statc") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1f8gs9k3d7gb89sqy8asc450lw9vrx5kpx3gbk3n2npw7nfnkywk")))

(define-public crate-statc-0.0.2 (c (n "statc") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1lyh8hmn3ll32cinq5mky044gl92pffr8yzcpdbwx8bn0ryn477z")))

(define-public crate-statc-0.0.3 (c (n "statc") (v "0.0.3") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1b94dr47imm30pwmh95lzvc0m5cm0m5lbjjgg6c9q12613v2p5s0")))

(define-public crate-statc-0.0.4 (c (n "statc") (v "0.0.4") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "07af2y6qk6dhhxh5v0shbvbkdm1amjdplzv68xl1i73j8a49f1my")))

(define-public crate-statc-0.0.5 (c (n "statc") (v "0.0.5") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1myirrb99khhn0f65mv7mvl2m45jhc0fflvz8f8577ch2wnbbniy")))

(define-public crate-statc-0.0.6 (c (n "statc") (v "0.0.6") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1r5kaj9hbq37y9gsrlfba24pgsg7hd265jrck3iz67aw5mcgrf2v")))

(define-public crate-statc-0.0.7 (c (n "statc") (v "0.0.7") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "17z2c4ggdlk3axkz58yvslppxmxwnizwn3dxggd0vwdkcc6x9xcm")))

(define-public crate-statc-0.0.8 (c (n "statc") (v "0.0.8") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "meval") (r "^0.2.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0bmsf7igf7cbyr94ss8z29jlj05dk38dz8klxar6kswllaa3cn18")))

