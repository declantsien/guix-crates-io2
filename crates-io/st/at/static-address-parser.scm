(define-module (crates-io st at static-address-parser) #:use-module (crates-io))

(define-public crate-static-address-parser-0.1.0 (c (n "static-address-parser") (v "0.1.0") (d (list (d (n "mv-core-types") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1f941sjy8k3ifrpy6j1dpfixprnfc0q35w7vygkzcajnag0q9pdl") (f (quote (("default") ("address32" "mv-core-types/address32") ("address20" "mv-core-types/address20"))))))

(define-public crate-static-address-parser-0.1.1 (c (n "static-address-parser") (v "0.1.1") (d (list (d (n "mv-core-types") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vcwhmyqsi493k94vz1lbafc3hpx64ly138jnf2s12ds1fryp9gr") (f (quote (("default") ("address32" "mv-core-types/address32") ("address20" "mv-core-types/address20"))))))

(define-public crate-static-address-parser-0.1.2 (c (n "static-address-parser") (v "0.1.2") (d (list (d (n "mv-core-types") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rysx28qvg4pd9h5ipn7dj3vdqxhsmhiq5jgcc2hbjb780z3ricm") (f (quote (("default") ("address32" "mv-core-types/address32") ("address20" "mv-core-types/address20"))))))

(define-public crate-static-address-parser-0.2.0 (c (n "static-address-parser") (v "0.2.0") (d (list (d (n "account-address") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mfd0z3q7dm96z2lwxgvi9hxifmyzsspaymajza4d94zp2rj1gz7") (f (quote (("default") ("address32" "account-address/address32") ("address20" "account-address/address20"))))))

(define-public crate-static-address-parser-0.2.2 (c (n "static-address-parser") (v "0.2.2") (d (list (d (n "account-address") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0znrk1wdrihapzsc6zy1mvvj7dl3rwm2iqx6fdwd5y6yy3wrdvzn") (f (quote (("default") ("address32" "account-address/address32") ("address20" "account-address/address20"))))))

