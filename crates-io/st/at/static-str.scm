(define-module (crates-io st at static-str) #:use-module (crates-io))

(define-public crate-static-str-0.1.0 (c (n "static-str") (v "0.1.0") (h "0kqdcvlq7d5m3brn8sj7ccs5n8dsdnxsz2jha2dddwai1njd5v9s")))

(define-public crate-static-str-0.2.0 (c (n "static-str") (v "0.2.0") (h "02vdx6f4fkkmyv7lg51lrdx2yx21gjkj6dy0mr43mg434h2b8aq5")))

