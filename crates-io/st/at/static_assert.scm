(define-module (crates-io st at static_assert) #:use-module (crates-io))

(define-public crate-static_assert-0.1.0 (c (n "static_assert") (v "0.1.0") (h "0zprczm6fndcr7gc0kxpg3apxs0l992mh4dlf0mlksj0w0pibgq6") (y #t)))

(define-public crate-static_assert-0.1.1 (c (n "static_assert") (v "0.1.1") (h "0k1x92pzkb3kkrvy335gy9ziyz55gn1pbl7v6i040y3h1ag9id2x") (y #t)))

(define-public crate-static_assert-0.1.2 (c (n "static_assert") (v "0.1.2") (h "1a4ad62yiqd4qzca58q02x4nnxvjpjjbi11lal9i3s3xdwjs336k") (y #t)))

(define-public crate-static_assert-0.1.3 (c (n "static_assert") (v "0.1.3") (h "1jn68yxn5bcayh4kr0kx0cw9ppfhy3skxlx6xvacfdqpz8gz9ld3") (y #t)))

(define-public crate-static_assert-0.2.0 (c (n "static_assert") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1ppk11hgkidjnljg7zjnplh4vih4hc0bbsvhhiwaj9dhx68cb9r8") (y #t)))

(define-public crate-static_assert-0.3.0 (c (n "static_assert") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.1.1") (d #t) (k 2)))) (h "00fl84f2ky2kw9mx8q25svkrncyc7hk69llld51hfrmvcp3bhka5") (y #t)))

(define-public crate-static_assert-0.3.1 (c (n "static_assert") (v "0.3.1") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)))) (h "0hvayb3qgb60jd5rhgm8rcacrlphsrpzzl86p3wy4rw8pqy0lmgg") (y #t)))

