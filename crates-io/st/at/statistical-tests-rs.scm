(define-module (crates-io st at statistical-tests-rs) #:use-module (crates-io))

(define-public crate-statistical-tests-rs-0.1.0 (c (n "statistical-tests-rs") (v "0.1.0") (h "115zngxjgn383wl7gdsl6mynv5380q82nlh3ar5rsn97nvz74dq9")))

(define-public crate-statistical-tests-rs-0.1.1 (c (n "statistical-tests-rs") (v "0.1.1") (h "1zzlqqmlmjj1h8mfs0s38q1va99r38bpngg15f14p6j6qwmgz42c")))

(define-public crate-statistical-tests-rs-0.1.2 (c (n "statistical-tests-rs") (v "0.1.2") (h "1hhcg16k4nd0m56dilndcalxw39f806afhdkphnhsfqpn19w5cz2")))

