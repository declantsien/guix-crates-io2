(define-module (crates-io st at static-router-macros) #:use-module (crates-io))

(define-public crate-static-router-macros-0.1.0 (c (n "static-router-macros") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "06d195qg1c1sgwf91xnaj3km58j9m1lb8c8chfgzmzx578j3483g") (y #t)))

(define-public crate-static-router-macros-0.1.1 (c (n "static-router-macros") (v "0.1.1") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0i6ydw1frcyqhc3zz3v8lzirac5k63rqzfidlcr9idl49gp4blsm") (y #t)))

(define-public crate-static-router-macros-0.1.2 (c (n "static-router-macros") (v "0.1.2") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ikcnvl7v1h5cv56drzi0awskyqmqb0wz1nvgl69slk66ajmldm0") (y #t)))

(define-public crate-static-router-macros-0.1.3 (c (n "static-router-macros") (v "0.1.3") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "055qjikyh98fn1b2ynb59729534c7ck4chdgca51nngi42ag92dl") (y #t)))

(define-public crate-static-router-macros-0.1.4 (c (n "static-router-macros") (v "0.1.4") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "137y8wzhr6asaqshxnm52r3nbid3b50j6zig8rvjda5cg3mzfbck")))

(define-public crate-static-router-macros-0.1.5 (c (n "static-router-macros") (v "0.1.5") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "04zn24r117z4lp295qkm8dw3a7mjzkmgfpq0xq4f3xnvmr6sqkci")))

