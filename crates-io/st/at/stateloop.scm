(define-module (crates-io st at stateloop) #:use-module (crates-io))

(define-public crate-stateloop-0.1.0 (c (n "stateloop") (v "0.1.0") (d (list (d (n "glium") (r "^0.16.0") (d #t) (k 0)))) (h "1jmgmpv1nwlr69l7753dbs6gv4wzi20js2aw6m4nk0rn5id0yqw8")))

(define-public crate-stateloop-0.2.0 (c (n "stateloop") (v "0.2.0") (d (list (d (n "glium") (r "^0.16.0") (d #t) (k 0)))) (h "0dm8862i9q3m6fd51xl967pn418m75zip777ckxi5ks2zpy5xgbi")))

(define-public crate-stateloop-0.6.0 (c (n "stateloop") (v "0.6.0") (d (list (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "09vp1hdw5m4lfmnv7nx8j1hlla5rwr4hw1gl2k02ngna6vviq8na")))

(define-public crate-stateloop-0.7.0 (c (n "stateloop") (v "0.7.0") (d (list (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "0gm9vim1vrq6wsgqy3rvfkb5njlgfn84w9wnjkbqmrqg0l6f63rx")))

