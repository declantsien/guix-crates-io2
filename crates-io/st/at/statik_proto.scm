(define-module (crates-io st at statik_proto) #:use-module (crates-io))

(define-public crate-statik_proto-0.2.0 (c (n "statik_proto") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "statik_common") (r "^0.2.0") (d #t) (k 0) (p "statik_common")) (d (n "statik_derive") (r "^0.2.0") (d #t) (k 0) (p "statik_derive")) (d (n "uuid") (r "^1.3.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1yx60zhv9bx82s8f9x1c3pzj0jdjzhxlq0r91iipj17khgbmh94n")))

(define-public crate-statik_proto-0.2.1 (c (n "statik_proto") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "statik_common") (r "^0.2.0") (d #t) (k 0) (p "statik_common")) (d (n "statik_derive") (r "^0.2.0") (d #t) (k 0) (p "statik_derive")) (d (n "uuid") (r "^1.3.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1qal859ygss7dxkzwnnwfhywcpga52mzd1f9dcq44kf4s3xzpv39")))

