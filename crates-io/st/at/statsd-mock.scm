(define-module (crates-io st at statsd-mock) #:use-module (crates-io))

(define-public crate-statsd-mock-0.1.0 (c (n "statsd-mock") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "statsd") (r "^0.15") (d #t) (k 2)))) (h "06hdac3yfn7a5ll8bhw4b6rang2i9g86lxwz4mda83483bv9m49c")))

(define-public crate-statsd-mock-0.1.1 (c (n "statsd-mock") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "statsd") (r "^0.15") (d #t) (k 2)))) (h "0pwzqmrs376fkm8jzp297azyp43z1m7rk151g1752d46jy7w6dff")))

