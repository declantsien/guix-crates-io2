(define-module (crates-io st at static_merkle_tree) #:use-module (crates-io))

(define-public crate-static_merkle_tree-1.0.0 (c (n "static_merkle_tree") (v "1.0.0") (h "01phglsn1amhdvfy3cfdbw85s6wyxhqci2m5s5wz6cq85l90f8dd")))

(define-public crate-static_merkle_tree-0.1.1 (c (n "static_merkle_tree") (v "0.1.1") (h "17sz2n94q6pdgs8i2sbprxar8wkgm5hkrxid9bfj7zvfrqld3ipq")))

(define-public crate-static_merkle_tree-1.1.0 (c (n "static_merkle_tree") (v "1.1.0") (h "0hd0kjznc1kmf252gbxg18ndgrf1qwb1xp56kv7nyw9c499byfkp")))

