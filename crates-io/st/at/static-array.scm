(define-module (crates-io st at static-array) #:use-module (crates-io))

(define-public crate-static-array-0.1.0 (c (n "static-array") (v "0.1.0") (h "1i9hq9v1xn66xdk96pp314lzdxa26f0zgvzxdj6wqvpysbnlvzdl")))

(define-public crate-static-array-0.2.0 (c (n "static-array") (v "0.2.0") (h "1bshk1aj19iy97m672my0lvallhwv532yvw8cydqrjjrl8l4afkc")))

(define-public crate-static-array-0.3.0 (c (n "static-array") (v "0.3.0") (h "15r84wdj620xqj8y95icliqk3h0li3y87rnmyicrrlqyq1drp89w")))

(define-public crate-static-array-0.4.0 (c (n "static-array") (v "0.4.0") (d (list (d (n "rayon") (r "^1.8.1") (o #t) (d #t) (k 0)))) (h "1992ncdp6zj4rrkhmg70fzqcnd0zz92db90dggmrgyvjipw18s1n") (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-static-array-0.5.0 (c (n "static-array") (v "0.5.0") (d (list (d (n "rayon") (r "^1.8.1") (o #t) (d #t) (k 0)))) (h "1wilf27h74rxza0p00d8z6zhi1jkj10jvq055g0b540drbhp94vw") (s 2) (e (quote (("rayon" "dep:rayon"))))))

