(define-module (crates-io st at stats-rs) #:use-module (crates-io))

(define-public crate-stats-rs-0.1.0 (c (n "stats-rs") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.25") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.48") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Window" "Document" "Element" "Node" "HtmlElement" "HtmlCanvasElement" "CanvasRenderingContext2d" "Performance" "CssStyleDeclaration" "HtmlDivElement"))) (d #t) (k 0)))) (h "0kjmcsaw5rialq3dninx64c9grb2ly8wf8snkwqwpsxnlzdq7z0q") (y #t)))

