(define-module (crates-io st at statusio) #:use-module (crates-io))

(define-public crate-statusio-1.0.0 (c (n "statusio") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wm6s4q00p53xlzi0mb156sa829di62yfmvb12hknvf2grsbqkdx")))

