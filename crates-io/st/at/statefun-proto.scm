(define-module (crates-io st at statefun-proto) #:use-module (crates-io))

(define-public crate-statefun-proto-0.1.0 (c (n "statefun-proto") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "protobuf") (r "^2.15") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.15") (d #t) (k 1)))) (h "12n20snf3g6rx2jw8y9cfc7jxqgjmqidq5r29wd4v40gxkvw5m9s") (y #t)))

(define-public crate-statefun-proto-0.1.1 (c (n "statefun-proto") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "protobuf") (r "^2.15") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.15") (d #t) (k 1)))) (h "1nh025gmfc7fr24z28znni1vfpi06akhba7w1a6b9qa1z0vh5c5x")))

(define-public crate-statefun-proto-0.2.0-alpha.1 (c (n "statefun-proto") (v "0.2.0-alpha.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "protobuf") (r "^2.15") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.15") (d #t) (k 1)))) (h "0kal9vdncd11d1x8ka59qdnaif1iv81hvc3f3r3cj2is9j0g4h31") (y #t)))

(define-public crate-statefun-proto-0.2.0-alpha.2 (c (n "statefun-proto") (v "0.2.0-alpha.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "protobuf") (r "^2.15") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.15") (d #t) (k 1)))) (h "0g1hrcywjl3ych4zpvdxv1bdp4z4fy4c5ilmax2g518a169qi4xn")))

