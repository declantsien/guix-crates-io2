(define-module (crates-io st at state-space) #:use-module (crates-io))

(define-public crate-state-space-0.1.0 (c (n "state-space") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "array-matrix") (r "^0.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1amgfx61jg9qpqbz151pkqi3hln1w9a7f2sl2pmdz998rp5hkjd0")))

(define-public crate-state-space-0.1.1 (c (n "state-space") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "array-matrix") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "transfer_function") (r "^0.1.2") (d #t) (k 0)))) (h "1f24i1vv3mhawl1nchw1y0y2zl5wq76q2f6gjqwvrk1jpyffx2c7")))

