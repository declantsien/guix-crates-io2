(define-module (crates-io st at static_init_macro) #:use-module (crates-io))

(define-public crate-static_init_macro-0.1.1 (c (n "static_init_macro") (v "0.1.1") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m8wpp0cw7hs9261xn0hmn8a27435cw192sfx4h38p3m92zwwhwp")))

(define-public crate-static_init_macro-0.1.2 (c (n "static_init_macro") (v "0.1.2") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nqpkd5agjy1dw7w9p1qrg7rdk4bqzdf4y1vsw5i40f393n5941d")))

(define-public crate-static_init_macro-0.2.0 (c (n "static_init_macro") (v "0.2.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "007b1k3bxf712k01p9n53nrd8ac6gf4lw86ds9p312hg9kclj7i4")))

(define-public crate-static_init_macro-0.3.0 (c (n "static_init_macro") (v "0.3.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rw1va1z5y41ym02yqlh07bb1plk36p2r3ggzlkhwk03khy28snz")))

(define-public crate-static_init_macro-0.4.0 (c (n "static_init_macro") (v "0.4.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "048rxnxpn3j0arx426prp4nwcq8q15fjkck53giqn6iggilicjj4")))

(define-public crate-static_init_macro-0.4.2 (c (n "static_init_macro") (v "0.4.2") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09ja9almbhjmb8gj6ds3ji1pxsr2wi9j38157kbhwpxpsdbj1dij") (f (quote (("lazy_drop") ("lazy"))))))

(define-public crate-static_init_macro-0.5.0 (c (n "static_init_macro") (v "0.5.0") (d (list (d (n "cfg_aliases") (r "^0.1") (d #t) (k 1)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gvbai0vj351bnki8p2iw3nzl517v24hxf6iqhzxq7ja0f8iq9pj") (f (quote (("thread_local_drop") ("lazy") ("debug_order") ("atexit"))))))

(define-public crate-static_init_macro-1.0.0 (c (n "static_init_macro") (v "1.0.0") (d (list (d (n "cfg_aliases") (r "^0.1") (d #t) (k 1)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hngr7pjvmkyl57jl19yx9i5qszlfnzv2mg9b4h0s6660xqrs1xw") (f (quote (("debug_order"))))))

(define-public crate-static_init_macro-1.0.1 (c (n "static_init_macro") (v "1.0.1") (d (list (d (n "cfg_aliases") (r "^0.1") (d #t) (k 1)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cp722n7z6hk91jpzn1qzik8ig2wkykr88dv90sshbpl33hvcgsv") (f (quote (("debug_order"))))))

(define-public crate-static_init_macro-1.0.2 (c (n "static_init_macro") (v "1.0.2") (d (list (d (n "cfg_aliases") (r "^0.1") (d #t) (k 1)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1krs03rjqjwih1vwf73nfcr8dpc24ar2bm2xwk8g4y5aqdgmk8kh") (f (quote (("debug_order"))))))

