(define-module (crates-io st at static-address-example-aptos) #:use-module (crates-io))

(define-public crate-static-address-example-aptos-0.1.1 (c (n "static-address-example-aptos") (v "0.1.1") (d (list (d (n "mv-core-types") (r "^0.1.0") (f (quote ("address32"))) (d #t) (k 0)) (d (n "static-address") (r "^0.1.0") (f (quote ("address32"))) (d #t) (k 0)))) (h "1aybn8hdh2f1040r892jv5yjgbmqj5j5w9qkk3m1rfcaiv9mm0jf")))

(define-public crate-static-address-example-aptos-0.1.2 (c (n "static-address-example-aptos") (v "0.1.2") (d (list (d (n "mv-core-types") (r "^0.1.0") (f (quote ("address32"))) (d #t) (k 0)) (d (n "static-address") (r "^0.1.0") (f (quote ("address32"))) (d #t) (k 0)))) (h "0h0zkjcb568lq7awfwhb1xgq1mrkdhvdqi0ws4ghw83aa40z6r55")))

(define-public crate-static-address-example-aptos-0.2.1 (c (n "static-address-example-aptos") (v "0.2.1") (d (list (d (n "account-address") (r "^0.2.0") (f (quote ("address32"))) (d #t) (k 0)) (d (n "static-address") (r "^0.2.0") (f (quote ("address32"))) (d #t) (k 0)))) (h "0l887qvs44yp1rafman6gqav68j90dzirc2vlh4cw98ankv5ml4h")))

(define-public crate-static-address-example-aptos-0.2.2 (c (n "static-address-example-aptos") (v "0.2.2") (d (list (d (n "static-address") (r "^0.2.0") (f (quote ("address32"))) (d #t) (k 0)))) (h "1zvwhqi8labynqs0w7rqh551zpz6ijkwdj9zpkg7wv5bpjw4bnhl")))

