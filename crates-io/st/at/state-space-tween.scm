(define-module (crates-io st at state-space-tween) #:use-module (crates-io))

(define-public crate-state-space-tween-0.1.0 (c (n "state-space-tween") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "flo_curves") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jgqlqwcibr1xh7xikr9s7qc8w85bnsjiam68hymwln3imsmv2xs")))

