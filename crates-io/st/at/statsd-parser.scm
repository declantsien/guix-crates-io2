(define-module (crates-io st at statsd-parser) #:use-module (crates-io))

(define-public crate-statsd-parser-0.1.0 (c (n "statsd-parser") (v "0.1.0") (h "0wa8ahnrqml93ncw00770sjxmywn690rh5b412a3hr7rrsz7f5z2")))

(define-public crate-statsd-parser-0.1.1 (c (n "statsd-parser") (v "0.1.1") (h "0z59pmg495x3rycv9w72bgyma2p1hccdz71rqrxc7fxcp2sxv0x6")))

(define-public crate-statsd-parser-0.2.0 (c (n "statsd-parser") (v "0.2.0") (h "08v011dqnahws28ad8b5mclg69x5byhhzr0ar76gipqsbfml1p4d")))

(define-public crate-statsd-parser-0.3.0 (c (n "statsd-parser") (v "0.3.0") (h "13zggd5kw1kpaanm2szdp7yk8lq3rglhwkbxxsjy5ga0g0w8b3xd")))

