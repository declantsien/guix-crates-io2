(define-module (crates-io st at stator) #:use-module (crates-io))

(define-public crate-stator-0.1.0 (c (n "stator") (v "0.1.0") (h "01mw1zdpjg6n66h4sn584dlnazqbxzqic7vywfi854bbdy9iv021")))

(define-public crate-stator-0.1.1 (c (n "stator") (v "0.1.1") (h "11zkij50x0gyc7c0sd8jc8rzi75vkgidpz4a694rama1q4a8faca")))

