(define-module (crates-io st at statusbar) #:use-module (crates-io))

(define-public crate-statusbar-0.1.1 (c (n "statusbar") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)))) (h "0bksnsx179ma0ljhc8ndgdwr5cwf6cqmrrwmgf814g8p9icscp21")))

(define-public crate-statusbar-0.2.0 (c (n "statusbar") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)))) (h "1x1z6m7j66p2aagnz24c0w1k3m3lp1bhwgmp40xjnprzl78cjn2j")))

(define-public crate-statusbar-0.3.0 (c (n "statusbar") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)))) (h "0pmqmb67y53y8z3qbmaf8pxrdz06kxmrbg7y4h4xwjzwsaa40bm9")))

(define-public crate-statusbar-0.3.1 (c (n "statusbar") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)))) (h "0q1zvqzpkf475bv01ys7drpgvbr96m90jq7jlfscv565p5smksdq")))

(define-public crate-statusbar-0.3.2 (c (n "statusbar") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.0") (d #t) (k 0)))) (h "16n8cw8wxsa6an1vyl9n9yzg4948xzh9pgpajwymfrvpavj1qg6w")))

(define-public crate-statusbar-0.3.3 (c (n "statusbar") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.0") (d #t) (k 0)))) (h "1j0qp5ryrygn01svfqxsrnl4cb980k4n0qyylicllp0nav7pfk9r")))

(define-public crate-statusbar-0.3.4 (c (n "statusbar") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.0") (d #t) (k 0)))) (h "1ynpmjq9l28wkiksrs8d04a6qqkb8iblnb44j925xfv4jwj64v0c")))

(define-public crate-statusbar-0.3.5 (c (n "statusbar") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.0") (d #t) (k 0)))) (h "1id6n2n1wak8dc6fzq4ql44lr9vhy5isgx0zwljw282mhbwca0ab")))

