(define-module (crates-io st at stateroom-stdio) #:use-module (crates-io))

(define-public crate-stateroom-stdio-0.2.0 (c (n "stateroom-stdio") (v "0.2.0") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1jzzfhqc3a2y6x14w1hn4f45lmd4p23czl7nkp4nd1ad24wka01m")))

(define-public crate-stateroom-stdio-0.2.2 (c (n "stateroom-stdio") (v "0.2.2") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1nvw01kka2cmnywir0215ha5ax6blcbxk10f42y5d9z7wjr7qz0p")))

(define-public crate-stateroom-stdio-0.2.3 (c (n "stateroom-stdio") (v "0.2.3") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "07rk0qg3flxmxhlvshv21f8r3709sxa7fmgp496v42mjk5wzf6mv")))

(define-public crate-stateroom-stdio-0.2.4 (c (n "stateroom-stdio") (v "0.2.4") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0k7fjlsnkzd0hqni787vgzzy8c1l6h0xb3i1alps5r20njis8hs6")))

(define-public crate-stateroom-stdio-0.2.5 (c (n "stateroom-stdio") (v "0.2.5") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "00643ncigx4j891a1hck2irqxmsrn94w2jr977dwf6892axpwwx6")))

(define-public crate-stateroom-stdio-0.2.6 (c (n "stateroom-stdio") (v "0.2.6") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "0nmcdkq01s0h25lvx89vjkcpm490ibvyjs5146xc3qms0nl5q3k0")))

(define-public crate-stateroom-stdio-0.2.7 (c (n "stateroom-stdio") (v "0.2.7") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "0yh0cnqn810ip0zwpfzx04abzgbpvh85idhkb868bqflkjrci0a4")))

(define-public crate-stateroom-stdio-0.2.8 (c (n "stateroom-stdio") (v "0.2.8") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.8") (f (quote ("serde"))) (d #t) (k 0)))) (h "01c20hf60fyhkxn4bsqkrppca20cl4xr2b556g3656krs70jxw89")))

(define-public crate-stateroom-stdio-0.2.9 (c (n "stateroom-stdio") (v "0.2.9") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "stateroom") (r "^0.2.8") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kl8z4kdrzd01ggawjb1lan2qaskwyrhnwyb9nkmhxcgqi2az4ci")))

