(define-module (crates-io st io stio) #:use-module (crates-io))

(define-public crate-stio-0.0.1 (c (n "stio") (v "0.0.1") (d (list (d (n "http-body-util") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^1.2") (f (quote ("http1" "server"))) (d #t) (k 2)) (d (n "mio") (r "^0.8.11") (f (quote ("net" "os-poll"))) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1c0il6mw77v328finhqdlxs0pwsfwgb9aicg8cllw9hnsnnp37kj")))

