(define-module (crates-io st rl strloin) #:use-module (crates-io))

(define-public crate-strloin-0.1.0 (c (n "strloin") (v "0.1.0") (h "10hhij46ijklb5242czn4qwv3ig4qxf2y1r7qdg1llvnvc94vwr2") (r "1.56")))

(define-public crate-strloin-0.1.1 (c (n "strloin") (v "0.1.1") (h "0nfy5qy1qmj2bjmkn8w3k0ini4xzif6p9wkvali5nnanbqfsj4m9") (r "1.56")))

(define-public crate-strloin-0.1.2 (c (n "strloin") (v "0.1.2") (d (list (d (n "beef") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1072k9qpqx9pvkjf940maykphzskhs18nsw3xmcdb96qb71zv9iz") (r "1.56")))

(define-public crate-strloin-0.1.3 (c (n "strloin") (v "0.1.3") (d (list (d (n "beef") (r "^0.5") (o #t) (d #t) (k 0)))) (h "16imv0bwcx82n80n0xrgfmbsmnwy7zssmw8dp8y74qmj1ain2l0x") (r "1.56")))

(define-public crate-strloin-0.1.4 (c (n "strloin") (v "0.1.4") (d (list (d (n "beef") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0wwxhl9w8dwmdsp9l88l46y50ix28rxg8nl00kvx8d0py13bwwz6") (r "1.56")))

