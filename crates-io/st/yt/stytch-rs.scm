(define-module (crates-io st yt stytch-rs) #:use-module (crates-io))

(define-public crate-stytch-rs-0.1.0 (c (n "stytch-rs") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hyqq35glpjl31gwmlyv27zvnn0jk4v5plw27nx6ai8yhzm4kzbx") (y #t)))

