(define-module (crates-io st ic sticker-tf-proto) #:use-module (crates-io))

(define-public crate-sticker-tf-proto-0.1.0 (c (n "sticker-tf-proto") (v "0.1.0") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (o #t) (d #t) (k 1)))) (h "14wp3xr6544gh3vzzay011jpkq8drl6ma3wlx8ilziy6m9lp5m27") (f (quote (("proto-compile" "protoc-rust"))))))

(define-public crate-sticker-tf-proto-0.6.0 (c (n "sticker-tf-proto") (v "0.6.0") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (o #t) (d #t) (k 1)))) (h "00yfni9mrcxmr6dr2ivrzcidzq4cpzdhmv192xgzff9nj97zjj2q") (f (quote (("proto-compile" "protoc-rust"))))))

(define-public crate-sticker-tf-proto-0.7.0 (c (n "sticker-tf-proto") (v "0.7.0") (d (list (d (n "protobuf") (r "= 2.8.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (o #t) (d #t) (k 1)))) (h "108h7d4405bq7a3rlfmms8fk6jqxfgfa0sj5fmgji9cfg78h5q34") (f (quote (("proto-compile" "protoc-rust"))))))

(define-public crate-sticker-tf-proto-0.10.0 (c (n "sticker-tf-proto") (v "0.10.0") (d (list (d (n "protobuf") (r "= 2.8.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (o #t) (d #t) (k 1)))) (h "1hw2phnby31d0r32fwcdmhbin9ii3qjl5kf4kxf2q7s97f6wdkpi") (f (quote (("proto-compile" "protoc-rust"))))))

(define-public crate-sticker-tf-proto-0.11.0 (c (n "sticker-tf-proto") (v "0.11.0") (d (list (d (n "protobuf") (r "= 2.8.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (o #t) (d #t) (k 1)))) (h "1w07z2f2mr7l7d66wrfy05i576pgfvr1gd724bi4cx941hql3dpw") (f (quote (("proto-compile" "protoc-rust"))))))

