(define-module (crates-io st ic sticnarf-demo-lib) #:use-module (crates-io))

(define-public crate-sticnarf-demo-lib-0.1.0 (c (n "sticnarf-demo-lib") (v "0.1.0") (h "0v7s5vcbqkpr2glbziqakrv09nzrc6hp1v9mxgbk65a58xkng2wd") (y #t)))

(define-public crate-sticnarf-demo-lib-0.1.1 (c (n "sticnarf-demo-lib") (v "0.1.1") (h "0fyhlx3pw68v7s06mda1p4wjfp41dyp1mw7m681vln7061n04j39") (y #t)))

