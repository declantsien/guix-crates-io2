(define-module (crates-io st ic stick) #:use-module (crates-io))

(define-public crate-stick-0.1.0 (c (n "stick") (v "0.1.0") (h "06qinvjxw50nd2bxa0zdj8lp0s30i4av8hkyidnq3crxxyakga2h")))

(define-public crate-stick-0.2.0 (c (n "stick") (v "0.2.0") (h "13srqyvsdvji9j1i8s7xgcw4zqncbbjkz2wwilwmnq0mpc8py3x7")))

(define-public crate-stick-0.3.0 (c (n "stick") (v "0.3.0") (h "0q8f5hv0w3cpbbkil9iyd9i6ilk7hpp7v3j51fhrzhp5gy6pr4hy")))

(define-public crate-stick-0.4.0 (c (n "stick") (v "0.4.0") (h "0w7zd8zp3b0a6dypn20rm83qqx3k1qd3zw9h2mdlavsasp9yn2bd")))

(define-public crate-stick-0.4.1 (c (n "stick") (v "0.4.1") (h "0gpjvv1afbmn5fnrhxyjdwh75ca268cd9cbg2kcrrc57grf2yl7g")))

(define-public crate-stick-0.5.0 (c (n "stick") (v "0.5.0") (h "1hvcczfl5yaza8s85mgljxc2m34dw6avppa7zl0y0yl4irw10xs2")))

(define-public crate-stick-0.6.0 (c (n "stick") (v "0.6.0") (h "16x20v2vs8r8p1im0pmsf7wildqh9gi3kzkh8qvxn23fa202vn2s")))

(define-public crate-stick-0.7.0 (c (n "stick") (v "0.7.0") (h "1136dphcgi896b63bcrkis1zyxa8lixbpz5kvpq2j3zv2695bfw3")))

(define-public crate-stick-0.7.1 (c (n "stick") (v "0.7.1") (h "0ns47p8rlalfxjiixbcvy455c046xbpmv6aj4smxns70fjlb9vgf")))

(define-public crate-stick-0.8.0 (c (n "stick") (v "0.8.0") (d (list (d (n "pasts") (r "^0.1") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.1") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)))) (h "163jphrl51ak2q1li80k05xxffmm3q2frwrkrlg3mm3vyavp2vzc") (f (quote (("std") ("docs-rs") ("default"))))))

(define-public crate-stick-0.8.1 (c (n "stick") (v "0.8.1") (d (list (d (n "pasts") (r "^0.2") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.1") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)))) (h "1gkb85fhdbfnbiyij90kdhizh7sbvga1xd34xz66whm9nb5x3q9c") (f (quote (("std") ("docs-rs") ("default"))))))

(define-public crate-stick-0.9.0 (c (n "stick") (v "0.9.0") (d (list (d (n "pasts") (r "^0.4") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.1") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)))) (h "0by73d1mk4zwmi9hcbq0xnv8zxdy80byyfhhqxbm35gr9z8rdbic") (f (quote (("std") ("docs-rs") ("default"))))))

(define-public crate-stick-0.10.0 (c (n "stick") (v "0.10.0") (d (list (d (n "pasts") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "smelling_salts") (r "^0.1") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "0rskszx9a5p60gm7g92z4rbigk22zymzwpgldklmwa5a8hj03ky3") (f (quote (("padfont") ("docs-rs") ("default"))))))

(define-public crate-stick-0.10.1 (c (n "stick") (v "0.10.1") (d (list (d (n "pasts") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "smelling_salts") (r "^0.1") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "0l2dgi580r6jqsqzzbrv2i1p0wnfx8v2h3cf95hjn1zxxznv8k3y") (f (quote (("padfont") ("docs-rs") ("default"))))))

(define-public crate-stick-0.10.2 (c (n "stick") (v "0.10.2") (d (list (d (n "pasts") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "smelling_salts") (r "^0.1") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "1gzmfizsszbmk8l4ajpl5qy2l1ap1c6gwid6kqq7dzm34qqfdwa0") (f (quote (("padfont") ("docs-rs") ("default"))))))

(define-public crate-stick-0.11.0 (c (n "stick") (v "0.11.0") (d (list (d (n "pasts") (r ">=0.6.0, <0.7.0") (d #t) (k 2)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "smelling_salts") (r ">=0.1.0, <0.2.0") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "toml") (r ">=0.5.0, <0.6.0") (d #t) (k 1)))) (h "0msfgbwdkycln5nlj62c9x4nlp8yw10dchdibsjwj35pkkn3lzsa") (f (quote (("padfont") ("default"))))))

(define-public crate-stick-0.11.1 (c (n "stick") (v "0.11.1") (d (list (d (n "pasts") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "smelling_salts") (r "^0.1") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "0l3frj9pixnyrvwv72686b61h11z97lzwvaf4s26blg4h8qnrn8m") (f (quote (("padfont") ("default"))))))

(define-public crate-stick-0.12.0 (c (n "stick") (v "0.12.0") (d (list (d (n "log") (r "^0.4") (t "cfg(windows)") (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.4") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0l5yqv9680ha8981m1bryv089bacqc42ai80asaghzmp3gd6cf22") (f (quote (("sdb") ("gcdb") ("default" "sdb"))))))

(define-public crate-stick-0.12.1 (c (n "stick") (v "0.12.1") (d (list (d (n "log") (r "^0.4") (t "cfg(windows)") (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.4") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "161p2lbh4lxgm3isfpxjwqjr6ak1ais61ww3jnnm10w6n20h6qw3") (f (quote (("sdb") ("gcdb") ("default" "sdb"))))))

(define-public crate-stick-0.12.2 (c (n "stick") (v "0.12.2") (d (list (d (n "log") (r "^0.4") (t "cfg(windows)") (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.4") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09h7vazn46jgnks4nm20p7j2mhfl6zy6czlg3ymxdvhk4wi1wk86") (f (quote (("sdb") ("gcdb") ("default" "sdb"))))))

(define-public crate-stick-0.12.3 (c (n "stick") (v "0.12.3") (d (list (d (n "log") (r "^0.4") (t "cfg(windows)") (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.4") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lm1q30gw6g5p6p8cpl2wgl7013rwpjsbfy9l60s8hn7nf7dl0mv") (f (quote (("sdb") ("gcdb") ("default" "sdb"))))))

(define-public crate-stick-0.12.4 (c (n "stick") (v "0.12.4") (d (list (d (n "log") (r "^0.4") (t "cfg(windows)") (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.4") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1shcdjf1sakkpfd8j780mxl4w9rkvvfb0nf7ik5250d1gkbra2p6") (f (quote (("sdb") ("gcdb") ("default" "sdb"))))))

(define-public crate-stick-0.13.0 (c (n "stick") (v "0.13.0") (d (list (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.4") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"), target_os = \"linux\"))") (k 0)) (d (n "log") (r "^0.4") (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0s4dgpkg7j5c9gwh5f98vqjkrdfig368k6g38g612m1a2n6xni1d") (f (quote (("sdb") ("gcdb") ("default" "sdb"))))))

