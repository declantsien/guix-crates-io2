(define-module (crates-io st ic sticky) #:use-module (crates-io))

(define-public crate-sticky-1.0.0 (c (n "sticky") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "14bkwg4pi90l0vpvaw5lfqyry0hxn907mvq00nnddy3qql72n178")))

