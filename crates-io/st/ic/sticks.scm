(define-module (crates-io st ic sticks) #:use-module (crates-io))

(define-public crate-sticks-0.1.1 (c (n "sticks") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "188i5w1raha7bn5ydg3qgqwgw412jky32mg9xz7a6jbn9nqxkfdz")))

(define-public crate-sticks-0.1.2 (c (n "sticks") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "00x9z94hgxh8hczkwv6y5jgb3g3r5nabd0ys1arbbdz170b8nyv4")))

(define-public crate-sticks-0.1.3 (c (n "sticks") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1v21br76a02rdqg7932idwsmb6ncnqkjdqf6vnx6fzjc91pigy2p")))

(define-public crate-sticks-0.1.4 (c (n "sticks") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "15ripqzj5jbgifawvs4yxr77qhghm9sdbd1xppm88jv7kxfwwp7g")))

(define-public crate-sticks-0.1.5 (c (n "sticks") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "18z0kdfpgy5z5ln4ana4wxsvs1r5bml70h684axxkws2v63kll5z")))

(define-public crate-sticks-0.1.6 (c (n "sticks") (v "0.1.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0bpcxacx6q3a8cfz3xw7awm84yh11asmifdfjvlc52jy74gv3gwm")))

(define-public crate-sticks-0.1.7 (c (n "sticks") (v "0.1.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1jkbwl8frf8k90chzkcglzsyghvzcdjnl5h55ncsxb6d7d3b75c4")))

