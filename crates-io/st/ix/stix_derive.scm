(define-module (crates-io st ix stix_derive) #:use-module (crates-io))

(define-public crate-stix_derive-0.1.0 (c (n "stix_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.12.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11p4pxsc4rl2kmgb720ggwvgh8gbydck5z090cllj2l4plw42ni9")))

(define-public crate-stix_derive-0.2.0 (c (n "stix_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.12.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xqcs0hi4yi2s1ggjsryswh1f9smjpgz6h4p9q382ks6nih104k9")))

(define-public crate-stix_derive-0.3.0 (c (n "stix_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.12.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wdq8hz9wi7p34ihjd1s1igp8hk7jpsgj4nifyg15gnq9gvl8xhk")))

