(define-module (crates-io st db stdbm) #:use-module (crates-io))

(define-public crate-stdbm-0.0.1 (c (n "stdbm") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gyx958rcfs32cp9p3sr2jsi7x2hk33hvjg9zrlsqw9k1ywd113r")))

(define-public crate-stdbm-0.0.2 (c (n "stdbm") (v "0.0.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xh32dqwccmvk029kn6nzw5bf7wndixfary9y2kjnmkddjrkg5vh")))

(define-public crate-stdbm-0.0.3 (c (n "stdbm") (v "0.0.3") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01jxwwl0csg2s361hrxm15irw2dgx6c96c14dm3xcyas9x41kcsq")))

(define-public crate-stdbm-0.0.4 (c (n "stdbm") (v "0.0.4") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "config") (r "^0.13.1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14g2qnnz2gk76kw296nbv1s2mrxgwb5jicyvlb5m8vvdhzvflknk")))

