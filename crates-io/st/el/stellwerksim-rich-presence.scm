(define-module (crates-io st el stellwerksim-rich-presence) #:use-module (crates-io))

(define-public crate-stellwerksim-rich-presence-0.1.1 (c (n "stellwerksim-rich-presence") (v "0.1.1") (d (list (d (n "backon") (r "^0.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "discord-sdk") (r "^0.3.4") (d #t) (k 0)) (d (n "stellwerksim") (r "^0.2") (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1wf4q8yk44c19qsz0yb6g49c4jhp5ckaijwv7hl9gq1yx5vzvbfd")))

