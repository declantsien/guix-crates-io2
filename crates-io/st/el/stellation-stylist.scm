(define-module (crates-io st el stellation-stylist) #:use-module (crates-io))

(define-public crate-stellation-stylist-0.2.0 (c (n "stellation-stylist") (v "0.2.0") (d (list (d (n "stellation-backend") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "stylist") (r "^0.12.1") (f (quote ("yew_integration"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0sn1j9siffhjbxhm3k22zg4azrsgdxfvm25xc5brpb8h12vgbwmw") (f (quote (("frontend" "stylist/hydration")))) (s 2) (e (quote (("backend" "stylist/ssr" "dep:stellation-backend")))) (r "1.66")))

(define-public crate-stellation-stylist-0.3.0 (c (n "stellation-stylist") (v "0.3.0") (d (list (d (n "stellation-backend") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "stylist") (r "^0.12.1") (f (quote ("yew_integration"))) (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0nlgp2vk3bf67djhkn81qpppgff8gv4s2xriyrr3pycwqzkq9i29") (f (quote (("frontend" "stylist/hydration")))) (s 2) (e (quote (("backend" "stylist/ssr" "dep:stellation-backend")))) (r "1.66")))

