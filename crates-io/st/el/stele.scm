(define-module (crates-io st el stele) #:use-module (crates-io))

(define-public crate-stele-0.1.0 (c (n "stele") (v "0.1.0") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)))) (h "0rz783cvi6ddy3dbd32g4rrd981c38yyikhq4iizzx0c8bsd5ic4")))

(define-public crate-stele-0.2.0 (c (n "stele") (v "0.2.0") (d (list (d (n "loom") (r "^0.5.0") (d #t) (t "cfg(loom)") (k 0)))) (h "11bwi1m6gpvf13bjh48icrlingx40c5nhici06v7s1bww1mncigj")))

(define-public crate-stele-0.2.1 (c (n "stele") (v "0.2.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0k2pkadbdns3r38jfq6njsdv0kf9mic7kpv3abnka5l0ib7xy4za")))

(define-public crate-stele-0.3.0 (c (n "stele") (v "0.3.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "16p940b8p6j3jf6xg2vc8lvy90av0rd3gzx08an35zs16a8smpcf") (f (quote (("std") ("default" "std") ("allocator_api"))))))

(define-public crate-stele-0.3.1 (c (n "stele") (v "0.3.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1np5pprkxh93nwil7ixdbf6pdwqxy6y2127mbi03p94czb37lhmm") (f (quote (("std") ("default" "std") ("allocator_api"))))))

(define-public crate-stele-0.3.2 (c (n "stele") (v "0.3.2") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0rgkxkjg29zp17zdh1j1cyl8v7c0p0srd63774h1zsy14wbrz1xm") (f (quote (("std") ("default" "std") ("allocator_api"))))))

(define-public crate-stele-0.3.3 (c (n "stele") (v "0.3.3") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "05dzwpc9ps61l655kl319pdggc5g63xv4ny06j7f52vihx5nswxg") (f (quote (("std") ("default" "std") ("allocator_api"))))))

(define-public crate-stele-0.3.4 (c (n "stele") (v "0.3.4") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1nalyrz13cy7m6mbzq22xkb0vgz5flp1vhf4kvwi90i8mm8gq3nc") (f (quote (("std") ("default" "std") ("allocator_api"))))))

(define-public crate-stele-0.3.5 (c (n "stele") (v "0.3.5") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0ahsvgvf1s3q747bsl5ii0dmza9iymqjzdlir45nbaci70a2530l") (f (quote (("std") ("default" "std") ("allocator_api"))))))

