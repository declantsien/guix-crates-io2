(define-module (crates-io st el stellar-strkey) #:use-module (crates-io))

(define-public crate-stellar-strkey-0.0.1 (c (n "stellar-strkey") (v "0.0.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "02knk6jmbj7axmnq95rf40gndyrhmwdvgbgfd69bzv79r3l5d4gv") (r "1.63")))

(define-public crate-stellar-strkey-0.0.2 (c (n "stellar-strkey") (v "0.0.2") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0d7xqb65yx9hbgjw1wg3zgwkpz2hwpcjpg59gwprfb80cl3wh3im") (r "1.63")))

(define-public crate-stellar-strkey-0.0.6 (c (n "stellar-strkey") (v "0.0.6") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "0fd4cjwryv14j61m8yknl14r5yvzw2w5ffb950haqb5rfylj9p7v") (r "1.64")))

(define-public crate-stellar-strkey-0.0.7 (c (n "stellar-strkey") (v "0.0.7") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "048wf4509d1fq11z1kphj0hvava4hijp5if2zwp7zji6043qj1hc") (r "1.66")))

(define-public crate-stellar-strkey-0.0.8 (c (n "stellar-strkey") (v "0.0.8") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("std" "derive" "usage" "help"))) (o #t) (k 0)) (d (n "crate-git-revision") (r "^0.0.6") (d #t) (k 1)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1ggq7dx25s2c17p8z60psykkpgmgs5pq82l23nlpw48lw52vzlhj") (f (quote (("default")))) (s 2) (e (quote (("cli" "dep:clap")))) (r "1.67")))

