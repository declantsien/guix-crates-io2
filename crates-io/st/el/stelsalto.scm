(define-module (crates-io st el stelsalto) #:use-module (crates-io))

(define-public crate-stelsalto-0.1.0 (c (n "stelsalto") (v "0.1.0") (d (list (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "1qgp0mypmdgx21pp9i0dzy9ak71p0v3854zjbvlzf5r4rrd7a4f7")))

