(define-module (crates-io st el stellation) #:use-module (crates-io))

(define-public crate-stellation-0.1.1 (c (n "stellation") (v "0.1.1") (d (list (d (n "stctl") (r "^0.1.1") (d #t) (k 0)) (d (n "stellation-backend") (r "^0.1.1") (d #t) (k 0)) (d (n "stellation-bridge") (r "^0.1.1") (d #t) (k 0)) (d (n "stellation-core") (r "^0.1.1") (d #t) (k 0)) (d (n "stellation-frontend") (r "^0.1.1") (d #t) (k 0)))) (h "1gzb30gbjia9p5k57ch4hdxjinw0a933wcnzywh68r0hxzi33bc3") (r "1.66")))

(define-public crate-stellation-0.1.3 (c (n "stellation") (v "0.1.3") (d (list (d (n "stctl") (r "^0.1.3") (d #t) (k 0)) (d (n "stellation-backend") (r "^0.1.3") (d #t) (k 0)) (d (n "stellation-bridge") (r "^0.1.3") (d #t) (k 0)) (d (n "stellation-core") (r "^0.1.3") (d #t) (k 0)) (d (n "stellation-frontend") (r "^0.1.3") (d #t) (k 0)))) (h "0yvhna0gcmzpp5cipl5df9nh9dmgm4d89v0xn7a7iimwgh9x4rk8") (r "1.66")))

(define-public crate-stellation-0.1.4 (c (n "stellation") (v "0.1.4") (d (list (d (n "stctl") (r "^0.1.4") (d #t) (k 0)) (d (n "stellation-backend") (r "^0.1.4") (d #t) (k 0)) (d (n "stellation-bridge") (r "^0.1.4") (d #t) (k 0)) (d (n "stellation-core") (r "^0.1.4") (d #t) (k 0)) (d (n "stellation-frontend") (r "^0.1.4") (d #t) (k 0)))) (h "0vj2bkljwn6da85h706sc564jis1d6x7z67hw1djxxljnbz0ssrj") (r "1.66")))

(define-public crate-stellation-0.2.0 (c (n "stellation") (v "0.2.0") (d (list (d (n "stctl") (r "^0.2.0") (d #t) (k 0)) (d (n "stellation-backend") (r "^0.2.0") (d #t) (k 0)) (d (n "stellation-bridge") (r "^0.2.0") (d #t) (k 0)) (d (n "stellation-core") (r "^0.2.0") (d #t) (k 0)) (d (n "stellation-frontend") (r "^0.2.0") (d #t) (k 0)))) (h "13s02gb62c3d99vr1ha0wkl6jn1f2hmkhxki8i1byf4y5v7g8xq7") (r "1.66")))

(define-public crate-stellation-0.3.0 (c (n "stellation") (v "0.3.0") (d (list (d (n "stctl") (r "^0.3.0") (d #t) (k 0)) (d (n "stellation-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "stellation-bridge") (r "^0.3.0") (d #t) (k 0)) (d (n "stellation-core") (r "^0.3.0") (d #t) (k 0)) (d (n "stellation-frontend") (r "^0.3.0") (d #t) (k 0)))) (h "12fs4pd9s67nd4h29grlcbx2y6jhg8frsacxiswb4b1qkp9wlbpr") (r "1.66")))

