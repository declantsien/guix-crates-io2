(define-module (crates-io st el stellar-notation) #:use-module (crates-io))

(define-public crate-stellar-notation-0.1.0 (c (n "stellar-notation") (v "0.1.0") (h "1fk2449jdn0zwni815bw3r0nr9hblassjafyc03nhk6k8i61mg98")))

(define-public crate-stellar-notation-0.1.1 (c (n "stellar-notation") (v "0.1.1") (h "1qly3sgmcc9jspp5b4bsgahznq55vvvvh8wxfm4ydpg25gzvirsy")))

(define-public crate-stellar-notation-0.1.2 (c (n "stellar-notation") (v "0.1.2") (h "1m5lbk6skn71zhk39ja1ln975skdv1msp0gj94zn880i9xf5m5qi")))

(define-public crate-stellar-notation-0.2.0 (c (n "stellar-notation") (v "0.2.0") (h "0djsp4x2x0nhnzknk8ranfdcvvh101i9f18fqvf01dfngxpkzfap")))

(define-public crate-stellar-notation-0.2.1 (c (n "stellar-notation") (v "0.2.1") (h "11bh15c05l4cxb1d7vdhbqd0wb4f10dv89vfnv78873kjljdv5ix")))

(define-public crate-stellar-notation-0.2.2 (c (n "stellar-notation") (v "0.2.2") (h "14g9d1y3avm4jm8636camrbdjr0ckyz33jm5i6zfccmc7igic3ay")))

(define-public crate-stellar-notation-0.3.0 (c (n "stellar-notation") (v "0.3.0") (h "18x4hh96889yvz2nm2676fs9vqvp0l3g7nq5xr5gg0g2rg722grb")))

(define-public crate-stellar-notation-0.5.0 (c (n "stellar-notation") (v "0.5.0") (h "1gsqgii0sa2vlmi3d9aqkyfn1gz0s1b96czvd41wrj1gx5kq7rwp")))

(define-public crate-stellar-notation-0.6.0 (c (n "stellar-notation") (v "0.6.0") (h "0r77g9akdy8ybsw8h65vc8gj6mmxixaflw41skzykp41r55fw88x")))

(define-public crate-stellar-notation-0.7.0 (c (n "stellar-notation") (v "0.7.0") (h "0whnswbb65plj0kq6xhbr63ar0yj3jcpp8kqxwzh1s841fchm9a2")))

(define-public crate-stellar-notation-0.9.0 (c (n "stellar-notation") (v "0.9.0") (h "0yg65m63hil39dkvyq21pbdfr9789g99arqda1rbsnwgrpcjdb41")))

(define-public crate-stellar-notation-0.9.1 (c (n "stellar-notation") (v "0.9.1") (h "0rg1bxghcga0bzspja1qfij5fw4x23vmknql9d64naxqyz7fzar4")))

(define-public crate-stellar-notation-0.9.2 (c (n "stellar-notation") (v "0.9.2") (h "0lqdh7n4n43i0j50jpbh235hskf1psfdsxxwvx44zvrjj5pw6r2g")))

(define-public crate-stellar-notation-0.9.3 (c (n "stellar-notation") (v "0.9.3") (h "17frx21z3k395jf9vn788iahlxiswvapfcvb662qchxrqnjdw2z6")))

(define-public crate-stellar-notation-0.9.4 (c (n "stellar-notation") (v "0.9.4") (h "0zvzgjhj09f54qmm3g1cv6jl0pc7qvrrl4cyv6kl0awl6vnsa186")))

(define-public crate-stellar-notation-0.9.5 (c (n "stellar-notation") (v "0.9.5") (h "1w6jy0v39sy6aqrx5wy2zvkqjir3256v0rvrydrl7h639p8p7cqz")))

(define-public crate-stellar-notation-0.9.6 (c (n "stellar-notation") (v "0.9.6") (h "1a1ipi7f1r3ff4s3fms6l4rk0r6kvcc6hvkdf4nlhn3vi5q3xbd8")))

(define-public crate-stellar-notation-0.9.8 (c (n "stellar-notation") (v "0.9.8") (h "10k3l9vlqnisxsyr6wq6kfn0bl10cdb1f0cnk4x4baff8ab0jscs")))

(define-public crate-stellar-notation-0.9.9 (c (n "stellar-notation") (v "0.9.9") (h "0cxf3lfdvnw0284b54pkfz2a3127bxsfrspcx36i7v6v92gqvkkp")))

(define-public crate-stellar-notation-0.9.10 (c (n "stellar-notation") (v "0.9.10") (h "1giz4calc2rbq7s0d2pnb0495p5wb8p4chnw9vw0y8l2hm3885k9")))

(define-public crate-stellar-notation-0.9.11 (c (n "stellar-notation") (v "0.9.11") (h "0h272bfyk6zr057dyrwf2wgyzzj9iaisnfrf42q2ppcl2g4q98ki")))

(define-public crate-stellar-notation-0.9.12 (c (n "stellar-notation") (v "0.9.12") (h "1rn75sv1sz12d0blk2lw81gfqhxvglvyvb8q1vah73fdjq810ap5")))

(define-public crate-stellar-notation-0.9.13 (c (n "stellar-notation") (v "0.9.13") (h "1p8kyg4l238g2cfb0pniqvkhqv11zf7l6fdghhqg3mvz62dvl74z")))

(define-public crate-stellar-notation-0.9.15 (c (n "stellar-notation") (v "0.9.15") (h "15nhh1x27jdfmryg4y6bga0cjqr8g8jvh36i4ild3pgwq9f7fzcq")))

(define-public crate-stellar-notation-0.9.16 (c (n "stellar-notation") (v "0.9.16") (h "0xxykf0xaz4hvvq6gn9azl9l0qh617hb69mm1mn8ya0dqx9gy5r2")))

(define-public crate-stellar-notation-0.9.17 (c (n "stellar-notation") (v "0.9.17") (h "0i9399cp22bhwv87441g81024f6rzkx27b137bh4b23smy633z7l")))

(define-public crate-stellar-notation-0.9.18 (c (n "stellar-notation") (v "0.9.18") (h "12086vkw1xlr6ma0g504d8mzifczbyqmac7h143r7b4m0bhqqx6q")))

