(define-module (crates-io st el stelar) #:use-module (crates-io))

(define-public crate-stelar-0.1.0 (c (n "stelar") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 2)))) (h "02n2bj88m5f43wlc08lrpkxsk01im9s68h8xg9rbgdyimxrg5jca") (f (quote (("print_table" "prettytable-rs"))))))

(define-public crate-stelar-0.1.1 (c (n "stelar") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 2)))) (h "0igg5013f204p77xcga2kpbgaw4ys388gs56nd2hr9a5xdjh4ddn") (f (quote (("print_table" "prettytable-rs"))))))

(define-public crate-stelar-0.1.2 (c (n "stelar") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 2)))) (h "1yjfk2aixxw808w61y8dvgfknzjm6icmasanfi13cb4r4ky2nwj3") (f (quote (("print_table" "prettytable-rs"))))))

(define-public crate-stelar-0.1.3 (c (n "stelar") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 2)))) (h "1dl6qdgfkz1hb36sqwccpa5f1gzvf4lgdnqgbrvkhr8l899w365y") (f (quote (("print_table" "prettytable-rs"))))))

