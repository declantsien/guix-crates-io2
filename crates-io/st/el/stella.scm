(define-module (crates-io st el stella) #:use-module (crates-io))

(define-public crate-stella-0.0.0 (c (n "stella") (v "0.0.0") (d (list (d (n "rug") (r "^1.19.0") (d #t) (k 0)))) (h "1aprpd4bcsykm5j3aadv2ch29ay8g84ni0i8292fzbkx2l09mbjy")))

(define-public crate-stella-0.0.1 (c (n "stella") (v "0.0.1") (d (list (d (n "rug") (r "^1.19.2") (d #t) (k 0)))) (h "015r175lsqg7s7qbpsa9zq21mr1yl2j306pikwzbpsgb19ki4rii")))

(define-public crate-stella-0.0.2 (c (n "stella") (v "0.0.2") (d (list (d (n "rug") (r "^1.19.2") (d #t) (k 0)))) (h "1rxf3n9ykb8ycsk19f0z47vcg4c5389rlkwr910vj5a0y0qb9k49")))

(define-public crate-stella-0.0.3 (c (n "stella") (v "0.0.3") (d (list (d (n "rug") (r "^1.19.2") (d #t) (k 0)))) (h "073z8p3dyz8mis4wmffr4rm0wbq7nxc44w17avqyqnapb53hsvxp")))

