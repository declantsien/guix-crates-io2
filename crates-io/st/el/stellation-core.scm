(define-module (crates-io st el stellation-core) #:use-module (crates-io))

(define-public crate-stellation-core-0.1.1 (c (n "stellation-core") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0l4x8kjg0hpy6rfj4crml2lzdrzfcfklsigkdgcdggz3bplk5v51") (r "1.66")))

(define-public crate-stellation-core-0.1.3 (c (n "stellation-core") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0rfk21ilkvpbawg69y8csqy8ha71fp836dyfr695yx1g5mz8kqf0") (r "1.66")))

(define-public crate-stellation-core-0.1.4 (c (n "stellation-core") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0pin4h22y1h6yvpg1w0mqzny3i51yfjpbs43jpyybj3hqjp6apzj") (r "1.66")))

(define-public crate-stellation-core-0.2.0 (c (n "stellation-core") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "02dga1fkagnjcc0figxb019b6i9cmr9crw1p11w120yyz3gy33a5") (r "1.66")))

(define-public crate-stellation-core-0.3.0 (c (n "stellation-core") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "11vmplbvxpgnmb27cdsnl0plvbi5hkshx14knp8hcxqmzrjkjdfd") (r "1.66")))

