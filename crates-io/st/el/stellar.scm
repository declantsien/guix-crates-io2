(define-module (crates-io st el stellar) #:use-module (crates-io))

(define-public crate-stellar-0.1.0-alpha (c (n "stellar") (v "0.1.0-alpha") (d (list (d (n "byte_sha") (r "^1.0.1") (d #t) (k 0)))) (h "0szmfl653ysn19y5bdrhg3qiv6j3nz0qmq6zy6jzjqjzqgp2rp1l")))

(define-public crate-stellar-0.1.1-alpha (c (n "stellar") (v "0.1.1-alpha") (d (list (d (n "byte_sha") (r "^1.0.1") (d #t) (k 0)))) (h "0l1mhk3gb6iwxirsvq29w77radfsrz2wxdy90jgqijfv5i8kpswb")))

(define-public crate-stellar-0.1.2-alpha (c (n "stellar") (v "0.1.2-alpha") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0r6x6dv0gcgg1v5jcy9ay5a46nglf6vhwqq51r2zz7rbwy335cd6")))

