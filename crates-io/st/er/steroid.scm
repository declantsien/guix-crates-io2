(define-module (crates-io st er steroid) #:use-module (crates-io))

(define-public crate-steroid-0.1.0 (c (n "steroid") (v "0.1.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "nix") (r "0.20.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "0.7.*") (d #t) (k 2)))) (h "1j1c59amssdihlgrw3njvk3ffa5k4sj07dfk86746hvb37wkhr30")))

(define-public crate-steroid-0.1.1 (c (n "steroid") (v "0.1.1") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "nix") (r "0.20.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "0.7.*") (d #t) (k 2)))) (h "051sdj8j3x575zcmi518fcrxw5yi0sg2qg27lh04g4iy9m8l9n09")))

(define-public crate-steroid-0.2.0 (c (n "steroid") (v "0.2.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "nix") (r "0.20.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "0.7.*") (d #t) (k 2)))) (h "121xf3rlc2bx0h1wicv5wm39liq4hp74syyvpn8kdkikpgzppp0k")))

(define-public crate-steroid-0.3.0 (c (n "steroid") (v "0.3.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "nix") (r "0.20.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "0.7.*") (d #t) (k 2)))) (h "1vk2rfzh2qwagmf00a9s7f46carlcqfvj8flijmyhzj0wi6y9alh")))

(define-public crate-steroid-0.4.0 (c (n "steroid") (v "0.4.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "nix") (r "0.26.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "0.7.*") (d #t) (k 2)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)))) (h "161rnivafan3wzb2f0i62jns0mp76w34rc7mqqya0ni9hyz7cxd6")))

(define-public crate-steroid-0.5.0 (c (n "steroid") (v "0.5.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "nix") (r "0.26.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "0.7.*") (d #t) (k 2)) (d (n "thiserror") (r "1.0.*") (d #t) (k 0)))) (h "0v2fnhjdxpqsyis024h5hbh349ln1s4fanbrch990v0zsaqg2a60")))

