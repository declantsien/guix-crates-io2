(define-module (crates-io st er stereokit-sys-mod) #:use-module (crates-io))

(define-public crate-stereokit-sys-mod-1.0.0 (c (n "stereokit-sys-mod") (v "1.0.0") (d (list (d (n "bevy_ecs") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "palette") (r "^0.7.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11d8jh73zjkgng313r9wp2hy7ikwxx7ask6py18m6bkf590q0xp1") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

