(define-module (crates-io st er stereokit-locomotion) #:use-module (crates-io))

(define-public crate-stereokit-locomotion-0.1.0 (c (n "stereokit-locomotion") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "glam") (r "^0.22.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "stereokit") (r "^0.15.0") (d #t) (k 0)))) (h "1zhfxxvz9b8cfx2qdqnv7n7sdjcs01gc42h259w4hlinz230mxpw")))

