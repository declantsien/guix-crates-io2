(define-module (crates-io st er stereokit-sys) #:use-module (crates-io))

(define-public crate-stereokit-sys-0.1.0 (c (n "stereokit-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "02pwgzck59mlgb7h1fn6rfb3rw8gjbqi25cdra9wsya75mbqmzr6") (l "StereoKitC")))

(define-public crate-stereokit-sys-0.1.1 (c (n "stereokit-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0cqb3108366ry1z5k0mx40n09d8akn80h6fkn9px66xvdckjd52z") (l "StereoKitC")))

(define-public crate-stereokit-sys-1.0.0 (c (n "stereokit-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "18fgipvaan4dnj693nsnmnm4ja6ajaw4p82dp7yljqzi6ag989nx") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-1.0.1 (c (n "stereokit-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1hxyfxjr9g3s1inynfs1wz4w1hzv8fgqqnrmga4709f11n16wfls") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-1.0.2 (c (n "stereokit-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "01nplzj6plrlvyxy8sx1iqz33n91h8ggf4kg9p6i28zc6zbdiz0v") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-1.0.3 (c (n "stereokit-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "15wzlv8j117im131nvlf2kccnk12szx1yqcmamwyi9a33f8ia6sx") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-1.0.4 (c (n "stereokit-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0f29q1ihlq8jk6rhzh6zgllnmylyh1a9aqysyilv1vqman1jv199") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.0.0 (c (n "stereokit-sys") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "14ig9rwzgx08pv1i21pcqvdsfppbsihcx3qy0fi0370wrrzharj9") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.0.1 (c (n "stereokit-sys") (v "2.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1h524ag4459d9qszayad3iv2749y7dfxhd9dqhqw1yy8ha19mbwj") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.2.0 (c (n "stereokit-sys") (v "2.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0n2vl2cs7viwj69ap8clpvb08279xr025qs7qnkjhpkbmjgv0kc4") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.3.0 (c (n "stereokit-sys") (v "2.3.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1z4vs71sv3jkyhzdqiznrk9l0qr3q26i7d66azc4kqjbsslqf4v6") (f (quote (("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.4.0 (c (n "stereokit-sys") (v "2.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1nz5zd8paiij8inpxyp4qwpvql5l3c58jih66h14cxfyx50q3qzh") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.4.1 (c (n "stereokit-sys") (v "2.4.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "09sx9a6jmx7x0spyl4h3wikckpfnhwciavjbbhy2j117l31hrnpq") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.4.2 (c (n "stereokit-sys") (v "2.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0gq04v00xv5dlf9xxinvn77d2qcixcrq0sl9kqagrwk6hw4g7mj4") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.4.3 (c (n "stereokit-sys") (v "2.4.3") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0ir4svhhzxdyqpj09ssipgwp7kdg74n4kpl4ljdvpcdd566wk7bc") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.5.3 (c (n "stereokit-sys") (v "2.5.3") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0p6hbv8pm96vd5d7a3fdl8yfca8v8zr5fn2f0ydymq73zvrpmmvw") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.5.4 (c (n "stereokit-sys") (v "2.5.4") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "185mlf431jg8bslg3igw45plynpjvf45kbdr9k7qz8gbqi45i8lz") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.5.5 (c (n "stereokit-sys") (v "2.5.5") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0zz7r7mnhnsd8nb96c29vg9xld21733bswj13nld7083fbmplz5z") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

(define-public crate-stereokit-sys-2.5.6 (c (n "stereokit-sys") (v "2.5.6") (d (list (d (n "bevy_ecs") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "bevy_reflect") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "prisma") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0y2avz2d4pnw6s8yaw7wyig7kbbjj4hsgs31qfqsdr2nk90zgssl") (f (quote (("physics") ("linux-egl") ("default")))) (l "StereoKitC")))

