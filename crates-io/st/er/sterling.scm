(define-module (crates-io st er sterling) #:use-module (crates-io))

(define-public crate-sterling-0.1.0 (c (n "sterling") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1w0ylgi95hljahgmba7n4l4yy1njcvvys3zhycwxn19hcd1mdjxx")))

(define-public crate-sterling-0.2.0 (c (n "sterling") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "13pip3ilycgjxz4vfnfpanhsapqbh0pxg0yg9p32hnqq5j3ik4y6")))

(define-public crate-sterling-0.3.0 (c (n "sterling") (v "0.3.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1b7x5bkgs2xknabggh94ip4jdzkxk5c7szz2wgwcg1d2jwja7aa7")))

(define-public crate-sterling-1.0.0 (c (n "sterling") (v "1.0.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "separator") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0iwph2yh5z8dk3bljlfbzp8aiaizk00sfwxr4rg0x3l15917sk2y")))

(define-public crate-sterling-1.0.1 (c (n "sterling") (v "1.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "lazysort") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "separator") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0p1j3wwy8mjhlf5g3rhr31wb5lgxa3nlljcrggddwlwh09v5ps8w")))

