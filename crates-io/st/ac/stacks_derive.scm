(define-module (crates-io st ac stacks_derive) #:use-module (crates-io))

(define-public crate-stacks_derive-0.2.1 (c (n "stacks_derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "01n8na45rz9vr7xvam6fkxnmlf4p05k9n7nmfhwka1yq432avzyf")))

(define-public crate-stacks_derive-0.2.3 (c (n "stacks_derive") (v "0.2.3") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0mnj0r8pxlis94vklgj67d5mx3s360w2499m4k9v1bvzrr3w48ky")))

(define-public crate-stacks_derive-0.2.4 (c (n "stacks_derive") (v "0.2.4") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1phhz2djy3x8ya50n7l8h19cy626fff8ss4li39mjk5aflqhb6h5")))

(define-public crate-stacks_derive-0.2.5 (c (n "stacks_derive") (v "0.2.5") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0n6iw1h05d5202w8cpdglx1ggk260q4n15a6kwwcffklrvrwwmlp")))

(define-public crate-stacks_derive-0.2.6 (c (n "stacks_derive") (v "0.2.6") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0wn15ag3d0017c1mlwg60lylaqdvrfvj69qa68zjna82gf7vgwsr")))

(define-public crate-stacks_derive-0.2.7 (c (n "stacks_derive") (v "0.2.7") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1k5c5hwka15alx6bzmf159b42wyszbbh09193p6wgh5qxjhj31kp")))

(define-public crate-stacks_derive-0.2.8 (c (n "stacks_derive") (v "0.2.8") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1mix1dmkivdpbid72757i4kwixnrxlndqknpaa4q2hgigybans6f")))

(define-public crate-stacks_derive-0.3.0 (c (n "stacks_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0gw7rqjlhipii8jgjvyhf1qhvqq64ya07047fqgjfhikjykmgwyq")))

(define-public crate-stacks_derive-0.3.1 (c (n "stacks_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1r19yrf0s9nmdz2ka5kac9wh049v4awjhmpaf51cmys421a0ahq1")))

(define-public crate-stacks_derive-0.3.2 (c (n "stacks_derive") (v "0.3.2") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0631fk9az6mpr2cpyzf9jklrkgsax38phfxby0ilmk89njfqg7qa")))

(define-public crate-stacks_derive-0.3.3 (c (n "stacks_derive") (v "0.3.3") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0lh2k8j2z790zhiwbf0fm9a18ac1mkcdsqij7b69i5zpxk715j0s")))

