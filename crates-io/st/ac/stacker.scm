(define-module (crates-io st ac stacker) #:use-module (crates-io))

(define-public crate-stacker-0.1.0 (c (n "stacker") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.9") (d #t) (k 1)))) (h "0qspsb5d69y7vwny910f9ilkphkjrysynbw318xk17xwp1l9wj6a")))

(define-public crate-stacker-0.1.1 (c (n "stacker") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.10") (d #t) (k 1)))) (h "151pjd4m72shp7zwq1ad25qak98j6fwkq2y9yjwrjq0j1mdd6sdz")))

(define-public crate-stacker-0.1.2 (c (n "stacker") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y0669raganbrzvnjmmy4ys7abc24w8dd61xkmbj625zs7bv7lfx")))

(define-public crate-stacker-0.1.3 (c (n "stacker") (v "0.1.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q42x2zyiniwrb3zrxw6dclvqi3k753ikpfvi78ixa3qbd451hc2")))

(define-public crate-stacker-0.1.4 (c (n "stacker") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wanbh78h1r9w3xqvm0nplxqwb37czd7w2cv8brjm0zyvs7rfi6f")))

(define-public crate-stacker-0.1.5 (c (n "stacker") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0js0axz5nla1mkr2dm2vrv9rj964ng1lrv4l43sqlnfgawplhygv")))

(define-public crate-stacker-0.1.6 (c (n "stacker") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12igajfgqz96c7vcwi91xdfsphawik6g36ndlglqih0a7bqw8vyr")))

(define-public crate-stacker-0.1.7 (c (n "stacker") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0cripq4r48249m42mpr2z39l7iqxmln5afk9l83wssybqqd8b6x8")))

(define-public crate-stacker-0.1.8 (c (n "stacker") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hwpkjx1dbdgy64sjzsaf5vrv74vf4lj5migwvj1gd5vi9xldhij")))

(define-public crate-stacker-0.1.9 (c (n "stacker") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lndihyq9lqr5jlcpa7fi8l834mnslkca9wzddnh073f8ldr9pbj")))

(define-public crate-stacker-0.1.10 (c (n "stacker") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18g5glvy63jpw1qs1h757asd1shrn2pfbkgvml2abyxw6nzbqxl3")))

(define-public crate-stacker-0.1.11 (c (n "stacker") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vy6fchch3lrga3n9i1rhdxnbg1j2ldczckakm9qrrva013c6ax9")))

(define-public crate-stacker-0.1.12 (c (n "stacker") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11hpk7vqk72baqz52vrswh08fr6pcclil3v11wnwhyy5dv0b9k11")))

(define-public crate-stacker-0.1.13 (c (n "stacker") (v "0.1.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "046fafhpy3zmx6wn3yylcggrnl98f96bihzvhnvz67801n27xx63")))

(define-public crate-stacker-0.1.14 (c (n "stacker") (v "0.1.14") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1d693prg71h7p3f5khjlahrpfwqyci6rbj7vylzhnhm4f58rv4wh")))

(define-public crate-stacker-0.1.15 (c (n "stacker") (v "0.1.15") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "psm") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi" "winbase" "fibersapi" "processthreadsapi" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1klz4mk1iqn3jixhnls6ia4ql4fpinnfjibxabpx6pqmh12bv1n8")))

