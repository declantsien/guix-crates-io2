(define-module (crates-io st ac stackdump-core) #:use-module (crates-io))

(define-public crate-stackdump-core-0.1.0 (c (n "stackdump-core") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "gimli") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "1flb04hck50qdf0vc46va1xdaycqkvq5y2k7ps0pjydgpad72z01") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.1.1 (c (n "stackdump-core") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "gimli") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "15q7vnzlzm3gr1xiqrkw83p25r8lfij7k0xsp1alr50w6mf8i2rk") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.1.2 (c (n "stackdump-core") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "gimli") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "15fgklyn3bk51vhjjy1ch86ynrmrawmnh100sm9y3jykyq2hpcy7") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.1.3 (c (n "stackdump-core") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "gimli") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "1zs21pbzlbpaky9vvhx0dpq92fh7m8bnjba7rb3mxrp6zkkghsny") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.2.0 (c (n "stackdump-core") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "gimli") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "0k8g3ly7cb6pglvbia9iiy9c1q56fajshs425hipaadsq0hkf6wp") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.2.1 (c (n "stackdump-core") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "gimli") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "1v6glc2yl073jfiqg395dybgywc0yllzrq4yhgfcrbzkgl5vdywf") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.2.2 (c (n "stackdump-core") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "gimli") (r "^0.26.1") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "0l6xb8j2j70kwrhbn269v3hl49y702nd1ydmqqih3slqi9866s0z") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.3.0 (c (n "stackdump-core") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "gimli") (r "^0.27.2") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "1v0am4r9mw6lyjrzv2nxk0v64ch1pjw04c1ym1dnrw3rhyd6f3hg") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

(define-public crate-stackdump-core-0.4.0 (c (n "stackdump-core") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7.2") (f (quote ("serde"))) (k 0)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "gimli") (r "^0.28.0") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "02dg6azi0ldm8myxrq0r0lyj16qqdjxq6jpr821dk3qhhzpaq0vz") (f (quote (("std" "arrayvec/std" "serde/std") ("default" "std"))))))

