(define-module (crates-io st ac stackfuture) #:use-module (crates-io))

(define-public crate-stackfuture-0.1.0 (c (n "stackfuture") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)))) (h "0dpid9k8hdp9q4kai0if3p0nay978rgaz5v02aziq11swmrld9hm")))

(define-public crate-stackfuture-0.1.1 (c (n "stackfuture") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)))) (h "06g4l6vxiz6npbc8hwqb6xmpw0whfq5amwr1pv6qg8h50jafyh21")))

(define-public crate-stackfuture-0.2.0 (c (n "stackfuture") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)))) (h "15adfpc8dgdpyn2cgpmyhsgy5yaqm4vdfcfrzag3m85kidw5b1b3") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-stackfuture-0.3.0 (c (n "stackfuture") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)))) (h "1wnpg856zcr726yggqjbayimqzn7zymxpv8nzbd71vvj5c2r5bkf") (f (quote (("default" "alloc") ("alloc"))))))

