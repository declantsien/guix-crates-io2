(define-module (crates-io st ac stackbox) #:use-module (crates-io))

(define-public crate-stackbox-0.0.1 (c (n "stackbox") (v "0.0.1") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)))) (h "1kqj6gz2mp0am4n9vlf3pwkisj2qrw3jk0kgdv2s73r8rqd221fg")))

(define-public crate-stackbox-0.0.2 (c (n "stackbox") (v "0.0.2") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)))) (h "125j4j0fkjn511vjz7c7qlanjjp08ra9sw7sy9mdxh93vrc6fp58")))

(define-public crate-stackbox-0.1.0-rc1 (c (n "stackbox") (v "0.1.0-rc1") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "with_locals") (r "^0.3.0-rc1") (d #t) (k 2)))) (h "1jr66wj7lqaaj03bgsxapfxcxidjidvsljndcpi9zp7m108kp0r6") (f (quote (("std" "alloc") ("docs") ("default" "alloc") ("const-generics") ("alloc"))))))

(define-public crate-stackbox-0.1.0 (c (n "stackbox") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "with_locals") (r "^0.3.0-rc1") (d #t) (k 2)))) (h "11q07axlfclk7yxn3aajxdggi2d2fr87g12nh36hjmjk1rdipj46") (f (quote (("std" "alloc") ("docs") ("default" "alloc") ("const-generics") ("alloc"))))))

(define-public crate-stackbox-0.1.1-rc1 (c (n "stackbox") (v "0.1.1-rc1") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "with_locals") (r "^0.3.0-rc1") (d #t) (k 2)))) (h "06hng2w5anhv944g82g3qq0bjl683bgxbg2a5v13c7zyg3l4l1zs") (f (quote (("std" "alloc") ("docs") ("default" "alloc") ("const-generics") ("alloc"))))))

(define-public crate-stackbox-0.1.1 (c (n "stackbox") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "with_locals") (r "^0.3.0-rc1") (d #t) (k 2)))) (h "00bp1p9q1ckr77w1qsd0qcm5fa7jpl5zq83syfzgwrsh6pkayycs") (f (quote (("std" "alloc") ("docs") ("default" "alloc") ("const-generics") ("alloc"))))))

(define-public crate-stackbox-0.1.2 (c (n "stackbox") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "with_locals") (r "^0.3.0-rc1") (d #t) (k 2)))) (h "1fhb1mhjh1b5ivi24rd0gkyvspwgmn6zwzxwk6px7f41wflnxf34") (f (quote (("std" "alloc") ("docs") ("default" "alloc") ("const-generics") ("alloc"))))))

