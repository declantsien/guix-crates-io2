(define-module (crates-io st ac stack-buffer) #:use-module (crates-io))

(define-public crate-stack-buffer-0.1.0 (c (n "stack-buffer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0wbxlk3bbn31qixmb9gj4rxqmb4iz517b49jgkxc24zn8bhihpa1") (y #t)))

(define-public crate-stack-buffer-0.2.0 (c (n "stack-buffer") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1v0dyhvvmd3cymvnjrykhrnl9c0wwg3a71qbd6l3ca7bxiag0cd5") (y #t)))

(define-public crate-stack-buffer-0.3.0 (c (n "stack-buffer") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0hmlpmsa3qwfx73i1d3nyz3nn72lvb1r1g2riy1gck0iglmv3lqx") (y #t)))

