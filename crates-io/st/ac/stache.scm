(define-module (crates-io st ac stache) #:use-module (crates-io))

(define-public crate-stache-0.1.0 (c (n "stache") (v "0.1.0") (d (list (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "pest") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)))) (h "0qc258kj50nk1iz6smalvsawgbk7i5ri9sqvsaxdxpm1f68w2y51")))

(define-public crate-stache-0.2.0 (c (n "stache") (v "0.2.0") (d (list (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "pest") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~0.1") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.3") (d #t) (k 0)))) (h "1b5jplb8ic4mq7cb21gyzm1aqf3x2icsik5zznj9flvdz79fbfc6")))

