(define-module (crates-io st ac staccato) #:use-module (crates-io))

(define-public crate-staccato-0.1.0 (c (n "staccato") (v "0.1.0") (d (list (d (n "clap") (r "^2.10.0") (d #t) (k 0)))) (h "1m7dnl86zxhc01lkxx2gmb50riwacg68zswwxabjlg670k4jf9xb")))

(define-public crate-staccato-0.1.1 (c (n "staccato") (v "0.1.1") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "0pd0a24p6bvh2lj9c92df59fva4cmkdlz6v4374gvn27flhm4j0a")))

(define-public crate-staccato-0.1.2 (c (n "staccato") (v "0.1.2") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "0262iag0a21w8zzmcnwpdzr7mqa0adnqpzjxdd9gdgsxn4xfxxfj")))

(define-public crate-staccato-0.1.3 (c (n "staccato") (v "0.1.3") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "1zbidlmps2g2968civd0q0gnnacz54gzkrg5sp9im3wlcxpxr1f9")))

(define-public crate-staccato-0.1.4 (c (n "staccato") (v "0.1.4") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "1kb8gh9jw585jdizs71p0rw2yrrrijwzknj3wv8xhd4gf362jzgs")))

(define-public crate-staccato-0.1.5 (c (n "staccato") (v "0.1.5") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "0h6gg73qdmnim4j24jq67ml0a2z6n60z0vfhk1a568205kpqf918")))

(define-public crate-staccato-0.1.6 (c (n "staccato") (v "0.1.6") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "14wbgwnz5z1w08wsspzny20pg46x3awg7g1jkq9848k91xgrv9g5")))

(define-public crate-staccato-0.1.7 (c (n "staccato") (v "0.1.7") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "1w1wsar9wklcw1y7h6pxk7pf0d7pm2bv401xg8lx1ll3a1damijn")))

(define-public crate-staccato-0.1.8 (c (n "staccato") (v "0.1.8") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "0fwdn1vhyiqrrvwirs0gv2krzs3hxpkcv5layfwia04a345da81c")))

(define-public crate-staccato-0.1.9 (c (n "staccato") (v "0.1.9") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)))) (h "1c9l2m6gm4cnf89jqhzqmjdch0i9cg7ykhc0ak57qc000jbzkq69")))

