(define-module (crates-io st ac stack-server) #:use-module (crates-io))

(define-public crate-stack-server-0.1.0 (c (n "stack-server") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.2") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "tera") (r "^1.12.0") (d #t) (k 0)))) (h "19yn9ppg00yk9d5s6yibcs7mda82lapkn40pnnh6bz08xpsmbggl")))

