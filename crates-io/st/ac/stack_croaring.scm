(define-module (crates-io st ac stack_croaring) #:use-module (crates-io))

(define-public crate-stack_croaring-0.3.10 (c (n "stack_croaring") (v "0.3.10") (d (list (d (n "croaring-sys") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1rds5xbbk698prnjswp4v27v6fa233754g2vlgyzyykx2wfx066d")))

