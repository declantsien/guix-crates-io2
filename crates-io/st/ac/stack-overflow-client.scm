(define-module (crates-io st ac stack-overflow-client) #:use-module (crates-io))

(define-public crate-stack-overflow-client-0.1.0 (c (n "stack-overflow-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01z0mq398r8jah7vgzdr7bwkx7xy1r90l5ks1vjyad2jfi0smag0")))

(define-public crate-stack-overflow-client-0.1.1 (c (n "stack-overflow-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0035gqb0mwfjyx9wd4ww48k0s3pibkj6fr8qc1xkl7anzgd08kpw")))

