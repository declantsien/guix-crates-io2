(define-module (crates-io st ac stack_test_randomx) #:use-module (crates-io))

(define-public crate-stack_test_randomx-0.1.1 (c (n "stack_test_randomx") (v "0.1.1") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "cmake") (r "^0.1.40") (d #t) (k 1)) (d (n "filetime") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1gdqywb4rhq4151xhf1d60i3r2cz0kydzfnfiaa53s4q67rybygp")))

