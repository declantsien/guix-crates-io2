(define-module (crates-io st ac stacked) #:use-module (crates-io))

(define-public crate-stacked-0.1.0 (c (n "stacked") (v "0.1.0") (d (list (d (n "kerr") (r "^0.1") (d #t) (k 0)))) (h "0s8133w2rgmfnqjwd8ck9csij1dx8jxv0gnhlh6anfch6v5miblx")))

(define-public crate-stacked-0.1.1 (c (n "stacked") (v "0.1.1") (d (list (d (n "kerr") (r "^0.1") (d #t) (k 0)))) (h "1xmrmwbv1j3nb6wq27wccii6yab1wzpg0zbjayxbyjn0grlwhfdi")))

(define-public crate-stacked-0.1.2 (c (n "stacked") (v "0.1.2") (d (list (d (n "kerr") (r "^0.1") (d #t) (k 0)))) (h "1bkwrggdsvczm97zrmb7j43grgrxbgi5cwz05nb9d474sgzs9r84")))

(define-public crate-stacked-0.1.3 (c (n "stacked") (v "0.1.3") (d (list (d (n "kerr") (r "^0.1") (d #t) (k 0)))) (h "1lmj5435dr5l0srnnd5ndmpkpvnq7syk6qwzp2p8c96f76c2p82d")))

