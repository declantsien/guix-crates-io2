(define-module (crates-io st ac stacktrace) #:use-module (crates-io))

(define-public crate-stacktrace-0.1.0 (c (n "stacktrace") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "15j7rp65hxrmrwg1q4lcb706d6ivn5c4llvc9cgq774ihw1sl37v")))

(define-public crate-stacktrace-0.1.1 (c (n "stacktrace") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "1skl9jyw4a9dpkirmd2r6i566ccfaqyx5v4146jy14sil20qpijj")))

(define-public crate-stacktrace-0.1.2 (c (n "stacktrace") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "036j3g9j02h5z0kfll3xhlsdawhl11ynnh6a354yay4xjlbilynj")))

(define-public crate-stacktrace-0.1.3 (c (n "stacktrace") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.1.8") (d #t) (k 0)))) (h "10kmyhcd4z4g4hmi7ll4p30q0k8sg4zwm11larlqdgrv3a455yq0")))

(define-public crate-stacktrace-0.2.0 (c (n "stacktrace") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)))) (h "04d4iwvijx6gmpwl20g09fkbv0j9mlgrn8m7726bq3iassb0qkni")))

(define-public crate-stacktrace-0.2.1 (c (n "stacktrace") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.2") (d #t) (k 0)))) (h "1s27isfnzrd4kbd2w07sssyzx74p36kfqkg88js2b4s255wzwq36")))

