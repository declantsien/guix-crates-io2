(define-module (crates-io st ac stackfmt) #:use-module (crates-io))

(define-public crate-stackfmt-0.1.0 (c (n "stackfmt") (v "0.1.0") (h "0rjkxfpgg84hi10ikpcjasp8kl0k0axzgdvsb2d6d2zbww1pa1v3")))

(define-public crate-stackfmt-0.1.1 (c (n "stackfmt") (v "0.1.1") (h "13ddgzv9xrpqf9fj38qbqr03nj2cfcghj9hpr582k6vrkd1aafxz")))

(define-public crate-stackfmt-0.1.2 (c (n "stackfmt") (v "0.1.2") (h "05ab2pdafyr19yrlx0zlyr8xkayzkn3aj49h754ajsvd05g7fpj4")))

