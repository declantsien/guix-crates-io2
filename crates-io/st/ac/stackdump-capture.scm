(define-module (crates-io st ac stackdump-capture) #:use-module (crates-io))

(define-public crate-stackdump-capture-0.1.0 (c (n "stackdump-capture") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 2)) (d (n "stackdump-core") (r "^0.1.0") (k 0)))) (h "1w3637v892jpbc2h38z4isqgc4pb98b88abr0rqa315wpxjh87g5") (r "1.59")))

(define-public crate-stackdump-capture-0.1.1 (c (n "stackdump-capture") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 2)) (d (n "stackdump-core") (r "^0.1.1") (k 0)))) (h "1m7n0sn0v4xiky68l59yqgiw7dz6px8vm7cz22pvbqx02v45j9rl") (r "1.59")))

(define-public crate-stackdump-capture-0.1.2 (c (n "stackdump-capture") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 2)) (d (n "stackdump-core") (r "^0.1.2") (k 0)))) (h "14wmnxf85xckc5h9qg81dqk71zg222qmsxqhzwlccfz1cwzixj94") (r "1.59")))

(define-public crate-stackdump-capture-0.1.3 (c (n "stackdump-capture") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 2)) (d (n "stackdump-core") (r "^0.1.3") (k 0)))) (h "10bh6x41341mfk9mmi438nn9ar0z0wl6awc6si771fhbwwbyscgs") (r "1.59")))

(define-public crate-stackdump-capture-0.2.0 (c (n "stackdump-capture") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 2)) (d (n "stackdump-core") (r "^0.1.3") (k 0)))) (h "0l2avwy4hsma03x86p3vnh03547w5lilr902ri50ws3rpi53nxl8") (r "1.59")))

(define-public crate-stackdump-capture-0.3.0 (c (n "stackdump-capture") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 2)) (d (n "stackdump-core") (r "^0.2.0") (k 0)))) (h "089jvgrifk1lbbr9c2gaaarl1ph62jvvkq0l4l3c21zxsxr870ss") (r "1.59")))

(define-public crate-stackdump-capture-0.4.0 (c (n "stackdump-capture") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "stackdump-core") (r "^0.3.0") (k 0)))) (h "1vkbyqd0pazbqsdq6kxka1p6hycxr5cx0776l85d2x6jsb7ygbyj") (r "1.59")))

(define-public crate-stackdump-capture-0.5.0 (c (n "stackdump-capture") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "stackdump-core") (r "^0.4.0") (k 0)))) (h "106j03rclql4zcwddlxy83vyzlv8905bmj9hkzviswpg2q6say6d") (r "1.59")))

