(define-module (crates-io st ac stackvec) #:use-module (crates-io))

(define-public crate-stackvec-0.0.1 (c (n "stackvec") (v "0.0.1") (h "1kqvp8frb7gzrrrlbi6baknl0l9i0qvzia2xdqg6qbnd07kmjn1h")))

(define-public crate-stackvec-0.0.2 (c (n "stackvec") (v "0.0.2") (h "1hiw4bskbpvnizyw9psdzgsfjivf1g5cy1r9njbb46w4d983zrys") (f (quote (("nightly"))))))

(define-public crate-stackvec-0.1.0 (c (n "stackvec") (v "0.1.0") (h "195pj5z6xwal1nmdyshyzvf2v6ij3i4387rjwb3dr8aq1rvm2pym") (f (quote (("nightly") ("default" "a_thousand_array_impls") ("a_thousand_array_impls"))))))

(define-public crate-stackvec-0.1.1 (c (n "stackvec") (v "0.1.1") (h "05aqfzfxmsn35syq0pw3clqkw09xzjflhm4wy6bfavfambma45lm") (f (quote (("nightly") ("default" "a_thousand_array_impls") ("a_thousand_array_impls"))))))

(define-public crate-stackvec-0.2.1 (c (n "stackvec") (v "0.2.1") (h "05ic2lhhsydm8kqkc0xi5047kg3wwbbjxq3jfig5j7yzvbd3g0x2") (f (quote (("nightly") ("default" "a_thousand_array_impls") ("a_thousand_array_impls"))))))

