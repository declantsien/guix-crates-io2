(define-module (crates-io st ac stackpin) #:use-module (crates-io))

(define-public crate-stackpin-0.0.1 (c (n "stackpin") (v "0.0.1") (h "1ca9ibk0hkjflli9nrjmhahwrydr5dm5hr7l6q3y5pwby40wfnay")))

(define-public crate-stackpin-0.0.2 (c (n "stackpin") (v "0.0.2") (h "0dy9mld2n5rmkcaxqyjlirg42xpifwh2rwvffn75wiqmwskfklsb")))

