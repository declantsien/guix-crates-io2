(define-module (crates-io st ac stackbt_automata_impl) #:use-module (crates-io))

(define-public crate-stackbt_automata_impl-0.1.0 (c (n "stackbt_automata_impl") (v "0.1.0") (h "02w5pwappkcc51wxfhfckg020s0p3iwsvvlbzdid8k6xw2apqqyp") (f (quote (("unsized_locals") ("nightly" "unsized_locals") ("default"))))))

(define-public crate-stackbt_automata_impl-0.1.1 (c (n "stackbt_automata_impl") (v "0.1.1") (h "0avarkgbskr1zz2nirk0k3p2n3rjz5w49vrmphk0fvpj24kkh64v") (f (quote (("unsized_locals") ("nightly" "unsized_locals") ("default"))))))

(define-public crate-stackbt_automata_impl-0.1.2 (c (n "stackbt_automata_impl") (v "0.1.2") (h "1sk4qmqxyid85s9s7kn9ijmq62ayxyqmk8c6zc9c894y1c564z4d") (f (quote (("unsized_locals") ("nightly" "unsized_locals") ("default"))))))

