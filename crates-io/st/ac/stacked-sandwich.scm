(define-module (crates-io st ac stacked-sandwich) #:use-module (crates-io))

(define-public crate-stacked-sandwich-1.0.0 (c (n "stacked-sandwich") (v "1.0.0") (h "0m6xklwyby0bsv8xhfh8r9kcb8bjc1qyiym2cpsn2j4syk9kg5n3") (y #t)))

(define-public crate-stacked-sandwich-1.1.0 (c (n "stacked-sandwich") (v "1.1.0") (h "1sbh5xwx4v2c14cqzcm3kjqjidlavvibv6rnq4d5yryz331ib2zp") (y #t)))

(define-public crate-stacked-sandwich-1.3.0 (c (n "stacked-sandwich") (v "1.3.0") (h "0nnqr5v1q9azhv9pi7pn9ajm6kjj5m3zr5fg8i01aif25d0cphv8") (y #t)))

(define-public crate-stacked-sandwich-1.4.0 (c (n "stacked-sandwich") (v "1.4.0") (h "0gi18gdl7j4f2l544n5sgp9mw62l73mszcwy216d5hyhznnx1ya9")))

