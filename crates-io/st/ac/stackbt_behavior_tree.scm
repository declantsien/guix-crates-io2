(define-module (crates-io st ac stackbt_behavior_tree) #:use-module (crates-io))

(define-public crate-stackbt_behavior_tree-0.1.0 (c (n "stackbt_behavior_tree") (v "0.1.0") (d (list (d (n "stackbt_automata_impl") (r "^0.1.0") (d #t) (k 0)))) (h "1h4ijlac9mxzbiv6pc6rf5wx7bsc4m2d5s6qhz0w9f71bysa6yxc") (f (quote (("unsized_locals") ("try_trait") ("nightly" "try_trait" "unsized_locals") ("default"))))))

(define-public crate-stackbt_behavior_tree-0.1.1 (c (n "stackbt_behavior_tree") (v "0.1.1") (d (list (d (n "stackbt_automata_impl") (r "^0.1.1") (d #t) (k 0)))) (h "00zk6ya3gjvv3bssl2rfr30073khmk663vfd4a0wig0skhksrqs0") (f (quote (("unsized_locals") ("try_trait") ("nightly" "try_trait" "unsized_locals") ("default"))))))

(define-public crate-stackbt_behavior_tree-0.1.2 (c (n "stackbt_behavior_tree") (v "0.1.2") (d (list (d (n "stackbt_automata_impl") (r "^0.1.2") (d #t) (k 0)))) (h "07igvfssds1lw4xfnflzjb9rwh593dg8dfq44795gn172fvk5jcz") (f (quote (("try_trait") ("nightly" "try_trait") ("default"))))))

