(define-module (crates-io st ac stack-bitset) #:use-module (crates-io))

(define-public crate-stack-bitset-0.1.0 (c (n "stack-bitset") (v "0.1.0") (h "19aj3k7qp2y6hrf2vj8bf0m3d290py4bv3xjfrm01g2jiw3q8pdy")))

(define-public crate-stack-bitset-0.2.0 (c (n "stack-bitset") (v "0.2.0") (h "0g620n18b0vy6h3sl7z042p9ys3vbqvd4vp9m0m3zq4wysxml2l6")))

(define-public crate-stack-bitset-0.2.5 (c (n "stack-bitset") (v "0.2.5") (h "1sflp4vz9gc3k427szzxm6bsbdp9x7a74pdxh6as4dhrd9jc6h3k")))

