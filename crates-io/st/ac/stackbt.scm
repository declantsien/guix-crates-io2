(define-module (crates-io st ac stackbt) #:use-module (crates-io))

(define-public crate-stackbt-0.1.0 (c (n "stackbt") (v "0.1.0") (d (list (d (n "stackbt_automata_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "stackbt_behavior_tree") (r "^0.1.0") (d #t) (k 0)) (d (n "stackbt_macros") (r "^0.1.0") (d #t) (k 0)))) (h "03bsbsr8mlbyxw67snpdhvx5mfw7qf46528h7w0cjc59326mqdb2") (f (quote (("unsized_locals" "stackbt_automata_impl/unsized_locals" "stackbt_behavior_tree/unsized_locals") ("try_trait" "stackbt_behavior_tree/try_trait") ("nightly" "try_trait" "unsized_locals") ("default"))))))

(define-public crate-stackbt-0.1.1 (c (n "stackbt") (v "0.1.1") (d (list (d (n "stackbt_automata_impl") (r "^0.1.1") (d #t) (k 0)) (d (n "stackbt_behavior_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "stackbt_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0n3a5p5mf6aax2jngkygd8lcpwsh85hlwg60zjsjafdhfm16jp2y") (f (quote (("unsized_locals" "stackbt_automata_impl/unsized_locals" "stackbt_behavior_tree/unsized_locals") ("try_trait" "stackbt_behavior_tree/try_trait") ("nightly" "try_trait" "unsized_locals") ("default"))))))

(define-public crate-stackbt-0.1.2 (c (n "stackbt") (v "0.1.2") (d (list (d (n "stackbt_automata_impl") (r "^0.1.2") (d #t) (k 0)) (d (n "stackbt_behavior_tree") (r "^0.1.2") (d #t) (k 0)) (d (n "stackbt_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1snbcw2yjbi1sc40x1hh02k9i80m4fs2n9pgzf8xghf9812n6zlf") (f (quote (("try_trait" "stackbt_behavior_tree/try_trait") ("nightly" "try_trait") ("default"))))))

