(define-module (crates-io st ac stacking_heaps) #:use-module (crates-io))

(define-public crate-stacking_heaps-0.1.0 (c (n "stacking_heaps") (v "0.1.0") (h "19zmlxzd7bxim6zss9k14cjvin6xq5ayfcy316x0fvb0p58hnb59") (y #t)))

(define-public crate-stacking_heaps-0.1.1 (c (n "stacking_heaps") (v "0.1.1") (h "0ybx39vzvfmpvp9q9z72b6nk1vrbjdimv6hc7jm7fvajjkryrqgg") (y #t)))

(define-public crate-stacking_heaps-0.1.2 (c (n "stacking_heaps") (v "0.1.2") (h "0xb7074p4dakb5cnih7p4ijqaby9gia8b49w4kqhhkxfvh0f55fy") (y #t)))

(define-public crate-stacking_heaps-0.1.3 (c (n "stacking_heaps") (v "0.1.3") (h "19k0lm9agqxgjgfkkvqg503iqn81yp45jxnfhpz3mqn5b4spzcy0") (y #t)))

