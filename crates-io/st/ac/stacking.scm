(define-module (crates-io st ac stacking) #:use-module (crates-io))

(define-public crate-stacking-0.1.0 (c (n "stacking") (v "0.1.0") (h "0r9dj9jxxx45jnl0qaigmjz1sq3nj01naaid4d1wb12al33wpj4s")))

(define-public crate-stacking-0.1.1 (c (n "stacking") (v "0.1.1") (h "0ciyrlgrnhaalfpwva2fr9dbxnjsbhdiz6j0fc5lxi0dwbl1x701")))

(define-public crate-stacking-0.2.1 (c (n "stacking") (v "0.2.1") (h "1c50xsmr8s7x6zj5x9gnwh2nkkmn7sbj0g9j8nbk81whbqwrqap3")))

(define-public crate-stacking-0.2.2 (c (n "stacking") (v "0.2.2") (h "0l3hz91qnpqs3wnjikb26d92n4zw1p3zjzmc6gph0ix2rfc594hj")))

(define-public crate-stacking-0.2.3 (c (n "stacking") (v "0.2.3") (h "0rp703dl22xzsrmlryj1sx3flcl9ralkmv72z34pfp268ksq7aba")))

(define-public crate-stacking-0.2.4 (c (n "stacking") (v "0.2.4") (h "005k5fb6dcxpi9dws8lpbvsfap3m3qc1slr54px6qjp53sai9wx9")))

(define-public crate-stacking-0.2.5 (c (n "stacking") (v "0.2.5") (h "1rwvpj7hhvgxywv2asmpa2va3gv2kvcjs8j9gd7hmj3rm58aic1n")))

