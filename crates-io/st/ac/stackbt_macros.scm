(define-module (crates-io st ac stackbt_macros) #:use-module (crates-io))

(define-public crate-stackbt_macros-0.1.0 (c (n "stackbt_macros") (v "0.1.0") (h "1z2lhdcl6syxbaxa4hsvh81m3dzr36nmswcl3g1l4xblmjxg3xbn")))

(define-public crate-stackbt_macros-0.1.1 (c (n "stackbt_macros") (v "0.1.1") (h "16iwga6ggbz1kiyb8szh07x67z61ipgy16agvra606kn6l81wac7")))

(define-public crate-stackbt_macros-0.1.2 (c (n "stackbt_macros") (v "0.1.2") (h "03qrmknv6pjs8ln0xq8saalqxc48lpbk3z3vdg43jqpkzcvwfz1p")))

