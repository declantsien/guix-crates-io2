(define-module (crates-io st ac stackedconfig) #:use-module (crates-io))

(define-public crate-stackedconfig-0.1.0 (c (n "stackedconfig") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1np76c4sqzzhajswq27acgv7zsixxqyj8nknz46has5iz156i6lw")))

(define-public crate-stackedconfig-0.1.1 (c (n "stackedconfig") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04r4p0cbd01yx8kfw8z9dqhva8fahcsn5n22zzwnblff7ll4xvn4")))

(define-public crate-stackedconfig-0.1.2 (c (n "stackedconfig") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01knrq7iv65dcf8s9xpcvj59pahcx2xg41r2j5z3f3rklsszdpki")))

