(define-module (crates-io st ac stack-map) #:use-module (crates-io))

(define-public crate-stack-map-0.1.0 (c (n "stack-map") (v "0.1.0") (h "055kdnwy4i4h6lij94rbzlrj13yqpmnq5yd9rkfk38pkb7qq4d5p")))

(define-public crate-stack-map-0.1.1 (c (n "stack-map") (v "0.1.1") (h "0miwipap49dfi5m34vv87j18f1p9g0n8f9yldrr5wa70a0ilz0gy")))

(define-public crate-stack-map-0.1.2 (c (n "stack-map") (v "0.1.2") (h "1n7m55h10hjl3pdj3jg1cd6ddf17f6xdnlik6n8mvypknk1jir88")))

(define-public crate-stack-map-0.1.3 (c (n "stack-map") (v "0.1.3") (h "1ja5dj6fw28yha2s9vl6g2c19gb4k3vgb111wv0p89x95ggxj4h5")))

(define-public crate-stack-map-0.1.4 (c (n "stack-map") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "02hvdj1i547ml4rlz6ikw7wbrhqn1azwx69a7z2za8fnfcbiv65s") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-stack-map-1.0.1 (c (n "stack-map") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "0w0qyk48barw2f09m7vrlsds0jx3sfn6j2v6j57426gl3ravn09j") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-stack-map-1.0.2 (c (n "stack-map") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "1mbzyx2hxvfbnc3vra3a0xdlsrq6lsfww60vm5prx1jhi9ybnhzw") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-stack-map-1.0.3 (c (n "stack-map") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "1wl8nmckrh41lsdqvxlc1vxx8hc3q0byqv3rmnq09xh4pf77mj8d") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-stack-map-1.0.4 (c (n "stack-map") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "0rp3pdigmi1mrh4v7kvvbzdi9s2wczy2lzg3mhlprlsgw3z40f5z") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-stack-map-1.0.5 (c (n "stack-map") (v "1.0.5") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "0z7kd1lc4q2wknrkg4j2qa7baxv66nr3sq133vcsl3z6zqv6v7dl") (s 2) (e (quote (("serde" "dep:serde"))))))

