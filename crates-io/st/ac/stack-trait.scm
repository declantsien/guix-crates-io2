(define-module (crates-io st ac stack-trait) #:use-module (crates-io))

(define-public crate-stack-trait-0.1.0 (c (n "stack-trait") (v "0.1.0") (h "1fhvjfw0alml1dl0k9sdhyanvp9n4zj3zhkia734lhx68ha8lwrc")))

(define-public crate-stack-trait-0.2.0 (c (n "stack-trait") (v "0.2.0") (h "02h3psq0brm7v86y9npgwwgppqr50rqkiwnjygckw7n5gisi8fbj")))

(define-public crate-stack-trait-0.3.0 (c (n "stack-trait") (v "0.3.0") (h "1cskpdax1ki8l4kzh71ay0j01l4gnl0h2j465w5b2v595f7ayx4w")))

