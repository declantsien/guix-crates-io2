(define-module (crates-io st ac stacks-codec) #:use-module (crates-io))

(define-public crate-stacks-codec-2.4.1 (c (n "stacks-codec") (v "2.4.1") (d (list (d (n "clarity-vm") (r "^2.3.0") (f (quote ("canonical" "developer-mode" "log"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wsts") (r "^8.1.0") (k 0)))) (h "06zp2mr4xhgs3rgrdc1kchy3fx95981ilbq4amzpdpprbf2r2mwx")))

