(define-module (crates-io st ac stack_test_progpow) #:use-module (crates-io))

(define-public crate-stack_test_progpow-0.1.0 (c (n "stack_test_progpow") (v "0.1.0") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "cmake") (r "^0.1.40") (d #t) (k 1)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "stack_test_progpow_cpu") (r "^0.1.0") (d #t) (k 0)) (d (n "stack_test_progpow_gpu") (r "^0.1.0") (f (quote ("cuda" "opencl"))) (o #t) (d #t) (k 0)))) (h "0b3598z1wpvylaz7iaild3wjj2pcwyxqxj5d50gqw9vpgjb152z1")))

