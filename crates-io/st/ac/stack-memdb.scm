(define-module (crates-io st ac stack-memdb) #:use-module (crates-io))

(define-public crate-stack-memdb-0.2.0 (c (n "stack-memdb") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "wasmium-random") (r "^1.0.0") (d #t) (k 2)))) (h "0lc3n05nv4i2d3hdgl7bs9yvc8j4hb2prdffk2w81bdwr2zkin6l") (f (quote (("heap_available")))) (y #t)))

(define-public crate-stack-memdb-0.3.0 (c (n "stack-memdb") (v "0.3.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "wasmium-random") (r "^1.0.0") (d #t) (k 2)))) (h "121i9j1mwsplb3r1hszj17dylha0l5q4yar7dp233f0gzmnyh70w") (f (quote (("satoshis_ghost" "bs58") ("hex_debug" "hex")))) (y #t)))

