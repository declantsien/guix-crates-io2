(define-module (crates-io st ac stack_replace) #:use-module (crates-io))

(define-public crate-stack_replace-0.1.0 (c (n "stack_replace") (v "0.1.0") (h "0xzk48kypvw1ls1g1xplvni7a2s1ln31kvhp0n466iivyqdr2kj6")))

(define-public crate-stack_replace-0.1.1 (c (n "stack_replace") (v "0.1.1") (h "1s3arkxzzbdc0vmj4q05vgvcjrphjl8f0cm3w0c4blf7b9ny5qhf")))

