(define-module (crates-io st ac stack-croaring-sys) #:use-module (crates-io))

(define-public crate-stack-croaring-sys-0.3.10 (c (n "stack-croaring-sys") (v "0.3.10") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "05n19q6zffvj0bzp63ggfyp2fqh5ymg9rw0a7b0prs73lp05kz5x")))

