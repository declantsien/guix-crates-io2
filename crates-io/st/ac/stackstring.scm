(define-module (crates-io st ac stackstring) #:use-module (crates-io))

(define-public crate-stackstring-0.0.0-Reserve (c (n "stackstring") (v "0.0.0-Reserve") (h "1pax374arlfml0dcr512dd7vv6blasjmxf2rsgy1kb0l4mk3xayh")))

(define-public crate-stackstring-0.0.1 (c (n "stackstring") (v "0.0.1") (h "05c836c0bbca1a4q8xfff5w8dfylvqz1lnns60wnslp39p1ncmlk")))

(define-public crate-stackstring-0.0.2 (c (n "stackstring") (v "0.0.2") (d (list (d (n "rkyv") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0jiv8gawwzs9n0kazm8j5ndjswhm0vadcvh614vq9dsw9hwj9w30") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.0.3 (c (n "stackstring") (v "0.0.3") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1vy919p8f7pxja8719i4gv8wanx39ikk5j7ijw0ay5jxfz4v4j7h") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.0 (c (n "stackstring") (v "0.1.0") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1lk1ai8vgzy04q7r3j4gzgba4aarpjflj16yfg22qy30plshygrv") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.1 (c (n "stackstring") (v "0.1.1") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0zk7ydl2d64i6qm623zpwiyc2qbg2imnf7zf5ymd5a0d2r8mpr4c") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.2 (c (n "stackstring") (v "0.1.2") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0p6qdc4iz7l0yfnv0sd3x6lnh2q596gmn4rj4jv8qk5gz1hqkd67") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.3 (c (n "stackstring") (v "0.1.3") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1ng4i5h1466qpgsw0sgvicifk4lb75pj8s9yn081jm2ggx7v3nw2") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.4 (c (n "stackstring") (v "0.1.4") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1pa7rqdw4pggc2s6lzm0cmywx50wcn5iwhwjvrh62cnpx53wir4r") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.5 (c (n "stackstring") (v "0.1.5") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1nbqc51b44vh9875bdq1jd35gflmrrkkb7h4iysalss872wwh7ys") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.6 (c (n "stackstring") (v "0.1.6") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0n7i5la3jdpxj6szry0s12q2s0i008dyp3v6q6lh9zi7x4ssjgwd") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.7 (c (n "stackstring") (v "0.1.7") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0kw9ic8p6y22i454223lhff5hcw2hprgvvjkcbwdmsds4yn679l3") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.8 (c (n "stackstring") (v "0.1.8") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ikb6i0zsqy10slnakkcz6by3h6nc1339a70vka1q7p77hzsckdc") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.1.9 (c (n "stackstring") (v "0.1.9") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pq1p9y5ps1mpygljy06rn38ywgcp5vzyd72b5rw8glbd5vf2vv2") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.2.0 (c (n "stackstring") (v "0.2.0") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1pmk0v72f7lgd856qjnqhcwxsaz7y29nsz9yhrbrwvcvl4jyi204") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.3.0 (c (n "stackstring") (v "0.3.0") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0qjypjk4699wz29gxdsbhxkzgl9qzwrs365qjd02pw266d74xi30") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

(define-public crate-stackstring-0.3.1 (c (n "stackstring") (v "0.3.1") (d (list (d (n "rkyv") (r "^0.7.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1h706dbgzganw8vs22mxjsa4xlqp6sf3qz1r6y0w7r176jdb1r4x") (f (quote (("serde-derive" "serde") ("rkyv-derive" "rkyv"))))))

