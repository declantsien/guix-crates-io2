(define-module (crates-io st ac stacked_bar_chart) #:use-module (crates-io))

(define-public crate-stacked_bar_chart-1.0.1 (c (n "stacked_bar_chart") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "easy-error") (r "^1.0.0") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "^0.17.0") (d #t) (k 0)) (d (n "yansi") (r "^1.0.1") (d #t) (k 0)))) (h "1vbbc70pgdkskf5km4893xswjc6mvy6i9v5shwkq92vj82m8c5ax")))

