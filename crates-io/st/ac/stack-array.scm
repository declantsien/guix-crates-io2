(define-module (crates-io st ac stack-array) #:use-module (crates-io))

(define-public crate-stack-array-0.1.0 (c (n "stack-array") (v "0.1.0") (h "1jc0x45s4djs84a98jicxr78fbnj2jf83v1d80lg37lbcsagga07")))

(define-public crate-stack-array-0.1.1 (c (n "stack-array") (v "0.1.1") (h "1ryq25vdna4aq6x1b3mgvmgnaq31x1r8a03nhh5gghd76zm84hny")))

(define-public crate-stack-array-0.1.2 (c (n "stack-array") (v "0.1.2") (h "1wwvqp478hppdcdb4i2klz8i4p75p9jb9vx0c7c3jvh2lasiikp5")))

(define-public crate-stack-array-0.1.3 (c (n "stack-array") (v "0.1.3") (h "1skk9jq9hzydd2fl3086x5b94psn205incxgaypag0dja2g7j1n3")))

(define-public crate-stack-array-0.1.5 (c (n "stack-array") (v "0.1.5") (h "1k659aswiv172bs0p6iwrd55d20hrjddg1xagl3w3vzbyq9bgv37")))

(define-public crate-stack-array-0.2.0 (c (n "stack-array") (v "0.2.0") (h "0g4hrzs1gw9mqv5q2b0xgjqrj4lyqprx7pdqgna1idlj70nxvih8")))

(define-public crate-stack-array-0.2.1 (c (n "stack-array") (v "0.2.1") (h "105v3c7q099nlc1n79b0yr12ynq4672xp5vvlaf9a4hsd4y6w0y3")))

(define-public crate-stack-array-0.2.2 (c (n "stack-array") (v "0.2.2") (h "11rfy87dbh3bsfb33zknijls92m8d85sz5s0g63rra641lgg099i")))

(define-public crate-stack-array-0.2.3 (c (n "stack-array") (v "0.2.3") (h "0xxm2qgxgyycyv74862cldh905snn1sf4rbvfndas256cfy6xa59")))

(define-public crate-stack-array-0.2.4 (c (n "stack-array") (v "0.2.4") (h "0knncmcdx66lj74h8r10qmx255iadq3nm5d22c09l4qmcp50y0dp")))

(define-public crate-stack-array-0.3.0 (c (n "stack-array") (v "0.3.0") (h "1bsidjzs1c3is4jrm1cp04x69srpfdl50dldskd0gmvbjgmg5gm3")))

(define-public crate-stack-array-0.4.0 (c (n "stack-array") (v "0.4.0") (h "190zw1w031x54jlq6n4kyk5mqdxvpn6ns13qdkf6z3j98abmr8lr")))

(define-public crate-stack-array-0.4.1 (c (n "stack-array") (v "0.4.1") (h "1dykdxpxijqc1xvl7a723sga9qkj2bpx42zx6v0xsh8falmy2ark")))

