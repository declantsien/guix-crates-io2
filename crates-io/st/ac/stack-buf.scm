(define-module (crates-io st ac stack-buf) #:use-module (crates-io))

(define-public crate-stack-buf-0.1.0 (c (n "stack-buf") (v "0.1.0") (h "06hw785aml7kvxhq5jwf4lw84kq81madpqdl92ggl8lb05vpyrxk")))

(define-public crate-stack-buf-0.1.1 (c (n "stack-buf") (v "0.1.1") (h "138346zlsyh62cpl76vqrp04hxhjvdfajs2cz1q748lxh0ckw0a8")))

(define-public crate-stack-buf-0.1.2 (c (n "stack-buf") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1d7ywdh5vinfx6wzrfvjj6bna5dkjv4hsv9lm1w5x58clbrixq72")))

(define-public crate-stack-buf-0.1.3 (c (n "stack-buf") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "101214r5znmx5sn50c1x0q2bknd36b9acykdwaywfkywm18z6rww")))

(define-public crate-stack-buf-0.1.4 (c (n "stack-buf") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "057nbabphlbpx62mdx1hwvgv6s9rh85hv9w26ildmik792rdn4ll") (f (quote (("str") ("std") ("default" "std"))))))

(define-public crate-stack-buf-0.1.5 (c (n "stack-buf") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w7b7w239875a6f7bcmj03clkwhc1d8bnb4dvbq67j0lh9i5w2n7") (f (quote (("str") ("std") ("default" "std")))) (y #t)))

(define-public crate-stack-buf-0.1.6 (c (n "stack-buf") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w1kbzaabzgcw503yj2kipxrkjxw2i4n1gfkpypnyzr8rd4nnf77") (f (quote (("str") ("std") ("default" "std"))))))

