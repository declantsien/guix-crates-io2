(define-module (crates-io st ac stack_test_progpow_gpu) #:use-module (crates-io))

(define-public crate-stack_test_progpow_gpu-0.1.0 (c (n "stack_test_progpow_gpu") (v "0.1.0") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.40") (d #t) (k 1)) (d (n "filetime") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1n3nkwjyv1bp607lyfyv13pjs2x17sawym4w13b9cxw1624cr6gw") (f (quote (("opencl") ("default") ("cuda"))))))

