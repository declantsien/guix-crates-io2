(define-module (crates-io st ac stack_test_epic_wallet_config) #:use-module (crates-io))

(define-public crate-stack_test_epic_wallet_config-3.0.0 (c (n "stack_test_epic_wallet_config") (v "3.0.0") (d (list (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "stack_test_epic_wallet_util") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "041anp08jar05lxl26avvgpyl7gami0f2pyyzh67lj3mdci8ir2a")))

(define-public crate-stack_test_epic_wallet_config-3.0.1 (c (n "stack_test_epic_wallet_config") (v "3.0.1") (d (list (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "stack_test_epic_wallet_util") (r "^3.0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1h8hjdy316zzs5g28h390fqvlsa9pz71lfamn9sbi6g4jdwjxgww")))

(define-public crate-stack_test_epic_wallet_config-3.0.2 (c (n "stack_test_epic_wallet_config") (v "3.0.2") (d (list (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "stack_test_epic_wallet_util") (r "^3.0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1aiy05q07yvy8njkhxfvr2r6hnhzhm8p43yc8wxg984n181f47b5")))

