(define-module (crates-io st ac stackpng) #:use-module (crates-io))

(define-public crate-stackpng-0.1.0 (c (n "stackpng") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "12ja1glam1lbdm37j3sp6zkwcbs6i92q0hg8hm60cqgy5c5f2kww")))

(define-public crate-stackpng-0.1.1 (c (n "stackpng") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1b9z51q8dawd5yxh4g66qbbdv7qjm0kgihj62ci8rrcydag866fk")))

