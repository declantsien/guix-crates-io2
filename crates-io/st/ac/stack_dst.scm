(define-module (crates-io st ac stack_dst) #:use-module (crates-io))

(define-public crate-stack_dst-0.0.1 (c (n "stack_dst") (v "0.0.1") (h "0nywpcdhjdh4vg4hxzgn1d8vfbs86l92xzpdrdvwgy13h1yczafb")))

(define-public crate-stack_dst-0.1.0 (c (n "stack_dst") (v "0.1.0") (h "1bhg3hvfvjm29ypi2374qa8575irkr67kk6xfdzjfwlxqrn9v4wn")))

(define-public crate-stack_dst-0.1.1 (c (n "stack_dst") (v "0.1.1") (h "1hr5y13a3pg2r8ia6sicb3lylb2yf44xxsm24nqqyvxddkx17jrv")))

(define-public crate-stack_dst-0.2.0 (c (n "stack_dst") (v "0.2.0") (h "1srf133jjwg90aa34gjdx5vmkiknr0v6a19kpwxv0zdw84dhxdai")))

(define-public crate-stack_dst-0.2.1 (c (n "stack_dst") (v "0.2.1") (h "0cpzsz6jvihi18i0a9llbqxcl3jv93z0li3g1h54bmlgamjmnpw4")))

(define-public crate-stack_dst-0.3.0 (c (n "stack_dst") (v "0.3.0") (h "1pzcgyz7v8cbyi634yf8fgdaa3pyxhsgvk5fk1xz54pzsqy2c43k")))

(define-public crate-stack_dst-0.3.1 (c (n "stack_dst") (v "0.3.1") (h "0n0n8y1psrs2z061n6nabzzmz16zy1pdqcb1abn1cysv6inxx1zf")))

(define-public crate-stack_dst-0.4.0 (c (n "stack_dst") (v "0.4.0") (h "10hx9mph1hhx5qrxp31hc27wajgrvgixax7ap1dsdalia5pxc6b9")))

(define-public crate-stack_dst-0.4.1 (c (n "stack_dst") (v "0.4.1") (h "0w8x3vqmhn8gghjdb997lpzvgwcf9dii9rpy5hfvvddijxsr3b11")))

(define-public crate-stack_dst-0.4.2 (c (n "stack_dst") (v "0.4.2") (h "06bz0rim6yszzdag37gb47c6gmp5s7hc79wv9acv1cj5srihc6vq")))

(define-public crate-stack_dst-0.4.3 (c (n "stack_dst") (v "0.4.3") (h "1nv4a68fgl2nf7qyj5b8plx52w6xgj6ppa8rvrcshf1gmbqr40ka") (f (quote (("std") ("default" "std"))))))

(define-public crate-stack_dst-0.5.0 (c (n "stack_dst") (v "0.5.0") (h "1a0wp3v1s5ifwjlnp2aknbamd11j45wnkns46rrw2812igwmschc") (f (quote (("std") ("default" "std"))))))

(define-public crate-stack_dst-0.6.0 (c (n "stack_dst") (v "0.6.0") (h "1sq7jgmnnnzn7q8a7mpl0g58xdawsanfwzn815kwajjzkbff3mc6") (f (quote (("unsize") ("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-stack_dst-0.6.1 (c (n "stack_dst") (v "0.6.1") (h "1nkhrk2dkcf242spnmmd5yw1p6d52l19ylq8a18hnsdjg0qjv8rx") (f (quote (("unsize") ("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-stack_dst-0.7.0 (c (n "stack_dst") (v "0.7.0") (h "0by5ykl70g7ll1y57k28p0wv38r8a9v69k16n1zfjhq0j6j2vffz") (f (quote (("unsize") ("std") ("full_const_generics") ("default" "std" "alloc" "const_generics") ("const_generics") ("alloc"))))))

(define-public crate-stack_dst-0.7.1 (c (n "stack_dst") (v "0.7.1") (h "0fq4c6lr8c9ba7zn5i3sh0yp61dv25xwyv4dk9flgznk93jwcs3a") (f (quote (("unsize") ("std") ("default" "std" "alloc" "const_generics") ("const_generics") ("alloc"))))))

(define-public crate-stack_dst-0.7.2 (c (n "stack_dst") (v "0.7.2") (h "0vi5k8s0vbjnsxf9w10ppspsklrsfbgl0zvm2cgj0wary2x89y8k") (f (quote (("unsize") ("std") ("default" "std" "alloc" "const_generics") ("const_generics") ("alloc"))))))

(define-public crate-stack_dst-0.8.1 (c (n "stack_dst") (v "0.8.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1ls3r18jw1rpy87zpav0y7p3v1gjpxxkaypv637j9m0m0b16fk2d") (f (quote (("unsize") ("default" "alloc" "const_generics") ("const_generics") ("alloc"))))))

