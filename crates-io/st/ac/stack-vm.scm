(define-module (crates-io st ac stack-vm) #:use-module (crates-io))

(define-public crate-stack-vm-0.1.0 (c (n "stack-vm") (v "0.1.0") (h "05k39d99g1wvb8zmddv3mb5z0arq00qbqq7291ihmjh8kszxy8nn")))

(define-public crate-stack-vm-0.2.0 (c (n "stack-vm") (v "0.2.0") (d (list (d (n "rmp") (r "^0.8") (d #t) (k 0)))) (h "0smgrnfckm2pk8hgr06z6r9ldyizijm8iz6lqx34ij74jc2kd38a")))

(define-public crate-stack-vm-0.3.0 (c (n "stack-vm") (v "0.3.0") (d (list (d (n "rmp") (r "^0.8") (d #t) (k 0)))) (h "1d3mvp3xqqyizywf3bphlg99hh0qxi7gjqaqvq66fpkj1rdqrdnf")))

(define-public crate-stack-vm-1.0.1 (c (n "stack-vm") (v "1.0.1") (d (list (d (n "rmp") (r "^0.8") (d #t) (k 0)))) (h "0gg1mpb7psqdzwyljnpn5vsf4n2z7milmii185750wbs9pls0mya")))

