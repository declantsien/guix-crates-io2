(define-module (crates-io st ac stacko) #:use-module (crates-io))

(define-public crate-stacko-0.1.0 (c (n "stacko") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0y5rl5y9kb2xszyvvrzchabc1sxgvs9ahh6qygxyy0q3ddyspbzz") (y #t)))

(define-public crate-stacko-0.1.1 (c (n "stacko") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "168lal17iyacfnxz43xvkk0v7n4xc4pvzi0szs610yjcdxjcilmc") (y #t)))

