(define-module (crates-io st ac stack-algebra) #:use-module (crates-io))

(define-public crate-stack-algebra-0.1.0 (c (n "stack-algebra") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "stride") (r "^0.3.0") (d #t) (k 0)) (d (n "vectrix-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1w70q02vsc6p23dx0mv5asi6qf6jjrlcaxpjknn0z4hg2bd6vf6l")))

