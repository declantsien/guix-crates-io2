(define-module (crates-io st ac stack) #:use-module (crates-io))

(define-public crate-stack-0.1.0 (c (n "stack") (v "0.1.0") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^0.0") (d #t) (k 0)))) (h "0w74k8rd5bv1yvwgm6q4bnahp5b6xmmhymjsaj6f9bd98hh3vhx7") (f (quote (("unstable"))))))

(define-public crate-stack-0.1.1 (c (n "stack") (v "0.1.1") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^0.0") (d #t) (k 0)))) (h "0p37w46f9akj26dgp24q0nf3aj481djq68h18dgva5i5dk83yrbd") (f (quote (("unstable"))))))

(define-public crate-stack-0.1.2 (c (n "stack") (v "0.1.2") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^0.0") (d #t) (k 0)))) (h "0w2jjzdic1vapyw5l3wwvbgvzs3jzypcjzzwidmwmny81m0a11y2") (f (quote (("unstable" "nodrop/no_drop_flag"))))))

(define-public crate-stack-0.1.3 (c (n "stack") (v "0.1.3") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^0.0") (d #t) (k 0)))) (h "1lsf9pfxyaxzn1zg2n7xlszq4g2z4lvx0ap5ndd5waba19qmh8l6") (f (quote (("unstable" "nodrop/no_drop_flag"))))))

(define-public crate-stack-0.2.0 (c (n "stack") (v "0.2.0") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1mg3nbn0n0l449g60i89fg5h0bnw9l5phfqyf9rbzb87qkp5g0aq") (f (quote (("unstable"))))))

(define-public crate-stack-0.3.0 (c (n "stack") (v "0.3.0") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "158phs22l11q1af3ib5ip0f5vim9yaqxlrjnhys6lpfnaqlfvwx7") (f (quote (("unstable"))))))

(define-public crate-stack-0.3.1 (c (n "stack") (v "0.3.1") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "0j5an0wsgiik9szzr0gbsf5bhcs9gzf9l5h6w12953qcwpj4sldh") (f (quote (("unstable"))))))

(define-public crate-stack-0.4.0 (c (n "stack") (v "0.4.0") (d (list (d (n "coalesce") (r "^0.1") (d #t) (k 0)))) (h "18xqxl0icfqdrw23vmnpa1ss2cxkx5am6g3hpq1hj73m1w9ywzf2") (f (quote (("unstable"))))))

