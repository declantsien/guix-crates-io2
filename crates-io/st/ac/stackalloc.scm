(define-module (crates-io st ac stackalloc) #:use-module (crates-io))

(define-public crate-stackalloc-1.0.0 (c (n "stackalloc") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0k7gvd079ixx1rzx0bq4sprgkwprla354jlqz6cmz7zqn32lvjy6")))

(define-public crate-stackalloc-1.0.1 (c (n "stackalloc") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "00pq5bql4jlbvj9wll3p5k81a242s4jd9cip6f9lr2rzqvyvmscn")))

(define-public crate-stackalloc-1.1.0 (c (n "stackalloc") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0lfyfy175i4c102n4cg90f8pzxl3s2sg3w17bm8pia40540g7467")))

(define-public crate-stackalloc-1.1.1 (c (n "stackalloc") (v "1.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "08b107nccbxrk7ygi37km7wr4al691gix1mfivf4m2pb7zfwkxd4")))

(define-public crate-stackalloc-1.1.2 (c (n "stackalloc") (v "1.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1iqb8y82kd8mz547byyvpyh5dqgw1y8qi7dvbmrw19hvfnk9hm9s")))

(define-public crate-stackalloc-1.2.0 (c (n "stackalloc") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0d7jhf3w641ag5qk9x5w514mx5jy2g934jr82hvrdn6hlr4jh8s1") (f (quote (("no_std") ("default"))))))

(define-public crate-stackalloc-1.2.1 (c (n "stackalloc") (v "1.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0y0yab02s4zic1ici3hb1xi1hfb9lg4nh9540n1zgdw4h04bgd80") (f (quote (("no_unwind_protection") ("no_std") ("default"))))))

