(define-module (crates-io st ac stackdump-capture-probe) #:use-module (crates-io))

(define-public crate-stackdump-capture-probe-0.1.0 (c (n "stackdump-capture-probe") (v "0.1.0") (d (list (d (n "probe-rs") (r "^0.12") (d #t) (k 0)) (d (n "stackdump-core") (r "^0.2.0") (d #t) (k 0)))) (h "1kkgjj8qjradxkn6xcgmbgzy5gqgig1chs7pyjr1vj7rbmx5sph8")))

(define-public crate-stackdump-capture-probe-0.2.0 (c (n "stackdump-capture-probe") (v "0.2.0") (d (list (d (n "probe-rs") (r "^0.13") (d #t) (k 0)) (d (n "stackdump-core") (r "^0.2.0") (d #t) (k 0)))) (h "06lg7qn8pwwbbs2d7w8z8ns8j5vwr105y0zyg8ydzfzzw1zkx4wp")))

(define-public crate-stackdump-capture-probe-0.3.0 (c (n "stackdump-capture-probe") (v "0.3.0") (d (list (d (n "probe-rs") (r "^0.18.0") (d #t) (k 0)) (d (n "stackdump-core") (r "^0.3.0") (d #t) (k 0)))) (h "12szhy9yqampmmflw96jfqkbf422x5x4w5h5axy8l109v4pxcrnf")))

(define-public crate-stackdump-capture-probe-0.4.0 (c (n "stackdump-capture-probe") (v "0.4.0") (d (list (d (n "probe-rs") (r "^0.20.0") (d #t) (k 0)) (d (n "stackdump-core") (r "^0.4.0") (d #t) (k 0)))) (h "0a96clf32x05pz75yf6bzzk0z7akymihqxh62xr13hs27qs4s3y2")))

