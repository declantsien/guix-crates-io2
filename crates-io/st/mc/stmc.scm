(define-module (crates-io st mc stmc) #:use-module (crates-io))

(define-public crate-stmc-0.1.0 (c (n "stmc") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qka5p4m17rc3zjc1wnyfs78jfzng7y16akpvmld0x24yxcvdzvs")))

(define-public crate-stmc-0.1.1 (c (n "stmc") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "19jrc4pdkqb6fqlnam4wh74i9scmcmj3p1k5qrkhzms6zzfd1914")))

(define-public crate-stmc-0.2.0 (c (n "stmc") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "15xlxl6fw9db8ax52z1yka4a3wsfk3fiphh4jrnhnbmcb8sppk4c")))

(define-public crate-stmc-0.2.1 (c (n "stmc") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k78byhkg84i0l5842bpym8sy896jd0q90nds1cfyplsn0va658h")))

(define-public crate-stmc-0.2.2 (c (n "stmc") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yhskpy0xb3fnf9vc864sfhfr6dvhx9bycapvicmc7q1av0l5bjq")))

(define-public crate-stmc-0.2.3 (c (n "stmc") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sna9s97p2k27rm29k3aywvh7bab1hqmjq8zm42fb3cm3pjxdiln") (f (quote (("big_endian"))))))

