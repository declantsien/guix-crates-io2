(define-module (crates-io st ir stir) #:use-module (crates-io))

(define-public crate-stir-0.0.1 (c (n "stir") (v "0.0.1") (h "02qjj0g9dfa3qa1lfdr9ca2l5vw8mscgiidpj9i26pi6b80vahzr")))

(define-public crate-stir-0.0.2 (c (n "stir") (v "0.0.2") (h "0j2dkjzddqc202lr853bnkc1w19fi8dxg6znq91wbvnkhf8rpb2g")))

