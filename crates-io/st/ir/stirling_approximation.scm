(define-module (crates-io st ir stirling_approximation) #:use-module (crates-io))

(define-public crate-stirling_approximation-0.1.0 (c (n "stirling_approximation") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.4.1") (d #t) (k 0)))) (h "1fiw9asljpjk61l0vk231yp2sj5423p6s7dgpnavwcvn604alw68")))

