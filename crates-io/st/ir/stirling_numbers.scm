(define-module (crates-io st ir stirling_numbers) #:use-module (crates-io))

(define-public crate-stirling_numbers-0.1.0 (c (n "stirling_numbers") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "09jh3kdp83x6nkjccv1qjzal978nfrb9c67xn5jnx84b21zlxkvn")))

(define-public crate-stirling_numbers-0.1.1 (c (n "stirling_numbers") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "081mikah07whysv5d594122pv2iv6idqyih7nmvs5n9lc5bcba5h")))

(define-public crate-stirling_numbers-0.1.2 (c (n "stirling_numbers") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 2)))) (h "1fmajjslh7akhj28mr2sry2a725dvs1pwsmkr7rvb6p062rdq0q4")))

(define-public crate-stirling_numbers-0.1.3 (c (n "stirling_numbers") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (d #t) (k 2)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 2)))) (h "00zfmjf66q0f3d9j7xakwqmkh7rgrx98snyvn8mi13irici83dqw")))

(define-public crate-stirling_numbers-0.1.4 (c (n "stirling_numbers") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.9") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (d #t) (k 2)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 2)))) (h "0bpihvgfxmw43jarivyvsr25l09z6lw3sf2j3q9hmlindfkhqcpr")))

(define-public crate-stirling_numbers-0.1.5 (c (n "stirling_numbers") (v "0.1.5") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.9") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (d #t) (k 2)) (d (n "vector_utils") (r "^0.1") (d #t) (k 2)))) (h "1dqxkc7jcj4ah3i6ynv4q15jqnn8f7fkz5md43jrwdm10qbn8xzh")))

(define-public crate-stirling_numbers-0.1.6 (c (n "stirling_numbers") (v "0.1.6") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.9") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (d #t) (k 2)) (d (n "vector_utils") (r "^0.1") (d #t) (k 2)))) (h "15ffbslk6zi2pq6rdd051ls9i46vg5d7sayyfzy7gb6d2n132f4v")))

(define-public crate-stirling_numbers-0.1.7 (c (n "stirling_numbers") (v "0.1.7") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-rational") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.9") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "vector_utils") (r "^0.1") (d #t) (k 2)))) (h "055880qvbr4ps4gs55642jx4gm4nif832m4pm9izw5m8sni8azsc")))

