(define-module (crates-io st #{75}# st7567s) #:use-module (crates-io))

(define-public crate-st7567s-0.1.0 (c (n "st7567s") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)))) (h "0sbbpv1z7d04z1053l5lq2w24yjham3fmyscpxg4d73450jw8rpk") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-st7567s-0.2.0 (c (n "st7567s") (v "0.2.0") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 2)))) (h "0jxnnsxhp77s9dfk7axqqz2vcbkxi4wqmih51n1nr383mnmjgz0y") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-st7567s-0.2.1 (c (n "st7567s") (v "0.2.1") (d (list (d (n "display-interface") (r "^0.4.1") (d #t) (k 0)) (d (n "display-interface-i2c") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 2)))) (h "1lhrf3i0ylm48zjm5q77mkxi19q6ph5lsizlwjcy26h53qnn3iyg") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

