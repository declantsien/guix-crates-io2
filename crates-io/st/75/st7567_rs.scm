(define-module (crates-io st #{75}# st7567_rs) #:use-module (crates-io))

(define-public crate-st7567_rs-0.1.0 (c (n "st7567_rs") (v "0.1.0") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0) (p "embedded-hal")))) (h "0m523z5bgn1y9vf18i6xipdb8ivpdjn4d4ajpiwfzlbgd1syzghl")))

(define-public crate-st7567_rs-0.1.1 (c (n "st7567_rs") (v "0.1.1") (d (list (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0) (p "embedded-hal")))) (h "0shv47yi358w583a1bc318jdych5psj9pqnbas8a1d38a6bmn37a")))

