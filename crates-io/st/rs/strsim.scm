(define-module (crates-io st rs strsim) #:use-module (crates-io))

(define-public crate-strsim-0.1.0 (c (n "strsim") (v "0.1.0") (h "1728jqm5gi65m6g30ms33zlbs1r0bfhbgpjjanq9l0i0x7lqdr1j")))

(define-public crate-strsim-0.1.1 (c (n "strsim") (v "0.1.1") (h "15bvbxqip56l8g5vr14clmixnr09r5cmx7m9gj5zy7zgl3qgahn1")))

(define-public crate-strsim-0.2.0 (c (n "strsim") (v "0.2.0") (h "0733bxbcrp40gf5cbkfcv1plgavakm8xar3g3kahp5qigzwj6wp8")))

(define-public crate-strsim-0.2.1 (c (n "strsim") (v "0.2.1") (h "1mrni0k41hz3lfcwwzip7k7sraqngb0pbnj616whqj27wriznz4b")))

(define-public crate-strsim-0.2.2 (c (n "strsim") (v "0.2.2") (h "1vkwdlikc1gmljq9zh7ll269j0d305swg0q9pfzlm3x2r52flqnk")))

(define-public crate-strsim-0.2.3 (c (n "strsim") (v "0.2.3") (h "1il0b0zlmdp6mjvb05m4x88sr18y16sjlk3xsy8lf6iqjxj0fy2m")))

(define-public crate-strsim-0.2.4 (c (n "strsim") (v "0.2.4") (h "0xcd92iy91d59p9cqz7s0ldayl13zsgcxzaqnhhhnfam0garhbzm")))

(define-public crate-strsim-0.2.5 (c (n "strsim") (v "0.2.5") (h "1y9fw4nzs3m7krf2v49ai076gwpsf4aa3bpdhpb0x6kgddf46xni")))

(define-public crate-strsim-0.3.0 (c (n "strsim") (v "0.3.0") (h "09dq9a22cx3myjrq4ykz8irk31jrq7mcpxbd3bnrbl546qn3mmz4")))

(define-public crate-strsim-0.4.0 (c (n "strsim") (v "0.4.0") (h "1nc941gspbswrh95vbd4xs3410dvgiwrxiqjlwhj5as90cha5mmi")))

(define-public crate-strsim-0.4.1 (c (n "strsim") (v "0.4.1") (h "11g9rdaqh6yqhb28vcj8j5zdq1xrv9ij316bqjjk8rpdbifmfpqd")))

(define-public crate-strsim-0.5.0 (c (n "strsim") (v "0.5.0") (h "0a9ib4f4lyavy4yc6ma10g2x2whd2vh9fk96y8c013m4qipjxap9")))

(define-public crate-strsim-0.5.1 (c (n "strsim") (v "0.5.1") (h "0bj4fsm1l2yqbfpspyvjf9m3m50pskapcddzm0ji9c74jbgnkh2h")))

(define-public crate-strsim-0.5.2 (c (n "strsim") (v "0.5.2") (h "0z3zzvmilfldp4xw42qbkjf901dcnbk58igrzsvivydjzd24ry37")))

(define-public crate-strsim-0.6.0 (c (n "strsim") (v "0.6.0") (h "151ngha649cyybr3j50qg331b206zrinxqz7fzw1ra8r0n0mrldl")))

(define-public crate-strsim-0.7.0 (c (n "strsim") (v "0.7.0") (h "0l7mkwvdk4vgnml67b85mczk466074aj8yf25gjrjslj4l0khkxv")))

(define-public crate-strsim-0.8.0 (c (n "strsim") (v "0.8.0") (h "0sjsm7hrvjdifz661pjxq5w4hf190hx53fra8dfvamacvff139cf")))

(define-public crate-strsim-0.9.0 (c (n "strsim") (v "0.9.0") (h "08an22v4r62aj3qinl8ydjcbdxdswyzbc8xlnjkhrww7hm3j7gm7")))

(define-public crate-strsim-0.9.1 (c (n "strsim") (v "0.9.1") (d (list (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)))) (h "0hvnnpaywxiphafrb2z2yinq8nnvc9p7pm2cplxsjfj2n5m6db1l")))

(define-public crate-strsim-0.9.2 (c (n "strsim") (v "0.9.2") (h "1xphwhf86yxxmcpvm4mikj8ls41f6nf7gqyjm98b74mfk81h6b03")))

(define-public crate-strsim-0.9.3 (c (n "strsim") (v "0.9.3") (h "0k497pv882qn3q977ckznm13vxx927g8s1swvcv68j3c1pccwik4")))

(define-public crate-strsim-0.10.0 (c (n "strsim") (v "0.10.0") (h "08s69r4rcrahwnickvi0kq49z524ci50capybln83mg6b473qivk")))

(define-public crate-strsim-0.10.1 (c (n "strsim") (v "0.10.1") (h "13f0bnp7428g6bm817ixyh9p3sig8k4v5xl3xvdpisrl8prsdg6c") (y #t)))

(define-public crate-strsim-0.11.0 (c (n "strsim") (v "0.11.0") (h "00gsdp2x1gkkxsbjxgrjyil2hsbdg49bwv8q2y1f406dwk4p7q2y")))

(define-public crate-strsim-0.11.1 (c (n "strsim") (v "0.11.1") (h "0kzvqlw8hxqb7y598w1s0hxlnmi84sg5vsipp3yg5na5d1rvba3x") (r "1.56")))

