(define-module (crates-io st rs strs) #:use-module (crates-io))

(define-public crate-strs-0.1.0 (c (n "strs") (v "0.1.0") (h "1fa3dxxmg27xm7xva3kbf4dn3b8j16mzplnblak4azq5bgs885mz")))

(define-public crate-strs-0.1.1 (c (n "strs") (v "0.1.1") (h "17v4sl192v1bfrwr3bd42fy9d901f52j035cznayms469byk8n2a")))

(define-public crate-strs-0.1.2 (c (n "strs") (v "0.1.2") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0hpiiq8ca8m8vjysplaz56jh35qyc8y1wazf3manv5g2aq7l0d2c")))

(define-public crate-strs-0.1.3 (c (n "strs") (v "0.1.3") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "16si2x2yf8583jmrl63mr46cm72ip9f4h5gli800k27k9b5gsdq1")))

