(define-module (crates-io st rs strseq) #:use-module (crates-io))

(define-public crate-strseq-0.1.0 (c (n "strseq") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0isc2qfdl5hv05qb16qa4lxndxvwwj917k5fmnni87i3pyci0w4k") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-strseq-0.1.1 (c (n "strseq") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "10xnx1l06ipcl4zlycm8rwvp5ghpbaglnm6lgv2by81cy8cjgax7") (s 2) (e (quote (("serde" "dep:serde"))))))

