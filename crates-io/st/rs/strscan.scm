(define-module (crates-io st rs strscan) #:use-module (crates-io))

(define-public crate-strscan-0.1.0 (c (n "strscan") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)))) (h "0d6d81xi829gqar9cfvwp81q71iqgkkfgkk698n2vx05q262m34b")))

(define-public crate-strscan-0.1.1 (c (n "strscan") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)))) (h "0gzppkfq9f3g50gy4f9bjrc633s6m2m2cmnyhdz8giyywjbdn41i")))

