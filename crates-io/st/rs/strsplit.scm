(define-module (crates-io st rs strsplit) #:use-module (crates-io))

(define-public crate-strsplit-0.1.0 (c (n "strsplit") (v "0.1.0") (h "0lh8rahhzs5ddzsiajxnkf8j9wnra3ha1b9kdfhpqspn554q96fh")))

(define-public crate-strsplit-0.1.1 (c (n "strsplit") (v "0.1.1") (h "1yvgkg23nq8i0baqj2kls54k9sy9sxh0bwanjlmsb673zl7fxb9g")))

