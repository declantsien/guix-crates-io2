(define-module (crates-io st pl stpl) #:use-module (crates-io))

(define-public crate-stpl-0.1.0 (c (n "stpl") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0c3ry51z6xmcgylsg23bn3lqm05kf2w7ibwi99kr94fbjxgjz44j")))

(define-public crate-stpl-0.2.0 (c (n "stpl") (v "0.2.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "03p0gg95swcr5dq11r6xsrvsxnxflfhnmjsdbipgzwgz93gr8av2")))

(define-public crate-stpl-0.3.0 (c (n "stpl") (v "0.3.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0r2swjgdyw42bgaiqbv9bhsqcrrd5fw46895gsfxvlp3i92pcadf")))

(define-public crate-stpl-0.3.1 (c (n "stpl") (v "0.3.1") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "10wgpsq90pfj39vi5sakyzwmh6a4q0db17rs1jbmidmwhjaa9532")))

(define-public crate-stpl-0.4.0 (c (n "stpl") (v "0.4.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "19h83yl9nsgh9xc9ppak21cy8gfzkgnaaymsa0c84asc97505ivm")))

(define-public crate-stpl-0.5.0 (c (n "stpl") (v "0.5.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0n75jbz0qy7q9av0b4p49dl03ax26ha3sbylyf9bfmhn0709q7g5")))

