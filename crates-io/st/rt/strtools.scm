(define-module (crates-io st rt strtools) #:use-module (crates-io))

(define-public crate-strtools-0.1.0 (c (n "strtools") (v "0.1.0") (h "0pg5jy9w8gbz7ww9sc6rmw7c7ip35imq9ml85g7f02rjr40cs55a")))

(define-public crate-strtools-0.1.1 (c (n "strtools") (v "0.1.1") (h "1fhv10hjriysnnzbwj25rd1fdj1fg5q5c0x153al2gyrg02mm6js")))

(define-public crate-strtools-0.2.0 (c (n "strtools") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i3bg1qlvb8dz5j2b6q831gr5n982jwx0sdy8iqnc8hjhg58sp1g")))

(define-public crate-strtools-0.3.0 (c (n "strtools") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lnqlcb3pgfmxz48vysfalzklhhki9nw5kzq8qb3fkb9p3584xmb")))

(define-public crate-strtools-0.3.1 (c (n "strtools") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dl04n98craz7aji292f4d6jzlavhic5kypq02k583dnk6x43acy")))

