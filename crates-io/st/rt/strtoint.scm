(define-module (crates-io st rt strtoint) #:use-module (crates-io))

(define-public crate-strtoint-0.1.0-alpha (c (n "strtoint") (v "0.1.0-alpha") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "1zyr62gk7sw04siav4a04qrjybckq0bzygpjwjqbjb0fnckzyy4x") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-strtoint-0.1.0 (c (n "strtoint") (v "0.1.0") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "1x7jlyi0lj95dyfj4f4rga5hdxdhwisp4nlmnq52wilqn1r1024i") (f (quote (("std") ("default" "std")))) (r "1.56")))

