(define-module (crates-io st ee steel-cli) #:use-module (crates-io))

(define-public crate-steel-cli-0.1.0 (c (n "steel-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.10") (f (quote ("std" "derive" "unicode"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "steel-api") (r "=0.1.0") (d #t) (k 0)) (d (n "steel-lang") (r "=0.1.0") (d #t) (k 0)))) (h "0dmdpky2xq9x84b30lkn98hcgigs7132fr0vjn9zn6b04k12k7q8")))

