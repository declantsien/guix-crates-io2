(define-module (crates-io st ee steel-repl) #:use-module (crates-io))

(define-public crate-steel-repl-0.2.0 (c (n "steel-repl") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.1.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "steel-core") (r "^0.2.0") (f (quote ("modules"))) (d #t) (k 0)) (d (n "steel-parser") (r "^0.2.0") (d #t) (k 0)))) (h "1z3grn7prxn9dkall4zn6svm4p0hy30f0jkdn7nb9rmgncaal96v")))

(define-public crate-steel-repl-0.3.0 (c (n "steel-repl") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.1.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "steel-core") (r "^0.3.0") (f (quote ("modules"))) (d #t) (k 0)) (d (n "steel-parser") (r "^0.2.0") (d #t) (k 0)))) (h "0ipr13n69d5pk85i3gdag6wzs8hlm8q0d8avwk02kdimv49lvx6c")))

(define-public crate-steel-repl-0.4.0 (c (n "steel-repl") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.1.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "steel-core") (r "^0.4.0") (f (quote ("web" "sqlite" "blocking_requests" "dylibs" "markdown"))) (d #t) (k 0)) (d (n "steel-parser") (r "^0.4.0") (d #t) (k 0)))) (h "1qf4jiaih7j2m853n1dki3cjyc6mj3p1qmgg5m4g8a1n08d5b86w")))

(define-public crate-steel-repl-0.5.0 (c (n "steel-repl") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.1.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "steel-core") (r "^0.5.0") (f (quote ("web" "sqlite" "blocking_requests" "dylibs" "markdown" "colors"))) (d #t) (k 0)) (d (n "steel-parser") (r "^0.4.0") (d #t) (k 0)))) (h "1gqf22p2vi9br484c9n4v56lkllzdzydsqbqabhzvld38gp0yj8r")))

