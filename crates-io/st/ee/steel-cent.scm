(define-module (crates-io st ee steel-cent) #:use-module (crates-io))

(define-public crate-steel-cent-0.1.0 (c (n "steel-cent") (v "0.1.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "0sa9d2nnj7f5kczj5lk6p3g55plmrmx9i6ndlpwvx1ff0mqqnxbx")))

(define-public crate-steel-cent-0.1.1 (c (n "steel-cent") (v "0.1.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "03y52v3w82h0sxf109b10cd7fkxqjfk3h3jw3dzvcvsj7dmywa61")))

(define-public crate-steel-cent-0.1.2 (c (n "steel-cent") (v "0.1.2") (d (list (d (n "csv") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1k5ls1g41n1ww57f6mrcnnjh26hpjvdz66lvpsdjpksyqzkrcw5c")))

(define-public crate-steel-cent-0.1.3 (c (n "steel-cent") (v "0.1.3") (d (list (d (n "csv") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "09khgj0yp1gasakmyfgq8a4rq70mkv42i1zravafbnjxhss97a6p")))

(define-public crate-steel-cent-0.2.0 (c (n "steel-cent") (v "0.2.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1k5ck4yy6wnzmnxzmqjymhj09y0m105g4ba5nyyglq7kw78nbv81")))

(define-public crate-steel-cent-0.2.1 (c (n "steel-cent") (v "0.2.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1chcbk88mhsxs3x0nlw6ddpa3sa3w4gdi6bp65b7a2ml29wpzv6d")))

(define-public crate-steel-cent-0.2.2 (c (n "steel-cent") (v "0.2.2") (d (list (d (n "csv") (r "^0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0mqbqznkzl8xq41rlq4rrspg9dbc1iliyl31wrr0h80xs8h9hxgx")))

(define-public crate-steel-cent-0.2.3 (c (n "steel-cent") (v "0.2.3") (d (list (d (n "csv") (r "^0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0xxfxgr0qakhwx3mpn9wqd12nkzg3yp8ks18llp7zizpfwrwwdc8")))

