(define-module (crates-io st ee steel-copper) #:use-module (crates-io))

(define-public crate-steel-copper-0.1.0 (c (n "steel-copper") (v "0.1.0") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "rocket") (r "0.4.*") (d #t) (k 0)) (d (n "rocket-include-static-resources") (r "0.9.*") (d #t) (k 0)) (d (n "rocket-include-tera") (r "0.4.*") (d #t) (k 0)) (d (n "rocket_contrib") (r "0.4.*") (f (quote ("tera_templates"))) (k 0)) (d (n "tera") (r "0.11.*") (d #t) (k 0)))) (h "1jqvxavgbsrhsqr5rrslqwfcfzghgfkzvsd7846pz585lblpgn5c")))

