(define-module (crates-io st ee steel-api) #:use-module (crates-io))

(define-public crate-steel-api-0.1.0 (c (n "steel-api") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)))) (h "1pzr429rx1blv86x6zwk2wkgk98hwy6kgp4ff6xcxhhbxvb161g7")))

