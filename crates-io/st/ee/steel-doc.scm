(define-module (crates-io st ee steel-doc) #:use-module (crates-io))

(define-public crate-steel-doc-0.2.0 (c (n "steel-doc") (v "0.2.0") (d (list (d (n "steel-core") (r "^0.2.0") (f (quote ("modules"))) (d #t) (k 0)))) (h "0nj7ivihfrgf0yjndl81cdp77073ss6g9bqb7zrq31isxvafl4v0")))

(define-public crate-steel-doc-0.3.0 (c (n "steel-doc") (v "0.3.0") (d (list (d (n "steel-core") (r "^0.3.0") (f (quote ("modules"))) (d #t) (k 0)))) (h "1m3ck8zw878w2y5bw00wkaldys14cnq7wlm5rnd1sy9vdr7nrhjh")))

(define-public crate-steel-doc-0.4.0 (c (n "steel-doc") (v "0.4.0") (d (list (d (n "steel-core") (r "^0.4.0") (f (quote ("modules"))) (d #t) (k 0)))) (h "1748dwp8bfqpnyl5ngq697vr17ic0632i87laqwbliv668aidw5g")))

(define-public crate-steel-doc-0.5.0 (c (n "steel-doc") (v "0.5.0") (d (list (d (n "steel-core") (r "^0.5.0") (f (quote ("modules"))) (d #t) (k 0)))) (h "1vhzpkjm17d2g55r49if0pb0yvlrwa5hmfx3b9bp9l1p9xm4065q")))

