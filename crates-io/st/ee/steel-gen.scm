(define-module (crates-io st ee steel-gen) #:use-module (crates-io))

(define-public crate-steel-gen-0.2.0 (c (n "steel-gen") (v "0.2.0") (d (list (d (n "codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "02zk9c9lz7w0cj689hv15xysg7zgl9s46fhqp6isbi3i8rqs7m2c")))

