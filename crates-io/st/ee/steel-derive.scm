(define-module (crates-io st ee steel-derive) #:use-module (crates-io))

(define-public crate-steel-derive-0.2.0 (c (n "steel-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1w5m5km1yl61ss05m739cr4qrxr96kim0am2wyfh8f4c0ykvrwz6")))

(define-public crate-steel-derive-0.4.0 (c (n "steel-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0jxkvhyz6qj7v37a4nxs7jgpzyc4p432c0rkf87rcxszv0hly8hd")))

