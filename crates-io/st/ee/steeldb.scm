(define-module (crates-io st ee steeldb) #:use-module (crates-io))

(define-public crate-steeldb-0.1.0 (c (n "steeldb") (v "0.1.0") (d (list (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "steeldb-parser") (r "^0.1.0") (d #t) (k 0)))) (h "04qfaj3kiha398fhqrrchsa8503zgagcmp3n4zs1rqhd3lm75zn7")))

(define-public crate-steeldb-0.1.1 (c (n "steeldb") (v "0.1.1") (d (list (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "steeldb-parser") (r "^0.1.1") (d #t) (k 0)))) (h "0l3vawbwy6h2my1vzpyga7cz8d6sdp0rg79vrz1b2dkq6cpjqn10")))

(define-public crate-steeldb-0.1.2 (c (n "steeldb") (v "0.1.2") (d (list (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "steeldb-parser") (r "^0.1.2") (d #t) (k 0)))) (h "13m8wh62ym0vfq8hh0ygfcv4ysw0a1imagwviaraalcysgigayvv")))

(define-public crate-steeldb-0.1.4 (c (n "steeldb") (v "0.1.4") (d (list (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)) (d (n "steeldb-parser") (r "^0.1.2") (d #t) (k 0)))) (h "166fqdkq6wq6j3vj8r7yl5qhb70x1vf2qsyc4yfvnmqpnbz2qjr3")))

