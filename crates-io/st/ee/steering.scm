(define-module (crates-io st ee steering) #:use-module (crates-io))

(define-public crate-steering-0.1.0 (c (n "steering") (v "0.1.0") (d (list (d (n "alga") (r "^0.5.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 2)) (d (n "tui") (r "^0.1.3") (d #t) (k 2)))) (h "151miz53m3g6bw9vcb469yn9h6vdhcg7qjmphdpnidf3v9s5ybkd")))

