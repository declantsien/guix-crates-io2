(define-module (crates-io st ee steelix-onnx) #:use-module (crates-io))

(define-public crate-steelix-onnx-0.1.0 (c (n "steelix-onnx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0ndhn370427ghm7rm9ph9br6mmgfm5xbg54qr1l8lnizjyfsa04r") (r "1.60.0")))

