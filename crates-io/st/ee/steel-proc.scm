(define-module (crates-io st ee steel-proc) #:use-module (crates-io))

(define-public crate-steel-proc-0.1.0 (c (n "steel-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "01h4mg8qgax9sz2n1xai9pilfanb6cxr6gyqmiywjb8p9mcw1x2d")))

