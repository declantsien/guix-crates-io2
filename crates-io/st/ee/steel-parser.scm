(define-module (crates-io st ee steel-parser) #:use-module (crates-io))

(define-public crate-steel-parser-0.2.0 (c (n "steel-parser") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "0cw2w7ff81c6rrlfzlfkrppn88rc9a0jkk3kpa5zlg6j1jvb329q")))

(define-public crate-steel-parser-0.4.0 (c (n "steel-parser") (v "0.4.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "1351fzb9s56mb4pvcsl5fbx076ryy74hbbrlgy0mf8pmfjk7y0p8")))

