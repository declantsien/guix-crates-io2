(define-module (crates-io st ee steeldb-parser) #:use-module (crates-io))

(define-public crate-steeldb-parser-0.1.0 (c (n "steeldb-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)))) (h "0v6p6mkj6mpz73s9sj1i047z8yr429ic6kwp6vl674gqbi623ddj")))

(define-public crate-steeldb-parser-0.1.1 (c (n "steeldb-parser") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)))) (h "1x09jz8hkydrls3xmjf16m5npisq841c81gdbbjriiqwjxz26yx0")))

(define-public crate-steeldb-parser-0.1.2 (c (n "steeldb-parser") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "unicode"))) (d #t) (k 0)))) (h "1sx4g2jy89w24mz28wijfmnyn8hqjsd7pprccmn83iny2v2a061r")))

