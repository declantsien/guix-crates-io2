(define-module (crates-io st rf strftime-ruby) #:use-module (crates-io))

(define-public crate-strftime-ruby-1.0.0 (c (n "strftime-ruby") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "01f92m9kz25zvc5w1rjym6d9f3csiwvi07g0l7f7a12q151x40kb") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.58.0")))

(define-public crate-strftime-ruby-1.0.1 (c (n "strftime-ruby") (v "1.0.1") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0y2a1dq99n95h7c11xkrq876k8iipc4ai3ns804kp306wkh1sm8y") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.58.0")))

