(define-module (crates-io st rf strfn) #:use-module (crates-io))

(define-public crate-strfn-0.1.0 (c (n "strfn") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "siesta") (r "^0.1.8") (d #t) (k 0)))) (h "0ycqfgaipmb6c7pzq9sfh9ibvrymwff7g1723x22slg43bvwna0a")))

