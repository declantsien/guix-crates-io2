(define-module (crates-io st rf strfry) #:use-module (crates-io))

(define-public crate-strfry-0.0.0 (c (n "strfry") (v "0.0.0") (h "0r0g5sgr9grqs8qrrdvd8gxxmnbqvafgmcdfr854k6fyi2sbdqr1")))

(define-public crate-strfry-0.1.0 (c (n "strfry") (v "0.1.0") (h "0p571dysgmqwqfwffqr3wm2x1v5xyjmmyjapn2ivcrd2wn8qmy7m")))

