(define-module (crates-io st rf strfile) #:use-module (crates-io))

(define-public crate-strfile-0.1.0 (c (n "strfile") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.1") (d #t) (k 0)))) (h "0kcg5gz5fdqydg93a1rr9rn56xigwkll0lnkdv3h39j5jdy44y75")))

(define-public crate-strfile-0.1.1 (c (n "strfile") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3.1") (d #t) (k 0)))) (h "1s97iz1jmywjv81hqxhnf3z4ad7sw0ib98vv6mh8cq8a8cfif8yj") (y #t)))

(define-public crate-strfile-0.1.2 (c (n "strfile") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3.1") (d #t) (k 0)))) (h "0wkkfdbdrxrw145f13w4vrlq5v8y03bwsdsag1f157ky3nmijr5l")))

