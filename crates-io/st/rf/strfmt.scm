(define-module (crates-io st rf strfmt) #:use-module (crates-io))

(define-public crate-strfmt-0.1.0 (c (n "strfmt") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0j4fvbir4pnpf7qzcwmnf1k4dmc5sx6i0igzrpsmbybjlaf6120h")))

(define-public crate-strfmt-0.1.1 (c (n "strfmt") (v "0.1.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "19890a202kifyw0dcjiffpabshm1chmyw5phjav43f1pwq29hrcy")))

(define-public crate-strfmt-0.1.2 (c (n "strfmt") (v "0.1.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1589mjbm7mv85jpdqdcdbx5r27mm6syba95l0zj4gx92qhql4z8n")))

(define-public crate-strfmt-0.1.3 (c (n "strfmt") (v "0.1.3") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "15a51q1759fqnnddd2yz6lkjz1ahkkhlg02cy1p9wcm6ki5bi029")))

(define-public crate-strfmt-0.1.4 (c (n "strfmt") (v "0.1.4") (h "13i9p1xv96szdprwwafrpq0r69001vb8kjjkwdzkhp04y8qlksiz")))

(define-public crate-strfmt-0.1.5 (c (n "strfmt") (v "0.1.5") (h "085jgpdvddybv2g2380p1288fk4y4xq73fm3743hikjzf389gjn6")))

(define-public crate-strfmt-0.1.6 (c (n "strfmt") (v "0.1.6") (h "03x34w92aqyymncxdxp136ci0fmcdh6dslkz4wmqb9bsxx2b4y5j")))

(define-public crate-strfmt-0.2.0 (c (n "strfmt") (v "0.2.0") (h "1bdk15ccapm7rd3qxly72pizzdj25zf8pbh4543yrzdgzj202ni0")))

(define-public crate-strfmt-0.2.1 (c (n "strfmt") (v "0.2.1") (h "1hhkwgrzqwcs2wj7dca5rpsdfrwd55h1djm8rmj1bgwrflkdhxlh")))

(define-public crate-strfmt-0.2.2 (c (n "strfmt") (v "0.2.2") (h "13w2likqqw9jslbmwchh7sz8prw3xhbyjq0imk1fi9vdmg6spk96")))

(define-public crate-strfmt-0.2.3 (c (n "strfmt") (v "0.2.3") (h "1d0z1whm8m8ri52c542pan86gx35wz9l2c7ag5slq777wmcy2gdp")))

(define-public crate-strfmt-0.2.4 (c (n "strfmt") (v "0.2.4") (h "0raysrg8giw52nwbb9334d5zamifvgcdkf1khy62bhwz5npli0vs")))

