(define-module (crates-io st dc stdcode) #:use-module (crates-io))

(define-public crate-stdcode-0.1.0 (c (n "stdcode") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1gk338bbn1lh1y8m50kisxcwkpbwkz46y7qx6axjfcl4ghfsgwr4")))

(define-public crate-stdcode-0.1.1 (c (n "stdcode") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "15pk6110p5fgq0v282xsg6d3h7scyqcn9rlvfbcn3h7ym59hnsd4")))

(define-public crate-stdcode-0.1.2 (c (n "stdcode") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0add5vh52x155hlmk377fcv0nmdhqssqpcsshp578aij8lax3ahs")))

(define-public crate-stdcode-0.1.3 (c (n "stdcode") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "02jiy4na09h9dmjiap2w1fnqf5iwj52cvym07jz3280mykh1id4c")))

(define-public crate-stdcode-0.1.4 (c (n "stdcode") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "11iy2qwg3564zxylr6wdkrsky6ay5cpgxn1l7v1ncnmdnly1c1y7")))

(define-public crate-stdcode-0.1.5 (c (n "stdcode") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0wpcfbsrs20bxvns8h6m82lmi78yd2sj3h1nn9l8mjpxawvj820s")))

(define-public crate-stdcode-0.1.6 (c (n "stdcode") (v "0.1.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0xr6y49qg9fk7i9n2lsd43ran4k8qmfrggrazds8an2jnfqsjj8i")))

(define-public crate-stdcode-0.1.7 (c (n "stdcode") (v "0.1.7") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mdrz5xpm0498fgwm2lsd2pgq8v51xzcwb83qj34wqhlraql89h9")))

(define-public crate-stdcode-0.1.8 (c (n "stdcode") (v "0.1.8") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nkr0w1hsbrwwx2055xh05mj4k2kpjxhd4wr8rismd0k6fgfgi93")))

(define-public crate-stdcode-0.1.9 (c (n "stdcode") (v "0.1.9") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "19h399m2kpsmzsacbv8jh3c6cgnfsgaa26rmgl6az1mikb6fj1q4")))

(define-public crate-stdcode-0.1.10 (c (n "stdcode") (v "0.1.10") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rs8fas1awbpdvsqr90l2avk6s7fdzkzp13h7bx9p4jin8v1hfg3")))

(define-public crate-stdcode-0.1.11 (c (n "stdcode") (v "0.1.11") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "1znn0qm9s3sq7qb64qmkmjq3dsnrbmrvzlk7hpgjcq9nssqcmzgd")))

(define-public crate-stdcode-0.1.12 (c (n "stdcode") (v "0.1.12") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "1rkyk1ca37yjl6f912a2a8mqymgq5sn7nsmva0w55w7phyf31d98")))

(define-public crate-stdcode-0.1.13 (c (n "stdcode") (v "0.1.13") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "08rwsbkzv7y4b16dvl8jigsv2w70x6i37kjab1ljrrw1ckid9h8i")))

(define-public crate-stdcode-0.1.14 (c (n "stdcode") (v "0.1.14") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_with") (r "^1") (d #t) (k 0)))) (h "02cld6wn27m722ziwry3r9av00dqmkgzxpc8v333h3fm201sfc46")))

