(define-module (crates-io st rg strg) #:use-module (crates-io))

(define-public crate-strg-0.1.0 (c (n "strg") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)))) (h "0acfjjm35qv0m017r8vfwmks4klbm1fcxcmsx8rx51g4fakhq2js")))

