(define-module (crates-io vv de vvdecli) #:use-module (crates-io))

(define-public crate-vvdecli-0.0.1 (c (n "vvdecli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vvdec") (r "^0.0.4") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "1kk8xk85rv7axp6accik5za01fnqsnfh8flh6vqxqfzqc7mvgdyv")))

(define-public crate-vvdecli-0.0.2 (c (n "vvdecli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vvdec") (r "^0.0.5") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "05h4y197q9rbriwj8gkl94k12aaw2b7si8m50nbni5jsgaq70gpp")))

(define-public crate-vvdecli-0.0.3 (c (n "vvdecli") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.0.5") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "1wqw97d1y4kl18bddx9qvsv9k57p186kyndfqfafljj2w33whp8f")))

(define-public crate-vvdecli-0.1.0 (c (n "vvdecli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.1.0") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "1zyp52k6p2hab9hnk9ja73bas5d9zp31gn6zv6lilg8m83zi83c1")))

(define-public crate-vvdecli-0.2.0 (c (n "vvdecli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.2.0") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "0bh67jlc4bzibdv8vyz68bvk21mj1xrbw2ifjxilxiih28fxq866")))

(define-public crate-vvdecli-0.3.0 (c (n "vvdecli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.3.0") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "1rx89x12myf78z6mzmlawb4jl840zm156k0vpaj0xf8db7cs26lq")))

(define-public crate-vvdecli-0.3.1 (c (n "vvdecli") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.3.1") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "1wg9z6bampv8zdzhdbxjikiyivi47yphg3xs15h51rydqgpd9j35")))

(define-public crate-vvdecli-0.3.2 (c (n "vvdecli") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.3.2") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "05kfh7lqfbwz0vk2x5qygayw1kpj7bbgdszqwylr1pfvyrn1l6vp")))

(define-public crate-vvdecli-0.4.0 (c (n "vvdecli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.4.0") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "1iikna6zxsv9k1w90i91jwcpw8v396666ldszr1jz9w5xx5x9k6i")))

(define-public crate-vvdecli-0.5.0 (c (n "vvdecli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.5.0") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "00h03c4hj177vm1fdaldry2ms2s2if4xm9m1r9v1bhpf5av5vgcf") (f (quote (("vendored" "vvdec/vendored"))))))

(define-public crate-vvdecli-0.5.1 (c (n "vvdecli") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec") (r "^0.5.1") (d #t) (k 0)) (d (n "y4m") (r "^0.8") (d #t) (k 0)))) (h "14prrlm0r5kyvvsn6pa4npr0lib27vkf50sigdv8ywlbkpy1fiw5") (f (quote (("vendored" "vvdec/vendored"))))))

