(define-module (crates-io vv de vvdec) #:use-module (crates-io))

(define-public crate-vvdec-0.0.1 (c (n "vvdec") (v "0.0.1") (d (list (d (n "vvdec-sys") (r "^0.0.3") (d #t) (k 0)))) (h "06w267qj19inbdvjclhx0djzja536ssdmsm9h3k62bzggy6723cl")))

(define-public crate-vvdec-0.0.2 (c (n "vvdec") (v "0.0.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0h2j7ls01xz0fj8ijz89hcqg98n0lj2q2j31h2r0v23wmqf0p0i8")))

(define-public crate-vvdec-0.0.3 (c (n "vvdec") (v "0.0.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1nfdrsii1i568gap96ldvlrp7pignr4x7i60swa9kyf44bina8fj")))

(define-public crate-vvdec-0.0.4 (c (n "vvdec") (v "0.0.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.0.6") (d #t) (k 0)))) (h "1215vnnxvm0vigypd36qkncxl5xh75xqqdb30g3lvyrf5kjywbmz")))

(define-public crate-vvdec-0.0.5 (c (n "vvdec") (v "0.0.5") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.0.6") (d #t) (k 0)))) (h "09h7b4wdf17af3qm66gh32sk5r7npripi8fnj1bp1hq4255xl5ig")))

(define-public crate-vvdec-0.1.0 (c (n "vvdec") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1i8p6qcxnh35g77zqsn200i7m400yxxkdm6js9cvpy809nvqlqdq")))

(define-public crate-vvdec-0.2.0 (c (n "vvdec") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1nadmwpndx8pn0axaipj4l5sbg7wp20rpc3rfcbc3facl9qxp8ii")))

(define-public crate-vvdec-0.3.0 (c (n "vvdec") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1l1mcpn2y1h9jw7q88pghljn9x3q8fqkwmp9w8rh8bdj5rh8x61c")))

(define-public crate-vvdec-0.3.1 (c (n "vvdec") (v "0.3.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.3.2") (d #t) (k 0)))) (h "0203q9hqlm8fdqjb984ir4lk292jm8jp4xi4saccadk85wnfpgi3")))

(define-public crate-vvdec-0.3.2 (c (n "vvdec") (v "0.3.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.3.4") (d #t) (k 0)))) (h "12kpkqpcak7a051ywxjpss37g76br9g2mligh2axnsb59rz8j4bk")))

(define-public crate-vvdec-0.4.0 (c (n "vvdec") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.3.4") (d #t) (k 0)))) (h "10k74zj6x0gw21avr9h41whq1xn6qrw9b2azvajczk9y2d7hsvxw")))

(define-public crate-vvdec-0.5.0 (c (n "vvdec") (v "0.5.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.4.0") (d #t) (k 0)))) (h "13n41bapx477p02azhlkpqrv7i43mqdvwnhsic6wl14dc20kyyhv") (f (quote (("vendored" "vvdec-sys/vendored"))))))

(define-public crate-vvdec-0.5.1 (c (n "vvdec") (v "0.5.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "vvdec-sys") (r "^0.4.1") (d #t) (k 0)))) (h "11kpl4zq7874xjzc7biijs9afs8jvfv2jprg5ra5bywk4x71v5zp") (f (quote (("vendored" "vvdec-sys/vendored"))))))

