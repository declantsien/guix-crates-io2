(define-module (crates-io vv de vvdec-sys) #:use-module (crates-io))

(define-public crate-vvdec-sys-0.0.1 (c (n "vvdec-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mknbbjv9h876rd7ddaian1njq216b5yx3x5ycj216mx1qrhb93i") (y #t) (l "vvdec")))

(define-public crate-vvdec-sys-0.0.2 (c (n "vvdec-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10qi2vx4capanq92z82ndmsfwhzw1nvayp387pp3hkkl45b1brfm") (l "vvdec")))

(define-public crate-vvdec-sys-0.0.3 (c (n "vvdec-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02d2va5x1y1qg07k6ga0fnvhb0zm0mclj61zzaz80fwhhv32226c") (l "vvdec")))

(define-public crate-vvdec-sys-0.0.4 (c (n "vvdec-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09rcf6wrqfdxhf76j35nrf5rljrabdz1pvkilsywwhjl6a4q8cg6") (l "vvdec")))

(define-public crate-vvdec-sys-0.0.5 (c (n "vvdec-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pc2iai7gmg5smkhxraa365lyb3lzf1xmsynymn6shypyw1xsby3") (l "vvdec")))

(define-public crate-vvdec-sys-0.0.6 (c (n "vvdec-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0y2db45vv64d0rccqdvsm9dc3rc1sa58igsq6wczlp4yhys2zlfs") (l "vvdec")))

(define-public crate-vvdec-sys-0.1.0 (c (n "vvdec-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0q81gd2nxsvrvkhk259c6ydas63al6n36q2sg5yx4iqp77d3wyi7") (l "vvdec")))

(define-public crate-vvdec-sys-0.2.0 (c (n "vvdec-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "013cc8d5gyscqwl7scj5ydvwqgkq946w4s2fm1vpd99x9fnvmqa9") (l "vvdec")))

(define-public crate-vvdec-sys-0.3.0 (c (n "vvdec-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0j102lllxz6gb6d26l1jpsw52528l3654pgr56azkqg82jl2g3q0") (l "vvdec")))

(define-public crate-vvdec-sys-0.3.1 (c (n "vvdec-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0l8ilg3iqf9m1d3fabkyh603ldnn55ablqrz86n0ls79ixl9jrx4") (l "vvdec")))

(define-public crate-vvdec-sys-0.3.2 (c (n "vvdec-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05vxvwzychkx4js59yzd564pml3xhyrdccvpi2igyi6ijzsf563w") (l "vvdec")))

(define-public crate-vvdec-sys-0.3.3 (c (n "vvdec-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0njzf6dckx7g4pz29xckz7b09091hxn7zqglyvrabhabx9znvw2w") (l "vvdec")))

(define-public crate-vvdec-sys-0.3.4 (c (n "vvdec-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "141zss1x68gsx0fwx2vdlldxrm6yljfh9fjdclbcf33jchc9i8sn") (l "vvdec")))

(define-public crate-vvdec-sys-0.4.0 (c (n "vvdec-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dkvspbx053i9ydqpk7gy16qzw4k2zhyqlm6j0zdq6zcc4i3y1y0") (f (quote (("vendored")))) (l "vvdec")))

(define-public crate-vvdec-sys-0.4.1-alpha.1 (c (n "vvdec-sys") (v "0.4.1-alpha.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "044bn0whvz0ksap5m9vczwpg34dvmwz7h5xggsdmagng250awl80") (l "vvdec") (s 2) (e (quote (("vendored" "dep:cmake"))))))

(define-public crate-vvdec-sys-0.4.1 (c (n "vvdec-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0naspx9nnfqwsj0ca52kml3ifq15f6ywl1jhlvnfd1qnhl9jfvq7") (l "vvdec") (s 2) (e (quote (("vendored" "dep:cmake"))))))

