(define-module (crates-io vv co vvcore) #:use-module (crates-io))

(define-public crate-vvcore-0.0.1 (c (n "vvcore") (v "0.0.1") (h "1wzzshxzyfz3fll9kg5z0cf7rzfygpc0lbhs1yxlckxx1vwg9zz2")))

(define-public crate-vvcore-0.0.2 (c (n "vvcore") (v "0.0.2") (h "14sm9yifnhy2z6bd7s2fw1cdx10ag7zb40abawgfc97i28m99wki")))

