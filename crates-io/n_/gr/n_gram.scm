(define-module (crates-io n_ gr n_gram) #:use-module (crates-io))

(define-public crate-n_gram-0.1.0 (c (n "n_gram") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "182jjp47rgarac528iyhl7jvpqfzn3f52fjbj4dymqhacfqvb3gs")))

(define-public crate-n_gram-0.1.1 (c (n "n_gram") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "11xp5zxfsarzvkai14mxh41da42vcyrh2qcigq4w5ybqyqw59yf3")))

(define-public crate-n_gram-0.1.2 (c (n "n_gram") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "12mvkkghmqfkfx7swv0f7wg32c6vxvd1sk0xj1p2abgh1kl0x7md")))

(define-public crate-n_gram-0.1.3 (c (n "n_gram") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0sqassfgdynp5rd5gbbp13b83pg5n65b5n0cf7zxk74sbx7b690r")))

(define-public crate-n_gram-0.1.4 (c (n "n_gram") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "04y968lbikq5w93khz9pjdapilbi1pshkl69sn85qwjmgj0ycf2c")))

(define-public crate-n_gram-0.1.5 (c (n "n_gram") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1saqmlifx4grb9564zxip94bprwjrs7px04daaznrdnv3qv7cjb3")))

(define-public crate-n_gram-0.1.6 (c (n "n_gram") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1khsm2qs78akz8lcky5dx9nkqvkgmc5w47la9vy6spdmvbnvn5pq")))

(define-public crate-n_gram-0.1.7 (c (n "n_gram") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1c8w7mmcmrnsjm4si6hbvclirpy4x33ykkj0bpaw82ijhv2d6hns")))

(define-public crate-n_gram-0.1.8 (c (n "n_gram") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0m0ryrysqp7hm6n21yasb52msyc2rldn705v5aypf0ggrj2sq4vi")))

(define-public crate-n_gram-0.1.9 (c (n "n_gram") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (o #t) (d #t) (k 0)))) (h "0zcx69wv7xzna9x0b6im18lafjqnqv8xjz8fh3i38ixa1zgv5rf0") (f (quote (("corpus")))) (s 2) (e (quote (("saveload" "dep:serde" "dep:serde_json"))))))

(define-public crate-n_gram-0.1.11 (c (n "n_gram") (v "0.1.11") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (o #t) (d #t) (k 0)))) (h "1g3xaxbdj0bqcfxf2i6nipgn1iny1lsy8rfxjnz6ib99jwncq7ad") (f (quote (("default" "serde") ("corpus")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

