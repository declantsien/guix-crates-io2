(define-module (crates-io n_ ar n_array) #:use-module (crates-io))

(define-public crate-n_array-0.1.0 (c (n "n_array") (v "0.1.0") (h "1bjzsj223l4kajfckrmwla17iwlnxgx5j5safib0w53ichbpybz1")))

(define-public crate-n_array-0.1.1 (c (n "n_array") (v "0.1.1") (h "1834af48lamycmwwkci8d5qmy0xb76bfp1wxyz0x0yq9c95vi7jw")))

(define-public crate-n_array-0.1.2 (c (n "n_array") (v "0.1.2") (h "0qzwrv3k519xkwbxgyqmqbwx7zqlr0dgy81slv4lc4fjqjcf8izg")))

(define-public crate-n_array-0.1.3 (c (n "n_array") (v "0.1.3") (h "0ij334npzq0vlgcam7qciyx20b2mbxj0gf54iap8i6h1p10hpwm0")))

