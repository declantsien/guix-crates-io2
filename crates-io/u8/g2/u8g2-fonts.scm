(define-module (crates-io u8 g2 u8g2-fonts) #:use-module (crates-io))

(define-public crate-u8g2-fonts-0.1.0-alpha.1 (c (n "u8g2-fonts") (v "0.1.0-alpha.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "boxed-array") (r "^0.1.0") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 2)))) (h "0kl12rqs4y5sk06sny126fvhjxn44l36n3kf2gv0pz8fdchvrpz0") (f (quote (("std"))))))

(define-public crate-u8g2-fonts-0.1.0 (c (n "u8g2-fonts") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "boxed-array") (r "^0.1.0") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 2)))) (h "0r5554sd2d2ir1rsgfcs9q6wsv7whj99v54nmsfjaycj23j4wj4x") (f (quote (("std"))))))

(define-public crate-u8g2-fonts-0.2.0 (c (n "u8g2-fonts") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "boxed-array") (r "^0.1.0") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 2)))) (h "00hjj8pccvp0z2wg24l80clh8fmphpqb22j0gz365sci0c4zigy2") (f (quote (("std")))) (s 2) (e (quote (("embedded_graphics_textstyle" "dep:embedded-graphics"))))))

(define-public crate-u8g2-fonts-0.3.0 (c (n "u8g2-fonts") (v "0.3.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 2)))) (h "1vb4a257dly4mw2m22iw4yrsk97zimdqjz77zxllpaywg5jxffy5") (f (quote (("std")))) (s 2) (e (quote (("embedded_graphics_textstyle" "dep:embedded-graphics"))))))

(define-public crate-u8g2-fonts-0.4.0 (c (n "u8g2-fonts") (v "0.4.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "embedded-graphics") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 2)))) (h "1h0mcij9fxvy3v64c1qlx8ch62ar0l33gnn17i79g20w2gi7b1x6") (f (quote (("std")))) (s 2) (e (quote (("embedded_graphics_textstyle" "dep:embedded-graphics")))) (r "1.62")))

