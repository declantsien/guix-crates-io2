(define-module (crates-io vp tr vptr) #:use-module (crates-io))

(define-public crate-vptr-0.1.0 (c (n "vptr") (v "0.1.0") (d (list (d (n "vptr-macros") (r "= 0.1.0") (d #t) (k 0)))) (h "0k59x3mc9flbmdygn6zlan3fwq63slmjb10a7sil7s8w5qmahiy0") (f (quote (("std") ("default" "std"))))))

(define-public crate-vptr-0.2.0 (c (n "vptr") (v "0.2.0") (d (list (d (n "vptr-macros") (r "= 0.2.0") (d #t) (k 0)))) (h "17dzq44n5yjxikl3h9ygahzpb0irkhbbr6q1qq6qhaplnzrmfd24") (f (quote (("std") ("default" "std"))))))

(define-public crate-vptr-0.2.1 (c (n "vptr") (v "0.2.1") (d (list (d (n "vptr-macros") (r "= 0.2.1") (d #t) (k 0)))) (h "1ipi4z25yaskrfkwzg0d91jz1aanw4jgl9a4d9zy6zf4qjy04n07") (f (quote (("std") ("default" "std"))))))

