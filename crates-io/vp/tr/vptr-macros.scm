(define-module (crates-io vp tr vptr-macros) #:use-module (crates-io))

(define-public crate-vptr-macros-0.1.0 (c (n "vptr-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cxwr2ljq3n56ya955nb7z3dsviwzms9fz36aj000wvrb7cnd4ck")))

(define-public crate-vptr-macros-0.2.0 (c (n "vptr-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rvy5gyawlbv9rchjxav0j0rzjhijcj4nd55y334r9dyg521zz5v")))

(define-public crate-vptr-macros-0.2.1 (c (n "vptr-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03ims7a7ka58pnp0mdkmbrjnh2c6vdw8ajf2xv4ig60pkjrpxm8f")))

