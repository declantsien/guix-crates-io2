(define-module (crates-io vp se vpsearch) #:use-module (crates-io))

(define-public crate-vpsearch-0.1.0 (c (n "vpsearch") (v "0.1.0") (h "0ap2l6ff4z5hal62fr7r007m3hkm2hpj665v6qs2dq1lham5gsb5")))

(define-public crate-vpsearch-0.2.0 (c (n "vpsearch") (v "0.2.0") (h "0q2ihcr8hnmgmrj0abp6cpzn8ib31vg5vpr88y4yxyp0vjip37vr") (y #t)))

(define-public crate-vpsearch-0.3.0 (c (n "vpsearch") (v "0.3.0") (h "021xhd6pmjwaxhc4j8gbvivii7z5asb2729xpzz35s0bbdw4ribl") (y #t)))

(define-public crate-vpsearch-0.4.0 (c (n "vpsearch") (v "0.4.0") (h "1vj6h291wvq4m4hr5y3m4yivmgrbgxpm38phnn4xalzsdb82msjw") (y #t)))

(define-public crate-vpsearch-0.4.1 (c (n "vpsearch") (v "0.4.1") (h "1cla09ljf1bnpa0ybk0s2xay7yvjix9cg65a3pichd0fh1zv2va8") (y #t)))

(define-public crate-vpsearch-0.5.0 (c (n "vpsearch") (v "0.5.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "10rifqmn1kgvxq5bvjmyscqay8rl1x4wm2wjmlws4cfzmva3qili") (y #t)))

(define-public crate-vpsearch-1.0.0 (c (n "vpsearch") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "18g6mgydc2d1jxksr0jlbkwnl70lswplfv403i5k2dapn1r2bh6j")))

(define-public crate-vpsearch-1.1.0 (c (n "vpsearch") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "1y9y0qlppnbb953cs52zl4gxj5mkszk608sanpisq38bqhx85ihd")))

(define-public crate-vpsearch-1.2.0 (c (n "vpsearch") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "06y5sxfzgyal3pnmf3n8k5xdjk7kckg2lkc52m4dniayjljisfza")))

(define-public crate-vpsearch-1.3.0 (c (n "vpsearch") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "1nax62g4q4jxr7ybd8jas9srhlmx689mmx72cdim1jngbanqvjq9")))

(define-public crate-vpsearch-1.3.1 (c (n "vpsearch") (v "1.3.1") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "0k81ngacprgds7r33s00r03nisyxmarak9na3lhh55zhjbjm3win")))

(define-public crate-vpsearch-1.3.2 (c (n "vpsearch") (v "1.3.2") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "09irmyn68wlmncabgfa275g5a8c7rn420zxq1fbzqcx1f05pbmls")))

(define-public crate-vpsearch-1.3.3 (c (n "vpsearch") (v "1.3.3") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "0i41l054rzi4vzflbg3kkh59x8pzlkx66q7ysvhpv6dax3x0p3if")))

(define-public crate-vpsearch-1.3.4 (c (n "vpsearch") (v "1.3.4") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "14fydmaw69fd82yv2xcjd9641s11977xc9wcbcnsvsl6d9hry575")))

(define-public crate-vpsearch-1.3.5 (c (n "vpsearch") (v "1.3.5") (d (list (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)))) (h "0vdffv7k8nn3s7gkp1934l9yddncs9wqbavw8biwdkxc1aln5ala")))

(define-public crate-vpsearch-1.3.6 (c (n "vpsearch") (v "1.3.6") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0hxhp5rqyha11j7qww0cj1hzxj81mgbyzkglxlz4l53djlpw8ccf")))

(define-public crate-vpsearch-2.0.0-alpha.1 (c (n "vpsearch") (v "2.0.0-alpha.1") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0qyq13q8b7lls4053qph3cmnpnmj2qp2d80xc57karglkdl76bm0")))

(define-public crate-vpsearch-2.0.0 (c (n "vpsearch") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "05ahn1blf3ijfxiv35ps6cnp1g5zaw7akwsah090cryp97d41kf6")))

(define-public crate-vpsearch-2.0.1 (c (n "vpsearch") (v "2.0.1") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0q22py5gdvihkqa927xcn283qxfy9zplh98fg2kg521rv5apwrcq")))

