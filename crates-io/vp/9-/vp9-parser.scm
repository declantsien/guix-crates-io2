(define-module (crates-io vp #{9-}# vp9-parser) #:use-module (crates-io))

(define-public crate-vp9-parser-0.0.1 (c (n "vp9-parser") (v "0.0.1") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "1xpqic7kgqp6nz3bi0y4bgn6j0wp9k1ih1vg76f72fqnd2qbg9cg")))

(define-public crate-vp9-parser-0.1.0 (c (n "vp9-parser") (v "0.1.0") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "1hfvlpxnh6sqhiiawxkbqkqk8i1lb7hdgjjj6rai9jrgv0sz6zzr")))

(define-public crate-vp9-parser-0.2.0 (c (n "vp9-parser") (v "0.2.0") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "1drr1bpp9kf7gsals2lgixwz8y53jv8m5iz108jn7xk5rlv028x8")))

(define-public crate-vp9-parser-0.2.1 (c (n "vp9-parser") (v "0.2.1") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "0z2mr47q3mif039fhq2fh589cv3fp52jgff4vkz7ryh7mr5771sz")))

(define-public crate-vp9-parser-0.2.2 (c (n "vp9-parser") (v "0.2.2") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "0z43saq2sazdf7dcpxd6dw3hj0m83gr9mvh3h2n5rb892j1s0v3v")))

(define-public crate-vp9-parser-0.2.3 (c (n "vp9-parser") (v "0.2.3") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "1s9l4s79ni2fpxglpcbgjiwjnd97jbnrrg26ssza48v6lhwf190c")))

(define-public crate-vp9-parser-0.2.4 (c (n "vp9-parser") (v "0.2.4") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "0brfwka0r8bib8mpc56b0abmxwrwgkfqvxz1l3167h1k2mmrma6j")))

(define-public crate-vp9-parser-0.2.5 (c (n "vp9-parser") (v "0.2.5") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "1v8vcxa928ph36bnx3zn2yh6m2kgnjiqdj0w0xmxd3hs6s4ql3h1")))

(define-public crate-vp9-parser-0.2.6 (c (n "vp9-parser") (v "0.2.6") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "15z6n3cbzmjba0la6wg80xd3hydsmka66mq8f6yy70jdixiys47s")))

(define-public crate-vp9-parser-0.3.0 (c (n "vp9-parser") (v "0.3.0") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "0yzv3y2kaq8p2sj4wxr265rspxlzin4smmpn2mmglg12drpsc7h1")))

(define-public crate-vp9-parser-0.3.1 (c (n "vp9-parser") (v "0.3.1") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)))) (h "0zj358f1ahydvy4q89a0cgi32wk3q4vryqzxlv3bqhndlsaixwpl")))

