(define-module (crates-io vp x- vpx-encode) #:use-module (crates-io))

(define-public crate-vpx-encode-0.3.0 (c (n "vpx-encode") (v "0.3.0") (d (list (d (n "env-libvpx-sys") (r "^4.0.9") (d #t) (k 0)))) (h "1rnr2kif02r13lrdzxjhmv0gr2asq8f379hjp2g2snh6mspnyffg") (f (quote (("vp9"))))))

(define-public crate-vpx-encode-0.4.0 (c (n "vpx-encode") (v "0.4.0") (d (list (d (n "env-libvpx-sys") (r "^4.0.10") (d #t) (k 0)))) (h "0rn15dscwz1rc2d34fidcx6j3haxrybqc4dadmkaa4lx08im92n1") (f (quote (("vp9"))))))

(define-public crate-vpx-encode-0.4.1 (c (n "vpx-encode") (v "0.4.1") (d (list (d (n "env-libvpx-sys") (r "^5.0.0") (d #t) (k 0)))) (h "19iyl8yj279q76cgn90zr3xg7d9qyljy41rgv22z4l3avscdyym9") (f (quote (("vp9"))))))

(define-public crate-vpx-encode-0.5.0 (c (n "vpx-encode") (v "0.5.0") (d (list (d (n "env-libvpx-sys") (r "^5.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c61v4688kdydank97k7swmfnd7b7czmqpxh05q3jrk5r81zc024") (f (quote (("vp9") ("backtrace"))))))

(define-public crate-vpx-encode-0.6.0 (c (n "vpx-encode") (v "0.6.0") (d (list (d (n "env-libvpx-sys") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nzba0qdq0m6d9h28kz4bv1bqmm8pbk3cylxhqb4qvcc684r4c33") (f (quote (("vp9") ("backtrace"))))))

(define-public crate-vpx-encode-0.6.1 (c (n "vpx-encode") (v "0.6.1") (d (list (d (n "env-libvpx-sys") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ss9mw9d8gmvqhdmjzmgdg6ibj5fxr9ypmr05lwxzqahq9lvmscr") (f (quote (("vp9") ("ffi-generate" "env-libvpx-sys/generate") ("backtrace"))))))

(define-public crate-vpx-encode-0.6.2 (c (n "vpx-encode") (v "0.6.2") (d (list (d (n "env-libvpx-sys") (r "^5.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1dplns7fs3bwllpc748j1ybxx7kfqcw8qkm4vg5nfxny8apl27yd") (f (quote (("vp9") ("ffi-generate" "env-libvpx-sys/generate") ("backtrace"))))))

