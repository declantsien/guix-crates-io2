(define-module (crates-io vp x- vpx-sys) #:use-module (crates-io))

(define-public crate-vpx-sys-0.1.0 (c (n "vpx-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "19zknndzs3bkaf5s7z1a46h5gnzlx7iyn0l2gsazcn1qzakhpfhl")))

(define-public crate-vpx-sys-0.1.1 (c (n "vpx-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0ha1p5mqcgp791b4pvsnmcvdfgcgcxb1jc0d7sl65nmamvzdj0x7")))

