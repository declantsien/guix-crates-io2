(define-module (crates-io vp nk vpnkitrc) #:use-module (crates-io))

(define-public crate-vpnkitrc-0.1.0 (c (n "vpnkitrc") (v "0.1.0") (d (list (d (n "ntex") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1crh1w9269hfh46lwczw4y93lifpc723zrw2k88gvhsj2sdghzs0") (f (quote (("tokio" "ntex/tokio") ("glommio" "ntex/glommio") ("default" "tokio") ("async-std" "ntex/async-std"))))))

(define-public crate-vpnkitrc-0.1.1 (c (n "vpnkitrc") (v "0.1.1") (d (list (d (n "ntex") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1frrzj8q4g9y5v3y1nmf94bcbqgix1i3k7wa985q3n06v4nhv6zy") (f (quote (("tokio" "ntex/tokio") ("glommio" "ntex/glommio") ("default" "tokio") ("async-std" "ntex/async-std"))))))

(define-public crate-vpnkitrc-0.1.2 (c (n "vpnkitrc") (v "0.1.2") (d (list (d (n "ntex") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "0aq3zb53bicpcqifjs2b31q44irp65bhgczlhgxkixrdjq18b8cn") (f (quote (("tokio" "ntex/tokio") ("glommio" "ntex/glommio") ("default" "tokio") ("async-std" "ntex/async-std"))))))

(define-public crate-vpnkitrc-0.1.3 (c (n "vpnkitrc") (v "0.1.3") (d (list (d (n "ntex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16jbvdh3j3yjqri3bw80jb5y8hz43rl07fgpp3g85axy81w0magp") (f (quote (("tokio" "ntex/tokio") ("glommio" "ntex/glommio") ("default" "tokio") ("async-std" "ntex/async-std"))))))

