(define-module (crates-io vp -a vp-avl) #:use-module (crates-io))

(define-public crate-vp-avl-0.1.0 (c (n "vp-avl") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bjrhcsmiqw4lrn6c1mxzjx9lihwj88cb6396j97jdk8ln7xjb9c")))

(define-public crate-vp-avl-0.1.1 (c (n "vp-avl") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "141pn293fxnwp18s0f3whxr6vjfxdysh9wf2g0cdn4a7g67lgzcl")))

(define-public crate-vp-avl-0.1.2 (c (n "vp-avl") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wsjigf306zsyzqqk0yvplw129dkw0h1gwk7nflj4m65wj9szgn6")))

(define-public crate-vp-avl-0.1.3 (c (n "vp-avl") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1572a11zkxs6a64a57ncgvi0gnb8v4ylr2ywizj6l056jxaqyzrm")))

(define-public crate-vp-avl-0.1.4 (c (n "vp-avl") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "047fw42g5difgg823icmda36ycvkbnj62vlh08664z7lc66nc7ch")))

(define-public crate-vp-avl-0.1.5 (c (n "vp-avl") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02c40k8ark0sf9f6yqpd84kz6fks9bxh16zbd6y5cl7aw3482my4")))

