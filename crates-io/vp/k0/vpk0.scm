(define-module (crates-io vp k0 vpk0) #:use-module (crates-io))

(define-public crate-vpk0-0.8.1 (c (n "vpk0") (v "0.8.1") (d (list (d (n "bitstream-io") (r "^0.8.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)))) (h "15y01yiix87fmd7z3w3x20wcz9w0dz2czkngrw1pndm8wlp6z1v2")))

(define-public crate-vpk0-0.8.2 (c (n "vpk0") (v "0.8.2") (d (list (d (n "bitstream-io") (r "^0.8.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)))) (h "0fndplabngdm8mslxjchiyk7ld6rg2ig05a762x8zcydbbykw08k")))

