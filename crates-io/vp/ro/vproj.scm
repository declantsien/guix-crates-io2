(define-module (crates-io vp ro vproj) #:use-module (crates-io))

(define-public crate-vproj-0.1.0 (c (n "vproj") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.17.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt-multi-thread" "fs" "sync"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1fjg97imdxazgp5pay6sa96n3c1ri3m3h3d86aay9dzmr8v7fm93") (y #t)))

(define-public crate-vproj-0.1.1 (c (n "vproj") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt-multi-thread" "fs" "sync"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1pdg8j4fb8imfj8piaal8zy3l8wzx777x7jk0dk5s6ayfqhd7ldq") (y #t)))

(define-public crate-vproj-0.1.2 (c (n "vproj") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt-multi-thread" "fs" "sync"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "0nwsswf71pc26vzx2wggkfh19q4478q0qs8719747p6cyw4iskjk") (y #t)))

(define-public crate-vproj-0.1.3 (c (n "vproj") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt-multi-thread" "fs" "sync"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1ywf3aw8kg96yby9y2j5aprw56vja4vfab05iz2z4l5x8pckan85")))

