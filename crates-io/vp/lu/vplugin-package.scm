(define-module (crates-io vp lu vplugin-package) #:use-module (crates-io))

(define-public crate-vplugin-package-0.1.0 (c (n "vplugin-package") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.151") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "vplugin") (r "^0.2.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "0p6hc4mqwv11q3x0bcpwfam3gi0piwlcxp0apj0029phi42iz5f5")))

