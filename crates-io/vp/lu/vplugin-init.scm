(define-module (crates-io vp lu vplugin-init) #:use-module (crates-io))

(define-public crate-vplugin-init-0.1.0 (c (n "vplugin-init") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("cargo" "std"))) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1jb4qzsb66igp38iqizkk4wvx4c0q0arvz37jrhfjfpwgqfgf2pc")))

