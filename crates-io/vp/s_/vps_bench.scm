(define-module (crates-io vp s_ vps_bench) #:use-module (crates-io))

(define-public crate-vps_bench-0.0.1-pre (c (n "vps_bench") (v "0.0.1-pre") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1cypq9l9v8k2r8p9fqd858qmm596mz0g4q3b1psxz97q1b49ypyj")))

(define-public crate-vps_bench-0.0.1 (c (n "vps_bench") (v "0.0.1") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1g2rs9nxijak4cf7p5kk90wlq65l3r2g804d7z32wz5d890jg66d")))

(define-public crate-vps_bench-0.0.2 (c (n "vps_bench") (v "0.0.2") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0lw2kvrwn9xhj1a3znn3050f73i1hcwf2c02ak7ypdb7zy9aa58k")))

(define-public crate-vps_bench-0.0.3 (c (n "vps_bench") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dqa90bj5hzmbfa5f7jqspshzf72b5rl8fibv5c8s8vk02gydjw3")))

(define-public crate-vps_bench-0.0.5 (c (n "vps_bench") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0v6anj2lwkqg6zz09qf5psgzcwbwgd2258sykzl8h24h5kca6zrd")))

(define-public crate-vps_bench-0.1.0 (c (n "vps_bench") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1p34h3j6axlvixh6ybwj9fadh7vg88nimlrbml871rlqfvdp3ywf")))

(define-public crate-vps_bench-0.1.1 (c (n "vps_bench") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1r4699wcliahpvz7d9wr2s493qnb1vbgwjz69s5x42v0m5cycq1j")))

(define-public crate-vps_bench-0.1.2 (c (n "vps_bench") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dykhvkngz22d7l1vrgv2s268zf3avbn68xghkf2lfpkbianlh4i")))

(define-public crate-vps_bench-0.2.0 (c (n "vps_bench") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng" "getrandom"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("sqlite" "runtime-tokio-native-tls"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1j304358jhzvfsk84a54098afdaisqckdf9mzn8a7mks9xf1kl92")))

