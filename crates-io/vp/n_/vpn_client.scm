(define-module (crates-io vp n_ vpn_client) #:use-module (crates-io))

(define-public crate-vpn_client-0.0.1 (c (n "vpn_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1fan8c3ll22b1xknqrhs2hn2kw25ipklrmjqmnag16wyaggyik15")))

(define-public crate-vpn_client-0.0.2 (c (n "vpn_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)) (d (n "vpncloud") (r "^1.0.0") (d #t) (k 0)))) (h "01bq6i4yvc0rn0jad7dwd3vjcxziss5dckdbfzrm20lm7mf928si")))

(define-public crate-vpn_client-0.0.3 (c (n "vpn_client") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)) (d (n "vpncloud") (r "^1.0.0") (d #t) (k 0)))) (h "1qka1v0ixn3932pn58rj3b5iwzy35kzwsaq8m8lv6brksjfscldp")))

(define-public crate-vpn_client-0.0.4 (c (n "vpn_client") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "16p6hnfzk24iyh4wgyrrg0dx9jjxgczy99s5hn8wxrivi5zhdlnw")))

(define-public crate-vpn_client-0.0.5 (c (n "vpn_client") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "09p3vn9ikkslyv48c7hklnpazivjmqqdqd9dp115ac2pl17ww4yf")))

(define-public crate-vpn_client-0.0.6 (c (n "vpn_client") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.41") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0ps7kjlsy2l8mqnj33vrsg6yvrcghsdwnqb03i71v6k4k21mz080")))

(define-public crate-vpn_client-0.0.7 (c (n "vpn_client") (v "0.0.7") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1jkgrw36vdsccf10i8dh5zqvvlszdn8z5hz36zzy9pi8k4sdwfgk")))

