(define-module (crates-io vp p- vpp-api-transport) #:use-module (crates-io))

(define-public crate-vpp-api-transport-0.1.0 (c (n "vpp-api-transport") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08vbs5cm3r52xr50y5j2479yjdfnkfx0s3p7lyaw6n5scflyn6mk")))

(define-public crate-vpp-api-transport-0.1.1 (c (n "vpp-api-transport") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02fi2yrhnwqk84l5a7rqy598kyvv4f72ad2zsqxsdjnbi9lr04fz")))

(define-public crate-vpp-api-transport-0.1.2 (c (n "vpp-api-transport") (v "0.1.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0smm0kd1yq2d82y288wpydf4n4v288vr7298mgcqal6qmaq5khsw")))

(define-public crate-vpp-api-transport-0.1.3 (c (n "vpp-api-transport") (v "0.1.3") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0423hzhj0l348xn5ffsska80cllx7lgqzdic7agy3m5ddbidpkqk")))

(define-public crate-vpp-api-transport-0.1.4 (c (n "vpp-api-transport") (v "0.1.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1ig1348avzrga3qjf61ay9q3fal944wr4vwfm3mcyjhgfhb9r4qc")))

(define-public crate-vpp-api-transport-0.1.5 (c (n "vpp-api-transport") (v "0.1.5") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "vpp-api-message") (r "^0.0.1") (d #t) (k 0)))) (h "1gzpch6nwikdrn6dksddfqvfwssqfrrrfp95b5nr905n2h09nygi")))

