(define-module (crates-io vp ip vpipe) #:use-module (crates-io))

(define-public crate-vpipe-0.2.1 (c (n "vpipe") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wxr6vfadcmma63fgmyl2xxwn95szp6fqzriljzzf1symzhzq5fb") (r "1.59")))

(define-public crate-vpipe-0.2.2 (c (n "vpipe") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)))) (h "1iavx9ggrzsamxs9zykbx56smi4f7b8yxhxj0sif206kny39synn") (r "1.59")))

(define-public crate-vpipe-0.3.0 (c (n "vpipe") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "1jb35gpi56l0741j3482fs7h898ly5hnqjip6m95ba7jpn3mr42v") (r "1.59")))

