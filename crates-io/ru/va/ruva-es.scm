(define-module (crates-io ru va ruva-es) #:use-module (crates-io))

(define-public crate-ruva-es-0.3.2 (c (n "ruva-es") (v "0.3.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.2") (d #t) (k 0)))) (h "06k524vr262scglx1lvhi00k2p2q9i04af8yy7pvjsjdz2dqsvgf") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.3 (c (n "ruva-es") (v "0.3.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.3") (d #t) (k 0)))) (h "1czzgxrdnb32yf356bffl5fgmfzkq33c47fgp4ihj0m05vbhh3vl") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.4 (c (n "ruva-es") (v "0.3.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.4") (d #t) (k 0)))) (h "13jcw7x8qaxp87mbbjr0lq5l06s82c8vn4hfqafi9hiv7w3mk813") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.5 (c (n "ruva-es") (v "0.3.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.5") (d #t) (k 0)))) (h "07dlw8r4nkaw93f57jimbcqbknimij62ikbx76wqhmwnd2ggdbgq") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.6 (c (n "ruva-es") (v "0.3.6") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.6") (d #t) (k 0)))) (h "0gbymyyx0dvq6d2smkkqaiy5d0ixiqwf1i4r6fl8rxz9wcghlp7b") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.7 (c (n "ruva-es") (v "0.3.7") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.7") (d #t) (k 0)))) (h "16y3zi37m9f0zrl6l76vrd05s5p2hzrdhja38p0yxz58dvb1a46y") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.8 (c (n "ruva-es") (v "0.3.8") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.8") (d #t) (k 0)))) (h "140g4hddk8nnxd1d62hdjc3673q1fdml1yamdlsx0wpqd9v9s0c3") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.9 (c (n "ruva-es") (v "0.3.9") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.9") (d #t) (k 0)))) (h "12z2y796jfx255l56ca0671j9nx3kg3lgpmj30hc9njz84bnmd41") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.10 (c (n "ruva-es") (v "0.3.10") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.10") (d #t) (k 0)))) (h "1mf4m5a6rsvx7xaxgfhymprrljrf63qfd2zs0ns6r36lm9a189vb") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.11 (c (n "ruva-es") (v "0.3.11") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.11") (d #t) (k 0)))) (h "07qnkxx6gvplwmid5f651nvlrppgxbld5k8rhsax2prhba7jpy22") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.12 (c (n "ruva-es") (v "0.3.12") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.12") (d #t) (k 0)))) (h "19yas9dxrpnriq9chgskdlnb6n5rm6vnk0c79dm5c9xm2i00ziyf") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.13 (c (n "ruva-es") (v "0.3.13") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.13") (d #t) (k 0)))) (h "1wz40x4dm40kcss9bvavca3a7v6ipgr4477b4r4rzrc8prk9lkgy") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.14 (c (n "ruva-es") (v "0.3.14") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.14") (d #t) (k 0)))) (h "121f0c0g8322k4rzzxlg9zac9glh11xhs4pzd8w7q8six2yns7c2") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.15 (c (n "ruva-es") (v "0.3.15") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.15") (d #t) (k 0)))) (h "0ni4ggfjkkcn6f24qh4bzkhvcqj6p7q8ndiqfhdgwyl488c9zm43") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.16 (c (n "ruva-es") (v "0.3.16") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.16") (d #t) (k 0)))) (h "11yyncv9iv5haws7z8byip5lf222gw2wviqa285y4i89j93ffgv2") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.17 (c (n "ruva-es") (v "0.3.17") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.17") (d #t) (k 0)))) (h "0sh8sfp0z677m42b4czim5yrarll8qp3iin4yb3b3762pi9nrzwv") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.18 (c (n "ruva-es") (v "0.3.18") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.18") (d #t) (k 0)))) (h "1ldvkbpfqa7s5blrm9vsz32zxi05iwzyabdbkyiighbhpw7lkhqx") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.19 (c (n "ruva-es") (v "0.3.19") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.19") (d #t) (k 0)))) (h "038x9f4nf08mkvb6wmqwbv24fj862sggpqd3yrj70hbfzs8gycn7") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.20 (c (n "ruva-es") (v "0.3.20") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.20") (d #t) (k 0)))) (h "02qfdxadshsg6slisgghkdvhwj9imlnm0f6g5c9z3n532jyv4k8n") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.21 (c (n "ruva-es") (v "0.3.21") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.21") (d #t) (k 0)))) (h "0x207f069mclp55mym7kgq6rgabmwi0y3b8zv3lmrj4v1vgzkivc") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.22 (c (n "ruva-es") (v "0.3.22") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.22") (d #t) (k 0)))) (h "1vwk3kz0h3flil9c77b47ih2bz3lqn7qx049nn1p96lr82d8w60p") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.23 (c (n "ruva-es") (v "0.3.23") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.23") (d #t) (k 0)))) (h "03q74lby82wiqm7mfhp9pk3rckaf5v8gc31razpsz5v3nwn7jqi1") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.24 (c (n "ruva-es") (v "0.3.24") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.24") (d #t) (k 0)))) (h "1v3m5n4v60kbn2pw7w37cfivgbn8071mimlv8r5nd8i0ynaa3vj0") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.25 (c (n "ruva-es") (v "0.3.25") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.25") (d #t) (k 0)))) (h "04mq2ls8fb9sdn994qcjscjz8wdlag7hxzk8n956jjn80h29fqzh") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-es-0.3.26 (c (n "ruva-es") (v "0.3.26") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ruva-core") (r "^0.3.26") (d #t) (k 0)))) (h "04x3vbvsdzy033rkjpw21sqqwlzmk3arwl28b5igiy75kqwddjcb") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

