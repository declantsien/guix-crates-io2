(define-module (crates-io ru va ruva) #:use-module (crates-io))

(define-public crate-ruva-0.1.49 (c (n "ruva") (v "0.1.49") (d (list (d (n "ruva-core") (r "^0.1.49") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.49") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1gn5cijcv2267igig80qk1abcn9m58khx0ha8ydqwpkglv808x3x")))

(define-public crate-ruva-0.1.50 (c (n "ruva") (v "0.1.50") (d (list (d (n "ruva-core") (r "^0.1.50") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.50") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0sa9wb8irs58bxgp8wbqqhbkkfr0cxvac2dif5bxrgna29cfj4mw")))

(define-public crate-ruva-0.1.51 (c (n "ruva") (v "0.1.51") (d (list (d (n "ruva-core") (r "^0.1.51") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.51") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1mh27znwbmq7dhalvkjhnqnx8a54296i6q1f2y7d540z19f97khi")))

(define-public crate-ruva-0.1.52 (c (n "ruva") (v "0.1.52") (d (list (d (n "ruva-core") (r "^0.1.52") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.52") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1khsj86zl72dlg2f8jib1mvn17nr01fxvx32myzmvq11kzf6s6pj")))

(define-public crate-ruva-0.1.53 (c (n "ruva") (v "0.1.53") (d (list (d (n "ruva-core") (r "^0.1.53") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.53") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "181v8rmg6hdglf4p8ky172hp2hg09nqy86rhhv18h59mw8ma9cwc")))

(define-public crate-ruva-0.1.54 (c (n "ruva") (v "0.1.54") (d (list (d (n "ruva-core") (r "^0.1.54") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.54") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0jfc3ws7nim8c0qh071hinh5va95bv8j04j51bxxzxnkmrk1gygh")))

(define-public crate-ruva-0.1.55 (c (n "ruva") (v "0.1.55") (d (list (d (n "ruva-core") (r "^0.1.55") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.55") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0ij05mvi88g5awqding5qqh44mczlr55hj2c4wj2qwjcvg4m99r4")))

(define-public crate-ruva-0.1.56 (c (n "ruva") (v "0.1.56") (d (list (d (n "ruva-core") (r "^0.1.56") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.56") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0k0xvy03k07ar2a52ypwd2vl1j6f5l9y3w2hn0xb54xhqrn79f4r")))

(define-public crate-ruva-0.1.57 (c (n "ruva") (v "0.1.57") (d (list (d (n "ruva-core") (r "^0.1.57") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.57") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1hb2kxbkm27y73yv1n3dk53nh7717ilbnpf9vfvcacs224jfnkcx")))

(define-public crate-ruva-0.1.58 (c (n "ruva") (v "0.1.58") (d (list (d (n "ruva-core") (r "^0.1.58") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.58") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "10k025bl6c8gs2fvxw781x3xlj35gk9z7x6myy03m21pi0m1c8i8")))

(define-public crate-ruva-0.1.59 (c (n "ruva") (v "0.1.59") (d (list (d (n "ruva-core") (r "^0.1.59") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.1.59") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0f4lyggds2xw0dvavplc1473km1r5jf9cmw1gnw1c6b4vvhdcbjc")))

(define-public crate-ruva-0.2.0 (c (n "ruva") (v "0.2.0") (d (list (d (n "ruva-core") (r "^0.2.0") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "09hq9p20scw8m8cj85glf36bi3258xd0niifla057xcmycacfm60")))

(define-public crate-ruva-0.2.1 (c (n "ruva") (v "0.2.1") (d (list (d (n "ruva-core") (r "^0.2.1") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0kpczrwijk0y9x04fxl3y96zr9rba8fycfgdkbw72q5bs4vw57pb")))

(define-public crate-ruva-0.2.2 (c (n "ruva") (v "0.2.2") (d (list (d (n "ruva-core") (r "^0.2.2") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0v62yw792d2hgsga7immapmflydvw0nnjdnahiv0axpq94gxvmmd")))

(define-public crate-ruva-0.2.3 (c (n "ruva") (v "0.2.3") (d (list (d (n "ruva-core") (r "^0.2.3") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0hzyzgyw8nx8b7hcrj8qr572wgcdzqgpq9shj9vnh61qb019fz30")))

(define-public crate-ruva-0.2.4 (c (n "ruva") (v "0.2.4") (d (list (d (n "ruva-core") (r "^0.2.4") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1v2b2n0qx5rmzhhafgdp256k5qig16pg1747dzwx2li2cg84ws49")))

(define-public crate-ruva-0.2.5 (c (n "ruva") (v "0.2.5") (d (list (d (n "ruva-core") (r "^0.2.5") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "03j6z3cg3gs1ipdlnav76ipahwk7w64cwlrr7v40p85ddq02h830")))

(define-public crate-ruva-0.2.6 (c (n "ruva") (v "0.2.6") (d (list (d (n "ruva-core") (r "^0.2.6") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.6") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0khl5zrg5k9n9bwhnrlhfbrdphkcgivk6y80z47fzx6avwag1639")))

(define-public crate-ruva-0.2.7 (c (n "ruva") (v "0.2.7") (d (list (d (n "ruva-core") (r "^0.2.7") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.7") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "114wn1qq036scgq6158j6vav7pgz476jdhfcg90pgxg4sw44j9wd")))

(define-public crate-ruva-0.2.8 (c (n "ruva") (v "0.2.8") (d (list (d (n "ruva-core") (r "^0.2.8") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.8") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "01l8w42ad2dsj8lchwd6nnrjlq5n96p2fv89dghwqs2v9gffhjdr")))

(define-public crate-ruva-0.2.9 (c (n "ruva") (v "0.2.9") (d (list (d (n "ruva-core") (r "^0.2.9") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1z68dl434fma2jz86ling14ixlmm843p9fdkknb737bqmjjq0zdi")))

(define-public crate-ruva-0.2.10 (c (n "ruva") (v "0.2.10") (d (list (d (n "ruva-core") (r "^0.2.10") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "06nzlrcr8r35vy88jg7kmzx3k7i61pjqjba3qcwv4k5lszwlcx90")))

(define-public crate-ruva-0.2.11 (c (n "ruva") (v "0.2.11") (d (list (d (n "ruva-core") (r "^0.2.11") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1fnk0wf9w48r9apzb8vixl5q4p92i3zyhhsf1rqmw9fz0c9njp04")))

(define-public crate-ruva-0.2.12 (c (n "ruva") (v "0.2.12") (d (list (d (n "ruva-core") (r "^0.2.12") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.12") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "11sxkm0z37dywfy0xgzv6m0a7g44gd73nmj0rx7fvkcqk0kii6za")))

(define-public crate-ruva-0.2.13 (c (n "ruva") (v "0.2.13") (d (list (d (n "ruva-core") (r "^0.2.13") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.13") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0i9478hfyf9m3rd7k1phjxhirw29183psppkp0vxsxha2g6biiai")))

(define-public crate-ruva-0.2.14 (c (n "ruva") (v "0.2.14") (d (list (d (n "ruva-core") (r "^0.2.14") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1788a73rxwf7jysyyz66z2pv91d3h8b5077nb02b9j4v524svb8d")))

(define-public crate-ruva-0.2.15 (c (n "ruva") (v "0.2.15") (d (list (d (n "ruva-core") (r "^0.2.15") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.15") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1hqhacq7ma754hdwddab87y5ryq1ws2c3npdnk6l7jhlpggh2c52")))

(define-public crate-ruva-0.2.16 (c (n "ruva") (v "0.2.16") (d (list (d (n "ruva-core") (r "^0.2.16") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.16") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0x2vnl5yhvbxy3cgi971s2kizny8442lsgi9acbgiqnckr63xvgy")))

(define-public crate-ruva-0.2.17 (c (n "ruva") (v "0.2.17") (d (list (d (n "ruva-core") (r "^0.2.17") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.17") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "132lgc8hmgv9370qdh8rl9p8fxv6j0sl3vgfslj2mzs84sl45dgw")))

(define-public crate-ruva-0.2.18 (c (n "ruva") (v "0.2.18") (d (list (d (n "ruva-core") (r "^0.2.18") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.18") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1vj7sjyvr1q608xrqdrpcx2mlh1cf7zpq2myrlzaiccwzymmq09c")))

(define-public crate-ruva-0.2.19 (c (n "ruva") (v "0.2.19") (d (list (d (n "ruva-core") (r "^0.2.19") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.19") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1kyrz9szzrqi02srkip2g5fl5jknp4daj76y06sncz7c3s9i8vkg")))

(define-public crate-ruva-0.2.20 (c (n "ruva") (v "0.2.20") (d (list (d (n "ruva-core") (r "^0.2.20") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.2.20") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1l12wwrcjxs5dihw5yradgb31bbkvjkbsgkb6v2l96hs0pibkxqr") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.0 (c (n "ruva") (v "0.3.0") (d (list (d (n "ruva-core") (r "^0.3.0") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1jkr48s2gb0h1ac0lwljqd5j1jqzz6s367z7x5liby7fi3x1an8d") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.1 (c (n "ruva") (v "0.3.1") (d (list (d (n "ruva-core") (r "^0.3.1") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "18x7kz6dbslml55b1zqpjxj94qypssg6hi780asc58wmg14fh9aj") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.2 (c (n "ruva") (v "0.3.2") (d (list (d (n "ruva-core") (r "^0.3.2") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.2") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1g865j60r40za82x7k5mh0qjpn4n01l36yc6imhjwypha711dhl3") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.3 (c (n "ruva") (v "0.3.3") (d (list (d (n "ruva-core") (r "^0.3.3") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.3") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1vkvw122q01am8r3z7867h9xydc6r8ja9sk3x3k63nscxnwsazsr") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.4 (c (n "ruva") (v "0.3.4") (d (list (d (n "ruva-core") (r "^0.3.4") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.4") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "038r4cc1ik74a7vrnv5mbb9l873hr1acq5l4dk5i262plhka49zh") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.5 (c (n "ruva") (v "0.3.5") (d (list (d (n "ruva-core") (r "^0.3.5") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.5") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "00ccw9w8r59q33gf4kzkgzqs63mshdccg7ipx9nw505slf3v2mnk") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.6 (c (n "ruva") (v "0.3.6") (d (list (d (n "ruva-core") (r "^0.3.6") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.6") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.6") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0djq3f5p435ik6s07h29bmy85zbscdg924h229j7z3g9w2drkrlj") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.7 (c (n "ruva") (v "0.3.7") (d (list (d (n "ruva-core") (r "^0.3.7") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.7") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.7") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0066zr5n7v41d9kmz6mhgr5l60ba1y8yq4y149gqgc8xyxs5sck1") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.8 (c (n "ruva") (v "0.3.8") (d (list (d (n "ruva-core") (r "^0.3.8") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.8") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.8") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1k0m6zj46x5bdmq3g6c88hvcympy5mbd1nb6cfc7bv8n7y71w50w") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.9 (c (n "ruva") (v "0.3.9") (d (list (d (n "ruva-core") (r "^0.3.9") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.9") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.9") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "090zkvcn5qak0hylfcrw8l8404j7wsy59phaxkxrphaqna0hbls4") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.10 (c (n "ruva") (v "0.3.10") (d (list (d (n "ruva-core") (r "^0.3.10") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.10") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.10") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1q7phszl69q97ri5fhky0a1vjrn1s8xj3mlh618i8qga18diqhaf") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.11 (c (n "ruva") (v "0.3.11") (d (list (d (n "ruva-core") (r "^0.3.11") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.11") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0gwr2aqbslszg2f1xlzy7p97sglfabx3sc38797q9ljzf3r3d518") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.12 (c (n "ruva") (v "0.3.12") (d (list (d (n "ruva-core") (r "^0.3.12") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.12") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.12") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1phlykv9liphjggwpzyzbhllwndmifmja10dvy2wjif0h9sxgsgf") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.13 (c (n "ruva") (v "0.3.13") (d (list (d (n "ruva-core") (r "^0.3.13") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.13") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.13") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1gkj5kpvwxlc8f60pv1as0labh4bisc0h7k49rf6cj9hgg25l450") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.14 (c (n "ruva") (v "0.3.14") (d (list (d (n "ruva-core") (r "^0.3.14") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.14") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0skv5gbx9gg809bcxjhkry23rjzl3vl8vwxq7x30awp0y2qwr5pa") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.15 (c (n "ruva") (v "0.3.15") (d (list (d (n "ruva-core") (r "^0.3.15") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.15") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.15") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "12y01l0d9m92055mq3wfqj5mb4hmyxhnr8as7iqfq7zwl2bsh7m4") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.16 (c (n "ruva") (v "0.3.16") (d (list (d (n "ruva-core") (r "^0.3.16") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.16") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.16") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "10ag2javshrm8k1b3ck409lib8gjv0qaqss02warwr55in0lp7za") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.17 (c (n "ruva") (v "0.3.17") (d (list (d (n "ruva-core") (r "^0.3.17") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.17") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.17") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "150ya9nb3igymz9fwmidds7335zjg3xkikc7v8pfwggqf1qbqxhb") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.18 (c (n "ruva") (v "0.3.18") (d (list (d (n "ruva-core") (r "^0.3.18") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.18") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.18") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1svbh0kxh2p96ghchld2iwf6gm3xan47lavl808s73zymc34znak") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.19 (c (n "ruva") (v "0.3.19") (d (list (d (n "ruva-core") (r "^0.3.19") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.19") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.19") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0qb2c9rrrcva8wrnpp45iif8wcl71m3dy7nrkww2akhsz381dkw7") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.20 (c (n "ruva") (v "0.3.20") (d (list (d (n "ruva-core") (r "^0.3.20") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.20") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.20") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "05snbw0pcid7ps7w3wda5awjfdvd93q5bahx5sf0xscqdzn0fbbc") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.21 (c (n "ruva") (v "0.3.21") (d (list (d (n "ruva-core") (r "^0.3.21") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.21") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1047pn206cw9ydcdbw939ppwd7lsd89qz9pzpbkn1n4byc0pv5yc") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.22 (c (n "ruva") (v "0.3.22") (d (list (d (n "ruva-core") (r "^0.3.22") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.22") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.179") (f (quote ("derive"))) (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0ah608pfg2f2ig7mbpv695z3caawj4bdz6z442qwi826rrkgk294") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.23 (c (n "ruva") (v "0.3.23") (d (list (d (n "ruva-core") (r "^0.3.23") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.23") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.179") (f (quote ("derive"))) (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0yqzs1zsdbqqlmjiczpg320ylizsvpq5qr4j295gyr9x98mb0v23") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.24 (c (n "ruva") (v "0.3.24") (d (list (d (n "ruva-core") (r "^0.3.24") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.24") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.179") (f (quote ("derive"))) (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0pwbzgjx6k7vdlbyr5s08sg5hyj692x4l2n6j3dx3phwa0wa81d4") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.25 (c (n "ruva") (v "0.3.25") (d (list (d (n "ruva-core") (r "^0.3.25") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.25") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.179") (f (quote ("derive"))) (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0vc8k32nriym0kpc6aa7s926dwlp66disi329snjcw3pgn1b2dg5") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

(define-public crate-ruva-0.3.26 (c (n "ruva") (v "0.3.26") (d (list (d (n "ruva-core") (r "^0.3.26") (d #t) (k 0)) (d (n "ruva-es") (r "^0.3.26") (d #t) (k 0)) (d (n "ruva-macro") (r "^0.3.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.179") (f (quote ("derive"))) (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "181r18vyv5j1bl21krblh418f5629s58i9xbfz3zjp9w8dplcjhq") (f (quote (("sqlx-postgres" "ruva-core/sqlx-postgres"))))))

