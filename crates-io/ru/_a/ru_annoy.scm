(define-module (crates-io ru _a ru_annoy) #:use-module (crates-io))

(define-public crate-ru_annoy-0.1.0-dev1 (c (n "ru_annoy") (v "0.1.0-dev1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1a56mpp09j3r7dvbqddbyna9ba8i2k6vyzwspj82kfhn0pksmhrr") (f (quote (("cffi" "libc"))))))

(define-public crate-ru_annoy-0.1.1 (c (n "ru_annoy") (v "0.1.1") (d (list (d (n "libc") (r "^0") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0") (d #t) (k 0)))) (h "185fyl6sy3i5knz5s59glwnydjbmk9q6b3kkaw0z1h8w6h49l9v0") (f (quote (("cffi" "libc"))))))

(define-public crate-ru_annoy-0.1.2 (c (n "ru_annoy") (v "0.1.2") (d (list (d (n "libc") (r "^0") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0") (d #t) (k 0)))) (h "14jnbsadh1rr3j5nhf4al4isckhzibhjblb249jl51dim4fvk8sx") (f (quote (("cffi" "libc"))))))

(define-public crate-ru_annoy-0.1.3 (c (n "ru_annoy") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0") (d #t) (k 0)))) (h "0vi5g6myz5vp0bwbd4qshgbj4qzwqbq47r181wi0ypadk3krbqns") (f (quote (("cffi" "libc"))))))

