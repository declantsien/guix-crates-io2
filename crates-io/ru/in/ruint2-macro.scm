(define-module (crates-io ru in ruint2-macro) #:use-module (crates-io))

(define-public crate-ruint2-macro-1.0.2 (c (n "ruint2-macro") (v "1.0.2") (h "09vnhf8ig7as7i78i8r1fh7c2k0cnhbfv4ia7g80fahhqiaikxxm")))

(define-public crate-ruint2-macro-1.0.3 (c (n "ruint2-macro") (v "1.0.3") (h "135j745prc4yzl4bv898pxk4nvjzxlhjxakcp6l14ifgq0xmbp49")))

