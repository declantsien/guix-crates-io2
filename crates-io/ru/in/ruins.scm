(define-module (crates-io ru in ruins) #:use-module (crates-io))

(define-public crate-ruins-0.0.1 (c (n "ruins") (v "0.0.1") (h "15f6kzihi71i7173azy6k7lyyszs0136lfhiy5f89i5rlfmfkavx")))

(define-public crate-ruins-0.0.2 (c (n "ruins") (v "0.0.2") (h "051pczlh8jiz8km0v5vq19vzkalcbl9mbsam70biqq38d2gmjixb")))

