(define-module (crates-io ru in ruint-macro) #:use-module (crates-io))

(define-public crate-ruint-macro-0.1.0 (c (n "ruint-macro") (v "0.1.0") (h "015mmh8kl2y0g1dan8s7ry8lfq05qypwj3rlfj54f03man3d5vcl")))

(define-public crate-ruint-macro-0.1.1 (c (n "ruint-macro") (v "0.1.1") (d (list (d (n "ruint") (r "^0.1") (d #t) (k 2)))) (h "1saix7p7cw7qjdz2k3fs60h0j3qc2cx8mj826v2cr0ddf93f8hnk")))

(define-public crate-ruint-macro-0.2.0 (c (n "ruint-macro") (v "0.2.0") (h "1csfjbpjfkmnb70lnmpqwbkbmcqzwzvz8zshvhm0vwm78alq64nx")))

(define-public crate-ruint-macro-0.2.1 (c (n "ruint-macro") (v "0.2.1") (h "0263i56yq023rrwidszrvvafzf5mhwcgdrawb62v7hvvz25dvx60")))

(define-public crate-ruint-macro-1.0.0 (c (n "ruint-macro") (v "1.0.0") (h "0mbp4qz9clshiyv84viy398sdyxmalv07yq8pxg5wx6zr6i0zfx4")))

(define-public crate-ruint-macro-1.0.1 (c (n "ruint-macro") (v "1.0.1") (h "0b9ylxp6zmdkk6jjgr8gd0b6lr7l71i3sy3fgrjb8cr2mfcfs9y8")))

(define-public crate-ruint-macro-1.0.2 (c (n "ruint-macro") (v "1.0.2") (h "1ldjyc25h2bck82qgg95l6blg7phrg0g7pz7cz9jk8iy4rh5gk32")))

(define-public crate-ruint-macro-1.1.0 (c (n "ruint-macro") (v "1.1.0") (h "02cajz30f844sch2rrmxj5aw34z055p11xhgrpdqc88bd94sarp6") (r "1.65")))

(define-public crate-ruint-macro-1.2.0 (c (n "ruint-macro") (v "1.2.0") (h "0hr33vpl47n5f58gd7sf6d69lg2c55f9m1q90m9934i5a37m8s7q") (r "1.65")))

