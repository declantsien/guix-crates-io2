(define-module (crates-io ru nd rundeck) #:use-module (crates-io))

(define-public crate-rundeck-0.1.0 (c (n "rundeck") (v "0.1.0") (d (list (d (n "clap") (r "^2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0f17jzji644mqvwbxi33p59rm3rcs5zfq94q9kb6i3s753gfy1yz")))

