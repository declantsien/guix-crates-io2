(define-module (crates-io ru nd rundo_attrsca) #:use-module (crates-io))

(define-public crate-rundo_attrsca-0.1.0 (c (n "rundo_attrsca") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fh8yqhpq7r4sz9d38i9s195nn132vrma2adwgskr5flwjj4adm0")))

