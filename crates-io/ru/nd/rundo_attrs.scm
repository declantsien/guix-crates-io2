(define-module (crates-io ru nd rundo_attrs) #:use-module (crates-io))

(define-public crate-rundo_attrs-0.1.0 (c (n "rundo_attrs") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sssnkjsvxq2wra38sqkkx9lzs0z48aj04i1nxcmz12byw1n5x4s")))

(define-public crate-rundo_attrs-0.1.1 (c (n "rundo_attrs") (v "0.1.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ibhwpr78hmwr5irwgifdrpd668p0am1qydrijvchay1sqm4w0dr")))

(define-public crate-rundo_attrs-0.2.0 (c (n "rundo_attrs") (v "0.2.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03j3smq7jnxdnax53c3pli93r4rja4ximbjqak9ilmylfmqgzr7f")))

(define-public crate-rundo_attrs-0.2.1 (c (n "rundo_attrs") (v "0.2.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g83bwmlx0fmxva6p8g7y6s0h549n5vza8m1p20myjxv4p0fy3as")))

(define-public crate-rundo_attrs-0.2.2 (c (n "rundo_attrs") (v "0.2.2") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11fvygxnpgm9k23d5c2pd9ldf8chb5nvxkb3b20ywxkg0ld5gprx")))

(define-public crate-rundo_attrs-0.3.1 (c (n "rundo_attrs") (v "0.3.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aphaikjl16m7743kwxvq9g1r60609kydjr5iwnczijycbx987bk")))

(define-public crate-rundo_attrs-0.3.2 (c (n "rundo_attrs") (v "0.3.2") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1frcv76sqd6xa59lgb4c7sx3cwbjvbd2xrgnxikcx4q60nhsfjh6")))

(define-public crate-rundo_attrs-0.4.0 (c (n "rundo_attrs") (v "0.4.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pnb94s62zd9gnr7z9v1asbvkibwvba8bc3kh37vkyik39hf3wbk")))

