(define-module (crates-io ru nd rundeck-api) #:use-module (crates-io))

(define-public crate-rundeck-api-0.1.0 (c (n "rundeck-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0h0ydgvg7vihnlh6vbmcmjars8x8yfzvqx5p3fgyv7pfnn9arqk4")))

