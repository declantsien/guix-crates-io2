(define-module (crates-io ru nd rundo) #:use-module (crates-io))

(define-public crate-rundo-0.1.1 (c (n "rundo") (v "0.1.1") (d (list (d (n "rundo_attrs") (r "^0.1") (d #t) (k 0)) (d (n "rundo_types") (r "^0.1") (d #t) (k 0)))) (h "0x4qwq7h1a7r7nsjm0qnpkgnwd1i9mwkbg91z3s2djxnr3szxqm3")))

(define-public crate-rundo-0.2.0 (c (n "rundo") (v "0.2.0") (d (list (d (n "rundo_attrs") (r "^0.2") (d #t) (k 0)) (d (n "rundo_types") (r "^0.2") (d #t) (k 0)))) (h "14hyizz3984glnlq01y7w39j673pgsgxlvafg927iyvw1iskjp9h")))

(define-public crate-rundo-0.3.2 (c (n "rundo") (v "0.3.2") (d (list (d (n "bson") (r "^0.11") (d #t) (k 0)) (d (n "rundo_attrs") (r "^0.3") (d #t) (k 0)) (d (n "rundo_types") (r "^0.3") (d #t) (k 0)))) (h "0kqnny1xs6l6d9cgyvjzjg8w99f0g0ancagg7zvii1aaigmdmpk0")))

(define-public crate-rundo-0.4.0 (c (n "rundo") (v "0.4.0") (d (list (d (n "bson") (r "^0.11") (d #t) (k 0)) (d (n "rundo_attrs") (r "^0.4") (d #t) (k 0)) (d (n "rundo_types") (r "^0.4") (d #t) (k 0)))) (h "07wmlsdp1wkhpmh1cx1ryp894zgnrxd6gxj2jag4gdik5xldg9p6")))

