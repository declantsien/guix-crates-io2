(define-module (crates-io ru nd rundo_types) #:use-module (crates-io))

(define-public crate-rundo_types-0.1.0 (c (n "rundo_types") (v "0.1.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "1n7f26sxrghyjxrfkvv3vnqjb1nqqxs1qmm68vk5wvvczxrh96wz")))

(define-public crate-rundo_types-0.1.1 (c (n "rundo_types") (v "0.1.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "0glw3c7qc7rsc26k201qlqix8chjmyp24q8hq4w0fz85ifsxzdqp")))

(define-public crate-rundo_types-0.2.0 (c (n "rundo_types") (v "0.2.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "0s5dbgvk8r15km4zhczqaij9v02ak5dwwacz92g5kmdrl1x0l12z")))

(define-public crate-rundo_types-0.2.1 (c (n "rundo_types") (v "0.2.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "1hd2jbcslb0jxz6gsrzi75g0b4c18cg9a2a9c2fkkr98w1hd5499")))

(define-public crate-rundo_types-0.2.2 (c (n "rundo_types") (v "0.2.2") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "05a0273fi76spyk3mcfrx4w8qijhr4dfd6ln1ca795fg2pn0iq4v")))

(define-public crate-rundo_types-0.3.1 (c (n "rundo_types") (v "0.3.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "0p10f9kggycab3xyggdi83p5k3wqyih9xd03dgas0ycg40mrld0n")))

(define-public crate-rundo_types-0.3.2 (c (n "rundo_types") (v "0.3.2") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "0fikaw14q3ggjashvixl539yr14sd2ry07myy56fygcyajh5y5n1")))

(define-public crate-rundo_types-0.4.0 (c (n "rundo_types") (v "0.4.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)))) (h "0sh7hbi2jr8ffcrjmyqhcfafdx8l5wkc4nspj0rcknf6yyrn3hjk")))

