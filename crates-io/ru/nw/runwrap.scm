(define-module (crates-io ru nw runwrap) #:use-module (crates-io))

(define-public crate-runwrap-0.1.0 (c (n "runwrap") (v "0.1.0") (d (list (d (n "partial_application") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)))) (h "1i45vnn2vk8b4lscydcqxsvzwm5kkw7f06gn0jpzn8jqmbw0l6my")))

(define-public crate-runwrap-0.2.0 (c (n "runwrap") (v "0.2.0") (d (list (d (n "partial_application") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.10") (d #t) (k 2)) (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "1l5m6x3nvxb6ivgj34rnzqznkpxnpj8c9sriw6rn9h5l2rvdk2i0")))

