(define-module (crates-io ru nw runwhen) #:use-module (crates-io))

(define-public crate-runwhen-0.0.1 (c (n "runwhen") (v "0.0.1") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "humantime") (r "~1.0.0") (d #t) (k 0)) (d (n "md5") (r "~0.3.2") (d #t) (k 0)) (d (n "notify") (r "~3.0.0") (d #t) (k 0)) (d (n "subprocess") (r "~0.1.7") (d #t) (k 0)))) (h "14id0qr2jdyywlywsvgvgi0if79si592bb95fjk0h82sj6hzimsl")))

(define-public crate-runwhen-0.0.2 (c (n "runwhen") (v "0.0.2") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "humantime") (r "~1.0.0") (d #t) (k 0)) (d (n "notify") (r "~3.0.0") (d #t) (k 0)))) (h "1qkwm4nipzamdd2mr8gyhfrmhpp1zs30gx07wn5b8nahlmvdprw1")))

(define-public crate-runwhen-0.0.3 (c (n "runwhen") (v "0.0.3") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "humantime") (r "~1.0.0") (d #t) (k 0)) (d (n "notify") (r "~3.0.0") (d #t) (k 0)))) (h "0ii1r8kbphi0ndqj43k00vaxq9hsd5lx4rkr685xb4axm0cphfiz")))

(define-public crate-runwhen-0.0.8 (c (n "runwhen") (v "0.0.8") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "0si7b28iqpz1jv2kczdkxndvnmm8rykxxp2rldlcmjb8n48cvlqh")))

