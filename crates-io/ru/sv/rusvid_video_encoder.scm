(define-module (crates-io ru sv rusvid_video_encoder) #:use-module (crates-io))

(define-public crate-rusvid_video_encoder-0.2.0 (c (n "rusvid_video_encoder") (v "0.2.0") (d (list (d (n "ffmpeg-sys-next") (r "^5.1.1") (f (quote ("avformat" "swscale"))) (k 0)) (d (n "rusvid_core") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dvfpx46yqllnh7bxmq4791zwkgqm4chqk1g8dlcad13vncb1zlm")))

(define-public crate-rusvid_video_encoder-0.2.1 (c (n "rusvid_video_encoder") (v "0.2.1") (d (list (d (n "ffmpeg-sys-next") (r "^5.1.1") (f (quote ("avformat" "swscale"))) (k 0)) (d (n "rusvid_core") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pr0w9anknn8vv93if2wr5pvdk6ff1qlf7ymqdsmrbkfa0b0p320")))

