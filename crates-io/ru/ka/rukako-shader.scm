(define-module (crates-io ru ka rukako-shader) #:use-module (crates-io))

(define-public crate-rukako-shader-0.0.0 (c (n "rukako-shader") (v "0.0.0") (d (list (d (n "bytemuck") (r "^1.6.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "float-ord") (r "^0.3.1") (d #t) (t "cfg(not(target_arch = \"spirv\"))") (k 0)) (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_arch = \"spirv\"))") (k 0)) (d (n "spirv-std") (r "^0.4.0-alpha.10") (f (quote ("glam"))) (d #t) (k 0)))) (h "0slcm8m9bcyrs7kzr6k5xxskdjaacddwhs9m5qby31s3lrn4j2hx")))

