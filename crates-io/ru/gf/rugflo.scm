(define-module (crates-io ru gf rugflo) #:use-module (crates-io))

(define-public crate-rugflo-0.1.0 (c (n "rugflo") (v "0.1.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)) (d (n "rugint") (r "^0.1") (d #t) (k 0)) (d (n "rugrat") (r "^0.1") (d #t) (k 0)))) (h "0li0d6dw7b611i1k460049gnsfzix4vwpb10a09zd2m7mmh3p353")))

(define-public crate-rugflo-0.1.1 (c (n "rugflo") (v "0.1.1") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugint") (r "^0.1") (k 0)) (d (n "rugrat") (r "^0.1") (d #t) (k 0)))) (h "0wj5kgpgaf2v4i222z83ps73iw0r497vbkifw3a8xw4f06swcy01") (f (quote (("default" "rand"))))))

(define-public crate-rugflo-0.1.2 (c (n "rugflo") (v "0.1.2") (d (list (d (n "gmp-mpfr-sys") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugint") (r "^0.1.2") (k 0)) (d (n "rugrat") (r "^0.1.2") (d #t) (k 0)))) (h "1mnw8r60y7w1rndks6n4nvnvhql89sdn3g71w7dzcw9cbdly7k4w") (f (quote (("default" "rand"))))))

(define-public crate-rugflo-0.1.3 (c (n "rugflo") (v "0.1.3") (d (list (d (n "gmp-mpfr-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugint") (r "^0.1.3") (k 0)) (d (n "rugrat") (r "^0.1.3") (d #t) (k 0)))) (h "1g57fl84by295aqx7hbgkim47n956qxfh6dagf8sfj9rmmmdjwrf") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugflo-0.2.0 (c (n "rugflo") (v "0.2.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugint") (r "^0.2.0") (k 0)) (d (n "rugrat") (r "^0.2.0") (d #t) (k 0)))) (h "1hq4ihxja0crcyzcclc2b7fq68n5wqcfirr0hx0j377xz47g3vkw") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugflo-0.2.1 (c (n "rugflo") (v "0.2.1") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugint") (r "^0.2.1") (k 0)) (d (n "rugrat") (r "^0.2.1") (d #t) (k 0)))) (h "09k187h9sk3v6nvafvx9gcc53znvhyb3432cmqic4hlw31rxrdy1") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugflo-0.2.2 (c (n "rugflo") (v "0.2.2") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (f (quote ("mpfr"))) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugint") (r "^0.2.2") (k 0)) (d (n "rugrat") (r "^0.2.2") (d #t) (k 0)))) (h "0ssj7yr16pqzw4ddakzizvlhdpr3p7zvq2bg2ddpgsj5h9094qai") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugflo-0.3.0 (c (n "rugflo") (v "0.3.0") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (f (quote ("mpfr"))) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugint") (r "^0.3.0") (k 0)) (d (n "rugrat") (r "^0.3.0") (d #t) (k 0)))) (h "0z79qg7iksigcz7q818028x2qmng13l0m2w3y212w62hjvlk0rmw") (f (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugflo-0.4.0 (c (n "rugflo") (v "0.4.0") (h "0i3n54ys40dnh1m8r3zqvh65lj74mi6vnj4awz98kw53zjxfj26i")))

(define-public crate-rugflo-0.4.1 (c (n "rugflo") (v "0.4.1") (h "1vf9lkayssrw41j5scnh1vc1lwxxpnfpg6qmi64f3a0ki921ja21")))

