(define-module (crates-io ru gf rugfield) #:use-module (crates-io))

(define-public crate-rugfield-0.1.0 (c (n "rugfield") (v "0.1.0") (d (list (d (n "peroxide") (r "^0.34.7") (d #t) (k 0)) (d (n "peroxide") (r "^0.34.7") (f (quote ("plot"))) (d #t) (k 2)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "0gq4fbn1y3x2g3yzg5wkdhqsin0wg24vyiprjrlz22rs92g28zk0")))

(define-public crate-rugfield-0.2.0 (c (n "rugfield") (v "0.2.0") (d (list (d (n "peroxide") (r "^0.35") (d #t) (k 0)) (d (n "peroxide") (r "^0.35") (f (quote ("plot"))) (d #t) (k 2)) (d (n "puruspe") (r "^0.2.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rc2wng3i2v2ddf8h1nn9890q93v23dpwsgif1q3hashyhmb6pkb") (s 2) (e (quote (("serde" "dep:serde" "peroxide/serde"))))))

(define-public crate-rugfield-0.2.1 (c (n "rugfield") (v "0.2.1") (d (list (d (n "peroxide") (r "^0.35") (d #t) (k 0)) (d (n "peroxide") (r "^0.35") (f (quote ("plot"))) (d #t) (k 2)) (d (n "puruspe") (r "^0.2.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s4yfxnsalmiznpac7i64470pxzq5i82lzrzfxynnf867x4400bs") (s 2) (e (quote (("serde" "dep:serde" "peroxide/serde"))))))

(define-public crate-rugfield-0.2.2 (c (n "rugfield") (v "0.2.2") (d (list (d (n "peroxide") (r "^0.37") (d #t) (k 0)) (d (n "peroxide") (r "^0.37") (f (quote ("plot"))) (d #t) (k 2)) (d (n "puruspe") (r "^0.2.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "024v86sc54isllbxp9wxj197vhj2ssgclyf8w4cwlxw710ssq3hb") (s 2) (e (quote (("serde" "dep:serde" "peroxide/serde"))))))

