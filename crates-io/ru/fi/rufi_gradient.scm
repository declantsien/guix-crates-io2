(define-module (crates-io ru fi rufi_gradient) #:use-module (crates-io))

(define-public crate-rufi_gradient-1.0.0 (c (n "rufi_gradient") (v "1.0.0") (d (list (d (n "rufi_core") (r "^15.1.0") (d #t) (k 0)))) (h "0kdzmax7js99c61a15826vf7r13n63g7wr3wwql79ddyimpr8r9g")))

(define-public crate-rufi_gradient-2.0.0 (c (n "rufi_gradient") (v "2.0.0") (d (list (d (n "rf-core") (r "^0.1.0") (d #t) (k 0)))) (h "1a1963kj2nzi39minpfhk58l3f5827zkiaxrsrw8a7wchjsz0v8f")))

(define-public crate-rufi_gradient-2.0.1 (c (n "rufi_gradient") (v "2.0.1") (d (list (d (n "rf-core") (r "^0.1.2") (d #t) (k 0)))) (h "1i7i1i2b2szna5q4vv6z4k90xavr3mwkvchkklvbirzj0k0k1ac6")))

(define-public crate-rufi_gradient-2.0.2 (c (n "rufi_gradient") (v "2.0.2") (d (list (d (n "rf-core") (r "^0.2.1") (d #t) (k 0)))) (h "0irdsxlbhccx76an1al0ag8jrmnpxl4ji0h1fk1id6gzc4qlf4xk")))

(define-public crate-rufi_gradient-2.0.3 (c (n "rufi_gradient") (v "2.0.3") (d (list (d (n "rf-core") (r "^0.3.0") (d #t) (k 0)))) (h "0fp69xp1j3ll8yasgfc39ih89g19hymczw7mznv0ajqmxci2kknl")))

(define-public crate-rufi_gradient-2.0.4 (c (n "rufi_gradient") (v "2.0.4") (d (list (d (n "rf-core") (r "^0.3.1") (d #t) (k 0)))) (h "0mwl9j3mxfwn4h28yk4bzp2860kd1bmpyzsh6caxfzgzb2ccdxq4")))

(define-public crate-rufi_gradient-2.0.5 (c (n "rufi_gradient") (v "2.0.5") (d (list (d (n "rf-core") (r "^0.4.0") (d #t) (k 0)))) (h "0wn0hmg95k4fj31kvsv334rn63pka2gkqqx727bg470xfal4v3a9")))

(define-public crate-rufi_gradient-2.0.7 (c (n "rufi_gradient") (v "2.0.7") (d (list (d (n "rf-core") (r "^0.4.2") (d #t) (k 0)))) (h "03281gm6fs85drikrk1y5qa7dz146ndd579wqvr96hrgmckpl86r")))

(define-public crate-rufi_gradient-2.0.8 (c (n "rufi_gradient") (v "2.0.8") (d (list (d (n "rf-core") (r "^0.4.3") (d #t) (k 0)))) (h "0y4nad92z6vjjnmi2h0wffalw0gpcgjcy1hnygnj92i1yz09750v")))

(define-public crate-rufi_gradient-2.0.9 (c (n "rufi_gradient") (v "2.0.9") (d (list (d (n "rf-core") (r "^0.5.0") (d #t) (k 0)))) (h "1z5i0kpjh4bvplg4hqfkjgfh1v9mvg9li0z9nyqsyf5qr6hr4bcb")))

(define-public crate-rufi_gradient-2.0.10 (c (n "rufi_gradient") (v "2.0.10") (d (list (d (n "rf-core") (r "^0.5.1") (d #t) (k 0)))) (h "141dwx0bl79g6kpb2v9gi4pi8qgdc6ik4pxzcadp0sj3nyi14sm6")))

(define-public crate-rufi_gradient-2.0.11 (c (n "rufi_gradient") (v "2.0.11") (d (list (d (n "rf-core") (r "^0.5.2") (d #t) (k 0)))) (h "0hfajk2zlgrramf290b74j80dy05gp8a76lirq7rhfl1qj1q4d5l")))

(define-public crate-rufi_gradient-2.0.12 (c (n "rufi_gradient") (v "2.0.12") (d (list (d (n "rf-core") (r "^0.6.0") (d #t) (k 0)))) (h "1xwarpmp34jp2agi4fppnjbf2a2r4ck1sg1ssq5d9yn1a1259jv9")))

