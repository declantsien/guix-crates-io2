(define-module (crates-io ru fi rufi_core) #:use-module (crates-io))

(define-public crate-rufi_core-1.0.0 (c (n "rufi_core") (v "1.0.0") (h "01szyji18jmnm6ww7lwwkd9p97hfwkfk8lnnrvir2474kdwfifs4")))

(define-public crate-rufi_core-1.1.0 (c (n "rufi_core") (v "1.1.0") (h "16hix9g2kra3cng09bc1csb99lhhfy9p3dv8x1vsrl0rmkcfhflh")))

(define-public crate-rufi_core-1.2.0 (c (n "rufi_core") (v "1.2.0") (h "00p8v2dvh17x345wzlkq9ffcjddysvn0k996d1562f4bpsp3bv6m")))

(define-public crate-rufi_core-2.0.0 (c (n "rufi_core") (v "2.0.0") (h "1nh70vk0zcp9h0vbjc3hy5j0vzng37h7c6r63bkm8fq9xn90kbrg")))

(define-public crate-rufi_core-3.0.0 (c (n "rufi_core") (v "3.0.0") (h "1kqgw0ix25pr1m75cbcgj53f3vm5p8l50pmw27hx0gmhb418zmfy")))

(define-public crate-rufi_core-4.0.0 (c (n "rufi_core") (v "4.0.0") (h "1z5p1igr31f9qqpg5pq17h1rxbi68cwy6a42mvpg5kzi6bj7yy8b")))

(define-public crate-rufi_core-5.0.0 (c (n "rufi_core") (v "5.0.0") (h "1j7k1dy7zc8nda92fgsgl8sxv77i9v11mjjrmxyqxbphlxjjj0z5")))

(define-public crate-rufi_core-5.1.0 (c (n "rufi_core") (v "5.1.0") (h "1f67i9rwrn9fjam276m5nlxzmq4jgpg2xp5rmlhkp30fg2x3337k")))

(define-public crate-rufi_core-6.0.0 (c (n "rufi_core") (v "6.0.0") (h "0a2bwhsggz21bhynl56alyarsg2nsqjpa18j8781jvmj53bf7nn6")))

(define-public crate-rufi_core-6.0.1 (c (n "rufi_core") (v "6.0.1") (h "0jsc7812z2cvsa0jcxprnwlv35i2llwpryhzw8rnmfcbbbbv1k49")))

(define-public crate-rufi_core-6.1.0 (c (n "rufi_core") (v "6.1.0") (h "0dxsd93jfsc98f68m8vy290sb5fnmn09jh6pzxq03a3l97rwl50h")))

(define-public crate-rufi_core-7.0.0 (c (n "rufi_core") (v "7.0.0") (h "1waflc93frd907ahsny94qcmhj6rh7qprmnjll2x6n7nvlfhl2jc")))

(define-public crate-rufi_core-8.0.0 (c (n "rufi_core") (v "8.0.0") (h "05zky04hk5zavvh7srkvfpg4bzsxr14h75bkl1rzlrf5kiissq4b")))

(define-public crate-rufi_core-9.0.0 (c (n "rufi_core") (v "9.0.0") (h "0hmh8fa5by346biznwv8l2h79rp8h0xd2ssqzspkdbk481y45lgl")))

(define-public crate-rufi_core-9.0.1 (c (n "rufi_core") (v "9.0.1") (h "1kvwxxmyi6a16icpnqgxgl3py3ncsva6q8j6h1f26ig62wg9pk05")))

(define-public crate-rufi_core-9.1.0 (c (n "rufi_core") (v "9.1.0") (h "1c2wyv5xd17p949hvidrb247589ym9y6iyn2m16g7mjs9f8ki3r1")))

(define-public crate-rufi_core-10.0.0 (c (n "rufi_core") (v "10.0.0") (h "1fmc3qw71jgp3s08x1rmni4pxrrjy7l2ak1xn23nxarhr28jihsb")))

(define-public crate-rufi_core-10.1.0 (c (n "rufi_core") (v "10.1.0") (h "0h603k1lxzjx344k2kkgdmc214mi5690wqx88rqhkpqrv7njbzsi")))

(define-public crate-rufi_core-11.0.0 (c (n "rufi_core") (v "11.0.0") (h "0dlgcpyyjs99dqngq5dy65v85s9iw4wkks8z0mscc3m7hpxlnjgj")))

(define-public crate-rufi_core-11.1.0 (c (n "rufi_core") (v "11.1.0") (h "0hxdgdjr8vl65y0c21c9pz7niz129z4zaw2ayrhyrxccq1cjc8y3")))

(define-public crate-rufi_core-12.0.0 (c (n "rufi_core") (v "12.0.0") (h "0av0lb63522pj0jb540qnqmz54gdad4z2wsakz5d26xvnnv1ksk6")))

(define-public crate-rufi_core-12.0.1 (c (n "rufi_core") (v "12.0.1") (h "0c7l10ffyqxl74q2cpc8j830cwvn14yin9xx111261v2ivdjq9j1")))

(define-public crate-rufi_core-12.0.2 (c (n "rufi_core") (v "12.0.2") (h "11fp8xv7hk2097xd124vaf8nakyy5aa1ybjm7zz9282j04516hid")))

(define-public crate-rufi_core-13.0.0 (c (n "rufi_core") (v "13.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m9y1dryll7p1bpw4h12f0jrf2rjijg3x8s95fsl3adigfifd3am")))

(define-public crate-rufi_core-13.0.1 (c (n "rufi_core") (v "13.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n4ynx57i7zy2dnarl1szw6g90m88p0acg9qhxafvaddzh1fmh39")))

(define-public crate-rufi_core-13.1.0 (c (n "rufi_core") (v "13.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1941a9mv3smpms4n33a3nj9bbryfs61pp8xhwmisi1z6qk71l0qn")))

(define-public crate-rufi_core-14.0.0 (c (n "rufi_core") (v "14.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kjkjg84nbc133xlcnd7n0xbzgvqk0x4mlf1jila10xhiycj2wrx")))

(define-public crate-rufi_core-15.0.0 (c (n "rufi_core") (v "15.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06q99s983y6l2sl563k0b3a9lqvcbhr4g58xg40cm0amnl8s6ih6")))

(define-public crate-rufi_core-15.1.0 (c (n "rufi_core") (v "15.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0354qvh6yq2zbzh85gqzif5fg1rsq7hlnk5h71vgf7kxilh6kx11")))

(define-public crate-rufi_core-15.1.1 (c (n "rufi_core") (v "15.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01kq8m7s2lg81fmdfcxw84myrnb0b8nwmakqx67xa10sqbxwd69i")))

