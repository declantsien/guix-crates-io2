(define-module (crates-io ru as ruast) #:use-module (crates-io))

(define-public crate-ruast-0.0.1 (c (n "ruast") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15im3bnazdy8hs5jbwbvypw0n438bkrmhy6slwjy0rmqzhqlddd9") (s 2) (e (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.2 (c (n "ruast") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rxyhk6wyk07ifmhpsnaf3hradhqmldi69av6081x1irv9vd3b9x") (s 2) (e (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.3 (c (n "ruast") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)))) (h "057c8avw98s49pkiy6byj7ww75q7w5dlr8f6nc8w1by8p8asxg8z") (s 2) (e (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.4 (c (n "ruast") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gh4f4p40dypgasg05yra661czq5zgag01pd0xjhcsg47z3y27cv") (s 2) (e (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.5 (c (n "ruast") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)))) (h "136536cr48am3w6f236qv1cm2s1sw9w4lxq55wmj3ngzsii0clws") (s 2) (e (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

