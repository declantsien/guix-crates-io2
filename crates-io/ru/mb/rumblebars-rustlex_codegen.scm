(define-module (crates-io ru mb rumblebars-rustlex_codegen) #:use-module (crates-io))

(define-public crate-rumblebars-rustlex_codegen-0.3.1 (c (n "rumblebars-rustlex_codegen") (v "0.3.1") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "quasi") (r "*") (o #t) (d #t) (k 0)) (d (n "quasi_codegen") (r "*") (o #t) (d #t) (k 1)) (d (n "syntex") (r "*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "*") (o #t) (d #t) (k 1)) (d (n "syntex_syntax") (r "*") (o #t) (d #t) (k 0)))) (h "1jglqldwp9a86qmrcfqfvzqj3h4hdqcv4r6khhz7jjmnihvrmgnc") (f (quote (("with-syntex" "quasi/with-syntex" "quasi_codegen/with-syntex" "syntex" "syntex_syntax")))) (y #t)))

