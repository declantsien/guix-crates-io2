(define-module (crates-io ru ly ruly2) #:use-module (crates-io))

(define-public crate-ruly2-0.1.0 (c (n "ruly2") (v "0.1.0") (d (list (d (n "lr_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0la8ccv8j2d1rpypwky6ymczh1a6nll645h2jh7mnf6ym45klf3n")))

(define-public crate-ruly2-0.1.1 (c (n "ruly2") (v "0.1.1") (d (list (d (n "lr_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0fa51nkmazkl0s0r3sqqi2xrmjrsx5f6fs6ljnhi6s7l4ncrjd5v")))

(define-public crate-ruly2-0.1.2 (c (n "ruly2") (v "0.1.2") (d (list (d (n "lr_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1k9zvd2g0bkl7x5cqzny09g7zslsk8q4nb83dy096y24flz8wb1q")))

(define-public crate-ruly2-0.1.3 (c (n "ruly2") (v "0.1.3") (d (list (d (n "lr_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0igcv2r71n85naipvbrgr0x8m5pp2ny11y5nyvf18fjkk0s7j1q1")))

(define-public crate-ruly2-0.1.4 (c (n "ruly2") (v "0.1.4") (d (list (d (n "lr_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0l0vv7p7wljfcs6477hqz15h1qlzp82pwqi7a6qz3q4kawm76nqr")))

(define-public crate-ruly2-0.1.5 (c (n "ruly2") (v "0.1.5") (d (list (d (n "lr_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "177kfi50n3qsw8y5gcw38w7f8125781da2l61lvy4h6nmndw0a3q")))

(define-public crate-ruly2-0.1.6 (c (n "ruly2") (v "0.1.6") (d (list (d (n "lr_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0nfswrj73k8vzj6cykqv23ghfgql26asqxjl362xg6wc9i4ir2p9")))

(define-public crate-ruly2-0.1.7 (c (n "ruly2") (v "0.1.7") (d (list (d (n "lr_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "05z5mxxxa865f7wca81x4yyxyyjzw1i2nfyhqlx1pzl721zid293")))

(define-public crate-ruly2-0.1.8 (c (n "ruly2") (v "0.1.8") (d (list (d (n "lr_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1n1qws2mnzahyw32vzmczm10kx3b88h5xjl3i0ihwqi49nr2vijn")))

