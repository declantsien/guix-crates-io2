(define-module (crates-io ru go rugo) #:use-module (crates-io))

(define-public crate-rugo-0.1.1 (c (n "rugo") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "02s1lj64f4vp8ly2knncv9dnxnzbrx626r127nb0csbd6iixhcds")))

(define-public crate-rugo-0.1.3 (c (n "rugo") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "072pgxyvqypijwyajy1yg51x46fsyimrvyz0840nhj1rm1bvihkl")))

