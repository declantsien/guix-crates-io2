(define-module (crates-io ru ma ruma-server-util) #:use-module (crates-io))

(define-public crate-ruma-server-util-0.1.0 (c (n "ruma-server-util") (v "0.1.0") (d (list (d (n "headers") (r "^0.3") (d #t) (k 0)) (d (n "ruma-common") (r "^0.11.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)) (d (n "yap") (r "^0.8.0") (d #t) (k 0)))) (h "1sw4x1rlz1s2y1hn54pzdafi44xl67qk641jb7gnklm3rlr188by") (r "1.64")))

(define-public crate-ruma-server-util-0.1.1 (c (n "ruma-server-util") (v "0.1.1") (d (list (d (n "headers") (r "^0.3") (d #t) (k 0)) (d (n "ruma-common") (r "^0.11.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)) (d (n "yap") (r "^0.8.0") (d #t) (k 0)))) (h "0xw442cs29zwap9bds3z8w0fvh1dsk5apbnw7083ck4inh72cyx7") (r "1.64")))

(define-public crate-ruma-server-util-0.2.0 (c (n "ruma-server-util") (v "0.2.0") (d (list (d (n "headers") (r "^0.3") (d #t) (k 0)) (d (n "ruma-common") (r "^0.12.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)) (d (n "yap") (r "^0.11.0") (d #t) (k 0)))) (h "0sr01j9qanlyz3nj8zvdk1blhhffhgk5p13x4jjimhii988fadh8") (r "1.70")))

(define-public crate-ruma-server-util-0.3.0 (c (n "ruma-server-util") (v "0.3.0") (d (list (d (n "headers") (r "^0.4.0") (d #t) (k 0)) (d (n "ruma-common") (r "^0.13.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)) (d (n "yap") (r "^0.12.0") (d #t) (k 0)))) (h "0pn2vzsw9q6s9f3jiq6f8vhbd9w1s5s2kxz5am092cgg8655dap1") (r "1.75")))

