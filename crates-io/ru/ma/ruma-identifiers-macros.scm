(define-module (crates-io ru ma ruma-identifiers-macros) #:use-module (crates-io))

(define-public crate-ruma-identifiers-macros-0.17.1 (c (n "ruma-identifiers-macros") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ruma-identifiers") (r "=0.17.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (d #t) (k 0)))) (h "1ibx42m1phjlq99rlzy2n6p1p007jdlyi4qbwi8k2d23yf1qj73n")))

(define-public crate-ruma-identifiers-macros-0.17.2 (c (n "ruma-identifiers-macros") (v "0.17.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (d #t) (k 0)))) (h "0nx16y1kfzgsv9qyw9ai23sbwxl0wvf45v9y24fdksy71smns1ih")))

(define-public crate-ruma-identifiers-macros-0.17.3 (c (n "ruma-identifiers-macros") (v "0.17.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (d #t) (k 0)))) (h "1mkcvav5c6cyyagnxf7faishhl1m2nyhk1rmfcq7kallwya7xxl5")))

(define-public crate-ruma-identifiers-macros-0.17.4 (c (n "ruma-identifiers-macros") (v "0.17.4") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (d #t) (k 0)))) (h "03p3qp12h4aajixcyk44q2bcid6fa4nmipysvcpy4w5cbak7wvwq")))

(define-public crate-ruma-identifiers-macros-0.18.0-alpha.1 (c (n "ruma-identifiers-macros") (v "0.18.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.2.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0cigzbrb27rl33rncjxzps0kpwb57m9iaavhl47z7pbpg5pj7216")))

(define-public crate-ruma-identifiers-macros-0.18.0 (c (n "ruma-identifiers-macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.2.1") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0npxkwlfmi32y4qa0grczsryrvlpzc960dryv1vg3nddqws2di3s")))

(define-public crate-ruma-identifiers-macros-0.18.1 (c (n "ruma-identifiers-macros") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.2.2") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "1klgnvs7za09ajw01g6b9s59s5k5v0sh0f0cgi3a3xm5q5xcm2qj")))

(define-public crate-ruma-identifiers-macros-0.19.0 (c (n "ruma-identifiers-macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.2.3") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "1bliqblbp51539j2g1053g5n9s9l03bl4sb7h1w3zwgpna5lnqch")))

(define-public crate-ruma-identifiers-macros-0.19.1 (c (n "ruma-identifiers-macros") (v "0.19.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.3.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "04mdsh56gsn8zx351l2cnslpbnjh95fiahgh816r8kikzpw9nzm7")))

(define-public crate-ruma-identifiers-macros-0.19.2 (c (n "ruma-identifiers-macros") (v "0.19.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.4.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "1nwxnpsfmwcbv1wvb75cghc350bn6l32vsi6x60mllzcd49a05kh")))

(define-public crate-ruma-identifiers-macros-0.19.3 (c (n "ruma-identifiers-macros") (v "0.19.3") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.4.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0jdsqn9mr5aajayfy4fhc3bjm5gjwq1mg9cgjnhlkinxycjpa4hx")))

(define-public crate-ruma-identifiers-macros-0.19.4 (c (n "ruma-identifiers-macros") (v "0.19.4") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.4.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0820grf08p8jp5lyx7y2wpz7zn3vlg9n7kr4778gi11a2dzbriz3")))

(define-public crate-ruma-identifiers-macros-0.20.0 (c (n "ruma-identifiers-macros") (v "0.20.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.5.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "1cc2m1sdp8m5bsk5r7h006mjigqza3bvl74mdkr3hmk0gynys267")))

(define-public crate-ruma-identifiers-macros-0.21.0 (c (n "ruma-identifiers-macros") (v "0.21.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.6.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "1lvbzzgx3wynizrmdq5zgz8j0k47bj97n9z38c55mdy34cmn3z3z")))

(define-public crate-ruma-identifiers-macros-0.22.0 (c (n "ruma-identifiers-macros") (v "0.22.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.6.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0z6g8jgajl9spp020cqz3q7xglh7pf6lfndix1vqcdrsir43hdsh")))

(define-public crate-ruma-identifiers-macros-0.22.1 (c (n "ruma-identifiers-macros") (v "0.22.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "ruma-identifiers-validation") (r "^0.7.0") (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)))) (h "0f7l1c6d0jchxb6j2axzzqzj127mzdz5kav7aq6nkjfzxh04hw8k")))

