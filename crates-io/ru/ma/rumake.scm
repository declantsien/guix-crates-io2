(define-module (crates-io ru ma rumake) #:use-module (crates-io))

(define-public crate-rumake-0.1.0 (c (n "rumake") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellwords") (r "^1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1ynzslhrbrpjzi6f0dkn9jz5ffvghzbp18ykc71jrnqprgmckh9a")))

