(define-module (crates-io ru ma rumage) #:use-module (crates-io))

(define-public crate-rumage-0.5.0 (c (n "rumage") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "178f7zzyj5j3lj3a5z8n3xl81lfrfbaylij3lh1yrzhkzlhyys79")))

(define-public crate-rumage-0.5.1 (c (n "rumage") (v "0.5.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1f5c7z01x8jw2g41vpikxdsrwimik2mgxp3sagqmv8d3829yp2z5")))

(define-public crate-rumage-0.5.2 (c (n "rumage") (v "0.5.2") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1anzx0li8760hip2vl7q92c04mzihvzykqkpigywb02y0cgf5lqk")))

(define-public crate-rumage-0.5.3 (c (n "rumage") (v "0.5.3") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1bw5rizaa5bcmjcsz7bzwmxqa6i1w83iylz50dv1xnykjsyf574l") (y #t)))

(define-public crate-rumage-0.5.4 (c (n "rumage") (v "0.5.4") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "04cisiirna6d59z5bdnm3y2p4c2bpx1vk530j0k5yf551gfai4g3")))

(define-public crate-rumage-0.5.5 (c (n "rumage") (v "0.5.5") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1gr4nmpd6scwwfw22ycpfnwp963ky1hmfvy7wwh7vyyyxr7wm06k")))

(define-public crate-rumage-0.5.6 (c (n "rumage") (v "0.5.6") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1z0x992c7n0x6d3v77wj976w1rzqmigwrzaia25zdbwrra5n0aki")))

(define-public crate-rumage-0.5.7 (c (n "rumage") (v "0.5.7") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1c2c7fy4bxgp268ph134pnk3vsf16rql0h3nkgp5m5qp4y5c3vxr")))

(define-public crate-rumage-0.5.8 (c (n "rumage") (v "0.5.8") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0q4j4iqiw8ycj6sr8j7pici22wfd9xjhfwk7c1ppf6ap8fqpgh4x")))

(define-public crate-rumage-0.5.9 (c (n "rumage") (v "0.5.9") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.14.0") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1v821cnbcich82x9ibf634x745xvnxg9cp2h00w99infn82jwkp5")))

