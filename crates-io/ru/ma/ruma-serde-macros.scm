(define-module (crates-io ru ma ruma-serde-macros) #:use-module (crates-io))

(define-public crate-ruma-serde-macros-0.0.0 (c (n "ruma-serde-macros") (v "0.0.0") (h "19slmbglwlpjrcgdzijmzjx5j5jgc8sh46cmir1c9ww709n00jx6")))

(define-public crate-ruma-serde-macros-0.3.0 (c (n "ruma-serde-macros") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xmfbxaxjmlpzplnyxh5ph0j83ak8rm0rs682vg5ggcjzf03qdbq")))

(define-public crate-ruma-serde-macros-0.3.1 (c (n "ruma-serde-macros") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05rr7mvmbg8lndls1yh3j3fms74g3njpf1jsmz5i76phq6fzkz5l")))

(define-public crate-ruma-serde-macros-0.3.2 (c (n "ruma-serde-macros") (v "0.3.2") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qys5avgpjysamvzw4b9igy7dkrbs6i31jr7dkaszvgyjlj4vpi8")))

(define-public crate-ruma-serde-macros-0.4.0 (c (n "ruma-serde-macros") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1byhz0bpf7nl0165hpcwsv6vnm84vsqy3b6r48avygjrw4z8yhdy")))

(define-public crate-ruma-serde-macros-0.4.1 (c (n "ruma-serde-macros") (v "0.4.1") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "0b6k34cy8k1p3wh8n57vr58c5ccaxhwh7widkrgvjzrfjrxssp02")))

(define-public crate-ruma-serde-macros-0.4.2 (c (n "ruma-serde-macros") (v "0.4.2") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "1k6pbg3p3wdkjd9k8qkrb9cphn3jba6bwgdish5ln6nsarqnxbzn")))

(define-public crate-ruma-serde-macros-0.5.0 (c (n "ruma-serde-macros") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "0a0x70bbxizqmiqlb3ibddirwyihxg8qybxwg56gj24b33prngi4")))

(define-public crate-ruma-serde-macros-0.6.0 (c (n "ruma-serde-macros") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "00rwyx0hrdpqz7pxb8n3rw7702xfklkj9jsf9fg9ph7bxpvdd36k")))

(define-public crate-ruma-serde-macros-0.6.1 (c (n "ruma-serde-macros") (v "0.6.1") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "0brq4bqqr9lpg58izsvsp8cnp3dawdx134aw9p38833i3yaddhk4")))

