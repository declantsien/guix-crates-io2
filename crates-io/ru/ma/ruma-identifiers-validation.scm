(define-module (crates-io ru ma ruma-identifiers-validation) #:use-module (crates-io))

(define-public crate-ruma-identifiers-validation-0.1.0 (c (n "ruma-identifiers-validation") (v "0.1.0") (d (list (d (n "strum") (r "^0.18.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "005glv3q46sn99wv5as32wg5hmxiycsmrxacnfn6w7yhwb91m7z1")))

(define-public crate-ruma-identifiers-validation-0.1.1 (c (n "ruma-identifiers-validation") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nv0zqsq5lhybckvfr5wmcp8pipqfs2hjgn9kvn9w33xacfzs4n1") (f (quote (("default" "serde"))))))

(define-public crate-ruma-identifiers-validation-0.2.0 (c (n "ruma-identifiers-validation") (v "0.2.0") (d (list (d (n "ruma-serde") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "0vgsaylbn0jki329b30kr47nsdykf0nxl9qkpsg0llxglnzbsjv3") (f (quote (("default" "serde"))))))

(define-public crate-ruma-identifiers-validation-0.2.1 (c (n "ruma-identifiers-validation") (v "0.2.1") (h "17lznkv8gj7zc72ggkdan8hqaa65m6a8h4r3af2v0zpji81bbxzv")))

(define-public crate-ruma-identifiers-validation-0.2.2 (c (n "ruma-identifiers-validation") (v "0.2.2") (h "11vm96nv8znyrk5j14haci12klldal9vjghcq2cpknw0rxxhv42c")))

(define-public crate-ruma-identifiers-validation-0.2.3 (c (n "ruma-identifiers-validation") (v "0.2.3") (h "0adw4f4br6ans5w4z350icpp0j2bb09zk5byix4g5afcib4f9x8y") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.2.4 (c (n "ruma-identifiers-validation") (v "0.2.4") (h "1ypkjrhzx301s7lqk5ygs23zdny0q56qs7x48ni2m09gzi6yld26") (f (quote (("serde") ("compat"))))))

(define-public crate-ruma-identifiers-validation-0.3.0 (c (n "ruma-identifiers-validation") (v "0.3.0") (h "19b9yp6qy4lsf889cpwb9fsl5h6cpzvbwd7cbh6wzk515fin9dpc") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.4.0 (c (n "ruma-identifiers-validation") (v "0.4.0") (h "0z1gckz29y3jsy3sp63qgsf5sb7mslpv75kk3cywkf6wqijv3plf") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.5.0 (c (n "ruma-identifiers-validation") (v "0.5.0") (h "1jxjpgrj2d9wp9cla6p1q6gzvinm6qz6ifs5iqkdbwnmnmzmwa22") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.6.0 (c (n "ruma-identifiers-validation") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1y9g0d44rlvwjpycpy6913cg45kilni6dzabkgzzbsl67ifc8w66") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.7.0 (c (n "ruma-identifiers-validation") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1wclayfb12aqlmm07g4bj3xq75lk5dx1ss6l6l08fz2kgn8pbijx") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.8.0 (c (n "ruma-identifiers-validation") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1lh3a4kywh0q2ncmfngxwzcvp6ir0lngkvc3ifgcz17isk6d1ai6") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.8.1 (c (n "ruma-identifiers-validation") (v "0.8.1") (d (list (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0fd1ad4y1f8pz2i3ks2x8lgpypdc3jy72naxy938gpax3g8b3hvl") (f (quote (("compat"))))))

(define-public crate-ruma-identifiers-validation-0.9.0 (c (n "ruma-identifiers-validation") (v "0.9.0") (d (list (d (n "js_int") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0xa2rkrm0l08yrm7ck7414xvkayf9pbpkivram1qfnj62qnwdfpa") (f (quote (("compat")))) (r "1.60")))

(define-public crate-ruma-identifiers-validation-0.9.1 (c (n "ruma-identifiers-validation") (v "0.9.1") (d (list (d (n "js_int") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0wmvna312zxld87v1ij70rvqqspydk1r3kfjgk849bqi8frxmvzb") (f (quote (("compat")))) (r "1.64")))

(define-public crate-ruma-identifiers-validation-0.9.2 (c (n "ruma-identifiers-validation") (v "0.9.2") (d (list (d (n "js_int") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1j3i9z2fqvsp442mdjjx17k8k7bijszmxs5s5z1lfa6hkm4i4lf2") (f (quote (("compat-user-id") ("compat-key-id") ("compat-arbitrary-length-ids")))) (r "1.70")))

(define-public crate-ruma-identifiers-validation-0.9.3 (c (n "ruma-identifiers-validation") (v "0.9.3") (d (list (d (n "js_int") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0hcviq2jzc1qcdxqh78wklb273sabv6d26wh41ygbwklj8jx32mz") (f (quote (("compat-user-id") ("compat-key-id") ("compat-arbitrary-length-ids")))) (r "1.70")))

(define-public crate-ruma-identifiers-validation-0.9.4 (c (n "ruma-identifiers-validation") (v "0.9.4") (d (list (d (n "js_int") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ynlhlwxh2ypwsk30izk857k1c4ahn0scgv7p49mqf21cp170p1b") (f (quote (("compat-user-id") ("compat-key-id") ("compat-arbitrary-length-ids")))) (y #t) (r "1.75")))

(define-public crate-ruma-identifiers-validation-0.9.5 (c (n "ruma-identifiers-validation") (v "0.9.5") (d (list (d (n "js_int") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1fpzyjwh11xqa4y6ibm1q7bddd6kk9xwbbhh1vhd87lhyms8k8wz") (f (quote (("compat-user-id") ("compat-key-id") ("compat-arbitrary-length-ids")))) (r "1.75")))

