(define-module (crates-io ru ma ruma-html) #:use-module (crates-io))

(define-public crate-ruma-html-0.1.0 (c (n "ruma-html") (v "0.1.0") (d (list (d (n "as_variant") (r "^1.2.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std" "attributes"))) (k 0)) (d (n "wildmatch") (r "^2.0.0") (d #t) (k 0)))) (h "0nrsm1rp1psk97kvn2cjpn52a0g2kv42i9ab2zi5z5m6n54s2x4n") (r "1.70")))

(define-public crate-ruma-html-0.2.0 (c (n "ruma-html") (v "0.2.0") (d (list (d (n "as_variant") (r "^1.2.0") (d #t) (k 0)) (d (n "assert_matches2") (r "^0.1.0") (d #t) (k 2)) (d (n "html5ever") (r "^0.27.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "ruma-common") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std" "attributes"))) (k 0)) (d (n "wildmatch") (r "^2.0.0") (d #t) (k 0)))) (h "0zrxc5cgg3p3pqpsfra7s4yla8db1hzplk1dzk8v8pysg63r8vfb") (s 2) (e (quote (("matrix" "dep:ruma-common")))) (r "1.75")))

