(define-module (crates-io ru ma ruma-federation) #:use-module (crates-io))

(define-public crate-ruma-federation-0.0.0 (c (n "ruma-federation") (v "0.0.0") (h "1k806na2mvqcry4sg02jagf07l24qhijw8k9ly09y3xxl3xk382m")))

(define-public crate-ruma-federation-0.0.1 (c (n "ruma-federation") (v "0.0.1") (h "1sp1p922q7yl4kiskndhgkl2x0vcsdz5v7riswrl3rknr7h4p61s")))

(define-public crate-ruma-federation-0.0.2 (c (n "ruma-federation") (v "0.0.2") (h "1icx4ibvzn6p1skp668vqx8gzqqyn4zh9amwa6pvgsw852gxn56b")))

(define-public crate-ruma-federation-0.0.3 (c (n "ruma-federation") (v "0.0.3") (h "13ywagls65kl5ww4dgnw75h0d8hma072k7cy7k5cw475j1syipij")))

