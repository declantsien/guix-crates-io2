(define-module (crates-io ru gg ruggine_ulid) #:use-module (crates-io))

(define-public crate-ruggine_ulid-0.1.0 (c (n "ruggine_ulid") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "rusty_ulid") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1dzqxj4gbx8qh6134227bllz5qyydlk0sq9kv0ighp9886ypxwz0")))

(define-public crate-ruggine_ulid-0.1.1 (c (n "ruggine_ulid") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "rusty_ulid") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.7.0") (d #t) (k 2)))) (h "17nf7qbljv4qphl85l4yqyy6km6g8ffa342hbjpb5bjmzrd20lmq")))

(define-public crate-ruggine_ulid-0.1.2 (c (n "ruggine_ulid") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "rusty_ulid") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0ryplf17f74wahikbj9dscqy7lbbrkzv0gyf9dms9kypmgfqivbl")))

