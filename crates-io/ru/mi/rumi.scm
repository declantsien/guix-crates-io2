(define-module (crates-io ru mi rumi) #:use-module (crates-io))

(define-public crate-rumi-0.2.0 (c (n "rumi") (v "0.2.0") (d (list (d (n "basebits") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.26") (d #t) (k 0)))) (h "1iwx10b4axnhp9l8zfzaa9s3bysifk8pxpjwpcbw7vmi46w551m2")))

(define-public crate-rumi-0.2.1 (c (n "rumi") (v "0.2.1") (d (list (d (n "basebits") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "rust-htslib") (r "^0.26") (d #t) (k 0)))) (h "17kijnvgl988bcqvk6cx6akjq4lb2fp1f3bvbaf4i972rqi9gfh7")))

