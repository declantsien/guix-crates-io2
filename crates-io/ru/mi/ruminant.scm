(define-module (crates-io ru mi ruminant) #:use-module (crates-io))

(define-public crate-ruminant-0.1.0 (c (n "ruminant") (v "0.1.0") (h "12csip2krl60frnp4sgypbrcz4blmn2sn8flw24w4ln46xm7rqi9")))

(define-public crate-ruminant-0.2.0 (c (n "ruminant") (v "0.2.0") (h "06m5rljh440mk0sgff2mpgcv2afx94midivygif594pkda841a7k")))

(define-public crate-ruminant-0.3.0 (c (n "ruminant") (v "0.3.0") (h "0vkdzb5mv8bcc3ncwqv2jlk80mzm02mnzq1c9y7qlgxrdgjz2a3s")))

(define-public crate-ruminant-0.4.0 (c (n "ruminant") (v "0.4.0") (h "1gj52hjpxyxg5wllmgsi0c5rrnw7inr2bvqhka1i9gg14gl8vbg2")))

(define-public crate-ruminant-0.5.0 (c (n "ruminant") (v "0.5.0") (h "19nbiw8hgdcbblwza5icqnfqyzrrx7dsp2jf6la7khgd1dc43pwc")))

(define-public crate-ruminant-0.6.0 (c (n "ruminant") (v "0.6.0") (h "1m17v70r4kgl573p70bnsfjn93lxll6ilirm1f0snp8wx6v468cw")))

(define-public crate-ruminant-0.7.0 (c (n "ruminant") (v "0.7.0") (h "0hsbakj7kq80jkfylsix5pz9y5ciyvikm3y9f6sz7zyv3z3pykxb")))

(define-public crate-ruminant-0.8.0 (c (n "ruminant") (v "0.8.0") (h "1086dvffafv2cxzl448sf9p5r4nzvqi0b4fjpq2ybf7xhfds3br3")))

(define-public crate-ruminant-0.9.0 (c (n "ruminant") (v "0.9.0") (h "1hsgf8jwjqwkmcwgb2scp1pn0q2pdgdls72bkjfsk8yki76gywf0")))

(define-public crate-ruminant-0.10.0 (c (n "ruminant") (v "0.10.0") (h "03hcqr3pihhri3hhaim46iaacc1r6v853qcg0rlw02bgq43n2ipy")))

(define-public crate-ruminant-0.11.0 (c (n "ruminant") (v "0.11.0") (h "1wwkwsl0rl7ahqq2h43p1liscjdilsrzc7b7njh7jd8sfvw5fqw9")))

(define-public crate-ruminant-0.12.0 (c (n "ruminant") (v "0.12.0") (h "1s4qziv6vbp5l9qnls8xjaj7z4igyi9dkhm88va6aspvvfs4cnd5")))

(define-public crate-ruminant-0.13.0 (c (n "ruminant") (v "0.13.0") (h "0fr70jfpsmi8mi1alhnnjbi8kpd0vvl35nwsmf8r65ys1wbiv0fk")))

(define-public crate-ruminant-0.14.0 (c (n "ruminant") (v "0.14.0") (h "1sqynaj6v3m5ppammgj6zdaqmlhiclc112lf9vhxy8gasrfd1inf")))

(define-public crate-ruminant-0.15.0 (c (n "ruminant") (v "0.15.0") (h "06m19lpp5vn8zin570y2bh2yb5dpvaj9j58a73xwlggqcqmdjicw")))

(define-public crate-ruminant-0.16.0 (c (n "ruminant") (v "0.16.0") (h "1k04y8jlw40iyzc5rhh8ymcilpwahbbjagaa1yjh60b9575bnxl5")))

(define-public crate-ruminant-0.17.0 (c (n "ruminant") (v "0.17.0") (h "0zjphyp82jj19b9dl6p4jgpk7z250s49yz3677c4wa6ga4vhm02w")))

