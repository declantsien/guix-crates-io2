(define-module (crates-io ru mi rumio) #:use-module (crates-io))

(define-public crate-rumio-0.1.0 (c (n "rumio") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0vq83hp375gyfjx1gw9qxkl9iasf15k2492wbr15ky6vjs87zl11") (f (quote (("example_generated")))) (y #t)))

(define-public crate-rumio-0.1.1 (c (n "rumio") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0l81mczv4wcr45m2vnhagz0cf5dnqg1iinqfbpm2l9q9mm133awk") (f (quote (("example_generated"))))))

(define-public crate-rumio-0.1.2 (c (n "rumio") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defile") (r "^0.1") (d #t) (k 0)))) (h "128d1aaxjd7xvdgqvrvc6ky0xxbxgznmgiip98kqg8kcqbib50il") (f (quote (("example_generated"))))))

(define-public crate-rumio-0.1.3 (c (n "rumio") (v "0.1.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defile") (r "^0.1") (d #t) (k 0)))) (h "15d2p8vkqxcxz1ind3i909714l9ki1zgyg7qnbd1zcklcn3xg6rl") (f (quote (("example_generated")))) (y #t)))

(define-public crate-rumio-0.1.4 (c (n "rumio") (v "0.1.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defile") (r "^0.1") (d #t) (k 0)))) (h "09jkw7rxqaj760s6n301pgz9rsv6iqs5hrlvzg2ni5scqvazq2dw") (f (quote (("example_generated"))))))

(define-public crate-rumio-0.2.0 (c (n "rumio") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "defile") (r "^0.1") (d #t) (k 0)))) (h "0al118vffk92p7cl9y1y5ksds7pi1pjgbr8fa4pafifjdi7l7pj9") (f (quote (("example_generated"))))))

