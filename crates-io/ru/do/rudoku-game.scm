(define-module (crates-io ru do rudoku-game) #:use-module (crates-io))

(define-public crate-rudoku-game-0.1.0 (c (n "rudoku-game") (v "0.1.0") (d (list (d (n "box_drawing") (r "^0.1.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rudoku-core") (r "^0.1") (d #t) (k 0)))) (h "16a1rcdgl8pn5lz4gg1pkbhdb65yvgvk1pkccwifrcf4s2bxsgsn")))

(define-public crate-rudoku-game-0.1.1 (c (n "rudoku-game") (v "0.1.1") (d (list (d (n "box_drawing") (r "^0.1.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rudoku-core") (r "^0.1") (d #t) (k 0)))) (h "1v831lhkxy7h30gzmfb1acpjcq5i7bihvc854xdsbpld7zhkaf4m")))

(define-public crate-rudoku-game-0.1.2 (c (n "rudoku-game") (v "0.1.2") (d (list (d (n "box_drawing") (r "^0.1.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rudoku-core") (r "^0.2") (d #t) (k 0)))) (h "0xmxp3cdmk4l6bibqpq54nlg73jn3lf0x1izc9201bllh26gc36a")))

(define-public crate-rudoku-game-0.1.3 (c (n "rudoku-game") (v "0.1.3") (d (list (d (n "box_drawing") (r "^0.1.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rudoku-core") (r "^0.2") (d #t) (k 0)))) (h "1w5js1knpg91al9xm017511gqw19li61jgwyxs8z2a3646x79in8")))

(define-public crate-rudoku-game-0.1.4 (c (n "rudoku-game") (v "0.1.4") (d (list (d (n "box_drawing") (r "^0.1.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rudoku-core") (r "^0.2") (d #t) (k 0)))) (h "0m14a2am00carjbcl2m1f9c0cqvf6nmdswfiw3jf0sjwwfbrc0z8")))

