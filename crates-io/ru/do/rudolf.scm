(define-module (crates-io ru do rudolf) #:use-module (crates-io))

(define-public crate-rudolf-0.0.1 (c (n "rudolf") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)))) (h "0784bzykpww6gi66nfj1cy0b0c1s4lngjrs4mjqzhpvykphmsyrz")))

