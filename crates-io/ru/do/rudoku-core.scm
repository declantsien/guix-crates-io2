(define-module (crates-io ru do rudoku-core) #:use-module (crates-io))

(define-public crate-rudoku-core-0.1.0 (c (n "rudoku-core") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1dh88rn1i03n39fii80zqhmn9d8izz685v23c96hhand6f9f7rca")))

(define-public crate-rudoku-core-0.1.1 (c (n "rudoku-core") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1pnb57jmrh6jz80hq7fq1im0i86fl33vhybn0d70mkp2a92ardi6")))

(define-public crate-rudoku-core-0.2.0 (c (n "rudoku-core") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0b39v9b7g5fc8wknarldjb6sp3d6y23fb1jyz5g1x40lgx2j61fw")))

