(define-module (crates-io ru xn ruxnasm) #:use-module (crates-io))

(define-public crate-ruxnasm-0.1.0 (c (n "ruxnasm") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "09qmp589b4r9s6bjm8i29cvqz72jqidv41snxh5k4hbixgyzjjqz") (f (quote (("default" "codespan-reporting"))))))

(define-public crate-ruxnasm-0.1.1 (c (n "ruxnasm") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "167lfajipl886hss1ix3h8r3qlkg0c2sq8shl0nakdhmwhxpwcax") (f (quote (("default" "codespan-reporting"))))))

(define-public crate-ruxnasm-0.1.2 (c (n "ruxnasm") (v "0.1.2") (d (list (d (n "codespan-reporting") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "11ssh90fpl463r2rfgdikakl7gra3q73ppxbp71hysk9npif5ba5") (f (quote (("default" "codespan-reporting"))))))

(define-public crate-ruxnasm-0.2.0 (c (n "ruxnasm") (v "0.2.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)))) (h "1a7d890nc4vn5pi7319zhdmddm16ix5fxy1l4pdpv8gyj699dxfd") (f (quote (("default" "bin") ("bin" "codespan-reporting"))))))

