(define-module (crates-io ru xt ruxt_macros) #:use-module (crates-io))

(define-public crate-ruxt_macros-0.1.0 (c (n "ruxt_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1y125rnhv3pg7jlmkb3n8z9qwpmawzc2sjyyyrpr70ybj6hyaiw4")))

(define-public crate-ruxt_macros-0.1.1 (c (n "ruxt_macros") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "072lkxzji5rilppk6d24isfdv9ch28gydbgmq8akdlqipg915qqr")))

(define-public crate-ruxt_macros-0.1.2 (c (n "ruxt_macros") (v "0.1.2") (d (list (d (n "actix-web") (r "^4.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "12nmmfj698k3a5vhq6y5f7p4sdjw7mnynbhd8x55v3607pqrks56")))

(define-public crate-ruxt_macros-0.1.3 (c (n "ruxt_macros") (v "0.1.3") (d (list (d (n "actix-web") (r "^4.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0gkvq9brb4a89jghzwk6520d2lw6s9dajq46bfyqs01aihcr9y8l")))

(define-public crate-ruxt_macros-0.1.4 (c (n "ruxt_macros") (v "0.1.4") (d (list (d (n "actix-web") (r "^4.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "01kf3ii4d4fd3xqq9kmjzz9brx6kjcgkr71z9rrpb2bahc6v7zi2")))

