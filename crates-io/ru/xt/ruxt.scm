(define-module (crates-io ru xt ruxt) #:use-module (crates-io))

(define-public crate-ruxt-0.1.0 (c (n "ruxt") (v "0.1.0") (d (list (d (n "ruxt_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1lmrp40hdrpy6fcsbb1vv3j1cisp72krijhpch1fqrp0sclc8rr6")))

(define-public crate-ruxt-0.1.1 (c (n "ruxt") (v "0.1.1") (d (list (d (n "ruxt_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1ry9idk3hydn8ksifgxa8rhdvi4q65jy1z7gwxw2aqb071dxq2hy")))

(define-public crate-ruxt-0.1.2 (c (n "ruxt") (v "0.1.2") (d (list (d (n "ruxt_macros") (r "^0.1.4") (d #t) (k 0)))) (h "0p915vsy25w754lw1w028hjzhqj65x7mffkypngkw638n2dqm7bc")))

