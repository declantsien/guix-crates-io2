(define-module (crates-io ru na runa) #:use-module (crates-io))

(define-public crate-runa-0.1.0 (c (n "runa") (v "0.1.0") (h "1f3kxpl9gldp6wils4xgx4wwqrf2byczn5jc7bjv26z2bd6lv2fh")))

(define-public crate-runa-0.1.1 (c (n "runa") (v "0.1.1") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "fermium") (r "^20014.4.2") (d #t) (k 0)) (d (n "mira") (r "^0.1.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0m2ny99c18zgn4w8fhyg7dphlxv3pph0gqmvvwlbjq4wqs6apxm9")))

