(define-module (crates-io ru na runa-wayland-scanner) #:use-module (crates-io))

(define-public crate-runa-wayland-scanner-0.1.0 (c (n "runa-wayland-scanner") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "codegen") (r "^0.1.0") (d #t) (k 0) (p "runa-wayland-scanner-codegen")) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "macros") (r "^0.1.0") (d #t) (k 0) (p "runa-wayland-scanner-macros")) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "runa-io-traits") (r "^0.0.1-alpha1") (d #t) (k 0)) (d (n "runa-wayland-types") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "09z9vpqvkbvr9shzizxgr79svzy5n9v3w79zbkhaz491r3abpvgi")))

