(define-module (crates-io ru na runall) #:use-module (crates-io))

(define-public crate-runall-0.1.0 (c (n "runall") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0px444qg72lc8w5r6las5x3y13wqshlzw9kf1gsnr5iaql4fzqsp")))

(define-public crate-runall-0.1.1 (c (n "runall") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)))) (h "1rgn3fjywngmd8g94651qqiqk2y3qlp9ghdflz7glv22rcv17a75")))

