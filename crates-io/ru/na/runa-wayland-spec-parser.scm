(define-module (crates-io ru na runa-wayland-spec-parser) #:use-module (crates-io))

(define-public crate-runa-wayland-spec-parser-0.1.0 (c (n "runa-wayland-spec-parser") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "03h6ryzqdgqhjascl2qhlwvpvwd9342211csnipc8z2m1klfxia0")))

