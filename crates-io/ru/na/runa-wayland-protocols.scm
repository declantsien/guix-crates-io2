(define-module (crates-io ru na runa-wayland-protocols) #:use-module (crates-io))

(define-public crate-runa-wayland-protocols-0.1.0 (c (n "runa-wayland-protocols") (v "0.1.0") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "runa-wayland-scanner") (r "^0.1.0") (d #t) (k 0)) (d (n "runa-wayland-scanner") (r "^0.1.0") (d #t) (k 1)) (d (n "runa-wayland-spec-parser") (r "^0.1.0") (d #t) (k 1)))) (h "1439jn48dh7nfbgqg5hnxav0wp5bm2g7br9pk300zcxm809c1ygz")))

