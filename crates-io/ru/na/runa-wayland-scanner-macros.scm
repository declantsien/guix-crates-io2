(define-module (crates-io ru na runa-wayland-scanner-macros) #:use-module (crates-io))

(define-public crate-runa-wayland-scanner-macros-0.1.0 (c (n "runa-wayland-scanner-macros") (v "0.1.0") (d (list (d (n "codegen") (r "^0.1.0") (d #t) (k 0) (p "runa-wayland-scanner-codegen")) (d (n "parser") (r "^0.1.0") (d #t) (k 0) (p "runa-wayland-spec-parser")) (d (n "proc-macro-error") (r "^1.0.4") (f (quote ("syn-error"))) (k 0)) (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0i97pglsnvikg2jm1c057pgng9ci051bqvcsf29c4lz5iy9h3hxn")))

