(define-module (crates-io ru co rucola) #:use-module (crates-io))

(define-public crate-rucola-0.1.0 (c (n "rucola") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rv56m771a876jyyzgca6b35ab284fc0b2blbi30lxizdampjdkp") (f (quote (("serialize") ("deny_unknown_fields") ("default" "debug_attr") ("debug_attr")))) (y #t)))

(define-public crate-rucola-0.1.1 (c (n "rucola") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "06qjqaaav2cqss60vk3rhbr0jzayrhj7sn1ww84vhxn4zp5kwygx") (f (quote (("serialize") ("deny_unknown_fields") ("default" "debug_attr") ("debug_attr")))) (y #t)))

(define-public crate-rucola-0.1.2 (c (n "rucola") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "14hz878y9bgsmgi5a46m8r1ibx5xv1ahmkvsqcdwbnb4vq9c5y45") (f (quote (("serialize") ("deny_unknown_fields") ("default" "debug_attr") ("debug_attr")))) (y #t)))

