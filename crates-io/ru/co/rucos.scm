(define-module (crates-io ru co rucos) #:use-module (crates-io))

(define-public crate-rucos-0.1.0 (c (n "rucos") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "1mw1ms4s4whbb1armm8sliybl28zxfci19yyi497iyazvr1m238m")))

(define-public crate-rucos-0.1.1 (c (n "rucos") (v "0.1.1") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)))) (h "0w2z2nmz7cqc7r7rpckwgqjd93clsz2rcy5dikhg3hjalmnqfqk3")))

