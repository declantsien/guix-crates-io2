(define-module (crates-io ru bb rubble-nrf5x) #:use-module (crates-io))

(define-public crate-rubble-nrf5x-0.0.4 (c (n "rubble-nrf5x") (v "0.0.4") (d (list (d (n "nrf51-hal") (r "^0.12") (o #t) (k 0)) (d (n "nrf52810-hal") (r "^0.12") (o #t) (k 0)) (d (n "nrf52811-hal") (r "^0.12") (o #t) (k 0)) (d (n "nrf52832-hal") (r "^0.12") (o #t) (k 0)) (d (n "nrf52833-hal") (r "^0.12") (o #t) (k 0)) (d (n "nrf52840-hal") (r "^0.12") (o #t) (k 0)) (d (n "rubble") (r "^0.0.4") (k 0)))) (h "0xsw5qmjd1210f5n04rijid1x2cb3zgcf6jkn2rmwgy300jslj6s") (f (quote (("52840" "nrf52840-hal") ("52833" "nrf52833-hal") ("52832" "nrf52832-hal") ("52811" "nrf52811-hal") ("52810" "nrf52810-hal") ("51" "nrf51-hal"))))))

