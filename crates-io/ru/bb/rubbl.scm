(define-module (crates-io ru bb rubbl) #:use-module (crates-io))

(define-public crate-rubbl-0.1.2 (c (n "rubbl") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rubbl_core") (r "^0.1.2") (d #t) (k 0)))) (h "1bkr8v3kih78rq904ggmdpxznsf4mkyl3gfhrq1xj4hmzxs7b2bn")))

(define-public crate-rubbl-0.2.2 (c (n "rubbl") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rubbl_core") (r "^0.2.0") (d #t) (k 0)))) (h "1iph6gxgc2yh93h8x8bm81xhwxi3vsykxp01cqbppbxgl34bfj21")))

(define-public crate-rubbl-0.3.0 (c (n "rubbl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rubbl_core") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "1yy6q59q6bnhzmax3fbfcgnw5bg4f2cm9chh5x469hv6nyxzrxg7")))

