(define-module (crates-io ru bb rubbl_fits) #:use-module (crates-io))

(define-public crate-rubbl_fits-0.2.2 (c (n "rubbl_fits") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rubbl_core") (r "^0.2.0") (d #t) (k 0)) (d (n "rubbl_visdata") (r "^0.2.0") (d #t) (k 0)))) (h "1rhgbmf72xrp9jw0bl654fp8yciyks846pi9bqk9f48hhbaamnr7")))

(define-public crate-rubbl_fits-0.2.3 (c (n "rubbl_fits") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rubbl_core") (r "^0.2.0") (d #t) (k 0)) (d (n "rubbl_visdata") (r "^0.2.0") (d #t) (k 0)))) (h "0mh5bnvl4k9fjzwrqqhhzris4ddgyqhmybm350x8pys26kwjln02")))

(define-public crate-rubbl_fits-0.2.4 (c (n "rubbl_fits") (v "0.2.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rubbl_core") (r "^0.2.0") (d #t) (k 0)) (d (n "rubbl_visdata") (r "^0.2.0") (d #t) (k 0)))) (h "14kwjzshyf586d61pg3rkjb66mz1garwd5x9i8g2hlkh2jn8nwkz")))

(define-public crate-rubbl_fits-0.3.0 (c (n "rubbl_fits") (v "0.3.0") (d (list (d (n "rubbl_core") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "rubbl_visdata") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.0") (d #t) (k 2)) (d (n "clap") (r "^4.0.26") (f (quote ("cargo"))) (d #t) (k 2)))) (h "1v8p5xkrggqyyrfpxxf4jn2qaz2xfpsanxbvdg9wbyi3la83vkp8")))

