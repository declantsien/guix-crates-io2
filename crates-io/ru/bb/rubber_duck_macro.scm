(define-module (crates-io ru bb rubber_duck_macro) #:use-module (crates-io))

(define-public crate-rubber_duck_macro-0.1.0 (c (n "rubber_duck_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut" "visit" "extra-traits"))) (d #t) (k 0)))) (h "12q9x1462l21ldlx0d62v3kz1f3j0vcxlsccn4cw9ilhzrdy4qwj")))

(define-public crate-rubber_duck_macro-0.2.0 (c (n "rubber_duck_macro") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut" "visit" "extra-traits" "printing" "parsing"))) (d #t) (k 0)))) (h "14qyy14dbfwmx8inl564q2fckrmm4i4hjvci2i6q40dzhf6dskrg") (f (quote (("nightly" "proc-macro2/nightly") ("default"))))))

