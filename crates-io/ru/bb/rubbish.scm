(define-module (crates-io ru bb rubbish) #:use-module (crates-io))

(define-public crate-rubbish-0.1.0 (c (n "rubbish") (v "0.1.0") (d (list (d (n "bincode") (r "^0.6.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "136m7xrmmkhc0ydjsnc85ci0x1r86lijlcx2slshsy9d3jwmgdrr")))

