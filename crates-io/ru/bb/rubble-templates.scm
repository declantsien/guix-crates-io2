(define-module (crates-io ru bb rubble-templates) #:use-module (crates-io))

(define-public crate-rubble-templates-0.1.0 (c (n "rubble-templates") (v "0.1.0") (h "074fxi58b5b1jjwz8kazr3xpyii5qgvwnddpmqfpcxlipgqjniki")))

(define-public crate-rubble-templates-0.1.1 (c (n "rubble-templates") (v "0.1.1") (h "0pxk1iixzk1c32wk6aj453lpaq6ck0pnw7zdk7d6wgx2kjwijz09")))

(define-public crate-rubble-templates-0.1.2 (c (n "rubble-templates") (v "0.1.2") (h "0fnrkpdl4pd8h71gd0h1zh1rrj4simq5yz2vb977fdmwf2bz8iwx")))

(define-public crate-rubble-templates-0.1.3 (c (n "rubble-templates") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)))) (h "0bg9yq1q3m9whllnnpm9mzr8cx3hjkrbpmjyzc3y4x0f11afy8m4")))

(define-public crate-rubble-templates-0.2.0 (c (n "rubble-templates") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "rubble-templates-core") (r "^0.2.0") (f (quote ("ast" "evaluator" "compiler"))) (d #t) (k 0)) (d (n "rubble-templates-evaluators") (r "^0.2.0") (f (quote ("simple"))) (d #t) (k 0)))) (h "0gg3nqk887a0dc4dhakhlhniib9grcjznq06zbqvrr9frj537pd6")))

(define-public crate-rubble-templates-0.2.1 (c (n "rubble-templates") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "rubble-templates-core") (r "^0.2.0") (f (quote ("ast" "evaluator" "compiler"))) (d #t) (k 0)) (d (n "rubble-templates-evaluators") (r "^0.2.1") (f (quote ("simple"))) (d #t) (k 0)))) (h "04rxzyr0lic9x1mv37z08ix443xrwxx1sm7g2ylx37wf3g4rw0z2")))

