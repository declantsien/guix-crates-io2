(define-module (crates-io ru bb rubber_duck) #:use-module (crates-io))

(define-public crate-rubber_duck-0.1.0 (c (n "rubber_duck") (v "0.1.0") (d (list (d (n "rubber_duck_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0m0ayz5n7dpcf884b2k1mf3qm65armdv8gyyk3303qaaqhy99v6v")))

(define-public crate-rubber_duck-0.2.0 (c (n "rubber_duck") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rubber_duck_macro") (r "^0.2.0") (d #t) (k 0)))) (h "1480vncvs2s5yrwlskq410y07j1r0c0indrj0lzg6kb8mm5qzcj1") (f (quote (("nightly" "rubber_duck_macro/nightly") ("default"))))))

(define-public crate-rubber_duck-0.2.1 (c (n "rubber_duck") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rubber_duck_macro") (r "^0.2.0") (d #t) (k 0)))) (h "117nrxywram9xmhzhb15i48vmkggi7i42f0qni8078i7myghh9gk") (f (quote (("nightly" "rubber_duck_macro/nightly") ("default"))))))

(define-public crate-rubber_duck-0.2.2 (c (n "rubber_duck") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rubber_duck_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0kqkhz3c7k3x3ssrza5wxylii6kjx33ygvi76iw0rg2lch8z17zl") (f (quote (("nightly" "rubber_duck_macro/nightly") ("default"))))))

