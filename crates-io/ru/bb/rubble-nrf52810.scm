(define-module (crates-io ru bb rubble-nrf52810) #:use-module (crates-io))

(define-public crate-rubble-nrf52810-0.0.1 (c (n "rubble-nrf52810") (v "0.0.1") (d (list (d (n "nrf52810-hal") (r "^0.8.0") (d #t) (k 0)) (d (n "rubble") (r "^0.0.1") (k 0)))) (h "0pddb5pvjvhvbcrc5adxikb86a3w627l8mnxcyczz0jbrvz9ni1m")))

(define-public crate-rubble-nrf52810-0.0.2 (c (n "rubble-nrf52810") (v "0.0.2") (d (list (d (n "nrf52810-hal") (r "^0.8.0") (d #t) (k 0)) (d (n "rubble") (r "^0.0.2") (k 0)))) (h "0yl7nfais8d02zfbyx6j5zds5wrsgscv3l38dfz9wir0vmw2lm7j")))

(define-public crate-rubble-nrf52810-0.0.3 (c (n "rubble-nrf52810") (v "0.0.3") (h "1h54k8jykhdcq80scdzx9mj49c0ab52y4cl0229x68j8g3bq5cl5")))

