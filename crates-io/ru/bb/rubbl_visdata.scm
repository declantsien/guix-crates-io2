(define-module (crates-io ru bb rubbl_visdata) #:use-module (crates-io))

(define-public crate-rubbl_visdata-0.2.1 (c (n "rubbl_visdata") (v "0.2.1") (d (list (d (n "rubbl_core") (r "^0.2.0") (d #t) (k 0)))) (h "11h5aqyh4wvy6rn9x18igh3kd45jkgcfdd3bddi95aw9cnvy39kj")))

(define-public crate-rubbl_visdata-0.3.0 (c (n "rubbl_visdata") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "rubbl_core") (r ">=0.2.0, <1") (d #t) (k 0)))) (h "0zpnnrx6k8sfmql6bw5vi7rbd7clr4bppgf1i2s5dfk142wgibz0")))

