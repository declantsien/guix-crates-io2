(define-module (crates-io ru bb rubble_tea) #:use-module (crates-io))

(define-public crate-rubble_tea-0.1.0 (c (n "rubble_tea") (v "0.1.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "04sgkpyfj64gfy3y4vzxwfdvqwr19y3p0g7vam3km5h7f80653ip")))

(define-public crate-rubble_tea-0.2.0 (c (n "rubble_tea") (v "0.2.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0i5kqskpblacqj1sy98wly2iswqm24wadjzzfycxv7nspjzyvlfy")))

(define-public crate-rubble_tea-1.0.0 (c (n "rubble_tea") (v "1.0.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "01m6i5fv37vk2hhf6n7xdgwvykdiynwsvmkv7n88swz5ai03xqlw")))

(define-public crate-rubble_tea-3.0.0 (c (n "rubble_tea") (v "3.0.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1bccd30arijjrdz1r8frqhbp494x5s59mpfk0h0vrqwyr5piipg6")))

(define-public crate-rubble_tea-4.0.0 (c (n "rubble_tea") (v "4.0.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1m3qwrgh7nlndp70kgxl47mlwvj1lslcbpcpszxi80hanwg73hh4")))

(define-public crate-rubble_tea-4.0.1 (c (n "rubble_tea") (v "4.0.1") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "11nmxl70kg6w21b5q7v4k1chkfh7n73n4c11fnaxdql0m7vkc8jb")))

(define-public crate-rubble_tea-4.1.0 (c (n "rubble_tea") (v "4.1.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0yk3ndjgbwci49sgl30q0gjr7avw2sgkyw7pgfxk9v0x31yczcn4")))

(define-public crate-rubble_tea-5.0.0 (c (n "rubble_tea") (v "5.0.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0jznba8bm7anl4gcdff55kyajd36rz3ygkkdyvml0a8i3g56znrc")))

(define-public crate-rubble_tea-5.0.1 (c (n "rubble_tea") (v "5.0.1") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1xpk9sfbw0mg1ncka6fm8824d9h59c04vcx4395diig2ykqis2hb")))

(define-public crate-rubble_tea-5.2.0 (c (n "rubble_tea") (v "5.2.0") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1ib89hzb86j9imzmr49bkrkkqyygxrjcppdpk0l9z6339ixnasd9")))

(define-public crate-rubble_tea-5.2.1 (c (n "rubble_tea") (v "5.2.1") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0yw0a01w5hj3apq4vxiy9zh3w9n967qjhry05lprjcrqv7l8a1nh")))

