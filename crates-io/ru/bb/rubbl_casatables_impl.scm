(define-module (crates-io ru bb rubbl_casatables_impl) #:use-module (crates-io))

(define-public crate-rubbl_casatables_impl-0.1.0 (c (n "rubbl_casatables_impl") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1a26p5my6zfr6m7q0dn4dmlsbj0vsd6bv3rv4pf9yw2bph5zzjaf")))

(define-public crate-rubbl_casatables_impl-0.1.1 (c (n "rubbl_casatables_impl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1ldyzg0s31szdkj1hmzhvzr4lhvxzckmcqv7mshry6g7q2yyrzr8")))

(define-public crate-rubbl_casatables_impl-0.2.311 (c (n "rubbl_casatables_impl") (v "0.2.311") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0y3flfi7lryr9zlmimh5d4c74iadca2nnvwfgigw039rl9vcdfq8")))

(define-public crate-rubbl_casatables_impl-0.2.31100 (c (n "rubbl_casatables_impl") (v "0.2.31100") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "17rrhcgnssicjfvx25sdjrr17mmhj440kdykadp9gnjzdkxc0mli")))

(define-public crate-rubbl_casatables_impl-0.2.31101 (c (n "rubbl_casatables_impl") (v "0.2.31101") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0ab2yxl8bn7cdjwz75b5kvr3m3vvqzszbyyyia8qgf8hpqkxnx8b") (l "casa")))

(define-public crate-rubbl_casatables_impl-0.2.31104 (c (n "rubbl_casatables_impl") (v "0.2.31104") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0m5wgn3rf0fsn45zprwf1y4g8f5nwriad9y3q47cw7ypvwxpb2aj") (l "casa")))

(define-public crate-rubbl_casatables_impl-0.2.31105 (c (n "rubbl_casatables_impl") (v "0.2.31105") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "046aasbx18gvh0244grzhc3a0wda14ha3gv748qw4x3k7diwppkf") (l "casa")))

(define-public crate-rubbl_casatables_impl-0.3.31100 (c (n "rubbl_casatables_impl") (v "0.3.31100") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "12q5cfl377qhvi3nmh8zsx6nrsgg8fr5vb8q9dxkgw3lyjdwmmvi") (l "casa")))

(define-public crate-rubbl_casatables_impl-0.3.31101 (c (n "rubbl_casatables_impl") (v "0.3.31101") (d (list (d (n "cc") (r "^1.0.42") (f (quote ("parallel"))) (d #t) (k 1)))) (h "19v4azraxzx2d65924lv7s63xgaiy38k5kwca837rf63c3ci70mp") (l "casa")))

