(define-module (crates-io ru bb rubble-templates-evaluators) #:use-module (crates-io))

(define-public crate-rubble-templates-evaluators-0.2.0 (c (n "rubble-templates-evaluators") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rubble-templates-core") (r "^0.2.0") (f (quote ("ast" "evaluator"))) (d #t) (k 0)))) (h "13zv87py1n2bci39n31m63m3zbc2idm6410xfy1bwcny4ysblicy") (f (quote (("simple") ("default" "simple"))))))

(define-public crate-rubble-templates-evaluators-0.2.1 (c (n "rubble-templates-evaluators") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rubble-templates-core") (r "^0.2.0") (f (quote ("ast" "evaluator"))) (d #t) (k 0)))) (h "19bjnns6qxcp1p22svjdnwcfv395v0hsv98jn4k1qc2m9jzb26nl") (f (quote (("simple") ("default" "simple"))))))

