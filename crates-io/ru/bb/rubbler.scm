(define-module (crates-io ru bb rubbler) #:use-module (crates-io))

(define-public crate-rubbler-0.1.0 (c (n "rubbler") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.24.0") (d #t) (k 1)))) (h "01fk7v1yw81fv4fmj23ryvnaapcrg0h5lmzizij2lpmw4j09l3zg")))

(define-public crate-rubbler-0.1.1 (c (n "rubbler") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.24.0") (d #t) (k 1)))) (h "1v56w577i0qg4lyqsasfr1wwryy64m9bbclsrki67fvfx379xl22") (y #t)))

(define-public crate-rubbler-0.1.2 (c (n "rubbler") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.24.0") (d #t) (k 1)))) (h "1f726pmnc004b99prc5rhwfq1q7ldnpswf4wd0bgx7wa7fi9qjan")))

