(define-module (crates-io ru bb rubble) #:use-module (crates-io))

(define-public crate-rubble-0.0.0 (c (n "rubble") (v "0.0.0") (h "1mdwpdgfq03sm1iflb04hn956mr6qihk39ckxpcac39r86nia9rc")))

(define-public crate-rubble-0.0.1 (c (n "rubble") (v "0.0.1") (d (list (d (n "bbqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (k 0)))) (h "0czrn4vyjfivjjdnkp6gfvc10fisajklig6niprz9xddcdqhjyaw")))

(define-public crate-rubble-0.0.2 (c (n "rubble") (v "0.0.2") (d (list (d (n "bbqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (k 0)))) (h "0g3dhx30i3403jmfli37355mvw2q4kfdim3l66qrfjp0i3a6vmp2")))

(define-public crate-rubble-0.0.3 (c (n "rubble") (v "0.0.3") (d (list (d (n "bbqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (k 0)))) (h "1xx45m9a0dmsqyyxjjnr131vziqhnkdp2jhpzni8xgfcjgkqxpsn")))

(define-public crate-rubble-0.0.4 (c (n "rubble") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "p256") (r "^0.9.0") (f (quote ("arithmetic"))) (k 0)) (d (n "p256") (r "^0.9.0") (f (quote ("arithmetic"))) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (o #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 2)) (d (n "sha2") (r "^0.9.0") (k 0)))) (h "0i3k9a365pi3bbnqwrcm6dxda3kzv2h00mys00vmjall3s52pvy2")))

