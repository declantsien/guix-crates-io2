(define-module (crates-io ru bb rubble-nrf52) #:use-module (crates-io))

(define-public crate-rubble-nrf52-0.0.3 (c (n "rubble-nrf52") (v "0.0.3") (d (list (d (n "nrf52810-hal") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "nrf52832-hal") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "rubble") (r "^0.0.3") (k 0)))) (h "1wplrxv2n53sa3ngi16hpf3vxyn9d2bpdf4nvp3q24j5zq06fcd5") (f (quote (("52840" "nrf52840-hal") ("52832" "nrf52832-hal") ("52810" "nrf52810-hal"))))))

