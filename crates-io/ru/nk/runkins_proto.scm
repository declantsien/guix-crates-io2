(define-module (crates-io ru nk runkins_proto) #:use-module (crates-io))

(define-public crate-runkins_proto-0.1.0 (c (n "runkins_proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "tonic") (r "^0.5") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "1n2jmr57a7nn3d00iw6bzppzqmnn6mj9b3gzvrj6zl2plg1l2972")))

