(define-module (crates-io ru nk runkr) #:use-module (crates-io))

(define-public crate-runkr-0.1.0 (c (n "runkr") (v "0.1.0") (d (list (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0rn85yiff29gjf1dkhsz2mcim5ialnzj3jw3kv4digqcq23mdx0v")))

(define-public crate-runkr-1.0.0 (c (n "runkr") (v "1.0.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1j4zfddv5sq5nk8sjl9r4lpfvbrgpd5cwn0i1ryi5iwvpy6gn2xj")))

