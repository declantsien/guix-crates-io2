(define-module (crates-io ru mq rumq-cli) #:use-module (crates-io))

(define-public crate-rumq-cli-0.1.0-alpha.1 (c (n "rumq-cli") (v "0.1.0-alpha.1") (h "1f4dcvxska8chfvdw3n0qc0q43239y6z3mn1x6k9rxn4ykk796mf")))

(define-public crate-rumq-cli-0.1.0-alpha.4 (c (n "rumq-cli") (v "0.1.0-alpha.4") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "rumq-broker") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0h08y21n2m2v5jlnb93723vji5vnxmkagaxdz50wksh19hfmgcka")))

(define-public crate-rumq-cli-0.1.0-alpha.5 (c (n "rumq-cli") (v "0.1.0-alpha.5") (d (list (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "rumq-broker") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "131dv353gg0w7zf9igbsc3j3g2s59myxf8bbr3lphcycl8vwpyhl")))

