(define-module (crates-io ru nr runrs) #:use-module (crates-io))

(define-public crate-runrs-0.1.0 (c (n "runrs") (v "0.1.0") (h "1d9qbfkhnr94ry7knj6z2l92pjhlh55gbq5bccasv6r064vfpy8z")))

(define-public crate-runrs-0.2.3 (c (n "runrs") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust_util") (r "^0.6.41") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)))) (h "0dv6k7jyxr3rdj9y8smyixk2mgjsfdg5lpvk7627b4jakw6pa8lx")))

