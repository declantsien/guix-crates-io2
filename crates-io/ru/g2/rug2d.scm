(define-module (crates-io ru g2 rug2d) #:use-module (crates-io))

(define-public crate-rug2d-0.1.0 (c (n "rug2d") (v "0.1.0") (d (list (d (n "gl") (r "^0.9.0") (d #t) (k 0)) (d (n "glutin") (r "^0.12.0") (d #t) (k 0)))) (h "0y1w3i3wm2rq1xv1ir2mjgz6xncf3fy971560zrn3pahasa7ii3m")))

(define-public crate-rug2d-0.1.1 (c (n "rug2d") (v "0.1.1") (d (list (d (n "gl") (r "^0.9.0") (d #t) (k 0)) (d (n "glutin") (r "^0.12.0") (d #t) (k 0)))) (h "16afmxnmi007mhcllx2474k13zlvl08l21q8644vlwlsihn14pnl")))

