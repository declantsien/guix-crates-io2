(define-module (crates-io ru fl ruflex) #:use-module (crates-io))

(define-public crate-ruflex-0.1.0 (c (n "ruflex") (v "0.1.0") (h "128mady7njpwjibhv25gsi1slpczahvrc7rlz9kqrqab3mjrx32r") (y #t)))

(define-public crate-ruflex-0.1.1 (c (n "ruflex") (v "0.1.1") (h "0dk7mhyglinmfp09ar1z4y2x863prcpf2j5lps9sv4f9jm2pi0q4") (y #t)))

(define-public crate-ruflex-0.1.2 (c (n "ruflex") (v "0.1.2") (h "0s7f6k75m7h9mqh5crq5vkjnwm2zifkx6b41pn9ylk33jndhis44") (y #t)))

