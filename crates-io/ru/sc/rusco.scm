(define-module (crates-io ru sc rusco) #:use-module (crates-io))

(define-public crate-rusco-0.1.0 (c (n "rusco") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05m5rqgghmpv4jv7h9g0250pgx99q98igi3na2h6ch1ax4id1c3i")))

(define-public crate-rusco-0.1.1 (c (n "rusco") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gnmz3pcsm8nhfrgm15w6psiifjxmdf1wwf4vn596hyraj5dfghz")))

(define-public crate-rusco-0.1.2 (c (n "rusco") (v "0.1.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iajv33fyjdmavqr3v39xn2ji0q60gzjrjnc13cg3s0i24mg48z5")))

(define-public crate-rusco-0.1.3 (c (n "rusco") (v "0.1.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05hbnxchcg5ba1v6lppyipz4dp1y97xcpy7zfawdd2cspwc19abi")))

