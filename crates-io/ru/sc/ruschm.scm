(define-module (crates-io ru sc ruschm) #:use-module (crates-io))

(define-public crate-ruschm-0.1.2-alpha.3 (c (n "ruschm") (v "0.1.2-alpha.3") (d (list (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "0rd1n3r58yqbjqnl4hcn19i8khq7z4pnjm5hxafnj94r9k8ly9gn")))

(define-public crate-ruschm-0.1.2 (c (n "ruschm") (v "0.1.2") (d (list (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "08qzpqp0kc0g2jyf01bss95f22qqmgs1g62mkaq9fwysk7wskx46")))

(define-public crate-ruschm-0.1.3 (c (n "ruschm") (v "0.1.3") (d (list (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "1s6ar06hfflz38afjrilcnjbhkggbkid9xsp7nnwivd3jmp1zhii")))

(define-public crate-ruschm-0.2.0-alpha.0 (c (n "ruschm") (v "0.2.0-alpha.0") (d (list (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "cell") (r "^0.1.8") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0zgp2gvfgx6k01rhvzi8fjd7drr9v129c3gasr5vyih4898bszh1")))

(define-public crate-ruschm-0.2.0 (c (n "ruschm") (v "0.2.0") (d (list (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "cell") (r "^0.1.8") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0spladvkw6k9b0y2gj2jialdy4s6iq2m5plyp5d7hjdv92v0pla5")))

