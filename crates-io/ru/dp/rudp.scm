(define-module (crates-io ru dp rudp) #:use-module (crates-io))

(define-public crate-rudp-0.1.0 (c (n "rudp") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0d2nda79gg6kvy5jl9as8j1fa4j0s2a1vfsqm4pa7jqdagkks97y") (y #t)))

(define-public crate-rudp-0.2.1 (c (n "rudp") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "08z05lg8nhd5rc1gnilbyw0wcqs78r9i3kb6cizq8qb7h8d9f37d")))

