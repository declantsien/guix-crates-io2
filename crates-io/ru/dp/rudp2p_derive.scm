(define-module (crates-io ru dp rudp2p_derive) #:use-module (crates-io))

(define-public crate-rudp2p_derive-0.1.0 (c (n "rudp2p_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rudp2p") (r "^0.4.0") (d #t) (k 2)) (d (n "serialize_bits") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1m6nf4kingbqkkvfxqb5336p2m0qc3mjhayqiq5jwl7kkybjjfls")))

