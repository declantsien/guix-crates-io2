(define-module (crates-io ru ra rural) #:use-module (crates-io))

(define-public crate-rural-0.0.0 (c (n "rural") (v "0.0.0") (h "1rpf4afh6d71qxk2n0dx19njlkpgc384da3xh1f63m8zn2ybz5g8")))

(define-public crate-rural-0.1.0 (c (n "rural") (v "0.1.0") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)))) (h "0k9ia224vr2d9pkqij1y4gv5f20kc33climhpm5jjf5gsikaiqr0")))

(define-public crate-rural-0.2.0 (c (n "rural") (v "0.2.0") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "1yak47rnhnk9wpr9rr1206yg23w9hs5vdm05lvdpdj3174zzjjpp")))

(define-public crate-rural-0.2.1 (c (n "rural") (v "0.2.1") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "1yxn1zkjgaqpyll3iwk584l75larkx207gsbnzw5c01m574a0czc")))

(define-public crate-rural-0.3.0 (c (n "rural") (v "0.3.0") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "040b3j17kapk7vigkiqiiid9sn11j4m9jgq2x50sqy9zki5z7pg2")))

(define-public crate-rural-0.4.0 (c (n "rural") (v "0.4.0") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "088n0xnm4axnixc0j88hlajfs5xqk24wkzywc1fpnnmxm7mabdvb")))

(define-public crate-rural-0.4.1 (c (n "rural") (v "0.4.1") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "0vgmfd2fxqp6r810ifj53x2kyxpkka3pcv3qm762s6248y3xadi6")))

(define-public crate-rural-0.5.0 (c (n "rural") (v "0.5.0") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "json-color") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "01ashshh34nrf530176vic5zg2aj343dwdqfz75008sf3xx443f8")))

(define-public crate-rural-0.6.0 (c (n "rural") (v "0.6.0") (d (list (d (n "clap") (r "^2.19.3") (d #t) (k 0)) (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "json-color") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "0jyq333ard5zndrfl8va0fms16frs79s99jv5kmpgp5c5qiyv1m5")))

(define-public crate-rural-0.6.1 (c (n "rural") (v "0.6.1") (d (list (d (n "clap") (r "^2.20.0") (d #t) (k 0)) (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "json-color") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "0z4yjh00s4isa1l36kib9jfjkb6v5myhn9rs012hm9czr9lzqxys")))

(define-public crate-rural-0.6.2 (c (n "rural") (v "0.6.2") (d (list (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "json-color") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.7") (d #t) (k 0)))) (h "0m4dn1bcxfm6k10n7xqcyfl8vfvhimbgmhwl01da3j149gn76864")))

(define-public crate-rural-0.6.3 (c (n "rural") (v "0.6.3") (d (list (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "colored") (r "^1.4.1") (d #t) (k 0)) (d (n "json-color") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0d8krzipx00knma4y915arx7pdvi6xa6hy1qck1ljmjbnhwicx96")))

(define-public crate-rural-0.6.4 (c (n "rural") (v "0.6.4") (d (list (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "colored") (r "^1.4.1") (d #t) (k 0)) (d (n "json-color") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "1wcwsbk5s2ck7bz3ppkid4a4i72wimy81rbplzsf5mg057fcbwmg")))

(define-public crate-rural-0.7.0 (c (n "rural") (v "0.7.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "colored") (r "^1.5.1") (d #t) (k 0)) (d (n "json-color") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1paf0gvmm9gggkqrzwl23f8vf9h4f3y2y09dscdp1kxw3jd54bwz")))

(define-public crate-rural-0.7.1 (c (n "rural") (v "0.7.1") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "colored") (r "^1.5.1") (d #t) (k 0)) (d (n "json-color") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1s25k1qwi48ks9jcbjd34nv9sxdh4wvhdw111h81hyyv3s65qxpf")))

(define-public crate-rural-0.7.2 (c (n "rural") (v "0.7.2") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "json-color") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "10scam0xr45p78idczqsybg0hm17nai7ldfhclyl3rn1xn85ssl0")))

(define-public crate-rural-0.7.3 (c (n "rural") (v "0.7.3") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "json-color") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0yxs3b7ylw4942r289izymn0hi041vvad6wkbgyn7dg90m8d35sf")))

(define-public crate-rural-0.7.4 (c (n "rural") (v "0.7.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "json-color") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "0zqg02jinzrw28fslgz4pamkkg6fcn83hqhpc6j135aqbcaic7ya")))

(define-public crate-rural-0.8.0 (c (n "rural") (v "0.8.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "json-color") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0whbln6v5abc065q7ly67m4mz142rf828gk7139j1z4pn3g6w02h")))

(define-public crate-rural-0.8.1 (c (n "rural") (v "0.8.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "json-color") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1wwx24205jhbd23cn49v44bk82vjrn8n70b2qimcy0v49h4x74vq")))

