(define-module (crates-io ru gc rugcom) #:use-module (crates-io))

(define-public crate-rugcom-0.1.0 (c (n "rugcom") (v "0.1.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)) (d (n "rugflo") (r "^0.1") (d #t) (k 0)) (d (n "rugint") (r "^0.1") (d #t) (k 0)) (d (n "rugrat") (r "^0.1") (d #t) (k 0)))) (h "136na15736fxbgz4dr4lrj0s445vm0bs84m0yaw62pmf0bbsprly")))

(define-public crate-rugcom-0.1.1 (c (n "rugcom") (v "0.1.1") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugflo") (r "^0.1.1") (d #t) (k 0)) (d (n "rugint") (r "^0.1") (d #t) (k 0)) (d (n "rugrat") (r "^0.1") (d #t) (k 0)))) (h "05wzzcxnb5v9ll1n49l9fzjhki0swp1h044pq01fzjib6zagcq8p") (f (quote (("default" "rand"))))))

(define-public crate-rugcom-0.1.2 (c (n "rugcom") (v "0.1.2") (d (list (d (n "gmp-mpfr-sys") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugflo") (r "^0.1.2") (d #t) (k 0)) (d (n "rugint") (r "^0.1.2") (k 0)) (d (n "rugrat") (r "^0.1.2") (d #t) (k 0)))) (h "1sglimhlrhd4yz2av2azgxqbf0wiggaafyvsc0s7y94dc95ac6fg") (f (quote (("default" "rand"))))))

(define-public crate-rugcom-0.1.3 (c (n "rugcom") (v "0.1.3") (d (list (d (n "gmp-mpfr-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugflo") (r "^0.1.3") (k 0)) (d (n "rugint") (r "^0.1.3") (k 0)) (d (n "rugrat") (r "^0.1.3") (d #t) (k 0)))) (h "0rm7haqzdrvnjck847s4kdcg40a9yr4k4dfh05vsp3cnr02vwpfa") (f (quote (("random" "rand" "rugflo/random") ("default" "random"))))))

(define-public crate-rugcom-0.2.0 (c (n "rugcom") (v "0.2.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugflo") (r "^0.2.0") (k 0)) (d (n "rugint") (r "^0.2.0") (k 0)) (d (n "rugrat") (r "^0.2.0") (d #t) (k 0)))) (h "0bpx0sqla45lpyym4fg0g68wfqihmjw8pk65sr7aiijf51yn1sz0") (f (quote (("random" "rand" "rugflo/random") ("default" "random"))))))

(define-public crate-rugcom-0.2.1 (c (n "rugcom") (v "0.2.1") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugflo") (r "^0.2.1") (k 0)) (d (n "rugint") (r "^0.2.1") (k 0)) (d (n "rugrat") (r "^0.2.1") (d #t) (k 0)))) (h "0xfjap53w1lq5rzfad0wi6nnv1azja65yvy4cb87lp9pi0r67jlj") (f (quote (("random" "rand" "rugflo/random") ("default" "random"))))))

(define-public crate-rugcom-0.2.2 (c (n "rugcom") (v "0.2.2") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (f (quote ("mpc"))) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugflo") (r "^0.2.2") (k 0)) (d (n "rugint") (r "^0.2.2") (k 0)) (d (n "rugrat") (r "^0.2.2") (d #t) (k 0)))) (h "00g0rj2v51d38cx8j8g996jlgbdrzlmlv8m9d7ayqqhgi4ayyjh8") (f (quote (("random" "rand" "rugflo/random") ("default" "random"))))))

(define-public crate-rugcom-0.3.0 (c (n "rugcom") (v "0.3.0") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (f (quote ("mpc"))) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rugflo") (r "^0.3.0") (k 0)) (d (n "rugint") (r "^0.3.0") (k 0)) (d (n "rugrat") (r "^0.3.0") (d #t) (k 0)))) (h "0vvdhrkcmrvf0q7yicxvvv4qc3vvn0hldhwaq182kwmcpksc4jad") (f (quote (("random" "rand" "rugflo/random") ("default" "random"))))))

(define-public crate-rugcom-0.4.0 (c (n "rugcom") (v "0.4.0") (h "176slamn2ic2hbdjr4yyf788gn33sys6ba6a2v23kv2fpw45s6j9")))

(define-public crate-rugcom-0.4.1 (c (n "rugcom") (v "0.4.1") (h "0cdcch3nbqjjg845xciwwsabqz7054sfnkw6kpa9sswi37r406nq")))

