(define-module (crates-io ru ni runic) #:use-module (crates-io))

(define-public crate-runic-0.1.0 (c (n "runic") (v "0.1.0") (d (list (d (n "etcd") (r "^0.9.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "hyper") (r "^0.12.29") (d #t) (k 0)) (d (n "tokio") (r "^0.1.20") (d #t) (k 0)))) (h "0kilw8i11d3wkyy8gsfcpck4yh0x15cbi5rg2275rm2d62prx24m") (y #t)))

(define-public crate-runic-1.0.0 (c (n "runic") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("wasmbind"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "09vzzk3px5v9kn49lziaipsxzsn7nn1pbad4l6w2yi0da95x7xa3")))

(define-public crate-runic-1.1.0 (c (n "runic") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)))) (h "02ynhavhwjawlmr9nzyzsfx9ywpafjqzbzn692r71cp1idhqqbqr")))

