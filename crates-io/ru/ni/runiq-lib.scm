(define-module (crates-io ru ni runiq-lib) #:use-module (crates-io))

(define-public crate-runiq-lib-1.2.2 (c (n "runiq-lib") (v "1.2.2") (d (list (d (n "bytelines") (r "^2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (d #t) (k 0)))) (h "0h0as946dbini2fdz8r12yk4wmsvmvv3f3skmnynwwvwr56bbw86")))

