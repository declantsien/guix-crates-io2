(define-module (crates-io ru ni runiq) #:use-module (crates-io))

(define-public crate-runiq-1.0.0 (c (n "runiq") (v "1.0.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "xxhash2") (r "^0.1") (d #t) (k 0)))) (h "08jpakc66lp9rvvhzmivhr4rlyw324cjwqjpq22bi9bifvxrsfl6")))

(define-public crate-runiq-1.1.0 (c (n "runiq") (v "1.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "xxhash2") (r "^0.1") (d #t) (k 0)))) (h "043sxkyw2h7wgmn8wj2zn3y67hzm7x0sw0sqkpjqa80l2s32qciv")))

(define-public crate-runiq-1.1.1 (c (n "runiq") (v "1.1.1") (d (list (d (n "bytelines") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "xxhash2") (r "^0.1") (d #t) (k 0)))) (h "16yarkibgj6g0hwjm81px9ylcxiplhjaflm8zd4w12vrlh6dj7bh")))

(define-public crate-runiq-1.1.2 (c (n "runiq") (v "1.1.2") (d (list (d (n "bytelines") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "xxhash2") (r "^0.1") (d #t) (k 0)))) (h "1dw4kj35ghgvgmc55f233q1vmpks14pch0ad8f69j2zfl9skzcf8")))

(define-public crate-runiq-1.1.3 (c (n "runiq") (v "1.1.3") (d (list (d (n "bytelines") (r "^2.1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "xxhash2") (r "^0.1") (d #t) (k 0)))) (h "1zyll271p1gk0r473js9j302vhnby4rj35w6fpq8lx06zlak451k")))

(define-public crate-runiq-1.1.4 (c (n "runiq") (v "1.1.4") (d (list (d (n "bytelines") (r "^2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "xxhash2") (r "^0.1") (d #t) (k 0)))) (h "1p8v53zm4l6nryfgrls00vjiix2ynfxk7d2hnqxpjj822hqfa4df")))

(define-public crate-runiq-1.2.0 (c (n "runiq") (v "1.2.0") (d (list (d (n "bytelines") (r "^2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "09v4n2vfv6zfafd2dryw0zn6hzv0dxy0qmfliy6950wgy127p1fp")))

(define-public crate-runiq-1.2.1 (c (n "runiq") (v "1.2.1") (d (list (d (n "bytelines") (r "^2.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (d #t) (k 0)))) (h "0iv7p3f8hv71xxsciq79klpw788q3yknm882r4gcapba6nkf8hlq")))

(define-public crate-runiq-1.2.2 (c (n "runiq") (v "1.2.2") (d (list (d (n "bytelines") (r "^2.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "scalable_bloom_filter") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (d #t) (k 0)))) (h "14qj49f8ac778n00fdrpjg49l1398k5sy1basxj4ya38mrzhzzgk")))

(define-public crate-runiq-2.0.0 (c (n "runiq") (v "2.0.0") (d (list (d (n "bytelines") (r "^2.5") (o #t) (k 0)) (d (n "bytesize") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "format_num") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "growable-bloom-filter") (r "^2.1") (d #t) (k 0)) (d (n "identity-hash") (r "^0.1") (d #t) (k 0)) (d (n "jen") (r "^1.6") (d #t) (k 2)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8") (f (quote ("xxh64"))) (d #t) (k 0)))) (h "0pyyra16kh677kvdv76rrhch5kgk1riw1nf0ryphgdj9agdn30n0") (f (quote (("default" "cli") ("cli" "bytelines" "bytesize" "clap" "cli-table" "format_num"))))))

