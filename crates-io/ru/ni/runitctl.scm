(define-module (crates-io ru ni runitctl) #:use-module (crates-io))

(define-public crate-runitctl-1.0.0 (c (n "runitctl") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fspp") (r "^2.1.0") (d #t) (k 0)))) (h "1w82ha6fw49xv2pvbll4ds33mfq0f4q7b965fgac52by5x7l7sxr")))

