(define-module (crates-io ru ni runiter-wot) #:use-module (crates-io))

(define-public crate-runiter-wot-0.8.0-a0.7 (c (n "runiter-wot") (v "0.8.0-a0.7") (d (list (d (n "bincode") (r "1.0.*") (d #t) (k 0)) (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "rayon") (r "1.0.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)))) (h "0cbya15xhidcnrpmcpfcakxrzqngnqyz1vf8qmg1yhqn0id3r2jv") (f (quote (("strict"))))))

