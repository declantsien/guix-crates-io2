(define-module (crates-io ru ni runiverse) #:use-module (crates-io))

(define-public crate-runiverse-0.1.0 (c (n "runiverse") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "libmath") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1b8h0maxcddd9jq58bdyswhgp09gq71ghlps03yzcmhynddljh8q")))

(define-public crate-runiverse-0.2.0 (c (n "runiverse") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "libmath") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "1bs481d8bp9r4vqpn3iggw0ap9k2xjb6wksf37wcylnzphxrfhp1")))

