(define-module (crates-io ru ni runiter-crypto) #:use-module (crates-io))

(define-public crate-runiter-crypto-0.2.0-a0.1 (c (n "runiter-crypto") (v "0.2.0-a0.1") (d (list (d (n "base58") (r "0.1.*") (d #t) (k 0)) (d (n "base64") (r "0.9.*") (d #t) (k 0)) (d (n "rust-crypto") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)))) (h "02hnrbyj23pngfa4xvarr51asybbdr57hnzq9ggh3w4ycvcak8x6") (f (quote (("strict"))))))

