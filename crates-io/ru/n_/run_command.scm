(define-module (crates-io ru n_ run_command) #:use-module (crates-io))

(define-public crate-run_command-0.0.1 (c (n "run_command") (v "0.0.1") (h "12x745nad0im4cjys50ya6ccignz40bcnzzc52y0g710kz45al8d")))

(define-public crate-run_command-0.0.2 (c (n "run_command") (v "0.0.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1g25vw60sxaw23lz2bzsgwrcymya8pnkasnlbd8lz6ihhzif9sx0") (s 2) (e (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.3 (c (n "run_command") (v "0.0.3") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1hjk0fxa64x74ssr643sqv004qkfj252lfns6vzv4hpcx7gl49y7") (s 2) (e (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.4 (c (n "run_command") (v "0.0.4") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "01k17c8kw43a28bn3g9d4y0yy6kq68a99ks6m3m5bqmv91va79hc") (s 2) (e (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.5 (c (n "run_command") (v "0.0.5") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "03kv4nli7cqvg6w6anqwmh94b0b84fjslqiym15la1krqv4xj06b") (s 2) (e (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.6 (c (n "run_command") (v "0.0.6") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "01skcwlmlz4ag7ap99q6rncna2y6xyrviv5gayx0kn2bm83133iq") (s 2) (e (quote (("async_tokio" "dep:tokio"))))))

