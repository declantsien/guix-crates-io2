(define-module (crates-io ru n_ run_cmd) #:use-module (crates-io))

(define-public crate-run_cmd-0.1.0 (c (n "run_cmd") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0a63n8b1ilzmgxhg45gillf4c3d2ncpxak8cf4w3zi32a4ir545l") (y #t)))

(define-public crate-run_cmd-0.1.1 (c (n "run_cmd") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1kbwl9zqkh7wmb0l3319mbqn8v65417g5kdd81gkfiwwad0cpf69") (y #t)))

(define-public crate-run_cmd-0.1.2 (c (n "run_cmd") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zddbglrzgbqvgsx61i3dh0m8a9p4nq2svmjjiz8p8pcpcf8pggg") (y #t)))

(define-public crate-run_cmd-0.1.3 (c (n "run_cmd") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0nfvaq1zll00a2igczpm3hvfg5343l3ar8fjhafhbgjig87ls085") (y #t)))

