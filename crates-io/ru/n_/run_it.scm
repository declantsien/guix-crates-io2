(define-module (crates-io ru n_ run_it) #:use-module (crates-io))

(define-public crate-run_it-0.1.0 (c (n "run_it") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0p5gxnfb4525j8yin603ln0rbz38m7vy0yg8issg7azw37i5rhi5")))

(define-public crate-run_it-0.2.0 (c (n "run_it") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1wiaabmcrbqswl1ninp26zjgnnm5a0lkff6cvj2qyl593faxcm5r")))

