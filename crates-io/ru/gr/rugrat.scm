(define-module (crates-io ru gr rugrat) #:use-module (crates-io))

(define-public crate-rugrat-0.1.0 (c (n "rugrat") (v "0.1.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)) (d (n "rugint") (r "^0.1") (d #t) (k 0)))) (h "0w378zchzcyafvvg3nzf4ccsy60pmgviizlkd1cdh5jfdxiib0b6")))

(define-public crate-rugrat-0.1.1 (c (n "rugrat") (v "0.1.1") (d (list (d (n "gmp-mpfr-sys") (r "^0.3") (d #t) (k 0)) (d (n "rugint") (r "^0.1") (k 0)))) (h "1vp351khriwld8750q8d2fyb04f59y61sknm947i2cfkb6df6w2m")))

(define-public crate-rugrat-0.1.2 (c (n "rugrat") (v "0.1.2") (d (list (d (n "gmp-mpfr-sys") (r "^0.5") (d #t) (k 0)) (d (n "rugint") (r "^0.1.2") (k 0)))) (h "0yqc3wdk5lmdc5bdbniq2z35jw3r16m6wqcia5c1p8kdw3f0fv2v")))

(define-public crate-rugrat-0.1.3 (c (n "rugrat") (v "0.1.3") (d (list (d (n "gmp-mpfr-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "rugint") (r "^0.1.3") (k 0)))) (h "1lfc424a4yj44y7k5as1df4gnbmbmiqa4pxs2k3mgkdd2w0fqmxz")))

(define-public crate-rugrat-0.2.0 (c (n "rugrat") (v "0.2.0") (d (list (d (n "gmp-mpfr-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "rugint") (r "^0.2.0") (k 0)))) (h "025xgvqs9y8x33j08w5a6aix4blswm5dkwj974ds0yjixsxjjg9g")))

(define-public crate-rugrat-0.2.1 (c (n "rugrat") (v "0.2.1") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (d #t) (k 0)) (d (n "rugint") (r "^0.2.1") (k 0)))) (h "003510df8bwjr6pkwr40q7b8m1x8rnbyml5ac48wm4rx6x68mjmj")))

(define-public crate-rugrat-0.2.2 (c (n "rugrat") (v "0.2.2") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (k 0)) (d (n "rugint") (r "^0.2.2") (k 0)))) (h "05i1kgi9gfw9dx3dvw58l8jzsd36y5hybr9y68b0fq7wj16b0kid")))

(define-public crate-rugrat-0.3.0 (c (n "rugrat") (v "0.3.0") (d (list (d (n "gmp-mpfr-sys") (r "^1.0") (k 0)) (d (n "rugint") (r "^0.3.0") (k 0)))) (h "06p3yksgdpk28pz7w28gcyjw5r77jn9ll4gmy6m7j9pq8cq74dqr")))

(define-public crate-rugrat-0.4.0 (c (n "rugrat") (v "0.4.0") (h "17xhh56b7rc6yy1qhbyi0xkymvxs1xfdsnsqlsbd58bamr0g73xl")))

(define-public crate-rugrat-0.4.1 (c (n "rugrat") (v "0.4.1") (h "1vjqn6bsh43kscjqmz7h7lfvwf50x3pfclxzd06vcyzrh6zipy3s")))

