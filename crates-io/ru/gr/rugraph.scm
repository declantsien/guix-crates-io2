(define-module (crates-io ru gr rugraph) #:use-module (crates-io))

(define-public crate-rugraph-0.2.0 (c (n "rugraph") (v "0.2.0") (h "1bi5hz123kw3akj9x6fns971fywzvwgndvqqi2j9r8s1cvyrfrjb")))

(define-public crate-rugraph-0.2.1 (c (n "rugraph") (v "0.2.1") (h "0xs0qy0671d81697nkfy0w6h3ak1qk7s61lpj57mvsmgdl40p1d5")))

(define-public crate-rugraph-0.2.2 (c (n "rugraph") (v "0.2.2") (h "0i43a0dh0626ymlqzs7vc1igsfbpcbxkz0iq1k9mbhxbiahsg8fk")))

(define-public crate-rugraph-0.3.0 (c (n "rugraph") (v "0.3.0") (h "0lkfdpn4ncfnp9frkz7dkgszki94l0h6v784yzpn6f23j1vxz67l")))

(define-public crate-rugraph-1.0.0 (c (n "rugraph") (v "1.0.0") (h "1k902h62z9hkab1jnc5pc61l3bhcral0vvsmyzwqggjgk8gxdxi1")))

(define-public crate-rugraph-1.1.0 (c (n "rugraph") (v "1.1.0") (h "0y0qz5xq9z9bdnzg7zx07wk5rj0b6qa2gdvy6d1bg0gyi2cqrnkz")))

(define-public crate-rugraph-1.1.1 (c (n "rugraph") (v "1.1.1") (h "1njjkynr6bimmkx7ig4awzp9kk9vgp1hq071n3gwn768lcm8wl6l")))

(define-public crate-rugraph-1.2.0 (c (n "rugraph") (v "1.2.0") (h "16gciqdh1rwk2c87dsryn9jmd2sl4g818wjb39ajdbynfzinsryx")))

(define-public crate-rugraph-1.2.1 (c (n "rugraph") (v "1.2.1") (h "07h0x3s0lhs54d00lf80pizq1ica7wc4gqmnngphqgcq2mafj5hi")))

