(define-module (crates-io ru xa ruxafor) #:use-module (crates-io))

(define-public crate-ruxafor-0.1.0 (c (n "ruxafor") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.4.2") (d #t) (k 0)))) (h "0dwhiidzvp3ikfwj3snyfii4qkghxjpjq37ylx5gs09vxd5xfwqd")))

(define-public crate-ruxafor-1.0.0 (c (n "ruxafor") (v "1.0.0") (d (list (d (n "hidapi") (r "^1.4.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "0wzrp0ci80rbzw4s61liiaj45f035b05i6xqs9sm1ifq4a5i43lb")))

