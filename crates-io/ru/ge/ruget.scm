(define-module (crates-io ru ge ruget) #:use-module (crates-io))

(define-public crate-ruget-0.1.0 (c (n "ruget") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "146ffscv977r31gib365rss0k6jhpwrgrwv214xq32yi26ir1zlq")))

(define-public crate-ruget-0.1.1 (c (n "ruget") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "03xvb04kcnbb59f1hk5mnrmsi1sirljy88b59jzac9i1lx6p8jmg")))

(define-public crate-ruget-0.1.2 (c (n "ruget") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1xj29if8i68253qxs5p06ym1ify0fbz78ihdy3vcsp9ydy21hp7x")))

(define-public crate-ruget-0.1.3 (c (n "ruget") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "05r0ac7l9zasqd8zqqfk2gx5r93lpl7w753c8mnym8hl7fzsfhqy")))

(define-public crate-ruget-0.2.0 (c (n "ruget") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "seahorse") (r "^0.3.1") (d #t) (k 0)))) (h "1slaamgj0rs42fvyiwqbz8fy6lh2d1rdy5zjzn5kwqbrgbq161j6")))

(define-public crate-ruget-0.2.2 (c (n "ruget") (v "0.2.2") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "seahorse") (r "^0.3.1") (d #t) (k 0)))) (h "0h8wvdcwr6hp1pq8lnda27y88a818n8n0rzlkjmmvrycawkhzs3s")))

(define-public crate-ruget-0.3.1 (c (n "ruget") (v "0.3.1") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "seahorse") (r "^0.6.1") (d #t) (k 0)))) (h "0s7j8k2lj3m2x77p0izy4dw223p558wsyzpc5c0z2vicqkh0bqq0")))

(define-public crate-ruget-0.4.0 (c (n "ruget") (v "0.4.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "seahorse") (r "^1.0.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)))) (h "0vzknnjp508ak3mzxlddrbkkrkd8szbjimrjgwfmp4jda4nji24g")))

(define-public crate-ruget-0.4.1 (c (n "ruget") (v "0.4.1") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "seahorse") (r "^1.0.0") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)))) (h "1ckcpxszzmmiwabf8dr8aslgqzzlfmwfsixvab3ickm16mrqb2ck")))

(define-public crate-ruget-0.4.2 (c (n "ruget") (v "0.4.2") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "seahorse") (r "^1.0.0") (d #t) (k 0)))) (h "10jrx8ygh01v22sgw5b9pq71wr23k08jln3wc0gvgvmsfqkidcx8")))

(define-public crate-ruget-0.4.3 (c (n "ruget") (v "0.4.3") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)))) (h "0wak5byqx0xp1gsw88yzk5qql57dwx595rbyalrsidiy28ywm3sx")))

(define-public crate-ruget-0.5.0 (c (n "ruget") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "seahorse") (r "^2.0") (d #t) (k 0)))) (h "1ng1m4syc1kskz2ha1ink853g5nf4gp03j5r89kpqlv9nzprnm8k")))

