(define-module (crates-io ru vo ruvoke) #:use-module (crates-io))

(define-public crate-ruvoke-0.1.0 (c (n "ruvoke") (v "0.1.0") (d (list (d (n "freedesktop_entry_parser") (r "^1.3.0") (d #t) (k 0)) (d (n "gtk") (r "^0.7.2") (f (quote ("v4_12"))) (d #t) (k 0) (p "gtk4")))) (h "0fm9qkv3ghzyi2gaahzcy4azqyakmrmv1if5zw7inqrjr0ixrh5n")))

