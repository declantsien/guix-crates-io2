(define-module (crates-io ru md rumdb) #:use-module (crates-io))

(define-public crate-rumdb-0.1.0 (c (n "rumdb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x7k7siqx5bf3lnflx02b6sngl30jpxk60r85qr9rsaydk6j43dp")))

(define-public crate-rumdb-0.2.0 (c (n "rumdb") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10rn1j6jyf0l8plkp4cpil0ksx6liisgm7rqk1kb3svacwkb0r6i")))

