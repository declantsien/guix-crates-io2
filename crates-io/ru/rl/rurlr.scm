(define-module (crates-io ru rl rurlr) #:use-module (crates-io))

(define-public crate-rurlr-0.2.0 (c (n "rurlr") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "rustout") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1i82zij76rwsw2ka976f7lxj1zsnzz6vc8s6fyfnr86b83f0z409")))

(define-public crate-rurlr-0.3.0 (c (n "rurlr") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "rustout") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "18ivh27swjdg2s9g0sbl12nwqvhi8xrrxw01jkk5jx0jwwqqfgs3")))

