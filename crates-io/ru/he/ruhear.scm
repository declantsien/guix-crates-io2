(define-module (crates-io ru he ruhear) #:use-module (crates-io))

(define-public crate-ruhear-0.1.0 (c (n "ruhear") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (t "cfg(not(target_os = \"macos\"))") (k 0)) (d (n "screencapturekit") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0hmp09v1s9d01bm5r26h2mpnfn4j973qsxc5sjv6ap3cr6zlhbfc")))

