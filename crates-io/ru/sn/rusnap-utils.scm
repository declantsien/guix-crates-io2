(define-module (crates-io ru sn rusnap-utils) #:use-module (crates-io))

(define-public crate-rusnap-utils-0.1.0 (c (n "rusnap-utils") (v "0.1.0") (d (list (d (n "const-hex") (r "^1.9.1") (f (quote ("hex"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "09dqa1b97d72gyikyfq1m21s1jiqxlj8l079wxhr74lzinbw9n29")))

