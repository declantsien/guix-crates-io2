(define-module (crates-io ru sn rusnap-macros) #:use-module (crates-io))

(define-public crate-rusnap-macros-0.1.0 (c (n "rusnap-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0dgf7705iwyq33bspjc0ma58f22p8xc4z7wvki0sab4p85ifpws0")))

(define-public crate-rusnap-macros-0.1.1 (c (n "rusnap-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qvsc6ybqc8gqzkzi48adx9sn2a9bhrmvdvk16xrb2w63rdvhf2z")))

(define-public crate-rusnap-macros-0.1.2 (c (n "rusnap-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1h293gnygww3ak71p55vyc2s3nl12ygj5ymy3cqqcs70gaz6z6wg")))

