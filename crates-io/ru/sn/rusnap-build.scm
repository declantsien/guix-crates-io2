(define-module (crates-io ru sn rusnap-build) #:use-module (crates-io))

(define-public crate-rusnap-build-0.1.0 (c (n "rusnap-build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "16cdnnc7v2pfsyca9dl1dzfvbwy3vac9fzlb46ri14dqriim02ba")))

(define-public crate-rusnap-build-0.1.1 (c (n "rusnap-build") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1vn31sbwbrcyf9j3sc2jfkdh2r1lqjkncp8wvcbadnj5n8igy1c0")))

(define-public crate-rusnap-build-0.1.2 (c (n "rusnap-build") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "109f4i8559zf0v9aglf4zsljrjms3z6agsjxcizfps7zw1745flm")))

(define-public crate-rusnap-build-0.1.3 (c (n "rusnap-build") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1ir0w1gq072y3ynzk5rzhs0fm1fpav8k68kjk87wxk678kmmksfg")))

(define-public crate-rusnap-build-0.1.4 (c (n "rusnap-build") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1p0z7sxk8yn0i9v8kz33cy7wwarcjxk8ywwig5sid1lnwwaig229")))

(define-public crate-rusnap-build-0.1.5 (c (n "rusnap-build") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "097w1xzdb648a0s0j2361hbv8ymh8ham4cvh0yrwbnhqj3gpff9b")))

(define-public crate-rusnap-build-0.1.6 (c (n "rusnap-build") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "0413x5fywy2kd27kh1imhd64k4fsv8fa2ccll29dnriy6p7vl3sm")))

(define-public crate-rusnap-build-0.1.7 (c (n "rusnap-build") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "16kwb6jinl2x5s57pbw78kgr65iv0rynmlg8xnsbgx0qds0lriiy")))

