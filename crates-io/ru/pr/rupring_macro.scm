(define-module (crates-io ru pr rupring_macro) #:use-module (crates-io))

(define-public crate-rupring_macro-0.1.0 (c (n "rupring_macro") (v "0.1.0") (h "0wfy3ch79lj0xsrpd1c7l6kivc4b36yc771zpxk423y2pp2lhkms")))

(define-public crate-rupring_macro-0.2.0 (c (n "rupring_macro") (v "0.2.0") (h "0ngz3h57f32rgm0alfiihsh9xdwyz17akzfa201xhm10akgigi98")))

(define-public crate-rupring_macro-0.3.0 (c (n "rupring_macro") (v "0.3.0") (h "07d1zrw7yakapgwwv4i38ghnsx9k5w42mq307i3i97fr2f3rrj11")))

(define-public crate-rupring_macro-0.4.0 (c (n "rupring_macro") (v "0.4.0") (h "0fc9dylvnrf4vcq7gnmzwkwhgfvl5jh3dxd5wk9pvxg0frw8zl67")))

(define-public crate-rupring_macro-0.5.0 (c (n "rupring_macro") (v "0.5.0") (h "0vm7krkj74j5qp19zvnp0ylbs9n99rq8n8ssq4ynzgg4hn3j39z3")))

(define-public crate-rupring_macro-0.5.1 (c (n "rupring_macro") (v "0.5.1") (h "17wrf0fi23zwm899l8a9fldyvlqp6m3jaxfgs68dxi17qr7q1x92")))

(define-public crate-rupring_macro-0.6.0 (c (n "rupring_macro") (v "0.6.0") (h "0nr7l451gz4sv925bxnyswr77166hbq9i9dl571cd9d7g2p6x3ma")))

(define-public crate-rupring_macro-0.7.0 (c (n "rupring_macro") (v "0.7.0") (h "0ix491xg1zsfsgii5dl4niky0h8hna0qicrpjpv9y1r9wv001wdj")))

(define-public crate-rupring_macro-0.7.1 (c (n "rupring_macro") (v "0.7.1") (h "189rbklwqwr0jxi7wff0f1hw69f5x2p2nsx7zajwjmnv1ngp855z")))

