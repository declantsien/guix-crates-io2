(define-module (crates-io ru ui ruuid) #:use-module (crates-io))

(define-public crate-ruuid-0.1.0 (c (n "ruuid") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v1" "v3" "v4" "v5" "v6" "v7" "v8" "std" "rng" "rand"))) (d #t) (k 0)))) (h "12y567pvn2rkxnqvkk1y79cxd9ikr7vvkcgczi732glfxp91h3k1")))

(define-public crate-ruuid-0.1.1 (c (n "ruuid") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v1" "v3" "v4" "v5" "v6" "v7" "v8" "std" "rng" "rand"))) (d #t) (k 0)))) (h "0f3gd7xzviygzflma3f0nn3s1c95lvmmirj8hy6qg0pbpziknvmm")))

(define-public crate-ruuid-0.1.2 (c (n "ruuid") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v1" "v3" "v4" "v5" "v6" "v7" "v8" "std" "rng" "rand"))) (d #t) (k 0)))) (h "1d6g4xgppzd3a10zhf780qhzak88h5kksavrqfnmy9j6hy97id71")))

(define-public crate-ruuid-0.1.4 (c (n "ruuid") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v1" "v3" "v4" "v5" "v6" "v7" "v8" "std" "rng" "rand"))) (d #t) (k 0)))) (h "05xsw3mn3synhjcnpgabrgjmkp0jhys34hh0b8gr7spxc4kbpv4k")))

(define-public crate-ruuid-0.1.5 (c (n "ruuid") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v1" "v3" "v4" "v5" "v6" "v7" "v8" "std" "rng" "rand"))) (d #t) (k 0)))) (h "1dfn6ajka18njqd9ri8dzcs5lxdw28bsi1v6a95hcnd67avxg7la")))

(define-public crate-ruuid-0.1.6 (c (n "ruuid") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v1" "v3" "v4" "v5" "v6" "v7" "v8" "std" "rng" "rand"))) (d #t) (k 0)))) (h "0ahf53arv6wvjgjhf9k8asvs3kyywbdmswvkmzhvx1s5ak2l76jw")))

