(define-module (crates-io ru du rudux) #:use-module (crates-io))

(define-public crate-rudux-0.1.0 (c (n "rudux") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "time" "sync"))) (d #t) (k 0)))) (h "1s7liyh99igxrg10m6ck4s8h9p6pg2br1rfii79k4yl5k78q3488")))

