(define-module (crates-io ru du ruduino) #:use-module (crates-io))

(define-public crate-ruduino-0.1.1 (c (n "ruduino") (v "0.1.1") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)))) (h "1vw48wr32nzc6capvyidqxngzmdnjkfpafwcx446fnilkknhj214")))

(define-public crate-ruduino-0.1.2 (c (n "ruduino") (v "0.1.2") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)))) (h "029imp5ypq3akszl75xv99wjdcr9cwb2gza7ad5mdan41ix2nizf")))

(define-public crate-ruduino-0.2.0 (c (n "ruduino") (v "0.2.0") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)))) (h "0ni3wzqg6ny1czfvn2h40ag30f4rvlj2hylbykj4b6kgqzbsh5vy")))

(define-public crate-ruduino-0.2.1 (c (n "ruduino") (v "0.2.1") (d (list (d (n "avr-mcu") (r "^0.2") (d #t) (k 1)))) (h "0qp29bqnwxjfqvahg6ns9mnldbnmnx7rc2wh1ck8y9ydrsc49zxv")))

(define-public crate-ruduino-0.2.2 (c (n "ruduino") (v "0.2.2") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 1)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "1x5ayvm1nb26l4vhljvyncqh85md7aglq85j526w6n8bfrbsdshw")))

(define-public crate-ruduino-0.2.3 (c (n "ruduino") (v "0.2.3") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 1)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "01yix3129zf4h8yif0dx2mwlara0nlcffy37c1c4l0mg9sm9xg34") (f (quote (("default" "avr-std-stub"))))))

(define-public crate-ruduino-0.2.4 (c (n "ruduino") (v "0.2.4") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 1)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "018x8hm7rwnfcpxpxihcdfz3fh2rrdmi0bd2fgsx24x1qj0s8hg0") (f (quote (("default" "avr-std-stub"))))))

(define-public crate-ruduino-0.2.5 (c (n "ruduino") (v "0.2.5") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 1)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "1zz3ldq31i552jf01fmfv4nxr8289wrd0gasx945xayiskgqgr18") (f (quote (("default" "avr-std-stub"))))))

(define-public crate-ruduino-0.2.6 (c (n "ruduino") (v "0.2.6") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 1)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "0pg211hzj77x5gai0m26phnwc3rif3qsfr4i03hhg8z5nfni5y33") (f (quote (("default" "avr-std-stub"))))))

(define-public crate-ruduino-0.2.7 (c (n "ruduino") (v "0.2.7") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "target-cpu-fetch") (r "^0.1") (d #t) (k 1)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "1nnc3j0k22hpwaznvm8lc152gglkjalbp70ihpn2xa4lhi8h9daf") (f (quote (("default" "avr-std-stub"))))))

(define-public crate-ruduino-0.3.0 (c (n "ruduino") (v "0.3.0") (d (list (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "151n8fa0h3sx8akjn01whxbz77hhk800pdjgva6jlk0q12jqi4d9") (f (quote (("default" "avr-std-stub") ("all-mcus"))))))

(define-public crate-ruduino-0.3.1 (c (n "ruduino") (v "0.3.1") (d (list (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "1vdwj9yrvycpbadbip35l4zh7l5h7k5i6hlsknxrwzkciibnhn7l") (f (quote (("default" "avr-std-stub") ("all-mcus"))))))

(define-public crate-ruduino-0.3.2 (c (n "ruduino") (v "0.3.2") (d (list (d (n "avr-config") (r "^2.0") (f (quote ("cpu-frequency"))) (d #t) (k 0)) (d (n "avr-mcu") (r "^0.3") (d #t) (k 1)) (d (n "avr-std-stub") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "avr_delay") (r "^0.3") (d #t) (k 0)) (d (n "const_env--value") (r "^0.1") (d #t) (k 0)) (d (n "target-cpu-macro") (r "^0.1") (d #t) (k 0)))) (h "05vwk7jqs0qja1way7prlh8azfj8rq1r75i2q27cwm1v3l0ry2v1") (f (quote (("default" "avr-std-stub") ("all-mcus"))))))

