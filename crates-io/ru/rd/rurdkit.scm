(define-module (crates-io ru rd rurdkit) #:use-module (crates-io))

(define-public crate-rurdkit-0.1.0 (c (n "rurdkit") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1q9w0m2nlsiyzr08c6029xzxf8ar9vyvvn7jagkwz76j8bx7nkag") (y #t)))

(define-public crate-rurdkit-0.1.1 (c (n "rurdkit") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "08rpmfzh8vmypns7qkp7kfp1nq94rwfj6fbg377vki5n2grj43mh")))

