(define-module (crates-io ru ic ruic) #:use-module (crates-io))

(define-public crate-ruic-0.1.1 (c (n "ruic") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1s6x4g2z4k8kawbmm40d2mk0gpk7zb91dlxkgh86ji37mhf6mwr0")))

