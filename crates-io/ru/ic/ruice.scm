(define-module (crates-io ru ic ruice) #:use-module (crates-io))

(define-public crate-ruice-0.1.0 (c (n "ruice") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "02029gk3xqp4ap1xw1sq2rnc7pkx9qnmxqj1gpd9xr5f6kda5lcc") (r "1.67.1")))

(define-public crate-ruice-0.1.1 (c (n "ruice") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1r8drx496h6ah5d42wq4qyqvhf6z4dk8ckkamm3jskdk27djyn9h") (r "1.67.1")))

(define-public crate-ruice-0.1.2 (c (n "ruice") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0wl6c1k44rvglnf3kxwv4c65g8cxvd2s2i23alh177anm475pc3b") (r "1.67.1")))

(define-public crate-ruice-0.2.0 (c (n "ruice") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "05csbsz9y7fgvfzp44vp90vfqlyx0i0yys8s8w6i89gyz69wfcx2") (r "1.67.1")))

