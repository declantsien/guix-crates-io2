(define-module (crates-io ru ic ruice-axum) #:use-module (crates-io))

(define-public crate-ruice-axum-0.1.0 (c (n "ruice-axum") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "axum") (r "^0.6.8") (d #t) (k 0)) (d (n "ruice") (r "=0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lnr2wwjkpvmig7nj9mxv7xc4dfdnzcrn5x3i6p34g0qk1x0kwi2") (r "1.67.1")))

(define-public crate-ruice-axum-0.1.1 (c (n "ruice-axum") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "axum") (r "^0.6.8") (d #t) (k 0)) (d (n "ruice") (r "=0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12sb5m1nh77myxzkzj3f0svg571sz2kgyimwwgavsv2s4p4n5a8g") (r "1.67.1")))

(define-public crate-ruice-axum-0.1.2 (c (n "ruice-axum") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "ruice") (r "=0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hv607jj1jrjc8xydcydciyaq134h76xzn56hm1lc3hdn901743p") (r "1.67.1")))

(define-public crate-ruice-axum-0.2.0 (c (n "ruice-axum") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "ruice") (r "=0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16yy7gzkrypylrrwf98sasl743gc0ikrnm3z60z25vzmqriy060g") (r "1.67.1")))

