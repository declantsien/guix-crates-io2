(define-module (crates-io ru bo ruboy_lib) #:use-module (crates-io))

(define-public crate-ruboy_lib-0.1.0 (c (n "ruboy_lib") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0adslkp2fz844aw4xd87f8wkrsii1wvsm0m9v7nzadqzrmrynx4v") (f (quote (("default" "boot_dmg") ("boot_mgb" "boot_img_enabled") ("boot_img_enabled") ("boot_dmg0" "boot_img_enabled") ("boot_dmg" "boot_img_enabled"))))))

(define-public crate-ruboy_lib-0.1.1 (c (n "ruboy_lib") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "13s8njbirrsng5cz7ksi553c616i9c9a6xk56l87qp0bxryshd1v") (f (quote (("isa_display") ("default" "boot_dmg" "isa_display") ("boot_mgb" "boot_img_enabled") ("boot_img_enabled") ("boot_dmg0" "boot_img_enabled") ("boot_dmg" "boot_img_enabled"))))))

