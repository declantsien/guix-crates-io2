(define-module (crates-io ru bo ruboy_binutils) #:use-module (crates-io))

(define-public crate-ruboy_binutils-0.1.1 (c (n "ruboy_binutils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "ruboy_lib") (r "^0.1.1") (d #t) (k 0)))) (h "1l5dmdsy14qxrf8v6pg27zrrxdm2dgmi6kw40ia26ab0239vxlis")))

