(define-module (crates-io ru bo ruboy) #:use-module (crates-io))

(define-public crate-ruboy-0.1.0 (c (n "ruboy") (v "0.1.0") (d (list (d (n "ruboy_lib") (r "^0.1.0") (d #t) (k 0)))) (h "1d4m748x8ms6qgv2fsx96z48zh0s6nvj3prq5hhmxvp8b6vrqs7h")))

(define-public crate-ruboy-0.1.1 (c (n "ruboy") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "ruboy_lib") (r "^0.1.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.2") (d #t) (k 0)))) (h "05mlcbqs3i6wyz8zlhw9zz0pj99ahdv3yhiv7f2f4immi28kh9xn")))

