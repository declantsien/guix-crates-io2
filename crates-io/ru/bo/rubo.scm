(define-module (crates-io ru bo rubo) #:use-module (crates-io))

(define-public crate-rubo-0.1.0 (c (n "rubo") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "106r13imdxl16kh0zzy2gplycyq093dm02w6zbj7619y6ynvxmma")))

(define-public crate-rubo-0.1.1 (c (n "rubo") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1w4d69j5wg8i8r9vgmsz7pavy4hwyvrvxz1jx3vhalin9lz44i9m")))

(define-public crate-rubo-0.1.2 (c (n "rubo") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0ag0y987ymyj9lrizb232rwdh7ryffkchkn33d6fpns243aihb16")))

(define-public crate-rubo-0.1.3 (c (n "rubo") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0am78wx3fbsk0bfdi0782zr4fjpahw650fkim0jd2jnqzqlapa72")))

(define-public crate-rubo-0.1.4 (c (n "rubo") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1vn92mmfsy0z09k7x0fg9f9fz56nhz3gk2r8nw5kh8gjr6z7vn6m")))

(define-public crate-rubo-0.1.5 (c (n "rubo") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0lkq9vcx70rk1rjb4ayyrpxslnnfmmbhmdf5f2kvyg7hvinb5g73")))

(define-public crate-rubo-0.1.6 (c (n "rubo") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1jwd0lbg0rig29zy62kllpggmxrqgpmx2hmimnhzqj0zdlx0gkd1")))

