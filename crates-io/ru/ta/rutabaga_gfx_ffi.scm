(define-module (crates-io ru ta rutabaga_gfx_ffi) #:use-module (crates-io))

(define-public crate-rutabaga_gfx_ffi-0.1.2 (c (n "rutabaga_gfx_ffi") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rutabaga_gfx") (r "^0.1.2") (d #t) (k 0)))) (h "0wfj7iixzk7xficai5br50w1v8l3khx6ghy9rmb79yqg25vfg2j8") (f (quote (("vulkano" "rutabaga_gfx/vulkano") ("virgl_renderer_next" "rutabaga_gfx/virgl_renderer_next") ("virgl_renderer" "rutabaga_gfx/virgl_renderer") ("minigbm" "rutabaga_gfx/minigbm") ("gfxstream" "rutabaga_gfx/gfxstream"))))))

(define-public crate-rutabaga_gfx_ffi-0.1.3 (c (n "rutabaga_gfx_ffi") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "rutabaga_gfx") (r "^0.1.3") (d #t) (k 0)))) (h "16a2ffnw35hyn2kjgmamr8lrrbhadhz6cpwmdzb0h7xa29lqxhm7") (f (quote (("vulkano" "rutabaga_gfx/vulkano") ("virgl_renderer" "rutabaga_gfx/virgl_renderer") ("minigbm" "rutabaga_gfx/minigbm") ("gfxstream" "rutabaga_gfx/gfxstream"))))))

