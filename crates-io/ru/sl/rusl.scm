(define-module (crates-io ru sl rusl) #:use-module (crates-io))

(define-public crate-rusl-0.1.0 (c (n "rusl") (v "0.1.0") (d (list (d (n "linux-rust-bindings") (r "^0.1.1") (f (quote ("all"))) (d #t) (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (k 0)))) (h "02aas7wfj0p3545h145aa3vzpzcrfr2nkimx7vx8zr0mvf22h0yi") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-rusl-0.2.0 (c (n "rusl") (v "0.2.0") (d (list (d (n "linux-rust-bindings") (r "^0.1.1") (f (quote ("all"))) (d #t) (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (k 0)))) (h "1y2xynnv45c1833wigc1zyalybm0djalild10qxy5qilbc7sq16j") (f (quote (("integration-test") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-rusl-0.2.1 (c (n "rusl") (v "0.2.1") (d (list (d (n "linux-rust-bindings") (r "^0.1.1") (f (quote ("all"))) (d #t) (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (k 0)))) (h "140ynj51kk5xgy111l8vc2r851k9j18lmsqpk0v1yrjr54fx4qhv") (f (quote (("integration-test") ("default" "alloc") ("alloc"))))))

(define-public crate-rusl-0.2.2 (c (n "rusl") (v "0.2.2") (d (list (d (n "linux-rust-bindings") (r "^0.1.1") (f (quote ("all"))) (d #t) (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (k 0)))) (h "0mavkjhhkcz4qz1grz580bn25q5jshw18jlkpwpw7knwlp9vx07z") (f (quote (("integration-test") ("default" "alloc") ("alloc"))))))

(define-public crate-rusl-0.3.0 (c (n "rusl") (v "0.3.0") (d (list (d (n "linux-rust-bindings") (r "^0.1.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "sc") (r "^0.2.7") (d #t) (k 0)))) (h "1apqc8ydchdbii1dhlji5hnv2l93d2f6cqnyxs9ahig1z6vz7kip") (f (quote (("integration-test") ("default" "alloc") ("alloc"))))))

