(define-module (crates-io ru mp rumpunch) #:use-module (crates-io))

(define-public crate-rumpunch-0.0.0 (c (n "rumpunch") (v "0.0.0") (h "193w1xbvb7w9b6cwviazgw6kb08mx3fmzzrhdnvg87jjvx089wy7")))

(define-public crate-rumpunch-0.0.1 (c (n "rumpunch") (v "0.0.1") (h "0a2dkqnn3v6sd6794jqxkd7g4c4869y5m6li1y9xk1q7bz9n0f97")))

(define-public crate-rumpunch-0.0.2 (c (n "rumpunch") (v "0.0.2") (h "1qkxm1mhj4fhkm8m2q4j7pb6axxm7lkxd3jwdjl008p641dvdx0r")))

