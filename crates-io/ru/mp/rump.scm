(define-module (crates-io ru mp rump) #:use-module (crates-io))

(define-public crate-rump-0.0.1 (c (n "rump") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "0kfz7qcsswi2cdwjfshg3j2vm182sav1jb4y0q7f8j9n8nvvlckw")))

(define-public crate-rump-0.1.0 (c (n "rump") (v "0.1.0") (d (list (d (n "getopts") (r ">= 0.2.4") (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.2.12") (d #t) (k 0)))) (h "1npgy0yj8fc00y8x8vfy9jc0gma0dbsj1b5pfdg8dj0gc4nhrmay")))

