(define-module (crates-io ru mp rumpsteak) #:use-module (crates-io))

(define-public crate-rumpsteak-0.1.0 (c (n "rumpsteak") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rumpsteak-macros") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0wll7ar0yx3ikaq3w0pdgy2a5wyjdynpy7vpwfk3akfdjaz6g8f2")))

