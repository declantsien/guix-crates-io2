(define-module (crates-io ru mp rumpkernel) #:use-module (crates-io))

(define-public crate-rumpkernel-0.0.2 (c (n "rumpkernel") (v "0.0.2") (d (list (d (n "num_cpus") (r "^1.9") (d #t) (k 1)))) (h "0bxdv6wlm59zmynycji0iby169k2y4zan9dzwfvhigadf6jvldcp") (l "rkapps")))

(define-public crate-rumpkernel-0.0.3 (c (n "rumpkernel") (v "0.0.3") (d (list (d (n "num_cpus") (r "^1.9") (d #t) (k 1)))) (h "098rf0p51lshv57phbhss9b81a2yikngl3jk2x8n4zwbqyph772v") (l "rkapps")))

(define-public crate-rumpkernel-0.0.4 (c (n "rumpkernel") (v "0.0.4") (d (list (d (n "num_cpus") (r "^1.9") (d #t) (k 1)))) (h "0a1d5xxk5sxkygbskvzvbb9agx05qqjnjsbvg87nhsnyb1ya2bnv") (l "rkapps")))

(define-public crate-rumpkernel-0.0.5 (c (n "rumpkernel") (v "0.0.5") (d (list (d (n "num_cpus") (r "^1.9") (d #t) (k 1)))) (h "087335c63nf849kf7clffh8akimm0w6xbmajvj64djip8rvzr25l") (l "rkapps")))

