(define-module (crates-io ru ns runsh) #:use-module (crates-io))

(define-public crate-runsh-0.1.0 (c (n "runsh") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "17jli5r07dzyyv7hdp5ipbnix05k1snc100xxylx8x9ma467mjha")))

(define-public crate-runsh-0.1.1 (c (n "runsh") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0xkqfidfw97blj6z33ylbipk07nzmmk0ncn4hqx02g8lscczz720")))

(define-public crate-runsh-0.1.2 (c (n "runsh") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "10r29i3cd7gi8j8xk5m5k1ygjkcbh4nxi1sw0xjmzbhanb5cw701")))

(define-public crate-runsh-0.1.3 (c (n "runsh") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1w5xynidbhzvfjm2z8dqzrhxz4kx9mnjfj3lbz4agb78m5wsdfab")))

(define-public crate-runsh-0.1.4 (c (n "runsh") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0mih4aps5vrsqxlni57fkqxrwayjwmb17kqk0rsxmd9zdqd3w687")))

(define-public crate-runsh-0.1.5 (c (n "runsh") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0q2xlnix275rf9qxkgq1rbnpwckm8k0gigd754kkq843iyqpxi51")))

(define-public crate-runsh-0.1.6 (c (n "runsh") (v "0.1.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1zizhlh64xi9d0ccw2shrimwigq8ns3ig2ydca8rka607bh9xdcp")))

(define-public crate-runsh-0.1.7 (c (n "runsh") (v "0.1.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1gzmyxxw0x5fqgh47yar3x00immngcxc4q2mm18nr7m9pnq5jcxf")))

(define-public crate-runsh-0.1.8 (c (n "runsh") (v "0.1.8") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0mv7mlgpy987vxqhs2ryljazvjmqczsivrz6hr6symkrykcki6hf")))

