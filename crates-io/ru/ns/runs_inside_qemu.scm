(define-module (crates-io ru ns runs_inside_qemu) #:use-module (crates-io))

(define-public crate-runs_inside_qemu-1.0.0 (c (n "runs_inside_qemu") (v "1.0.0") (d (list (d (n "raw-cpuid") (r "^9.1") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.40.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "06sfclf3i84dpmi34qgvrvqxvjndyq5285qqwij25vsi1yqad2sn")))

(define-public crate-runs_inside_qemu-1.0.1 (c (n "runs_inside_qemu") (v "1.0.1") (d (list (d (n "raw-cpuid") (r "^9.1") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.40.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "0amyy5g2q7a6l51s0bi04f4clnw7x8kq8qz81lpzj5lg5p8gbpcy")))

(define-public crate-runs_inside_qemu-1.0.2 (c (n "runs_inside_qemu") (v "1.0.2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "raw-cpuid") (r "^9.1") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.40") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "0bsz7mml17xpccbkb94a47gk1nia1gvnsqf1avv3c6r89818rwch")))

(define-public crate-runs_inside_qemu-1.0.3 (c (n "runs_inside_qemu") (v "1.0.3") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "raw-cpuid") (r "^10.2") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.40") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "1pffprcvnrhg45cja17jps9x8d31zr3pcnq66wjgd1dizqrgqbl5")))

(define-public crate-runs_inside_qemu-1.0.4 (c (n "runs_inside_qemu") (v "1.0.4") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "raw-cpuid") (r "^10.2") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.40") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "0337ln76h2mfk4pspw7388cvl3wyv6q929pcwlxpcgb9m31mamrk")))

(define-public crate-runs_inside_qemu-1.1.0 (c (n "runs_inside_qemu") (v "1.1.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "raw-cpuid") (r "^10.2") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.40") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "0lma8vmx5vy4f2l6i3asy0mbg8nlydpakyz52n70dh7qfn70hmvw")))

(define-public crate-runs_inside_qemu-1.2.0 (c (n "runs_inside_qemu") (v "1.2.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "raw-cpuid") (r "^10.2") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.43") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "0qpbcc0m2k10cdz26ca7v90f6lc03nbd8nbxiv7n6m1gy5n20p66")))

(define-public crate-runs_inside_qemu-1.2.1 (c (n "runs_inside_qemu") (v "1.2.1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "raw-cpuid") (r "^10.2") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "x86") (r "^0.43") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "1qx4jkl681jz25bk496bd76fqgw25fclhh81dfvc9i565hijls31")))

