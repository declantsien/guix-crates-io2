(define-module (crates-io ru sy rusync) #:use-module (crates-io))

(define-public crate-rusync-0.1.0 (c (n "rusync") (v "0.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)))) (h "17qlhxk02rdy8lzi0gy3fgcll9yb4j800yx20s33ni7jnmyd252i")))

(define-public crate-rusync-0.1.1 (c (n "rusync") (v "0.1.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)))) (h "14g2ij3g51qxkxsb91x9zd1wi17yb0kmqbn28hg22c92fykp8fji")))

(define-public crate-rusync-0.1.2 (c (n "rusync") (v "0.1.2") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)))) (h "0wrvzrm6n9s4xjz0175nj54m24nwjgg0ivx00qqlmvg2azjjd542")))

(define-public crate-rusync-0.2.0 (c (n "rusync") (v "0.2.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "097jq5nimh1hxrjbik1g3x3q7g20hk5v41zji0nx5qnm4335hc8i")))

(define-public crate-rusync-0.2.1 (c (n "rusync") (v "0.2.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0n5nqbi6nq7c26ih77y00vq53a564xk54frxcn2jc83j9whnd7wi")))

(define-public crate-rusync-0.2.3 (c (n "rusync") (v "0.2.3") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0nfqjv6y696vpdfncbw1185v4csr87c1szb66d6j8lv4hffkkfk7")))

(define-public crate-rusync-0.3.0 (c (n "rusync") (v "0.3.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1h04zkj2d3w9ivkhgjnmyb56vm9fagzhhc386l4api887gmgxyrr")))

(define-public crate-rusync-0.3.1 (c (n "rusync") (v "0.3.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1nrh89jxc8d6fzsdi01ccl8a64vpb4by91qvpi67s7jin8n5g0cp")))

(define-public crate-rusync-0.4.0 (c (n "rusync") (v "0.4.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1cf0nyfjc6gcfbdcqp00r9s640rdc9vk67x87yw2g9q20993m0z4")))

(define-public crate-rusync-0.4.1 (c (n "rusync") (v "0.4.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "132a0rmgsnzsfnjk9mrvjj960vr01dp2i2zc5i4fvxgh1ziws6dp")))

(define-public crate-rusync-0.4.2 (c (n "rusync") (v "0.4.2") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "0bcysvri6x2xaqvr0b84bh47qb47y4zgx7679zpkcl7k5s8hibfy")))

(define-public crate-rusync-0.4.3 (c (n "rusync") (v "0.4.3") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1kn526xcxv74qi7606lf1775w0l82mkzb7y3ps6npvvai11299gh")))

(define-public crate-rusync-0.5.0 (c (n "rusync") (v "0.5.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1aq078avcglvf0jjd44c4vpwnvkxr0rk26j8mawdddgrcgvl415n")))

(define-public crate-rusync-0.5.1 (c (n "rusync") (v "0.5.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1v5lmkjwgs72ky2irh06iq5kqlzq9czq74wkazj4sp1637ch9y8h")))

(define-public crate-rusync-0.5.2 (c (n "rusync") (v "0.5.2") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "11dnppmhj4jh2yxqdwhjkmbsn8nrl2nfzva69ghysnfghlsci5xh")))

(define-public crate-rusync-0.5.3 (c (n "rusync") (v "0.5.3") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "07xgvpj3alf2y6z79y088dk1w1qkkqjwz7jral8jn1qm88cnlaxm")))

(define-public crate-rusync-0.6.0 (c (n "rusync") (v "0.6.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0vjjkwsanad048b8h4gcixqb6c0mjv031yvqmvbd6nhiffyzk9ry")))

(define-public crate-rusync-0.7.0 (c (n "rusync") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1dr7wgnlbxrhvw6qxmda11bp5brpvamvfk81yc2zcpf6mbd9gg1g")))

(define-public crate-rusync-0.7.2 (c (n "rusync") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "humansize") (r "^1.1.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1jayxjsr56k6macr4n0yp8ks9rq41lm7d23lkybxpgxh5r9nsqvi")))

