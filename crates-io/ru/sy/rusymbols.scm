(define-module (crates-io ru sy rusymbols) #:use-module (crates-io))

(define-public crate-rusymbols-0.1.0 (c (n "rusymbols") (v "0.1.0") (h "0lzkvw9izpzan407462pjh24dm85bvp3z87gqm58mpa4vps1bby8")))

(define-public crate-rusymbols-0.1.1 (c (n "rusymbols") (v "0.1.1") (h "1ipwsqqbbdh56k5qyh3zyi3859ar65f28i7w003fiic8dzz779vh")))

(define-public crate-rusymbols-0.1.2 (c (n "rusymbols") (v "0.1.2") (h "1wy1izcib397c0mcznrpf2fzfz6g0ih85wnillzrqpcqqrgqyq0c")))

