(define-module (crates-io ru lo rulox) #:use-module (crates-io))

(define-public crate-rulox-0.1.0 (c (n "rulox") (v "0.1.0") (d (list (d (n "rulox_macro") (r "^0.1") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "1dp7zdqpn8anqnxgnp9ljgx6idpwhj47fri3ybslj99xafinjmh0")))

(define-public crate-rulox-0.2.0 (c (n "rulox") (v "0.2.0") (d (list (d (n "rulox_macro") (r "^0.2") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "1qpqljkccp7h6y3q94az8y3hcg59azr3pfb8yz1qrhgc9xfckp36")))

(define-public crate-rulox-0.2.1 (c (n "rulox") (v "0.2.1") (d (list (d (n "rulox_macro") (r "^0.2") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "02fbs9nahpp2x3mzjqv63jvcswxa5ykwhyhc14p5bzvkv4f1nwpg")))

(define-public crate-rulox-0.3.0 (c (n "rulox") (v "0.3.0") (d (list (d (n "rulox_macro") (r "^0.3") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "1v6kpymznlwf2nhqb428if8v569c5ydrg4piy8v5xjxjyv87k2qf")))

(define-public crate-rulox-0.4.0 (c (n "rulox") (v "0.4.0") (d (list (d (n "rulox_macro") (r "^0.4") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "1gbrxcmc29f49r0lmwb93pqzw0gx89jal9d0l7hinzifc16f67qv")))

(define-public crate-rulox-0.4.1 (c (n "rulox") (v "0.4.1") (d (list (d (n "rulox_macro") (r "^0.4") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "0sgzahak7v6bw32jdddvmw2aiflqh3vszrr5r550y9w33wdlkq85")))

(define-public crate-rulox-0.4.2 (c (n "rulox") (v "0.4.2") (d (list (d (n "rulox_macro") (r "^0.4") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "1kzq12ms4chsdw56vbajw7rgkkpa6fbpc9i25vcldgbz6jpvvkc2")))

(define-public crate-rulox-0.4.3 (c (n "rulox") (v "0.4.3") (d (list (d (n "rulox_macro") (r "^0.4") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)))) (h "1snxq6wlf8vj037iiqzw4cwby3cdmw85did0z9hxq7a80m183kxd")))

(define-public crate-rulox-0.5.0 (c (n "rulox") (v "0.5.0") (d (list (d (n "rulox_macro") (r "^0.5") (d #t) (k 0)) (d (n "rulox_types") (r "^0.2") (d #t) (k 0)))) (h "0cnwylq6qwchg9470829d98l5frjxwvirbd6kq1l3m0wk572cgn0")))

(define-public crate-rulox-0.5.1 (c (n "rulox") (v "0.5.1") (d (list (d (n "rulox_macro") (r "^0.5") (d #t) (k 0)) (d (n "rulox_types") (r "^0.2") (d #t) (k 0)))) (h "0vd4qzldz0w45zcgl90zfp55h3d5667fcldqlsx1azapy55hn6fg")))

(define-public crate-rulox-0.6.0 (c (n "rulox") (v "0.6.0") (d (list (d (n "rulox_macro") (r "^0.6") (d #t) (k 0)) (d (n "rulox_types") (r "^0.3") (d #t) (k 0)))) (h "1jj56fgnzbm5c3a57flpspwbw89y26qwfiy0w9p9rmmp4yxnbnqr")))

(define-public crate-rulox-0.6.1 (c (n "rulox") (v "0.6.1") (d (list (d (n "rulox_macro") (r "^0.6") (d #t) (k 0)) (d (n "rulox_types") (r "^0.3") (d #t) (k 0)))) (h "17d7wm30qldgcvdfv15n0gq2cl6w3hzz637nsbwmgqaypb5bznvd")))

(define-public crate-rulox-0.7.1 (c (n "rulox") (v "0.7.1") (d (list (d (n "rulox_macro") (r "^0.7") (d #t) (k 0)) (d (n "rulox_types") (r "^0.4") (d #t) (k 0)))) (h "0ixlrblrx3g7q8r2p8bvp77ks8gs9y3x2d55mv6f5gb7klnk5xl0")))

(define-public crate-rulox-0.7.2 (c (n "rulox") (v "0.7.2") (d (list (d (n "rulox_macro") (r "^0.7") (d #t) (k 0)) (d (n "rulox_types") (r "^0.4") (d #t) (k 0)))) (h "09gvii9vij67iv7gakbfbs2lz92qcqw2dwb9jw622fxw9s2r8n92")))

(define-public crate-rulox-0.8.0 (c (n "rulox") (v "0.8.0") (d (list (d (n "rulox_macro") (r "^0.8") (d #t) (k 0)) (d (n "rulox_types") (r "^0.5") (d #t) (k 0)))) (h "1psy207iw5h11ar4zwf2y2xn3pi78zvpr7y64rns96p6310m07p2") (f (quote (("sync" "rulox_types/sync") ("default" "sync"))))))

(define-public crate-rulox-0.8.1 (c (n "rulox") (v "0.8.1") (d (list (d (n "rulox_macro") (r "^0.8") (d #t) (k 0)) (d (n "rulox_types") (r "^0.5") (d #t) (k 0)))) (h "1qk3k1qwgn7mpq4iyfpa4q33vd9ayz2bcfmnv8h0d42hvfy65r1i") (f (quote (("sync" "rulox_types/sync") ("default" "sync"))))))

(define-public crate-rulox-0.9.0 (c (n "rulox") (v "0.9.0") (d (list (d (n "rulox_macro") (r "^0.9") (d #t) (k 0)) (d (n "rulox_types") (r "^0.6") (d #t) (k 0)))) (h "18565rhwj9pxr0y1x1asl30sskcyy94bl45nzr7xzgqpbncp2jis") (f (quote (("sync" "rulox_types/sync") ("serialise" "rulox_types/serialise") ("default" "sync" "async") ("async" "sync" "rulox_types/async")))) (r "1.70")))

