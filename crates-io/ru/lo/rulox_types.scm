(define-module (crates-io ru lo rulox_types) #:use-module (crates-io))

(define-public crate-rulox_types-0.1.0 (c (n "rulox_types") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1x9a6a8y49cmxv88vfbccpmyww78r73qia11hvhsf4bwj9fyjad2")))

(define-public crate-rulox_types-0.1.1 (c (n "rulox_types") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1hhnmasdpypiavhcagvarsyllh3j645x5g5p46dg4vhngwjmzrky")))

(define-public crate-rulox_types-0.1.2 (c (n "rulox_types") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "00i96qblvirkm1876l5ihcx6gj05z0xrn2d56xh44jai1hlggskq")))

(define-public crate-rulox_types-0.2.0 (c (n "rulox_types") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1z7immjbf7dgqkd3rpmijkfcs514cd3x24kfzzcrh45zp4khrl3w")))

(define-public crate-rulox_types-0.3.0 (c (n "rulox_types") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1dxqbxhbbg6r8r3jyiyhwp98ydqa2myq8xz1d9fys75cfvpim52i")))

(define-public crate-rulox_types-0.4.0 (c (n "rulox_types") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "19qlhasd5hhbyf58j4n4rhbhl3phbhvvyhxbni6j96vacl1m3370")))

(define-public crate-rulox_types-0.4.1 (c (n "rulox_types") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0l14pzcy6fklhbq27i3r71y3s7b3jadwsig6v9l16k3px8ycv47y")))

(define-public crate-rulox_types-0.5.0 (c (n "rulox_types") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1fpnig288lx7cjrcv0d7kz2nj9j85xbbv46sf62lqvxfyf9vxwpf") (f (quote (("sync"))))))

(define-public crate-rulox_types-0.6.0 (c (n "rulox_types") (v "0.6.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "castaway") (r "^0.2") (d #t) (k 0)) (d (n "fastrand") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "16mpmkv5002fdgj1a6n6k1c2ll13n0whny0wvljv3mab0y5sm4kz") (f (quote (("sync") ("async" "sync")))) (s 2) (e (quote (("serialise" "dep:serde" "dep:serde_json" "bytes/serde")))) (r "1.70")))

