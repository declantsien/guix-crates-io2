(define-module (crates-io ru lo rulox_macro) #:use-module (crates-io))

(define-public crate-rulox_macro-0.1.0 (c (n "rulox_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0isvnrfh17llh6s54d7m10pmgxknkm04wjqcrdkdzf20bszqx4k0")))

(define-public crate-rulox_macro-0.2.0 (c (n "rulox_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0iwnlk7hlpjypiiidv6fpd6i6brzymkv37350qnpph2hg1id31ag")))

(define-public crate-rulox_macro-0.3.0 (c (n "rulox_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yzi5p5bn559s54171gndazd3a7xybbq4aixwl4sdsdlgc75mmp2")))

(define-public crate-rulox_macro-0.4.0 (c (n "rulox_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cd4yx5gwm4v8k51skfmh79xz5h7cbg79ch55s4kg6bpm12a35j1")))

(define-public crate-rulox_macro-0.5.0 (c (n "rulox_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ky3159pa87zdk76vkzrdhf5g5p6rxd074c54zasrgny88sls60w")))

(define-public crate-rulox_macro-0.6.0 (c (n "rulox_macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03dgjs91gwij10f0vzwhwly39898j7vakvjm26g3ah95in38fnhw")))

(define-public crate-rulox_macro-0.7.0 (c (n "rulox_macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mf1w3f1p2w74a9q1x6mlflnp5vr294dv0zswg4gfy0bqvq2w6nb")))

(define-public crate-rulox_macro-0.7.1 (c (n "rulox_macro") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zmhz2y0n6rdxs4ji3wni8158fwa4h8y71s0b2j8sr1dmky3ii31")))

(define-public crate-rulox_macro-0.8.0 (c (n "rulox_macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0q9iyc8hplbmz860d0y9w81cvpr31jrnwgkxdips1q11nr1xlmsx")))

(define-public crate-rulox_macro-0.9.0 (c (n "rulox_macro") (v "0.9.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rulox_types") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0z1qx5g49l0bkjpqq22n4c3hf3vzjzsrrpaba7bmdijqca6hm2pl")))

