(define-module (crates-io ru di rudis) #:use-module (crates-io))

(define-public crate-rudis-0.1.0 (c (n "rudis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 2)) (d (n "atoi") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jw1mxiyi1ifpj8qydg2srlakdkdiqr1rznbc6lb6y2zi3v6caic")))

