(define-module (crates-io ru di rudiments) #:use-module (crates-io))

(define-public crate-rudiments-0.1.0 (c (n "rudiments") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m417s0i1gcdzmj0hzg87gkkqc7drzihb85qqkwlz0y2jcm6as5w")))

(define-public crate-rudiments-0.1.1 (c (n "rudiments") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rodio") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pc419rvmch1d8xpqrw8n44qydrc4dvzgiwwymcbfgdggr0c5saa")))

