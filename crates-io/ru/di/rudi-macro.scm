(define-module (crates-io ru di rudi-macro) #:use-module (crates-io))

(define-public crate-rudi-macro-0.1.0 (c (n "rudi-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "14cyfnhi62m8rdl2viqi20kg701wbj90yci230fc9knanh0x82ml") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.1.1 (c (n "rudi-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1grv3mww16z1pbypdcnxam2cmzlvdw0247f0j2wgn0zjxb6lpkii") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.1.2 (c (n "rudi-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "09a6ff12201sh4pr2p1n03r52j8wn7n5w9w3ifsd9abqvg1a5d2s") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.1.3 (c (n "rudi-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1igxp1mdg6mr13qd8ga29ddyrb0034brkiljykrm26my3j6gf6v2") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.2.0 (c (n "rudi-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0w216qnirh1nna4vj4nq47cw1laj5lb12pwp5cvpw7yhc0kbl9wc") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.2.1 (c (n "rudi-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0hi46d9i5ishvzi5qa5hqrc2lnh6vci3ddf16296nbkc3x27h1cp") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.3.0 (c (n "rudi-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0a3jvmqbz1cl1sral6rf60j9p6bc3dvvydfxsy1vh4986ky0w9g0") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.3.1 (c (n "rudi-macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0z8nd8s12alkgr4ymka984sngnrklnc5pp7xffzpisn8li11mnq1") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.4.0 (c (n "rudi-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0bkbcwzk9zv0q0c8whb2q4wqxb4ix9v3dnkz7izr2vj3xwxqnkyf") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.5.0 (c (n "rudi-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0367mbi992vf3p8sxw8hj26vkwicsjczg7z74irnbayqh9hp4ah4") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.6.0 (c (n "rudi-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0s3mhjwgvvvjgmv0pw21ra61gkcxax4a8g4rmdwgirafk07z78an") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.7.0 (c (n "rudi-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "rudi-core") (r "^0.1.0") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0pc975rqy2g6bvi6qkyn530lmp79msiz591nj5mv2qfc4c6sdi27") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.8.0 (c (n "rudi-macro") (v "0.8.0") (d (list (d (n "from-attr") (r "^0.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "rudi-core") (r "^0.1.0") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0n2bkf56jf29ahzlm9jmqvab55v4nsh3j2b9pji6ny7h63r5qb2q") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.8.1 (c (n "rudi-macro") (v "0.8.1") (d (list (d (n "from-attr") (r "^0.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "rudi-core") (r "^0.1.0") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0v5kdi5cc92g0qrmwz5ssz5lsgn96fw81canh49wjzvhgrfyg015") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.8.2 (c (n "rudi-macro") (v "0.8.2") (d (list (d (n "from-attr") (r "^0.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "rudi-core") (r "^0.1.0") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "13w7a53bgpcjygjxaq26phpn1js7agqwl585203abxckldg00c4g") (f (quote (("default" "auto-register") ("auto-register"))))))

(define-public crate-rudi-macro-0.8.3 (c (n "rudi-macro") (v "0.8.3") (d (list (d (n "from-attr") (r "^0.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "rudi-core") (r "^0.1.0") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "0p78fy4qnxjrn3n56wyyf6v0ha6hbdzxwgajh00j4gj1sbf262a2") (f (quote (("default" "auto-register") ("auto-register"))))))

