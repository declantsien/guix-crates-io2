(define-module (crates-io ru di rudis-http) #:use-module (crates-io))

(define-public crate-rudis-http-0.1.0 (c (n "rudis-http") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hzvri3yxhyakx5vfx2pfjjvbzva0njlyzpf439k2wgq0q9imfdx")))

(define-public crate-rudis-http-0.1.1 (c (n "rudis-http") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ggqwrmqzx5an6x3pmshqbl6ri4g2ym1kirglgnsvpcygjgypw97")))

(define-public crate-rudis-http-0.1.2 (c (n "rudis-http") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "162fn849sc6m7z7f00kzsm0isqb40h6zw2qlsvqq2ivk65yhcsxr")))

(define-public crate-rudis-http-0.1.3 (c (n "rudis-http") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01xfpabf3qfg1512vj7lvpb1xbfwvrf2kb4dkpv9lpdid1r9ayw6")))

(define-public crate-rudis-http-0.2.0 (c (n "rudis-http") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c6ay95nbz0g661qg4j74y3jdrvy5riyff0dv4iwsr93xcm8qnim")))

