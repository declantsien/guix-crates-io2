(define-module (crates-io ru di rudie) #:use-module (crates-io))

(define-public crate-rudie-0.1.0 (c (n "rudie") (v "0.1.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "04a3ay08l2b9wbbzj97sbmj9in0zb1xga08rnhfi6rgrl96ryk3z")))

(define-public crate-rudie-0.1.1 (c (n "rudie") (v "0.1.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1n48qqfczl9ljaiwaspxzpwwyj2d3r5wz1zxgcada3kc9979pm36")))

