(define-module (crates-io ru zz ruzzzt) #:use-module (crates-io))

(define-public crate-ruzzzt-0.0.1 (c (n "ruzzzt") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "11b4gghvaw3ckilamkmbgglnsyivj0rpwbx793xws8h3m8j1izzw")))

(define-public crate-ruzzzt-0.0.2 (c (n "ruzzzt") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "12q8hcm02sd4gc1xnxbxcmlk0q5j5q90aafy30252jdfa6hwkvi0")))

