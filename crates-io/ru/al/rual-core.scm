(define-module (crates-io ru al rual-core) #:use-module (crates-io))

(define-public crate-rual-core-0.0.3 (c (n "rual-core") (v "0.0.3") (h "0dqa9k7z8z3fgvsxcbrwl521vw360c51wc1lkp1ishj90284bgk7")))

(define-public crate-rual-core-0.0.4 (c (n "rual-core") (v "0.0.4") (h "1z4z5s7gxpr06w8q13m0i2fg5s00xj2fv57n4078aqncadx8qrd2")))

