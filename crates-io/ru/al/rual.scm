(define-module (crates-io ru al rual) #:use-module (crates-io))

(define-public crate-rual-0.0.0 (c (n "rual") (v "0.0.0") (h "0a0xv29ac9d33w8qqgf9g9mrwawdw5r8c7zdc8776nm401dz35m9")))

(define-public crate-rual-0.0.1 (c (n "rual") (v "0.0.1") (h "1alls1lggsnd5ryxaasp6n0cgw123d0yh5064kwcmris6zd6m736")))

(define-public crate-rual-0.0.2 (c (n "rual") (v "0.0.2") (h "1y9rma75692d3m0jfvanfdszxhs7d5212klnk63yj6bchvp7hfyv")))

