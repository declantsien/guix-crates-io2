(define-module (crates-io ru #{51}# ru5102) #:use-module (crates-io))

(define-public crate-ru5102-0.1.0 (c (n "ru5102") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "0i9vg5zh5bkhjvvsy42bnc6pjg6271rpp15v8qr1q6nk9kncwdax")))

(define-public crate-ru5102-0.1.1 (c (n "ru5102") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "1d5ckyjly8vy734di7020z09fkvh02hhnjiajbgi6p496xc9fvk9")))

(define-public crate-ru5102-0.1.2 (c (n "ru5102") (v "0.1.2") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "0z52hj4gdqmb0kcdr1nwyjkf64lgiq0c3f8a7pbwjwwd40d6c077")))

