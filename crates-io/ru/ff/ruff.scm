(define-module (crates-io ru ff ruff) #:use-module (crates-io))

(define-public crate-ruff-0.0.220 (c (n "ruff") (v "0.0.220") (h "0f792lf1v5q1d0ikjgrifgrqrsp2cws3llki19va4waq6j6rczim") (y #t) (r "1.65.0")))

(define-public crate-ruff-0.0.1 (c (n "ruff") (v "0.0.1") (h "1g2j0m1qz4dq8qj0v9wkffpy3qs7jj2wis0fksn5j7jnngk1nx53") (r "1.65.0")))

