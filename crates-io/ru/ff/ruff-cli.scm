(define-module (crates-io ru ff ruff-cli) #:use-module (crates-io))

(define-public crate-ruff-cli-0.0.0 (c (n "ruff-cli") (v "0.0.0") (h "1ikv2vb7axs20zfcmn2zrysx0wysmc6rg2d3dnlns4a0w4p639w5") (r "1.65.0")))

(define-public crate-ruff-cli-0.0.1 (c (n "ruff-cli") (v "0.0.1") (h "05zyhn5y6wcy9vq960i4vqmsw64p8b7am5x0aqlrcpjrnqz1p467") (r "1.65.0")))

