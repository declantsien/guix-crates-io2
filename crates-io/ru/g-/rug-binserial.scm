(define-module (crates-io ru g- rug-binserial) #:use-module (crates-io))

(define-public crate-rug-binserial-0.1.0 (c (n "rug-binserial") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "rug") (r "^1.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "0biicrz7c700f7nxf7fbibyrgscva312ldl009ivd2jhy990k459")))

(define-public crate-rug-binserial-0.1.1 (c (n "rug-binserial") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "rug") (r "^1.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "0f6swfgpw5s6sxp4a3yaczzkrksc9ms4a65x2vis1frf38ngc9s1")))

(define-public crate-rug-binserial-0.1.2 (c (n "rug-binserial") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "rug") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "1q8k7m5h7c7l424sihd8cmwplg07xjph2adpcr3h8rb6mnld29b5")))

(define-public crate-rug-binserial-0.1.3 (c (n "rug-binserial") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "rug") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "1kgsw93a99g93m3lfy68f3wlph3dy2nkgzgvnlfg4mjwgsyk2xmp")))

(define-public crate-rug-binserial-0.2.0 (c (n "rug-binserial") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "rug") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)))) (h "1fhv5wg7b55p9fadnlz5s3jzrvn9hwv5xrr1zi5glywv9lwbivdc")))

