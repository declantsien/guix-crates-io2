(define-module (crates-io ru g- rug-miller-rabin) #:use-module (crates-io))

(define-public crate-rug-miller-rabin-0.1.0 (c (n "rug-miller-rabin") (v "0.1.0") (d (list (d (n "miller_rabin") (r "^1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.24") (f (quote ("integer" "std" "rand"))) (k 0)))) (h "0mq7ks5qcg6gx6czls00a2rn0svv1q3n22ib08x6ck32sbndz0iw") (f (quote (("default" "rayon")))) (r "1.74")))

