(define-module (crates-io ru g- rug-fft) #:use-module (crates-io))

(define-public crate-rug-fft-0.1.0 (c (n "rug-fft") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "0fj0qpqj1qmqhr8nnbvc761jnrqjfqzli1glpgwf0rcwpmqxbxqj")))

(define-public crate-rug-fft-0.1.1 (c (n "rug-fft") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "0lglcxkmvzsdljn4a0la5xg48202ffdv1rkyyii9hf88xknb9mgn")))

(define-public crate-rug-fft-0.1.2 (c (n "rug-fft") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "143x4kkyzhmy73zn8d0266k6y3fh09vkg52si3haxk15xh20wgrk")))

