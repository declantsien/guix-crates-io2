(define-module (crates-io ru g- rug-maths) #:use-module (crates-io))

(define-public crate-rug-maths-0.1.0 (c (n "rug-maths") (v "0.1.0") (d (list (d (n "maths-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rug") (r "^1.6.0") (d #t) (k 0)))) (h "1xj2mq76h1dxvkh0rffrxk1hwkx966gcbsa70qbnl8755hbf2a1h")))

(define-public crate-rug-maths-0.2.0 (c (n "rug-maths") (v "0.2.0") (d (list (d (n "maths-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rug") (r "^1.6.0") (d #t) (k 0)))) (h "1l7svq1r41xs11s1znmn361rdwsk4zpzgkbhwngx0pk0c21ss9bj")))

(define-public crate-rug-maths-0.2.1 (c (n "rug-maths") (v "0.2.1") (d (list (d (n "maths-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rug") (r "^1.7.0") (d #t) (k 0)))) (h "02w1fz1sm4nrw5lmmjpl96c3k8wqjwpn5sqrg84sz6ylwspk8a94")))

(define-public crate-rug-maths-0.2.2 (c (n "rug-maths") (v "0.2.2") (d (list (d (n "maths-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "rug") (r "^1.7.0") (d #t) (k 0)))) (h "1fx4azmj0mjd5ynxfl2bq8p6if2jki44sgp6m0dczc9lb0f1pl52")))

(define-public crate-rug-maths-0.2.3 (c (n "rug-maths") (v "0.2.3") (d (list (d (n "maths-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "rug") (r "^1.8.0") (d #t) (k 0)))) (h "1dhyl3y269qns77vb03668kbqmjlh6q4a5xmybl8d973rh31q2jq")))

(define-public crate-rug-maths-0.2.4 (c (n "rug-maths") (v "0.2.4") (d (list (d (n "maths-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "02zhvgmans1hdpak057bnbsrgp0mr2wb53gq471h862dcw9pnsfa")))

(define-public crate-rug-maths-0.2.5 (c (n "rug-maths") (v "0.2.5") (d (list (d (n "maths-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "0cnpyxszj3byaq0cwxa88y535snf9zh80zxzl2ysdsfzdz0zbndk")))

