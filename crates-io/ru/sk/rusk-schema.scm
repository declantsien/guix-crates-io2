(define-module (crates-io ru sk rusk-schema) #:use-module (crates-io))

(define-public crate-rusk-schema-0.1.0 (c (n "rusk-schema") (v "0.1.0") (d (list (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "dusk-pki") (r "^0.10.0-rc") (d #t) (k 0)) (d (n "phoenix-core") (r "^0.16.0-rc") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "18cgqmqm1ygylf5kn6hdpllph3rkagrvhk3c1z9zhzf0qn5jfj4f")))

(define-public crate-rusk-schema-0.2.0 (c (n "rusk-schema") (v "0.2.0") (d (list (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "dusk-pki") (r "^0.10.0-rc") (d #t) (k 0)) (d (n "phoenix-core") (r "^0.16.0-rc") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "1hwkv89d5yni4ranp89c3wvn6x29a59j585b5a15nwd9w446waqi")))

(define-public crate-rusk-schema-0.3.0 (c (n "rusk-schema") (v "0.3.0") (d (list (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "dusk-pki") (r "^0.10.0-rc") (d #t) (k 0)) (d (n "phoenix-core") (r "^0.16.0-rc") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "17caj5wxgrrvqp24dx4nx7n25158ng4zfz1f0mjr8f9b2pklk89a")))

(define-public crate-rusk-schema-0.4.0 (c (n "rusk-schema") (v "0.4.0") (d (list (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "dusk-pki") (r "^0.10.0-rc") (d #t) (k 0)) (d (n "phoenix-core") (r "^0.16.0-rc") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "12ggz13m8nnm3s33g4wg4r3vikaiq9lpyvi54mppqnv7zgpdn6gi")))

