(define-module (crates-io ru sk rusk_crate) #:use-module (crates-io))

(define-public crate-rusk_crate-0.1.0 (c (n "rusk_crate") (v "0.1.0") (h "12rrkr09i8vrzj8gi34qlzis6pnhv3c8mbpm8faw9zn86s0a43jz") (y #t)))

(define-public crate-rusk_crate-0.2.0 (c (n "rusk_crate") (v "0.2.0") (h "02zylh2l92isywbxjch2ahdfcld5n99199bzvd9x5q1x7paxyajf")))

