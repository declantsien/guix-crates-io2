(define-module (crates-io ru sk rusk) #:use-module (crates-io))

(define-public crate-rusk-0.1.9 (c (n "rusk") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sirlvz9gcl9sw8acdr181v0m5dlpqyn09ww3mabdqq2m1pni3sf")))

(define-public crate-rusk-0.1.11 (c (n "rusk") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sf7gz7lcfix1xx6agzfcda5g7k2wch7n48s1k89k23v05lxm7bg")))

