(define-module (crates-io ru sk rusky) #:use-module (crates-io))

(define-public crate-rusky-0.1.0 (c (n "rusky") (v "0.1.0") (h "1hyb81sqnv4q3nqq2clfwlw2ddhwgirbbr25fy6bl45242jlhkch")))

(define-public crate-rusky-0.2.0 (c (n "rusky") (v "0.2.0") (h "1yrp48rrh6zgqqn04x9xgp1s835nm87i0xbk24ldvfqcas0dxvah")))

(define-public crate-rusky-0.2.1 (c (n "rusky") (v "0.2.1") (h "1y9i0rah2r417n46fhnhnrsc94xcrmkqfgyakql9mchgf7nwv0ha")))

