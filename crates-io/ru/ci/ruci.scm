(define-module (crates-io ru ci ruci) #:use-module (crates-io))

(define-public crate-ruci-0.1.0 (c (n "ruci") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "147wbvwxyqmpdxfggd6ad4vcgh5vgrdha9lkwbj6z70xs5a6w3jh")))

(define-public crate-ruci-0.1.1 (c (n "ruci") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1j33ijf1hcfv65ss449p7fidida78jpq5hn6r2bh61g8yf73c19n")))

(define-public crate-ruci-0.1.2 (c (n "ruci") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "151hhzarj3skwf02igy8i88awynvvckpxkhigf5pilidbz0ffnl2")))

(define-public crate-ruci-0.1.3 (c (n "ruci") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1vyip3093anjaakd76mjxrllj4zxdmm2q4d6lm1x3hx404565wxr")))

(define-public crate-ruci-0.1.4 (c (n "ruci") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1f4mjp2pmwwg2a4ba678dp3ci0znscfyvckzs19y6kphhliz5gfq")))

(define-public crate-ruci-0.1.5 (c (n "ruci") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "195khflw5p8vdi8bnbkfwgk5lylxjf6g7pj6jhqhgyd04pnnynnb")))

(define-public crate-ruci-0.1.6 (c (n "ruci") (v "0.1.6") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "0xxq4wkp4x1si9d15jadmmrfamlbfp6rxq5bd08z4xcim1gjcmgq") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.1.7 (c (n "ruci") (v "0.1.7") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "102yaxnqxyf9asbjc77xv3if1mhm0f0vysv0jaq23j9f4wlw23p5") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.1.8 (c (n "ruci") (v "0.1.8") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1dc0n7bl5smq5igyjq7qhkk0v8sjl9mnsz38dp1z2r4jbr7s0swy") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.1.9 (c (n "ruci") (v "0.1.9") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "0cnra51nawmj7qxvpxpn4rnxq4p77r7131dvyy4piizjcfnw15nh") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.1.10 (c (n "ruci") (v "0.1.10") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "0inibip3fv7j1lpiljys758q3agj1nf3fzh773zi4jilnqrbgc9y") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.1.11 (c (n "ruci") (v "0.1.11") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "13hyb9wyd5pbppigg2ap6spngimy18gmxx037hgbmp3v3qikrbnl") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.1.12 (c (n "ruci") (v "0.1.12") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1ws4nyfsicyzdbzwrq30k9b2829mm9na6cs2hljjkx3zh349gc43") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.1.13 (c (n "ruci") (v "0.1.13") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1rlx1l1iiacgvb701i129nkrfjpl0818vshqn9cdk3q8blsn5b2c") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.2.0 (c (n "ruci") (v "0.2.0") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "067kbnzfa70sq3zj49lkky7rycjlp7hmcs0rsb7ifn1w66c4qhx0") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.2.1 (c (n "ruci") (v "0.2.1") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "0wsimbpnpaqjwrhz2c0xln03rlffcp4jjc4pv62mhcjkilp20w4w") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.2.2 (c (n "ruci") (v "0.2.2") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1vmfn03xn4kf40w147x57fcpv85pidq8358lggb3zgkw9cp2y5zm") (f (quote (("uci-connection") ("default" "uci-connection"))))))

(define-public crate-ruci-0.2.3 (c (n "ruci") (v "0.2.3") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.2") (f (quote ("arc_lock"))) (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "06brdiqvwnrkv7nbbaj3nyd2365mvrxm499sszfap3hy24jpg0b0") (f (quote (("uci-connection-go-async" "parking_lot") ("default"))))))

(define-public crate-ruci-0.2.4 (c (n "ruci") (v "0.2.4") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.2") (f (quote ("arc_lock"))) (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)))) (h "1h8qzmn4w8bmcxkz1kg6bliq0as50c3fp8lqykpc7bhq0zi7ck77") (f (quote (("uci-connection-go-async" "parking_lot") ("default"))))))

