(define-module (crates-io ru nc runcmd) #:use-module (crates-io))

(define-public crate-runcmd-0.1.0 (c (n "runcmd") (v "0.1.0") (d (list (d (n "execute") (r "^0.2.11") (d #t) (k 0)))) (h "0ivicf4w0lzaykn9hq381cg4v52qr80ajs3p1j6p179lfm4h8s62")))

(define-public crate-runcmd-0.1.1 (c (n "runcmd") (v "0.1.1") (d (list (d (n "execute") (r "^0.2.11") (d #t) (k 0)))) (h "03wg4dlnns3xkvm0ngx7xhm7gxqvcyv7cdx9cgdjmmda33kbf1yv")))

