(define-module (crates-io ru ll rulloc) #:use-module (crates-io))

(define-public crate-rulloc-0.1.0 (c (n "rulloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.137") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_System_Memory" "Win32_Foundation" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug"))) (t "cfg(windows)") (k 0)))) (h "19cj9cad9nzxdq06gd3j2zcyhzg1qvhd35c6r1a4nzmb8klg318s")))

