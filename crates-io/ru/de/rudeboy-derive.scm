(define-module (crates-io ru de rudeboy-derive) #:use-module (crates-io))

(define-public crate-rudeboy-derive-0.1.0 (c (n "rudeboy-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03gyrnzqg0z50dzray6dvcanwc1rglpy4k921sj0d0g9hwgam35a")))

(define-public crate-rudeboy-derive-0.2.0 (c (n "rudeboy-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04mx4dq5rwskr9wssqn5ylwrjjp3iyzgb1j2kn0j518frw0qq6id")))

