(define-module (crates-io ru de rudeboy) #:use-module (crates-io))

(define-public crate-rudeboy-0.1.0 (c (n "rudeboy") (v "0.1.0") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)) (d (n "rudeboy-derive") (r "^0.1") (d #t) (k 0)))) (h "1mkr4dfv3appl6f19nmk0s5cav7sfdgrq46dnmqq0ndma9zygakn")))

(define-public crate-rudeboy-0.2.0 (c (n "rudeboy") (v "0.2.0") (d (list (d (n "rlua") (r "^0.17") (d #t) (k 0)) (d (n "rudeboy-derive") (r "^0.2") (d #t) (k 0)))) (h "1qpxk4acb6z8gm6806kg36gfvz3rqr1rrhzpcbxavly2kh6yq0ck")))

