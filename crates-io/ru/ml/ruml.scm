(define-module (crates-io ru ml ruml) #:use-module (crates-io))

(define-public crate-ruml-0.1.0 (c (n "ruml") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits" "full" "parsing" "printing"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0hqa8ki4wyn4h97rvqprh7j7fa1dfl99lq9izw1mq2dyhhsyxbx9")))

