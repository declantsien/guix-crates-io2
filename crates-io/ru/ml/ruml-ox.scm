(define-module (crates-io ru ml ruml-ox) #:use-module (crates-io))

(define-public crate-ruml-ox-0.1.0 (c (n "ruml-ox") (v "0.1.0") (h "0nr3rybhiv124a233n77r5q16a3larhidnqgs8fy0v5klrhfkmxw")))

(define-public crate-ruml-ox-0.1.1 (c (n "ruml-ox") (v "0.1.1") (h "0vi8g510hdbd8khzn9ff5yjg326f45xzifb1a5ys7rq0wd9f12dw")))

