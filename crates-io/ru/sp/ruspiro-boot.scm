(define-module (crates-io ru sp ruspiro-boot) #:use-module (crates-io))

(define-public crate-ruspiro-boot-0.0.3 (c (n "ruspiro-boot") (v "0.0.3") (h "0b2nxhvv0sbf0fsw3da2dy4c82sba9ivb8z5r7pkafi27h04dib3") (f (quote (("with_panic") ("with_exception"))))))

(define-public crate-ruspiro-boot-0.1.0 (c (n "ruspiro-boot") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "1xyr1fsqlwr0lx6x1mhw2bc3k9fklywasfcslfs0vxjpz4nbb43m") (f (quote (("with_panic") ("with_exception") ("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-boot-0.1.1 (c (n "ruspiro-boot") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "01ryvrq47cl5z4dcx9am62k0byxhz3f301lnqyq8lacskrybz33i") (f (quote (("with_panic") ("with_exception") ("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-boot-0.2.0 (c (n "ruspiro-boot") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "0lr3p7cry5y3an7b6bs73yzd3cj70il4b98r67lwj5d1gqzn6wv3") (f (quote (("with_panic") ("with_exception") ("singlecore") ("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-boot-0.2.1 (c (n "ruspiro-boot") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "1xzd5v7kayi6pyj8nl0izbc71sv7rrimpzf68hy789hxkywrawd6") (f (quote (("with_panic") ("with_exception") ("singlecore") ("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-boot-0.3.0 (c (n "ruspiro-boot") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)) (d (n "ruspiro-cache") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-console") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-gpio") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-interrupt") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-timer") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-uart") (r "^0.3") (d #t) (k 0)))) (h "0w9ik9827a38fvcbpkx6fxsjsc9dj5mg3qb74i8cdd9z0vyjnkby") (f (quote (("with_panic") ("singlecore") ("ruspiro_pi3" "ruspiro-gpio/ruspiro_pi3" "ruspiro-uart/ruspiro_pi3" "ruspiro-timer/ruspiro_pi3")))) (l "ruspiro-boot")))

(define-public crate-ruspiro-boot-0.3.1 (c (n "ruspiro-boot") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)) (d (n "ruspiro-cache") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-console") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-interrupt") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-mailbox") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-timer") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-uart") (r "^0.3") (d #t) (k 0)))) (h "1169vf0jk7h35gj3lar41izafb3ix0mxi7w3gna3gwqahg72z7jw") (f (quote (("with_panic") ("singlecore") ("ruspiro_pi3" "ruspiro-mailbox/ruspiro_pi3" "ruspiro-uart/ruspiro_pi3" "ruspiro-timer/ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3")))) (l "ruspiro-boot")))

(define-public crate-ruspiro-boot-0.3.2 (c (n "ruspiro-boot") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "ruspiro-cache") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-console") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-interrupt") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-mailbox") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-timer") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-uart") (r "^0.3") (d #t) (k 0)))) (h "0p5i9l56p98mw65pjxgkfzzf7ci9h0qg85akqq4nafr3kc4cjwjk") (f (quote (("with_panic") ("singlecore") ("ruspiro_pi3" "ruspiro-mailbox/ruspiro_pi3" "ruspiro-uart/ruspiro_pi3" "ruspiro-timer/ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3")))) (l "ruspiro-boot")))

(define-public crate-ruspiro-boot-0.4.0 (c (n "ruspiro-boot") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "ruspiro-cache") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "0kn03mz5cy88kriwk58fa7kw9p04md7vvld81msrgfmxx0rfxsxk") (f (quote (("ruspiro_pi3") ("multicore")))) (l "ruspiro_boot")))

(define-public crate-ruspiro-boot-0.4.1 (c (n "ruspiro-boot") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "ruspiro-cache") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "01y81j5rwly7a7zj33gifp9s861kb6a0azs3qzp7afhxq1dnckcy") (f (quote (("ruspiro_pi3") ("multicore")))) (l "ruspiro_boot")))

(define-public crate-ruspiro-boot-0.4.2 (c (n "ruspiro-boot") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "ruspiro-cache") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "0h1xrydr7cv4fwkij7ikdpisw4jmm23v7lhmc62ak9snd6gx02r0") (f (quote (("ruspiro_pi3") ("multicore")))) (l "ruspiro_boot")))

(define-public crate-ruspiro-boot-0.5.0 (c (n "ruspiro-boot") (v "0.5.0") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "log") (r "~0.4") (k 0)) (d (n "ruspiro-cache") (r "~0.4") (d #t) (k 0)) (d (n "ruspiro-register") (r "~0.5") (d #t) (k 0)))) (h "1hjhknyqnlrc0l4ygijwpnzqq650k6ikla3i15ixd5xrfscy3z12") (f (quote (("multicore")))) (l "ruspiro_boot")))

(define-public crate-ruspiro-boot-0.5.1 (c (n "ruspiro-boot") (v "0.5.1") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "log") (r "~0.4") (k 0)) (d (n "ruspiro-cache") (r "~0.4") (d #t) (k 0)) (d (n "ruspiro-register") (r "~0.5") (d #t) (k 0)))) (h "0n8id1pdc95gc4c2fbdv1p61khbxxs3ibmws74a5yfx19a562qns") (f (quote (("multicore")))) (l "ruspiro_boot")))

(define-public crate-ruspiro-boot-0.5.2 (c (n "ruspiro-boot") (v "0.5.2") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "log") (r "~0.4.14") (k 0)) (d (n "ruspiro-cache") (r "~0.4.1") (d #t) (k 0)) (d (n "ruspiro-register") (r "~0.5.4") (d #t) (k 0)))) (h "1yn3fq2n4vsxa5p2hv75ivjlj328jd0yd35frqjlqnwbvcx7qr8a") (f (quote (("multicore")))) (l "ruspiro_boot")))

(define-public crate-ruspiro-boot-0.5.3 (c (n "ruspiro-boot") (v "0.5.3") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "log") (r "~0.4.14") (k 0)) (d (n "ruspiro-cache") (r "~0.4.1") (d #t) (k 0)) (d (n "ruspiro-register") (r "~0.5.4") (d #t) (k 0)))) (h "1mzlqlbvddz28k4y2akxv7ppfxg58bc6fhhjmn3p97ai98ndf2r4") (f (quote (("multicore")))) (l "ruspiro_boot")))

(define-public crate-ruspiro-boot-0.5.4 (c (n "ruspiro-boot") (v "0.5.4") (d (list (d (n "cc") (r "~1.0") (d #t) (k 1)) (d (n "log") (r "~0.4.14") (k 0)) (d (n "ruspiro-cache") (r "~0.4.1") (d #t) (k 0)) (d (n "ruspiro-register") (r "~0.5.5") (d #t) (k 0)))) (h "1bq894i59l59nwqz40rx1r1sajjvl45xq9l062pyil77nmapkg5q") (f (quote (("panic") ("multicore") ("default" "panic")))) (l "ruspiro_boot")))

