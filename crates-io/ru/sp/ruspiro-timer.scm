(define-module (crates-io ru sp ruspiro-timer) #:use-module (crates-io))

(define-public crate-ruspiro-timer-0.0.1 (c (n "ruspiro-timer") (v "0.0.1") (d (list (d (n "ruspiro-register") (r "^0.0.2") (d #t) (k 0)))) (h "07wr92lrvwnwakbk78lyas6w787qpwrm7jwa5arf05gxzvff93jk")))

(define-public crate-ruspiro-timer-0.1.0 (c (n "ruspiro-timer") (v "0.1.0") (d (list (d (n "ruspiro-register") (r "^0.1.1") (d #t) (k 0)))) (h "0f8x7v7nrxj8ls80afk94fgw2bmds0yaf7r710zaidgk676wbwmb") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-timer-0.3.0 (c (n "ruspiro-timer") (v "0.3.0") (d (list (d (n "ruspiro-register") (r "^0.3") (d #t) (k 0)))) (h "1r29r77xbjgldap9ay9j1vdd2yj7b67pmdk59ziwc1hxnyggsmgk") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-timer-0.4.0 (c (n "ruspiro-timer") (v "0.4.0") (d (list (d (n "ruspiro-interrupt") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "0g5z2036svbhrc3q5pcv0kg7j4j43lbmaq3dw30vbij4c7488jhr") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-timer-0.4.1 (c (n "ruspiro-timer") (v "0.4.1") (d (list (d (n "ruspiro-interrupt") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "02hwxb9kmvc40n9w72lqqh9am7b4xm1kz05zplkqdrfl9i12489b") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-timer-0.5.0 (c (n "ruspiro-timer") (v "0.5.0") (d (list (d (n "ruspiro-arch-aarch64") (r "~0.1.4") (d #t) (k 0)) (d (n "ruspiro-interrupt") (r "~0.4.3") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.2") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "~0.4.2") (d #t) (k 0)))) (h "15f7alrd90qcz7wymm60rg33m1cn8hf6aw4bis7kvcbzbipgiv09") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-timer-0.5.1 (c (n "ruspiro-timer") (v "0.5.1") (d (list (d (n "ruspiro-arch-aarch64") (r "~0.1.4") (d #t) (k 0)) (d (n "ruspiro-interrupt") (r "~0.4.3") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.2") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "~0.4.2") (d #t) (k 0)))) (h "1a00v522vcgsz85plf9r6z22pl6l521c5r4g7crqb9pdn5djv12z") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-timer-0.5.2 (c (n "ruspiro-timer") (v "0.5.2") (d (list (d (n "ruspiro-arch-aarch64") (r "~0.1.5") (d #t) (k 0)) (d (n "ruspiro-interrupt") (r "~0.4.3") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.3") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "~0.4.3") (d #t) (k 0)))) (h "17rqdjm7hzpnzcd6r69szvdaapazny26slp1l9wl7x4g117sjjbm") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-timer-0.6.0 (c (n "ruspiro-timer") (v "0.6.0") (d (list (d (n "ruspiro-arch-aarch64") (r "~0.1.5") (d #t) (k 0)) (d (n "ruspiro-interrupt") (r "~0.5.0") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.3") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "~0.4.3") (d #t) (k 0)))) (h "0q8i401mc738g6rkrjkv0qr3gji9zd1d9vvcm7zcbs07di9k58sc") (f (quote (("pi4_low" "ruspiro-interrupt/pi4_low") ("pi4_high" "ruspiro-interrupt/pi4_high") ("pi3" "ruspiro-interrupt/pi3"))))))

