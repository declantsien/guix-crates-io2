(define-module (crates-io ru sp ruspiro-mmio-register) #:use-module (crates-io))

(define-public crate-ruspiro-mmio-register-0.1.0 (c (n "ruspiro-mmio-register") (v "0.1.0") (h "0ghxk58f082r0xfxmi42w8k5zdjqy6lq2zcs5sqhc4vzm1pid8xr") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-mmio-register-0.1.1 (c (n "ruspiro-mmio-register") (v "0.1.1") (d (list (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "1a079p83hb2731f96x5w108bprm3xv6gi9f3fzclv53vsljksg9s") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-mmio-register-0.1.2 (c (n "ruspiro-mmio-register") (v "0.1.2") (d (list (d (n "ruspiro-register") (r "~0.5.4") (d #t) (k 0)))) (h "0jy5pzn3pxzzxni6f6hn4isyba3sw9sbys2m58d4x086izl5sil9")))

(define-public crate-ruspiro-mmio-register-0.1.3 (c (n "ruspiro-mmio-register") (v "0.1.3") (d (list (d (n "ruspiro-register") (r "~0.5.4") (d #t) (k 0)))) (h "18kbx46jlcm8ldba1jkjiaw9cxhk5cqnxb368vzmjcbsax4z1yzb")))

(define-public crate-ruspiro-mmio-register-0.1.4 (c (n "ruspiro-mmio-register") (v "0.1.4") (d (list (d (n "ruspiro-register") (r "~0.5.5") (d #t) (k 0)))) (h "113cn16h4xbgmavi4dap0vy2vvw80pz267ir85r1vi7pvs2vb57r")))

