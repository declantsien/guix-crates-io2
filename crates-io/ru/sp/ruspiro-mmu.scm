(define-module (crates-io ru sp ruspiro-mmu) #:use-module (crates-io))

(define-public crate-ruspiro-mmu-0.1.0 (c (n "ruspiro-mmu") (v "0.1.0") (d (list (d (n "ruspiro-arch-aarch64") (r "^0.1") (d #t) (k 0)))) (h "0kxgnphb856fnh3v5f1kfnypjw5g7j6h713y02j7d1z0rcg504v2") (f (quote (("ruspiro_pi3" "ruspiro-arch-aarch64/ruspiro_pi3")))) (l "ruspiro_mmu")))

(define-public crate-ruspiro-mmu-0.1.1 (c (n "ruspiro-mmu") (v "0.1.1") (d (list (d (n "ruspiro-arch-aarch64") (r "~0.1.4") (d #t) (k 0)))) (h "1a8xbfyl2rwajvnypl3yxq2z490iyk4314bdragvb2wgfihzgk5m") (l "ruspiro_mmu")))

