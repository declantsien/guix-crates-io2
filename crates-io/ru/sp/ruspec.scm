(define-module (crates-io ru sp ruspec) #:use-module (crates-io))

(define-public crate-ruspec-0.1.0 (c (n "ruspec") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.28") (f (quote ("full"))) (d #t) (k 0)))) (h "005c8c9pwy088vcngbpp9agc833kkp1q4inlqhmh5m6yiyx09y2x")))

(define-public crate-ruspec-0.1.1 (c (n "ruspec") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0lwxrzass91p3v10v27ikw0fx66l834bds49cyn1i73wnvj9xpk8") (y #t)))

(define-public crate-ruspec-0.1.2 (c (n "ruspec") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0kama2r6g7cqg9adgvnxxm9rfa9ml1galy46qj9l3chnh0g8199h")))

(define-public crate-ruspec-0.1.3 (c (n "ruspec") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.32") (f (quote ("full"))) (d #t) (k 0)))) (h "0vhfw86mv7vdmikcz7r7pi0kbcf87z2nanxizrdvhqg235kd7w09")))

