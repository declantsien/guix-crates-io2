(define-module (crates-io ru sp ruspiro-mailbox) #:use-module (crates-io))

(define-public crate-ruspiro-mailbox-0.0.2 (c (n "ruspiro-mailbox") (v "0.0.2") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.0.2") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.0.2") (d #t) (k 0)))) (h "0rsiii9f44p1ninj8yndv32dhjff2dfv4krm5l1h1v0113pfcrmi")))

(define-public crate-ruspiro-mailbox-0.1.0 (c (n "ruspiro-mailbox") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.1.1") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.1.0") (d #t) (k 0)))) (h "1giz48065a2nbzfxc2v2a9nnkkavmjrq6dbm2y7d5sfmbndf32fb") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-mailbox-0.1.1 (c (n "ruspiro-mailbox") (v "0.1.1") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "ruspiro-cache") (r "^0.1.1") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.1.1") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.1.0") (d #t) (k 0)))) (h "00brl2w31k769q3sisifijl7kh841i2pwg8aan288b3786cdcxp7") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-mailbox-0.2.0 (c (n "ruspiro-mailbox") (v "0.2.0") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "ruspiro-cache") (r "^0.1") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.1") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.2") (d #t) (k 0)))) (h "00pj0f3zsgdcgklh0q324p51951l78hj46jdc884a64x2js0a0w5") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-mailbox-0.3.0 (c (n "ruspiro-mailbox") (v "0.3.0") (d (list (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "ruspiro-cache") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-console") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "0g8whc99dm9s8lf1hwgggvr8k9i0gzbabg3b2cfq57a86ngcnhbz") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-mailbox-0.3.1 (c (n "ruspiro-mailbox") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "ruspiro-cache") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-console") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "1ksh36is6amb9ldhcjccwkxcn3lv8d8ih7mahcyfrgci044q8gpw") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-mailbox-0.4.0 (c (n "ruspiro-mailbox") (v "0.4.0") (d (list (d (n "paste") (r "^1.0.1") (d #t) (k 0)) (d (n "ruspiro-cache") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-error") (r "^0.1") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "^0.1") (d #t) (k 0)))) (h "0jhivck6ylza17cw0ha98qviysqg9mjgsqrn0wvfdykmc09436vn") (f (quote (("ruspiro_pi3" "ruspiro-mmio-register/ruspiro_pi3" "ruspiro-cache/ruspiro_pi3" "ruspiro-error/ruspiro_pi3"))))))

(define-public crate-ruspiro-mailbox-0.4.1 (c (n "ruspiro-mailbox") (v "0.4.1") (d (list (d (n "paste") (r "~1.0.5") (d #t) (k 0)) (d (n "ruspiro-cache") (r "~0.4.1") (d #t) (k 0)) (d (n "ruspiro-error") (r "~0.1.1") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.3") (d #t) (k 0)))) (h "1dmfpppk13i6215nysdhip2knjm9l9c083k2nxzrn4hwls8ra9ah") (f (quote (("ruspiro_pi3"))))))

