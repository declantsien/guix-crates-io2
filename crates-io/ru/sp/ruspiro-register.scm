(define-module (crates-io ru sp ruspiro-register) #:use-module (crates-io))

(define-public crate-ruspiro-register-0.0.2 (c (n "ruspiro-register") (v "0.0.2") (h "18f3y3hvdw2ld4r7bkq2g8m2qzgmpvqjm8y632awbyb7y1n93a7x")))

(define-public crate-ruspiro-register-0.1.0 (c (n "ruspiro-register") (v "0.1.0") (h "04w1fhy9h12gf0z9ik18nqv3aiv6arlhx5fwgk3r3m7dkxsp8fvv")))

(define-public crate-ruspiro-register-0.1.1 (c (n "ruspiro-register") (v "0.1.1") (h "051ly9ljljix2lj3q5yx736h6ydg5pfaqfg5lzrg5agbjj4z505r")))

(define-public crate-ruspiro-register-0.3.0 (c (n "ruspiro-register") (v "0.3.0") (h "0j66r1mh5rcsn7p1piqw6xqvvhhzz0szkg6pl90zqw2as66q0c5k")))

(define-public crate-ruspiro-register-0.3.1 (c (n "ruspiro-register") (v "0.3.1") (h "1wzzs5i5vlspa308fg64agj6qhvnldnp84nlr52hfbqq62bj6va4")))

(define-public crate-ruspiro-register-0.4.0 (c (n "ruspiro-register") (v "0.4.0") (h "1lg53l9fs91imvz0ablvcvcnw0j7dzkyx7rnjc40a945y98rbd6z")))

(define-public crate-ruspiro-register-0.4.1 (c (n "ruspiro-register") (v "0.4.1") (h "084l6jd94zdipq5hf7lns8yyf4d8xl232izfb30cbyxlff85bxp9")))

(define-public crate-ruspiro-register-0.4.2 (c (n "ruspiro-register") (v "0.4.2") (h "1wcyy6sqdpvl7zq4c1hlyrhlirc48vma558yf1mp2b9q41mc5rk6")))

(define-public crate-ruspiro-register-0.4.3 (c (n "ruspiro-register") (v "0.4.3") (h "0f6kl8p6pf0lzb5difnxc6njw1881lba8l8q6i3b78k2ywg3i79g")))

(define-public crate-ruspiro-register-0.5.0 (c (n "ruspiro-register") (v "0.5.0") (h "13z6j8rmnqjp45hnplx0sfcjipyn2g675kdf5f8r7vx1b986kadv") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-register-0.5.1 (c (n "ruspiro-register") (v "0.5.1") (h "0z87pzjmdy3nf6vahd2dkqal43dx5w941llr71qcjlypilypkwqx") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-register-0.5.2 (c (n "ruspiro-register") (v "0.5.2") (h "04lidcnsq1zck69gyqyq3cl8v2qjc61vdbw78v5mm0qzd7wk1107")))

(define-public crate-ruspiro-register-0.5.3 (c (n "ruspiro-register") (v "0.5.3") (h "1qzgpk9xls628fwjcfvhkscjii2kmf3fkw94g1sby7cl7zqmdxz3")))

(define-public crate-ruspiro-register-0.5.4 (c (n "ruspiro-register") (v "0.5.4") (h "0jg35v3mwv6k6nm1g398vf3bld7vcmi880c3yx62m0ddj9wmy78n")))

(define-public crate-ruspiro-register-0.5.5 (c (n "ruspiro-register") (v "0.5.5") (h "1xpwf3l0i6161q1fj510n1c0wlgdiyrfw6213b4s12mrzdzqlfbc")))

