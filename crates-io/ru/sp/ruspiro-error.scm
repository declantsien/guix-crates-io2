(define-module (crates-io ru sp ruspiro-error) #:use-module (crates-io))

(define-public crate-ruspiro-error-0.1.0 (c (n "ruspiro-error") (v "0.1.0") (h "1n1j7b2dlm79kwanb8xz3k0ls5iywdlja6px5635skcww4f777nr") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-error-0.1.1 (c (n "ruspiro-error") (v "0.1.1") (h "0hmn7xl34bxl1qk6w2pshjnjimwprc4z0pj0xrb25bcisa5i00wp")))

