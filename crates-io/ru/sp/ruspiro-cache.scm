(define-module (crates-io ru sp ruspiro-cache) #:use-module (crates-io))

(define-public crate-ruspiro-cache-0.1.0 (c (n "ruspiro-cache") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "0g0sqpqjqqmll13cvxazlz98z0lk3539g472ww4fqfvrg0b54akh") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-cache-0.1.1 (c (n "ruspiro-cache") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "1h8akjv8rww3a916ajvk5ywh9f9pyjzh2429n1b2gmwaypr0icb7") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-cache-0.3.0 (c (n "ruspiro-cache") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)))) (h "015fhzkzr1sizd52ir6di55var5cnz25m3psv0d81qhdj729d050") (l "ruspiro-cache")))

(define-public crate-ruspiro-cache-0.4.0 (c (n "ruspiro-cache") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.60") (d #t) (k 1)) (d (n "ruspiro-arch-aarch64") (r "^0.1.3") (d #t) (k 0)))) (h "12gdmbgbkara5jja9ah8xi244q1915pymf6bz8nwasdfx6k6hrlj") (f (quote (("ruspiro_pi3" "ruspiro-arch-aarch64/ruspiro_pi3")))) (l "ruspiro_cache")))

(define-public crate-ruspiro-cache-0.4.1 (c (n "ruspiro-cache") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.60") (d #t) (k 1)) (d (n "ruspiro-arch-aarch64") (r "^0.1.3") (d #t) (k 0)))) (h "1wcjmp4qp8ll44w8c3bdlvj759nk1nycas7kb1dcy6f3lnkjbghf") (l "ruspiro_cache")))

