(define-module (crates-io ru sp ruspiro-uart) #:use-module (crates-io))

(define-public crate-ruspiro-uart-0.0.2 (c (n "ruspiro-uart") (v "0.0.2") (d (list (d (n "ruspiro-console") (r "^0.0.2") (d #t) (k 0)) (d (n "ruspiro-gpio") (r "^0.0.2") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.0.2") (d #t) (k 0)))) (h "1jfxcfz9xwv88gs2xqgcjzvnaifvsqc8jzbj059dmn591xmc7gx4") (y #t)))

(define-public crate-ruspiro-uart-0.0.3 (c (n "ruspiro-uart") (v "0.0.3") (d (list (d (n "ruspiro-console") (r "^0.0.2") (d #t) (k 0)) (d (n "ruspiro-gpio") (r "^0.0.2") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.0.2") (d #t) (k 0)))) (h "0hkz7q6vj4wn72vcpyjdrbyi6i8gxy9zldx51snmrrnzdf3ycijy")))

(define-public crate-ruspiro-uart-0.1.0 (c (n "ruspiro-uart") (v "0.1.0") (d (list (d (n "ruspiro-console") (r "^0.1.0") (d #t) (k 0)) (d (n "ruspiro-gpio") (r "^0.1.0") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.1.1") (d #t) (k 0)))) (h "059wvh9vr4zcxllaf20cpcsjc08gd4qv0yiakq2ji3a8rbn5hbyl") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-uart-0.2.0 (c (n "ruspiro-uart") (v "0.2.0") (d (list (d (n "ruspiro-console") (r "^0.2") (d #t) (k 0)) (d (n "ruspiro-gpio") (r "^0.2") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.1") (d #t) (k 0)) (d (n "ruspiro-timer") (r "^0.1") (d #t) (k 0)))) (h "0slckahhy3lil5xp8jfsh45wy8f9zx5crs60bn8rdw1qp9vaxkid") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-uart-0.3.0 (c (n "ruspiro-uart") (v "0.3.0") (d (list (d (n "ruspiro-console") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-gpio") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-timer") (r "^0.3") (d #t) (k 0)))) (h "1myfq4jzv94kk5miy328npsizkpvq23qis1p41a6jvw2lhg2a3gn") (f (quote (("ruspiro_pi3" "ruspiro-gpio/ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-uart-0.3.1 (c (n "ruspiro-uart") (v "0.3.1") (d (list (d (n "ruspiro-console") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-gpio") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-timer") (r "^0.4") (d #t) (k 0)))) (h "1an33sb14xgbvc9lxwp28wmhr4rb1syxnvf8j3964r583a8rrk69") (f (quote (("ruspiro_pi3" "ruspiro-gpio/ruspiro_pi3" "ruspiro-timer/ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-uart-0.4.0 (c (n "ruspiro-uart") (v "0.4.0") (d (list (d (n "ruspiro-gpio") (r "~0.4.3") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "~0.5.4") (d #t) (k 0)) (d (n "ruspiro-timer") (r "~0.5.2") (d #t) (k 0)))) (h "00bb9q2wp94nwk047bl10q6g4g3rj79qqchxhyd42gaq1xwnn0y4") (f (quote (("ruspiro_pi3" "ruspiro-gpio/ruspiro_pi3" "ruspiro-timer/ruspiro_pi3"))))))

