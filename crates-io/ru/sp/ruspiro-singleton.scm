(define-module (crates-io ru sp ruspiro-singleton) #:use-module (crates-io))

(define-public crate-ruspiro-singleton-0.0.2 (c (n "ruspiro-singleton") (v "0.0.2") (d (list (d (n "ruspiro-lock") (r "^0.0.2") (d #t) (k 0)))) (h "00vw7i31zx9w1zsc29pvbnch19ljv5r80jxdk1xpgz15nimxyd23")))

(define-public crate-ruspiro-singleton-0.1.0 (c (n "ruspiro-singleton") (v "0.1.0") (d (list (d (n "ruspiro-lock") (r "^0.1.0") (d #t) (k 0)))) (h "0gc6xx5jwhgaxmb9h6x1hxx3kxqgw8rqy3mdi36a9hqv3x6dxyq3")))

(define-public crate-ruspiro-singleton-0.2.0 (c (n "ruspiro-singleton") (v "0.2.0") (d (list (d (n "ruspiro-interrupt-core") (r "^0.2") (d #t) (k 0)) (d (n "ruspiro-lock") (r "^0.2") (d #t) (k 0)))) (h "1qi297b6qk09682f4qwy9asvwm3gsq28l8qiwwih66y3x92ypd52")))

(define-public crate-ruspiro-singleton-0.2.1 (c (n "ruspiro-singleton") (v "0.2.1") (d (list (d (n "ruspiro-lock") (r "^0.2") (d #t) (k 0)))) (h "1gzpnxgs0n6qsjpsyjl3iiplcxmbvvnm3qip9r5rlv5crrf38rh2")))

(define-public crate-ruspiro-singleton-0.2.2 (c (n "ruspiro-singleton") (v "0.2.2") (d (list (d (n "ruspiro-lock") (r "^0.2") (d #t) (k 0)))) (h "0x0q30g3hckbzjgn8h1lya3h40rngjg0j42dv0wqqndwq7biq8dn")))

(define-public crate-ruspiro-singleton-0.3.0 (c (n "ruspiro-singleton") (v "0.3.0") (d (list (d (n "ruspiro-lock") (r "^0.3") (d #t) (k 0)))) (h "15sgg6vmijinlk99izk74qjmf7a577227ik74l9v851ysn6rp4g7")))

(define-public crate-ruspiro-singleton-0.3.1 (c (n "ruspiro-singleton") (v "0.3.1") (d (list (d (n "ruspiro-lock") (r "^0.3") (d #t) (k 0)))) (h "1np2ymimdz9s5zyadzwn70cx2k6mqcywqn77z90sqdxaiphp9ngg")))

(define-public crate-ruspiro-singleton-0.4.0 (c (n "ruspiro-singleton") (v "0.4.0") (d (list (d (n "ruspiro-lock") (r "^0.4.0") (d #t) (k 0)))) (h "1j0wqr0mp6kf8q02zlm41f1z0qz2lr4r5hmp9fiv07p2acpqflnm") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-singleton-0.4.1 (c (n "ruspiro-singleton") (v "0.4.1") (d (list (d (n "ruspiro-lock") (r "^0.4.1") (d #t) (k 0)))) (h "1d9z6wdd4mhb8zm63iw9c6xlwn9wh9wwwp0csav1xip76153mgb1") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-singleton-0.4.2 (c (n "ruspiro-singleton") (v "0.4.2") (d (list (d (n "ruspiro-lock") (r "~0.4.2") (d #t) (k 0)))) (h "14ay3qhwngsw86krl8861819abz45bln8r5qcn3k03mp8z6h04ly")))

(define-public crate-ruspiro-singleton-0.4.3 (c (n "ruspiro-singleton") (v "0.4.3") (d (list (d (n "ruspiro-lock") (r "~0.4.2") (d #t) (k 0)))) (h "1v8rvf9vjss9m7pf7bvhjsyxiyrzr4brgnhsc2z2y79nrsn5wb3v")))

