(define-module (crates-io ru sp ruspiro-console) #:use-module (crates-io))

(define-public crate-ruspiro-console-0.0.2 (c (n "ruspiro-console") (v "0.0.2") (d (list (d (n "ruspiro-allocator") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.0.2") (d #t) (k 0)))) (h "0c84li138qsbkhhmp5ibbqa0qgwwygf14h1233adyafwwwvg0fvp") (f (quote (("with_allocator" "ruspiro-allocator"))))))

(define-public crate-ruspiro-console-0.1.0 (c (n "ruspiro-console") (v "0.1.0") (d (list (d (n "ruspiro-allocator") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.1.0") (d #t) (k 0)))) (h "1gl8mxmswfpawwxrf24qrhh1bc4p2hbpznmyb4k07pj06cyr222z") (f (quote (("with_allocator" "ruspiro-allocator"))))))

(define-public crate-ruspiro-console-0.1.1 (c (n "ruspiro-console") (v "0.1.1") (d (list (d (n "ruspiro-allocator") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.1.0") (d #t) (k 0)))) (h "09jvbkxarckbd7r5nyxxxigbq0cfmg06im7mj4zzvy46wkfm0zaa") (f (quote (("with_allocator" "ruspiro-allocator"))))))

(define-public crate-ruspiro-console-0.2.0 (c (n "ruspiro-console") (v "0.2.0") (d (list (d (n "ruspiro-allocator") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.2") (d #t) (k 0)))) (h "1dlhvznrjbdpq5j8piwyhix7g2d6k2mgyjmayhlvly9nin81hwsh") (f (quote (("with_allocator" "ruspiro-allocator"))))))

(define-public crate-ruspiro-console-0.2.1 (c (n "ruspiro-console") (v "0.2.1") (d (list (d (n "ruspiro-allocator") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.2") (d #t) (k 0)) (d (n "ruspiro-timer") (r "^0.1") (d #t) (k 0)))) (h "07p47kdiq9p28w4j8l6nahipxrjy0mxxifln61rl3xc9fxllanq9") (f (quote (("with_allocator" "ruspiro-allocator"))))))

(define-public crate-ruspiro-console-0.2.2 (c (n "ruspiro-console") (v "0.2.2") (d (list (d (n "ruspiro-allocator") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.2") (d #t) (k 0)))) (h "12mxzv885z858mhgifffmf88n56nijz1j8jl8lia296gmnn6yl2a") (f (quote (("with_allocator" "ruspiro-allocator"))))))

(define-public crate-ruspiro-console-0.3.0 (c (n "ruspiro-console") (v "0.3.0") (d (list (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "03i333psjk2zai7hkg7qi61v84w3i2nnyv6zb70jm0i53f70fkz8")))

(define-public crate-ruspiro-console-0.3.1 (c (n "ruspiro-console") (v "0.3.1") (d (list (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "0hxyfzfs5981dxj16isw15b6hfx9k8jlazp6w2r4wc84bsxhj4lv")))

(define-public crate-ruspiro-console-0.3.2 (c (n "ruspiro-console") (v "0.3.2") (d (list (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "1bryd36lgnm89r1ky7x4jfj2hj9fpbwad6n10j1g071ca5bka55b")))

(define-public crate-ruspiro-console-0.4.0 (c (n "ruspiro-console") (v "0.4.0") (d (list (d (n "ruspiro-singleton") (r "^0.4.0") (d #t) (k 0)))) (h "0zpx416vpvl8dggfmaidl6s2fn8knphrk7wcf7zhzn4kzdcdx434") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-console-0.4.1 (c (n "ruspiro-console") (v "0.4.1") (d (list (d (n "ruspiro-singleton") (r "^0.4.1") (d #t) (k 0)))) (h "0iakr03csasvlwqdmk6nsabn9c34w9grsp1p1hr7g8cza69liiwh") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-console-0.5.0 (c (n "ruspiro-console") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "ruspiro-singleton") (r "^0.4.1") (d #t) (k 0)))) (h "1r7h7k1szwkzncp8j9ff6yd2aybm6ymfmlawj4lwpscv11mviixx") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-console-0.5.1 (c (n "ruspiro-console") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "ruspiro-singleton") (r "^0.4.1") (d #t) (k 0)))) (h "0jylw4rj0xw5x839g5ivyrqgyrirxwm6zx7plrrcm20pda4qhp95") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-console-0.5.2 (c (n "ruspiro-console") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "ruspiro-singleton") (r "^0.4.1") (d #t) (k 0)))) (h "0wlyanpwipkiwapsxwrinjvwhh32n3wrd7fj42q6m7g92rl76y51") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-console-0.5.3 (c (n "ruspiro-console") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "ruspiro-singleton") (r "^0.4.1") (d #t) (k 0)))) (h "1zskrh9k4i7rf2xjykx7divgaa0s622hs7aam30mjnynk647rfvq") (f (quote (("ruspiro_pi3"))))))

