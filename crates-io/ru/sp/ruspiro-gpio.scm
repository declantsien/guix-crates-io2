(define-module (crates-io ru sp ruspiro-gpio) #:use-module (crates-io))

(define-public crate-ruspiro-gpio-0.0.2 (c (n "ruspiro-gpio") (v "0.0.2") (d (list (d (n "ruspiro-register") (r "^0.0.2") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.0.2") (d #t) (k 0)))) (h "1yy5a6jm6mdvn114vbdjwbyp96awsjy5ag88d019amm4hl62hz44")))

(define-public crate-ruspiro-gpio-0.1.0 (c (n "ruspiro-gpio") (v "0.1.0") (d (list (d (n "ruspiro-register") (r "^0.1.1") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.1.0") (d #t) (k 0)))) (h "1i4yncl18d2j6rcil6hi584izqjklpq4310k43s1amz78yk99h80") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-gpio-0.2.1 (c (n "ruspiro-gpio") (v "0.2.1") (d (list (d (n "ruspiro-register") (r "^0.1") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.2") (d #t) (k 0)))) (h "12fzgg2ysbnd3gdfvlj8ddrjga72w9kn1qwbmc51wn7sr2f2w810") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-gpio-0.3.0 (c (n "ruspiro-gpio") (v "0.3.0") (d (list (d (n "ruspiro-register") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "1r7yhac47lksv6p5g1ryr3ahcidycz7c297y8k6ravh0k5yd8hbl") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-gpio-0.4.0 (c (n "ruspiro-gpio") (v "0.4.0") (d (list (d (n "ruspiro-interrupt") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "0y1rbk0hyqdf8z263qds6pqkxxmg8kq9ry1gzi3yxff1bns8hvz1") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-gpio-0.4.1 (c (n "ruspiro-gpio") (v "0.4.1") (d (list (d (n "ruspiro-interrupt") (r "^0.3") (d #t) (k 0)) (d (n "ruspiro-register") (r "^0.4") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "^0.3") (d #t) (k 0)))) (h "11s9sfq0yns0y0fagrfvn73f2gaggy32f2vq2gsdslg9n5qx1dhz") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-gpio-0.4.2 (c (n "ruspiro-gpio") (v "0.4.2") (d (list (d (n "ruspiro-interrupt") (r "~0.4.3") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.2") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "~0.4.2") (d #t) (k 0)))) (h "0x02smsqplf194zqshakxyjklnm6bklm86rj5k7vznigmaarp3hz") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

(define-public crate-ruspiro-gpio-0.4.3 (c (n "ruspiro-gpio") (v "0.4.3") (d (list (d (n "ruspiro-interrupt") (r "~0.4.3") (d #t) (k 0)) (d (n "ruspiro-mmio-register") (r "~0.1.3") (d #t) (k 0)) (d (n "ruspiro-singleton") (r "~0.4.3") (d #t) (k 0)))) (h "1h7b0zwfjgzg638dk5xdrfqmqplxkwf4w0nqsvla5xniaapgn49x") (f (quote (("ruspiro_pi3" "ruspiro-interrupt/ruspiro_pi3"))))))

