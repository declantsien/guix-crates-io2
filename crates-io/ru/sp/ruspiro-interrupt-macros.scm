(define-module (crates-io ru sp ruspiro-interrupt-macros) #:use-module (crates-io))

(define-public crate-ruspiro-interrupt-macros-0.1.0 (c (n "ruspiro-interrupt-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10116i1bvjhwy59a4q3wlh40s4wgw9g33n926v60prm0i6hf9gg3")))

(define-public crate-ruspiro-interrupt-macros-0.2.0 (c (n "ruspiro-interrupt-macros") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1lncdqam97qqynwncdxp76d1dsvy7aqarf7v89mhglzvj8b19i4p")))

(define-public crate-ruspiro-interrupt-macros-0.2.1 (c (n "ruspiro-interrupt-macros") (v "0.2.1") (d (list (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ravh9l55vrhyk46x89bn3pazyvml4xly3vrzca0d2z3m8spix1v")))

(define-public crate-ruspiro-interrupt-macros-0.2.2 (c (n "ruspiro-interrupt-macros") (v "0.2.2") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12f6qhz446b5h34b63mcjfyrpq4g38pfg64y01bsk6q46pzfsc5r") (f (quote (("async")))) (y #t)))

(define-public crate-ruspiro-interrupt-macros-0.3.1 (c (n "ruspiro-interrupt-macros") (v "0.3.1") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1amcj4xlb5z579xykn24mmlrgxsi50g59y3d68qzvf0w7s9wy490") (f (quote (("async"))))))

(define-public crate-ruspiro-interrupt-macros-0.3.2 (c (n "ruspiro-interrupt-macros") (v "0.3.2") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1slv704yv43hfwh483p4vlp5ps0l8cs5x6pid2nm76ndjk1k43gj") (f (quote (("async"))))))

(define-public crate-ruspiro-interrupt-macros-0.5.0 (c (n "ruspiro-interrupt-macros") (v "0.5.0") (d (list (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1rbnj242l5k3a7c87iw1va0m9xlbwzlcymhzvy9r7m2dsqsc739c") (f (quote (("async"))))))

