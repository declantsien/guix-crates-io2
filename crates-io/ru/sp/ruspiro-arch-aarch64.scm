(define-module (crates-io ru sp ruspiro-arch-aarch64) #:use-module (crates-io))

(define-public crate-ruspiro-arch-aarch64-0.1.0 (c (n "ruspiro-arch-aarch64") (v "0.1.0") (h "0fj3lnfar32j72k6fnwjiq0w9bzhficni4w4g1w2my52wcahndc7") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-arch-aarch64-0.1.1 (c (n "ruspiro-arch-aarch64") (v "0.1.1") (h "153a23i7vrrwhhsh0nlygfgidcargzc97kz7c3w5jxyr9waxyxm3") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-arch-aarch64-0.1.2 (c (n "ruspiro-arch-aarch64") (v "0.1.2") (d (list (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "13iwjwda5k7896bnm82242k0jlr0jqgqindjv1j2nxa7gkvhg3kc") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-arch-aarch64-0.1.3 (c (n "ruspiro-arch-aarch64") (v "0.1.3") (d (list (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "0kxkrcmr6j01nlyfiph1xk2bhk9sqm9m0cyy72xs05q828639g22") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-arch-aarch64-0.1.4 (c (n "ruspiro-arch-aarch64") (v "0.1.4") (d (list (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "0l40bs5962gysy480f5w777ix17mdh0x2prjjh1cc1kskr1qaxa8") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-arch-aarch64-0.1.5 (c (n "ruspiro-arch-aarch64") (v "0.1.5") (d (list (d (n "ruspiro-register") (r "^0.5") (d #t) (k 0)))) (h "08bcp7vabm67ka6fisilj4kim1fqz8iv2s3sfpim1dwg45gnaw3h") (f (quote (("ruspiro_pi3"))))))

(define-public crate-ruspiro-arch-aarch64-0.1.6 (c (n "ruspiro-arch-aarch64") (v "0.1.6") (d (list (d (n "ruspiro-register") (r "~0.5.4") (d #t) (k 0)))) (h "12wv22bb6wfffakzlpcmb382b5fcpwl6bmcpxh04axldqmwwq5sp")))

(define-public crate-ruspiro-arch-aarch64-0.1.7 (c (n "ruspiro-arch-aarch64") (v "0.1.7") (d (list (d (n "ruspiro-register") (r "~0.5.5") (d #t) (k 0)))) (h "01wl9jgfdhvzm26yg9f3gp719mrp2cpfdz5jac99qv0snwbf3sv0")))

