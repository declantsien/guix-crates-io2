(define-module (crates-io ru sp ruspiro-channel) #:use-module (crates-io))

(define-public crate-ruspiro-channel-0.1.0 (c (n "ruspiro-channel") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (o #t) (k 0)) (d (n "rlibc") (r "=1.0.0") (d #t) (k 0)) (d (n "ruspiro-arch-aarch64") (r "^0.1") (d #t) (k 0)))) (h "1m42rmjpmv0bvii7jf6bvc9q0fh102gl31jzxyd2cqi8104bqz1z") (f (quote (("async" "futures-util"))))))

(define-public crate-ruspiro-channel-0.1.1 (c (n "ruspiro-channel") (v "0.1.1") (d (list (d (n "futures-util") (r "~0.3.14") (o #t) (k 0)) (d (n "ruspiro-arch-aarch64") (r "~0.1.4") (d #t) (k 0)))) (h "0axcvd65di3a4pnppq45zqzghcdf5jxcp0rbi8dk3z52lihrq203") (f (quote (("async" "futures-util"))))))

