(define-module (crates-io ru sp ruspiro-allocator) #:use-module (crates-io))

(define-public crate-ruspiro-allocator-0.0.2 (c (n "ruspiro-allocator") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "1kpj6apcsxbcnnlrvj9x9aagg0b1znb6fjafv7dqddag55gigmji")))

(define-public crate-ruspiro-allocator-0.1.0 (c (n "ruspiro-allocator") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)))) (h "12hfy4kci9ch34zlcafj1j8bg429qhkiygvdmw5d3g2pmr2d0c62")))

(define-public crate-ruspiro-allocator-0.1.1 (c (n "ruspiro-allocator") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "ruspiro-allocator-oom") (r "^0.1.0") (d #t) (k 0)))) (h "0yfkkgvvlwc4ph8bl8r583sw03hy9zq2dx35vz8xcwhf3k6vc0py")))

(define-public crate-ruspiro-allocator-0.1.2 (c (n "ruspiro-allocator") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "ruspiro-allocator-oom") (r "^0.1.0") (d #t) (k 0)))) (h "173p9hrj9f2nkdg78jm87xpcrcv8lckc5q5cw4xxi0im4dnvkajv")))

(define-public crate-ruspiro-allocator-0.2.0 (c (n "ruspiro-allocator") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "ruspiro-allocator-oom") (r "^0.1") (d #t) (k 0)) (d (n "ruspiro-lock") (r "^0.2") (d #t) (k 0)))) (h "0rv207rzjw4kjdfv261apncnvdlb9y2ahn0msi6753lsqwq0f9gh")))

(define-public crate-ruspiro-allocator-0.3.0 (c (n "ruspiro-allocator") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "ruspiro-lock") (r "^0.3") (d #t) (k 0)))) (h "1msd8zjz28yn1pmgykpdj37q9gbq4iww8zwcs9qc2fr9b7wh0zra")))

(define-public crate-ruspiro-allocator-0.3.1 (c (n "ruspiro-allocator") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.37") (d #t) (k 1)) (d (n "ruspiro-lock") (r "^0.3") (d #t) (k 0)))) (h "1vd560jvkybxq61m256gcvbn3drrylhd14qy3q81bh7syjzd6w12")))

(define-public crate-ruspiro-allocator-0.4.0 (c (n "ruspiro-allocator") (v "0.4.0") (h "00wx77d91nz1fx0al0bib3cip4mn9xnm6qhgar8j6v9vlzl6rywc")))

(define-public crate-ruspiro-allocator-0.4.1 (c (n "ruspiro-allocator") (v "0.4.1") (h "0pg8a0n5pi1x2m8nqnkrxy2khd1a2dqfrl2krjqm1z2p00c81jzz") (l "ruspiro_allocator")))

(define-public crate-ruspiro-allocator-0.5.0 (c (n "ruspiro-allocator") (v "0.5.0") (h "0xl38kyijkz9l4mycgjlvnqyz6syd2jlc2x33cnpms913qkfywh2") (f (quote (("ruspiro_pi3")))) (y #t) (l "ruspiro_allocator")))

(define-public crate-ruspiro-allocator-0.4.2 (c (n "ruspiro-allocator") (v "0.4.2") (h "00sch0gz7i6wyjv6rq21c14m6f0hypy7b878id1qmmhqr96qmp41") (f (quote (("ruspiro_pi3")))) (l "ruspiro_allocator")))

(define-public crate-ruspiro-allocator-0.4.3 (c (n "ruspiro-allocator") (v "0.4.3") (h "1yvy3r1pqfxzkz22fxmb1w0zar8nicahrwyb6ccfz4g25i6qz5cn") (f (quote (("ruspiro_pi3")))) (l "ruspiro_allocator")))

(define-public crate-ruspiro-allocator-0.4.4 (c (n "ruspiro-allocator") (v "0.4.4") (h "0hl759qmdb9ia97jg41bnzqfz4cn1ahal1rz81wkgfckr5wckqp8") (l "ruspiro_allocator")))

(define-public crate-ruspiro-allocator-0.4.5 (c (n "ruspiro-allocator") (v "0.4.5") (d (list (d (n "rlibc") (r "~1.0.0") (d #t) (k 0)))) (h "1l4m82w9ki32akrin452p25h346cjmkgv7pspsgjkrm78fq50q5c") (l "ruspiro_allocator")))

(define-public crate-ruspiro-allocator-0.4.6 (c (n "ruspiro-allocator") (v "0.4.6") (d (list (d (n "rlibc") (r "~1.0.0") (d #t) (k 0)))) (h "1606pgy8ykpp2c1s56anw5ka196grwn24pr77vwz2555myc6mfcf") (l "ruspiro_allocator")))

