(define-module (crates-io ru sp ruspiro-lock) #:use-module (crates-io))

(define-public crate-ruspiro-lock-0.0.2 (c (n "ruspiro-lock") (v "0.0.2") (h "0ppf820brw7klk337yl5day2swwsqa5w562hl35k03q5ixk68zvl")))

(define-public crate-ruspiro-lock-0.1.0 (c (n "ruspiro-lock") (v "0.1.0") (h "1g79jqmcn26rk7hpl09kd61cd1xlgl6wywc3d62dr42bzq2zwman")))

(define-public crate-ruspiro-lock-0.2.0 (c (n "ruspiro-lock") (v "0.2.0") (d (list (d (n "ruspiro-interrupt-core") (r "^0.2") (d #t) (k 0)))) (h "0hmpyj87mqxcgw9bkwv7sfm5z0vawhpy7qv568kim59v2jnvgibz") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-lock-0.2.1 (c (n "ruspiro-lock") (v "0.2.1") (d (list (d (n "ruspiro-interrupt-core") (r "^0.2") (d #t) (k 0)))) (h "0llfh96aijnk0qjjv7in7hs6vqvfkqpkrkx6ffhlwab5kwfii93r") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-lock-0.2.2 (c (n "ruspiro-lock") (v "0.2.2") (d (list (d (n "ruspiro-interrupt-core") (r "^0.2") (d #t) (k 0)))) (h "0j0g1fxzcviwcrh2xafk70w5rmvwjx8kd2ndc50rpcygqspmgn7k") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-lock-0.3.0 (c (n "ruspiro-lock") (v "0.3.0") (d (list (d (n "ruspiro-interrupt-core") (r "^0.3") (d #t) (k 0)))) (h "0hwyl1wzmgyy6rnlbjmai9rnhpiw00ib5lncp11qx41gbazbczg8") (f (quote (("ruspiro_pi3") ("default" "ruspiro_pi3"))))))

(define-public crate-ruspiro-lock-0.3.1 (c (n "ruspiro-lock") (v "0.3.1") (d (list (d (n "ruspiro-interrupt-core") (r "^0.3") (d #t) (k 0)))) (h "13ip0nqxrg1yhaf55fxdci2p3lcgmp1za5s1vhnj6wpqpmfs396w")))

(define-public crate-ruspiro-lock-0.3.2 (c (n "ruspiro-lock") (v "0.3.2") (h "0p18gbbxi5v5hd5hjqz1fqhqfmr8shq7787bn6585ibwjxwv44j7")))

(define-public crate-ruspiro-lock-0.3.3 (c (n "ruspiro-lock") (v "0.3.3") (h "1y3mm3dfwgx250xdbjxpnv4kjckjsa0y1ad0f8xq97sc4fcl79r8")))

(define-public crate-ruspiro-lock-0.4.0 (c (n "ruspiro-lock") (v "0.4.0") (d (list (d (n "async-std") (r "^1.6.4") (f (quote ("attributes" "unstable"))) (d #t) (k 2)))) (h "0k8yd0mw08jncjf1fv50c5qs9cdag2x6k2c2jvzzilrj2w54b240") (f (quote (("ruspiro_pi3") ("async_locks"))))))

(define-public crate-ruspiro-lock-0.4.1 (c (n "ruspiro-lock") (v "0.4.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)))) (h "14a2n8h4kg8yxhr0gi584b344cjgfa33xncw7g68372npwaf37zf") (f (quote (("ruspiro_pi3") ("async_locks"))))))

(define-public crate-ruspiro-lock-0.4.2 (c (n "ruspiro-lock") (v "0.4.2") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)))) (h "1ga00yr07fzd2fi9i7qiy89mypqjq81kvrp4xlz324cxyh11qfqp") (f (quote (("async_locks"))))))

(define-public crate-ruspiro-lock-0.4.3 (c (n "ruspiro-lock") (v "0.4.3") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)))) (h "1sb5br4l1ny1lx6h9rb66pf4izmgg2vm4n395kj5kxm3r6ikv0r2") (f (quote (("async_locks"))))))

(define-public crate-ruspiro-lock-0.5.0 (c (n "ruspiro-lock") (v "0.5.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)))) (h "06a8j9i2xydz10sqhm7xx12wzmyrchmx137m1cpq59vj7p5axhiw") (f (quote (("async_locks"))))))

