(define-module (crates-io ru nt runtime-macros) #:use-module (crates-io))

(define-public crate-runtime-macros-0.1.0 (c (n "runtime-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("parsing" "full" "visit" "extra-traits"))) (k 0)))) (h "0vz4whjypa1j71icqz6n3rf4xvisijchxhpqkjxk9fj29ybh6gl2")))

(define-public crate-runtime-macros-0.2.0 (c (n "runtime-macros") (v "0.2.0") (d (list (d (n "cargo-tarpaulin") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing" "full" "visit" "extra-traits"))) (k 0)))) (h "0m0i2qpcmag6f8b410zyr69prhfipd1c88f6lrylqwsj8xrzfa30")))

(define-public crate-runtime-macros-0.3.0 (c (n "runtime-macros") (v "0.3.0") (d (list (d (n "cargo-tarpaulin") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing" "full" "visit" "extra-traits"))) (k 0)))) (h "0bli6sdhrxfpcxr3nr534f5xg0dg4fs5n3x4hl811fnfbviv1h1c")))

(define-public crate-runtime-macros-0.4.0 (c (n "runtime-macros") (v "0.4.0") (d (list (d (n "cargo-tarpaulin") (r "^0.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "visit" "extra-traits"))) (k 0)))) (h "04yx4ki56ks0x81i6jmhc4sjc58vpzplifzkkj9cgcwywf73pzcl")))

(define-public crate-runtime-macros-1.0.0 (c (n "runtime-macros") (v "1.0.0") (d (list (d (n "cargo-tarpaulin") (r "^0.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "visit" "extra-traits"))) (k 0)))) (h "0avw8rzpjb05g4615wg7pk48dyqnwi4r01vxdaizjz0qdyz0k73m")))

(define-public crate-runtime-macros-1.0.1 (c (n "runtime-macros") (v "1.0.1") (d (list (d (n "cargo-tarpaulin") (r "^0.21") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "visit" "extra-traits"))) (k 0)))) (h "0ij2ph6n1gkad170amcy0lrgkgqgr9qw1qx1s05m1nw7j762nzpk")))

(define-public crate-runtime-macros-1.1.0 (c (n "runtime-macros") (v "1.1.0") (d (list (d (n "cargo-tarpaulin") (r "^0.30.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("parsing" "visit" "extra-traits"))) (k 0)))) (h "1j18scc7y3an73wchbyqwhsw1wv91irxfg8mgn17a3n6n1d14m76")))

