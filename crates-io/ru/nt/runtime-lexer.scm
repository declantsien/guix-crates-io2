(define-module (crates-io ru nt runtime-lexer) #:use-module (crates-io))

(define-public crate-runtime-lexer-0.1.0 (c (n "runtime-lexer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0i2h90a56j3i1sm4w7fircwsa2bfbrha4k4ly4294b9k1nr0mnh8")))

