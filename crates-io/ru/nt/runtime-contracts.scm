(define-module (crates-io ru nt runtime-contracts) #:use-module (crates-io))

(define-public crate-runtime-contracts-0.1.0 (c (n "runtime-contracts") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "05r99l66ll9gbs5wfvvkns4nfp9pd26k70k9n930fzfnxz8ac42x")))

(define-public crate-runtime-contracts-0.1.1 (c (n "runtime-contracts") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0piix3bdq1ijhh2p80q54f4c48na7k2wv6psc31cvzpa81nxh0yj")))

(define-public crate-runtime-contracts-0.1.2 (c (n "runtime-contracts") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0krzwnidra13vxv9nwa7kzha3ls6mknzyjmnlvq9rzrf861k4mp1")))

(define-public crate-runtime-contracts-0.1.3 (c (n "runtime-contracts") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0gfvyzrl4mzysih3b5yr7i3s9qv39ih9n715qqcmb8p72jcm6yag")))

(define-public crate-runtime-contracts-0.2.0 (c (n "runtime-contracts") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1jdnyj8pn425alnczn90ykr47pdaw41n17ck4r5bh0gwsj6bv68s")))

