(define-module (crates-io ru nt runtime_injector_actix) #:use-module (crates-io))

(define-public crate-runtime_injector_actix-0.1.0 (c (n "runtime_injector_actix") (v "0.1.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "runtime_injector") (r "^0.3") (f (quote ("arc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rlmd03sq84pj8m2p0rby81aasxd7i86v1lxh72pz8zarrj81xiz") (f (quote (("default") ("arc"))))))

(define-public crate-runtime_injector_actix-0.1.1 (c (n "runtime_injector_actix") (v "0.1.1") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "runtime_injector") (r "^0.3") (f (quote ("arc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0yhqcmjjcdy7v7f5al2p7khqw6ba27n11nsd3h2pb0pd7c2wrf86") (f (quote (("default") ("arc"))))))

(define-public crate-runtime_injector_actix-0.2.0 (c (n "runtime_injector_actix") (v "0.2.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "runtime_injector") (r "^0.4") (f (quote ("arc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "155y3753vg9xxzipfx3jzha5cmvvp18hgm3bgk6wi01f197c794m") (f (quote (("default") ("arc"))))))

