(define-module (crates-io ru nt runtime-attributes) #:use-module (crates-io))

(define-public crate-runtime-attributes-0.0.0 (c (n "runtime-attributes") (v "0.0.0") (h "1kgccdhsrfbcaybx2jxh6yjcybcxixpvsxf0fb6kbi28xb4lc9wj")))

(define-public crate-runtime-attributes-0.3.0-alpha.1 (c (n "runtime-attributes") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "runtime-raw") (r "^0.3.0-alpha.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0hlabpk8qs26wz9bh3f0jh7gg759ihvjiiqkrl4mc56gldxxnnjf")))

(define-public crate-runtime-attributes-0.3.0-alpha.2 (c (n "runtime-attributes") (v "0.3.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "runtime-raw") (r "^0.3.0-alpha.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0yznhv8239w74dx3c203iwdh7y9lxrirb188yb7b1n828hrasmqz")))

(define-public crate-runtime-attributes-0.3.0-alpha.3 (c (n "runtime-attributes") (v "0.3.0-alpha.3") (d (list (d (n "proc-macro2") (r "^0.4.29") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "runtime-raw") (r "^0.3.0-alpha.2") (d #t) (k 2)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "126444mnmy5wx1sxkrrj21fqw05fw3km5yn4fy5ab86kzvl9g0lp")))

(define-public crate-runtime-attributes-0.3.0-alpha.4 (c (n "runtime-attributes") (v "0.3.0-alpha.4") (d (list (d (n "proc-macro2") (r "^0.4.29") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "runtime-raw") (r "^0.3.0-alpha.3") (d #t) (k 2)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0arcvx9kjnljcy8pd7wbafx86xim86kv9z41994v057kn0p5d8kd")))

(define-public crate-runtime-attributes-0.3.0-alpha.5 (c (n "runtime-attributes") (v "0.3.0-alpha.5") (d (list (d (n "proc-macro2") (r "^0.4.29") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "runtime-raw") (r "^0.3.0-alpha.4") (d #t) (k 2)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0lfbbgd30x1mzspspbjpa1xjmg903qh8dhxbh9zds7i4skf8kgzc")))

(define-public crate-runtime-attributes-0.3.0-alpha.6 (c (n "runtime-attributes") (v "0.3.0-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "runtime-raw") (r "^0.3.0-alpha.5") (d #t) (k 2)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "08n9sqpiwbjm7scyqz0xp57nggzb15rb0sydidw50lx0j0k9xn2n") (f (quote (("native") ("default" "native"))))))

