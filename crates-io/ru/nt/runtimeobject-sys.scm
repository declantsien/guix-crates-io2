(define-module (crates-io ru nt runtimeobject-sys) #:use-module (crates-io))

(define-public crate-runtimeobject-sys-0.0.1 (c (n "runtimeobject-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1m0gyjzy1px7ka4h0j5yklfp2r6gijah0s0zgq997pcgzyv43iz8")))

(define-public crate-runtimeobject-sys-0.2.0 (c (n "runtimeobject-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0wxjapc0rzvf8lqsac8xza89b1srcxv0mf19f026lsylsrr288n2")))

