(define-module (crates-io ru nt runtime_injector) #:use-module (crates-io))

(define-public crate-runtime_injector-0.1.0 (c (n "runtime_injector") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)))) (h "18ni3nb2674cah1hqfxhar16pmmn7db6k7axrs2k7ia9fgx99qav") (f (quote (("rc") ("default" "rc") ("arc"))))))

(define-public crate-runtime_injector-0.1.1 (c (n "runtime_injector") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)))) (h "1qs0yamrkss0qgbdpf7hsgn7xxd8mz6wsi932hy9v35wfy6f71g8") (f (quote (("rc") ("default" "rc") ("arc"))))))

(define-public crate-runtime_injector-0.1.2 (c (n "runtime_injector") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)))) (h "0digwqwscb9xqmlxlg2cnnamksx2m5aqa808y2jngmyf8vvs5iq5") (f (quote (("rc") ("default" "rc") ("arc"))))))

(define-public crate-runtime_injector-0.2.0 (c (n "runtime_injector") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)))) (h "0wz1gi62ac9jdw1wrj1756pwlcrbbvj8d2m1khyayaf3x25cjv72") (f (quote (("rc") ("default" "rc") ("arc"))))))

(define-public crate-runtime_injector-0.2.1 (c (n "runtime_injector") (v "0.2.1") (h "1hssj61f9ba6pmkk8590234v8659ffkpji11g972g4bxm6jhprz8") (f (quote (("rc") ("default" "rc") ("arc"))))))

(define-public crate-runtime_injector-0.3.0 (c (n "runtime_injector") (v "0.3.0") (h "0ffzb7lg2fnazca0hilhwmg1mi7bs9ib6i29qzsxrfm7nz5n292h") (f (quote (("rc") ("default" "arc") ("arc"))))))

(define-public crate-runtime_injector-0.4.0 (c (n "runtime_injector") (v "0.4.0") (h "1sdfdwznhx54q1bhdfx20i7xfjcpl25kvf5vn7wma19z9ffcxl4d") (f (quote (("rc") ("default" "arc") ("arc"))))))

