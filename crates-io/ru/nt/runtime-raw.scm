(define-module (crates-io ru nt runtime-raw) #:use-module (crates-io))

(define-public crate-runtime-raw-0.3.0-alpha.1 (c (n "runtime-raw") (v "0.3.0-alpha.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)))) (h "0dwl4lb0hcpllbj0ylwc6h0haljc4dwvrk1hy121vr33wm43n96c")))

(define-public crate-runtime-raw-0.3.0-alpha.2 (c (n "runtime-raw") (v "0.3.0-alpha.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)))) (h "1vha52aipzn8va85z7l417lai0gr26pwsl37hq6b0q4h5gy0p1p6")))

(define-public crate-runtime-raw-0.3.0-alpha.3 (c (n "runtime-raw") (v "0.3.0-alpha.3") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "13bizbdx6a5aa7pfwgpv9lxj3ynykhqcm5vzq23462vslgj04iqk")))

(define-public crate-runtime-raw-0.3.0-alpha.4 (c (n "runtime-raw") (v "0.3.0-alpha.4") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "03zsj6d8r4wipwvvs0zqgwv2639nvlafl00hcwfnr9n63d6kalbm")))

(define-public crate-runtime-raw-0.3.0-alpha.5 (c (n "runtime-raw") (v "0.3.0-alpha.5") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)))) (h "0k02nd0cg27s5ixflzsxqhil5rfzw4z3v0yvqbxzlhps90rq19y1")))

