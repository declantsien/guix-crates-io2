(define-module (crates-io ru nt runtime-otel-metrics) #:use-module (crates-io))

(define-public crate-runtime-otel-metrics-0.1.0 (c (n "runtime-otel-metrics") (v "0.1.0") (d (list (d (n "memory-stats") (r "^1") (o #t) (k 0)) (d (n "opentelemetry") (r ">=0.21, <0.23") (f (quote ("metrics"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (k 0)))) (h "14drl3j6v36ad5g84vz42dhcpkp2ix4hrsfzkjjr83cs72z1kakq") (f (quote (("default" "tokio" "memory-stats"))))))

