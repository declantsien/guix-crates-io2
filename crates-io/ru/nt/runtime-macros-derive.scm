(define-module (crates-io ru nt runtime-macros-derive) #:use-module (crates-io))

(define-public crate-runtime-macros-derive-0.4.0 (c (n "runtime-macros-derive") (v "0.4.0") (d (list (d (n "cargo-tarpaulin") (r "^0.10.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("derive" "parsing" "full" "visit" "extra-traits"))) (k 0)))) (h "0q7kn164xf6rgw1bx7x5bdgv04c1ca74pirmaps2vmdsrvif6hbw")))

(define-public crate-runtime-macros-derive-0.5.0 (c (n "runtime-macros-derive") (v "0.5.0") (d (list (d (n "cargo-tarpaulin") (r "^0.18.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("derive" "parsing" "full" "visit" "extra-traits"))) (k 0)))) (h "0inzzx01zv5x2a058m1yz5nsc8mb6xm399d4v3bx8hcc1sml64va")))

(define-public crate-runtime-macros-derive-0.6.0 (c (n "runtime-macros-derive") (v "0.6.0") (d (list (d (n "cargo-tarpaulin") (r "^0.21.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("derive" "parsing" "full" "visit" "extra-traits" "printing"))) (k 0)))) (h "18m8086l2gg9bwlyzszm8k83mw40f9y0qjqf52iz1qip6fg6vkqp")))

