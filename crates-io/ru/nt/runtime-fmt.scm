(define-module (crates-io ru nt runtime-fmt) #:use-module (crates-io))

(define-public crate-runtime-fmt-0.1.0 (c (n "runtime-fmt") (v "0.1.0") (h "1lpas3093yvva5dvflf20vi2f7jx7sw75iyqv4k1zfq6rh69mfdd")))

(define-public crate-runtime-fmt-0.2.0 (c (n "runtime-fmt") (v "0.2.0") (d (list (d (n "runtime-fmt-derive") (r "^0.2.0") (d #t) (k 2)))) (h "184fk8l4yr3mr946y7dbyazqxnhx2pb190db3cvn2l88k3x42aif")))

(define-public crate-runtime-fmt-0.3.0 (c (n "runtime-fmt") (v "0.3.0") (d (list (d (n "runtime-fmt-derive") (r "= 0.2.0") (d #t) (k 2)))) (h "0zxqamxv3zczlz6rax09hjj826qqs5ww7hrzk76ar7q4cqfq4yk4")))

(define-public crate-runtime-fmt-0.4.0 (c (n "runtime-fmt") (v "0.4.0") (d (list (d (n "runtime-fmt-derive") (r "= 0.2.0") (d #t) (k 2)))) (h "0jck1d6zskindjjbm4bg3wqcwbqbbidf9phnj6z65a3cjb564q0r")))

(define-public crate-runtime-fmt-0.4.1 (c (n "runtime-fmt") (v "0.4.1") (d (list (d (n "runtime-fmt-derive") (r "= 0.2.0") (d #t) (k 2)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1s1cr8k7nhbargq2qa0qvi027p764gc29b160xcix5jhhkvjad3h")))

