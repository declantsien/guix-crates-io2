(define-module (crates-io ru nt runtime-format) #:use-module (crates-io))

(define-public crate-runtime-format-0.1.0 (c (n "runtime-format") (v "0.1.0") (d (list (d (n "tinyvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0f48a6wcc1jx8x6h07pdwpbn6qp4fqwky0z48ag49rg4p745f20n") (f (quote (("std" "tinyvec/alloc") ("default" "std"))))))

(define-public crate-runtime-format-0.1.1 (c (n "runtime-format") (v "0.1.1") (d (list (d (n "tinyvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1p3v8qjk3p7jdmgk3wllz4cn02wzlp6qj5x8hffacmnqkncq7zgh") (f (quote (("std" "tinyvec/alloc") ("default" "std"))))))

(define-public crate-runtime-format-0.1.2 (c (n "runtime-format") (v "0.1.2") (d (list (d (n "tinyvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1594ayzc5r2xr0sy4wxvjfw0hgs4cs1r5z64mj1ldbxi26230ddh") (f (quote (("std" "tinyvec/alloc") ("default" "std"))))))

(define-public crate-runtime-format-0.1.3 (c (n "runtime-format") (v "0.1.3") (d (list (d (n "tinyvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "154c7jq7kbpc5acn2ysa2ilab2x0i5y7d34jwznni9xw71dqv589") (f (quote (("std" "tinyvec/alloc") ("default" "std"))))))

