(define-module (crates-io ru nt runtime_cfg) #:use-module (crates-io))

(define-public crate-runtime_cfg-0.1.0 (c (n "runtime_cfg") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (o #t) (d #t) (k 0)))) (h "0cbhsx77srz7q3wv56hs6vxyckv7105mfkwk4ha701gqyq4hzrnn") (f (quote (("std") ("printing") ("parsing" "std" "syn" "proc-macro2") ("default" "all") ("all" "std" "parsing" "printing"))))))

