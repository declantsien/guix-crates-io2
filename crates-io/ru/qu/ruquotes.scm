(define-module (crates-io ru qu ruquotes) #:use-module (crates-io))

(define-public crate-ruquotes-0.1.0 (c (n "ruquotes") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08r197bbvw90k4m8dpf83br77557zxippj8mxj7rpq6h1r3daj1g")))

(define-public crate-ruquotes-0.2.0 (c (n "ruquotes") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0h54mkcnyhcwny41azklvf5kg0vbqkbaln1rziq3g0fh8m3bzfsx")))

