(define-module (crates-io ru ps rupsc) #:use-module (crates-io))

(define-public crate-rupsc-0.1.0 (c (n "rupsc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nut-client") (r "^0.1.0") (d #t) (k 0)))) (h "1j7pwajmxq0f4cia6cngp7pqgx67dkvdqjdkh1v72n0n47absphc")))

(define-public crate-rupsc-0.2.0 (c (n "rupsc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nut-client") (r "^0.2.0") (f (quote ("ssl"))) (d #t) (k 0)))) (h "15cv2k1ifcm0vihf7r1hxhrygznzkqb2daxj7gkparbbfyq01kd4")))

(define-public crate-rupsc-0.2.1 (c (n "rupsc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nut-client") (r "^0.2.1") (f (quote ("ssl"))) (d #t) (k 0)))) (h "108fj5v5725mh1mx0s89n8l6gd20bz5j84d5cq2s9qq0g1410544")))

(define-public crate-rupsc-0.2.2 (c (n "rupsc") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nut-client") (r "^0.2.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0am43c00ay3lhg13c6zrb70xcmbh67nf64vp8gr913r54112rc2a")))

(define-public crate-rupsc-0.3.0 (c (n "rupsc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nut-client") (r "^0.3.0") (f (quote ("ssl"))) (d #t) (k 0)))) (h "02iq0hqjh62pl8c45b427yw4bmf9jz32xgwx7f5mh7n19lhriyfg")))

(define-public crate-rupsc-0.3.1 (c (n "rupsc") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nut-client") (r "^0.3.1") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1svqv8ax2a3iivm3h1xarzd1qwvh140g4b077h2rzm3hjmmdlmr9")))

(define-public crate-rupsc-0.4.0 (c (n "rupsc") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nut-client") (r "^0.4.0") (f (quote ("ssl"))) (d #t) (k 0)))) (h "108b65yj1k0v4ygyqqgcnhavw6lfizz83l6sw03sf1nqcwjcchag")))

(define-public crate-rupsc-0.5.0 (c (n "rupsc") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rups") (r "^0.5.0") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0qzk7473rwa2b351wg8zfr56g2alp8868spaibldb9b1in77s0cl")))

(define-public crate-rupsc-0.5.1 (c (n "rupsc") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rups") (r "^0.5.1") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1rq09x78w781yhpy5zgwqqx837k0m1bixhqhygnf0sy6jpk6hxsx")))

(define-public crate-rupsc-0.5.2 (c (n "rupsc") (v "0.5.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rups") (r "^0.5.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1rri92ylr08a0b7zns2qj2w5n28m8n9lq3jj6fnh1j9fkmcrikip")))

(define-public crate-rupsc-0.5.3 (c (n "rupsc") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rups") (r "^0.5.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0aq7ij82lq68xlzqhqiyiqy45qj33njrqc2p02yim31f9w5d2crw")))

(define-public crate-rupsc-0.6.0 (c (n "rupsc") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rups") (r "^0.6.0") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0z2jvlbri4p3bmchqz8x1crpaw3ama5klnj0ljg8rz7v7bx2fsva")))

(define-public crate-rupsc-0.6.1 (c (n "rupsc") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rups") (r "^0.6.1") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0s2v81z4zs86c2wslm3ylzqnh3ah5ljqc83961ssasfwlrmzz73r")))

