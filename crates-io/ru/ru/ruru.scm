(define-module (crates-io ru ru ruru) #:use-module (crates-io))

(define-public crate-ruru-0.5.0 (c (n "ruru") (v "0.5.0") (d (list (d (n "libc") (r "^0.1.12") (d #t) (k 0)))) (h "0bhb2lpiqyqbdn255mh0085dir93m3id57svi57lk6xfl4dviia2")))

(define-public crate-ruru-0.5.1 (c (n "ruru") (v "0.5.1") (d (list (d (n "libc") (r "^0.1.12") (d #t) (k 0)))) (h "0hvlxipbidmwsi2mx4d3cz2qjqmcnmy9d6f7vhh0jdfm80jgg6x4")))

(define-public crate-ruru-0.5.2 (c (n "ruru") (v "0.5.2") (d (list (d (n "libc") (r "^0.1.12") (d #t) (k 0)))) (h "1rymkw5rwyi5yh75knd6ijdpsdfxgny8lx3qjbgr2wv9ymn5z04y")))

(define-public crate-ruru-0.5.3 (c (n "ruru") (v "0.5.3") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "1hnilzcc3vwggh92m5irmz2icd0xxk0ddbrys077klblvnz5l3zc")))

(define-public crate-ruru-0.5.4 (c (n "ruru") (v "0.5.4") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "1hipmkxwypzh4bh089j60wwb1601gx0qy29dhi2y70bff2w39q14")))

(define-public crate-ruru-0.5.5 (c (n "ruru") (v "0.5.5") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0msiqwp52a93cr0m93d1fncv9ihfzhadavbl0r6cg88zypaqg1d5")))

(define-public crate-ruru-0.6.0 (c (n "ruru") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.1.1") (d #t) (k 0)))) (h "182abmvs7v5rv7xgs1mvjigsp52y0x3vyja2z3p64bn5sa5xv4nm")))

(define-public crate-ruru-0.7.0 (c (n "ruru") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0n9qa6rna9192h1rxlvprcykacgj2c6iv478mjpww1jz1hzz20hr")))

(define-public crate-ruru-0.7.1 (c (n "ruru") (v "0.7.1") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.1.3") (d #t) (k 0)))) (h "080270frm0bx97xbidk1z4m892ymck20ii1xmb0jbh6gqnnmymh5")))

(define-public crate-ruru-0.7.2 (c (n "ruru") (v "0.7.2") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0li47bj3dl5f19k70g46d1kvxiac86cya7yf48r151pf3r18fba3")))

(define-public crate-ruru-0.7.3 (c (n "ruru") (v "0.7.3") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.1.5") (d #t) (k 0)))) (h "12j1m5r0d756waisi7hbrbas75nr68kcjr1k5837d9xf46ap3jdc")))

(define-public crate-ruru-0.7.4 (c (n "ruru") (v "0.7.4") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.2.3") (d #t) (k 0)))) (h "17q5h7k7cd0nq8fj3jxlm19qyrdr2wi41qv2waibkra46jnld2sq")))

(define-public crate-ruru-0.7.5 (c (n "ruru") (v "0.7.5") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.2.4") (d #t) (k 0)))) (h "1q6ccpzdsmcgy0x1dsv7l5jnp4kb5dznlbxxxd68gkjccwr15l0w")))

(define-public crate-ruru-0.7.6 (c (n "ruru") (v "0.7.6") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.2.5") (d #t) (k 0)))) (h "0a4h61zb0z0xs9a5in0b25901c71mzwd9xf4dnk6zbav5qznylz5")))

(define-public crate-ruru-0.7.7 (c (n "ruru") (v "0.7.7") (d (list (d (n "ruby-sys") (r "^0.2.6") (d #t) (k 0)))) (h "12y3d96ha17plb9640yl09mwk0zss7c1wj37yix92n8msi6mgkz5")))

(define-public crate-ruru-0.7.8 (c (n "ruru") (v "0.7.8") (d (list (d (n "ruby-sys") (r "^0.2.6") (d #t) (k 0)))) (h "0zc3yn4pdmc9s5q2bmlqf8wlffb7aq9w813axkwhyh8449j83gmv")))

(define-public crate-ruru-0.8.0 (c (n "ruru") (v "0.8.0") (d (list (d (n "ruby-sys") (r "^0.2.12") (d #t) (k 0)))) (h "12acc2wh0b4fyy8nmg84swhjfgp46kjwffpmq3rvc1w6inbdfdqr")))

(define-public crate-ruru-0.8.1 (c (n "ruru") (v "0.8.1") (d (list (d (n "ruby-sys") (r "^0.2.13") (d #t) (k 0)))) (h "0razsf856r1q00cmdjz38jqv2lvwhk68c66br8x4zwx160r989kk")))

(define-public crate-ruru-0.9.0 (c (n "ruru") (v "0.9.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.2.15") (d #t) (k 0)))) (h "09wl6bzvf8ipggh84gnf6azvnz931m9lpj4ix377n3ncqd48dj7v")))

(define-public crate-ruru-0.9.1 (c (n "ruru") (v "0.9.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.2.17") (d #t) (k 0)))) (h "0n3b2cmr2k2zqzv60mbaw9s3lhm31l5spxrzmv4jy1xcj8vm2zn7")))

(define-public crate-ruru-0.9.2 (c (n "ruru") (v "0.9.2") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.2.19") (d #t) (k 0)))) (h "1q7riimllqa4qgv3lka3swyq8rsi9knnxhb1bpfic0bg11bclm9b")))

(define-public crate-ruru-0.9.3 (c (n "ruru") (v "0.9.3") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "ruby-sys") (r "^0.2.20") (d #t) (k 0)))) (h "0wq2iqlzc0xn7r21s11qq50pv5y16lv8jq9a95m2823s1g2xd1k4")))

