(define-module (crates-io ru ex ruex) #:use-module (crates-io))

(define-public crate-ruex-0.1.0 (c (n "ruex") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0nkp4lg6j90j52dz0czbll1y42j1pd9pvznscbs466jap2qxai1q")))

(define-public crate-ruex-0.1.1 (c (n "ruex") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pwa1bshqfxvm60d0zg7mir360f4bwbm7if8slqs2sl03v247bbf")))

(define-public crate-ruex-0.1.3 (c (n "ruex") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12b3dlhyc2p1si0w4wmc288d4l7cjic1hajzpasark3lm0a29xhr")))

(define-public crate-ruex-0.1.5 (c (n "ruex") (v "0.1.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.5") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored" "syslog-6" "meta-logging-in-format" "chrono"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "ruex-macro") (r "^0") (d #t) (k 0)) (d (n "rust-companion") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syslog") (r "^6") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)) (d (n "rust-companion") (r "^0") (d #t) (k 1)))) (h "0l80q0a0pq0zzyam0g0rsaaw6khhixfl2jk1xsfwqrayccmya5az")))

