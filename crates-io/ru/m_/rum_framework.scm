(define-module (crates-io ru m_ rum_framework) #:use-module (crates-io))

(define-public crate-rum_framework-0.0.1 (c (n "rum_framework") (v "0.0.1") (d (list (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "145fz17a28j62q4viz35bir6g4iyj0hfmjq7h6vm9mjfhbvp8z4g")))

