(define-module (crates-io ru ss russo) #:use-module (crates-io))

(define-public crate-russo-0.1.0 (c (n "russo") (v "0.1.0") (h "0ahhk52ig527a98v9j16n370x1w1cfm76b4mxas7k14mf01f5hx2") (y #t)))

(define-public crate-russo-0.0.1 (c (n "russo") (v "0.0.1") (h "18wc0vsbpx0bk5q5595wsk7cj3f26s73vc21j90dbq81h7hii080") (y #t)))

