(define-module (crates-io ru ss russell_stat) #:use-module (crates-io))

(define-public crate-russell_stat-0.1.0 (c (n "russell_stat") (v "0.1.0") (h "161h59srj15wm9091s3jy89jx82zjc2rh4zv56naj5lx6ifd5r3g")))

(define-public crate-russell_stat-0.1.1 (c (n "russell_stat") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)))) (h "11i7f7f1dvsaazy49gy78pi4anl96n73sqxj7cy9kyzhc0w0mss8")))

(define-public crate-russell_stat-0.1.2 (c (n "russell_stat") (v "0.1.2") (d (list (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "0hpnw0r5ci9f02mf82qrxdcy9i566i4sf7pdm7jqjikmlw7bzl8k")))

(define-public crate-russell_stat-0.1.3 (c (n "russell_stat") (v "0.1.3") (d (list (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "0g6ngi54vw9g7ciakjj1mys8ila94w4pvyfkcq7b9iizdny0pjkb")))

(define-public crate-russell_stat-0.1.4 (c (n "russell_stat") (v "0.1.4") (d (list (d (n "russell_chk") (r "^0.2") (d #t) (k 0)))) (h "15hlrzjgk78193n7sj5ps8b1vbr9mgydnp0sa7gjgpzj7vc759ik")))

(define-public crate-russell_stat-0.2.0 (c (n "russell_stat") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.2") (d #t) (k 0)))) (h "1ag70c89aa5hb636ngs0la373fah7pgh2dyqxzm7j2hax2dxr5zr")))

(define-public crate-russell_stat-0.2.1 (c (n "russell_stat") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.3") (d #t) (k 0)))) (h "00spi58abx3m85sfz960rvqff6rvc5bcqlshl923hb8y5qk3x1ff")))

(define-public crate-russell_stat-0.4.1 (c (n "russell_stat") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.4.1") (d #t) (k 0)))) (h "1mp10133w8k1c4xnmnmwv02is164kk659ikll5grchl4g2ri9ba7")))

(define-public crate-russell_stat-0.5.0 (c (n "russell_stat") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.5.0") (d #t) (k 0)) (d (n "russell_lab") (r "^0.5.0") (d #t) (k 0)))) (h "0xh84vyy7kny2rybnj3q8xg0v8ysihyvcly76jw2iwbrdzgv1151")))

(define-public crate-russell_stat-0.6.1 (c (n "russell_stat") (v "0.6.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^0.6.0") (d #t) (k 0)))) (h "1v9ms9zgjv7h1wf9j3z1sk4cd079psvrwxx3dwbwlv31g0ii0rgy")))

(define-public crate-russell_stat-0.7.1 (c (n "russell_stat") (v "0.7.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^0.7.1") (d #t) (k 0)))) (h "0c9g6wqhy5yqm73y9m2rs33zdkzagclw4l2h1y8nlc97p1zh13mx")))

(define-public crate-russell_stat-0.8.0 (c (n "russell_stat") (v "0.8.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^0.8.0") (d #t) (k 0)))) (h "0i85bxbv3xj84vzh1v1m6fl6v2i0pbiswcks5kwrlrazrkdzp77a")))

(define-public crate-russell_stat-1.0.0 (c (n "russell_stat") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "1qk9z53ji8p91vkcbfbfvrjsa8qryx7bra9rjshyyvsa0v6j77yq") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.1.1 (c (n "russell_stat") (v "1.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "18dqpjxcs9gyn4g152rj7k85grq0bc48pv76vrrmp0razvdlpy7n") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.1.2 (c (n "russell_stat") (v "1.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "1d762461bs9z5s2271v1pa6rrbj1d7njb70wssl2jpk61s9nshkw") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.2.0 (c (n "russell_stat") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "0hpwxc5ab5jsrld7frk8s4xhfjzlwxxhg9wz9w0giqdacavpl1vd") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.2.1 (c (n "russell_stat") (v "1.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotpy") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "0kdiyxghg0d36adaas98mmkys21fmw7sa60igxm9w95l364adk7g") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.3.0 (c (n "russell_stat") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotpy") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "08msc8049xi90bhj5jsw3d5h0mvrjccpf18f1mrfcx9fv36ndvz5") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.4.0 (c (n "russell_stat") (v "1.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotpy") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "0jb2z1w21fdasjdhpzmain64dva011fiia59j9xid4cgbyj43flf") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.4.1 (c (n "russell_stat") (v "1.4.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotpy") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "1vjfna51rbbwd2bdvyagj8b7yq0sdrzsvjn0g4ac6w787dwx59d3") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.5.0 (c (n "russell_stat") (v "1.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotpy") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "1qdjy5k19cj308xbzwyfraw4hk5rs3qkxxha6pymhm0gvfl8hk13") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.5.1 (c (n "russell_stat") (v "1.5.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotpy") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "0vp7iwwrrljmhlhia5csl4qqjdwk7jji8kshd2lxyjjlj37838r0") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_stat-1.6.0 (c (n "russell_stat") (v "1.6.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotpy") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)))) (h "0bggca7kvz9x3hz30jaalf7qydwlfg8nj7ijja6hr25s2mkb7w50") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

