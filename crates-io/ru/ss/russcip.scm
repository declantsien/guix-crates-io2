(define-module (crates-io ru ss russcip) #:use-module (crates-io))

(define-public crate-russcip-0.1.0 (c (n "russcip") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1rk0knh1lvdmg1s6b0gyakd83091ps6l8mxnpnkw7r0np63iq6pz")))

(define-public crate-russcip-0.1.1 (c (n "russcip") (v "0.1.1") (d (list (d (n "scip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "16i5d1y8r9jfkliv8zrpxzly5kmwcsmszr9sqd5ccvvvhqjgvls3")))

(define-public crate-russcip-0.1.2 (c (n "russcip") (v "0.1.2") (d (list (d (n "scip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0krkrrxbdbpy57bg4apinp8gfkbc1dfcvf7y5cvchhnwml2sflgk")))

(define-public crate-russcip-0.1.3 (c (n "russcip") (v "0.1.3") (d (list (d (n "scip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "19ackd9ppcypsvwq2bgjslwlffdyab0ydz6fmrfgj1k41jh7xcr5")))

(define-public crate-russcip-0.1.4 (c (n "russcip") (v "0.1.4") (d (list (d (n "scip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1dfd8nssjxj5n89qd0h0xy9bnn7g8mc7q5y0fssdbs6b6c28fidf")))

(define-public crate-russcip-0.1.5 (c (n "russcip") (v "0.1.5") (d (list (d (n "scip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0s7n6r2w6hzsqyj8yxzcjq2d9g43vs7xdjwm7zfax1il3pf53hvx")))

(define-public crate-russcip-0.1.6 (c (n "russcip") (v "0.1.6") (d (list (d (n "scip-sys") (r "^0.1.0") (d #t) (k 0)))) (h "08dxapfvcgnz56dr0kxkj5dhbwrjxr06icfa9wqpm5akkcm6mv06")))

(define-public crate-russcip-0.1.7 (c (n "russcip") (v "0.1.7") (d (list (d (n "scip-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0942d4a744si3nckxnsam4qcg9h5hiyd45vb2pbfvs4hl365nl5d")))

(define-public crate-russcip-0.1.8 (c (n "russcip") (v "0.1.8") (d (list (d (n "scip-sys") (r "^0.1.1") (d #t) (k 0)))) (h "03j2y9rzb0ida24vx93piar408pw6sw4a1q38zw2xfji83iw3b0x")))

(define-public crate-russcip-0.1.9 (c (n "russcip") (v "0.1.9") (d (list (d (n "scip-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1ab6zf1rqpdfhsjkycdybv0zfvkcfr2v2i2hwzxkzfld9302maxx")))

(define-public crate-russcip-0.1.11 (c (n "russcip") (v "0.1.11") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.2") (d #t) (k 0)))) (h "12a4lswcg8zxhj5kiqk78w4yk4bxgq1djf0ji52r49j2ms2ayfy9")))

(define-public crate-russcip-0.1.12 (c (n "russcip") (v "0.1.12") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0agby4fkamys9jv05my04m0fgxbpyarsala169fp4zjb6bm8zykj")))

(define-public crate-russcip-0.1.13 (c (n "russcip") (v "0.1.13") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1fp0knail03mmca9j4wc0ac0b2isxlgq8jx689v9zaql61da8bmy")))

(define-public crate-russcip-0.1.14 (c (n "russcip") (v "0.1.14") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0sbiygc8vgmaf05qd1g7lg2a9h7azz12yb5c61jbwg88rina8458") (f (quote (("raw"))))))

(define-public crate-russcip-0.2.0 (c (n "russcip") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1f4nvscbcry8mz6mw9c8vs0q94lfp044xziirsp1ysfqg1xjglrl") (f (quote (("raw"))))))

(define-public crate-russcip-0.2.1 (c (n "russcip") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1qqdm1cyrjcciy07pq793j9qlkaxqhx0dqk84xp88cx1kqnryd2g") (f (quote (("raw"))))))

(define-public crate-russcip-0.2.2 (c (n "russcip") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "12rg9j5kjvcrh6ra1m2s6zrwvr8h2zkhnlrab2lqza4xylq5gidd") (f (quote (("raw"))))))

(define-public crate-russcip-0.2.3 (c (n "russcip") (v "0.2.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0ihishjwfa5m0lp9rca5097r0ksbzk12jjsncbrsaypd5xqwpx9c") (f (quote (("raw"))))))

(define-public crate-russcip-0.2.4 (c (n "russcip") (v "0.2.4") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1xf0519x1i3vvp8bbz2a6gcc18dn1c21a8dlz3rcskngn97wiqq0") (f (quote (("raw"))))))

(define-public crate-russcip-0.2.5 (c (n "russcip") (v "0.2.5") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "126slq95sa1fvm1rm7lya00ph1c3pz2dga1pm7mkpcy09pg72nnj") (f (quote (("raw"))))))

(define-public crate-russcip-0.2.6 (c (n "russcip") (v "0.2.6") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "scip-sys") (r "^0.1.4") (d #t) (k 0)))) (h "170c22wcla9zvh9gsbpn0i0g95ylahn63jwqxxlb6ixvihfzca9h") (f (quote (("raw"))))))

(define-public crate-russcip-0.3.0 (c (n "russcip") (v "0.3.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "scip-sys") (r "^0.1.8") (d #t) (k 0)))) (h "0k80gh2pn73w0bj5ar50jnjawbbmab54hf1n2175mzbqzlnpx1nc") (f (quote (("raw") ("bundled" "scip-sys/bundled"))))))

(define-public crate-russcip-0.3.1 (c (n "russcip") (v "0.3.1") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "scip-sys") (r "^0.1.9") (d #t) (k 0)))) (h "0hs0w7k0fp5bhrx8035gzv6wikqp03a8ma12i4d5x9cn08qyr7cw") (f (quote (("raw") ("bundled" "scip-sys/bundled"))))))

(define-public crate-russcip-0.3.2 (c (n "russcip") (v "0.3.2") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "scip-sys") (r "^0.1.9") (d #t) (k 0)))) (h "09979yyc1gz4cgd0wy56f2hdihdda067yzzr9sq93gwfgppkz0pi") (f (quote (("raw") ("bundled" "scip-sys/bundled"))))))

(define-public crate-russcip-0.3.3 (c (n "russcip") (v "0.3.3") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "scip-sys") (r "^0.1.11") (d #t) (k 0)))) (h "0drmr1v9q22fgx2hmkari039lv153d81jb0yb2idzbi9lbg1xxd7") (f (quote (("raw") ("from-source" "scip-sys/from-source") ("bundled" "scip-sys/bundled"))))))

(define-public crate-russcip-0.3.4 (c (n "russcip") (v "0.3.4") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "scip-sys") (r "^0.1.12") (d #t) (k 0)))) (h "1zldsd7hya85i6zzq55mgxqa2j50vcamd45kbhqf89hmiklnfil6") (f (quote (("raw") ("from-source" "scip-sys/from-source") ("bundled" "scip-sys/bundled"))))))

