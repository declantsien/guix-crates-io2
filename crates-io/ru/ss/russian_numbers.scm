(define-module (crates-io ru ss russian_numbers) #:use-module (crates-io))

(define-public crate-russian_numbers-0.1.0 (c (n "russian_numbers") (v "0.1.0") (h "0f83wksgvbwq5c2c58pn4iic3jj7h6x7l29fqzrli0vb0gm54fz8")))

(define-public crate-russian_numbers-0.2.0 (c (n "russian_numbers") (v "0.2.0") (h "0vd0hplrfb5zxzm8mrq28zc7s652nhk0mb7gzcsibxgiwz189hyq")))

