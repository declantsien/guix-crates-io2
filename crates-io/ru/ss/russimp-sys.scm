(define-module (crates-io ru ss russimp-sys) #:use-module (crates-io))

(define-public crate-russimp-sys-0.1.0 (c (n "russimp-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0pqkcbk72wp3rj7hv6fma5bqb0gi0hn8kaakb9j3j3x65fq959n7")))

(define-public crate-russimp-sys-0.1.1 (c (n "russimp-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "12zlsaiccd475hrfc3mflfxd5lmcl7r4sc1md78b93hvgvv6gl4v")))

(define-public crate-russimp-sys-0.1.2 (c (n "russimp-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1diw6aaj7qsqr0mha7jpsl3yik90pxds7xr4gyrxkh3c9ygki9p3")))

(define-public crate-russimp-sys-0.1.3 (c (n "russimp-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0lyy4wm8p3rhkmglpz092hjga0cbrx84lbnvi0slr14ccwb688dc")))

(define-public crate-russimp-sys-0.1.4 (c (n "russimp-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "19higfsbm4lil3h5gyaqq65v8l2175hv8az3masp0amlbmlaqs4l")))

(define-public crate-russimp-sys-0.1.5 (c (n "russimp-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1cdgg4lis6q6sddaljfmfngk8vcp4shq53ils8h6jchiqrvrxdg9")))

(define-public crate-russimp-sys-0.1.6 (c (n "russimp-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "06fvzmsa1bdpa371895sc3xynl8w8m2jb9zwnx1q45mbmzwqbr6z")))

(define-public crate-russimp-sys-0.1.7 (c (n "russimp-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "1qkfaiwfqxh8q6vz5wqkxyhnwps5plnnqir77s3m8358lh4hkbn6")))

(define-public crate-russimp-sys-0.1.8 (c (n "russimp-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "1kizfp7a5vz7kn0civbvbz40pns8bxhka5r7cf5d06k5i6bn8r85")))

(define-public crate-russimp-sys-0.1.9 (c (n "russimp-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)))) (h "0bk9f8ziyjapnbhsr8kfry6wjiw3x2ayypaz1h1ayz21g5h1xk7d")))

(define-public crate-russimp-sys-0.2.0 (c (n "russimp-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0qhd2a4685a499rxmnyy3n8yh4y5bk9hhfgh9583cwjp9sa6ik73")))

(define-public crate-russimp-sys-0.2.1 (c (n "russimp-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1jaq6xdzh0wcrhx087bmaszdbl6mllh9syga6p66aj1w3rslxz1v")))

(define-public crate-russimp-sys-0.2.2 (c (n "russimp-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1xznvcszpgljhpiglhaq0wb79xyhbp12mcnkl3bsfxzq3vxdj351")))

(define-public crate-russimp-sys-0.2.3 (c (n "russimp-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "15mgjdx307b11fl313hqppgdf6q7l1lsgsgpaal8f2067wkpzm94")))

(define-public crate-russimp-sys-0.2.4 (c (n "russimp-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1fdyl9h7sy38vbdmbgzw0xf4h4617g66vri6gbri7pg438rmf1yg")))

(define-public crate-russimp-sys-0.2.5 (c (n "russimp-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1hdfsjd006z2dclp2rzzpxyd9vxyjpqzf09aky7z8jc0hhgwryan")))

(define-public crate-russimp-sys-0.2.6 (c (n "russimp-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1qf4xih0f4zjml73dar3zigq91wlah9zqinfw10hmhll0ab9ibnq")))

(define-public crate-russimp-sys-1.0.0 (c (n "russimp-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1zsfbm11z7slfavqrw2j22d3hvh5sdclw33mzizwfw7vnk2fan8n")))

(define-public crate-russimp-sys-1.0.1 (c (n "russimp-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.13") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "0irqxq8xsl30qf398vilm60ll4s7j2d19a0d57511hg8hw1wsbzk") (f (quote (("static-link") ("prebuilt" "static-link") ("nozlib") ("nolibcxx") ("default"))))))

(define-public crate-russimp-sys-1.0.3 (c (n "russimp-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.13") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.5") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "14fmamjbxg93xaljgvymmb829ii8yk9w5snc11q7xwfzdfgasv7z") (f (quote (("static-link") ("prebuilt" "static-link") ("nozlib") ("nolibcxx") ("default"))))))

(define-public crate-russimp-sys-2.0.0 (c (n "russimp-sys") (v "2.0.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "0wgi5qi2xd0fxcqcvqdwlg44znzrm0z7cfh1zxjwcvfnjf1fv7nc") (f (quote (("static-link" "build-assimp") ("prebuilt") ("default") ("build-assimp"))))))

(define-public crate-russimp-sys-2.0.1 (c (n "russimp-sys") (v "2.0.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "0mr55va1gjnmpmlkysswgg78a27iw4svqnq3bswnhww38mmxxl2p") (f (quote (("static-link" "build-assimp") ("prebuilt") ("nozlib") ("default") ("build-assimp"))))))

(define-public crate-russimp-sys-2.0.2 (c (n "russimp-sys") (v "2.0.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "built") (r "^0.5.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "0fxhsfjkpjq51ydpxp72qh7wzgvr42zjjlvsdxyviijiqs9py9fk") (f (quote (("static-link" "build-assimp") ("prebuilt") ("nozlib") ("default") ("build-assimp"))))))

