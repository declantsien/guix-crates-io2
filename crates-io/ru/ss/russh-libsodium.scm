(define-module (crates-io ru ss russh-libsodium) #:use-module (crates-io))

(define-public crate-russh-libsodium-0.3.0-beta.1 (c (n "russh-libsodium") (v "0.3.0-beta.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "11pi88ckxrb4gzilibaajj64z9z3cp083lwmqaxdnza4m1jlzgh1")))

