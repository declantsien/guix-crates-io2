(define-module (crates-io ru ss russell_openblas) #:use-module (crates-io))

(define-public crate-russell_openblas-0.1.0 (c (n "russell_openblas") (v "0.1.0") (d (list (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "1rgsny5cd2pn1b0kqflj2dz0bxvwi98f93qf3x1df60vb5gm8522")))

(define-public crate-russell_openblas-0.1.2 (c (n "russell_openblas") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "13cd44q59rz39w201g5q1699pljq0hqjd6pkjddsj6gk9b6z2ghr")))

(define-public crate-russell_openblas-0.1.3 (c (n "russell_openblas") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "116hxhkz9hhrcp7pwkgm3q38mb2c006fx84495im5vdk485w7apd")))

(define-public crate-russell_openblas-0.1.4 (c (n "russell_openblas") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "175901jg86gylwjskdbmjsizqpf213rkmzbykp2vcxrfcha0cbn6")))

(define-public crate-russell_openblas-0.2.0 (c (n "russell_openblas") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "0bsmdlshl542k765fc5vj5ljvnd9dba4vb4pnvpxiizl7jc92b7j")))

(define-public crate-russell_openblas-0.2.1 (c (n "russell_openblas") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "1jcs2ff67zv6hj07jph6l9y0bz6cc159dfgsby1nb0jgi7cya59q")))

(define-public crate-russell_openblas-0.2.2 (c (n "russell_openblas") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "1krbyjrxliryyzi9cmi4x8ghaml2mw8645spj1l4679v5bc9lq77")))

(define-public crate-russell_openblas-0.2.3 (c (n "russell_openblas") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "russell_chk") (r "^0.2") (d #t) (k 0)))) (h "1422lx6sxh6w0fb3cjp9i3igna1n5zbdzrvzhaml6n7ydayjqm45")))

(define-public crate-russell_openblas-0.3.0 (c (n "russell_openblas") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "russell_chk") (r "^0.3") (d #t) (k 0)))) (h "0wmr655zbbml7g3310rsngiba2n2cagvi9nssvyqv2hqc0f0p9gj")))

(define-public crate-russell_openblas-0.4.1 (c (n "russell_openblas") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "russell_chk") (r "^0.4.1") (d #t) (k 0)))) (h "1a6pdzg59b7fn2fxcpqvmmrvkjfqhfll8nsjpmjs9h3d3c3vqnkv")))

(define-public crate-russell_openblas-0.5.0 (c (n "russell_openblas") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "russell_chk") (r "^0.5.0") (d #t) (k 0)))) (h "1yri1ab39q9hyn7hsfqhigbhrxpayffdy16n73b6fd0y3sp624np")))

(define-public crate-russell_openblas-0.6.0 (c (n "russell_openblas") (v "0.6.0") (h "0x592v982g22ny66bm3d7gv6ayncvlvva1n6qii72m45ji8nisrf")))

