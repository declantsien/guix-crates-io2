(define-module (crates-io ru ss russh-config) #:use-module (crates-io))

(define-public crate-russh-config-0.7.0-beta.1 (c (n "russh-config") (v "0.7.0-beta.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util" "net" "macros" "process"))) (d #t) (k 0)) (d (n "whoami") (r "^1.0") (d #t) (k 0)))) (h "0nlvybjd9cj3hsriwsc69srfj25plhwwyqhm1v3hmddv5wqkysrm")))

(define-public crate-russh-config-0.7.0 (c (n "russh-config") (v "0.7.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util" "net" "macros" "process"))) (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)))) (h "08rmikp20q141dshyh593f57y2d3ybpslp9vkc74lyrc5v1ii11w")))

(define-public crate-russh-config-0.7.1 (c (n "russh-config") (v "0.7.1") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util" "net" "macros" "process"))) (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)))) (h "1c4jmv7vln26h2l3255jda2k9fb1q7lx87lvnagqmwfqkiv1b9lp") (r "1.65")))

