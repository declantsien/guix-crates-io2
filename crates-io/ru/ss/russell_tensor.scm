(define-module (crates-io ru ss russell_tensor) #:use-module (crates-io))

(define-public crate-russell_tensor-0.1.1 (c (n "russell_tensor") (v "0.1.1") (d (list (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "1j3mchcv6z9d17glknaji41a5g4r5r2qq1hxa79xwjz5wnz30q07")))

(define-public crate-russell_tensor-0.1.2 (c (n "russell_tensor") (v "0.1.2") (d (list (d (n "russell_chk") (r "^0.1") (d #t) (k 0)))) (h "1wjhs8ih4cbav63jn56sdkqxm1lyf4mgk1i230gci1920avl5si2")))

(define-public crate-russell_tensor-0.1.3 (c (n "russell_tensor") (v "0.1.3") (d (list (d (n "russell_chk") (r "^0.2") (d #t) (k 0)))) (h "1v77zsd4bwa411gk8hw2cy34jav5l8jdihxwisyzc469w2kfwggb")))

(define-public crate-russell_tensor-0.2.0 (c (n "russell_tensor") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^1.0.0") (d #t) (k 2)) (d (n "russell_chk") (r "^0.2") (d #t) (k 0)) (d (n "russell_lab") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zn6aqs6c7rmz3489m83nnng4lcp9dk4i0szpqyrgjpkz7vdijj1")))

(define-public crate-russell_tensor-0.2.1 (c (n "russell_tensor") (v "0.2.1") (d (list (d (n "rmp-serde") (r "^1.0.0") (d #t) (k 2)) (d (n "russell_chk") (r "^0.3") (d #t) (k 0)) (d (n "russell_lab") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "05wrb9wsr4dv8f7z3pny86ihd6ri5zmwrflfyl27wgmql657cslm")))

(define-public crate-russell_tensor-0.4.1 (c (n "russell_tensor") (v "0.4.1") (d (list (d (n "rmp-serde") (r "^1.0.0") (d #t) (k 2)) (d (n "russell_chk") (r "^0.4.1") (d #t) (k 0)) (d (n "russell_lab") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jsf4jxlwkqkbmnas9pbspx859w8dckjgbrmjj16sqvlbyp0ld4z")))

(define-public crate-russell_tensor-0.5.0 (c (n "russell_tensor") (v "0.5.0") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "russell_chk") (r "^0.5.0") (d #t) (k 0)) (d (n "russell_lab") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "197gwf65fdh9clh3rrry7b7xjaz0rzpxmzz9783fv61jgsn1w0pg")))

(define-public crate-russell_tensor-0.6.1 (c (n "russell_tensor") (v "0.6.1") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "russell_lab") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k9y36b88x9sz66ywsn948ckk359v7hfzlx963xl03rml3kascdn")))

(define-public crate-russell_tensor-0.7.1 (c (n "russell_tensor") (v "0.7.1") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "russell_lab") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a9mn8fyqwnknwhq2hf59240az6mbbwphv83mdd9isyn1i5plj3n")))

(define-public crate-russell_tensor-0.7.2 (c (n "russell_tensor") (v "0.7.2") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "russell_lab") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rlhd74ka8v2ipmzxldy15q19z34kfsigm84r7wsxxqxmcgw6xcv")))

(define-public crate-russell_tensor-0.8.0 (c (n "russell_tensor") (v "0.8.0") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "russell_lab") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j8mg7sj834b3w6r7b5ddjn5ck7an7f3xj2gfks90dr8y75igbdx")))

(define-public crate-russell_tensor-1.0.0 (c (n "russell_tensor") (v "1.0.0") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12f3hjpl0w0dwz13kgpr6p7x1vf9kchrva941lrhdf4fmzrbyzds") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.1.0 (c (n "russell_tensor") (v "1.1.0") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01kjgc2qhj1lddz8vqsrml7q610czxfk0x4516hja3a327mn63l9") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.1.1 (c (n "russell_tensor") (v "1.1.1") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gacl2wb4xc1s01scvk9r2kd82xavcr7shx700z6xdrk2qf4gs08") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.1.2 (c (n "russell_tensor") (v "1.1.2") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hn7ikfvhjsibj2lsl1lvgcil584jnpgnq7kg603jqxd1sl0302d") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.2.0 (c (n "russell_tensor") (v "1.2.0") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13vjznrng6qwcj8j8viynrc6pfkng3wwljiivk9r8r74pvxwdy4a") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.2.1 (c (n "russell_tensor") (v "1.2.1") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1anw90k7wk242n52ylfxxmbj95rbgmga4m9lr05zigcl3pkfwwya") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.3.0 (c (n "russell_tensor") (v "1.3.0") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01chkzgaz22821zr0nw00wz55yb3ap4n9wjy00x97i2g5hg2rd3n") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.4.0 (c (n "russell_tensor") (v "1.4.0") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lxbxd8znvyynvaz5gmaa6my107rsxzjk7hsaccbq36zrbrvcvpv") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.4.1 (c (n "russell_tensor") (v "1.4.1") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n4h0gmgvswnsk0wfkww305mmwsw8mddvjsbrphw9dbzmjpz95w2") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.5.0 (c (n "russell_tensor") (v "1.5.0") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mpcyipayqsiziilppy4qq523mlrrc8y9vydnhgllrsmza8pjca7") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.5.1 (c (n "russell_tensor") (v "1.5.1") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cyv29phqgjl809v7qvns13qywcdnsxp2a3h3qwzc2c163iff6aj") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

(define-public crate-russell_tensor-1.6.0 (c (n "russell_tensor") (v "1.6.0") (d (list (d (n "russell_lab") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zsmcqa41bi1mjiqq8cfic4hkwjlbyjabn8yywa73r1f5fn6dqm4") (f (quote (("intel_mkl" "russell_lab/intel_mkl"))))))

