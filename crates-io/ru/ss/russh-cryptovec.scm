(define-module (crates-io ru ss russh-cryptovec) #:use-module (crates-io))

(define-public crate-russh-cryptovec-0.7.0-beta.1 (c (n "russh-cryptovec") (v "0.7.0-beta.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (k 0)))) (h "0bkygd1mkn49vsiyx98kpw85i7a7l9bbrm892iigm3gr5q5d77z8")))

(define-public crate-russh-cryptovec-0.7.0 (c (n "russh-cryptovec") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (k 0)))) (h "0kh3mjzg9ni5k2hlwjdc00xx28hpbks4m7fiad05asr1q8vg1zf3")))

(define-public crate-russh-cryptovec-0.7.1 (c (n "russh-cryptovec") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1v48xx393nwy6y1d123hnzvwymyjpb0js68f0g1273ab8fbifmd9")))

(define-public crate-russh-cryptovec-0.7.2 (c (n "russh-cryptovec") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0x5wc2zlsbsgk6xbyj9dlbvwfq6zhddcqzrgqvd8bh6qv1npn1rb") (r "1.60")))

(define-public crate-russh-cryptovec-0.7.3 (c (n "russh-cryptovec") (v "0.7.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "100qwl88xz8bc6kpp18k6apbvn36yw3fx53gamk1rqjhnc52rpgs") (r "1.60")))

