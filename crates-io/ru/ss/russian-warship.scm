(define-module (crates-io ru ss russian-warship) #:use-module (crates-io))

(define-public crate-russian-warship-0.1.0 (c (n "russian-warship") (v "0.1.0") (h "1k4xc6g0yp78hha31af4bd4hpwbr0kpj0ady0afihbvi315vvrys")))

(define-public crate-russian-warship-0.2.0 (c (n "russian-warship") (v "0.2.0") (h "1ybm2vr0c89g5mk33k0g5rcd45g9jc7i4gcncq0alm5k6f8hydns")))

(define-public crate-russian-warship-0.2.1 (c (n "russian-warship") (v "0.2.1") (h "0avzjnx8cb1h68gazybp2sjyjffayjyznlmzvsvqrl7dkwskb6g4")))

