(define-module (crates-io ru ss russenger_macro) #:use-module (crates-io))

(define-public crate-russenger_macro-0.1.0 (c (n "russenger_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0pkgpr8ayms4ikfi88llxx0kjfyj14ca3pl6n6xca00gjar5a646")))

