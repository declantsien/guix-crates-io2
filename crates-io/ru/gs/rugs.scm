(define-module (crates-io ru gs rugs) #:use-module (crates-io))

(define-public crate-rugs-0.0.1 (c (n "rugs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dh1l719id38nydha7msmw1bxgl4g09c2nmb0dx3namhqbbg7z7a")))

