(define-module (crates-io ru nn runnt) #:use-module (crates-io))

(define-public crate-runnt-0.1.0 (c (n "runnt") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0h63i1pdx3lzrbihsf9k7hbdfmb7n2j6cm1ww1l0k7vi49g7m0im")))

(define-public crate-runnt-0.2.0 (c (n "runnt") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "19qpy0sp6hx57y5gv4cpd1hqvq40ih6s9l107mcywj7si2pam8qj")))

(define-public crate-runnt-0.3.0 (c (n "runnt") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "0wsh5sxdf8qqyjsz3i9hhdpicyfvmhzb0b8nzlbcnbp6dy8h8rah")))

(define-public crate-runnt-0.4.0 (c (n "runnt") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "1m8q48bmchqyc2b4b12lgm1311kzqnm4iyvw4cgb62nr80rzpq09")))

(define-public crate-runnt-0.5.0 (c (n "runnt") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "0msdj00mdb64y8rwwql2zriqywsrs7xazk98fp9ajkz4s0hyw81d")))

(define-public crate-runnt-0.6.0 (c (n "runnt") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0w3b841bg7apx41nz7c04ng4sgg84sxdc6y6fkm9l1qkpbh2yyvn")))

(define-public crate-runnt-0.7.0 (c (n "runnt") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0b8b3w69yjl1i6flrvkfslw88n4ra50pwkpiva9crj7awm6abazr")))

