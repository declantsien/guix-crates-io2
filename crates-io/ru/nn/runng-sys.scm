(define-module (crates-io ru nn runng-sys) #:use-module (crates-io))

(define-public crate-runng-sys-0.1.0 (c (n "runng-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "05vz8wq3y55qd16h97954a6vphslx1f7237aryrg576i5fjnij8h")))

(define-public crate-runng-sys-0.1.1 (c (n "runng-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "01hwpxndmi7wqhc3cb2sv6wfvy56mjdd3xfm92vh9cfy8n1n6is9")))

(define-public crate-runng-sys-0.1.2 (c (n "runng-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0xfqpyqxcyf7y66pqfliyq97v7qcf0kjc23w7hq64x3w1mb760la")))

(define-public crate-runng-sys-0.1.3 (c (n "runng-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1zbhvlsnlz2ywdxz10k4b8m1c4ba4cp6zz4r5r5j3l9mvx8l4dkc")))

(define-public crate-runng-sys-0.1.4 (c (n "runng-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vl3v4f0mzjrkb9x533wnmynq28g9ya8ln7f00mfci28n3zd93nz")))

(define-public crate-runng-sys-1.0.1 (c (n "runng-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ky370ad22hp3rmjzn7irzw5a6xxpjyz5wdkby0a1v7mbkf5yh3k")))

(define-public crate-runng-sys-1.1.0-rc (c (n "runng-sys") (v "1.1.0-rc") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0v59fmpmf36xzv9xavrqygj0xqynac76ip765smisrlxf5ng67kx")))

(define-public crate-runng-sys-1.1.0 (c (n "runng-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1caiiqd7fmbyb5rhpzm0pv9sc4w7aiy9h7kzqndwkk6ayqj9jvqr") (y #t)))

(define-public crate-runng-sys-1.1.0+1 (c (n "runng-sys") (v "1.1.0+1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "185xmysx20v4xm7yqs5gmvzjzz6pnwib0zxl6fkky8a08zzj7c9g") (y #t)))

(define-public crate-runng-sys-1.1.0+2 (c (n "runng-sys") (v "1.1.0+2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1nncf6hiisz3a83b2119isix0zw3ifngsmd8a8dv9dlg5g0v180p")))

(define-public crate-runng-sys-1.1.1 (c (n "runng-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "09pnf7vichs0d39sjqwczp9pf7kc3k3g18pf5llmna8c7vg3nbyk") (y #t)))

(define-public crate-runng-sys-1.1.1+1 (c (n "runng-sys") (v "1.1.1+1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1af23479iplibh842ixyrgcsj761vjprxfhaj7dz5gqnh35j9rcv") (y #t)))

(define-public crate-runng-sys-1.1.1+2 (c (n "runng-sys") (v "1.1.1+2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1n8fczzb9sj4q4by262mlrj2rwb0hkidldkifpf0av1g4gkprfvz") (f (quote (("default" "build") ("build")))) (y #t)))

(define-public crate-runng-sys-1.1.1+3 (c (n "runng-sys") (v "1.1.1+3") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0q3c63b3lfwhfp965dz58y5j6wiyvc4j8h9xrjr5br08dz0nrlnx") (f (quote (("ninja")))) (y #t)))

(define-public crate-runng-sys-1.1.1+4 (c (n "runng-sys") (v "1.1.1+4") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ikbgw9s2r8vwh0mlix5i6dy3sdk0x71zq5miw8nnii6d3z10pvi") (f (quote (("ninja")))) (y #t)))

(define-public crate-runng-sys-1.1.1-ver.1 (c (n "runng-sys") (v "1.1.1-ver.1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03hrw6g5x8krqdrais1cbazs23ask23xrnfggvhaz4b9wrkh1xgb") (f (quote (("ninja")))) (y #t)))

(define-public crate-runng-sys-1.1.2-ver.1 (c (n "runng-sys") (v "1.1.2-ver.1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1axxa4qfzxvw8783nmps062krlhd5nkbbqx5xrcymgcral7cdf9l") (f (quote (("ninja")))) (y #t)))

(define-public crate-runng-sys-1.1.2-ver.2 (c (n "runng-sys") (v "1.1.2-ver.2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f5wzk3cw15zl16z4mk02fppl6b53f2gybxsms72ks242wr2sy14") (f (quote (("ninja")))) (y #t)))

(define-public crate-runng-sys-1.1.1-rc.1 (c (n "runng-sys") (v "1.1.1-rc.1") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0wfvdca9fjgif924djj8gkqm9pir1mg18l4cxrrb6wqd68w26ph0") (f (quote (("ninja"))))))

(define-public crate-runng-sys-1.1.1-rc.2 (c (n "runng-sys") (v "1.1.1-rc.2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07hzi0i5qmifxx5y3kw7mhdvnnhl6x9drq4v0fk9gwgr75qy2vzc") (f (quote (("tls") ("stats") ("ninja") ("default" "stats"))))))

(define-public crate-runng-sys-1.1.1-rc.3 (c (n "runng-sys") (v "1.1.1-rc.3") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15snzgsapxb74ndmp31dxvm6d2jkqyrg1ik6227ka0sy78c51vrw") (f (quote (("vs2017") ("tls") ("stats") ("ninja") ("default" "stats"))))))

(define-public crate-runng-sys-1.1.1-rc.4 (c (n "runng-sys") (v "1.1.1-rc.4") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "14bnvcm6kywd3nccls618qqiw3lsqckmyh7ff1n32d0gs20z5ssm") (f (quote (("nng-tls") ("nng-stats") ("default" "nng-stats") ("cmake-vs2017-win64") ("cmake-vs2017") ("cmake-ninja"))))))

(define-public crate-runng-sys-1.1.2-rc.1 (c (n "runng-sys") (v "1.1.2-rc.1") (d (list (d (n "bindgen") (r "^0.47") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.36") (o #t) (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1a52hcmv8imi8lmw6ry8kkbx5yiz1ncc9b52d0c7ddv4503jxd9l") (f (quote (("source-update-bindings" "build-bindgen") ("no_std" "cty") ("nng-tls" "build-nng") ("nng-supplemental" "build-bindgen") ("nng-stats" "build-nng") ("nng-compat" "build-bindgen") ("default" "build-nng" "nng-stats") ("cmake-vs2019" "build-nng") ("cmake-vs2017-win64" "build-nng") ("cmake-vs2017" "build-nng") ("cmake-unix" "build-nng") ("cmake-ninja" "build-nng") ("build-nng" "cmake") ("build-bindgen" "bindgen"))))))

(define-public crate-runng-sys-1.2.3-rc.1 (c (n "runng-sys") (v "1.2.3-rc.1") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.36") (o #t) (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "17wckafg6by1ys65rq9h44f4a1fb79bkkcz2s8yry9ml17lk48aa") (f (quote (("source-update-bindings" "build-bindgen") ("no_std" "cty") ("nng-tls" "build-nng") ("nng-supplemental" "build-bindgen") ("nng-stats" "build-nng") ("nng-compat" "build-bindgen") ("default" "build-nng" "nng-stats") ("cmake-vs2019" "build-nng") ("cmake-vs2017-win64" "build-nng") ("cmake-vs2017" "build-nng") ("cmake-unix" "build-nng") ("cmake-ninja" "build-nng") ("build-nng" "cmake") ("build-bindgen" "bindgen"))))))

(define-public crate-runng-sys-1.2.4-rc.1 (c (n "runng-sys") (v "1.2.4-rc.1") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.36") (o #t) (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0l7fn281sf2shcr82c7ws5b4cb2mxwy5a7vz4zn30d3514fvzyms") (f (quote (("source-update-bindings" "build-bindgen") ("no_std" "cty") ("nng-tls" "build-nng") ("nng-supplemental" "build-bindgen") ("nng-stats" "build-nng") ("nng-compat" "build-bindgen") ("default" "build-nng" "nng-stats") ("cmake-vs2019" "build-nng") ("cmake-vs2017-win64" "build-nng") ("cmake-vs2017" "build-nng") ("cmake-unix" "build-nng") ("cmake-ninja" "build-nng") ("build-nng" "cmake") ("build-bindgen" "bindgen"))))))

