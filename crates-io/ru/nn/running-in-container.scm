(define-module (crates-io ru nn running-in-container) #:use-module (crates-io))

(define-public crate-running-in-container-0.0.1 (c (n "running-in-container") (v "0.0.1") (h "1mz2g3vjl7vn8kw489iwsm9p51y0zwr05mffnmbpk5p8594rlypw") (y #t)))

(define-public crate-running-in-container-0.0.2 (c (n "running-in-container") (v "0.0.2") (h "1gd4w0l0i9a6pvbv1bj75nmkg68gcsx0rhwh3zfazj9pa7k78k74") (y #t)))

(define-public crate-running-in-container-0.0.3 (c (n "running-in-container") (v "0.0.3") (h "1lawz9aqyri34b7afv5x2h7ymqjr2l9yid1g7w5f80yiwhkbhysy")))

