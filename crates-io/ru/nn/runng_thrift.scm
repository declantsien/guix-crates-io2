(define-module (crates-io ru nn runng_thrift) #:use-module (crates-io))

(define-public crate-runng_thrift-0.1.0 (c (n "runng_thrift") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "runng") (r "^0.1") (d #t) (k 0)) (d (n "thrift") (r "^0.12.0") (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "1ad1hwh52gl3shag81qr9vjyxy7j4m2rhx0whv56fkny1d3farm9")))

