(define-module (crates-io ru nn runner_iac) #:use-module (crates-io))

(define-public crate-runner_iac-0.1.0 (c (n "runner_iac") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0hfl31im8qg71h91yxpyk5h4q4qivjpw6qjlpza8pm864vxpvgvq") (y #t)))

