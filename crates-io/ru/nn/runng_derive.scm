(define-module (crates-io ru nn runng_derive) #:use-module (crates-io))

(define-public crate-runng_derive-0.1.0 (c (n "runng_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zdrxicpjhn6b8hbcdrgw1i7wkrcjc13sh9rgzadxl7s02yxyhzi")))

(define-public crate-runng_derive-0.1.1 (c (n "runng_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0b7i7hraz19ls18rmlc0zj0ja6di4haaq7rp4na3yyyrxsgdj13v")))

(define-public crate-runng_derive-0.1.2 (c (n "runng_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0g7dxgg2ym97fk8n8nj598f8yxrk036kmjs0klwq7accmylk3vvm")))

(define-public crate-runng_derive-0.2.0 (c (n "runng_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08ckqwcck6ry4wmxwzbvb9h6052jmdc5b52l3d0a2fcsiikqg2db")))

