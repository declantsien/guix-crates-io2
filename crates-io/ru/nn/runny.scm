(define-module (crates-io ru nn runny) #:use-module (crates-io))

(define-public crate-runny-1.0.0 (c (n "runny") (v "1.0.0") (d (list (d (n "fd") (r "^0.2.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.2") (d #t) (k 0)))) (h "1plrkmhj6h18a60rxyldw3rxvxp2pnbnji0f7srya9qpx49402mr")))

(define-public crate-runny-1.0.1 (c (n "runny") (v "1.0.1") (d (list (d (n "fd") (r "^0.2.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.2") (d #t) (k 0)))) (h "051601rn74qggz7fpxh86w19ym8drqgk66wdr6qmky87iyirdpch")))

(define-public crate-runny-1.2.1 (c (n "runny") (v "1.2.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.2") (d #t) (k 0)))) (h "0vvw6frkzghwajzyhfmx9w1ardjhqhh2vnz2zq1cp6ssqrblf5wz")))

(define-public crate-runny-1.2.3 (c (n "runny") (v "1.2.3") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.2") (d #t) (k 0)))) (h "0aipqj7xr64qwydbvq5q1zbvnp02jy40rm20gc9mn5xkr2fq3qiq")))

(define-public crate-runny-1.2.4 (c (n "runny") (v "1.2.4") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.2") (d #t) (k 0)))) (h "01dqqxh9llxxi8z2x0xq13m3k4mpj3sfa5srwf2cpyyabsl1hg0i")))

(define-public crate-runny-1.2.5 (c (n "runny") (v "1.2.5") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.2") (d #t) (k 0)))) (h "03qgcgadw4zmy0jddcfgj8axfv1rhk14a62kd9crdnayma0lpk6z")))

