(define-module (crates-io ru nn runner) #:use-module (crates-io))

(define-public crate-runner-0.1.0 (c (n "runner") (v "0.1.0") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)))) (h "0rxrj3fv94rvbv4hgapz6zqkcfryr70ijqjwq1r2wxzr1qcz38w4")))

(define-public crate-runner-0.1.1 (c (n "runner") (v "0.1.1") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)))) (h "0irrfdwyv0czgh9vijn6n25a5hdqiyk7i5sbp88cz196i0j1gglv")))

(define-public crate-runner-0.2.0 (c (n "runner") (v "0.2.0") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)))) (h "1kl8l6mnwn2brc368vi9dxhqyigsp17k5ryk1prlnag9c7klw97y")))

(define-public crate-runner-0.2.1 (c (n "runner") (v "0.2.1") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)))) (h "1zb808dql10bx37rlhd9yysj7bc2m3a72j6pr4nqs1041h23m5qp")))

(define-public crate-runner-0.2.2 (c (n "runner") (v "0.2.2") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)))) (h "18z905xjl3xw9r5pkpcnma95yq45mwlrpklcczn99v23kad4ainl")))

(define-public crate-runner-0.3.0 (c (n "runner") (v "0.3.0") (d (list (d (n "easy-shortcuts") (r "^0.2") (d #t) (k 0)) (d (n "lapp") (r "^0.2.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)))) (h "134h5im14g29v3q7zqscp0fi9swaf6i7mg1yyqsxzahg26az9j97")))

(define-public crate-runner-0.3.1 (c (n "runner") (v "0.3.1") (d (list (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0hxnrmkryzlw6dfvm6fyixilkiffmlvmm3hzwm6jaacmi6c9lgas")))

(define-public crate-runner-0.3.3 (c (n "runner") (v "0.3.3") (d (list (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "160qf6walmfix3cnd8lllxh3cs8ymh8m8f88czbz9n6cffvwdgk1")))

(define-public crate-runner-0.3.4 (c (n "runner") (v "0.3.4") (d (list (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "11d8hpxfa03sg8xnq5026z81h0hg68zghsw0fdihkjhcpcjq8yj4")))

(define-public crate-runner-0.3.5 (c (n "runner") (v "0.3.5") (d (list (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.8") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0v8cpwrgky32sw3z5p1csb50m47d3hm3zf9xzqch5s3b0404kxs4")))

(define-public crate-runner-0.3.6 (c (n "runner") (v "0.3.6") (d (list (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.8") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lapp") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0m2npws0bvm7823yhdlm3ri6ka6gqnq0lbhcgfpxc52ab4ivvbdl")))

(define-public crate-runner-0.3.7 (c (n "runner") (v "0.3.7") (d (list (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.8") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lapp") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0xm0w9rl7ng2v077wwdcvagvjbqpibmm6caf19j9yqw12af207dx")))

(define-public crate-runner-0.4.0 (c (n "runner") (v "0.4.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.8") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lapp") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0k2q5fvznz85ly52md6i7i2fg9gr9hxnpmr5kv9s13mvi81d389n")))

(define-public crate-runner-0.5.0 (c (n "runner") (v "0.5.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "easy-shortcuts") (r "^0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.8") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lapp") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0vj87h4m6499zdwza7y1jn328k81nb9qsvkd55ndkqmy3bamk8nz")))

