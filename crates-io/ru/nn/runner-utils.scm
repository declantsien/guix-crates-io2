(define-module (crates-io ru nn runner-utils) #:use-module (crates-io))

(define-public crate-runner-utils-0.0.1 (c (n "runner-utils") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0rwzccf3jyvcj7sxk2v60vcaybyzf3m4pjb92nwcwmjzx7038q40")))

(define-public crate-runner-utils-0.0.2 (c (n "runner-utils") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "13wqq9cl3j43s4sqdcg82dhak95ypmb5bakj3vahr6ann146ip69")))

