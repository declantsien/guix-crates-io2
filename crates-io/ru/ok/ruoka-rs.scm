(define-module (crates-io ru ok ruoka-rs) #:use-module (crates-io))

(define-public crate-ruoka-rs-1.0.0 (c (n "ruoka-rs") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0pclc0vsl4xksxiqnq78vvqaz327h6xmzxd7jfnj21s1bh5q4aqp")))

(define-public crate-ruoka-rs-1.0.1 (c (n "ruoka-rs") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1vzs4r3bvnzlqr80g5d354da3dskqydqxkfsd4g5xvqq58zj0fza")))

(define-public crate-ruoka-rs-1.0.2 (c (n "ruoka-rs") (v "1.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1i7q22kn6qgbcpxvbr44va5kasxbfs66i3bba2ci34p4229kw10i")))

(define-public crate-ruoka-rs-1.0.3 (c (n "ruoka-rs") (v "1.0.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0pb49yl029jkly6k3b8gi2vk0fwdvv5xj0v3m550xmvnj98mvpj2")))

(define-public crate-ruoka-rs-1.0.4 (c (n "ruoka-rs") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0qk7lvzcr0wv3rjipb71dv4rhx29clr812zmz9v8pckgwmj0a38l")))

(define-public crate-ruoka-rs-1.0.5 (c (n "ruoka-rs") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "16pmpnfsq7jg1nqdf4wbyrzqiq4acylm2fjla3271clafz687y5m")))

(define-public crate-ruoka-rs-1.0.6 (c (n "ruoka-rs") (v "1.0.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0n1afqwav81v31w2ji63accfzicqay4kc599c0043p91aqfs4793")))

(define-public crate-ruoka-rs-1.0.7 (c (n "ruoka-rs") (v "1.0.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "03cnjkhq3vvscayadkfdfinrm54lizy2m9fymw1ra8zynn4pxvif")))

