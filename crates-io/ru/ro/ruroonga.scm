(define-module (crates-io ru ro ruroonga) #:use-module (crates-io))

(define-public crate-ruroonga-0.1.0 (c (n "ruroonga") (v "0.1.0") (h "1x3cwj70h2saspjy2b7as955x2ah4r7axw2hha4gbw0787k6xa16")))

(define-public crate-ruroonga-0.2.0 (c (n "ruroonga") (v "0.2.0") (h "06raaqv48lzbxisb0l8314b9dlibc1cr9fn0rrnnnsvdmc7sm0gp")))

(define-public crate-ruroonga-0.2.1 (c (n "ruroonga") (v "0.2.1") (h "0ljr61lsmyjd8i1jmm0yvvl67zaj29favcqacc1ff2nf4a87mz08")))

(define-public crate-ruroonga-0.3.0 (c (n "ruroonga") (v "0.3.0") (h "1bmprzp73078rcjpyhkxmdl8fxc6ykbjns9h63wh6na6bnmjzvc2")))

(define-public crate-ruroonga-0.4.0 (c (n "ruroonga") (v "0.4.0") (d (list (d (n "groonga-sys") (r "~0.1.0") (d #t) (k 0)))) (h "0aw13bqyn8dpyd87dl0jhz4jw8dhb6hwbcjcci0ivv27gwdrmzqa")))

(define-public crate-ruroonga-0.4.1 (c (n "ruroonga") (v "0.4.1") (d (list (d (n "groonga-sys") (r "~0.1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0y61kigl2ayp8kwy6m3krpxkhzk8ccrv59kk126zaprsdidna3nc")))

(define-public crate-ruroonga-0.5.0 (c (n "ruroonga") (v "0.5.0") (d (list (d (n "groonga-sys") (r "~0.2.0") (d #t) (k 0)) (d (n "libc") (r "~0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "1is5k7i8safxaf2dflliplhxjmrk3ff1w4srr04swsy14zf6yvc2")))

(define-public crate-ruroonga-0.6.0 (c (n "ruroonga") (v "0.6.0") (d (list (d (n "groonga-sys") (r "~0.3.0") (d #t) (k 0)) (d (n "libc") (r "~0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)))) (h "0fc4i7zwdshghkrg002023q0dr3qz8c55dkq1dirxk08d1hgidpv")))

