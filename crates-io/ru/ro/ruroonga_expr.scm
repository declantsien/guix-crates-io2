(define-module (crates-io ru ro ruroonga_expr) #:use-module (crates-io))

(define-public crate-ruroonga_expr-0.1.0 (c (n "ruroonga_expr") (v "0.1.0") (d (list (d (n "regex-syntax") (r "~0.3") (d #t) (k 0)))) (h "1hy8pw4mckfgjxrqnv9mkaayfxzkmp0f4hjmlnjzdr7nz3j3cmdw")))

(define-public crate-ruroonga_expr-0.2.0 (c (n "ruroonga_expr") (v "0.2.0") (d (list (d (n "regex-syntax") (r "~0.3") (d #t) (k 0)))) (h "0zn6m6h42ysdqnma0zbpfybbq57rpayhy49xfz3fzzhckprli88g")))

