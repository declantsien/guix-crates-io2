(define-module (crates-io ru ro ruroonga_command) #:use-module (crates-io))

(define-public crate-ruroonga_command-0.1.0 (c (n "ruroonga_command") (v "0.1.0") (d (list (d (n "url") (r "~0.5.2") (d #t) (k 0)))) (h "10lk2grl6y1f8zl7sa1jmrah5qn6an4za0qrwl1a4h2hh6aa1sxd") (f (quote (("unstable") ("normalizer_mysql"))))))

(define-public crate-ruroonga_command-0.2.0 (c (n "ruroonga_command") (v "0.2.0") (d (list (d (n "skeptic") (r "~0.4") (k 1)) (d (n "skeptic") (r "~0.4") (k 2)) (d (n "url") (r "~0.5.2") (d #t) (k 0)))) (h "1x80hih4kqa445jzfdin64jyk4amcnp9rma6sncadxz4n56rqrkl") (f (quote (("unstable") ("normalizer_mysql"))))))

(define-public crate-ruroonga_command-0.2.1 (c (n "ruroonga_command") (v "0.2.1") (d (list (d (n "skeptic") (r "~0.4") (k 1)) (d (n "skeptic") (r "~0.4") (k 2)) (d (n "url") (r "~1.0.0") (d #t) (k 0)))) (h "0wm23y21sk6cmlgvim2rp0x9y0d5jh20ywi79f5ma9l7qxgbhvqp") (f (quote (("unstable") ("normalizer_mysql"))))))

(define-public crate-ruroonga_command-0.2.2 (c (n "ruroonga_command") (v "0.2.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "~0.4") (k 1)) (d (n "skeptic") (r "~0.4") (k 2)) (d (n "url") (r "~1.1.0") (d #t) (k 0)))) (h "0vadlfjz3anc5ysyck834rlhjbdly9pxqv0qyblqqm60vjjmwx56") (f (quote (("unstable") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.2.3 (c (n "ruroonga_command") (v "0.2.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "~0.4") (k 1)) (d (n "skeptic") (r "~0.4") (k 2)) (d (n "url") (r "~1.1.0") (d #t) (k 0)))) (h "1z2rikwq0nc6j7kf00sz5nr6jqxa0cjlxh11yss67yq5csld92j6") (f (quote (("unstable") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3.0 (c (n "ruroonga_command") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "~0.4") (k 1)) (d (n "skeptic") (r "~0.4") (k 2)) (d (n "url") (r "~1.1.0") (d #t) (k 0)))) (h "143i1kzgc09kwmi9b838hj7ib4xi8g59d2w5nmmc4xpkiv3x3dbs") (f (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3.1 (c (n "ruroonga_command") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "~0.4") (k 1)) (d (n "skeptic") (r "~0.4") (k 2)) (d (n "url") (r "~1.2.0") (d #t) (k 0)))) (h "1gx7v9i7xsn7hrlsbdd5h3q3k2vns7s2hxvryawghwp3416mc45p") (f (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3.2 (c (n "ruroonga_command") (v "0.3.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "~0.6") (k 1)) (d (n "skeptic") (r "~0.6") (k 2)) (d (n "url") (r "~1.2.0") (d #t) (k 0)))) (h "16pyxnx5x39b48gx8vikfsms27nxlrgdkh9knncn97rs4a7f062p") (f (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3.3 (c (n "ruroonga_command") (v "0.3.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "~0.6") (k 1)) (d (n "skeptic") (r "~0.6") (k 2)) (d (n "url") (r "~1.2.0") (d #t) (k 0)))) (h "18sz71p06a4bf30wjksr2xhkb6dyjk6jsfwhndpfn28dpwps3k6r") (f (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3.4 (c (n "ruroonga_command") (v "0.3.4") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "~0.6") (k 1)) (d (n "skeptic") (r "~0.6") (k 2)) (d (n "url") (r "~1.2.0") (d #t) (k 0)))) (h "1if0gk5pf0b469qyxy6hf3mh9qbr6bwk29zfrs032axx9s19cbcg") (f (quote (("unstable") ("sharding") ("normalizer_mysql") ("groonga_611") ("dev" "clippy"))))))

