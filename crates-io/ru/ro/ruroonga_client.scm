(define-module (crates-io ru ro ruroonga_client) #:use-module (crates-io))

(define-public crate-ruroonga_client-0.1.0 (c (n "ruroonga_client") (v "0.1.0") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "^0.5.2") (d #t) (k 0)))) (h "11zz8m0mqcz6aayhh9p35mdkcj1qfvl92gs6979q56li21hq38w7")))

(define-public crate-ruroonga_client-0.1.1 (c (n "ruroonga_client") (v "0.1.1") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "^0.5.2") (d #t) (k 0)))) (h "0999cs9ilcw5z12rq2w1a621mrcxkxz2ydknwa8yvbfgwzqkd8hp")))

(define-public crate-ruroonga_client-0.2.0 (c (n "ruroonga_client") (v "0.2.0") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "^0.5.2") (d #t) (k 0)))) (h "0j05ivf9q2grqh3q1sbrwyrki94psw636ndpjwb42i8xhd8w3dsd")))

(define-public crate-ruroonga_client-0.3.0 (c (n "ruroonga_client") (v "0.3.0") (d (list (d (n "hyper") (r "~0.7.2") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "~0.5.2") (d #t) (k 0)))) (h "1z5w9xs4qamixs36ajr4a5jqasq63czdxxa16jprgyia7yfbvcmy")))

(define-public crate-ruroonga_client-0.4.0 (c (n "ruroonga_client") (v "0.4.0") (d (list (d (n "hyper") (r "~0.7.2") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "~0.5.2") (d #t) (k 0)))) (h "0jl7mfmx2sm6xhyjqd0m3861fh5gq93jqlg8cmrgdvbff0wgsimn")))

(define-public crate-ruroonga_client-0.4.1 (c (n "ruroonga_client") (v "0.4.1") (d (list (d (n "hyper") (r "~0.8.0") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "~0.5.2") (d #t) (k 0)))) (h "1y6mr06pr6wkjkj4b7gdh0zddcrl8j0mns1iawrw37jawsa9x980")))

(define-public crate-ruroonga_client-0.4.2 (c (n "ruroonga_client") (v "0.4.2") (d (list (d (n "hyper") (r "~0.8.0") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "~1.0.0") (d #t) (k 0)))) (h "01qajy1bhfnyr3kzfd3g55qr6d4a4agx3lil24whs3dzz112xncb")))

(define-public crate-ruroonga_client-0.4.3 (c (n "ruroonga_client") (v "0.4.3") (d (list (d (n "hyper") (r "~0.9.0") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "~1.0.0") (d #t) (k 0)))) (h "05d5qj70698kva7r588gfld7jnm8cq2zcl1k0w0sqr82cvrhcmsp")))

(define-public crate-ruroonga_client-0.5.0 (c (n "ruroonga_client") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hyper") (r "~0.9.0") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "~1.2.0") (d #t) (k 0)))) (h "0pckpaa7ivf6r74b53ydsd4dgj6a3jv7gcqp6i374s1akm1zhm24") (f (quote (("gqtp" "byteorder"))))))

(define-public crate-ruroonga_client-0.5.1 (c (n "ruroonga_client") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hyper") (r "~0.10.0") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "~1.2.0") (d #t) (k 0)))) (h "1n4n2zjjw8dqr32d4ihx7p43hdq5xgzhw1p01hq219pshiv3mypp") (f (quote (("gqtp" "byteorder"))))))

