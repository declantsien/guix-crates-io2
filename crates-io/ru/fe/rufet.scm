(define-module (crates-io ru fe rufet) #:use-module (crates-io))

(define-public crate-rufet-0.1.0 (c (n "rufet") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "1qmcmd64pdblim32xfqcsyc6kg6l4lgmpivj6wgm5zg6wmbdj2kr")))

(define-public crate-rufet-0.1.1 (c (n "rufet") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0wrldcqabs72l38jj2w15hdpq015w9scy02wslb4hk7minn9d35i")))

