(define-module (crates-io ru fe rufetch) #:use-module (crates-io))

(define-public crate-rufetch-0.2.5 (c (n "rufetch") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0s6iy3cwbd1p9jlbnq1zh3pwjngrlagyrmhrzy6w6vgcsczv5n5q")))

