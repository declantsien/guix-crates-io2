(define-module (crates-io ru dg rudg) #:use-module (crates-io))

(define-public crate-rudg-0.1.0 (c (n "rudg") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot_graph") (r "^0.1.0") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.104") (d #t) (k 0)))) (h "0h28hakman80rkk3fl127xc476d0g96ii2w0bgxdqrk50pq50wf6")))

(define-public crate-rudg-0.1.1 (c (n "rudg") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dot_graph") (r "^0.2.0") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.104") (d #t) (k 0)))) (h "18wma4yg2qpvbyz5p2bj89vw55js1zhlx5ddlkqfwzh5sr5p24fy")))

