(define-module (crates-io ru st rustutils-seq) #:use-module (crates-io))

(define-public crate-rustutils-seq-0.1.0 (c (n "rustutils-seq") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "0acyd0sa7n7wak366sgy5fw9lh8qqhjcy0l0da3xr23m1r47m9rq")))

