(define-module (crates-io ru st rustycrate-tr) #:use-module (crates-io))

(define-public crate-rustycrate-tr-0.1.1 (c (n "rustycrate-tr") (v "0.1.1") (h "1lcfmp0f6qgs5bdd8v7jakin2laq1fc2bfallrli85skahfca62c") (y #t)))

(define-public crate-rustycrate-tr-0.1.2 (c (n "rustycrate-tr") (v "0.1.2") (h "05yf6h2prwn60wzakly0r62kx056cdzl0i1v7a92qrzh8bj3in2s")))

