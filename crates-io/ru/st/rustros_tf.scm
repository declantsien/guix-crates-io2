(define-module (crates-io ru st rustros_tf) #:use-module (crates-io))

(define-public crate-rustros_tf-0.1.0 (c (n "rustros_tf") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.20") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12") (f (quote ("netlib"))) (d #t) (k 0)) (d (n "rosrust") (r "^0.9") (d #t) (k 0)) (d (n "rosrust_msg") (r "^0.1") (d #t) (k 0)))) (h "0hnnxxwd6gd6i2mgxfgji3dzzdaqbylamb9b2r4q5ry7p7h40xkq")))

