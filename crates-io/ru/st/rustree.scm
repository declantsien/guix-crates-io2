(define-module (crates-io ru st rustree) #:use-module (crates-io))

(define-public crate-rustree-0.1.0 (c (n "rustree") (v "0.1.0") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "fl2rust") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "fltk") (r "^1.4.19") (f (quote ("fltk-bundled"))) (d #t) (k 0)) (d (n "fltk-theme") (r "^0.7.2") (d #t) (k 0)) (d (n "html2md") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "014yw1a48w789wqjacyahdjd1picj84x23r928cr4n8kdnhi5bfw")))

