(define-module (crates-io ru st rust_compile_run) #:use-module (crates-io))

(define-public crate-rust_compile_run-1.0.0 (c (n "rust_compile_run") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "10ny1g1gryfyfhqqz4kzkqax5fw1fzzdn98plf7xr2spzmf0nrqr")))

(define-public crate-rust_compile_run-1.0.1 (c (n "rust_compile_run") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1cmh9ccvsj3fl1dnq6lfa8haz8xsksr43bfgm34hr0k4zpxvkak3")))

(define-public crate-rust_compile_run-1.0.2 (c (n "rust_compile_run") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1avxdgvn2q4ylhccza88wq6n7nypibh11kr0jk25b858qkdryb5d")))

