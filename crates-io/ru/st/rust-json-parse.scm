(define-module (crates-io ru st rust-json-parse) #:use-module (crates-io))

(define-public crate-rust-json-parse-0.6.3 (c (n "rust-json-parse") (v "0.6.3") (d (list (d (n "bumpalo") (r "^3") (f (quote ("collections" "boxed"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("bumpalo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (f (quote ("aarch64_neon"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "124zmym1dldw81r55p5a1g2b70h08bak6kiljclj6vafysrfyiv8")))

(define-public crate-rust-json-parse-0.6.4 (c (n "rust-json-parse") (v "0.6.4") (d (list (d (n "bumpalo") (r "^3") (f (quote ("collections" "boxed"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("bumpalo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "simdutf8") (r "^0.1") (f (quote ("aarch64_neon"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nl2zhq0blndj4i4rgkczg4116sgr464lp4il4m4bqhxl4ywb4j2")))

