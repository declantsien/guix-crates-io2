(define-module (crates-io ru st rusty_ulid) #:use-module (crates-io))

(define-public crate-rusty_ulid-0.1.0 (c (n "rusty_ulid") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "12ad8r9pdaa9sifj36hf5j029qci5qyh38hwjn86wwm31fb9d3d3")))

(define-public crate-rusty_ulid-0.2.0 (c (n "rusty_ulid") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1874n3z2rqf808qhkwjns9wld0ja5mimwq3wiyzp9rnv3cabyaqv")))

(define-public crate-rusty_ulid-0.2.1 (c (n "rusty_ulid") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "00gighj7lx1xk3sihrg9vr2dzwdysr1wmchcw1m2rfbhg1b3y70d")))

(define-public crate-rusty_ulid-0.3.0 (c (n "rusty_ulid") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1hmvgc4h6gh3j8hzadkm86dsrxn2c8bk2b0zhigajq1fbw3gj0rq")))

(define-public crate-rusty_ulid-0.4.0 (c (n "rusty_ulid") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1fwb8lmw97qmaf6wx8xfbnwp37qny4r70z4yxgbw9zlqgcyhk434")))

(define-public crate-rusty_ulid-0.4.1 (c (n "rusty_ulid") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1k3s40asvihh762ksmj0f4w0cvqd3c34cby0qxsfz4vh04rb241k")))

(define-public crate-rusty_ulid-0.5.0 (c (n "rusty_ulid") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1ljmfm2wfyxgsxvn8g42bvlqlpbx40grlpnalh1w8dk607dvkw3z")))

(define-public crate-rusty_ulid-0.6.0 (c (n "rusty_ulid") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1689kgwa3cjsq1i7vbgf7jaxvqachy8yfihjg69fiypa77c4hf1f")))

(define-public crate-rusty_ulid-0.6.1 (c (n "rusty_ulid") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "1jfgdxq4ynbx0m56i6c60iq9fkfn6s5i8xsg7iwchb9i7bv3c3ng")))

(define-public crate-rusty_ulid-0.7.0 (c (n "rusty_ulid") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "02gpy3w5kdm7x7gvak1c75pdcw6vnz634d2rmizmfz5akfk204ma")))

(define-public crate-rusty_ulid-0.8.0 (c (n "rusty_ulid") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1psx2yhw640vxr21ams794qbdbh3jp63hirly5bywrx57bb36hxx") (f (quote (("default" "serde"))))))

(define-public crate-rusty_ulid-0.9.0 (c (n "rusty_ulid") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1frl48sgfn1i4w0db7nn2s1pyql94n15fznl0kag77hla6jbpw80") (f (quote (("ulid-generation" "chrono" "rand") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-0.9.1 (c (n "rusty_ulid") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1ssx74nvcfw9yr7a4x3w9jnwi5s30z6h7ynigixyafph5qgzmm9v") (f (quote (("ulid-generation" "chrono" "rand") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-0.9.2 (c (n "rusty_ulid") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0ffl5yh8xigjvajfz3wnn9534akaydgabs6216g4s31xzd1h6zd5") (f (quote (("ulid-generation" "chrono" "rand") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-0.9.3 (c (n "rusty_ulid") (v "0.9.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0wp4zvrs17ka4a4m4pggysbjipdrbb5mfhygi9n447r9nvrz6agg") (f (quote (("ulid-generation" "chrono" "rand") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-0.10.0 (c (n "rusty_ulid") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "12zwbmvi7ir654bicf2ifdmbxa066i3drbflggymj056nf2vw5m1") (f (quote (("ulid-generation" "chrono" "rand") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-0.10.1 (c (n "rusty_ulid") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "clock"))) (o #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0bbvysfr67bkr3hb5p4zmj7v4120pd2qiy6c7gkyvzljkqppa80z") (f (quote (("ulid-generation" "chrono" "rand") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-0.11.0 (c (n "rusty_ulid") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "clock"))) (o #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0lxa163x0lsvkfhn8yqln9r4g6bfldy2206w7sj2q7s79c55gyir") (f (quote (("ulid-generation" "chrono" "rand") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-1.0.0 (c (n "rusty_ulid") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "clock"))) (o #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)))) (h "183si4qkr5b3vvlqqy0g3cdmh2zyb6fx1x8d36d3g6z7cks6jqfg") (f (quote (("ulid-generation-time" "rand" "time") ("ulid-generation" "rand" "chrono") ("default" "ulid-generation" "serde" "doc-comment"))))))

(define-public crate-rusty_ulid-2.0.0 (c (n "rusty_ulid") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "clock"))) (o #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("std" "formatting"))) (o #t) (k 0)))) (h "09i62mgrrpja2iz12kkxwmfpij4z1yrdi2rcl3swdpqbab01j1ry") (f (quote (("default" "rand" "time" "serde")))) (r "1.60")))

