(define-module (crates-io ru st rust-stemmers) #:use-module (crates-io))

(define-public crate-rust-stemmers-0.1.0 (c (n "rust-stemmers") (v "0.1.0") (h "1ivzj90h1svzsxsl1mpcy5zblc92rky72hfcgzw3h0kly6gf7643")))

(define-public crate-rust-stemmers-1.0.0 (c (n "rust-stemmers") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0g1nbmkrrc98rmjkl97wn8anf6cwzwzb8sqzhlidx7p6hsn3dx6w")))

(define-public crate-rust-stemmers-1.0.1 (c (n "rust-stemmers") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sh3n67ak17dmsnv7jkdx7175mc46iyy723i6682y212r1f29844")))

(define-public crate-rust-stemmers-1.0.2 (c (n "rust-stemmers") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1h2b9fq0xg2ya46i92iz1ygliyqsxl620d2n99k2a41rxi4n3w7v")))

(define-public crate-rust-stemmers-1.1.0 (c (n "rust-stemmers") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1aj6sqrshxsgb8rfcp2scdim297h4ix0ahyvk1mqzcw5gcc8r4h5")))

(define-public crate-rust-stemmers-1.2.0 (c (n "rust-stemmers") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0m6acgdflrrcm17dj7lp7x4sfqqhga24qynv660qinwz04v20sp4")))

