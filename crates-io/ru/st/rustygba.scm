(define-module (crates-io ru st rustygba) #:use-module (crates-io))

(define-public crate-rustygba-0.0.1 (c (n "rustygba") (v "0.0.1") (d (list (d (n "gba") (r "^0.5.2") (d #t) (k 0)))) (h "1ngqyjayz2g7ymv0az9r4pzmffjr4fcivvf7zf8v8cpf07n0i6aq")))

(define-public crate-rustygba-0.0.2 (c (n "rustygba") (v "0.0.2") (d (list (d (n "gba") (r "^0.5.2") (d #t) (k 0)))) (h "0visgaqrdwj08wkdfwgkvyxq2h1faxy2zi7ikl3k9rmrbznckrbi")))

