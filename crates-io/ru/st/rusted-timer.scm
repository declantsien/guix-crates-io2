(define-module (crates-io ru st rusted-timer) #:use-module (crates-io))

(define-public crate-rusted-timer-1.1.4 (c (n "rusted-timer") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "0.3.*") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "153i0xagckl7d975h8w33ykp3f92v04b0vrvz8l026344lg8sjmp")))

