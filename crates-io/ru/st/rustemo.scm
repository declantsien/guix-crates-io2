(define-module (crates-io ru st rustemo) #:use-module (crates-io))

(define-public crate-rustemo-0.1.0 (c (n "rustemo") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1isbj12b6lqcxzsnsang3z4lsrxgkkglmyqicd6c37bzdab11s6x") (r "1.69")))

(define-public crate-rustemo-0.2.0 (c (n "rustemo") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1mvzwzrgj4q82qpdh30cyfvihzr4ibxzpvy2kq44v3fhi1nvl69i") (f (quote (("default" "glr")))) (s 2) (e (quote (("glr" "dep:petgraph")))) (r "1.64")))

(define-public crate-rustemo-0.3.0 (c (n "rustemo") (v "0.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0pykpvmirsbjcznyzshd7mdd8fjrza73i1v9pf4867kak0jc474p") (f (quote (("default" "glr")))) (s 2) (e (quote (("glr" "dep:petgraph")))) (r "1.64")))

(define-public crate-rustemo-0.4.0 (c (n "rustemo") (v "0.4.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0k66sgs0ci3dln73gbv4s0nrsk60gl2wq5zpip0yg0l0wfgjrm9p") (f (quote (("default" "glr")))) (s 2) (e (quote (("glr" "dep:petgraph")))) (r "1.74")))

(define-public crate-rustemo-0.5.0 (c (n "rustemo") (v "0.5.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1hhhjvdjnpllsxwkdasxp3ck2ghsnn2hflvyv3rnh8piljg16bqr") (f (quote (("default" "glr")))) (s 2) (e (quote (("glr" "dep:petgraph")))) (r "1.74")))

(define-public crate-rustemo-0.6.0 (c (n "rustemo") (v "0.6.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (o #t) (d #t) (k 0)))) (h "16c6g08cf8qylb19i2pms7xzaslm1jy0nzrcvlxfd16qnf4l2az8") (f (quote (("default" "glr")))) (s 2) (e (quote (("glr" "dep:petgraph")))) (r "1.74")))

