(define-module (crates-io ru st rust-gc-count) #:use-module (crates-io))

(define-public crate-rust-gc-count-0.1.0 (c (n "rust-gc-count") (v "0.1.0") (d (list (d (n "base64-url") (r "^2.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1gvq04vm017bjmwwv03fyaw3jx3dqwnn8xlgk3qzln55yn5cz53l")))

