(define-module (crates-io ru st rusty-variation) #:use-module (crates-io))

(define-public crate-rusty-variation-0.1.0 (c (n "rusty-variation") (v "0.1.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 2)))) (h "0bxf6isc5is6k7mcl8ca6iw8mlnhy6x8757cd5av1s95fmr9parj")))

