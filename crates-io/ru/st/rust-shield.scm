(define-module (crates-io ru st rust-shield) #:use-module (crates-io))

(define-public crate-rust-shield-0.1.0 (c (n "rust-shield") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "shuttle-rocket") (r "^0.27.0") (d #t) (k 0)) (d (n "shuttle-runtime") (r "^0.27.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (d #t) (k 0)))) (h "0jd2sk1msmb2vxqalq514cj7n91fdv4blrwj13ir2y01ng5sw7ri")))

