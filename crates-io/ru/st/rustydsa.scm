(define-module (crates-io ru st rustydsa) #:use-module (crates-io))

(define-public crate-RustyDSA-0.1.0 (c (n "RustyDSA") (v "0.1.0") (h "0z9sh860iq3i2vzmqz8vp3gwq4lx9zl78knvvnwasj33mhb16j98") (y #t)))

(define-public crate-RustyDSA-0.1.1 (c (n "RustyDSA") (v "0.1.1") (h "0ralfd6kxyq9l1xb7s1kjh74acmm3ch4hpkavs389pdqxkz6yzq1") (y #t)))

(define-public crate-RustyDSA-0.1.2 (c (n "RustyDSA") (v "0.1.2") (h "05m5rkykpf6rw62ck74p2byhb75ml4jkznwq1qaxjkf0dq0lf7ix")))

