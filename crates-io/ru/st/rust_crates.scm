(define-module (crates-io ru st rust_crates) #:use-module (crates-io))

(define-public crate-rust_crates-0.1.0 (c (n "rust_crates") (v "0.1.0") (h "1c23lwws6ppba81prli53j1xxwn9myc3fsndd6qxyl2ra1isbcwz")))

(define-public crate-rust_crates-0.1.1 (c (n "rust_crates") (v "0.1.1") (h "1b0j09jpqcr18cipkq6m4g04ajfp1477agjc63xjhjhv138bx0mf")))

