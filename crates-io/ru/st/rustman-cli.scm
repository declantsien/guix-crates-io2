(define-module (crates-io ru st rustman-cli) #:use-module (crates-io))

(define-public crate-rustman-cli-0.1.0 (c (n "rustman-cli") (v "0.1.0") (h "0734mqa0ii0k74858x6q2lg1dx9077jds62h2gv1jwnvkh1k2wq1")))

(define-public crate-rustman-cli-0.0.1 (c (n "rustman-cli") (v "0.0.1") (h "18pakbl2jwi9jrrcf6b540y007hv61hk83lznbqz1sgcf3k0yi5k")))

