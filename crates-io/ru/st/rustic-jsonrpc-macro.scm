(define-module (crates-io ru st rustic-jsonrpc-macro) #:use-module (crates-io))

(define-public crate-rustic-jsonrpc-macro-0.1.0 (c (n "rustic-jsonrpc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1c15xjsgng78db2qbc0ncg0kf9d1f2ryzq3s1a5h36dbvp8h2r3z")))

(define-public crate-rustic-jsonrpc-macro-0.1.1 (c (n "rustic-jsonrpc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "05qxhc0vr1wz15cvjf51bmssrg05b4gwdr1dibds84gll5lda8kj")))

(define-public crate-rustic-jsonrpc-macro-0.1.2 (c (n "rustic-jsonrpc-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "02yngwi2hhf60rb3nrbdd7fv47ink6lmvwhy4p4y1xibd6inganq")))

