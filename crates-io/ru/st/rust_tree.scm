(define-module (crates-io ru st rust_tree) #:use-module (crates-io))

(define-public crate-rust_tree-0.1.0 (c (n "rust_tree") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "101g9rxmq49wynsbbzq8scb6fpz5a80859hylrvagj5fb69ik9m8")))

(define-public crate-rust_tree-0.1.1 (c (n "rust_tree") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0v4x0c8qfam2f34351bqc7p4cljxqa6dyv67q27kq8zjsd56xh5m")))

(define-public crate-rust_tree-0.1.2 (c (n "rust_tree") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1ws71bk6nqvlibgmgjz0alapifgw086qk0whrp04h8pwcnzn8y5i")))

(define-public crate-rust_tree-0.1.4 (c (n "rust_tree") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1kndg1pxmnrfdkbbnjjx9j0zf3iq0vpgjpn6kxkci4alwy2ay5y6")))

