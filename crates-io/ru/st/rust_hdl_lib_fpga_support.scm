(define-module (crates-io ru st rust_hdl_lib_fpga_support) #:use-module (crates-io))

(define-public crate-rust_hdl_lib_fpga_support-0.44.0 (c (n "rust_hdl_lib_fpga_support") (v "0.44.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust_hdl_lib_core") (r "^0.44.0") (d #t) (k 0)) (d (n "rust_hdl_lib_widgets") (r "^0.44.0") (d #t) (k 0)))) (h "1v1cbgdgjihs72n9ka9kvy865kjaznard8dq7jkkjlg0cmkalpq0")))

