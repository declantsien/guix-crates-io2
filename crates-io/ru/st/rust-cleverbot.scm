(define-module (crates-io ru st rust-cleverbot) #:use-module (crates-io))

(define-public crate-rust-cleverbot-0.0.1 (c (n "rust-cleverbot") (v "0.0.1") (d (list (d (n "hyper") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "09i2xdc3ig4bqn81p3mzkp68xyvykxxgjz57npm5digqjjky3bgs")))

(define-public crate-rust-cleverbot-0.0.2 (c (n "rust-cleverbot") (v "0.0.2") (d (list (d (n "hyper") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0p4npvqmlkd6acw5s8g2rvf7vp7mszlzwzya5yk351nvnd6yaynj")))

(define-public crate-rust-cleverbot-0.1.0 (c (n "rust-cleverbot") (v "0.1.0") (d (list (d (n "hyper") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "19870m2iwn2f0l8cpwsqhd1j30d5pyn72qkvxqwlbwfnqgik626q")))

(define-public crate-rust-cleverbot-0.1.1 (c (n "rust-cleverbot") (v "0.1.1") (d (list (d (n "hyper") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0kfp8v72fc2kf3vic90ksyp12hzrq6kv8ccfwd5idd20qdzrvci2")))

