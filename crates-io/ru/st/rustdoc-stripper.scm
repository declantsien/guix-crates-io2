(define-module (crates-io ru st rustdoc-stripper) #:use-module (crates-io))

(define-public crate-rustdoc-stripper-0.1.0 (c (n "rustdoc-stripper") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "11jzh8lknkhqzvfpj9p2hzksy84ph6ndqygc1clnmklwa2ankc4z")))

(define-public crate-rustdoc-stripper-0.1.1 (c (n "rustdoc-stripper") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1j8kycyax4p7ygxsyz3nznddsq3cpgzz1pga5fk7fka4404rcbj8")))

(define-public crate-rustdoc-stripper-0.1.2 (c (n "rustdoc-stripper") (v "0.1.2") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "08ib58jmv9029dr6h0sz58abc37b9ky4fffzplnfvqd2hddw7a73")))

(define-public crate-rustdoc-stripper-0.1.3 (c (n "rustdoc-stripper") (v "0.1.3") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0rsv10rc77ng5jlz3prqbvna0ykcnfin9jns0qb3b9y4gm3ym1m5")))

(define-public crate-rustdoc-stripper-0.1.4 (c (n "rustdoc-stripper") (v "0.1.4") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "15fgfsqnf8461yyjvglavfl9ig8k76xlg3p236y55dzci0wrfw5b")))

(define-public crate-rustdoc-stripper-0.1.5 (c (n "rustdoc-stripper") (v "0.1.5") (d (list (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0gd9l68mk2r998bbwydsyivbbmbl5hlip6xf1cypc3cbn422wjac")))

(define-public crate-rustdoc-stripper-0.1.6 (c (n "rustdoc-stripper") (v "0.1.6") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "17v8d6qnx4ixnhbysanz2vwii6avwpij3f9y35prgqypqnzqxvrw")))

(define-public crate-rustdoc-stripper-0.1.7 (c (n "rustdoc-stripper") (v "0.1.7") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1lkzipkb65qx1rc81cxawgbmv9824wkyp1jwkga2x3aypvwpa72c")))

(define-public crate-rustdoc-stripper-0.1.8 (c (n "rustdoc-stripper") (v "0.1.8") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "10vcl3shpfbymibczrnizd6jvbpf1d73v5jjpzchxscab5x7mbby")))

(define-public crate-rustdoc-stripper-0.1.9 (c (n "rustdoc-stripper") (v "0.1.9") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "13rf65a0xk1zyb92jd8p12x529rsbfchwk9zvjr0snr9savpxw19")))

(define-public crate-rustdoc-stripper-0.1.11 (c (n "rustdoc-stripper") (v "0.1.11") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0bjnxnmhd99m54b7wibdx6wykpfbdhcp2n0nhzjp7l34d09bd55k")))

(define-public crate-rustdoc-stripper-0.1.12 (c (n "rustdoc-stripper") (v "0.1.12") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "14qpm5ysrbj9m0panp31n9ybbdr1ml51na8jrxhg4zxkmbwwj488")))

(define-public crate-rustdoc-stripper-0.1.13 (c (n "rustdoc-stripper") (v "0.1.13") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "08ara74j9lmvsqz57k1ahmzhd2apj96cfmxzlhpa71827ck30amc")))

(define-public crate-rustdoc-stripper-0.1.14 (c (n "rustdoc-stripper") (v "0.1.14") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1qxcjps2p9406af4vr63cwkrsb8brscai3hp2v915iz1bmfvwc3y")))

(define-public crate-rustdoc-stripper-0.1.15 (c (n "rustdoc-stripper") (v "0.1.15") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0212jnld4mrqrhr2p7hbd2asgr0shxdinx70y05yw80llfbvawb5")))

(define-public crate-rustdoc-stripper-0.1.16 (c (n "rustdoc-stripper") (v "0.1.16") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "053041694rjfcs0c6nkfz164d67klmj66wkf8dwlcc7y75gf57wp")))

(define-public crate-rustdoc-stripper-0.1.17 (c (n "rustdoc-stripper") (v "0.1.17") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "18d465yrlr49n48g0hsxb0bcvil58shsjd57kv4vwcsh5l7g7vr4")))

(define-public crate-rustdoc-stripper-0.1.18 (c (n "rustdoc-stripper") (v "0.1.18") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1420y45mvr6ax7wc27pp0fbc9a46khppbafhpyy2shldpk9mzhvh")))

(define-public crate-rustdoc-stripper-0.1.19 (c (n "rustdoc-stripper") (v "0.1.19") (d (list (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "03gliqsc0mmk03p8c33sdxvwcxg4n0v784vsk02b9lv9v8m47cks")))

