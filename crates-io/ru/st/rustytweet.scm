(define-module (crates-io ru st rustytweet) #:use-module (crates-io))

(define-public crate-rustytweet-0.1.0 (c (n "rustytweet") (v "0.1.0") (d (list (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kc03kr8ad4n54qzr19jkvafpq1k2xwy7n99576zh3x8461w430k")))

