(define-module (crates-io ru st rust-gist) #:use-module (crates-io))

(define-public crate-rust-gist-0.1.0 (c (n "rust-gist") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "0mnyxxv6bvi130jbp0ib1m18dmciw0gkw2hqn0q9w82vrwjmq0qf")))

(define-public crate-rust-gist-0.1.1 (c (n "rust-gist") (v "0.1.1") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "1z769y6w0nz4yajpq7pn4ka94kjk54174da0y8mdnja7na8gh1pl")))

(define-public crate-rust-gist-0.1.2 (c (n "rust-gist") (v "0.1.2") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "0v5xrzxgncsfkg8vg1fp113kzcnmp5sivllkgq1fg98lhim0l87a")))

(define-public crate-rust-gist-0.1.3 (c (n "rust-gist") (v "0.1.3") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "145a6q5bxw5ggwdrsdbp6wskszywap7x36s64mirshnnlf4vlr81")))

(define-public crate-rust-gist-0.1.4 (c (n "rust-gist") (v "0.1.4") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "1cxjl7g228990li9qrc7mmzxz3fpp612shq84m99if96b97hz4qh")))

