(define-module (crates-io ru st rust7z) #:use-module (crates-io))

(define-public crate-rust7z-0.1.0 (c (n "rust7z") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.20") (d #t) (k 1)))) (h "14xqkaimrv51ywhpf6yv7pqiq3zs823jrjvvih8yv5yrfcrzq10s")))

(define-public crate-rust7z-0.2.0 (c (n "rust7z") (v "0.2.0") (d (list (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "1lw9a82987rf598hyfh1yqs81nv3wx03q3br26s4lxd8pq2r2b7k")))

