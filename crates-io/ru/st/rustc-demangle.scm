(define-module (crates-io ru st rustc-demangle) #:use-module (crates-io))

(define-public crate-rustc-demangle-0.1.0 (c (n "rustc-demangle") (v "0.1.0") (h "1mi3qr92b2sqrp7yllcird2x81xx57yw90asscac5qz8gmbhj16v")))

(define-public crate-rustc-demangle-0.1.1 (c (n "rustc-demangle") (v "0.1.1") (h "1w2cx91cmdmhcjhfx30awx63kzvf9pp6x8r6mm7yqk6r5rdx7hn4")))

(define-public crate-rustc-demangle-0.1.2 (c (n "rustc-demangle") (v "0.1.2") (h "16wqzlpkgqld5yripkyivpmajpwbbjais365qkvpvw8zyaajfav6")))

(define-public crate-rustc-demangle-0.1.3 (c (n "rustc-demangle") (v "0.1.3") (h "1p46jn6wl3lw6zs7kdbbj89p3jc2r53m9ql5r0bpq8yvra3d4c0l")))

(define-public crate-rustc-demangle-0.1.4 (c (n "rustc-demangle") (v "0.1.4") (h "158w8zzs820l2zh61qhvjzdyk98g5mx00f5fnf90nb9cv8xa8n1h")))

(define-public crate-rustc-demangle-0.1.5 (c (n "rustc-demangle") (v "0.1.5") (h "0zl1kvravivggp7dswcywrwzjldc5la59h4c22d7nby6mhr59r5f")))

(define-public crate-rustc-demangle-0.1.6 (c (n "rustc-demangle") (v "0.1.6") (h "1s0cq476gzziapgqa699jl5s2cvc1nkj7x56h4siv8sgi9zla4pk")))

(define-public crate-rustc-demangle-0.1.7 (c (n "rustc-demangle") (v "0.1.7") (h "1jx3b51cg8dh8qi2frzngz6bd3hzjk5rpkygszzichd00si47yqi")))

(define-public crate-rustc-demangle-0.1.8 (c (n "rustc-demangle") (v "0.1.8") (h "0jc6hil8nlaij9wpbfdbyp0076j8s8mq3vcfsgr49npsx8gvmmvn")))

(define-public crate-rustc-demangle-0.1.9 (c (n "rustc-demangle") (v "0.1.9") (h "158kh7g0ak7baw0aadcwcdfdk5wifvbyppyaqbjphk8v449mpzmw")))

(define-public crate-rustc-demangle-0.1.10 (c (n "rustc-demangle") (v "0.1.10") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "03l13hbvchsmwfi8zblhl67pngfd1hy751k41275s1dkl5x9bbl2") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.11 (c (n "rustc-demangle") (v "0.1.11") (h "1xxjx9qm2cni7r4dp21pj6w74cgxs5fxr6ymc0sdhkb6p1wh7f81")))

(define-public crate-rustc-demangle-0.1.13 (c (n "rustc-demangle") (v "0.1.13") (h "06cnrbhn1hmg4bpd70qrdpkqv5v5ygfzrb67zlvyrdh2dphsmb5d")))

(define-public crate-rustc-demangle-0.1.12 (c (n "rustc-demangle") (v "0.1.12") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "07kjy977zawdr9k2z3jbs575z3kdpczigdd5mwmk4w4azpqks9zq") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.14 (c (n "rustc-demangle") (v "0.1.14") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "123jlmvra36pk9rw5pm8mxpjv9dibjg8kkzzkklg6yydbbyqpiyc") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.15 (c (n "rustc-demangle") (v "0.1.15") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1brqf2bknkxsdzn3kd3wfifvzfc33bmvdy9r1k6fp4a8dz7xrx57") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.16 (c (n "rustc-demangle") (v "0.1.16") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "10qp42sl1wrdbgbbh8rnay2grm976z7hqgz32c4y09l1c071qsac") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.17 (c (n "rustc-demangle") (v "0.1.17") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "02sgyqvfxvyslmdg1wai39hfc5wn4r89jj1vzxywh61xcizhnqdj") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.18 (c (n "rustc-demangle") (v "0.1.18") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0cn2hdq0glr875hvpi0hvb19xj3y9gfnk0lnsw3wl538wc7asfvf") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.19 (c (n "rustc-demangle") (v "0.1.19") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1kkhizz8nj94qlprypsxj5hc9rpl9fnnnm6rqlklb95k7k7pl3s1") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.20 (c (n "rustc-demangle") (v "0.1.20") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0jcdh4zanli1r4pcfj5ah1xcbxv87vh02syb9f0rqgp0nnq71bfy") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.21 (c (n "rustc-demangle") (v "0.1.21") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0hn3xyd2n3bg3jnc5a5jbzll32n4r5a65bqzs287l30m5c53xw3y") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.22 (c (n "rustc-demangle") (v "0.1.22") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0nrfs6g3hb7xk13mp904qqdqknb4jr7hkpivqmvrlgw7s516r8yl") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.23 (c (n "rustc-demangle") (v "0.1.23") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0xnbk2bmyzshacjm2g1kd4zzv2y2az14bw3sjccq5qkpmsfvn9nn") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-rustc-demangle-0.1.24 (c (n "rustc-demangle") (v "0.1.24") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "07zysaafgrkzy2rjgwqdj2a8qdpsm6zv6f5pgpk9x0lm40z9b6vi") (f (quote (("std") ("rustc-dep-of-std" "core" "compiler_builtins"))))))

