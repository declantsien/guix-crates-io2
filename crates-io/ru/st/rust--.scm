(define-module (crates-io ru st rust--) #:use-module (crates-io))

(define-public crate-rust---0.1.0 (c (n "rust--") (v "0.1.0") (h "0vcmbxk1kqym3vvyxdc78gz6q1xm0r02czchwdyc1fh8klpcgf39")))

(define-public crate-rust---0.1.1 (c (n "rust--") (v "0.1.1") (h "00jlvpbcb7ajrv603b34w0nhsarx951184j671d4g5d02a7b35lx")))

