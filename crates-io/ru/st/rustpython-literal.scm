(define-module (crates-io ru st rustpython-literal) #:use-module (crates-io))

(define-public crate-rustpython-literal-0.3.0 (c (n "rustpython-literal") (v "0.3.0") (d (list (d (n "hexf-parse") (r "^0.2.1") (d #t) (k 0)) (d (n "is-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "lexical-parse-float") (r "^0.8.0") (f (quote ("format"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unic-ucd-category") (r "^0.9") (d #t) (k 0)))) (h "0yirp6c5ql5b8wc74ivqj4gbb51dqxsksdlhfnxpdmwcwsip1m7m")))

(define-public crate-rustpython-literal-0.3.1 (c (n "rustpython-literal") (v "0.3.1") (d (list (d (n "hexf-parse") (r "^0.2.1") (d #t) (k 0)) (d (n "is-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "lexical-parse-float") (r "^0.8.0") (f (quote ("format"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unic-ucd-category") (r "^0.9") (d #t) (k 0)))) (h "1h3jrvn81d52h0iw0myva68xq039f5a6dc94gvkpp8l73ayvqg24") (r "1.72.1")))

