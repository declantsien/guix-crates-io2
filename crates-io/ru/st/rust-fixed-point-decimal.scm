(define-module (crates-io ru st rust-fixed-point-decimal) #:use-module (crates-io))

(define-public crate-rust-fixed-point-decimal-0.1.0 (c (n "rust-fixed-point-decimal") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-macros") (r "^0.1.0") (d #t) (k 0)))) (h "11mkfkdrhbi2wvg3wpflkr59n7phlakphyblgi7snmrgan631v40")))

(define-public crate-rust-fixed-point-decimal-0.1.1 (c (n "rust-fixed-point-decimal") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-macros") (r "^0.1.0") (d #t) (k 0)))) (h "13nxkqq2yg0hqyb60kshx0wxvzhy2ydphq2q54z2wzrfm59ikh7b")))

(define-public crate-rust-fixed-point-decimal-0.1.2 (c (n "rust-fixed-point-decimal") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1xk76j44vw1hwgwpszq5nigjmv4q1zdgs524d6cjzj0f09b0fwnp")))

