(define-module (crates-io ru st rusty_hearth) #:use-module (crates-io))

(define-public crate-rusty_hearth-0.1.0 (c (n "rusty_hearth") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "rocket") (r "^0.2.8") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.2.8") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "116alvz3179np9qnmxaznlq5pra2acl8vydraiqd35jj34qfr59r")))

(define-public crate-rusty_hearth-0.1.1 (c (n "rusty_hearth") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "rocket") (r "^0.2.8") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.2.8") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0csq5qv8cyblx4i810yxh72j7kz56s06ylsfmkr8w4cab4mkyq45")))

