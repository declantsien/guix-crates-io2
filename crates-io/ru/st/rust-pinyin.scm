(define-module (crates-io ru st rust-pinyin) #:use-module (crates-io))

(define-public crate-rust-pinyin-0.1.0 (c (n "rust-pinyin") (v "0.1.0") (h "1bxmpqrg1bk7jm0kwy4acmd4xca50pwwnl6z1p72w910vn9x1a2c")))

(define-public crate-rust-pinyin-0.1.1 (c (n "rust-pinyin") (v "0.1.1") (h "15xbl35iaf3pmdrv9y26z5hjaf5c5cww02dk4swn7n0kch2z40jz")))

(define-public crate-rust-pinyin-0.1.2 (c (n "rust-pinyin") (v "0.1.2") (h "0f1plzbqkhdp62cplhh4h6mf0516ipcsxgdsd01ffhlz7d2zdwy4")))

(define-public crate-rust-pinyin-0.1.3 (c (n "rust-pinyin") (v "0.1.3") (h "1bx5wggpkw8vb5xd0a32d048zv3pjhb9f68jgh9jzzfc6fyc6x88")))

