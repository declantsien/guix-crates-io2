(define-module (crates-io ru st rustc-host) #:use-module (crates-io))

(define-public crate-rustc-host-0.1.0 (c (n "rustc-host") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0iq7n3rcgl9bk5m468mslyf8m10ppvnlarq8cx65635lga8i06gj") (r "1.56.1")))

(define-public crate-rustc-host-0.1.1 (c (n "rustc-host") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1c625cpy0igrymihds339f9wcbjp6pf5xi4j5xkp6j86schgdn7d") (r "1.56.1")))

(define-public crate-rustc-host-0.1.2 (c (n "rustc-host") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0jbg5l3q0ixv68shiz3yv3lg6mq7jvmwsqrh64mjbs2wga8rlk0y") (r "1.56.1")))

(define-public crate-rustc-host-0.1.3 (c (n "rustc-host") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0rgshqn97jiq64fv5vml33mvyws79xd36cfiv4masfz0vgm4vfz1") (r "1.56.1")))

(define-public crate-rustc-host-0.1.4 (c (n "rustc-host") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1zj8jcwpkdibl9bmzp12bx7c6cya6gfyk67zp9d83dan0szb2isv") (r "1.56.1")))

(define-public crate-rustc-host-0.1.5 (c (n "rustc-host") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "069hbrq8fdy53dwk549jwn78mbvsmnd4rgqhv95s71cfym6dyy5q") (r "1.56.1")))

(define-public crate-rustc-host-0.1.6 (c (n "rustc-host") (v "0.1.6") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0lql94rrnmnxxz04hrnldc5l0gskv7jakx29hl9015cig8qw1ar0") (r "1.56.1")))

(define-public crate-rustc-host-0.1.7 (c (n "rustc-host") (v "0.1.7") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1wmrqz4vcqvhhywsa8h13q4i8a0n7xynx01q2rj7wl6chk1a7k1j") (f (quote (("unsafe")))) (r "1.56.1")))

