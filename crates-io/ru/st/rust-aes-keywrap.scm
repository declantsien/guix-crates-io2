(define-module (crates-io ru st rust-aes-keywrap) #:use-module (crates-io))

(define-public crate-rust-aes-keywrap-0.1.0 (c (n "rust-aes-keywrap") (v "0.1.0") (d (list (d (n "aes") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "0x5dbyazs160lpyiisjv4dzrhknsy7788rw0n0ssi11yydjb1193")))

