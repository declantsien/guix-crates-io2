(define-module (crates-io ru st rust-scheme) #:use-module (crates-io))

(define-public crate-rust-scheme-0.0.1 (c (n "rust-scheme") (v "0.0.1") (h "07d3xi7x9v3bb00n54v9chq6jk8ic6yllw968vc622acav3nfzka")))

(define-public crate-rust-scheme-0.1.0-alpha.0 (c (n "rust-scheme") (v "0.1.0-alpha.0") (h "1ld2gkq92pk9mhwwabhb64v2vw33931g82flqybq9bc7slc6wsxf")))

(define-public crate-rust-scheme-0.1.0-alpha.1 (c (n "rust-scheme") (v "0.1.0-alpha.1") (h "0rfvn3i3jlwq73h3qwrqqxy1bqk5malylnyyqw13dqzfmvbq07bl")))

(define-public crate-rust-scheme-0.1.0-alpha.2 (c (n "rust-scheme") (v "0.1.0-alpha.2") (h "111gm4h54kzbay6lz99lyrh0pn5jsaaff3xzrpg31x41ysmnsf0j")))

(define-public crate-rust-scheme-0.1.0 (c (n "rust-scheme") (v "0.1.0") (h "06a2jmvsc9lv4x10vh8bp14zjp7dp0kwahccpq7nw4qhamm4lzgk") (y #t)))

(define-public crate-rust-scheme-0.1.1-alpha.0 (c (n "rust-scheme") (v "0.1.1-alpha.0") (h "0jb49fyns104xbc3x2qhbrpzbh0l072r7l442jvsmkj6h6rfg114")))

(define-public crate-rust-scheme-0.1.1-alpha.1 (c (n "rust-scheme") (v "0.1.1-alpha.1") (h "1gg311asbzp92797clkyb67f6ijmvicm8fn6s24ji6bhrv069xmk")))

(define-public crate-rust-scheme-0.1.0-alpha.3 (c (n "rust-scheme") (v "0.1.0-alpha.3") (h "1sbcywv8mxww9ydrllbbsagi1x6hxgr6rgifa6nr5bclaza6rkhp")))

(define-public crate-rust-scheme-0.1.1-alpha.3 (c (n "rust-scheme") (v "0.1.1-alpha.3") (h "117j4njhm7wk51fxb2iyf6aawyxc743ijcfz45q2h79zlccvg9sb")))

(define-public crate-rust-scheme-0.1.1-alpha.4 (c (n "rust-scheme") (v "0.1.1-alpha.4") (h "0s4paplnh8ia03wvx12h6qq1dnf0700dw3121c1gz38pw6dz66hi")))

(define-public crate-rust-scheme-0.1.1-alpha.5 (c (n "rust-scheme") (v "0.1.1-alpha.5") (h "13a43a4ajx9nax5sq2rd865ry6786skqadb93sx9dzqwr5s0a7ia")))

(define-public crate-rust-scheme-0.1.1-alpha.6 (c (n "rust-scheme") (v "0.1.1-alpha.6") (h "096j8m7y8h6dvzg3kkm67r6fdjsv7nm3v2d2was8245c89zlp624")))

(define-public crate-rust-scheme-0.1.1-alpha.7 (c (n "rust-scheme") (v "0.1.1-alpha.7") (h "0vbz30xlz4yq9xa8mw0f14fnz4ifhkfqbrvvf3srw1mr83ajl2sr")))

(define-public crate-rust-scheme-0.1.1 (c (n "rust-scheme") (v "0.1.1") (h "0jmmbaqjblxhhsyasl074b1j3j7qxzg0y4rq4c2qm22pzby81ys0") (y #t)))

(define-public crate-rust-scheme-0.1.2-alpha.0 (c (n "rust-scheme") (v "0.1.2-alpha.0") (h "0l6r9k3x6aq4k78j54451vjwpp3qw076gxcpqkm5x01wh9ffvvz0")))

(define-public crate-rust-scheme-0.1.2-alpha.1 (c (n "rust-scheme") (v "0.1.2-alpha.1") (h "1755s0929402knyzzc38axcni43z8xa04vf9i4raab2g873y89s1")))

(define-public crate-rust-scheme-0.1.2-alpha.2 (c (n "rust-scheme") (v "0.1.2-alpha.2") (h "19blj6yk5879yg60hlg9iccn5pvg56dvr0i9w9r0infjhl4c9qwz") (y #t)))

