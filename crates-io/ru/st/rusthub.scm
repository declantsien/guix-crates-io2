(define-module (crates-io ru st rusthub) #:use-module (crates-io))

(define-public crate-rusthub-0.1.1 (c (n "rusthub") (v "0.1.1") (h "0aixv5ap9c4szpf7pvf5i8n1a5y1hr544rrf8w9gqqji3m11j8hp") (y #t)))

(define-public crate-rusthub-0.1.0 (c (n "rusthub") (v "0.1.0") (h "1xchqca3nl100k5gaaycv3rzz912q3xif5c4vrhh6xddvaa4l8nw")))

