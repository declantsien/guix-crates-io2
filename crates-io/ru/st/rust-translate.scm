(define-module (crates-io ru st rust-translate) #:use-module (crates-io))

(define-public crate-rust-translate-0.1.0 (c (n "rust-translate") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10d5lqqxzpk07r70kd699r5ax0dwy46jn0gc9zzxj2f1yvv93jv3")))

(define-public crate-rust-translate-0.1.1 (c (n "rust-translate") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08rvkaxkp5ljp4gk2sy1d9mnzp9giyd12w1i4gfm0s3gxkcj2m45")))

(define-public crate-rust-translate-0.1.2 (c (n "rust-translate") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pj9acmp8jks8zgdrp1j64ksypkprslharp1vx5liaq161nvaj1z")))

(define-public crate-rust-translate-0.1.3 (c (n "rust-translate") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ykhkdhpqicmkdz1a8hpm209rdpd97dz3d9jdck7jn7lj2svz7ss")))

