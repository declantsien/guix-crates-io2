(define-module (crates-io ru st rust-algorithm) #:use-module (crates-io))

(define-public crate-rust-algorithm-0.1.0 (c (n "rust-algorithm") (v "0.1.0") (h "1p49y0w2q4wxfkr91fxp8rgdr6pbrwx3q74h6wjqivfqy3ykq0ml")))

(define-public crate-rust-algorithm-0.1.1 (c (n "rust-algorithm") (v "0.1.1") (h "18n5izxxdvlw8navz9hrhxnj11n5la8ybgpyjmqznydh2izwaijw")))

(define-public crate-rust-algorithm-0.1.2 (c (n "rust-algorithm") (v "0.1.2") (h "1viinivpk7dfqf73sl28qvarvfnv4y30p471qj686kh6bxbpjbyf")))

(define-public crate-rust-algorithm-0.1.3 (c (n "rust-algorithm") (v "0.1.3") (h "0vaspav5z8nmw8qwdxbnhcgy4caz0kviq0gw3bycjfgdwl7hn4gr")))

(define-public crate-rust-algorithm-0.1.4 (c (n "rust-algorithm") (v "0.1.4") (h "1n9x5n0zfka6b5x2q5x8kkfmpksha7gdh2nmi3xx3bsn0m5dqns9")))

