(define-module (crates-io ru st rust-simple-console-tool) #:use-module (crates-io))

(define-public crate-rust-simple-console-tool-0.1.0 (c (n "rust-simple-console-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0pyvipilz68ycyj8xp5fw8a4smb4j1hy9msifazbsv94a567lyvr")))

