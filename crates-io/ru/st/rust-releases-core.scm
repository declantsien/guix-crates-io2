(define-module (crates-io ru st rust-releases-core) #:use-module (crates-io))

(define-public crate-rust-releases-core-0.15.1 (c (n "rust-releases-core") (v "0.15.1") (d (list (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1y60zyp7wk8yaiv0jm4nj4jz5aiifwa0y7z54gwdlmh5xd2g12bi")))

(define-public crate-rust-releases-core-0.16.0 (c (n "rust-releases-core") (v "0.16.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0yikyl3k5186fdmn1k3hial5w44mafvmr1lmixhj675qgh0hqwvr")))

(define-public crate-rust-releases-core-0.17.0 (c (n "rust-releases-core") (v "0.17.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "15fsn66i8qb2f8q9b0wkval07c84ni317l1q0h6fx33qyyhr19bn") (y #t)))

(define-public crate-rust-releases-core-0.18.0 (c (n "rust-releases-core") (v "0.18.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1aya26v10v8c3kzw1dz9nklvaznfmqwy6ikim972gvhy3ipq1gfk") (y #t)))

(define-public crate-rust-releases-core-0.20.0 (c (n "rust-releases-core") (v "0.20.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "19d6qgy90pf80z3cnbhiqlafli5pkr1mli5rnrrnxsvdyl2vl21p") (y #t)))

(define-public crate-rust-releases-core-0.21.0 (c (n "rust-releases-core") (v "0.21.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1r8c4y7m6xw11x7bwn3wg2vyv8n4197a3i00a1m4nswx9p8z393z")))

(define-public crate-rust-releases-core-0.21.1 (c (n "rust-releases-core") (v "0.21.1") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "17iiwxig13wd66555y6yxvxrpk616cgfqhrjb6dgrkv2118jpg2f")))

(define-public crate-rust-releases-core-0.22.0 (c (n "rust-releases-core") (v "0.22.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "14c31a9shc57777bzjq00kdfiqha8qllq2g2n1f97gj25hi4qlj6")))

(define-public crate-rust-releases-core-0.22.1 (c (n "rust-releases-core") (v "0.22.1") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0fhzcd3f5wq626l5rr8yr1rvahg7gqap8w8gadijm2rxnqibc95b")))

(define-public crate-rust-releases-core-0.23.0 (c (n "rust-releases-core") (v "0.23.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "05k74vh3c956kbf459fbm9b305f5rj08gkm1yrc45qfcjb3vzncm") (r "1.63")))

(define-public crate-rust-releases-core-0.24.0 (c (n "rust-releases-core") (v "0.24.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "11yjgng4zsqjjk3c0ss8nic1m4vypk6pnbzfb99fhl4pmq7gxv9j") (r "1.63")))

(define-public crate-rust-releases-core-0.25.0 (c (n "rust-releases-core") (v "0.25.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1839q0w80v75g5fnldnq5l9b2vn6j5xjxl1xjbgkpp3wnqij9vqs") (r "1.63")))

(define-public crate-rust-releases-core-0.26.0 (c (n "rust-releases-core") (v "0.26.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0adzhzlsnawrj3cj0annclpr3pml5ndl3ljls6cx0z1y0ra9s05r") (r "1.63")))

(define-public crate-rust-releases-core-0.27.0 (c (n "rust-releases-core") (v "0.27.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0s38lp3r1mklfcglc6xqhipls6y1gw2kmmqv2p3iw66j8fjrnc6a") (r "1.63")))

(define-public crate-rust-releases-core-0.28.0 (c (n "rust-releases-core") (v "0.28.0") (d (list (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "yare") (r "^2.0.0") (d #t) (k 2)))) (h "008yq0gjkhd8zamq3m4kf0nns3n5q0dgwq1d7lq312f0y7xmzyqv") (r "1.63")))

