(define-module (crates-io ru st rust_masscan) #:use-module (crates-io))

(define-public crate-rust_masscan-0.1.0 (c (n "rust_masscan") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ym41rg7akwa8aj9sl46ac8j7dlhf3mxybnd3029jpiczsgd5zf1")))

(define-public crate-rust_masscan-0.1.1 (c (n "rust_masscan") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s9id1hmwj9j7gnx4p87pm49irwccwbkm2az5amgf6qkiq1sb72c")))

(define-public crate-rust_masscan-0.1.2 (c (n "rust_masscan") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g52291fy90dfs06xafkx6va0w73sk15ll4q08ry9jyrwmbwkswy")))

(define-public crate-rust_masscan-0.1.3 (c (n "rust_masscan") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "005bb4in6wy22cpvxcszsjr6d31vigrc3hrsmip1fkfnxh9lal35")))

(define-public crate-rust_masscan-0.1.4 (c (n "rust_masscan") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02527l0nv1w8ccndx4y6cf2rjjn38m0fnkgb9kc15dg9cl9vmvmn")))

(define-public crate-rust_masscan-0.1.5 (c (n "rust_masscan") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qv83bsxsdn06snrgb3d91p88wnw9caa80yb1iqri5hwj8jj5sxj")))

(define-public crate-rust_masscan-0.1.6 (c (n "rust_masscan") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00ghh42wy4rrgzl3lmnk16jrm44y52zkyx3as58iapfr99ykm9wm")))

(define-public crate-rust_masscan-0.1.7 (c (n "rust_masscan") (v "0.1.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19hryckjhsc3073i2xfbz39xf12b9j317akadyjrys0msndd46ay")))

