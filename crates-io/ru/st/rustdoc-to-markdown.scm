(define-module (crates-io ru st rustdoc-to-markdown) #:use-module (crates-io))

(define-public crate-rustdoc-to-markdown-0.1.0 (c (n "rustdoc-to-markdown") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "01iks16j8ycpnsrbjnih2sdw57dlr8f127valclvf7zpcl38whh3")))

