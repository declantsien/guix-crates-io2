(define-module (crates-io ru st rusty-cffi) #:use-module (crates-io))

(define-public crate-rusty-cffi-0.0.1 (c (n "rusty-cffi") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "1.*") (d #t) (k 0)))) (h "1i3xj5b3n5djjqb7apm6d1f8945lbm8ga22vh6f0c7xn5c0wzdv8")))

(define-public crate-rusty-cffi-0.0.2 (c (n "rusty-cffi") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pry52ziwmyhlbsybvfxsddlvn7larw0myhpmpadvis7vxi6zmd4")))

