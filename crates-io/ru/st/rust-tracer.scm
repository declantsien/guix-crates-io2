(define-module (crates-io ru st rust-tracer) #:use-module (crates-io))

(define-public crate-rust-tracer-0.1.0 (c (n "rust-tracer") (v "0.1.0") (h "0r1cimfkgyhrm7qk0jcxn4fxxl9x7bm9i5k7q8472g9x3qaxvkh7")))

(define-public crate-rust-tracer-0.1.1 (c (n "rust-tracer") (v "0.1.1") (h "1x1csqzlrlwq9j700cvblgfscpga39qr0v9j6a3scj0ws30xmqxg")))

(define-public crate-rust-tracer-0.2.0 (c (n "rust-tracer") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "03dihy3788l7gkj98qg54sybgwgkhvjp7hw66kr4lff9h52xy01p")))

(define-public crate-rust-tracer-0.3.0 (c (n "rust-tracer") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1a752dcriqkva04k7ggccwy05lqllp993q3czwc4m7ff65kkjnaj")))

(define-public crate-rust-tracer-0.4.0 (c (n "rust-tracer") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0l4mnxqjrzg1m9zpq7szrzpglggj3jsln1p31jkcybkw9s29jh2r")))

(define-public crate-rust-tracer-0.4.1 (c (n "rust-tracer") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "09xi0ph1zl18qnrwwaby8fk3z6dgplvk9gp4bnp67q1amr4xicnq")))

(define-public crate-rust-tracer-0.5.0 (c (n "rust-tracer") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "11s0h6ijmgn0b720g3glc1dr03cmfphwn62hwcah3apnkj3cii02")))

