(define-module (crates-io ru st rust-oauth-proxy) #:use-module (crates-io))

(define-public crate-rust-oauth-proxy-0.1.0 (c (n "rust-oauth-proxy") (v "0.1.0") (d (list (d (n "base64") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "slog") (r "^1") (d #t) (k 0)) (d (n "slog-term") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1i4zmj221jc7ak0cbc32j80q0mkhkmhnlgagnnz7iv0vybmfq229") (y #t)))

