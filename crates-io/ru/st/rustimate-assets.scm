(define-module (crates-io ru st rustimate-assets) #:use-module (crates-io))

(define-public crate-rustimate-assets-0.0.1 (c (n "rustimate-assets") (v "0.0.1") (d (list (d (n "rust-embed") (r "^5.1.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "1az3bdb1fv3g81aiwlmw1gmidqdcs32r9mdsdfm51j0clv0kf3zh")))

(define-public crate-rustimate-assets-0.1.0 (c (n "rustimate-assets") (v "0.1.0") (d (list (d (n "rust-embed") (r "^5.2.0") (f (quote ("interpolate-folder-path"))) (d #t) (k 0)))) (h "15ky1v7pm46r3mivm2pb3v93filh0gmalia1s798vaahrg0chz57")))

