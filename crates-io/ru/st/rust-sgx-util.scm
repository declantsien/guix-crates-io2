(define-module (crates-io ru st rust-sgx-util) #:use-module (crates-io))

(define-public crate-rust-sgx-util-0.1.0 (c (n "rust-sgx-util") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0a83a95w5b5zx7ms8i697a7m4l0ysc9xirc5lqqapmchybah158s")))

(define-public crate-rust-sgx-util-0.1.1 (c (n "rust-sgx-util") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1an09gv058ka1wp96b9blsd42s85z4i8l7zw8lf5yh5wzlmb4wyn")))

(define-public crate-rust-sgx-util-0.2.0 (c (n "rust-sgx-util") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "base64") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sc17ciwlnzwgmi135jfcr12ynz7c6r44irvycqdz7rpwmpj3lcv") (f (quote (("with_serde" "serde" "base64") ("default"))))))

(define-public crate-rust-sgx-util-0.2.1 (c (n "rust-sgx-util") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "base64") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10ki1phc9igjnrx36czjwga14da42dlxwj77ps1gmi2r47zynzn1") (f (quote (("with_serde" "serde" "base64") ("default"))))))

(define-public crate-rust-sgx-util-0.2.2 (c (n "rust-sgx-util") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "base64") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "13smg98s95d40n9p5vzmjs7v2588v8m2wwj8dx092611607hr5k1") (f (quote (("with_serde" "serde" "base64") ("default"))))))

(define-public crate-rust-sgx-util-0.2.3 (c (n "rust-sgx-util") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "base64") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hwhph88y8mmp36q095x27hc10a57v5amxcls5kf0qwykzd4d844") (f (quote (("with_serde" "serde" "base64") ("default"))))))

