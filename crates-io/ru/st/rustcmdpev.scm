(define-module (crates-io ru st rustcmdpev) #:use-module (crates-io))

(define-public crate-rustcmdpev-0.1.0 (c (n "rustcmdpev") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.4.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.12") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0w57w1dcwakx15vxyj5fspccgzsici4slc2lqpimmk3247a2h4vi")))

