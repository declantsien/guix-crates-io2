(define-module (crates-io ru st rust-hdl-sim-chips) #:use-module (crates-io))

(define-public crate-rust-hdl-sim-chips-0.1.0 (c (n "rust-hdl-sim-chips") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-yosys-synth") (r "^0.1") (d #t) (k 0)))) (h "1j9cbh9hh87jrs5gz4bhmvysqw4z09spjhwvjfpqadsb82d2nfji") (y #t)))

