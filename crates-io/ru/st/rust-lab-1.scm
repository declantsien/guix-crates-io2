(define-module (crates-io ru st rust-lab-1) #:use-module (crates-io))

(define-public crate-rust-lab-1-0.1.0 (c (n "rust-lab-1") (v "0.1.0") (h "0x93lkqn2qz87dwqkma5w2zlif568k9xfghzdm1zffd2xy7lcym7")))

(define-public crate-rust-lab-1-0.1.1 (c (n "rust-lab-1") (v "0.1.1") (h "1cab6c85sy792jv1xi0xqw6mzsqbp0brdcc44hhbgaakdmnxky4y")))

