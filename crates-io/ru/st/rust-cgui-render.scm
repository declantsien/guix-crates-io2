(define-module (crates-io ru st rust-cgui-render) #:use-module (crates-io))

(define-public crate-rust-cgui-render-0.1.1 (c (n "rust-cgui-render") (v "0.1.1") (d (list (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "rust-cgui") (r "^0.1.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.5") (d #t) (k 0)))) (h "1lvaklc81z3kxb9d9y1ak94nq7r0biphvbg4mp7gxf9i5pvl6ali") (y #t)))

(define-public crate-rust-cgui-render-0.1.2 (c (n "rust-cgui-render") (v "0.1.2") (d (list (d (n "image") (r "^0.20.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "rust-cgui") (r "^0.1.12") (d #t) (k 0)) (d (n "rusttype") (r "^0.5") (d #t) (k 0)))) (h "03i34g2xcswkbwhw77hpcsrdxxx53akawrjdhizl5mwy97b66djx") (y #t)))

