(define-module (crates-io ru st rusty-duplication) #:use-module (crates-io))

(define-public crate-rusty-duplication-0.1.0 (c (n "rusty-duplication") (v "0.1.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "1kqh6hbnww96f2pz1lx986lx61wc4a12yyr0a77ljfxc5x6rq8kn")))

(define-public crate-rusty-duplication-0.2.0 (c (n "rusty-duplication") (v "0.2.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "1z0ky2k3hr0mma8g0nfrfyv7vqz5mc77mmgjcgrwxvzs3c6s4xrs")))

(define-public crate-rusty-duplication-0.3.0 (c (n "rusty-duplication") (v "0.3.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "0l33i5d5v9gvy51ib5cih33mndrq6qj6vb70qhwlj3xmxb9zfj3s")))

(define-public crate-rusty-duplication-0.4.0 (c (n "rusty-duplication") (v "0.4.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "17fr367yqfn6vgcap459nxgcjl367l7hva4lf1ypca67pl6xc6lq")))

(define-public crate-rusty-duplication-0.4.1 (c (n "rusty-duplication") (v "0.4.1") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "0n88cxpv2hhs5lzzm3ih1dr7d2374hqxj7y6kqba49bdabr5959z")))

(define-public crate-rusty-duplication-0.4.2 (c (n "rusty-duplication") (v "0.4.2") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "0aa7w554s45xmvp84wq19agci4sg7sychlkmn7chqz28vfhr4asd")))

(define-public crate-rusty-duplication-0.4.3 (c (n "rusty-duplication") (v "0.4.3") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "0lp24j93kxcd417b8r7mfp8axgdhnbziznqjkvkl3p07i2xll277")))

(define-public crate-rusty-duplication-0.4.4 (c (n "rusty-duplication") (v "0.4.4") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "1gdc6d6bn8332b1qz4cynwycr9k0bw48qhplf2qv2v5crcl6yg88")))

(define-public crate-rusty-duplication-0.4.5 (c (n "rusty-duplication") (v "0.4.5") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "1jagc5jz8hqqx1pwrrxflvjzzpp0pj44w864vfm0vmg3hbjyb04c")))

(define-public crate-rusty-duplication-0.5.0 (c (n "rusty-duplication") (v "0.5.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Graphics_Dxgi" "Win32_Graphics_Direct3D11" "Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_System_Memory" "Win32_Security"))) (d #t) (k 0)))) (h "1m7fq2an9f3qp3mkz2b753zz739cm7mirdf4696q2rkryhj3xa2d")))

