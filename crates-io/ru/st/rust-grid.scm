(define-module (crates-io ru st rust-grid) #:use-module (crates-io))

(define-public crate-rust-grid-0.1.0 (c (n "rust-grid") (v "0.1.0") (h "054cnfqg9vp67m963y9b4krhjm10fxwv6ph74hyf13zrj6xmafr6")))

(define-public crate-rust-grid-0.1.1 (c (n "rust-grid") (v "0.1.1") (h "11b6kliyxkvjqkw0w4a9rdg4mfhns8czjx8gppjnz68qw0kz8z84")))

