(define-module (crates-io ru st rust_kits) #:use-module (crates-io))

(define-public crate-rust_kits-0.1.0 (c (n "rust_kits") (v "0.1.0") (h "0rxisqciwpps11b6jmk789qna3p6a03jb3krwcb5gdakqlz5ddgc")))

(define-public crate-rust_kits-0.1.2 (c (n "rust_kits") (v "0.1.2") (d (list (d (n "atomic") (r "^0.6.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)))) (h "0sp60a32hxsfkwxqifmj0hyrx4x8nj22w4f3dmhlc6vn2p2a18ip")))

