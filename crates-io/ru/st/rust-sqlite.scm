(define-module (crates-io ru st rust-sqlite) #:use-module (crates-io))

(define-public crate-rust-sqlite-0.1.1 (c (n "rust-sqlite") (v "0.1.1") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "05x9h5h7i2x8pzg8vpkkxx0agwm514kfgrjb7955l9wax877mf15")))

(define-public crate-rust-sqlite-0.1.2 (c (n "rust-sqlite") (v "0.1.2") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1hyzf93h1wkydypdjspi254ql6nh5k1bnf8f610svbbpnbh1kyrx")))

(define-public crate-rust-sqlite-0.1.3 (c (n "rust-sqlite") (v "0.1.3") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1n8gi99jlyja2b1d6j2hpkrikm9wr26axsb34rg2j5nr4fwivzih")))

(define-public crate-rust-sqlite-0.2.0 (c (n "rust-sqlite") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_primitive") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0a6v8vliwkzl7vrb238dm7q57ps2m7v4q3f09q33wxrqyyrc8ppw")))

(define-public crate-rust-sqlite-0.3.0 (c (n "rust-sqlite") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "time") (r "^0.1.5") (d #t) (k 0)))) (h "1lw532smrz662slzffpnl4n34rrpq6cfhzkyhvn90ww11k5zzgxp")))

