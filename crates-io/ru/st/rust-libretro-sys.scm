(define-module (crates-io ru st rust-libretro-sys) #:use-module (crates-io))

(define-public crate-rust-libretro-sys-0.1.0 (c (n "rust-libretro-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.117") (d #t) (k 0)))) (h "1xd232qmxbsfxl18js8d6dryz9gbvh4bcrbvrd12abg7sw37h61z")))

(define-public crate-rust-libretro-sys-0.1.4 (c (n "rust-libretro-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)))) (h "13k07rw00bdjw63ifnrv5cba982pjkmsmjxi0m6lz75yyisnjvvj")))

(define-public crate-rust-libretro-sys-0.1.5 (c (n "rust-libretro-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)))) (h "0rjcp2xdb3p3h8cxnkc1c2sbhdgpsgzav0mqxgnjrc0510jfz0xd")))

(define-public crate-rust-libretro-sys-0.2.3 (c (n "rust-libretro-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "rust-libretro-sys-proc") (r "^0.2.3") (d #t) (k 0)))) (h "01pp5pikbf1b5h32p426yzfl40m8r4ix5zqafwg50ylqb8h31yjh")))

(define-public crate-rust-libretro-sys-0.3.0 (c (n "rust-libretro-sys") (v "0.3.0") (d (list (d (n "ash") (r "^0.37.2") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "rust-libretro-sys-proc") (r "^0.3.0") (d #t) (k 0)))) (h "03fpjhfi43firvcmplyix3h55f3nn0rpgr2vqs4qd705l7nxjrjk") (s 2) (e (quote (("vulkan" "dep:ash"))))))

(define-public crate-rust-libretro-sys-0.3.1 (c (n "rust-libretro-sys") (v "0.3.1") (d (list (d (n "ash") (r "^0.37.2") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "rust-libretro-sys-proc") (r "^0.3.1") (d #t) (k 0)))) (h "1lpq2srnnvxsl76vqr3a3x4hgdmld84ksdk6z7rpkwmpjki3hv22") (s 2) (e (quote (("vulkan" "dep:ash"))))))

(define-public crate-rust-libretro-sys-0.3.2 (c (n "rust-libretro-sys") (v "0.3.2") (d (list (d (n "ash") (r "^0.37.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "rust-libretro-sys-proc") (r "^0.3.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "1ijqr6h6ypqljpwkm7yyldpr528y7ri14x0972z9blcpr83b3g8w") (s 2) (e (quote (("vulkan" "dep:ash"))))))

