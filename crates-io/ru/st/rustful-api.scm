(define-module (crates-io ru st rustful-api) #:use-module (crates-io))

(define-public crate-rustful-api-0.0.1 (c (n "rustful-api") (v "0.0.1") (h "05z2ys7js2n1waxjn74y0f34rpwhdlq6jlnwx6cl2mimxd6lx0kr")))

(define-public crate-rustful-api-0.0.2 (c (n "rustful-api") (v "0.0.2") (h "029mdp70mld2l2b8p9rw4aqwyqvxy27qvwampladyy79rdbf1j29")))

