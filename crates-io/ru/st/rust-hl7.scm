(define-module (crates-io ru st rust-hl7) #:use-module (crates-io))

(define-public crate-rust-hl7-0.2.0 (c (n "rust-hl7") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1prs8b51b5xvnhfi1819vxhmhva3h695nbqwllzddrbgfn6jv9hz")))

(define-public crate-rust-hl7-0.2.1 (c (n "rust-hl7") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gmk9vfi2rwhpms6m9dqklxif29b5xsj6cfs2y4p4sn16kb6c486")))

(define-public crate-rust-hl7-0.2.2 (c (n "rust-hl7") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j57c0b4mhizs4bfafs4ws6d9cyx9pjianyhsr7933s61c1p8y8r")))

(define-public crate-rust-hl7-0.3.0 (c (n "rust-hl7") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07jbdbb6mivisr6rd9hw3mq2dfd7rv8fqmyxp8ix6snx0r117wvz")))

(define-public crate-rust-hl7-0.4.0 (c (n "rust-hl7") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c2p7gpr4vj27b6h3754wbya3zvp1g0px13ldw0jwg77bk4skhq9")))

(define-public crate-rust-hl7-0.5.0 (c (n "rust-hl7") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "012ywl0bh8qpsrkvsh1ydyg8indh8c76a8cczay7syg8qzq6khs8") (f (quote (("string_index"))))))

