(define-module (crates-io ru st rust-fp-pfds) #:use-module (crates-io))

(define-public crate-rust-fp-pfds-0.0.1 (c (n "rust-fp-pfds") (v "0.0.1") (d (list (d (n "rust-fp-categories") (r "^0.0.1") (d #t) (k 0)))) (h "08yzsr6ywprlmj746hsc0ds1n5cgnl87x8l5imv1k0498dnxjr6w")))

(define-public crate-rust-fp-pfds-0.0.2 (c (n "rust-fp-pfds") (v "0.0.2") (d (list (d (n "rust-fp-categories") (r "^0.0.3") (d #t) (k 0)))) (h "10x8ai1cs6fvsg9s1plpsh2x7r4a729av49985j7786i8f844vi9")))

(define-public crate-rust-fp-pfds-0.0.4 (c (n "rust-fp-pfds") (v "0.0.4") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rust-fp-categories") (r "^0.0.4") (d #t) (k 0)))) (h "1xnzhk5q7fqrqa3zwf7qgxlk0qc5jlcxvqdsg4fja9pn4isyqhy4")))

