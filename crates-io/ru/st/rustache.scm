(define-module (crates-io ru st rustache) #:use-module (crates-io))

(define-public crate-rustache-0.0.1 (c (n "rustache") (v "0.0.1") (d (list (d (n "memstream") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0nli7q6bz2ivh1y6c9fl4dgjrzmcd72y2ax4z8sbqylv21d246aa")))

(define-public crate-rustache-0.0.2 (c (n "rustache") (v "0.0.2") (d (list (d (n "memstream") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1jrib01zmgp1xjzmw7i8pcb6i9ayx4caypmrlx6ml7pxawipf28p")))

(define-public crate-rustache-0.0.3 (c (n "rustache") (v "0.0.3") (d (list (d (n "memstream") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1q0lifq56vnhb9cvgv2jjfhwm5rb6jdq43qvm2xx1qiy6gzhp8l8")))

(define-public crate-rustache-0.0.4 (c (n "rustache") (v "0.0.4") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0b1x64ha57sw924yss6y1lrkj9jk2dw2pxa5msd9pgd1iz7zkxk8")))

(define-public crate-rustache-0.1.0 (c (n "rustache") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0282w3viw3d7fljrbf3f8hb9kbby4av8xg8vsph1hmhs7i2fjvf8")))

