(define-module (crates-io ru st rusty-tool) #:use-module (crates-io))

(define-public crate-rusty-tool-0.1.0 (c (n "rusty-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "1a2m180ich3vxybk5af0hh8jpa0amvv45gl12y8462w87crsj5br")))

