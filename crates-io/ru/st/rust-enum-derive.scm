(define-module (crates-io ru st rust-enum-derive) #:use-module (crates-io))

(define-public crate-rust-enum-derive-0.2.1 (c (n "rust-enum-derive") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)))) (h "1hll62b0ivz2hmrb80hf4yy7l8l27d5gad3kwccdza78vz6sh257")))

(define-public crate-rust-enum-derive-0.3.0 (c (n "rust-enum-derive") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "00blp2hjjqfrsayy08svb6m6sf1cc45p3yk0xd86494jjcbpr8cd")))

(define-public crate-rust-enum-derive-0.3.1 (c (n "rust-enum-derive") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "146p34kxkkjxgn21ga306vhxbnq40vfmd7hrambnzaij710iwkh1")))

(define-public crate-rust-enum-derive-0.3.2 (c (n "rust-enum-derive") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "027vz54zqfi26pqvyhh94pf3l03s6cygzk3lf4lar8qxxyj0i1c6")))

(define-public crate-rust-enum-derive-0.3.3 (c (n "rust-enum-derive") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1g20m949139m2b3gry6ymsvpb8x2v7v0nyklf6nmhqvmfapjpbx4")))

(define-public crate-rust-enum-derive-0.4.0 (c (n "rust-enum-derive") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "05mpmy1s9aqmhx3kqykwyr9yxsnv3rdkf3cv9wnhp3nl9cgx42ph")))

