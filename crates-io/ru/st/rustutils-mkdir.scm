(define-module (crates-io ru st rustutils-mkdir) #:use-module (crates-io))

(define-public crate-rustutils-mkdir-0.1.0 (c (n "rustutils-mkdir") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "1lzjr543jyim5zdgf43b8ia9x5l3wd0y73pwyp0b144rlv1b5c7v")))

