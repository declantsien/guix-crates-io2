(define-module (crates-io ru st rustviz-svg) #:use-module (crates-io))

(define-public crate-rustviz-svg-0.1.0 (c (n "rustviz-svg") (v "0.1.0") (d (list (d (n "handlebars") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qpq8y397f844p0biykyff10w2rg8i27j2ph43jm2p3j337ki4p5")))

