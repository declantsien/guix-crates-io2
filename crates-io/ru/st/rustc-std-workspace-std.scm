(define-module (crates-io ru st rustc-std-workspace-std) #:use-module (crates-io))

(define-public crate-rustc-std-workspace-std-1.0.0 (c (n "rustc-std-workspace-std") (v "1.0.0") (h "0zh7daiqhaq5bb1mbc4g1539a47yblzyivy4614d38a4p7cdrn0s")))

(define-public crate-rustc-std-workspace-std-1.0.1 (c (n "rustc-std-workspace-std") (v "1.0.1") (h "1vq4vaclamwhk0alf4f7wq3i9wxa993sxpmhy6qfaimy1ai7d9mb")))

