(define-module (crates-io ru st rust_hdl_lib_widgets) #:use-module (crates-io))

(define-public crate-rust_hdl_lib_widgets-0.44.0 (c (n "rust_hdl_lib_widgets") (v "0.44.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_hdl_lib_core") (r "^0.44.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1n3wrnh2ya988dg2rlqxcw5qlwjpniawbrsc07acjgyy8lw44byr")))

