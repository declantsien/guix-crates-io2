(define-module (crates-io ru st rustotp_cli) #:use-module (crates-io))

(define-public crate-rustotp_cli-0.1.0 (c (n "rustotp_cli") (v "0.1.0") (d (list (d (n "base32") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustotp") (r "^0.1.0") (d #t) (k 0)))) (h "15zlfmrs6dhjyj0a23c0xp4v2jimb7wjb6nc1qrpfyx9f1fddfz5")))

