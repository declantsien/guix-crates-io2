(define-module (crates-io ru st rustls-extra) #:use-module (crates-io))

(define-public crate-rustls-extra-0.14.0 (c (n "rustls-extra") (v "0.14.0") (d (list (d (n "ct-logs") (r "^0.5.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)) (d (n "rustls") (r "^0.15.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "webpki") (r "^0.19.1") (d #t) (k 0)))) (h "1lp9ijvg0gdjygzahjapwx7i1ifdk7lbn7kp6pgvh3wzsxg02dgk")))

(define-public crate-rustls-extra-0.15.0 (c (n "rustls-extra") (v "0.15.0") (d (list (d (n "ct-logs") (r "^0.5.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)) (d (n "rustls") (r "^0.15.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "webpki") (r "^0.19.1") (d #t) (k 0)))) (h "0y814yc0i81jqmlq7a00sqqgaswy0280z97afp4a1z8hqxljqxf5")))

