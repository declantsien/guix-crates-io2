(define-module (crates-io ru st rusty-slip) #:use-module (crates-io))

(define-public crate-rusty-slip-0.1.0 (c (n "rusty-slip") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serialport") (r "^4.0.1") (d #t) (k 2)))) (h "0n5wszy6s4x1pkvdgfm5bxmad4ijhb43bz9q5mqy89sp0hvd8whx")))

