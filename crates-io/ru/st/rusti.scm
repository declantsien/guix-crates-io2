(define-module (crates-io ru st rusti) #:use-module (crates-io))

(define-public crate-rusti-0.1.0 (c (n "rusti") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "1d9qm6z8jgl1w3rfb3q50x4r0givmci9ianirj5zdykmn7r8mqc5")))

(define-public crate-rusti-0.1.1 (c (n "rusti") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "0jx6jbnrsz5d814s871kpgaqaihidavnj8f4pnlr578sic0jjqxs")))

(define-public crate-rusti-0.1.2 (c (n "rusti") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "1k5ngxqdaij48173y4vgwi6lh33xkg6mi0x7nw0g3ww8lmgx7sbv")))

(define-public crate-rusti-0.1.3 (c (n "rusti") (v "0.1.3") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "1xnklbhqypk43snsqvd5frhpmyfxxxhdwj0hvbihdsdnibgq0wq1")))

(define-public crate-rusti-0.1.4 (c (n "rusti") (v "0.1.4") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "17mhx4zm3ipzn4913sg50v2ib4bv289600kg2w16c8xwj1xszy65")))

