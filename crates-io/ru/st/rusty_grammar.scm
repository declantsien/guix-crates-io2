(define-module (crates-io ru st rusty_grammar) #:use-module (crates-io))

(define-public crate-rusty_grammar-0.1.1 (c (n "rusty_grammar") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)))) (h "1kyabjcpf26ziavgwxyifjp11jp6rx4q8vc2np0yjnh3hfi7i41y")))

(define-public crate-rusty_grammar-0.1.2 (c (n "rusty_grammar") (v "0.1.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)))) (h "1yzgiagn6rxlpv6p6vrj6baf7g8idfrczsjwpk7wffwnaw3xbqak")))

