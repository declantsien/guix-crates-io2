(define-module (crates-io ru st rustfetch) #:use-module (crates-io))

(define-public crate-rustfetch-0.1.0 (c (n "rustfetch") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "os_info") (r "^3.0") (k 0)) (d (n "sys-info") (r "^0.8") (d #t) (k 0)))) (h "00fii0w9vib4ix58m2z2vgyw5vvwkbnpl3w8yk9q2xqql79pjizg")))

(define-public crate-rustfetch-0.1.1 (c (n "rustfetch") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "os_info") (r "^3.0") (k 0)) (d (n "systemstat") (r "^0.1.7") (d #t) (k 0)))) (h "093cwc7fvxkcm0yzxk8a1478cb80j8h58zlbgjisl3ijbcbwpcp8")))

(define-public crate-rustfetch-0.5.0 (c (n "rustfetch") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "os_info") (r "^3.2.0") (k 0)) (d (n "systemstat") (r "^0.1.10") (d #t) (k 0)))) (h "1v84idccyy001h9cfk5kk9pf9k40rhpg1vrar538mxawxsmi4c1z")))

