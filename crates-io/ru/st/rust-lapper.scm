(define-module (crates-io ru st rust-lapper) #:use-module (crates-io))

(define-public crate-rust-lapper-0.1.0 (c (n "rust-lapper") (v "0.1.0") (h "1shm182r5mwjyhhns3njpwkkkfrpgy0ayplv5cm7izh7v4mlr8nc")))

(define-public crate-rust-lapper-0.1.1 (c (n "rust-lapper") (v "0.1.1") (h "1wxi1hl9sj3wnyg7yhwkl6bc8mdy94wrc1bwsw5sv3d31lzpq2ad")))

(define-public crate-rust-lapper-0.1.2 (c (n "rust-lapper") (v "0.1.2") (h "1b2qzx3qpgw2c25zr77c7vk58q7ah5dv19ljyjj2m3gpvwx2jkbq")))

(define-public crate-rust-lapper-0.2.0 (c (n "rust-lapper") (v "0.2.0") (h "1mj3vnggy5q1l5nqgh8a0vkk0lp2j86ikkmp833zhyp99q1423q4")))

(define-public crate-rust-lapper-0.2.1 (c (n "rust-lapper") (v "0.2.1") (h "12w1bckb914vwi43jcji4ci079ya1pphnrnc7mmh7gr3kj4786qy")))

(define-public crate-rust-lapper-0.2.2 (c (n "rust-lapper") (v "0.2.2") (h "160yq0xkm98layxcb0mq9wgxflpv06bkg2ks92chwcqrj10wnhnb")))

(define-public crate-rust-lapper-0.3.0 (c (n "rust-lapper") (v "0.3.0") (h "05f1znjvbn9192g1dhr3znq39gxpy0ik9np51dgfhzqly7i8ixhz")))

(define-public crate-rust-lapper-0.3.1 (c (n "rust-lapper") (v "0.3.1") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0k1342cifylwhvzp71dvl0h088p5a5m4nzll9xmimdgi55ckigla")))

(define-public crate-rust-lapper-0.3.2 (c (n "rust-lapper") (v "0.3.2") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0amvcjhvbwvhgbjbyp92z765v1fjdklsgpi60cjfz7h2gqs8a9vn")))

(define-public crate-rust-lapper-0.3.3 (c (n "rust-lapper") (v "0.3.3") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1p5xa25mjwgwsz3cy0npvp5wwsi3nk1a1mxgd68kmx1nsqiv2gi0")))

(define-public crate-rust-lapper-0.3.4 (c (n "rust-lapper") (v "0.3.4") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0j6grkgcb4q2gv0pkqyyf7jwby7pwsd9ba4hxkysjlx64r1m8yws")))

(define-public crate-rust-lapper-0.3.5 (c (n "rust-lapper") (v "0.3.5") (d (list (d (n "bio") (r "^0.28.2") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nested_intervals") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "05znkhnsn2749mfdzaqbxy8w1iz4ijq9d1w6bg3ila0scim646ja")))

(define-public crate-rust-lapper-0.3.6 (c (n "rust-lapper") (v "0.3.6") (d (list (d (n "bio") (r "^0.28.2") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nested_intervals") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "01i35vji59g1i3bggs24w9a2ks9j9qs67skvp0l654i0a9j4d9fw")))

(define-public crate-rust-lapper-0.3.7 (c (n "rust-lapper") (v "0.3.7") (d (list (d (n "bio") (r "^0.28.2") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nested_intervals") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1jz42p4lfqz8lsg8nadz7qj0hjfj401c3mzdzlki6d0yy7ybllvf")))

(define-public crate-rust-lapper-0.3.8 (c (n "rust-lapper") (v "0.3.8") (d (list (d (n "bio") (r "^0.28.2") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nested_intervals") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1am36b0qwc3kailm4rlz5gcjiyfb2ib31z4la419jbkwfr7ay15k")))

(define-public crate-rust-lapper-0.3.9 (c (n "rust-lapper") (v "0.3.9") (d (list (d (n "bio") (r "^0.28.2") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nested_intervals") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0l1w94pjrg10ql1nvpffxcja5i3rxgb8xzn8q5bd6jy8l9x4a9zm")))

(define-public crate-rust-lapper-0.4.0 (c (n "rust-lapper") (v "0.4.0") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0nx89c06mgqhkrc4rmvyw74gm3qljc4zbllxmb8iwgdz52aax0ry")))

(define-public crate-rust-lapper-0.4.1 (c (n "rust-lapper") (v "0.4.1") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1kcc2hd5s0gz5nflwnv3xh8jk9wbwccihbzdrm4ffkkzcmrwrs89")))

(define-public crate-rust-lapper-0.4.2 (c (n "rust-lapper") (v "0.4.2") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1mjs85p39ii2yxsxiiy7r16fknqq4nkb9nz0ap10jwrgjwfm7zny")))

(define-public crate-rust-lapper-0.4.3 (c (n "rust-lapper") (v "0.4.3") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1n1x55r3nhzf0bm7an2cqnrgn1dy458j3jm32ixjrahmwrj612gx")))

(define-public crate-rust-lapper-0.4.4 (c (n "rust-lapper") (v "0.4.4") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "108gj6hjapyhfwy5g39ab7q1qvn2xz4z21q69wjbpfqppgnr4i4q")))

(define-public crate-rust-lapper-0.5.0 (c (n "rust-lapper") (v "0.5.0") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0qi0vsx1p4y17lqjp205rr4imnh7grkjsk99772p08amx9wq4737")))

(define-public crate-rust-lapper-0.5.1 (c (n "rust-lapper") (v "0.5.1") (d (list (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0b0na0w4ddaack6vc28yddbf1x5x16mkazk5injcjzfp7767bla5")))

(define-public crate-rust-lapper-1.0.0 (c (n "rust-lapper") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02mgp2ak683hvfbhslsf6rsc94lcnsms8bkbvspqpqn2r20cngnw") (f (quote (("with_serde" "serde/derive") ("default"))))))

(define-public crate-rust-lapper-1.0.1 (c (n "rust-lapper") (v "1.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i61d7ajjqn885q43h9kkv6cjqd46qndnnkhkpr6p44bi617znn0") (f (quote (("with_serde" "serde/derive") ("default"))))))

(define-public crate-rust-lapper-1.1.0 (c (n "rust-lapper") (v "1.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "cpu-time") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09zd4fzsxan8h2c0kg5d3sqs6jbvjm5r8smbvcqk105c47kxhhzf") (f (quote (("with_serde" "serde/derive") ("default"))))))

