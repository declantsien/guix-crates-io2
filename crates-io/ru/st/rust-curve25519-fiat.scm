(define-module (crates-io ru st rust-curve25519-fiat) #:use-module (crates-io))

(define-public crate-rust-curve25519-fiat-0.1.0 (c (n "rust-curve25519-fiat") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0yi59f7l4a61pwp18a9f8sys598g7l3aksaw2mxh9b1mkjdl8jpw") (y #t)))

(define-public crate-rust-curve25519-fiat-0.1.1 (c (n "rust-curve25519-fiat") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1md9w15zr5fyidg75781sak9gl8jxr47yjjxrk48yr9jkxyvqvw7") (y #t)))

(define-public crate-rust-curve25519-fiat-0.1.2 (c (n "rust-curve25519-fiat") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1l2vc6ss2imc95nqa5y71269axc8svw8kgkpglrmaa207yasv2fb") (y #t)))

(define-public crate-rust-curve25519-fiat-0.1.3 (c (n "rust-curve25519-fiat") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00j4dc3cfr9fk1rgc7wfhahc9d5pd7mj6l49cd963s7k8gkia3rx") (y #t)))

