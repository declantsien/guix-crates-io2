(define-module (crates-io ru st rust-extra) #:use-module (crates-io))

(define-public crate-rust-extra-0.0.1 (c (n "rust-extra") (v "0.0.1") (h "0280192kz9apll459y3634rp5546rr99lpy1w4mlwb3gavwh2yn3")))

(define-public crate-rust-extra-0.0.2 (c (n "rust-extra") (v "0.0.2") (h "1yggs6xxf5iyn980hmv99mgj90plkn7chzfammcq78a5wi7f1b0j")))

(define-public crate-rust-extra-0.0.3 (c (n "rust-extra") (v "0.0.3") (h "09n6c8qm90752c3p6h02yh1fz00kb15ysrk3l26n8ppzj33di59r")))

(define-public crate-rust-extra-0.0.7 (c (n "rust-extra") (v "0.0.7") (h "17whr18z161vgkm1bar701jgq1ql8w3lsdgvr63sd88wmi6vlihl")))

(define-public crate-rust-extra-0.0.8 (c (n "rust-extra") (v "0.0.8") (h "1wwygd2y2ixr34yh6nl500m6adjnzbqznz67jmx55irr4v8gz23v")))

(define-public crate-rust-extra-0.0.9 (c (n "rust-extra") (v "0.0.9") (h "100ggykmpk4yi15cig54shzpcax2ivfcwz78idgl0b2c19z5cdhp")))

(define-public crate-rust-extra-0.0.10 (c (n "rust-extra") (v "0.0.10") (h "00l1855vpycml96wn388jp9a1c9f0j9rcijcrdgxf1l56ziwjqyn")))

(define-public crate-rust-extra-0.0.11 (c (n "rust-extra") (v "0.0.11") (h "1pw01xy238wzdcgpvq6hyv174q9rxw6gssbhjyg27giflg2lzw52")))

(define-public crate-rust-extra-0.0.12 (c (n "rust-extra") (v "0.0.12") (h "06jh2bcmggrzzcpbwxxv6gvrvm9br282c9jwdc9jrpks90352xgi")))

(define-public crate-rust-extra-0.0.13 (c (n "rust-extra") (v "0.0.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s5qws6nsv5wvzv613jklbcf0czbywfmlakx785032w9gvh0af8z")))

(define-public crate-rust-extra-0.0.14 (c (n "rust-extra") (v "0.0.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nqvrlw9svj9kv4lidxxrkcw19z6zija96v9a7i7sih14kfnc32d")))

(define-public crate-rust-extra-0.0.15 (c (n "rust-extra") (v "0.0.15") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06iwpamilzxgbyqf3zbj3p63zcx38n5nnxgzp3g5pmzwvv9dbm9z")))

(define-public crate-rust-extra-0.0.16 (c (n "rust-extra") (v "0.0.16") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a3xcxw0s5irfszj1pggbsngv7fiwsa0gd6ws7cw6aq0bl75l9w0")))

(define-public crate-rust-extra-0.0.17 (c (n "rust-extra") (v "0.0.17") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02xzhj38flhxkyqvph0xkni3fc56c9v9b2bawxpzij1j5fwihz0i")))

