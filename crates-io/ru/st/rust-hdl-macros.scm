(define-module (crates-io ru st rust-hdl-macros) #:use-module (crates-io))

(define-public crate-rust-hdl-macros-0.1.0 (c (n "rust-hdl-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "19fraqih3fbr2g155r2kh7jxglwrd0kl2h717qs3i3csk31iv161")))

(define-public crate-rust-hdl-macros-0.2.0 (c (n "rust-hdl-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0nf1qjkijsmvrc33hdivc8gsyjiajsz9mn0nqkhhm1ghn142i28k")))

(define-public crate-rust-hdl-macros-0.3.0 (c (n "rust-hdl-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1iq1ngvprn2slldr72zzwba8j418wz0smgh6mr7rln4xi03rn1qm")))

(define-public crate-rust-hdl-macros-0.4.0 (c (n "rust-hdl-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0f798l7gsqbsa1mcbkj2g5bz9r1dg8alz1kcn38fhxfar522sy53")))

(define-public crate-rust-hdl-macros-0.5.0 (c (n "rust-hdl-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0x99n64c7qkhl0yr9359hxyk5y67f6l8ady9ag0f425w3pgz3c90")))

(define-public crate-rust-hdl-macros-0.6.0 (c (n "rust-hdl-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "06mbfprp16d1rq8lpwvckvnrmmv9qwwvjpzgv3653bxggh1h5657")))

(define-public crate-rust-hdl-macros-0.7.0 (c (n "rust-hdl-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0jx8rnjvypkhczv5d4anrqyxh8gxmklwxwwdn7zvhsm0nf8kcz13")))

(define-public crate-rust-hdl-macros-0.8.0 (c (n "rust-hdl-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0n1miv55fd7kbk73x4ww25qk7kzjz0aavldg8s29k81nlwszyik3")))

(define-public crate-rust-hdl-macros-0.10.0 (c (n "rust-hdl-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0dx7yv2qw8clg0d97q1pxp9qv8r2lp8vdfbzd8gqkxdh3kp9fpbs")))

(define-public crate-rust-hdl-macros-0.10.1 (c (n "rust-hdl-macros") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1dq98cybjr0i1xpjyyhknsvhwlwq3wlmq2s8i9b1wjvxp6na39yn")))

(define-public crate-rust-hdl-macros-0.11.0 (c (n "rust-hdl-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "149c9mb7dp20326kdgrrykaaj0ra8b3ysmizwzdn9f1p2w8ypx20")))

(define-public crate-rust-hdl-macros-0.12.0 (c (n "rust-hdl-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "17nv6vazqlr8xzkk609z46lc3was933i46rzdq4jnhqqfr8kn5x4")))

(define-public crate-rust-hdl-macros-0.13.0 (c (n "rust-hdl-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1703lymq43rx8qnd57jdiyx2hn7cmpa48iqk6a8mawhmxbz6ffid")))

(define-public crate-rust-hdl-macros-0.14.0 (c (n "rust-hdl-macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1vsxhv7pr43m57iy8ivw6nb86fbw0v8sc17xxjllpxxiwvkdidjm")))

(define-public crate-rust-hdl-macros-0.15.0 (c (n "rust-hdl-macros") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1gpc9k4c4ll1ldrfxh9fbpl7kxqlgxa0mk08ap50bwffvjd65irw")))

(define-public crate-rust-hdl-macros-0.16.0 (c (n "rust-hdl-macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0xldd6wp4syqb4x7xkkla6l57hb22g14r0p5gw85rml4ajak0r75")))

(define-public crate-rust-hdl-macros-0.17.0 (c (n "rust-hdl-macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1b076zwdcpbfmxyazc00vlda0fi0fkwrrga6npk74mjhf4niqbf4")))

(define-public crate-rust-hdl-macros-0.18.0 (c (n "rust-hdl-macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0cc4bpjgacklq3m2fi68mlq7var2km6xnaajbm738mfwszd3wsyn")))

(define-public crate-rust-hdl-macros-0.18.1 (c (n "rust-hdl-macros") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "193rpgiaq4h9lvb1mys1w76dhkqgc0k7jzr7wa1s0y7f0pwjplw9")))

(define-public crate-rust-hdl-macros-0.18.2 (c (n "rust-hdl-macros") (v "0.18.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0a9jbk5km49lvirvbskszbb3b6v5cycmz3c6is75d3c4037fw4mi")))

(define-public crate-rust-hdl-macros-0.19.0 (c (n "rust-hdl-macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0yidzhw436kfz1wy2yp1c9dw85fwxhl8illgsxhi23wz982641cz")))

(define-public crate-rust-hdl-macros-0.20.0 (c (n "rust-hdl-macros") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1jad3ki3rh6vh7hss3zb23pfn6cq6lzgzxp1ji3vzp8xk11smydl")))

(define-public crate-rust-hdl-macros-0.20.1 (c (n "rust-hdl-macros") (v "0.20.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0kblivy18vb9lla1in6zgvp14npqp501i9f6dk13w8l6jag6z3nr")))

(define-public crate-rust-hdl-macros-0.21.0 (c (n "rust-hdl-macros") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1skdhi4mpqa35r3x5jnwcqfxycdxl8xidassazr1z8q5bbyq6x5v")))

(define-public crate-rust-hdl-macros-0.21.2 (c (n "rust-hdl-macros") (v "0.21.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1kry8ab2991y83b4nb2mdxngdxwjvssci92qhdazr24slg9sr2l3")))

(define-public crate-rust-hdl-macros-0.21.3 (c (n "rust-hdl-macros") (v "0.21.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "17740iq3wf4wz7gz5cigg4l5k35fp6kj737sfqc65slnw610zh8h")))

(define-public crate-rust-hdl-macros-0.21.4 (c (n "rust-hdl-macros") (v "0.21.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0h4064z7i1q67fvz7g95zyl2xkglc6zh7mld7zksrz4z11lm7rq1")))

(define-public crate-rust-hdl-macros-0.21.5 (c (n "rust-hdl-macros") (v "0.21.5") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1hmd6g6f69609czaj40h8r8g6k2s0clx1mpqwifdaxnqwimv4jw7")))

(define-public crate-rust-hdl-macros-0.21.6 (c (n "rust-hdl-macros") (v "0.21.6") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1cyhrk0phkpyfyd0mhiyw9390g0xijjdaks5yhx09ka97iaq264g")))

(define-public crate-rust-hdl-macros-0.22.0 (c (n "rust-hdl-macros") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0znxfklhwhs1lc8vdhjinf0d441x4h4g856jw293556z51a0rfcs")))

(define-public crate-rust-hdl-macros-0.23.0 (c (n "rust-hdl-macros") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1z9f19spicwp70srp5draair7ldg4rlqvzbs0lfnnf6qiifbd5x2")))

(define-public crate-rust-hdl-macros-0.24.0 (c (n "rust-hdl-macros") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1ar9h7rdyh8nd5z1qn8js73njg5pii7ifg84fdvl5ac9jqlakpq3")))

(define-public crate-rust-hdl-macros-0.24.1 (c (n "rust-hdl-macros") (v "0.24.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1wy0wladd2wp8kxl6mzvkyqb72c6gjyivf98qgkaqy18sajgmhzr")))

(define-public crate-rust-hdl-macros-0.42.0 (c (n "rust-hdl-macros") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1rclvpil92817dgmdjz49fz6dx15bxilv5f71qwpsgahm2l3hy0p")))

(define-public crate-rust-hdl-macros-0.43.0 (c (n "rust-hdl-macros") (v "0.43.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0pa4y10mmdd4x7kc7475d2pyrgpzxh1h9n9gn5kpjkhkw766i9mq")))

(define-public crate-rust-hdl-macros-0.44.0 (c (n "rust-hdl-macros") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "01qsjssxxj6xkznx7qdn6mippjqva83sr9jmfgxsxpqc10dcr3z3")))

(define-public crate-rust-hdl-macros-0.44.2 (c (n "rust-hdl-macros") (v "0.44.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0377jh7ingl6srkrnj7zrkdwjh3s9vksidfb6xrd1p9zgqy59z4x")))

(define-public crate-rust-hdl-macros-0.45.0 (c (n "rust-hdl-macros") (v "0.45.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1a1p5qn5hcpcq7a3zdavr0dic76imbqzbzm1l5rfs414n5dyfmx1")))

(define-public crate-rust-hdl-macros-0.45.1 (c (n "rust-hdl-macros") (v "0.45.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0vmig9528hin40jrxj3m1vqzw30qn3j6m9xdzaj1lh0pwmh5disi")))

(define-public crate-rust-hdl-macros-0.46.0 (c (n "rust-hdl-macros") (v "0.46.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "08di04vvy9zqwf0qfxbrlhpqacnq1wpfxhvd2w2ycdplwy7qk49n")))

