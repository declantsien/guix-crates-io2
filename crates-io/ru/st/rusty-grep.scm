(define-module (crates-io ru st rusty-grep) #:use-module (crates-io))

(define-public crate-rusty-grep-0.1.0 (c (n "rusty-grep") (v "0.1.0") (d (list (d (n "ferris-says") (r "^0.2") (d #t) (k 0)))) (h "1fhy5987kch9jp03p9yb8gv0ry8a7qdrryi0dw95caa9h67n9v8c")))

(define-public crate-rusty-grep-0.1.1 (c (n "rusty-grep") (v "0.1.1") (h "0mpii6gpmgmny3r2ng22s52kqdm9aa6wyvi2qlm21j61nwy80jbx")))

