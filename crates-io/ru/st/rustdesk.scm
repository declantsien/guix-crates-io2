(define-module (crates-io ru st rustdesk) #:use-module (crates-io))

(define-public crate-rustdesk-0.1.0 (c (n "rustdesk") (v "0.1.0") (h "0nkcvf983dfi3pjahbqab86ibw2g4i8hipw0lgxx55gv5mg6kh0x")))

(define-public crate-rustdesk-0.1.1 (c (n "rustdesk") (v "0.1.1") (d (list (d (n "penrose") (r "^0.2") (d #t) (k 0)))) (h "0ksxnaqqkjxdcq4ahh5fipa74bnqrkg7ziqcn3crd2mawcg700k7")))

