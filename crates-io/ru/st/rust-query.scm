(define-module (crates-io ru st rust-query) #:use-module (crates-io))

(define-public crate-rust-query-0.1.0 (c (n "rust-query") (v "0.1.0") (d (list (d (n "elsa") (r "^1.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (f (quote ("modern_sqlite" "buildtime_bindgen"))) (d #t) (k 0)) (d (n "sea-query") (r "^0.30") (d #t) (k 0)))) (h "0bj1aczdzx4x6h409q74bc04d49rr79i77gp9f6ygpw058ysvq02")))

(define-public crate-rust-query-0.1.1 (c (n "rust-query") (v "0.1.1") (d (list (d (n "elsa") (r "^1.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("modern_sqlite"))) (d #t) (k 0)) (d (n "sea-query") (r "^0.30") (d #t) (k 0)))) (h "1h9mxvnjrxpg1x5clwsx8b05dwb27s1g1glym42kclpvhw2wdga8")))

(define-public crate-rust-query-0.1.2 (c (n "rust-query") (v "0.1.2") (d (list (d (n "elsa") (r "^1.10.0") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("modern_sqlite"))) (d #t) (k 0)) (d (n "sea-query") (r "^0.30") (d #t) (k 0)))) (h "1kmgmw2l6h1r1r05rq33hwph4hpwd1hmv8qwzqa96l9xqr2cgsaj")))

