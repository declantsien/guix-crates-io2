(define-module (crates-io ru st rust-health-endpoint) #:use-module (crates-io))

(define-public crate-rust-health-endpoint-0.1.0 (c (n "rust-health-endpoint") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "196wzpkk70zdzzdrlkk8pdz3666xf0hs9b09m2f9mqh1pgan1q69") (y #t)))

(define-public crate-rust-health-endpoint-0.1.1 (c (n "rust-health-endpoint") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z9c7l3n1a3yqzfj2254xwfwsh6ys48ic4x74z9rdznpzghami64") (y #t)))

(define-public crate-rust-health-endpoint-0.1.2 (c (n "rust-health-endpoint") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1913dsm3kqyixi7wnzf8ai7q20xdjz3ia9r7211pz79jayj30di9")))

