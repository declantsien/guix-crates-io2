(define-module (crates-io ru st rusty_german_entity_macro) #:use-module (crates-io))

(define-public crate-rusty_german_entity_macro-0.1.0 (c (n "rusty_german_entity_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rusty_german_types") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1b0vvndvh4sjg1vfmd9jbbqj7r2fnb8b14nlicfjk7rq0kfcnp5h")))

(define-public crate-rusty_german_entity_macro-0.1.1 (c (n "rusty_german_entity_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rusty_german_types") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "161k3yls1nqrdywbqn8qyg4bxcspnsk1kqa0yjr2rlbfvc2d0h6p")))

