(define-module (crates-io ru st rustypot) #:use-module (crates-io))

(define-public crate-rustypot-0.1.0 (c (n "rustypot") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1da0w8xr9bbp5gws96pm1r0473w2sz6s06mgwsr7zx1wjbjhcv1l")))

(define-public crate-rustypot-0.2.0 (c (n "rustypot") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "17g470jzni3iiw5nqbpgmqirq6yxasazsqf0npq91vd2q0qbsjmv")))

(define-public crate-rustypot-0.3.0 (c (n "rustypot") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1vwnghzj3v47ap3r87gq1cq8g2fnxrn97s8ggkm12a2p6np9acxa")))

(define-public crate-rustypot-0.3.1 (c (n "rustypot") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1a1ni7yiw7zmpx0hfmraqivmzijhvahayfwj0diq3kdf2rz7bgv3")))

(define-public crate-rustypot-0.4.0 (c (n "rustypot") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1qd5q120slq60jr18fgzljmw7i3i8as76kqmplrp5bayw9l1v00m")))

(define-public crate-rustypot-0.4.1 (c (n "rustypot") (v "0.4.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1y2374ni65xhq1r069wdvh3rh3d7875fcady12sx9q7bbwq1sild")))

(define-public crate-rustypot-0.4.2 (c (n "rustypot") (v "0.4.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "paste") (r "^1.0.10") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "0jj1jz1c1ib4ck71yyrhisf43m4lcc7a57h1mhgzf0zhfjvrc1x1")))

