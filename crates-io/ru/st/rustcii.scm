(define-module (crates-io ru st rustcii) #:use-module (crates-io))

(define-public crate-rustcii-1.0.0 (c (n "rustcii") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("cargo"))) (d #t) (k 0)))) (h "089cmra9bmdp32zjrh3cfyd479v2kkpr5ll7pgzypwz1hklv90dm")))

(define-public crate-rustcii-1.0.1 (c (n "rustcii") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("cargo"))) (d #t) (k 0)))) (h "01c719jvxnz3zhzs1zmrxk6siva5m62bg0aq8qab8blpizmh3cwx")))

(define-public crate-rustcii-1.0.2 (c (n "rustcii") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("cargo"))) (d #t) (k 0)))) (h "1x2c7dxnh30dha1i20wdhxqgjaw5lrg3bpnlrzcb3dc3qawk165j")))

