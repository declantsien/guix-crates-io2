(define-module (crates-io ru st rustic-zen) #:use-module (crates-io))

(define-public crate-rustic-zen-0.0.1 (c (n "rustic-zen") (v "0.0.1") (d (list (d (n "png") (r "^0.13.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1y6mlhxzprmkh02bqfl6r9gsvjp9br6fgbicdscrv9yrb1y5d3xx")))

(define-public crate-rustic-zen-0.0.2 (c (n "rustic-zen") (v "0.0.2") (d (list (d (n "png") (r "^0.13.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "129mkhsq86wyhpm2san8zrh2fs9p2f93lz8qirrf4izv7fig70nl")))

(define-public crate-rustic-zen-0.6.0 (c (n "rustic-zen") (v "0.6.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "png") (r "^0.13.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "1psacmxc3hfb7zrg0arx154x2yizaa3mwgacwh00y0s21a7c9ryn") (y #t)))

(define-public crate-rustic-zen-0.2.0 (c (n "rustic-zen") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "png") (r "^0.13.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)))) (h "0djlpgk8yl0y77j1ln18mb6mn172lhf43d3i47hsq460qzdwvgaa")))

(define-public crate-rustic-zen-0.3.0 (c (n "rustic-zen") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "png") (r "^0.13.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "vulkano") (r "^0.34.0") (o #t) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.34.0") (o #t) (d #t) (k 0)))) (h "0vdsd9yq1a5zvh8418gzzz9rz0x8hbq0sy57h4frim06fw1r71fg") (f (quote (("gpu" "crossbeam" "vulkano" "vulkano-shaders") ("default" "gpu"))))))

