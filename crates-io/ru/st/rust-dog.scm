(define-module (crates-io ru st rust-dog) #:use-module (crates-io))

(define-public crate-rust-dog-0.3.0 (c (n "rust-dog") (v "0.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "config_struct") (r "^0.5.0") (f (quote ("toml-parsing" "yaml-parsing"))) (d #t) (k 1)) (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "terminal_size") (r "^0.1.15") (d #t) (k 0)))) (h "0fmy5vawgscynbr6fzxbn2ipgqqvhl0xrbd4mykryk4xawjxckxg") (y #t)))

