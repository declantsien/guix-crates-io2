(define-module (crates-io ru st rust-pigpio) #:use-module (crates-io))

(define-public crate-rust-pigpio-0.1.0 (c (n "rust-pigpio") (v "0.1.0") (h "1a71pfh4cpr23ihbd90g1cja656v728q4hyhnkxf8jc9vmiby1y2")))

(define-public crate-rust-pigpio-0.1.1 (c (n "rust-pigpio") (v "0.1.1") (h "16pcrzw8q80b8m037iczvdlxin148v1h2f1mxz7s8hpn70kfqdc2")))

(define-public crate-rust-pigpio-0.1.2 (c (n "rust-pigpio") (v "0.1.2") (h "0yhmac6srrzlnxjwhpmfwz3napd35g82zjlxqhjzy4c42nw9k4yk")))

(define-public crate-rust-pigpio-0.1.3 (c (n "rust-pigpio") (v "0.1.3") (h "122pnl1hlzxy6s23z4ni8h3xy3m0mrhmkx4dic39vv62l19dp717")))

(define-public crate-rust-pigpio-0.2.0 (c (n "rust-pigpio") (v "0.2.0") (h "1y3pkiqs2afh6k4l91rcbkwm275mybg6r2kyryypnja58izcyhjy")))

