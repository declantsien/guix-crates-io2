(define-module (crates-io ru st rustbee) #:use-module (crates-io))

(define-public crate-rustbee-0.1.0 (c (n "rustbee") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serialport") (r "^3.3") (d #t) (k 0)))) (h "0c095zsjay8am1d2khrwjp1a3xyk1bgqvr2gl4d6ynw5jd57g8jg")))

(define-public crate-rustbee-0.1.1 (c (n "rustbee") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serialport") (r "^3.3") (f (quote ("libudev"))) (d #t) (k 0)))) (h "1mpyz54y0wh9q6l8wrm09hilixim9dqhw8bi54g8l3r6b9zj19cv")))

