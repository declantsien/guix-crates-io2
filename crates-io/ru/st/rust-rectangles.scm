(define-module (crates-io ru st rust-rectangles) #:use-module (crates-io))

(define-public crate-rust-rectangles-0.1.0 (c (n "rust-rectangles") (v "0.1.0") (h "1vcj92322a9s5k3zqfgi9cl80rxl8kwxsynyj4rkkdbh188y3ryq")))

(define-public crate-rust-rectangles-0.1.1 (c (n "rust-rectangles") (v "0.1.1") (h "1f2rjqzlsp2g29s8d4ax5cwsmq9487ln175dc0x68n1xz92hzjr6")))

