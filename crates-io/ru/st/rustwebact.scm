(define-module (crates-io ru st rustwebact) #:use-module (crates-io))

(define-public crate-rustwebact-0.0.1 (c (n "rustwebact") (v "0.0.1") (h "1qwiqf70xisqlf3f3m93r3a282yr5kc1ypddimc2532rxagsmb0b") (y #t)))

(define-public crate-rustwebact-0.1.0 (c (n "rustwebact") (v "0.1.0") (h "0pww3l6flsnc7waw9cm4lvvj1mqm5hvgl3ky8y26jjah78cacxsw") (y #t)))

(define-public crate-rustwebact-0.1.1 (c (n "rustwebact") (v "0.1.1") (h "1r6dyjn2axh7srxn7a2pa8idbaq8nfgarvj8afkcsraax5wx1ww2") (y #t)))

(define-public crate-rustwebact-0.1.2 (c (n "rustwebact") (v "0.1.2") (h "0jgx6dkr4b7iq41ax7sr8y9bfw10ar0ig4rfp9sy2pf5w2jfjj69") (y #t)))

(define-public crate-rustwebact-0.1.3 (c (n "rustwebact") (v "0.1.3") (d (list (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)))) (h "0664g00jfl5l7xigrlm7bqwlbykf7fwpj4lrd2d9vycfswwwv09g") (y #t)))

(define-public crate-rustwebact-0.1.4 (c (n "rustwebact") (v "0.1.4") (d (list (d (n "jsmx") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)))) (h "1wxaybssccd6qx2dnc417l8frz8ixvvpcw77kj170d4rqqm5cj6i") (y #t)))

(define-public crate-rustwebact-0.1.5 (c (n "rustwebact") (v "0.1.5") (d (list (d (n "jsmx") (r "^0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)))) (h "14r2mccljbv5zq02c6b24gm2k630gbcrbc7qv1bcmn81wdwid2q7") (y #t)))

(define-public crate-rustwebact-0.2.1 (c (n "rustwebact") (v "0.2.1") (d (list (d (n "jsmx") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "0hf193iz4hk3rydmh6wvbm9bnqb7c3i06ghaplbyafnrv5iva1xc") (y #t)))

(define-public crate-rustwebact-0.2.2 (c (n "rustwebact") (v "0.2.2") (d (list (d (n "jsmx") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "0hzx9hzvnin33cn6fj9qx21y6bszak52k9zr539yq8v83jx4yld1") (y #t)))

(define-public crate-rustwebact-0.2.3 (c (n "rustwebact") (v "0.2.3") (d (list (d (n "jsmx") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "1f1zszb84cn08gxg4i2hcn1i75b1jj1vr6ja10z7f45c7y9wj990") (y #t)))

(define-public crate-rustwebact-0.2.4 (c (n "rustwebact") (v "0.2.4") (d (list (d (n "jsmx") (r "^0.3.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.1.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "0qv58nhyjzzf0jlcr3hqz682vwylqh4pk378hn95c6zpfirk29c3") (y #t)))

(define-public crate-rustwebact-0.2.5 (c (n "rustwebact") (v "0.2.5") (d (list (d (n "jsmx") (r "^0.3.1") (d #t) (k 0)) (d (n "rdxl") (r "^0.1.18") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.45") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.22") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "04nmv2bykhrgydfla382bsim9fyddawp3zvl2m3vsk0yyqs3bfx2") (y #t)))

