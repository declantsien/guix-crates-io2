(define-module (crates-io ru st rust-fundamentals-andrea-restlelay) #:use-module (crates-io))

(define-public crate-rust-fundamentals-andrea-restlelay-0.1.0 (c (n "rust-fundamentals-andrea-restlelay") (v "0.1.0") (d (list (d (n "opentelemetry") (r "^0.21.0") (f (quote ("metrics" "trace"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1gp002cyxgg4b50wvn3d4i2ckj5izlc7jjrjgb0vx0mxqkjx9rq0") (y #t)))

(define-public crate-rust-fundamentals-andrea-restlelay-0.1.1 (c (n "rust-fundamentals-andrea-restlelay") (v "0.1.1") (d (list (d (n "opentelemetry") (r "^0.21.0") (f (quote ("metrics" "trace"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1nivsd4hslnkwq0a1plka1vwb7hz45g31azdy9a9fdkhmjpsf18r") (y #t)))

