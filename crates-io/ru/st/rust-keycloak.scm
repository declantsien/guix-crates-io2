(define-module (crates-io ru st rust-keycloak) #:use-module (crates-io))

(define-public crate-rust-keycloak-0.0.1 (c (n "rust-keycloak") (v "0.0.1") (d (list (d (n "jsonwebtoken") (r "^5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "0xhjncx7xdqi7g56f5zdxsb7m8h5n2ny8n4dg3bp1qragbcsxl25")))

(define-public crate-rust-keycloak-0.0.2 (c (n "rust-keycloak") (v "0.0.2") (d (list (d (n "jsonwebtoken") (r "^5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "1byb2ci7brv20a3b839sl43rxpg41jbdzckn68f90q0snhwb6b6k")))

(define-public crate-rust-keycloak-0.0.3 (c (n "rust-keycloak") (v "0.0.3") (d (list (d (n "jsonwebtoken") (r "^6.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "1ffxbndjsfjwd5mkhwk76g0rz9zagmim81ihlh491wkvxbxq53yp")))

(define-public crate-rust-keycloak-0.0.6 (c (n "rust-keycloak") (v "0.0.6") (d (list (d (n "jsonwebtoken") (r "^7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cx39rg50llxc6k26b39kxq08y7f8iw9hbys77ilz23bv9dm3830")))

(define-public crate-rust-keycloak-0.0.7 (c (n "rust-keycloak") (v "0.0.7") (d (list (d (n "jsonwebtoken") (r "^7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ah4cbv5hajp6r8c1c2v5rgwkxfgn4mh45z88vv8qi8j45w3f79m")))

