(define-module (crates-io ru st rusty_pipe) #:use-module (crates-io))

(define-public crate-rusty_pipe-0.1.0 (c (n "rusty_pipe") (v "0.1.0") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "01ii9g32yfsi7kl63n5x8jyqb5ckmj9qwzfvygn9hhbmc0b7h0hq")))

(define-public crate-rusty_pipe-0.1.1 (c (n "rusty_pipe") (v "0.1.1") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1l9x17qzik5h7v8aiazzp6dsfrqcvybmvk3ll5xga0mrvigv7p3a")))

(define-public crate-rusty_pipe-0.1.2 (c (n "rusty_pipe") (v "0.1.2") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0zb3b56dz2hcirfav8lq88jw79ny23g3wgaw8hhhjw9avxxfx93w")))

(define-public crate-rusty_pipe-0.1.3 (c (n "rusty_pipe") (v "0.1.3") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1qyl63lmx263id0zpf5blqgzxn4xbl2p1rq517b7nmmm0hhp657d")))

(define-public crate-rusty_pipe-0.1.4 (c (n "rusty_pipe") (v "0.1.4") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1jadpwn9sf25halky5nj32npv5c1bma7rk892linwigwcxlk93v0")))

(define-public crate-rusty_pipe-0.1.5 (c (n "rusty_pipe") (v "0.1.5") (d (list (d (n "iron") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "03ax09n177bq0s1ffc9s1q49ql9ak0h8ksd6bnlj0w9gnpk5239m")))

