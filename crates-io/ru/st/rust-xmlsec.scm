(define-module (crates-io ru st rust-xmlsec) #:use-module (crates-io))

(define-public crate-rust-xmlsec-1.0.0 (c (n "rust-xmlsec") (v "1.0.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)) (d (n "xml_serde") (r "^1") (d #t) (k 0)))) (h "1d32z6kikrnlvdgsdp97sjk3g2fg3nk2b3jbjv3f6162hfb60ji7")))

