(define-module (crates-io ru st rust-crypto-wasm) #:use-module (crates-io))

(define-public crate-rust-crypto-wasm-0.2.36 (c (n "rust-crypto-wasm") (v "0.2.36") (h "03safghgpxkfhsc0vlwxar2b3c0ncqk50lianvggkkrh75rnnwpa")))

(define-public crate-rust-crypto-wasm-0.3.0 (c (n "rust-crypto-wasm") (v "0.3.0") (h "1m3gjs0yplxabdmy2wfgxg84r7nzr7gd38rd0dqp1imp5q0lf5bb")))

(define-public crate-rust-crypto-wasm-0.3.1 (c (n "rust-crypto-wasm") (v "0.3.1") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "hex") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1v5n3kfhlyy23298fr4h8mqjxyr6vl4yknwxm6sgy3lspkni3kwx") (f (quote (("with-bench"))))))

