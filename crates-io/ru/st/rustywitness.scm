(define-module (crates-io ru st rustywitness) #:use-module (crates-io))

(define-public crate-rustywitness-0.1.0 (c (n "rustywitness") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(windows)") (k 0)))) (h "1s8fj1hp0gkg7ag3n0ylzdw3kv9a89z9q38lsnwg0pj4bf4gapcg")))

