(define-module (crates-io ru st rustwt) #:use-module (crates-io))

(define-public crate-rustwt-1.0.0 (c (n "rustwt") (v "1.0.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "openssl") (r "~0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "~0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "01jfbirs21z1qqyah203r05izxy0y05wwkvxvxqcgsc7ckbwfv23")))

(define-public crate-rustwt-1.0.1 (c (n "rustwt") (v "1.0.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "openssl") (r "~0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "~0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1xv3kfw2c5sivs1rbmrg1n06gnvpw276v10al30p8mm6ivjdgwcm")))

