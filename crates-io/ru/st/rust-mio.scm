(define-module (crates-io ru st rust-mio) #:use-module (crates-io))

(define-public crate-rust-mio-0.1.0 (c (n "rust-mio") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "run_script") (r "^0.6.3") (d #t) (k 1)))) (h "0qz57xvghr6wyd6rb11ra7wf9w14rrhkahlzavmrqw7pvlc41914")))

(define-public crate-rust-mio-0.2.0 (c (n "rust-mio") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "run_script") (r "^0.6.3") (d #t) (k 1)))) (h "0yadcwp0azj30snzqhzqmwv37z7fi2ah7n40mi3kyd832w7rvcad")))

