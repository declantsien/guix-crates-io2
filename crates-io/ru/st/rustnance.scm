(define-module (crates-io ru st rustnance) #:use-module (crates-io))

(define-public crate-rustnance-0.1.0 (c (n "rustnance") (v "0.1.0") (h "1jk3di854n86yggplagcjxy6qp7b3i1ragf3qa2j4886mi0wwrpp")))

(define-public crate-rustnance-0.2.0 (c (n "rustnance") (v "0.2.0") (h "0xr9zf6k4g9b3kcqkk5ml2sbk4vbrsz2rqdwibszinjnbfr2n485")))

(define-public crate-rustnance-0.2.1 (c (n "rustnance") (v "0.2.1") (h "1rl0xy1z6bpacm6lx2063cm4vdirjslb17xy650hq161pvzw64lv")))

(define-public crate-rustnance-0.2.2 (c (n "rustnance") (v "0.2.2") (h "13lkyd6pal6yw9gbl6r8snm68qg68axhmcrc96rhx5617lchmsha")))

