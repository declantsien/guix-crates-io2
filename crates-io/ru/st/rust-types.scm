(define-module (crates-io ru st rust-types) #:use-module (crates-io))

(define-public crate-rust-types-0.1.0 (c (n "rust-types") (v "0.1.0") (d (list (d (n "automod") (r "^1.0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11.2") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.2") (d #t) (k 0)) (d (n "tonic") (r "^0.8.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.2") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0lmdvi3qp1hycqx44cbpnkrif5j5wsg1cq8d3lfakv9bpy9afnjf")))

