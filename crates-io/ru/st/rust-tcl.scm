(define-module (crates-io ru st rust-tcl) #:use-module (crates-io))

(define-public crate-rust-tcl-0.1.0 (c (n "rust-tcl") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "rust-tcl-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1x8ink7sv23b8jzk8b4yb6fp9mc09vr5d1dmy9l9flb6r71qn37z") (f (quote (("use-pkgconfig" "rust-tcl-sys/use-pkgconfig") ("default"))))))

(define-public crate-rust-tcl-0.1.1 (c (n "rust-tcl") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "rust-tcl-sys") (r "^0.1.0") (d #t) (k 0)))) (h "13zwqczgv5aishyy852sjnny4cq3gwnb63fw9nbg6cw89br1m85q") (f (quote (("use-pkgconfig" "rust-tcl-sys/use-pkgconfig") ("default"))))))

(define-public crate-rust-tcl-0.2.0 (c (n "rust-tcl") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "rust-tcl-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0vpnkdj4rwri73nc2rmv5f9xkrh7q7ikj8svlrzw8kcalapggdl8") (f (quote (("use-pkgconfig" "rust-tcl-sys/use-pkgconfig") ("default"))))))

(define-public crate-rust-tcl-0.3.0 (c (n "rust-tcl") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "rust-tcl-sys") (r "^0.2.0") (d #t) (k 0)))) (h "05wvgwba2m20afm2dh63b5z2l34x15xfp22grla1nnfcvxirwj4n") (f (quote (("use-pkgconfig" "rust-tcl-sys/use-pkgconfig") ("default"))))))

