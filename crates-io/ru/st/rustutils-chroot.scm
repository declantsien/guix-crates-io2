(define-module (crates-io ru st rustutils-chroot) #:use-module (crates-io))

(define-public crate-rustutils-chroot-0.1.0 (c (n "rustutils-chroot") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "19jjmcs8gx47fvkgl0s10qdpns8gz0hyzzpjmffypg54nrml3gvs")))

