(define-module (crates-io ru st rust-xfinal-macro) #:use-module (crates-io))

(define-public crate-rust-xfinal-macro-0.1.0 (c (n "rust-xfinal-macro") (v "0.1.0") (h "1n6swhmpli0l527y4cvf6a5pzm93x37dygwnndlgisjj3g569yq1")))

(define-public crate-rust-xfinal-macro-0.1.2 (c (n "rust-xfinal-macro") (v "0.1.2") (h "17b5jm89421x843i44sik6yqji1dcxlmrb71ka2mqb9m5wicxkb9")))

