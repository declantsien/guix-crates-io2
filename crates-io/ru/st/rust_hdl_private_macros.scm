(define-module (crates-io ru st rust_hdl_private_macros) #:use-module (crates-io))

(define-public crate-rust_hdl_private_macros-0.44.0 (c (n "rust_hdl_private_macros") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1qkx5ivamz8dxlywlvxi4r2a5j4zgf6gphhzyvjsqams27q8rsaj")))

