(define-module (crates-io ru st rusty-panther) #:use-module (crates-io))

(define-public crate-rusty-panther-0.0.0 (c (n "rusty-panther") (v "0.0.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0xwrck6zxvkls2bpjg1rxxs9hv249hsicn7q2rdxy6b2wainp88h")))

(define-public crate-rusty-panther-0.1.0 (c (n "rusty-panther") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)))) (h "1sj6xj8f0zw0xbmf62fix67bq7z732v55b9fzqn0amfvmnbw1xac")))

