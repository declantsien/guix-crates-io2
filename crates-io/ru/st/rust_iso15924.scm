(define-module (crates-io ru st rust_iso15924) #:use-module (crates-io))

(define-public crate-rust_iso15924-0.0.1 (c (n "rust_iso15924") (v "0.0.1") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1khziji6mjxf39xyhxzica5dm0dzlr9i4nnp7zdhy4vz04cv9nmx")))

(define-public crate-rust_iso15924-0.0.2 (c (n "rust_iso15924") (v "0.0.2") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0bpgjfrhwdps8cpqja19fkpahi2vmi7cbkczq3xm8mj4fcl5mhzx")))

(define-public crate-rust_iso15924-0.0.3 (c (n "rust_iso15924") (v "0.0.3") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1yvpq4md6ashblv88iv4vdpj2mn3l1iisyrqq5p93hf1ny26r341")))

