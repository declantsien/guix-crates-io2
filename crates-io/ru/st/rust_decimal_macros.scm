(define-module (crates-io ru st rust_decimal_macros) #:use-module (crates-io))

(define-public crate-rust_decimal_macros-0.6.1 (c (n "rust_decimal_macros") (v "0.6.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.6.1") (d #t) (k 0)))) (h "0skx2j2c1qr73bqw96h6bji1xh2b6pw4by4jzcrif1lwaiq7h6id")))

(define-public crate-rust_decimal_macros-0.9.0 (c (n "rust_decimal_macros") (v "0.9.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.9.0") (d #t) (k 0)))) (h "0l555wn033ycyb6vrsbwx369lfa86d74maq9h7qnxj1fmlmmn0jq")))

(define-public crate-rust_decimal_macros-0.9.1 (c (n "rust_decimal_macros") (v "0.9.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.9.1") (d #t) (k 0)))) (h "04dp4p45fvsqvrdw9jyshq07jyg4r69f70aijp2j0ll8w1mimb8c")))

(define-public crate-rust_decimal_macros-0.10.2 (c (n "rust_decimal_macros") (v "0.10.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.10.2") (d #t) (k 0)))) (h "1agv2ns2lnh921lz7hb44a5271yvz7af32hsk47lcpyziqkdnvmr")))

(define-public crate-rust_decimal_macros-0.11.0 (c (n "rust_decimal_macros") (v "0.11.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.11.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^0.11.0") (d #t) (k 0)))) (h "16pjzm135arrasp2fvqhvkvn4cynx4cc2bkhz5zmfh18i7c2k4cl")))

(define-public crate-rust_decimal_macros-0.11.1 (c (n "rust_decimal_macros") (v "0.11.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.11.1") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^0.11.1") (d #t) (k 0)))) (h "0850mjlf09qwdx588lvqrc1gx60npik5crh019hsknvs0bsf8qrz")))

(define-public crate-rust_decimal_macros-1.0.0 (c (n "rust_decimal_macros") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.0.0") (d #t) (k 0)))) (h "0wdhrf0k4ajm6n3byvqaijjxcis77l344xnvnh2hl7rl6khza5z8")))

(define-public crate-rust_decimal_macros-1.0.1 (c (n "rust_decimal_macros") (v "1.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.1") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.0.1") (d #t) (k 0)))) (h "0mgj1v4hmvqxpwdv6280bcg5xmpm5w9lnrcig8qpbg0qq78s4bbm")))

(define-public crate-rust_decimal_macros-1.0.2 (c (n "rust_decimal_macros") (v "1.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.2") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.0.2") (d #t) (k 0)))) (h "1xy6n6pwpfp915jill1jrmfqxf5abyrhgkh85xr05z1yhs9mxccy")))

(define-public crate-rust_decimal_macros-1.0.3 (c (n "rust_decimal_macros") (v "1.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.3") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.0.3") (d #t) (k 0)))) (h "0zh1k2bi1j698br2ynpbrdavkvcbrv1507zc6pws50yyq8m19c1w")))

(define-public crate-rust_decimal_macros-1.1.0 (c (n "rust_decimal_macros") (v "1.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.1.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.1.0") (d #t) (k 0)))) (h "0g3k0gfmmha1a3dlh6w5z5x03hqbxxs37jik9l3c6jsb7a6v9dja")))

(define-public crate-rust_decimal_macros-1.2.0 (c (n "rust_decimal_macros") (v "1.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.2.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.2.0") (d #t) (k 0)))) (h "0qya9h3n0jpqyk3djqvapdfhkljdx4lnwbg7v4nf590fj513a34a")))

(define-public crate-rust_decimal_macros-1.2.1 (c (n "rust_decimal_macros") (v "1.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.2.1") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.2.1") (d #t) (k 0)))) (h "0qhc16spn731wv7lln4rxs8mwfnflxzi5rp98ww9c754bf15vijm")))

(define-public crate-rust_decimal_macros-1.3.0 (c (n "rust_decimal_macros") (v "1.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.3.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.3.0") (d #t) (k 0)))) (h "0ax93lj99prpka3mmf41mr3pkxgis8d7jqqrafsij0aj60i4h7md")))

(define-public crate-rust_decimal_macros-1.4.0 (c (n "rust_decimal_macros") (v "1.4.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.4.0") (d #t) (k 0)))) (h "1qfm6vwmayf6w96g1ak6rwni7dzjxzh2jzs4xrpff18zkfmx2z88")))

(define-public crate-rust_decimal_macros-1.4.1 (c (n "rust_decimal_macros") (v "1.4.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.1") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.4.1") (d #t) (k 0)))) (h "0m043m00h9q8yxxb2nlr76danqra7n6g3jb8mdx1iifc798w2jz8")))

(define-public crate-rust_decimal_macros-1.5.0 (c (n "rust_decimal_macros") (v "1.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.5.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.5.0") (d #t) (k 0)))) (h "104yg5dqy6h7jzhhpvhcs7g9l7cl4ad79sxyycfydgcwjibmixxq")))

(define-public crate-rust_decimal_macros-1.6.0 (c (n "rust_decimal_macros") (v "1.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.6.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.6.0") (d #t) (k 0)))) (h "14qms111m9mmgs9w99fvav355kzjsapq1rbjxqjf1d310g4zwk4p")))

(define-public crate-rust_decimal_macros-1.7.0 (c (n "rust_decimal_macros") (v "1.7.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.7.0") (d #t) (k 0)) (d (n "rust_decimal_macro_impls") (r "^1.7.0") (d #t) (k 0)))) (h "04pva9fwy9ls82glmyr19g3sv97vz5ljh62y9syvrvp7vyzdra6a")))

(define-public crate-rust_decimal_macros-1.8.0 (c (n "rust_decimal_macros") (v "1.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.0") (f (quote ("std"))) (d #t) (k 0)))) (h "0dmavv2vibnaf2aqggqw62gn07a8p2bllkddv35dw2dfkvkkc0w9")))

(define-public crate-rust_decimal_macros-1.8.1 (c (n "rust_decimal_macros") (v "1.8.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 0)))) (h "1nx2wf554x0332grllcsapzplmncbibabkh2ajmwxaprndcz1vd5")))

(define-public crate-rust_decimal_macros-1.9.0 (c (n "rust_decimal_macros") (v "1.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.9.0") (d #t) (k 0)))) (h "0pw4w28ki4ppnhc4063h9j0linwzbdsvdj0s1zj4pnmr2m8b1w8d")))

(define-public crate-rust_decimal_macros-1.10.0 (c (n "rust_decimal_macros") (v "1.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.10.0") (d #t) (k 0)))) (h "15hz6cks6c3b9j2mnw1cvkhwblspphx10kn1b1p385rjz4j3n541")))

(define-public crate-rust_decimal_macros-1.10.1 (c (n "rust_decimal_macros") (v "1.10.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.10.1") (d #t) (k 0)))) (h "0k8c6fsiby4kf7wjngzz2c14jnl4w0n1dhbj928svsv2j6x8dvr3")))

(define-public crate-rust_decimal_macros-1.10.2 (c (n "rust_decimal_macros") (v "1.10.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.10.2") (d #t) (k 0)))) (h "0n2zjzabwvdmhp8n0lfshg8j6qfpsyh2bjqq74bqpjg1kc0jd04x")))

(define-public crate-rust_decimal_macros-1.10.3 (c (n "rust_decimal_macros") (v "1.10.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.10.3") (d #t) (k 0)))) (h "0lwzasxz3x3j8j1xzp0wqihxhlxg7p31xq3fmh3xrnakaww64s77")))

(define-public crate-rust_decimal_macros-1.11.0 (c (n "rust_decimal_macros") (v "1.11.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.11.0") (d #t) (k 0)))) (h "1gfjisck39c2xk5dl6mf389rz6nfdksh3fg7iinxa8siky360h8c") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.11.1 (c (n "rust_decimal_macros") (v "1.11.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.11.1") (d #t) (k 0)))) (h "0vb6r9c08kc38rmv8rmg0yr9ng8qd46vf0bnn0lh75m576hgssd7") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.12.0 (c (n "rust_decimal_macros") (v "1.12.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.11.1") (d #t) (k 0)))) (h "1hgbgf4xbgbi0yb528di2klgwahjszjwdf3lzd474xqxqamjw7x7") (f (quote (("reexportable") ("default")))) (y #t)))

(define-public crate-rust_decimal_macros-1.12.1 (c (n "rust_decimal_macros") (v "1.12.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.12.1") (d #t) (k 0)))) (h "091hmdig8cp6fp34dqqly6r3pr8hzcv38yqqa8gqajim82i2q3mk") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.12.2 (c (n "rust_decimal_macros") (v "1.12.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.12.2") (d #t) (k 0)))) (h "0cvq0v657bfgivadbyivbhvlgb50vjm95wsilfdpllhdyliginw1") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.12.3 (c (n "rust_decimal_macros") (v "1.12.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.12.3") (d #t) (k 0)))) (h "0cxwrj6s38xz1s1hj7bafidd9wv3b8nqbsjlx828ylsqdsr08ldp") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.12.4 (c (n "rust_decimal_macros") (v "1.12.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.12.4") (d #t) (k 0)))) (h "0hdr087v634ydzai1zi3cr613gj2pkd190p9amma2d60xlwdfqry") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.13.0 (c (n "rust_decimal_macros") (v "1.13.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.13.0") (d #t) (k 0)))) (h "0k48ahhdl4dg7kd94z91938dj51kx9j1xkj6y4ngqsz4b0hw4mqy") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.14.0 (c (n "rust_decimal_macros") (v "1.14.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.14.0") (d #t) (k 0)))) (h "1ygp74mxzvz2jfhzrppc60nmk4c9qsrcjni638wr428fj6kb333a") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.14.1 (c (n "rust_decimal_macros") (v "1.14.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.14.1") (d #t) (k 0)))) (h "14vqbw1qimr2hmfpvnsdd18mavn3dj0gf4nj1bapyr6cbw50mb5r") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.14.3 (c (n "rust_decimal_macros") (v "1.14.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.14.3") (d #t) (k 0)))) (h "06b8vy25ajyjpzpwp7k9zbhbfy0zvnklqh8kg2fmjbiyfwggwxm7") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.15.0 (c (n "rust_decimal_macros") (v "1.15.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.15.0") (d #t) (k 0)))) (h "1pxic59x8sh6yhzir9y4ivyixy85vqlz7bidbwrwbrsj4mgpz4pi") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.16.0 (c (n "rust_decimal_macros") (v "1.16.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.16.0") (d #t) (k 0)))) (h "1agi2i14j76irzgvmy3bg30wwv7p9qbk4n32nxig5fxlzfnwccjp") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.17.0 (c (n "rust_decimal_macros") (v "1.17.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.17.0") (d #t) (k 0)))) (h "0bmjh3bqhz2mqff58axm48xya9zagxb8mw8qalkminfypidpx16h") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.18.0 (c (n "rust_decimal_macros") (v "1.18.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.18.0") (d #t) (k 0)))) (h "11mw4cg6g1w8np9qi53nhs73xz3q9v866vc7v88knqalfm21x9zj") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.19.0 (c (n "rust_decimal_macros") (v "1.19.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.19.0") (d #t) (k 0)))) (h "10bizqbi76808jkdmj5afqrz1pzvs4zpzv3vd3zdsp93c7c8z9fx") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.20.0 (c (n "rust_decimal_macros") (v "1.20.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.20.0") (d #t) (k 0)))) (h "1ysnjb318sx887aa7v7xmb9ynmnadg5yygk3aym8rrk2xraxkv87") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.21.0 (c (n "rust_decimal_macros") (v "1.21.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.21.0") (d #t) (k 0)))) (h "1wl04rf373ma9rygkaigxmw2li94asgr778l6jq8rvfca9vrdwia") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.22.0 (c (n "rust_decimal_macros") (v "1.22.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.22.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "16x52d3jcykr3lvnabgp7brqsjfpijy3xbcab8d0x01lnkvvljhq") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.23.0 (c (n "rust_decimal_macros") (v "1.23.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.23.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1xlp82il9svjyf9dm5xp3c3jdzk78x6cpc6sy9l9bxzb60mmafnv") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.23.1 (c (n "rust_decimal_macros") (v "1.23.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.23.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "0m06acfyscjd2id7ca65mx6ys2cxh4dx8j0bs6aw0jvx6vlhpix4") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.24.0 (c (n "rust_decimal_macros") (v "1.24.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.23.1") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "16rhzhkhvpp3356q0xm04v9csw7j6rq0bbszjr6cpj1xdc80w0vj") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.25.0 (c (n "rust_decimal_macros") (v "1.25.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1i16g6hlx8lvl22svvw0r2y06wllpi2wysrll1d1c5f1qxb7aini") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.26.0 (c (n "rust_decimal_macros") (v "1.26.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1mws1krrgw4fnibvyca39d99rgb8q9x9jfml8dwfnal1ql4p19sn") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.26.1 (c (n "rust_decimal_macros") (v "1.26.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1cki4605lyb8760kbx4dhr4ca1gqdxfh6643raciccnjh7dxh0s9") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.27.0 (c (n "rust_decimal_macros") (v "1.27.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.27") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1zyxj6av78wvxh1dg0byvgcvimnnl2dk5946cwbg3sa22fx2szjs") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.28.0 (c (n "rust_decimal_macros") (v "1.28.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.28") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1vmlv4rg10myfd29f5rj8pmwijim17z785agyf2fddz39qqph6np") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.28.1 (c (n "rust_decimal_macros") (v "1.28.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.28") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1r9cavf719mmsz74ys827r1pmid1h2fxvivldlil92q8qlr1b77p") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.29.0 (c (n "rust_decimal_macros") (v "1.29.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "1ykfcpbnkcrvfj9ag8pqimzyi3s6l8j2pw0ybv6kqs166i00jivz") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.29.1 (c (n "rust_decimal_macros") (v "1.29.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "11y6aixpxlcrvc5mwgypqddmb2llf94xpkzkzmr29m0yvb9kyxqf") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.30.0 (c (n "rust_decimal_macros") (v "1.30.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "02365xg6hsqiixxxlw7hzkmcba15hq2a4m779njbk0szv2cc79bw") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.31.0 (c (n "rust_decimal_macros") (v "1.31.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "0anmwl2lb905rahili8qhblv38wkdq50z5iyzn98jq430g8brhy9") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.32.0 (c (n "rust_decimal_macros") (v "1.32.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "0d8q7hc2nk8rh87aa0afvq2rf6sl7fsdvdb3wp2hmcg05n04ni46") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.33.0 (c (n "rust_decimal_macros") (v "1.33.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "0zr6z09qfb2z9bf9lmqdhsgkclrfy48alixrkhbx9as8g5b4dl33") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.33.1 (c (n "rust_decimal_macros") (v "1.33.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "0ib9sxfl5k8v31s7dx07hb5695906dvpbpiy5jy0w1pp9qgp4hrf") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.34.0 (c (n "rust_decimal_macros") (v "1.34.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "11hq2qhbygzi1jh92p80xri0wchacysvnnzp71hc18mg0hdv5pk9") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.34.1 (c (n "rust_decimal_macros") (v "1.34.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "=1.33.1") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "09cg15fcalnrpmr40v1r0jqfcic9qdqf1lz473ya5wnbvgfw074d") (f (quote (("reexportable") ("default"))))))

(define-public crate-rust_decimal_macros-1.34.2 (c (n "rust_decimal_macros") (v "1.34.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.33") (k 0)) (d (n "trybuild") (r "^1.0.55") (d #t) (k 2)))) (h "10kdmfm95z9yx3ypk3kfqjbvnr5dhfsg4md6wyayz6vji0ap0674") (f (quote (("reexportable") ("default"))))))

