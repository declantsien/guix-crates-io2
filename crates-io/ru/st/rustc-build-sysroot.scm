(define-module (crates-io ru st rustc-build-sysroot) #:use-module (crates-io))

(define-public crate-rustc-build-sysroot-0.1.0 (c (n "rustc-build-sysroot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1fii0p1m01a28sjjkwwz0dricyfy4xa8ch2mn1m3va0plfv4vxba")))

(define-public crate-rustc-build-sysroot-0.2.0 (c (n "rustc-build-sysroot") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0g9md747ssy527n8vq411v6g8i2dpbl4hjix7bfm4irp13ja88m9")))

(define-public crate-rustc-build-sysroot-0.2.1 (c (n "rustc-build-sysroot") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1pfa9y99z2a9nwv6q7wql0gwkjyww7qh95w9nbhny69lbam317pa")))

(define-public crate-rustc-build-sysroot-0.3.0 (c (n "rustc-build-sysroot") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1ik2kwbpkdl5746s745hjpv9r3bamssqidvqi0rx7hlb473kyrp2")))

(define-public crate-rustc-build-sysroot-0.3.1 (c (n "rustc-build-sysroot") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0rrbjsv0lqsrrndk12lk5jgwa8fj6g0za9imfz5p90kapk9qwbj5")))

(define-public crate-rustc-build-sysroot-0.3.2 (c (n "rustc-build-sysroot") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1vvvdjx7mlyia8dm1lpn7dzzvp9q4n6cllxqagp4q8w5ba8v0z1x")))

(define-public crate-rustc-build-sysroot-0.3.3 (c (n "rustc-build-sysroot") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "14ic1mk5nw8nvrh2sd26rx7hp1yd0i1fbjvzl6ixcq65ns4kcpzc")))

(define-public crate-rustc-build-sysroot-0.4.0 (c (n "rustc-build-sysroot") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1zfz1wd9h17sqxfgfk10l08kpavzmn89npidz368q57bbrib9i10")))

(define-public crate-rustc-build-sysroot-0.4.1 (c (n "rustc-build-sysroot") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0vdcj00ixgdl6cyvlzn45kfs5pj5v4sylw4mnmqmndmcrmqi4nyn")))

(define-public crate-rustc-build-sysroot-0.4.2 (c (n "rustc-build-sysroot") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0v6fn7nvqx0wix8gq1pa2srmlcazcpgx8lqxy9gyscjjz86skllf")))

(define-public crate-rustc-build-sysroot-0.4.3 (c (n "rustc-build-sysroot") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "14ysfww12cghafl1faqh9n4g1y0ihnfwmwmj9zf5dwnccicaz3fy")))

(define-public crate-rustc-build-sysroot-0.4.4 (c (n "rustc-build-sysroot") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0i6m62wnsb98xzsnsnpsxrk9c2j4vd2dr15jpmws2y8z5gcgip1r")))

(define-public crate-rustc-build-sysroot-0.4.5 (c (n "rustc-build-sysroot") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)))) (h "1sm4cpwc0rqn0z0wpab44z41h7zw7k8qs663rry2z8wyszhp0qd2")))

(define-public crate-rustc-build-sysroot-0.4.6 (c (n "rustc-build-sysroot") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)))) (h "133nyn6ya9m4cxpz6xdbw1nx5q3dav25zizqkrm3mkcm6i13ggx9")))

(define-public crate-rustc-build-sysroot-0.4.7 (c (n "rustc-build-sysroot") (v "0.4.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)))) (h "1h5s3lglph6c105bd172ih87k8bv2k56qpw89k2dlpznpp8vn7db")))

(define-public crate-rustc-build-sysroot-0.5.0 (c (n "rustc-build-sysroot") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)))) (h "0hzvkqjs62ysryg40c36q879x4dqy60pln7496dpfihc7x3pfq6y")))

(define-public crate-rustc-build-sysroot-0.5.1 (c (n "rustc-build-sysroot") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)))) (h "1njgp8zc0jap26jzypd2642z7jmqcgypnh2ih3qkpnkfgy4jx9zr")))

(define-public crate-rustc-build-sysroot-0.5.2 (c (n "rustc-build-sysroot") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)))) (h "1ysahcf9prh5msjdb5scr1md7k52zp2szh62wilwph9pqlyacg7s")))

