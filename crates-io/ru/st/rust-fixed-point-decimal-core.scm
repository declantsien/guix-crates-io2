(define-module (crates-io ru st rust-fixed-point-decimal-core) #:use-module (crates-io))

(define-public crate-rust-fixed-point-decimal-core-0.1.0 (c (n "rust-fixed-point-decimal-core") (v "0.1.0") (h "101d9h683h4gshs4dbcz4rhbhyjpw1p3p5134vz6lq6n2x5h5b5r")))

(define-public crate-rust-fixed-point-decimal-core-0.1.1 (c (n "rust-fixed-point-decimal-core") (v "0.1.1") (h "19vvm7qpivc29glh1pkzll2qab5afa20266vfikmm39d6lah812g")))

(define-public crate-rust-fixed-point-decimal-core-0.1.2 (c (n "rust-fixed-point-decimal-core") (v "0.1.2") (h "1ya7v9scqflrjn0y79z17fpcmg6v18wr0ffzgd93i4cass89dj5m")))

