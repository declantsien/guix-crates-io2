(define-module (crates-io ru st rustls-pemfile) #:use-module (crates-io))

(define-public crate-rustls-pemfile-0.1.0 (c (n "rustls-pemfile") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "1jnwmlkmgdkq8i642nsh075dgj4h6p5fcz4d7adb4q74mrgrr7dq") (y #t)))

(define-public crate-rustls-pemfile-0.2.0 (c (n "rustls-pemfile") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)))) (h "0ip2rff4bzg7ydy31rzk6p03sp00sqa56rrc7w8rsyfcpdqh2w09")))

(define-public crate-rustls-pemfile-0.2.1 (c (n "rustls-pemfile") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jfi97lqnnnnxhmfy6ygrsp0x70m8wsdpaw45svvz1qc6vmymssy")))

(define-public crate-rustls-pemfile-0.3.0 (c (n "rustls-pemfile") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0q3k136sna6yhq98js7n7lf341w47j6gxzin2lfncz1ajxinvs0y")))

(define-public crate-rustls-pemfile-1.0.0 (c (n "rustls-pemfile") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1afrjj5l8gw8qm7njwf55nrgb8whqyfq56pyb0a0dzw7wyfjqlp7")))

(define-public crate-rustls-pemfile-1.0.1 (c (n "rustls-pemfile") (v "1.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mdxhxp73vxh5pqk5nx2xdxg1z1xkn1yzrc6inh5mh7qagzswr08")))

(define-public crate-rustls-pemfile-1.0.2 (c (n "rustls-pemfile") (v "1.0.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "16x5jhja8z0j5hcrlaqqz5qnyg9qgv8qqffwbdil6fl0b1nvb56i")))

(define-public crate-rustls-pemfile-1.0.3 (c (n "rustls-pemfile") (v "1.0.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1cplx6hgkr32nq31p3613b2sj7csrrq3zp6znx9vc1qx9c4qff9d")))

(define-public crate-rustls-pemfile-2.0.0-alpha.0 (c (n "rustls-pemfile") (v "2.0.0-alpha.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "pki-types") (r "^0.1") (d #t) (k 0) (p "rustls-pki-types")))) (h "015xbw3y0k0a1qap4g88xbrsf5xjq78jninvshv3drds25d5gjk0")))

(define-public crate-rustls-pemfile-2.0.0-alpha.1 (c (n "rustls-pemfile") (v "2.0.0-alpha.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "pki-types") (r "^0.2") (d #t) (k 0) (p "rustls-pki-types")))) (h "1zk73ww73844x0c6im50dlaq01l8wp1ni5grm2vdvyir7gllzaja")))

(define-public crate-rustls-pemfile-1.0.4 (c (n "rustls-pemfile") (v "1.0.4") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1324n5bcns0rnw6vywr5agff3rwfvzphi7rmbyzwnv6glkhclx0w")))

(define-public crate-rustls-pemfile-2.0.0-alpha.2 (c (n "rustls-pemfile") (v "2.0.0-alpha.2") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "pki-types") (r "^0.2") (d #t) (k 0) (p "rustls-pki-types")))) (h "1959vbjfa04dn505mybf796qmja0vi2xb26jjzh82xl0y3hpb6af") (f (quote (("std" "base64/std") ("default" "std"))))))

(define-public crate-rustls-pemfile-2.0.0 (c (n "rustls-pemfile") (v "2.0.0") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "pki-types") (r "^1") (d #t) (k 0) (p "rustls-pki-types")))) (h "1x34xidvzn4br2vl8f8xwmhgbjv4lmlb0ggv5whlnk4yl87rir1m") (f (quote (("std" "base64/std") ("default" "std"))))))

(define-public crate-rustls-pemfile-2.1.0 (c (n "rustls-pemfile") (v "2.1.0") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "pki-types") (r "^1.3") (d #t) (k 0) (p "rustls-pki-types")))) (h "02y7qn9d93ri4hrm72yw4zqlbxch6ma045nyazmdrppw6jvkncrw") (f (quote (("std" "base64/std") ("default" "std"))))))

(define-public crate-rustls-pemfile-2.1.1 (c (n "rustls-pemfile") (v "2.1.1") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "pki-types") (r "^1.3") (d #t) (k 0) (p "rustls-pki-types")))) (h "1awxak91qgraqrsk7bwxyn2aijhzyrs7flmaddajmxbgbrl750gl") (f (quote (("std" "base64/std") ("default" "std"))))))

(define-public crate-rustls-pemfile-2.1.2 (c (n "rustls-pemfile") (v "2.1.2") (d (list (d (n "base64") (r "^0.22") (f (quote ("alloc"))) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "pki-types") (r "^1.3") (d #t) (k 0) (p "rustls-pki-types")))) (h "0ggpmk5n7p096nim2hn57facx6rwf76l55qqsj4fny37d0jkm699") (f (quote (("std" "base64/std") ("default" "std"))))))

