(define-module (crates-io ru st rustini) #:use-module (crates-io))

(define-public crate-rustini-0.1.0 (c (n "rustini") (v "0.1.0") (h "0v7jfdr7b5sz3ah4yla8xdhpqr2j1gmrzmj764zrbdpglbbhsx8f")))

(define-public crate-rustini-0.2.1 (c (n "rustini") (v "0.2.1") (h "1n89jdf6q7qnq19zijkkafm9ai3rdsjdd95dkf4z9339zgka7j6i")))

(define-public crate-rustini-0.2.2 (c (n "rustini") (v "0.2.2") (h "16r08jkr65vv18vbyrjd7ikxzz9mwipq00iibp06z53c6adv8dhs")))

(define-public crate-rustini-0.2.3 (c (n "rustini") (v "0.2.3") (h "0g9z1nv77x3m1gls877v7a15i8wr0sbgc9d5bvkds22ghkzd8hcp")))

