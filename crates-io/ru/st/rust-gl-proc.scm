(define-module (crates-io ru st rust-gl-proc) #:use-module (crates-io))

(define-public crate-rust-gl-proc-0.1.0 (c (n "rust-gl-proc") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "1vl2afnd626yzgzfs4vk22wf2x5d8s7d3prph5cg34imbm0xhy2z")))

(define-public crate-rust-gl-proc-0.1.1 (c (n "rust-gl-proc") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "1qcb6za7avs6iq533cjlh3xw6jaz23hr0f0ws65dcixsfwb6wg7y")))

(define-public crate-rust-gl-proc-0.1.2 (c (n "rust-gl-proc") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "0ywcn3rknczd97vvmqzq7yay3h25cmvn6qzknp5408c4dydx7x80")))

(define-public crate-rust-gl-proc-0.1.3 (c (n "rust-gl-proc") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "185ic235cm070km3w8iwf12prmhs92mkiid2yylmbbm9cagrk0sp")))

(define-public crate-rust-gl-proc-0.1.4 (c (n "rust-gl-proc") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "03yxxn36a6k8pyj9nr20zljd3cj4xv0k5hii0a4d5gscnxj5x6qp")))

(define-public crate-rust-gl-proc-0.1.5 (c (n "rust-gl-proc") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "0228vbhrb3zw10ipf1l55mq2rbi1psqgkwkkg0jvi88sqaqfhx7g")))

(define-public crate-rust-gl-proc-0.1.6 (c (n "rust-gl-proc") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "1qahw23d8wyx0l2zydbyma6nw6l0h0f2xqzggblwxcw374vyf6ml")))

