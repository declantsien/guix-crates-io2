(define-module (crates-io ru st rust_sodium_holochain_fork) #:use-module (crates-io))

(define-public crate-rust_sodium_holochain_fork-0.10.2 (c (n "rust_sodium_holochain_fork") (v "0.10.2") (d (list (d (n "hex") (r "~0.3.1") (d #t) (k 2)) (d (n "libc") (r "~0.2.40") (d #t) (k 0)) (d (n "rand") (r "~0.4.2") (d #t) (k 0)) (d (n "rmp-serde") (r "~0.13.7") (d #t) (k 2)) (d (n "rust_sodium_holochain_fork-sys") (r "~0.10.3") (d #t) (k 0)) (d (n "serde") (r "~1.0.37") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.13") (d #t) (k 2)) (d (n "unwrap") (r "~1.2.0") (d #t) (k 0)))) (h "15lrhjfrrq62lp8asb0vybq6bkrw5c88a1nczdxwijl1rrspgh1w") (f (quote (("benchmarks"))))))

