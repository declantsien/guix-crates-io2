(define-module (crates-io ru st rust8) #:use-module (crates-io))

(define-public crate-rust8-0.1.0 (c (n "rust8") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vdx8hnzmgfzqn4l87641ah0sg9s7qsipy9kk0xchipwdw94qpq8")))

(define-public crate-rust8-0.2.0 (c (n "rust8") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "029fb3sz8wj79mlmfzlmzhgzjswd42mww7kilxcligvrg39nj5wv")))

(define-public crate-rust8-0.2.1 (c (n "rust8") (v "0.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1avil74d7vdi6yyq4zg9nysbkdx8ikigi3a1x3zichxr32fwf12r")))

