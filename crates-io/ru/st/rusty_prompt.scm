(define-module (crates-io ru st rusty_prompt) #:use-module (crates-io))

(define-public crate-rusty_prompt-0.3.0 (c (n "rusty_prompt") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "rusty_linked_list") (r "^0.0.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "typed-builder") (r "^0.16") (d #t) (k 0)))) (h "03c166siqi81n1lncarbdxhd0r3n2brvc9ma7rmkvhhjdv4p4m00")))

