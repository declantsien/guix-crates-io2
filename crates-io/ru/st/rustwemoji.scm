(define-module (crates-io ru st rustwemoji) #:use-module (crates-io))

(define-public crate-rustwemoji-0.1.0 (c (n "rustwemoji") (v "0.1.0") (h "1yqslmghpniq3giw16krjzq8ak5gc1w7czgsdhnzffmp4qhwm27p")))

(define-public crate-rustwemoji-0.1.1 (c (n "rustwemoji") (v "0.1.1") (h "1dlp7xdk3l5l6159d8r3a4rmh09cb6wrjxg21mrvznykjxp93wsq")))

