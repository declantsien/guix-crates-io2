(define-module (crates-io ru st rust-reime005-test) #:use-module (crates-io))

(define-public crate-rust-reime005-test-0.1.0 (c (n "rust-reime005-test") (v "0.1.0") (h "030b9a8mscvs0lyk4ghivbfb2fnrlqdvn1rwv14v4gfw13l8786x")))

(define-public crate-rust-reime005-test-0.2.0 (c (n "rust-reime005-test") (v "0.2.0") (h "00qazafgj3ky0wkk438dy5i9rjf77v9r8zwb8rszjw1pyi7xkx82")))

(define-public crate-rust-reime005-test-0.3.0 (c (n "rust-reime005-test") (v "0.3.0") (h "1q9zdkkj2f064b40iggpnwi0kwc693qhc65bymxgpl84b28mciqi")))

