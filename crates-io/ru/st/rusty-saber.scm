(define-module (crates-io ru st rusty-saber) #:use-module (crates-io))

(define-public crate-rusty-saber-1.0.0 (c (n "rusty-saber") (v "1.0.0") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0zqbrsf50g174mqwa76q86jgz0bxdpl9ap16arbphgacph4krpv7") (f (quote (("saber") ("lightsaber") ("firesaber") ("default") ("cref"))))))

