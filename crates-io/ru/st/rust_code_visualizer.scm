(define-module (crates-io ru st rust_code_visualizer) #:use-module (crates-io))

(define-public crate-rust_code_visualizer-0.1.0 (c (n "rust_code_visualizer") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "08s8wnw6jyc604m46fv4a11l5vv6q7bvggv8ckwnnsa5kxh09j20")))

(define-public crate-rust_code_visualizer-0.1.1 (c (n "rust_code_visualizer") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0mi76dgh7a6pp1rb1n3137m1mk9b03124cxvqhr257cnim0l6nv9")))

(define-public crate-rust_code_visualizer-0.1.2 (c (n "rust_code_visualizer") (v "0.1.2") (d (list (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1dn0s8zn01wlr4f1iciiqq730qgn54ah6d9b1dg02rmcpqrxbpsw")))

