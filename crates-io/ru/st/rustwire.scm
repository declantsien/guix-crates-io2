(define-module (crates-io ru st rustwire) #:use-module (crates-io))

(define-public crate-rustwire-0.1.0 (c (n "rustwire") (v "0.1.0") (d (list (d (n "prost") (r "^0.12") (f (quote ("derive"))) (d #t) (k 2)))) (h "1hym1b762fvyqj9fm9v1q20n8llwg6006h3s967qxgx1jmjn3hij")))

(define-public crate-rustwire-0.1.0-docfix (c (n "rustwire") (v "0.1.0-docfix") (d (list (d (n "prost") (r "^0.12") (f (quote ("derive"))) (d #t) (k 2)))) (h "19ff6ygiycfjjx4dz55pbxqq5kpha5d6k36c6abdva5l6pmg2n8f")))

(define-public crate-rustwire-0.2.0 (c (n "rustwire") (v "0.2.0") (d (list (d (n "prost") (r "^0.12") (f (quote ("derive"))) (d #t) (k 2)))) (h "1fwp1rgxsskdqpvw1ba1siky8p9vpdicwygycsgldhh9sl6zmn2z")))

