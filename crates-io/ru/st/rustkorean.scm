(define-module (crates-io ru st rustkorean) #:use-module (crates-io))

(define-public crate-rustkorean-1.0.0 (c (n "rustkorean") (v "1.0.0") (h "06cpmxvhbpxla8835k7g1x1i66kw6d6081ggrqdrsw9ka5z8pz4n")))

(define-public crate-rustkorean-1.0.1 (c (n "rustkorean") (v "1.0.1") (h "0lvv7l6fa2ff11rx7x53brq4lb9ib3qfcfa6rs1sxvwjc5qs5lx5")))

(define-public crate-rustkorean-1.0.2 (c (n "rustkorean") (v "1.0.2") (h "0682f94b4dhn01wzdj02119yd1v0klh974y8p48bhbz61579wqsv")))

(define-public crate-rustkorean-1.1.0 (c (n "rustkorean") (v "1.1.0") (h "1jr3d8pd7ljylgjywfavr37f3fqc89b1ykjhshwslfsdf3ykigka")))

