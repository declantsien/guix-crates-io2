(define-module (crates-io ru st rust-hdl-ok-frontpanel-sys) #:use-module (crates-io))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.1.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1z1z2jjnwnhsa5jwc2b6qyj6bc58qdxwj2vrqrdk2aaq75x83zfp") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.2.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0qh36v0s2w3afx0gm18rn5hiw778iddlhbyi6mvwaq06qsfj7yx3") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.3.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "14hck89inzwf3kh6rwx8vj08cxkq47hs71sb0py4xz6qrf0zyvxr") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.4.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0gpb9cd70wda99jlrpj2b2m1abs7fak9x95hs0j7j916fyl21pk1") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.5.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1i06d63xr0xys6apwq80ypqncjgk0k8hg6hb9bw0dzdy8hg2r4w2") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.5.2 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "08vnmahqqckrah14123kldd19cpisb4gabarpccx2szcn4v989gf") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.6.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1ir12wda91c0yjzq8k5ma48m4fgv0gqds1zda1zzqjkqd6m07pmp") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.7.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0b8g9as94a94xv5bi4pzhdvxpiscvf2s2wa084svy6vzxbb7amwp") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.7.1 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "11b11hdlrq7vyl0d1fpw1ca0635isvr8isaadrk7m28y91dkl83f") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.8.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0z7072bxy2rinqq4kqpx8a784l1zy9dfcff87k836lcax5vmdy2m") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.8.2 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "05qxskgckpa2b7ysdvbij4n3q62i7g76w0knv73f96yhzihff0gr") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.8.4 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.8.4") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0lx02xi06k0w9qhiykfk2nsj1anl2p37s2zf4pm7pzav8wyv5ph1") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.8.5 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.8.5") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "077ckfcpp8zi1hjljiv9fanr7yj9f025gv7d0qff0bxk60l5azg9") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.8.6 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.8.6") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1rz5czsp2lqsgmcp9b3r9khy4maa6sx87nirp0mah2wwl97caz9p") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.11.1 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0wls559fs94i4dvlvdw7gbxxmgmrjylbhc4bkvswxwfvpq2nixks") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.42.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.42.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0a9a821yvd1k9aj5pkpwi0vmd0cspnjbdn7pps5d25daxpgxfrhg") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.43.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.43.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0fm8ywfkqpnsd0viqd59395amb63yysr7r55jy7jgilp00yz2c0j") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.44.2 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.44.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "1mkb0rwxqin5sbxha8397jv4xwyn4rkpmy47br62y2vbx1qzhkdh") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.45.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.45.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0pjfmyxpfzkd26194zcy9cbir2smrwc09bq3ripl1xr7hxa16wfn") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.45.1 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.45.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "075x3hz4g2cjvwlywxmd3mk1xq76mjbha5i4dqkfnmc22650pwj3") (l "okFrontPanel")))

(define-public crate-rust-hdl-ok-frontpanel-sys-0.46.0 (c (n "rust-hdl-ok-frontpanel-sys") (v "0.46.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "10l24vycc435k852c7469yfihc234gg8b3rdj0xrmdypbwlg0y6m") (l "okFrontPanel")))

