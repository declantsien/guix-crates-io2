(define-module (crates-io ru st rust-seeder) #:use-module (crates-io))

(define-public crate-rust-seeder-0.1.0 (c (n "rust-seeder") (v "0.1.0") (h "1nfna012p2wb4rm2m391h4dixgsj07ff6xjp04lih59pnmsvrps2") (y #t)))

(define-public crate-rust-seeder-0.1.1 (c (n "rust-seeder") (v "0.1.1") (h "0hhyrhjzqgx9x68s33iwandn9wjpmlg1239r73c3393kjdmfsqmi") (y #t)))

(define-public crate-rust-seeder-0.1.2 (c (n "rust-seeder") (v "0.1.2") (h "1nm1j83laqq4vmv25hxgnfd3xl9jw55hrsc2p4p8m5bdjl7viwhq") (y #t)))

(define-public crate-rust-seeder-0.1.3 (c (n "rust-seeder") (v "0.1.3") (h "01rsnbk08dcm7aj56z6raxaw9nm9s3wqpn9x4x8vk52cyxc0vwkk") (y #t)))

