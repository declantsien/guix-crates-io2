(define-module (crates-io ru st rustache-lists) #:use-module (crates-io))

(define-public crate-rustache-lists-0.1.0 (c (n "rustache-lists") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1dj69j1kn64krdabdw13dvdnssri3hf6sh23h5p9fbxahzxw06fn")))

(define-public crate-rustache-lists-0.1.1 (c (n "rustache-lists") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "01cdfxyc6n3wf52a2k9j9yd3hv45g9zzaw8pmvadb1nw7zbivi7h")))

(define-public crate-rustache-lists-0.1.2 (c (n "rustache-lists") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "088dk2bjdpccqv0apvkff7nkc1gwlaf0cwhkpd5mn9v73kc157vr")))

