(define-module (crates-io ru st rustunnel) #:use-module (crates-io))

(define-public crate-rustunnel-0.1.2 (c (n "rustunnel") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clear_on_drop") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "seccomp-sys") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1xpmhcldgmd4ma5d8qz97jrgwrfyslivz4j3qsqwfdd68sk8lyj9") (r "1.55")))

