(define-module (crates-io ru st rusty-cute-macros) #:use-module (crates-io))

(define-public crate-rusty-cute-macros-0.2.0 (c (n "rusty-cute-macros") (v "0.2.0") (h "140zryhn32m006rqk9qc4fl8rb8kz6xyn8nqbikn7f127hwnkgsj")))

(define-public crate-rusty-cute-macros-0.2.1 (c (n "rusty-cute-macros") (v "0.2.1") (h "1ph0yq6nsmg13c87p34flrjxc5sl0ijn48whs89b33fpym8ndc21")))

(define-public crate-rusty-cute-macros-0.2.2 (c (n "rusty-cute-macros") (v "0.2.2") (h "0cj61cfvaj1hcl87salbbps3jjiy1zz1s009zkavrb1lm9fn7qzk")))

(define-public crate-rusty-cute-macros-0.2.3 (c (n "rusty-cute-macros") (v "0.2.3") (h "0i6jycg3fp1ql24wf6fay6b3mymnn3zclykw8yzvmd8n62m0b8zw")))

(define-public crate-rusty-cute-macros-0.2.4 (c (n "rusty-cute-macros") (v "0.2.4") (h "1zahd22skbn4lvs4liaw0qx2py8rbgshli1mibgwlqrzaa3299c4")))

(define-public crate-rusty-cute-macros-0.2.5 (c (n "rusty-cute-macros") (v "0.2.5") (h "0n6f8iyp7davq7f9ji53ynwavbzrpjmr8acyr2b2r4ybni0hq0ik")))

(define-public crate-rusty-cute-macros-0.2.6 (c (n "rusty-cute-macros") (v "0.2.6") (h "0zwdpfskg71ykaqdcfx713jpqn9ks3caan93n63h224m67sy3w4z")))

(define-public crate-rusty-cute-macros-0.3.0 (c (n "rusty-cute-macros") (v "0.3.0") (h "1r5xpnj5rjc7027ia1rp3fdy4l2ray1n40f2q715il4lgflbn4xv")))

(define-public crate-rusty-cute-macros-0.3.1 (c (n "rusty-cute-macros") (v "0.3.1") (d (list (d (n "time") (r "*") (o #t) (d #t) (k 0)))) (h "18jvldpjih0z819m1f9hl7y4rn7cx1qkxqk11fyjmmqg9rmrsf1g")))

(define-public crate-rusty-cute-macros-0.3.2 (c (n "rusty-cute-macros") (v "0.3.2") (d (list (d (n "time") (r "^0.1.34") (o #t) (d #t) (k 0)))) (h "15v5cmhv1dzp75pplarfri01jd4nrrnh3bp7skxdkjbjylwcyj46")))

