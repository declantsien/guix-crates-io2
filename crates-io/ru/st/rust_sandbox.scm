(define-module (crates-io ru st rust_sandbox) #:use-module (crates-io))

(define-public crate-rust_sandbox-0.1.0 (c (n "rust_sandbox") (v "0.1.0") (d (list (d (n "adsb") (r "^0.3.0") (d #t) (k 0)) (d (n "cancellation") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1qg39dk9bwjmnwrcphxjsskkp7g1x71312idzqvx5lb214pqvckj") (y #t)))

