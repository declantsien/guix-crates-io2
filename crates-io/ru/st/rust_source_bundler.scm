(define-module (crates-io ru st rust_source_bundler) #:use-module (crates-io))

(define-public crate-rust_source_bundler-0.2.2 (c (n "rust_source_bundler") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rust-format") (r "^0.3.4") (f (quote ("token_stream"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0qcbz9gg9lf88pr1swamddd9w8n2gw381pjabpi565nqfl4yd870")))

