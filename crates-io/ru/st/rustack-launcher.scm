(define-module (crates-io ru st rustack-launcher) #:use-module (crates-io))

(define-public crate-rustack-launcher-0.1.0 (c (n "rustack-launcher") (v "0.1.0") (d (list (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-envfile") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "10z3ri2cv8lybxgyjrckh2918riicfw3j03jjrh6b112dpicf5ki")))

