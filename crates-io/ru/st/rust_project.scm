(define-module (crates-io ru st rust_project) #:use-module (crates-io))

(define-public crate-rust_project-0.1.0 (c (n "rust_project") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0l563ccxv4hf27dhmhjgv3wd5dxq95g8zkh9p3hc5b62ma23gb6b")))

(define-public crate-rust_project-0.1.1 (c (n "rust_project") (v "0.1.1") (h "1z6pc9q8nqrxqcniby3aj90v0bsz6vxf78bhcsy3k26b06a67nw9") (y #t)))

(define-public crate-rust_project-0.1.2 (c (n "rust_project") (v "0.1.2") (h "13lyl1vgkyjfgmbzplynjgycidibdayxzfsgc9dykk4cgmaxpbir")))

(define-public crate-rust_project-0.1.3 (c (n "rust_project") (v "0.1.3") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "futures") (r "0.3.*") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "jsonpath") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13syhnb6n15lm2xfxzvnfrsibbj6gkjspyhxshzmwhjamf9cn661")))

(define-public crate-rust_project-0.1.4 (c (n "rust_project") (v "0.1.4") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "futures") (r "0.3.*") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "jsonpath") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vsic7mm43afz1mz2fafwimcyn57cm6w701z5lpc5sr57f4h1cdl")))

(define-public crate-rust_project-0.1.5 (c (n "rust_project") (v "0.1.5") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "futures") (r "0.3.*") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "jsonpath") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yv25g8w4s0px6gsdb7rhpjxnp7xdr1yhb6lgiqlwx2fwzyi2jmj")))

