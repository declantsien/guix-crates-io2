(define-module (crates-io ru st rusty-wam) #:use-module (crates-io))

(define-public crate-rusty-wam-0.7.9 (c (n "rusty-wam") (v "0.7.9") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.7.9") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "1pfdqk6icvbdy17xhml71m2rq26lr5q9adspwkn7a1l3vnnq20w6")))

(define-public crate-rusty-wam-0.7.10 (c (n "rusty-wam") (v "0.7.10") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.7.11") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "0sj9b577r0aya70n073ng7vgg696qw29qb67saah0cjqhcir4rwn")))

(define-public crate-rusty-wam-0.7.11 (c (n "rusty-wam") (v "0.7.11") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.7.12") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "1f0g9gjah7d3lzj0cjkr9mfz9qnwj55p4pfsj026y9ay9qby37kc")))

(define-public crate-rusty-wam-0.7.12 (c (n "rusty-wam") (v "0.7.12") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.7.13") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "1f1ra2y44xrr3v25q9lczqg3v2m79wzshl2qvcl92ir98y06wz5j")))

(define-public crate-rusty-wam-0.7.13 (c (n "rusty-wam") (v "0.7.13") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.7.14") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "180b684nq3kspjjci5cc03iqmdy6bc2y3y8m2zczjyrp660f9xy0")))

(define-public crate-rusty-wam-0.7.15 (c (n "rusty-wam") (v "0.7.15") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.7.18") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "0m3z83lbc7bf1maimrxwgpr2mlyskjd3wilgx1gfa2r24l2yr8na")))

(define-public crate-rusty-wam-0.7.16 (c (n "rusty-wam") (v "0.7.16") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.7.18") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "0h6rwvnqnlgq6sv9rirbiwvjvgwshy3mzadkx8p2q3jfc3jnm53c")))

(define-public crate-rusty-wam-0.8.0 (c (n "rusty-wam") (v "0.8.0") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "0vl1jg0r4p5rks2a1y7v3kdxpvgw9vnlhw9rvxg2hzd1y4ys4yjs")))

(define-public crate-rusty-wam-0.8.1 (c (n "rusty-wam") (v "0.8.1") (d (list (d (n "downcast") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "prolog_parser") (r "^0.8.1") (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)))) (h "0ygh9ga25irz2pqwk7x11kdcx3j031pqk17j023wwk03f7s2qfv4")))

