(define-module (crates-io ru st rust-hdl-bsp-tang-nano-4k) #:use-module (crates-io))

(define-public crate-rust-hdl-bsp-tang-nano-4k-0.45.1 (c (n "rust-hdl-bsp-tang-nano-4k") (v "0.45.1") (d (list (d (n "rust-hdl") (r "^0.45.1") (f (quote ("fpga"))) (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.45.1") (d #t) (k 0)))) (h "0r2aasrln9vdk6c4zppr70h2pybf0w3jp5m0gj3z0k3m6nw8s25q") (f (quote (("yowasp") ("default"))))))

