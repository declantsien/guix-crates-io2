(define-module (crates-io ru st rust-csv2beancount) #:use-module (crates-io))

(define-public crate-rust-csv2beancount-0.1.0 (c (n "rust-csv2beancount") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "06n88przc38x0ibzadmxfblwsba44p6sxgp1qf25kmxw7isd96z9")))

(define-public crate-rust-csv2beancount-0.2.0 (c (n "rust-csv2beancount") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1q1yi3npzs885dmfjrklj949pxm2cilx31mi7ydygddrfbd3mx7z")))

