(define-module (crates-io ru st rusty-slackbot) #:use-module (crates-io))

(define-public crate-rusty-slackbot-0.1.0 (c (n "rusty-slackbot") (v "0.1.0") (d (list (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rss") (r "^1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "slack") (r "^0.22") (d #t) (k 0)))) (h "0vpwamql95jw7d21cs38f6609d3g45r0vl1719lc339762i3wb5d")))

(define-public crate-rusty-slackbot-0.2.0 (c (n "rusty-slackbot") (v "0.2.0") (d (list (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rss") (r "^1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "slack") (r "^0.22") (d #t) (k 0)))) (h "1bhws79hjp00h6qjf4z3gz7jkjya7zvspnvfpl6jsbrzkbvfs13q")))

(define-public crate-rusty-slackbot-0.2.2 (c (n "rusty-slackbot") (v "0.2.2") (d (list (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rss") (r "^1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "slack") (r "^0.22") (d #t) (k 0)))) (h "03g92ssv1srx8hxw9iv4pqvdd2lynavjvpxgwr32aga5pbjwabaw")))

(define-public crate-rusty-slackbot-0.2.4 (c (n "rusty-slackbot") (v "0.2.4") (d (list (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rss") (r "^1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "slack") (r "^0.22") (d #t) (k 0)))) (h "11x8z33kcaipvk42kn4ngj1pc60xkva6d2cf5vxig8q636b1dy8l")))

(define-public crate-rusty-slackbot-0.2.5 (c (n "rusty-slackbot") (v "0.2.5") (d (list (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rss") (r "^1") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "slack") (r "^0.22") (d #t) (k 0)))) (h "1awz7ii0zg91g2n2bi31fnwpir1555v5hxnndj58qfx1sys1xkzi")))

