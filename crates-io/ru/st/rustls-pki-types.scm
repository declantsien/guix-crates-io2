(define-module (crates-io ru st rustls-pki-types) #:use-module (crates-io))

(define-public crate-rustls-pki-types-0.1.0 (c (n "rustls-pki-types") (v "0.1.0") (h "11hidb8zjkz65w32gykhc56d4nssnc5i21x7v9xqy0zfc3cahb2m") (f (quote (("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-0.1.1 (c (n "rustls-pki-types") (v "0.1.1") (h "1sc8q18qlpra1kra8zddx2qwjy75969p2sc0kwxq738y3hrqj3y2") (f (quote (("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-0.1.2 (c (n "rustls-pki-types") (v "0.1.2") (h "12yb9jbicirhabf3nvs7s305c2pl9r20y0kg3l3pi99xwf4yj420") (f (quote (("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-0.2.0 (c (n "rustls-pki-types") (v "0.2.0") (h "1mfdgzgrsavg9gdzhp1way4shqxv5xhg2krwn4hawpc5i8pngdpb") (f (quote (("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-0.2.1 (c (n "rustls-pki-types") (v "0.2.1") (h "1n5m5k2pl5qfq0jxhq031a7jy3bds4545yk0f2v8shga9lk06w54") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-0.2.2 (c (n "rustls-pki-types") (v "0.2.2") (h "1ld9va22iyn2d80bz1f5mmivfyw00ggzxdxj8sw7wxv8pk1cpw6d") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-0.2.3 (c (n "rustls-pki-types") (v "0.2.3") (h "03amfn2bfqz63nnbb94b1dnncvlq8f1h6mw7agd6xwndrpbfvlzh") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.0.0 (c (n "rustls-pki-types") (v "1.0.0") (h "10wvhb659kv3bhafkpwqxia3g36qxdc3xannwqr0vizyksdiy2pb") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.0.1 (c (n "rustls-pki-types") (v "1.0.1") (h "16rkx6gn5l2zximxy8fx9h2vzks1hfxi5z5cd9y97r0fl853wrz7") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.1.0 (c (n "rustls-pki-types") (v "1.1.0") (h "0ajf64gycqv02md59bpg23mg4v7b4l0q3iv04zj950g67jdrg7cy") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.2.0 (c (n "rustls-pki-types") (v "1.2.0") (h "1kxsl7dkjjmb5hpq9as54zhbs9vf45axi4yd2w7fjn1ibsv6ww8a") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.3.0 (c (n "rustls-pki-types") (v "1.3.0") (h "1dqhad2c868g3pxgzin6h38d4wvrlighp502siw6v6dcngjn72h4") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.3.1 (c (n "rustls-pki-types") (v "1.3.1") (h "1a0g7453h07701vyxjj05gv903a0shi43mf7hl3cdd08hsr6gpjy") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.4.0 (c (n "rustls-pki-types") (v "1.4.0") (d (list (d (n "web-time") (r "^1") (o #t) (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "1nlj5lgmg0djig0m4wqy2zw58gb2fg600bk5dfpyz3r2vbx213l6") (f (quote (("web" "web-time") ("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.4.1 (c (n "rustls-pki-types") (v "1.4.1") (d (list (d (n "web-time") (r "^1") (o #t) (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "0izj2sa55iwziwsckxfqc66il5s3df616jjw6ca4agly4p26rlzc") (f (quote (("web" "web-time") ("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.5.0 (c (n "rustls-pki-types") (v "1.5.0") (d (list (d (n "web-time") (r "^1") (o #t) (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "0m7s6yl4zlhfc1l0ks7kc5mgzngl5iv54k3qa7qjyb7fgi863d5y") (f (quote (("web" "web-time") ("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.6.0 (c (n "rustls-pki-types") (v "1.6.0") (d (list (d (n "web-time") (r "^1") (o #t) (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "02mk56svjx0bsxcmk2bqlxq5z0542nw4jwy2xq8b1qf50v949wsi") (f (quote (("web" "web-time") ("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

(define-public crate-rustls-pki-types-1.7.0 (c (n "rustls-pki-types") (v "1.7.0") (d (list (d (n "web-time") (r "^1") (o #t) (d #t) (t "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (k 0)))) (h "0banlc9xzwqrx8n0h4bd0igmq3z5hc72rn941lf22cp3gkkraqlp") (f (quote (("web" "web-time") ("std" "alloc") ("default" "alloc") ("alloc")))) (r "1.60")))

