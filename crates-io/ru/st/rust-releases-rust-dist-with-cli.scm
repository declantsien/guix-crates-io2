(define-module (crates-io ru st rust-releases-rust-dist-with-cli) #:use-module (crates-io))

(define-public crate-rust-releases-rust-dist-with-cli-0.15.1 (c (n "rust-releases-rust-dist-with-cli") (v "0.15.1") (d (list (d (n "rust-releases-core") (r "^0.15.1") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.15.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1bxkd5w6h8sk0qz6hkr97lv27x62yxdp832i3ws14n76jmy0fqda")))

(define-public crate-rust-releases-rust-dist-with-cli-0.16.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.16.0") (d (list (d (n "rust-releases-core") (r "^0.16.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0azcmaphsrn94m22g5jmzzi9xhash4sd9ngagmk5cjxly5fqfvcv")))

(define-public crate-rust-releases-rust-dist-with-cli-0.17.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.17.0") (d (list (d (n "rust-releases-core") (r "^0.16.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0k5nczzcninqjdl0zb4zlq19hf9dh2wds1xz9h4jibxyj7xi85zv") (y #t)))

(define-public crate-rust-releases-rust-dist-with-cli-0.21.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.21.0") (d (list (d (n "rust-releases-core") (r "^0.21.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0y6wdsgxnr7wl7nv63nb095n50gnsmp9vc2avpbrs56255dyfw84")))

(define-public crate-rust-releases-rust-dist-with-cli-0.21.1 (c (n "rust-releases-rust-dist-with-cli") (v "0.21.1") (d (list (d (n "rust-releases-core") (r "^0.21.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "167cazicifv71wjr2q6yyj4pmy9yh04bgr8xlqm2z2f4qyjwsf31")))

(define-public crate-rust-releases-rust-dist-with-cli-0.22.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.22.0") (d (list (d (n "rust-releases-core") (r "^0.22.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1r173gc6xb8dvfdr1kkadxrxal4jvgy2fz1k0ba7vkk0hh9phm1i")))

(define-public crate-rust-releases-rust-dist-with-cli-0.22.1 (c (n "rust-releases-rust-dist-with-cli") (v "0.22.1") (d (list (d (n "rust-releases-core") (r "^0.22.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ssp1qia8ib9sx85dkfv5vz25r287lan0cwls5hi7kjhljab545d")))

(define-public crate-rust-releases-rust-dist-with-cli-0.23.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.23.0") (d (list (d (n "rust-releases-core") (r "^0.23.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "06pby920jjlsl9v4diffxf2ckslzqzsfaw4qllms7pv7c9ih0b7h") (y #t) (r "1.63")))

(define-public crate-rust-releases-rust-dist-with-cli-0.24.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.24.0") (d (list (d (n "rust-releases-core") (r "^0.24.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.24.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0mica8dhmhfl07c54px1447rli7mxv0pb44iiy9cz6sz9ar0xq43") (r "1.63")))

(define-public crate-rust-releases-rust-dist-with-cli-0.25.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.25.0") (d (list (d (n "rust-releases-core") (r "^0.25.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "07fibx6fvjwld6ssv1ix41idih5bmsnkimgisz169p9w2x4702gm") (r "1.63")))

(define-public crate-rust-releases-rust-dist-with-cli-0.26.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.26.0") (d (list (d (n "rust-releases-core") (r "^0.26.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "10h86savkj0icqls3aqycs5xdd852651fjixyq2vy810yyhp9lf2") (r "1.63")))

(define-public crate-rust-releases-rust-dist-with-cli-0.27.0 (c (n "rust-releases-rust-dist-with-cli") (v "0.27.0") (d (list (d (n "rust-releases-core") (r "^0.27.0") (d #t) (k 0)) (d (n "rust-releases-io") (r "^0.27.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "059c26bcjxdbsaxwp0q6i5r02l3idnmhvnsncmgz98j00b5ihjyn") (r "1.63")))

