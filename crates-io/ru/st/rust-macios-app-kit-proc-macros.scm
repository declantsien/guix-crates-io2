(define-module (crates-io ru st rust-macios-app-kit-proc-macros) #:use-module (crates-io))

(define-public crate-rust-macios-app-kit-proc-macros-0.2.2 (c (n "rust-macios-app-kit-proc-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0qnl46vb3n3d8ygiqf5d1a946zhnmjp4klfmclm10p7j6jlwwpzn")))

(define-public crate-rust-macios-app-kit-proc-macros-0.2.3 (c (n "rust-macios-app-kit-proc-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0yjk6l18dhvfhp6abqsy5v4ca9yhvaflr4gz35fn1disxibigcbp")))

