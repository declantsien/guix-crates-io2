(define-module (crates-io ru st rustiny_linear_algebra) #:use-module (crates-io))

(define-public crate-rustiny_linear_algebra-0.1.0 (c (n "rustiny_linear_algebra") (v "0.1.0") (d (list (d (n "rustiny_fixed_point") (r "^0.1.0") (d #t) (k 2)) (d (n "rustiny_number") (r "^0.1.0") (d #t) (k 0)))) (h "16hrda27131lnp2c6zd3l0j7ww8nx8qv67na069i8gwsx140ip8p")))

(define-public crate-rustiny_linear_algebra-0.1.1 (c (n "rustiny_linear_algebra") (v "0.1.1") (d (list (d (n "rustiny_fixed_point") (r "^0.1.1") (d #t) (k 2)) (d (n "rustiny_number") (r "^0.1.1") (d #t) (k 0)))) (h "0ykdgyz9g73rf88v9gblxc2svsvq00qdwg570pd7c2ija4v1k06v")))

(define-public crate-rustiny_linear_algebra-0.1.2 (c (n "rustiny_linear_algebra") (v "0.1.2") (d (list (d (n "rustiny_fixed_point") (r "^0.1.4") (d #t) (k 2)) (d (n "rustiny_number") (r "^0.1.3") (d #t) (k 0)))) (h "0wgmw29k4q9mf15y7z0aq0skwwx6iy1x4lqqqcndyv1anjgiy6hf")))

(define-public crate-rustiny_linear_algebra-0.1.3 (c (n "rustiny_linear_algebra") (v "0.1.3") (d (list (d (n "rustiny_fixed_point") (r "^0.1.5") (d #t) (k 2)) (d (n "rustiny_number") (r "^0.1.4") (d #t) (k 0)))) (h "04m9kksais7fdr5msv03pizv6iwfd90sr5g5d1ly6h5ycyq9vqb5")))

(define-public crate-rustiny_linear_algebra-0.1.4 (c (n "rustiny_linear_algebra") (v "0.1.4") (d (list (d (n "rustiny_fixed_point") (r "^0.1.7") (d #t) (k 2)) (d (n "rustiny_number") (r "^0.1.6") (d #t) (k 0)))) (h "1bcbd06ljnij5bpnxkwcjcrqis6kj9ifgj63qr69whxd4wki8gd6")))

(define-public crate-rustiny_linear_algebra-0.1.5 (c (n "rustiny_linear_algebra") (v "0.1.5") (d (list (d (n "rustiny_fixed_point") (r "^0.1.10") (d #t) (k 2)) (d (n "rustiny_number") (r "^0.1.10") (d #t) (k 0)))) (h "112zi3238cwgvgdyj9gps1kl4b20sgi1jqy097nskkqgj6hjzhis")))

(define-public crate-rustiny_linear_algebra-0.2.0 (c (n "rustiny_linear_algebra") (v "0.2.0") (d (list (d (n "rustiny_fixed_point") (r "^0.2.0") (d #t) (k 2)) (d (n "rustiny_number") (r "^0.2.0") (d #t) (k 0)))) (h "127k11y3bgdv219bsa8xry90z219mqq54cx1cnqiyb5n3x8dm126")))

