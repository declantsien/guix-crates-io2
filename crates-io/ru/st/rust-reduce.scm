(define-module (crates-io ru st rust-reduce) #:use-module (crates-io))

(define-public crate-rust-reduce-0.1.0 (c (n "rust-reduce") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "syn-inline-mod") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1w1r95w2bcxq3fzcrrvdii75my0p3mrp1vs0cpy7d8l7bys0s53q")))

