(define-module (crates-io ru st rustkraken) #:use-module (crates-io))

(define-public crate-RustKraken-0.1.0 (c (n "RustKraken") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "156m7xj31cxl2aq31pmvz8b2rk23kapvfqnfv9pp4lb4ysgz6qa8")))

