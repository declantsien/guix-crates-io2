(define-module (crates-io ru st rustmq) #:use-module (crates-io))

(define-public crate-rustmq-1.0.0 (c (n "rustmq") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "flatbuffers") (r "^0.6") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lapin") (r "^0.28") (f (quote ("futures"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "rt-threaded" "time"))) (d #t) (k 2)))) (h "0bnbi14g4jh84m607irihv6xq07678b52aasls4xai9i1yvm6x6y") (y #t)))

