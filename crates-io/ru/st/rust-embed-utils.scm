(define-module (crates-io ru st rust-embed-utils) #:use-module (crates-io))

(define-public crate-rust-embed-utils-5.0.0 (c (n "rust-embed-utils") (v "5.0.0") (d (list (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "01g1c5lix6fl8q8yghd28d70bx7hdsalrjqwzcn2vjsc0xc52rcp") (f (quote (("debug-embed"))))))

(define-public crate-rust-embed-utils-5.1.0 (c (n "rust-embed-utils") (v "5.1.0") (d (list (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0vc7492a6qlq7r899p7vyx5cwiqwkw2pf85mfw5anwr42ccj4l9a") (f (quote (("debug-embed"))))))

(define-public crate-rust-embed-utils-6.0.0 (c (n "rust-embed-utils") (v "6.0.0") (d (list (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1l6648b790nwy1wjr170n9n5f7d7970nyjgjhzjnr18v0q4jjpsv") (f (quote (("debug-embed"))))))

(define-public crate-rust-embed-utils-7.0.0 (c (n "rust-embed-utils") (v "7.0.0") (d (list (d (n "glob") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1clpbihaja87r2rp6wnx5hcp6wkcry7isa905w7zcqx9ksmyf5k1") (f (quote (("include-exclude" "glob") ("debug-embed"))))))

(define-public crate-rust-embed-utils-7.1.0 (c (n "rust-embed-utils") (v "7.1.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1v0sihipyiw7wfasa4xdvx5brgwmx5sza7djx96i0aa8dqicf8md") (f (quote (("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-7.2.0 (c (n "rust-embed-utils") (v "7.2.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0ab02sv9zlinf6gacmac0zm9iv7q9kcyrx013mx4icfbmyiyqvvm") (f (quote (("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-7.3.0 (c (n "rust-embed-utils") (v "7.3.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0m7h33gabh4afgf2w5i2paq94qbc2jz8nsw5wbwbbldbvy0rsrn1") (f (quote (("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-7.4.0 (c (n "rust-embed-utils") (v "7.4.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0h9zplfjacj6s4zyrq6262z0lgg48g4hp8qkd3wn02bkranzflkz") (f (quote (("include-exclude" "globset") ("debug-embed")))) (y #t)))

(define-public crate-rust-embed-utils-7.5.0 (c (n "rust-embed-utils") (v "7.5.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0cbp7whwhfjmlqamkak6xnfbnj7psr1wnjkmr3ii8zizhnv0lasi") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-7.8.0 (c (n "rust-embed-utils") (v "7.8.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0wfckx80panyhyzsl7r19gdj35r0gkprb3l28fb0hv4jy6xkii7i") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-7.8.1 (c (n "rust-embed-utils") (v "7.8.1") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0x7dvlmzzx0db3ra73g8h0fsfgy3c1xrzkhg22vkpp3hymmzyf4x") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-8.0.0 (c (n "rust-embed-utils") (v "8.0.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1nisb2dr0h59alcbv96pgniy9x2ra74j3fvi1bgnzy3vrgwfygw7") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-8.1.0 (c (n "rust-embed-utils") (v "8.1.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0h0xz4p6rpszwbyxwcxfkggcnsa3zyy09f2lpgb564j3fm4csv41") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-8.2.0 (c (n "rust-embed-utils") (v "8.2.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0rc6zawqs8y6j8js626faw49knc70c1wzhkr450b9m5jzdds5c4c") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-8.3.0 (c (n "rust-embed-utils") (v "8.3.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "10am6j29b5p7na45cz6vqxkg8gy47xbir95d9vzzyrr50f4r1xl6") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

(define-public crate-rust-embed-utils-8.4.0 (c (n "rust-embed-utils") (v "8.4.0") (d (list (d (n "globset") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0cmgq7f9slzw94yd43v8m55fysykgxsxwj0kf0q0hql5c5l4mirq") (f (quote (("mime-guess" "mime_guess") ("include-exclude" "globset") ("debug-embed"))))))

