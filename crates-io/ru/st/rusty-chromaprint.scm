(define-module (crates-io ru st rusty-chromaprint) #:use-module (crates-io))

(define-public crate-rusty-chromaprint-0.1.0 (c (n "rusty-chromaprint") (v "0.1.0") (d (list (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "065j5xi1nlqk122fzalx2r1qfg82hrygajp33gj2gngxms9fff58") (y #t)))

(define-public crate-rusty-chromaprint-0.1.1 (c (n "rusty-chromaprint") (v "0.1.1") (d (list (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1l0mlydvc7pq17jbzdskmc0q2zjg2bpj6ymm9q02c4d7kq9jl2rx")))

(define-public crate-rusty-chromaprint-0.1.2 (c (n "rusty-chromaprint") (v "0.1.2") (d (list (d (n "rubato") (r "^0.12.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "18kwi364wmh869c76z17kk5wmp406vzv9vihdp2m4rzkhj4bvdw6")))

(define-public crate-rusty-chromaprint-0.1.3 (c (n "rusty-chromaprint") (v "0.1.3") (d (list (d (n "rubato") (r "^0.12.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "1b7s56w00337zy6sqragxxki5n1fv719iwrl66qqs86345424fh2")))

(define-public crate-rusty-chromaprint-0.2.0 (c (n "rusty-chromaprint") (v "0.2.0") (d (list (d (n "rubato") (r "^0.14.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "0h04n8s0qqw2fbg45h90vyxd6xksal5smpkn2wwwnvn3cxl68m8p") (r "1.63")))

