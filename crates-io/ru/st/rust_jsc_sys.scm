(define-module (crates-io ru st rust_jsc_sys) #:use-module (crates-io))

(define-public crate-rust_jsc_sys-0.1.0 (c (n "rust_jsc_sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "01lvpmk456gyyndysjkm8hlv5kamswmi84dcz2s2gcabv240xlsx") (f (quote (("patches") ("default" "patches"))))))

(define-public crate-rust_jsc_sys-0.1.2 (c (n "rust_jsc_sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "09gx25r7iixnfn95kzjg8sjn42kpcd79a9m9amsazjwchvddbycr") (f (quote (("patches") ("default" "patches"))))))

(define-public crate-rust_jsc_sys-0.1.3 (c (n "rust_jsc_sys") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "03f9v0jjpvjjfpz241ydbkaalrbwd2rn37bs2vi1k56z5i40q1y1") (f (quote (("patches") ("default" "patches"))))))

(define-public crate-rust_jsc_sys-0.1.4 (c (n "rust_jsc_sys") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "106wpgmfrn5sw748p4d11790mznjx9hj9dslk9m851l7fklx8m2v") (f (quote (("patches") ("default" "patches"))))))

(define-public crate-rust_jsc_sys-0.1.5 (c (n "rust_jsc_sys") (v "0.1.5") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0m1bj5fdl2r84755kpibl2dm401krll6gdlzmxbqybxlvjlly3sz") (f (quote (("patches") ("default" "patches"))))))

(define-public crate-rust_jsc_sys-0.1.6 (c (n "rust_jsc_sys") (v "0.1.6") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1qs6rcnswk0a353160mkj0i7rmr6w7ap2s5637gpwhlyawpvnhhv") (f (quote (("patches") ("default" "patches"))))))

