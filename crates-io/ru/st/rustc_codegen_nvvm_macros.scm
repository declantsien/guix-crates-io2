(define-module (crates-io ru st rustc_codegen_nvvm_macros) #:use-module (crates-io))

(define-public crate-rustc_codegen_nvvm_macros-0.1.0 (c (n "rustc_codegen_nvvm_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "1ng3krckl50vi8ir5j1ylzsjafskc07pwwmmi7q2494506n9ryvb")))

