(define-module (crates-io ru st rust_function) #:use-module (crates-io))

(define-public crate-rust_function-0.1.0 (c (n "rust_function") (v "0.1.0") (h "1r3920zqf0ydjr8xddjskx5b0mv2g0dxxbk1i2a5g48cmmqzph8m")))

(define-public crate-rust_function-0.1.2 (c (n "rust_function") (v "0.1.2") (h "1n4py4incw1zd8kv4qaddh9d1xh17dr8618s2n0azn6gyvzamgcp")))

