(define-module (crates-io ru st rust-datastructures) #:use-module (crates-io))

(define-public crate-rust-datastructures-0.1.0 (c (n "rust-datastructures") (v "0.1.0") (h "15n285xkd6rj9mv29x09vk9283k1ligfi3rrvwyyfbqcdfn49ayf") (r "1.65.0")))

(define-public crate-rust-datastructures-0.1.1 (c (n "rust-datastructures") (v "0.1.1") (h "076mgn5xhdy1q1xx1yway6aqmzlpq62mwzbz18bp6smy599ky8yl") (r "1.65.0")))

