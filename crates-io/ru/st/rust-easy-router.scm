(define-module (crates-io ru st rust-easy-router) #:use-module (crates-io))

(define-public crate-rust-easy-router-0.1.0 (c (n "rust-easy-router") (v "0.1.0") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)))) (h "0gp5340q74hf44vih42v6mxs8pwq2k8grp81iwna6l522kggqa70")))

(define-public crate-rust-easy-router-0.1.1 (c (n "rust-easy-router") (v "0.1.1") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)))) (h "052rzbj14zqk9ak1wp648bhfq1m83db8lkr5pgcc10qp567qrxnb")))

