(define-module (crates-io ru st rustwemoji-image) #:use-module (crates-io))

(define-public crate-rustwemoji-image-0.1.0 (c (n "rustwemoji-image") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "rustwemoji-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "185rp5787bnd676hy9yy1mbgywr0mdy1zaks32xilpm3k8ix8vng") (f (quote (("tokio" "rustwemoji-parser/tokio") ("discord" "rustwemoji-parser/discord") ("default" "discord") ("async-std" "rustwemoji-parser/async-std"))))))

