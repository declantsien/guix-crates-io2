(define-module (crates-io ru st rust-whichsort) #:use-module (crates-io))

(define-public crate-rust-whichsort-0.1.0 (c (n "rust-whichsort") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "06dj2dqj70ql0dp0rfycdv3b491827sbz3285qxj5ap0wq66rzrm")))

