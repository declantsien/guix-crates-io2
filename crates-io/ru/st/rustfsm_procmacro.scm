(define-module (crates-io ru st rustfsm_procmacro) #:use-module (crates-io))

(define-public crate-rustfsm_procmacro-0.1.0 (c (n "rustfsm_procmacro") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustfsm_trait") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "037nh6qzj8r9557sfjnmncshkmbid4msrvyq4bnr1fvz2w63x4kc")))

