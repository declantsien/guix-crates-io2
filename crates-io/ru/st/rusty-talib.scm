(define-module (crates-io ru st rusty-talib) #:use-module (crates-io))

(define-public crate-rusty-talib-0.1.0 (c (n "rusty-talib") (v "0.1.0") (d (list (d (n "polars") (r "^0.33.2") (f (quote ("lazy" "describe" "rolling_window"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kdswq973ba3db2k69v09chypywglfl9hz5qrwk3lfs9dn6ijclq") (f (quote (("overlap_studies") ("default" "overlap_studies"))))))

(define-public crate-rusty-talib-0.1.1 (c (n "rusty-talib") (v "0.1.1") (d (list (d (n "polars") (r "^0.33.2") (f (quote ("lazy" "describe" "rolling_window"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kw5y7k5vcrcx871r7gvdsqh62s7m6zv8l7kpvybkvbln2fpak2v") (f (quote (("overlap_studies") ("default" "overlap_studies"))))))

(define-public crate-rusty-talib-0.1.2 (c (n "rusty-talib") (v "0.1.2") (d (list (d (n "polars") (r "^0.33.2") (f (quote ("lazy" "describe" "rolling_window"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19wgmv5ygd64200z8llrnp3zhyssvm4r4zr3n6gyvzpviflqjznr") (f (quote (("overlap_studies") ("default" "overlap_studies"))))))

(define-public crate-rusty-talib-0.1.3 (c (n "rusty-talib") (v "0.1.3") (d (list (d (n "polars") (r "^0.33.2") (f (quote ("lazy" "describe" "rolling_window"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mijh46ris64cr34vqj3arn4ya4i6shk59rch8zy3m7gscq97z6z") (f (quote (("overlap_studies") ("default" "overlap_studies"))))))

(define-public crate-rusty-talib-0.1.4 (c (n "rusty-talib") (v "0.1.4") (d (list (d (n "polars") (r "^0.33.2") (f (quote ("lazy" "describe" "rolling_window"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dw5apibgqy75vx45pr3810yzfh06lrs79q9vl1f37k6arys3kwj") (f (quote (("overlap_studies") ("default" "overlap_studies"))))))

(define-public crate-rusty-talib-0.1.5 (c (n "rusty-talib") (v "0.1.5") (d (list (d (n "polars") (r "^0.33.2") (f (quote ("lazy" "describe" "rolling_window"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0slkc48vx19f1ph7bkw873r08m090fc2yvvw5vhd9r5448gnb1c7") (f (quote (("overlap_studies") ("default" "overlap_studies"))))))

(define-public crate-rusty-talib-0.1.6 (c (n "rusty-talib") (v "0.1.6") (d (list (d (n "polars") (r "^0.33.2") (f (quote ("lazy" "describe" "rolling_window"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f47xyrpfl4yh7sx0dimljsk715mznlqnmwkrsy33p8hh84jn179") (f (quote (("overlap_studies") ("default" "overlap_studies"))))))

