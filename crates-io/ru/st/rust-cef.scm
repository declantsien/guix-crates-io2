(define-module (crates-io ru st rust-cef) #:use-module (crates-io))

(define-public crate-rust-cef-0.0.1 (c (n "rust-cef") (v "0.0.1") (h "0f0ib36pjv8g9gdqlcs2x0sam448bg2n7avnp1rvrxm6bq15w3qp")))

(define-public crate-rust-cef-0.0.2 (c (n "rust-cef") (v "0.0.2") (h "0yw6h1yw09h1bkbm949k7svsqsc4dazyqrzd9k20ypzy11kpyjwz")))

(define-public crate-rust-cef-0.0.3 (c (n "rust-cef") (v "0.0.3") (h "1953q8a0w8fsv55fbsfglpk2ply5q1hzwwv1wn8wj4nrcz622501")))

(define-public crate-rust-cef-0.0.4 (c (n "rust-cef") (v "0.0.4") (h "0pfybmh9zc0p21g8s83hmsraaza8yj3ry9pdd3psyr83gpryxbif")))

(define-public crate-rust-cef-0.0.5 (c (n "rust-cef") (v "0.0.5") (h "0i6pr4pipmcdqn5sqmv60l08zmfhmnbhppv94ycrxz0z6abs4m8j")))

(define-public crate-rust-cef-0.1.0 (c (n "rust-cef") (v "0.1.0") (h "0i4pmxnwddv9wd3fy0y4s4cd7iikg0b70j106caq7smbl9qxv32v")))

(define-public crate-rust-cef-0.1.1 (c (n "rust-cef") (v "0.1.1") (h "17yh9khigshi4i0s5qlvmvzbdsy5jds1fbz0frm5lr6g2wbwwryj")))

(define-public crate-rust-cef-0.1.3 (c (n "rust-cef") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "01pmva3d0pdzbl06jpxqd1j1nlp74sflxy2r78wkl91wrn4i5irf")))

(define-public crate-rust-cef-0.2.0 (c (n "rust-cef") (v "0.2.0") (d (list (d (n "time") (r "^0.2.23") (d #t) (k 0)))) (h "12baa29fvrlzf5qk7gala6y6sghdxb6vij80iy3v5q2b9kd9nc51")))

(define-public crate-rust-cef-0.2.1 (c (n "rust-cef") (v "0.2.1") (d (list (d (n "time") (r "^0.2.23") (d #t) (k 0)))) (h "02wdi56z0ndsp41nbmjh4mrf5px86a06jw1w29rc134qqzpixgdh")))

(define-public crate-rust-cef-0.2.2 (c (n "rust-cef") (v "0.2.2") (d (list (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "0vyshyfmnmcvr9rjg7cg6ik8wszsv8vl1vw8ym0p5bq5y1c8q2m7")))

(define-public crate-rust-cef-0.2.4 (c (n "rust-cef") (v "0.2.4") (d (list (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1m884h6br92pkspc17qkrzcs8m38ni005vcri21h1r7g0mvdsdi4")))

(define-public crate-rust-cef-0.2.5 (c (n "rust-cef") (v "0.2.5") (d (list (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1x69lj8wg6rp37yr02iz646rn3d7ki4jr0r5pl7666amyghdjgfr")))

(define-public crate-rust-cef-0.2.6 (c (n "rust-cef") (v "0.2.6") (d (list (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "0rj9hdpmlp5kq70xf8xaqicxl0vfgwxrzybkrjcs7x7g63g5pkkh")))

