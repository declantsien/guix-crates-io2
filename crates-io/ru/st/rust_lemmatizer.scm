(define-module (crates-io ru st rust_lemmatizer) #:use-module (crates-io))

(define-public crate-rust_lemmatizer-0.1.0 (c (n "rust_lemmatizer") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "00bcsvbfh1ldv26f61dlyawidiwwhzfrkgszvnwl3slab0ncfiws")))

(define-public crate-rust_lemmatizer-0.2.0 (c (n "rust_lemmatizer") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1qrhhn93skbswj556ahb3spzb0x4id3md906811cad1fszcnwn6x")))

(define-public crate-rust_lemmatizer-0.3.0 (c (n "rust_lemmatizer") (v "0.3.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "02nwa1fh627l8icj7wyhnpgwkd7vdf4i36fgp33h0gyc0p7f9wnb")))

