(define-module (crates-io ru st rustfmt_emitter) #:use-module (crates-io))

(define-public crate-rustfmt_emitter-1.0.0 (c (n "rustfmt_emitter") (v "1.0.0") (d (list (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "rustfmt_configuration") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)))) (h "1ag3j8z5mcwg2dp78dx6kfw3h3gz1sp9kpzw0q7a6wp84za85shf")))

