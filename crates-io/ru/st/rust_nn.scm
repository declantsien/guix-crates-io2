(define-module (crates-io ru st rust_nn) #:use-module (crates-io))

(define-public crate-rust_nn-0.1.0 (c (n "rust_nn") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a6dx80mk83k2lmmaa3bag4qhbik61l5nrwkv46x4rh8pkgv93h7")))

