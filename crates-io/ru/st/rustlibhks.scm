(define-module (crates-io ru st rustlibhks) #:use-module (crates-io))

(define-public crate-rustLibhks-0.1.0 (c (n "rustLibhks") (v "0.1.0") (h "1b1jwdgdz3ws4vyv144xyfrd3spp7xz0qslyp5r6cagwlhayh1v3")))

(define-public crate-rustLibhks-0.1.1 (c (n "rustLibhks") (v "0.1.1") (h "0vsmgz8lf1a0qciml79w2h5y8h4vab2syskdqh6v86plg56izk1f")))

