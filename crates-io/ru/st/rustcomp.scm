(define-module (crates-io ru st rustcomp) #:use-module (crates-io))

(define-public crate-rustcomp-0.1.0 (c (n "rustcomp") (v "0.1.0") (h "1v24vxbfsgcmfhirmdgj8rl37z83ybv4ra1f0iydh42qzwkw8lyk")))

(define-public crate-rustcomp-0.2.0 (c (n "rustcomp") (v "0.2.0") (h "07gw1mv4vrkcjvssks8qqnfj5ipjwkmavmqs48a3p63h0in8l285")))

(define-public crate-rustcomp-0.3.0 (c (n "rustcomp") (v "0.3.0") (h "08gi13vjzf1xf9dhnv3n6w3cfbk2pgiq61i95daz5i1w5qcilwxi")))

(define-public crate-rustcomp-0.3.1 (c (n "rustcomp") (v "0.3.1") (h "0p0vjvcxxgq2pvsrd6w2m6ppp238dw7xd33vw578xbj3l7m3knaq")))

(define-public crate-rustcomp-0.4.0 (c (n "rustcomp") (v "0.4.0") (h "0w139ar7wfh8g408lyj44fm9fc2089n0n7rf81wddy978bxa0vk4")))

