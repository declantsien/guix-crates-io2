(define-module (crates-io ru st rusty_crc16) #:use-module (crates-io))

(define-public crate-rusty_crc16-0.1.0 (c (n "rusty_crc16") (v "0.1.0") (h "0sqjrplm5zdk5j32na775fa9s6sxbsgpriz20308np054rxi95c3")))

(define-public crate-rusty_crc16-0.1.1 (c (n "rusty_crc16") (v "0.1.1") (h "07ksllq74daw3ffmd0rj1chqcwasjfqc828d1qahb2fgp8zfqz1s")))

