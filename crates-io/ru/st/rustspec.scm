(define-module (crates-io ru st rustspec) #:use-module (crates-io))

(define-public crate-rustspec-0.1.2 (c (n "rustspec") (v "0.1.2") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "0y455gbzb13csdfcbf1i9l0k48iszazc711qlcw183wq5685hald")))

(define-public crate-rustspec-0.1.3 (c (n "rustspec") (v "0.1.3") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "1hncww6a617j4zn7s6mq2d8ywa6ha6xhgz4vwvqa6ka17kmb5fgc")))

(define-public crate-rustspec-0.1.4 (c (n "rustspec") (v "0.1.4") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "12fq6rhhzfsqb6prcbij90hxp4w3g6n4fg6pw1ga6yg4dm9cs021")))

(define-public crate-rustspec-0.1.5 (c (n "rustspec") (v "0.1.5") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "0cn65n7k7nzq3mgymqm25z8n1qwrvnq6hywgv9d0x1wahl55g704")))

(define-public crate-rustspec-0.1.6 (c (n "rustspec") (v "0.1.6") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "0rqnm5j9il1gq99gmr7q1rl9x4iwc34rcmd4zi52i33ria5dsgh3")))

(define-public crate-rustspec-0.1.7 (c (n "rustspec") (v "0.1.7") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "10bcak6f3kibfyaianvvp5c6drd7rfsj1z2n5xfd776l0gp4l8a7")))

(define-public crate-rustspec-0.1.8 (c (n "rustspec") (v "0.1.8") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "16bw5laxvdhsdnyhrxx5591y1zlga6ifybhnhvbipm9a1f6dfzpg")))

(define-public crate-rustspec-0.1.9 (c (n "rustspec") (v "0.1.9") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "0jwpdd4fkgb98avqjj8l7lk10fqgf61g017l0djlkh64j6xz4gzz")))

(define-public crate-rustspec-0.1.10 (c (n "rustspec") (v "0.1.10") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "0mmlv5qbxim99i4ahdlmzyw3zsbpfhpjfvwdnpv0nzgab7br47zx")))

(define-public crate-rustspec-0.1.11 (c (n "rustspec") (v "0.1.11") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "1jhpfcz2sgc9pxis8dagcgswilvhrg8sgdgv96mzl3wqll6bsdak")))

(define-public crate-rustspec-0.1.12 (c (n "rustspec") (v "0.1.12") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "0cbkq4hyc8xiqcw64y6mc0d7fw05kck4a4vdcc9q28c1l16f2186")))

(define-public crate-rustspec-0.1.13 (c (n "rustspec") (v "0.1.13") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "03i0r32n1glg40bynv34ah4n81lxn382lsh4ig9js23afs1y34c1")))

(define-public crate-rustspec-0.1.14 (c (n "rustspec") (v "0.1.14") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "1djq59xrkmcn68z9bj9jjjyhnn0fnd29sdv8y982k1cw365hz61z")))

(define-public crate-rustspec-0.1.15 (c (n "rustspec") (v "0.1.15") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "1f70wchri464wfn4phs65pm1m44hyvvy6q7pvfcjm9rzhz83g4xh")))

(define-public crate-rustspec-0.1.16 (c (n "rustspec") (v "0.1.16") (d (list (d (n "rustspec_assertions") (r "~0.1.0") (d #t) (k 0)))) (h "107mnh09gr7d8h3qs9p47zw74wiqd5qx4h1i9ibl35g5i07ldmmh")))

