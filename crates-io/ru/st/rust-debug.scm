(define-module (crates-io ru st rust-debug) #:use-module (crates-io))

(define-public crate-rust-debug-0.0.1 (c (n "rust-debug") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gimli") (r "^0.23.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fcmh5j8201my8h32bdjajkhdcw2bglny6gn1kl8sy2bgfi9vcs7")))

(define-public crate-rust-debug-0.1.0 (c (n "rust-debug") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1l5q2101nn7y44ssync5j1kk9rdnrpb81rla3ba6xcc9l6w5jwvm")))

