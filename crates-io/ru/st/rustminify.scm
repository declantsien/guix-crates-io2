(define-module (crates-io ru st rustminify) #:use-module (crates-io))

(define-public crate-rustminify-0.1.0 (c (n "rustminify") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "test-case") (r "^1.2.0") (d #t) (k 2)))) (h "1na3a1jy5p56xmkblnwss9i5sf0raj4fnn7s2mi5v0mpvdajggwf")))

(define-public crate-rustminify-0.2.0 (c (n "rustminify") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "test-case") (r "^1.2.0") (d #t) (k 2)))) (h "0qlpi2mwpmqbsl8r0i7bmzm64xhm26nrp4fx4h9nyh11mh18im1c")))

