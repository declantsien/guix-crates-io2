(define-module (crates-io ru st rustedflask) #:use-module (crates-io))

(define-public crate-rustedflask-1.0.0 (c (n "rustedflask") (v "1.0.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1gsv6fd75hjw1nhkf6dx9s376nwa6r33rrnngfq59xmm4h1hplc9") (f (quote (("jinja") ("all" "jinja"))))))

(define-public crate-rustedflask-1.0.1 (c (n "rustedflask") (v "1.0.1") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "03x7qzwpjr0xyhyg05wflh7yg8idj28d4zbv44z715zsv7kgwidg") (f (quote (("jinja") ("all" "jinja"))))))

(define-public crate-rustedflask-1.1.0 (c (n "rustedflask") (v "1.1.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0yab6qw3a0nb7qrvw1i9kjlrkgjk9irxrvx8jyidhqpx63mam0jg") (f (quote (("jinja") ("all" "jinja"))))))

(define-public crate-rustedflask-2.0.0 (c (n "rustedflask") (v "2.0.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "12b3lr2jb4wyx1yk9iy8g6jcm45p1g4j2bxzrqpi1rykilm1rnvq") (f (quote (("jinja") ("all" "jinja"))))))

(define-public crate-rustedflask-2.1.0 (c (n "rustedflask") (v "2.1.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "11b11mhwv3rx6cyg4nb818c57aq1l98an9jsb9lih16f8acnw75z") (f (quote (("jinja") ("all" "jinja"))))))

(define-public crate-rustedflask-2.2.0 (c (n "rustedflask") (v "2.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "04k2yqm1rxqr2sk48zrzk9mqzsgpfw090c6pqsjf08zgfbc509zl") (f (quote (("jinja") ("all" "jinja"))))))

(define-public crate-rustedflask-2.3.0 (c (n "rustedflask") (v "2.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0545kf2xbm0hq91k2gqgn7m0ij2x8kzllx4bxj1csixb1rj65hn5") (f (quote (("jinja") ("all" "jinja"))))))

