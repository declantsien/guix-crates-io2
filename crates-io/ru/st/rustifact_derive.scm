(define-module (crates-io ru st rustifact_derive) #:use-module (crates-io))

(define-public crate-rustifact_derive-0.1.0 (c (n "rustifact_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "13snv06bqn5578zp9k1wd2cs4214w49np65v36cyssfxd0gkpav5")))

(define-public crate-rustifact_derive-0.1.1 (c (n "rustifact_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("parsing"))) (d #t) (k 0)))) (h "07x3pmniamk3r0ljprwy5f1iv40x2xhcmgk5b0k6cy6mb98bz6sf")))

(define-public crate-rustifact_derive-0.2.0 (c (n "rustifact_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0h185w00cap1xgziyhy7k2v2vm9w5schs9zvh7rr0axp4dvp1jm9")))

