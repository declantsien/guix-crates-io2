(define-module (crates-io ru st rustmqtt) #:use-module (crates-io))

(define-public crate-RustMqtt-0.1.0 (c (n "RustMqtt") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)))) (h "07lypzrxvxg9grm03bkidih32ms7r8nz16kyqydmrdnq33fcsbb3")))

(define-public crate-RustMqtt-0.1.1 (c (n "RustMqtt") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)))) (h "1cqx4zn98pjnzddxi3m6s9h477n97k0b1k373v67sdiv7n62ijvl")))

