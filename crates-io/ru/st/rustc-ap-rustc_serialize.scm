(define-module (crates-io ru st rustc-ap-rustc_serialize) #:use-module (crates-io))

(define-public crate-rustc-ap-rustc_serialize-663.0.0 (c (n "rustc-ap-rustc_serialize") (v "663.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1w76iacnn0c6miip7azlcrh0l5vfgslwl1nq631p8sna9jj0wqzp")))

(define-public crate-rustc-ap-rustc_serialize-664.0.0 (c (n "rustc-ap-rustc_serialize") (v "664.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ij00i24xiaqw41c3mmsm030dn3gwn1fpinjvyvs5nbi088lrgq0")))

(define-public crate-rustc-ap-rustc_serialize-665.0.0 (c (n "rustc-ap-rustc_serialize") (v "665.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0zqqma32sgrp5lxpmwf7g5anmkq2qkk287hh8kx0yfga8zfpg6xq")))

(define-public crate-rustc-ap-rustc_serialize-666.0.0 (c (n "rustc-ap-rustc_serialize") (v "666.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1g8bsvh1lz5rqjzzfh0gm4z930k0h0i2f44njsv7gm53lv1rjsh2")))

(define-public crate-rustc-ap-rustc_serialize-667.0.0 (c (n "rustc-ap-rustc_serialize") (v "667.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0f4pkcfcrhhlnzn5z8wadr6rdwjf33wrfsf4x3hr30byxkc84901")))

(define-public crate-rustc-ap-rustc_serialize-668.0.0 (c (n "rustc-ap-rustc_serialize") (v "668.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "18h2j5kw1h3pwfkycr3bh23hi1spi2fn62kr0y8r9wz09lpksy4b")))

(define-public crate-rustc-ap-rustc_serialize-669.0.0 (c (n "rustc-ap-rustc_serialize") (v "669.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0j5wrrff3fwwbw7yhgx76igmp8jfnlg7bvjjjpgf9sxfprlysv1h")))

(define-public crate-rustc-ap-rustc_serialize-670.0.0 (c (n "rustc-ap-rustc_serialize") (v "670.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1g75rlpq4c558izkj6xphxknhkzzljcydvp4p02hgfz9g5ig22lh")))

(define-public crate-rustc-ap-rustc_serialize-671.0.0 (c (n "rustc-ap-rustc_serialize") (v "671.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1yfr7x3308lgd8r9hc3llk27wfdbq2xndi8spdybkv9x9rq0p30f")))

(define-public crate-rustc-ap-rustc_serialize-672.0.0 (c (n "rustc-ap-rustc_serialize") (v "672.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1j9m36fqqf14wscwbwfrd7ss9h38zd7xkkxfkwx4qhiy7vzy5s8b")))

(define-public crate-rustc-ap-rustc_serialize-673.0.0 (c (n "rustc-ap-rustc_serialize") (v "673.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1n7g3pd6v3ipmckijpps6i5v5617kzdm973rsspk5rlnlihd1ffz")))

(define-public crate-rustc-ap-rustc_serialize-674.0.0 (c (n "rustc-ap-rustc_serialize") (v "674.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ppdiqs51flsw86kl8kkpkax79sdsp0blkd5jnb5qaly5n87qc43")))

(define-public crate-rustc-ap-rustc_serialize-675.0.0 (c (n "rustc-ap-rustc_serialize") (v "675.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ck0czrg7qqcxpipfl0y1rf0l3ph1h1hnk17gbn9wllyswbwv0cc")))

(define-public crate-rustc-ap-rustc_serialize-676.0.0 (c (n "rustc-ap-rustc_serialize") (v "676.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0pkwwq0vs67kp8iw5p982xzw288pppj941gh7vlc4dgv3kbg588v")))

(define-public crate-rustc-ap-rustc_serialize-677.0.0 (c (n "rustc-ap-rustc_serialize") (v "677.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "184d0mf5r31mvywpg9ja6lcnbl51cjb1rg68w6r4kcw8g784d066")))

(define-public crate-rustc-ap-rustc_serialize-678.0.0 (c (n "rustc-ap-rustc_serialize") (v "678.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1wk4146b9da43i7pg1jz336cs6milmpfwa0m7ci30wbf33xid32v")))

(define-public crate-rustc-ap-rustc_serialize-679.0.0 (c (n "rustc-ap-rustc_serialize") (v "679.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0v30w0w3k0gz17pn0hfivzn8a2iwsfwd665afdjvywwdjvnmb0j3")))

(define-public crate-rustc-ap-rustc_serialize-680.0.0 (c (n "rustc-ap-rustc_serialize") (v "680.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "02zcskbqrfsb2xx14db7m36rlfa4qdj40x54q2xc4kar4nddvw2c")))

(define-public crate-rustc-ap-rustc_serialize-681.0.0 (c (n "rustc-ap-rustc_serialize") (v "681.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0wbajdmhj2hxnzsbmw9czn5552a3mwlzg22vs029jia3qzz9jjvs")))

(define-public crate-rustc-ap-rustc_serialize-682.0.0 (c (n "rustc-ap-rustc_serialize") (v "682.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1wqmh7vsy75qlm39mdf9i8fm7b9i61xrd34xxnrpih0q3avbg04y")))

(define-public crate-rustc-ap-rustc_serialize-683.0.0 (c (n "rustc-ap-rustc_serialize") (v "683.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0by5a7x3653mv85z9ykw59f24hmym1178g110xgq1hhcyr4ql37w")))

(define-public crate-rustc-ap-rustc_serialize-684.0.0 (c (n "rustc-ap-rustc_serialize") (v "684.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "00q398alg4f3f7g617l3xjbsz233n4xfl1cda51h47jmsiw8rf0v")))

(define-public crate-rustc-ap-rustc_serialize-685.0.0 (c (n "rustc-ap-rustc_serialize") (v "685.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0mhkl6crab8r7i04f52kkk8ywnw41b7hi2az86c034gk6ivzi7h1")))

(define-public crate-rustc-ap-rustc_serialize-686.0.0 (c (n "rustc-ap-rustc_serialize") (v "686.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1dq5q11gf42g8hbiwc75spjsq1fxna97qh34x16qryaq3rb20yf6")))

(define-public crate-rustc-ap-rustc_serialize-687.0.0 (c (n "rustc-ap-rustc_serialize") (v "687.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cwcnq6a6vrdkln8byljjq5m4xcwwzmrly029yzlwzbmwv4waqjx")))

(define-public crate-rustc-ap-rustc_serialize-688.0.0 (c (n "rustc-ap-rustc_serialize") (v "688.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1xa1bc7qdywdj2xiydig0bm1555cphvzih9van8x996c29hbjb9k")))

(define-public crate-rustc-ap-rustc_serialize-689.0.0 (c (n "rustc-ap-rustc_serialize") (v "689.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "03zs4l8k5p2xzf80fyf4xcqmlc700y4dlm8k2rmbl187vlj06a3q")))

(define-public crate-rustc-ap-rustc_serialize-690.0.0 (c (n "rustc-ap-rustc_serialize") (v "690.0.0") (d (list (d (n "indexmap") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "smallvec") (r ">=1.0.0, <2.0.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "06fg6p5ys6cpxhnz7y9dsml60psawvd9hqdf1k3kf27vkgxx0r0v")))

(define-public crate-rustc-ap-rustc_serialize-691.0.0 (c (n "rustc-ap-rustc_serialize") (v "691.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "02hlj1wfyiw4559dp1y7n5383gna2ynrx2i860vimdd815kzyp1d")))

(define-public crate-rustc-ap-rustc_serialize-692.0.0 (c (n "rustc-ap-rustc_serialize") (v "692.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "094v8nzwmakyi6idh8kjkvwq8y51bfb7s89blghrmb0si3czyfi7")))

(define-public crate-rustc-ap-rustc_serialize-693.0.0 (c (n "rustc-ap-rustc_serialize") (v "693.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0kscsv335lmhzwdmsrb9h7zbvqj67a0nfxv4dmy77bn9k54blldw")))

(define-public crate-rustc-ap-rustc_serialize-694.0.0 (c (n "rustc-ap-rustc_serialize") (v "694.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0bjmy29jd54v780ng9j3y710wxm8xnh51gxvgkklzmxq54z2816y")))

(define-public crate-rustc-ap-rustc_serialize-695.0.0 (c (n "rustc-ap-rustc_serialize") (v "695.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1bdkdfjqf0xqz85q3gfgj7qid8sjbxdfzwrjabggfdkprbj5ksmh")))

(define-public crate-rustc-ap-rustc_serialize-696.0.0 (c (n "rustc-ap-rustc_serialize") (v "696.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0i33pdpiz10i41zlg4211qa0p1cl3bgrz2glcbz4zh3v3rraq7c9")))

(define-public crate-rustc-ap-rustc_serialize-697.0.0 (c (n "rustc-ap-rustc_serialize") (v "697.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "10j6ywlvzbcilrc9q5835vmy1jbnvdvxzgjbkcbiwzfk3gvmvvg7")))

(define-public crate-rustc-ap-rustc_serialize-698.0.0 (c (n "rustc-ap-rustc_serialize") (v "698.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cc6avjbjswivprigzawlm0fqja816b7id0nv842kbcp3yc9j2ff")))

(define-public crate-rustc-ap-rustc_serialize-699.0.0 (c (n "rustc-ap-rustc_serialize") (v "699.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1fxqyjhdqvh33rip7khjal5vaia7zm278gwizw03h8av1zwbrxvw")))

(define-public crate-rustc-ap-rustc_serialize-700.0.0 (c (n "rustc-ap-rustc_serialize") (v "700.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0gayvf2iyf0dm5prdj788phq5lnzcjqvisamxzpx3s2sw1lwp1l5")))

(define-public crate-rustc-ap-rustc_serialize-701.0.0 (c (n "rustc-ap-rustc_serialize") (v "701.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "18lhxfng2z1nwcz0m462fvhfarf87az41nik69pf6f726bkpaz17")))

(define-public crate-rustc-ap-rustc_serialize-702.0.0 (c (n "rustc-ap-rustc_serialize") (v "702.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1yd206c1j1gvy20ds9yk0bfppv3abmjw3j9x7niphdxbf6ibzdjm")))

(define-public crate-rustc-ap-rustc_serialize-703.0.0 (c (n "rustc-ap-rustc_serialize") (v "703.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0mv9sxa2msirfy21p143c7mpf6849aa0szfsal8jvpvilhq763cx")))

(define-public crate-rustc-ap-rustc_serialize-704.0.0 (c (n "rustc-ap-rustc_serialize") (v "704.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1hkwsrz1dhjffnxqadlsnqqaz9kndn1p1y90b7sj4xcjsn6kc13g")))

(define-public crate-rustc-ap-rustc_serialize-705.0.0 (c (n "rustc-ap-rustc_serialize") (v "705.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0r5554h7kr4cga0vyyw91cdppvr2xrrf6v1qy743308x6lm2w8xw")))

(define-public crate-rustc-ap-rustc_serialize-706.0.0 (c (n "rustc-ap-rustc_serialize") (v "706.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0wwfc4gaazwz4dgxplcw9dwc6v5fm9c0a3n8i6mzbf5lsz892myy")))

(define-public crate-rustc-ap-rustc_serialize-707.0.0 (c (n "rustc-ap-rustc_serialize") (v "707.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1iddd92r6z0kn2ah90m9rr0mwwsn9qkcsmlp23v9a0dam4nbm9mi")))

(define-public crate-rustc-ap-rustc_serialize-708.0.0 (c (n "rustc-ap-rustc_serialize") (v "708.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1acrwx57zd2yifz78lsisy0ssz4sdsv1kgagbff4bqaw9a2pywn8")))

(define-public crate-rustc-ap-rustc_serialize-709.0.0 (c (n "rustc-ap-rustc_serialize") (v "709.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "006sii9v7q6dn6ipnvr0ahbgsv79nsgmn5n20v22idly2k8q7zi0")))

(define-public crate-rustc-ap-rustc_serialize-710.0.0 (c (n "rustc-ap-rustc_serialize") (v "710.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ghv7r5fh06jd3f5am0cvazhmhg94md7cjmyf45vslc4spadr6dc")))

(define-public crate-rustc-ap-rustc_serialize-711.0.0 (c (n "rustc-ap-rustc_serialize") (v "711.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1hhdcfxps7x06pismyy8afzc2cf79v21xzrc341xgglx8hqf9hi8")))

(define-public crate-rustc-ap-rustc_serialize-712.0.0 (c (n "rustc-ap-rustc_serialize") (v "712.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1w9m0gkmcqbdib4kdbxl0c828zjd0r1ayv3syha04g4m400p2g6k")))

(define-public crate-rustc-ap-rustc_serialize-713.0.0 (c (n "rustc-ap-rustc_serialize") (v "713.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i5rf7jbb484qv08dkrwf35hsqilx3lbl1ay8q2p283xzd1yyvff")))

(define-public crate-rustc-ap-rustc_serialize-714.0.0 (c (n "rustc-ap-rustc_serialize") (v "714.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "06p33wxn5g9m83s85n50cflgqd01hyms8f3w7jg9mrqvr6g88qmq")))

(define-public crate-rustc-ap-rustc_serialize-715.0.0 (c (n "rustc-ap-rustc_serialize") (v "715.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1n946349l5162l2i7vnggim6gjaf6yrapx02i884v62hx83x3v55")))

(define-public crate-rustc-ap-rustc_serialize-716.0.0 (c (n "rustc-ap-rustc_serialize") (v "716.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0pzj4jqkq237g30kaiz3md20adrbgz3p26adn1f5n06016wh8wrg")))

(define-public crate-rustc-ap-rustc_serialize-717.0.0 (c (n "rustc-ap-rustc_serialize") (v "717.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0kbhakdqxr8sjnp7b2n4xzyjsknmjra11zjacdcbnxlx8glgc039")))

(define-public crate-rustc-ap-rustc_serialize-718.0.0 (c (n "rustc-ap-rustc_serialize") (v "718.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1954crchi5ai8k6d8ww6rx5sa0ykhsphy6wikqvilk1c8r5859p9")))

(define-public crate-rustc-ap-rustc_serialize-719.0.0 (c (n "rustc-ap-rustc_serialize") (v "719.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mzk54g9kd4zl3424k7pizhwm4ck39grq6anrmriqm36x43hghli")))

(define-public crate-rustc-ap-rustc_serialize-720.0.0 (c (n "rustc-ap-rustc_serialize") (v "720.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1sipsnabw9fh4vh1dx4c112xc901vc0la6jvih36wzkv3km2gvvx")))

(define-public crate-rustc-ap-rustc_serialize-721.0.0 (c (n "rustc-ap-rustc_serialize") (v "721.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "12s7jjlsakbq37gk9q1643947szz3dhb50m04yr3yc17wpnv2kjp")))

(define-public crate-rustc-ap-rustc_serialize-722.0.0 (c (n "rustc-ap-rustc_serialize") (v "722.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1zxirlc2bxjfd0gcjah7q062anlac2898ci7w7xj6qzfka71yx2z")))

(define-public crate-rustc-ap-rustc_serialize-723.0.0 (c (n "rustc-ap-rustc_serialize") (v "723.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "015pc5y3pgy9zy9vfa6pwlamwpmal50q9ar1dsahb36jvc0zahki")))

(define-public crate-rustc-ap-rustc_serialize-724.0.0 (c (n "rustc-ap-rustc_serialize") (v "724.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1xqnbp6qk50ny7r2kn0xlqr4390dpkcv1351rzvf3h9lywvjljna")))

(define-public crate-rustc-ap-rustc_serialize-725.0.0 (c (n "rustc-ap-rustc_serialize") (v "725.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "17csmk9gvqq5h622pjz33c2snwzgfh65p4z68rdvb9xl70yjcsbr")))

(define-public crate-rustc-ap-rustc_serialize-726.0.0 (c (n "rustc-ap-rustc_serialize") (v "726.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0idbl60iql6ii22kdak4h3fyx2h083fvskslqcis6d4nsn9k5xbk")))

(define-public crate-rustc-ap-rustc_serialize-727.0.0 (c (n "rustc-ap-rustc_serialize") (v "727.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cl0ga60ny3pwk63qjp2h1gnmkwaiqljl5xvv761m6wr7dk6ws6a")))

