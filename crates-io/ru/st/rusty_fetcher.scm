(define-module (crates-io ru st rusty_fetcher) #:use-module (crates-io))

(define-public crate-rusty_fetcher-0.0.1 (c (n "rusty_fetcher") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "02xx59jbals4vkaawj1rax99mncmc5ghq2gh71h01hmz2j03y18c") (y #t)))

(define-public crate-rusty_fetcher-0.1.0 (c (n "rusty_fetcher") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "15pqpp322yxh92pnvp36zwh64dhnzqdax9xpk8l67xp319aq7xsf")))

