(define-module (crates-io ru st rust_gpiozero) #:use-module (crates-io))

(define-public crate-rust_gpiozero-0.1.0 (c (n "rust_gpiozero") (v "0.1.0") (d (list (d (n "linux-embedded-hal") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 0)))) (h "1l470fmdkbcr4cxna3fyxxxjvxxy7a96f0bnycyxzfpx2iplnf56") (f (quote (("nightly" "linux-embedded-hal"))))))

(define-public crate-rust_gpiozero-0.2.0 (c (n "rust_gpiozero") (v "0.2.0") (d (list (d (n "rppal") (r "^0.11.1") (d #t) (k 0)))) (h "1a3g6jwb8qlvd8x0q8h3w7bj3nww3mvzyg5izhk8145iwzyy5q1d")))

(define-public crate-rust_gpiozero-0.2.1 (c (n "rust_gpiozero") (v "0.2.1") (d (list (d (n "rppal") (r "^0.12.0") (d #t) (k 0)))) (h "1wr30ns8aaliaj242avw3ky7y1fvn5dwb7b460h0iphkd4i1a3pg")))

