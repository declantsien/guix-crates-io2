(define-module (crates-io ru st rustutils-cat) #:use-module (crates-io))

(define-public crate-rustutils-cat-0.1.0 (c (n "rustutils-cat") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0036wrfqqwsk72ajv915rin8yjl0i4cmx0c1msh6fvg618nhlqgj")))

