(define-module (crates-io ru st rust_warc) #:use-module (crates-io))

(define-public crate-rust_warc-1.0.0 (c (n "rust_warc") (v "1.0.0") (h "0xnx9m9g1d3r86lyx37y00v926wssr6kizvqr1lq9pyqmmcxc0dm")))

(define-public crate-rust_warc-1.1.0 (c (n "rust_warc") (v "1.1.0") (h "0xhjzjr7ygx5nhfqj5v3gs9riy8acl5aiw7kf3dlaxqvm59r013s")))

