(define-module (crates-io ru st rustiff) #:use-module (crates-io))

(define-public crate-rustiff-0.1.0 (c (n "rustiff") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "0q3j4f4l7rzmpmij7dz99zcvlsczkpyal17im2mn525nakmv702b")))

(define-public crate-rustiff-0.1.1 (c (n "rustiff") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "0sqvj8w27fg3wlr52mnc1z638cibd9lgd5cp2n4yfcsh7invkjx0")))

(define-public crate-rustiff-0.1.2 (c (n "rustiff") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "0y5l082dc2ng4fqs8dxwjzrh5dymvfzy1hijxj8s5vsmrcqmx4hh")))

