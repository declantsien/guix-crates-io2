(define-module (crates-io ru st rust-memory-analyzer) #:use-module (crates-io))

(define-public crate-rust-memory-analyzer-0.1.0 (c (n "rust-memory-analyzer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-rc.7") (d #t) (k 0)) (d (n "clap_derive") (r "=3.0.0-rc.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (f (quote ("use_alloc" "use_std"))) (d #t) (k 0)) (d (n "near-rust-allocator-proxy") (r "^0.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "=0.1.21") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.3") (d #t) (k 0)))) (h "0q4di8i7k77vp80xcf246gcgij71clygfx0jniy5qshcfdy6ksxf") (y #t)))

