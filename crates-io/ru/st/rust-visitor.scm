(define-module (crates-io ru st rust-visitor) #:use-module (crates-io))

(define-public crate-rust-visitor-0.1.0 (c (n "rust-visitor") (v "0.1.0") (d (list (d (n "ra_ap_hir") (r "=0.0.16") (d #t) (k 0)) (d (n "ra_ap_ide_db") (r "=0.0.16") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "=0.0.16") (d #t) (k 0)))) (h "0hkfi7afvbjp6hxad3xmirr33zfh587r6s2idfirdh9y85jc0mn2")))

(define-public crate-rust-visitor-0.2.0 (c (n "rust-visitor") (v "0.2.0") (d (list (d (n "ra_ap_syntax") (r "=0.0.16") (d #t) (k 0)))) (h "1q46bfy9amgfhn497mvx8hkha3vq4s3gsl0aykns72zd99caczlm")))

(define-public crate-rust-visitor-0.2.1 (c (n "rust-visitor") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "=0.0.16") (d #t) (k 0)))) (h "1y57qrlmgbzkc9g6mp08wdb7vgkslzra1xlqidkwmiq2l23f1y9s")))

(define-public crate-rust-visitor-0.3.0 (c (n "rust-visitor") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "=0.0.77") (d #t) (k 0)))) (h "0rmmdr9sn31px0sqsa800iva0w4fyah2hb8j0nnv395jp80g97bw")))

(define-public crate-rust-visitor-0.3.1 (c (n "rust-visitor") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.2") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "=0.0.77") (d #t) (k 0)))) (h "0kamp1n237bbxddq5psa08v0nfsq4m24s1kmvzlzv6hvys211hs0")))

