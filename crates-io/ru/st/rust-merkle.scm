(define-module (crates-io ru st rust-merkle) #:use-module (crates-io))

(define-public crate-rust-merkle-0.1.0 (c (n "rust-merkle") (v "0.1.0") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "1vfjil24j838sz2mmgb92j6y7h78bzaphkgs2ig9vyq001qla403")))

(define-public crate-rust-merkle-0.1.1 (c (n "rust-merkle") (v "0.1.1") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "0cgyky5xyx8b5189bmp0cmv1wldskzkmg2xk402glg1wx6byxmf1")))

(define-public crate-rust-merkle-0.1.2 (c (n "rust-merkle") (v "0.1.2") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "0q473kqlw70ywmv69fgkm3xsbgc941vrjqsd97dnnsk5vdbhl2qy")))

(define-public crate-rust-merkle-0.1.3 (c (n "rust-merkle") (v "0.1.3") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "1fq5q5h00srb5c2p50k1b5l52sfvd4fx95kvaj5rnbmd60rl3wrk")))

(define-public crate-rust-merkle-0.1.4 (c (n "rust-merkle") (v "0.1.4") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "1iqfzivkbfvad9llna1y6x7h5q8s013p7b992phrsv1dsknh12bl")))

(define-public crate-rust-merkle-0.1.5 (c (n "rust-merkle") (v "0.1.5") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "0ndjhi17p0gc5v05h3vqkf78d4qkw0d034i7wisfhhb2bjr04k3s")))

(define-public crate-rust-merkle-0.1.6 (c (n "rust-merkle") (v "0.1.6") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "12bpvq57f3zhac3xj5xl0r1wjhh8k9pb1y8jiq6bl5zjsfxribjy")))

(define-public crate-rust-merkle-0.1.7 (c (n "rust-merkle") (v "0.1.7") (d (list (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10.0") (d #t) (k 0)))) (h "0220xs0dgxl707n0fs9viqswc6iykhp2p970zg4ar6yyprl66x7h")))

