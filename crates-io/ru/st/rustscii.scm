(define-module (crates-io ru st rustscii) #:use-module (crates-io))

(define-public crate-rustscii-0.1.0 (c (n "rustscii") (v "0.1.0") (d (list (d (n "gfx") (r "^0.18.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "15bbvrf2ahm01a815msxnp0m442wmjabx658m9c54l2yn23hykr9")))

(define-public crate-rustscii-0.1.1 (c (n "rustscii") (v "0.1.1") (d (list (d (n "gfx") (r "^0.18.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "069g0pva55gjgbc3lr4cxl9vx2imgzmaibqh6s8cxcfzmvlcrg4g")))

(define-public crate-rustscii-0.1.2 (c (n "rustscii") (v "0.1.2") (d (list (d (n "gfx") (r "^0.18.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "1l3yhm1knxpxri87i68arsz7z5kk8vs6c85dzawa9a1xh1mnnr2q")))

