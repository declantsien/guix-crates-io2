(define-module (crates-io ru st rust-fontconfig) #:use-module (crates-io))

(define-public crate-rust-fontconfig-0.1.0 (c (n "rust-fontconfig") (v "0.1.0") (d (list (d (n "allsorts") (r "^0.5.1") (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "0a661mwjgr4k9z2x49jjf4aa1698556m5cx0i2d420dgky3hzg68")))

(define-public crate-rust-fontconfig-0.1.1 (c (n "rust-fontconfig") (v "0.1.1") (d (list (d (n "allsorts") (r "^0.5.1") (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "1ircxk70hahbm4vxwij32pqr0gy6aazsssxqg12r2c8157i0c20k") (f (quote (("std") ("default" "std"))))))

(define-public crate-rust-fontconfig-0.1.2 (c (n "rust-fontconfig") (v "0.1.2") (d (list (d (n "allsorts") (r "^0.5.1") (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "1mrxxrh6agvzklrx4krj0gmlixwxfxbi5i2wnfvhh1v7pgbmban7") (f (quote (("std") ("default" "std"))))))

(define-public crate-rust-fontconfig-0.1.3 (c (n "rust-fontconfig") (v "0.1.3") (d (list (d (n "allsorts_no_std") (r "^0.5.2") (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "041ainnkf7qf9k07qr4zafi3wca5c222j8zsr7axadlr84rq7514") (f (quote (("std" "allsorts_no_std/std") ("default" "std"))))))

(define-public crate-rust-fontconfig-0.1.4 (c (n "rust-fontconfig") (v "0.1.4") (d (list (d (n "allsorts_no_std") (r "^0.5.2") (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "rayon") (r "^1.5.0") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "09pv4r2hhzl8wyjy0czlwsyv7s4x7kapl9ncakdf3mapispkpcw9") (f (quote (("std" "allsorts_no_std/std") ("default" "std"))))))

(define-public crate-rust-fontconfig-0.1.5 (c (n "rust-fontconfig") (v "0.1.5") (d (list (d (n "allsorts_no_std") (r "^0.5.2") (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "rayon") (r "^1.5.0") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "1dxg1kihd6a5z1810rnk0x5if40rd4ls1nhnmk4a9hxbf5n5lq5v") (f (quote (("std" "allsorts_no_std/std") ("default" "std"))))))

(define-public crate-rust-fontconfig-0.1.6 (c (n "rust-fontconfig") (v "0.1.6") (d (list (d (n "allsorts") (r "^0.14.0") (f (quote ("flate2_rust"))) (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "rayon") (r "^1.5.0") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "0xxibm0dqaq6qsgfa46c061khf8218ycsh2lm6rs2k18vmc80lb5") (f (quote (("std") ("default" "std"))))))

(define-public crate-rust-fontconfig-0.1.7 (c (n "rust-fontconfig") (v "0.1.7") (d (list (d (n "allsorts") (r "^0.14.0") (f (quote ("flate2_rust"))) (k 0)) (d (n "mmapio") (r "^0.9.1") (k 0)) (d (n "rayon") (r "^1.5.0") (k 0)) (d (n "xmlparser") (r "^0.13.3") (k 0)))) (h "0jzbb45lvqvl7309444fniqy1z2bhmlfv8jy9rgjzhh659h04q7a") (f (quote (("std") ("default" "std"))))))

