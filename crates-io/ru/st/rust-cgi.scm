(define-module (crates-io ru st rust-cgi) #:use-module (crates-io))

(define-public crate-rust-cgi-0.6.1 (c (n "rust-cgi") (v "0.6.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1cdvsl18y7ipsayykkda2xzz6pxqdlimfnzxcbf653380a9q1ljx")))

(define-public crate-rust-cgi-0.6.2 (c (n "rust-cgi") (v "0.6.2") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)))) (h "1r05zhbz20p1w4wrxql7vbn7r5hmxlpxfa9ijhnd91lynms3674i")))

(define-public crate-rust-cgi-0.7.0 (c (n "rust-cgi") (v "0.7.0") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)))) (h "0k4la7139an7sj8wlihq97jgg5kx2gibx86126hj783b890ip0af")))

(define-public crate-rust-cgi-0.7.1 (c (n "rust-cgi") (v "0.7.1") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)))) (h "1ymcaikqhb32qm0agw1rva6dvfdg02n4fvl53lrk7rr9755zcagl")))

