(define-module (crates-io ru st rustyxml) #:use-module (crates-io))

(define-public crate-RustyXML-0.1.0 (c (n "RustyXML") (v "0.1.0") (h "066gby6010pib2wq0i9a35lc1h0j0j47vmv0x1ii8y7fgfwb6i91")))

(define-public crate-RustyXML-0.1.1 (c (n "RustyXML") (v "0.1.1") (h "0afmbwmkmb83xind91p97m1msknk58afr8lq42wf7rnrrj5xa8lj")))

(define-public crate-RustyXML-0.2.0 (c (n "RustyXML") (v "0.2.0") (h "0mvc588l66j5nvqwa25l99bvlv2g9mm0lrhghyzw6i10a8vzxmk0") (f (quote (("bench"))))))

(define-public crate-RustyXML-0.3.0 (c (n "RustyXML") (v "0.3.0") (h "1iaq1sclma2ri7nkhs77z6q5ivzd12nnas2lq0vxw5ijxqlwwnlb") (f (quote (("bench"))))))

