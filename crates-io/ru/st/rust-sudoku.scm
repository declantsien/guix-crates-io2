(define-module (crates-io ru st rust-sudoku) #:use-module (crates-io))

(define-public crate-rust-sudoku-0.1.0 (c (n "rust-sudoku") (v "0.1.0") (h "0bjaqzmdqdbdc9kh3njzb5j4vggv03d19sglnbgnmwlsji9k36v7")))

(define-public crate-rust-sudoku-0.2.0 (c (n "rust-sudoku") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpaf") (r "^0.6.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hvk6mg3x6f12gx4dkijhx5m8kpgc75achwr191p54d8xyh0z8yn")))

(define-public crate-rust-sudoku-0.2.1 (c (n "rust-sudoku") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpaf") (r "^0.6.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00pra2q2ij3ri2mkwlfa7gaz51z1xfx79b245my9xfvh30mm2bn2")))

(define-public crate-rust-sudoku-0.2.2 (c (n "rust-sudoku") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bpaf") (r "^0.6.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zpp06wzy8yrk67ky9rv5hahzs336i2s129bnwzk4ya0flq1py9j")))

