(define-module (crates-io ru st rust_gui_macros) #:use-module (crates-io))

(define-public crate-rust_gui_macros-0.1.0 (c (n "rust_gui_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0bxayhk69ywpxb59lkyy27ghfvimrgphkqp73hvhp1a2slikxz6c")))

(define-public crate-rust_gui_macros-0.2.1 (c (n "rust_gui_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0qblxwdh297hq01g0lh7xfcbqawipcfx4jgvir13iaiv1xijgxjg")))

