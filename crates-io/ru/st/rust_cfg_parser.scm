(define-module (crates-io ru st rust_cfg_parser) #:use-module (crates-io))

(define-public crate-rust_cfg_parser-0.1.0 (c (n "rust_cfg_parser") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0s2gd78xn9568gbphp0w479078ylk2hc2saxyrqc2b6fqa2a0s99") (f (quote (("std") ("default" "all") ("all" "std"))))))

