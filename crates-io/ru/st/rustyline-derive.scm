(define-module (crates-io ru st rustyline-derive) #:use-module (crates-io))

(define-public crate-rustyline-derive-0.1.0 (c (n "rustyline-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "007z9v9ywwry16453hqm2w4mdcmvyd3nqkzsjfjignh24mgfkbs9")))

(define-public crate-rustyline-derive-0.2.0 (c (n "rustyline-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n3iw9kaq70dw1rvvma0gjwydbj0f2mvvqvrva69f5cl6yv1dnd0")))

(define-public crate-rustyline-derive-0.3.0 (c (n "rustyline-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04h2b8qw3rfv7iw652l0jrvhpd11vb58k1wgpd7j6233jdb70dv9")))

(define-public crate-rustyline-derive-0.3.1 (c (n "rustyline-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0daj9szvfi442vj2fhm7qb92wmzv7g75qsjq9a6ycnqac4lhx9al")))

(define-public crate-rustyline-derive-0.4.0 (c (n "rustyline-derive") (v "0.4.0") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1wcmxdl0kv1fp7ac59rl94b1ksk0yikk167nz96f6782f3sgp7fv")))

(define-public crate-rustyline-derive-0.5.0 (c (n "rustyline-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1132kn1cbayl1yqyc9b3sfvxxbzzm4sl6jg119fi0hlzmfyrk1b8")))

(define-public crate-rustyline-derive-0.6.0 (c (n "rustyline-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0mw0nfi8xxsm4q80mv4va7ff8m0kgnsfjvv067zc1d8hp1daaddv")))

(define-public crate-rustyline-derive-0.7.0 (c (n "rustyline-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "139dpx6zp0v6p5wc5n317jivi52dz5sq79v2zagc02ipgxfksz0h")))

(define-public crate-rustyline-derive-0.8.0 (c (n "rustyline-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09408fs1lk7ik9z0kpmk5v9dilwdi3x2kwdhl5wc9qv0v7syl642")))

(define-public crate-rustyline-derive-0.9.0 (c (n "rustyline-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0hvaj1n0k7ys8iqfxvymmakv9aqqpvm53hagw55jw7954xaaycjs")))

(define-public crate-rustyline-derive-0.10.0 (c (n "rustyline-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0lfr25qdrn0awccq999d6g8m7bhsyxbkliibdpzimbzniff9bbz5")))

