(define-module (crates-io ru st rustwtxt) #:use-module (crates-io))

(define-public crate-rustwtxt-0.1.0 (c (n "rustwtxt") (v "0.1.0") (d (list (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xswhpsv7qnyjdidrsd23f6sdkv3z4pjs6jiy0kndd0h4v25az42")))

(define-public crate-rustwtxt-0.1.1 (c (n "rustwtxt") (v "0.1.1") (d (list (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mvr585n2fq3h9zld572v18sfyrxblx5iwc2kq1knl7956kajnp9")))

(define-public crate-rustwtxt-0.1.2 (c (n "rustwtxt") (v "0.1.2") (d (list (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11k3inj5h7gxzy19wdxnshn48n7fz2a80cyw9v3gmxcvn391cmfy")))

(define-public crate-rustwtxt-0.1.3 (c (n "rustwtxt") (v "0.1.3") (d (list (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pkhm5drg67k5a26lr4nl2h1gyn1rrns6rl5gha5zv33l0dgvapr")))

(define-public crate-rustwtxt-0.1.4 (c (n "rustwtxt") (v "0.1.4") (d (list (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h02d9s9xsncqxy4c7vrdilazk2a5vx3g25gzb19h1caar5yh733")))

(define-public crate-rustwtxt-0.1.5 (c (n "rustwtxt") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^0.11.3") (d #t) (k 0)))) (h "0d4l3mnfvjv3swin92f5l2cmrl86jpp3xlayhngjv4gqqa6m8br4")))

