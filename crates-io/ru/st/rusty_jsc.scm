(define-module (crates-io ru st rusty_jsc) #:use-module (crates-io))

(define-public crate-rusty_jsc-0.0.1 (c (n "rusty_jsc") (v "0.0.1") (d (list (d (n "rusty_jsc_sys") (r "^0.0.1") (d #t) (k 0)))) (h "1ph7qci7zkhib7q483l5b1q8xi0xm2yaarvzk7fbmk55c3m19wwp")))

(define-public crate-rusty_jsc-0.0.2 (c (n "rusty_jsc") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "rusty_jsc_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "rusty_jsc_sys") (r "^0.0.2") (d #t) (k 0)))) (h "09vz127p4k7j34nj1i0wbflw00c26a514jbb0r34p6k8sjnj2y6s")))

(define-public crate-rusty_jsc-0.0.3 (c (n "rusty_jsc") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "rusty_jsc_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "rusty_jsc_sys") (r "^0.0.3") (d #t) (k 0)))) (h "08yb6y6z3pjzh5n458my3dk7c0jiywll6qd2y50q0ak28rv0sza9")))

(define-public crate-rusty_jsc-0.1.0 (c (n "rusty_jsc") (v "0.1.0") (d (list (d (n "rusty_jsc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty_jsc_sys") (r "^0.1.0") (d #t) (k 0)))) (h "1l7xvag2mz884xpm3m43byvjka0cwwhddg30mv1gxi49qj5873sg")))

