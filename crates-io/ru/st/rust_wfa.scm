(define-module (crates-io ru st rust_wfa) #:use-module (crates-io))

(define-public crate-rust_wfa-0.1.0 (c (n "rust_wfa") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "14v21g1qihmkj7a7bka65l796h278wx7apzim8ysvwvwl627azrd")))

(define-public crate-rust_wfa-0.2.0 (c (n "rust_wfa") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1q3m30vd5nlljmgy6jvqy9wfci08p2546pqr9fdzcccia00ax49f")))

(define-public crate-rust_wfa-1.0.0 (c (n "rust_wfa") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0gfs0x5c7830hlhiz8j9wmimnjgkn62a0r4nvqd035bf501z6psc")))

