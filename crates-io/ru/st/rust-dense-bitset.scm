(define-module (crates-io ru st rust-dense-bitset) #:use-module (crates-io))

(define-public crate-rust-dense-bitset-0.1.0 (c (n "rust-dense-bitset") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "19lzmj1qzgv8pqsdx7lwngpf54q1jdnkk3x9crqx3rmdhkvs176j")))

(define-public crate-rust-dense-bitset-0.1.1 (c (n "rust-dense-bitset") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1frz3w8lal1mbjqkcp0f6146wjnzc6zmkkgns8z9qhizcyzjdkij")))

