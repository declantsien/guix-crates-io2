(define-module (crates-io ru st rustic_ml) #:use-module (crates-io))

(define-public crate-rustic_ml-0.0.0 (c (n "rustic_ml") (v "0.0.0") (h "0zckxjs725dy4qa5501wzmv0xbvybq93z4rbm3f6514fvnhgqyzr")))

(define-public crate-rustic_ml-0.0.1 (c (n "rustic_ml") (v "0.0.1") (h "0p3cl80yb8s3hdpnb5sjzrmdz8fjm783y79bdwcrlsj1qbf49x4r") (y #t)))

(define-public crate-rustic_ml-0.0.2 (c (n "rustic_ml") (v "0.0.2") (h "0kd2r5yskd0g5chr0dmjrr97i2mv4v139pyqzi62k1g7kfdjbi5v")))

