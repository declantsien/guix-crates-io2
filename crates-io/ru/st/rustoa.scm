(define-module (crates-io ru st rustoa) #:use-module (crates-io))

(define-public crate-rustoa-0.1.0 (c (n "rustoa") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1xsd7jjvyxaav2zx4yv4hffxy3hszgxa2p0sqb8d0jqvpdyh70yv")))

(define-public crate-rustoa-0.1.1 (c (n "rustoa") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0knqn56vhfyxjssvi2g7n2bra4ddks44s3nlplcrgib3wksq8v9q")))

(define-public crate-rustoa-0.1.2 (c (n "rustoa") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1syd5dxz76csv8g9qqyyqv1vgg6ia6rfpi6z3iilngfvy4q9q7l8")))

(define-public crate-rustoa-0.1.3 (c (n "rustoa") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1rr12wzh6cblk85zkgw00i5by72h13pi8s4xya2hwfa5idvqkvjj")))

(define-public crate-rustoa-0.1.4 (c (n "rustoa") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i3xwipdsxk26jx3i4k3vgvf9ig5m2cqp8y21wpx12divq2msb0s")))

(define-public crate-rustoa-0.1.5 (c (n "rustoa") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i09ipifhnmxprv62gfbs6pjl8wkcbd4bvi06g0077m7p9ajzbd2")))

(define-public crate-rustoa-0.1.6 (c (n "rustoa") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1alhpxmb0hj3flb348kd8gb1wc6j3b48ysw2566hlky7b0i93ig9")))

(define-public crate-rustoa-0.1.7 (c (n "rustoa") (v "0.1.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hphvy7g0aqvh8jgq962dy8ra8cds4r2khivq0k7jfs7c392lsnm")))

(define-public crate-rustoa-0.1.8 (c (n "rustoa") (v "0.1.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f24qqly3kipbbn5x1rrn8318dqnkykgpg0wddd59786hh5vgwpn")))

