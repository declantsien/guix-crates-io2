(define-module (crates-io ru st rust-gmp-kzen) #:use-module (crates-io))

(define-public crate-rust-gmp-kzen-0.5.0 (c (n "rust-gmp-kzen") (v "0.5.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1iww2wzpg5m5kx09xqnr0gd92rrq28gc1f7lvi4w7s3ck32x51xb") (f (quote (("serde_support" "serde" "serde_derive" "serde_json") ("default"))))))

(define-public crate-rust-gmp-kzen-0.5.1 (c (n "rust-gmp-kzen") (v "0.5.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "024dvpia06dcbwjgq2y40a13hl9j676877m0xxx5d2lm0jrlnr8f") (f (quote (("serde_support" "serde") ("default"))))))

