(define-module (crates-io ru st rustv) #:use-module (crates-io))

(define-public crate-rustv-0.1.0 (c (n "rustv") (v "0.1.0") (d (list (d (n "elfloader") (r "^0.0.3") (d #t) (k 0)))) (h "05qdanl947yrd18cmg5vd506mmnwhfsz7fizpk30lh83673r07z7")))

(define-public crate-rustv-0.2.0 (c (n "rustv") (v "0.2.0") (d (list (d (n "elfloader") (r "^0.0.3") (d #t) (k 0)))) (h "18q9i4kywbknfiwk9bvli6v9j9vhyjzdham7d46gqx1sjkrjwdcm")))

(define-public crate-rustv-0.2.1 (c (n "rustv") (v "0.2.1") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "1xs5bq1p8zhrql8pzkm4mmpdiim1bq4c6k0gdm0kpdid313nkvxc")))

(define-public crate-rustv-0.2.2 (c (n "rustv") (v "0.2.2") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "0jqdivirhpx7w0d7i4hdmikjc4747n66ip6swzh6px5nzrk20yf3")))

(define-public crate-rustv-0.2.3 (c (n "rustv") (v "0.2.3") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "12yg0sflaa0zxwfji1wlq48z6wfpp3ayy16y0djz3b8yqjb43bim")))

(define-public crate-rustv-0.2.4 (c (n "rustv") (v "0.2.4") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "1w1zi03r21cfy0ba8srmjdc622xddpa3q3ahcj2xksz8x70mjrza")))

(define-public crate-rustv-0.2.5 (c (n "rustv") (v "0.2.5") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "1jf9yqgzjr7h0gsb6ggby6zds5m86xycn7sb0cnzc5bg4aiq6y1x")))

(define-public crate-rustv-0.3.0 (c (n "rustv") (v "0.3.0") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "1yifyz98qsgdcq3r7kdgdkkc5bzn1khrkqw38mdgp95wwbd02s4d")))

(define-public crate-rustv-0.4.0 (c (n "rustv") (v "0.4.0") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "1bpy8l3c3w7zgdd6ppmxrk1kyxdyzz960al7jjh1cbsrkn6zpvn4")))

(define-public crate-rustv-0.4.1 (c (n "rustv") (v "0.4.1") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "1yrlhimkgdawd5j6mx4wpjl9icpfyqdvfgajc58ndrw2wp7smdzj")))

(define-public crate-rustv-0.5.0 (c (n "rustv") (v "0.5.0") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "1adphjycgz73hircjhscdyv3k5ay9aaqr4fhlgw96y5h3495ln74")))

(define-public crate-rustv-0.5.1 (c (n "rustv") (v "0.5.1") (d (list (d (n "elfloader32") (r "^0.0.3") (d #t) (k 0)))) (h "170gbzi5nharrr1296z3v0gb82b0npmbjvklan81ckrxcfhv1wz1")))

