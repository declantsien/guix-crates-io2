(define-module (crates-io ru st rusty-bind-build) #:use-module (crates-io))

(define-public crate-rusty-bind-build-0.1.0 (c (n "rusty-bind-build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.12") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18q0pjnalnnnf53pikm9rsllxn4dq820x94km9ny04blrdfsm177")))

(define-public crate-rusty-bind-build-0.1.1 (c (n "rusty-bind-build") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.14") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hyj1wxmksrxzvgblsqd790ld7lz6wmdnrvh874mxpngq5y1g7fw")))

(define-public crate-rusty-bind-build-0.1.2 (c (n "rusty-bind-build") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.14") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12fw4hf737isyyp6pjr96ral3bqpkiqk156p47fhkgyfcwcvrk99")))

(define-public crate-rusty-bind-build-0.1.3 (c (n "rusty-bind-build") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.14") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wafsqynvd22ys3w1ppldh8h4zav8nr9ycrxph5vmww9hkp6knqd")))

(define-public crate-rusty-bind-build-0.2.0 (c (n "rusty-bind-build") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z5y032irq81asws7dr9yiqlhnk3c80lxi6c09137k78rxf4gvb3")))

(define-public crate-rusty-bind-build-0.2.1 (c (n "rusty-bind-build") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m3n674x3k01647djl1nczb3i7b2v9rm3djcvzas15g7zb4fb5lp")))

(define-public crate-rusty-bind-build-0.3.0 (c (n "rusty-bind-build") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07w00r54yjmclw9ngxjfkk5mgi60lkz0v46a7y8b5k8rj96449lr")))

(define-public crate-rusty-bind-build-0.3.1 (c (n "rusty-bind-build") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07bcsgv2w621y0mzfjdmrjvfncldfdr6sv13jxyj3xf6fvr9dnnv")))

(define-public crate-rusty-bind-build-0.3.2 (c (n "rusty-bind-build") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lkkjj2ksa5apmkf8z0s6iib2i93i3kymbb61wymkrgg2dfvn0vp")))

(define-public crate-rusty-bind-build-0.3.3 (c (n "rusty-bind-build") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gnrlk4f6kf7k8xksjx23j55f9lza83g3rxsc72adpmipi2la0g1")))

(define-public crate-rusty-bind-build-0.3.4 (c (n "rusty-bind-build") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ra0wh1kr9j5p6wgh9nfgby62a7lbca8cg45ppdmzppy8z75f1zc")))

(define-public crate-rusty-bind-build-0.3.5 (c (n "rusty-bind-build") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hqzxn21k3rv0akcnd5c8gyqchl00s2xaxnbl4v4bdzrmfqrycaw")))

(define-public crate-rusty-bind-build-0.3.6 (c (n "rusty-bind-build") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.6") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1g4adcsab7ag0ds2qqprrjnw2jpway1gz4p4m0gxnihxf9dk1w")))

(define-public crate-rusty-bind-build-0.3.7 (c (n "rusty-bind-build") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cfg-expr") (r "^0.15") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bmjlqsgjgvfmh9l0w8k1c74vg571nijbdn8x675b5qbfiqw9p8f")))

