(define-module (crates-io ru st rusty_dns) #:use-module (crates-io))

(define-public crate-rusty_dns-0.0.1 (c (n "rusty_dns") (v "0.0.1") (h "0hkhmqj7j4nqx4cxyshwjvb9dx31gg50wd5q50r9xs1fp6sgarxx")))

(define-public crate-rusty_dns-0.0.2 (c (n "rusty_dns") (v "0.0.2") (h "1rxg5cyxfvazjfpr45x9n9srfxinj7djyq5sbpiyha23lx9xlwm8")))

(define-public crate-rusty_dns-0.0.3 (c (n "rusty_dns") (v "0.0.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0h4246mykl08ahn1gzr8cwhd0c48p65j18wcx2z62pd17cznic5i")))

