(define-module (crates-io ru st rusty_jsc_sys) #:use-module (crates-io))

(define-public crate-rusty_jsc_sys-0.0.1 (c (n "rusty_jsc_sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1hk80n7kjc5abzafsakz83xd3bqda228a8p8nwlrkwr5y8gfjfg1")))

(define-public crate-rusty_jsc_sys-0.0.2 (c (n "rusty_jsc_sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1f2rq3knd0813w8a11apks5dy9anxxi32al83i300byk5866y7dz")))

(define-public crate-rusty_jsc_sys-0.0.3 (c (n "rusty_jsc_sys") (v "0.0.3") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1zycd58hj6545mavf3ya04ijjj9zrnkwi0899f3vn2l7b9k4nyjq")))

(define-public crate-rusty_jsc_sys-0.1.0 (c (n "rusty_jsc_sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1pyqm75fdykljz01afwfvfxcn5h2nzzy1gribs332wr6gaml6x4j")))

