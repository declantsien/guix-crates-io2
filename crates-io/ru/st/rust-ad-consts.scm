(define-module (crates-io ru st rust-ad-consts) #:use-module (crates-io))

(define-public crate-rust-ad-consts-0.7.0 (c (n "rust-ad-consts") (v "0.7.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)))) (h "1aissjs9zsvzf9rfwsv4mi3hyjj2jb1cnkbhb6dhb0i5c8y992nn")))

(define-public crate-rust-ad-consts-0.7.1 (c (n "rust-ad-consts") (v "0.7.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)))) (h "1p9rjf6lrdb2r6ziv6p33578m7ry3km801306a3r86k7bbflkx8m")))

(define-public crate-rust-ad-consts-0.7.2 (c (n "rust-ad-consts") (v "0.7.2") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)))) (h "00v60i7d9bv0ip0h2y7iqsi3snhic4varq744k8yyd4ibdyrjn3l")))

(define-public crate-rust-ad-consts-0.8.0 (c (n "rust-ad-consts") (v "0.8.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)))) (h "0aiya89ia82mir584d8c2f17hd8d93m2zrdifprhx9m283c6hdd6")))

