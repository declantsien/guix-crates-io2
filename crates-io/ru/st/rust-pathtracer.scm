(define-module (crates-io ru st rust-pathtracer) #:use-module (crates-io))

(define-public crate-rust-pathtracer-0.1.0 (c (n "rust-pathtracer") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1kka275ajra3p3p5qxay5yyg2s7r1939xb5qg0rkw0ic7vshr814")))

(define-public crate-rust-pathtracer-0.1.1 (c (n "rust-pathtracer") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0g7s06hy3l7vkbg8rj71pwzyl7wr7ahdd2br5hwqf0r4cshkmy4h")))

(define-public crate-rust-pathtracer-0.1.2 (c (n "rust-pathtracer") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1gh1j9zvlbsxn5q59ji4907ys5dclqj1n36mirzi2bini4rc1q9i")))

(define-public crate-rust-pathtracer-0.2.0 (c (n "rust-pathtracer") (v "0.2.0") (d (list (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rhai") (r "^1.12.0") (f (quote ("sync" "internals"))) (d #t) (k 0)))) (h "0lx0acvwb0qabr6ffn623s38k0d6s0yrhgpnfykhsrrf7b61y702")))

(define-public crate-rust-pathtracer-0.2.1 (c (n "rust-pathtracer") (v "0.2.1") (d (list (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rhai") (r "^1.12.0") (f (quote ("sync" "internals"))) (d #t) (k 0)))) (h "1rwbx4d6hl5m0bbpnp1kf2l9gg3ybsvv12y1fmq8zyj1sll9dp09")))

(define-public crate-rust-pathtracer-0.2.2 (c (n "rust-pathtracer") (v "0.2.2") (d (list (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rhai") (r "^1.12.0") (f (quote ("sync" "internals"))) (d #t) (k 0)))) (h "011ss3lks4kc30wbz17ivcxcxxm3240ah79szmkw088rq4bic1vw")))

(define-public crate-rust-pathtracer-0.2.3 (c (n "rust-pathtracer") (v "0.2.3") (d (list (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "rhai") (r "^1.12.0") (f (quote ("sync" "internals"))) (d #t) (k 0)))) (h "007338vq3ziqm39ljy7kf391w6zv20x34fp54f13dj0bxp1cvsi6")))

(define-public crate-rust-pathtracer-0.2.4 (c (n "rust-pathtracer") (v "0.2.4") (d (list (d (n "colors-transform") (r "^0.2.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rhai") (r "^1.12.0") (f (quote ("sync" "internals"))) (d #t) (k 0)))) (h "1ywjqcv4di9var12fyra89w80qc9kn29ih1387pzw31ssijq236s")))

