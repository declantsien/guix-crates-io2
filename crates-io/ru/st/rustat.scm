(define-module (crates-io ru st rustat) #:use-module (crates-io))

(define-public crate-rustat-0.1.0 (c (n "rustat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "11zja8c8ksrfdh0p57kiwrdcnlk0bqvr1lkis2plnchr1rkb358p")))

(define-public crate-rustat-0.1.1 (c (n "rustat") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "0z39m77bmi07dg7jqyky8m5kcmqpj3jxxm5229i6vrxfrfjwvlj7")))

