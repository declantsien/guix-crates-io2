(define-module (crates-io ru st rustsbi-macros) #:use-module (crates-io))

(define-public crate-rustsbi-macros-0.0.0 (c (n "rustsbi-macros") (v "0.0.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1xj2w6m152cczlkflrcz918hsqmj40rlybkvwxi84xzs8561b4b9") (f (quote (("machine") ("default"))))))

(define-public crate-rustsbi-macros-0.0.1 (c (n "rustsbi-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0wgr22md9jj3b4bhpwr7g83hvgj0chpyrfp6kjqzl8h5k3i0vlry") (f (quote (("machine") ("default"))))))

(define-public crate-rustsbi-macros-0.0.2 (c (n "rustsbi-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1z0p9rzjk13yzz6qwwxkj1b5bj8ncp0d5isj6rpnpk42jpd4f4x7") (f (quote (("machine") ("default"))))))

