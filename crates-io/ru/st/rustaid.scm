(define-module (crates-io ru st rustaid) #:use-module (crates-io))

(define-public crate-rustaid-0.1.0 (c (n "rustaid") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0jv0qim0w8p1kafirnqpr7ixx67avvi4rmzwh3d8h9jm492yf38b") (y #t)))

(define-public crate-rustaid-0.1.1 (c (n "rustaid") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1sx697h9wj7s2xcl14qbiy4fagl81nszk540p5v21j5081czmwry") (y #t)))

