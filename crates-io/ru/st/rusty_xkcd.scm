(define-module (crates-io ru st rusty_xkcd) #:use-module (crates-io))

(define-public crate-rusty_xkcd-0.1.0 (c (n "rusty_xkcd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1x0s75ds3g2inr1c7alijwjvnym2vdckkpg1m4hkmbscn78vc6mr")))

(define-public crate-rusty_xkcd-0.1.1 (c (n "rusty_xkcd") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0k174psr1gb15h26yan39ig13wmqpqn033j3n37hy1fszmm32hbw")))

(define-public crate-rusty_xkcd-0.1.2 (c (n "rusty_xkcd") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "scraper") (r "^0.9.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1g1a5fxg7iv4m79i4cgr79nfwsjfjmspg79bk1fwqj7b4h3b1qlw")))

