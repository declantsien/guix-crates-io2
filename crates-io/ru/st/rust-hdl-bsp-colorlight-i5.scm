(define-module (crates-io ru st rust-hdl-bsp-colorlight-i5) #:use-module (crates-io))

(define-public crate-rust-hdl-bsp-colorlight-i5-0.45.1 (c (n "rust-hdl-bsp-colorlight-i5") (v "0.45.1") (d (list (d (n "rust-hdl") (r "^0.45.1") (f (quote ("fpga"))) (d #t) (k 0)))) (h "0hg2wzb0x7n06jzaq1gmq3pykbi1g33p9vw0bmyih8kviqk552j6") (f (quote (("yowasp") ("default"))))))

