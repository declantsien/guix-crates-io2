(define-module (crates-io ru st rust-servo) #:use-module (crates-io))

(define-public crate-rust-servo-0.1.0 (c (n "rust-servo") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "06v7wm7zlpx0a8hrx6vav978xzq5kn5fbwi8w432qygvb2rfg7p0")))

(define-public crate-rust-servo-0.1.1 (c (n "rust-servo") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "170h91xibd9w5awzvs3b9j8hcw0cbfk2hwhlk84zsqb7fhgyzavg")))

