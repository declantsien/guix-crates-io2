(define-module (crates-io ru st rust-http-logger) #:use-module (crates-io))

(define-public crate-rust-http-logger-0.1.0 (c (n "rust-http-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "params") (r "^0.3") (d #t) (k 0)) (d (n "persistent") (r "^0.2") (d #t) (k 0)) (d (n "router") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0mxf2436w2qkz0ga276a7facsx1z0vp9xjg73s88mvn0v96hfw3n")))

(define-public crate-rust-http-logger-0.2.0 (c (n "rust-http-logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "params") (r "^0.3") (d #t) (k 0)) (d (n "persistent") (r "^0.2") (d #t) (k 0)) (d (n "router") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07zd4n4rgr8qc8lvl9lsjcxs9hp3f5lcq21j310mlhy5g933g81n")))

