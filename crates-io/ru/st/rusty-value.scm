(define-module (crates-io ru st rusty-value) #:use-module (crates-io))

(define-public crate-rusty-value-0.1.0 (c (n "rusty-value") (v "0.1.0") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0z42qzcj987dn6xlxl4rcgsf830npk5rj3d9jgxyyxda29k7dbgn") (f (quote (("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.2.0 (c (n "rusty-value") (v "0.2.0") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0w7v1ir7q8ddfynwjgacn7l9jgb54rakcz2hy2d94qpghf0kmiq6") (f (quote (("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.2.1 (c (n "rusty-value") (v "0.2.1") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1r5cjgv8gjj4c3s9h1wy2yswynryp81xq2wg71c6c1lvsavl7nkr") (f (quote (("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.3.0 (c (n "rusty-value") (v "0.3.0") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0ddygihqy8r30mjfbji52kpk7885kii2jn3xyw43gcdiq6aniasw") (f (quote (("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.4.0 (c (n "rusty-value") (v "0.4.0") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0wfgnmh6d27fgsc7am8nyvgsgf29kywk2n4ssa86gwx49ifykmiz") (f (quote (("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.4.1 (c (n "rusty-value") (v "0.4.1") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "06pab551ywy4r0pqmzn6fp9yda7dxxybszbwxrjnlmglv082r1lz") (f (quote (("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.4.2 (c (n "rusty-value") (v "0.4.2") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1vv8g6yyx1ar8y3ynj5q3r2wlxw3s8mgijpnna0fj35403m5d93n") (f (quote (("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.5.0 (c (n "rusty-value") (v "0.5.0") (d (list (d (n "rusty-value-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("std"))) (o #t) (k 0)))) (h "1iizg3yvzp99hdckw9mvxwqdka8mv98w1dw4nxmb3l6nbgzllpdv") (f (quote (("json" "serde_json") ("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.5.1 (c (n "rusty-value") (v "0.5.1") (d (list (d (n "rusty-value-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("std"))) (o #t) (k 0)))) (h "0giw6f73h5m3brgvns7ml3vjl5dkgg7bdbyi7r747rnbq7ps3p28") (f (quote (("json" "serde_json") ("derive" "rusty-value-derive") ("default"))))))

(define-public crate-rusty-value-0.6.0 (c (n "rusty-value") (v "0.6.0") (d (list (d (n "rusty-value-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("std"))) (o #t) (k 0)))) (h "163wb9kyfqy2fir198b2j4p8gdz34pyz33k0d8vvmha7njqwlz7b") (f (quote (("json" "serde_json") ("derive" "rusty-value-derive") ("default"))))))

