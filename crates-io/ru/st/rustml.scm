(define-module (crates-io ru st rustml) #:use-module (crates-io))

(define-public crate-rustml-0.0.1 (c (n "rustml") (v "0.0.1") (d (list (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "15mxf4qrdnjxy2szacr54a2nxn0ny5bn23rl83fjp5v4s92cqgxv")))

(define-public crate-rustml-0.0.2 (c (n "rustml") (v "0.0.2") (d (list (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1b6n2pxyq39lbmh5b9zv5lzz7dasnwdznpvfhlrjr8y443zrvc24")))

(define-public crate-rustml-0.0.3 (c (n "rustml") (v "0.0.3") (d (list (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1krcw1sbcdyj05h5ggsbrk3m3q0m5a0gdsw7756a8pns6xb60746")))

(define-public crate-rustml-0.0.4 (c (n "rustml") (v "0.0.4") (d (list (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1b3wqxi4kdns8j3v708a2n93z13p10p0g1qhhnmfvapr6alamps6")))

(define-public crate-rustml-0.0.5 (c (n "rustml") (v "0.0.5") (d (list (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "178yrfrayw94bv4h0qzf0lgqx7mzv2ipjplz73bf3im038m1sqic")))

(define-public crate-rustml-0.0.6 (c (n "rustml") (v "0.0.6") (d (list (d (n "flate2") (r "^0.2.20") (d #t) (k 0)) (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "0b0fvrjj6kwnia2zjv3mag9mhmv6w10jqhcazh441abm0vbim3np")))

(define-public crate-rustml-0.0.7 (c (n "rustml") (v "0.0.7") (d (list (d (n "flate2") (r "^0.2.20") (d #t) (k 0)) (d (n "getopts") (r "^0.2.17") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "num") (r "^0.1.41") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.8") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1c6sc8fn0w2ycggxrxy2g6j97zl1zz0q6mnla6jykzd7ykngs3dv")))

