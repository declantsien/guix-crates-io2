(define-module (crates-io ru st rust_json_parser) #:use-module (crates-io))

(define-public crate-rust_json_parser-0.1.0 (c (n "rust_json_parser") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hpffw8bdz7wawkp2il1672mqnkys85lkakgh3y0hahb8i2rjhwa")))

(define-public crate-rust_json_parser-1.0.0 (c (n "rust_json_parser") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fyykqdlh3xs5qh5z6qs03xybxpx1cj59skkf4s4gazpjkaykzzn")))

(define-public crate-rust_json_parser-1.0.1 (c (n "rust_json_parser") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vd1v98331ajl1gykiwfpi9hdh7g89jzf6fh7j90rwcilnkf6w0h")))

