(define-module (crates-io ru st rustlr) #:use-module (crates-io))

(define-public crate-rustlr-0.1.0 (c (n "rustlr") (v "0.1.0") (h "0l091bpmw00fxq6swd541fgikzv19rxf26wsp867r65n64my3rm8")))

(define-public crate-rustlr-0.1.1 (c (n "rustlr") (v "0.1.1") (h "1d59m7lwspd491ad85r1c5ps26b8mm25bvmn2jj17pvk77giqg1m")))

(define-public crate-rustlr-0.1.2 (c (n "rustlr") (v "0.1.2") (h "042pq89k7x0scyvh2zhy47v2ah9iij9p1pdpbckvr43pkgjy7nra")))

(define-public crate-rustlr-0.1.3 (c (n "rustlr") (v "0.1.3") (h "138v9ahk3b8ngn64ibrjbi8ksmqdsrpf6f6kgfr4ajqygsgl9q4k")))

(define-public crate-rustlr-0.1.4 (c (n "rustlr") (v "0.1.4") (h "1dx1rvsn48dip63qfdjw4h2b17p6hjr7x368d2xcv1w66swkm2l3")))

(define-public crate-rustlr-0.2.0 (c (n "rustlr") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rxpkbjkw6m7lzz2hrx8fb688gnh5vp4z7nq8qs1k6lkl1g8c4f4")))

(define-public crate-rustlr-0.2.1 (c (n "rustlr") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0paa6gxdllfn8y00mlf89kq7blkzh4y46nmqywa8j1n4xkzhglv1")))

(define-public crate-rustlr-0.2.2 (c (n "rustlr") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1y28l80y49zy7p3jjr1g90qdkplpwpry846sfql82jr7hivdd9z2")))

(define-public crate-rustlr-0.2.3 (c (n "rustlr") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1x6484q3cakmzk9hsd45s5ycgd9vrx1y2hp400z6qg6g0bs40sn7")))

(define-public crate-rustlr-0.2.4 (c (n "rustlr") (v "0.2.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "121zmf8aswap865if3py72fc5h7hais19cw7dxhipi9f245zlx2k")))

(define-public crate-rustlr-0.2.5 (c (n "rustlr") (v "0.2.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zd539c83mdash7z01n51sss0xyzj8gs5zzwd25fdqwzhdv4wh68")))

(define-public crate-rustlr-0.2.6 (c (n "rustlr") (v "0.2.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r8v4sniyh8iqlpw10f3jvw15gmslwzb7ighilad0396h2l4k1qd") (y #t)))

(define-public crate-rustlr-0.2.7 (c (n "rustlr") (v "0.2.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fd7vf60k7zfcq8sdclg8fixdnwwsjk0pf0nv5fpg9k9sjjx1wd8") (y #t)))

(define-public crate-rustlr-0.2.8 (c (n "rustlr") (v "0.2.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xrf96a5m4ajpck79fr4xlwra3npmflapdlpiq7x14c11iyjla35")))

(define-public crate-rustlr-0.2.9 (c (n "rustlr") (v "0.2.9") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08l6i16lzdx7c24d6h7h2hq6yrs7rzag8gzvmqhf28id8rhhxlgw")))

(define-public crate-rustlr-0.2.91 (c (n "rustlr") (v "0.2.91") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0008836lmsd39sfg283kabzzh9pzpbkxqjvk7pgqn0fywzga61y1")))

(define-public crate-rustlr-0.2.92 (c (n "rustlr") (v "0.2.92") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00lp0h89y0zhmj16n9lhm6yxyh1hidxyxbr96miz07szkpcvx41s") (y #t)))

(define-public crate-rustlr-0.2.93 (c (n "rustlr") (v "0.2.93") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fy3cqklsgwgi7xjxd6yg425s832rlvkb033xcn9771mjfdmbli2")))

(define-public crate-rustlr-0.2.94 (c (n "rustlr") (v "0.2.94") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mx507swd6d8v3iz2bxi6w24ca4jfdqdqrddp7rz6bdfbsjs65rv")))

(define-public crate-rustlr-0.2.95 (c (n "rustlr") (v "0.2.95") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sghv9acymsiiwbp5fw8gnbb1gvgay52hkybi87ywv1y8wr2ssi2")))

(define-public crate-rustlr-0.2.96 (c (n "rustlr") (v "0.2.96") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1c7gfm1gglnbwazvwg0y4q24d16n421wdhihlb69nhjk147g69g5")))

(define-public crate-rustlr-0.2.97 (c (n "rustlr") (v "0.2.97") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fjvnk54l65ybdmvpwgxcjl1r6d5r1zlpy14089ymzvkrp8w9g4j")))

(define-public crate-rustlr-0.2.98 (c (n "rustlr") (v "0.2.98") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ccx35pswgr4i7h15416bz894kxf87h1zr3irzak8iyqfxp8i6b4")))

(define-public crate-rustlr-0.2.99 (c (n "rustlr") (v "0.2.99") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "076mzv8dsb79kpx105zhzw8kbbn7xw3irzr7nijbljvmlw7yw0g0")))

(define-public crate-rustlr-0.3.0 (c (n "rustlr") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00czvwrrgxvvy6vxjxsr1rj25a1sljv6rzgc6lwphp4ngvmrw62s")))

(define-public crate-rustlr-0.3.1 (c (n "rustlr") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0x6fpyj2b4pbj8k44mkqg4h31a3bqzdpimrni2y1ga8hh49hvm2m")))

(define-public crate-rustlr-0.3.2 (c (n "rustlr") (v "0.3.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01wbl3raq6qg6r3nh8s9dpryvwhyldadnbay3i8q32j8v8382ydj")))

(define-public crate-rustlr-0.3.3 (c (n "rustlr") (v "0.3.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k50lq3d44wbdhnxvnrcz3w9n0xmxmkr3mi4x0cp0az0qynn07kn")))

(define-public crate-rustlr-0.3.4 (c (n "rustlr") (v "0.3.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04i5snmv9pak69ir6anlvg8pg0lj0hjhq9wgk5ysfd8brvbi46sm")))

(define-public crate-rustlr-0.3.5 (c (n "rustlr") (v "0.3.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08jgaf8j095wx922arb7cqiysybr8v66zb3a3kffp0ddznzhmnlq")))

(define-public crate-rustlr-0.3.6 (c (n "rustlr") (v "0.3.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gkwhmayg258l33bq8bx53811b62z07d57f5lvbyndgcar9i659m")))

(define-public crate-rustlr-0.3.7 (c (n "rustlr") (v "0.3.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "007aph35frdzrvy3ghjchzlwxxvyd2qp3pszaqqazcld2727z682")))

(define-public crate-rustlr-0.3.8 (c (n "rustlr") (v "0.3.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08i22nhzv7ivz2sqpb33j3xgqi4d0gcsn5dn5lnlfywxs0vjyb05")))

(define-public crate-rustlr-0.3.9 (c (n "rustlr") (v "0.3.9") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16srq251ha6xz34fh6jq2nm78db13smw0sxw33sdnlsc33n7x23z")))

(define-public crate-rustlr-0.3.91 (c (n "rustlr") (v "0.3.91") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jhr1mz5jdrm8mwcdgy4zzirn5h646b0xhmazz7is0y24bd39akn")))

(define-public crate-rustlr-0.3.92 (c (n "rustlr") (v "0.3.92") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bjpaqw8g0znrhq0v0imz7a2yjyhff5957hrfvzw2fh91nnzbrlm")))

(define-public crate-rustlr-0.3.93 (c (n "rustlr") (v "0.3.93") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1z7mv1fyrc8s93zs7r56546x2l6ri5sd3qkkljdv2f245pn9dyv1")))

(define-public crate-rustlr-0.3.94 (c (n "rustlr") (v "0.3.94") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12a1vgl9m7hz2k1ic1cv26wb9pjq77gl9nr7zkva4ddl6x54bw80")))

(define-public crate-rustlr-0.3.95 (c (n "rustlr") (v "0.3.95") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0m02nnaz9vl3w5pfslxz3yxcjsz7li2a1j16mi3cwqryrzk4dr1z")))

(define-public crate-rustlr-0.3.96 (c (n "rustlr") (v "0.3.96") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "075g1k4yrnh7gcp6ly3ykx6c6zdg327k63w3lz1vwcy661966qcw")))

(define-public crate-rustlr-0.3.97 (c (n "rustlr") (v "0.3.97") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jynxl889py5b92971s991yd9f9blrcf70hr85qgzhr2hs1n41m4")))

(define-public crate-rustlr-0.3.98 (c (n "rustlr") (v "0.3.98") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1876169r73yicxg946nch66im6cczxkpgq2divq2ng4idhm4pkld")))

(define-public crate-rustlr-0.3.99 (c (n "rustlr") (v "0.3.99") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11dq3m76jb9mycaz75z6apskk0w01aiaaqd1pv3g23nigi2ziiiy")))

(define-public crate-rustlr-0.4.0 (c (n "rustlr") (v "0.4.0") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "08ms9p4sllk29n11p4sg6518kmmvnbpqz10szvs41zsrkc05zyjj")))

(define-public crate-rustlr-0.4.1 (c (n "rustlr") (v "0.4.1") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0li55pb5298w2gvlfr9gcjh2ppj6zyralp0mw0qm0aw8ybihc9x6")))

(define-public crate-rustlr-0.4.2 (c (n "rustlr") (v "0.4.2") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0g0fa0lp4smkv2rzx7b0yb18hvwbif4vg4qrgimbzbfkfi28srh2")))

(define-public crate-rustlr-0.4.3 (c (n "rustlr") (v "0.4.3") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b2j4nvagvc8hmmm5f1q2k765flzk67a8z49fryykrxbp7llvm3z")))

(define-public crate-rustlr-0.4.4 (c (n "rustlr") (v "0.4.4") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pyqn7gaba1fx2z4220d1y9kzs3w1g19y8rh5jp1qpwnlgzcah3l") (y #t)))

(define-public crate-rustlr-0.4.5 (c (n "rustlr") (v "0.4.5") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r6pj609ii6wkvigk1fbza4jkxgm1w75hfwcsfaq8c86znqh7mr9")))

(define-public crate-rustlr-0.4.6 (c (n "rustlr") (v "0.4.6") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01hriw225scb9xzfa8xrcmpssrgkblg6g1pc31v7qrlhyqd8aagj")))

(define-public crate-rustlr-0.4.7 (c (n "rustlr") (v "0.4.7") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lz9yv7jx6khn7idhxzdyqrl4bki0mz26a0rbaxbijsq5x9037yh")))

(define-public crate-rustlr-0.4.8 (c (n "rustlr") (v "0.4.8") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cmwn76akai6hssc5zqyjqbi1pvqg8aa73b2lyj2qlfik890gaz7")))

(define-public crate-rustlr-0.4.9 (c (n "rustlr") (v "0.4.9") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "125ar7wfg428jk7h9yl89gnlikjkzal59pki5hhhz7266m17kip9")))

(define-public crate-rustlr-0.4.10 (c (n "rustlr") (v "0.4.10") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l9smzcgj4i639f6a5mg017wisx0032lvxssckjg0wbl6rizzw2l")))

(define-public crate-rustlr-0.4.11 (c (n "rustlr") (v "0.4.11") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bp0033ly88sh1qnn1qsbmy3ihxmpi7v2nyw6h05a8rqkhypybbc")))

(define-public crate-rustlr-0.4.12 (c (n "rustlr") (v "0.4.12") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0i3p6sldrdkdc0b5lshng75d29bncnxh8cnxwhmdnd056zksy8vq") (y #t)))

(define-public crate-rustlr-0.4.13 (c (n "rustlr") (v "0.4.13") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0156vrz81c51wkgxpwzmlbn4ibpcfakrh256rz04s0kzk0dwj20m")))

(define-public crate-rustlr-0.5.0 (c (n "rustlr") (v "0.5.0") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0196pd0jhv0qkxdrv8cqgkn9s3pxzc1zwf70j1zj4gr6lpb9h1hs") (f (quote (("legacy-parser") ("generator") ("default" "generator"))))))

(define-public crate-rustlr-0.5.1 (c (n "rustlr") (v "0.5.1") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ki9vpvjlwfiz2m59c0mivwy6aj3adxd2rlx063p2nnn0lkxr4w9") (f (quote (("legacy-parser") ("generator") ("default" "generator"))))))

(define-public crate-rustlr-0.6.0 (c (n "rustlr") (v "0.6.0") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jkxws94gkf6lvvmi68m84i8b127rg7sfwv04469nf42ija2zxrb") (f (quote (("legacy-parser") ("generator") ("default" "generator"))))))

