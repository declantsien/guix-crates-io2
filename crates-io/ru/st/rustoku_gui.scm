(define-module (crates-io ru st rustoku_gui) #:use-module (crates-io))

(define-public crate-rustoku_gui-0.1.0 (c (n "rustoku_gui") (v "0.1.0") (d (list (d (n "druid") (r "^0.7.0") (f (quote ("im"))) (d #t) (k 0)) (d (n "rustoku") (r "^0.1.0") (d #t) (k 0)))) (h "1ffmf46vyqyn4vmfhs7cdyq96jl13zzgiash2fn24l79zbwfdld1")))

