(define-module (crates-io ru st rust_observable_literal) #:use-module (crates-io))

(define-public crate-rust_observable_literal-0.1.0 (c (n "rust_observable_literal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "0mnbclnm357g8kngq8k8nmd2rqgy8jjv6h674slc87w8lsvz7alh")))

