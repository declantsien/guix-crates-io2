(define-module (crates-io ru st rust-graph) #:use-module (crates-io))

(define-public crate-rust-graph-0.0.1 (c (n "rust-graph") (v "0.0.1") (h "0n58wg70n7qpv50mjxhsvbgx9pjv9smxwzrpa8lnlamr17y1p0an")))

(define-public crate-rust-graph-0.0.2 (c (n "rust-graph") (v "0.0.2") (h "134dpxk7p0n7763w46wm5islkr8r9zmg6li5glswl4p33yk9qw3j")))

(define-public crate-rust-graph-0.0.3 (c (n "rust-graph") (v "0.0.3") (h "1rcdsvz60pfdvszcpg5h95svcjzp9aq4agk95vrbjd2i3w05c361")))

