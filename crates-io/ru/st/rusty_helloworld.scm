(define-module (crates-io ru st rusty_helloworld) #:use-module (crates-io))

(define-public crate-rusty_helloworld-0.1.0 (c (n "rusty_helloworld") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "18f1kbz3h3x31acgbd4zlh3n6gig66hfch07qg2y4abzxnnilhyy")))

