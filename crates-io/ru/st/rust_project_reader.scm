(define-module (crates-io ru st rust_project_reader) #:use-module (crates-io))

(define-public crate-rust_project_reader-0.1.0 (c (n "rust_project_reader") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1wxsrq5b6z2qvdy4daxq7xia6nrpk157crcr8mz679fwnwlyj1py") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.1 (c (n "rust_project_reader") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "182xmn6dp1nvd5gxddn2i9b06w2ab9z34f33r3dm39z4jpf1cks5") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.2 (c (n "rust_project_reader") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "06qn38p83jaaln5687qcsli25z1v4px27ah5x381gw7n2g3rlk2f") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.3 (c (n "rust_project_reader") (v "0.1.3") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1gpdgdm5v1rl7nq58ipzigv4bqd9a5yprmh1ji9yb2fgcgrlqv2d") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.4 (c (n "rust_project_reader") (v "0.1.4") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0qbzpssj9njkvlpqz1wy99cxfnsfvsg78dqs7mgra27s5r6g5ymc") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.5 (c (n "rust_project_reader") (v "0.1.5") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1m6mknxg57b35wbadh6hp2ff3vvqbg15c8qji104d01chq59ng5f") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.6 (c (n "rust_project_reader") (v "0.1.6") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1xs5yax2dqwmsd0awp49klklw9254b1p8sskk0vwx6w3araaicsh") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.7 (c (n "rust_project_reader") (v "0.1.7") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0ppj3imhjm1p3zhyvkzvka03dl2p0j3w5j44qcj0i6vjp7lsph7a") (r "1.68.0")))

(define-public crate-rust_project_reader-0.1.8 (c (n "rust_project_reader") (v "0.1.8") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1zwggxjccf3arv7vj1wsvrgkpnbjlr9k8adqa71p36fnsqwd716c") (r "1.68.0")))

