(define-module (crates-io ru st rustables-macros) #:use-module (crates-io))

(define-public crate-rustables-macros-0.1.0 (c (n "rustables-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "093ygmvwd4w69qiry4p99xvyzm2g4ywf8zx0hxrqhyrwy1fldqxm")))

(define-public crate-rustables-macros-0.1.1-alpha1 (c (n "rustables-macros") (v "0.1.1-alpha1") (d (list (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvvzg4xx0kaqxc1njhcy4pbyvdhyc6cyzy5nhdghs6ykgmdqd3p")))

(define-public crate-rustables-macros-0.1.1 (c (n "rustables-macros") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01rps7wr8522jp2gfxb4cqqdrgi8jzbn1ji1r9zhb6aafl6jlx58")))

