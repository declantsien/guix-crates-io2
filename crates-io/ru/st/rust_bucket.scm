(define-module (crates-io ru st rust_bucket) #:use-module (crates-io))

(define-public crate-rust_bucket-0.1.0 (c (n "rust_bucket") (v "0.1.0") (d (list (d (n "adjective_adjective_animal") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0jh410qq59r4rrc8nqz0c7ps37a84iyaajxin13wp1kaji62wkvm")))

