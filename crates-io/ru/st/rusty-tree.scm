(define-module (crates-io ru st rusty-tree) #:use-module (crates-io))

(define-public crate-rusty-tree-0.0.1 (c (n "rusty-tree") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rusty-kernel-tools") (r "^0.1") (d #t) (k 0)) (d (n "vtkio") (r "^0.6") (d #t) (k 0)))) (h "1p3sizm1zz8c8y4i5y57w5aj8mkakjlfw1sjvqangfhlr6z7hxz0")))

