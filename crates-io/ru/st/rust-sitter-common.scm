(define-module (crates-io ru st rust-sitter-common) #:use-module (crates-io))

(define-public crate-rust-sitter-common-0.1.0 (c (n "rust-sitter-common") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lci50s8xhcs3nghmvk74qvgcf57lsqv8z609rpjvhlmcr3x1inh")))

(define-public crate-rust-sitter-common-0.1.1 (c (n "rust-sitter-common") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16x2zyxm10f1dsxd3vj1kzm4mj8jj0nfzy3wnddhbn3vy0rdrcbb")))

(define-public crate-rust-sitter-common-0.1.2 (c (n "rust-sitter-common") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "019q9i22kjqa2zr2pm159lmvsmyrhq7gyq5j9mwi421iw5p6wkrh")))

(define-public crate-rust-sitter-common-0.2.0 (c (n "rust-sitter-common") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a1gmzy1rm7nslb7dgiarshkjgd5lghghk3c372nykjpmxm8l5dr")))

(define-public crate-rust-sitter-common-0.2.1 (c (n "rust-sitter-common") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jgagfm5rf8sa0b9yl4jqrgi0dafsyh5j2m0i46gkj9maisdp558")))

(define-public crate-rust-sitter-common-0.3.0 (c (n "rust-sitter-common") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08nzmyi43mnq53mmg4yp7xzsysm493pv44v7kdcjdh92w4crj7kd")))

(define-public crate-rust-sitter-common-0.3.1 (c (n "rust-sitter-common") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06hq529mvj8f0cp1m4iynpxfvg82bnmcsx64k5grw5cxmfq9h6fp")))

(define-public crate-rust-sitter-common-0.3.2 (c (n "rust-sitter-common") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f46xdjqsljr6ahgws6gdjn8ixy6n9nvqm6dq54m8aj73xd79sgi")))

(define-public crate-rust-sitter-common-0.3.3 (c (n "rust-sitter-common") (v "0.3.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xvhsq55d2slzs5084q6gcp464z8aiwyhy9k30nxy8ym8mwmqih9")))

(define-public crate-rust-sitter-common-0.3.4 (c (n "rust-sitter-common") (v "0.3.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "151s2wmkf6b5hwikw7qygng55kxprd97qashfh9065vkvaqs1h63")))

(define-public crate-rust-sitter-common-0.4.0 (c (n "rust-sitter-common") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1msnwlnh7fdnm91p3hhv4mpxm9c13bp7x97x3q03zdbscjk1j557") (y #t)))

(define-public crate-rust-sitter-common-0.4.1 (c (n "rust-sitter-common") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qi7d3ib36v3phknixks9pb04p0hc8qxp4zjf0f6igrn5vdh9za9")))

(define-public crate-rust-sitter-common-0.4.2 (c (n "rust-sitter-common") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1466ipkyq7jdgxmrlc7i7vy3jj7q3cidf9gydnirilql87yynndm")))

