(define-module (crates-io ru st rust_sm) #:use-module (crates-io))

(define-public crate-rust_sm-0.1.0 (c (n "rust_sm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (k 0)))) (h "0absbv1nlznjb6pryyp8jc221bfnp8fbii6rrhxakz5yc9q6lrhh")))

(define-public crate-rust_sm-0.1.1 (c (n "rust_sm") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (k 0)))) (h "0ln00pf5bci7xny6bf9x6frkdqdqk71h4zgqa49babrf8w0c7npn")))

(define-public crate-rust_sm-0.1.3 (c (n "rust_sm") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (k 0)))) (h "1avfpms81hylhaa33mvvdn9zzxybmyispd6yh8qigzx83mwjswc0")))

(define-public crate-rust_sm-0.1.4 (c (n "rust_sm") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1hvdkir9xhngk413cc49h4rlabvrsvscazc0bk19k95xpja24iw5")))

(define-public crate-rust_sm-0.1.5 (c (n "rust_sm") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1skxb4hylijc20z7knrjwhr18h51wcf93fwx30xspf0grh10k0q0")))

(define-public crate-rust_sm-0.1.6 (c (n "rust_sm") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "16h3g6ryqw9ynz7iczr9zz997s8nqs746mvqpjdddjpva0c9yr5n")))

(define-public crate-rust_sm-0.1.7 (c (n "rust_sm") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0v77mvjrhlkgqbig1ima7gjarx5iiysf27jbqbpn5xgmkgbzvy2m")))

