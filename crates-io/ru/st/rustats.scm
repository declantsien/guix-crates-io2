(define-module (crates-io ru st rustats) #:use-module (crates-io))

(define-public crate-rustats-0.0.1 (c (n "rustats") (v "0.0.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0akhdr2jfclb6xsqibrsiwxmf8lx5yhsby24ln5bqd26qcp4hprq")))

(define-public crate-rustats-0.0.2 (c (n "rustats") (v "0.0.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0dsklll7jxqka61cl7zfnkm3mzyn017hflbk2k21v5rz6b8m6q4j")))

(define-public crate-rustats-0.0.3 (c (n "rustats") (v "0.0.3") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0s58r27af66q56fa5265qp84smvqr9zk4w9ciqsvd46nx1i8ajfb")))

(define-public crate-rustats-0.0.4 (c (n "rustats") (v "0.0.4") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1pr4ymqz40n2d3gyjrzc14wgvwszhr1m9ahv3v1sw0kr63kvxk5c")))

(define-public crate-rustats-0.0.5 (c (n "rustats") (v "0.0.5") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.11") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0s1c19zk3pnrngbispan7cbyy123dc78m31mxdrqfgc7w3pi3bxa")))

(define-public crate-rustats-0.0.6 (c (n "rustats") (v "0.0.6") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.11") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1g2czg7ra1bh05fzhyqrm6m14sk8fr1z7m4jvdh1kjp4wyfn8abq")))

(define-public crate-rustats-0.0.7 (c (n "rustats") (v "0.0.7") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.11") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0hyn5w5mln2k1gkvv5hdi6g0llrnwd2bc4qsnkii0xd9svxz6abj")))

(define-public crate-rustats-0.0.8 (c (n "rustats") (v "0.0.8") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.11") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "040bfd264b0523afz184z0afwxq2g9n790xnvdlwln7ac5mfhc9z")))

(define-public crate-rustats-0.0.9 (c (n "rustats") (v "0.0.9") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.11") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "18z6iw9a6kl0mv142gnavhq451qarcxfd1x6bhcp4acdvq5vpzbd")))

(define-public crate-rustats-0.0.10 (c (n "rustats") (v "0.0.10") (d (list (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1g48fpizlyfd381wc0vmb1qvyispb03iqsi1sz0qckb0wqb94p4x")))

(define-public crate-rustats-0.1.0 (c (n "rustats") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)))) (h "16p54x6vmr2p1904gdc7724vwaxlmg7k3i0h8vky0d2x6w0ixjwz")))

(define-public crate-rustats-0.1.1 (c (n "rustats") (v "0.1.1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)))) (h "13pbwy5dca1dfdc4h4qrws6j9b94m65ff7qmlbqf3609kcxvfg6j")))

(define-public crate-rustats-0.1.2 (c (n "rustats") (v "0.1.2") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)))) (h "0fs2wya58yqww44iahq8hpqkhvb6j6363qgi9bw6m5l4w74cc41j")))

