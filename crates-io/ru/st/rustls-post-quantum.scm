(define-module (crates-io ru st rustls-post-quantum) #:use-module (crates-io))

(define-public crate-rustls-post-quantum-0.1.0 (c (n "rustls-post-quantum") (v "0.1.0") (d (list (d (n "aws-lc-rs") (r "^1.6") (f (quote ("unstable"))) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "rustls") (r "^0.23.2") (f (quote ("aws_lc_rs"))) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (d #t) (k 2)))) (h "0csv6cd8mwmqmkadspr268w0mj4n78dgf45wf9pmzyw52r6n5j5j")))

