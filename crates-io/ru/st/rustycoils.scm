(define-module (crates-io ru st rustycoils) #:use-module (crates-io))

(define-public crate-rustycoils-0.1.0 (c (n "rustycoils") (v "0.1.0") (d (list (d (n "GSL") (r "^4.0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "12fx1p3yh587c3g9a5h2x2xl0l59n2db1nhgzxs35cwvgmmcryib") (f (quote (("parallel" "rayon" "ndarray") ("default"))))))

(define-public crate-rustycoils-0.1.1 (c (n "rustycoils") (v "0.1.1") (d (list (d (n "GSL") (r "^4.0.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "01cr5bjhni1czq2mvn4xb4n8j1a7xm2cz7j6lmwgiqjambkha01x") (f (quote (("parallel" "rayon" "ndarray") ("default"))))))

