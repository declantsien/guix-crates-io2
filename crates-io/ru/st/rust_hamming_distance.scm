(define-module (crates-io ru st rust_hamming_distance) #:use-module (crates-io))

(define-public crate-rust_hamming_distance-0.1.0 (c (n "rust_hamming_distance") (v "0.1.0") (h "1n5795yvv9gywi42d21a3isijn6g3h7wmyd2x844ql1wkgn14628")))

(define-public crate-rust_hamming_distance-0.1.1 (c (n "rust_hamming_distance") (v "0.1.1") (h "18yfw6c9kh55b3xh08h8zkak7gfwjlxs20mxmmbdwcygy2r12p0f")))

