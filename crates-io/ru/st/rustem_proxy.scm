(define-module (crates-io ru st rustem_proxy) #:use-module (crates-io))

(define-public crate-rustem_proxy-0.1.0 (c (n "rustem_proxy") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.51.0") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0pzhza1nn6j8ibi5jbmy3bg4fnsvj73k34r56262ziwqn6yycjmj")))

(define-public crate-rustem_proxy-0.1.1 (c (n "rustem_proxy") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.51.0") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "19s8r29yc1946dq4r2d17p76yc7laribzk0abfzlb9j20x1g66x4")))

(define-public crate-rustem_proxy-0.1.2 (c (n "rustem_proxy") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.51.0") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wxz91sad9zw8qx2vpsw7sky227brdrym4w53izkq87czj3d7j9a")))

(define-public crate-rustem_proxy-0.1.3 (c (n "rustem_proxy") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.51.0") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03s2i47r7gcrpbjv56069vjmalh9vld24hs725ng6pbzvqmw5n4b")))

(define-public crate-rustem_proxy-0.1.4 (c (n "rustem_proxy") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.51.0") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1bks236ckmwdkcbk0j6iml8v90a1xgg2rfh1kf8vmghkgbz1f45l")))

(define-public crate-rustem_proxy-0.1.5 (c (n "rustem_proxy") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("wininet"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winreg") (r "^0.51.0") (f (quote ("transactions"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0bgq06jxq016q6vk984b2q9byd7bidk36gv9xjwlg97npyabs9j3")))

