(define-module (crates-io ru st rusty-boy-advance) #:use-module (crates-io))

(define-public crate-rusty-boy-advance-0.0.0 (c (n "rusty-boy-advance") (v "0.0.0") (d (list (d (n "beryllium") (r "^0.0.11") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "10x4h7jrc3k74wp1jc6hib62q5cy0ps8cf9nfvij91q7n5cz9dkl") (y #t)))

(define-public crate-rusty-boy-advance-0.0.1 (c (n "rusty-boy-advance") (v "0.0.1") (d (list (d (n "beryllium") (r "^0.0.11") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1jjnd8dyg9zg3fxx4mr3f2dc67h5lxw24bmj61wd0vfrjfqm40ma")))

(define-public crate-rusty-boy-advance-0.0.2 (c (n "rusty-boy-advance") (v "0.0.2") (h "13nnxbhv91l36zlb688dvrrpbarw5g5sdhq8sr8wrsxk6xfyz7im")))

(define-public crate-rusty-boy-advance-0.0.3 (c (n "rusty-boy-advance") (v "0.0.3") (h "1pzj89rjp4pjmyv5qj7qwxffbf3y35wmf65b88rwfxky70668vnd") (f (quote (("std") ("default" "std"))))))

(define-public crate-rusty-boy-advance-0.0.4 (c (n "rusty-boy-advance") (v "0.0.4") (h "06icbknnymbxlcprs09fwx80ljpv9vl4vfd6c306lci25gmbis6i") (f (quote (("std") ("default" "std"))))))

