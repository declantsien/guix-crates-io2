(define-module (crates-io ru st rustypex) #:use-module (crates-io))

(define-public crate-rustypex-0.4.3 (c (n "rustypex") (v "0.4.3") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "include-flate") (r "^0.1.4") (f (quote ("stable"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0cn6rpnkdjrji0syk9k25hywsr9h0zhv48v0gax5wzw9lb25ilnq")))

(define-public crate-rustypex-0.4.5 (c (n "rustypex") (v "0.4.5") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "include-flate") (r "^0.1.4") (f (quote ("stable"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1jsixgwzmk4w255g821zglihscsjargsrmz3pdayjf28rnwd6348")))

