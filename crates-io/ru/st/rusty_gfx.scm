(define-module (crates-io ru st rusty_gfx) #:use-module (crates-io))

(define-public crate-rusty_gfx-0.1.0 (c (n "rusty_gfx") (v "0.1.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "10jlx1s2bjljkxwhflm380s851f4qgkpbbmzvbk00v84c18am9ij")))

(define-public crate-rusty_gfx-0.2.0 (c (n "rusty_gfx") (v "0.2.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "1fsqgdc0ddfnla21vf04q77l9pk2k74gwhqq1n6shbp93nzln839")))

(define-public crate-rusty_gfx-0.3.0 (c (n "rusty_gfx") (v "0.3.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cy8c8pbx3w7vmaf6fplkx9ii4w8kamq6ww1lxwv36bj5ws2yzgh")))

(define-public crate-rusty_gfx-0.4.0 (c (n "rusty_gfx") (v "0.4.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.5.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "05q2yzv3nckksha5jh6pqy1sfr2x1f6n693axxn2rfh3ax3ybw46")))

(define-public crate-rusty_gfx-0.4.1 (c (n "rusty_gfx") (v "0.4.1") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.5.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1371fp4sxdnb18b7ndqpa5sz30ah3kyggmmfs58rqxc8g1vydpn8")))

(define-public crate-rusty_gfx-0.5.0 (c (n "rusty_gfx") (v "0.5.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g0q8ql6gpqp8b5dlw3rynhrsiqrx9x0ji6yga2ajm4m1di3l92r")))

(define-public crate-rusty_gfx-0.6.0 (c (n "rusty_gfx") (v "0.6.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "121by57a90bmmjs2i69j91p3cz62knfirv85gylh3d2i32srbbqw")))

(define-public crate-rusty_gfx-0.7.0 (c (n "rusty_gfx") (v "0.7.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "10rfchk3q9f5z1bdsbdfqqqhi6kbxxzdlmcpaygdyqikn697mp94")))

(define-public crate-rusty_gfx-0.8.0 (c (n "rusty_gfx") (v "0.8.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1139hkargrch42h04fppl2wdk0hhg7q6bjlga509s81fbccsc8bx")))

(define-public crate-rusty_gfx-0.9.0 (c (n "rusty_gfx") (v "0.9.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vp7ynvpk19dmma0h99f8sd6gn8z1nx9qmczxdr1ixxwii6qknfp")))

(define-public crate-rusty_gfx-0.10.0 (c (n "rusty_gfx") (v "0.10.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jg12j87wl3xabnqz9nddvhilb7rkvnsqiqc8k9ryjy2awskpfla")))

(define-public crate-rusty_gfx-0.11.0 (c (n "rusty_gfx") (v "0.11.0") (d (list (d (n "glium") (r "^0.26.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "16yq5f1kwyhj9zpqc1sf0rpvla0dgn350a7s7rddl6rql4b9sn9w")))

(define-public crate-rusty_gfx-0.11.1 (c (n "rusty_gfx") (v "0.11.1") (d (list (d (n "glium") (r "^0.29.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty_core") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ki1b245p48lfyip1y60ykrad3iz9y84qqnmnx7b18ap1k97hgvq")))

(define-public crate-rusty_gfx-0.11.2 (c (n "rusty_gfx") (v "0.11.2") (d (list (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusty_core") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r885w2bfqjkcqb5ilryi46hmjqj1gzwlidfpihjalkvgchnrwma")))

