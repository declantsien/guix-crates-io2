(define-module (crates-io ru st rust-blas) #:use-module (crates-io))

(define-public crate-rust-blas-0.1.0 (c (n "rust-blas") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "116lrc4ia35mzr01f74c0rzsgg6mqza1jn06ds5307hwjqb220ba")))

(define-public crate-rust-blas-0.1.1 (c (n "rust-blas") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1xb45d9l4y7gqvy0ga1slnzwhdzh8h9zk6lglcy4wn4m54idbnix")))

(define-public crate-rust-blas-0.1.3 (c (n "rust-blas") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "158sk9b65ywx3zz0cw4pxynnv56djb75chisndscnpfa2crdy60w")))

(define-public crate-rust-blas-0.2.0 (c (n "rust-blas") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "14npklm0fqgfalr1dg5yqkqzwzd6yi39fwki2mi963i0nnfflj1f")))

