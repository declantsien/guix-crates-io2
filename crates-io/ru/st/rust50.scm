(define-module (crates-io ru st rust50) #:use-module (crates-io))

(define-public crate-rust50-0.1.0 (c (n "rust50") (v "0.1.0") (h "10i3aw2cjf3bhc0zbg1rz3ia7lmjivdnvm4dparznsk2ysl952j2")))

(define-public crate-rust50-0.1.1 (c (n "rust50") (v "0.1.1") (h "0j10j18vwvksz0h7d011hp3b14rx757fdjscflbd8rbynvp5z2gk")))

(define-public crate-rust50-0.1.2 (c (n "rust50") (v "0.1.2") (h "1nsswg3xkf7li7gb3y2r6dqrf7sf8gcqkni2vh2sp8248hmqani1")))

(define-public crate-rust50-0.1.3 (c (n "rust50") (v "0.1.3") (h "1bbar6j1z6fwjbndrj7qqxk0scsq9sza0lfyb8frk6jwdrvzaayd")))

(define-public crate-rust50-0.1.4 (c (n "rust50") (v "0.1.4") (h "0x6vix5cppf5igs4cnzsjc49jnqhrwwgg5x874lnq7hxmvkassn8")))

(define-public crate-rust50-0.1.5 (c (n "rust50") (v "0.1.5") (h "0ys31nd8cafsc3phmjkjfsgwxvrlaj8nymg0n1kpy061y59kskrz")))

(define-public crate-rust50-0.2.0 (c (n "rust50") (v "0.2.0") (h "1acrbwmrpvch5lwb9cfr66ms334bmzrrn4fnc5privqslb5jkv82")))

(define-public crate-rust50-0.2.1 (c (n "rust50") (v "0.2.1") (h "18nvpg0nyh1445q6b17m17grckjsrfk9sm2dih8g3z4rdvlsff1q")))

