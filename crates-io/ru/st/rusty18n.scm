(define-module (crates-io ru st rusty18n) #:use-module (crates-io))

(define-public crate-rusty18n-0.1.0 (c (n "rusty18n") (v "0.1.0") (d (list (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "deep-struct-update") (r "^0.1.0") (d #t) (k 0)))) (h "12vaaisl66x7b9z5a136qn90krf2m58scbrzyrnh7rq702clskxl")))

(define-public crate-rusty18n-0.1.1 (c (n "rusty18n") (v "0.1.1") (d (list (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "deep-struct-update") (r "^0.1.0") (d #t) (k 0)))) (h "01apiph9inbl6pmss01hg1l2xh3dyfl148zq8jwzcpvjyymlg3a2")))

(define-public crate-rusty18n-0.1.2 (c (n "rusty18n") (v "0.1.2") (d (list (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "deep-struct-update") (r "^0.1.0") (d #t) (k 0)))) (h "100h328kvr9cky21z54giazykfvamckw7gcr9c3ngbp62857ax77")))

(define-public crate-rusty18n-0.1.3 (c (n "rusty18n") (v "0.1.3") (d (list (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "deep-struct-update") (r "^0.1.0") (d #t) (k 0)))) (h "0c9vrqabq55ixi5ksl0k49g6bpnk3sqbrfpw1gl3d3m87wv0phy1")))

(define-public crate-rusty18n-0.1.4 (c (n "rusty18n") (v "0.1.4") (d (list (d (n "bevy_reflect") (r "^0.12.1") (d #t) (k 0)) (d (n "deep-struct-update") (r "^0.1.0") (d #t) (k 0)))) (h "0fnz09zfnj6ds1zy0gmjrrms2j3g7ns2a55vkd9bic2ran0sdp90")))

(define-public crate-rusty18n-0.1.5 (c (n "rusty18n") (v "0.1.5") (d (list (d (n "bevy_reflect") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "1hamypjml80f7d2dnw6l1s5w9xqyivrlcs3mqfy540wvnypd81pl")))

