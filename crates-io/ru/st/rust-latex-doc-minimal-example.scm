(define-module (crates-io ru st rust-latex-doc-minimal-example) #:use-module (crates-io))

(define-public crate-rust-latex-doc-minimal-example-0.1.0 (c (n "rust-latex-doc-minimal-example") (v "0.1.0") (h "0ppgxpx57yls55l9chhwajgv3039pcb159c7ly3ipljc4bpxxwp3")))

(define-public crate-rust-latex-doc-minimal-example-0.1.1 (c (n "rust-latex-doc-minimal-example") (v "0.1.1") (h "0lzfy82yp8md9id74wx57i2s3sr2w68ppn1x5y17ydg9wrrp386w")))

(define-public crate-rust-latex-doc-minimal-example-0.1.2 (c (n "rust-latex-doc-minimal-example") (v "0.1.2") (h "0k4zhj3p6dj7rm36w5619lhx2a3xv566q7crmdqap88jlxgvxwz8")))

(define-public crate-rust-latex-doc-minimal-example-0.2.0 (c (n "rust-latex-doc-minimal-example") (v "0.2.0") (h "0hvm688ifbdqwk5bhhmdmfy167k34c8a0w8bkf2p9h717kjrlccq")))

