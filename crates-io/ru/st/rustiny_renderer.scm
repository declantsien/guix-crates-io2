(define-module (crates-io ru st rustiny_renderer) #:use-module (crates-io))

(define-public crate-rustiny_renderer-0.1.0 (c (n "rustiny_renderer") (v "0.1.0") (d (list (d (n "rustiny_computer_graphic") (r "^0.1.0") (d #t) (k 0)) (d (n "rustiny_xfile") (r "^0.1.0") (d #t) (k 0)))) (h "1sc9gs5axfgv59lzy91lblnd16sy46ljd6583rp8qz2rsp376sym")))

(define-public crate-rustiny_renderer-0.2.0 (c (n "rustiny_renderer") (v "0.2.0") (d (list (d (n "rustiny_computer_graphic") (r "^0.2.0") (d #t) (k 0)) (d (n "rustiny_fixed_point") (r "^0.2.0") (d #t) (k 0)) (d (n "rustiny_xfile") (r "^0.2.0") (d #t) (k 0)))) (h "04w9m0pzq683z9d5mz4ipd5nxylsfklj9nmj9gwabcn0n8k38j1l")))

