(define-module (crates-io ru st rust-riemann_health) #:use-module (crates-io))

(define-public crate-rust-riemann_health-0.1.0 (c (n "rust-riemann_health") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "riemann_client") (r "^0.7.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.4") (d #t) (k 0)))) (h "0aplv1w7zgb5q2yq03q7l2j2invsfy275zrmv7h47mhjwpimmjzq")))

(define-public crate-rust-riemann_health-0.2.0 (c (n "rust-riemann_health") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "^1.4.3") (d #t) (k 0)) (d (n "riemann_client") (r "^0.7.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.4") (d #t) (k 0)))) (h "1iqvn7ig5wap0nniqxn8678r19d7r626rrwvxhj77d1hys2p3h6f")))

