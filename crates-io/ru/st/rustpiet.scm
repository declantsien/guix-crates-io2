(define-module (crates-io ru st rustpiet) #:use-module (crates-io))

(define-public crate-rustpiet-0.1.0 (c (n "rustpiet") (v "0.1.0") (d (list (d (n "image") (r "^0.14.0") (d #t) (k 0)))) (h "0z8va2x63l54fy706b811vnw5amvqayaqzypvsqzbbb6qcqqpzlq")))

(define-public crate-rustpiet-0.1.1 (c (n "rustpiet") (v "0.1.1") (d (list (d (n "image") (r "^0.14.0") (d #t) (k 0)))) (h "0d5hi1j8bdfzq4as1qjqaafgmp6pczln6643nwqkdap1d3h7nxxk")))

