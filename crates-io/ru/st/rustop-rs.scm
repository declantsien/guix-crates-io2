(define-module (crates-io ru st rustop-rs) #:use-module (crates-io))

(define-public crate-rustop-rs-0.4.2 (c (n "rustop-rs") (v "0.4.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "12af0hij93zk73z29fgpvgjm91c8jkksfnwlmh1vjysvgwx7xkrv")))

(define-public crate-rustop-rs-0.4.3 (c (n "rustop-rs") (v "0.4.3") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1vc2fmv8r9cj6mpm5ws606alzdm1isldlglc6flgn2hhl3hjqfg8")))

(define-public crate-rustop-rs-0.4.4 (c (n "rustop-rs") (v "0.4.4") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1c6cr9p9wd87hs06nib3whq3glv2rm3g1jpmfklpj73hdxjnmwi6")))

(define-public crate-rustop-rs-0.4.5 (c (n "rustop-rs") (v "0.4.5") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "0yfnykvb4wwmax5qpdzx6fz8j8wxwmpnxvjc1lgf416yzhmydah9")))

(define-public crate-rustop-rs-0.4.6 (c (n "rustop-rs") (v "0.4.6") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rs201lrf8jkyxl6lbij86jhk49vxp2wnkxac22n6z135qhjb969")))

