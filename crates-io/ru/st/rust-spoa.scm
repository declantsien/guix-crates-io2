(define-module (crates-io ru st rust-spoa) #:use-module (crates-io))

(define-public crate-rust-spoa-0.1.0 (c (n "rust-spoa") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1816bg3a3ya4rzq6vsnbxmhd09r9l368wv9lkpqbrv9gl00cr0z5")))

(define-public crate-rust-spoa-0.2.0 (c (n "rust-spoa") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1rcqnxlb0zikvnkxgfr7g7082gd147a4yjcbwgswcl9fv121p01b")))

(define-public crate-rust-spoa-0.2.1 (c (n "rust-spoa") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0g94vl05lc44hpb9md3qcyd640yx7iz5g15rp86wn6c49bv8ssjm")))

(define-public crate-rust-spoa-0.2.2 (c (n "rust-spoa") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0h7yfqmxwzs4ay9kbiiz5j2pnghlf6gkyk0wkmskksj4v4j2s32l")))

(define-public crate-rust-spoa-0.2.3 (c (n "rust-spoa") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ywa9ihd0486r2hci6sqccnwi542h4xkjpx6bw9hw24k2s9hi2pq")))

(define-public crate-rust-spoa-0.2.4 (c (n "rust-spoa") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0dfgf54w5zcq2xd0nfb2lzjcphg5la50w13g3qx7c5xgp54vbqnk")))

