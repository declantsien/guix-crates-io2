(define-module (crates-io ru st rusttype_bugfix_19072017) #:use-module (crates-io))

(define-public crate-rusttype_bugfix_19072017-0.2.1 (c (n "rusttype_bugfix_19072017") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.3.13") (d #t) (k 0)) (d (n "glium") (r "^0.14") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.0.10") (d #t) (k 0)) (d (n "stb_truetype_bugfix_19072017") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 2)))) (h "0cpbk29i87zjnzczvr1b6s1fxwcmvpcgsxf6npbs56mfpqmc0j3f")))

