(define-module (crates-io ru st rust_nb) #:use-module (crates-io))

(define-public crate-rust_nb-0.1.0 (c (n "rust_nb") (v "0.1.0") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "050305g98agzkmlj3k5w2prxwxva1iji89d8ik8pj1j6c4rldyh8")))

(define-public crate-rust_nb-0.1.1 (c (n "rust_nb") (v "0.1.1") (d (list (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_regex") (r "^0.4") (d #t) (k 0)))) (h "044xr9kcl5g9a578jkkybnrla7xnq2c9l6xg4z8k4c4f5mpi8ij6")))

