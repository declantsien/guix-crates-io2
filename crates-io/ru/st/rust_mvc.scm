(define-module (crates-io ru st rust_mvc) #:use-module (crates-io))

(define-public crate-rust_mvc-0.1.0 (c (n "rust_mvc") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (k 0)))) (h "0rn64rhq90yfjj73wa4iv58a97hl8g6czfcc85cxxii7vx1966bl") (y #t)))

(define-public crate-rust_mvc-0.1.1 (c (n "rust_mvc") (v "0.1.1") (h "1ncwpvnjfziv8w6x96b2ri9pmg98hfqch8ncbfdmkagcr7f5fliy")))

