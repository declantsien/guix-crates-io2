(define-module (crates-io ru st rust-argon2) #:use-module (crates-io))

(define-public crate-rust-argon2-0.1.0 (c (n "rust-argon2") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0dlqwqzzrhhb2r8mszvp3j5acd16w4wlwzkkd4wqanj2lb8y9h0z")))

(define-public crate-rust-argon2-0.2.0 (c (n "rust-argon2") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "057zcd9dgh50flzsy7pqy8bfm5nc0a06b2j5dk9pknvi7ny1vrmg")))

(define-public crate-rust-argon2-0.3.0 (c (n "rust-argon2") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0y1qi81h386kl4sanzkgdp5l2aak4fpy0jp3nzbgkxmn0q72k0hl")))

(define-public crate-rust-argon2-0.4.0 (c (n "rust-argon2") (v "0.4.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0qc63ylsfvf7rwvlh47ag6cdkvcd3az8an9m7r6qy1xwn0sxk3k4")))

(define-public crate-rust-argon2-0.5.0 (c (n "rust-argon2") (v "0.5.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1wwz04p51k529nmhgwsrsvrzyl4fl9rghvylr10afi4b4828vvc1")))

(define-public crate-rust-argon2-0.5.1 (c (n "rust-argon2") (v "0.5.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1krjkmyfn37hy7sfs6lqia0fsvw130nn1z2850glsjcva7pym92c")))

(define-public crate-rust-argon2-0.6.0 (c (n "rust-argon2") (v "0.6.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1kh2jvlqkkiblvzq1r53kd9qbx6znb3l1fil6zb17dfwiy4083i0")))

(define-public crate-rust-argon2-0.6.1 (c (n "rust-argon2") (v "0.6.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0dgbqfyj73s33lxlilgqjg4h8xlfhfbr40jcy32cw4ylpl4m2vs1")))

(define-public crate-rust-argon2-0.7.0 (c (n "rust-argon2") (v "0.7.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "05xh5wfxgzq3b6jys8r34f3hmqqfs8ylvf934n9z87wfv95szj1b")))

(define-public crate-rust-argon2-0.8.0 (c (n "rust-argon2") (v "0.8.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0l9dxfp8a2lgxa4dqn2qbqpmq8yxp1r4r20rbkkxnpz27bh6hlrp") (f (quote (("default" "crossbeam-utils"))))))

(define-public crate-rust-argon2-0.8.1 (c (n "rust-argon2") (v "0.8.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "0hka10mf102ap9g08632ik0m4478qdf9na9j4w9g825wmfc3in8k") (f (quote (("default" "crossbeam-utils"))))))

(define-public crate-rust-argon2-0.8.2 (c (n "rust-argon2") (v "0.8.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "069syiy3ncg9yai58p3dalwvgpr1aqisqls0x1sk54vm0wjn3awx") (f (quote (("default" "crossbeam-utils"))))))

(define-public crate-rust-argon2-0.8.3 (c (n "rust-argon2") (v "0.8.3") (d (list (d (n "base64") (r ">=0.13.0, <0.14.0") (d #t) (k 0)) (d (n "blake2b_simd") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "constant_time_eq") (r ">=0.1.4, <0.2.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r ">=0.8.0, <0.9.0") (o #t) (d #t) (k 0)) (d (n "hex") (r ">=0.4.0, <0.5.0") (d #t) (k 2)) (d (n "serde") (r ">=1.0.116, <2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yvqkv04fqk3cbvyasibr4bqbxa6mij8jdvibakwlcsbjh6q462b") (f (quote (("default" "crossbeam-utils"))))))

(define-public crate-rust-argon2-1.0.0 (c (n "rust-argon2") (v "1.0.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fav7dbkkfqlz9349x7nd6rwhi8dwh7rixm6xhf9q0h4jk8n40dm") (f (quote (("default" "crossbeam-utils"))))))

(define-public crate-rust-argon2-1.0.1 (c (n "rust-argon2") (v "1.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1z0v2kcmqdb61jh6y26797x1rlkqx0y57l88z3gnrgphzn9m9255") (f (quote (("default" "crossbeam-utils"))))))

(define-public crate-rust-argon2-2.0.0 (c (n "rust-argon2") (v "2.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sp42kn06wm1mglrfxnv6js0fxjirgdjhhsakrv0xbmk44c9fw8y")))

(define-public crate-rust-argon2-2.1.0 (c (n "rust-argon2") (v "2.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "blake2b_simd") (r "^1.0") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s66kgbvnv5vaq4vlglx587bq93c662whrniz6ycpjb03m9li64x")))

