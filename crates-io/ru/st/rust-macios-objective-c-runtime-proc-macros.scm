(define-module (crates-io ru st rust-macios-objective-c-runtime-proc-macros) #:use-module (crates-io))

(define-public crate-rust-macios-objective-c-runtime-proc-macros-0.2.2 (c (n "rust-macios-objective-c-runtime-proc-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydak7xzc9an23xp9x1iwn93d0v1fwgykg94b8sq19lpqsarcvq6")))

(define-public crate-rust-macios-objective-c-runtime-proc-macros-0.2.4 (c (n "rust-macios-objective-c-runtime-proc-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1dq4q2mzaplkw4zb4l16apqqxrds4m6abc121r8m0mn3ngnk75n0")))

