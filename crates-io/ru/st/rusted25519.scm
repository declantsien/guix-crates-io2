(define-module (crates-io ru st rusted25519) #:use-module (crates-io))

(define-public crate-rusted25519-0.1.0 (c (n "rusted25519") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)))) (h "0ygkqk9cas7xqfjwn5z62mj8jkmw3llpbxv8k9zhj9lvxcpl0q1r")))

(define-public crate-rusted25519-0.1.1 (c (n "rusted25519") (v "0.1.1") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)))) (h "000nsk2dyf65csmn56asabn4mhfkn6xa07042m9ypgvdzdvqm630")))

