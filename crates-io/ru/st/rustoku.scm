(define-module (crates-io ru st rustoku) #:use-module (crates-io))

(define-public crate-rustoku-0.1.0 (c (n "rustoku") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "1hifijq0ji087jf35w0la415qf7i76hi08bpzx6vbiy5p58a0lni")))

(define-public crate-rustoku-0.1.1 (c (n "rustoku") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 2)))) (h "0lfyyw8jgigyr8zgdbpk92vcwi11cz5bx25a7q1s02y5nmnm3wjj")))

