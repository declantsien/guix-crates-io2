(define-module (crates-io ru st rust_cargo_test) #:use-module (crates-io))

(define-public crate-rust_cargo_test-0.1.0 (c (n "rust_cargo_test") (v "0.1.0") (h "1zckxhim2ivav7n81c3kk79scgpdfs4yfr1s56wr5x09vc79y7y1")))

(define-public crate-rust_cargo_test-0.1.1 (c (n "rust_cargo_test") (v "0.1.1") (h "13gm0ak8bzfq1qz5b3xkvskqixikv5bp0wwy82r0vkah99h0sssi")))

