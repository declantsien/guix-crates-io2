(define-module (crates-io ru st rusty-lines) #:use-module (crates-io))

(define-public crate-rusty-lines-0.1.0 (c (n "rusty-lines") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1c779sq2a2wwsm2s50qv697aqasqflk801zb9rsr9cvf1gjlvfl0")))

(define-public crate-rusty-lines-0.2.0 (c (n "rusty-lines") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "18q6zrvr3q6c8h9mfmiimv4vpm26pb4gjdz9hjw6wcbywhi7s4p2")))

(define-public crate-rusty-lines-0.2.1 (c (n "rusty-lines") (v "0.2.1") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "11fvb66axjyg3x6k2b64nh4jlkycd0bjnhc562m9mya1pfyg6w7a")))

(define-public crate-rusty-lines-0.3.0 (c (n "rusty-lines") (v "0.3.0") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rustyline") (r "^14.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "08jfdgb8qyfgdh1jkqd44l0g0w3hmskisrsliarbkp583rd2n24v")))

