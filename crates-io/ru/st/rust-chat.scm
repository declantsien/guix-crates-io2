(define-module (crates-io ru st rust-chat) #:use-module (crates-io))

(define-public crate-rust-chat-0.1.0 (c (n "rust-chat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.6") (d #t) (k 0)))) (h "0l49q74l0k0f6vi0clxi05449whb6rsg1vkz7xkfnwccjg5x4gqn")))

(define-public crate-rust-chat-0.1.1 (c (n "rust-chat") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.6") (d #t) (k 0)))) (h "1p07cym51wa3l8xnicyj0b7f39jpgmkpx60d74ij1sh659d7n04d")))

(define-public crate-rust-chat-0.1.3 (c (n "rust-chat") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.6") (d #t) (k 0)))) (h "16d9c853xr0385hifqiwk1r27y48v498rz44ckbjvswb39ab45q5")))

(define-public crate-rust-chat-0.1.4 (c (n "rust-chat") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.6") (d #t) (k 0)))) (h "1px56xkbkqv56r8gm8dvdh49qlk9cjd5pn08ajvhwkpgb4cswlpp")))

