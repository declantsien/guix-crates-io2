(define-module (crates-io ru st rustypy) #:use-module (crates-io))

(define-public crate-rustypy-0.1.17 (c (n "rustypy") (v "0.1.17") (d (list (d (n "cpython") (r "~0.3.0") (f (quote ("python3-sys"))) (d #t) (k 0)) (d (n "libc") (r "~0.2.62") (d #t) (k 0)) (d (n "syn") (r "~1.0.17") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1xgkng31m72f1fb6n1w5rshpip5ppy0xkvxpnpnl708psmhi4pq4")))

