(define-module (crates-io ru st rustt) #:use-module (crates-io))

(define-public crate-rustt-0.1.0 (c (n "rustt") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "0p4l0k32xfjq3rkz1wry5wi4pppsh0mq4yq2kjmbha13hyk542xd") (y #t)))

(define-public crate-rustt-0.1.1 (c (n "rustt") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "07lc7pydi2yrsnrzwmcfs50vxg8mq2bbfxldvf88ny4i8yj87lh5")))

(define-public crate-rustt-0.1.2 (c (n "rustt") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "0hg240dj1kkv68wa7cwhhq321fjvdvh8zzl9pcsl7khvxhh3ll39")))

