(define-module (crates-io ru st rust-fixed-point-decimal-macros) #:use-module (crates-io))

(define-public crate-rust-fixed-point-decimal-macros-0.1.0 (c (n "rust-fixed-point-decimal-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-core") (r "^0.1.0") (d #t) (k 0)))) (h "0xdh2b30nzlpp69rw9lv0fmrwhrwrmfw7lsszqklfk06lqflxd5b")))

(define-public crate-rust-fixed-point-decimal-macros-0.1.1 (c (n "rust-fixed-point-decimal-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-core") (r "^0.1.0") (d #t) (k 0)))) (h "1jnbvwzhm7wf0gc6avwsckcik308aaks77iqx0w16c2m31xg0px3")))

(define-public crate-rust-fixed-point-decimal-macros-0.1.2 (c (n "rust-fixed-point-decimal-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-fixed-point-decimal-core") (r "^0.1.0") (d #t) (k 0)))) (h "0gr39bdiijp0b6nys7v0z7n9ski76pa6v91n6gzykdw82av9nskp")))

