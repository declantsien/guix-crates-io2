(define-module (crates-io ru st rust_erl_ext) #:use-module (crates-io))

(define-public crate-rust_erl_ext-0.1.1 (c (n "rust_erl_ext") (v "0.1.1") (d (list (d (n "num") (r ">= 0.1.8") (d #t) (k 0)))) (h "1y7k6jwb5hf3rh627q46ns47549rvrnva8zpcn31lxsmg4qzwrfi")))

(define-public crate-rust_erl_ext-0.2.0 (c (n "rust_erl_ext") (v "0.2.0") (d (list (d (n "byteorder") (r ">= 0.3.11") (d #t) (k 0)) (d (n "num") (r ">= 0.1.25") (d #t) (k 0)))) (h "1p1236x6dqx8fkfswxyriyl5m43ckd28cdmk4y29pbdkd5z82zbs")))

(define-public crate-rust_erl_ext-0.2.1 (c (n "rust_erl_ext") (v "0.2.1") (d (list (d (n "byteorder") (r ">= 0.3.11") (d #t) (k 0)) (d (n "getopts") (r ">= 0.2.12") (d #t) (k 2)) (d (n "num") (r ">= 0.1.25") (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.15") (d #t) (k 2)))) (h "1djvi9lzwj4jm8daiwbkl38vffpfxzfyibzaldrdwmm90yr7b4fz")))

