(define-module (crates-io ru st rusty_textui) #:use-module (crates-io))

(define-public crate-rusty_textui-0.1.0 (c (n "rusty_textui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1j50a4njgsff3lsfi0v9a1hk3y42dbkg8wj5c23kyk7zmv0mcdc7")))

(define-public crate-rusty_textui-0.2.0 (c (n "rusty_textui") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1py9fc98v8qd7ih5kqyg7sph88vs81v2db9q4ld9irhxdr4p60fb")))

(define-public crate-rusty_textui-0.2.1 (c (n "rusty_textui") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0wf6dmlj9zysw31zhr3nd7665j6yg2f4wvcd6g7f8qnnig23jlhw")))

