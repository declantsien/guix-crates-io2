(define-module (crates-io ru st rusty-machine) #:use-module (crates-io))

(define-public crate-rusty-machine-0.0.1 (c (n "rusty-machine") (v "0.0.1") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "0pn8pi9vli17gp6xwacjd49780fjcb2nij1wpp5djjpivvj14lfi")))

(define-public crate-rusty-machine-0.0.2 (c (n "rusty-machine") (v "0.0.2") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "0yafahibk4q1ss0knk0kgdm9p9fylkiwvp10ghlnxhmp7qdfal8y")))

(define-public crate-rusty-machine-0.0.3 (c (n "rusty-machine") (v "0.0.3") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "02720faw6dvl10qvxf9k7xb12fk1xycdjcg83yq0vql0984dlpcq")))

(define-public crate-rusty-machine-0.0.4 (c (n "rusty-machine") (v "0.0.4") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "0sa2d3dmk6c5ybx5108x4vszwx9v014lkn6ydr7yhjgg421hmf8w")))

(define-public crate-rusty-machine-0.0.5 (c (n "rusty-machine") (v "0.0.5") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "0kgmpcsnj5sbvabdc3h2x65zx6s7j9rx9jgm7qcxy4i7gqfnc4h9")))

(define-public crate-rusty-machine-0.0.6 (c (n "rusty-machine") (v "0.0.6") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "0zrw6ivp5p9ajfahh0a6i5gw2pkhnf6wsss4qcn2m5xj136zaa51")))

(define-public crate-rusty-machine-0.0.7 (c (n "rusty-machine") (v "0.0.7") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "0rsxr7irsir1k705qhp8ih1y6yban8imxbl9vwxa03cv8g8qi5q9")))

(define-public crate-rusty-machine-0.0.8 (c (n "rusty-machine") (v "0.0.8") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "15mrgas6rvq95zmggg1yjsynxdw7jyvh1z5qby9zxm5qsgrg4is2")))

(define-public crate-rusty-machine-0.0.9 (c (n "rusty-machine") (v "0.0.9") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "07fdmg4cmkmbxmy5yak2xi2zdfld9gsb3wxvnbiavwdncnf9v3j8")))

(define-public crate-rusty-machine-0.1.0 (c (n "rusty-machine") (v "0.1.0") (d (list (d (n "num") (r "^0.1.28") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0pgi4ci8i9jffqw14zql42fd9z5wfbfjxdjrw0zkwcswrzd67k3s")))

(define-public crate-rusty-machine-0.1.1 (c (n "rusty-machine") (v "0.1.1") (d (list (d (n "num") (r "^0.1.28") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0m7k5gzwyxdcbjmmbirq15r46m1qx2f9bcqypfkjv9rs9q3ss38h")))

(define-public crate-rusty-machine-0.1.2 (c (n "rusty-machine") (v "0.1.2") (d (list (d (n "num") (r "^0.1.28") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0ahwzi3pmbrj219bdn1r600glcx1bvk3amb69glf489vrzl0yi02")))

(define-public crate-rusty-machine-0.1.3 (c (n "rusty-machine") (v "0.1.3") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1991ndzzrr1v9vpcz3hyq83bcklr593a8br6481igg20z2gzi6z3")))

(define-public crate-rusty-machine-0.1.4 (c (n "rusty-machine") (v "0.1.4") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1dbf2i0rkfr5dbrh5cdxl0w7byrix69jdf5s5xjw8qpq5sb1s1qj")))

(define-public crate-rusty-machine-0.1.5 (c (n "rusty-machine") (v "0.1.5") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "13fxwwr9sr8yq8dnchh58jdhw5qi5ygvajp012mnwfl2dzbawzac")))

(define-public crate-rusty-machine-0.1.6 (c (n "rusty-machine") (v "0.1.6") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1dlacsqg9i1qbzkpq8c14xkig63h78jfcpa75s4xliw4rpd7xx0h")))

(define-public crate-rusty-machine-0.1.7 (c (n "rusty-machine") (v "0.1.7") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "17whvkgylfpp9kiiiam468npl93zhzx82cz52vcini6bdgd2zns6") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.1.8 (c (n "rusty-machine") (v "0.1.8") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0i6ifhsb1cs3p1mglykfv53j1cj4kzmbizzmrxwjn5rh9c9gqm46") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.0 (c (n "rusty-machine") (v "0.2.0") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0vwd51fab8qi8wm4sj488smknlaqdwvnn6gwybs6r386n9gdbknh") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.1 (c (n "rusty-machine") (v "0.2.1") (d (list (d (n "num") (r "0.1.*") (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "154kna4l4n3aycw6588mgs9bx5qq6fbxbbv81pw6nhpdmmdxxwaf") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.2 (c (n "rusty-machine") (v "0.2.2") (d (list (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "10gidhhijaix71s7xak27hfksv27xd6l04kq3s47h19bakxhwdhz") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.3 (c (n "rusty-machine") (v "0.2.3") (d (list (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0rmw5lw43raxic3gblqc51a7i00z0iw0sp10b08gy0y8fysi1j42") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.4 (c (n "rusty-machine") (v "0.2.4") (d (list (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1w9zr92cs3mrygcm5kr49yk0nmam3v4rr8v06wacax5g2r0g3sw4") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.5 (c (n "rusty-machine") (v "0.2.5") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1cx6sz1scl7pg4m43ai5pn7lm58niadbw1xkv9clh1ll5762hfg6") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.6 (c (n "rusty-machine") (v "0.2.6") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1szmff791vb8srxqlsfzypsghfd4y42yrsh1gjxabisxfjpspwwm") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.7 (c (n "rusty-machine") (v "0.2.7") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "16p3s4rlrwp5kd2ajvwdigk44invk6h2d83r3r2j3358myzfmf9a") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.2.8 (c (n "rusty-machine") (v "0.2.8") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "190s9dxx65k6c592if0m1flbs1jsrqgnd5g550ijvjddqkldb99a") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.3.0 (c (n "rusty-machine") (v "0.3.0") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "12591kss7y02wqcv6abgmawx9mgcw4r9m06f7v1k0cnp6bl0rrpp") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.3.1 (c (n "rusty-machine") (v "0.3.1") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0fjmim9xw4ix7jgami5lix7f37apb58ph169lg3cyfryfppgyxsi") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.3.2 (c (n "rusty-machine") (v "0.3.2") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "189niz5kxjasm1yx9wsd11wn2j8x9psdj1xq1dfmvd2h212pcla9") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.3.3 (c (n "rusty-machine") (v "0.3.3") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "067s13ngg5905s5f4dzpgqkbgvj3nkf97m34yiqzmilvl53wa23k") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.4.0 (c (n "rusty-machine") (v "0.4.0") (d (list (d (n "num") (r "^0.1.31") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.1.0") (d #t) (k 0)))) (h "1h3byys7l4x2xz6dc3al4vzx1nb6ly3km9qkn0jxrfs5yz86i4w6") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.4.1 (c (n "rusty-machine") (v "0.4.1") (d (list (d (n "num") (r "^0.1.32") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.2.0") (d #t) (k 0)))) (h "1gvnixq3jn1ykghc3xwkd7rvdsj6sjihdsmjizyd097fk3kvjzzx") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.4.2 (c (n "rusty-machine") (v "0.4.2") (d (list (d (n "num") (r "^0.1.32") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.2.0") (d #t) (k 0)))) (h "0wwm4mvij8xjnsl357qfhj2azdi8g6sq68axyj9wldszmainq3y0") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.4.3 (c (n "rusty-machine") (v "0.4.3") (d (list (d (n "num") (r "^0.1.34") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.2.1") (d #t) (k 0)))) (h "08ca2kjswycy9p64fg95whbx6l95akbhkwj8kn43zhb0b2xknz64") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.4.4 (c (n "rusty-machine") (v "0.4.4") (d (list (d (n "num") (r "^0.1.34") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.2.1") (d #t) (k 0)))) (h "1hjgvg82cfyxf7221zhll1aca4sk0r12hxphbdl5m68sf7z7w1hs") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.5.1 (c (n "rusty-machine") (v "0.5.1") (d (list (d (n "num") (r "^0.1.35") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.3") (d #t) (k 0)))) (h "0jy7mcb5pbdaxiq3k7z8iifs76picrjjmwxylvgz7mcxfz4x9vln") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.5.2 (c (n "rusty-machine") (v "0.5.2") (d (list (d (n "num") (r "^0.1.35") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.3") (d #t) (k 0)))) (h "1jq8i3a7klsp0v68n3hd346ldp7jmdy1q5npmfiywqngg8q9yiw8") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.5.3 (c (n "rusty-machine") (v "0.5.3") (d (list (d (n "num") (r "^0.1.35") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.7") (d #t) (k 0)))) (h "13rxjl0lr18nw8yhkdcys9zwiza2csgdwk1hnwd9izy536gndhxs") (f (quote (("stats"))))))

(define-public crate-rusty-machine-0.5.4 (c (n "rusty-machine") (v "0.5.4") (d (list (d (n "num") (r "^0.1.35") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.7") (d #t) (k 0)))) (h "0wcizc34cywmac9ag8d18fiaw2lvid6b1a79xrd87l5v6s13brfy") (f (quote (("stats") ("datasets"))))))

