(define-module (crates-io ru st rustrix) #:use-module (crates-io))

(define-public crate-rustrix-0.1.0 (c (n "rustrix") (v "0.1.0") (h "1qnbxksscdcwrdljyazrvarr2a0qbsl580mq90g0zsjz2ni75s8z")))

(define-public crate-rustrix-0.1.1 (c (n "rustrix") (v "0.1.1") (h "0hq8yznbrq6wdxrhzh2v5r94pkxhxl0iqppvrvdgn9ca7h7z929b")))

(define-public crate-rustrix-0.1.2 (c (n "rustrix") (v "0.1.2") (h "0lkpch1pkcq1cf5303z12993axzlpdq2cs13mg5yzisb84250lx0")))

