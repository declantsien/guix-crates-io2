(define-module (crates-io ru st rust-ai-generator) #:use-module (crates-io))

(define-public crate-rust-ai-generator-0.1.5 (c (n "rust-ai-generator") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rust-ai") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l41yv8x3l30djvhd5irq63sfkbpszf4qgcyvnapamzf39dia943")))

(define-public crate-rust-ai-generator-0.1.6 (c (n "rust-ai-generator") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rust-ai") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "078b5sw7bsqnw7bd976yd3m8aaqhb0pc2vvsi4zgbjsk35ah6yrj")))

(define-public crate-rust-ai-generator-0.1.7 (c (n "rust-ai-generator") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rust-ai") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hzvz75r9129qxmxyw1yzzsyfm3lwy9hkq3245pa4qfp2z3nk6fr")))

(define-public crate-rust-ai-generator-0.1.8 (c (n "rust-ai-generator") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "rust-ai") (r "^0.1.8") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zi89wg6d9xjkrfx77jjifgffj5k1wddlshwzkh5binyqvsiiaqb")))

(define-public crate-rust-ai-generator-0.1.9 (c (n "rust-ai-generator") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cv82d7icjc7w9xl1h9f0cx53bhv8cpsybbpd7kzbpclxv9izl3w")))

