(define-module (crates-io ru st rust-cli-commands) #:use-module (crates-io))

(define-public crate-rust-cli-commands-0.1.0 (c (n "rust-cli-commands") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "mobc") (r "^0.5.7") (d #t) (k 0)) (d (n "mobc-redis") (r "^0.5.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "136xkmk6qww5a1j7zd0z842s2iaynxi5mz6bvskdn5vnykax1vbg")))

