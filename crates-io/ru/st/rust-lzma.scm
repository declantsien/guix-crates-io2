(define-module (crates-io ru st rust-lzma) #:use-module (crates-io))

(define-public crate-rust-lzma-0.1.0 (c (n "rust-lzma") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "16p0v1z3kps484kq86d4jw5n37xiz8asrsv66sczn3mn3lhsi5k3")))

(define-public crate-rust-lzma-0.1.1 (c (n "rust-lzma") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pk7p7s3y0bag3yiqpzxwi7ric6xkrpdbfm5540k6hkyai016fai")))

(define-public crate-rust-lzma-0.2.1 (c (n "rust-lzma") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19chpasr7gqhjy9c66nr008sg599c7d4wldhipcvm47m89sx6rb7")))

(define-public crate-rust-lzma-0.3.0 (c (n "rust-lzma") (v "0.3.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wk3454vl0fidrmly6idazai1w5w7grgk7jbsfds3rir1nfw4b03")))

(define-public crate-rust-lzma-0.4.0 (c (n "rust-lzma") (v "0.4.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19bvl9c3jhkiajihpyahb2951a7d1rbf56ghagl3w600ilrvd165")))

(define-public crate-rust-lzma-0.5.0 (c (n "rust-lzma") (v "0.5.0") (d (list (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "1vp63s26vf4qqf6hx52ba1d7a7q5mfvqa5vdkvsb70kv65m76adq")))

(define-public crate-rust-lzma-0.5.1 (c (n "rust-lzma") (v "0.5.1") (d (list (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "1njlmh9hq2qg5ssdangwbdkz1lrfj2brf8kfp65k7vmfmr6w0pc9")))

(define-public crate-rust-lzma-0.6.0 (c (n "rust-lzma") (v "0.6.0") (d (list (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "113w8d99pc0603zc5hsfdsfzg3q59y5z407wybby3kpn11b92qkx") (f (quote (("static")))) (l "lzma")))

