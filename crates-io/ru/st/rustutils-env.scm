(define-module (crates-io ru st rustutils-env) #:use-module (crates-io))

(define-public crate-rustutils-env-0.1.0 (c (n "rustutils-env") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0q0p486nmng3a5s6ryzs9sr67nwq4n884crcqg7563v3w0wz4cv1") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

