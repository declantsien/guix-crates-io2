(define-module (crates-io ru st rust_ta-lib) #:use-module (crates-io))

(define-public crate-rust_ta-lib-0.6.0-rc.1.build.0 (c (n "rust_ta-lib") (v "0.6.0-rc.1.build.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "concat-idents") (r "^1.1.5") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "1mv2l4zbyrn5bsyfi7r202naw157kry3rk7yj18iki31xhjxciyd") (y #t)))

(define-public crate-rust_ta-lib-0.6.0-rc.1.build.1 (c (n "rust_ta-lib") (v "0.6.0-rc.1.build.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "concat-idents") (r "^1.1.5") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "01r0lr5dzb4pydq3s6ffymja5az65r55jzjyq9rdg1zl368zw7ln") (y #t)))

(define-public crate-rust_ta-lib-0.6.0-rc.1.build.2 (c (n "rust_ta-lib") (v "0.6.0-rc.1.build.2") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "concat-idents") (r "^1.1.5") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "17361i0vkgpad4zlj9rfpyhv37ryghvqhgpsqb7i7rgly4ic3a0x") (y #t)))

(define-public crate-rust_ta-lib-0.6.0-rc.1.build.3 (c (n "rust_ta-lib") (v "0.6.0-rc.1.build.3") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "concat-idents") (r "^1.1.5") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "1c4c0892izvxpqhmg259810smjfhj8r0f1dgyvsnfhc76l7lqni1")))

(define-public crate-rust_ta-lib-0.6.0-rc.1.build.4 (c (n "rust_ta-lib") (v "0.6.0-rc.1.build.4") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "concat-idents") (r "^1.1.5") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "1nvb1mgmnfyhl923kips4gq3abg3p9lgs1s9ndsg2yid39qrdpxd")))

(define-public crate-rust_ta-lib-0.6.0-rc.1.build.5 (c (n "rust_ta-lib") (v "0.6.0-rc.1.build.5") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "concat-idents") (r "^1.1.5") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "04fa2mr6cbqllhql8xif0py0q5zhhll64n8krfa8zr2nsm1zafkh")))

(define-public crate-rust_ta-lib-0.6.0-rc.1.build.6 (c (n "rust_ta-lib") (v "0.6.0-rc.1.build.6") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "concat-idents") (r "^1.1.5") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 1)))) (h "0hmdg5zpraw3i4vdhb37z10b8skl1axxnafh28hijw18yqwk3h9i")))

