(define-module (crates-io ru st rusty-snake) #:use-module (crates-io))

(define-public crate-rusty-snake-0.1.0 (c (n "rusty-snake") (v "0.1.0") (d (list (d (n "bracket-terminal") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a44y07xpg58jgla9rabbyl2alca0lwhqscyldqzaxznhw5wjx11")))

(define-public crate-rusty-snake-0.1.1 (c (n "rusty-snake") (v "0.1.1") (d (list (d (n "bracket-terminal") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dr1x53knz85iwlmwryfjqs1hfdkbxiakagnad57br2siwplf12i")))

(define-public crate-rusty-snake-0.1.2 (c (n "rusty-snake") (v "0.1.2") (d (list (d (n "bracket-terminal") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11j4fn9h94qsfhi6fwrzzrcili871r7sk2mpncml58w7ybf1nk10")))

