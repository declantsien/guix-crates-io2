(define-module (crates-io ru st rustmex_core) #:use-module (crates-io))

(define-public crate-rustmex_core-0.1.0 (c (n "rustmex_core") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)))) (h "09k9xjwblrchh9qcgx8ddm90gg8nrfqcqq4jpgzz8085kz76zgdv")))

(define-public crate-rustmex_core-0.2.0 (c (n "rustmex_core") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)))) (h "0bvjp4vap51a078yryijf843yxppnkxxz5lclqiqvsrfb4w6dmlx")))

(define-public crate-rustmex_core-0.3.0 (c (n "rustmex_core") (v "0.3.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)))) (h "1ixwjzs0lzhpidws7p535wxapw7agnjvk1jpzgy907sq9838nb09")))

