(define-module (crates-io ru st rust-rectangle-dividing) #:use-module (crates-io))

(define-public crate-rust-rectangle-dividing-0.1.0 (c (n "rust-rectangle-dividing") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "19kp69x11nh9f5rsfl439v1wj3vpfhh74fxzvnc1q47784zbwiha")))

(define-public crate-rust-rectangle-dividing-0.1.1 (c (n "rust-rectangle-dividing") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "1w5p91q6n4nj05xakd1y0vsc70ddgd5qgd0191qzb9wx3lky3c7r")))

(define-public crate-rust-rectangle-dividing-0.1.2 (c (n "rust-rectangle-dividing") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "12nhqxs197d6zh7nr2ijfgzj5n7w8f1ccpg3g0x70yi1dj3dsz9s")))

(define-public crate-rust-rectangle-dividing-0.1.3 (c (n "rust-rectangle-dividing") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)))) (h "0ygck84gq0chrw22jllhndd389iy7fc9vz5qxs9dcf61c6880y92")))

