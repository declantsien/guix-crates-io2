(define-module (crates-io ru st rustblox) #:use-module (crates-io))

(define-public crate-rustblox-0.4.0 (c (n "rustblox") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros"))) (k 2)))) (h "1gy573jlq14cr16y85vq1aijaswj05gygs6rvy19j9hmzifhwlwl") (y #t)))

