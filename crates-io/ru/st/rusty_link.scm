(define-module (crates-io ru st rusty_link) #:use-module (crates-io))

(define-public crate-rusty_link-0.2.0 (c (n "rusty_link") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 2)))) (h "1r8ihwlkv4zdwfxrziwlhphxflvqyx4y19cb82fn3z09h6rizw2d")))

(define-public crate-rusty_link-0.2.1 (c (n "rusty_link") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 2)))) (h "18kz8ilc7ypf6b8wl98znwz9xk6dwdi3bvl4dbzc3bkskiaf5wd4")))

(define-public crate-rusty_link-0.2.2 (c (n "rusty_link") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)))) (h "1kvnvh25ffqp1k8h052yydyay05nbc1xjww7isb8cplz0f3dgrjq")))

(define-public crate-rusty_link-0.2.3 (c (n "rusty_link") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)))) (h "1kmml0qy4j59ir124sf4lf6vr4acyh7k0ia6ajqiby4s08y4zdc9")))

(define-public crate-rusty_link-0.3.0 (c (n "rusty_link") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "cpal") (r "^0.14.0") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0m8m5g2l6iyxn4xrax58bsk3ixfhfis2lhnz9bmvb9k3hjlvk2xy")))

(define-public crate-rusty_link-0.3.1 (c (n "rusty_link") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "cpal") (r "^0.14.0") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0r55bmrknbhwlnq04jd18qn6cpk6hmgp8mhgdsnlvpmbkwha88rs")))

(define-public crate-rusty_link-0.3.2 (c (n "rusty_link") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "cpal") (r "^0.14.0") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "06hd0pwdkjs7hn8hr6575v0a48kgkdqhr917y9yy7gkcf08fz131")))

(define-public crate-rusty_link-0.3.3 (c (n "rusty_link") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "cpal") (r "0.14.*") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1rrblymyb17l60556gb6xa7f3ys1n0g02cg2zqq9nnhlac4zas3h")))

(define-public crate-rusty_link-0.3.4 (c (n "rusty_link") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "cpal") (r "0.14.*") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)))) (h "1s7kkqx1pyfwy49g111b364s2wbqma326nb964gniwj1ggcd2kqq")))

(define-public crate-rusty_link-0.3.5 (c (n "rusty_link") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "cpal") (r "~0.14.1") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)))) (h "0qj78mjap45v5s03m7cywqls3jwsb61g4x72vpp2qmw2wk3dkba6")))

(define-public crate-rusty_link-0.3.6 (c (n "rusty_link") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cpal") (r "~0.14.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)))) (h "0778xcfvap14kcln3hr8qwbh6n0pir58mav7igcassx4a59jfbi1")))

(define-public crate-rusty_link-0.4.0 (c (n "rusty_link") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cpal") (r "~0.15.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)))) (h "0fgqqy7gcj6c8f8q688vg7vgxsqshwswz5v6d1msxjfi6k4w1ym4")))

(define-public crate-rusty_link-0.4.1 (c (n "rusty_link") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "cpal") (r "~0.15.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)))) (h "044lx39pyaq5gq2fzhv9wwyfzyj3hsrqp7ky7z6rrla5783gfpc6")))

(define-public crate-rusty_link-0.4.2 (c (n "rusty_link") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "cpal") (r "~0.15.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)))) (h "1chrq6pnw5p91xpcbm4lx9nk5pyap8vkhrd7d6sdpjrc36mbqs57")))

