(define-module (crates-io ru st rustla) #:use-module (crates-io))

(define-public crate-ruSTLa-0.38.0 (c (n "ruSTLa") (v "0.38.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0qrlj9flnh1wlcjnpjrijm1hywayxf1c4wzksxnnva7yca53qzx6")))

