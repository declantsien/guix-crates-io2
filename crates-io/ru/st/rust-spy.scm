(define-module (crates-io ru st rust-spy) #:use-module (crates-io))

(define-public crate-rust-spy-0.1.0 (c (n "rust-spy") (v "0.1.0") (d (list (d (n "addr2line") (r "^0.21.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "rstack") (r "^0.3.3") (f (quote ("dw"))) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0bl4hmi2zqzv0nczn3qdf0bvwmw6q6a0ih568k8ijcnxx9rhvmks")))

(define-public crate-rust-spy-0.2.0 (c (n "rust-spy") (v "0.2.0") (d (list (d (n "addr2line") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "rstack") (r "^0.3.3") (f (quote ("dw"))) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0bsklghksfwsdgp2i7f1hi6ahyjbwh4d259hvdgjhdwz5b0pi1ca")))

