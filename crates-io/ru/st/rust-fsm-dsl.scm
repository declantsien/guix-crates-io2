(define-module (crates-io ru st rust-fsm-dsl) #:use-module (crates-io))

(define-public crate-rust-fsm-dsl-0.3.0 (c (n "rust-fsm-dsl") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1wjyh9ffkichnihyf3bgvg6jafcrvgr0rwb9cjf7pn9vmy8lrvl6")))

(define-public crate-rust-fsm-dsl-0.4.0 (c (n "rust-fsm-dsl") (v "0.4.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0p0qi2j67ka25rz4pxlwp3dmnwxhv97hc4x1wpnghz1nmr8s9qv5")))

(define-public crate-rust-fsm-dsl-0.5.0 (c (n "rust-fsm-dsl") (v "0.5.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1lrs6rxiwgf06r89w0gqdc53jl0dzfw7188vaqp77j2ihyz3v8ic")))

(define-public crate-rust-fsm-dsl-0.6.0 (c (n "rust-fsm-dsl") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0n4wv8hjvp2f9r9gdd5rvpd6ns4mzrhk074lfiin1qr1jr17q8s4")))

(define-public crate-rust-fsm-dsl-0.6.1 (c (n "rust-fsm-dsl") (v "0.6.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06kr7b11a71bpq8h8vqhk9pf4j9878gssjmhy969w1ql60kv2rla")))

(define-public crate-rust-fsm-dsl-0.6.2 (c (n "rust-fsm-dsl") (v "0.6.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "02aihlisf33sl8m4n1zm4m2lwh5iggdiv5rbclywpwhgg8x7x5m3")))

