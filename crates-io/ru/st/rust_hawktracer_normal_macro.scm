(define-module (crates-io ru st rust_hawktracer_normal_macro) #:use-module (crates-io))

(define-public crate-rust_hawktracer_normal_macro-0.1.0 (c (n "rust_hawktracer_normal_macro") (v "0.1.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hdszaah7gnvvi3x66r003bcx073l0p8lwrjb2470hbj456d3waj") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_normal_macro-0.2.0 (c (n "rust_hawktracer_normal_macro") (v "0.2.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07crvwbv4whw63vsawcj36j9f60rxhfdh805bfa35czx4jn165jm") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_normal_macro-0.3.0 (c (n "rust_hawktracer_normal_macro") (v "0.3.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0xpjyp5b3bxcjfjw7mdq366miz2inmd92g9zbc9i8rqr6b5ffdcr") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_normal_macro-0.4.0 (c (n "rust_hawktracer_normal_macro") (v "0.4.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1igcxgwkws7ar4vf7gvjykdgzyxh34gpgk7z7q4qfc7i24qwmgb6") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_normal_macro-0.4.1 (c (n "rust_hawktracer_normal_macro") (v "0.4.1") (d (list (d (n "rust_hawktracer_sys") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1sfjmipdbb5s498c150czr6wihjlkwwgla2jyg3cs7cyjich0mwa") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("pkg_config" "rust_hawktracer_sys" "rust_hawktracer_sys/pkg_config") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

