(define-module (crates-io ru st rust-toolchain-file) #:use-module (crates-io))

(define-public crate-rust-toolchain-file-0.1.0 (c (n "rust-toolchain-file") (v "0.1.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0sy8rk6vn52f8mkpajvwzwlbks9wj3yb8j1n2fk8fdxpq4bq61s6") (f (quote (("default")))) (r "1.56")))

(define-public crate-rust-toolchain-file-0.1.1 (c (n "rust-toolchain-file") (v "0.1.1") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml_edit") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "174kn69ba304y04l0bg2ag8sgbjwff9lj87n6jrib3mndbvpkljr") (f (quote (("default")))) (r "1.66")))

