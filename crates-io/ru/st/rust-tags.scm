(define-module (crates-io ru st rust-tags) #:use-module (crates-io))

(define-public crate-rust-tags-0.2.0 (c (n "rust-tags") (v "0.2.0") (h "15wxvwk54jr653qkmskb8chg5v4x8hhwb5jdfczsph1chqqhn2zz")))

(define-public crate-rust-tags-0.3.0 (c (n "rust-tags") (v "0.3.0") (h "1fvsack14y80kjw6ff6lfpigg8ckl85xq388mwbgag0fwlrn0m3p")))

(define-public crate-rust-tags-0.3.1 (c (n "rust-tags") (v "0.3.1") (h "0hlwf2jr51n6s2slsfwfx2mplqph9vly3gz17lqrbrkdqiv3x0xf")))

