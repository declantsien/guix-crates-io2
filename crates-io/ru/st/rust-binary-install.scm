(define-module (crates-io ru st rust-binary-install) #:use-module (crates-io))

(define-public crate-rust-binary-install-0.0.1 (c (n "rust-binary-install") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "is_executable") (r "^0.1") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0p4x0d4l7v60pddr2cyrs38ij2xysa82iil5k3x0b1j4xfc2q7p6")))

