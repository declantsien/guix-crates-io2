(define-module (crates-io ru st rustyfetch) #:use-module (crates-io))

(define-public crate-rustyfetch-0.1.0 (c (n "rustyfetch") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "0ff7g016cqwx1jrlk8zzy6q9699ya8q905h7zc6f3df3vp7pdssj")))

(define-public crate-rustyfetch-0.1.1 (c (n "rustyfetch") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "1v19lsq76m5hhcc78p3q0wxk916npcywwg0lvi81pv8d3ianwz6d")))

(define-public crate-rustyfetch-0.1.2 (c (n "rustyfetch") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "1qmadd4d43dp048rawp9y3292agfca58z6692w9hzayn71pcf19k")))

(define-public crate-rustyfetch-0.1.3 (c (n "rustyfetch") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "1i9s8kjaf03j9hbm537g03wmrdrhwmfash36cjqnpjc4g2qyak63")))

(define-public crate-rustyfetch-0.1.4 (c (n "rustyfetch") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "0b5ryh8myhri21yb4wydglvimyip3hhpj9rgl63j29wv3abh9gni")))

(define-public crate-rustyfetch-0.1.5 (c (n "rustyfetch") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "0xbxx2kqfsv5f094gid5vhwwnw289rfw8svbrzscg00vh0z8yd69")))

(define-public crate-rustyfetch-0.1.6 (c (n "rustyfetch") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "0n0c9855f7y5pinchvsipbyb7pshnih1sgp8c304gs5s0vazryfm")))

(define-public crate-rustyfetch-0.1.7 (c (n "rustyfetch") (v "0.1.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (d #t) (k 0)))) (h "0qnmkx0dy0m3bpbai3bzckzp22mgnlbvy4wj8vbidzyb0vypp4xh")))

(define-public crate-rustyfetch-0.1.8 (c (n "rustyfetch") (v "0.1.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "03fab603379bff834gh58xnk4i6gnnddl6lll3hvnbx9ilhqk4mn")))

(define-public crate-rustyfetch-0.1.9 (c (n "rustyfetch") (v "0.1.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "13d1nbhk95xac3ka37i7n0yp7k20x8x94lca7fq17ksc7gkn0yx0")))

(define-public crate-rustyfetch-0.1.91 (c (n "rustyfetch") (v "0.1.91") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "0hasbncjmkikbmpn4flvar2c5ywzq5s5y0mg0qcx63j5s2rzs4yc")))

