(define-module (crates-io ru st rustycss) #:use-module (crates-io))

(define-public crate-rustycss-0.1.0 (c (n "rustycss") (v "0.1.0") (h "19jqlvfkb8ii6wsgq6z60mib89gylg979yf4ri75b93gsw6w730m")))

(define-public crate-rustycss-0.1.1 (c (n "rustycss") (v "0.1.1") (h "0mbyc88miilm97fblzn0g96ki2qw0n3drahlnhmg2jirmx74c68c")))

