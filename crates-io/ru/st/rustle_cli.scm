(define-module (crates-io ru st rustle_cli) #:use-module (crates-io))

(define-public crate-rustle_cli-0.0.1-alpha (c (n "rustle_cli") (v "0.0.1-alpha") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "rustle_lib") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1qn8ravdq4558fz2adpyh1jzq2k60487ybxzx57jn8wgdvw08iyk") (y #t)))

(define-public crate-rustle_cli-0.0.2-alpha (c (n "rustle_cli") (v "0.0.2-alpha") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "rustle") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1ag2m5rln59w7ryrn4bayk505s9nz0sfja7i5ysr9xvkvsjvc0kg")))

(define-public crate-rustle_cli-0.1.0 (c (n "rustle_cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "rustle") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "07x6hn5hspyw24bfmz7x99kzc7bgl5sgss4rbbnb9gvs7gmmh4pf")))

(define-public crate-rustle_cli-0.1.1 (c (n "rustle_cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "rustle") (r "0.1.*") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0m62wvmr175z1ka0d4i3x2rsyjcyx28x8f1ac28hld3y4k4xz55s")))

(define-public crate-rustle_cli-0.1.2 (c (n "rustle_cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "rustle") (r "0.1.*") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0apfjpcaw96085bjdsgmsqgpjvz7hcv4xhfmr68fdxhsapi8mlq0")))

(define-public crate-rustle_cli-0.1.3 (c (n "rustle_cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "rustle") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "1780qd89c5rd9j5gk8gkcvyab10mnyz9s180arxqnv10v47n1683")))

