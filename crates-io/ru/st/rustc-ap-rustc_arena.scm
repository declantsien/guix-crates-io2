(define-module (crates-io ru st rustc-ap-rustc_arena) #:use-module (crates-io))

(define-public crate-rustc-ap-rustc_arena-663.0.0 (c (n "rustc-ap-rustc_arena") (v "663.0.0") (d (list (d (n "rustc_data_structures") (r "^663.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "15j7ncv5zm7s3fk6kcz7bq9xpy9zi07irkk5z67n5b20937klc10")))

(define-public crate-rustc-ap-rustc_arena-664.0.0 (c (n "rustc-ap-rustc_arena") (v "664.0.0") (d (list (d (n "rustc_data_structures") (r "^664.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nn1c4mhjpj9rga8cdgbjj68r5grr2vccgf3pqrb3y09jas86rhc")))

(define-public crate-rustc-ap-rustc_arena-665.0.0 (c (n "rustc-ap-rustc_arena") (v "665.0.0") (d (list (d (n "rustc_data_structures") (r "^665.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "15lid8v1s4z33wlvncnmw3f2pgrzq34k9bk23gqajh8l7n9ksxvq")))

(define-public crate-rustc-ap-rustc_arena-666.0.0 (c (n "rustc-ap-rustc_arena") (v "666.0.0") (d (list (d (n "rustc_data_structures") (r "^666.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "15sxhs801xfwjbpmw9w2hyxwsiycklrslm5mi8xsvqddww4wvqff")))

(define-public crate-rustc-ap-rustc_arena-667.0.0 (c (n "rustc-ap-rustc_arena") (v "667.0.0") (d (list (d (n "rustc_data_structures") (r "^667.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0lparglqvl5y9c23bvwhycqcqy710gwpf6hbx8cn3yj7ymb9m63s")))

(define-public crate-rustc-ap-rustc_arena-668.0.0 (c (n "rustc-ap-rustc_arena") (v "668.0.0") (d (list (d (n "rustc_data_structures") (r "^668.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "155p1qsc6r0r157xlm05yizvpb77qv0z2s4ksz8i3702855x5dvr")))

(define-public crate-rustc-ap-rustc_arena-669.0.0 (c (n "rustc-ap-rustc_arena") (v "669.0.0") (d (list (d (n "rustc_data_structures") (r "^669.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0y1jpw84nszdnabd50dfky125zahr2b73h2g72nmxcfwx40x7kf9")))

(define-public crate-rustc-ap-rustc_arena-670.0.0 (c (n "rustc-ap-rustc_arena") (v "670.0.0") (d (list (d (n "rustc_data_structures") (r "^670.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0r7z226sgajp1qvhnc03xsxv0lk3mm3a5q5wf8800gcf1n657z5z")))

(define-public crate-rustc-ap-rustc_arena-671.0.0 (c (n "rustc-ap-rustc_arena") (v "671.0.0") (d (list (d (n "rustc_data_structures") (r "^671.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mkxf0jlkln837qdq0kb1yxcjbq58hi6jd0sc5wagzrr7hrl2faa")))

(define-public crate-rustc-ap-rustc_arena-672.0.0 (c (n "rustc-ap-rustc_arena") (v "672.0.0") (d (list (d (n "rustc_data_structures") (r "^672.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i9483d9a9qlfb97fj71bpki86sp8ih3i8b83kq3cjp8fq1s656h")))

(define-public crate-rustc-ap-rustc_arena-673.0.0 (c (n "rustc-ap-rustc_arena") (v "673.0.0") (d (list (d (n "rustc_data_structures") (r "^673.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0nlrj6i4n0ydjksfhlydqkmi5bv79rxnlg5n31742vdyjj0g134d")))

(define-public crate-rustc-ap-rustc_arena-674.0.0 (c (n "rustc-ap-rustc_arena") (v "674.0.0") (d (list (d (n "rustc_data_structures") (r "^674.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0a350v4hzcqkbrac5bwsdmbsqs9gw81apj8pkyrw6i1zgkz72lp5")))

(define-public crate-rustc-ap-rustc_arena-675.0.0 (c (n "rustc-ap-rustc_arena") (v "675.0.0") (d (list (d (n "rustc_data_structures") (r "^675.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0myrd2a4jqp8mb6890hir1j0z0m18dpmjrd9q384zsr4h8c17xrv")))

(define-public crate-rustc-ap-rustc_arena-676.0.0 (c (n "rustc-ap-rustc_arena") (v "676.0.0") (d (list (d (n "rustc_data_structures") (r "^676.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "12x8d7brq8rz99msfbwkp9nid2g0fy1pcm3v6qq3raz7ki9nmq7x")))

(define-public crate-rustc-ap-rustc_arena-677.0.0 (c (n "rustc-ap-rustc_arena") (v "677.0.0") (d (list (d (n "rustc_data_structures") (r "^677.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0sw62bi55v0dabfhm40byldzf94zw7v9cfnd4m546n04dq6syn19")))

(define-public crate-rustc-ap-rustc_arena-678.0.0 (c (n "rustc-ap-rustc_arena") (v "678.0.0") (d (list (d (n "rustc_data_structures") (r "^678.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ds2q16dgbwq3dl2ix19h3lyk0smjgdjv4mjsxmd9qrkzv653ads")))

(define-public crate-rustc-ap-rustc_arena-679.0.0 (c (n "rustc-ap-rustc_arena") (v "679.0.0") (d (list (d (n "rustc_data_structures") (r "^679.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xilcmpz5591b492nh8bdw9q9n12jpipizmvs88s2y1qzjl43sg8")))

(define-public crate-rustc-ap-rustc_arena-680.0.0 (c (n "rustc-ap-rustc_arena") (v "680.0.0") (d (list (d (n "rustc_data_structures") (r "^680.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0zlx822slfbzld8p3sxhrx0phhy007cpph982ggsk76zmrn0bw9a")))

(define-public crate-rustc-ap-rustc_arena-681.0.0 (c (n "rustc-ap-rustc_arena") (v "681.0.0") (d (list (d (n "rustc_data_structures") (r "^681.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13sfpkjn9c9j894ac9qrakm8zggi1abxavsw8wj40lyx8m0rqzrj")))

(define-public crate-rustc-ap-rustc_arena-682.0.0 (c (n "rustc-ap-rustc_arena") (v "682.0.0") (d (list (d (n "rustc_data_structures") (r "^682.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "05svffm1gp5y7hnc7gjvizrb3jw77dczn60bwcmc7ymj6bh1p0qh")))

(define-public crate-rustc-ap-rustc_arena-683.0.0 (c (n "rustc-ap-rustc_arena") (v "683.0.0") (d (list (d (n "rustc_data_structures") (r "^683.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "02ip5ybpyfkfig1f4ybj7d693g4nxblvnv4aqw5dj9l09mskmjfi")))

(define-public crate-rustc-ap-rustc_arena-684.0.0 (c (n "rustc-ap-rustc_arena") (v "684.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0g6j3q2bhdx7vq1w4sl5y8xvk24fz717raaivwny04fllxcy2yyp")))

(define-public crate-rustc-ap-rustc_arena-685.0.0 (c (n "rustc-ap-rustc_arena") (v "685.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "067ycdrbmcga89bq93sd3la6k32zr5klynbk90jc2yffnb5pwn57")))

(define-public crate-rustc-ap-rustc_arena-686.0.0 (c (n "rustc-ap-rustc_arena") (v "686.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0x1ij3vi1bxrmj59fl57lhqrmhy84wwcqxw5qs2j1wfjzvp8aw27")))

(define-public crate-rustc-ap-rustc_arena-687.0.0 (c (n "rustc-ap-rustc_arena") (v "687.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0yq7ww95996yzk7ahvs19zy9zkkxqyzq44h6xnppgfh70byqwm6r")))

(define-public crate-rustc-ap-rustc_arena-688.0.0 (c (n "rustc-ap-rustc_arena") (v "688.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1v6dbxin5h9zd9fbh3z09v7a7kvb6k36cw4dja7bjbhdbkn68wcx")))

(define-public crate-rustc-ap-rustc_arena-689.0.0 (c (n "rustc-ap-rustc_arena") (v "689.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "05z8c032qmljhig7lwkvi09fpyq0sq8vkp6igij7pv1nnrh3hkvb")))

(define-public crate-rustc-ap-rustc_arena-690.0.0 (c (n "rustc-ap-rustc_arena") (v "690.0.0") (d (list (d (n "smallvec") (r ">=1.0.0, <2.0.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0b5gg8l9cbzabyrrirhcyx2nnhk802p28653gsghyh3drb1w01xv")))

(define-public crate-rustc-ap-rustc_arena-691.0.0 (c (n "rustc-ap-rustc_arena") (v "691.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0phbdj1gn0n2farza7dj570li3cjz34b8g4363rb2ybgbaybkxw1")))

(define-public crate-rustc-ap-rustc_arena-692.0.0 (c (n "rustc-ap-rustc_arena") (v "692.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ak17sil3y410qgvwcdym1fwcn9v4gvvcgjcjy08iyzv0skfy6hr")))

(define-public crate-rustc-ap-rustc_arena-693.0.0 (c (n "rustc-ap-rustc_arena") (v "693.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1w1wg13kkichcqlh4np6lc29ywkk2dfam0b72nlcq8ircf72mzm4")))

(define-public crate-rustc-ap-rustc_arena-694.0.0 (c (n "rustc-ap-rustc_arena") (v "694.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "08mcimihjsca9qzj9b3nnqx54mvf3kyj3z0wh4fvqdh62si4r368")))

(define-public crate-rustc-ap-rustc_arena-695.0.0 (c (n "rustc-ap-rustc_arena") (v "695.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1x9zi1018yw1f1m0ila6l3082gnir07jlr519dw35cm4wdh9sda3")))

(define-public crate-rustc-ap-rustc_arena-696.0.0 (c (n "rustc-ap-rustc_arena") (v "696.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qz79mg0imfan9r5aaii2kg9qkkrc2gsijniqk1q3fk3lz0g2hl4")))

(define-public crate-rustc-ap-rustc_arena-697.0.0 (c (n "rustc-ap-rustc_arena") (v "697.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "05kmdg72xlz82g0yz6iak6037isq2vakxzd6067lq60643m3p5gv")))

(define-public crate-rustc-ap-rustc_arena-698.0.0 (c (n "rustc-ap-rustc_arena") (v "698.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "112fh7s1yhyp5g633l80hwkr2m9gc143z8l5pq2q6k8z3jnbr93i")))

(define-public crate-rustc-ap-rustc_arena-699.0.0 (c (n "rustc-ap-rustc_arena") (v "699.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0w16n1ykny27jksxrajcdxxx8c6l1fybxwbzr66yk3bnzg97asnh")))

(define-public crate-rustc-ap-rustc_arena-700.0.0 (c (n "rustc-ap-rustc_arena") (v "700.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0g7h3cr755chmzxj6yiyh9jjdkj4y6m5cmnfgj657d2hjzlf8qkb")))

(define-public crate-rustc-ap-rustc_arena-701.0.0 (c (n "rustc-ap-rustc_arena") (v "701.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0y47vvnx94np9249bjywxndlzf75zm800z9kyi59wgzw31rn2f1x")))

(define-public crate-rustc-ap-rustc_arena-702.0.0 (c (n "rustc-ap-rustc_arena") (v "702.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mfgmfpfs335bmmn0nv0f53a1r97r7gag3zfpv81wiz2p0yfzd00")))

(define-public crate-rustc-ap-rustc_arena-703.0.0 (c (n "rustc-ap-rustc_arena") (v "703.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "10vvpl5np7ma3wc3n5q5pdhh4lf3ppc6n00hmj1qx503gml558rw")))

(define-public crate-rustc-ap-rustc_arena-704.0.0 (c (n "rustc-ap-rustc_arena") (v "704.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "18jkw4pidk53khbn939x4avhvj9d1blc6xm0jv9jbz4q7vrs7c11")))

(define-public crate-rustc-ap-rustc_arena-705.0.0 (c (n "rustc-ap-rustc_arena") (v "705.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "04ij2b6f5r6ncsshk6k138jxv7zyc17fm2i0r299n246lbzmlmwk")))

(define-public crate-rustc-ap-rustc_arena-706.0.0 (c (n "rustc-ap-rustc_arena") (v "706.0.0") (d (list (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "05ds47i40idfy8crhc1wjya6lzkd0d4ca2sxx3r9b088cmwsynvh")))

(define-public crate-rustc-ap-rustc_arena-707.0.0 (c (n "rustc-ap-rustc_arena") (v "707.0.0") (d (list (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1h7bs566p2nk2mhbrz3m735j3pwgy1axwvsd3j42ljrccpgbx71m")))

(define-public crate-rustc-ap-rustc_arena-708.0.0 (c (n "rustc-ap-rustc_arena") (v "708.0.0") (d (list (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ikjair3lgxvdrjqy72ffas3pyc5p6248i0yd5znwa2f9fi303vw")))

(define-public crate-rustc-ap-rustc_arena-709.0.0 (c (n "rustc-ap-rustc_arena") (v "709.0.0") (d (list (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qwjxw7s2jrz6liqzdyq9hnhy0cnb525g8qa5hdb34zkkvkr4l3y")))

(define-public crate-rustc-ap-rustc_arena-710.0.0 (c (n "rustc-ap-rustc_arena") (v "710.0.0") (d (list (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "108r2v8wcxc9hzysz545wgh276lr4m24dvjmvy7lbrfbl7ankk09")))

(define-public crate-rustc-ap-rustc_arena-711.0.0 (c (n "rustc-ap-rustc_arena") (v "711.0.0") (d (list (d (n "rustc_data_structures") (r "^711.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0x3791p1z0gx2rx5zvj2n9pnk2ijrfkq1yqqld7m4vjz4va51h2k")))

(define-public crate-rustc-ap-rustc_arena-712.0.0 (c (n "rustc-ap-rustc_arena") (v "712.0.0") (d (list (d (n "rustc_data_structures") (r "^712.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "045n7imchzwby6kmds04zhp7kzrh1hl4bkls53yhbjsyjw7cm715")))

(define-public crate-rustc-ap-rustc_arena-713.0.0 (c (n "rustc-ap-rustc_arena") (v "713.0.0") (d (list (d (n "rustc_data_structures") (r "^713.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0rf3ylp4y0m7m1ma4am90hrwi85g3v5bpia0yrmjfxwkn2i94yyk")))

(define-public crate-rustc-ap-rustc_arena-714.0.0 (c (n "rustc-ap-rustc_arena") (v "714.0.0") (d (list (d (n "rustc_data_structures") (r "^714.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "10l5hx284vmk386vzwkrb5jxk7ch2msmpq5749psh24k145mfr8m")))

(define-public crate-rustc-ap-rustc_arena-715.0.0 (c (n "rustc-ap-rustc_arena") (v "715.0.0") (d (list (d (n "rustc_data_structures") (r "^715.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1xm3ymgjp414asksvyhk79ilcf7hbxvv6i2lp3b33mx1syxqy5s8")))

(define-public crate-rustc-ap-rustc_arena-716.0.0 (c (n "rustc-ap-rustc_arena") (v "716.0.0") (d (list (d (n "rustc_data_structures") (r "^716.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mdyg9vl4d49hxngclrzvgw2klbbhq7ms4aq4xmznqa9lv8j751s")))

(define-public crate-rustc-ap-rustc_arena-717.0.0 (c (n "rustc-ap-rustc_arena") (v "717.0.0") (d (list (d (n "rustc_data_structures") (r "^717.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0n39bsa70br0wipql046snx0pn3lgbzjz90cx6gx8r7ga5pyppjv")))

(define-public crate-rustc-ap-rustc_arena-718.0.0 (c (n "rustc-ap-rustc_arena") (v "718.0.0") (d (list (d (n "rustc_data_structures") (r "^718.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xnniyqsklkibhllwaskgbsmz07ab4xmafc22x0a9vrrf7s10rjj")))

(define-public crate-rustc-ap-rustc_arena-719.0.0 (c (n "rustc-ap-rustc_arena") (v "719.0.0") (d (list (d (n "rustc_data_structures") (r "^719.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16cd1ykwr3wkr8nd8hig7px5haymj792b25ixawi0jjf236crvbg")))

(define-public crate-rustc-ap-rustc_arena-720.0.0 (c (n "rustc-ap-rustc_arena") (v "720.0.0") (d (list (d (n "rustc_data_structures") (r "^720.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1k84jzm9xqirink4d3vgy7nk6nzk3ic4djwm5qsz9j34lb0yc3c1")))

(define-public crate-rustc-ap-rustc_arena-721.0.0 (c (n "rustc-ap-rustc_arena") (v "721.0.0") (d (list (d (n "rustc_data_structures") (r "^721.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "06acshi40i8py8ly30mj137w5x0mvg0m9gwlx7cq52fs5377nnsp")))

(define-public crate-rustc-ap-rustc_arena-722.0.0 (c (n "rustc-ap-rustc_arena") (v "722.0.0") (d (list (d (n "rustc_data_structures") (r "^722.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0wbky791phh4a8d1ygly65p2hfyzbagwh64v12ps0caxjaha232m")))

(define-public crate-rustc-ap-rustc_arena-723.0.0 (c (n "rustc-ap-rustc_arena") (v "723.0.0") (d (list (d (n "rustc_data_structures") (r "^723.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0yrl569l9zz1i8s2xv7b501mrf40wa0kjx1mdwdb0lx1c5gg3ah4")))

(define-public crate-rustc-ap-rustc_arena-724.0.0 (c (n "rustc-ap-rustc_arena") (v "724.0.0") (d (list (d (n "rustc_data_structures") (r "^724.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1gxrgknlp0am75gnaa7sql80c8vs6pqlfkxgk54wb0f351ksgb8y")))

(define-public crate-rustc-ap-rustc_arena-725.0.0 (c (n "rustc-ap-rustc_arena") (v "725.0.0") (d (list (d (n "rustc_data_structures") (r "^725.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1xc8993zmip8582ffk3czcirxvkw5yl6syvf04nmwpd98zch02jr")))

(define-public crate-rustc-ap-rustc_arena-726.0.0 (c (n "rustc-ap-rustc_arena") (v "726.0.0") (d (list (d (n "rustc_data_structures") (r "^726.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i1xgpcglpz0mgw9v974x5fpnbjx3cd0bma7hmpfdyjgzy6palck")))

(define-public crate-rustc-ap-rustc_arena-727.0.0 (c (n "rustc-ap-rustc_arena") (v "727.0.0") (d (list (d (n "rustc_data_structures") (r "^727.0.0") (d #t) (k 0) (p "rustc-ap-rustc_data_structures")) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0h7yxgrhdkck6jcjdlci9sj3k4bgwyncfprvc5gycdcyjpkl7d72")))

