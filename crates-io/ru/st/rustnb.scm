(define-module (crates-io ru st rustnb) #:use-module (crates-io))

(define-public crate-rustnb-0.1.0 (c (n "rustnb") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19mhzrfdc92m253cz84sr4p8n982phcxk59fhwn8mxjg2hqwgf82")))

(define-public crate-rustnb-0.1.1 (c (n "rustnb") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1762fivplx7fqnjcg8ig5zcjs2p5zn2m1knzb3wrycs1vqh0410d")))

(define-public crate-rustnb-0.1.2 (c (n "rustnb") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n6ij23h4jxcd6x7w8nhk55ihkr5lss99czq8q559g69s0bn4a16")))

(define-public crate-rustnb-0.1.3 (c (n "rustnb") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0snvywi1k9wlsic3b716pyzx6k09sq65vfwhbkrj7k3ff7zy1a2l")))

(define-public crate-rustnb-0.1.4 (c (n "rustnb") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i5z3q1nz00xxvwb7gb2l1bg2dp24ivl7h1i8hd8m871scd8i42z")))

