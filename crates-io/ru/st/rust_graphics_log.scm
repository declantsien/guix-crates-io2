(define-module (crates-io ru st rust_graphics_log) #:use-module (crates-io))

(define-public crate-rust_graphics_log-0.1.0 (c (n "rust_graphics_log") (v "0.1.0") (h "0c89byi5q6c8dy8zx40pdfff4jk6xk5vbdzaqg19fqbm44r1dxgd")))

(define-public crate-rust_graphics_log-0.1.1 (c (n "rust_graphics_log") (v "0.1.1") (h "1sfcxaphnchhq3qj62vlv39ilxp8idlq52vbn7a63kmx42cf4kwv")))

(define-public crate-rust_graphics_log-0.1.2 (c (n "rust_graphics_log") (v "0.1.2") (h "065gdmz8gbn8r4zj6r6hjhkfs71k3h38zcj95vrsj58mb9phjvlg")))

(define-public crate-rust_graphics_log-0.1.3 (c (n "rust_graphics_log") (v "0.1.3") (h "1nj5macyzzqsxxgljq8jkp8q1327gxzrhpmm842915mcvigjak45")))

(define-public crate-rust_graphics_log-0.1.4 (c (n "rust_graphics_log") (v "0.1.4") (h "0v24l92hkgp0bvrmsa9qvirbfljgp9js4gcpv203qf1jvkgpd46q")))

(define-public crate-rust_graphics_log-0.1.5 (c (n "rust_graphics_log") (v "0.1.5") (h "0z72gvhn4hxxbwdmpcfdvq21i4wzvhbhnmfrc8x5l5kwyacr256g")))

(define-public crate-rust_graphics_log-0.1.6 (c (n "rust_graphics_log") (v "0.1.6") (h "1ym74c59j4555634i3da3vs1yfj2ypfjkcr6dwvdflh38xxdr0az")))

(define-public crate-rust_graphics_log-0.1.7 (c (n "rust_graphics_log") (v "0.1.7") (h "186v1n9qjg14bbb0g90ygmfjmaq5kdwwb0d0rsllib67s9yqji68")))

