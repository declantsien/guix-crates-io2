(define-module (crates-io ru st rustcastai) #:use-module (crates-io))

(define-public crate-rustcastai-0.1.0 (c (n "rustcastai") (v "0.1.0") (d (list (d (n "curs") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.17") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "09lnjhy35p56vl2k3qz9qhks1nm4fdakx64igy1y75l0p73kfxh1")))

(define-public crate-rustcastai-0.2.0 (c (n "rustcastai") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.17") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1yhwpihsgpdcw2n8ycv2inv37k5cj9xi6klm1rc33yxpdii147kk")))

(define-public crate-rustcastai-0.2.1 (c (n "rustcastai") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "16sv776p0dbddqy91nkj5782f2i20kkgr59hnjlfcsqyvbjr0wpc")))

(define-public crate-rustcastai-0.2.2 (c (n "rustcastai") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "07vld9g62zhz0d7cky5s96bp22m1cqdqkqd5vjsycvd8yqrzg0hv")))

