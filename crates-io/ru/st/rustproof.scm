(define-module (crates-io ru st rustproof) #:use-module (crates-io))

(define-public crate-rustproof-0.1.0 (c (n "rustproof") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "lalrpop") (r "^0.11.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.7") (d #t) (k 0)) (d (n "rustproof-libsmt") (r "^0.1.0") (d #t) (k 0)))) (h "1qb7axig4kw0v8zg3k2rv3c27dxh6kxyhfs9yjm903lnziq88kva")))

