(define-module (crates-io ru st rustminify-cli) #:use-module (crates-io))

(define-public crate-rustminify-cli-0.1.0 (c (n "rustminify-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "rustminify") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1r3abz6wjxmhxrd5qc3q5w877fam91jab0pz0rdw0mjqcbnw7xkk")))

