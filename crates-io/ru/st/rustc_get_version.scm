(define-module (crates-io ru st rustc_get_version) #:use-module (crates-io))

(define-public crate-rustc_get_version-0.1.0 (c (n "rustc_get_version") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0x6hkd39gs5fsb25li9xb4yy7h1j68i79x6i9qisikdwa0jzzkv9")))

(define-public crate-rustc_get_version-0.1.1 (c (n "rustc_get_version") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1wzrj44wajsnwk879w2vzs911zypnj61azw3zkl6y10bc1mmhir2")))

(define-public crate-rustc_get_version-0.1.2 (c (n "rustc_get_version") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0lsz4yvga1ils59ymkb95db84qm86wqizban7mrgn5jd1268ilqs")))

