(define-module (crates-io ru st rusta) #:use-module (crates-io))

(define-public crate-rusta-0.0.1 (c (n "rusta") (v "0.0.1") (h "01h8k5c24x0mm4v9chadw48zchkkinkgadhy01xchvr79msswsbw")))

(define-public crate-rusta-0.0.2 (c (n "rusta") (v "0.0.2") (h "1ml5n5lfpd0kmmfvnwx2gaksy2pnk92lmpcvzpkhcwb2ykjmndws")))

