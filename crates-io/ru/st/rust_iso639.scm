(define-module (crates-io ru st rust_iso639) #:use-module (crates-io))

(define-public crate-rust_iso639-0.0.1 (c (n "rust_iso639") (v "0.0.1") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0wxxyigwgli788jfbvz2dh1m4y376xkmncjg4kbl57frbyczlyd0")))

(define-public crate-rust_iso639-0.0.2 (c (n "rust_iso639") (v "0.0.2") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0jc17j8rw5fsyh2gkagdk5i4l459a523xpczq6sf9kmyz673l72n")))

(define-public crate-rust_iso639-0.0.3 (c (n "rust_iso639") (v "0.0.3") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "07fnd7x6np2fh6lphk80jg133sffpijn34r4id0q340q2aq6w046")))

