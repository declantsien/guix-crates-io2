(define-module (crates-io ru st rustkernel) #:use-module (crates-io))

(define-public crate-rustkernel-0.0.1 (c (n "rustkernel") (v "0.0.1") (h "10b0g2gimplcps2k5846qbqkhdy4dlw04g2f0snb3a98jz64h38j")))

(define-public crate-rustkernel-0.0.2 (c (n "rustkernel") (v "0.0.2") (h "0a2x999rba9av6mqi8q5pxbj18qpfc30kh0wnlbps8p4b6axpmxv")))

(define-public crate-rustkernel-0.0.3 (c (n "rustkernel") (v "0.0.3") (h "1fy0j5a4n0hj5riz11hfql2a2mlih0ic140x1659cicfwjwvgi53")))

(define-public crate-rustkernel-0.0.4 (c (n "rustkernel") (v "0.0.4") (h "1m9jy0cifjw0x3g8339svxld7dk96sndixdzi7rbgqpzxhkjngwp")))

(define-public crate-rustkernel-0.0.5 (c (n "rustkernel") (v "0.0.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "03i0a4x4gy0y1jzkrlc2zn26yf7k2ilxjsc7099zxpyiwcd5l3g7")))

(define-public crate-rustkernel-0.0.6 (c (n "rustkernel") (v "0.0.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0x43vbnplhrn3dyjninaq5j03x148sal1fxmjij2xm9pik6wj05f")))

(define-public crate-rustkernel-0.0.7 (c (n "rustkernel") (v "0.0.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0g09yksiddqzcf8rzjv3alzwppi8q2gfb9p2nwswa40nj62syhms")))

(define-public crate-rustkernel-0.0.8 (c (n "rustkernel") (v "0.0.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0bzb573h52ypspc6k2vwjvdbg4khbypww7vajnwdls5g9b2aw908")))

(define-public crate-rustkernel-0.0.9 (c (n "rustkernel") (v "0.0.9") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "13nwj0bz28x869sf0k79bn1ws145fwj6dwkrw4qiblzywx4iwf82")))

(define-public crate-rustkernel-0.0.10 (c (n "rustkernel") (v "0.0.10") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0v7plm53gssx1rhg1639n5n6w4rkzv4yjlhs94x9ims9jwwxwfpx")))

(define-public crate-rustkernel-0.0.11 (c (n "rustkernel") (v "0.0.11") (h "1xcx5n931d1yczallxlhq9hvjiy95lisgjds5vz062vcg5gjhggm")))

(define-public crate-rustkernel-0.0.12 (c (n "rustkernel") (v "0.0.12") (h "1a4m4ssgg6b38rxs9903wxqqgrif74n2n14szqq1id14bnkz6lf9")))

(define-public crate-rustkernel-0.0.13 (c (n "rustkernel") (v "0.0.13") (h "04kwxri9wvy7mnmypw6f25g8f5h2ss5rzaqr046a52k1c6sj4lmz")))

(define-public crate-rustkernel-0.0.14 (c (n "rustkernel") (v "0.0.14") (h "113nj97iy6b1ab8lah0i2kjx9yi0daa4zb7dyfkaqv0nj25kf54s")))

(define-public crate-rustkernel-0.0.15 (c (n "rustkernel") (v "0.0.15") (h "0r3i7w8imhgfppl7cvi0zdryfggayxs6yiw0spxqnf44j7j7wdqi")))

(define-public crate-rustkernel-0.0.16 (c (n "rustkernel") (v "0.0.16") (h "0ysg5gnkf753lmpzg96xqfz9lzy6qdn8nfxp15gl8lnh9798c971")))

(define-public crate-rustkernel-0.0.17 (c (n "rustkernel") (v "0.0.17") (h "067fhmmgbq69ysd1ak76afvq2bxkjqlcg5qhqa6i0zxrd0lhr93b")))

(define-public crate-rustkernel-0.0.18 (c (n "rustkernel") (v "0.0.18") (h "13a66n595wf4y5ip151p6fxsq4bk2cssnymhnfgar1w7xn4bdjvv")))

(define-public crate-rustkernel-0.0.19 (c (n "rustkernel") (v "0.0.19") (h "1g18kr998q13q2hr2s0mwa132vf3mvpf4xb4aqpzbrhv1mv4zxzb")))

(define-public crate-rustkernel-0.0.20 (c (n "rustkernel") (v "0.0.20") (h "0bq626sjf24r8d8h3c1xiprwsn1hmn3kj8ym2b5mfg59xfk07vhy")))

(define-public crate-rustkernel-0.0.21 (c (n "rustkernel") (v "0.0.21") (h "1d7r3nf557lgiq2i3m5yy32604nknzcfrziggjk098n7cd4aqy6q")))

