(define-module (crates-io ru st rustsynth-derive) #:use-module (crates-io))

(define-public crate-rustsynth-derive-0.3.0 (c (n "rustsynth-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rp6xvrk0p6qd0i7cmb4pjcqcjr6xxdbsyjjwlmhabd2q17qiqa7")))

(define-public crate-rustsynth-derive-0.4.0 (c (n "rustsynth-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bbxscf5d3as4vwkgiqkwlyiswqmrvm0k2aslprycc1a7kczwq9m")))

