(define-module (crates-io ru st rusty_neat) #:use-module (crates-io))

(define-public crate-rusty_neat-0.1.0 (c (n "rusty_neat") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hmxgzgl72cjvyyczkzpalbbhyxcrg6czglwdgf8ar7c28w4nr91")))

