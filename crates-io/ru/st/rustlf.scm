(define-module (crates-io ru st rustlf) #:use-module (crates-io))

(define-public crate-rustlf-1.0.0 (c (n "rustlf") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v1rbmf09ijzq3zx5j1n4kgjbanlh6cf0jv1j3w0bdcf3vd2mdnm")))

(define-public crate-rustlf-1.0.1 (c (n "rustlf") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "025lmipiwwnfsql0z560wqblsaarayc31cfjc3k8i9c0az5wx0b4")))

