(define-module (crates-io ru st rustfolio) #:use-module (crates-io))

(define-public crate-rustfolio-0.1.0 (c (n "rustfolio") (v "0.1.0") (d (list (d (n "args") (r "^2.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "piechart") (r "^1.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.0") (d #t) (k 0)))) (h "0z5zxbb8nhlv9rvsk712q75326hcpy2ygyg15l9z8wvnly8kc2nn")))

(define-public crate-rustfolio-0.2.0 (c (n "rustfolio") (v "0.2.0") (d (list (d (n "args") (r "^2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "piechart") (r "^1.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.0") (d #t) (k 0)))) (h "0crismz4r98n1ym46r55wbc1ygich62lshgk109x54iqsrpk31ld")))

(define-public crate-rustfolio-0.3.0 (c (n "rustfolio") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "piechart") (r "^1.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.0") (d #t) (k 0)))) (h "00yzbcyq9b8brhpi8yji7rml3g60ghmq9xwak387da6cdzpf8izv")))

(define-public crate-rustfolio-1.0.0 (c (n "rustfolio") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.34") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "piechart") (r "^1.0.0") (d #t) (k 0)) (d (n "xdg") (r "^2.0") (d #t) (k 0)))) (h "0jvsl7x2v9g0hb1pnwaxjxfi0lxv9glr1vhn0w96qbm3w2nww97q")))

