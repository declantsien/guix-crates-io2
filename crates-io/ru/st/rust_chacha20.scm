(define-module (crates-io ru st rust_chacha20) #:use-module (crates-io))

(define-public crate-rust_chacha20-0.1.0 (c (n "rust_chacha20") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1n6dlibbg9g2kvbrk6y81v9y0h1qrmhrdmlaygl4gzcq5ggzv6l9") (r "1.56.1")))

(define-public crate-rust_chacha20-0.2.0 (c (n "rust_chacha20") (v "0.2.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1i3z8q4hdd1spdvbknrm0gq2p1s6mm9w3ygjxrjilcan6px58xvp") (r "1.56.1")))

(define-public crate-rust_chacha20-0.3.0 (c (n "rust_chacha20") (v "0.3.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0byz44scpfmrxhy2s4chdss740yri0ij626yw3yw1p2i60gzanh3") (r "1.56.1")))

