(define-module (crates-io ru st rustbook_instances_cn) #:use-module (crates-io))

(define-public crate-RustBook_Instances_CN-0.3.0 (c (n "RustBook_Instances_CN") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1gvfrpkknjqy498fs4sxjx2v15d38mijxign5dd699rp14x2g0ax") (y #t)))

(define-public crate-RustBook_Instances_CN-0.3.1 (c (n "RustBook_Instances_CN") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0pdyiphz6xazfamv60cns1b0jjdsybv6n11lhxcchz7lc5wpfaap") (y #t)))

(define-public crate-RustBook_Instances_CN-0.4.2 (c (n "RustBook_Instances_CN") (v "0.4.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0ih1g0hwg70v9dzdzp2pgmz2sd2jg535bdqg9v51qjmqy46dhx30") (y #t)))

(define-public crate-RustBook_Instances_CN-0.5.0 (c (n "RustBook_Instances_CN") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0xzks99x06g39hq08jdvvnhqlw1vqkwdybx265n62f8lzdpryn6r") (y #t)))

(define-public crate-RustBook_Instances_CN-0.6.0 (c (n "RustBook_Instances_CN") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1h6i1rqgmlgjbrbzxvm89md3l9drsw7lhpyq7jiqqv877npi445d")))

(define-public crate-RustBook_Instances_CN-0.6.1 (c (n "RustBook_Instances_CN") (v "0.6.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "04sxy0vhq4ipgdnip3pmr8k07ghr21bf4hj6q1zik10ymiyn1gfb")))

(define-public crate-RustBook_Instances_CN-0.6.5 (c (n "RustBook_Instances_CN") (v "0.6.5") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "07qacqakisaf5dn27jyqjds9fzyhwn6y94k6arz0rjn8420blf0w")))

(define-public crate-RustBook_Instances_CN-0.7.0 (c (n "RustBook_Instances_CN") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0b6lyjs46hi4yl1fms34ywrccs34hwf2x0rfbi5hc1imj5dmvdjd")))

(define-public crate-RustBook_Instances_CN-0.7.1 (c (n "RustBook_Instances_CN") (v "0.7.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0almsw2spw0hd2kn6szixygk6if2iyzg99c3zp5mn9sg713g8wl4")))

(define-public crate-RustBook_Instances_CN-0.7.2 (c (n "RustBook_Instances_CN") (v "0.7.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1jpbn9i0yzj43h81x6wfr2vhp465myavg3qga4cpbblcml4qdawb")))

