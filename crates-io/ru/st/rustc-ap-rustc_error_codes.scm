(define-module (crates-io ru st rustc-ap-rustc_error_codes) #:use-module (crates-io))

(define-public crate-rustc-ap-rustc_error_codes-629.0.0 (c (n "rustc-ap-rustc_error_codes") (v "629.0.0") (h "0nnhq2l9vv4j2ak2b7yqa6sq226v0cg25r1i3v92b0gzhgwwi3zq")))

(define-public crate-rustc-ap-rustc_error_codes-630.0.0 (c (n "rustc-ap-rustc_error_codes") (v "630.0.0") (h "19vri5gs8vvrmrlx9i6v6szf4ixyyck9aw9hrvyh4xjzlilh7j1j")))

(define-public crate-rustc-ap-rustc_error_codes-631.0.0 (c (n "rustc-ap-rustc_error_codes") (v "631.0.0") (h "1mrf0p62v1lcrnc33nwfvrr8imfylvlc8ccg0h1m1v2fljq4pfhl")))

(define-public crate-rustc-ap-rustc_error_codes-632.0.0 (c (n "rustc-ap-rustc_error_codes") (v "632.0.0") (h "121vnd08ldwc3769zga7j36qqnjgcip1rr5w7wznis8bm7s91f1g")))

(define-public crate-rustc-ap-rustc_error_codes-633.0.0 (c (n "rustc-ap-rustc_error_codes") (v "633.0.0") (h "1bvf03qxr3m6b3isdxahf6c6xx83597f733g26fjv47yvy4k2665")))

(define-public crate-rustc-ap-rustc_error_codes-634.0.0 (c (n "rustc-ap-rustc_error_codes") (v "634.0.0") (h "1y47837xq5ln8s6fj6dcsjq7h90zmq72f5g85l82f6jmfvcfbcvw")))

(define-public crate-rustc-ap-rustc_error_codes-635.0.0 (c (n "rustc-ap-rustc_error_codes") (v "635.0.0") (h "052zl9z6kfcvgy4l99i2c0cmn5aknws054p56597926kdr5pdb7m")))

(define-public crate-rustc-ap-rustc_error_codes-636.0.0 (c (n "rustc-ap-rustc_error_codes") (v "636.0.0") (h "1mmx23ln5q0n1rhsggk0rmakcm6iqzmrrsvmxgf5fwdcbz6nz7jf")))

(define-public crate-rustc-ap-rustc_error_codes-637.0.0 (c (n "rustc-ap-rustc_error_codes") (v "637.0.0") (h "1gjrkl1g9c9ls2ng28l5qppqb0x2si85csmdx1iy1v2d17ipfmp4")))

(define-public crate-rustc-ap-rustc_error_codes-638.0.0 (c (n "rustc-ap-rustc_error_codes") (v "638.0.0") (h "1cddqb0hwdgx5rw0l40qas47mi5xz4dwrjbsaykjxripjhqxfdhq")))

