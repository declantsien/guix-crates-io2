(define-module (crates-io ru st rusty-snowflake) #:use-module (crates-io))

(define-public crate-rusty-snowflake-0.1.0 (c (n "rusty-snowflake") (v "0.1.0") (h "13pbqamz4cn84ggb8fqanqjrwd68fqkwlq9y319nbb0zsg5kskrd")))

(define-public crate-rusty-snowflake-0.1.1 (c (n "rusty-snowflake") (v "0.1.1") (h "1gghsqagvccijlzl61vnc10l2i4m2q0hc8gxri0nyq860aw0h64r")))

(define-public crate-rusty-snowflake-0.2.0 (c (n "rusty-snowflake") (v "0.2.0") (h "0kxb44p6f5dkiic6q5k5wnjq6k7wmk77cc478z2b8jqwalpwawxs")))

