(define-module (crates-io ru st rusty-data) #:use-module (crates-io))

(define-public crate-rusty-data-0.0.1 (c (n "rusty-data") (v "0.0.1") (h "1kczv6misydzrp20xn39p5nxrd909fh96yqq13kl0j4v88a4f5g7")))

(define-public crate-rusty-data-0.0.2 (c (n "rusty-data") (v "0.0.2") (d (list (d (n "num") (r "^0.1.28") (k 0)))) (h "01dlfihpkw48788zlyzzw6yip3bs6lzqpk8lf5ixirydk6xys5mf")))

(define-public crate-rusty-data-0.0.3 (c (n "rusty-data") (v "0.0.3") (d (list (d (n "num") (r "^0.1.28") (k 0)))) (h "0nvkjalbg5nlavih6jyhm620bqscvcdakh29kpvj744pxiakd4vw")))

