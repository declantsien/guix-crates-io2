(define-module (crates-io ru st rust-sk) #:use-module (crates-io))

(define-public crate-rust-sk-0.1.0 (c (n "rust-sk") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0dabpqn7xs0x8fd0d2pd5haj8p4s70n4hcgl8zyni55vy7jc2hbz")))

(define-public crate-rust-sk-0.2.0 (c (n "rust-sk") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0c37863idf1lwa9x54qyranb4b3d0gw051dqi1qcf2071bg873ly")))

