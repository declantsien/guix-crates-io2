(define-module (crates-io ru st rustfits) #:use-module (crates-io))

(define-public crate-rustfits-0.1.0 (c (n "rustfits") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 2)))) (h "14hl2pxcm2hy9qrqpyxca2cr12wcfwqhgyq5jdm50k4h40awdc16") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-rustfits-0.1.1 (c (n "rustfits") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 2)))) (h "1nr1i0vjgwmdfmvlvyrl9dfcmkq6jd9w2gjnaj6si3zvc560niw0") (f (quote (("default" "console_error_panic_hook"))))))

