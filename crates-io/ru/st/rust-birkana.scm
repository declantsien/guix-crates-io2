(define-module (crates-io ru st rust-birkana) #:use-module (crates-io))

(define-public crate-rust-birkana-0.1.0 (c (n "rust-birkana") (v "0.1.0") (d (list (d (n "svg") (r "^0.5.3") (d #t) (k 0)))) (h "0hrpr3fclc6raj013q2vd6lbql7fgfqnnkwbqbj7qssn48qzn2ak")))

(define-public crate-rust-birkana-0.1.1 (c (n "rust-birkana") (v "0.1.1") (d (list (d (n "svg") (r "^0.5.3") (d #t) (k 0)))) (h "00rw0lcm2y8f0yldvgn3haqf4pb3vc37fa6drsp5kcdccx5s7k22")))

(define-public crate-rust-birkana-0.1.2 (c (n "rust-birkana") (v "0.1.2") (d (list (d (n "svg") (r "^0.5.3") (d #t) (k 0)))) (h "1m7hwmxq811gkw5gzb9mnyqin2vdhk9n06byd8q989avgnpqzbpj")))

(define-public crate-rust-birkana-1.0.0 (c (n "rust-birkana") (v "1.0.0") (d (list (d (n "svg") (r "^0.5.3") (d #t) (k 0)))) (h "1zf3i1qj59m0hh8dmajznn75n38jq4h0r4gsfhvbaz7vd7lydk0r")))

(define-public crate-rust-birkana-1.1.0 (c (n "rust-birkana") (v "1.1.0") (d (list (d (n "svg") (r "^0.5.3") (d #t) (k 0)))) (h "133q9bhnimdar8kp1d19lh9w5f1km9pxdzw8w69fyib246575i10")))

(define-public crate-rust-birkana-1.1.1 (c (n "rust-birkana") (v "1.1.1") (d (list (d (n "svg") (r "^0.5.3") (d #t) (k 0)))) (h "0f11cnk965kc8sbliri0a0358lg049lkxrps3nwsscwlp52cpyfb")))

(define-public crate-rust-birkana-1.1.2 (c (n "rust-birkana") (v "1.1.2") (d (list (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "0pznw6vhlv5gkrrz77vfbbx33w80nads3j8lg0pmqhnagyajxgkg")))

