(define-module (crates-io ru st rust-sadari-cli) #:use-module (crates-io))

(define-public crate-rust-sadari-cli-0.1.0 (c (n "rust-sadari-cli") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.8.0") (d #t) (k 0)))) (h "0irjbpxkij75czh4vf07vkf8dd83kh4fvnkgfpxw8m0rh5a7ra8g")))

(define-public crate-rust-sadari-cli-0.1.1 (c (n "rust-sadari-cli") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.8.0") (d #t) (k 0)))) (h "1n3f10jwl6szcxc2b1vg821hiv7wl6mjfwp6jj1zlwbi3hnavwgk")))

