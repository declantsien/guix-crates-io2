(define-module (crates-io ru st rustutils-base64) #:use-module (crates-io))

(define-public crate-rustutils-base64-0.1.0 (c (n "rustutils-base64") (v "0.1.0") (d (list (d (n "base64-stream") (r "^1.2.7") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "0ir974zlwjxld9kwdsjiws917k4j1y0bc1k7lv9cwr9jp1hj0c66")))

