(define-module (crates-io ru st rust-rcon) #:use-module (crates-io))

(define-public crate-rust-rcon-0.1.0 (c (n "rust-rcon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tungstenite") (r "^0.12.0") (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "03z8z801v8g0y8dscq728hamfx57bc8mrjq66668q6rgafx8sgx7")))

(define-public crate-rust-rcon-0.1.1 (c (n "rust-rcon") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "tungstenite") (r "^0.12.0") (k 0)))) (h "1mqd6b0vf1gwn93n3yr5kr8qi7q7qw6g9axsghh1gkqz0r9a5mzy")))

(define-public crate-rust-rcon-0.1.2 (c (n "rust-rcon") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("std"))) (k 0)) (d (n "env_logger") (r "^0.9.0") (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.2") (k 0)))) (h "056rb0mbljya8fb0napl8nxknq1b4i6gvvxbjl8rgj476qx4b884")))

