(define-module (crates-io ru st rust-miller-rabin) #:use-module (crates-io))

(define-public crate-rust-miller-rabin-0.1.0 (c (n "rust-miller-rabin") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "007mblq5fwxap207hvylkxh0bb6ssjf315c97f76gwi3za9fhhxw")))

(define-public crate-rust-miller-rabin-0.1.1 (c (n "rust-miller-rabin") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11ja9z9hkigkw6sls4n46hpha5558ba991pc76flk1j97xrc4p7m")))

(define-public crate-rust-miller-rabin-0.1.2 (c (n "rust-miller-rabin") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "18d1z54xp8vkwzn8qqc6grzs5w34n15vff959lxmgsl3lrihkpsn")))

(define-public crate-rust-miller-rabin-0.1.3 (c (n "rust-miller-rabin") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11c0rdqkdgjdbw8xkfy8qx357llifhr1bcphnazlpkr82s1fz3k7")))

(define-public crate-rust-miller-rabin-0.1.4 (c (n "rust-miller-rabin") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vrg79n8f8ysl2jlnpq6yfh09afaviasz4c1waap71y23fslaa5p")))

