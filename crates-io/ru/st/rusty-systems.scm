(define-module (crates-io ru st rusty-systems) #:use-module (crates-io))

(define-public crate-rusty-systems-0.3.0 (c (n "rusty-systems") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 2)))) (h "0ggclfhpldf05bn10x3acc7r30py6z7ahf253kgnmk6rsyszqsy5")))

(define-public crate-rusty-systems-1.0.0 (c (n "rusty-systems") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 2)))) (h "1ck8l1nhxg136qi1sm2ih1gvsf2mqd98h8s7lxf8pibf52gy9fj8")))

