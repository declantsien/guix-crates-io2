(define-module (crates-io ru st rust-texas) #:use-module (crates-io))

(define-public crate-rust-texas-0.2.4 (c (n "rust-texas") (v "0.2.4") (h "0hmzshisric7ccplxmxxngpzpk6gkqd278ssr9s3ybgqvj9s7d01")))

(define-public crate-rust-texas-0.2.5 (c (n "rust-texas") (v "0.2.5") (h "1c931jfxac3j1l4gbqicn9wfck4a3kw57x7qb4q1cbg7lgwphm86")))

(define-public crate-rust-texas-0.2.6 (c (n "rust-texas") (v "0.2.6") (h "0vgskpylnmlg57fmgswyrlxyw5i4ac061zw8f4hyz9agzg761i94")))

(define-public crate-rust-texas-0.2.7 (c (n "rust-texas") (v "0.2.7") (h "0jv4c615bwbb5nq095j49q6iqnpf162y0bg40wjp3p6v8wsdvpp9")))

(define-public crate-rust-texas-0.2.8 (c (n "rust-texas") (v "0.2.8") (h "1k21wz0g7c18zq1jyaxb9k4cnqb4hbgaqa1c16fwskx56n7maahz")))

(define-public crate-rust-texas-0.3.0 (c (n "rust-texas") (v "0.3.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)))) (h "14pvypc13if48fvgrl4sdff37nxvfwrlz20jk74cfvjbqy8g03y3")))

