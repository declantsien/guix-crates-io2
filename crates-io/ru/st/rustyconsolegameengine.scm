(define-module (crates-io ru st rustyconsolegameengine) #:use-module (crates-io))

(define-public crate-rustyConsoleGameEngine-0.1.0 (c (n "rustyConsoleGameEngine") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "wincon" "winbase" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a8x47brh9qdwjfrw9pl3bly1y2y87dhfn1shsh5kx31p48m7v06")))

