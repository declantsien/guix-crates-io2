(define-module (crates-io ru st rust-stellar-xdr) #:use-module (crates-io))

(define-public crate-rust-stellar-xdr-0.1.0 (c (n "rust-stellar-xdr") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 2)) (d (n "multi_reader") (r "^0.1") (d #t) (k 1)) (d (n "xdr-codec") (r "^0.4.4") (d #t) (k 0)) (d (n "xdrgen") (r "^0.4.4") (d #t) (k 1)))) (h "11fj0r46nyza26krm0qm0wh32abgpfj9y0q4njwklj8c0b7k2yfm")))

(define-public crate-rust-stellar-xdr-0.2.0 (c (n "rust-stellar-xdr") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "multi_reader") (r "^0.1") (d #t) (k 1)) (d (n "xdr-codec") (r "^0.4.4") (d #t) (k 0)) (d (n "xdrgen") (r "^0.4.4") (d #t) (k 1)))) (h "11xddbwvnzvnz83hbicq6bx1shwm8bv1g13gbp5r7i5gygmfpdb3")))

(define-public crate-rust-stellar-xdr-0.3.0 (c (n "rust-stellar-xdr") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1") (d #t) (k 0)) (d (n "multi_reader") (r "^0.1") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "xdr-codec") (r "^0.4.4") (d #t) (k 0)) (d (n "xdrgen") (r "^0.4.4") (d #t) (k 1)))) (h "1n4gasra61vssqkvlyzbpgx5kbps4v9lc0ac1h1ayckdz9w5flcy")))

