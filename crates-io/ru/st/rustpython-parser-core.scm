(define-module (crates-io ru st rustpython-parser-core) #:use-module (crates-io))

(define-public crate-rustpython-parser-core-0.3.0 (c (n "rustpython-parser-core") (v "0.3.0") (d (list (d (n "is-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "rustpython-parser-vendored") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (k 0)))) (h "1p8vyad67nzw442zvxnm9s61fk071rvqrm5s9d6pgp6ldl65d7ay") (f (quote (("location") ("default"))))))

(define-public crate-rustpython-parser-core-0.3.1 (c (n "rustpython-parser-core") (v "0.3.1") (d (list (d (n "is-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "rustpython-parser-vendored") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (k 0)))) (h "0zq9vysarw8yclc4wyjvv7c0bspzs66id3y00i9y0i9dh9g4dya7") (f (quote (("location") ("default")))) (r "1.72.1")))

