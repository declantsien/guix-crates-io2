(define-module (crates-io ru st rustdoc-highlight) #:use-module (crates-io))

(define-public crate-rustdoc-highlight-0.1.1 (c (n "rustdoc-highlight") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "12xg0iraph6d6zz8x7ld7hc3a8in9j309yvybx64m4wmb36bxnxy")))

(define-public crate-rustdoc-highlight-0.1.2 (c (n "rustdoc-highlight") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1n3nj5xza69aihxnlm8885saz99rchn6pycvip88skm117lffjvz")))

(define-public crate-rustdoc-highlight-0.1.3 (c (n "rustdoc-highlight") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15dvvlgrkw7qhxm6h5alxmrs817xbg639abq6hwy1wcsk77m77pc")))

(define-public crate-rustdoc-highlight-0.1.4 (c (n "rustdoc-highlight") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "027dbnq7wkjkavys9r49cza9q4s3gr62wj0w66jsv3q5i73kq6h9")))

(define-public crate-rustdoc-highlight-0.1.5 (c (n "rustdoc-highlight") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0c3gpvx72yiakzlljmmd8j9kiqr5zvsirgk5d14n0z9jmyd5cjh6")))

(define-public crate-rustdoc-highlight-0.1.6 (c (n "rustdoc-highlight") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14mka7p5wl79ic3nz4i7s10cf6yf0g1cbgs17d9zaxh8v37ajb3i")))

(define-public crate-rustdoc-highlight-0.1.7 (c (n "rustdoc-highlight") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jnk4dv8ay3jb911hirv6jqs6yvsy898xxqp6zr865mycis49ab1")))

(define-public crate-rustdoc-highlight-0.1.8 (c (n "rustdoc-highlight") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dd3y1vjh0q47xcjar8bh8xb8nwgl06xi6naq92v56f8bxkc5dnf")))

(define-public crate-rustdoc-highlight-0.1.9 (c (n "rustdoc-highlight") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fpbw1kyr10iiiw3c40qzcz3ndnalqsy71nxsjc2mjalaz82zgzm")))

(define-public crate-rustdoc-highlight-0.1.10 (c (n "rustdoc-highlight") (v "0.1.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0aravhny0ba7racrkgrcfmh3g8s3q0yznd5lnsasda96kbllabgy")))

