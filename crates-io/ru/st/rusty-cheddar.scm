(define-module (crates-io ru st rusty-cheddar) #:use-module (crates-io))

(define-public crate-rusty-cheddar-0.1.0 (c (n "rusty-cheddar") (v "0.1.0") (d (list (d (n "clippy") (r "^0") (d #t) (k 0)))) (h "1zf4krlsrwc9kd1jhnzqybskq6g0q72zagkg990v868km1l3666s")))

(define-public crate-rusty-cheddar-0.2.0 (c (n "rusty-cheddar") (v "0.2.0") (h "1sli5bagr0y6lh9cvp4ym6ap101z023kzgjfmp2d4dh0qm6hl5ik")))

(define-public crate-rusty-cheddar-0.3.0 (c (n "rusty-cheddar") (v "0.3.0") (d (list (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.24.0") (d #t) (k 0)) (d (n "toml") (r "^0.1.25") (d #t) (k 0)))) (h "1qx59sy2klc0mfq45792s9r49qbnf15a4libx569b0ckyl77mggr")))

(define-public crate-rusty-cheddar-0.3.1 (c (n "rusty-cheddar") (v "0.3.1") (d (list (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.24.0") (d #t) (k 0)) (d (n "toml") (r "^0.1.25") (d #t) (k 0)))) (h "0m5319qb0idxzwdmi2hhzxmr8jfbzqn1m3j7gvzgbldqbn3vm6sw")))

(define-public crate-rusty-cheddar-0.3.2 (c (n "rusty-cheddar") (v "0.3.2") (d (list (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.24.0") (d #t) (k 0)) (d (n "toml") (r "^0.1.25") (d #t) (k 0)))) (h "1jilyd6827ccsyvzz4pbcl8ggh8lri5an5g96vpp24qsm906dj34")))

(define-public crate-rusty-cheddar-0.3.3 (c (n "rusty-cheddar") (v "0.3.3") (d (list (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.24.0") (d #t) (k 0)) (d (n "toml") (r "^0.1.25") (d #t) (k 0)))) (h "1b1akn06jax9za5wdwfhspb9ixxfi74pq0b25vs5s6sk0vri1h4d")))

