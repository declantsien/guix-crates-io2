(define-module (crates-io ru st rusty-pkl) #:use-module (crates-io))

(define-public crate-rusty-pkl-0.1.0 (c (n "rusty-pkl") (v "0.1.0") (h "0c33fpcq8pkjm9lqw00qd9r51afjkwjx05rg35i18v3ix5f4zjs1")))

(define-public crate-rusty-pkl-0.1.1 (c (n "rusty-pkl") (v "0.1.1") (h "15xblirgnvqrmyb0mza8zqrlzy19lfn5ykpx5sv7y24zrbd9w7r0") (y #t)))

(define-public crate-rusty-pkl-0.1.2 (c (n "rusty-pkl") (v "0.1.2") (h "1jir0a5p29isxa5jxb9gsd9ycr8wkqg22hflgxkd0y3g465dvp23")))

