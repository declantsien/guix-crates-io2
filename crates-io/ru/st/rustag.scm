(define-module (crates-io ru st rustag) #:use-module (crates-io))

(define-public crate-rustag-0.1.0 (c (n "rustag") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "postcard") (r "^1.0.8") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)))) (h "1n9mhnbndf1xhpp4xmjq8krrffsrqnvrlyqazfzw148nascz6n0c")))

