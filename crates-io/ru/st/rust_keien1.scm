(define-module (crates-io ru st rust_keien1) #:use-module (crates-io))

(define-public crate-rust_keien1-0.1.0 (c (n "rust_keien1") (v "0.1.0") (d (list (d (n "rust_keien") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_wbc") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_wek") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_wrr") (r "^0.1.0") (d #t) (k 0)) (d (n "sort_rust") (r "^0.2.0") (d #t) (k 0)))) (h "03y11c4izg47p2z21r0dj2aqfqpkcgdmmn9chdwb7ps2xrdhv8pl")))

