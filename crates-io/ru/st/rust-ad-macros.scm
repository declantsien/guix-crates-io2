(define-module (crates-io ru st rust-ad-macros) #:use-module (crates-io))

(define-public crate-rust-ad-macros-0.1.0 (c (n "rust-ad-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zcr4s39kdfn5hn5y31vx95n6lllsakvhs91jyzzf3lavk01pk71")))

(define-public crate-rust-ad-macros-0.2.0 (c (n "rust-ad-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n5h82cksxz59giwsl2wmz1334jrjm46mpc3mlzx0ywr88yzh8aj")))

(define-public crate-rust-ad-macros-0.2.1 (c (n "rust-ad-macros") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k5b200vmjxl23a37x6aa2fqc1rm9bypb9v94y90lknc0qvda6bn")))

(define-public crate-rust-ad-macros-0.2.2 (c (n "rust-ad-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wkfd8jmwhqj9k3q3765k2ffsvj0f3f6gd9j77bqngxrlks304n3")))

(define-public crate-rust-ad-macros-0.2.3 (c (n "rust-ad-macros") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.2.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13j8yjp20piqv6kzd2kyv7b098wdwxgw66bviyy55gy62x9c1h2q")))

(define-public crate-rust-ad-macros-0.2.4 (c (n "rust-ad-macros") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12sfskf0zwsgrsql0hzn31l5i1s6mm2lw9p5n6hcm3idh2hbzayg")))

(define-public crate-rust-ad-macros-0.3.0 (c (n "rust-ad-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cjd4kz6kvxk6aa6da03jp745670psqnmr8mi1rf0i6hyhrwvbav")))

(define-public crate-rust-ad-macros-0.3.1 (c (n "rust-ad-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03l05127j119rsy05vnk6v95gvbc003y0k96g19jqys53cr1gqny")))

(define-public crate-rust-ad-macros-0.4.0 (c (n "rust-ad-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "110r9frvp22mpy1k8ivz2yn05a4pil4k76v917al2hcbs384ypaz")))

(define-public crate-rust-ad-macros-0.4.1 (c (n "rust-ad-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zazsxl0whmyln3aawpffx783yyq5c0kbddp8ag5fxni8ql9rqj0")))

(define-public crate-rust-ad-macros-0.4.2 (c (n "rust-ad-macros") (v "0.4.2") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w7nd7ycx1h3yiljmk99d58vvrqcyjhi86qkrdc5592dn5kpq34v")))

(define-public crate-rust-ad-macros-0.7.0 (c (n "rust-ad-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-consts") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02hqvxgy9cv1qafxzjvwa4qnqmsni40j584nxc3f3js2hzn97kyj")))

(define-public crate-rust-ad-macros-0.7.1 (c (n "rust-ad-macros") (v "0.7.1") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-consts") (r "^0.7.1") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09hwym36xwp7nhq5c9wlgdbql63l5d8cnh6sskbgyz6vybm67pf5")))

(define-public crate-rust-ad-macros-0.7.2 (c (n "rust-ad-macros") (v "0.7.2") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-consts") (r "^0.7.2") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.7.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1na1dmxa9xhji8vvprkiivd17l0rwwf4v9jl9kald20yl23npn0l")))

(define-public crate-rust-ad-macros-0.8.0 (c (n "rust-ad-macros") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rust-ad-consts") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a6nw94ripcfmgpwsgfxh7mjkzazq22dx0n6a6vkval2gjlb2adh")))

