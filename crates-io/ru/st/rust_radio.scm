(define-module (crates-io ru st rust_radio) #:use-module (crates-io))

(define-public crate-rust_radio-1.0.0 (c (n "rust_radio") (v "1.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pls") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "rodio") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "18fapcpxa74isd6r7n892n0vdzbrd7ix2db9akz21lnz4lgaxgdv")))

(define-public crate-rust_radio-1.0.1 (c (n "rust_radio") (v "1.0.1") (d (list (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "pls") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("symphonia-mp3"))) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dyr10aj2j2a6q9ghb8ywfc3sfbjzwbsg1a3z860c5l9zfgvi3zl")))

(define-public crate-rust_radio-1.0.2 (c (n "rust_radio") (v "1.0.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pls") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("symphonia-mp3"))) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "18h10pl1himmc69gbnk8nz9nfk7ypppsl9g1k27h8cy5sglzzqi2")))

