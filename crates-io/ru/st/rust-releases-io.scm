(define-module (crates-io ru st rust-releases-io) #:use-module (crates-io))

(define-public crate-rust-releases-io-0.15.1 (c (n "rust-releases-io") (v "0.15.1") (d (list (d (n "attohttpc") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "07n7fzjv9x9m5ahpbzmvw4q1wpm0s2y3vqr7j348w37qkpvxx460") (f (quote (("internal_dl_test") ("http_client" "attohttpc"))))))

(define-public crate-rust-releases-io-0.16.0 (c (n "rust-releases-io") (v "0.16.0") (d (list (d (n "attohttpc") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1rmp01f5n6ikc15w9s5hwz5wq0382w1wsb9r9skrg7nklvvag1c4") (f (quote (("internal_dl_test") ("http_client" "attohttpc"))))))

(define-public crate-rust-releases-io-0.17.0 (c (n "rust-releases-io") (v "0.17.0") (d (list (d (n "attohttpc") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0py1isqsz9nkj029z456lain7xbswknvq8byvz897pg46gkyqhfb") (f (quote (("internal_dl_test") ("http_client" "attohttpc")))) (y #t)))

(define-public crate-rust-releases-io-0.18.0 (c (n "rust-releases-io") (v "0.18.0") (d (list (d (n "attohttpc") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0m9r6klj0kg4xflpw6qqhjqqyg63zgfhvkdfda8vdkh1wcagchf3") (f (quote (("internal_dl_test") ("http_client" "attohttpc")))) (y #t)))

(define-public crate-rust-releases-io-0.20.0 (c (n "rust-releases-io") (v "0.20.0") (d (list (d (n "attohttpc") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qmw3cn037xq7vywirn8aibnrn5f4c0xhj74y0gibjjgkd176zhq") (f (quote (("internal_dl_test") ("http_client" "attohttpc")))) (y #t)))

(define-public crate-rust-releases-io-0.21.0 (c (n "rust-releases-io") (v "0.21.0") (d (list (d (n "attohttpc") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1fhpazrkl51nmbz3i5gnnywxzca4x83y59wfg0iil670kzn1p9xg") (f (quote (("internal_dl_test") ("http_client" "attohttpc"))))))

(define-public crate-rust-releases-io-0.21.1 (c (n "rust-releases-io") (v "0.21.1") (d (list (d (n "attohttpc") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0rafmcps5w5mfyd35hkk25fwnn0yg2x4ra6j6zfwcscwwpmz15ca") (f (quote (("internal_dl_test") ("http_client" "attohttpc"))))))

(define-public crate-rust-releases-io-0.22.0 (c (n "rust-releases-io") (v "0.22.0") (d (list (d (n "attohttpc") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "00xpz5jyq5ln5byl4dccyfv134hijjl6qxn11xxyn8b8fh6d3fvj") (f (quote (("internal_dl_test") ("http_client" "attohttpc"))))))

(define-public crate-rust-releases-io-0.22.1 (c (n "rust-releases-io") (v "0.22.1") (d (list (d (n "attohttpc") (r "^0.19.1") (o #t) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0giy993x1zz0x0w1cgjfvsn0la6m7cf0npckyvzp9x95sh1igqa6") (f (quote (("internal_dl_test") ("http_client" "attohttpc"))))))

(define-public crate-rust-releases-io-0.23.0 (c (n "rust-releases-io") (v "0.23.0") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (o #t) (d #t) (k 0)))) (h "1p9ca5r58ig44v7rjk57ix7v81bm2l0lsa5inb94bkjfm9594093") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (y #t) (r "1.63")))

(define-public crate-rust-releases-io-0.23.1 (c (n "rust-releases-io") (v "0.23.1") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (o #t) (d #t) (k 0)))) (h "0mw3ndb2fhg9mmh5nglcvvna5yi1a7ngm1cy389ysm75wqfpf8l1") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (y #t) (r "1.63")))

(define-public crate-rust-releases-io-0.24.0 (c (n "rust-releases-io") (v "0.24.0") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (o #t) (d #t) (k 0)))) (h "0kjg3l2dp8f46kcrqkh7b3fah0id3b1xah7ic5v6gzsrn4ccva14") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (r "1.63")))

(define-public crate-rust-releases-io-0.25.0 (c (n "rust-releases-io") (v "0.25.0") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (o #t) (d #t) (k 0)))) (h "1hic6lkirmmva9fwb4vgq4944v4apsr8lh78sg5igx8v7nwh0zy7") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (r "1.63")))

(define-public crate-rust-releases-io-0.26.0 (c (n "rust-releases-io") (v "0.26.0") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (o #t) (d #t) (k 0)))) (h "00g3h57b5306c9gyvf3wzhdjr8rk9v5x775gris03p38vvppjfb9") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (r "1.63")))

(define-public crate-rust-releases-io-0.27.0 (c (n "rust-releases-io") (v "0.27.0") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (o #t) (d #t) (k 0)))) (h "0y7his4vypl024lwsikv4bf0bn1hm1llng1xi1m9zdl0d67nlkd5") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (r "1.63")))

(define-public crate-rust-releases-io-0.27.1 (c (n "rust-releases-io") (v "0.27.1") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (o #t) (d #t) (k 0)))) (h "19kqm5fx7i0v3jpgd3cy6gf3cfgrk15y6fxqng0qhg5kws10xq69") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (r "1.63")))

(define-public crate-rust-releases-io-0.28.0 (c (n "rust-releases-io") (v "0.28.0") (d (list (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (o #t) (d #t) (k 0)))) (h "00h2j66f0ggd1karf96g5mn24a7vgvh675gr44gw62i2cnmyfsy6") (f (quote (("internal_dl_test") ("http_client" "ureq")))) (r "1.63")))

