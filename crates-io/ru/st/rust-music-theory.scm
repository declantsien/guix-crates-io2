(define-module (crates-io ru st rust-music-theory) #:use-module (crates-io))

(define-public crate-rust-music-theory-0.1.0 (c (n "rust-music-theory") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "0y4ihd8ydbn9iy2p4mff24va8ijakilq5xksqs415s4x1chbjcll")))

(define-public crate-rust-music-theory-0.1.1 (c (n "rust-music-theory") (v "0.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "08q6djlyp794nz12zrviy2ch9hg60v7givd0bmlg5daim0g12lhl")))

(define-public crate-rust-music-theory-0.1.2 (c (n "rust-music-theory") (v "0.1.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "0b4sv6dgqq8l2f58f2rpfk77vyqlpbyfylc96yzk8471yglcpljz")))

(define-public crate-rust-music-theory-0.1.3 (c (n "rust-music-theory") (v "0.1.3") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "0jhj8hpsww8n12m50s3lpgysbz0gz28w3n6q8rkkhiaxvb30khl7")))

(define-public crate-rust-music-theory-0.1.4 (c (n "rust-music-theory") (v "0.1.4") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "16x10k1asflh2qqy11v5mibhcxr19bh1lfd4djx4r7d8ycw08xbv")))

(define-public crate-rust-music-theory-0.1.5 (c (n "rust-music-theory") (v "0.1.5") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "0s21z9yl8i53kh0i4zfi5gzircj3jfca2m1n8dypy1dg40fvlc4c")))

(define-public crate-rust-music-theory-0.1.6 (c (n "rust-music-theory") (v "0.1.6") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "0wyrq7v8h3nc23kdh5ddcq39z6cc629gw326sf6ywdv7p68fpdd2")))

(define-public crate-rust-music-theory-0.1.7 (c (n "rust-music-theory") (v "0.1.7") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "1wggxi98pxaiik5s6bv7qycvhpm4bpjl9gsljfvi5ahcjpyg7dpv")))

(define-public crate-rust-music-theory-0.2.0 (c (n "rust-music-theory") (v "0.2.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)))) (h "031i207hysm3igia53535aisdfrxvd2lbwc3qsfksfi55pl1bxx4")))

