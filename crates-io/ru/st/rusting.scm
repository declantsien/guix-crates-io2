(define-module (crates-io ru st rusting) #:use-module (crates-io))

(define-public crate-rusting-0.1.0 (c (n "rusting") (v "0.1.0") (h "1iq96w1rq1jyhlshp96favva6gh8aq7sdlcbf6mvqik607y0079z")))

(define-public crate-rusting-0.2.0 (c (n "rusting") (v "0.2.0") (h "0dm7ini1f6h1ys38bh09iqv88fr13nwi7vm4abq5qg5j8w8yys71")))

