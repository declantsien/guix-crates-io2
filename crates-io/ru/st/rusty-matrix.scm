(define-module (crates-io ru st rusty-matrix) #:use-module (crates-io))

(define-public crate-rusty-matrix-0.1.0 (c (n "rusty-matrix") (v "0.1.0") (h "19msc5n49k9sknw97897krcf7q256slcvdbikb8giavdlxxcqj3a")))

(define-public crate-rusty-matrix-0.1.1 (c (n "rusty-matrix") (v "0.1.1") (h "0i41m64x1s34251qj8h0qbkskhkgih9rjrjgkck0dhfay9vxf38i")))

(define-public crate-rusty-matrix-0.1.2 (c (n "rusty-matrix") (v "0.1.2") (h "1iq56c9h3z3fb3qdzcqhlcdmr8iv40dsgwmnjq42rjgzgmf3y4gs")))

