(define-module (crates-io ru st rusty-sandbox) #:use-module (crates-io))

(define-public crate-rusty-sandbox-0.1.0 (c (n "rusty-sandbox") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "1qnd1w3za5pj6kcc5fsr8phlrvkqlrridqpfgj2ipwm0hl67621g")))

(define-public crate-rusty-sandbox-0.2.0 (c (n "rusty-sandbox") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "0yys71f3lyf8459wrwm4l3g102hbipkb6f71bakrbkadagalnb05")))

(define-public crate-rusty-sandbox-0.2.1 (c (n "rusty-sandbox") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "1r3k7jbrhll02nkb5kcbkz8xkf2nfcpch1dnpp75bzqph4rf22sy")))

