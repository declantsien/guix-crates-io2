(define-module (crates-io ru st rust-otp) #:use-module (crates-io))

(define-public crate-rust-otp-1.0.0 (c (n "rust-otp") (v "1.0.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "17k8viy573sf4fmi1bf1szy7ja4mqakii523v206b66xi79n4vr6")))

(define-public crate-rust-otp-2.0.0 (c (n "rust-otp") (v "2.0.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.5.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "1acb0yypxb83vm0p8qm1ch72jsna82km5nbzdn4lm0f9ljj8643j")))

