(define-module (crates-io ru st rust_elm_typegen) #:use-module (crates-io))

(define-public crate-rust_elm_typegen-0.0.1 (c (n "rust_elm_typegen") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "extra-traits"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y13q4q97syy323xi7xkl4xblsq188m2hk7i9f9db9dgdizg8mdh")))

