(define-module (crates-io ru st rust_motr) #:use-module (crates-io))

(define-public crate-rust_motr-0.1.0 (c (n "rust_motr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 0)) (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.102") (d #t) (k 0)) (d (n "run_script") (r "^0.6.3") (d #t) (k 1)))) (h "1rk1k4gc5iyyylan4prxljj7zny0ggzy1m5zjqrdsj7kmdr7l1qh")))

