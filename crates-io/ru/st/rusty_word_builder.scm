(define-module (crates-io ru st rusty_word_builder) #:use-module (crates-io))

(define-public crate-rusty_word_builder-0.6.0 (c (n "rusty_word_builder") (v "0.6.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "198cmy4jxnv934ji9a39nidm339ljd3gvjknkk84db5kiwvq8m69")))

(define-public crate-rusty_word_builder-0.6.1 (c (n "rusty_word_builder") (v "0.6.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "0vjnl7v8mkw4jz88421dxm1vdiqi92861s6mryvs29gks6kpr2g4")))

(define-public crate-rusty_word_builder-0.6.2 (c (n "rusty_word_builder") (v "0.6.2") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "1pvygiwjd50pwil723l7mhi32b3rckanaj5vk3wnyfyygc7gc2bi")))

(define-public crate-rusty_word_builder-0.6.3 (c (n "rusty_word_builder") (v "0.6.3") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "1hvvrgfqd33pnrqsx81293xhmqarcqaaj2d7npr2ll3x1wij0q6l")))

