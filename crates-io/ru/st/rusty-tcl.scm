(define-module (crates-io ru st rusty-tcl) #:use-module (crates-io))

(define-public crate-rusty-tcl-0.9.2 (c (n "rusty-tcl") (v "0.9.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rusty-tcl-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1z35fawhirbm0l7dxbjznlaw450vcaf4429g10n3mcgwpi1slzy2")))

(define-public crate-rusty-tcl-0.9.3 (c (n "rusty-tcl") (v "0.9.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rusty-tcl-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1blwr21l3bjcmpxmmmb18bpz03yr83qmpp6wa6qj3252h598935v")))

(define-public crate-rusty-tcl-0.9.4 (c (n "rusty-tcl") (v "0.9.4") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rusty-tcl-sys") (r "^1.1.0") (d #t) (k 0)))) (h "1a3vi6fh4adqy9207bsjk45dh69yhs0g7yd1k16342qy5h7x0fzd")))

(define-public crate-rusty-tcl-0.9.5 (c (n "rusty-tcl") (v "0.9.5") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rusty-tcl-sys") (r "^1.1.1") (d #t) (k 0)))) (h "1pxps21l47bgx5dmqh5rsixqwgmpnfp4z9dcnq4gfj66qk6znxql")))

(define-public crate-rusty-tcl-0.9.7 (c (n "rusty-tcl") (v "0.9.7") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rusty-tcl-sys") (r "^1.1.3") (d #t) (k 0)))) (h "04ym6badls3lbksrwc3kw606g3v1id1yzyidy97wk9rc3hb8mxxb")))

(define-public crate-rusty-tcl-0.10.0 (c (n "rusty-tcl") (v "0.10.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rusty-tcl-sys") (r "^1.1.3") (d #t) (k 0)))) (h "1l2k1knhbb5jk2wpld25ljf2jsddb8s87az6x7m9n0rq3acdnlbm")))

