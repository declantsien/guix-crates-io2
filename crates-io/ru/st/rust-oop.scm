(define-module (crates-io ru st rust-oop) #:use-module (crates-io))

(define-public crate-rust-oop-0.1.0 (c (n "rust-oop") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "055kz0ynzb5yzq95z62pm53y259pjsjg0g67fs6343yagyhmwdgp")))

(define-public crate-rust-oop-0.1.1 (c (n "rust-oop") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1k2w95f96aaaqrarp0prid4faq90kjwz4h2ypxnfqwrf5bvb9vbm")))

