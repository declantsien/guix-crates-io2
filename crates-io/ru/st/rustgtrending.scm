(define-module (crates-io ru st rustgtrending) #:use-module (crates-io))

(define-public crate-rustgtrending-0.1.0 (c (n "rustgtrending") (v "0.1.0") (d (list (d (n "mockito") (r "^0.28.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0icivjnqkqxrb3w91gvrpkyqbwm2rg4lmxragbqs1jwwhxh6waa1")))

(define-public crate-rustgtrending-0.1.1 (c (n "rustgtrending") (v "0.1.1") (d (list (d (n "mockito") (r "^0.28.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0v9rnrgh6cf5v32m426xvw4rszd50avf8nzahsjxmx8iy616pd75")))

