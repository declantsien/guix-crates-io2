(define-module (crates-io ru st rustfmt-snippet) #:use-module (crates-io))

(define-public crate-rustfmt-snippet-0.1.0 (c (n "rustfmt-snippet") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "100cp2l39p2zm7padbrpbd6zaibz9hgdkv9x4rz028c711znhplz")))

(define-public crate-rustfmt-snippet-0.1.1 (c (n "rustfmt-snippet") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "09zrj8yv4n06ysqr37iv202vd1fcy6svj3r14wdvr4d8yyndhbwa")))

(define-public crate-rustfmt-snippet-0.1.2 (c (n "rustfmt-snippet") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0w8ww7r6jzmmx83rqlisw3yxick2q7zh6j87qs901a2rv7l3xak5")))

