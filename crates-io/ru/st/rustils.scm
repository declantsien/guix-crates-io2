(define-module (crates-io ru st rustils) #:use-module (crates-io))

(define-public crate-rustils-0.0.1 (c (n "rustils") (v "0.0.1") (h "0ib8da6w0hav16qsmjsjfwffslfavzx4r9p2zaip17l3g17995g7")))

(define-public crate-rustils-0.0.2 (c (n "rustils") (v "0.0.2") (h "1shdl87dv7fm80gpcj962c3zps6rkx76rjngix53nd9qnjzl7j6s")))

(define-public crate-rustils-0.0.3 (c (n "rustils") (v "0.0.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0c2svjkrcsd84w8smdzr2dszsgw0m8lh8ni4hgy72riwisx80vq8")))

(define-public crate-rustils-0.0.5 (c (n "rustils") (v "0.0.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1md8bllyvayri0i3p4zv8kb7x99nxidlkhzqm7j8awb1mlaczw4y")))

(define-public crate-rustils-0.0.8 (c (n "rustils") (v "0.0.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "160ly3c0bsps9ximz4w0388hmaqc619f8qqhpx8f283mw073kfd2")))

(define-public crate-rustils-0.0.9 (c (n "rustils") (v "0.0.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1cfjibm6hcmj6d4j28f5gwz4s6x6iwfdlar848iisspz0j3fnj9n")))

(define-public crate-rustils-0.1.0 (c (n "rustils") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1x3x0hwq82hmpgf00vjh1h1lkfivg7a0n7hqyiq7ymf1jkq32xh5")))

(define-public crate-rustils-0.1.1 (c (n "rustils") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1pfnz65qrlpy50dknkd30viq48ca28qiz5858kv011xqyrpxcbia")))

(define-public crate-rustils-0.1.6 (c (n "rustils") (v "0.1.6") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0x6862b82jxg49l08fcc7fwlry3cppw19ypj36m79lvlb2ypnvzr")))

(define-public crate-rustils-0.1.8 (c (n "rustils") (v "0.1.8") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "01bmwqy9z6csf2kr3ppi58qkkfg9lk8i3bfmsz9xg30ambhll1bs")))

(define-public crate-rustils-0.1.12 (c (n "rustils") (v "0.1.12") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1cxlvg8ynv18n1673v8xzkmhg9ywk92c4mb2i5j67zg8jfx99s0q")))

(define-public crate-rustils-0.1.19 (c (n "rustils") (v "0.1.19") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "10hv8bc2hk6bd35snp817hhx1i71r8lqp6yiibra81l9dba8a6qm")))

(define-public crate-rustils-0.1.23 (c (n "rustils") (v "0.1.23") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0d9b13sccdbj2q1qsr1mn4pr2qz0wglk93622pmdgf2107f7knz1")))

