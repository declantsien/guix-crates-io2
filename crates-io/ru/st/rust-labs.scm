(define-module (crates-io ru st rust-labs) #:use-module (crates-io))

(define-public crate-rust-labs-0.1.0 (c (n "rust-labs") (v "0.1.0") (h "1i7fb6jg79sgsb162spamxvhb4i22qj2mwqbflz6cm7n2qg4gxbd")))

(define-public crate-rust-labs-0.1.1 (c (n "rust-labs") (v "0.1.1") (h "0mxsqz7dd2vqpl5bjxd8s325rpvhdfyk8gk0cjvdvqk70m7r8a5n")))

