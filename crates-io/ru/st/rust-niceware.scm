(define-module (crates-io ru st rust-niceware) #:use-module (crates-io))

(define-public crate-rust-niceware-0.1.0 (c (n "rust-niceware") (v "0.1.0") (d (list (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0pyb5m8pyzdyxmgyzrc5yma8k3xs9mm7jarjyphiwwqhkjbd1m7a") (y #t)))

(define-public crate-rust-niceware-0.2.0 (c (n "rust-niceware") (v "0.2.0") (d (list (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "1fc92lcq375s78pw7k91nzpnzpl6419vr3v6gpymx74gf36g364h") (y #t)))

(define-public crate-rust-niceware-0.3.0 (c (n "rust-niceware") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1s06xk42fg8syfnzj6m1wsky1rr3pxz1gxh7k830yhkabpp0mjyx") (y #t)))

