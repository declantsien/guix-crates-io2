(define-module (crates-io ru st rust-logger) #:use-module (crates-io))

(define-public crate-rust-logger-0.1.0 (c (n "rust-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "try-catch") (r "^0.2.2") (d #t) (k 0)))) (h "1aq52bj7cr5j77kqvga7c8yl7nq86x7jf3z8jvlg27qb5fpzw7m0") (y #t)))

