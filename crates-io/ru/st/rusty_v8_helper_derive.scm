(define-module (crates-io ru st rusty_v8_helper_derive) #:use-module (crates-io))

(define-public crate-rusty_v8_helper_derive-1.0.0 (c (n "rusty_v8_helper_derive") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qzrf11zl15ad4fvl7wyq5crvinpfshb68cw61s9lpsc4bkddg3l")))

(define-public crate-rusty_v8_helper_derive-1.0.1 (c (n "rusty_v8_helper_derive") (v "1.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0496q9w3723y5l4hsc7d6i1ccb46y3xlpir4jn6iwcyk1ggc2qw7")))

(define-public crate-rusty_v8_helper_derive-1.0.2 (c (n "rusty_v8_helper_derive") (v "1.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1brif6dgvr29fwbpd0myccgsb93l0gypdckg57m909pcs6bjhz4d")))

(define-public crate-rusty_v8_helper_derive-1.0.3 (c (n "rusty_v8_helper_derive") (v "1.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1p54sbgk4jw9hmbwd6w13is749dv6ncmn7n4646209p17n7pc234")))

