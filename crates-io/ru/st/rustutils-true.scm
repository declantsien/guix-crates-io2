(define-module (crates-io ru st rustutils-true) #:use-module (crates-io))

(define-public crate-rustutils-true-0.1.0 (c (n "rustutils-true") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "0rldy5fr72c4i3rcjr60vasnq0l1zf1wqpvvjcv53mfdj5lzw11b")))

