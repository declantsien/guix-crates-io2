(define-module (crates-io ru st rustywind_core) #:use-module (crates-io))

(define-public crate-rustywind_core-0.1.0 (c (n "rustywind_core") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "aho-corasick") (r "^1.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.9") (d #t) (k 0)))) (h "153wjix8l11r9fwyd41mwxxnrq2qhzpkxb8l253nzhzf29vbm26b")))

(define-public crate-rustywind_core-0.1.1 (c (n "rustywind_core") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "aho-corasick") (r "^1.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.9") (d #t) (k 0)))) (h "06br6dfr9zdyn8y8lkwhi6rmrm5v0j9aiwcyn0yvvkhi05n84z8s")))

(define-public crate-rustywind_core-0.1.2 (c (n "rustywind_core") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "aho-corasick") (r "^1.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0w9047b82kvxy8kyz1j4nb1g2kzshw57vwiggpfc8016c2a33fqm")))

