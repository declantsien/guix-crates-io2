(define-module (crates-io ru st rust-cc) #:use-module (crates-io))

(define-public crate-rust-cc-0.1.0 (c (n "rust-cc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0d7gzw3r75zwz5wii8rn8ph2pwcf0f2fyrfbwxnjpj3glbz1k950") (f (quote (("nightly") ("default" "auto-collect") ("auto-collect"))))))

(define-public crate-rust-cc-0.1.1 (c (n "rust-cc") (v "0.1.1") (d (list (d (n "iai") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1n1nr2y5207p0i0lf5b6apwhaf84v42rnwdv7s6p98bnvhradbnm") (f (quote (("pedantic-debug-assertions") ("nightly") ("full" "auto-collect") ("default" "auto-collect") ("auto-collect"))))))

(define-public crate-rust-cc-0.2.0 (c (n "rust-cc") (v "0.2.0") (d (list (d (n "iai") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1a1b3iwbsm6ph0gnfivij0qkkn0y8y1xmlw5yisn1n3b8bbh9a5p") (f (quote (("pedantic-debug-assertions") ("nightly") ("full" "auto-collect" "finalization") ("finalization") ("default" "auto-collect" "finalization") ("auto-collect"))))))

