(define-module (crates-io ru st rust_xlsxwriter_derive) #:use-module (crates-io))

(define-public crate-rust_xlsxwriter_derive-0.1.0 (c (n "rust_xlsxwriter_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0mpkvxdq41cxz4lask6nxnq3vnw5fnw7vfr7jl9gbrk39hm5mrq3")))

(define-public crate-rust_xlsxwriter_derive-0.2.0 (c (n "rust_xlsxwriter_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0i6cs54j9rii8xjfy50lk3mgw17n965r1mwpv073y50ny6xwa320")))

