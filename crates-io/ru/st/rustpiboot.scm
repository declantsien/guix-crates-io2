(define-module (crates-io ru st rustpiboot) #:use-module (crates-io))

(define-public crate-rustpiboot-0.1.0 (c (n "rustpiboot") (v "0.1.0") (d (list (d (n "dd-lib") (r "^0.2.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)))) (h "13rc9gl5m36f6q1xlf7dqvrybfkp6b14z3c91q5s855ffgm5k9af")))

(define-public crate-rustpiboot-0.2.0 (c (n "rustpiboot") (v "0.2.0") (d (list (d (n "dd-lib") (r "^0.2.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kacrrinrbsx3lbk9g89vpmn70hv1zc7sz7xqhxajnbyvix1lrlb")))

(define-public crate-rustpiboot-0.3.0 (c (n "rustpiboot") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.9.2") (d #t) (k 0)))) (h "16ivhqkvjiy92yx3pssk79xmpqgpi2747kxj7bfjpd8yaavcsrw4")))

