(define-module (crates-io ru st rusty_scissors) #:use-module (crates-io))

(define-public crate-rusty_scissors-1.1.0 (c (n "rusty_scissors") (v "1.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "18cp8w9jax6dplm68jr679wxag0ac1cssg2b1i8vdqjsc87vzxbd")))

(define-public crate-rusty_scissors-1.1.1 (c (n "rusty_scissors") (v "1.1.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1rwgkab450n35lrzibc47d65m9vzy9pdk6gjndx1nbmz87gd742j")))

(define-public crate-rusty_scissors-1.2.0 (c (n "rusty_scissors") (v "1.2.0") (d (list (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0m9skfwr4ia0dx2qiqf2jbcjmxkgibpy0hm7fvp3d9n13sn1r6iv")))

