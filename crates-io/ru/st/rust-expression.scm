(define-module (crates-io ru st rust-expression) #:use-module (crates-io))

(define-public crate-rust-expression-0.1.0 (c (n "rust-expression") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "03cbr6f3lr3lag6dzpf3hhfbmbp45ri70x950qmhp9l9if3vydhb")))

(define-public crate-rust-expression-0.1.1 (c (n "rust-expression") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0xk8p4w0mri6q95gbhbj5891n3am8qs160cvsigf4q96c519spxh")))

(define-public crate-rust-expression-0.2.0 (c (n "rust-expression") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vz3hy3k4wzgz33rixir9095jmw7bx4d7w3lq1lr44p3lb3ljih1")))

(define-public crate-rust-expression-0.3.0 (c (n "rust-expression") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "101yq7k13rz92samrvp85afqzwgri8jk3f0fxa83wi0ql1h0qkyb")))

(define-public crate-rust-expression-0.3.1 (c (n "rust-expression") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nv6d55mr66m9idfrbcm4f0q7nz63cicmzp0xrim1gam1niv583m")))

