(define-module (crates-io ru st rustracts) #:use-module (crates-io))

(define-public crate-rustracts-0.0.1 (c (n "rustracts") (v "0.0.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "15y2x5sb9c85zkpr3abi27l85izflbw7ka1pf0z1302ki4106d9r") (y #t)))

(define-public crate-rustracts-0.1.0-alpha.2 (c (n "rustracts") (v "0.1.0-alpha.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "1jdi1wvc11vl0qiin5mi7pz2kf93j49ad9z2zzj1rx666dzjgldc")))

(define-public crate-rustracts-0.1.0-alpha.3 (c (n "rustracts") (v "0.1.0-alpha.3") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "0r72y55ij5mz2bhz3z8cv3plq40gb10hm1ghgdmsg2avxln6rm5s")))

(define-public crate-rustracts-0.2.0-alpha.3 (c (n "rustracts") (v "0.2.0-alpha.3") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "0bf09fxhz6d2v1x2i4y53kvcwn244g9gmzib2lml5dv0igp35lgc")))

(define-public crate-rustracts-0.2.0-alpha.4 (c (n "rustracts") (v "0.2.0-alpha.4") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1m6svgipiw8gs1aqdw6a91cfnr5lk2yig5cpxcwzppckwlhq1khv")))

(define-public crate-rustracts-0.2.0 (c (n "rustracts") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "parc") (r "^1.0.1") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "18hl2da6pwl96l98p2rixxavvicr8y9sqg2zzjhbcn28mcws4ylr")))

