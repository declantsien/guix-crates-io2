(define-module (crates-io ru st rust_of_clans) #:use-module (crates-io))

(define-public crate-rust_of_clans-0.1.0 (c (n "rust_of_clans") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)))) (h "0gcx89qf6whs4asili30jck5vd2jvx5gs4sl6rr4ni697k0ychc5")))

(define-public crate-rust_of_clans-0.1.1 (c (n "rust_of_clans") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "11pakv13p6d8nq3w0r5ls1r6wkvkqkl3vsg3rsb5c3x92w9818x8")))

(define-public crate-rust_of_clans-0.2.0 (c (n "rust_of_clans") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0ymyp8al8zignfnqpvyxlr9zkv3md7vmzz8g5p5534wygmmy0dsc")))

(define-public crate-rust_of_clans-0.3.0 (c (n "rust_of_clans") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "05wvlv5qfk3nifxb4065y857461g2gkaz2y26xrzksk7hmllbp65")))

(define-public crate-rust_of_clans-0.3.1 (c (n "rust_of_clans") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0ys3iaxk8bgamlzazwlpmq8gbl47ms7y9qfjvg1id9an82mhw5yp")))

(define-public crate-rust_of_clans-0.3.2 (c (n "rust_of_clans") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0vkp5xpwyh34vwcsx3bj3xbjjacmci2im24pmsv6vrkcqxhzmfgb")))

(define-public crate-rust_of_clans-0.3.3 (c (n "rust_of_clans") (v "0.3.3") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "14lr4m15z58sqfgaqyhspgifca6vi65ia3gzzrdppsl07wm107pg")))

(define-public crate-rust_of_clans-0.4.0 (c (n "rust_of_clans") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0vpcxybzszb79vccww2aicyc81skpk61sfz4kb52gfkcrw6wirc9")))

(define-public crate-rust_of_clans-0.5.0 (c (n "rust_of_clans") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "06i0qycka4mqszaj2fg2aayxiv769q02k72awic466yivd9yd5lk")))

(define-public crate-rust_of_clans-0.5.1 (c (n "rust_of_clans") (v "0.5.1") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "08mmvsd6m3yargyr0cfk385a3qhh6dsybxv5q699pq5iz9k9lkab")))

(define-public crate-rust_of_clans-0.5.2 (c (n "rust_of_clans") (v "0.5.2") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0hr9mgnqr18cbmgw0hiv1cygxmrxw2150clidhsyhnkvsv42s243")))

(define-public crate-rust_of_clans-0.6.0 (c (n "rust_of_clans") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "16qi0kijx3hgl79l9bkapny2p93b572mdd1ka13wlzxxbr8ck06i")))

(define-public crate-rust_of_clans-0.6.1 (c (n "rust_of_clans") (v "0.6.1") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "136z3ca455rhmva9rqidksmlgqm7ki04i4bgsdl1frn1q8q6z789")))

(define-public crate-rust_of_clans-0.6.2 (c (n "rust_of_clans") (v "0.6.2") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0zgcdickcjycymjrdfhrcspip9m8j1vdskxfahdlxxa4q0x52ny2")))

(define-public crate-rust_of_clans-0.7.0 (c (n "rust_of_clans") (v "0.7.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1s6479jdxjflhvn987232cs5k2vlakz1kxvbmkplarcir1kfgxfy")))

