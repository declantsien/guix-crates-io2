(define-module (crates-io ru st rust-beam) #:use-module (crates-io))

(define-public crate-rust-beam-0.1.0 (c (n "rust-beam") (v "0.1.0") (h "136r033jzpr2mznrwrwavv7iidf7p4h01imcbm1x990dc509szwq")))

(define-public crate-rust-beam-0.1.1 (c (n "rust-beam") (v "0.1.1") (h "13n8dbj9d95j7x2dhdcc5rknzyhz4qhanm80kb105sis25fwlaa3")))

(define-public crate-rust-beam-0.2.0 (c (n "rust-beam") (v "0.2.0") (h "04apmgm3wy2qlz12ia13f53ybsa9jg705fq3xf7l0q92rbb9p0yc")))

(define-public crate-rust-beam-0.3.0 (c (n "rust-beam") (v "0.3.0") (h "1b892lskfxbb7m64yj3ayj0miyzqn1b8yfrrila8rchd2847637l")))

