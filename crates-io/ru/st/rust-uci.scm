(define-module (crates-io ru st rust-uci) #:use-module (crates-io))

(define-public crate-rust-uci-0.1.0 (c (n "rust-uci") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.91") (d #t) (k 0)) (d (n "libuci-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0cxrqhyk7ian860yy8chia9rwfmmz19d817hqw7v6sq5sqagq7sn") (y #t)))

(define-public crate-rust-uci-0.1.1 (c (n "rust-uci") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.91") (d #t) (k 0)) (d (n "libuci-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0arn3nl1bwll3wqr8ljncsgvs99xllmia9si9f3d0890107din9p") (y #t)))

(define-public crate-rust-uci-0.1.2 (c (n "rust-uci") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.91") (d #t) (k 0)) (d (n "libuci-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1fp2hqdnmchkad2lb5r9v3gqs63kjaqb2mq8vi5ig0l2rwrpxzi5") (y #t)))

(define-public crate-rust-uci-0.1.3 (c (n "rust-uci") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.91") (d #t) (k 0)) (d (n "libuci-sys") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "143gxdhgsqja8hm7s4api867ww00gc1sb598yfjgpy889y3a7hng") (y #t)))

(define-public crate-rust-uci-0.1.4 (c (n "rust-uci") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.91") (d #t) (k 0)) (d (n "libuci-sys") (r "^1.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1q6xf2vj4v4fbrx3gmp80p61jdq1q7bpp16s1xvynh622cdyw0g9") (y #t)))

(define-public crate-rust-uci-0.1.5 (c (n "rust-uci") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.91") (d #t) (k 0)) (d (n "libuci-sys") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1fy378axbpnni63znqmrvfpymfsl2f3082wn8qkiy708xjrysr66")))

