(define-module (crates-io ru st rusty-santa) #:use-module (crates-io))

(define-public crate-rusty-santa-0.1.0 (c (n "rusty-santa") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1v2lrlzv0gmh1cqadzhl1wsm4nqnr5k050bzc88kb2qg74gwfpnn")))

