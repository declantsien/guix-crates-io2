(define-module (crates-io ru st rustorm-derive) #:use-module (crates-io))

(define-public crate-rustorm-derive-0.1.0 (c (n "rustorm-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0r1bm9p5zfzhl0hrw3fkmj73w6rjpzz3wzvpm38nbcr1yr019v3r")))

