(define-module (crates-io ru st rust-gomoku) #:use-module (crates-io))

(define-public crate-rust-gomoku-0.0.1 (c (n "rust-gomoku") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1jgdmfk5fwrcvx4hgk6nb5s0dpcmkz8yf5s6d3mrd2w1c85xp4v6") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-rust-gomoku-0.0.2 (c (n "rust-gomoku") (v "0.0.2") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "17hlrx6h0azj3r3kk0sjvgjcgdh85k355mjp8hsn3cjk0nq10rjl") (f (quote (("default" "console_error_panic_hook"))))))

