(define-module (crates-io ru st rustcap) #:use-module (crates-io))

(define-public crate-rustcap-0.1.0 (c (n "rustcap") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pcap-sys") (r "^0.1") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (o #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("ws2def" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "196g5g29c4lnrxw33gbcj03yvx6yb0c5r9sv0l0zk6qlabll3wfp") (f (quote (("libpnet" "pnet"))))))

(define-public crate-rustcap-0.1.1 (c (n "rustcap") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pcap-sys") (r "^0.1") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (o #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("ws2def" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q7dvdld9g7l6798xxh60gy9byk9c04vm8wbb2hphn6rg3gnw7l1") (f (quote (("libpnet" "pnet"))))))

(define-public crate-rustcap-0.1.2 (c (n "rustcap") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pcap-sys") (r "^0.1") (d #t) (k 0)) (d (n "pnet") (r "^0.21.0") (o #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("ws2def" "ws2ipdef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lymkh4pm0h852kazwwyvgvmsii8dlx8vk0yqybp4syg2dwhy0pl") (f (quote (("libpnet" "pnet"))))))

