(define-module (crates-io ru st rustql_parser) #:use-module (crates-io))

(define-public crate-rustql_parser-0.1.0 (c (n "rustql_parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "rustql_common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "0vl2idmvbqq1hq9mmfrf9sh50hdlbmz5yp743g7rls4d4fvwdl6m")))

