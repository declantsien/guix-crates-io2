(define-module (crates-io ru st rust_mpi) #:use-module (crates-io))

(define-public crate-rust_mpi-0.0.1 (c (n "rust_mpi") (v "0.0.1") (h "1dyr2wpk84ncdpnif69g9v7rwq1p0mx5nblb11i0lv2yxdp1rjcz")))

(define-public crate-rust_mpi-0.0.2 (c (n "rust_mpi") (v "0.0.2") (h "0dhfakb920h0fvyid9r2wgxycjgx1p935bchx3la5xb37cs841hg")))

(define-public crate-rust_mpi-0.0.3 (c (n "rust_mpi") (v "0.0.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ml24vz7rc34y0wmpdi7fjhwlx32jfp7wxmhnzl0jlbb3av9wbhm")))

