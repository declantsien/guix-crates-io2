(define-module (crates-io ru st rustelebot) #:use-module (crates-io))

(define-public crate-rustelebot-0.1.0 (c (n "rustelebot") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1rcgki1rcahisz3v1hwzcnp7d7v6bzjj7yxslb1xrphgbgi7hrm8") (y #t)))

(define-public crate-rustelebot-0.1.1 (c (n "rustelebot") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0saqjvqshgaqxknahxgsdivhzg99ci34724lcvvz278yr3dqfz69") (y #t)))

(define-public crate-rustelebot-0.1.2 (c (n "rustelebot") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "18fqgj9q8cichk1vf7gk3vj9sjkbnxrklim4dgqkgkdjcy2vwq8g") (y #t)))

(define-public crate-rustelebot-0.2.0 (c (n "rustelebot") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "026ndk4p917av6q409l9xy25yc2ishhpclq3fj2w76ynhlxzjagx")))

(define-public crate-rustelebot-0.3.0 (c (n "rustelebot") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0j0if1ra7cxpxx41qx8rsp3s7pxmpjhyfd286cg225qkjky95kpw")))

(define-public crate-rustelebot-0.3.1 (c (n "rustelebot") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1xrk8m6r2gircnl9hmm3f94wwgdj82n6lkj82w1664z6zvagk11y")))

(define-public crate-rustelebot-0.3.2 (c (n "rustelebot") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0ddjjyyy1fl19pm9fifsyync6agf32sdkakjbcjx3knfk43b1fz8")))

