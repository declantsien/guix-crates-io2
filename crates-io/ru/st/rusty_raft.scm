(define-module (crates-io ru st rusty_raft) #:use-module (crates-io))

(define-public crate-rusty_raft-0.1.0 (c (n "rusty_raft") (v "0.1.0") (d (list (d (n "apache-avro") (r "^0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xbsjvhqdgpxf6pk1ygcdivzir23vvlccgl26lw65ilrh5rfvlrm")))

