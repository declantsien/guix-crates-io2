(define-module (crates-io ru st rusty_crypto) #:use-module (crates-io))

(define-public crate-rusty_crypto-0.1.0 (c (n "rusty_crypto") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "03siaasdxpx1lqip4cb750imw7gsgbmcll5bp6sb4vnz5xlzwgwg")))

(define-public crate-rusty_crypto-0.1.1 (c (n "rusty_crypto") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "061nmp6vs9v2f4xr194vprv9i2vi70wzc1zna9sfkcj5rf7mr6my")))

(define-public crate-rusty_crypto-0.1.2 (c (n "rusty_crypto") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0xjm93pwydwdaxrkvx07kh72yll2ljwnhki9im8lsm928d5ilc5f")))

(define-public crate-rusty_crypto-0.1.3 (c (n "rusty_crypto") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "01w29a19qmfqvjdm4m4bvk2yhil4xvlc9pgjv6g74vacv9r09imn")))

(define-public crate-rusty_crypto-0.1.4 (c (n "rusty_crypto") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "13ic1liansxcjsa9a84lippkjl1swybpd0yf7rgjgjn1xi8xspxl")))

(define-public crate-rusty_crypto-0.1.6 (c (n "rusty_crypto") (v "0.1.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1vx2h854dm911kk7sqmyi20m2x0lx337s4zg8cpcldsq9q5g3r62")))

(define-public crate-rusty_crypto-0.1.7 (c (n "rusty_crypto") (v "0.1.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jz4p3d9jqj92kbrmwd930avflfiadim91djwa5byhhhsqbm4rvj")))

(define-public crate-rusty_crypto-0.1.8 (c (n "rusty_crypto") (v "0.1.8") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "050hmq2nhb6v8bf9064wrc4avihvzaa15n53pgd2r7x3jy61qnq1")))

(define-public crate-rusty_crypto-0.1.9 (c (n "rusty_crypto") (v "0.1.9") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ldsa44svmxd7hv150gpk88rj858z5dv9q6nb2sx3sfrx24am7qc")))

(define-public crate-rusty_crypto-0.1.10 (c (n "rusty_crypto") (v "0.1.10") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1gx4kx2yis4chc20cf9r7k0sg0jr2c3pdy9cwky28v3bnaaq344a")))

(define-public crate-rusty_crypto-0.1.11 (c (n "rusty_crypto") (v "0.1.11") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1m6lshkihf78ddshw0lbj92nhk03asbz6v7f1hgpqnyj2vrflg6y")))

(define-public crate-rusty_crypto-0.1.12 (c (n "rusty_crypto") (v "0.1.12") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0lzxgvv4ivxyx7nysqpjbdkxxqdf94ms6a8z9xas0fy06hgrv33j")))

(define-public crate-rusty_crypto-0.1.13 (c (n "rusty_crypto") (v "0.1.13") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14i3cs51mwsqal1vskxbf3wn30hgji2ff7j273cwmiv3fj8ld2i1")))

(define-public crate-rusty_crypto-0.1.14 (c (n "rusty_crypto") (v "0.1.14") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "13gv93vayv4kfy93ifj0cviqc894sjkjv6dhmx81qwqz0nxk4gsh")))

(define-public crate-rusty_crypto-0.1.15 (c (n "rusty_crypto") (v "0.1.15") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vmalykb6pdv3jd2gvajqldhlv6mqw7dw8z7mj1i45pkanxhc4rd")))

