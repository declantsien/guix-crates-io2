(define-module (crates-io ru st rustyard) #:use-module (crates-io))

(define-public crate-rustyard-0.1.0 (c (n "rustyard") (v "0.1.0") (h "12dwiggm3dnrp17247z5ndgm4jlvd0h2m7zimjgi675igyzvicaz")))

(define-public crate-rustyard-0.2.0 (c (n "rustyard") (v "0.2.0") (h "1794kqiqx3icbpd7fvh45rm00f16b5b73b500p3p4fimrb4syz2l")))

(define-public crate-rustyard-0.3.0 (c (n "rustyard") (v "0.3.0") (h "0by4cb13fi768f76n81j167csjv69pnd0ilqik1i9bdy78kpsq7v")))

(define-public crate-rustyard-0.5.0 (c (n "rustyard") (v "0.5.0") (h "0253vx011vkprnny5b8x3g137l8bldpgp303726nk0w66fyma275")))

(define-public crate-rustyard-0.6.0 (c (n "rustyard") (v "0.6.0") (h "13xayzg6mhf59h90mmplll2vx3xjzci8ll52qavnr9qs6jwp13r3")))

