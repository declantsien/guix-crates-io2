(define-module (crates-io ru st rustchain_discord_bot) #:use-module (crates-io))

(define-public crate-rustchain_discord_bot-1.0.0 (c (n "rustchain_discord_bot") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "llm-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "llm-chain-openai") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "poise") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1760p1fk3n4l5n5z5s3y36k7bd2wmbc3x7wl2d765djrbrnhr1ak")))

