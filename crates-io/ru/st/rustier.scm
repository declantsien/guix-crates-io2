(define-module (crates-io ru st rustier) #:use-module (crates-io))

(define-public crate-rustier-0.1.0 (c (n "rustier") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rustier-macros") (r "^0.1") (d #t) (k 0)))) (h "17qjpbvnqjz2qz24zrjmgmd6anbly3i61wkdkz0c1i9zh48iw2wz")))

