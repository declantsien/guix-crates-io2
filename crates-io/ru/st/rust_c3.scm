(define-module (crates-io ru st rust_c3) #:use-module (crates-io))

(define-public crate-rust_c3-0.1.0 (c (n "rust_c3") (v "0.1.0") (d (list (d (n "kd-tree") (r "^0.5.1") (d #t) (k 0)) (d (n "kolor") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "09pivvn01i3sgbrawppbgcq722v1xp413gqzqp0zvacir6b7qjnb")))

(define-public crate-rust_c3-0.1.1 (c (n "rust_c3") (v "0.1.1") (d (list (d (n "kd-tree") (r "^0.5.1") (d #t) (k 0)) (d (n "kolor") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "1rpjjr8fm6krqpwiy49asxk7v2r7fyxl9wb1q782srg9v5sz1wby")))

(define-public crate-rust_c3-0.1.2 (c (n "rust_c3") (v "0.1.2") (d (list (d (n "kd-tree") (r "^0.5.1") (d #t) (k 0)) (d (n "kolor") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "11dbl0f7crh6222mw3m3wvr14yzm101l8v0pj2wkcc2axkdrbmfn")))

(define-public crate-rust_c3-0.1.3 (c (n "rust_c3") (v "0.1.3") (d (list (d (n "kd-tree") (r "^0.5.1") (d #t) (k 0)) (d (n "kolor") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0l722jqpi4b012l153pd86whbzi2s4pgx468gynbfq0cnsr0ix6z")))

(define-public crate-rust_c3-0.1.4 (c (n "rust_c3") (v "0.1.4") (d (list (d (n "kd-tree") (r "^0.5.1") (d #t) (k 0)) (d (n "kolor") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1hx0v1fwygq252zqbwn2s5iqbaym3qjlahcky4c29m75j01ymij2")))

(define-public crate-rust_c3-0.1.5 (c (n "rust_c3") (v "0.1.5") (d (list (d (n "kd-tree") (r "^0.5.1") (d #t) (k 0)) (d (n "kolor") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "09vz949bynb77g005anmzx6szd8j1k14bq5ra6rvknwcrxis47rh")))

