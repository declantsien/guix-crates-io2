(define-module (crates-io ru st rusty_dumb_tools) #:use-module (crates-io))

(define-public crate-rusty_dumb_tools-0.1.0 (c (n "rusty_dumb_tools") (v "0.1.0") (h "0lc914v6c1lx8ahfyz4s7l5b1imw5xrx28aigxva01g5fdxp5gsq")))

(define-public crate-rusty_dumb_tools-0.1.1 (c (n "rusty_dumb_tools") (v "0.1.1") (h "0vqs2xb4y06152wrd7nx1hzinbfganxi5mqpa06cz0h5kcbxssan")))

(define-public crate-rusty_dumb_tools-0.1.2 (c (n "rusty_dumb_tools") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "iced") (r "^0.10") (d #t) (k 0)))) (h "0jjjv5rq296m4pjv2vxngmgy84idg070vj8p4z93fb1yxja4np7g")))

(define-public crate-rusty_dumb_tools-0.1.3 (c (n "rusty_dumb_tools") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "iced") (r "^0.10") (d #t) (k 0)))) (h "0l52ifhkwppgr9cqin5blvfjzz7xp57bzvyhsjjka27465iak0gp") (y #t)))

(define-public crate-rusty_dumb_tools-0.1.3-r1 (c (n "rusty_dumb_tools") (v "0.1.3-r1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0n6pykbh54ny5s12x88ckc5jgjj7v0lvvaaxh6lpwdkrz2cj5837") (y #t)))

(define-public crate-rusty_dumb_tools-0.1.4 (c (n "rusty_dumb_tools") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "1nb4lvhm792gh2czzad4879x4x2g32azcn2bcy2yzwhfcgd6r0kb")))

(define-public crate-rusty_dumb_tools-0.1.5 (c (n "rusty_dumb_tools") (v "0.1.5") (h "028crr1prn3zav2pypbnqr64851gag0kv47kibldnb1vvzvfm5r1")))

(define-public crate-rusty_dumb_tools-0.1.6 (c (n "rusty_dumb_tools") (v "0.1.6") (h "031wy2vs0p4gdlza1n6v3z5kk02hy5y9cvmf0ihyfm652ww25b0f")))

(define-public crate-rusty_dumb_tools-0.1.7 (c (n "rusty_dumb_tools") (v "0.1.7") (h "01nz8misa1pqsqdcx02c8y8j6jmm1jgjh4vpyrmkaglvnf62ash7")))

(define-public crate-rusty_dumb_tools-0.1.8 (c (n "rusty_dumb_tools") (v "0.1.8") (h "1sdx62bhi5hrwj4r6njb1c17dg9xnj6rlz2n3cg2ak4jglpg9xlc")))

(define-public crate-rusty_dumb_tools-0.1.9 (c (n "rusty_dumb_tools") (v "0.1.9") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "1f0qrmjw6nzfmzp50am1w5m11visa1nb7xngghj4kvdhdkn2vzja")))

(define-public crate-rusty_dumb_tools-0.1.10 (c (n "rusty_dumb_tools") (v "0.1.10") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "036rb1gjl8bqpfq7xsqvia173pmx6s8ysqj4m273jmfq16j3k77g")))

(define-public crate-rusty_dumb_tools-0.1.11 (c (n "rusty_dumb_tools") (v "0.1.11") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "042cypx20iabnd3gngsfj62sj20xh55ycs8zq63i65k6jfsndcfv")))

(define-public crate-rusty_dumb_tools-0.1.12 (c (n "rusty_dumb_tools") (v "0.1.12") (h "0l6kyqqjcifjc578b3j2a2wsl6ga7rs308ybsln05djlhpyfq5cl")))

(define-public crate-rusty_dumb_tools-0.1.13 (c (n "rusty_dumb_tools") (v "0.1.13") (h "03w58zkgfbj1yij74m1djg7zyk3nzp24krcx5n20amgyd0qr5xnd")))

