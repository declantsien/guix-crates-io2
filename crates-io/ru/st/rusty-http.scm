(define-module (crates-io ru st rusty-http) #:use-module (crates-io))

(define-public crate-rusty-http-0.0.0 (c (n "rusty-http") (v "0.0.0") (h "0rkp9yp66j8fbhnvdiw45512c60f8mv9m2ggn5p441j2lm9fx21a") (y #t)))

(define-public crate-rusty-http-0.1.0 (c (n "rusty-http") (v "0.1.0") (h "0llpps9b1i21l7f4ddanrqisr6l96w4wkml1wbqxwv7qb2v6s9pk") (y #t)))

