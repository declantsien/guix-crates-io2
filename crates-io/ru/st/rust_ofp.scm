(define-module (crates-io ru st rust_ofp) #:use-module (crates-io))

(define-public crate-rust_ofp-0.1.0 (c (n "rust_ofp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0833rq01wz0xpm7mam3fcmki4ciwnlbdqir9i3zgh9nsbi7hd2pk")))

(define-public crate-rust_ofp-0.2.0 (c (n "rust_ofp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1r9nzkc9dgamlrfq9vp943m2agv48c9jmcflqq22wjg137324xq4")))

(define-public crate-rust_ofp-0.2.1 (c (n "rust_ofp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1rxrffmg6k1z62b6gsx7p1i4ng8imng5i66apyszdj2rkp319b2d")))

