(define-module (crates-io ru st rustcolor) #:use-module (crates-io))

(define-public crate-rustcolor-0.4.0 (c (n "rustcolor") (v "0.4.0") (h "02ldmwqw912knrgadyscli22zylgn4n6ydlzxf4rzc3dvw6bk78k") (y #t)))

(define-public crate-rustcolor-0.4.1 (c (n "rustcolor") (v "0.4.1") (h "1kdmx28ns723zl2mpl5w59knmdxg7pbqa480720aw166c3cz6rpa")))

(define-public crate-rustcolor-0.5.0 (c (n "rustcolor") (v "0.5.0") (h "06czqm2b3pgiy19bdj4cpwq3k5y0kdd0vfp9chfh6m7d6gl8kg6d")))

