(define-module (crates-io ru st rusty-money) #:use-module (crates-io))

(define-public crate-rusty-money-0.1.0 (c (n "rusty-money") (v "0.1.0") (d (list (d (n "rust_decimal") (r "^1.0.3") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.0.3") (d #t) (k 0)))) (h "05lnqd191xnrk6nl953k17w2ib5fxavyhrvjc71vl1rndba9wq5q")))

(define-public crate-rusty-money-0.2.0 (c (n "rusty-money") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.3") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dv75901p5ycz4av3b7x34ipm8kz0hcy28byxlx00b1l3qmivdfh")))

(define-public crate-rusty-money-0.3.0 (c (n "rusty-money") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.1.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.1.0") (d #t) (k 0)))) (h "01yykcb1pwz3812qaz8n33pb4qfjpl2ivn3x3dfnidls9897vibi")))

(define-public crate-rusty-money-0.3.1 (c (n "rusty-money") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.1.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.1.0") (d #t) (k 0)))) (h "0ry2qr1s6pr90g13kdx37g4sbgx3brdrfki54j75g1zr1964dqqh")))

(define-public crate-rusty-money-0.3.2 (c (n "rusty-money") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.2.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.2.0") (d #t) (k 0)))) (h "0lsp719f74rmjycl0yqz1a04hm84ws1l814vg5gag08x0h95irxx")))

(define-public crate-rusty-money-0.3.3 (c (n "rusty-money") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.3.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.3.0") (d #t) (k 0)))) (h "08gjsdl1jybs22pp4l33glvi2g5wcyhdz9fzarn0ab506vjcvswr")))

(define-public crate-rusty-money-0.3.4 (c (n "rusty-money") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.4.1") (d #t) (k 0)))) (h "03yciy3zlz4px9iy7b589scq2614ny8b3dhysz2bddl8f1xyk7c2")))

(define-public crate-rusty-money-0.3.5 (c (n "rusty-money") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.7.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.7.0") (d #t) (k 0)))) (h "1pm709wa70n9l1r1gw10acfzd4kn4l8xc1w01ka0yg2a958w9lp0")))

(define-public crate-rusty-money-0.3.6 (c (n "rusty-money") (v "0.3.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.8.1") (d #t) (k 0)))) (h "1lh4d9hs71pdlw0xpik4s2kgx7j1ad85a07pw7wqz8mmd5c4dlcl")))

(define-public crate-rusty-money-0.4.0 (c (n "rusty-money") (v "0.4.0") (d (list (d (n "rust_decimal") (r "^1.9.0") (k 0)) (d (n "rust_decimal_macros") (r "^1.9.0") (d #t) (k 0)))) (h "1npj4zqj53z6fx9pjs40fa846cnhxy27mv469c2053whkmsijvb5") (f (quote (("iso") ("default" "iso") ("crypto"))))))

(define-public crate-rusty-money-0.4.1 (c (n "rusty-money") (v "0.4.1") (d (list (d (n "rust_decimal") (r "^1.9.0") (k 0)) (d (n "rust_decimal_macros") (r "^1.9.0") (d #t) (k 0)))) (h "10ynmpfr95h6zhh8g8w39xzvs8pklmfhfvvbskc7mb2y020zha2v") (f (quote (("iso") ("default" "iso") ("crypto"))))))

