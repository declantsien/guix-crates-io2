(define-module (crates-io ru st rustrawi) #:use-module (crates-io))

(define-public crate-rustrawi-0.1.0 (c (n "rustrawi") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "103yvgws570g3pap4h8lanp41fman2hd87965n6m8w9sfikxlx65") (y #t)))

(define-public crate-rustrawi-0.1.1 (c (n "rustrawi") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0gbrgjs8hckw627sgzm04361qffkgjsz311nbx7gwzwqzn3wvpxw") (y #t)))

(define-public crate-rustrawi-0.1.2 (c (n "rustrawi") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0s19fwgslpp37v12vdp2c1pns2bkz36iqb16cliqnm2kvggv7nvw")))

