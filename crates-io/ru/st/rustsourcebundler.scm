(define-module (crates-io ru st rustsourcebundler) #:use-module (crates-io))

(define-public crate-rustsourcebundler-0.1.1 (c (n "rustsourcebundler") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "067cr20frq8v9vysny0ajjlwp3ghn7jl23s2p9h11wdj7944diwp")))

(define-public crate-rustsourcebundler-0.2.0 (c (n "rustsourcebundler") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1x2rl91j5mqzvgpn4d1zhsqzvlg5v2sj0kpx06ahds1ba0h1qa29")))

(define-public crate-rustsourcebundler-0.3.0 (c (n "rustsourcebundler") (v "0.3.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "06pl52b6f88g6yihmmi3vcdpi4d29la8r0rkjp6mg8lg9i23f4i6")))

(define-public crate-rustsourcebundler-0.4.0 (c (n "rustsourcebundler") (v "0.4.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0an6q0m62xxhhx7kbhgxdhsk4k9hssnfc5fbpkfma0mg3dpzgzx0") (y #t)))

(define-public crate-rustsourcebundler-0.5.0 (c (n "rustsourcebundler") (v "0.5.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "15ss65jyzdwqxjp8vb9isllpaj1qz9j8s4qw04ra0bm2ii4ln3r4")))

(define-public crate-rustsourcebundler-0.6.0 (c (n "rustsourcebundler") (v "0.6.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1kk6jpxbryml97npw27n812anp5ra6zmnifvdvdr5lqi1bwifx2q")))

(define-public crate-rustsourcebundler-0.8.0 (c (n "rustsourcebundler") (v "0.8.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0721skigdj0pyaw1i9svw7vfl8g5fjxfxlvz79mjvd5zkx9vdss7")))

(define-public crate-rustsourcebundler-0.9.0 (c (n "rustsourcebundler") (v "0.9.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0wgdyp8nhgmc1gsppq8x67bmzlkwgrfp9yxwg2bf63yn53s1y50v")))

(define-public crate-rustsourcebundler-0.10.0 (c (n "rustsourcebundler") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "02rv91a781x9lq60ifjba1gb5hvbcgmi2c0j58cga8b5vf2c4yfa")))

(define-public crate-rustsourcebundler-0.10.1 (c (n "rustsourcebundler") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "10k94ffdmy38p774c48ns4lc8fxkcyxfqjrjrgygi4xw9j7mbvjp")))

(define-public crate-rustsourcebundler-0.10.2 (c (n "rustsourcebundler") (v "0.10.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.12.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "07r7szfyqydf3p7q9wps03gr9ddrwmz49x726hzjbxlg6flclsbf")))

(define-public crate-rustsourcebundler-0.11.0 (c (n "rustsourcebundler") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1xr7p655bmvdygy9a6d3hhggwka1d8njzkzdarn1adnxs6hgzvqx")))

(define-public crate-rustsourcebundler-0.11.1 (c (n "rustsourcebundler") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.4.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1jqsc8x7vwk3ab1475hx5s8l8fj9xhw5k6412x6hznnldvvca26s")))

(define-public crate-rustsourcebundler-0.11.2 (c (n "rustsourcebundler") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.15.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.4.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0rfdpz3dzclhpg7skp2jg8sb26phjf4k9gz2vcy3wnddpw0afdm1")))

(define-public crate-rustsourcebundler-0.11.3 (c (n "rustsourcebundler") (v "0.11.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.5.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "1vl7cymglrxkggh57vbjnq7q4zidbfd3hs616cdi4zz4pdzkkgra")))

(define-public crate-rustsourcebundler-0.11.4 (c (n "rustsourcebundler") (v "0.11.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)))) (h "15hi6zj6y364dhw79jway4gd25qkwpmms6c98vg65s3xmnfggfgg")))

(define-public crate-rustsourcebundler-0.11.5 (c (n "rustsourcebundler") (v "0.11.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "1qfbzvbrrwhn37bvchshablbd5fl5n8kbmfgg09ifap6zjnbwp84")))

(define-public crate-rustsourcebundler-0.11.6 (c (n "rustsourcebundler") (v "0.11.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.19.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "0j166r86rfpfjy1g7j0v0827j4px9i9zin3v1rp98ns1c83mvfw1")))

(define-public crate-rustsourcebundler-0.11.7 (c (n "rustsourcebundler") (v "0.11.7") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "cargo_toml") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "goldenfile") (r "^1.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "11pvxsgi7fzmrvkp1whmmzai0dk83m4jyrz61arbf69z3r27ifxp")))

