(define-module (crates-io ru st rust_process_interface_library) #:use-module (crates-io))

(define-public crate-rust_process_interface_library-0.1.0 (c (n "rust_process_interface_library") (v "0.1.0") (h "0v4mdhwj4crazgyy9y5i2whxx30w00bya2arik41xwxr8xx8652n")))

(define-public crate-rust_process_interface_library-0.1.1 (c (n "rust_process_interface_library") (v "0.1.1") (h "0ggxiz1dlxm1zhikwwhw4lfxyninm30jnkxn6da3ya62szl7qdl6")))

(define-public crate-rust_process_interface_library-0.1.2 (c (n "rust_process_interface_library") (v "0.1.2") (h "0k4z5grbh2an3wg7z2gbl54a96nb4c6h7hi862bjfm8n7vpic6pz")))

(define-public crate-rust_process_interface_library-0.1.3 (c (n "rust_process_interface_library") (v "0.1.3") (h "1a4ipf8q8yiv8s6lmr1h3r0zwla9sayn4mlfkq9rdanlmw9ady3m")))

(define-public crate-rust_process_interface_library-0.1.4 (c (n "rust_process_interface_library") (v "0.1.4") (h "1mhpbw24jsgs5ka3k7xb7s157pwksr5sj8py822lxm241xyy5n6f")))

