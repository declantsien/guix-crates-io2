(define-module (crates-io ru st rusty_drone) #:use-module (crates-io))

(define-public crate-rusty_drone-0.1.0 (c (n "rusty_drone") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02s2fg9045r1f7wvs7k5wgw225prfhxjq2dm93s8y1c0rjf0rk0p")))

(define-public crate-rusty_drone-0.1.1 (c (n "rusty_drone") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k45vmn0wqwyikjg617rhnn65sw8ian3h72v9gvsd17xvvn7hddh")))

(define-public crate-rusty_drone-0.1.2 (c (n "rusty_drone") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x0n4fbfjlahfpab722s5wy9zbi7zv31k4rdm9d38fx512q3vi9l")))

