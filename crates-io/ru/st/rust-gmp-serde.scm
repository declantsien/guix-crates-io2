(define-module (crates-io ru st rust-gmp-serde) #:use-module (crates-io))

(define-public crate-rust-gmp-serde-0.5.0 (c (n "rust-gmp-serde") (v "0.5.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wq88bffyf0isl7s4hlp1bmib5h7rr3g8kckawnr9bf0scgr2bi8") (f (quote (("serde_support" "serde" "serde_derive" "serde_json") ("default"))))))

