(define-module (crates-io ru st rusty_time) #:use-module (crates-io))

(define-public crate-rusty_time-0.11.0 (c (n "rusty_time") (v "0.11.0") (h "10s0vbyll04h92xcacj6x6r88wrlnj8jfzxnfgind1gw6kgbsz1h")))

(define-public crate-rusty_time-0.11.1 (c (n "rusty_time") (v "0.11.1") (h "0cvpw8qccycfsjd25jvyi3ffcn41g8n4kaj0mk0ajgm4jnza656i")))

(define-public crate-rusty_time-0.11.3 (c (n "rusty_time") (v "0.11.3") (h "1yaydnb6qaaiz2qbv2vm3mr8l1krlr1a5axf19r4bzyn05lqylpa")))

(define-public crate-rusty_time-0.12.0 (c (n "rusty_time") (v "0.12.0") (h "1cpjdswdzkpj8syfy78rzzsdab2a87l4b48fsyp12aibr780xfm7")))

(define-public crate-rusty_time-1.0.0 (c (n "rusty_time") (v "1.0.0") (h "0bvvb0cfgmqimq8fmd6ij90vkfs503dfmj3r6xjnv0k64ck0wzj4")))

(define-public crate-rusty_time-1.1.0 (c (n "rusty_time") (v "1.1.0") (h "1v06p443f5wgpcf2ifvq7sqcjl9ah77imbnhjw8lifsqvqr35w8m")))

