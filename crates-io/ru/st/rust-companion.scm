(define-module (crates-io ru st rust-companion) #:use-module (crates-io))

(define-public crate-rust-companion-0.1.0 (c (n "rust-companion") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "logging") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.7") (d #t) (k 0)) (d (n "syslog") (r "^6.0") (o #t) (d #t) (k 0)))) (h "0zr9ka07l7xjrf5dsd9hfz8bh05v4b747n8nw5k05mlqijaq3qk7") (f (quote (("log" "logging" "syslog") ("default"))))))

(define-public crate-rust-companion-0.1.1 (c (n "rust-companion") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "logging") (r "^0.4") (o #t) (d #t) (k 0) (p "log")) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.28") (d #t) (k 0)) (d (n "syslog") (r "^6.0") (o #t) (d #t) (k 0)))) (h "12vai448rqd61scknqkgzpaywny9573algw8hqh3h2270jrq9w1l") (f (quote (("log" "logging" "syslog") ("default"))))))

