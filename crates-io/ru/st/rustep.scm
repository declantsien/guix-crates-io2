(define-module (crates-io ru st rustep) #:use-module (crates-io))

(define-public crate-rustep-0.1.0 (c (n "rustep") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33.2") (d #t) (k 1)) (d (n "enumflags") (r "^0.3.0") (d #t) (k 0)) (d (n "enumflags_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1sngndvphx2a9w3p6ckm95vby7lf7mqf4wz9gmcnbpfs53l1b2q2")))

(define-public crate-rustep-0.1.1 (c (n "rustep") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.33.2") (d #t) (k 1)) (d (n "enumflags") (r "^0.3.0") (d #t) (k 0)) (d (n "enumflags_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1p8xl96r33pzy4s4aq1a5ha293lrcr2jc7d3g7w928syn0zm9hrx")))

(define-public crate-rustep-0.1.2 (c (n "rustep") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.33.2") (d #t) (k 1)) (d (n "enumflags") (r "^0.3.0") (d #t) (k 0)) (d (n "enumflags_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0aaivqkr65x2pqxp9jldy16bi845m1w2j9l8z39zsyx96wpjwp9q")))

