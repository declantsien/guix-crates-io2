(define-module (crates-io ru st rust-rapport) #:use-module (crates-io))

(define-public crate-rust-rapport-0.0.1 (c (n "rust-rapport") (v "0.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01aqx7ic5dnd37yhjzw8xrzri8cmyinbj4h1a5s55s4flixsylh5")))

