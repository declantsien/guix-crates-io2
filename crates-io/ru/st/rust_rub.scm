(define-module (crates-io ru st rust_rub) #:use-module (crates-io))

(define-public crate-rust_rub-0.0.1 (c (n "rust_rub") (v "0.0.1") (d (list (d (n "buildable") (r "= 0.0.1") (d #t) (k 0)) (d (n "docopt") (r "= 0.6.14") (d #t) (k 0)))) (h "0ki6yb1jij3fij7scql8qzwpzrbzbbv9kvkvcz25apjdj7s2687d")))

(define-public crate-rust_rub-0.0.2 (c (n "rust_rub") (v "0.0.2") (d (list (d (n "buildable") (r "= 0.0.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)))) (h "10d4z1ncm9749z1iffkc388bjznmi72akfj0a1v7c495nqmz238k")))

(define-public crate-rust_rub-0.0.3 (c (n "rust_rub") (v "0.0.3") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "1qgl5bykb06ib0l1b97v3q48s3as609qm4xv7dv006g4gp3i6jri")))

(define-public crate-rust_rub-0.0.4 (c (n "rust_rub") (v "0.0.4") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "1v6d89nnn292k6a2fzzy573669ff6rrhpf0cwj1v6vb0zxw7p70q")))

(define-public crate-rust_rub-0.0.5 (c (n "rust_rub") (v "0.0.5") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "10pk6mgxxs0ijl6pm8dw1m87crg6a54d8f7487j92pjcbyln4ck3")))

