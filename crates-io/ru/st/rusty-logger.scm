(define-module (crates-io ru st rusty-logger) #:use-module (crates-io))

(define-public crate-rusty-logger-0.1.0 (c (n "rusty-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "19wawz4kg38sshvb3ybz3ljkm5xqayss866fihjv1wavdwgm7rcx")))

(define-public crate-rusty-logger-0.1.1 (c (n "rusty-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "11p5mid3sy711nci07vjjnrrdyv288bshhpcz4xb3nhjf4vc5laa")))

(define-public crate-rusty-logger-0.2.0 (c (n "rusty-logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1ml25swyn5fslj67q5pk7zybq2xgz6n80nbjxfll9ymhr2i2g5kj")))

(define-public crate-rusty-logger-0.3.0 (c (n "rusty-logger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "09h3v30hvkng8bbjmzxd5rg06qgfsnp91q48qlv55n2pk5k385m8")))

