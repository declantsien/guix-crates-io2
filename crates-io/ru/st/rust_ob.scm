(define-module (crates-io ru st rust_ob) #:use-module (crates-io))

(define-public crate-rust_ob-1.0.1 (c (n "rust_ob") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1fwripxb7wyyhbrvn0a44g0dv7hss9w10w8ijyzjphqym4hc0xzc")))

(define-public crate-rust_ob-1.0.2 (c (n "rust_ob") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "0a2vqllil2anawcxfiyvzmdylpxa8q1dlbm3pvdfniwdk6hn86b2")))

(define-public crate-rust_ob-1.0.3 (c (n "rust_ob") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1kfmmmcfm6zqx2qaps5ry0q9adkg068d90kg741798lnpwqp75zj")))

(define-public crate-rust_ob-1.0.4 (c (n "rust_ob") (v "1.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1b3bkgs6f9kdrp01bs3faxz19cl21f6zyza6l7m9yj7v5ck2yn2i")))

(define-public crate-rust_ob-1.0.5 (c (n "rust_ob") (v "1.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1kx2x161rg1bdkk1nx8hiysbbll7wchsfs7gbmfsrkbpxnsm9ziy")))

(define-public crate-rust_ob-1.0.6 (c (n "rust_ob") (v "1.0.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "0534i9sa5nanc5h4lls9bcxpbq75svwsdcfq8fb9k2p1pjwrbn9v")))

(define-public crate-rust_ob-2.0.0 (c (n "rust_ob") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1x7fkq8993frh9p9cjs295j3pnr29fa1a11gs2dld3jiw1b25zy1")))

(define-public crate-rust_ob-2.0.1 (c (n "rust_ob") (v "2.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1vnbbhdjwsk2kddkn1msnxp15dqsw2b488vzzs7jn15cq02l67pl")))

(define-public crate-rust_ob-2.1.0 (c (n "rust_ob") (v "2.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "03sm7jdqqlrmcdg5ryimbcf7mas0wkc0c3j5a1zkb2mjcfzi9nd2")))

(define-public crate-rust_ob-2.1.1 (c (n "rust_ob") (v "2.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1lvcv7rfhd52f5749ib5xap3vqjldg993gxgb1ncn6bpyjxv35wj")))

(define-public crate-rust_ob-2.1.2 (c (n "rust_ob") (v "2.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1im4c3vk9pmw4qlbcj996arihhlya50vk8rqj9n1am32a2gz1n7k")))

(define-public crate-rust_ob-2.1.3 (c (n "rust_ob") (v "2.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "0l4znv4pawqsrcpjfvycgan141xbdi7cvdm1hl3zq14mrm1k2hs1")))

(define-public crate-rust_ob-2.2.0 (c (n "rust_ob") (v "2.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1spnkwhwq4kdjgsrx42s4biqxkld7p3j352rwin2i28mqmizg7sp")))

(define-public crate-rust_ob-2.2.1 (c (n "rust_ob") (v "2.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "0vbx8vs3zx81gqvv6j68n8jf01lwcmvd5bjjyhvjavvaxlfyjblz")))

(define-public crate-rust_ob-2.3.0 (c (n "rust_ob") (v "2.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1j3bdwiqqf02r4zfp0f7k9przk00dhanqqw8fvq5z5rwq6igbxaw")))

(define-public crate-rust_ob-2.4.0 (c (n "rust_ob") (v "2.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1jipn5hdilk7fyv5x6ssqljngph3dc14ac5hk990n1c1p1vnjz4g")))

(define-public crate-rust_ob-2.5.0 (c (n "rust_ob") (v "2.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "016k4axrrspwc6y74d8h3jnp8dkvpwdj8jvqgk95pk51bghczxah")))

(define-public crate-rust_ob-2.5.1 (c (n "rust_ob") (v "2.5.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "0qy29hp27jny9bqbmk00c9yd4q1q8sh7167k9ad7idyx2zs5sdsl")))

(define-public crate-rust_ob-2.5.2 (c (n "rust_ob") (v "2.5.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)))) (h "1jijb37gnrqfs9nvkf5kh1sajx35bd809384wrq5av6p07rwbda7")))

