(define-module (crates-io ru st rustc-ap-rustc_macros) #:use-module (crates-io))

(define-public crate-rustc-ap-rustc_macros-441.0.0 (c (n "rustc-ap-rustc_macros") (v "441.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1ryskaf4fjwyyjvp9iqwlngr0626gl7z17gsynn8v71yxs6485ak")))

(define-public crate-rustc-ap-rustc_macros-442.0.0 (c (n "rustc-ap-rustc_macros") (v "442.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0xfqgkjjw3dcr4lad8xbcx8k5rr6mgvg11ni05cdmz3mc9dmly73")))

(define-public crate-rustc-ap-rustc_macros-443.0.0 (c (n "rustc-ap-rustc_macros") (v "443.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1i6gc92dg18520pgpgnxnh73vshjw0wvcs45skf8cpvrbpynbdj4")))

(define-public crate-rustc-ap-rustc_macros-444.0.0 (c (n "rustc-ap-rustc_macros") (v "444.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0abzfmk6fhb115kv2knwlk0mgcs215cc18mw282f3v7292kljwnh")))

(define-public crate-rustc-ap-rustc_macros-445.0.0 (c (n "rustc-ap-rustc_macros") (v "445.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0nb7bmmfkq8m08k47fd1iyrp44gzz30r7vnn5drb6905j1kalp4k")))

(define-public crate-rustc-ap-rustc_macros-446.0.0 (c (n "rustc-ap-rustc_macros") (v "446.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1jgdvckpjcz7a6cvzy7iqq5wkk5nmjsdyip56w5bmjzpasj7a1ni")))

(define-public crate-rustc-ap-rustc_macros-447.0.0 (c (n "rustc-ap-rustc_macros") (v "447.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0iarc8cv7vip14370145m0d3d9s9mydb05qr463j4p1d26xk124x")))

(define-public crate-rustc-ap-rustc_macros-448.0.0 (c (n "rustc-ap-rustc_macros") (v "448.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1ancbvgjp30j007chi09nd3m32al12pzgykqf72r4jjqcl5wp33f")))

(define-public crate-rustc-ap-rustc_macros-449.0.0 (c (n "rustc-ap-rustc_macros") (v "449.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1hf94v0az8l398mfyj8bhm1y04mqc8j6770i0s4zs3l8bsn3hncb")))

(define-public crate-rustc-ap-rustc_macros-450.0.0 (c (n "rustc-ap-rustc_macros") (v "450.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1s9k8g1rbw0viyzdz46m9d3mrj5j8kp3xhgbi7qkq83gd8xjszf3")))

(define-public crate-rustc-ap-rustc_macros-451.0.0 (c (n "rustc-ap-rustc_macros") (v "451.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1ds8sr3b4pl82abg7k8mp65lww6pqfv7x6mhwmvp8kc9il194q76")))

(define-public crate-rustc-ap-rustc_macros-452.0.0 (c (n "rustc-ap-rustc_macros") (v "452.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0smlhlr4vpxfn96lrazm0zhfag49icazk4m8hrd5387gpy4g8lsj")))

(define-public crate-rustc-ap-rustc_macros-453.0.0 (c (n "rustc-ap-rustc_macros") (v "453.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0gpgkvbwxzh4xbzjak4dfkbya7c9lgp5vk3rxdspkflzjma5vjw8")))

(define-public crate-rustc-ap-rustc_macros-454.0.0 (c (n "rustc-ap-rustc_macros") (v "454.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "174gjyqx4cj5fzgcxp1n1ls4s0y5nxbbny0gnykz6qgbk554dnm4")))

(define-public crate-rustc-ap-rustc_macros-455.0.0 (c (n "rustc-ap-rustc_macros") (v "455.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0vmpc95h0fcr5ajfcp5yfjszfynn052rdsjd450rq3pyzsr4bl4z")))

(define-public crate-rustc-ap-rustc_macros-456.0.0 (c (n "rustc-ap-rustc_macros") (v "456.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0v6qr86pzcdcid3r2nnclv63j8cyp6g7ycj21c8kab56lr3jhv2f")))

(define-public crate-rustc-ap-rustc_macros-457.0.0 (c (n "rustc-ap-rustc_macros") (v "457.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1rs5zbk51ppm73c1p54afby81n7f8lp2cn2crvi9a1zc0gkwwp46")))

(define-public crate-rustc-ap-rustc_macros-458.0.0 (c (n "rustc-ap-rustc_macros") (v "458.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1pm69fndrszplzc5l64900ksr1pqd2sym9a74g01j4s3i184zxzz")))

(define-public crate-rustc-ap-rustc_macros-459.0.0 (c (n "rustc-ap-rustc_macros") (v "459.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "10ajykh99m0nynvil4jaw8vm98n482r7h857ilk955askh2qwmyc")))

(define-public crate-rustc-ap-rustc_macros-460.0.0 (c (n "rustc-ap-rustc_macros") (v "460.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0mb53slx8c8437gm70bxhgraynv4dnxzsmrywmxancnxykfw82dd")))

(define-public crate-rustc-ap-rustc_macros-461.0.0 (c (n "rustc-ap-rustc_macros") (v "461.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1d5rikagb3376l48afss4sb65fxk9icii6bgsym9vim7rkivsnlp")))

(define-public crate-rustc-ap-rustc_macros-462.0.0 (c (n "rustc-ap-rustc_macros") (v "462.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0m0qdm67nksfr6n33f8lhwg50jy8f3rfrlla6ysyxfjpkbj5w0fl")))

(define-public crate-rustc-ap-rustc_macros-463.0.0 (c (n "rustc-ap-rustc_macros") (v "463.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1r8sijvaysa6cwyjmpg8cfbxscihwz7mnv4lnn21pjzaysnyvsvv")))

(define-public crate-rustc-ap-rustc_macros-464.0.0 (c (n "rustc-ap-rustc_macros") (v "464.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0rh6zh623d03iv6bgdxasgfgws4nrglvbq1amb7wb9q728s2azhw")))

(define-public crate-rustc-ap-rustc_macros-465.0.0 (c (n "rustc-ap-rustc_macros") (v "465.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0khkwx3x6rhx7dmyh14i4d1r2ilwllv1cvd8q209d1s59x06xaz1")))

(define-public crate-rustc-ap-rustc_macros-466.0.0 (c (n "rustc-ap-rustc_macros") (v "466.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1pc5lqyy2wyxfmvj2jlj45iy4wnwddsyzn17q1bjsxf5dlmh7l5v")))

(define-public crate-rustc-ap-rustc_macros-467.0.0 (c (n "rustc-ap-rustc_macros") (v "467.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0p900jv15fscd27s4p5jwjrvwhg8f4rq9x0hf5xxwnbs5i4y3glj")))

(define-public crate-rustc-ap-rustc_macros-468.0.0 (c (n "rustc-ap-rustc_macros") (v "468.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0c2kvy3nymx3jdnw6qlrxkc4gjwp3gxsi9g38bncm138yfp5hkcg")))

(define-public crate-rustc-ap-rustc_macros-469.0.0 (c (n "rustc-ap-rustc_macros") (v "469.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1ifs1nlxv49m4jp3mja35lcbgynprnd6c71cpfvmf5lvgrm7dgnk")))

(define-public crate-rustc-ap-rustc_macros-470.0.0 (c (n "rustc-ap-rustc_macros") (v "470.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1kz21lvapj1r0s2xv9fy8fcxs32k2pk85gbp82sdd5sgdcfbrihv")))

(define-public crate-rustc-ap-rustc_macros-471.0.0 (c (n "rustc-ap-rustc_macros") (v "471.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0xrwpn8zryvhrpf4ml12129h0sb3dpk1v164r93hx6j4b5pn1b6z")))

(define-public crate-rustc-ap-rustc_macros-472.0.0 (c (n "rustc-ap-rustc_macros") (v "472.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0c658n7fm31s6vy12wxlc1prmkw3hhpvgvmn9sbj9jyzd431v0x0")))

(define-public crate-rustc-ap-rustc_macros-473.0.0 (c (n "rustc-ap-rustc_macros") (v "473.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0lbzwjyzzn105vpsyadhihzhyvzmvf3facvg4sn4imwg6y8r61fv")))

(define-public crate-rustc-ap-rustc_macros-474.0.0 (c (n "rustc-ap-rustc_macros") (v "474.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1yy8h1jx5clbhaqigshxnnscg154nn9cqf87kldv1svl7hwbpcl2")))

(define-public crate-rustc-ap-rustc_macros-475.0.0 (c (n "rustc-ap-rustc_macros") (v "475.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "16zya4sl48j7f32w9fjcrfqn4igfr29k48pzb681y22sf75f0265")))

(define-public crate-rustc-ap-rustc_macros-476.0.0 (c (n "rustc-ap-rustc_macros") (v "476.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1nnr5c8qsjfjglgszxwssgrsh0y0c41mkgcsrnrpmz9cjnjx1hav")))

(define-public crate-rustc-ap-rustc_macros-477.0.0 (c (n "rustc-ap-rustc_macros") (v "477.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1a4ncmpybqmdwiic6z4s3bvdm8xa4006gbbxxc5kqqv5zjwd6mzg")))

(define-public crate-rustc-ap-rustc_macros-478.0.0 (c (n "rustc-ap-rustc_macros") (v "478.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "18z69fwmbmpvpb0zz88z2yfsl86y4z4k2xirccxbkz6zm015bnby")))

(define-public crate-rustc-ap-rustc_macros-479.0.0 (c (n "rustc-ap-rustc_macros") (v "479.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "08m6ykf154jln3hz6x24pdcsp71v5nfv6s4r8hqazf59dvzjld28")))

(define-public crate-rustc-ap-rustc_macros-480.0.0 (c (n "rustc-ap-rustc_macros") (v "480.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0yx8s3zaqmnmvd1i76xrqi4nc7jxahx5yzbh5y44wqdzhh586zfn")))

(define-public crate-rustc-ap-rustc_macros-481.0.0 (c (n "rustc-ap-rustc_macros") (v "481.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1l9331n4l51sqhg39s8di94drn2kwrnm176qz015hbknvjg6gal6")))

(define-public crate-rustc-ap-rustc_macros-482.0.0 (c (n "rustc-ap-rustc_macros") (v "482.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0sbl1p5qnly5iw5a7lg554rdmvksbrlyy7a9x74sgvzqw41v592x")))

(define-public crate-rustc-ap-rustc_macros-483.0.0 (c (n "rustc-ap-rustc_macros") (v "483.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1k2wmwyz8laq96vkf5pmhs6zs0c2jzqhyr8qdk7vlhkaq1zgn0na")))

(define-public crate-rustc-ap-rustc_macros-484.0.0 (c (n "rustc-ap-rustc_macros") (v "484.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1kcb9fz34jhf1gk5zb3x6v4zga1scjbmxw4gj6qsi00s9yr14xxf")))

(define-public crate-rustc-ap-rustc_macros-485.0.0 (c (n "rustc-ap-rustc_macros") (v "485.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1n5vzxhmzhm13nzylid2v762i3kgcyx6an5m0vjqymaxqh783nhv")))

(define-public crate-rustc-ap-rustc_macros-486.0.0 (c (n "rustc-ap-rustc_macros") (v "486.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1rrhl1mndmdp95wivkyz2bcqhk7i10z78cp127av26nk7s3zb1fd")))

(define-public crate-rustc-ap-rustc_macros-487.0.0 (c (n "rustc-ap-rustc_macros") (v "487.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1sq2d5g55r2712lfxfx90d425657h429q9vj2ps6zcqwc5h45ldp")))

(define-public crate-rustc-ap-rustc_macros-488.0.0 (c (n "rustc-ap-rustc_macros") (v "488.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "043mzswayzi5s508sjqr1qn0kjazf2la5fxmk9zjf84k8dc15xwa")))

(define-public crate-rustc-ap-rustc_macros-489.0.0 (c (n "rustc-ap-rustc_macros") (v "489.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "10641jhfqqx7nh3ypahc16dr9gppz63r0vnxqc9gvz1rd4xxhy1s")))

(define-public crate-rustc-ap-rustc_macros-490.0.0 (c (n "rustc-ap-rustc_macros") (v "490.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1wz4748f1fqr0nvsbji7j1y15gaxpkxcgmia63y43rc20npxjq8q")))

(define-public crate-rustc-ap-rustc_macros-491.0.0 (c (n "rustc-ap-rustc_macros") (v "491.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "068s1xd4rq1pn3qpi7n4sv6rs12xgzrmc8wpsk8pkiyjcbbj0507")))

(define-public crate-rustc-ap-rustc_macros-492.0.0 (c (n "rustc-ap-rustc_macros") (v "492.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1jwgb383mgw9cq98yhyi1a7d4np9d3dld8div5krfshmjnn110ax")))

(define-public crate-rustc-ap-rustc_macros-493.0.0 (c (n "rustc-ap-rustc_macros") (v "493.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "01a05p2jlzqf9qjxhp3jbnvhy7im018v5nbdw1wjasky4p86hzis")))

(define-public crate-rustc-ap-rustc_macros-494.0.0 (c (n "rustc-ap-rustc_macros") (v "494.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0chh5z1x7pmp8imxb6lsnhn1sk9cr2qpha6vppdfvycv7d577acp")))

(define-public crate-rustc-ap-rustc_macros-495.0.0 (c (n "rustc-ap-rustc_macros") (v "495.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0xmyr2z6sfd6dfixkqs0prx6hdmvll2xxx9r2p285i25yd9zd2ks")))

(define-public crate-rustc-ap-rustc_macros-496.0.0 (c (n "rustc-ap-rustc_macros") (v "496.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0kn6nm6115hdparbi0m3k1lbafqhzcbaabmgl4dd815jjhnylxaw")))

(define-public crate-rustc-ap-rustc_macros-497.0.0 (c (n "rustc-ap-rustc_macros") (v "497.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "09b60amgx6w0ldrq2lbjycpbxkz65xrdnh4q7p8qbgk66dfc6vvv")))

(define-public crate-rustc-ap-rustc_macros-498.0.0 (c (n "rustc-ap-rustc_macros") (v "498.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0gqksdv8nxywp9xrw7zwwxnfzp5679pfqbzmqc0k6hvc8yjxdjlv")))

(define-public crate-rustc-ap-rustc_macros-499.0.0 (c (n "rustc-ap-rustc_macros") (v "499.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "18qvaxcjxg4wj2majfs3cxp8bmfypbq5khps4b0i2jk35qidn4kg")))

(define-public crate-rustc-ap-rustc_macros-500.0.0 (c (n "rustc-ap-rustc_macros") (v "500.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "17ii40kms1i42wylsz041cp8mk20qh0rgkikkbrxhkll2vh4307y")))

(define-public crate-rustc-ap-rustc_macros-501.0.0 (c (n "rustc-ap-rustc_macros") (v "501.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0jadzzikr43na2w9zjd07lsfsbjkibf7ggkp8dwnam01hy33pbnq")))

(define-public crate-rustc-ap-rustc_macros-502.0.0 (c (n "rustc-ap-rustc_macros") (v "502.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0xwslkf7cjrkgw0dgyknvdy0n7c5qwqyicy3w26l2rjafwa2clq8")))

(define-public crate-rustc-ap-rustc_macros-503.0.0 (c (n "rustc-ap-rustc_macros") (v "503.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1s6dad4iv6j551s4clnz71w4nkascfc5an94wilxnwrz24syz5ni")))

(define-public crate-rustc-ap-rustc_macros-504.0.0 (c (n "rustc-ap-rustc_macros") (v "504.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1hshr1lml9j7gfkz1mg4dskr48vz2nr92dm6gx93ig8rjmc9511l")))

(define-public crate-rustc-ap-rustc_macros-505.0.0 (c (n "rustc-ap-rustc_macros") (v "505.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0ia47n7hh2bfm22xzpsryvafv9hjmvvngjq8dkxlisi3f4igp80v")))

(define-public crate-rustc-ap-rustc_macros-506.0.0 (c (n "rustc-ap-rustc_macros") (v "506.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "01n4i24c7yqv9fykabilcnimghf66m6rcppp92ax1q72ymzyv2i1")))

(define-public crate-rustc-ap-rustc_macros-507.0.0 (c (n "rustc-ap-rustc_macros") (v "507.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0qvfiib99psm2m5i6qk3630g1xmp45fmqc0j4322gq93pcihi35q")))

(define-public crate-rustc-ap-rustc_macros-508.0.0 (c (n "rustc-ap-rustc_macros") (v "508.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0gv5qsaha1drkbf9jhlbmhfjypqyddkiq1wai7qw2nj9ilz63kgp")))

(define-public crate-rustc-ap-rustc_macros-509.0.0 (c (n "rustc-ap-rustc_macros") (v "509.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "14hwk1rd0s0sy3rv7nhgg3f644yrh5h6b53kc96lwbczmbmgpv0c")))

(define-public crate-rustc-ap-rustc_macros-510.0.0 (c (n "rustc-ap-rustc_macros") (v "510.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "11c003hfzbrqhxbnr535ihg3fp4dh6p7n9jjnz5r47lljnc3m4ql")))

(define-public crate-rustc-ap-rustc_macros-511.0.0 (c (n "rustc-ap-rustc_macros") (v "511.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0kpshq9r85z1gd46vhz7yx4g7zinbqwh40hssffw0kzw2r282ry7")))

(define-public crate-rustc-ap-rustc_macros-512.0.0 (c (n "rustc-ap-rustc_macros") (v "512.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1qjz2gd3bm6gcjmms3b6vjqdv6v7iw0w3z7qaq4gdzfbwc4kyb5p")))

(define-public crate-rustc-ap-rustc_macros-513.0.0 (c (n "rustc-ap-rustc_macros") (v "513.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0qmpc1f848cd0y96ch76sqbyqpil2kx5mxd33niav86j33whzn5q")))

(define-public crate-rustc-ap-rustc_macros-514.0.0 (c (n "rustc-ap-rustc_macros") (v "514.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0y82z34sr99x08v5bqshg1f4lmz1r428iymgcd0milxpbdbbf7sd")))

(define-public crate-rustc-ap-rustc_macros-515.0.0 (c (n "rustc-ap-rustc_macros") (v "515.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0hzyzk7pqvwysfbmfs3ks89k03vc18d6n0bfd6rck81084hrs9hp")))

(define-public crate-rustc-ap-rustc_macros-516.0.0 (c (n "rustc-ap-rustc_macros") (v "516.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0rdcs36nh2xvzhpmb9fvaxngjwik62bbgylhydk7f0vgrsl0fj09")))

(define-public crate-rustc-ap-rustc_macros-517.0.0 (c (n "rustc-ap-rustc_macros") (v "517.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0s4ca2dr2pxmwx78ancwyjjbz9k56vmxpawh36w9pr7bap983hsg")))

(define-public crate-rustc-ap-rustc_macros-518.0.0 (c (n "rustc-ap-rustc_macros") (v "518.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "15kxjw9dznbmal42mm6ylidayyq16f65br9f5ry7p69c69cy6ys5")))

(define-public crate-rustc-ap-rustc_macros-519.0.0 (c (n "rustc-ap-rustc_macros") (v "519.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "18067dr9jz7b30784xvq8qyvij9dkj1w9byn2qk7nlmm4zvf8zsd")))

(define-public crate-rustc-ap-rustc_macros-520.0.0 (c (n "rustc-ap-rustc_macros") (v "520.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "16b4xhk29m8k8cg6s9v0jwd1hf0wi5v17mknpricrg89y21yf8wd")))

(define-public crate-rustc-ap-rustc_macros-521.0.0 (c (n "rustc-ap-rustc_macros") (v "521.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1x453ra2ydh0w1r8ji4xx063kyr6d3pyvkj5g3dar87aq7yyy0rb")))

(define-public crate-rustc-ap-rustc_macros-522.0.0 (c (n "rustc-ap-rustc_macros") (v "522.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "04ra2rnlc1jz88g1l6yfv0j95hj7l35nd4llx5q7va49la8h9j6v")))

(define-public crate-rustc-ap-rustc_macros-523.0.0 (c (n "rustc-ap-rustc_macros") (v "523.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "10fgphn8bdgm0lw35h4953bnpiws0yf7m7hp3f6wvaq64lp333wm")))

(define-public crate-rustc-ap-rustc_macros-524.0.0 (c (n "rustc-ap-rustc_macros") (v "524.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "16bj33g34nyv347b35g749rzm6rf3aryn4ghj0akwzlqbn3d1p9a")))

(define-public crate-rustc-ap-rustc_macros-525.0.0 (c (n "rustc-ap-rustc_macros") (v "525.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0ij2axg3a52lil8b4q887inpnj4irh2fkdl4d47qk6li5w3irwip")))

(define-public crate-rustc-ap-rustc_macros-526.0.0 (c (n "rustc-ap-rustc_macros") (v "526.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1pzwsnrdc8432ay76ma7czdrnl7kf6iqn07cacsf4mbivq5h9ybd")))

(define-public crate-rustc-ap-rustc_macros-527.0.0 (c (n "rustc-ap-rustc_macros") (v "527.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0wqs52z3q8czldamsdirybqkqc6x0zfllx6lyrn4jnlkqhz31hbx")))

(define-public crate-rustc-ap-rustc_macros-528.0.0 (c (n "rustc-ap-rustc_macros") (v "528.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1nixy3517gg2slqcja1jpz0na76gg8yg3lmd56mjhlwsi3vhwsxx")))

(define-public crate-rustc-ap-rustc_macros-529.0.0 (c (n "rustc-ap-rustc_macros") (v "529.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1c6s3gpgq0zmy1c5m29cz1w5y9gaiqk929lcdqr718zi1vnv2nvx")))

(define-public crate-rustc-ap-rustc_macros-530.0.0 (c (n "rustc-ap-rustc_macros") (v "530.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1n7bhx6dzasvi3zi5w6xmc7s444cmr4qfp28i8kyrfvs7ckiyhsa")))

(define-public crate-rustc-ap-rustc_macros-531.0.0 (c (n "rustc-ap-rustc_macros") (v "531.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1l2y0dvcl2n1axg6dz5iinb66yvbn6rc6wdk0pb3c73343sdlzgw")))

(define-public crate-rustc-ap-rustc_macros-532.0.0 (c (n "rustc-ap-rustc_macros") (v "532.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0sqynzxgv1zhbncpx4w41d5iam39n08iqb1zsqc0lljxfim9dj0n")))

(define-public crate-rustc-ap-rustc_macros-533.0.0 (c (n "rustc-ap-rustc_macros") (v "533.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0szpz7prklcsnpdk4rk1358w8l7b2yflbqrbaiy5wk946liq3cyz")))

(define-public crate-rustc-ap-rustc_macros-534.0.0 (c (n "rustc-ap-rustc_macros") (v "534.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "10mi2sm98g97f57ssl22apcx7yxm813pwb8r49h8ypfnypddjayc")))

(define-public crate-rustc-ap-rustc_macros-535.0.0 (c (n "rustc-ap-rustc_macros") (v "535.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "16a9m18s3b2292v49kwss2cbx013rmk1xchy24q7g48mim1dl177")))

(define-public crate-rustc-ap-rustc_macros-536.0.0 (c (n "rustc-ap-rustc_macros") (v "536.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0lvza8f6i6j9wy9m1zafy330aabs94hw4yk6mxd6aq90s6w5ncbb")))

(define-public crate-rustc-ap-rustc_macros-537.0.0 (c (n "rustc-ap-rustc_macros") (v "537.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "19dxd20vby0w0f407yscin8wr244x8l1x3ldi43lqkva31hdhm90")))

(define-public crate-rustc-ap-rustc_macros-538.0.0 (c (n "rustc-ap-rustc_macros") (v "538.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0xnwpc1bikwanv1gvjjrmd8fwgbm5sqlj2y9dxaclyqm8jz6jdrd")))

(define-public crate-rustc-ap-rustc_macros-539.0.0 (c (n "rustc-ap-rustc_macros") (v "539.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1nvjarxrb9kl3zbxz6k1zp0cv93xa14c3m36vb659dv03dw14fky")))

(define-public crate-rustc-ap-rustc_macros-540.0.0 (c (n "rustc-ap-rustc_macros") (v "540.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0g4m2fy4bsxq98dwdak60mrs6y0iscd94jb911vs8k6yq7db4ipz")))

(define-public crate-rustc-ap-rustc_macros-541.0.0 (c (n "rustc-ap-rustc_macros") (v "541.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0y1yapgzria62cr4s574kgv6haxk1c05v93gf0n0hyzcb1zs99hr")))

(define-public crate-rustc-ap-rustc_macros-542.0.0 (c (n "rustc-ap-rustc_macros") (v "542.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "12ck0vpc18i9zgcjg8g0cqa9mwcjhwgyj0z1i56ckhsgvp4xrjnk")))

(define-public crate-rustc-ap-rustc_macros-543.0.0 (c (n "rustc-ap-rustc_macros") (v "543.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0w1243sf2iwq8lgn36m9g9c9axczpmggj3bvk75ii23nql8bmwg8")))

(define-public crate-rustc-ap-rustc_macros-544.0.0 (c (n "rustc-ap-rustc_macros") (v "544.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1bqpjx4j270zfavh6x7idykkf3x0priq5swx6d0kl3d7f02fhpjc")))

(define-public crate-rustc-ap-rustc_macros-545.0.0 (c (n "rustc-ap-rustc_macros") (v "545.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0fqpfhi880rs515pm4d1bvlxrycjjanlpywjf1lfpcjfvhx239qi")))

(define-public crate-rustc-ap-rustc_macros-546.0.0 (c (n "rustc-ap-rustc_macros") (v "546.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "103ybf83z218b2sqkbikhp6dj45f9044vd4d8rgq7s3g6gikf05r")))

(define-public crate-rustc-ap-rustc_macros-547.0.0 (c (n "rustc-ap-rustc_macros") (v "547.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "01is558pf8vhxqp6v5qg4rlzi4gy2b1w9mrmpqik08g490srf3iw")))

(define-public crate-rustc-ap-rustc_macros-548.0.0 (c (n "rustc-ap-rustc_macros") (v "548.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1x603qv5rxc51lqnh5fk6jamn2n2h8crq2fhsv1ywn79kdlchvdi")))

(define-public crate-rustc-ap-rustc_macros-549.0.0 (c (n "rustc-ap-rustc_macros") (v "549.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0xsab6hzd2qjar4wcs0zvdva4vfw895lfznbq15z2mbh0wc1k0yl")))

(define-public crate-rustc-ap-rustc_macros-550.0.0 (c (n "rustc-ap-rustc_macros") (v "550.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "08mjb75gqn72v1bw2jcx8blmhyjvwmg95y3qbgx6q5dxbhn373pk")))

(define-public crate-rustc-ap-rustc_macros-551.0.0 (c (n "rustc-ap-rustc_macros") (v "551.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "054x7lidpkg6am5wga39xp8xxgfldi1jic7yphpwchb32js2ckyw")))

(define-public crate-rustc-ap-rustc_macros-552.0.0 (c (n "rustc-ap-rustc_macros") (v "552.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1pfd71zsd5xvc912lygf8a12wzh43ka4iw1hd413g3wr7mi9kbmc")))

(define-public crate-rustc-ap-rustc_macros-553.0.0 (c (n "rustc-ap-rustc_macros") (v "553.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "09y4jl29qmgv4vy8m9nqivnag3m6hais3aiwjkkqq89vfr1cmhxb")))

(define-public crate-rustc-ap-rustc_macros-554.0.0 (c (n "rustc-ap-rustc_macros") (v "554.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "105a3hzc7lkgm02w7ahhj5vgqvsz5hdy8mgrcvmqj3wdfpaqwdhx")))

(define-public crate-rustc-ap-rustc_macros-555.0.0 (c (n "rustc-ap-rustc_macros") (v "555.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1cxhd7830big3nikxaawb1nds625ndnn4j04mnmchij6nkw8yfcd")))

(define-public crate-rustc-ap-rustc_macros-556.0.0 (c (n "rustc-ap-rustc_macros") (v "556.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0v1blm551d9a3z9rm5jfvjwmi897pb8rx20jx013367wyrhcl5i2")))

(define-public crate-rustc-ap-rustc_macros-557.0.0 (c (n "rustc-ap-rustc_macros") (v "557.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1yprma0s6dz8zv8h7g7a5558yx7wgacjpwpkpw8jlzwydsyinvmj")))

(define-public crate-rustc-ap-rustc_macros-558.0.0 (c (n "rustc-ap-rustc_macros") (v "558.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "12gjz4g9hh0hc86p4qgw0s22mvkrl7vflgphpr5ffygjpypsj4qy")))

(define-public crate-rustc-ap-rustc_macros-559.0.0 (c (n "rustc-ap-rustc_macros") (v "559.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "15mb2snj2whb21mksx0c4v7q17pwmy2s8w7lz6hbjvnbzywlwj2r")))

(define-public crate-rustc-ap-rustc_macros-560.0.0 (c (n "rustc-ap-rustc_macros") (v "560.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1wi94rmm197sr9x6k6jfkxx06d54jzc8wzmx8yyfr7vpj6i22dsp")))

(define-public crate-rustc-ap-rustc_macros-561.0.0 (c (n "rustc-ap-rustc_macros") (v "561.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "03qyrz539kayj0kpk0k4wag5pzjakbq6c9m9xh8w858byd45mjj4")))

(define-public crate-rustc-ap-rustc_macros-562.0.0 (c (n "rustc-ap-rustc_macros") (v "562.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0qpmd3bdplwzp028wb96czkikvqc17hvssplac71fgyr0nj7bflm")))

(define-public crate-rustc-ap-rustc_macros-563.0.0 (c (n "rustc-ap-rustc_macros") (v "563.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "19x7dxi9xdhbl3mxrg2q9dvx45l6xyl4kx35abh9jv2l0096g2gx")))

(define-public crate-rustc-ap-rustc_macros-564.0.0 (c (n "rustc-ap-rustc_macros") (v "564.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "138pcs44lnclhsbcinwipl04wjpxwwddjvsayjwrx720j6iv4akp")))

(define-public crate-rustc-ap-rustc_macros-565.0.0 (c (n "rustc-ap-rustc_macros") (v "565.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "03phda7qn0m05nbzxk7k71by1l651k7xf0ag963sadq8k0s8wk55")))

(define-public crate-rustc-ap-rustc_macros-566.0.0 (c (n "rustc-ap-rustc_macros") (v "566.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1p1d9va49v80xqdnp0nmylcq1ibw7rmrhxmjh85iwi3n7pglc6ln")))

(define-public crate-rustc-ap-rustc_macros-567.0.0 (c (n "rustc-ap-rustc_macros") (v "567.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0y6aqbnr2mwx9z9s3dn5idcx66i8j9ad5j9p5kj2kk9niszwrphm")))

(define-public crate-rustc-ap-rustc_macros-568.0.0 (c (n "rustc-ap-rustc_macros") (v "568.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1d9ips18agvmlmra59hd85x52cz8y26aa8x8hilc1hwavk0pci8y")))

(define-public crate-rustc-ap-rustc_macros-569.0.0 (c (n "rustc-ap-rustc_macros") (v "569.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0v1bgvf3pxwwch6c14w9fxih9f6m4qbvgy4qmayhz00yj8s30wma")))

(define-public crate-rustc-ap-rustc_macros-570.0.0 (c (n "rustc-ap-rustc_macros") (v "570.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "09di4wlm3xaznkrc8cv8k40ylicdw80b1j37mzys4qxkb9cylqig")))

(define-public crate-rustc-ap-rustc_macros-571.0.0 (c (n "rustc-ap-rustc_macros") (v "571.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0nl5zp2n04casz1y19jl04l4mfvds4a2xd6zczhcp4vwk491w19b")))

(define-public crate-rustc-ap-rustc_macros-572.0.0 (c (n "rustc-ap-rustc_macros") (v "572.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1acv1sn8m3r4gr07wx5xnq1cw3yvn9k3wvkpik9f0ky47aa86b5d")))

(define-public crate-rustc-ap-rustc_macros-573.0.0 (c (n "rustc-ap-rustc_macros") (v "573.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0p3f8bn1d2rq7kydphqq1fy03vf2jxyaajjkd904g1svbr1k0hxg")))

(define-public crate-rustc-ap-rustc_macros-574.0.0 (c (n "rustc-ap-rustc_macros") (v "574.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1l8csq92cb5r43y3l4lvgq2c8kqlrvkkzk7d2xr2l0096gh1jmqg")))

(define-public crate-rustc-ap-rustc_macros-575.0.0 (c (n "rustc-ap-rustc_macros") (v "575.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "03qp8jarkripwrlv1xk2n9rypffj46zwjypfifxqvxxrzizlv098")))

(define-public crate-rustc-ap-rustc_macros-576.0.0 (c (n "rustc-ap-rustc_macros") (v "576.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "07lcjvjrpsqd1y8ysfj22grsxhf6d6cgslf9m7hyl39m3aa84iqh")))

(define-public crate-rustc-ap-rustc_macros-577.0.0 (c (n "rustc-ap-rustc_macros") (v "577.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0nrjx99z8wvv0pfy4wm6wgpzw8jl00l7hyrx5immcvk7xd05avnq")))

(define-public crate-rustc-ap-rustc_macros-578.0.0 (c (n "rustc-ap-rustc_macros") (v "578.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0qzp39r6b6ydfhpj1gm2dx4dy0pb25p4xkqww2vcpmk98ry9533f")))

(define-public crate-rustc-ap-rustc_macros-579.0.0 (c (n "rustc-ap-rustc_macros") (v "579.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "07zvgx6dysc8vln08a91gvh6b9cclas3gn7iixgcjqrvnd3qvawm")))

(define-public crate-rustc-ap-rustc_macros-580.0.0 (c (n "rustc-ap-rustc_macros") (v "580.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0nr747pl4yrj821xfvsrxm30qjl9jb9z26fwv6nq85pqa1hx3j30")))

(define-public crate-rustc-ap-rustc_macros-581.0.0 (c (n "rustc-ap-rustc_macros") (v "581.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "12894ghakgl9b96gjsm3nkz8xg6ph3ph06cycnybgvvhwwgr7c49")))

(define-public crate-rustc-ap-rustc_macros-582.0.0 (c (n "rustc-ap-rustc_macros") (v "582.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0dp55m0hyg4xmf6nvww40r8rpq8hhrd6g4252ghlmz4hhybvanlw")))

(define-public crate-rustc-ap-rustc_macros-583.0.0 (c (n "rustc-ap-rustc_macros") (v "583.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0vs89yqng49ncv09a3hxg8hf0788203axlybz6bl96y5ximx7rg2")))

(define-public crate-rustc-ap-rustc_macros-584.0.0 (c (n "rustc-ap-rustc_macros") (v "584.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "06j4zm35lh4mjhcrahgncsa0nkwp727qsnal8k5sfvivp7b6rw6z")))

(define-public crate-rustc-ap-rustc_macros-585.0.0 (c (n "rustc-ap-rustc_macros") (v "585.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0p1jj12m3ixk4dapdqbj08sardy70badjcngwax06bzyv29zas9l")))

(define-public crate-rustc-ap-rustc_macros-586.0.0 (c (n "rustc-ap-rustc_macros") (v "586.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1ppp87q1z0jpyil5v8j7j2sljfcw3zrha9682yv6c3sx0yp4dvc2")))

(define-public crate-rustc-ap-rustc_macros-587.0.0 (c (n "rustc-ap-rustc_macros") (v "587.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1fj96pkkyi9rbyj13krgjz8431nx579fh9vnz7yh7yl2mjam8xhh")))

(define-public crate-rustc-ap-rustc_macros-588.0.0 (c (n "rustc-ap-rustc_macros") (v "588.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "15dhvmkgll6mbpzqvp3mplh3lig1k6w95zr4j7l1w1jn75nnc4q6")))

(define-public crate-rustc-ap-rustc_macros-589.0.0 (c (n "rustc-ap-rustc_macros") (v "589.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "04dpmm2xpn9i7xh4qpkga1w560vz6g2f8j2niqwjnpsn2j1hv128")))

(define-public crate-rustc-ap-rustc_macros-590.0.0 (c (n "rustc-ap-rustc_macros") (v "590.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "01r5hz6zrg4vf0lxrnx4184494nfm56z633ilmh12zq94piwb61q")))

(define-public crate-rustc-ap-rustc_macros-591.0.0 (c (n "rustc-ap-rustc_macros") (v "591.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0glc87qpr4i16l7l8kr6mrsaz19hcxnhgnlhlzjqxcda5gkgsidx")))

(define-public crate-rustc-ap-rustc_macros-592.0.0 (c (n "rustc-ap-rustc_macros") (v "592.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1ymqxylazwf9bvlj47r94ffbn2digmpy5y368yyy2n9i9ahlq9wv")))

(define-public crate-rustc-ap-rustc_macros-593.0.0 (c (n "rustc-ap-rustc_macros") (v "593.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "03jv2hkf31lc5aivyjr9n5lg0gwr6q0l3xrqw982pfyyjbdnqgjn")))

(define-public crate-rustc-ap-rustc_macros-594.0.0 (c (n "rustc-ap-rustc_macros") (v "594.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "17la501svi0ryridcsxqxln6sjw1lcra297fbhrrks89rqs6w7ac")))

(define-public crate-rustc-ap-rustc_macros-595.0.0 (c (n "rustc-ap-rustc_macros") (v "595.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0s04dqj45mm87m9mppsdz8ljyhg1d1x0pycc38q30a23bmym40ad")))

(define-public crate-rustc-ap-rustc_macros-596.0.0 (c (n "rustc-ap-rustc_macros") (v "596.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "16ibhqw7x3qm82dnl72bs5g1xhxk9x1jd0f26hshmhk5klw6rknx")))

(define-public crate-rustc-ap-rustc_macros-597.0.0 (c (n "rustc-ap-rustc_macros") (v "597.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1hx9ymm42mz57hkhih98h8405gymxag75i3n5zrgkd7y86gblivv")))

(define-public crate-rustc-ap-rustc_macros-598.0.0 (c (n "rustc-ap-rustc_macros") (v "598.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1r57rczwy879i96r6fbfh35dld3qcc78z23xi34r50iyg6brcmf6")))

(define-public crate-rustc-ap-rustc_macros-599.0.0 (c (n "rustc-ap-rustc_macros") (v "599.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0girnf9pk33h5adz0pc6pvj9wgrblrlc3bc7z4bzgr6bg77fcayw")))

(define-public crate-rustc-ap-rustc_macros-600.0.0 (c (n "rustc-ap-rustc_macros") (v "600.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0v883yzswdhiknqshdiaic12aynbcj0hj4xp4zh69slyyklj9av8")))

(define-public crate-rustc-ap-rustc_macros-601.0.0 (c (n "rustc-ap-rustc_macros") (v "601.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0qdg8gyk890nsx52mqs7a789g1017284g7dzwlycpj3xzbcpcyrw")))

(define-public crate-rustc-ap-rustc_macros-602.0.0 (c (n "rustc-ap-rustc_macros") (v "602.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1n37kwcv3w54vbhfjijz2lgm6z9482ag8cl13cbvwb91kbb7xsri")))

(define-public crate-rustc-ap-rustc_macros-603.0.0 (c (n "rustc-ap-rustc_macros") (v "603.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "0vaaa7qw6kiwmg8974kf4rs49g6xafidzmss8h6vnhja6gc1ypli")))

(define-public crate-rustc-ap-rustc_macros-604.0.0 (c (n "rustc-ap-rustc_macros") (v "604.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1cv6wj7fl82rl1rxqh942r191bw28s41b4a643y393qmmsm09266")))

(define-public crate-rustc-ap-rustc_macros-605.0.0 (c (n "rustc-ap-rustc_macros") (v "605.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "10hkrxb5kpvfzpzj58i8n0aj2vm9hqpifl2y9wvnqwcdbgsxmfg5")))

(define-public crate-rustc-ap-rustc_macros-606.0.0 (c (n "rustc-ap-rustc_macros") (v "606.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "1l7mw4zxcdks9h81xhq7wdjrr222sz4wvjyyhp2qhllw2m37xmxj")))

(define-public crate-rustc-ap-rustc_macros-607.0.0 (c (n "rustc-ap-rustc_macros") (v "607.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0pfgl5yj49j1y4vjmpi5mbdyn8632rv87jfk0hfqrr9ln32f29zd")))

(define-public crate-rustc-ap-rustc_macros-608.0.0 (c (n "rustc-ap-rustc_macros") (v "608.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0py34m6kcv4xb5h7ixamjzdvn75xmfny7fxq8qbagrrfvmn10wib")))

(define-public crate-rustc-ap-rustc_macros-609.0.0 (c (n "rustc-ap-rustc_macros") (v "609.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0km94zcri4kbzfg0vscgnr97glxppp92sw5xmrxw9d1y1lwsribx")))

(define-public crate-rustc-ap-rustc_macros-610.0.0 (c (n "rustc-ap-rustc_macros") (v "610.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1bvmbgidj1l6hksxwma66rrzc70cfkhh3wjrbphfjxs8pvl9b5zr")))

(define-public crate-rustc-ap-rustc_macros-611.0.0 (c (n "rustc-ap-rustc_macros") (v "611.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "08abnj805rrwglgx04fsvchfvn92790nhcwbgza0k87sygmscy2s")))

(define-public crate-rustc-ap-rustc_macros-612.0.0 (c (n "rustc-ap-rustc_macros") (v "612.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0m4ayqlsfs6h71g2b2acbr8z4byqakyfdb9s4b1liyiw2r4jg9ix")))

(define-public crate-rustc-ap-rustc_macros-613.0.0 (c (n "rustc-ap-rustc_macros") (v "613.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "009wjp0svlkcsr3rjpq0hl48q50r5qadain9f643iflbsh66ki32")))

(define-public crate-rustc-ap-rustc_macros-614.0.0 (c (n "rustc-ap-rustc_macros") (v "614.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "18b6bidsdwfwnaan6ajgia0ky6hbgqspjkb4jfmkqbj0wam200i4")))

(define-public crate-rustc-ap-rustc_macros-615.0.0 (c (n "rustc-ap-rustc_macros") (v "615.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1brrzkjasaafix4gh6dkpgkkd0jf1i2xjhgcdfljnzvpsv36bgz5")))

(define-public crate-rustc-ap-rustc_macros-616.0.0 (c (n "rustc-ap-rustc_macros") (v "616.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "110c2dbiw91x8im09y987ir4iaclgrr2ifa7vv3wzdwx2n0mc3xm")))

(define-public crate-rustc-ap-rustc_macros-617.0.0 (c (n "rustc-ap-rustc_macros") (v "617.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1xhi021r5436qwqvmmwznwl8cmsf359g1qj71g4nashkhgiib6is")))

(define-public crate-rustc-ap-rustc_macros-618.0.0 (c (n "rustc-ap-rustc_macros") (v "618.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "181pf95dzgzml55csfp07bmrqs6gvvl47zbxac4k3dfm8w7ks5lh")))

(define-public crate-rustc-ap-rustc_macros-619.0.0 (c (n "rustc-ap-rustc_macros") (v "619.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0gqagkvmdsi9gnl7hlqdpwid0bkk4y2jacp9nn8wp2iiai1ngn9c")))

(define-public crate-rustc-ap-rustc_macros-620.0.0 (c (n "rustc-ap-rustc_macros") (v "620.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1hhb7xpps0sxzf8py9sf6vspailsi6l58jzavmwcnd64r6qdswvr")))

(define-public crate-rustc-ap-rustc_macros-621.0.0 (c (n "rustc-ap-rustc_macros") (v "621.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0c4yqwrk5ncg92ahgrpzac0dqdc9npyzhqsjarhsjvljd8na05sb")))

(define-public crate-rustc-ap-rustc_macros-622.0.0 (c (n "rustc-ap-rustc_macros") (v "622.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1rbx3ywq9vhhjchnqbv7rb25y193ygi51q6l38m8ri12izc75cnz")))

(define-public crate-rustc-ap-rustc_macros-623.0.0 (c (n "rustc-ap-rustc_macros") (v "623.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0ngnwfzbsvnnqqq2llahb1qmaa2d8n0bd6gd5y4c3y9db97jg6vs")))

(define-public crate-rustc-ap-rustc_macros-624.0.0 (c (n "rustc-ap-rustc_macros") (v "624.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0dpyf743gh0whf6zlr625bz8hbd9sklyhzb2vsb7ljcdik9xb40w")))

(define-public crate-rustc-ap-rustc_macros-625.0.0 (c (n "rustc-ap-rustc_macros") (v "625.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0q4ss169qib0ic6mbyl2xb30llmgmhr90svyn82r0xh54s9cdxi7")))

(define-public crate-rustc-ap-rustc_macros-626.0.0 (c (n "rustc-ap-rustc_macros") (v "626.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1wnydwchgadwxbcrhfj9k5gb9dkdq21gvjdba9g5aww4cvymznsv")))

(define-public crate-rustc-ap-rustc_macros-627.0.0 (c (n "rustc-ap-rustc_macros") (v "627.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "03fpaz5fmz3m1za3dxqfv5cyndga8qvyxnjjr2ivpl2yhjb583wb")))

(define-public crate-rustc-ap-rustc_macros-628.0.0 (c (n "rustc-ap-rustc_macros") (v "628.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "01y1xd5hwaz2aj74z073436x7wsq4k3dvxm5i09cf3h0qxcgkqrl")))

(define-public crate-rustc-ap-rustc_macros-629.0.0 (c (n "rustc-ap-rustc_macros") (v "629.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0hhaiidjlpil038z9irwjpnmm0zcb5g5lqam844v9qj0cixrg8bj")))

(define-public crate-rustc-ap-rustc_macros-630.0.0 (c (n "rustc-ap-rustc_macros") (v "630.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "14qsw4dn9i5jj7q2av0vcljfscf555wa6yw6p5jl7s2zz0prc37b")))

(define-public crate-rustc-ap-rustc_macros-631.0.0 (c (n "rustc-ap-rustc_macros") (v "631.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0nsgrzbgcpkwjx36g9vafyd2mg44i6kib62s7jxip14s4c0byl8g")))

(define-public crate-rustc-ap-rustc_macros-632.0.0 (c (n "rustc-ap-rustc_macros") (v "632.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "14q76m74q2h8lj6chhnllldd51xz3yaqsi06chsalcscaqwrwzvp")))

(define-public crate-rustc-ap-rustc_macros-633.0.0 (c (n "rustc-ap-rustc_macros") (v "633.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0031aywhcnrd850alilskj6g580znhgx8s9sb640m7b83i36jzyd")))

(define-public crate-rustc-ap-rustc_macros-634.0.0 (c (n "rustc-ap-rustc_macros") (v "634.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "04qhd5njww0z78d9a02az5cmn8yhbjbw3gfwxqcgzgr123q6n5xd")))

(define-public crate-rustc-ap-rustc_macros-635.0.0 (c (n "rustc-ap-rustc_macros") (v "635.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0z8bvr2z0g90vx2id55ck22xz2llhmrjav3gsr6v32qj0l3hd77h")))

(define-public crate-rustc-ap-rustc_macros-636.0.0 (c (n "rustc-ap-rustc_macros") (v "636.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1jp8hs0fp17lr5l5nwks0ffc8pbx4mfnyahfn11zvp6029nwf8s2")))

(define-public crate-rustc-ap-rustc_macros-637.0.0 (c (n "rustc-ap-rustc_macros") (v "637.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "13xwcr82z0gbh0z2anhgmkq2sdvafmyjwvgbzmj7pn08qd97d5w2")))

(define-public crate-rustc-ap-rustc_macros-638.0.0 (c (n "rustc-ap-rustc_macros") (v "638.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "15qbnimzknq8df642an0kixijzghx4f260r0fz3a5wbvss2m4d51")))

(define-public crate-rustc-ap-rustc_macros-639.0.0 (c (n "rustc-ap-rustc_macros") (v "639.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "12i1g1bhs7xnrkcqxzjy9iwfm61ig2fz2bsmgp8vlg7bvys0gcgh")))

(define-public crate-rustc-ap-rustc_macros-640.0.0 (c (n "rustc-ap-rustc_macros") (v "640.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0q79gz6xkynjr4nss7399ca6wshdnklb9894ywf99hy34siqca9v")))

(define-public crate-rustc-ap-rustc_macros-641.0.0 (c (n "rustc-ap-rustc_macros") (v "641.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1a9ksv0x5p9q1v87ax6zprh8k6imprvc5g2zfm07gc6z4n4w7a45")))

(define-public crate-rustc-ap-rustc_macros-642.0.0 (c (n "rustc-ap-rustc_macros") (v "642.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "05mfzdjzykc63mmanj9lhay6gizlkf529jakv37hx4aix947pi3m")))

(define-public crate-rustc-ap-rustc_macros-643.0.0 (c (n "rustc-ap-rustc_macros") (v "643.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1zahjv5b6sgsbpwc5d4npvj7yn31d0k065nkpzc3x2f1v36kqxqq")))

(define-public crate-rustc-ap-rustc_macros-644.0.0 (c (n "rustc-ap-rustc_macros") (v "644.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "04d0v4mqmcj70j2zxwjvwsj12jl0ifmd28y9dxhcbampfh3djnnz")))

(define-public crate-rustc-ap-rustc_macros-645.0.0 (c (n "rustc-ap-rustc_macros") (v "645.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0ia3ajgx8xnanj69riwcf4lny0imp2bx7h9lxllk85vry7l4l5d3")))

(define-public crate-rustc-ap-rustc_macros-646.0.0 (c (n "rustc-ap-rustc_macros") (v "646.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "01gf8bpxbcp1jv3j7cb3gdknwjh26hdlmlcx3qb482xqfj9lz20c")))

(define-public crate-rustc-ap-rustc_macros-647.0.0 (c (n "rustc-ap-rustc_macros") (v "647.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0x1c11c93rzf9d56q6ixsarhnqvngw2ac125kh3nydvccyf62gk2")))

(define-public crate-rustc-ap-rustc_macros-648.0.0 (c (n "rustc-ap-rustc_macros") (v "648.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0wb2i0v5w43mxcgby578k3jwqz6dj954rrh6vdsrksan61l1fbmx")))

(define-public crate-rustc-ap-rustc_macros-649.0.0 (c (n "rustc-ap-rustc_macros") (v "649.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1x508zs45h7mjs4b7lnv0ngv4jkcl9ik0sx7whnwr3mv4sfbgnma")))

(define-public crate-rustc-ap-rustc_macros-650.0.0 (c (n "rustc-ap-rustc_macros") (v "650.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1h422ih9v7klsb43zv04s2zx5qnk2h4ig1w6pmc6nwavnqipy51f")))

(define-public crate-rustc-ap-rustc_macros-651.0.0 (c (n "rustc-ap-rustc_macros") (v "651.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "18flyb2dvm3nxgb21392ah3xqsz03ri5s2ml7v9aa4j3hvnba9ij")))

(define-public crate-rustc-ap-rustc_macros-652.0.0 (c (n "rustc-ap-rustc_macros") (v "652.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0f3kx8pyswlsjf7v5zmq5jf7zdavja7b28n2j2zmybimc3ipd36b")))

(define-public crate-rustc-ap-rustc_macros-653.0.0 (c (n "rustc-ap-rustc_macros") (v "653.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "08wafzfg3q108sbbkkqg396xm19w6wy99q50hlf589gg9ak76m1l")))

(define-public crate-rustc-ap-rustc_macros-654.0.0 (c (n "rustc-ap-rustc_macros") (v "654.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "03zfp8a10jz43z8lsx1drx7g5jimxmbw4w7hs13yvczismb6qs2r")))

(define-public crate-rustc-ap-rustc_macros-655.0.0 (c (n "rustc-ap-rustc_macros") (v "655.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "10yc36r81hf8718m27zy75y6v3dhrqk67vgj0li92114pqsc3nlm")))

(define-public crate-rustc-ap-rustc_macros-656.0.0 (c (n "rustc-ap-rustc_macros") (v "656.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0khxl5vwangs5srqmpvmc7xmldlhz1mq1rh0902761qk9ai6rwvk")))

(define-public crate-rustc-ap-rustc_macros-657.0.0 (c (n "rustc-ap-rustc_macros") (v "657.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0z972y7y02szksyqpla442cfwvdf1f5js73yww1yf0lrkdl3bkfq")))

(define-public crate-rustc-ap-rustc_macros-658.0.0 (c (n "rustc-ap-rustc_macros") (v "658.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1cdg6ssz5gcrrss7s468qq244x6pdmz0354yv2bjhjvgrxs7xcrp")))

(define-public crate-rustc-ap-rustc_macros-659.0.0 (c (n "rustc-ap-rustc_macros") (v "659.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1d816qr8dbjvxb5xkc5sg269qsxx1rzg76fd1cp7sdl9lqal241x")))

(define-public crate-rustc-ap-rustc_macros-660.0.0 (c (n "rustc-ap-rustc_macros") (v "660.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1q7figp40i3jlcixgvsx1yc6g59hnzfhilf9i1ak6zmnp05sxywi")))

(define-public crate-rustc-ap-rustc_macros-661.0.0 (c (n "rustc-ap-rustc_macros") (v "661.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1bfpgk6laf4wmn2n4j3b5q3675mbbqcrdzdafy5dklgc2akqnd2c")))

(define-public crate-rustc-ap-rustc_macros-662.0.0 (c (n "rustc-ap-rustc_macros") (v "662.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "14zvlg8hvmlf7w9z9hyi22g3i4ygr3hrva8fq1ip3d9f8ifs9d13")))

(define-public crate-rustc-ap-rustc_macros-663.0.0 (c (n "rustc-ap-rustc_macros") (v "663.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0bfckv9svw8nvks7kyk0jm9qj3gs1am0705sagizkika515lr318")))

(define-public crate-rustc-ap-rustc_macros-664.0.0 (c (n "rustc-ap-rustc_macros") (v "664.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "00al5shh5q6v7wqc4gwpn6kyjrijpkh0wrhq84b028wx9v8avhag")))

(define-public crate-rustc-ap-rustc_macros-665.0.0 (c (n "rustc-ap-rustc_macros") (v "665.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0ikv6ymnivz0wlwvis6amszvn72wsf225bcrmp1wnd7f3jhwwvlk")))

(define-public crate-rustc-ap-rustc_macros-666.0.0 (c (n "rustc-ap-rustc_macros") (v "666.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0l6f6n9zkps62v92kzgany6ih8v5ivq5vwf2jd8s9i25j98rahqw")))

(define-public crate-rustc-ap-rustc_macros-667.0.0 (c (n "rustc-ap-rustc_macros") (v "667.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1pnd4y23rks4rcjz92vmmdvkyrmxba8i5yn900f3ldibjxs6v4p3")))

(define-public crate-rustc-ap-rustc_macros-668.0.0 (c (n "rustc-ap-rustc_macros") (v "668.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "13sm7zbi4s98gn52pxjl5h9lvy5g440s2l5i1spf3x76hhm5cs47")))

(define-public crate-rustc-ap-rustc_macros-669.0.0 (c (n "rustc-ap-rustc_macros") (v "669.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0k1vpc38kwx5bm0lzq1zp5k68qlbk69bhim3b5vh84zkjb8srxk4")))

(define-public crate-rustc-ap-rustc_macros-670.0.0 (c (n "rustc-ap-rustc_macros") (v "670.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0hvds71gcq5ll4m0mf1zk2bs80rd5681v2yxzipr458j3gpc02nf")))

(define-public crate-rustc-ap-rustc_macros-671.0.0 (c (n "rustc-ap-rustc_macros") (v "671.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1jw7chf30cn83as82m3aks5gck7l16r3qc1sbdai7xafdkr4sxwb")))

(define-public crate-rustc-ap-rustc_macros-672.0.0 (c (n "rustc-ap-rustc_macros") (v "672.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0j96a3bhsni2yqllpmh4hvmdq5p3gb4inaimn0yyljlafmyqi3rb")))

(define-public crate-rustc-ap-rustc_macros-673.0.0 (c (n "rustc-ap-rustc_macros") (v "673.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1daxgj58famjdv7baasf11lhc1pqml6awiqw2z0qh0pxvram2k33")))

(define-public crate-rustc-ap-rustc_macros-674.0.0 (c (n "rustc-ap-rustc_macros") (v "674.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0dgnk7wwifmwzxjc7mp9mkfc3vir3ndq9myyfiaqw5hld820532m")))

(define-public crate-rustc-ap-rustc_macros-675.0.0 (c (n "rustc-ap-rustc_macros") (v "675.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1g81yjli6pvcsmqhc0pn0q2327mn1ywfk9k39wgjcf8gbl4k1wdr")))

(define-public crate-rustc-ap-rustc_macros-676.0.0 (c (n "rustc-ap-rustc_macros") (v "676.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "01bjc7irzvyqk7hm0px3xfkxainywr9p6vzphvpd9rqxf2w2ybwk")))

(define-public crate-rustc-ap-rustc_macros-677.0.0 (c (n "rustc-ap-rustc_macros") (v "677.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1sjz08hj7d5kinn9sqjd4yaxzsv2xx5253lsmjkxvascwj189z5k")))

(define-public crate-rustc-ap-rustc_macros-678.0.0 (c (n "rustc-ap-rustc_macros") (v "678.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1m0skbccsaf0ppgv9vw0vss9pc3rkl0s6iv3ll5ciwm4irij1xdh")))

(define-public crate-rustc-ap-rustc_macros-679.0.0 (c (n "rustc-ap-rustc_macros") (v "679.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0z6w3n1m623dx647bl6v6fnjyz5s9kaz9bb48ws7a7bfamkf7zsf")))

(define-public crate-rustc-ap-rustc_macros-680.0.0 (c (n "rustc-ap-rustc_macros") (v "680.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0xnkfdii3kg7rlid8w48p66545gij87qy6gk15hl2jrj0f7500kn")))

(define-public crate-rustc-ap-rustc_macros-681.0.0 (c (n "rustc-ap-rustc_macros") (v "681.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "19v740ljhpybav9id8yfrggf7cjha2zahjl51gcg0ml51ca40yci")))

(define-public crate-rustc-ap-rustc_macros-682.0.0 (c (n "rustc-ap-rustc_macros") (v "682.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "119886nhycm05fbsdgzwdiw7k242saw730wwjmyz6hm7fmjqljbv")))

(define-public crate-rustc-ap-rustc_macros-683.0.0 (c (n "rustc-ap-rustc_macros") (v "683.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1zzhxxd84wmyjffd4l8dknwfnf8x4cb1cadiqj0g725ip8flrj9k")))

(define-public crate-rustc-ap-rustc_macros-684.0.0 (c (n "rustc-ap-rustc_macros") (v "684.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0z7mfib6rlf2hch94y97kp0bwgscggp0azx55dn2kd8qyqrw3cqw")))

(define-public crate-rustc-ap-rustc_macros-685.0.0 (c (n "rustc-ap-rustc_macros") (v "685.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0nc2xc84a8k6m4irvshj4xyprcmikdj0ssagmidsyvfzvibvdnfr")))

(define-public crate-rustc-ap-rustc_macros-686.0.0 (c (n "rustc-ap-rustc_macros") (v "686.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1j7rd4r1ib3v9h3rykbfbymysx1nklj0s1b57k6rli2k71i6vv31")))

(define-public crate-rustc-ap-rustc_macros-687.0.0 (c (n "rustc-ap-rustc_macros") (v "687.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0i2gg6cikbbpkp8f4rdj68169785vnglm5skgi3yajbbnjdqg13i")))

(define-public crate-rustc-ap-rustc_macros-688.0.0 (c (n "rustc-ap-rustc_macros") (v "688.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "155vxmrgdlhjcfqs7dgj5bqpnmhxsvs0pz1lyv7hh31vn1x636gl")))

(define-public crate-rustc-ap-rustc_macros-689.0.0 (c (n "rustc-ap-rustc_macros") (v "689.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0l9b8kr7rf2pvq46wvv3k0kxwp8n15fcid7q93jlgfvqjk5xkndm")))

(define-public crate-rustc-ap-rustc_macros-690.0.0 (c (n "rustc-ap-rustc_macros") (v "690.0.0") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r ">=0.12.1, <0.13.0") (d #t) (k 0)))) (h "02ivspvs5ri9zc3gqjmgnwwdy5y6h059vgrwnr4qpijlkvfshads")))

(define-public crate-rustc-ap-rustc_macros-691.0.0 (c (n "rustc-ap-rustc_macros") (v "691.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1f56nnrq9w5jqxhrh630zbdfsw43q252vsanwfa78695jsy5yad3")))

(define-public crate-rustc-ap-rustc_macros-692.0.0 (c (n "rustc-ap-rustc_macros") (v "692.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0l5x4qp3hapcqjyr6fcpikcrnrk37q9f42l70jpl59s3cjaci5wz")))

(define-public crate-rustc-ap-rustc_macros-693.0.0 (c (n "rustc-ap-rustc_macros") (v "693.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "12h81ra1vwz0ahdvx210x764p0h03miqdx6s16hpmsg9jmg73pld")))

(define-public crate-rustc-ap-rustc_macros-694.0.0 (c (n "rustc-ap-rustc_macros") (v "694.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0y39237a3cl1ihs7gxikvlmp1wc9g1cw6lhyzgb8hs15y0s3lyfv")))

(define-public crate-rustc-ap-rustc_macros-695.0.0 (c (n "rustc-ap-rustc_macros") (v "695.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "03kg2mwdsjw6qcl0mcrxbm6pqclyv516qnvcn6x3fas8hh0mnfq9")))

(define-public crate-rustc-ap-rustc_macros-696.0.0 (c (n "rustc-ap-rustc_macros") (v "696.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0iv0qjwak088wfki5nj4q81ksiqinn0jqxg84615i5y90367amb8")))

(define-public crate-rustc-ap-rustc_macros-697.0.0 (c (n "rustc-ap-rustc_macros") (v "697.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "10bnvib0bjrps3q93b79aqild8db1vrv0lvxzndzs3vmcq5v2m64")))

(define-public crate-rustc-ap-rustc_macros-698.0.0 (c (n "rustc-ap-rustc_macros") (v "698.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0ws1nv7si3gv1vyybf1ch81fbrbad526iw0n4ag9ygs37a5f4qln")))

(define-public crate-rustc-ap-rustc_macros-699.0.0 (c (n "rustc-ap-rustc_macros") (v "699.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1x0qy8bpzhnlfk9kxxkql9mc6fn5i6dlp1kc5sp8gfjjbqhpnip2")))

(define-public crate-rustc-ap-rustc_macros-700.0.0 (c (n "rustc-ap-rustc_macros") (v "700.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "070v03iyi2mrlf8n0109zrwbdlr76yr7m7s6l5xz9p0qclzb4z09")))

(define-public crate-rustc-ap-rustc_macros-701.0.0 (c (n "rustc-ap-rustc_macros") (v "701.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1s187axlf28s0jcmklp93h1aqh9pa2wjbs2yxszcgbgdwk8d60zz")))

(define-public crate-rustc-ap-rustc_macros-702.0.0 (c (n "rustc-ap-rustc_macros") (v "702.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0za2qckcbd5hg8y6dmpqlnqliwr0x0bci7cnplbh8z37q2fi65hs")))

(define-public crate-rustc-ap-rustc_macros-703.0.0 (c (n "rustc-ap-rustc_macros") (v "703.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "04ywlfzkbyd654gc455iyck2hlv2zhxpxvlmg03n9nzx04fa9fjw")))

(define-public crate-rustc-ap-rustc_macros-704.0.0 (c (n "rustc-ap-rustc_macros") (v "704.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1kllnzlrdh50454d5k06vx8v1m55544x04yfsrnccvah5z6j65xj")))

(define-public crate-rustc-ap-rustc_macros-705.0.0 (c (n "rustc-ap-rustc_macros") (v "705.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "15p3nkf2x5hkagbpl38awqjyqzp7wb0y3l4csmba5xgn3d0dfgpy")))

(define-public crate-rustc-ap-rustc_macros-706.0.0 (c (n "rustc-ap-rustc_macros") (v "706.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "15rq4n6xpfj4i058m4qsb5xcq1ya9g8liz8firmr1kcgdnmhdq9k")))

(define-public crate-rustc-ap-rustc_macros-707.0.0 (c (n "rustc-ap-rustc_macros") (v "707.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0pqabjqsgqxblkbfds71y4a7i5ns2sz0wllfh8lhsmjjk4n73ks1")))

(define-public crate-rustc-ap-rustc_macros-708.0.0 (c (n "rustc-ap-rustc_macros") (v "708.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1nm9fw39f8pmc1v65i91v67ddb07jg8gg923hc6vxswhkrkam3i8")))

(define-public crate-rustc-ap-rustc_macros-709.0.0 (c (n "rustc-ap-rustc_macros") (v "709.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "07vdk9nf9sgafgdijz9x9bsf3ijnag0v7nf4y0116sq52pr398jv")))

(define-public crate-rustc-ap-rustc_macros-710.0.0 (c (n "rustc-ap-rustc_macros") (v "710.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0vbnsw64rm0gf9s514v7v90mcpd59x60417kq7zdkpbfbw1m5qhl")))

(define-public crate-rustc-ap-rustc_macros-711.0.0 (c (n "rustc-ap-rustc_macros") (v "711.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1ldh0c86hdmh6p67yls1a186lnb64bxhbwx9fj2wvwmfncd8mh48")))

(define-public crate-rustc-ap-rustc_macros-712.0.0 (c (n "rustc-ap-rustc_macros") (v "712.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0ifajsx0hllpmj4c1vijmjan5z8hflx4pjk90d6swfxfkj3xc1s6")))

(define-public crate-rustc-ap-rustc_macros-713.0.0 (c (n "rustc-ap-rustc_macros") (v "713.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "09hi035n52anwqnml8kbgs3f26i76mpdc2nlflbdcjk9ni4g1whc")))

(define-public crate-rustc-ap-rustc_macros-714.0.0 (c (n "rustc-ap-rustc_macros") (v "714.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0mqpx3griy2c3jvnihkdhw3h8j9mdqyg1wni2ql6a2n1akmvg4w2")))

(define-public crate-rustc-ap-rustc_macros-715.0.0 (c (n "rustc-ap-rustc_macros") (v "715.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0774mw2fqwia7w9zg49hbk1b5yr8120jswjq8p4sbv62f90yibv5")))

(define-public crate-rustc-ap-rustc_macros-716.0.0 (c (n "rustc-ap-rustc_macros") (v "716.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0587shcf3cc3mdar3gd3i6kzgn3hjvsjdvgk690z0z3bc678xdhv")))

(define-public crate-rustc-ap-rustc_macros-717.0.0 (c (n "rustc-ap-rustc_macros") (v "717.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "126hf327l56ii90i2vf3d53ix2jirzybq1i2ilhbv6d4v4z67bwr")))

(define-public crate-rustc-ap-rustc_macros-718.0.0 (c (n "rustc-ap-rustc_macros") (v "718.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "17466mlvz3mq1za8cf0ssdkwlyadcqh2l9lalvgjz7kbdp99qy17")))

(define-public crate-rustc-ap-rustc_macros-719.0.0 (c (n "rustc-ap-rustc_macros") (v "719.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1g7kr4kdraz2dwx0r34s35hjd6h6gglz7gi6by9bvy9wjw84vmxc")))

(define-public crate-rustc-ap-rustc_macros-720.0.0 (c (n "rustc-ap-rustc_macros") (v "720.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0s8qk2yss7lbyirkhxa2j2yvjj67qkrrvw26vxn3c4p9pam52llc")))

(define-public crate-rustc-ap-rustc_macros-721.0.0 (c (n "rustc-ap-rustc_macros") (v "721.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1qs2yk8hrdlzbxg9j0rfd413nxycsm0cxhllglbg1sxrnrrm6nfd")))

(define-public crate-rustc-ap-rustc_macros-722.0.0 (c (n "rustc-ap-rustc_macros") (v "722.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0446qmd1jw66x9p37g1zjkrrbg3y42sx2ycn9njca5bzydyqggc6")))

(define-public crate-rustc-ap-rustc_macros-723.0.0 (c (n "rustc-ap-rustc_macros") (v "723.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "19gbdg6x7jcl1brx55dyc66cwi9ak89kda0sabs9hzn0hh7q7050")))

(define-public crate-rustc-ap-rustc_macros-724.0.0 (c (n "rustc-ap-rustc_macros") (v "724.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1d3z1cs92j5qri4mad52spsrckmr9f0hnxyn9c16sixbcfka7rlz")))

(define-public crate-rustc-ap-rustc_macros-725.0.0 (c (n "rustc-ap-rustc_macros") (v "725.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "06v9xk49hik2f6nhckkj5xv20gxi17x7j28v7ibh678zfqjbv2cz")))

(define-public crate-rustc-ap-rustc_macros-726.0.0 (c (n "rustc-ap-rustc_macros") (v "726.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "0giypif3asy1c9csccxk81lipvc3jvqhpw1kr5nniiakzh96g0v4")))

(define-public crate-rustc-ap-rustc_macros-727.0.0 (c (n "rustc-ap-rustc_macros") (v "727.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.1") (d #t) (k 0)))) (h "1hqpagwvwsqwjcycr16sgxw7rkmalmk0jljsjjq0jndl4xlcv8ba")))

