(define-module (crates-io ru st rustygit) #:use-module (crates-io))

(define-public crate-rustygit-0.1.0 (c (n "rustygit") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h9rawqvl738jcpxjf6hxlhhqjwzjsyirdggd8gf7r1g7c6bb70w")))

(define-public crate-rustygit-0.1.1 (c (n "rustygit") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a30lpgcrlnh609v4p039vfk8swj2n85wlp39n4zl1yh2vs2z1jc")))

(define-public crate-rustygit-0.2.0 (c (n "rustygit") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1byp11a8r075q8d15nn9dklajcjsg9w4l9aripm4bkyw20jra9hr")))

(define-public crate-rustygit-0.2.1 (c (n "rustygit") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p5r4ycd4g8c6shawj2sggig6pm83xiqwwdng9rymrywkbdzk29j")))

(define-public crate-rustygit-0.2.2 (c (n "rustygit") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "147ndc96gdjfxpkspifg4alkdnw65px1vfs6a6iy1b6fr36iqkfs")))

(define-public crate-rustygit-0.2.3 (c (n "rustygit") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kh6fgbdx9yigs75cfpc2q28lic9aipj34hbajcfinby2h9dpv2v")))

(define-public crate-rustygit-0.2.4 (c (n "rustygit") (v "0.2.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b9z52vwg31pyn83flsqfpri6477qxq35807a4sk4fj2w9ccwvwz")))

(define-public crate-rustygit-0.2.5 (c (n "rustygit") (v "0.2.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04yvi48pn8p4qjwknn9ynaqjkz94qwf5ix000np5szfdf3r3qc3l")))

(define-public crate-rustygit-0.2.6 (c (n "rustygit") (v "0.2.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b7qc5yqdllrc919q35jikgb4f8yda0r3hyvzjnn73bd4zgcfbv0")))

(define-public crate-rustygit-0.2.7 (c (n "rustygit") (v "0.2.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rb0ymsd0dkv7g1a4ncclwm3kv8wbrxf4a0ydc0c87f1lhlsk5v6")))

(define-public crate-rustygit-0.3.0 (c (n "rustygit") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mbdrdq5775n5l1gcg5ybv5gjxb652hdq8jsdw54kb5q5r1799s1")))

(define-public crate-rustygit-0.4.0 (c (n "rustygit") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15k7wlics906122x553cm61zmpdl2ilr0b7i6rngqb5sv5vh6gj0")))

(define-public crate-rustygit-0.4.1 (c (n "rustygit") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "118v078qvydsc96ylr5gailvia7gympqpcly5r531nd9km9mbi4f")))

(define-public crate-rustygit-0.4.2 (c (n "rustygit") (v "0.4.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jsz60hfwxxnng2mm39d6ks6cqpf4fy1l5hc7dnd3y6vddzgvljf")))

(define-public crate-rustygit-0.4.3 (c (n "rustygit") (v "0.4.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ajjl3596ssalnc7k6agdch2n165n7zl4ifxh149lyvdc40ccwz6")))

(define-public crate-rustygit-0.4.4 (c (n "rustygit") (v "0.4.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "167b5b90w92c56pkhf8w3cshgif2zsvlclpb27y9lypkw116qcqp")))

(define-public crate-rustygit-0.5.0 (c (n "rustygit") (v "0.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ch8vq6pan1s9cd2kzm52hbs5l6czkyzh7giag20fyqwvmdpa9x6")))

