(define-module (crates-io ru st rustc-tools) #:use-module (crates-io))

(define-public crate-rustc-tools-0.1.0 (c (n "rustc-tools") (v "0.1.0") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "1kwh46dlyjng4lg8b8w7xs5qq5a39357jd04nmpmifbmnk710j46")))

(define-public crate-rustc-tools-0.1.1 (c (n "rustc-tools") (v "0.1.1") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "1sav2q0b0rr3sksj5scwb5vhdsdmjgnzwvn3m2dskrgqgxx7rqq0")))

(define-public crate-rustc-tools-0.1.2 (c (n "rustc-tools") (v "0.1.2") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "11akhs5b506yxncvdsjgfkzxdf5ih0s1ji6ya3a5jalb3qm324iz")))

(define-public crate-rustc-tools-0.2.0 (c (n "rustc-tools") (v "0.2.0") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "1h9vmnsv2r47p9mzw6lc00jzab3jw0gvpbihqjlms5k0wn8zydda")))

(define-public crate-rustc-tools-0.3.0 (c (n "rustc-tools") (v "0.3.0") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "0k7md16hyg3gdjvv17lny74zrdzsx790akbafvd3bcsfckl1n5ac")))

(define-public crate-rustc-tools-0.2.1 (c (n "rustc-tools") (v "0.2.1") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "0s5hx7sjvif6alinzif5q5ixigqy2x2w6v6dpq9gplfgr7v16s3m")))

(define-public crate-rustc-tools-0.3.1 (c (n "rustc-tools") (v "0.3.1") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "0cl1sgmkxlvraqp8hwk3m84j8hzz7b8kk1k9qk7xjm3zirxw5xb4")))

(define-public crate-rustc-tools-0.3.2 (c (n "rustc-tools") (v "0.3.2") (d (list (d (n "cargo_metadata") (r "^0.18") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "0s83l1izg8129q1vmk4qlrbn46k56li607xqfc1i0m3kyivlka9j")))

(define-public crate-rustc-tools-0.3.3 (c (n "rustc-tools") (v "0.3.3") (d (list (d (n "cargo_metadata") (r "^0.18") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "11z28n2ax9ym40bsdl0kbvzlf2jm0wakmy2wwph0dikd04lxdqvx")))

