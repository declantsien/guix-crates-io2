(define-module (crates-io ru st rust_interop_derive) #:use-module (crates-io))

(define-public crate-rust_interop_derive-0.0.1 (c (n "rust_interop_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05knjzz95z5qsw94ybf4ip71q3qsyq22za76ql1l9f4nypd2j0hp") (y #t)))

(define-public crate-rust_interop_derive-0.0.2 (c (n "rust_interop_derive") (v "0.0.2") (h "1fxpzm77w53r608d88h8rdrvbqq0sk2gykymsfyzm24qi5fv87h0") (y #t)))

