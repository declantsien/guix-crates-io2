(define-module (crates-io ru st rustfm) #:use-module (crates-io))

(define-public crate-rustfm-0.0.2 (c (n "rustfm") (v "0.0.2") (d (list (d (n "hyper") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17apzbgd79ahcmp2m43sg4bwsj1nx2ivivbnxf4gq3w3kj01ibbb")))

(define-public crate-rustfm-0.0.3 (c (n "rustfm") (v "0.0.3") (d (list (d (n "hyper") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1z6pwwq0gyzxbs536ha49q90w3f5aqi8c07jpm5ycjxd0lyzmcby")))

(define-public crate-rustfm-0.1.0 (c (n "rustfm") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "09b8mhdfkydhxhzsl18yi9rjf0bv3ilv6dn5h0q4kz0iyz93i6c7")))

(define-public crate-rustfm-0.1.1 (c (n "rustfm") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1nbmvpkszkjkyz4jqmxavwggzskfkk39iwqg5npxpfcs2wlxz0ss")))

(define-public crate-rustfm-0.1.2 (c (n "rustfm") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1fzg0h2wl4kgzn57kk2miqrqzr3iw6rmx05s5pyzw08w7w27daz2")))

