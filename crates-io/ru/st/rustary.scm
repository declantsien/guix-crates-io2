(define-module (crates-io ru st rustary) #:use-module (crates-io))

(define-public crate-rustary-0.1.0 (c (n "rustary") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("strum_macros"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "10djz401wr5j218s35dmss2gv3k2rmk2fm0xidsjppf1ay11zlgl")))

(define-public crate-rustary-0.1.1 (c (n "rustary") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("strum_macros"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)))) (h "13jan5xi92i4v1g3m8sc08115bv9mzw7pw655mjg8z2c456cj4zp")))

(define-public crate-rustary-0.1.2 (c (n "rustary") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("strum_macros"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "0dh0ld71wdrgk2f50ak7qknixqlw75l8m7y6r2ysydspgyp7gpdz")))

(define-public crate-rustary-0.1.3 (c (n "rustary") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("strum_macros"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "11zlzxfcanw5br6l3gsyc5mgykbhns9ymw5v40hn4nxam923dx58")))

