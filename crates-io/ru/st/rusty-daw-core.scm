(define-module (crates-io ru st rusty-daw-core) #:use-module (crates-io))

(define-public crate-rusty-daw-core-0.1.0 (c (n "rusty-daw-core") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rmvpdqyzsv1rwr3nwnfgsl7y51mahn9n6yz74hiqk6417gc0hhw")))

(define-public crate-rusty-daw-core-0.1.1 (c (n "rusty-daw-core") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jvc6k7k0v19vjc4rs3hbg5imy3swd4j5lmd13gpljs2fmphyq5h")))

(define-public crate-rusty-daw-core-0.1.2 (c (n "rusty-daw-core") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0c362glymxz89gyrvn67sg6anmc252pwg6qma8ab9ilvjxh41pjw")))

(define-public crate-rusty-daw-core-0.1.3 (c (n "rusty-daw-core") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "111jwxba3580rvbzgh8bialawq5vr9b0qrs24nmaznma7x96pa2h")))

(define-public crate-rusty-daw-core-0.2.0 (c (n "rusty-daw-core") (v "0.2.0") (h "0xf2rnlxcj0z450jk5d7w0r0f5fgsp8kilx0gir17igpncn28pg8")))

(define-public crate-rusty-daw-core-0.2.1 (c (n "rusty-daw-core") (v "0.2.1") (h "0k6q04r4qsvj0aixp3w5rw5jg1hvdlp91dvwwxvlgangixl4x8yk")))

(define-public crate-rusty-daw-core-0.2.2 (c (n "rusty-daw-core") (v "0.2.2") (h "1335dkc71933k1f5vygaxjinv6084nlapf5m2cwmwki7d6m7jl08")))

(define-public crate-rusty-daw-core-0.2.3 (c (n "rusty-daw-core") (v "0.2.3") (h "07yakl5nfrz6qvfsliyxfd2iqqhlr180hcrcvr14sp7cq7y224mc")))

(define-public crate-rusty-daw-core-0.3.0 (c (n "rusty-daw-core") (v "0.3.0") (h "0h6f7fcb232ppcv8xrxx6yx9bbyjw7rimb62jpkx1h4v75n1z763")))

(define-public crate-rusty-daw-core-0.3.1 (c (n "rusty-daw-core") (v "0.3.1") (h "0yqyl538rk62r6k62ilzyxs71zmcyr48sjrjkmxi6h3wmc03yc0v")))

(define-public crate-rusty-daw-core-0.3.2 (c (n "rusty-daw-core") (v "0.3.2") (h "1p6ccfvgjx8fb30c603igcd0n9hc1057ykivq3q6iykpkr7m60hb")))

(define-public crate-rusty-daw-core-0.4.0 (c (n "rusty-daw-core") (v "0.4.0") (h "18bf4jjg2nrc6gpa718s8cv92smfqrff1ns091ysqqlzd14asv5f")))

(define-public crate-rusty-daw-core-0.5.0 (c (n "rusty-daw-core") (v "0.5.0") (h "1b2wcwsp1ywmad4kn7arm78i6vbqca77r5q4iha6cxq3wm5cv0il")))

(define-public crate-rusty-daw-core-0.5.1 (c (n "rusty-daw-core") (v "0.5.1") (h "0slpf5wsmvk2kfqrv3bq2yj79pihy5jkp1725gr5pw6vp4ilfvph")))

(define-public crate-rusty-daw-core-0.6.0 (c (n "rusty-daw-core") (v "0.6.0") (h "1vsfwai5bl0p6c445b4iji21ywhwwim5i4yvjifisdbd1cbys1wc")))

(define-public crate-rusty-daw-core-0.7.0 (c (n "rusty-daw-core") (v "0.7.0") (h "065x5p3ly4jpanx8zwb5sv2ck9ja7vbh8v26l6gcs25378y5zp0y")))

(define-public crate-rusty-daw-core-0.7.1 (c (n "rusty-daw-core") (v "0.7.1") (h "16wylm9gjp2jqbhygi64c0r2kxlpwjv11w0hxyvyyksb2sss06g5")))

(define-public crate-rusty-daw-core-0.7.2 (c (n "rusty-daw-core") (v "0.7.2") (h "09yppv13390nv3nkmi7yi38z43f08dg2dw7zwydysay5fr9qcm9d")))

(define-public crate-rusty-daw-core-0.7.3 (c (n "rusty-daw-core") (v "0.7.3") (h "18967s078b9f6x9lmwid200w2mz2l3p5xzyaklgkgxxn6h2y30xg")))

(define-public crate-rusty-daw-core-0.7.4 (c (n "rusty-daw-core") (v "0.7.4") (h "117ing39az2i9n6h7pa3ida2fv68m2i87y5m0g0gavx00ansz5z5")))

(define-public crate-rusty-daw-core-0.8.0 (c (n "rusty-daw-core") (v "0.8.0") (h "1sa4fihy0z6a9160pamqr35calszcf5apwxs612gqc3bafmps4ck")))

(define-public crate-rusty-daw-core-0.8.1 (c (n "rusty-daw-core") (v "0.8.1") (h "0lk3m5m223373bi0qmnpj0zyxlq13w0681nkpw4fxm4m7bzwvh63")))

(define-public crate-rusty-daw-core-0.9.0 (c (n "rusty-daw-core") (v "0.9.0") (h "17v2a2y6hm1ww42mcsav3gs351vdczn7rgcnjq3s12sinl6n4cyx")))

