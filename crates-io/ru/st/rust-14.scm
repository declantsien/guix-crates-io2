(define-module (crates-io ru st rust-14) #:use-module (crates-io))

(define-public crate-rust-14-0.1.0 (c (n "rust-14") (v "0.1.0") (h "0f6ckiqjab03sfz3axsq5d4vkhs6l5v66mwb2s0fil8sid8cyvc1")))

(define-public crate-rust-14-0.2.0 (c (n "rust-14") (v "0.2.0") (h "1ajrcrp734ivbq4nd5y2frfqw3m3bxipr0ksgw5a2z5cc10vfaxh")))

