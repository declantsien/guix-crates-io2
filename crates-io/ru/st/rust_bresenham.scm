(define-module (crates-io ru st rust_bresenham) #:use-module (crates-io))

(define-public crate-rust_bresenham-0.1.0 (c (n "rust_bresenham") (v "0.1.0") (h "1n7mjxqhw8qa05wxkk4kd5knbifv9w0rrc51v8j36y0w6fkgwdg1") (y #t)))

(define-public crate-rust_bresenham-0.1.1 (c (n "rust_bresenham") (v "0.1.1") (h "1ks30sphjfszp5kpmszp069nb931k4p8mbq4g52f7shs4pzbna4w") (y #t)))

(define-public crate-rust_bresenham-0.1.2 (c (n "rust_bresenham") (v "0.1.2") (h "0z6kpdid6j7r93dp5z5phlacasg9d5dxiksslznmv73igsgjj8s6") (y #t)))

(define-public crate-rust_bresenham-0.1.3 (c (n "rust_bresenham") (v "0.1.3") (h "1cyhqrz0cr5phc4y39alvq2gghzlkfgyga2d9jybygc5fwjgfxd6") (y #t)))

(define-public crate-rust_bresenham-0.1.4 (c (n "rust_bresenham") (v "0.1.4") (h "0hyl7nymnz62sldkz82hapcskzkm576kw481r16b7ivpd44mmq3h") (y #t)))

(define-public crate-rust_bresenham-0.1.5 (c (n "rust_bresenham") (v "0.1.5") (h "0ia55rad69rjww6al33k7fzq0i59h9400ylbwqbm0ynxkjngky1c") (y #t)))

(define-public crate-rust_bresenham-0.1.6 (c (n "rust_bresenham") (v "0.1.6") (h "04yli07kmgs0a54ssww2wmphrvz3x8m7vbdy3aqd7avf9aibscqw") (y #t)))

(define-public crate-rust_bresenham-0.1.7 (c (n "rust_bresenham") (v "0.1.7") (h "1md2am47nbzr94mvxb6vy6093al3ar1az379kg09w8w4iplq4dr3") (y #t)))

(define-public crate-rust_bresenham-0.1.8 (c (n "rust_bresenham") (v "0.1.8") (h "0r8may2wzm9rzxdv1w8g6lz0q65j8jinl1rkfww2iyz56rx8n9j5")))

(define-public crate-rust_bresenham-0.0.0 (c (n "rust_bresenham") (v "0.0.0") (h "098i358drf427hxg2lq9lmi2g98wk1can5q17773lk4hxh8jrmbg") (y #t)))

