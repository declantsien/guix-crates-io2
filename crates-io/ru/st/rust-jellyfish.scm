(define-module (crates-io ru st rust-jellyfish) #:use-module (crates-io))

(define-public crate-rust-jellyfish-0.1.0 (c (n "rust-jellyfish") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "0l2dp9kfpc0h9ziwpglyd2hxpllwybcnqym4073nw425lx7iqh45") (y #t)))

