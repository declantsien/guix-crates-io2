(define-module (crates-io ru st rust-gh-example) #:use-module (crates-io))

(define-public crate-rust-gh-example-0.1.3 (c (n "rust-gh-example") (v "0.1.3") (h "1vmnq95ns12ydqbcql2b08xva8jjwbrgkk4ayh5xvhixvpz93q2w")))

(define-public crate-rust-gh-example-0.1.4 (c (n "rust-gh-example") (v "0.1.4") (h "0n8k7b6iw225rj7vlzajsd83bd7wy2ns5725zawjg3xyiri8inhf")))

(define-public crate-rust-gh-example-0.1.5 (c (n "rust-gh-example") (v "0.1.5") (h "0jqi04dkdahg08gdfw3mcyvjnj6nssrwii8fk4zr5qd4p1cgh5lx")))

(define-public crate-rust-gh-example-0.1.7 (c (n "rust-gh-example") (v "0.1.7") (h "06r2jc08n7iyrz1qrxmd1pgcvbmkqf66i9z7df779vaay4p1lial")))

