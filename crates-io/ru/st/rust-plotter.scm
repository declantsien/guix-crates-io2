(define-module (crates-io ru st rust-plotter) #:use-module (crates-io))

(define-public crate-rust-plotter-0.1.0 (c (n "rust-plotter") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde-pickle") (r "^0.5.0") (d #t) (k 0)))) (h "01lf2zwx0rlhd0c8wvqsysj81yzn03d9y9dvv9vk4lmnnnpm4a17")))

(define-public crate-rust-plotter-0.1.1 (c (n "rust-plotter") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde-pickle") (r "^0.5.0") (d #t) (k 0)))) (h "1hj5graag7ggd91kcljhklksr67rlzid3i8i9fdqfa1r9179wj0f")))

(define-public crate-rust-plotter-0.1.2 (c (n "rust-plotter") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde-pickle") (r "^0.5.0") (d #t) (k 0)))) (h "11sjj4zpvzlasb3rdy4psybl3h7jw3yzkzxnjabxwapf59shrmx7")))

