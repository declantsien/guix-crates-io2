(define-module (crates-io ru st rustkey) #:use-module (crates-io))

(define-public crate-rustkey-0.1.0 (c (n "rustkey") (v "0.1.0") (h "1zlnsmls43l2m43d6048xf8ymm076np2y4xxx4s6bcv0d26d99g7")))

(define-public crate-rustkey-0.2.0 (c (n "rustkey") (v "0.2.0") (h "09v5j1ybx9bv7hkx32ypzy4ykzvswki8rypms3kx6v971dfnv7hn")))

(define-public crate-rustkey-0.3.0 (c (n "rustkey") (v "0.3.0") (h "0kyhj3b00vda8xpg92778r37l1c2g22c1jd5kkysg13z4kb740y6")))

(define-public crate-rustkey-0.3.1 (c (n "rustkey") (v "0.3.1") (h "0a1avqqjcwrfhrga974gs4hjz3msdgj8sq1bhh5mpifn3xpqhkas")))

(define-public crate-rustkey-0.4.0 (c (n "rustkey") (v "0.4.0") (h "14yqny4fy4mz2qkx4zmhxsrvi5ra6y41lslwyaclsvgp40jlza7j")))

