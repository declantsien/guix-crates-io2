(define-module (crates-io ru st rust-add-lib) #:use-module (crates-io))

(define-public crate-rust-add-lib-0.1.0 (c (n "rust-add-lib") (v "0.1.0") (h "1c2c8waimlpijclnrnjxibxxa6by3g1dxp10gjzqi4jydl7a443i")))

(define-public crate-rust-add-lib-0.1.1 (c (n "rust-add-lib") (v "0.1.1") (h "0h7askgfv18b46dqp9lcqb5dz1myp2aasa5vcw55ii8bf4gsvf5h")))

