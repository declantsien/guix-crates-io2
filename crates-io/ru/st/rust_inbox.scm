(define-module (crates-io ru st rust_inbox) #:use-module (crates-io))

(define-public crate-rust_inbox-0.0.1 (c (n "rust_inbox") (v "0.0.1") (d (list (d (n "base64") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "06dlaa2ivgq1f52yzypn9bkjq8v2hixcpbf0azj6mhbs2xp8nrcf")))

(define-public crate-rust_inbox-0.0.2 (c (n "rust_inbox") (v "0.0.2") (d (list (d (n "base64") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ynq2g2blkd84rwcmwwq3kxi3pp4pawqvnsb7v5s6d1q6yww98sc")))

(define-public crate-rust_inbox-0.0.3 (c (n "rust_inbox") (v "0.0.3") (d (list (d (n "base64") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1j38y55fbvwvw8082ms9wldmw1cj5qjjvr3ppyhmivm10bx6sjar")))

(define-public crate-rust_inbox-0.0.4 (c (n "rust_inbox") (v "0.0.4") (d (list (d (n "base64") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1pm8wacasyj295s275sppfh0wlbrgb32315kc62kljak3qg0zk8h")))

(define-public crate-rust_inbox-0.0.5 (c (n "rust_inbox") (v "0.0.5") (d (list (d (n "base64") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0jjlky2vf5jnjyfxdzc7kimifng0s0aa5mccvqrwyfqi9bbysbx6")))

