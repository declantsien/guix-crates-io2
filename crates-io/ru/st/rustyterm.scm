(define-module (crates-io ru st rustyterm) #:use-module (crates-io))

(define-public crate-RustyTerm-0.1.2 (c (n "RustyTerm") (v "0.1.2") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1fcw7m308g3s4khg8m5fkjwd14ms8qjm1l8lhlgkirg6grgf97h9") (r "1.70")))

(define-public crate-RustyTerm-0.1.3 (c (n "RustyTerm") (v "0.1.3") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1pq2xd3iydl6lzrnnh32c90bh90zxadlnm2kqqdq29bdn15abxij") (r "1.70")))

