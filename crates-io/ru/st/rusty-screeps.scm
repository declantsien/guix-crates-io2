(define-module (crates-io ru st rusty-screeps) #:use-module (crates-io))

(define-public crate-rusty-screeps-0.0.1 (c (n "rusty-screeps") (v "0.0.1") (d (list (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "screeps-game-api") (r "^0.4") (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (d #t) (k 0)))) (h "0r9aiwipgd99q92wrjlqv0qfz39jsnk6kw01d5s3knnvv4jcy73k")))

(define-public crate-rusty-screeps-0.0.2 (c (n "rusty-screeps") (v "0.0.2") (d (list (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "screeps-game-api") (r "^0.6") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.18") (d #t) (k 0)))) (h "0r7p1cfhxhhslllfhjqp5bgfxza39rrf7iip8ffnmimn3w5mirrk")))

