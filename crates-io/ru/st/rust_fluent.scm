(define-module (crates-io ru st rust_fluent) #:use-module (crates-io))

(define-public crate-rust_fluent-0.0.1 (c (n "rust_fluent") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "11mz5kgqwgxwqidjjbbqjd61gxpd5x4apnd4v6wvjjmn6z7vknly")))

(define-public crate-rust_fluent-0.1.0 (c (n "rust_fluent") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "07n5q9bkbnlfiz0kbd4invhdrz918n2jri9dd1v85jnnmn2bq0yp")))

(define-public crate-rust_fluent-0.2.0 (c (n "rust_fluent") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1qblv0b1bba778rzjsxpafgr1173h83d1ph4zi18rk6g5csghygm")))

(define-public crate-rust_fluent-0.3.0 (c (n "rust_fluent") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1kklh0ah72lz5wx0q0xmspf6f4jjkg8n842p3bf3869y2mgms4hb")))

