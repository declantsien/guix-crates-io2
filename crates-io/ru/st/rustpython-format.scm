(define-module (crates-io ru st rustpython-format) #:use-module (crates-io))

(define-public crate-rustpython-format-0.3.0 (c (n "rustpython-format") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "malachite-bigint") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rustpython-literal") (r "^0.3.0") (d #t) (k 0)))) (h "0sfskgzwxb9cmhwxlv9sx1nx2py8m9f765iqkggzg1829l3mz1fr")))

(define-public crate-rustpython-format-0.3.1 (c (n "rustpython-format") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "malachite-bigint") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rustpython-literal") (r "^0.3.1") (d #t) (k 0)))) (h "0wmdlgg7s2ihz15krld6is6mnr5n94nwrgcigin4v253432vn1xs") (f (quote (("default" "malachite-bigint")))) (r "1.72.1")))

