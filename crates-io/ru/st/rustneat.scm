(define-module (crates-io ru st rustneat) #:use-module (crates-io))

(define-public crate-rustneat-0.1.0 (c (n "rustneat") (v "0.1.0") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0mga3nl0l0lz6zpvmwjd508bcmgzjjn5ggq19fqvrj5pnipbhyid")))

(define-public crate-rustneat-0.1.1 (c (n "rustneat") (v "0.1.1") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0blmsr0qb23rkwr85yy8isf13zigknzy9158z3knb44lnnbjijz3")))

(define-public crate-rustneat-0.1.2 (c (n "rustneat") (v "0.1.2") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1a0p0r20pgjij1hdmv874yx9mxa21wy055sjidjamql07smv7z1l")))

(define-public crate-rustneat-0.1.3 (c (n "rustneat") (v "0.1.3") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1mgng86p4gnl8wh7l1fijpjpgsqqvmvwl1sr3bmqbypik0hdzb55")))

(define-public crate-rustneat-0.1.4 (c (n "rustneat") (v "0.1.4") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0940ig8ys8k4w6j286c39mgzpvrzwz4g1lyz4vdzf7055yb5vacj")))

(define-public crate-rustneat-0.1.5 (c (n "rustneat") (v "0.1.5") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.4") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1099hf2ndpf4vv8mkb5gvrkm12gwhlvyg6nlfq53nkfm1iacs875")))

(define-public crate-rustneat-0.1.6 (c (n "rustneat") (v "0.1.6") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.4") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1vs8wj5ccghdw61piz7v8ikp20q9nwd906l5insf1ayrb4av6jch")))

(define-public crate-rustneat-0.1.7 (c (n "rustneat") (v "0.1.7") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.4") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "03m2jiavgahamkdhvpkrzai7cw0mha700jl4s633rxcmh1hmvdxy")))

(define-public crate-rustneat-0.1.8 (c (n "rustneat") (v "0.1.8") (d (list (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.4") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0sggzvqc3sx0a0b9g1873dj1d0gxkn19alga5j28vslylhzkmifz")))

(define-public crate-rustneat-0.2.0 (c (n "rustneat") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.103") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)) (d (n "rusty_dashed") (r "^0.2.1") (d #t) (k 0)))) (h "057g2h9dzgzsvv31vbll4wa7sc1aij24xhbdl5dlhyvv8rj60czh") (f (quote (("telemetry") ("default"))))))

(define-public crate-rustneat-0.2.1 (c (n "rustneat") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.103") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "open") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rulinalg") (r "^0.3.4") (d #t) (k 0)) (d (n "rusty_dashed") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lmk24wwv6kpc4g5smqcgsbryzdshc8bwcz4ljwk44adk1ymzmyl") (f (quote (("telemetry" "rusty_dashed" "open" "serde" "serde_derive" "serde_json") ("default"))))))

