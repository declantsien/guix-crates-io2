(define-module (crates-io ru st rustosc) #:use-module (crates-io))

(define-public crate-rustosc-0.1.0 (c (n "rustosc") (v "0.1.0") (h "0q68c1xirsq4b9728gjqmbcfhwrwyw9w27zsc6imws363rbskgaq")))

(define-public crate-rustosc-0.1.1 (c (n "rustosc") (v "0.1.1") (h "1nykxkjm7x67l125cn70c2c0d9q0p9gw73ihmmrz837smwslp7i2")))

(define-public crate-rustosc-0.1.2 (c (n "rustosc") (v "0.1.2") (h "0rxqjd92cij7yzrv6r3v75gqkfzvw4h90k1i40f9r5nyxqjhvwc7")))

