(define-module (crates-io ru st rustics) #:use-module (crates-io))

(define-public crate-rustics-0.1.0 (c (n "rustics") (v "0.1.0") (h "151lb0i02fl8c8gpx5vxd6cgg72h007fili678wjdfxd6g3mc8mb")))

(define-public crate-rustics-0.2.0 (c (n "rustics") (v "0.2.0") (h "1g101x6l3mmqaz15xmgciz2ka2mi4hcm97l341cmx98yyvyx18gq")))

(define-public crate-rustics-0.3.0 (c (n "rustics") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "009zvi1g4qhrqpd9fy7lrg0vdmbm89sm51r85mbf5hng3a7dwsrl")))

(define-public crate-rustics-0.3.1 (c (n "rustics") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ab76xilbbbbby2fbclbwspi4vv00ss9n20i7ld80290wpbk1dra")))

(define-public crate-rustics-0.4.0 (c (n "rustics") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07gw104ckl71cjy9a1545wrjlpfw40xhizxih7hjlr2vqabfairq")))

(define-public crate-rustics-0.4.1 (c (n "rustics") (v "0.4.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fd8k3a803r2dm2l89ydlpq73wxq1ikbbixx7z1yjng6kx76q91y")))

(define-public crate-rustics-0.5.0 (c (n "rustics") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "10wi33dxnkabicff77pnzpd1glvp4vfp3z4pqwiyz1n8c4sib7j8")))

