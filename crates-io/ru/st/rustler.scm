(define-module (crates-io ru st rustler) #:use-module (crates-io))

(define-public crate-rustler-0.1.0 (c (n "rustler") (v "0.1.0") (d (list (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "0ndjwr89iy9gv3s2ni8z7zx4ihnankn213q1nds2jzkj3a3sz2rx")))

(define-public crate-rustler-0.2.0 (c (n "rustler") (v "0.2.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "1az14906spvym857s4m5006hbkhhqhxjslxgmyw39kqcs6m0xmvj")))

(define-public crate-rustler-0.3.0 (c (n "rustler") (v "0.3.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "00zncfw5pksy2ipnmj8906434jy7sl01azplllhc1zc3v90gijdi")))

(define-public crate-rustler-0.4.0 (c (n "rustler") (v "0.4.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "012vya19afvblb7x63ggfa93dj1jdnqhwpkhy15mgmnj5sd79dph")))

(define-public crate-rustler-0.5.0 (c (n "rustler") (v "0.5.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "02kj32pm0ywl16ymms7pwpyn4kq146lfbjhdflv5jbd60f10hyg9")))

(define-public crate-rustler-0.6.0 (c (n "rustler") (v "0.6.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "0fdfi636nsj7a0kbx7zq17l1hyv5czkvh6dgib5c3lv17hvv0pgk")))

(define-public crate-rustler-0.7.0 (c (n "rustler") (v "0.7.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "110cq67gk6nljkgx702k3j769krciml6d6n6qac6migxbak0imc0")))

(define-public crate-rustler-0.8.0 (c (n "rustler") (v "0.8.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "0zm97drxgyjh4p17i08jb2x4p9vj917pyw3shcnzsdvp963vb2sd")))

(define-public crate-rustler-0.8.1 (c (n "rustler") (v "0.8.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.2") (d #t) (k 0)))) (h "1545dn1jv7lrk8mzwrkdgiv4dsn0j7y5mvk7r39m21zh7wcrs7fn")))

(define-public crate-rustler-0.8.2 (c (n "rustler") (v "0.8.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.4") (d #t) (k 0)))) (h "1bvlay33i8n594i3h9h4yz8n69w9ha80m2m14w5vrifz8y2fb7zk")))

(define-public crate-rustler-0.8.3 (c (n "rustler") (v "0.8.3") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "ruster_unsafe") (r ">= 0.4") (d #t) (k 0)))) (h "1r5avn3cbas270gb9ixdzgx40j7abcpsjwcvp6m5swkmnb42f67r")))

(define-public crate-rustler-0.9.0 (c (n "rustler") (v "0.9.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.5") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0y0jbk74wcgf6psd2sy7kbg3b5ci8pcg3ls2f4wxarl43vpv9y42")))

(define-public crate-rustler-0.9.1 (c (n "rustler") (v "0.9.1") (d (list (d (n "erlang_nif-sys") (r ">= 0.5") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "09zj74dlfzd8cnh0ixcs3fyh8zym63hija70krmnf2pdix97k91m")))

(define-public crate-rustler-0.10.0 (c (n "rustler") (v "0.10.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.5") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0s4x45ns2r9ci8lfrdxxrcwzk5h9dnllh8wcirqiymzyal54d23w")))

(define-public crate-rustler-0.11.0 (c (n "rustler") (v "0.11.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.5") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "11k1fp3l6bndl5z4p866x3whn2c8wmbq0ab1rw1h0449knm8wm5l")))

(define-public crate-rustler-0.12.0 (c (n "rustler") (v "0.12.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.5.4") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "14m6qfi4ylknmziddsxr4k175w915c4bca8dp3jh7b8862bzw7ip")))

(define-public crate-rustler-0.13.0 (c (n "rustler") (v "0.13.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.5.4") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "189fnyxy5kvikf8vpvy04a9zb61fjgfxd4whwal04cvza0n2x021")))

(define-public crate-rustler-0.14.0 (c (n "rustler") (v "0.14.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "09gkaj07dvlnc281xz2g6ib4wyd1vyjyvng513zwp55wf1hcfhqy")))

(define-public crate-rustler-0.15.0 (c (n "rustler") (v "0.15.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0qr73a9svbhsvswziikxmlsvqfszvyy5mnpnm93ra9q70a8y0wyl")))

(define-public crate-rustler-0.15.1 (c (n "rustler") (v "0.15.1") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "1vqw1bnw5p2ajmjadg421mj6bvj1ka097d8h12ij4b5rdf31iin9")))

(define-public crate-rustler-0.16.0 (c (n "rustler") (v "0.16.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "18k9wgqwdwbqv14nhz3sw5zq2iv507z998xjlakjbg0pgwwqwsrl") (f (quote (("alternative_nif_init_name"))))))

(define-public crate-rustler-0.17.0 (c (n "rustler") (v "0.17.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1ipjclv7zwpf3q69l6a75mx8yn3r3l9sni2crkzf8gm1fc0lc70g") (f (quote (("alternative_nif_init_name"))))))

(define-public crate-rustler-0.17.1 (c (n "rustler") (v "0.17.1") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1rmsbi09lkjs4qvvfdf1gv7mv6z2d5zk26b0bla694qsmgzgyb8p") (f (quote (("alternative_nif_init_name"))))))

(define-public crate-rustler-0.18.0 (c (n "rustler") (v "0.18.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1ck9qiq01y9fgnvfsxkg2ak8vd9ayqs8h62gs2hvx2gi28gz9j2n") (f (quote (("alternative_nif_init_name"))))))

(define-public crate-rustler-0.19.0 (c (n "rustler") (v "0.19.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "170rxkjxfc9nc2dnc4vm2hcz9qy9988jpcjd0ana7bzd2zvf3kzb") (f (quote (("alternative_nif_init_name"))))))

(define-public crate-rustler-0.20.0 (c (n "rustler") (v "0.20.0") (d (list (d (n "erlang_nif-sys") (r ">= 0.6.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 1)) (d (n "which") (r "^2") (d #t) (k 1)))) (h "10rwfj8303pj5dzsvi326qq0f83x6g1ffh2irgvbaigw03scmmk7") (f (quote (("alternative_nif_init_name"))))))

(define-public crate-rustler-0.21.0 (c (n "rustler") (v "0.21.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 1)) (d (n "rustler_codegen") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.0") (d #t) (k 0)) (d (n "which") (r "^2") (d #t) (k 1)))) (h "14rz80zcyavldwlr59nzgll7p5p24jxvwnnhkb8893p8hhh82z88") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.22.0-rc.0 (c (n "rustler") (v "0.22.0-rc.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.22.0-rc.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)))) (h "0bl3hfdmd5dbym7cm18s3vnr6n75gmyz2kcxas7q1xw394y9a9v6") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.21.1 (c (n "rustler") (v "0.21.1") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 1)) (d (n "rustler_codegen") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)) (d (n "which") (r "^2") (d #t) (k 1)))) (h "16wkdnkswxnfdimzqac7431ikkjjkcg98ai6rr4nf5hgk8vw6gak") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.22.0-rc.1 (c (n "rustler") (v "0.22.0-rc.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.22.0-rc.1") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)))) (h "0f2s9ncfhaz1cl1i3mdfjp8ix63jankm483rjyh9f7wqv66ym41g") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.22.0-rc.2 (c (n "rustler") (v "0.22.0-rc.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.22.0-rc.2") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)))) (h "0pfd2aqwjdjf2z0w25wp6y098p9dmsbf23kdygql0lhapjpcilng") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.22.0 (c (n "rustler") (v "0.22.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)))) (h "1hvzz7x9czncbvv8b58md0kijdirxg6i1hy0shfg81q0m2rd71xp") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.22.1 (c (n "rustler") (v "0.22.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)))) (h "026j3a8ggyd1i16hcp2mi55fkb7jxaj3sl3ybsqmjnphbq0mc00z") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.22.2 (c (n "rustler") (v "0.22.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.22.2") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)))) (h "1f3wrck3181di8nm93c4ip0zzg0xnba8drz3w97l35fk65cina08") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.23.0 (c (n "rustler") (v "0.23.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.1") (d #t) (k 0)))) (h "098ni53frk6djz0ssvnnr1z3p4wilbd4i9cdzwb9r05frxgrxiwa") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.24.0 (c (n "rustler") (v "0.24.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.2") (d #t) (k 0)))) (h "1psnsvprmnhlbda7f9rw22jw3l8wldqd78lyjgr7si3c00lh27gd") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.25.0 (c (n "rustler") (v "0.25.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.2") (d #t) (k 0)))) (h "1i9886zia5s2msc880qmzywnni8g3qk2xhcjwwnzpb3bm1zn3rn3") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.26.0 (c (n "rustler") (v "0.26.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.26.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.2") (d #t) (k 0)))) (h "0ba22cdqlyfcrr066fmm06xsp5d585rg3dnpals5286yfpgqs7nn") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.27.0 (c (n "rustler") (v "0.27.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.2") (d #t) (k 0)))) (h "0vng9adwkbmxx9rnmcw5shg50bcqrk9kdnpsavm9dfgacdq0795p") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.28.0 (c (n "rustler") (v "0.28.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.28.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.2") (d #t) (k 0)))) (h "0vxxd3cgwymw892zigmy9h414q32vspv6js3iraf0ai7rfc2yykx") (f (quote (("derive" "rustler_codegen") ("default" "derive") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.29.0 (c (n "rustler") (v "0.29.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.3.0") (d #t) (k 0)))) (h "09ihydpz1s1iamph6074ngv74f4zzqki8nwwc2008mk453xv0pq9") (f (quote (("nif_version_2_17" "nif_version_2_16" "rustler_sys/nif_version_2_17") ("nif_version_2_16" "nif_version_2_15" "rustler_sys/nif_version_2_16") ("nif_version_2_15" "nif_version_2_14" "rustler_sys/nif_version_2_15") ("nif_version_2_14" "rustler_sys/nif_version_2_14") ("derive" "rustler_codegen") ("default" "derive" "nif_version_2_15") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.29.1 (c (n "rustler") (v "0.29.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.29.1") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.3.0") (d #t) (k 0)))) (h "07izk35z0kw829l8c1v0dqzcynkax72p340zqpid6hwz7dicp108") (f (quote (("nif_version_2_17" "nif_version_2_16" "rustler_sys/nif_version_2_17") ("nif_version_2_16" "nif_version_2_15" "rustler_sys/nif_version_2_16") ("nif_version_2_15" "nif_version_2_14" "rustler_sys/nif_version_2_15") ("nif_version_2_14" "rustler_sys/nif_version_2_14") ("derive" "rustler_codegen") ("default" "derive" "nif_version_2_15") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.30.0 (c (n "rustler") (v "0.30.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.30.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.3.0") (d #t) (k 0)))) (h "0zjaxl6gk4dnsi6j85qcapd1il6j4ik9sxh65k26ipi3kskgxd64") (f (quote (("nif_version_2_17" "nif_version_2_16" "rustler_sys/nif_version_2_17") ("nif_version_2_16" "nif_version_2_15" "rustler_sys/nif_version_2_16") ("nif_version_2_15" "nif_version_2_14" "rustler_sys/nif_version_2_15") ("nif_version_2_14" "rustler_sys/nif_version_2_14") ("derive" "rustler_codegen") ("default" "derive" "nif_version_2_15") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.31.0 (c (n "rustler") (v "0.31.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.31.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.3.2") (d #t) (k 0)))) (h "123h521pgckkmzvnmr3pjw0r5ha1fajpqd2b1rnrfl7m727lapd7") (f (quote (("nif_version_2_17" "nif_version_2_16" "rustler_sys/nif_version_2_17") ("nif_version_2_16" "nif_version_2_15" "rustler_sys/nif_version_2_16") ("nif_version_2_15" "nif_version_2_14" "rustler_sys/nif_version_2_15") ("nif_version_2_14" "rustler_sys/nif_version_2_14") ("derive" "rustler_codegen") ("default" "derive" "nif_version_2_15") ("alternative_nif_init_name"))))))

(define-public crate-rustler-0.32.0 (c (n "rustler") (v "0.32.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.32.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.4.0") (d #t) (k 0)))) (h "01cw6pxz69an10dqnnh5zbl96yj73p668zvma7i3a802m08pn87s") (f (quote (("nif_version_2_17" "nif_version_2_16" "rustler_sys/nif_version_2_17") ("nif_version_2_16" "nif_version_2_15" "rustler_sys/nif_version_2_16") ("nif_version_2_15" "nif_version_2_14" "rustler_sys/nif_version_2_15") ("nif_version_2_14" "rustler_sys/nif_version_2_14") ("derive" "rustler_codegen") ("default" "derive" "nif_version_2_15") ("alternative_nif_init_name")))) (y #t) (s 2) (e (quote (("big_integer" "dep:num-bigint"))))))

(define-public crate-rustler-0.32.1 (c (n "rustler") (v "0.32.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.32.1") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0csy23vcm6n6mnpvi0qmbhasjxh65q2z3qh88z7fplxf3sh31hw3") (f (quote (("nif_version_2_17" "nif_version_2_16" "rustler_sys/nif_version_2_17") ("nif_version_2_16" "nif_version_2_15" "rustler_sys/nif_version_2_16") ("nif_version_2_15" "nif_version_2_14" "rustler_sys/nif_version_2_15") ("nif_version_2_14" "rustler_sys/nif_version_2_14") ("derive" "rustler_codegen") ("default" "derive" "nif_version_2_15") ("alternative_nif_init_name")))) (s 2) (e (quote (("serde" "dep:serde") ("big_integer" "dep:num-bigint"))))))

(define-public crate-rustler-0.33.0 (c (n "rustler") (v "0.33.0") (d (list (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustler_codegen") (r "^0.33.0") (o #t) (d #t) (k 0)) (d (n "rustler_sys") (r "~2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0lnk5v30l43s75fwi59wyf70g5b7rrd8bp83wsiw6mww4gh1mma5") (f (quote (("nif_version_2_17" "nif_version_2_16" "rustler_sys/nif_version_2_17") ("nif_version_2_16" "nif_version_2_15" "rustler_sys/nif_version_2_16") ("nif_version_2_15" "nif_version_2_14" "rustler_sys/nif_version_2_15") ("nif_version_2_14" "rustler_sys/nif_version_2_14") ("derive" "rustler_codegen") ("default" "derive" "nif_version_2_15") ("alternative_nif_init_name") ("allocator")))) (s 2) (e (quote (("serde" "dep:serde") ("big_integer" "dep:num-bigint")))) (r "1.70")))

