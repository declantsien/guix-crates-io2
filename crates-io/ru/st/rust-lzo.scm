(define-module (crates-io ru st rust-lzo) #:use-module (crates-io))

(define-public crate-rust-lzo-0.1.0 (c (n "rust-lzo") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0r2snj4qnj8i0kahc4dsj1c84vbvdqbb4zagadlyychza1a885bn")))

(define-public crate-rust-lzo-0.1.1 (c (n "rust-lzo") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0yqnqh4cshx0rlib1lzw36x2jara4f5hgpgk041741pz5n2l6jc7")))

(define-public crate-rust-lzo-0.2.0 (c (n "rust-lzo") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0cykq1zh7zc1ddrwqdafxh0gxi9a6r6l8chpigbd6nvqwfw9lg58")))

(define-public crate-rust-lzo-0.3.0 (c (n "rust-lzo") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1y7iviafrrrg77d0rhgjrx1jdqw8wfr7prrhi34l455li0jd6nbg")))

(define-public crate-rust-lzo-0.4.0 (c (n "rust-lzo") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "06zh0nr074a3yrmdg6g2w1hm5i75y8q8l88axxncns4sgxmylf48")))

(define-public crate-rust-lzo-0.4.1 (c (n "rust-lzo") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "084vp6qd46sk7jkfmcrykim9v7jxb6qfx6j48j5k61ra6s5wbk7i")))

(define-public crate-rust-lzo-0.5.0 (c (n "rust-lzo") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1bc70fwk5vim1xyg1z3dkssgcx55qb6cwxaar0wb7zwjh0jsnnr5")))

(define-public crate-rust-lzo-0.6.0 (c (n "rust-lzo") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1v5zgdwzkdh6bgv9phjvjvv60kzp41axl1a2171w8qkpfabzg3jn")))

(define-public crate-rust-lzo-0.6.1 (c (n "rust-lzo") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1j1z8vlyfr7js6i9nbp3x1klkkicrvyg2dicy4qd1mvqzamj4a2y")))

(define-public crate-rust-lzo-0.6.2 (c (n "rust-lzo") (v "0.6.2") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1a6ixk0m5c3x79xdxzgi21ha1ahdv0bnwdkzpfspqxalp6qil6dz")))

