(define-module (crates-io ru st rustifact_extra) #:use-module (crates-io))

(define-public crate-rustifact_extra-0.1.0 (c (n "rustifact_extra") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "rustifact") (r "^0.9") (d #t) (k 0)))) (h "1gjh6in6mzamq67j6j95k5syzmg76195w684jpwf235pynsmbwdw")))

