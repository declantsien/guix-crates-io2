(define-module (crates-io ru st rustwlc) #:use-module (crates-io))

(define-public crate-rustwlc-0.1.0 (c (n "rustwlc") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "0zpw8inywa5agzmy19jpafg1srmznjf2zmahpv850rzf6hxbqcfs")))

(define-public crate-rustwlc-0.2.0 (c (n "rustwlc") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "18b4yyc5f33lcmiw8504clvsq9cm6zd0wakwrixpbif92hkqw6l4")))

(define-public crate-rustwlc-0.2.1 (c (n "rustwlc") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "0yvvbhpfmhcpqbc1rwvi5kzzw6zy7zzz6s46fqik64xn8j9i71ik")))

(define-public crate-rustwlc-0.3.0 (c (n "rustwlc") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1hi7vs9fgqq79cgnlri6sfzq7px5sv7nbj362hcnfn460yhpksvm")))

(define-public crate-rustwlc-0.3.1 (c (n "rustwlc") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1bw3p4i2kj8qdlvnd7v47c4g65351v6vgii40hymjar612mmmf4n")))

(define-public crate-rustwlc-0.4.0 (c (n "rustwlc") (v "0.4.0") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0d59y6x8p90r3d2cm10z2g06rbqyjaddlcspi8nwrv6fqqpqxgas")))

(define-public crate-rustwlc-0.5.0 (c (n "rustwlc") (v "0.5.0") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 2)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "19wbszix177clnsmqyzdwni4n8pfk9yf3l6s9ng24js7dsa23nmm") (f (quote (("wlc-wayland" "wayland-sys"))))))

(define-public crate-rustwlc-0.5.1 (c (n "rustwlc") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "0cpm2wf8cvn7bs6qb78zqi2a334gybb6hyj2wkvw7yhdw3q00z4n") (f (quote (("wlc-wayland" "wayland-sys"))))))

(define-public crate-rustwlc-0.5.2 (c (n "rustwlc") (v "0.5.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "1kcm4bc46zakcr2bqdzbh2187pnhvd20anj4dlcncz7l1nlcjv1d") (f (quote (("wlc-wayland" "wayland-sys"))))))

(define-public crate-rustwlc-0.5.3 (c (n "rustwlc") (v "0.5.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "15cqyl77v60fy1ad84l2j3d08fxpv4snhlnas42452s3kd4z4n71") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc"))))))

(define-public crate-rustwlc-0.5.4 (c (n "rustwlc") (v "0.5.4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "0m65n1i3n442kchwh3pbmcbcgp3yr8kpv5v9vr04gj4l8f1nl9x7") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.5.5 (c (n "rustwlc") (v "0.5.5") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "0zd8zfbalgsnb0l6jcfqzhc44nh6p30g25kpq7xhzlhs3fp4xf5h") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.5.6 (c (n "rustwlc") (v "0.5.6") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "0g58c47hx2jxsl22w3rnii5bbs7fx2809hkw57wj9qhkagfzza2d") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.6.0 (c (n "rustwlc") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "1q548v82mh54hvrzxc34zxv4y790mjb3gbwxdh4wq4kcjj7j0rv2") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.6.1 (c (n "rustwlc") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "1k8gqfab49aw2fyrj4wdaqqj94ny5fafjz0y6xwqrh2ixh9sqhw8") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.6.2 (c (n "rustwlc") (v "0.6.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "1w9kc6ryabzcqw5s78gd58yw32prn9p6kzzrqwqs5riqy6l1bi0v") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.6.3 (c (n "rustwlc") (v "0.6.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "022x60bb06kbgxl1ds83523gcvlq1igr4izf6s149v0ijqbx9k3g") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.6.4 (c (n "rustwlc") (v "0.6.4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "1aa3vmhbapqxkyl2393c8k4npxs6jsyn7nvsfv9gdvljfhm0bg3h") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.6.5 (c (n "rustwlc") (v "0.6.5") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "18fhv32rgvsf76r3n72717ykg5s79i967x2mrl5hb0yc70s7v0qp") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

(define-public crate-rustwlc-0.7.0 (c (n "rustwlc") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("server"))) (o #t) (d #t) (k 0)))) (h "1qs3ama440hvxbmbsr5882rjpcybawd0zl5zr3rglqf61fg0lf1r") (f (quote (("wlc-wayland" "wayland-sys") ("static-wlc") ("dummy"))))))

