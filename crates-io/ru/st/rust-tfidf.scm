(define-module (crates-io ru st rust-tfidf) #:use-module (crates-io))

(define-public crate-rust-tfidf-1.0.0 (c (n "rust-tfidf") (v "1.0.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1r70fykqww4xylskavdc1s5iy63843zjgbyjarr0i12i73r99jhn")))

(define-public crate-rust-tfidf-1.0.3 (c (n "rust-tfidf") (v "1.0.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0bm6wcqp9nk5nxffqahdqk6kf1y55b880dvcpisf6y1bly41r67p")))

(define-public crate-rust-tfidf-1.0.4 (c (n "rust-tfidf") (v "1.0.4") (d (list (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "0qzw9bqmcji528fvaim552k7lmirz4hp2npjl79wz84gapkbkxjw")))

(define-public crate-rust-tfidf-1.1.0 (c (n "rust-tfidf") (v "1.1.0") (h "0c90mhv3i548fjh6qxyhlfsff79pklzfw79lxw9cr1a25yys14ag")))

(define-public crate-rust-tfidf-1.1.1 (c (n "rust-tfidf") (v "1.1.1") (h "1rjc5dzsd74qkn9ix448303cwby8x0y8w5xlbfg5cbghq8j5lr5s")))

