(define-module (crates-io ru st rust_graphics_library_loader) #:use-module (crates-io))

(define-public crate-rust_graphics_library_loader-0.1.0 (c (n "rust_graphics_library_loader") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "minwindef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "08z9igr7g8n3xnr0c70k0xq1yg4xxsmq4cc908s0znzw8rxsw9mr")))

(define-public crate-rust_graphics_library_loader-0.1.1 (c (n "rust_graphics_library_loader") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "minwindef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0xrwi2jy4isjw1pc7jj2r8mf16infgrp1sd9s82p2ygyd5vl4z9d")))

(define-public crate-rust_graphics_library_loader-0.1.2 (c (n "rust_graphics_library_loader") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "minwindef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1ij9vh7xr6mi4xv5wdcf4dmjj9957q18sancb7dybm6w5z8qpdf7")))

(define-public crate-rust_graphics_library_loader-0.1.3 (c (n "rust_graphics_library_loader") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "minwindef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0qa0rbymw1njhh6ayyckqmz7sgx52l6xamlk805yh4p6i5kjhkk3") (f (quote (("debug_derive"))))))

(define-public crate-rust_graphics_library_loader-0.1.4 (c (n "rust_graphics_library_loader") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "minwindef"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0pnjwg8sir7xp6cxxxrpj7zknafhj706mr2x0hjx9w6h06mfcjm6") (f (quote (("debug_derive"))))))

