(define-module (crates-io ru st rust_events) #:use-module (crates-io))

(define-public crate-rust_events-0.8.0 (c (n "rust_events") (v "0.8.0") (h "1m7z3rkfpn49gq5vj1yn58w2fv6xjajx0z5cnd8acmzsxf3qfdhd")))

(define-public crate-rust_events-0.8.1 (c (n "rust_events") (v "0.8.1") (h "0iq6l6g7ry1cvig6pijdalx8hxzny1vxfjy7pms4jdn3j8bj7wyq")))

(define-public crate-rust_events-0.9.0 (c (n "rust_events") (v "0.9.0") (h "1ylisqca733955lzsrxw5m7qmwc6nlz1xkjy55bs7ayb5kh8vcvc")))

