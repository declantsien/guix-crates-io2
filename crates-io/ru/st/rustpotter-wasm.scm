(define-module (crates-io ru st rustpotter-wasm) #:use-module (crates-io))

(define-public crate-rustpotter-wasm-0.9.1 (c (n "rustpotter-wasm") (v "0.9.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "rustpotter") (r "^0.9.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0iypgkmpfz6z9bsdj735mfgs1x669c66b8awjzzdkahfzilh0gxh") (f (quote (("default"))))))

