(define-module (crates-io ru st rusty-fork) #:use-module (crates-io))

(define-public crate-rusty-fork-0.1.0 (c (n "rusty-fork") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1gfk1ss6hssx85phia25hqfm3l5jv3jmv26vwgfq06qzjslrln4x") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

(define-public crate-rusty-fork-0.1.1 (c (n "rusty-fork") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0rpj76wajczz4d15qd7w3bkpgvhq201dgs5zf5imkyqixivi6401") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

(define-public crate-rusty-fork-0.2.0 (c (n "rusty-fork") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0b88p5i510fr6khld7070cp6aay5dn60xs9bdaab9n2gck9di67a") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

(define-public crate-rusty-fork-0.2.1 (c (n "rusty-fork") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1728v0ln0n37c71964cxn1xiv7wzjannc7y2g6v209w5sa8g34cm") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

(define-public crate-rusty-fork-0.2.2 (c (n "rusty-fork") (v "0.2.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1bjg8adk0i921088j52rn0hmvsry34q19g96x41pamqcw5j35n9x") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

(define-public crate-rusty-fork-0.3.0 (c (n "rusty-fork") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0kxwq5c480gg6q0j3bg4zzyfh2kwmc3v2ba94jw8ncjc8mpcqgfb") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

