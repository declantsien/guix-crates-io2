(define-module (crates-io ru st rusty-cmd) #:use-module (crates-io))

(define-public crate-rusty-cmd-0.1.0 (c (n "rusty-cmd") (v "0.1.0") (h "0mc4rn5rk2wck7jaw872fpr73s0dn478zb05nxy80z1gpf8g0pkz")))

(define-public crate-rusty-cmd-0.2.0 (c (n "rusty-cmd") (v "0.2.0") (h "14f0m6nqbppq7zspkg721y8h07692kx4zsg52iljq7i8ixfq86yn")))

(define-public crate-rusty-cmd-1.0.0 (c (n "rusty-cmd") (v "1.0.0") (h "1qz3g4nh1qgk0mwkdr3ddfd84hhkfnic55iaby8cvk1kjy9r6184")))

