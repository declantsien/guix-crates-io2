(define-module (crates-io ru st rusteams) #:use-module (crates-io))

(define-public crate-rusteams-0.1.0 (c (n "rusteams") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1q3lyg27h0v60x9wcsw16gh570mis1kj73v0sgqw09g0z8cs79cw") (y #t)))

(define-public crate-rusteams-1.0.0 (c (n "rusteams") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1n4awxv285w7kdirqzzdgms1nx0ml28av6fx105nl3813j2jzw4c") (y #t)))

(define-public crate-rusteams-1.0.1 (c (n "rusteams") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0vvc0n8lsgvvjf4957h8y4zhx1g4vy0hmwwfgiqa95n6lsw9788z") (y #t)))

(define-public crate-rusteams-1.0.2 (c (n "rusteams") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0pd7v69jjjyxnplr4vwnp15fpzdxc16rzd58frk9j3lrqxbzqxnl")))

