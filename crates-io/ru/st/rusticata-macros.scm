(define-module (crates-io ru st rusticata-macros) #:use-module (crates-io))

(define-public crate-rusticata-macros-0.1.0 (c (n "rusticata-macros") (v "0.1.0") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1agnlcdyfyg5b508hr3cy0iq8hc9q4jv3rifpi4yx8d9zn7w0zwv")))

(define-public crate-rusticata-macros-0.1.1 (c (n "rusticata-macros") (v "0.1.1") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "13ggwx93ndrpvz9mlg0kicq3s0sdypv2szzsby6y69pj1ixjpjw6")))

(define-public crate-rusticata-macros-0.1.2 (c (n "rusticata-macros") (v "0.1.2") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0xhgrhrq85xaw49mzga6k3w1wdy04zz37wjlhwn3b7fhpqs6dz49")))

(define-public crate-rusticata-macros-0.1.3 (c (n "rusticata-macros") (v "0.1.3") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1cy26xcjrgdhcxhl1zijnq0xzjb90hz6f8bd85k3nymfyp9zn9mq")))

(define-public crate-rusticata-macros-0.2.0 (c (n "rusticata-macros") (v "0.2.0") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1mz8xjs7q69zqmcw2yfbafn17ckvddldgs5aqlnazfygcgfpynyw")))

(define-public crate-rusticata-macros-0.3.0 (c (n "rusticata-macros") (v "0.3.0") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1i9jw3m8c9gwd9ksdv92ykprcjaa4fszf4hy1bn4ixhkw2wxayvk")))

(define-public crate-rusticata-macros-0.3.1 (c (n "rusticata-macros") (v "0.3.1") (d (list (d (n "nom") (r "^2") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0xlccaya0ybd61rf5r1wx6afq4j29inyazrhwpvrivb2mdrzix7w")))

(define-public crate-rusticata-macros-0.3.2 (c (n "rusticata-macros") (v "0.3.2") (d (list (d (n "nom") (r "^3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1j2cnfvb73zrzpmzv47b80fbh7hp974bfnsw9cgfjlr1jx6y5pwc")))

(define-public crate-rusticata-macros-0.3.3 (c (n "rusticata-macros") (v "0.3.3") (d (list (d (n "nom") (r "^3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "12gvvj8hr34xmcm604gv5mgqf67w27mm8gs63q9x4ca3az6ymhn4")))

(define-public crate-rusticata-macros-0.4.0 (c (n "rusticata-macros") (v "0.4.0") (d (list (d (n "nom") (r "^3") (d #t) (k 0)))) (h "1cjw7km2vxbdng1knli0s53ll7f10rxq7kz94rggc62h94s8scvj")))

(define-public crate-rusticata-macros-0.4.1 (c (n "rusticata-macros") (v "0.4.1") (d (list (d (n "nom") (r "^3") (d #t) (k 0)))) (h "0hxj8mx47a141j4sjdr2r4r273adxcsbbh3jkqjmvwcpaz70884j")))

(define-public crate-rusticata-macros-1.0.0 (c (n "rusticata-macros") (v "1.0.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0bd6jiyr0qh92a1sm6fzqv8wpwbrrkkxpl8afjf4wnj2g7h58kvc")))

(define-public crate-rusticata-macros-1.1.0 (c (n "rusticata-macros") (v "1.1.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "1fxpfz05dl3d0gs37p279wd8lk7zk7kr1ccr6r1b6v47a1p9g37y")))

(define-public crate-rusticata-macros-2.0.0 (c (n "rusticata-macros") (v "2.0.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1r617k84naxg4cpixw4bp33nng9j9zxh04jsf50fj77g0hwmjlzk")))

(define-public crate-rusticata-macros-2.0.1 (c (n "rusticata-macros") (v "2.0.1") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1czw1fn3hfsi0blvnkw04pm6cvq5vy21b74xc8w5wyy6jjvp1b29")))

(define-public crate-rusticata-macros-2.0.2 (c (n "rusticata-macros") (v "2.0.2") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1cz9dfy3jv7ngpvkclivb1q1arpr6hvp68gjyf7lgc10ya8fm2mv")))

(define-public crate-rusticata-macros-2.0.3 (c (n "rusticata-macros") (v "2.0.3") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1yfzhr2rqphd2q4b549nj07gdiw7lfzssvd6akpnv6mydm0fqbqs")))

(define-public crate-rusticata-macros-2.0.4 (c (n "rusticata-macros") (v "2.0.4") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1pxrcd8kxln57yw233yrwylyvpp3jkr058khv8bq0imf76labk4k")))

(define-public crate-rusticata-macros-2.1.0 (c (n "rusticata-macros") (v "2.1.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "0yrnnpjr25pwbgcnxkp3vna7bk91808rkghzpa3v98g86q30bagq")))

(define-public crate-rusticata-macros-3.0.0 (c (n "rusticata-macros") (v "3.0.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "0rvdz0x77xbh24x83v392r5w5gihbdjscny0hshyjxmp3q4rniax")))

(define-public crate-rusticata-macros-3.0.1 (c (n "rusticata-macros") (v "3.0.1") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "1j380i6cphsdf2f8hjrvswsyib5paljmza2y9h5i6i3cwrhaz43k")))

(define-public crate-rusticata-macros-3.1.0 (c (n "rusticata-macros") (v "3.1.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "1khnscc8fw1l0kgz2dzsyqs7ga0mbn9d8rhw2ag4gad4r513xnyq")))

(define-public crate-rusticata-macros-3.2.0 (c (n "rusticata-macros") (v "3.2.0") (d (list (d (n "nom") (r "^6.0") (f (quote ("std"))) (k 0)))) (h "1b91k644rqblbam6rfhhmgcxs0zddldi2h0w93aapv1kqq9fbgpv")))

(define-public crate-rusticata-macros-4.0.0 (c (n "rusticata-macros") (v "4.0.0") (d (list (d (n "nom") (r "^7.0") (f (quote ("std"))) (k 0)))) (h "03dmfxhgwzpm1360iwcpcg3y18ddgya0i0hc599am212pdvj7ib5")))

(define-public crate-rusticata-macros-4.1.0 (c (n "rusticata-macros") (v "4.1.0") (d (list (d (n "nom") (r "^7.0") (f (quote ("std"))) (k 0)))) (h "0ch67lljmgl5pfrlb90bl5kkp2x6yby1qaxnpnd0p5g9xjkc9w7s")))

