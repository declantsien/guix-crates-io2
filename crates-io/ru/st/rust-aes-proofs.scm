(define-module (crates-io ru st rust-aes-proofs) #:use-module (crates-io))

(define-public crate-rust-aes-proofs-0.1.0 (c (n "rust-aes-proofs") (v "0.1.0") (d (list (d (n "aes-soft") (r "^0.3.3") (d #t) (k 0)) (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "1r4r3dn55qh76jkdzl9pnaidj0wljxxycq2i4hgrw50i5pnrh6lk")))

(define-public crate-rust-aes-proofs-0.2.0 (c (n "rust-aes-proofs") (v "0.2.0") (d (list (d (n "aes-soft") (r "^0.3.3") (d #t) (k 0)) (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "ocl") (r "^0.19.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "0f15mp0a1cvcmd6g238i0iajwkgi0f46d66f1b8xpmi2afqngk2h")))

