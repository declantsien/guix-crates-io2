(define-module (crates-io ru st rust-hdl-test-core) #:use-module (crates-io))

(define-public crate-rust-hdl-test-core-0.1.0 (c (n "rust-hdl-test-core") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-macros") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-sim-chips") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-yosys-synth") (r "^0.1") (d #t) (k 0)))) (h "0gdyifbkpyf6j0lyajp99cq6vsayngai8g1nzpmvxi98hpg6hyfa") (y #t)))

