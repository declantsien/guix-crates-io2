(define-module (crates-io ru st rust-tcl-sys) #:use-module (crates-io))

(define-public crate-rust-tcl-sys-0.1.0 (c (n "rust-tcl-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "156sbf342nirnmjyxzxjmzvvwnl15g6vzgy4pz3wa02igfxzaz43") (f (quote (("use-pkgconfig") ("default"))))))

(define-public crate-rust-tcl-sys-0.2.0 (c (n "rust-tcl-sys") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0nfn43xywgzxgfjp3vl507anyswyms1rcqvhlgkfy0i6ia01hygh") (f (quote (("use-pkgconfig") ("default"))))))

