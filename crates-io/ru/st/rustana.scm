(define-module (crates-io ru st rustana) #:use-module (crates-io))

(define-public crate-rustana-0.0.1 (c (n "rustana") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "026k6vw684whpmlxpq6llzprxy29g9fl1m40x062qmxnbiip14zj")))

(define-public crate-rustana-0.0.2 (c (n "rustana") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "04b65imy2chqrw1nw8yrwzlwx554h6pzhbdm9dj5qspslrq8d8k7")))

