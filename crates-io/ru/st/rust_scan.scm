(define-module (crates-io ru st rust_scan) #:use-module (crates-io))

(define-public crate-rust_scan-1.0.0 (c (n "rust_scan") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "049r17267hg7y5lr98byfn8cd015yisjjrhmabj1f9r6qx7fjcnq")))

