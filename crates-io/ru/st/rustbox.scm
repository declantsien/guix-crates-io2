(define-module (crates-io ru st rustbox) #:use-module (crates-io))

(define-public crate-rustbox-0.1.0 (c (n "rustbox") (v "0.1.0") (h "0qcyjdbynjc8xdhcjx4cvgfdn474jr14slwwas0w0k9s10i6asf8")))

(define-public crate-rustbox-0.2.0 (c (n "rustbox") (v "0.2.0") (d (list (d (n "termbox-sys") (r "*") (d #t) (k 0)))) (h "15afb1jncd1hlfp5xss0ic845g4i8amawg09f8kslp76q4d65mfz") (y #t)))

(define-public crate-rustbox-0.2.1 (c (n "rustbox") (v "0.2.1") (d (list (d (n "termbox-sys") (r "^0.1.0") (d #t) (k 0)))) (h "17ivqsp25miq9ijm3f0v37s9hvqyhs029hpsa2w5fkkas33y9y84")))

(define-public crate-rustbox-0.2.2 (c (n "rustbox") (v "0.2.2") (d (list (d (n "termbox-sys") (r "^0.1.1") (d #t) (k 0)))) (h "14jjbf1191p03wpmw3wafslnd4w4w4q9xddyzxq9m66vvcf8pj79")))

(define-public crate-rustbox-0.2.3 (c (n "rustbox") (v "0.2.3") (d (list (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0fw6vd7vb9vcncrz25v0jkdlk85irwvb7n1glb5j8bax494hf6np")))

(define-public crate-rustbox-0.2.4 (c (n "rustbox") (v "0.2.4") (d (list (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1rjsnpr7jh98hzdnmmcml5p7y0nw6lz49y6bj80haf56c7n0gvmd")))

(define-public crate-rustbox-0.2.5 (c (n "rustbox") (v "0.2.5") (d (list (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1y1dgk4r3yjvhcgz9gn2ca8qq0q2gsk00kyc9nczxgl9l172a07m")))

(define-public crate-rustbox-0.2.6 (c (n "rustbox") (v "0.2.6") (d (list (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1dna401jw4299dfalpa2sdivwcx5l4yylxk35lhhc2icdxkqi6jr")))

(define-public crate-rustbox-0.2.7 (c (n "rustbox") (v "0.2.7") (d (list (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1f9vv2pc9hrp8zkckwixqyvszl8np2fbirnz16w504g15g83qpj5")))

(define-public crate-rustbox-0.2.8 (c (n "rustbox") (v "0.2.8") (d (list (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0b5wid0alyp1hqhd1yvp2f0kl38sfi9gqv8ybppdx0j8sxa5c1lc")))

(define-public crate-rustbox-0.2.9 (c (n "rustbox") (v "0.2.9") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "19rs4mjiq23j9zdixshrj9l25147s6w53a3822i84drp4i3lqd82")))

(define-public crate-rustbox-0.2.10 (c (n "rustbox") (v "0.2.10") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.2") (d #t) (k 0)))) (h "15gg6mhv79wp2wzy99sgq59cy7v3b6frmfns3viwyp3chsikxr4r")))

(define-public crate-rustbox-0.2.11 (c (n "rustbox") (v "0.2.11") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1wi5dvhb9hpgfjq1gqyr1b7ma1q9c2mcdrglmy0i0xqviz8afbk8")))

(define-public crate-rustbox-0.2.12 (c (n "rustbox") (v "0.2.12") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1wgp79d3d4y6l76nbm3byx3g1v2mk57c108n5954z45jymiqxi4k")))

(define-public crate-rustbox-0.2.13 (c (n "rustbox") (v "0.2.13") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.6") (d #t) (k 0)))) (h "1ar9w05j3hq3fhlnyxbnw04ycvh8l912w8a20phn1gsm800sx3d9")))

(define-public crate-rustbox-0.2.14 (c (n "rustbox") (v "0.2.14") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.6") (d #t) (k 0)))) (h "12lhcfv8hfy4bshsffmhjh72x72icqyr7bkj8jgpinqkyjfk6zrv")))

(define-public crate-rustbox-0.2.15 (c (n "rustbox") (v "0.2.15") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.7") (d #t) (k 0)))) (h "19bslxwix24vhankv0rnixilv0nihxhgpl58wrhyzn8iw4p2h2pr")))

(define-public crate-rustbox-0.2.16 (c (n "rustbox") (v "0.2.16") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.8") (d #t) (k 0)))) (h "17gmwbv4dl4m9hlzpr298np3fz8dr9qnrbqfnr3vxq8l5fzdzyqf")))

(define-public crate-rustbox-0.2.17 (c (n "rustbox") (v "0.2.17") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.9") (d #t) (k 0)))) (h "1pxa83yp31cxp9wrrpc9c9is1banc2zl7a5jbb7fs8w71xam8wn3")))

(define-public crate-rustbox-0.3.0 (c (n "rustbox") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.1.9") (d #t) (k 0)))) (h "0a8wil57yqynajfb3p45k8s0p750x5a5dwk2hbrqnkqj653cjj3s")))

(define-public crate-rustbox-0.3.1 (c (n "rustbox") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.0") (d #t) (k 0)))) (h "16gldf000aklrqidhhcmngql1yfg33w2x8qcyn75mfc5dgwxmagc")))

(define-public crate-rustbox-0.3.2 (c (n "rustbox") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.1") (d #t) (k 0)))) (h "09n9ba3iga5qk0w53xsnz2rjmpdfskqjcazq6kfqglhlfd75644a")))

(define-public crate-rustbox-0.4.0 (c (n "rustbox") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1zfp268g3v0jrdlbyczzhqgmzskxmx9b3jq87ysk500jq4q4p0z1")))

(define-public crate-rustbox-0.4.1 (c (n "rustbox") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1pa73y8smp1fmxcyqikvl27rf7j860flgrqsfdp78ljxlf60nlh3")))

(define-public crate-rustbox-0.4.2 (c (n "rustbox") (v "0.4.2") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.2") (d #t) (k 0)))) (h "1x833qrmladd7bf6mx4ik9p5by4sshzd3q2jwf2rbpnj05p38w0a")))

(define-public crate-rustbox-0.4.3 (c (n "rustbox") (v "0.4.3") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.3") (d #t) (k 0)))) (h "1n89gnnn2451zav8rfmrrr8jk3l5wzmj0aiff1rmnw648dcdsm8w")))

(define-public crate-rustbox-0.4.4 (c (n "rustbox") (v "0.4.4") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.4") (d #t) (k 0)))) (h "18ya8h8c9db7ac32w9npivappz6g030npj9k9qb83bpj2fwzd93i")))

(define-public crate-rustbox-0.4.5 (c (n "rustbox") (v "0.4.5") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.6") (d #t) (k 0)))) (h "1rwh1wmjanlbwjvzlhagczfk2s04vgl97ii8cz0gaab73c5gjas6")))

(define-public crate-rustbox-0.5.0 (c (n "rustbox") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "gag") (r "^0.1.4") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.6") (d #t) (k 0)))) (h "1r3wfbl3dn1gvx9xz8bhw0kmrad49h7ksk9vxgdmslzhc6knrw9i")))

(define-public crate-rustbox-0.5.1 (c (n "rustbox") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.0.2") (d #t) (k 0)) (d (n "gag") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.6") (d #t) (k 0)))) (h "1aaifkqcr217dvpl807pv11wm6hs3znfvg17md26j56wd8i8rgzb")))

(define-public crate-rustbox-0.6.0 (c (n "rustbox") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.0.2") (d #t) (k 0)) (d (n "gag") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.6") (d #t) (k 0)))) (h "167c8a2xjqj2vldzw00r34m8wf0yix8k3s70wc3l2m6gd02l6xqd")))

(define-public crate-rustbox-0.6.1 (c (n "rustbox") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.1.0") (d #t) (k 0)) (d (n "gag") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.6") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "142ylq6g7cd803pzwzz9vldnw51f968kznxv60x276sawysajpza")))

(define-public crate-rustbox-0.6.2 (c (n "rustbox") (v "0.6.2") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0d38b29m5w95kxn6s826kcg9qpk3sylrllsf7x5p63mrk36p7qpa")))

(define-public crate-rustbox-0.6.3 (c (n "rustbox") (v "0.6.3") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1h1ykg71cgidb5070kxkggpcyx75cv3mdnl9mbs4cyfy950907av")))

(define-public crate-rustbox-0.7.0 (c (n "rustbox") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.7") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "18clj6q45lkp9g5fjifjrq5fzcdqzg3a6izi6822ncq85fa15q0c")))

(define-public crate-rustbox-0.7.1 (c (n "rustbox") (v "0.7.1") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.8") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0xbw5nmpxm83hsahi3gckqa5l8fni74md3ycws3kbwpdbvclpj2h")))

(define-public crate-rustbox-0.7.2 (c (n "rustbox") (v "0.7.2") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.8") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1aj2kr4gkvlsjhd6shrzjw103mnx0an656cdp6z1zckqxj5gjqmv")))

(define-public crate-rustbox-0.8.0 (c (n "rustbox") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.13") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.8") (d #t) (k 0)))) (h "0637y3i1ji0bilbv51wrdasgkkcqkpk2kvaricnabsb3nza9bc97")))

(define-public crate-rustbox-0.8.1 (c (n "rustbox") (v "0.8.1") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.13") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.9") (d #t) (k 0)))) (h "0nlpmmxjcjsqjbyd0zs9svwpp9da6bp2b33sd1kxpjnh627pmnv7")))

(define-public crate-rustbox-0.9.0 (c (n "rustbox") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.13") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.9") (d #t) (k 0)))) (h "1n1y817hapmi7kn2rpzjk07bsvpb7l82101mzx3jspkc1wwhzi5c")))

(define-public crate-rustbox-0.10.0 (c (n "rustbox") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.13") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.9") (d #t) (k 0)))) (h "1lyc6m9052xbava8944qqyx33nvh40k5iyi66c1vp0k8w79p885i")))

(define-public crate-rustbox-0.11.0 (c (n "rustbox") (v "0.11.0") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.13") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.9") (d #t) (k 0)))) (h "1cahyxncijdwvy9kw87ahizpfbdq76hf333y4nrhbxzssajhdzcf")))

