(define-module (crates-io ru st rust_ipify) #:use-module (crates-io))

(define-public crate-rust_ipify-1.0.0 (c (n "rust_ipify") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1ckkk0ygsra7dwifds4aby2ynrrkkk90h0w5mp7ng2jcj4vxr8gd")))

(define-public crate-rust_ipify-1.0.1 (c (n "rust_ipify") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "078q6gqhjml7143jm47d3gmzzhw8yhzgsnp4sbjnbp5nf0mfvaj8")))

(define-public crate-rust_ipify-1.0.2 (c (n "rust_ipify") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0fy236ksyjhw0nlk3llia394y6rkamr0ql9r99ic8dmgmnlan0rl")))

(define-public crate-rust_ipify-1.1.0 (c (n "rust_ipify") (v "1.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0gknrbm6rh0qsnvl975n4l6admmskhr48czb1nczp2n5l7bl21hh")))

