(define-module (crates-io ru st rust-libretro-proc) #:use-module (crates-io))

(define-public crate-rust-libretro-proc-0.1.0 (c (n "rust-libretro-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0rl9kgkfagpdwxnv9b6g5d229vl1b0610dznwip183f34sbsz1fd")))

(define-public crate-rust-libretro-proc-0.1.1 (c (n "rust-libretro-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0dlsgxyfr9x3gj75kzqbmb4i9lil49z21c2mld7qgm9dx2s3kg2d")))

(define-public crate-rust-libretro-proc-0.1.4 (c (n "rust-libretro-proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1kcpvgkfm2h41qcqzingqnfcnfsvj3gp32l9byvpccx2znxqrxgl")))

(define-public crate-rust-libretro-proc-0.1.5 (c (n "rust-libretro-proc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "066m7vav2p5v1bc6wg50n8131iva30xlh804hbdw8v1p5bjy42mn")))

(define-public crate-rust-libretro-proc-0.2.3 (c (n "rust-libretro-proc") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1jz69csl1j5sgl5lzh41sgl3h0i5w0hky8nrlgd69gxg3zd8dd4z")))

(define-public crate-rust-libretro-proc-0.3.0 (c (n "rust-libretro-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1rrzsjxn8a0jnfryvgx02aba6fyf1193vwcydqslwr8q1h8j341j")))

(define-public crate-rust-libretro-proc-0.3.1 (c (n "rust-libretro-proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "092kl0wwvinkz59y4qckvj2a2050cnr8740hnvpz65ipx3fhvz8r")))

(define-public crate-rust-libretro-proc-0.3.2 (c (n "rust-libretro-proc") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rust-libretro-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "18cr7kjqihv6xzyfgf1n1sgpb6w1njvvj5xmw73i1yqiykz9qphr")))

