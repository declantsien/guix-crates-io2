(define-module (crates-io ru st rusteval-derive) #:use-module (crates-io))

(define-public crate-rusteval-derive-0.1.0 (c (n "rusteval-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1cv87ds4ckmqlw3642gkddz6v132gr4qzvr3913whdviclcgxg67") (f (quote (("std") ("default" "std"))))))

(define-public crate-rusteval-derive-0.1.1 (c (n "rusteval-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "012sd4sgaym65djmz8l4fgdpx7zy746y0w0ycycs8w65adxv4maf") (f (quote (("std") ("default" "std"))))))

(define-public crate-rusteval-derive-0.2.0 (c (n "rusteval-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.104") (f (quote ("full"))) (d #t) (k 0)))) (h "05lrcx47rah9p3cyrq7vwc1b0bl0939f8878js866yh4p8ak2ip3") (f (quote (("std") ("default" "std"))))))

