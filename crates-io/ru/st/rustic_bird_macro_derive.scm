(define-module (crates-io ru st rustic_bird_macro_derive) #:use-module (crates-io))

(define-public crate-rustic_bird_macro_derive-0.1.0 (c (n "rustic_bird_macro_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "048a0gwp9y4y6cyz2dyy46yndrwald67k844c9p8id0cbk9r4s13") (y #t)))

(define-public crate-rustic_bird_macro_derive-0.1.1 (c (n "rustic_bird_macro_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xlcgc2963hjga217dbrv5177vdqpa00j1m6l2abwh01zjvxbg7a") (y #t)))

(define-public crate-rustic_bird_macro_derive-0.1.2 (c (n "rustic_bird_macro_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xs2m5231pg71hi7kg4j71ylv00a6mqh08abfa7bzg2h6p90vjp2")))

