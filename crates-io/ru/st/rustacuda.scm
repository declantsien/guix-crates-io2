(define-module (crates-io ru st rustacuda) #:use-module (crates-io))

(define-public crate-rustacuda-0.1.0 (c (n "rustacuda") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cuda-sys") (r "^0.2") (d #t) (k 0)) (d (n "rustacuda_core") (r "^0.1.0") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1.0") (d #t) (k 0)))) (h "02nhclq8866xh2y95ynjyangxvpi96jbwlazn6qj1crlfjm3b681")))

(define-public crate-rustacuda-0.1.1 (c (n "rustacuda") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cuda-sys") (r "^0.2") (d #t) (k 0)) (d (n "rustacuda_core") (r "^0.1.1") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1.1") (d #t) (k 0)))) (h "14bfnsqm9rfi1npf52djpjabydrcplr3zzlmccqcmn4v47sisfk5")))

(define-public crate-rustacuda-0.1.2 (c (n "rustacuda") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cuda-sys") (r "^0.2") (d #t) (k 0)) (d (n "rustacuda_core") (r "^0.1.1") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1.1") (d #t) (k 0)))) (h "03lhywwj0k0m44p8x3876l5y3j31pakkfays9p2ksy55yr010yf0")))

(define-public crate-rustacuda-0.1.3 (c (n "rustacuda") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cuda-driver-sys") (r "^0.3") (d #t) (k 0)) (d (n "rustacuda_core") (r "^0.1.2") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0a726453p29dfkvyliq3i3nd01glmq7fjq1mss9baf2kmcb8a827")))

