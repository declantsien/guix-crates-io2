(define-module (crates-io ru st rust_release_channel) #:use-module (crates-io))

(define-public crate-rust_release_channel-0.1.0 (c (n "rust_release_channel") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2.0") (d #t) (k 0)))) (h "0v35qfyajwpsrpxhadv0qx1slz429r10r0lmw0w19393s0mjhn9z")))

(define-public crate-rust_release_channel-0.2.0 (c (n "rust_release_channel") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2.0") (d #t) (k 0)))) (h "1lhsycgwjyirsg1h51w434js8mb0b803nx77ng71n32j7l3yskd8")))

(define-public crate-rust_release_channel-0.3.0 (c (n "rust_release_channel") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2.0") (d #t) (k 0)))) (h "1nci688gmj9frmvbpif3bhyrl7rf09nvrwx8d3qr4zjgr8x5m3jx")))

