(define-module (crates-io ru st rust-snowflake) #:use-module (crates-io))

(define-public crate-rust-snowflake-0.1.0 (c (n "rust-snowflake") (v "0.1.0") (h "0fpqnzsghaa9acd371m0crwcc67qc0k4ccphnizqn3zlx4i3g1hc")))

(define-public crate-rust-snowflake-0.1.1 (c (n "rust-snowflake") (v "0.1.1") (h "0zy4fmg9dxw296zr5125wb2zb1vgz7c2p5cmhf1q9kykqq5xldga")))

(define-public crate-rust-snowflake-0.1.2 (c (n "rust-snowflake") (v "0.1.2") (h "122cmrq9d27bpwcc0gn1k9rghfb3agzz3zy0r2nw3laq647k2awv")))

(define-public crate-rust-snowflake-0.1.3 (c (n "rust-snowflake") (v "0.1.3") (h "08l7snnq8zrl4x6ylvp41by201jwch8m012gp4sx9iavh9khar5r")))

