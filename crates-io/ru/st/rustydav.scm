(define-module (crates-io ru st rustydav) #:use-module (crates-io))

(define-public crate-rustydav-0.1.0 (c (n "rustydav") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "115zz28x4l9b0mp92fsy5zx2b03qnk32mfajw1ll6izsx5mmwfnn")))

(define-public crate-rustydav-0.1.1 (c (n "rustydav") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "07x42v8h6ynbr7bav518d05745vn501mgbgy3w814b4j4qzb00z9")))

(define-public crate-rustydav-0.1.2 (c (n "rustydav") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1mmi5whna3rdlajwlzg1zn788hi5ki92q0vv7sxy3s72a745vd8h")))

(define-public crate-rustydav-0.1.3 (c (n "rustydav") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0hkf3drpc16n9g3gcyxfyck8gjpljc76311hazy8pb16f728ck5w")))

