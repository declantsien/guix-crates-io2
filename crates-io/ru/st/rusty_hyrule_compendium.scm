(define-module (crates-io ru st rusty_hyrule_compendium) #:use-module (crates-io))

(define-public crate-rusty_hyrule_compendium-0.1.0 (c (n "rusty_hyrule_compendium") (v "0.1.0") (d (list (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1rkzydf5x4ic7wrrwvvrra9qyfz45nx0d0jvm2xxpkmarq6saish")))

(define-public crate-rusty_hyrule_compendium-0.1.1 (c (n "rusty_hyrule_compendium") (v "0.1.1") (d (list (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "08mlmv0yhcgai36cv7yzpfjfpkmf9xhmrmf33r8xxgplb10kgbm0")))

(define-public crate-rusty_hyrule_compendium-0.1.2 (c (n "rusty_hyrule_compendium") (v "0.1.2") (d (list (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0y0xa9kdb3b695pjqpbhf3z955k2drjf72zaw0idlgri46325d87")))

(define-public crate-rusty_hyrule_compendium-0.1.3 (c (n "rusty_hyrule_compendium") (v "0.1.3") (d (list (d (n "mockito") (r "^0.31.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1hh0d43yvm54695wy82917xrl6idf3f5pgv7jj2adm2inkj4zw86")))

