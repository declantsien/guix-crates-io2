(define-module (crates-io ru st rustygram) #:use-module (crates-io))

(define-public crate-rustygram-0.1.0 (c (n "rustygram") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "128fkmdpsiyz7lwrjnjbq94j20ri2hw4k5bc1qa8p1c8rfqvcs9n")))

(define-public crate-rustygram-0.1.1 (c (n "rustygram") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1g0rx17dys0agz1y00zkd6njyyqb775jal7n0v6h07rdv0ghsjjr")))

(define-public crate-rustygram-0.1.2 (c (n "rustygram") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0zdnvck1v96ih58xbxr1gn3qswsicalc5xlyx3xv52c2f37qay79")))

(define-public crate-rustygram-0.1.3 (c (n "rustygram") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0dvmbdbzpa9jh52xq9dmfjffjzgzhlp5ybys1ybdcid5kds1xc46")))

(define-public crate-rustygram-0.1.4 (c (n "rustygram") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1z8j62cj33qh67s2iihsqi1s65px76qh2sf87h3lgh8g7xk7jx88")))

