(define-module (crates-io ru st rustls-cert-file-reader) #:use-module (crates-io))

(define-public crate-rustls-cert-file-reader-0.1.0 (c (n "rustls-cert-file-reader") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-cert-read") (r "^0.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)))) (h "04m5ddfr8b20d5xxv50134a2i51i4xwidra7di80pxs0if1hc1nq")))

(define-public crate-rustls-cert-file-reader-0.1.1 (c (n "rustls-cert-file-reader") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-cert-read") (r "^0.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)))) (h "1ibsgg8yl3250idx3945j0qkrsb4szq3vxxs60dip0ngp920ss53")))

(define-public crate-rustls-cert-file-reader-0.2.0 (c (n "rustls-cert-file-reader") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rustls-cert-read") (r "^0.2") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)))) (h "12zz9ay985k1az31dgrw53xirgr3c1qr0h403lfskk3an3phg4i4")))

(define-public crate-rustls-cert-file-reader-0.3.0 (c (n "rustls-cert-file-reader") (v "0.3.0") (d (list (d (n "rustls-cert-read") (r "^0.3") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)))) (h "12n5yrpfzrwn4kkv5gh0vdbn3imfywcp4r0ddgmj0f7pplrabzy4")))

(define-public crate-rustls-cert-file-reader-0.4.0 (c (n "rustls-cert-file-reader") (v "0.4.0") (d (list (d (n "rustls-cert-read") (r "^0.3") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)))) (h "04c0khz9jiadwajldsdgfsrkxsiy314bivrqdraz5idwfsnv2zxh")))

