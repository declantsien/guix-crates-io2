(define-module (crates-io ru st rust_json_derive) #:use-module (crates-io))

(define-public crate-rust_json_derive-0.1.0 (c (n "rust_json_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jlg2rha6lg9b4yc6i5gw8n7a9mcrqza0wlv09l73c0ifyvs4h96")))

(define-public crate-rust_json_derive-0.1.1 (c (n "rust_json_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1axcyfxcbrvid0gp843dxgm4j992n2zaidg4bxr1a182xbwypm7p")))

(define-public crate-rust_json_derive-0.1.2 (c (n "rust_json_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jdq14b37s975j39ps8kdrsklgdgnzyaym4mgsy3a9264afm19b3")))

