(define-module (crates-io ru st rustyspanner) #:use-module (crates-io))

(define-public crate-rustyspanner-0.1.0 (c (n "rustyspanner") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "grpcio") (r "^0.3.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0.1") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.3.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1b3yrc850cl229s6sqfx1k72ywzrqn0x7qkcd0m498anskfb3wrv")))

(define-public crate-rustyspanner-0.1.1 (c (n "rustyspanner") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "grpcio") (r "^0.3.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.0.1") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.3.0") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0g1yn39acbxmiapvamklms0ksr5xsq1scydcknj145a8np1am08k")))

