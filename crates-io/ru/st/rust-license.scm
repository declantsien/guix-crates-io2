(define-module (crates-io ru st rust-license) #:use-module (crates-io))

(define-public crate-rust-license-0.1.0 (c (n "rust-license") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rn836kma9979gj2gdj1qkhjvhhijkq2q769xc253ais9g7qliyf")))

