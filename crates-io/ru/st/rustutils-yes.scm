(define-module (crates-io ru st rustutils-yes) #:use-module (crates-io))

(define-public crate-rustutils-yes-0.1.0 (c (n "rustutils-yes") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "1fp35dii5la6bzqwnf0358fsrr410fvd1zqyc9ybc9922v60cncv")))

