(define-module (crates-io ru st rustler_bigint) #:use-module (crates-io))

(define-public crate-rustler_bigint-0.1.0 (c (n "rustler_bigint") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "rustler") (r "^0.25.0") (d #t) (k 0)))) (h "12qxgfrgcaj4k2982mn95mqwckp0y6z42dlrl9l7bxx587s0p6v0")))

