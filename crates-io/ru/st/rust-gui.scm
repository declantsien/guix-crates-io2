(define-module (crates-io ru st rust-gui) #:use-module (crates-io))

(define-public crate-rust-gui-0.1.0 (c (n "rust-gui") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_gui_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0hanp8p17jxw74akzd8m917ryifg67wsn5inil0f6hybbzzbdgjl")))

(define-public crate-rust-gui-0.1.1 (c (n "rust-gui") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_gui_macros") (r "^0.1.0") (d #t) (k 0)))) (h "12kb1l2fggmqz11w0gj8i0vksa0dfbds03vxx6fck15qw7zqjzn9")))

(define-public crate-rust-gui-0.2.0 (c (n "rust-gui") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "tokio") (r "^1.18.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "18iqd66365ijamniqch901mnfcsjkdwg3qr8w49fnpcmbsi94big")))

(define-public crate-rust-gui-0.3.0 (c (n "rust-gui") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rust_gui_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1xikb9sywgqxmhw11xa4m4q8kd08b4g0kyg4sy9f4k2ji3w98lbx")))

(define-public crate-rust-gui-0.3.1 (c (n "rust-gui") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rust_gui_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "041jrvfwkxm069j29vyb67cdb9j0ffydj5r4pjnhhrbr9r2034qv")))

