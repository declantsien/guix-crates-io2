(define-module (crates-io ru st rust-strings) #:use-module (crates-io))

(define-public crate-rust-strings-0.1.0 (c (n "rust-strings") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0yai2spmikx93rcc022s7293lb1n1yvll7wvk8rhmi194jdicxzi") (f (quote (("python_bindings" "pyo3") ("cli" "clap"))))))

(define-public crate-rust-strings-0.2.0 (c (n "rust-strings") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0m2qvrmiva2maaglvnc76gxqzixdpgwi15csprl5zwj21cjrh1is") (f (quote (("python_bindings" "pyo3") ("cli" "clap"))))))

(define-public crate-rust-strings-0.3.0 (c (n "rust-strings") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1v379h6771vsiy6lrpicm5spngmhz3h562xwvijp3h9k06ldb3mn") (f (quote (("python_bindings" "pyo3") ("cli" "clap"))))))

(define-public crate-rust-strings-0.4.0 (c (n "rust-strings") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0kclx306iylav3fccsx1r5wzhyz3prbr9jd7npy77kymgib1jwcd") (f (quote (("python_bindings" "pyo3") ("cli" "clap"))))))

(define-public crate-rust-strings-0.6.0 (c (n "rust-strings") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.9") (d #t) (k 2)))) (h "0xhlvbghbvvyxzmpipyw02lwwxrix9rj5y42jklsgc14xam5jxfi") (f (quote (("python_bindings" "pyo3") ("cli" "clap"))))))

