(define-module (crates-io ru st rusty-perm) #:use-module (crates-io))

(define-public crate-rusty-perm-0.1.0 (c (n "rusty-perm") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0svrjdnyvdlvr11pc71b8hzln6bqdyylr7v2rjymjh2vqlbhwjbb") (f (quote (("std") ("default" "std" "rand"))))))

(define-public crate-rusty-perm-0.2.0 (c (n "rusty-perm") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (o #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0rrgx9spwlphyllmm5kabxkrjryz9p3cz1y804xirja3mx5dq6d9") (f (quote (("std") ("default" "std" "rand"))))))

