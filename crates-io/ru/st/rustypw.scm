(define-module (crates-io ru st rustypw) #:use-module (crates-io))

(define-public crate-rustypw-0.1.1 (c (n "rustypw") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-argon2") (r "^0.5") (d #t) (k 0)) (d (n "rustyline") (r "^6.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wy2hyikhxrksi6prz6ddv14l9m91rdmrmcidffpiq6gyw9ac7k9")))

