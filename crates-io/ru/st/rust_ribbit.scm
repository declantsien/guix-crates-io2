(define-module (crates-io ru st rust_ribbit) #:use-module (crates-io))

(define-public crate-rust_RIBBIT-0.0.1 (c (n "rust_RIBBIT") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "07ycl4a6707k7r6v4aliypbxcmzpmfwa295hvvv5s33kppb2w1ik")))

