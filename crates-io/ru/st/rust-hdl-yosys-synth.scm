(define-module (crates-io ru st rust-hdl-yosys-synth) #:use-module (crates-io))

(define-public crate-rust-hdl-yosys-synth-0.1.0 (c (n "rust-hdl-yosys-synth") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.1") (d #t) (k 0)))) (h "1y00jfffhl85p6yaclnssy3z49c10g6k1fhr2228rjha5nd22mn8") (y #t)))

