(define-module (crates-io ru st rust-guile) #:use-module (crates-io))

(define-public crate-rust-guile-0.1.0 (c (n "rust-guile") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i7w55kkzlcjrv4i0b2c2sx9akn0fw8f2rf4j8jdd883ws1zrfjc") (y #t)))

(define-public crate-rust-guile-0.1.1 (c (n "rust-guile") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jp47r23li113mj9w65gns1navr38q2v7d2mixphfp9d2k11pnfa") (y #t)))

(define-public crate-rust-guile-0.1.2 (c (n "rust-guile") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0pj63qcnva38qhhxxq6z6nk7jkx6ii6clycx040y66yndfwllcfh")))

(define-public crate-rust-guile-0.1.3 (c (n "rust-guile") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "153jpf44xrm08ppjzvqr80vskw6kszhv7p84bzlrrbgv6mzd4mrc") (y #t)))

(define-public crate-rust-guile-0.1.4 (c (n "rust-guile") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1if6g4azjw631a5kgljkmzp2qx7sl3wc75gaxrjjiv8bdcnpg96n")))

(define-public crate-rust-guile-0.1.5 (c (n "rust-guile") (v "0.1.5") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s49b4n3346lxq9520yjjvsl56qxrzqj1qhjsnpljl45a588c2j2")))

(define-public crate-rust-guile-0.1.6 (c (n "rust-guile") (v "0.1.6") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rp6ajkhbxkig8z6w8qn6k11hh11h40zkxh4dqqpkajkh7irxmyh")))

