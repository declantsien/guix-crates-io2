(define-module (crates-io ru st rust_stock_analysis) #:use-module (crates-io))

(define-public crate-rust_stock_analysis-0.1.0 (c (n "rust_stock_analysis") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18cixqdm5kmvzw54yn6pslxmhckn1g8ivgcp2q43ybwbic084wss")))

(define-public crate-rust_stock_analysis-0.1.1 (c (n "rust_stock_analysis") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dj1d98dqy15fgwixm7x76ghbrq1jm8zc4j8cnhdg7psysyi7dx4")))

