(define-module (crates-io ru st rust_ecdsa) #:use-module (crates-io))

(define-public crate-rust_ecdsa-0.1.0 (c (n "rust_ecdsa") (v "0.1.0") (d (list (d (n "ec_core") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)))) (h "0a9d4mqj5l4db9x1ilxagz1nbpxyasgvgs4yg6mqyrmxvbac6ch4")))

