(define-module (crates-io ru st rust_lib_starter_sample) #:use-module (crates-io))

(define-public crate-rust_lib_starter_sample-0.0.1-beta.2 (c (n "rust_lib_starter_sample") (v "0.0.1-beta.2") (h "0lj65ckrx5ljr98p5zla7xbwibx9a9126fw34yl03jx50ws8ixi0")))

(define-public crate-rust_lib_starter_sample-0.0.1-beta.3 (c (n "rust_lib_starter_sample") (v "0.0.1-beta.3") (h "1k067l27dn0n5id6p9ar1hiwy27wb791y7khzcpg5fxmmsa2hh2v")))

(define-public crate-rust_lib_starter_sample-0.0.1-beta.4 (c (n "rust_lib_starter_sample") (v "0.0.1-beta.4") (h "1l3wv1xf5x41hwvxknc6ikyax695941mra9xsl12jnqg59hpi31k")))

(define-public crate-rust_lib_starter_sample-0.0.1-beta.5 (c (n "rust_lib_starter_sample") (v "0.0.1-beta.5") (h "0m358r87lw7813kzya79w75dgdwibrrv7w1czhvb96bfl4adckmx")))

(define-public crate-rust_lib_starter_sample-0.0.1-beta.6 (c (n "rust_lib_starter_sample") (v "0.0.1-beta.6") (h "01spq27nnqs63vgxlz2qxbcpq7az7184i0hz2w2m9d7bw6z4p6gp")))

(define-public crate-rust_lib_starter_sample-0.0.1-beta.7 (c (n "rust_lib_starter_sample") (v "0.0.1-beta.7") (h "0wppmsh9mr03a8dglh6qv3wn3hn3027vs3cxxkspdyaw58y9z01w")))

(define-public crate-rust_lib_starter_sample-0.0.1 (c (n "rust_lib_starter_sample") (v "0.0.1") (h "1gn1piw2l4kwf881di2g6b0dh2l8bj1pb0pfjhw4czx2dhkhwaa2")))

(define-public crate-rust_lib_starter_sample-0.0.2-beta.1 (c (n "rust_lib_starter_sample") (v "0.0.2-beta.1") (h "07wq77g4aqf9zvb4yb5v2saa4yl86v2njdijxgxlxvycrk4839vf")))

(define-public crate-rust_lib_starter_sample-0.0.2-beta.2 (c (n "rust_lib_starter_sample") (v "0.0.2-beta.2") (h "0xmidp12xk95i63vqkpkr7n6gayj4digkllh84ab0jf7lqwlymnl")))

(define-public crate-rust_lib_starter_sample-0.0.2-beta.3 (c (n "rust_lib_starter_sample") (v "0.0.2-beta.3") (h "19h4xkj3h68d5a1w00rrb3nz0s2jfk8xf9h066viipqsmkxj1b80")))

(define-public crate-rust_lib_starter_sample-0.0.2 (c (n "rust_lib_starter_sample") (v "0.0.2") (h "06g1b9dw5bg2nsf7p36psw416krh6qyavhz0247n418f9gfkm2zw")))

