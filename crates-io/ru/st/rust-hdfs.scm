(define-module (crates-io ru st rust-hdfs) #:use-module (crates-io))

(define-public crate-rust-hdfs-0.1.0 (c (n "rust-hdfs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1r3b66di3v7iv1qsq4i58yr4cj98mxcjsf5ijamby5wni0rk8q2i")))

