(define-module (crates-io ru st rustc-semver) #:use-module (crates-io))

(define-public crate-rustc-semver-0.1.0 (c (n "rustc-semver") (v "0.1.0") (h "1ky638azcvcgbg0y2b9899vivyhikaz767hamfdq3hqy2b0i2hyy")))

(define-public crate-rustc-semver-0.1.1 (c (n "rustc-semver") (v "0.1.1") (h "1kc3vd5dxrabgn0dacq2sv98qbvx5d4x4zklbyknvx8x9p5gxaim")))

(define-public crate-rustc-semver-1.0.0 (c (n "rustc-semver") (v "1.0.0") (h "19wwda248hsmy0xx80b481ng558ixbdbcp5kl1mcwhkl6n4ks9kq")))

(define-public crate-rustc-semver-1.1.0 (c (n "rustc-semver") (v "1.1.0") (h "116fsp2y72dnnvr4vjh6isqk28ajpbmbyyv22wk6k5pmxp3vvqav")))

