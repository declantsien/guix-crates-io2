(define-module (crates-io ru st rust2go-common) #:use-module (crates-io))

(define-public crate-rust2go-common-0.0.2 (c (n "rust2go-common") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03dszrrynwss3iqm4pkfnwli7szkhy90zd23yww4hmkqxg9gwg2m")))

(define-public crate-rust2go-common-0.1.0 (c (n "rust2go-common") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "07lmi69pvpxcgz2gm0n5p9zxnqlpq0xv3a8mj7ml1msnarcckm8x")))

(define-public crate-rust2go-common-0.2.0 (c (n "rust2go-common") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0p6122zwgs5mig1va0qkm48cc51cxf4m2751fcvls1aybqm7iisj")))

(define-public crate-rust2go-common-0.3.0 (c (n "rust2go-common") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0v0haziz11rhgxwpimll535r4brvvmdi6v37zpv04z77v7d1vjri")))

(define-public crate-rust2go-common-0.3.1 (c (n "rust2go-common") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0lnvimxidh19brv8c23gdv765l1a57ib2i7fsfwxxj050xpw07ng")))

(define-public crate-rust2go-common-0.3.2 (c (n "rust2go-common") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xnpbzx2ghhwy3m7ssh7pcpmr0fd7b57m6bxi63z55x3s7rdq0h6")))

(define-public crate-rust2go-common-0.3.3 (c (n "rust2go-common") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "02z0a4x4rqvyvj7jhji47y38bh1j4jinv342r4qzz093idpfwp0z")))

(define-public crate-rust2go-common-0.3.4 (c (n "rust2go-common") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0j4fflvcjms9xhfw8i6wdd4g7rqf5ysym80148zlfln7hyjsd8zd")))

(define-public crate-rust2go-common-0.3.5 (c (n "rust2go-common") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0k5z239vj9rnbgvcx6lqbyjs4gl04gzalj7mzqvqap5px6fqixfj")))

(define-public crate-rust2go-common-0.3.6 (c (n "rust2go-common") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1q8qnsbsycpb456sb5sx9js8dys2zg5lrz4x48zcjykrw7nxyv8a")))

