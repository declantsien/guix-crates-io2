(define-module (crates-io ru st rust_intfic) #:use-module (crates-io))

(define-public crate-rust_intfic-0.2.2 (c (n "rust_intfic") (v "0.2.2") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "10cyb6a8fg4v2mhpf1r12fkpsrhh65gg4y0fi64p3bqspqvgx999") (y #t)))

(define-public crate-rust_intfic-0.2.3 (c (n "rust_intfic") (v "0.2.3") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0x6mibj1k9xgqhkvx9zz6xrp7rcxqn799zr6izb90c3rbg17m3f4") (y #t)))

(define-public crate-rust_intfic-0.3.0 (c (n "rust_intfic") (v "0.3.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1glkwfhwnr09bqiq0k5jhnaivg1pb0iaf42720x03v0w8jqahq9m") (y #t)))

(define-public crate-rust_intfic-0.3.1 (c (n "rust_intfic") (v "0.3.1") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0ad5q650l5qsgliavij5gpb2x7irw3fbkrgmw9cy1hxgrk8ajg6k") (y #t)))

