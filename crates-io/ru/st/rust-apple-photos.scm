(define-module (crates-io ru st rust-apple-photos) #:use-module (crates-io))

(define-public crate-rust-apple-photos-0.1.0 (c (n "rust-apple-photos") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.18.0") (f (quote ("blob"))) (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1s3w500jf2l60ymxs39an12by36a3vy8jgaiz81lcglmrc8r65kp")))

