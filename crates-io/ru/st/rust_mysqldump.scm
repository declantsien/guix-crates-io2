(define-module (crates-io ru st rust_mysqldump) #:use-module (crates-io))

(define-public crate-rust_mysqldump-0.1.0 (c (n "rust_mysqldump") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0afsb8ljjil28llmw84gbvaf6hlwsvc3ksrb0vpdh02kpjj0k5w4")))

(define-public crate-rust_mysqldump-0.1.1 (c (n "rust_mysqldump") (v "0.1.1") (d (list (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "118h9hjrrsab9jjhr1g4wq8p0lk401f52dc7wbzl0904g8x7asiz")))

