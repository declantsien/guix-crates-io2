(define-module (crates-io ru st rust-shorcuts) #:use-module (crates-io))

(define-public crate-rust-shorcuts-0.1.0 (c (n "rust-shorcuts") (v "0.1.0") (h "0i24pifsq8hy3kppq9wbwvh4m43mwhp6jxzf9kkqp39x5k2ljc3i")))

(define-public crate-rust-shorcuts-0.1.1 (c (n "rust-shorcuts") (v "0.1.1") (h "15xd4m5f46hngdpflywli4ascxpzb6p6qw7fhvcbkr3lp1mi9d22")))

