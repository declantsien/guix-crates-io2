(define-module (crates-io ru st rust-log) #:use-module (crates-io))

(define-public crate-rust-log-0.1.0 (c (n "rust-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)))) (h "0hqqb3kxws0b9aw0gihrpa08hdmp5z7dyc57qdcafhq64fiwm8sx")))

(define-public crate-rust-log-0.2.0 (c (n "rust-log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)))) (h "0aca6nhibs119v3ilchpbxx89830xlp8z5ppfsmzg1yrylwwx890")))

(define-public crate-rust-log-0.2.1 (c (n "rust-log") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)))) (h "0p1xdzihhkwrns4hnrxjfiajzz09ds5iivq60pxhcycjr0mlh59s")))

