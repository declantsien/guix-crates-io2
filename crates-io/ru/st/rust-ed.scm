(define-module (crates-io ru st rust-ed) #:use-module (crates-io))

(define-public crate-rust-ed-0.1.0 (c (n "rust-ed") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "nom") (r "^2") (d #t) (k 0)) (d (n "nom-test-helpers") (r "^2.0.0") (d #t) (k 0)))) (h "1fls8nv5d1v2ashq7ji70kamqkg14kcgpmia53r722xg85pqpd7k")))

