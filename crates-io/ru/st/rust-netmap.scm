(define-module (crates-io ru st rust-netmap) #:use-module (crates-io))

(define-public crate-rust-netmap-0.0.1 (c (n "rust-netmap") (v "0.0.1") (h "155yvmas7vakv2xy2si5ind4qg35cv6n57jrcyaz1d6yx5r96hb6")))

(define-public crate-rust-netmap-0.0.2 (c (n "rust-netmap") (v "0.0.2") (h "1p8qy4gw190i557kjadv7lyy6mmrqd4ixzp53zlck8hidwi3fsxa") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-rust-netmap-0.0.3 (c (n "rust-netmap") (v "0.0.3") (h "1zzwxwp41x7rm0hvp5kc44xr6385b4l573m7cwpw8fnq51xd5nq8") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-rust-netmap-0.0.4 (c (n "rust-netmap") (v "0.0.4") (h "1cfc2ba8k3dlqnnx654md2n88jfg95f2s05hc04r2i2m8nwixkrg") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-rust-netmap-0.0.5 (c (n "rust-netmap") (v "0.0.5") (h "0hbca5cgvbv5njdk5l7k50qaw6yz1d5vrr90893k26lyggapr05m") (f (quote (("netmap_with_libs") ("default"))))))

(define-public crate-rust-netmap-0.0.7 (c (n "rust-netmap") (v "0.0.7") (h "1dw7zkj6knyvqnz064ghh0gv5pv4h2jnxyxlc45h32fk21qjlh33") (f (quote (("netmap_with_libs") ("default"))))))

