(define-module (crates-io ru st rust-wasm-dev-helper) #:use-module (crates-io))

(define-public crate-rust-wasm-dev-helper-0.1.2 (c (n "rust-wasm-dev-helper") (v "0.1.2") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0xiipv4rnradhnrg5z9wfnnl3fn8lkg0whs32ddj6fy1kwzlrgsf")))

