(define-module (crates-io ru st rust-mpfr) #:use-module (crates-io))

(define-public crate-rust-mpfr-0.1.0 (c (n "rust-mpfr") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.2.3") (d #t) (k 0)))) (h "1hiqx7pmhfh5myxn0qniavqlmz0smnrls7m04wihbx1vnknsga2d")))

(define-public crate-rust-mpfr-0.1.1 (c (n "rust-mpfr") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.2.3") (d #t) (k 0)))) (h "098nnsr7726hjh9zywmqmqf1ji4kgqbalj999xk72sp25qz06rl8")))

(define-public crate-rust-mpfr-0.1.2 (c (n "rust-mpfr") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rust-gmp") (r "*") (d #t) (k 0)))) (h "1qfsl35hkb18zcggrp1xlarn3jvnrbq1570fl7bvsbg9xq76nbqw")))

(define-public crate-rust-mpfr-0.1.3 (c (n "rust-mpfr") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rust-gmp") (r "*") (d #t) (k 0)))) (h "1jknyvp6hqgllniw28l5d33xfdcz9qqdgwpcxfm50jdlzzjpam30")))

(define-public crate-rust-mpfr-0.1.4 (c (n "rust-mpfr") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rust-gmp") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "0paplgv998xcz75kzfww7h86g37c297h6bijqf9pgvscz19m4958")))

(define-public crate-rust-mpfr-0.1.5 (c (n "rust-mpfr") (v "0.1.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rust-gmp") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "1f8wcdl6rmxipgbsbm8zj70n45n5bkzcz31g5angx7zlb102gql9")))

(define-public crate-rust-mpfr-0.1.6 (c (n "rust-mpfr") (v "0.1.6") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "rust-gmp") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "1r7wff7f1x6rqiz3z243m349djkg29h7427xxd2rh26asn5zr5xg")))

(define-public crate-rust-mpfr-0.1.7 (c (n "rust-mpfr") (v "0.1.7") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "rust-gmp") (r "~0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "1kfmg3j64xcpi9rqzqxpmn6vzk1a2gk8rdwifp27vv26zslsqgy5")))

