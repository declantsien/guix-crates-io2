(define-module (crates-io ru st rust-analyzer-salsa-macros) #:use-module (crates-io))

(define-public crate-rust-analyzer-salsa-macros-0.17.0-pre.3 (c (n "rust-analyzer-salsa-macros") (v "0.17.0-pre.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vyffl323l2gs9hd99ds7yv7g2ywcskh59ga2gpg54yjcs93b45i")))

(define-public crate-rust-analyzer-salsa-macros-0.17.0-pre.4 (c (n "rust-analyzer-salsa-macros") (v "0.17.0-pre.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d36mvjjqbns1y0904bfqag2c2syqy1hb88mpviav4im7y4b0wnv")))

(define-public crate-rust-analyzer-salsa-macros-0.17.0-pre.5 (c (n "rust-analyzer-salsa-macros") (v "0.17.0-pre.5") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bqyhip412il22d1rwvj8i49rmz15qkhpx46n3lk3bkzblw5y0x2")))

(define-public crate-rust-analyzer-salsa-macros-0.17.0-pre.6 (c (n "rust-analyzer-salsa-macros") (v "0.17.0-pre.6") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13jaiagkpifcf4ysivybbsljvi9pphp076f3frk8r144js74k5jd")))

