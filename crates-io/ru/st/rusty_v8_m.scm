(define-module (crates-io ru st rusty_v8_m) #:use-module (crates-io))

(define-public crate-rusty_v8_m-0.4.1 (c (n "rusty_v8_m") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "0z9d7h3gj2pfkz7lnlvbczdzl12bn4pg30lii46gk6shnpc310kg")))

(define-public crate-rusty_v8_m-0.4.2 (c (n "rusty_v8_m") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "1qqac6z96gp4ihbmjh1ym2a7a9r3qrf2ffcwj6j430i2jdpy99lv")))

(define-public crate-rusty_v8_m-0.4.3 (c (n "rusty_v8_m") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "03w24rk75gv9z2ly5ccanijzzmw6ha4375cs9ys8a5kgnq6jjm84")))

(define-public crate-rusty_v8_m-0.4.4 (c (n "rusty_v8_m") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.26") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "118w237llw61fl8b218pz5djcyz7jbdn1v1k25pm5l1hibxkc6zb") (y #t)))

(define-public crate-rusty_v8_m-0.4.5 (c (n "rusty_v8_m") (v "0.4.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.26") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "06wjdqs5a9cxnq7dif05pb8889g8dhipzkwq9z8dl1c128gbvm0b") (y #t)))

(define-public crate-rusty_v8_m-0.4.6 (c (n "rusty_v8_m") (v "0.4.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "1q28251srx7kl3iv0v9pm13s8gmpch1p6i8i0x6v8shc631q2wws") (y #t)))

