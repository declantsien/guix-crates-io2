(define-module (crates-io ru st rust-raknet) #:use-module (crates-io))

(define-public crate-rust-raknet-0.1.0 (c (n "rust-raknet") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pq71bf4hbp54rvhaqh8mprgg2maphd3vc618n2lh0cid7bf6fzg") (y #t)))

(define-public crate-rust-raknet-0.1.1 (c (n "rust-raknet") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cxk2zhdcdcfmfazzvdfhx5aa8nzv94aqd5945l2mg63p0595n6i") (y #t)))

(define-public crate-rust-raknet-0.2.0 (c (n "rust-raknet") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h3lid87cjqpdk7zjj02fb8cdik5acjmdb46fga4k63czhjyxp90") (y #t)))

(define-public crate-rust-raknet-0.3.0 (c (n "rust-raknet") (v "0.3.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1045hdn1sqypf8zsksh24ba320m22vc5y8ys3w5b25vzyw1dgd7r") (y #t)))

(define-public crate-rust-raknet-0.3.1 (c (n "rust-raknet") (v "0.3.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08dsvxx00k62smzvkfl1jw5rpxixfcz40c6xh2wdjfi5vi8kz82g") (y #t)))

(define-public crate-rust-raknet-0.4.0 (c (n "rust-raknet") (v "0.4.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qc4qzs2xpap2cm719fg5nzgr1m6khzphm36vinflfmsajk5cdfk") (y #t)))

(define-public crate-rust-raknet-0.5.0 (c (n "rust-raknet") (v "0.5.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14683955y2i5ksnsk1qqndf8xphzrkkdk30acvgvrwy45dw8klcv") (y #t)))

(define-public crate-rust-raknet-0.5.1 (c (n "rust-raknet") (v "0.5.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j3rqp0fjhhbyjbkxz62zbqxlbcf2b9q6rqd9jgky7q834i32gpp") (y #t)))

(define-public crate-rust-raknet-0.6.0 (c (n "rust-raknet") (v "0.6.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c26y8pql0vwnrmb8alpv8l8hnzghfj7597g0lxfx5lnw5qjsnqp") (y #t)))

(define-public crate-rust-raknet-0.7.0 (c (n "rust-raknet") (v "0.7.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19mg2f359f4ibf9ffs4mn4mrhagi0mpbnpqd664abah4zrn4qjin") (y #t)))

(define-public crate-rust-raknet-0.7.1 (c (n "rust-raknet") (v "0.7.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vr61xa4dm77irlf0xx7pcyqg13dhr7sy25sq6wvxakacip78b3c") (y #t)))

(define-public crate-rust-raknet-0.8.0 (c (n "rust-raknet") (v "0.8.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0ay2vkcp7yr8k2qnjbz284jqw8lq5lf3vdipf5xbranib5h9f0") (y #t)))

(define-public crate-rust-raknet-0.9.0 (c (n "rust-raknet") (v "0.9.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l894kncy8278ss9vv3iailiw8fwyw0yk5aazl1q0fbidzs71fxn") (y #t)))

(define-public crate-rust-raknet-0.10.0 (c (n "rust-raknet") (v "0.10.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a44c8z3wimjyi1hzrghh1qmkydp982smaiyqkmxxwcf4lv0qh68") (y #t)))

(define-public crate-rust-raknet-0.10.1 (c (n "rust-raknet") (v "0.10.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vdpg3h09bnfr58c4d37mmmqrjp65vihpl4fz8c28illbfn1c8qx") (y #t)))

(define-public crate-rust-raknet-0.11.0 (c (n "rust-raknet") (v "0.11.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a3vpzq01pdl74lz141gvq5agy6n79f3n3wxf8h2vp9dp8zf439b") (y #t)))

(define-public crate-rust-raknet-0.12.0 (c (n "rust-raknet") (v "0.12.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cdili7jp2l4xb1j2cjp4p6156g9jjhf5hsprgj8f2cnzay2lb63")))

