(define-module (crates-io ru st rustc_apfloat) #:use-module (crates-io))

(define-public crate-rustc_apfloat-0.0.0 (c (n "rustc_apfloat") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "07a70cvyipxay6syn8p4vqc9kwl93ps1kr6zmwinwh6nq3j5qqxf")))

(define-public crate-rustc_apfloat-0.1.0 (c (n "rustc_apfloat") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1k1nzbg3pcj6crx0f7c6jrvd64rcsmqwzqknnp758ilnhqpi48j0") (f (quote (("std") ("default" "std"))))))

(define-public crate-rustc_apfloat-0.1.1 (c (n "rustc_apfloat") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "083p4iiw1jm13q4scxixpikcyldk50wib04lmvc5i866fd927wlp") (f (quote (("std") ("default" "std"))))))

(define-public crate-rustc_apfloat-0.1.2 (c (n "rustc_apfloat") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "09jxhr3jvplqnqrpm00wqcin76s3y4qg66fzy9m7ifdqj0670r1b") (f (quote (("std") ("default" "std"))))))

(define-public crate-rustc_apfloat-0.1.3 (c (n "rustc_apfloat") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "04h5lmi5vmz3jqdbcspvwn9rgyy8ffkvqhzpq4bzkv2ri16p7v8d") (f (quote (("std") ("default" "std"))))))

(define-public crate-rustc_apfloat-0.2.0+llvm-462a31f5a5ab (c (n "rustc_apfloat") (v "0.2.0+llvm-462a31f5a5ab") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (f (quote ("const_generics" "union"))) (d #t) (k 0)))) (h "1gln8viw587hfsp9m2d097jzqa1ny26h1zk9dxbfx99k41vqfla6")))

