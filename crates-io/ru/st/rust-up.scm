(define-module (crates-io ru st rust-up) #:use-module (crates-io))

(define-public crate-rust-up-0.0.1 (c (n "rust-up") (v "0.0.1") (h "1ajig6hi1i2b134hy4wz94gkjhwxk01s8ani4a0vb5b21qk235ky")))

(define-public crate-rust-up-0.0.2 (c (n "rust-up") (v "0.0.2") (d (list (d (n "cargo-up") (r "^0.0.2") (d #t) (k 0)))) (h "1riy7kl9bgasc8za8nhnb81x18bzkm3w6sh7dbs8c8z617s4x7qq")))

(define-public crate-rust-up-0.0.3 (c (n "rust-up") (v "0.0.3") (d (list (d (n "cargo-up") (r "^0.0.3") (d #t) (k 0)))) (h "0974shhfhh7cdyhb5gm2w0b3lw5gq1v4nka9cjnj3mia5hb8mxiq")))

(define-public crate-rust-up-0.0.4 (c (n "rust-up") (v "0.0.4") (d (list (d (n "cargo-up") (r "^0.0.4") (d #t) (k 0)))) (h "0i80d5n4q45g0j9xz5rwis33d72dhpvn340lj5qdlgg8hcaz1i4j")))

(define-public crate-rust-up-0.0.6 (c (n "rust-up") (v "0.0.6") (d (list (d (n "cargo-up") (r "^0.0.6") (d #t) (k 0)))) (h "1sh5z37h063jl4b021sgmfpnzf4fdcmxfzgi36ghk3gn4qgnz5bd")))

