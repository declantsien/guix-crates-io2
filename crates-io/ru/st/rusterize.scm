(define-module (crates-io ru st rusterize) #:use-module (crates-io))

(define-public crate-rusterize-0.2.0 (c (n "rusterize") (v "0.2.0") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "1wrpikqjzq7hll67r9b710dp5mcwipdccsa0hnivy42xr8ff3z6v")))

(define-public crate-rusterize-0.3.0 (c (n "rusterize") (v "0.3.0") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "0w0djr527byy1y43xsf3n67w4jppslbq9ncx818ibcwasvkvxcbs")))

(define-public crate-rusterize-0.3.1 (c (n "rusterize") (v "0.3.1") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "0hwpnvg4p17v87bg1xi0q4znq3rcya1h3ywxbc3zxgq7cipd3r2z")))

(define-public crate-rusterize-0.3.2 (c (n "rusterize") (v "0.3.2") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "0cgvv1302yyip6jg62m8iyd65ld5g5k882y15i6frpdf5svnz7wc")))

(define-public crate-rusterize-0.3.3 (c (n "rusterize") (v "0.3.3") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "1q5gmkg68xscabpma107d47ynmh8v9drz0l4fxjjncyaihlxh3gd")))

(define-public crate-rusterize-0.3.4 (c (n "rusterize") (v "0.3.4") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "1vp5l8fkjikyad7vfn1cbw0vvgd4p831ncbpq26d41vbjw174hyv")))

(define-public crate-rusterize-0.3.5 (c (n "rusterize") (v "0.3.5") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "1yijl2m6ahiij210l3dkibdmn29c7012c191d4rxc7c5rwj0vbsz")))

(define-public crate-rusterize-0.3.6 (c (n "rusterize") (v "0.3.6") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "04x7kc9jasqhrygry684jvlv2fw6zcfw084iriwxkkbiszy8n62a")))

(define-public crate-rusterize-0.3.7 (c (n "rusterize") (v "0.3.7") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "1x1q9cghjyg38l45xbfpskwggbj0qvb4x0q1k0jsnd3bcw3i5cpg")))

(define-public crate-rusterize-0.3.8 (c (n "rusterize") (v "0.3.8") (d (list (d (n "sdl2") (r "^0.29") (d #t) (k 0)))) (h "0ymx5wfrzz22bw5ky304fj48aln7v4vbw444fwd35z4i494ibdcs")))

