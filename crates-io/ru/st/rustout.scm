(define-module (crates-io ru st rustout) #:use-module (crates-io))

(define-public crate-rustout-0.1.0 (c (n "rustout") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1i21w5z02vcw06n4653fdr7wz5bmz7dcarw8c6c3nvy7j5glp671")))

(define-public crate-rustout-0.1.1 (c (n "rustout") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x7ra3f067rc0929f9s0jpqc2hy99rv34wkwwpwk2lnnk3wc5m1b")))

(define-public crate-rustout-0.2.0 (c (n "rustout") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ljmchgqbk8grzfi0wlm8aw06v15z9dc1bprzxh4hsp3i5cbfgka")))

(define-public crate-rustout-0.2.1 (c (n "rustout") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00jfq6qfhhqpr6mq1pngdxy9v7plin7blgfxfrls4daqy29v0rl6")))

(define-public crate-rustout-0.3.0 (c (n "rustout") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0c5ap0f5g2xd34mrd8565z84zymmiarlg8xipav9pnps53j6zk2w")))

(define-public crate-rustout-0.4.0 (c (n "rustout") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "18wjkm6bkawbax4v4gsfdxz3w35yxfr852xqilbbnkbq710rhcbq")))

(define-public crate-rustout-0.4.1 (c (n "rustout") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1y3rafiicla55fcmplwn5ds4hjaqs1ng0kivpiswihi1ps933w6g")))

