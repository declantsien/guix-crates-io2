(define-module (crates-io ru st rust-lioness) #:use-module (crates-io))

(define-public crate-rust-lioness-0.1.2 (c (n "rust-lioness") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1v5k8frn38x83sif1nnq2iab4japbhg6nz4055g9wpvgymim9f2i")))

(define-public crate-rust-lioness-0.1.3 (c (n "rust-lioness") (v "0.1.3") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "00d86vnsgixif67mwmfwkw5sw605famfnk5hrjccyxvmngj3g0mj")))

(define-public crate-rust-lioness-0.1.4 (c (n "rust-lioness") (v "0.1.4") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "blake2b") (r "^0.7.0") (d #t) (k 0)) (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "keystream") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0l8alhzfh9rxcj809j0sm0rc0dg52ap2hn8pmbnmiiwhgdiajyzq")))

(define-public crate-rust-lioness-0.1.5 (c (n "rust-lioness") (v "0.1.5") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "blake2b") (r "^0.7.0") (d #t) (k 0)) (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "keystream") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "17b6hgjslhm7rkvw5xx355jb4a2samsyi3clk7snxkaaidq4wwgi")))

