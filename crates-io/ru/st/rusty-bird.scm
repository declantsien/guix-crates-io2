(define-module (crates-io ru st rusty-bird) #:use-module (crates-io))

(define-public crate-rusty-bird-0.0.1 (c (n "rusty-bird") (v "0.0.1") (h "17nzgi50hfjj8jhvyrb2ibslyp2njg1pywsybhh9xypgavdpw48l") (y #t)))

(define-public crate-rusty-bird-0.2.0 (c (n "rusty-bird") (v "0.2.0") (d (list (d (n "ggez") (r "^0") (d #t) (k 0)) (d (n "glam") (r "^0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "image") (r "^0") (d #t) (k 0)) (d (n "mint") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "064kx2yrxb9mi7s4878blw43794l59gadk2349p3hmwsv3plyqw2")))

