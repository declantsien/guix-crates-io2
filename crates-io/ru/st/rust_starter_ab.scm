(define-module (crates-io ru st rust_starter_ab) #:use-module (crates-io))

(define-public crate-rust_starter_ab-0.1.0 (c (n "rust_starter_ab") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rvdmi75cla3xl7v3ksi3bab7z7rb7yyxpjz5cwsbcq5dc28ib3c")))

