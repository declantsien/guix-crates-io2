(define-module (crates-io ru st rustbricks) #:use-module (crates-io))

(define-public crate-rustbricks-0.0.1 (c (n "rustbricks") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0csrd8nzp2f6n9h2199j4gpbdhcxq5f16a0hb2x4s6hnmqhdvrfk")))

(define-public crate-rustbricks-0.0.2 (c (n "rustbricks") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jak3pp0mpldf0j2r9xb63qyxdljh5m45fk87kxlqnxqqhly14lx")))

(define-public crate-rustbricks-0.0.3 (c (n "rustbricks") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zf3lv7g7j14man0gj1540qnzvqx2r1rzr8i7313mkz04fz2yfv5")))

(define-public crate-rustbricks-0.1.0 (c (n "rustbricks") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ya0vhmbljz6xxb8f678n86sy521i31p2j0lpwfhhbm8lg0wf5wk")))

(define-public crate-rustbricks-0.1.1 (c (n "rustbricks") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17bfifp4f9ss7nsy8d9f91mlg4plbvkk0xb9dgawiivcia1kggbx")))

