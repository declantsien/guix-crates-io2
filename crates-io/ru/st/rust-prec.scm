(define-module (crates-io ru st rust-prec) #:use-module (crates-io))

(define-public crate-rust-prec-0.8.0 (c (n "rust-prec") (v "0.8.0") (d (list (d (n "async-log-watcher") (r "^0.0.1") (d #t) (k 0)) (d (n "rcon") (r "^0.6.0") (f (quote ("rt-tokio"))) (k 0)) (d (n "steamlocate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "signal" "full"))) (d #t) (k 0)))) (h "16i7wcar9mfxw18zaryacr9kmvh7zd2frv6168qmy6zxla913f8x") (y #t)))

(define-public crate-rust-prec-1.0.0 (c (n "rust-prec") (v "1.0.0") (d (list (d (n "async-log-watcher") (r "^0.0.1") (d #t) (k 0)) (d (n "rcon") (r "^0.6.0") (f (quote ("rt-tokio"))) (k 0)) (d (n "steamlocate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "signal" "full"))) (d #t) (k 0)))) (h "05pxd4a6s6ki6c2hyzi2ri86bnjjclrch900cxmh7fbgcp2zbzkq") (y #t)))

(define-public crate-rust-prec-1.0.1 (c (n "rust-prec") (v "1.0.1") (d (list (d (n "async-log-watcher") (r "^0.0.1") (d #t) (k 0)) (d (n "rcon") (r "^0.6.0") (f (quote ("rt-tokio"))) (k 0)) (d (n "steamlocate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "signal" "full"))) (d #t) (k 0)))) (h "181qbkffx0l7x1i3hx04ajqf1wn33mamsl6x357463vxn413bw42") (y #t)))

(define-public crate-rust-prec-1.0.2 (c (n "rust-prec") (v "1.0.2") (d (list (d (n "async-log-watcher") (r "^0.0.1") (d #t) (k 0)) (d (n "rcon") (r "^0.6.0") (f (quote ("rt-tokio"))) (k 0)) (d (n "steamlocate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "signal" "full"))) (d #t) (k 0)))) (h "01xqsj00b3m8g71gyyxyidpyaqc6vdfns76knj8grm4y9vrc0zd4") (y #t)))

(define-public crate-rust-prec-1.0.3 (c (n "rust-prec") (v "1.0.3") (d (list (d (n "async-log-watcher") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rcon") (r "^0.6.0") (f (quote ("rt-tokio"))) (k 0)) (d (n "steamlocate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "signal" "full"))) (d #t) (k 0)))) (h "14c3xm7n1yndks99qh2lxl1lr42nzj5rwicgksm60l4sjrqjj1a2") (y #t)))

(define-public crate-rust-prec-1.0.4 (c (n "rust-prec") (v "1.0.4") (d (list (d (n "async-log-watcher") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rcon") (r "^0.6.0") (f (quote ("rt-tokio"))) (k 0)) (d (n "steamlocate") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "signal" "full"))) (d #t) (k 0)))) (h "0h80h7ikshl3l6dq4b55q6paj2cn7m9zl2sdy9418shhvf7q4zlj")))

