(define-module (crates-io ru st rustorm-cli) #:use-module (crates-io))

(define-public crate-rustorm-cli-0.1.0 (c (n "rustorm-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rustorm") (r "^0.15.2") (f (quote ("with-mysql"))) (d #t) (k 0)))) (h "0jqm164imxf4as8fi5gg0bfqgnl07gy3r8999br19n98g7g0w0wn")))

