(define-module (crates-io ru st rust-mnist) #:use-module (crates-io))

(define-public crate-rust-mnist-0.1.0 (c (n "rust-mnist") (v "0.1.0") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "rand") (r "~0.3") (d #t) (k 0)))) (h "0dnrrh52rgjrqjxmbvc0zasm0f2a0nfc0nyyp75p68l1njal8s75")))

(define-public crate-rust-mnist-0.1.1 (c (n "rust-mnist") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1xa7m9w7zzlcj39g5pdxflqacbdwvispn1vg5g3agd872hnjv2g7")))

(define-public crate-rust-mnist-0.1.2 (c (n "rust-mnist") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "00gz4dila5p3nankzczgd9sp54jnbsdb7da3561pnc04cn9nm5g5")))

(define-public crate-rust-mnist-0.1.3 (c (n "rust-mnist") (v "0.1.3") (d (list (d (n "criterion") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0n506fnifs5lwa0c0x3jf3k6ilwq6drch3fg01nw88shy62pinr0")))

(define-public crate-rust-mnist-0.1.4 (c (n "rust-mnist") (v "0.1.4") (d (list (d (n "criterion") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0s2aq99xfwwycjc3a15w7gca3aszhwgwq57rw6gmsmnj6dcg0rpx")))

(define-public crate-rust-mnist-0.2.0 (c (n "rust-mnist") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04irilh7j3l0cmkh8mcbr4lnlj0v703ga9vqam5n3i2lwwy620gp")))

