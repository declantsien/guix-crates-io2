(define-module (crates-io ru st rustutil) #:use-module (crates-io))

(define-public crate-rustutil-0.1.0 (c (n "rustutil") (v "0.1.0") (h "1hl2v8vjgpb9h01bkk04c94g4a2nl8ayn93i4sw5rix9i5jpwhfx")))

(define-public crate-rustutil-0.1.1 (c (n "rustutil") (v "0.1.1") (h "0v7sx3aq2gn3yisxd16yfcibp8pyl281qk3k95y738959fr74sag")))

(define-public crate-rustutil-0.1.2 (c (n "rustutil") (v "0.1.2") (h "17fqbsindgf3g5zj0byxixhy676x2azrj9i0xxbbiq3qdxln6n27")))

