(define-module (crates-io ru st rust-template-generated-lib) #:use-module (crates-io))

(define-public crate-rust-template-generated-lib-0.1.0 (c (n "rust-template-generated-lib") (v "0.1.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "12bnbnfqxzyf6vcprv2vlik6hyspk4qcgkj21a3x7x42w9g7ii4i") (r "1.56.1")))

(define-public crate-rust-template-generated-lib-0.2.0 (c (n "rust-template-generated-lib") (v "0.2.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)))) (h "13i6zv8x6rkswrnl5zmwf19yrgafgil1mx0y3ichyb34h7k8x3q1") (r "1.59.0")))

(define-public crate-rust-template-generated-lib-0.2.1 (c (n "rust-template-generated-lib") (v "0.2.1") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)))) (h "0jr114hmzln63xaxq56m6ixbjddrg0bb66qmywmh379zzfljkpjg") (r "1.59.0")))

(define-public crate-rust-template-generated-lib-0.2.2 (c (n "rust-template-generated-lib") (v "0.2.2") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)))) (h "1j1pk89p7lid990npdhgriq3fjfv416nqsfzwlyc5dqzn91r8mam") (r "1.59.0")))

(define-public crate-rust-template-generated-lib-0.2.3 (c (n "rust-template-generated-lib") (v "0.2.3") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)))) (h "02vig7fwnngxn2pj978ba1yi4cw7d477zvr78kwgy6rrd98kx374") (r "1.59.0")))

(define-public crate-rust-template-generated-lib-0.2.4 (c (n "rust-template-generated-lib") (v "0.2.4") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)))) (h "0hvl11x2c8qv4rc8c6ady4mr0paxnmhfxl9v95arax2z632ikb8f") (r "1.59.0")))

