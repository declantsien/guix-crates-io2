(define-module (crates-io ru st rust_slim) #:use-module (crates-io))

(define-public crate-rust_slim-0.1.0 (c (n "rust_slim") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "rust_slim_macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "slim_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zgd3ymgkk9fdxra350ywnkmhjgdcx9a341308jrvk5z90csd0wy") (s 2) (e (quote (("macros" "dep:rust_slim_macros"))))))

(define-public crate-rust_slim-0.1.1 (c (n "rust_slim") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "rust_slim_macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "slim_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "005s424khwwqj4vqligf6m72wf17hmcwqsmbdcwh9qni95haclay") (s 2) (e (quote (("macros" "dep:rust_slim_macros"))))))

(define-public crate-rust_slim-0.1.2 (c (n "rust_slim") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "rust_slim_macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "slim_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y6jpzwh3sciw2bw8v7ilwh9hs71lawmx4nz73xhz8mp1iz2zmcj") (s 2) (e (quote (("macros" "dep:rust_slim_macros"))))))

(define-public crate-rust_slim-0.2.2 (c (n "rust_slim") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "rust_slim_macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "slim_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ppaww0k53fn4ihjgn17fkhl6jf7pnwv617yss5m7dn8ks9agsrr") (s 2) (e (quote (("macros" "dep:rust_slim_macros"))))))

