(define-module (crates-io ru st rustutils-tee) #:use-module (crates-io))

(define-public crate-rustutils-tee-0.1.0 (c (n "rustutils-tee") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dsfb31ga3bmhzg7b9fdszh602p79djg5wb924jfxbb7kxhg5p4s")))

