(define-module (crates-io ru st rust-lib-template) #:use-module (crates-io))

(define-public crate-rust-lib-template-0.1.0 (c (n "rust-lib-template") (v "0.1.0") (h "0kfy0yhqpj7kjn6gld1r4jcq3map0iv0vfvpa3nvqq758yda0jvr")))

(define-public crate-rust-lib-template-0.1.1 (c (n "rust-lib-template") (v "0.1.1") (h "1nzk5hajx284var64l3ackg53i2cwyah3vzcfrmxw5cjphn20ij5") (r "1.68")))

(define-public crate-rust-lib-template-1.0.0 (c (n "rust-lib-template") (v "1.0.0") (h "1fs6na37z3afavyv49xngmklbhy3150bm3020lbisjyg1khjqsyl") (r "1.68")))

(define-public crate-rust-lib-template-1.0.1 (c (n "rust-lib-template") (v "1.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1dpc3fwp923ds5nrkpngxwhf84z8xczzhg177zkwllknwqg9i36r") (r "1.68")))

(define-public crate-rust-lib-template-1.0.2 (c (n "rust-lib-template") (v "1.0.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1zldn64m1ckxl570bnfppgrx4dcaxyr80v7nvvw235vr8085davn") (r "1.68")))

(define-public crate-rust-lib-template-1.0.3 (c (n "rust-lib-template") (v "1.0.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0qnwgd4j3jrjcwcf6y66g3wwwlszqdh2ncrzh077zivi1bz3303z") (r "1.68")))

