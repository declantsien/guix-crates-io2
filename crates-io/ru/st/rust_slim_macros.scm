(define-module (crates-io ru st rust_slim_macros) #:use-module (crates-io))

(define-public crate-rust_slim_macros-0.1.0 (c (n "rust_slim_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1cgbc5gmvha35w42a80radpsydx7krmv3sv4rhwv0m909d227sak")))

