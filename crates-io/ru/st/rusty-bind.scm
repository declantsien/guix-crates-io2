(define-module (crates-io ru st rusty-bind) #:use-module (crates-io))

(define-public crate-rusty-bind-0.1.0 (c (n "rusty-bind") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rusty-bind-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0flls09bd8yhlb3xgw93i6n7jfrh49igy71rc22640q81r7mhcq6")))

(define-public crate-rusty-bind-0.1.1 (c (n "rusty-bind") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "12m4kaikpnjsx0xrldymwmr1lfrg36id6q0c14hsjkphfqim8ha4")))

(define-public crate-rusty-bind-0.1.2 (c (n "rusty-bind") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0h92amd960vl15lz9waj923yq8f12hzj0lc24jdx6rdgicxzykqk")))

(define-public crate-rusty-bind-0.1.3 (c (n "rusty-bind") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0iz5621v9k7r56c3r2s4350w1a8hhsg2mza1b6fvkvl2mz7gl6qm")))

(define-public crate-rusty-bind-0.2.0 (c (n "rusty-bind") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0snvknvd02hv8h490krpwq4a8hk246727ql87nhd4qxvpc89vgvw")))

(define-public crate-rusty-bind-0.2.1 (c (n "rusty-bind") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0lvjz16zvxf7127hy8ajilyicxr1416j2f2ppc50nxda0mn81q59")))

(define-public crate-rusty-bind-0.3.0 (c (n "rusty-bind") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0a1zmf144aqy2zkmvahi2dhaacja7ng218lcyarfdq8dw22mcg7x")))

(define-public crate-rusty-bind-0.3.1 (c (n "rusty-bind") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1y2x9m9xxlpfq2p4ydyz3i4rc7k2s0hmp8ry2n5farp1l6fzhasc")))

(define-public crate-rusty-bind-0.3.2 (c (n "rusty-bind") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1byvri6424n5b5p8h6agy1sq2sbdsr7zm61frh42zrj7wi0xzi4f")))

(define-public crate-rusty-bind-0.3.3 (c (n "rusty-bind") (v "0.3.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1v94hf2a16mlw5kblrp2rpmi5njmc884s721hhd24hd87bxx0zfl")))

(define-public crate-rusty-bind-0.3.4 (c (n "rusty-bind") (v "0.3.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "05msddbmnjs6bcrzapp98avknlpdyjl5vd33vi1szy0xsgcxwifg")))

(define-public crate-rusty-bind-0.3.5 (c (n "rusty-bind") (v "0.3.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1h45pdr1bl5021lygwwh11gkb33ndpcbqpjf2sggd44446qw8s40")))

(define-public crate-rusty-bind-0.3.6 (c (n "rusty-bind") (v "0.3.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1jank8d4813s4vqx9ixlb631z051vv5rxc0impg57yqrr7ap9j1g")))

(define-public crate-rusty-bind-0.3.7 (c (n "rusty-bind") (v "0.3.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1v57yaghkvw9jfb1vygzicvfjgy7n0ykrmy6mfnnzgf3vpwxzgh6")))

