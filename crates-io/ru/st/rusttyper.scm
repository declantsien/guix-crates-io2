(define-module (crates-io ru st rusttyper) #:use-module (crates-io))

(define-public crate-rusttyper-0.3.0 (c (n "rusttyper") (v "0.3.0") (d (list (d (n "glium") (r "^0.24") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 0)))) (h "19j98ricv7gsy66sca1f7a4px9rg8jcg54cz2ssz4fb2gjhrx9ja")))

(define-public crate-rusttyper-0.4.0 (c (n "rusttyper") (v "0.4.0") (d (list (d (n "glium") (r "^0.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.8") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 0)))) (h "15gvcy8178mdxhf8f67v4ajc3bd83f6pydgvq6jik8kiv2qkya9l")))

(define-public crate-rusttyper-0.4.1 (c (n "rusttyper") (v "0.4.1") (d (list (d (n "glium") (r "^0.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.9") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 0)))) (h "16p6ngp572bax7b8c5fj5qv8kp13ly7g071q9qzc4pr5b7d35i90")))

(define-public crate-rusttyper-0.4.2 (c (n "rusttyper") (v "0.4.2") (d (list (d (n "glium") (r "^0.25") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.6") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 0)))) (h "0bnla5crc5z807y2rms3vzs7g8zffl78y0dflnwl65nywmf8iyz5")))

(define-public crate-rusttyper-0.5.0 (c (n "rusttyper") (v "0.5.0") (d (list (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.6") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 0)))) (h "0hfy6ck9ghhil1a8al3m91hzq538d0ky1hcs0f9ng123gkjxn7fm")))

(define-public crate-rusttyper-0.6.0 (c (n "rusttyper") (v "0.6.0") (d (list (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.6") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 0)))) (h "0gc6wwck7anpvjyv707dn0wzdmqgawmwz16fn1r6lmq7wigh6m4z")))

(define-public crate-rusttyper-0.6.1 (c (n "rusttyper") (v "0.6.1") (d (list (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.6") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.2") (d #t) (k 0)))) (h "0p9bz8c3dk5281f82zhq2085f6i4a02yy4iys8jk81z3lcmfy9mj")))

