(define-module (crates-io ru st rustup-configurator) #:use-module (crates-io))

(define-public crate-rustup-configurator-0.1.0 (c (n "rustup-configurator") (v "0.1.0") (d (list (d (n "insta") (r "^1.23") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r232zk92dsx0h32zr3jfdwzri20i1f8a5qisrcjcbhyq7hamp8m")))

(define-public crate-rustup-configurator-0.1.1 (c (n "rustup-configurator") (v "0.1.1") (d (list (d (n "insta") (r "^1.23") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13bqvgr9anqgxsjbhnyd93qigq6rm8a71ijc1ibicycf6hk4vspi")))

