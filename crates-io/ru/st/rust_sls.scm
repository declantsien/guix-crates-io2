(define-module (crates-io ru st rust_sls) #:use-module (crates-io))

(define-public crate-rust_sls-0.1.0 (c (n "rust_sls") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.40") (d #t) (k 1)) (d (n "cpp") (r "^0.5.1") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1lf2xzf9b6hxdmvii67f2xc95g1jjnjmsaa2awr25cc1qpnmm0j3")))

(define-public crate-rust_sls-0.1.1 (c (n "rust_sls") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.40") (d #t) (k 1)) (d (n "cpp") (r "^0.5.1") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1x3j8bb43gghyqy1qqzy08hcp7h36fi1hh5z3kg1giz1sw3bg9sz")))

