(define-module (crates-io ru st rustycli) #:use-module (crates-io))

(define-public crate-rustycli-0.1.0 (c (n "rustycli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (f (quote ("dots" "arc" "line"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07nxyh0b5hs4hirdihr1f9j9q4arcsrwxi5n8fa8k1rd5fg63x3c")))

(define-public crate-rustycli-0.1.1 (c (n "rustycli") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (f (quote ("dots" "arc" "line"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jrdd8rpj6xzfzss189hlyrnyci9z52liln0ffwv8zlvqn9mp516")))

