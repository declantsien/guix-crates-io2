(define-module (crates-io ru st rust-hello) #:use-module (crates-io))

(define-public crate-rust-hello-0.1.0 (c (n "rust-hello") (v "0.1.0") (h "0ksvx4f8jfcyi2yi4zsxrjcqsajwrip0ni8jc24ah6jqwydwk1as")))

(define-public crate-rust-hello-0.1.1 (c (n "rust-hello") (v "0.1.1") (h "02qq7ps1xvgvhhc9rcfxznvjs20aashwckw0lry1zxl461klp9m6")))

