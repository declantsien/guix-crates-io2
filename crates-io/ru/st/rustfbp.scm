(define-module (crates-io ru st rustfbp) #:use-module (crates-io))

(define-public crate-rustfbp-0.2.1 (c (n "rustfbp") (v "0.2.1") (h "02az3n6pxcc36bpyv3k9blad351n1rvz1h4ryzcj9sa4gxlhrj2m")))

(define-public crate-rustfbp-0.3.0 (c (n "rustfbp") (v "0.3.0") (d (list (d (n "capnp") (r "*") (d #t) (k 0)) (d (n "capnpc") (r "*") (d #t) (k 0)) (d (n "libloading") (r "*") (d #t) (k 0)) (d (n "nanomsg") (r "*") (d #t) (k 0)))) (h "0mxvwmhbvsmif82naf2iyxyh4jkjp4m95l0h9mb15blh5q4c373r")))

(define-public crate-rustfbp-0.3.1 (c (n "rustfbp") (v "0.3.1") (d (list (d (n "capnp") (r "*") (d #t) (k 0)) (d (n "capnpc") (r "*") (d #t) (k 0)) (d (n "libloading") (r "*") (d #t) (k 0)))) (h "0m1kfrpcxib99q1yabv9k7fbdcrwhd7c6j346xn0c7933ir7yplc")))

(define-public crate-rustfbp-0.3.2 (c (n "rustfbp") (v "0.3.2") (d (list (d (n "capnp") (r "*") (d #t) (k 0)) (d (n "capnpc") (r "*") (d #t) (k 0)) (d (n "libloading") (r "*") (d #t) (k 0)))) (h "1xxs3qz5g7510c801398ggb2ziav3ravsv4im9gy5ry8q64mg1hl")))

(define-public crate-rustfbp-0.3.3 (c (n "rustfbp") (v "0.3.3") (d (list (d (n "capnp") (r "*") (d #t) (k 0)) (d (n "capnpc") (r "*") (d #t) (k 0)) (d (n "libloading") (r "*") (d #t) (k 0)))) (h "0pbx9qavr3z33bqcvb9if9ljqymhddssis3l25pqyfzxi0kvsbws")))

(define-public crate-rustfbp-0.3.6 (c (n "rustfbp") (v "0.3.6") (d (list (d (n "capnp") (r "^0.6.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.6.0") (d #t) (k 0)) (d (n "dylib") (r "^0.0.2") (d #t) (k 0)))) (h "0fmq3lzmp3nc7g3jhjviaiim0ycq3k2p2j2461mrwamldyfiipbl")))

(define-public crate-rustfbp-0.3.7 (c (n "rustfbp") (v "0.3.7") (d (list (d (n "capnp") (r "^0.6.2") (d #t) (k 0)) (d (n "capnpc") (r "^0.6.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.1") (d #t) (k 0)))) (h "16nx028rjdb6bavbr7r5p9z1y8kf7qjlwwk1bv1ynj4wfv8jrcc7")))

(define-public crate-rustfbp-0.3.8 (c (n "rustfbp") (v "0.3.8") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)))) (h "0d2fd3h5mc7y4cdwm2m6xxqycnwwrl1062h4yng9n0bjdc6w19kv")))

(define-public crate-rustfbp-0.3.9 (c (n "rustfbp") (v "0.3.9") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)))) (h "0z26zmr5asz5b0iy6hxh23yslgngpz93qxmpxvki4ihnw8s1yq47")))

(define-public crate-rustfbp-0.3.10 (c (n "rustfbp") (v "0.3.10") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)))) (h "1xcprmr6agni9salqz1hwq12pinp1n1nbpgid486g3kz10h78cgc")))

(define-public crate-rustfbp-0.3.11 (c (n "rustfbp") (v "0.3.11") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)))) (h "15iv4k0kpya8l0yx00krzw47qrbnczz8jfvsj20cjq7cbk2dram6")))

(define-public crate-rustfbp-0.3.12 (c (n "rustfbp") (v "0.3.12") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)))) (h "07mnpn15jlpyrzv8x4p2a0ack9nkhaj0mrjyh5l9kjjj0xm1h4ls")))

(define-public crate-rustfbp-0.3.13 (c (n "rustfbp") (v "0.3.13") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "122kryggxlnvrky5dvg8hghk9jdwqf3yklqssp7h6xff7nqn4zym")))

(define-public crate-rustfbp-0.3.14 (c (n "rustfbp") (v "0.3.14") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "0bv182knyj0pci09109j9n9pf1vsxbfkc6yw281cavmzzxijl8p4")))

(define-public crate-rustfbp-0.3.15 (c (n "rustfbp") (v "0.3.15") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "1mm32nywafvs2zq5r8218f7n7qficr2813ljxsb2i07fs5mvhyb6")))

(define-public crate-rustfbp-0.3.16 (c (n "rustfbp") (v "0.3.16") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "0vxw0hv2qb0k2l4gpkdqdkcbl52h3is0yjx84vm8gfgi6r9a8p32")))

(define-public crate-rustfbp-0.3.17 (c (n "rustfbp") (v "0.3.17") (d (list (d (n "capnp") (r "^0.7.0") (d #t) (k 0)) (d (n "capnpc") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "13i24gj9vqwczaa2wm3y964ybhf0psmqwgg58hd3c96snibg793b")))

(define-public crate-rustfbp-0.3.18 (c (n "rustfbp") (v "0.3.18") (d (list (d (n "capnp") (r "^0.7.4") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "110cmh5kaihfzdlkcvl8a7vxkfrws2q9yjq9r3iaqlxi4b4y7d7v")))

(define-public crate-rustfbp-0.3.19 (c (n "rustfbp") (v "0.3.19") (d (list (d (n "capnp") (r "^0.7.4") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "1mkinnyj5yd80xhl7az31fqgp4n8y64p4hxp60gd3y9nnhb0pj4a")))

(define-public crate-rustfbp-0.3.20 (c (n "rustfbp") (v "0.3.20") (d (list (d (n "capnp") (r "^0.7.4") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "1d85kmxf65mvqy6gksssymwpppn2rklffl2y2szfijwg5mn30lwg")))

(define-public crate-rustfbp-0.3.21 (c (n "rustfbp") (v "0.3.21") (d (list (d (n "capnp") (r "^0.7.4") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "17yxlw97p422771p1by5sx2lzy04r27zkwy8f8qcv9mjs24fyb1a")))

(define-public crate-rustfbp-0.3.22 (c (n "rustfbp") (v "0.3.22") (d (list (d (n "capnp") (r "^0.7.4") (d #t) (k 0)) (d (n "libloading") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.0") (d #t) (k 0)))) (h "06c9xl9srrv9mcyk3rw3i8kdnha98jhd8nqgdcnngpwld1kmxdr8")))

(define-public crate-rustfbp-0.3.23 (c (n "rustfbp") (v "0.3.23") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0vz8yyrv1c95a52js4lqmjp0jzi1lfm1nr2hcxm524ms179f7hgq")))

(define-public crate-rustfbp-0.3.24 (c (n "rustfbp") (v "0.3.24") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "1b6s0aksz048dsc2a8lw84sd7m8gwp5q2lpnzgnv4xrr15iw0569")))

(define-public crate-rustfbp-0.3.25 (c (n "rustfbp") (v "0.3.25") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0dcfsq71ndsijcxzkwn5nk4zyy13z9b4lwb8xza2y3grl4kx5izx")))

(define-public crate-rustfbp-0.3.26 (c (n "rustfbp") (v "0.3.26") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "1l5h1kg7x19p9vznymh9ah0yashpphg1qij84x3gqpf1hrhy2vd0")))

(define-public crate-rustfbp-0.3.27 (c (n "rustfbp") (v "0.3.27") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0xvla2s1b1148mww5qns9wwbmf82h4rq7sb9gis58lpwksc6kv8n")))

(define-public crate-rustfbp-0.3.28 (c (n "rustfbp") (v "0.3.28") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0a1p6sahdibgci90d74kc45xjf65ih999qfv8653i53chf3bikhj")))

(define-public crate-rustfbp-0.3.29 (c (n "rustfbp") (v "0.3.29") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "1k11sjbxvrh2l7wi187fpyq15vf7qmym3ydhj4js4dql16cmh3hm")))

(define-public crate-rustfbp-0.3.30 (c (n "rustfbp") (v "0.3.30") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "1xmmwcczbcwnbw2fcj89f3bfmf0caxzwy08jwggdwn88kyk58ywr")))

(define-public crate-rustfbp-0.3.31 (c (n "rustfbp") (v "0.3.31") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0pwfr0jb4g1q27jrdhwdb4y391pm0xrfcv2symkss0hcha7hlm5a")))

(define-public crate-rustfbp-0.3.32 (c (n "rustfbp") (v "0.3.32") (d (list (d (n "capnp") (r "^0.7.5") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "1iv7669r78ps9xpijf15k7qarvaaqv0cw7kqjnpmjn0l28alh219")))

(define-public crate-rustfbp-0.3.33 (c (n "rustfbp") (v "0.3.33") (d (list (d (n "capnp") (r "^0.8.0") (d #t) (k 0)) (d (n "libloading") (r "^0.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0kmpl5zdb8n6cvrbf87sp76smbc323daavcn6wk1ji8z6z2az8iz")))

(define-public crate-rustfbp-0.3.34 (c (n "rustfbp") (v "0.3.34") (d (list (d (n "capnp") (r "^0.8.10") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.6.0") (d #t) (k 0)))) (h "1rl49w3y4pphl4sjlfqy5y9nh6c6m748dmcy825m2apb8z4j351i")))

