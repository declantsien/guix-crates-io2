(define-module (crates-io ru st rusterpassword) #:use-module (crates-io))

(define-public crate-rusterpassword-0.1.0 (c (n "rusterpassword") (v "0.1.0") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (d #t) (k 0)) (d (n "secstr") (r "^0") (d #t) (k 0)))) (h "1vwdj0zqv45j5gww0fx88r5g3zr9b2brk7xxfb0pvqrhmixdj9gv")))

(define-public crate-rusterpassword-0.2.0 (c (n "rusterpassword") (v "0.2.0") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (d #t) (k 0)) (d (n "secstr") (r "^0.2") (d #t) (k 0)))) (h "14rkcz5n7n121jg0rpppd0ccajb08zazcgqyymx6g97xalffszxi")))

(define-public crate-rusterpassword-0.2.1 (c (n "rusterpassword") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (d #t) (k 0)) (d (n "secstr") (r "^0.3") (d #t) (k 0)))) (h "0rh7kvfxskj8yygwziqaa1css74c1v71j309k5rqwnrhg8bjydkd")))

(define-public crate-rusterpassword-0.2.2 (c (n "rusterpassword") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (d #t) (k 0)) (d (n "secstr") (r "^0.3") (d #t) (k 0)))) (h "1f7whbylai93m7crqjd5fi3gxgd72fwx5qmnnns9aavxp8fspwwr")))

(define-public crate-rusterpassword-0.2.3 (c (n "rusterpassword") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0") (d #t) (k 0)) (d (n "secstr") (r "^0.3") (d #t) (k 0)))) (h "1s6ym9bca74wzimbrng0mvm60wn7snkpw3lz7gqsi9mpijsya4jv")))

