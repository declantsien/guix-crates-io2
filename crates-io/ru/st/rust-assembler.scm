(define-module (crates-io ru st rust-assembler) #:use-module (crates-io))

(define-public crate-rust-assembler-0.1.0 (c (n "rust-assembler") (v "0.1.0") (h "1qg2490v1rcrm8vh33zl5rpvp7vhqk9pccy81qqg5rdyj1bf9bqm")))

(define-public crate-rust-assembler-0.1.1 (c (n "rust-assembler") (v "0.1.1") (h "0rcvp6d64jbgwx2imzrk7p8p45k0pvim6700vc1bch0s4ib4isk0")))

(define-public crate-rust-assembler-0.1.10 (c (n "rust-assembler") (v "0.1.10") (h "0jvi48gw2v6a0hdg2af5azvqqa6nv7n382pg4vzmi6fp2djdm3gi")))

(define-public crate-rust-assembler-0.1.101 (c (n "rust-assembler") (v "0.1.101") (h "0nsd423n19zjrwbz6kzg8jy4m8l60mj129c11mcyjhzzwyr44cfm")))

(define-public crate-rust-assembler-0.1.11 (c (n "rust-assembler") (v "0.1.11") (h "18fsgvlq1l22qxdsxrp4xv4mkimqqv7cz1dm1f0wkgi4x7qxkjsh")))

(define-public crate-rust-assembler-0.1.12 (c (n "rust-assembler") (v "0.1.12") (h "1wqvb6s7b6kvs5f53wmw5qfb730amklskyvsxwaxkgjwmhz4ahnv")))

(define-public crate-rust-assembler-0.1.13 (c (n "rust-assembler") (v "0.1.13") (h "0dbfg1sk4dhb4387bmkkg47dbmmd4ziivickbygpq0sazliw5mwb")))

