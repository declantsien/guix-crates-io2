(define-module (crates-io ru st rust-completions) #:use-module (crates-io))

(define-public crate-rust-completions-0.1.0 (c (n "rust-completions") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jhb2p6ib8xpxprnqmrzl7q99wf5fiv0l9jcwk2hafv6qpsjphgh")))

