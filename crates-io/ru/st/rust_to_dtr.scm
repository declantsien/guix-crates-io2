(define-module (crates-io ru st rust_to_dtr) #:use-module (crates-io))

(define-public crate-rust_to_dtr-0.1.0 (c (n "rust_to_dtr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ri6kyixn7d3b6gfpialnwrr6b8ghbwli8i4mii5ll1grm6yrmfn")))

(define-public crate-rust_to_dtr-0.1.1 (c (n "rust_to_dtr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hrs69vgp8hnh1j7m0brf701wdai1x91hdsd10zhqm6lzvh8dimy")))

(define-public crate-rust_to_dtr-0.1.2 (c (n "rust_to_dtr") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kdlmwa7mikqc5ahra7491bvlz75q9jaxi3x8v6906yzxlwxwwrc")))

(define-public crate-rust_to_dtr-0.1.3 (c (n "rust_to_dtr") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a17d48d40dfzq8mas66l92q2giiffb7s6vciw7jpriz0x963zjr")))

(define-public crate-rust_to_dtr-0.1.4 (c (n "rust_to_dtr") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fky8aaw526y1m5kfnsagdhw6s2qb2ahrzwjl4vvpxjjis0xfnmp")))

(define-public crate-rust_to_dtr-0.1.5 (c (n "rust_to_dtr") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ywc7qq2za2rf8pjb461x3pnfkrh20mnpmc6q3bpi45n73mq3rk9")))

(define-public crate-rust_to_dtr-0.1.6 (c (n "rust_to_dtr") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15047xdf7z8i0kdlgg2mw9y7bxqbj8578q9hy0wpwwxi4pb5v2my")))

(define-public crate-rust_to_dtr-0.2.0 (c (n "rust_to_dtr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00kf67dcn67822x2ywiccmvbq3lfpng0w2apgjnp7l4d7h7jlysp")))

(define-public crate-rust_to_dtr-0.2.1 (c (n "rust_to_dtr") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10cw101kk38qhybxdigim4brvraivk4dglc8v2pfdnri6q3k9my2")))

(define-public crate-rust_to_dtr-0.2.2 (c (n "rust_to_dtr") (v "0.2.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11i31k8xjaf8bzpdac5h4rim9hmlnkg2kln7ajqjn5pkrbdi29ih")))

(define-public crate-rust_to_dtr-0.3.0 (c (n "rust_to_dtr") (v "0.3.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c5f7pdphbz8dvwnm9ap9ccs7qvcavy0a5xi3djn3x5g8kpkjh73")))

