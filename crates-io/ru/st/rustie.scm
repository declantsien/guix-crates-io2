(define-module (crates-io ru st rustie) #:use-module (crates-io))

(define-public crate-rustie-0.1.0 (c (n "rustie") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)))) (h "1k796cl3khyaw04gzhyv7k0s0yd7agzz4k6a8akvcq4mqj3zq02z")))

(define-public crate-rustie-0.1.1 (c (n "rustie") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)))) (h "0dx1md8x8bm8jg5a1ppmrqflwabiap7ish7n50yfjpi60fd4iw3f")))

(define-public crate-rustie-0.1.2 (c (n "rustie") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)))) (h "0dbw83xg7h4gad2xgxd14niq3lb09ihza51n54an741awjfl6g8v")))

(define-public crate-rustie-0.1.3 (c (n "rustie") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)))) (h "07sn6cdbahl44zb5312myr2552sdsdpqzcb98sx3f0djma5jsfj0")))

(define-public crate-rustie-0.1.4 (c (n "rustie") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "1bz5mbvmy2h8rpbs7a5dbk5swvpqgmhk7m29c2hiy5wvjvvlb51h")))

(define-public crate-rustie-0.1.5 (c (n "rustie") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "0k1r06isqfwghfqp4zhriaq5bbyi0j0w9r7wgjsx4n07fg0zkhpg")))

(define-public crate-rustie-0.1.6 (c (n "rustie") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "1ispnqn80vih9yfi6dbixaqwnkzs3bmrn1vn3k4rnd1vkyggbrfv")))

(define-public crate-rustie-0.1.7 (c (n "rustie") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "167akqcr3jsy2hgirp0y2swmv902yi2p80lvy51l2kdr629a2iql")))

(define-public crate-rustie-0.1.8 (c (n "rustie") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "1j21w55sx1j9k5v3hxm9b25lsc2wgxf4mrvz80xr0phm859g8n2s")))

(define-public crate-rustie-0.1.9 (c (n "rustie") (v "0.1.9") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "0vjdzmcanlbvc0n8bc3q3w2whgrmpc9lsj3kz3m7zs2l47lnlnzy")))

(define-public crate-rustie-0.1.10 (c (n "rustie") (v "0.1.10") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "01yiald624hxm3qbgr1pd6y9bxjhsqx0mpcwpsv3iya56gnn99br")))

(define-public crate-rustie-0.1.11 (c (n "rustie") (v "0.1.11") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "0ih1khba9xb00xp3gini67c8k7v7nga158rqargfykzxvnhrcjdk")))

(define-public crate-rustie-0.1.12 (c (n "rustie") (v "0.1.12") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "0ylbwzr8l0ki8bnr0vygcv1nfkfp2li9aj8x10728nkyf1a2xzk0")))

(define-public crate-rustie-0.1.14 (c (n "rustie") (v "0.1.14") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "12cjrrwajwvai58w2c9pfsmsqwy8i6wvb0zk00rin50v8c9al2zg")))

(define-public crate-rustie-0.1.15 (c (n "rustie") (v "0.1.15") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "evalexpr") (r "^4.1.0") (k 0)))) (h "0an25n9qpygn1mp8pz7iyc5vj48i3jsb7gi2ps4nknb8k8b6dq3k")))

(define-public crate-rustie-0.1.16 (c (n "rustie") (v "0.1.16") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "evalexpr") (r "^4.1.0") (k 0)))) (h "0kay1zn15s1ylym5v538qi1y3dq0v79kfdvg48vfkmh1hh8sg0hx")))

(define-public crate-rustie-0.1.17 (c (n "rustie") (v "0.1.17") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "evalexpr") (r "^4.1.0") (k 0)))) (h "021yqslnm4dr9fgsk51cyybdqjh56hjjpbnlrfw0a7qwjz0kghxk")))

(define-public crate-rustie-0.1.18 (c (n "rustie") (v "0.1.18") (d (list (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "evalexpr") (r "^4.1.0") (k 0)))) (h "1xfbikssx2dm9y5j7jj9ix9cn55394y4zps1w4anq00car9pj2r3")))

