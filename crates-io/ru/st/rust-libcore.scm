(define-module (crates-io ru st rust-libcore) #:use-module (crates-io))

(define-public crate-rust-libcore-0.0.1 (c (n "rust-libcore") (v "0.0.1") (h "04vdiw99a8f7q7dpyrr4f3ny77kd3ixdpi2nhnmxzar92xg0nfms")))

(define-public crate-rust-libcore-0.0.2 (c (n "rust-libcore") (v "0.0.2") (h "1szr0qjbcxi1230ycq7fhx9s54sc4zp66swyz9j7nk585cgx6qjc")))

(define-public crate-rust-libcore-0.0.3 (c (n "rust-libcore") (v "0.0.3") (h "0lx478vz7hz9jnzmicdz16z886jb9q77s1ah3sp06bf01j1k6ymf")))

