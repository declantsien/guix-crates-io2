(define-module (crates-io ru st rustbasic) #:use-module (crates-io))

(define-public crate-rustbasic-0.0.1 (c (n "rustbasic") (v "0.0.1") (h "0cam2yvrxglxn2hn3g2jz8qx9yic6xvpprg2nqw1pb8gn9xh9abr") (y #t)))

(define-public crate-rustbasic-0.0.2 (c (n "rustbasic") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08r52knafxfmk1ds6h4fb4icyg1g5mwrqcfmnycfwh1f2ba5rf3d") (y #t)))

(define-public crate-rustbasic-0.0.3 (c (n "rustbasic") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ax4l3as61c5s5cnp4hd1q8xrixfl2asmgx8841lymrv3mqsxrjd") (y #t)))

(define-public crate-rustbasic-0.0.4 (c (n "rustbasic") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "1x4vj8g5b5wniy7d12dfa13bg1cwrds4l37889f1c10dy7b07s96") (y #t)))

(define-public crate-rustbasic-0.0.5 (c (n "rustbasic") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "061n5ajbcc11rap9z2vg60phz1c8k3pa9s1f8z7jkbfd5pfnn0xd") (y #t)))

(define-public crate-rustbasic-0.0.6 (c (n "rustbasic") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0qdgx5ka4cca75kz2q5rsvwmbvy52p8nlz3jvjmfs0jlblwh9b7i") (y #t)))

(define-public crate-rustbasic-0.0.7 (c (n "rustbasic") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "14smpbrqk73ijlqkq6glzsma5jqn4vzrv6g0sgz5h7krkcp06vgh") (y #t)))

(define-public crate-rustbasic-0.0.8 (c (n "rustbasic") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0flf5h9mlp46j5h82cvw22v483bb1ihdh2jpkc46g53bjwaxa4cx") (y #t)))

(define-public crate-rustbasic-0.0.9 (c (n "rustbasic") (v "0.0.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "1bk0y49z4dkvaxknp8kkpiby67y7bfa5z5pcfwsvrmzvczap1rdv") (y #t)))

(define-public crate-rustbasic-0.0.10 (c (n "rustbasic") (v "0.0.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "1sswvvqfk65hh06sxq067mqzl5jzz8qh5vsv8ql41isl7j9kkhsw") (y #t)))

(define-public crate-rustbasic-0.0.11 (c (n "rustbasic") (v "0.0.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0zjr1s87simjv6vlz8gfsb8m1pzwr1c90rj408p8b829avyql6zq") (y #t)))

(define-public crate-rustbasic-0.0.12 (c (n "rustbasic") (v "0.0.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "1h137m4ry4m922kqvvjwbg3h05jhkzxnwfkv74q524mvf6007wzb")))

(define-public crate-rustbasic-0.0.13 (c (n "rustbasic") (v "0.0.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.2") (d #t) (k 0)))) (h "1qw0zkqs205bs9pqccgk3a7i1w57a7i1m6bhvj6zq419n6wvjr6p")))

(define-public crate-rustbasic-0.0.14 (c (n "rustbasic") (v "0.0.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.3") (d #t) (k 0)))) (h "11nn94z5n4njnbjfxjs4xmaar6ziidx3bm53qlq1jl0my51qn67m")))

(define-public crate-rustbasic-0.0.15 (c (n "rustbasic") (v "0.0.15") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.3") (d #t) (k 0)))) (h "0h3f2qajj7y97csap4p1a0n228z45ff44j23798gcklblfgkj2xs")))

(define-public crate-rustbasic-0.0.16 (c (n "rustbasic") (v "0.0.16") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.4") (d #t) (k 0)))) (h "0l231na979ph83mzcirxin9gy7db5k2yvv51kxy870pb02zlfmca")))

(define-public crate-rustbasic-0.0.17 (c (n "rustbasic") (v "0.0.17") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.4") (d #t) (k 0)))) (h "0aiqqpb9dd6wk6fdd4354xl9wacd8iwfrd2a8vw00rl3a38a6kmb")))

(define-public crate-rustbasic-0.0.18 (c (n "rustbasic") (v "0.0.18") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.4") (d #t) (k 0)))) (h "1mffb9jdh1i7ycy2jdcdiq8qfp22hi19hhms4dk3kxdmqr5qj2pj")))

(define-public crate-rustbasic-0.0.19 (c (n "rustbasic") (v "0.0.19") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.5") (d #t) (k 0)))) (h "1p6ybgaz08xm89l083kvjlngd56h2v7fsgkh6wm8vfn1wjzb7hzs")))

(define-public crate-rustbasic-0.0.20 (c (n "rustbasic") (v "0.0.20") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.6") (d #t) (k 0)))) (h "1fjbqm1pbx3ssscddjvsdqmfwl0ljg8ir4whgwaak8lcgjbxpzhm")))

(define-public crate-rustbasic-0.0.21 (c (n "rustbasic") (v "0.0.21") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.7") (d #t) (k 0)))) (h "0r4wlzjfa6i51v3b0lk0bz4q3klzlp2lxxwy9lraf6l8fhg8dn1r")))

(define-public crate-rustbasic-0.0.22 (c (n "rustbasic") (v "0.0.22") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.8") (d #t) (k 0)))) (h "17ch9c6ixzk778fij8d0x1bgpx65pa8x28c4xpkp1i1a127qfjcl")))

(define-public crate-rustbasic-0.0.23 (c (n "rustbasic") (v "0.0.23") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.9") (d #t) (k 0)))) (h "07byfqwlppfx63qv5gsaanr3spz4a9nnlz4m6qa2qgpjlkhf9vsn")))

(define-public crate-rustbasic-0.0.24 (c (n "rustbasic") (v "0.0.24") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustbasic-macro") (r "^0.0.10") (d #t) (k 0)))) (h "08r7bvvc7c5wi4zwz79nmlb55ylpd9dakzywzx3v3nf8vfsqpdh5")))

