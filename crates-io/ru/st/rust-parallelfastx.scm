(define-module (crates-io ru st rust-parallelfastx) #:use-module (crates-io))

(define-public crate-rust-parallelfastx-0.1.0 (c (n "rust-parallelfastx") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "0lqyr3s9pfd3cgb13p0xz35mms1y0z8as1g0ygfg6d6y67dzqmbg")))

(define-public crate-rust-parallelfastx-0.1.1 (c (n "rust-parallelfastx") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "seq_io") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "0b9i9qdyabay0bhhz0fhlxi4pahiyckngpv0lqrl6mj2ymrh3857")))

