(define-module (crates-io ru st rust-quickie) #:use-module (crates-io))

(define-public crate-rust-quickie-0.1.0 (c (n "rust-quickie") (v "0.1.0") (h "08qqiagmw7w3qazw6wp38q57z96wl3s7mnya8lmrv8nsq8d4i57j")))

(define-public crate-rust-quickie-0.1.1 (c (n "rust-quickie") (v "0.1.1") (h "1inq9vmf62j5rxbb8bapdx30i523k67m1idb6a51rl78b8pf7ri2")))

