(define-module (crates-io ru st rustproof-libsmt) #:use-module (crates-io))

(define-public crate-rustproof-libsmt-0.1.0 (c (n "rustproof-libsmt") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.2.7") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "00bp7pga41za3ajm0hdxi3fvzbn8ix9sknvlm4c2yvpj5v0pvfy2")))

