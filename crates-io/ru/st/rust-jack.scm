(define-module (crates-io ru st rust-jack) #:use-module (crates-io))

(define-public crate-rust-jack-0.1.0 (c (n "rust-jack") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rlknfc0mdgbj8yqm4bk83a8i61686mh2sw2k4dcclqw3vjx893d") (y #t)))

