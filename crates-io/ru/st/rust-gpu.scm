(define-module (crates-io ru st rust-gpu) #:use-module (crates-io))

(define-public crate-rust-gpu-0.1.0 (c (n "rust-gpu") (v "0.1.0") (h "0r0kgiw3by81zgpxy0xf4hcqyw2bb869l2wd4v6xv8ch1dmnn5l2") (y #t)))

(define-public crate-rust-gpu-0.1.1 (c (n "rust-gpu") (v "0.1.1") (h "07zfznz40iaaw1qgzaqs5p8xlkj6f29adqxyqczf10rhdf3ws5wa") (y #t)))

