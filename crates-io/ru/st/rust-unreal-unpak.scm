(define-module (crates-io ru st rust-unreal-unpak) #:use-module (crates-io))

(define-public crate-rust-unreal-unpak-0.1.0 (c (n "rust-unreal-unpak") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "compress") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-byteorder") (r "^0.3.0") (d #t) (k 0)))) (h "0ibfr8kw87p3s7ldxxzyswpcvl4hf9ask31vnp5ji31dyf6bv1lq")))

