(define-module (crates-io ru st rust-fast-framework) #:use-module (crates-io))

(define-public crate-rust-fast-framework-0.1.0 (c (n "rust-fast-framework") (v "0.1.0") (h "0l2vk45rw19iq81kd92nlay8rbcrlvbacp0kzw6xczrk7sdj2lph") (y #t)))

(define-public crate-rust-fast-framework-0.1.1 (c (n "rust-fast-framework") (v "0.1.1") (h "1bwzck1ghs7cmzp9s6b1a5y8xrym6razaqj8h51w1k6hbbq7hyx3") (y #t)))

