(define-module (crates-io ru st rusty_jsc_macros) #:use-module (crates-io))

(define-public crate-rusty_jsc_macros-0.0.2 (c (n "rusty_jsc_macros") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11ir5n4q1cc5h4fndhllldc0zj8w5nnnifkjjql5aixqwv9yvr1w")))

(define-public crate-rusty_jsc_macros-0.0.3 (c (n "rusty_jsc_macros") (v "0.0.3") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1avm2v49kz9ziz4yn1xpqb5ia9gpw3cx31p7xzpd2iprmgnm29ac")))

(define-public crate-rusty_jsc_macros-0.1.0 (c (n "rusty_jsc_macros") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wa0vh8vj2vd14rx1fi8g1pwj90pikk8ga1dvnp75lf2gh4hf93v")))

