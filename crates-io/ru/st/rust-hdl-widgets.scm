(define-module (crates-io ru st rust-hdl-widgets) #:use-module (crates-io))

(define-public crate-rust-hdl-widgets-0.1.0 (c (n "rust-hdl-widgets") (v "0.1.0") (d (list (d (n "rust-hdl-core") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-macros") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-yosys-synth") (r "^0.1") (d #t) (k 0)))) (h "13ng58fh9s3g08kl5jj13p7yr4illvyf3pmfxcywn0s2ddzq4ylr")))

(define-public crate-rust-hdl-widgets-0.44.2 (c (n "rust-hdl-widgets") (v "0.44.2") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-hdl-core") (r "^0.44.2") (d #t) (k 0)))) (h "0jag3yis5r5cw3qznxvicv2hw3nlw3mqb124h63c69ay36y4xrkb")))

(define-public crate-rust-hdl-widgets-0.45.0 (c (n "rust-hdl-widgets") (v "0.45.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-hdl-core") (r "^0.45.0") (d #t) (k 0)))) (h "095gyi0hadz83x1rv0asn4l2cfss5snv5n65i6lgpnv1k373q7ia")))

(define-public crate-rust-hdl-widgets-0.45.1 (c (n "rust-hdl-widgets") (v "0.45.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-hdl-core") (r "^0.45.1") (d #t) (k 0)))) (h "0qgx0k406p8xsm7ljkpr50k687rs908fc3n5b0vpp5m6gk15wam6")))

(define-public crate-rust-hdl-widgets-0.46.0 (c (n "rust-hdl-widgets") (v "0.46.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-hdl-core") (r "^0.46.0") (d #t) (k 0)))) (h "0dhygwbyg4zcnd2nn8a1gxhg7cvnrs0y63fwlh47pid9xd71gba7")))

