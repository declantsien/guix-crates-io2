(define-module (crates-io ru st rustyo) #:use-module (crates-io))

(define-public crate-rustyo-1.0.0 (c (n "rustyo") (v "1.0.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sw7ydba5vr8w1a4dk1bi2p69hnzn1kvl07xmi214r64f55czs43")))

(define-public crate-rustyo-1.0.1 (c (n "rustyo") (v "1.0.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1snlakqqv8zl5jf8d9v31sjmk1g70jcwbd39apvp6wad1snj8g32")))

(define-public crate-rustyo-1.0.2 (c (n "rustyo") (v "1.0.2") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fpz37r0v47awj1sx16apkpfkshqxgm045n0r8wbh4asbav7pgrh")))

(define-public crate-rustyo-1.0.3 (c (n "rustyo") (v "1.0.3") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15j8hiwalxa8gyx51labl7wazbfna8xw80w8qfq7x3bdbw873wzh")))

(define-public crate-rustyo-1.0.4 (c (n "rustyo") (v "1.0.4") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pjhmk38162bmbdcyrr3p45c223fa3slh7qxwbqmnrnbvqczhx85")))

(define-public crate-rustyo-1.0.5 (c (n "rustyo") (v "1.0.5") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11dk9nb7925xla2f1rm116zh4a1q0sylf5bxrd4b0rkr45m8alki")))

(define-public crate-rustyo-1.0.6 (c (n "rustyo") (v "1.0.6") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f05138ygb5cf28rw1pfwr5m2f25lr6mfg9q0qx2s0ms1aahxnvz")))

(define-public crate-rustyo-1.0.7 (c (n "rustyo") (v "1.0.7") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0l6bnx1yh40zhdkpmc75n6qyb041p6ppvirlpgyi96fv1jj50y9n")))

(define-public crate-rustyo-1.0.8 (c (n "rustyo") (v "1.0.8") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c1bfhv7g31gpm742akrr3bd77ryw48r545ihsgkd08d666aznrl")))

