(define-module (crates-io ru st rust_twitter_bot_lib) #:use-module (crates-io))

(define-public crate-rust_twitter_bot_lib-0.1.0 (c (n "rust_twitter_bot_lib") (v "0.1.0") (d (list (d (n "oauthcli") (r "^1.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "19jg7rdpcdyj9dsxirjm7ch7j9qq9xhm1v6x7wrrhszgfs9qwnjz")))

(define-public crate-rust_twitter_bot_lib-0.1.1 (c (n "rust_twitter_bot_lib") (v "0.1.1") (d (list (d (n "oauthcli") (r "^1.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "10bdiz3lzv1l544fxyrd4pb2kxwrqxm9wvmzh38qivnpmn6xn2zy")))

(define-public crate-rust_twitter_bot_lib-0.1.2 (c (n "rust_twitter_bot_lib") (v "0.1.2") (d (list (d (n "oauthcli") (r "^1.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0dw5ir4ckv3w0bdrfcff6cb5sxls8wlfby5b03sc8slka2z5w5b3")))

