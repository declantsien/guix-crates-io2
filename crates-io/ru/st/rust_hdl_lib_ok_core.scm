(define-module (crates-io ru st rust_hdl_lib_ok_core) #:use-module (crates-io))

(define-public crate-rust_hdl_lib_ok_core-0.44.0 (c (n "rust_hdl_lib_ok_core") (v "0.44.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rust_hdl_lib_core") (r "^0.44.0") (d #t) (k 0)) (d (n "rust_hdl_lib_hls") (r "^0.44.0") (d #t) (k 0)) (d (n "rust_hdl_lib_ok_frontpanel_sys") (r "^0.44.0") (d #t) (k 0)) (d (n "rust_hdl_lib_sim") (r "^0.44.0") (d #t) (k 0)) (d (n "rust_hdl_lib_widgets") (r "^0.44.0") (d #t) (k 0)))) (h "0pbr7751sa9dyxpr762hbph2bylc31cydqjqxc8rc6ya90ng1r6p")))

