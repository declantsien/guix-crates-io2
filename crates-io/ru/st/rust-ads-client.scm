(define-module (crates-io ru st rust-ads-client) #:use-module (crates-io))

(define-public crate-rust-ads-client-0.1.0 (c (n "rust-ads-client") (v "0.1.0") (d (list (d (n "ads-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0abz6q4cwz6qv58mysfk9n1xdwmjhzidv0d5c467vw9zy87lh2ii")))

