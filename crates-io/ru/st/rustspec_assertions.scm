(define-module (crates-io ru st rustspec_assertions) #:use-module (crates-io))

(define-public crate-rustspec_assertions-0.1.0 (c (n "rustspec_assertions") (v "0.1.0") (h "0x59dfyx4vcadk5ylgsyq3r2253pfm9f0qv1iwibzzygwxgvxnhr") (y #t)))

(define-public crate-rustspec_assertions-0.1.1 (c (n "rustspec_assertions") (v "0.1.1") (h "1q572sdqz068mj3psfj5rhx6y2db2gdsndyhr87i2cadg0r2inxh")))

(define-public crate-rustspec_assertions-0.1.2 (c (n "rustspec_assertions") (v "0.1.2") (h "06g10rsaphxxvsgq53l4589n8682xnrz4a7bw34zp5kqkc5j6wf7")))

(define-public crate-rustspec_assertions-0.1.3 (c (n "rustspec_assertions") (v "0.1.3") (h "1r9j2z27l4arhx3kav2qf8c5cc1j2iccspj20yx4adi08brjh9jr")))

(define-public crate-rustspec_assertions-0.1.4 (c (n "rustspec_assertions") (v "0.1.4") (h "07rw2n6w7m5py92xfsgbmv5v9mcsbs43zwvfvnb50kj11wvhxvaf")))

(define-public crate-rustspec_assertions-0.1.5 (c (n "rustspec_assertions") (v "0.1.5") (h "16byy8ckgh593k9znsqkkyrg2xbn806y0c2lgjr28n29rdgk1h6z")))

(define-public crate-rustspec_assertions-0.1.6 (c (n "rustspec_assertions") (v "0.1.6") (h "0w6dz3ps6ghfxla9ffay8457ymbblnjmpaqpwkavvfm4xa1h15wn")))

(define-public crate-rustspec_assertions-0.1.7 (c (n "rustspec_assertions") (v "0.1.7") (h "1nd7f6aqyrqf1drhgn4714l6hh174gl64ai5zncpyfnzycyddmb5")))

(define-public crate-rustspec_assertions-0.1.8 (c (n "rustspec_assertions") (v "0.1.8") (h "12zw0377gaphii2yvgbyvl68zn6wqynjxmxir6hya4jckdyvf6rm")))

(define-public crate-rustspec_assertions-0.1.9 (c (n "rustspec_assertions") (v "0.1.9") (h "06izclhckcsvvgqy2spjdkw26mnqbpgknshkg4jpl9jifjpskx96")))

