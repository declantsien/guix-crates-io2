(define-module (crates-io ru st rusty-ml) #:use-module (crates-io))

(define-public crate-rusty-ml-0.0.1 (c (n "rusty-ml") (v "0.0.1") (h "1l38xbshsqm0j00qcwas6knnmcyjvdx0r1yl4zjzabv1lq0gxpcj")))

(define-public crate-rusty-ml-0.0.2 (c (n "rusty-ml") (v "0.0.2") (h "0qhzvcaqy3bs6mvqh0b2faap547bc19ys5j3pwpsb9l6i84vm8km")))

