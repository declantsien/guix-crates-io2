(define-module (crates-io ru st rustc-ap-rustc_cratesio_shim) #:use-module (crates-io))

(define-public crate-rustc-ap-rustc_cratesio_shim-1.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0kgys28xa8fmq8glh4r00ckzpj5jik0b67c59dn1kzxv02yqncnw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-2.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0gjhv9h7z1rnlhp65q4y5mg39nis7bz83sx46sw26hxpnlgirw0z")))

(define-public crate-rustc-ap-rustc_cratesio_shim-3.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "3.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1ns2lzsmk8hqwkrvfn49lfjb14miii21kji255c8n8qyv2wlzzg8")))

(define-public crate-rustc-ap-rustc_cratesio_shim-4.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "4.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "01cx8k6m7bi6sqzx6abwz74mdf41n553c8j3p179rg8bmy058cm4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-5.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "5.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0yr1p1sdhbfxph7krr4cj2ckhg53dbal59db6bqvy71ra4zf0w7x")))

(define-public crate-rustc-ap-rustc_cratesio_shim-6.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "6.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0pbn9ivyk26smg8b97m9sz9kr6lxdpfskb6y3rgcprgai1mnbp8x")))

(define-public crate-rustc-ap-rustc_cratesio_shim-7.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "7.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0xmc87r3k8d10lpvxc1dk4xqmi8a9c6zb8cxs8l12dg38m78ybd0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-8.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "8.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0dnpyknh6a53n1x1vkrwmz096rjg1wfacw3hv52abys5vqvwj6lr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-9.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "9.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1vamz6rl4d244k4l5kw6ga2qb41cw4l3m2bky5m5q9587v9qa4y6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-10.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "10.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1y7vg44igm2h9vr9bfbq9sqj12vyhkbzphmzahlrxyqzb5b1bmyw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-11.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "11.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1j7pv37jdcxs7jyr3bb6ny5irq05grw6mkia286i82nhy2bbzrzb")))

(define-public crate-rustc-ap-rustc_cratesio_shim-12.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "12.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0pl4km7q5qljqysyvjq4xdpmzlg6d7l8fd3yidlmvgasmw81r9gi")))

(define-public crate-rustc-ap-rustc_cratesio_shim-13.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "13.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1frklp9v1z09mkara17wz65kbap92rm2gch0igc9iryns6iiaml0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-14.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "14.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1gpq9g53d3fs87x15dlsysaahvpgrxlg1yws15pkqd4h6pn19nbd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-15.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "15.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "02i14d46p585xy6xksa9zfyc8hxhfzv4nzz6wi7z0vc98yvk45yz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-16.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "16.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "110k7fxhcm0zymj608z50vlqpxg5z4kvz2cfp885m9248403wj94")))

(define-public crate-rustc-ap-rustc_cratesio_shim-17.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "17.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0l3gfkyc5y4l5hbmkl50xdh70d3ywaijpcwqi36saq0nfjdmwssd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-18.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "18.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "19aqxyc8v3pa8p4xqsccjclr0qraaan1m6awk1xc0p2s31yvkhw5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-19.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "19.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1rdz6nbbsms8fasqbf15cdccapng9gia14zhjwwxa30pdk3mmhia")))

(define-public crate-rustc-ap-rustc_cratesio_shim-20.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "20.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "105vkakcih20l6yngsx1g169y61ivfv09qiasy2l6gyzn3xk78iq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-21.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "21.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1cs0sx5xj4ghfcawyzqn4zd296vavdf0r2qk29l4b38inqg0ig86")))

(define-public crate-rustc-ap-rustc_cratesio_shim-22.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "22.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1cmypmf2qykin1lr8p4www0cbpwwgzkk726mq5h5i6lbm39lgy2k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-23.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "23.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0k1854nqs0cag5k1jw0c2vzf01n4dmfambi4r5z7y3vnahq2bylm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-24.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "24.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "13nfsany96am29a0rybd3wr5azhdcii1n9w6ha51w2il6r9z7pr9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-25.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "25.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "09ps3nyfm8rpgyf88kra6s6gldh8m02jj1ixnl2r1vh9xn511j7b")))

(define-public crate-rustc-ap-rustc_cratesio_shim-26.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "26.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "12p5wq88jc3v691fxw04xmjcigjfl1x79drs8syw7gsj7sd3z23c")))

(define-public crate-rustc-ap-rustc_cratesio_shim-27.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "27.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0kd5hz8jv73h5x84bs8as0xka42rqqynz2paxnxfp8q6mqpymjzq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-28.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "28.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1i5jwpramg27sqk5f7m10mipx499dzg7rc44dx2wvg8bakhl67qb")))

(define-public crate-rustc-ap-rustc_cratesio_shim-29.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "29.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "095m3z14aqyzbx5h4j25kzjgs8abzs5awxfxcikqm9sf0iifbmaa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-30.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "30.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0yq6ya7p3ls92hx1jm4mjcn117v0f786rw0b1vvmpszai54yr944")))

(define-public crate-rustc-ap-rustc_cratesio_shim-31.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "31.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "05wz1akhvs87yr74ndafhhrrssw9q4457i7xqsjiya8d067dxdh6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-32.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "32.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1vac53l97c43pkwbgpd4djfl1mnps9sjdmvr8d1sqi0jjdfnrcah")))

(define-public crate-rustc-ap-rustc_cratesio_shim-33.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "33.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0wsrsb3qxa2hy1pjk5cswj5jdf2bkbdjk5f6sxa50pswq8b6nj06")))

(define-public crate-rustc-ap-rustc_cratesio_shim-34.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "34.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0wpsjfkab1ypqfjk6wji2hwdrpd8bj99vzjfazfrf3fgf1pjl8jk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-35.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "35.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1wcxqj65n805ba12kjc06mcz2ry59r4xiffh899ss2liy3qr46y3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-36.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "36.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "117y7dnnz3w2zpigkkyfkh9nn5wahkrmxkpxlc13nlfj37mbnmwf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-37.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "37.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0mji3bgw7xar6yqj59mddfd0y6xjq4c58ylsnir1dmngjg72lrm0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-38.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "38.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0h22ayyc1q69f3650wgcb2hsgwfhkgacgmgm78x820nwp66nc2bx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-39.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "39.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "14i7fqjsf8yxzs17cbmrbzm95iqy1181dyh428j77qv2b620mbpa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-40.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "40.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1in4vfizxackr0kngcs56abink9gfxafhbxj5vb35na6lpxclwv7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-41.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "41.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0k6xpc7x0pyvbi3c9n5k3kvvr9hwbh6ks6b7zgyxs1a7smz54jys")))

(define-public crate-rustc-ap-rustc_cratesio_shim-42.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "42.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "19ycrypqjkydwnzkaiqqs6k0mga9qkjqn39qxmdzm51xincxs2dq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-43.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "43.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "067xpf27vqc0pf4l1p5yrr13n55449fnj5k709pxfmsjpnl5qdza")))

(define-public crate-rustc-ap-rustc_cratesio_shim-44.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "44.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1pik0zkkjyfkd4zinq04m0fkflwd1bz8y03lriri3vksr0789gfr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-45.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "45.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0ap7x2f63cnx71d9m6s9cp0nrc1qdhqhzdkpkjl3zz4drqis6z3r")))

(define-public crate-rustc-ap-rustc_cratesio_shim-46.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "46.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1mggvs7h8dn37nabp1gs0fll6gf0r6jpqxbw685kl9nraw2jc9i6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-47.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "47.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0c6v8aaxnkhps5psv307fpxap5s53sz1w25a0g2h1hg76387k6c5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-48.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "48.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0hn6m8ndnzqc101wby9a1rwsghazgb138qh79c8s5mh9hr5rp2p5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-49.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "49.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0p2vqkqqcm6ykcgylq68zrwmsjp9z8kldqyl1d6bxdz9h5v4dazf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-50.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "50.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "04zj3sqfirphqih65f1vxfx97982l03b65ywv3w9caqsjwc4gd7x")))

(define-public crate-rustc-ap-rustc_cratesio_shim-51.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "51.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0fjav9sq95cacs6wxg5pxqfzvgyifm91dx7cg41a0ag7jlpnxd7g")))

(define-public crate-rustc-ap-rustc_cratesio_shim-52.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "52.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0vrapg1aynx1107g24b6jla8gjcm6h6dqnn319kbwd3d9zj7kkcc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-53.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "53.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1glpqpw1zi3jlsj8ph93m6w47fymlnz52ddp8hpnr87ialybfcxc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-54.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "54.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "03x0qcw18029hh4j319xrxbbcxqd87f3qrbc1nlklmvgssd8hjry")))

(define-public crate-rustc-ap-rustc_cratesio_shim-55.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "55.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1dgs1b08nbwvmsisx0ki9d709k9x6j0944fagx01nranwnydxygr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-56.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "56.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "107vj4m0gkabq04n80x1m2jl4d2s714yskq0hf3jq2wzbqbnsiv1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-57.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "57.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0jhwzcw3rr1xlz3w2mj652iw41yr9jip4b36ki9xr1jm4xhprhff")))

(define-public crate-rustc-ap-rustc_cratesio_shim-58.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "58.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1fp6b03nc6w3rqjkgriy8rkgi2llplf8z2rja1s5987fxg586idh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-59.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "59.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0sczvx5pp5zwps499jsaqdm2iqw6gc9yn2rhc2mnxs4hwpqkd5pj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-60.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "60.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0090hg946n5rfcxqn3vam6nkcxcd6c711qi5pk1vp0a9fwavbnhc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-61.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "61.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1yddssv90blxw7kh35lpg60iqi2jcr9nj4r6pcgx7zp4g68kbxld")))

(define-public crate-rustc-ap-rustc_cratesio_shim-62.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "62.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "17ly5rr41jzsxr2pygs2j7l6281pm8wbl5pz293srirq8p8vxz8v")))

(define-public crate-rustc-ap-rustc_cratesio_shim-63.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "63.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "091c635a3hv49bwdkxynzyfghfd4vmcfxwc779zhvlsk9yd3fhyh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-64.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "64.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0ar8b6qwd7jbvxsf4lccgnlg2jnf1l1ka4a8qwi0cq30663ifbml")))

(define-public crate-rustc-ap-rustc_cratesio_shim-65.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "65.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1hs256w8djhnfh4w4wgqq3wb2kkszyp668q0j4ykz4qv97mwlpkv")))

(define-public crate-rustc-ap-rustc_cratesio_shim-66.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "66.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0c44c6ls34anhrnmn1s93s96lmwb06d65c0w5j74wfhxsgw8pb4x")))

(define-public crate-rustc-ap-rustc_cratesio_shim-67.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "67.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "06d9h20r0rj2akh5k0b5vxdmdjyzvfp3gmrk8syzz92hdr56xhdd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-68.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "68.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0mkmp4b3vza0wsc7jcwqzr2cjqlzj4w6v3hl36ad6d8nig1acqkv")))

(define-public crate-rustc-ap-rustc_cratesio_shim-69.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "69.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "189yib391cppmv02g86kk0ja38dmnrbx0my2ns38wzf6lcpp19py")))

(define-public crate-rustc-ap-rustc_cratesio_shim-70.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "70.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "099vkbl53lvqrabdxqnb96z28zh11j08sgxifgkfjkdlr9v9q0vf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-71.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "71.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1ay52hqx0bxzway25spmhgnfkgfrs0ilsh1bwab6xx2kjldnivvs")))

(define-public crate-rustc-ap-rustc_cratesio_shim-72.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "72.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "04pycdijvkqr3d6snmc2bjm93s7h06yf86sf91by529lssxgprnz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-73.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "73.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1rmqr6lrf0vm3sz8vxw6732dp0zq918afmv0wh367h164ki644j2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-74.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "74.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1fs27345q1r0cpqix7r9vyafmbl35zs2d2hz6lj5d27ywymlmqlk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-75.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "75.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1fl10yyks5k90v6kag6w82iggqmqr32qqd7nr33pm11shwc5zlis")))

(define-public crate-rustc-ap-rustc_cratesio_shim-76.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "76.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0m37k8wf178r89vcw9dpy98lgl26f0zmxrjycmsyyyiysa8k6r47")))

(define-public crate-rustc-ap-rustc_cratesio_shim-77.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "77.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1hw1z877q8zs54xz8vyabsvf9l3g5sga7gbj87z6zmyhvkkyvxpm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-78.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "78.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "06m979p0li1qa30vc0byz8027cl687bm624g0qa8xd1rzjakimcw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-79.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "79.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1r5p0532c9ibmks9j96x7mrr097vm7l3kckzsjkfld87h1x5w9gz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-80.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "80.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "10v12war2sg8qk7i2j4vkicc0sfinaq07g8m7rhb3kvyr1zzcka5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-81.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "81.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "06ghz11hw27ha3z5n7q83d5h8ly50zik0w8w5h07iw6asv8j3m8g")))

(define-public crate-rustc-ap-rustc_cratesio_shim-82.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "82.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "14v44qi91fhk364gx3r68r0wj88j2d3b6iq3736zgcmib5v2c23r")))

(define-public crate-rustc-ap-rustc_cratesio_shim-83.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "83.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0i5294h7vd4xzkm4jczsch544mmh362qqlnrqp4c4yqbchigzz2i")))

(define-public crate-rustc-ap-rustc_cratesio_shim-84.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "84.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0s5nw8r225pxjn3fam75h7k5wy6ablvy4hniva1bba0kq0yjyqxa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-85.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "85.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0zdfsirals4hg265rhlhvgsv6y3njljwz5hqxbibm1fhzzv1f0df")))

(define-public crate-rustc-ap-rustc_cratesio_shim-86.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "86.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "14y4pwr2s6gl3197al8vwr9z4ll4f15crr0gs5w7lvn39k0gshqn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-87.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "87.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1az0szsn5153c5v1draj019k0z03ik977isli54dggynccc7amms")))

(define-public crate-rustc-ap-rustc_cratesio_shim-88.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "88.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "12g014gy9m1qbv7zcnwq9lsrzw63aryy85v35ah9nbkp2mm9i6h7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-89.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "89.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "096wx308c49d617i68a608vgi18yjkk3ibjqwxqwsx448rlxm2x7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-90.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "90.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0nxb21kbmpv1vl7z0frxljgi01mc7mawmrcvyazddgkaxahna0yh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-91.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "91.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1b7hy8y2s50q3s3d6ld9nkxwyzwd98pygd1qyn32f8xkh0bmgmqd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-92.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "92.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0ypvnnfi6pgrvpkdpzqyhara3n6980yps1mrij3l6dwdkw9mpd06")))

(define-public crate-rustc-ap-rustc_cratesio_shim-93.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "93.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1azlcx73kr8lwa4q3by2wrjn2bqky7g0273wdzv3apchncb7pqhs")))

(define-public crate-rustc-ap-rustc_cratesio_shim-94.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "94.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1yjm9xpgvzh9frj8f9inshg9ax4yk8ic4xin3njh9251gmpj9aq2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-95.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "95.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "069k8zixkxj2i7znxxqldm0fp242sy84i4rcpzlj6jkzsgsbr1sa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-96.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "96.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0cgkxqsry43glwlmmr0i59bwp1h6pb5bnwpimf14zcpl72ww9c9d")))

(define-public crate-rustc-ap-rustc_cratesio_shim-98.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "98.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "11afx5arl54ahpc0mfc7j6pmxycyncz8mjc2i9ghqaqdny26amrb")))

(define-public crate-rustc-ap-rustc_cratesio_shim-99.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "99.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0chrlg1dzczbj7qq8jivhp14ca3a22ygcdks4k68ljw4ll7xildm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-100.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "100.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1ydyw2b0c07yfmx1l4ay1sg84ixas1v1fdpn48p2ijshd21gpn88")))

(define-public crate-rustc-ap-rustc_cratesio_shim-101.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "101.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "103svrlfhvap73s8naabz3as12z0490yd2jlikwk9f89b4kh2ww9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-102.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "102.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1yvznzv6pmhxamlljpxdf81v83kjpvh7ypgvnpax0jxqzcjgsvkz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-103.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "103.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1aj5cnkyd9zwgrsv7bss8bw94wn3z9sncn3kq1bcnxx7bsn38f26")))

(define-public crate-rustc-ap-rustc_cratesio_shim-104.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "104.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "057wqqy2ky44jqhhs150lhx7wjcw5jma3qnnab30c981vfjbs4ay")))

(define-public crate-rustc-ap-rustc_cratesio_shim-105.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "105.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0lblcmis75wbilpi5jdbc32m0ji42hlyz2f9srjdxnvk4jcp9sv9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-106.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "106.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "17wx1smw2awp12x5nr6vhsklfr55zfkwzyvvjqh84kg65c04xgm1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-107.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "107.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0jm6dzshs3gh7i049za9iixfss3x6l1g2yj28rlvf9029984hd3l")))

(define-public crate-rustc-ap-rustc_cratesio_shim-108.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "108.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0dal79x5kj3yq0nzawx25sac54cp7vmrf4jxxx7rh6ly84capx95")))

(define-public crate-rustc-ap-rustc_cratesio_shim-109.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "109.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1h0n8zjywhk9wvbjlgfc9ap88d1b9vcff7mbjxh0m0lnhmymwcbn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-110.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "110.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "056m2x3w8pxnnilxg4mv4433ca1h98nqy66p9g96gi4rmlcf6mh2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-111.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "111.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "16f8lnimgrdwn9r940g28048di9xawjac8kxfq8afy8hs70rvxg0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-112.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "112.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0sj0sgzsq40n0z7rpqysmrw81rdwmfvip90jqn7bhswziwv6v6nl")))

(define-public crate-rustc-ap-rustc_cratesio_shim-113.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "113.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1z1gl3q6qrpx03qvgfdrcmk0fjjcgdnn7f8bqingak2wgiwk84x0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-114.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "114.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1wqi2a3yv5jdc22pb3nk5vg2g3m16m0yhhmr37hnwqfqm68mvljx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-115.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "115.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0azygrxhr8vqlzcnxya3g52089pd6ncqnrnsjwmlf02lkjy27551")))

(define-public crate-rustc-ap-rustc_cratesio_shim-116.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "116.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0bn5s06g9837ggx7v5fsllgfa8dl7hzsvkcnkq7kaxiigma6rjwj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-117.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "117.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1mb8qpln87wfa2hxj1lssw6yymy620nn650pvh2m93ppvay0hlsk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-118.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "118.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1akjjiysh1ffi65bcbn25hbf013mc36vhpi2vyqyklgyk23v094n")))

(define-public crate-rustc-ap-rustc_cratesio_shim-119.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "119.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1rdxw533wqn9qbbz2kq1jwdnv22rryibdk64dki3yzvkqrs3ys7q")))

(define-public crate-rustc-ap-rustc_cratesio_shim-120.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "120.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0mff7a1g923zgaz790z69jv59ybzbvaf3gzbpxn711y82sjjcx0i")))

(define-public crate-rustc-ap-rustc_cratesio_shim-121.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "121.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "19szbgd796k6md763ni0kin2ilmsn3vn18mnxkmv3qvyjnzfzxwy")))

(define-public crate-rustc-ap-rustc_cratesio_shim-122.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "122.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1vpwkgad0ydvkqckq6vm782rcr9zigxb0hp97xk03zzgs9cp5yyh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-123.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "123.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "19wfzxmcz688gq8j2zz779zhk8kkjrra2s2dmidzrcngpfva861l")))

(define-public crate-rustc-ap-rustc_cratesio_shim-124.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "124.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0jd4jblpvg1jg43m1nk560qg7w6z9c3h23xl6lxsb5ydk5w2xlic")))

(define-public crate-rustc-ap-rustc_cratesio_shim-125.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "125.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "18iqw75i4y55zj96xl1b08y4r2aabyrwwc61n9bnq7d20n6q0vzq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-126.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "126.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "02yjcpcq9dx6v6q0cjp6sdcgms07wrlsjwy27qbdnwqxbvqdd3ns")))

(define-public crate-rustc-ap-rustc_cratesio_shim-127.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "127.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1vff9gldbdblrbsjhz0axlk4slm56li6zlys6zbr0napkxds7dyf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-128.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "128.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1d0kqd46xs76jm6ks0zxyzx5b4w40kih4lq4kr4cxqz6cssa4x3k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-129.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "129.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "03dnhmw1firhqag6b1r0sj7dwkdg31zr5lc8w0y2s02rzdgbrh2f")))

(define-public crate-rustc-ap-rustc_cratesio_shim-130.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "130.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1imrqx9qpnxz9l8h7zflb7p1jqvl4qa7r7h3yfw571rz4jfdi92q")))

(define-public crate-rustc-ap-rustc_cratesio_shim-131.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "131.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0msqgqb9iwnxx3x8cxzh0kln10mxwh48qqgvigxapw52aqy6m3vi")))

(define-public crate-rustc-ap-rustc_cratesio_shim-132.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "132.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xdw7y756finwz8xqsnmawz7z9l2q9227ks5qn9hxgis25hx4qz4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-133.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "133.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rwvx41kyj0shilvmrxnjwkcjshj1ka8b55mkgnr34h8ff0xal7m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-134.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "134.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0w3yixmcpjsd41j9nl0pq404nwjf9a432zxrk1d0l2qvai2nr94z")))

(define-public crate-rustc-ap-rustc_cratesio_shim-135.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "135.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "098363gf8dmqs3zhv1qdgbccp5srls7p8y0b2d9zzd9gw6wmkrjp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-136.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "136.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "057fci5dwhnsgnfgiv87vzsd8a6f4rkc8wdl8rrpan6x02z73lss")))

(define-public crate-rustc-ap-rustc_cratesio_shim-137.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "137.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g1ksksig5nb2mfr293jcy9dyfdfmsr2nnmp2ny1hpn1anbx08wk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-138.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "138.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hdb9zh7x0i5qn4572h6icgdrp7ydmbckxqx8vpldblcby9c8zci")))

(define-public crate-rustc-ap-rustc_cratesio_shim-139.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "139.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01jlh34xx8skpb0hdxplk1y0mxw8id1chvdp3i1p7dmjhmbndrd8")))

(define-public crate-rustc-ap-rustc_cratesio_shim-140.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "140.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xn92ds496dcwbdgaycycwwfx6p8z7ir7f0dxwxagsz250c6r2vw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-141.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "141.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17d9nmgggidadwb6k4h12hagcp6sy6znbjnh736gb2bb55213izg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-142.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "142.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vc9rrdk9ffwcz2g0d5hmhs89cqg3q9720shwgzccg431ka3xdb2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-143.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "143.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h9hlfbzqd22ra6lh62ms8arhk77dzd42inf6iyx2nim6bih6vw5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-144.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "144.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05kmdvlmiq44ibzc2rikqkf8lyg417442dg1yxz6y2hpvi01nl50")))

(define-public crate-rustc-ap-rustc_cratesio_shim-145.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "145.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bifmcnq97q481pwrzv8r5hbrann6gz075xvyjmd7ac6p63rq8z9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-146.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "146.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0c04kpsr1haqi526zmzd4l8wwpz1d9jpr2lf3yqv7x47v2lrggln")))

(define-public crate-rustc-ap-rustc_cratesio_shim-147.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "147.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sf05lmcqfq0vnw3xfkm0hc70a1kkgvflgz3frdqgix3mw7ws7bd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-148.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "148.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14mp026r8j57fjqrj8r9ds943i8lrcjk3h8g1476hknb220hdbic")))

(define-public crate-rustc-ap-rustc_cratesio_shim-149.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "149.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c4lg3y31w4pm4w9y28v7zjg5wag09yd7dxl469bnbmqxjr430m7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-150.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "150.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "174igcq1m1iwc2ximds9b0p9s676q44lms4xiyhabldg12bmzz8r")))

(define-public crate-rustc-ap-rustc_cratesio_shim-151.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "151.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cwya1cmsgdimpls8j5lknfarmh0fj2yv7kmc6n8nncfxclvczw2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-152.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "152.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0kdr37xkrv8n8wbhz6nzfj58lpxj5cml36y88wh7f0wrj6rc5gxm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-153.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "153.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "045q9kcgd8k4zgb2vzgsrzyjfysj3f6hbwhsga8lz6gqx4aayzdm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-154.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "154.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ydjxip4gh0n7bmkc3x5a692q2vjg6410lnx0gn7fwba0fnzh284")))

(define-public crate-rustc-ap-rustc_cratesio_shim-155.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "155.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0c81pwxkg8gdqxf8qb9h2wci2m32pr80d05kymadz0py6blviwpk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-156.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "156.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1r8q5ymp0lw5g7i8659fpm1rang7yy1nx0bifx9c2rjxvkxai3hy")))

(define-public crate-rustc-ap-rustc_cratesio_shim-157.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "157.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0707bgxabbym7cr144i8h4h6icwraz365nc3x3y19dnhn7vsc3sn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-158.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "158.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kvq917lsghlmbjnvkaxf4ixv5rfkfdvv2ybqxy6szdwcp4v4xsq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-159.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "159.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "036y33xbjimvk2ispapsv9v9bzsqj351lfrkqlqcravr5xspisr2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-160.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "160.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "145zbrb1phar69cm00nysjzsq82far26a4f7d3xayh9lg88hpmyy")))

(define-public crate-rustc-ap-rustc_cratesio_shim-161.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "161.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "186vm5l7cb4h7mgd1n5g3pzalwx0li676zsnkz5sbn291k5ialys")))

(define-public crate-rustc-ap-rustc_cratesio_shim-162.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "162.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fw24imbwpf89d6mdsfmdl2zqp4cr8xhw6w065jjgbd9i1f1dk52")))

(define-public crate-rustc-ap-rustc_cratesio_shim-163.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "163.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "065qgf6lf34v015xwk7z3ak3i5hi1g3y6yym1qy3mbagrk7wgfy2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-164.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "164.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19xb46hgmnc1lwad2ss9spp5inv79gardaaqq2rwsfwz40zbidbs")))

(define-public crate-rustc-ap-rustc_cratesio_shim-165.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "165.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1s57g8iahrzwqinj8z34ysrgjy00yw3zd4pp5l8nybc8435pqbi8")))

(define-public crate-rustc-ap-rustc_cratesio_shim-166.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "166.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1f46i8zhcdh42inipzfkrb4ywqs0j10lwmq43nrks15f2k7s92nm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-167.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "167.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13fmqikb52qi2wwv5xffbkjicmxbw34cvz22kpqg7fl2k0pjrhw3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-168.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "168.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fpaq1y8ywcb5p1bh93l9b89fzhv2s4j1k18x5k366mgkk2qg1l4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-169.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "169.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05i1zkdpxs2p77irzdxbjmrvwg0197w49r46rgnsmgvbizcnbvkv")))

(define-public crate-rustc-ap-rustc_cratesio_shim-170.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "170.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17w7747hwr1bmsivlg6raa5iiim8vanq1h5w80fq264b4nldl0li")))

(define-public crate-rustc-ap-rustc_cratesio_shim-171.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "171.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ysldnyrv6gwckc6zc1391x6njdpx4pz980a0zy2cq5dgan8gs46")))

(define-public crate-rustc-ap-rustc_cratesio_shim-172.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "172.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1i5f9h3vsw8wrwi4r010cx8js88by3icfr3qhclm3a9anf6gy800")))

(define-public crate-rustc-ap-rustc_cratesio_shim-173.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "173.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b8n3i338d06a7y445hn35i41q64w5maq0zg470ci1gz0ys36hk2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-174.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "174.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12nl505nk62m9yalcjxm8cjvbi0mj61631mh7x41h55pjpnfwc9k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-175.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "175.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qg8pl63wsk23y4pzp2wihg3v4pk1nc98wbp09zmggd0iplwsg1k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-176.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "176.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f5im9kyhvmwmm1gihwlmdbmzjn47iqhwr29dyi4s6hav8s5z3mk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-177.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "177.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13ahn3ij9gs7c7sr12sr4yyf6f5y20z2b5hj7fmr8s7gzlwvc766")))

(define-public crate-rustc-ap-rustc_cratesio_shim-178.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "178.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mqfdhsgchlvbqiwr8njl0084gm6kb3wxp3vy1wcq47hf8zxbja9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-179.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "179.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13q5n97q4s0523cq11rydxqq9958xc366dh6ycqnivwdqzyiig7c")))

(define-public crate-rustc-ap-rustc_cratesio_shim-180.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "180.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01kaix6ql3pafbk91zm2ff7qv6179ycxnh77ryql70iss1k97saj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-181.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "181.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "03hplqn8621aiaq5a75109pihrjkdw9a8m3jw51ky4xzi71zm9h2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-182.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "182.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dz8ndqxw9rbjv7fyil1nxva7b2yk5ssynxlihk39673c4li5g71")))

(define-public crate-rustc-ap-rustc_cratesio_shim-183.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "183.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fbhm4fgr2wp8n8vl4x838mfwzhsrigy5wbr15mllmg6l1scczl8")))

(define-public crate-rustc-ap-rustc_cratesio_shim-184.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "184.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fz5hvq8p437g95q68r3sgrr10jycf0x67swrij75a64xhkvgrff")))

(define-public crate-rustc-ap-rustc_cratesio_shim-185.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "185.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b00h1114k5wif1xfin4w3di4wis3cqzbdqsnvmrmmn1xvic52hk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-186.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "186.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02zd3ndrp279sidqhfajiis7lj97l0yd8kw3fcz5lg39yx0yafhh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-187.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "187.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0q5wwjiscc0dm2gb0iz2x7m0mddpvkihccdlwc9yfjh170wbdv6w")))

(define-public crate-rustc-ap-rustc_cratesio_shim-188.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "188.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "18qk7cgkw65ripv9ychf9fx4389dsphmsz86c9wi5ljjp1fb5sf1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-189.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "189.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x8lahcs6klcnyj3nh672ghvfw0fkx9pdxm3yx1paxf1lpwdd0bh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-190.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "190.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1y8wpvqfs72qp9zhng7spm99pyrkfh4sbqhyr75mcmwxj0v0frvq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-191.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "191.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h8kv5vsc3gd4zjglj3ccgvqy5403xzxxz03jn9gg79wbhcyy3fk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-192.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "192.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0chhlxvfflqmi203p6xdfkn6kclqc83gx7qn4mx1942ic0wpf75l")))

(define-public crate-rustc-ap-rustc_cratesio_shim-193.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "193.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0q19p17hx1vrd1vg1zvbh7kfsglgc60s720kynz5vcsfr8pqxvfq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-194.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "194.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17agamhk0vq26rxlpp6ayw0asnflgrnfhakhm35klh66dbwxp271")))

(define-public crate-rustc-ap-rustc_cratesio_shim-195.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "195.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0av08ncdy5kdbfw2bqdiv43lzhsrihgkz6yxswhnrp9igyvi5nw3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-196.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "196.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15ypqgfnxb9a3siz1v9yjx1fzh0m5g5rz1l82xh4qw4g7b3q2xmj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-197.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "197.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10kq0l59rafggfblpw1rfqk05g0x7ndqn1l349899fjjg1p4svhn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-198.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "198.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0q9ync6savncch9hsvpifn24ikqsw6ifibl8q089gyxkn2yg2f3m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-199.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "199.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nr2xbh447dss9kyvxc6rgf2lb8gr6yarap22l35h65ywms5w6sv")))

(define-public crate-rustc-ap-rustc_cratesio_shim-200.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "200.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lbi266vzmhbm8sd6f5qngkxsgc17z21xiwfvklzkxgv3mjg0004")))

(define-public crate-rustc-ap-rustc_cratesio_shim-201.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "201.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qqdgr8m8pj914cj60k2kfsswrzpl758gxk1b9c1f2qdivjg4wir")))

(define-public crate-rustc-ap-rustc_cratesio_shim-202.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "202.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1q96nbpm8sfvh6yp77pjrsw1vxs5v20qvd24p60dal804vq861cm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-203.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "203.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14bkz7g9k3wn1vjpzhdmffc6j58v01ww105pdsd7mr4aq2fsjf44")))

(define-public crate-rustc-ap-rustc_cratesio_shim-204.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "204.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15i8sxdkqxv15170lg39wwiqhlfqy12wf72sm0rv0w44z6a12m7x")))

(define-public crate-rustc-ap-rustc_cratesio_shim-205.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "205.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rh7qdkgr0c47687a8p8v0gx7r4kqy8d2v9c3wiyc2psvprw7nys")))

(define-public crate-rustc-ap-rustc_cratesio_shim-206.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "206.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "178phmaz41vk8yd34n9akdzqgyk11rdh3rz04xilqmvkgjkkg41b")))

(define-public crate-rustc-ap-rustc_cratesio_shim-207.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "207.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1riy1h39q3pzzp8xd5zbf8axmbpyrmrzza5rkcssn5bp4bmsmvdw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-208.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "208.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qklppk954m37r3x18xfcpxmc869lmzj1mc67lg5rcfgi5x82drg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-209.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "209.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v55cja6x8bgrwmq42yksp013g53ys1bjgvlr9dhiyqlx8yr4kgg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-210.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "210.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "100mmyldg8mcjdmfx2sy4sf7mm4vz3h05w2bs57ya8ldpaqxx676")))

(define-public crate-rustc-ap-rustc_cratesio_shim-211.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "211.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17nsh76xb0g1kwac06685sbdv46yy3nka23fjxdfdfpdbdhm8p8g")))

(define-public crate-rustc-ap-rustc_cratesio_shim-212.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "212.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ajfhvmymrm74fw7j5r5g7p35wi88d67lhq96gpn7fm2j6ka3cvs")))

(define-public crate-rustc-ap-rustc_cratesio_shim-213.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "213.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17glpki5xxaq7p2544cixc6z3xjrn2kwvn0kyy06bn13ianrynck")))

(define-public crate-rustc-ap-rustc_cratesio_shim-214.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "214.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h3m6my07hc9v3mdl8shdbk2splinfvc141fzx3kr3vskiyl1ggf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-215.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "215.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lv1dkmdmw6czpmyc9h35kixnv3wp5bs5qhqzvvf43mx8wscs1k1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-216.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "216.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17mwwsi49nx5qvgqbm8bh6vzkga9cws4ldnkjiyb2wn3x6hxr120")))

(define-public crate-rustc-ap-rustc_cratesio_shim-217.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "217.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x1jf0zf282pywpizysqiv4mm2q1hlygvm3zcxhn5n82mw6fjn7n")))

(define-public crate-rustc-ap-rustc_cratesio_shim-218.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "218.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "124v680xkhp08q4xrcp4bqnfyiv7a8y97yq050g0r3g72xca98f2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-219.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "219.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vcn0iyyjcnf56lda8lrvrd0xi7kmc3lxhvdafd5x3i1dqfv060i")))

(define-public crate-rustc-ap-rustc_cratesio_shim-220.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "220.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "098ci3122mwj4d96n8nmnk08lw0c9d5ai2c2fhi1w74f0wbiklri")))

(define-public crate-rustc-ap-rustc_cratesio_shim-221.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "221.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hdmhnnazf07zplnvpvpzxsmikgkf3xk7sdm12gww6wjvb71qi1c")))

(define-public crate-rustc-ap-rustc_cratesio_shim-222.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "222.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xq8j887hhck93hc2s6rxjxrj7pkx2nwssf9ks3hh5kc755ih66l")))

(define-public crate-rustc-ap-rustc_cratesio_shim-223.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "223.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zw121bk1z52mqj24pvxnb25fqf53gjrni4w8f3gxn6ydyzsyqi3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-224.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "224.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13as0mprrg10xvd1ws404zg24rv9wsvyypyj65g67mcz4i1v0gl3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-225.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "225.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vfzmlw7mbw1v83zssr03q03cdx0mzzhbmc7szlk04ypbcgvx0am")))

(define-public crate-rustc-ap-rustc_cratesio_shim-226.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "226.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05abi24i62yyrzh8dav51gilv3zi8j8p1sf1clfp64s20fjb9wkn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-227.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "227.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17pdfs71bv99pdk09q1ba8djyykbl1ggx0r011y16fvq435hx1bq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-228.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "228.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nqnn0wjprq7q9w40sji4p53vgy9ywrqws3y8afr9hy5mrd774hm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-229.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "229.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h920asf4mc98a0b194w880ka1d5q6kdb36z3mj89si4d61km6xb")))

(define-public crate-rustc-ap-rustc_cratesio_shim-230.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "230.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1aw15mxjs0py4ml3ayfxxlhssmlcx0al8d53gff5gna4s8n30wg7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-231.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "231.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vaw3w7arfbbwlicgaq69082jnxw4c6nflrbynwa7jgrkchq8i0i")))

(define-public crate-rustc-ap-rustc_cratesio_shim-232.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "232.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rfpymi362vpsv34rnfj37ars4rk9nkyarzy6xq5c0262mi0kf90")))

(define-public crate-rustc-ap-rustc_cratesio_shim-233.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "233.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qrf280dzyrb3ymy5m3y68rvn1smhhqrg2fg8w4hp1kwyby3a6b6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-234.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "234.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1i2155nwyv6vh37mzqlmjkz8ccvli34l26p5xn53bgm6kapgvqzn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-235.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "235.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mcpax4nvwq42rz9iny1zwn6mfw8s6ardyvg11n3bza62dmifb08")))

(define-public crate-rustc-ap-rustc_cratesio_shim-236.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "236.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kbgqwc7x7xb1zgacp89n2ndaa1bl01bbczklc39d7rm16c9jdaq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-237.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "237.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mmd5cn6q7dgv2idw07ix0qp8svdg40qf8n8jyf4xvnidk3h4nww")))

(define-public crate-rustc-ap-rustc_cratesio_shim-238.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "238.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rrirr5s80w0660i5r3dbfzay2ng9kigvzz6w4jr2vkfhfj4z7kd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-239.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "239.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0035k396mb84x30kix062wm02b7hw09xpmnnbfv3cj4l32i1ypay")))

(define-public crate-rustc-ap-rustc_cratesio_shim-240.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "240.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0i07xiqq26agyazdijgjc3fxp31zlv0215gwkggjkp7qw9hd88c3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-241.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "241.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hj7qzvhnjfpvns6vmgpfgq1gp19n2311izy69rd6skp4hd9vs05")))

(define-public crate-rustc-ap-rustc_cratesio_shim-242.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "242.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0c3ii4ng16a479rfqpcbh7jyy2bba0yynzmkdivnc69dg9wa30jz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-243.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "243.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0239dchhvpa0jcqmd85i2g5ym7d8i6wpqwxpi72lha79wx089wv7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-244.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "244.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0i01mad9dgamd5rp7q7fhji0avspy2vlc93ny677cmak8lb6gigv")))

(define-public crate-rustc-ap-rustc_cratesio_shim-245.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "245.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0q5n0whnc4c7w5skvkxmbgwpy8fv073hn24wbv8v09d49n4lln84")))

(define-public crate-rustc-ap-rustc_cratesio_shim-246.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "246.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nqvyab7bn7sssky8p9njj0k5kzgz9ixdh8x8a1dnxciignw9yd2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-247.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "247.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ppcybkffp9n78slh5gidi9ywh9swyfwqhjiln9ghymggyx428f3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-248.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "248.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1yqpsq9h1988crnwdmdcqs1x2m8nil6iil7d1rnc8mj6bf61r4f2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-249.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "249.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "11cl9j706hm9igrrv6i190379y5vx7dv3s400308ilj7m8c44y6m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-250.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "250.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "15ij8y2dr35srwjvmx6ip27lrkjhxwmmfr236csjp1wfxymhvz1w")))

(define-public crate-rustc-ap-rustc_cratesio_shim-251.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "251.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1qywkhlbwjxan1r862cd1hz3vbvv7palqc4vb6i2hw5fspaxwc9k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-252.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "252.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "12883d1gx5pa4c21aw7xyakzzqiqw0k2p2lyikgcmp0abqdxdzyn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-253.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "253.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "14hgh1rwyz5djvqaff92783fx8h6l97xyf4v9n4nx5vsqasqbmwn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-254.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "254.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0x1qq4fm049dcp6hachgmi9ma7ys81z7qqr2fqazhhxxisyjyqvx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-255.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "255.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0dinip3lmfxb5gl4h3w7ifxmi5lfvfia599qvpvndayq30r6nv04")))

(define-public crate-rustc-ap-rustc_cratesio_shim-256.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "256.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1s2sk6650jsslgzl6kgc3yk3fw7s2gfcfkrpr2b4x9lssn1pz5nx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-257.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "257.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1n8vhh9aynh0kqzbxr7hw0mw7pj4ss2alz5slwaa0x71846v9k2s")))

(define-public crate-rustc-ap-rustc_cratesio_shim-258.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "258.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "10wg3yflqdzhvahdlajfr7wq9q03hwrkzpkidzvagasgvf293p26")))

(define-public crate-rustc-ap-rustc_cratesio_shim-259.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "259.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0srcw1x41fflqhb9i9v77xbv6g527rwckh0x7w450j10589hp160")))

(define-public crate-rustc-ap-rustc_cratesio_shim-260.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "260.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0h9d2bnxisin5v180p98cxplnzij50ad8mlbm4118f5adi9nykr9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-261.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "261.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0nn1ysi8dx65grrn67pyis6md3flvjcaicbg9cbz3vffzlm2msx4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-262.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "262.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0lg0r4jk4wnmkqx4nfr17spq0krcqc1v5h360vwbk39ldk9q6klr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-263.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "263.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1jz7g85zrw1rc6rc54dwmzczbpwwgj8152im321w0ifrdcqq339l")))

(define-public crate-rustc-ap-rustc_cratesio_shim-264.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "264.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "06vs8wf3x78z63bm7a141ygi3w6n7wqhsmn9lhfmrv3rhfrrs6zx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-265.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "265.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "09v1i1nlzqdfcvmijl4h2aiq2ijz5p11djq3s26906qhlx1ixa6b")))

(define-public crate-rustc-ap-rustc_cratesio_shim-266.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "266.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1xbm271gjrfa2s6fnw5ckf9id8qrx3fqmpr29pw9i4xd1pbghrwm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-267.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "267.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1b8rhah9lpfvab1y0fsymbsdqryksy59h9iiszm0cvxwbiwragdx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-268.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "268.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1jamiy03g4dq9c8wk0bx085k0sin6wbwkf1337x0n6hi9hb4wm6h")))

(define-public crate-rustc-ap-rustc_cratesio_shim-269.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "269.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "119syjszgw5i41lvk936lf3k5lhzjrz775v7fq5j29vyfhnq59hk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-270.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "270.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0qp3wqrdzr07b55dlfylk7lqi2z7k1z6p8bhzxiqv33z86vvn6md")))

(define-public crate-rustc-ap-rustc_cratesio_shim-271.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "271.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1x8dd9j1vrq9kywfl8jmdfppdmjiknbsr9393cmd4b62d27d8fx6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-272.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "272.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "05m6izgwpv02s5dmblcdkg4i8kf68w5ap2bcllyrh9bvxxiglink")))

(define-public crate-rustc-ap-rustc_cratesio_shim-273.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "273.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0bjzbi1mhy2l0vchkpjp0ad8xcikppwj8yldp97s3pyqmifrvjc1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-274.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "274.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0bj16cb6slfir3zd7rdfgfl0k92f67wxb772psrlwyx924z39hmn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-275.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "275.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "04f1smh0ycs6sfp8hy683wjmk7q43mmf2y1sq9rjpz8hywj31r5q")))

(define-public crate-rustc-ap-rustc_cratesio_shim-276.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "276.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1s84ivbx2vqx38d14w6s9qgrdjaqsdf41w1aqcjqgi3n4ff4sv3m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-277.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "277.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "18hafq2773fbhs6r81gl02ff9y6lxs52flqblmsba3lgqj1jpvlq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-278.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "278.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0r777vhqh8zh9ncw89wlawv48y71y1cwcp0m77wigksm90l8r997")))

(define-public crate-rustc-ap-rustc_cratesio_shim-279.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "279.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "09ksx0y5fc0a6zkhasbmbjqgyk0hk5jmacggkns3n0hkm67y8wzc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-280.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "280.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "11kdd39gpi13nk8w459axw6ck3aiysgb1c23r7cpabz64f508nxm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-281.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "281.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "070qjdxlzhh6i05cs8hq0075600rvjzd1cia5q093lfjgzvrvkjf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-282.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "282.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0j44l6pxgcjqsrps984hj0szmv2cs0k9pr66xbjscrsydwlhjgvj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-283.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "283.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1zhh7211i0mp92ih2zm0h4r409b9cckafzx0vn8hc82gpgmssqyp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-284.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "284.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1y2rwnsbac8zm3z3v6x48p94fc0ysri9nr2lxxs0yxigy35kbwjq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-285.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "285.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "05kfy0kw821gqkrgqb3q04c42x7xf05pz1xc8fqbb0mkm76bp7y7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-286.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "286.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1vcs190zy40jzcz6v0ahgcjx8dkazra737ni99pl8m66x9f55ig7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-287.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "287.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0z86rs9vazgpr3krmz7bvxz6g9whajp7rdd2ijigh9m2fb1vl5ay")))

(define-public crate-rustc-ap-rustc_cratesio_shim-288.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "288.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1gqxzx7lzf30lsz97jax27x5bkzrrswhs3y8avfn0ccxhvhvg2yj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-289.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "289.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "086ybs501ahgqydkhdpywn76a0vgdxf9rxir1b7661nvy9shbhmx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-290.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "290.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0m1vjsl91nbyhxlskpk5kli8h1xdm42vjmw9mvhslccp5i4ggqn2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-291.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "291.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0pql7fwa83yiphsi0fig6xda1s8j9b9xff623y3zhdh3ms1p5pgs")))

(define-public crate-rustc-ap-rustc_cratesio_shim-292.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "292.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1pq57g2pb31kcbps94yzjf193x9zy082a3c5dlqsxkgyrbvh6rvx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-293.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "293.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "12xnqkads5zwrddmzgacwq8sz0fmh772f03z7dyhwn8pa6f3l6jz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-294.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "294.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "15s7lmjp09m0vpbh2bapgjqnfm08q5kacqr6119q0n94z9qaq4nr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-295.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "295.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1cj562h7m0kli0362kxr4n8y1apicmjhpichqb41qxsyb7ld812v")))

(define-public crate-rustc-ap-rustc_cratesio_shim-296.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "296.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1hpp7ifp2bjlwsb12zvmz64xq0h02nqz3ilyf14vxlwnh7rbziy3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-297.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "297.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0prxfn6bi7zhiaqdfaxgy74cknawj5z0jnmsn42isb9khbdgwwm9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-298.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "298.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0d348ay3c12zzfchiidqhyi5raqyx8i7smb9clhni6pxskiy524l")))

(define-public crate-rustc-ap-rustc_cratesio_shim-299.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "299.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1dd6v0nxj5qfjq372ikian7wlzll46vy9ryylj92yxmcrix5jzqa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-300.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "300.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "05gy7xyjgh21m6xhjrm9ddcrf7yrx3mypg8i4cmm9cgznvqlhga0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-301.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "301.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "00l19mywfxcgljb0139c2cvhfb9bkaqpybviinhp8xc0r4qpfyp2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-302.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "302.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0r82nmw30vzvsc41d0rcxj5lni24lgl8svz2vdff0g8903v58n19")))

(define-public crate-rustc-ap-rustc_cratesio_shim-303.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "303.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1n9ddxhsdm4fxwd2mns11szdwdc5a2x5qn3c2v2k9xikjz17aimh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-304.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "304.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0m4br5rf4ppprv6m2hf26bfagi03iiniaq8krdkbaidfyi74rnai")))

(define-public crate-rustc-ap-rustc_cratesio_shim-305.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "305.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0s4m8r07gj0grp3hsv388x11dq7l0qbjjarbdxnrsqrrnfqsg5ng")))

(define-public crate-rustc-ap-rustc_cratesio_shim-306.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "306.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1qkjqiswsk4bcx88lic1k7z30nd4liy7ayqd4j8fxws3nlrcy8dp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-307.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "307.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0g995g5zvkwivw4z4dqla0sdp8n8pg8rd4fx6pm90sq6i8n69z5m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-308.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "308.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0afrmxymjkj162wydk5kk51fi69m11x8l834nacmc2rkgzyxxyvs")))

(define-public crate-rustc-ap-rustc_cratesio_shim-309.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "309.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "16qmdirrzd143n4kymipl1vlzsfq70d0i1a73s9vcmi86qaab9q4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-310.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "310.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0h9y4q5adqcyvyn5v00nz4bgq76q844l660yxr4y3a5lpgmwsgwk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-311.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "311.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0n2gbylla1mmwd6ajfij44zpy8r2gwvircw3ip996f6ia163ljyr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-312.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "312.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "02y3b6zv94kxjwgi5b2khn54ilbigc4h15rpcmdzin4kg6axglfg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-313.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "313.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0gcp80zk4myl71d01blk8w4phrgjwds2jsv8wp6f6maggnzx301k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-314.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "314.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1i6fz0w1abgpm4b7cp2ypm8b8yfjxfrray5fwj9lyqrcz4sp18xa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-315.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "315.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "001ciawmjvn954dv7yhrzaw1mjgxiiqr8nhydn6f6ybwsny9xrl4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-316.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "316.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "137w817ijl3nnrz2qb5vb3m8d8l54bmjq3xfl1yrhgm2cnd1jxwh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-317.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "317.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1ixsdvchrwxj0hwiarr47rgmnbs41sqam011kq5lbzh24rz6lc8q")))

(define-public crate-rustc-ap-rustc_cratesio_shim-318.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "318.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0irgl36w9wz8b8fmx2i176bn7w0qkdywy4wwjqkpx1zsgvz8vd9s")))

(define-public crate-rustc-ap-rustc_cratesio_shim-319.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "319.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0x64mix794rrz4y8r9n4yydnbp18zx07d78wl39rhcmfq552kajx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-320.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "320.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1vg2cs2vvx4w2pgzb39z1rlmnkhm9135zhz9l3ynaz6waxz7rzv2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-321.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "321.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1lm31j35imxydmhmkvsn96znvx82yahnwbhj9jnv62rkyzgr0lp1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-322.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "322.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0ywggm9x8c8hi58kk93k48np8gdmgmifij6c5ips761a3v6yh8zn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-323.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "323.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0nbmlg9lcal7vwaa2q2hd81459bn9qb5dpcfrymqpz6jgrwz1znb")))

(define-public crate-rustc-ap-rustc_cratesio_shim-324.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "324.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "16y1kdc0l1b6r7rjavk1dl4gw2bry3vh1ijcd126wyhgnam3326n")))

(define-public crate-rustc-ap-rustc_cratesio_shim-325.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "325.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "092hcjnsvjzpw1g1bw2byqp1hyzr937rsp4q6wid9c4svvd1k1nn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-326.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "326.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1k61pzhr3gnwk9xjschh4djrcjfi82b0hzwchbg9c2hkpgrh57k7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-327.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "327.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1dfim5sglspx0s503pc7nics7bdby01xydq4hx3fhspkr4isy7zd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-328.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "328.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1w1x6c49jdrrb1kwssfhpb28g48rsdcw33m8z6n5xp9kl55pgqa3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-329.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "329.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0zpasavph8ny57wh7d4ys9fyq1wb8p0izf1czmpgfbid1nz45b0d")))

(define-public crate-rustc-ap-rustc_cratesio_shim-330.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "330.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1n3gqk6jn4qq6ziqz6la8h5j5vr52ri8lbw97z511627hvnzpsbr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-331.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "331.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1x25lk9dk4x7191j45sjchclsbap374yfi1mw3r3k4g0xl0j0qk4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-332.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "332.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "09f9hc34ncnzpm0f9w65bb5k5s0mxypfjb6qn4lj8xggr8jajyb0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-333.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "333.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0wdqigq4jp5jfbnspsb3f9yxj8jr5zhxiw3i8agm9fx5x5g0ggm6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-334.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "334.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0rd5z4kr048d9sx7xpyhz9lx881fmbyl1d4kk270ci8c90acidv4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-335.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "335.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0h386yi296gcvysxmy6g0dwbvb75d3q706bc25mfja96m3grf56a")))

(define-public crate-rustc-ap-rustc_cratesio_shim-336.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "336.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0jqwa611g6l9yrqrvviq77nvqccln0283zjzlwpl47b78xxm2457")))

(define-public crate-rustc-ap-rustc_cratesio_shim-337.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "337.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1ga1x9819q71yv7ykxzw4f0nazf1kijcj2rkq9jb8lwn4sn1qr1x")))

(define-public crate-rustc-ap-rustc_cratesio_shim-338.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "338.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1xc06ihcc1frrvgwfc92xqns2dqfdycaf06aj0zhsdnrq3p36i4s")))

(define-public crate-rustc-ap-rustc_cratesio_shim-339.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "339.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1r20g1f1w115d9i94vmrcml5n41cjbynszgdssapyrmy90jfwik4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-340.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "340.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "107dm72hbabdly245phvggay7p8cd10d58hssxfljmf6q4kskg3k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-341.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "341.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "18f22m53z0y23gq93ngx4q822b7liv34afhl415bqkfccj0wxngz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-342.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "342.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "14ma1bv4wc0k7l0dpygzi56bbymrhzwwfmxrb0g4q4zdxbgyxybz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-343.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "343.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0dh0ams5lz5m338kv3y52csiw891qjp17nachhmac0yp8brl24zf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-344.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "344.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "13q23vv12w7f3wn12d2sqcr6ir82rja7r5imc38sarrxnm3igw23")))

(define-public crate-rustc-ap-rustc_cratesio_shim-345.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "345.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "15ag926acfsyv4kfxrhw3l3dpmzx758wfimg89m1jhyg8aq0szk4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-346.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "346.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1p6l0y36jnmzx8h3amjkwi0z10xqc43wsjgca0xkshf9x9d8bvq4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-347.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "347.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "19dlpicnwkj8qfvzbpf58d70l3mc4adly8i275pbk4gjmbp7qvww")))

(define-public crate-rustc-ap-rustc_cratesio_shim-348.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "348.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "08085zz66kl7479465icdrmzk3pp20x4bzs0xlxippa4l39gkgl9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-349.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "349.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0grbngb5hkp7cl2liqjf0i5craxirfys72kms4mmvsfcxg1xndbc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-350.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "350.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0gk3ys2s9xvyxmsx8icg8h852kim0bbqm6mjq8lns2nmp0iq4rjp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-351.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "351.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1dairx6sfwf2hyb0hsf159g0zch2i6gmzia0p5dm6z914iirjy7f")))

(define-public crate-rustc-ap-rustc_cratesio_shim-352.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "352.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1arm3lzpvlb48ml3h281j443yz58s1fzv5wj69hf5lhf454q9412")))

(define-public crate-rustc-ap-rustc_cratesio_shim-353.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "353.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1sgjixjxsf8x40fz147qh7xmminilfzsclmmkx5hkillcra8a7dv")))

(define-public crate-rustc-ap-rustc_cratesio_shim-354.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "354.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0mkhvzs1x56ckdn823z8y8lk0b8x6m774dr1j645mki9h75r0mx5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-355.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "355.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1d40p0hgxi6g46ddaarw97nfzgapb8irq06sd1l4qf79nlhycfh5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-356.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "356.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "114cbiyva56k628zvv5qj2fp7njwh1ylj3b127d7jjxy7c39d561")))

(define-public crate-rustc-ap-rustc_cratesio_shim-357.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "357.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1iam2sxk6zhfwjzaqfxa8l2j8g14wn01wwzqr2v9wd6jcg6ri3j5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-358.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "358.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0bs3gq1j79knlwr5kv8mwka5gjrjinhss49jkyikdzraxflzs5sa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-359.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "359.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1gxhgcvpzsh496vm96gi38lmsfw2qy09bwqn7pzz4h2894yf39mj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-360.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "360.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "061j9mw0bns6q10hjw6c6knhgbpswq9l98zzsdn4jf7kjikrq77f")))

(define-public crate-rustc-ap-rustc_cratesio_shim-361.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "361.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "00xwb74sv1snqhbnmq9gzcxqw4x9z9dycz7fll6743l5rd3dsj6b")))

(define-public crate-rustc-ap-rustc_cratesio_shim-362.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "362.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0qp940qyzkckndyap8hvsdf4n0pxd7bxmc9hbxb6pwv3r9xfs1mm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-363.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "363.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0w6n98n37bxkb1am0kx9ncd14nwkzxp3zp0hngm8j4g9x5zabrai")))

(define-public crate-rustc-ap-rustc_cratesio_shim-364.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "364.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1653hmwvlg6gja70cfjyfbl4kx2yjxi72kijrxmm4dyw0yn4m3ks")))

(define-public crate-rustc-ap-rustc_cratesio_shim-365.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "365.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1ycr98ys2rihy17jpn6gg08gma3afjka44cmpi84pvw91spvr9nf")))

(define-public crate-rustc-ap-rustc_cratesio_shim-366.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "366.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "11d2jsh14i87fq0w1yrmwlkk1d2v819wgh33ns4fbszgrra3a5fl")))

(define-public crate-rustc-ap-rustc_cratesio_shim-367.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "367.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0xmjp1f0rlycb688g05500nd6ak781fb0ggl0007v6ijpzbvyc10")))

(define-public crate-rustc-ap-rustc_cratesio_shim-368.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "368.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "12jrnki9zvp58432npmkdyv2qsaqqpxb696anf34r92ks5airyi1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-369.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "369.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0qgdahdfbysvfc1fvg5vprw2sp0nbk4sj1da3waj10gd1r125ddx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-370.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "370.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1lwn2czz6df16nm36lyj05m1aqkbkf9m02s54qjv0jgqd4grvy59")))

(define-public crate-rustc-ap-rustc_cratesio_shim-371.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "371.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "06ix04wk2q70f7wd8nsffd3ajpkzpbjhri7b0w71a7vb4vr4rczm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-372.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "372.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1z18fyv4xdxci40l7ldjvzq1jvqvs4v6vrlqflnkmc9mk3zrxs5a")))

(define-public crate-rustc-ap-rustc_cratesio_shim-373.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "373.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0vn49k7hmcdga6rn6n1jx54xqa5fn4x0gydxg3416041xzhsz263")))

(define-public crate-rustc-ap-rustc_cratesio_shim-374.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "374.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0yy11h64jxxv64w3n31dnkk0xbzhf89h7w4sf9d8y84kbk4pcgw6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-375.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "375.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1xvps3az314zamhnam8p34sqvwg1ig6al7jby9yxpjhwwr79rmik")))

(define-public crate-rustc-ap-rustc_cratesio_shim-376.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "376.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "02c5ya8xhlq67h1a4mn0q0f5y3nb791h422lp25sxhq4sfyhs96q")))

(define-public crate-rustc-ap-rustc_cratesio_shim-377.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "377.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "184sjxia0zg1xzav3wq978gj1mdy75fqqnwyzyvaanqyvxwyznnz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-378.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "378.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "124b9kplr4b00mp44g3k7lb1lgzsxl8llvl63la0lbbai9rfpisg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-379.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "379.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "196c9cqja87s5gcjzg0w8c2zc98db8y4qf15z311z56qx3xw8a4q")))

(define-public crate-rustc-ap-rustc_cratesio_shim-380.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "380.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1xdmfjr84i6zwxr9cn9vspqm18wb39ncvq7cvbkvgrw9rxd9bnzq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-381.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "381.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1bv24xbzn9ibb975qdpyxhibj34nk4pvlhl2z5rnf8x0695ddhqy")))

(define-public crate-rustc-ap-rustc_cratesio_shim-382.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "382.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "12n5qrwqm7zx4b7ccvsmcxn4bpw8a0b6pybd0m9hapv7lciisaqn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-383.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "383.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "154blg0dv6r330aycch47ixsy8k1kzyb1yl6zin03j4y3fvq8qmi")))

(define-public crate-rustc-ap-rustc_cratesio_shim-384.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "384.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0ciczk4gj2p1zxamwd3j2jsy475jf1sq6avk7ci3w92vl99wx2gq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-385.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "385.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1xb4p3344n068ma73nz6vncza53hlfrcsnnhs8na7clvjz1a338s")))

(define-public crate-rustc-ap-rustc_cratesio_shim-386.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "386.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0hfiqf4ajspmf9a69rp18x7467nxi70nsb5kw1mvn9wagk2k86yg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-387.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "387.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0bqmxzrr3pd7mxs2bism7b7pb8y9xsyxj0715ff4gsbdiiicfj3a")))

(define-public crate-rustc-ap-rustc_cratesio_shim-388.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "388.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "01k4w69xxji13ln8rc34dvgngk6fppmar64l2mxdv9mddiwcb3bl")))

(define-public crate-rustc-ap-rustc_cratesio_shim-389.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "389.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1iaslbh6baihxrdymrvjslhi2jr0crqnfdwnnr07zwfk236x20zh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-390.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "390.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0hvn3l22ix6iz70wd16fmiwmfav1bpfqa5vivxi2mpqfz5arwj2i")))

(define-public crate-rustc-ap-rustc_cratesio_shim-391.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "391.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "19n34pz2377vbj30x3d84nr5r3x5ni3lyh83wmzv1lynzvggj7kl")))

(define-public crate-rustc-ap-rustc_cratesio_shim-392.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "392.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "16r088nfrwhvmqisqy80b4qmz499p8jpcwqzfi66ssjcb87wx343")))

(define-public crate-rustc-ap-rustc_cratesio_shim-393.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "393.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0ahlnwqxfswfga4m1k69ivlfl588k21z9pgasz94nz4fzsijribn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-394.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "394.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1plz2vm0zh8wyhsx8rih6kks2bkgz1954mk31yml3dsciwwc8akv")))

(define-public crate-rustc-ap-rustc_cratesio_shim-395.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "395.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "179ic50712sn7ffp1j3m5ycklcnkwn1nrr3kgi14i0aa48z5f8vr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-396.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "396.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1f8wxrmiahnsmiqmd88mcq40x9s221qf6mhwc4bscr6ywqxv22mx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-397.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "397.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "09aj4pcks5avd9gy5vih596zwvd11kngc25671l80ma9898pq04w")))

(define-public crate-rustc-ap-rustc_cratesio_shim-398.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "398.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1p7di7d21bsafplmbmcfvm7if0in94sam9mvkgjjbxfazgldjp57")))

(define-public crate-rustc-ap-rustc_cratesio_shim-399.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "399.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "04y5bf4ch536rpzg1s3f9dxrbc7s9ls77ix303dsyrg41b3v7cps")))

(define-public crate-rustc-ap-rustc_cratesio_shim-400.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "400.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1ry7l7pxp1scs6nvx1kshq2x821dm7ddn01c0cyqma1qs5sjqjc8")))

(define-public crate-rustc-ap-rustc_cratesio_shim-401.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "401.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0kl58a0shgvp07g8zdihi7xaq75c6xz1hwbfmjspmiwrm1gbpbkm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-402.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "402.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0r2vgqk2z4hgshvj5pp0w7axgzdx5wv4kpi24h5m8s0q98yzpg9r")))

(define-public crate-rustc-ap-rustc_cratesio_shim-403.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "403.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0cb1nf8annr98vlk4j7xkvvi64j5qzbmirdxg7bxqdw1df1vpcp1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-404.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "404.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0adnn2gw4nmh4h22kgcvbkh0x8h56yp4jj9x4s414zsznb7singr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-405.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "405.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0ibkdki5sby90fklk19r85r5ib3nfnv8h29b23fr77qb1d758vkd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-406.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "406.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "108rihhwwrn1lj2xnkdp39bmb7cc3jdbl7cr6lm2vi9bv2pqc5fw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-407.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "407.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0n7il05zryla3skvkm69a27r98k1iiln2fb3hgd9g1fxn284mq33")))

(define-public crate-rustc-ap-rustc_cratesio_shim-408.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "408.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0jr0xbbmr4h7pswrk1nlqzqjd938q8k2rhq79y5kr6z413gnz0lg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-409.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "409.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0gmlhngxpja5nwv2qn9hks3fixqw1mljvrk4dn2lfdxzljy6qlvc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-410.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "410.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1sf6adwvjgc2zjxxys7fxwd8b6c015zs8fapmfd3lz4kdqhrad0b")))

(define-public crate-rustc-ap-rustc_cratesio_shim-411.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "411.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0lgwm4cpw97k19yjzcfp9dh7rckaxnk43dxj4wcjyv1zj1r5rnr4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-412.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "412.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "01qxadli8z3p2yp70ql7xisid1sbh017lnfqqpfrwp8sf7mxj0ih")))

(define-public crate-rustc-ap-rustc_cratesio_shim-413.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "413.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1cyfmhbh2jfnjqjrzfxmipzhgrkjw5vmasirgz7x51a93h6p9bhz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-414.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "414.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1c354aac01aafb4flvbq83862zmcbnha8g85s5krr2izm3ahgcfc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-415.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "415.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1iqiv3hmrabqv3nlv8cglrr5cndhgbf7wf3bjw53hl92nkgp9x5m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-416.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "416.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "09r3jmm6f7a6iynwdjcq6kjrs8bv7yfqvbk9ia42qbrp0ndp6b0p")))

(define-public crate-rustc-ap-rustc_cratesio_shim-417.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "417.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "09qc00hqnw4iiqmmysamkg3846zj4n15fh7ixdjr5s2bl8xxi7yq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-418.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "418.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0cz2g7grxybmwh1j5widcy69b1wk9viw7zlqzp3j605y5i1ki6yc")))

(define-public crate-rustc-ap-rustc_cratesio_shim-419.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "419.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1kqjn93wvvnzhbbzd3hrcv9wpr6x92q8jsyqv0nqfs4bskjyciiw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-420.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "420.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0l8w9h9rljfds1arbzws6x1gxmqzqwi2ddjkm2mzlaj69jhir4a5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-421.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "421.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "18sf2nq1zr1i7s2b8yqjv4zz7jhvwhkvmlavgkbz6gkq6g5lil4v")))

(define-public crate-rustc-ap-rustc_cratesio_shim-422.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "422.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1ylx89gijvavpm2mip1mlw25gfamsbfwsarwrpkwl1i1vylsfmgr")))

(define-public crate-rustc-ap-rustc_cratesio_shim-423.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "423.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0lkwn4wsk1b0ady9z2b0v6vfvhbh47ja6wqr3294ww5sh2b42p52")))

(define-public crate-rustc-ap-rustc_cratesio_shim-424.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "424.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1jjhlxvrwx8bm74a9xlf8zn2jkwswrjyhf49n9mdjr5qw899ssnn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-425.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "425.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "09ggiaw8d9pcrl92i9rxhmmh3xqh1pgapcvj37g0gzfkm9r4gynm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-426.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "426.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1f2hk9fhndfjg6c7lq11svm3a5s14q0zxg9c8i84fgg3j5c8qdp4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-427.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "427.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0l53a50ni7bkmqwbfi0dw6mpgjjnrbyd7cqx19xv7ahl6jyzfhlk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-428.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "428.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1b6a8g4k60ylxccfl9bh16dfym1gai31cd5mss4cd4pfw702sv7f")))

(define-public crate-rustc-ap-rustc_cratesio_shim-429.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "429.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0a474vhkhz8wh7m5x9kylq0az7qpxb1q7ib183jws8zhw64k3c21")))

(define-public crate-rustc-ap-rustc_cratesio_shim-430.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "430.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1xv0msf047yzd203i98fyymb1d62d20p0dwqj05zs2475cpimpj9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-431.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "431.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "158ay0p1x4zcl1k0s8sh602mxyf2ymi24dzxwfmzywx00zb3v8pw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-432.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "432.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0ikdqylx15ir6vb4llwqgm0qi4kvsj4w6r304jx1vn24bikq57as")))

(define-public crate-rustc-ap-rustc_cratesio_shim-433.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "433.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "16m674klm7hf377sx551z5j8hd8j71n54ak7pr1dx3l8k0iaa4rn")))

(define-public crate-rustc-ap-rustc_cratesio_shim-434.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "434.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "13xlpda5xnxkj9lym0hnr2nsh8fli9fpzh6w1h57qjq7s1qiv45m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-435.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "435.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1xhbl48r6y7av2ryznmvk3lyi2gnwfkbfd4rn5ik26fk1dch471z")))

(define-public crate-rustc-ap-rustc_cratesio_shim-436.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "436.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "08l9xzzyinins9zcc9m0sg9kcf9bfvazl885xdlkc5lgb6c5jppl")))

(define-public crate-rustc-ap-rustc_cratesio_shim-437.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "437.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "19x9l1zczmi6xlavwyxs18vdycyzs3b89032q894ids8hmshnvj5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-438.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "438.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0q0inby428n9rh77ynyw814ygkzjkfhr2isp43acjsvv43xry05k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-439.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "439.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0qf6zmlz3cyvs7d9lqzhn3w9k47qjy79bvzzn8x4rwybfllk8dpk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-440.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "440.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "13wp3lx0kdaxzdmp1zh6c4hs8xm36ks8nm4c5h7llf9hsv3zs9c0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-441.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "441.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1widk7cgb4jns57cvqyhq3jd2vh5y5bxsgjk4w5xivyv0jag2jxi")))

(define-public crate-rustc-ap-rustc_cratesio_shim-442.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "442.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "14whbqibx1kimgk21gxbvzihihbgpkg0vjxsxzvb8q8ypdc2hi40")))

(define-public crate-rustc-ap-rustc_cratesio_shim-443.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "443.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "14c93d2nijwpskdv23rf1l34hxp1b8rab9fngzrqbls2c7p5yx82")))

(define-public crate-rustc-ap-rustc_cratesio_shim-444.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "444.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "069piya8vq3f9gmi9nva62mhr4vml8a8467hylkyyvvmg7p6h91b")))

(define-public crate-rustc-ap-rustc_cratesio_shim-445.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "445.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1l9bbzfddgcwby7lcmv35pzppqgx6khwp9d5whl82jg0j2a5704v")))

(define-public crate-rustc-ap-rustc_cratesio_shim-446.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "446.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1g943f7h1x5nrwkrl8xqrazhmpdzdrxvd7k57dlbicfsb9zz0nfw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-447.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "447.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "00hl15l1ids2wnqg71wliml67fcnh5kk874z1b34v25s2kkzmm09")))

(define-public crate-rustc-ap-rustc_cratesio_shim-448.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "448.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1gy94dr7wvh4570fg4kq8fkvw16apdibjanbfhqsfw8svk78xzwj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-449.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "449.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0ndkjqmrxxf5rxrh0dj12g062d5l9waba989s5bxqckcqhfk3pq0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-450.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "450.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0vi0gsm29gv5h6xmrqi9liz5cabl96175r48jshgmksm70l7dk15")))

(define-public crate-rustc-ap-rustc_cratesio_shim-451.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "451.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1h5r628l7ma20mprnmi5l4mnxfwhjhynqvc8wzkcvbqicaxks4jk")))

(define-public crate-rustc-ap-rustc_cratesio_shim-452.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "452.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "193h4aqkxgr3x7bhmir967m7ki7sjc7zh3cvj3k80sx6s6nbvkg9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-453.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "453.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0dlim90brv36p68fk35m654s4jnwgk81za104wjanq76hy8hc9q4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-454.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "454.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0rnchi33b8gcr6nf8xz8bqm6vmhaq9yy33zlvz73p6yrq4zlr4f7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-455.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "455.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1zjr3mnqczmh6b5wvanhg4i8lhc853d9bwnz22kfg80ivshxv8q8")))

(define-public crate-rustc-ap-rustc_cratesio_shim-456.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "456.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "081qw94362ka6yxrrrxhi76g9s3gml7rykjgj5fa66rliq2j5fby")))

(define-public crate-rustc-ap-rustc_cratesio_shim-457.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "457.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "06w0872f9vjbhbmzligb4xisbvhc2z3vyy45msj7w8hg46s3fwsa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-458.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "458.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1s47a4sa0kvpl2zqrdm6jp232b64c45h7zs6w68q39dyiz5r33hp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-459.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "459.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1rjq89j5a8gkvmc9wfcs52ysklkd9invmzf9p3ma6fdjj5sqqjwa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-460.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "460.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0yjf9ij3nbhbnj0l17d2369365d12jcg5srbvb0rvxkx894ad4m4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-461.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "461.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1qmnqpl494f9bwrvm1z9fg317lqmqfplykpi9slm6vcdzvhwwsc4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-462.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "462.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "04h7jdkzjfy60jy3qkj9dndll1a8bqdic0srcwkmyq9q9mfk1afx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-463.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "463.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "15msdj7wvkgmh2vsr87rpq2nhy2hnw9qaphz8qp1pjncyj4qj1qh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-464.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "464.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "16vmiiih4gfcipls8r021d0ar7fwdci8zlnvdkzazm6c7qfbdg02")))

(define-public crate-rustc-ap-rustc_cratesio_shim-465.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "465.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0jvmwlqcq6b9agqfchr2x0f4jc5gy5ymfw251kj7rzigl6wisw2a")))

(define-public crate-rustc-ap-rustc_cratesio_shim-466.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "466.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1jy8s4fj1h1akph8kvhqb36v602h0z962vmwar8dn2pdycyqpgzm")))

(define-public crate-rustc-ap-rustc_cratesio_shim-467.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "467.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0y3vdcj792697gjanzpmivwl1kyl653mba846pi7688d8hcs68ch")))

(define-public crate-rustc-ap-rustc_cratesio_shim-468.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "468.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "07vg1aw2bjpfaw9kq2mis2nskwzfz559fcvn4gir31fnyn6mz2p9")))

(define-public crate-rustc-ap-rustc_cratesio_shim-469.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "469.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0rxr29l4skg5q15jqggrzm7fm43zyw4nxjkf8gprm2xn3ayj9yv0")))

(define-public crate-rustc-ap-rustc_cratesio_shim-470.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "470.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0wc92y9md52kxic5bxxaf0gq82pgfs2wr5clnxk0z4n6fdsqic8c")))

(define-public crate-rustc-ap-rustc_cratesio_shim-471.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "471.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "14w31n9fhmyy8s8qxk7ldziaw4a9vrkc8sy1pwn8qd93hl7mzxnq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-472.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "472.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0yfwz5ywgflanhldgb3a8h02h9vywpimxhhr90gmqilv6l8n2a54")))

(define-public crate-rustc-ap-rustc_cratesio_shim-473.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "473.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1p1di2j3pfwpnphg2k3ji3dv7qnh3dhhakbvawysbccl3cv5hx8j")))

(define-public crate-rustc-ap-rustc_cratesio_shim-474.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "474.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "18hhyyrqhialhkmha45rh3m5s1qwlf77m0rhxx0z5lqknl0xg621")))

(define-public crate-rustc-ap-rustc_cratesio_shim-475.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "475.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0s8pwdcdf1hr51sxya31gbxfwakv5kmjm2s9iqx95ng6vlf3q0fa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-476.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "476.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "18l8n8myz65w6xq8bgy82is54d1fs381lmb37llb05z0l2w1sxk3")))

(define-public crate-rustc-ap-rustc_cratesio_shim-477.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "477.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1jdqf48dq670g4i8xrjlq1bc8wfxpil52kjf31vlvrsn6hdz50h4")))

(define-public crate-rustc-ap-rustc_cratesio_shim-478.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "478.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1qaff2n0rp2mlh9jggvbp3zxa1f1mlv20gi3qrrqi4fsnfxrz2wa")))

(define-public crate-rustc-ap-rustc_cratesio_shim-479.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "479.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "06kzqyflq7nha9av648yw869vfs662cckvz9zda9mkl3dpq5srvp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-480.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "480.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1ylpv02vncx92kwi8mr22k7q4c4396d402vn1y4haad1zy93ph3b")))

(define-public crate-rustc-ap-rustc_cratesio_shim-481.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "481.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1sfxkbn12s38w55rfqp6a7h5x3zirpwaq4j9qxay89xgq9d5w2dl")))

(define-public crate-rustc-ap-rustc_cratesio_shim-482.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "482.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "01hhnm0kkpxwbbcs52d397rmcfm6xnjdad6m6k8kjyanjvp1j4kq")))

(define-public crate-rustc-ap-rustc_cratesio_shim-483.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "483.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1a54rxc9yvakq062zcrdjl4ydkhaph547ilppvshqd3wwhih814k")))

(define-public crate-rustc-ap-rustc_cratesio_shim-484.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "484.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "16lsrrhj4fr9hgbw887sb47js7a7x20lxsxzm4nk3l24xbsrjyg2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-485.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "485.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "06k7jv61ivs39vr8zjvdgp80hjq0fz3p2i7y7fm4wc3xaapg0yxd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-486.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "486.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0cwd25jhj2fbxf4virgi4rx2vxn41zb5gn8chwm6z3mqjpxh0hbj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-487.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "487.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "04979wczssqly8ac1653ww10hmkgicj2fvabifwci0pzd51i22cb")))

(define-public crate-rustc-ap-rustc_cratesio_shim-488.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "488.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1vhg50v0n9bma00g1ndc4r3ihiz629lm0i8gbhcl0z47b29ify3a")))

(define-public crate-rustc-ap-rustc_cratesio_shim-489.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "489.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1kybb9gw3zxdq5fr78v40wvqayz9swr9pbmri4d7syifxc96vix5")))

(define-public crate-rustc-ap-rustc_cratesio_shim-490.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "490.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1lrbgm9aa824wppgarhish0rh69qli17mx994j2rkr3blqzrnlm1")))

(define-public crate-rustc-ap-rustc_cratesio_shim-491.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "491.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1b2m97cni02zncgk1spncpc3n888zvvajrz688zd4f80p396693m")))

(define-public crate-rustc-ap-rustc_cratesio_shim-492.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "492.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "04a0k95kqj43f0cdwfiqjyawdbsnlp2ibf9q1xksm98n2lw6zvrh")))

(define-public crate-rustc-ap-rustc_cratesio_shim-493.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "493.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0rvb8bvf0wcqz7n5l2l3jlfcwpf85py3va47pcbmlc7dm3k2dxws")))

(define-public crate-rustc-ap-rustc_cratesio_shim-494.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "494.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "018ska2a6kbxcphmlkb06plq2f40x0rzl8wbf5igcvk90c2wzi1r")))

(define-public crate-rustc-ap-rustc_cratesio_shim-495.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "495.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1pi59c1xkcbpimpnbch686860356pa1ij3fhdvcd9jy61d0lvyrp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-496.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "496.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1cyldgmpi6rn0gkm3qb74l7fj4rrkdsmsx7fnhcq449zm6q9lanw")))

(define-public crate-rustc-ap-rustc_cratesio_shim-497.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "497.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0jb1m0qzddz0llj2ha89q6rynmxqba4sbfzy0wr6smfsnkr7c0ls")))

(define-public crate-rustc-ap-rustc_cratesio_shim-498.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "498.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1h0qk88hvsxrq3zxr1k29kf1hvw2x10g0p0cki1dqmhv3pd9a2p2")))

(define-public crate-rustc-ap-rustc_cratesio_shim-499.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "499.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0wwhiyanqnfwl5rzzpnjmc2pfsykv07bqwvqm7shxxwqp2j0m396")))

(define-public crate-rustc-ap-rustc_cratesio_shim-500.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "500.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "100g2k37k08agprwv2n9dpspzmrq0mw8w805nm6w8v0nyrbxjk2w")))

(define-public crate-rustc-ap-rustc_cratesio_shim-501.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "501.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "13m83wp4svagnbdx2cw8f0mcxignypngs87gsa5n34jfmk8dg25c")))

(define-public crate-rustc-ap-rustc_cratesio_shim-502.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "502.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0c2qbvgpkas7vz0gwps0zaqgml4hcm163az7ricl8v9a1329h474")))

(define-public crate-rustc-ap-rustc_cratesio_shim-503.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "503.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0rfdv3108jdvs4klsli9kxbdp5mmbmjmmgc4ix39zbd1qygpg3n7")))

(define-public crate-rustc-ap-rustc_cratesio_shim-504.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "504.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "13kflb1zqr6p9sxpkhpjkw4jvvrjrnjacv2zzwmdkr1f02yazbqg")))

(define-public crate-rustc-ap-rustc_cratesio_shim-505.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "505.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0vrpv0a5j4prlh7093ghqnrjsyrd0nrlr3sfn1n5d8i2k6di09ny")))

(define-public crate-rustc-ap-rustc_cratesio_shim-506.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "506.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1kx7z0ch999r4k825lj0qnxbfxr6jahcbkkyrvgkp6rmv98h6jrd")))

(define-public crate-rustc-ap-rustc_cratesio_shim-507.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "507.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0yssa7ycfkm7dz1y8qhp8ssmkdjnpxrk5i8jk8jjj0r5sh4biqvz")))

(define-public crate-rustc-ap-rustc_cratesio_shim-508.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "508.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "03g4f07fymgp6a5f5nyi39byskcyf4796f59g9zadiy0wci9cq5x")))

(define-public crate-rustc-ap-rustc_cratesio_shim-509.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "509.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "03s1j2gzrss1hpn6jxw6qq7b4icsjzjfikslda1kb73pqzbvlvlx")))

(define-public crate-rustc-ap-rustc_cratesio_shim-510.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "510.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1ix65lrqwi341hq8pzd05rg993ka6gpjxsvhyhd9xlscrv7znkp6")))

(define-public crate-rustc-ap-rustc_cratesio_shim-511.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "511.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0mfi2q549mahz87a31sixq0wlqfvnab9vyp1lakxbr4q6ah54j0n")))

(define-public crate-rustc-ap-rustc_cratesio_shim-512.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "512.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1dr9sdgca3q8xlivlxs5psm80qgywla7i8a07wk13jh2hk9zq90h")))

(define-public crate-rustc-ap-rustc_cratesio_shim-513.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "513.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0viiysc4zcx7z4zd77j3v23dx4kynyxzab6ffilml2npgkpg560i")))

(define-public crate-rustc-ap-rustc_cratesio_shim-514.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "514.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "19chb7i106s4h9qv7cqxsh3g49r3x6c6fb06l3h8nfw9x4b41zgl")))

(define-public crate-rustc-ap-rustc_cratesio_shim-515.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "515.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "1l5c3k6j1a4y5vlwghvxxnfjjxnflzd2qdc5zc8w6i88i7dqgisj")))

(define-public crate-rustc-ap-rustc_cratesio_shim-516.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "516.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "0g1mrbz0qisj533ggzbw6p6wijhci1g64ddp4snzkzmp29p8lafp")))

(define-public crate-rustc-ap-rustc_cratesio_shim-517.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "517.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "08m7sa4czd0225x50dkc2bacz8v02hw38gzp46l42jirjfgcw9ji")))

(define-public crate-rustc-ap-rustc_cratesio_shim-518.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "518.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "02f5fy18s35ajb258893rlypvajd185wgjf9d99mgffkh93d50ha")))

(define-public crate-rustc-ap-rustc_cratesio_shim-519.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "519.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "17xqja9i7assxwzd6crfmkzh1n6svii8jvqbmr1q47n7q8dn9dz8")))

(define-public crate-rustc-ap-rustc_cratesio_shim-520.0.0 (c (n "rustc-ap-rustc_cratesio_shim") (v "520.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.4") (d #t) (k 0)))) (h "03v6mfnjdwkk80lrif35rg1az4w2jnc1dikwlhn8s023wzzrzd7j")))

