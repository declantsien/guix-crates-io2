(define-module (crates-io ru st rusty-rubik) #:use-module (crates-io))

(define-public crate-rusty-rubik-0.1.0 (c (n "rusty-rubik") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "12hsyzj5vw4f4d08hfd86gdjbn0pv7ynywkxfy26nzqxg7sbl70l")))

(define-public crate-rusty-rubik-0.1.1 (c (n "rusty-rubik") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "1fgfymbbs8kn1m8jz1zbjh6pajbkakfdj210s8qyah06fgi2rdap")))

(define-public crate-rusty-rubik-0.1.2 (c (n "rusty-rubik") (v "0.1.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "priority-queue") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "1k6crvmmwi1x2r3nh6pd5l6pg93ml59ni6chvslcsjjagprqlm6h")))

