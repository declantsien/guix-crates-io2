(define-module (crates-io ru st rusty-leveldb-arc) #:use-module (crates-io))

(define-public crate-rusty-leveldb-arc-2.0.0 (c (n "rusty-leveldb-arc") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "snap") (r "^1.0") (d #t) (k 0)) (d (n "time-test") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r ">=1.21") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)))) (h "1raymnpldrj9waiq5kr4zq50i896w6bylm1aj9w1491hpc9ixcpa") (f (quote (("default") ("async" "tokio"))))))

