(define-module (crates-io ru st rust-algo) #:use-module (crates-io))

(define-public crate-rust-algo-0.0.1 (c (n "rust-algo") (v "0.0.1") (h "051shbj660dl3gambzm6mnskbvkwx8by7k1gv8sxjrfx9gd25l2b")))

(define-public crate-rust-algo-0.1.1 (c (n "rust-algo") (v "0.1.1") (h "1ja9zr3d4xmy64vm7b7f6ahwpwdf9cificv6vfdrx33lci3w77bn")))

(define-public crate-rust-algo-0.2.1 (c (n "rust-algo") (v "0.2.1") (h "1xc81i7rflc600hnjdzsydlkh3na43vyas51n94g6xmk31wc448v")))

