(define-module (crates-io ru st rust_cli) #:use-module (crates-io))

(define-public crate-rust_cli-0.1.0 (c (n "rust_cli") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.10") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.30") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "term") (r "^0.2.7") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "0y2izg3kpidcsbsqwq5x24ad7cc5653x4zp1vdby5nv8qpf67ykv")))

