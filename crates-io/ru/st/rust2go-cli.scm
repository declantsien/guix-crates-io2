(define-module (crates-io ru st rust2go-cli) #:use-module (crates-io))

(define-public crate-rust2go-cli-0.0.1 (c (n "rust2go-cli") (v "0.0.1") (d (list (d (n "cbindgen") (r "^0.26") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go") (r "^0.0.1") (f (quote ("gen"))) (d #t) (k 0)))) (h "1sjh559ym90ji81l06gpcf3k57pmi7sva382fxibnp2lkq1x5g8r")))

(define-public crate-rust2go-cli-0.0.2 (c (n "rust2go-cli") (v "0.0.2") (d (list (d (n "cbindgen") (r "^0.26") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.0.2") (d #t) (k 0)))) (h "190yamv8vhz3a8lccm29rmm3xq9irbdnh72j2vyn1s94mg5kpyym")))

(define-public crate-rust2go-cli-0.1.0 (c (n "rust2go-cli") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.26") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.1.0") (d #t) (k 0)))) (h "055pvm5nbjbvbywcf8sazsrvmanl4w30vqv82nwlniql2nrg8i4y")))

(define-public crate-rust2go-cli-0.2.0 (c (n "rust2go-cli") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.26") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.2.0") (d #t) (k 0)))) (h "04pj9ys4rx5z6fpd221aqfdkky5rqc2mxxqs433qpsc2amjfp8v7")))

(define-public crate-rust2go-cli-0.3.0 (c (n "rust2go-cli") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.26") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.0") (d #t) (k 0)))) (h "1bjiygpirr5cwh4lb27gq8mm4pfl5n44h4v5agsvdbsqm03z1s4f")))

(define-public crate-rust2go-cli-0.3.1 (c (n "rust2go-cli") (v "0.3.1") (d (list (d (n "cbindgen") (r "^0.26") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.1") (d #t) (k 0)))) (h "15d1djrkn50546gzz31jg1dkasnw6g25xfs5qcnnips0481ndfhq")))

(define-public crate-rust2go-cli-0.3.2 (c (n "rust2go-cli") (v "0.3.2") (d (list (d (n "cbindgen") (r "^0.26") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.2") (d #t) (k 0)))) (h "0yvfmz0z38s0zfc17n192nlw56nlwmh1bqyd0x40vgmzapa2hg73")))

(define-public crate-rust2go-cli-0.3.3 (c (n "rust2go-cli") (v "0.3.3") (d (list (d (n "cbindgen") (r "^0.26") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.3") (d #t) (k 0)))) (h "0nrf60qky6mq5894758c6yjsc6wg100g2b6ik8kdrw9id6m73j3a")))

(define-public crate-rust2go-cli-0.3.4 (c (n "rust2go-cli") (v "0.3.4") (d (list (d (n "cbindgen") (r "^0.26") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.4") (d #t) (k 0)))) (h "1dyj9vslcz98dxnvy719ir999qqij52z6gl9j137g8c8a17mw4dg")))

(define-public crate-rust2go-cli-0.3.5 (c (n "rust2go-cli") (v "0.3.5") (d (list (d (n "cbindgen") (r "^0.26") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.5") (d #t) (k 0)))) (h "0b93npyv1pqw9nj3449skfbgyjl54amxh4a7w7hw4faszg7lqzin")))

(define-public crate-rust2go-cli-0.3.6 (c (n "rust2go-cli") (v "0.3.6") (d (list (d (n "cbindgen") (r "^0.26") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.6") (d #t) (k 0)))) (h "1bqqwzrp533z7zfg89kb5l975n1r5p6j1yi3x9i4lx74infn9hfi")))

