(define-module (crates-io ru st rust_tower_defense) #:use-module (crates-io))

(define-public crate-rust_tower_defense-0.1.0 (c (n "rust_tower_defense") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "19xa6izb4v130phfdgc187vlm4gj4rdqf673gsa46lhnq2rr53ar")))

