(define-module (crates-io ru st rusty-file-dialog) #:use-module (crates-io))

(define-public crate-rusty-file-dialog-0.0.0 (c (n "rusty-file-dialog") (v "0.0.0") (h "183ncpc72wb8mr4i0919a42w68kz2wxfs5cjrlz1b4c2vlllrv9j") (y #t)))

(define-public crate-rusty-file-dialog-0.0.1 (c (n "rusty-file-dialog") (v "0.0.1") (h "1pn4058kdplfirm632is2vmgdwrjqk5cihmnqi6wyjx734vrrzcg") (y #t)))

(define-public crate-rusty-file-dialog-0.0.2 (c (n "rusty-file-dialog") (v "0.0.2") (h "1lgwj590w0c32y5sj9r62yr94rcyw12qzhqr05fcjsywlcky1wgf") (y #t)))

