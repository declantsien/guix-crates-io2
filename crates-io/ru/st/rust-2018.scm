(define-module (crates-io ru st rust-2018) #:use-module (crates-io))

(define-public crate-rust-2018-1.0.0 (c (n "rust-2018") (v "1.0.0") (d (list (d (n "reedition") (r "=1.0.0") (d #t) (k 0)))) (h "01jns4sbhs5c440qx14wy78ngm64vr3kwjvfi74s8npbjmvv08ar")))

(define-public crate-rust-2018-1.1.0 (c (n "rust-2018") (v "1.1.0") (d (list (d (n "reedition") (r "=1.0.0") (d #t) (k 0)))) (h "0rbaj9drb5mns5a3dmdyhpi1f17hrz3xndirh9a7vpk1zp3gnwp4")))

