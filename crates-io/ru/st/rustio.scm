(define-module (crates-io ru st rustio) #:use-module (crates-io))

(define-public crate-rustio-0.0.1 (c (n "rustio") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (f (quote ("v3_22"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ymfvhz68qc23cl6lrjwzhkzckhd8iwqds1rwnrq1r3xl68xx8nj")))

(define-public crate-rustio-0.0.2 (c (n "rustio") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "restson") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11r66kazzjfavdy9y1639s48jqsv4nrz339qna7ypf6wpcg42jip")))

