(define-module (crates-io ru st rustlogic) #:use-module (crates-io))

(define-public crate-rustlogic-0.1.0 (c (n "rustlogic") (v "0.1.0") (h "1jf219mi8s1hyg2n67x9p4f6lm5msp7amc9pbh6m0df2syjs2kc0")))

(define-public crate-rustlogic-0.1.1 (c (n "rustlogic") (v "0.1.1") (h "0zjnbgfycfb75xhkdn0yg6601yhi94zd74dgs7ncxhg22r22wy7y")))

(define-public crate-rustlogic-0.1.2 (c (n "rustlogic") (v "0.1.2") (h "1ki02n5p6b9cqmi9wy82jx2s2l4lccf1ipynbh1d3psbfscxkwc0")))

(define-public crate-rustlogic-0.1.3 (c (n "rustlogic") (v "0.1.3") (h "1yd1sgb6d3nvrdrf5ii00n8pn1qpq98sr4y98qv8qpg105ls4npc")))

(define-public crate-rustlogic-0.2.0 (c (n "rustlogic") (v "0.2.0") (h "0lvf4082p5qh545zsqll4ia42n4058l1fqmbpfxf6ch7jqlxxymz")))

(define-public crate-rustlogic-0.2.1 (c (n "rustlogic") (v "0.2.1") (h "19kj7ri6c183ss2kid1ncyl50nwn2c36wgznj24lw74a0dszcpqi")))

