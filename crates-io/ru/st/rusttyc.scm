(define-module (crates-io ru st rusttyc) #:use-module (crates-io))

(define-public crate-rusttyc-0.2.0 (c (n "rusttyc") (v "0.2.0") (d (list (d (n "ena") (r "^0.13.0") (d #t) (k 0)))) (h "1qk9czrjf32k12y7zyx30m59jb7pgi7x9rrqx9v9aznkrr47nxr2")))

(define-public crate-rusttyc-0.2.1 (c (n "rusttyc") (v "0.2.1") (d (list (d (n "ena") (r "^0.13.0") (d #t) (k 0)))) (h "0dg1j4r8i6i8pv5244nnl1qddhcgpkikxiqmswfh53f6m2vqy04m")))

(define-public crate-rusttyc-0.2.2 (c (n "rusttyc") (v "0.2.2") (d (list (d (n "ena") (r "^0.13.0") (d #t) (k 0)))) (h "1241hxlhjy1k55c07a3mzz1ayigf4v7dd83hpz1g4lsnz7xwassx")))

(define-public crate-rusttyc-0.3.0 (c (n "rusttyc") (v "0.3.0") (h "1zcd7jl2qbwwch9fzvi54kyjm4p6jlkj4gvadkgrdv1n5rlrlv3v")))

(define-public crate-rusttyc-0.3.1 (c (n "rusttyc") (v "0.3.1") (h "0gbhklr3ans01ibn274m91xg08msmzd6grz5gyx7pkfajpy4vsbw")))

(define-public crate-rusttyc-0.3.2 (c (n "rusttyc") (v "0.3.2") (h "0rlgg9sgbp2bkvpvykjwq2ccm8wxnf3ffhknjqgcklg3zma01fg6")))

(define-public crate-rusttyc-0.4.0 (c (n "rusttyc") (v "0.4.0") (h "02i1k2x4519pa6lznr49wnp9p02py73rd1sb6h8ijp7f1cb6dz73")))

(define-public crate-rusttyc-0.4.1 (c (n "rusttyc") (v "0.4.1") (h "1x90k4p552rdml37wnp22fl2wy7vxqyvcp6i1kn5k9d0zbl5z1jz")))

(define-public crate-rusttyc-0.4.2 (c (n "rusttyc") (v "0.4.2") (h "0hrig3633fcpfx6lhiaq5m8v9yph7rlsbddv7i6f72lali17jx6n")))

(define-public crate-rusttyc-0.4.3 (c (n "rusttyc") (v "0.4.3") (h "118jfwiwqc0iwaf2cdss6vxzjqyslxnd1k2wkqi9gpny14g34s3h") (y #t)))

(define-public crate-rusttyc-0.5.0 (c (n "rusttyc") (v "0.5.0") (h "1js5s2psa8rjbs6829f2mddiw5hy13bxc1ady9qqy6iw8hd3pbhl")))

