(define-module (crates-io ru st rust_52_projects) #:use-module (crates-io))

(define-public crate-rust_52_projects-0.1.1 (c (n "rust_52_projects") (v "0.1.1") (h "020vbkkp1fg05idym3yq8scyd30m9a8qf96h7jjafcis28x8dpa2")))

(define-public crate-rust_52_projects-0.1.2 (c (n "rust_52_projects") (v "0.1.2") (h "1ciw4n5nkcy3rz4in2j9advnpzf5x8cw731x98bjng13p65ha9i2")))

