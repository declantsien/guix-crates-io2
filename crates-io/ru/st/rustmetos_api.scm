(define-module (crates-io ru st rustmetos_api) #:use-module (crates-io))

(define-public crate-rustmetos_api-1.0.0 (c (n "rustmetos_api") (v "1.0.0") (d (list (d (n "rustmetos_core") (r "^1.0") (d #t) (k 0)))) (h "15xx61av6ja8x2jw2j3zgq1sbqxb09ixg26pz2lil6bkm567cdga")))

(define-public crate-rustmetos_api-1.0.1 (c (n "rustmetos_api") (v "1.0.1") (d (list (d (n "rustmetos_core") (r "^1.0") (d #t) (k 0)))) (h "0bhgi3d34kmpglrd78dr582hp57byswfnk9npvfz2a2bmql92l71")))

