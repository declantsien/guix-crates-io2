(define-module (crates-io ru st rust_fzf) #:use-module (crates-io))

(define-public crate-rust_fzf-0.1.0 (c (n "rust_fzf") (v "0.1.0") (h "0qga2qzmbl4sb0sigmyfbrych2sfn7hq9y0rh2x7k0nv1sdb3lm0")))

(define-public crate-rust_fzf-0.1.1 (c (n "rust_fzf") (v "0.1.1") (h "0z3w496rfvvdn0vi47inckgs7qlsqbg2ynmg6f55vq56d6k746fi")))

(define-public crate-rust_fzf-0.1.2 (c (n "rust_fzf") (v "0.1.2") (h "1xifwpd7dpag3yi6ykx2m3rhdksysr3gyxqp2r61r7y539ww7v89")))

(define-public crate-rust_fzf-0.1.3 (c (n "rust_fzf") (v "0.1.3") (h "07p9kb486vcbgqk6ind9sajpzr79gwhm6yybcgivc002z754fb09")))

(define-public crate-rust_fzf-0.2.0 (c (n "rust_fzf") (v "0.2.0") (h "03d193h6alzwxxj1wa10sjvpnkmrcd3z1cwa5vrllq5lln941vp5")))

(define-public crate-rust_fzf-0.3.0 (c (n "rust_fzf") (v "0.3.0") (h "0ygqfgnqy15ds6kh7hzd57an0qa9vvb80mhq9fm1n4yf1mda4xvm")))

(define-public crate-rust_fzf-0.3.1 (c (n "rust_fzf") (v "0.3.1") (h "1c9cxj7v0159dpbk1351hsngh2bm7598l0yyqrn3b04qdaf4pabn")))

