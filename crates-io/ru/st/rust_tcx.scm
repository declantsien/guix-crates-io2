(define-module (crates-io ru st rust_tcx) #:use-module (crates-io))

(define-public crate-rust_tcx-0.9.0 (c (n "rust_tcx") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "13crmmy86vsa50n2fdp9csxbh1vnwi050i60n1c3icnrz0hgxx6r") (y #t)))

