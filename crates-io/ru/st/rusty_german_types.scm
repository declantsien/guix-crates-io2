(define-module (crates-io ru st rusty_german_types) #:use-module (crates-io))

(define-public crate-rusty_german_types-0.1.0 (c (n "rusty_german_types") (v "0.1.0") (h "0b1qjv05nzjfz8zamvsa1dcck7s4gv2dfmxmys62xmjn7lligqr5")))

(define-public crate-rusty_german_types-0.1.1 (c (n "rusty_german_types") (v "0.1.1") (h "0fqra3dlkfvq2fwmwh62bqnymc6vsfjdm84qs7z7h7zpi8lxdiar")))

