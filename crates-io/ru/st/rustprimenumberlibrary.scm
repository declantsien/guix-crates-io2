(define-module (crates-io ru st rustprimenumberlibrary) #:use-module (crates-io))

(define-public crate-RustPrimeNumberLibrary-0.1.0 (c (n "RustPrimeNumberLibrary") (v "0.1.0") (h "0372gj585vydkiazwnxns1674dxp830fll7z7f0gspx3xk3hyl1j")))

(define-public crate-RustPrimeNumberLibrary-0.1.1 (c (n "RustPrimeNumberLibrary") (v "0.1.1") (h "0b87i4b69mkzpbajrif33n7ypic537sq2gqk4i5qpkwswvw9pr36")))

