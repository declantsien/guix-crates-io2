(define-module (crates-io ru st rust-ini) #:use-module (crates-io))

(define-public crate-rust-ini-0.4.2 (c (n "rust-ini") (v "0.4.2") (h "0jiik6n04fpdjhn0qgx3wzdwvjmsbjp9l03ic306fzais9ciar1h")))

(define-public crate-rust-ini-0.4.3 (c (n "rust-ini") (v "0.4.3") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1bkprngr055ynbq2yx3cpmwad347003j2ds3x42rmkqzdhk6zwyb")))

(define-public crate-rust-ini-0.5.0 (c (n "rust-ini") (v "0.5.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "04mrhbpghjjlp30f87ykfrnjjswkr87b6idbv2ns7c8mb69ncckq")))

(define-public crate-rust-ini-0.5.1 (c (n "rust-ini") (v "0.5.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1425l6cvwrd5i6b5gk16wi1c5l9gww3zmvhlqn28sp8biza5yvs1")))

(define-public crate-rust-ini-0.6.0 (c (n "rust-ini") (v "0.6.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "19jaxmrm8jcv7bwvssy8jyvz3p1hylzzhm421hk5zwi9ixz92ac2")))

(define-public crate-rust-ini-0.7.0 (c (n "rust-ini") (v "0.7.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0fvyhwdhd8ad9x7z3cpp9jsxg87ak57vslsyhfg1p6ymvzyw8ihc")))

(define-public crate-rust-ini-0.7.1 (c (n "rust-ini") (v "0.7.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "18wiwhx7szmjbfl5krifmj4hdwwhn96iay8ahifhgyl08ivg5f0i")))

(define-public crate-rust-ini-0.8.0 (c (n "rust-ini") (v "0.8.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1k3q2y8mnjnpqichrqnan2l3z9qfwk5x5r41fn0c5xhqv7kn7sms")))

(define-public crate-rust-ini-0.8.1 (c (n "rust-ini") (v "0.8.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0ifslrf774kmk0b19aahzym4bg2lbiqnlk4z9plhmlg8ib61aznf")))

(define-public crate-rust-ini-0.8.2 (c (n "rust-ini") (v "0.8.2") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1iia156nfbp1ygakz9w2fr4hd5i0r74322w3pr3mkihfbqwsxmzx")))

(define-public crate-rust-ini-0.9.0 (c (n "rust-ini") (v "0.9.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1bj1x73x2kngb5rlhm189zbclss16d9qd2cn1m8h8g9262j63sfm")))

(define-public crate-rust-ini-0.9.1 (c (n "rust-ini") (v "0.9.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0f1sqh2myr703y1lpan0wpzq3gyly2y18rk06mq0hygh84npwdik")))

(define-public crate-rust-ini-0.9.2 (c (n "rust-ini") (v "0.9.2") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "13qgcj8ypdzqmfbfmz1fpmir5anjzsnryvpfihd2s5cc55mgnwj0")))

(define-public crate-rust-ini-0.9.4 (c (n "rust-ini") (v "0.9.4") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "11bp4rscfiv0nkf2i44ds2rbp85sv0kc3l11j10k9zvi6d9j64ww")))

(define-public crate-rust-ini-0.9.5 (c (n "rust-ini") (v "0.9.5") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1ngbvz29xv6ss4v8m7gl6m2wnyan5sv9k37isl48wkjp2irca4q7")))

(define-public crate-rust-ini-0.9.6 (c (n "rust-ini") (v "0.9.6") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "09djyil13mrswhv76y1hvv28pskkvclvggqghiyn91jsiggcn75m")))

(define-public crate-rust-ini-0.9.7 (c (n "rust-ini") (v "0.9.7") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "0gbficn6l64j976m0kf0nsr5xm1rcbmphsldv1v3h9r588azvvwd")))

(define-public crate-rust-ini-0.9.8 (c (n "rust-ini") (v "0.9.8") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "14pggjjd8s04d817kkc2s24j16bp941fa9q8ivx1vzsv1qamcf16")))

(define-public crate-rust-ini-0.9.9 (c (n "rust-ini") (v "0.9.9") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "032qavypfdy88dhszbnbd97kii9h63xxbxxb881w69n0wppy58c8")))

(define-public crate-rust-ini-0.9.10 (c (n "rust-ini") (v "0.9.10") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1ssn3ja8cnca0cpp0sp1wwa2nirr8jidnlhmzschv5rijlsv1phm")))

(define-public crate-rust-ini-0.9.11 (c (n "rust-ini") (v "0.9.11") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "11i2y3amwxz7q1v147nfwkpwzypdcqhz8qzmx8ayf3l8qy8zmhyb")))

(define-public crate-rust-ini-0.9.12 (c (n "rust-ini") (v "0.9.12") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "0n4698yfjjqzglczihafbq3mm9ds13zggj92mzzprdyapzcpasbw")))

(define-public crate-rust-ini-0.10.0 (c (n "rust-ini") (v "0.10.0") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "17q706l54qd8l2hh8m513wd3gxhwdrsak7v0gn17yzhfnnqfim06")))

(define-public crate-rust-ini-0.10.1 (c (n "rust-ini") (v "0.10.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "0s772dv454n73x3ggwp9hd0paxww9iir4437ffsd4n92vg67fy3w")))

(define-public crate-rust-ini-0.10.2 (c (n "rust-ini") (v "0.10.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)))) (h "00gnm17g66rphrzd7vydhqa6yl32rr96i995vcawnb0jx1avdni2")))

(define-public crate-rust-ini-0.10.3 (c (n "rust-ini") (v "0.10.3") (h "0qvjqdjqii3z8qz4qwjvv8l8j8fyj06lrzmhwsdnjb3jv9dlqrca")))

(define-public crate-rust-ini-0.11.0 (c (n "rust-ini") (v "0.11.0") (h "06m2yrhy5cjardg09cvib5mm09jzy1gf1xdig6czs69xizncycmp")))

(define-public crate-rust-ini-0.12.0 (c (n "rust-ini") (v "0.12.0") (h "0hx29i0hv8ayqz7kbzy6p4gx0r0r7jfc25ig1mwkvzj88q88hkyp")))

(define-public crate-rust-ini-0.12.1 (c (n "rust-ini") (v "0.12.1") (h "0pbwcwjy855j3qdq6ba6hkvqi0ji3967z495520yfsg94xqi05yh")))

(define-public crate-rust-ini-0.12.2 (c (n "rust-ini") (v "0.12.2") (h "05f760k9hwx3hfmsqfjs2p4ii99pf91vihba5dlll4jfc4bfhrmc")))

(define-public crate-rust-ini-0.12.3 (c (n "rust-ini") (v "0.12.3") (h "0a0b3ganri9w2vs7zqjj95p21g9n98hyy3lf1hf1z3w7hl1cdnyr") (y #t)))

(define-public crate-rust-ini-0.13.0 (c (n "rust-ini") (v "0.13.0") (h "1hifnbgaz01zja5995chy6vjacbif2m76nlxsisw7y1pxx4c2liy")))

(define-public crate-rust-ini-0.14.0 (c (n "rust-ini") (v "0.14.0") (d (list (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "multimap") (r "^0.8") (d #t) (k 0)))) (h "1wnbh64jqaj8vw7m3p83biiv1sd4aaia4azzd164ai19fbbag5hc") (f (quote (("preserve_order" "indexmap") ("inline_comment") ("default"))))))

(define-public crate-rust-ini-0.15.0 (c (n "rust-ini") (v "0.15.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "1mafyf53ck6vp91yri3mmwpacd2331ym1kv6dyj0cf2fj0jad6bf") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase"))))))

(define-public crate-rust-ini-0.15.1 (c (n "rust-ini") (v "0.15.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "0zhlpx28mv3380sj43aaizpc245czrmjvsm7xwsp7x72zkjqs43m") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase"))))))

(define-public crate-rust-ini-0.15.2 (c (n "rust-ini") (v "0.15.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "1xswq6xc8n8d9n0gya04c7ah2qsxwp8hk7nk726b300h2nl9yq4w") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase"))))))

(define-public crate-rust-ini-0.15.3 (c (n "rust-ini") (v "0.15.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "1qbrz7ir1d3wkzsraxzzl20w43x2r18vjfvgc1xnm1wcagfpjdks") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase"))))))

(define-public crate-rust-ini-0.16.0 (c (n "rust-ini") (v "0.16.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "0qnyp96kslw5ivcf5a7dhiwbqpb72sh8rq40m81vbkiim54z5nyw") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase"))))))

(define-public crate-rust-ini-0.16.1 (c (n "rust-ini") (v "0.16.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "1iiswwbjcvi6m9nap3lwyc1d5rwwmilyak1syy3b1q47g9v39cam") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase"))))))

(define-public crate-rust-ini-0.17.0 (c (n "rust-ini") (v "0.17.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "08hfh6p2svznza3m07vavsc4c8x4g6d715sz58rzh73sm551qiv3") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase"))))))

(define-public crate-rust-ini-0.18.0 (c (n "rust-ini") (v "0.18.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.4") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "1px22l3m84v7f46pa3p4bsjykivw8ryq6af8kpkzdd16c11z5mgn") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase") ("brackets-in-section-names"))))))

(define-public crate-rust-ini-0.19.0 (c (n "rust-ini") (v "0.19.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "14ah70q8k450d6cdwn2vsl1rsdha09nax2n8y4z5a4ziq773naky") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase") ("brackets-in-section-names"))))))

(define-public crate-rust-ini-0.20.0 (c (n "rust-ini") (v "0.20.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.7") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "12h608iy8kzxdrd0i5r20pzmgnw5fwg4rjwy5azq526bdch9h1iy") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase") ("brackets-in-section-names"))))))

(define-public crate-rust-ini-0.21.0 (c (n "rust-ini") (v "0.21.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.7") (d #t) (k 0)) (d (n "trim-in-place") (r "^0.1.7") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (o #t) (d #t) (k 0)))) (h "0hav0am43zl2zwbwbcdh1vvczb8zlzhl4k2iz9nayjcggpamwqhd") (f (quote (("inline-comment") ("default") ("case-insensitive" "unicase") ("brackets-in-section-names")))) (r "1.64")))

