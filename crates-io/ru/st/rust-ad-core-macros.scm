(define-module (crates-io ru st rust-ad-core-macros) #:use-module (crates-io))

(define-public crate-rust-ad-core-macros-0.1.0 (c (n "rust-ad-core-macros") (v "0.1.0") (h "02rbpmfi0lmcb04nz3ngpq92187k02x0sql45pb2yxfrlpp3dnbi")))

(define-public crate-rust-ad-core-macros-0.7.0 (c (n "rust-ad-core-macros") (v "0.7.0") (d (list (d (n "rust-ad-consts") (r "^0.7.0") (d #t) (k 0)))) (h "1r8hc8svrx1air8pq3dkfwwxxdf7m64y6fhndy0gm8rgmialj80g")))

(define-public crate-rust-ad-core-macros-0.7.1 (c (n "rust-ad-core-macros") (v "0.7.1") (d (list (d (n "rust-ad-consts") (r "^0.7.1") (d #t) (k 0)))) (h "06qw49p9hggd52xg0k2na5n48zfl2mlbbs17dfvk36kcn4gmn3rb")))

(define-public crate-rust-ad-core-macros-0.7.2 (c (n "rust-ad-core-macros") (v "0.7.2") (d (list (d (n "rust-ad-consts") (r "^0.7.2") (d #t) (k 0)))) (h "13lr41842fwc5jrmfi3lsrmi2jz35yjsf1r70hv6jb6wnx9p462h")))

(define-public crate-rust-ad-core-macros-0.8.0 (c (n "rust-ad-core-macros") (v "0.8.0") (d (list (d (n "rust-ad-consts") (r "^0.8.0") (d #t) (k 0)))) (h "09952d2q6176i3gv0a5vrkkx19a3bw411k0jqqrwzgm0cpl8gvwx")))

