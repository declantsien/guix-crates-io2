(define-module (crates-io ru st rustutils-sleep) #:use-module (crates-io))

(define-public crate-rustutils-sleep-0.1.0 (c (n "rustutils-sleep") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "1pz9gnyaanxl5gy9y6d3f0va9n0wsxvgfkwcj91f7q76d9k9lrar")))

