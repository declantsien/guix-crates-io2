(define-module (crates-io ru st rustatic) #:use-module (crates-io))

(define-public crate-rustatic-0.1.0 (c (n "rustatic") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.12") (d #t) (k 0)))) (h "14p1j3nk2ww8ij7qq1gkfypld3z1c918mb52qdzsfrhf4bmpm27w")))

(define-public crate-rustatic-0.2.0 (c (n "rustatic") (v "0.2.0") (d (list (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.12") (d #t) (k 0)))) (h "05jbha9kzdnn9cqyylwq691di1bfsgk5x2irnvki33gliyxyvm66")))

(define-public crate-rustatic-0.2.2 (c (n "rustatic") (v "0.2.2") (d (list (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tiny_http") (r "^0.12") (d #t) (k 0)))) (h "1pfbggz7vaa78kvnlw8z3544jbkwpasnn2zkggin39rzygann9c7")))

(define-public crate-rustatic-0.2.6 (c (n "rustatic") (v "0.2.6") (d (list (d (n "ascii") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12") (d #t) (k 0)))) (h "1jhpryrspmg01f97cahinn6y0j9rdk0nmhg85hisyd3i2nj3clk8")))

