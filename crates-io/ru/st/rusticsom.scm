(define-module (crates-io ru st rusticsom) #:use-module (crates-io))

(define-public crate-rusticsom-0.1.0 (c (n "rusticsom") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0njbvdyynz79lsw0hb2w8j7zcnzfaila6f36g5q8hff9dpy942gl")))

(define-public crate-rusticsom-1.0.0 (c (n "rusticsom") (v "1.0.0") (d (list (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1s4h8qpg27pq0qybsa9gxpg4xf7bgav6hyl708d2dg10mxzid8ka")))

(define-public crate-rusticsom-1.1.1 (c (n "rusticsom") (v "1.1.1") (d (list (d (n "ndarray") (r "^0.13") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q347d4cpgyry97ykhw18xg7wd4bhm2m31id1ykkm57cn4pzlp11")))

