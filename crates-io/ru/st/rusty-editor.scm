(define-module (crates-io ru st rusty-editor) #:use-module (crates-io))

(define-public crate-rusty-editor-0.11.0 (c (n "rusty-editor") (v "0.11.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rg3d") (r "^0.24") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hdmczvjy9m2byy0gn23y4cyrwqbh8633w5wphmkyrj6sv97wh3k") (f (quote (("enable_profiler" "rg3d/enable_profiler")))) (r "1.56")))

