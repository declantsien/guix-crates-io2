(define-module (crates-io ru st rusty_parser_derive) #:use-module (crates-io))

(define-public crate-rusty_parser_derive-0.1.0 (c (n "rusty_parser_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m0myyqz8fj2jqbfmr990zkcqzshgz4nc8anilm5k2wyin2hia76") (y #t)))

(define-public crate-rusty_parser_derive-0.1.1 (c (n "rusty_parser_derive") (v "0.1.1") (h "1alaw7v4691klj3l1k4b747ahdg2vav9kj20pr1gvm39cmzcykz0")))

