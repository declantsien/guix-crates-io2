(define-module (crates-io ru st rustcn-ui) #:use-module (crates-io))

(define-public crate-rustcn-ui-0.1.0 (c (n "rustcn-ui") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.5.0-alpha.0") (f (quote ("web"))) (d #t) (k 0)))) (h "0gr9rsrv56v1hsniiw6k0lr7c29091rqm6minvmahhhz6s09iq63")))

(define-public crate-rustcn-ui-0.1.1 (c (n "rustcn-ui") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.5.0-alpha.0") (f (quote ("web"))) (d #t) (k 0)))) (h "1fsgh79l50qynyljpqjv43s0r93lv4f39jc2lxwsw0jxs9j64g38")))

(define-public crate-rustcn-ui-0.1.2 (c (n "rustcn-ui") (v "0.1.2") (d (list (d (n "dioxus") (r "^0.5.0-alpha.0") (f (quote ("web"))) (d #t) (k 0)))) (h "0jfkq7fpiba6d5fk4011lx712hxl9bxzzjiq0fzakn0x7gli1pk4")))

(define-public crate-rustcn-ui-0.1.5 (c (n "rustcn-ui") (v "0.1.5") (d (list (d (n "dioxus") (r "^0.5.0-alpha.0") (f (quote ("web"))) (d #t) (k 0)))) (h "0lrh1y8ldjqaf4g8kixrj0j6sc21faygi4qcg7mdinbi1vh3kdpm")))

(define-public crate-rustcn-ui-0.1.6 (c (n "rustcn-ui") (v "0.1.6") (d (list (d (n "dioxus") (r "^0.5.0-alpha.0") (f (quote ("web"))) (d #t) (k 0)))) (h "1jyxdqhmxgm8vlh80wm7wwk5jxp5dnk8rwd42v0hgy7hf4q88af2")))

