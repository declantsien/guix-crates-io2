(define-module (crates-io ru st rust_admob_ssv) #:use-module (crates-io))

(define-public crate-rust_admob_ssv-0.1.0 (c (n "rust_admob_ssv") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "urlparse") (r "^0.7") (d #t) (k 0)))) (h "1qnxza72j2wbvy2wwakqgwdc0777nzp8yz5iid877hj2vhmj4c7a")))

