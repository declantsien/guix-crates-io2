(define-module (crates-io ru st rusty-tcl-sys) #:use-module (crates-io))

(define-public crate-rusty-tcl-sys-1.0.0 (c (n "rusty-tcl-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "17b27j16ihb9ffx4yjmbidznfmrf4h5p9b0bassz522h1y0zj041") (l "tcl8.5")))

(define-public crate-rusty-tcl-sys-1.1.0 (c (n "rusty-tcl-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "02lrs6hlxz5bii26x8y65bh7lvb5j6bai7y73sx11brq4n242mvw") (l "tcl8.5")))

(define-public crate-rusty-tcl-sys-1.1.1 (c (n "rusty-tcl-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "175q1hnrjmg3qiaqlxmr4aidhzx2qyq0qbxw12gq25yn4949p4xd") (l "tcl8.5")))

(define-public crate-rusty-tcl-sys-1.1.2 (c (n "rusty-tcl-sys") (v "1.1.2") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0cv5p787hy267znajq6fbjhjyx1v7d20rkfj82vmh4zc6hbmpy9h") (l "tcl8.5")))

(define-public crate-rusty-tcl-sys-1.1.3 (c (n "rusty-tcl-sys") (v "1.1.3") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "1x13knl7hkpiw7hh9kjzp48926wzw9ih65yrayg7lwj0cchcrjrh") (l "tcl8.5")))

