(define-module (crates-io ru st rustuino) #:use-module (crates-io))

(define-public crate-rustuino-0.1.0 (c (n "rustuino") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.7") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (d #t) (k 0)) (d (n "stm32f4") (r "^0.14.0") (f (quote ("stm32f446" "rt"))) (d #t) (k 0)))) (h "0ybqi1ww192gby15vwgp2hwdqwj0mc555fs41bkqg2vsmsgw4dh4")))

