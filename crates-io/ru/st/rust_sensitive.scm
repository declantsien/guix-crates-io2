(define-module (crates-io ru st rust_sensitive) #:use-module (crates-io))

(define-public crate-rust_sensitive-1.0.0 (c (n "rust_sensitive") (v "1.0.0") (h "06x7iy46vnq664hszky9754v2wbylqanpzbdd9dhzykva2pwk82p") (r "1.70")))

(define-public crate-rust_sensitive-1.0.1 (c (n "rust_sensitive") (v "1.0.1") (h "1w567c5948hhyn274cri2myl38q0v08fhp018ww3ryswx3kn1lk1") (r "1.70")))

(define-public crate-rust_sensitive-1.1.0 (c (n "rust_sensitive") (v "1.1.0") (d (list (d (n "aho-corasick") (r "^1.1.3") (d #t) (k 0)))) (h "1ch4rmmam3p88kz2w05zz22xb6vvffiw4hxdlml74qd26dvabsa1") (r "1.70")))

