(define-module (crates-io ru st rust_smt) #:use-module (crates-io))

(define-public crate-rust_smt-0.1.0 (c (n "rust_smt") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.4.0") (d #t) (k 0)))) (h "19hl2laj78inms97hc8w5zicas19jgji80k8lsmxws8153wdr5rd")))

(define-public crate-rust_smt-0.1.1 (c (n "rust_smt") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0q42kmbphdh5f3j5r0y79w6p3ik1f18ayrv7xjbg4kfn6hyasl9d")))

(define-public crate-rust_smt-0.2.0 (c (n "rust_smt") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "z3-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1707pi8ww3b7yl62bdbg567j707anh72sscmkz0q6z76g8ppq2aw")))

