(define-module (crates-io ru st rust-dash-handshake) #:use-module (crates-io))

(define-public crate-rust-dash-handshake-0.1.0 (c (n "rust-dash-handshake") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "11gl8b2q0zjsim986phw7fhiqxrjypfj5xcznq9gyy7q21293ng6")))

