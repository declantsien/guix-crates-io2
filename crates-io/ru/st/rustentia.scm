(define-module (crates-io ru st rustentia) #:use-module (crates-io))

(define-public crate-rustentia-0.1.0 (c (n "rustentia") (v "0.1.0") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-notify") (r "^0.6.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0vrnx2g0zmwwv4hiwqglfrv4lyfgjbab50zwh67za6f7678y9906")))

(define-public crate-rustentia-0.2.0 (c (n "rustentia") (v "0.2.0") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-notify") (r "^0.6.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1ml8qcsa14rzrsn3jvffljwi83qgvp0w0qfr4bdy27xix6rpv26n")))

(define-public crate-rustentia-0.2.1 (c (n "rustentia") (v "0.2.1") (d (list (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui-notify") (r "^0.6.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "00gs01p73sfx6pnklns2qfx4x07m6lzaxk0001v7lr7zv0hgdb06")))

