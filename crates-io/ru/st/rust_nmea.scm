(define-module (crates-io ru st rust_nmea) #:use-module (crates-io))

(define-public crate-rust_nmea-1.0.1 (c (n "rust_nmea") (v "1.0.1") (h "1mf2n16y58z96k1qbf1nmpvk867g3y9qjmaqyxsjzsmzzdj1jscp") (y #t)))

(define-public crate-rust_nmea-1.2.4 (c (n "rust_nmea") (v "1.2.4") (h "1b27w2cna454lx4gailzyr5n7i1j4w1v40hf7cy7x7rx1wzqm5ji") (y #t)))

(define-public crate-rust_nmea-1.2.5 (c (n "rust_nmea") (v "1.2.5") (h "1g9fljr01kmfbiihlfq1kgc1irfgf6ddck28r6vdmy2ns086xhp1") (y #t)))

(define-public crate-rust_nmea-1.2.6 (c (n "rust_nmea") (v "1.2.6") (h "0dvwvca09hpmxcxhqb5z2g0hn8336mgak8l537fm59fz3pnld7wy")))

