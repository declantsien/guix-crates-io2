(define-module (crates-io ru st rusticache) #:use-module (crates-io))

(define-public crate-rusticache-0.0.1 (c (n "rusticache") (v "0.0.1") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0l9jn3lmn62mb5p754x2w024yi20ls33vncm2nl5v3k5m359nzd1") (y #t)))

(define-public crate-rusticache-0.0.2 (c (n "rusticache") (v "0.0.2") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1f35awyjqp1ppvsg74l60x7az3ni382a0jamr507r0svrgi26mcr") (y #t)))

(define-public crate-rusticache-0.0.3 (c (n "rusticache") (v "0.0.3") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1lprjm5w8vbaclnc4xc34pq3nl5wxcm989dcwpqhnrwgjmp3ksr7") (y #t)))

(define-public crate-rusticache-0.1.0 (c (n "rusticache") (v "0.1.0") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "1dlnn9fjb073gsp16arspdanavs0yw5rjzkc9ni6acrl1fanij2j") (y #t)))

