(define-module (crates-io ru st rustract) #:use-module (crates-io))

(define-public crate-rustract-0.1.0 (c (n "rustract") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "mysql"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 2)))) (h "08v6rid0dw2rfjxr77wz3rh91qy36rvp6rla9235rb9ggjiz080v")))

