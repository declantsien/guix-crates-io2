(define-module (crates-io ru st rust-assimp) #:use-module (crates-io))

(define-public crate-rust-assimp-0.0.1-alpha.1 (c (n "rust-assimp") (v "0.0.1-alpha.1") (h "1lghb4ncvsvwgxa51v8xn0nxk4b11m6flf5ys119f18s4sw39ya3")))

(define-public crate-rust-assimp-0.0.1-alpha.2 (c (n "rust-assimp") (v "0.0.1-alpha.2") (d (list (d (n "vecmath") (r "*") (d #t) (k 0)))) (h "1rdfy6xjzx947zly1918farckspjb3fbnd1lhgiyb6j8p910r0gz")))

(define-public crate-rust-assimp-0.0.23 (c (n "rust-assimp") (v "0.0.23") (d (list (d (n "vecmath") (r "*") (d #t) (k 0)))) (h "1id4lxj4i6bg2wkgy4g2dni5bij645d2q8n7hgivq2ax2mqld22q")))

