(define-module (crates-io ru st rust-xmlrpc) #:use-module (crates-io))

(define-public crate-rust-xmlrpc-0.0.1 (c (n "rust-xmlrpc") (v "0.0.1") (h "016q1nj2q4mm3jwg3dfh4csvmgsiv2gdzrn5nxz3x73b6gr80wl3")))

(define-public crate-rust-xmlrpc-0.0.2 (c (n "rust-xmlrpc") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "1cdbmgvk0l250nmpydpx419jlgdxwy3c82fljkzgj4q325qrjz6l")))

(define-public crate-rust-xmlrpc-0.0.3 (c (n "rust-xmlrpc") (v "0.0.3") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "0bi096zqvw2ghq3lac73rjxn5gkgkmzrgwi72w2cv5hfp8mw5cjy")))

(define-public crate-rust-xmlrpc-0.0.4 (c (n "rust-xmlrpc") (v "0.0.4") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.1.12") (d #t) (k 0)))) (h "1i4hc5m9a5h2168s7rjiwqbixn8h4vj6yq4gsb4m5c98nnkpyx3k")))

(define-public crate-rust-xmlrpc-0.0.5 (c (n "rust-xmlrpc") (v "0.0.5") (d (list (d (n "hyper") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.1.12") (d #t) (k 0)))) (h "03jaaxqmwnkgwinlp4l61981bfynycyr7p9hsv7vjvr3qjmkyj1j")))

(define-public crate-rust-xmlrpc-0.0.6 (c (n "rust-xmlrpc") (v "0.0.6") (d (list (d (n "hyper") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.1.12") (d #t) (k 0)))) (h "1r69njh6xnc98wn85akvw0zcwqg1xk4m68l7fwzjd8s4aky78s7m")))

