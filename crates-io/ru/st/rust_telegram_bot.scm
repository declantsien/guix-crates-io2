(define-module (crates-io ru st rust_telegram_bot) #:use-module (crates-io))

(define-public crate-rust_telegram_bot-0.0.1 (c (n "rust_telegram_bot") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1iqsyc1s19y78l3pssf2s3dg9y01xjvb25qa26ayim6ld9s683kz")))

