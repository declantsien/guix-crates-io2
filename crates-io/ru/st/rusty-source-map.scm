(define-module (crates-io ru st rusty-source-map) #:use-module (crates-io))

(define-public crate-rusty-source-map-0.1.0 (c (n "rusty-source-map") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "source-map-mappings") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1acq1im0drg4fzmw24brs09m3v1gfp44dqnq0s9w6fl7mjl5p7v7")))

(define-public crate-rusty-source-map-0.2.0 (c (n "rusty-source-map") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "source-map-mappings") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0xqnfsw081nqyig6bhbv3jp9vckfgdk9zqkhdpzzqsx7wf1dgdic")))

(define-public crate-rusty-source-map-0.2.1 (c (n "rusty-source-map") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "source-map-mappings") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1aw8yi2lwh1cl13wx9w1sclhma0llb1avan8yjl2w92qchipj54k")))

(define-public crate-rusty-source-map-0.2.2 (c (n "rusty-source-map") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "source-map-mappings") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0qb3x5slgaw4ckyw6yrcb1fqbbqfzp9mfz2rd25jrlxg8zn6l8zg")))

