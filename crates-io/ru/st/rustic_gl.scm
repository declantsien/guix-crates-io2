(define-module (crates-io ru st rustic_gl) #:use-module (crates-io))

(define-public crate-rustic_gl-0.3.1 (c (n "rustic_gl") (v "0.3.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)))) (h "0fb0ysz2c2lvx8wk8yjzh8fnwn7767grhy3i13r21qwxfiqfwchm")))

(define-public crate-rustic_gl-0.3.2 (c (n "rustic_gl") (v "0.3.2") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)))) (h "04vqfffnm59mbdfc3qz359mwgz2frn45m5nr37lai19jcss3d389")))

