(define-module (crates-io ru st rust-wordl) #:use-module (crates-io))

(define-public crate-rust-wordl-0.1.0 (c (n "rust-wordl") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0na3w0hnh0i62f6qzh0lavsrw71hq11lpkmwx3w4z0n0mwfygajv")))

(define-public crate-rust-wordl-0.1.1 (c (n "rust-wordl") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "01rmm2b60nrjwiw1m189vn3msjcp1mb5jwb9z45d8nwcrlwpwx48")))

