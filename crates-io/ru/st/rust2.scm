(define-module (crates-io ru st rust2) #:use-module (crates-io))

(define-public crate-RUST2-0.1.0 (c (n "RUST2") (v "0.1.0") (h "1k3kjrmi5457wanjb5yqrwrz6rl1q02bajvg5n3vif5d7ff5k8fj")))

(define-public crate-RUST2-0.2.0 (c (n "RUST2") (v "0.2.0") (h "04hs83g7jl3vsdbg0c6akwnm1qr1n73gn7p7va2ycdzdrx8ya7n6")))

