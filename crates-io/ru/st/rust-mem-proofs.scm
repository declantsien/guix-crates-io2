(define-module (crates-io ru st rust-mem-proofs) #:use-module (crates-io))

(define-public crate-rust-mem-proofs-0.1.0 (c (n "rust-mem-proofs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "1viv6sxlnf70hzzyw2jlsxxm9hjwmlk456why22rq37m855lg7rx")))

