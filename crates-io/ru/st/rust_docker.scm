(define-module (crates-io ru st rust_docker) #:use-module (crates-io))

(define-public crate-rust_docker-0.1.0 (c (n "rust_docker") (v "0.1.0") (d (list (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.21") (d #t) (k 0)))) (h "15cshqaf4j1rai1d0mr2b24l98pqi3j0gd552alqdd3nligsbbq3")))

(define-public crate-rust_docker-0.1.1 (c (n "rust_docker") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.21") (d #t) (k 0)))) (h "1nzydgbkv616jy2dlqhq887ddshkknv1rfqsk9jihwysvcbjcr9z")))

