(define-module (crates-io ru st rust_programming_book) #:use-module (crates-io))

(define-public crate-rust_programming_book-0.1.0 (c (n "rust_programming_book") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1s02a71489iyc2p67wgzbia6in48qhncwg8p757x0dx3ah6a506a") (y #t)))

(define-public crate-rust_programming_book-0.1.1 (c (n "rust_programming_book") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "10xcd5ah0r2aswdbxcjaf8wmxg7g65d24q2nlsz7slhj5p67vp8s")))

