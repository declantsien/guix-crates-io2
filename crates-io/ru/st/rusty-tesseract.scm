(define-module (crates-io ru st rusty-tesseract) #:use-module (crates-io))

(define-public crate-rusty-tesseract-1.0.0 (c (n "rusty-tesseract") (v "1.0.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "polars") (r "^0.18.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0zql5ysn92jxc6rnlzad14bf58xfjhpaiqcm8gdirapmd6r30j2z")))

(define-public crate-rusty-tesseract-1.0.1 (c (n "rusty-tesseract") (v "1.0.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "polars") (r "^0.18.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "03gq3v60q86hbz08wx8iy004hml13z4smh8x9r8r2cg1960x8752")))

(define-public crate-rusty-tesseract-1.1.1 (c (n "rusty-tesseract") (v "1.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pnv7hcs3mj53mh72xp4iig2bz2jy3rrqklz1vmdvwjibkjv4343")))

(define-public crate-rusty-tesseract-1.1.3 (c (n "rusty-tesseract") (v "1.1.3") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "04icd44l8r1w3q9dy45b7nj9wbfdn2ms2jy2116vv7islylj8b51")))

(define-public crate-rusty-tesseract-1.1.4 (c (n "rusty-tesseract") (v "1.1.4") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "001n6kgpnyrr58gvfwfiwaiwhq41h0azdn2w8f5blmvw5kd295a3")))

(define-public crate-rusty-tesseract-1.1.5 (c (n "rusty-tesseract") (v "1.1.5") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1k79nqqs7ryv1dyks5kygbng57pdv8hym49h4w8l53qaqq1icbbv")))

(define-public crate-rusty-tesseract-1.1.6 (c (n "rusty-tesseract") (v "1.1.6") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "04q623nnmp4zskkc264bllzhkq2idbsjgblmnyii7rk7102axd26")))

(define-public crate-rusty-tesseract-1.1.7 (c (n "rusty-tesseract") (v "1.1.7") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1s7fwgjx523vw5jfiv4h0j138y989m37s72pd1wvdnr4dl17c8dg")))

(define-public crate-rusty-tesseract-1.1.8 (c (n "rusty-tesseract") (v "1.1.8") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1l2x6gcjn6paayyn2sjjadgp1ks9jxvlaq973acfrx8yvak0rvy1")))

(define-public crate-rusty-tesseract-1.1.9 (c (n "rusty-tesseract") (v "1.1.9") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0d53sg4kqi58avn76qn2528krarsk8yci8d90w50i06q4i9yr7z4")))

(define-public crate-rusty-tesseract-1.1.10 (c (n "rusty-tesseract") (v "1.1.10") (d (list (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "13vrkd0vyrhf9w04679bd0gxllr9j1sc3rkjx5yn11dvv1p2s7kq")))

