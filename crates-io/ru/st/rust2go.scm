(define-module (crates-io ru st rust2go) #:use-module (crates-io))

(define-public crate-rust2go-0.0.1 (c (n "rust2go") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1p7bk1q4caz94whayh08yb63gpajjj9fac54sjm16x6c7c8lkm9q") (f (quote (("gen" "syn") ("default" "gen")))) (y #t)))

(define-public crate-rust2go-0.0.2 (c (n "rust2go") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0wh5dxljkj03cvg7bz7sk2wa88f3xpl7wyzzy232y71xvfqlw8fj") (f (quote (("default") ("build" "syn" "quote" "proc-macro2")))) (y #t)))

(define-public crate-rust2go-0.1.0 (c (n "rust2go") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0vbzgx6srcqnvq5il29frds9dl526ynf534p0819lgzxkk2pscp0") (f (quote (("default") ("build" "syn" "bindgen")))) (y #t)))

(define-public crate-rust2go-0.2.0 (c (n "rust2go") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1368anz11wizfcj5gbxm93vakzyr6fl01a3xfxpbbysmrwmhjnk5") (f (quote (("default") ("build" "syn" "bindgen")))) (y #t)))

(define-public crate-rust2go-0.3.0 (c (n "rust2go") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "110cx3md3cr0jgpx81slrvrfj5xld6bxshfwxf6k2dxwqv9hyn46") (f (quote (("default") ("build" "syn" "bindgen"))))))

(define-public crate-rust2go-0.3.1 (c (n "rust2go") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1mgpsxcsp15kyxkmp9kjvacin53c2m7lzh1hri380hpb2n1491hn") (f (quote (("default") ("build" "syn" "bindgen"))))))

(define-public crate-rust2go-0.3.2 (c (n "rust2go") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "02jmnnp0bb6ykmghsraj2syrm8pax6qplmj1i6qr9l4xqivmm0ad") (f (quote (("default") ("build" "syn" "bindgen"))))))

(define-public crate-rust2go-0.3.3 (c (n "rust2go") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0b55liknk1xln0mqmz959rv5js8big0szswg0w8g24f2dsqc8cp2") (f (quote (("default") ("build" "syn" "bindgen"))))))

(define-public crate-rust2go-0.3.4 (c (n "rust2go") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-cli") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1f891m4gnlmanr7bfbpxrdxhlhgbh475hzd3r1jm656gr5bja3sa") (f (quote (("default") ("build" "syn" "bindgen" "rust2go-cli"))))))

(define-public crate-rust2go-0.3.5 (c (n "rust2go") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-cli") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1snr1r54wfy8fdmgjyr75mfy1hdybqbb0wf2s1l7fryi44dbhcqv") (f (quote (("default") ("build" "syn" "bindgen" "rust2go-cli"))))))

(define-public crate-rust2go-0.3.6 (c (n "rust2go") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-cli") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1iyqm5z3dyy29s97yzqca8vvl873xzarnhaj70qxrgrdkblbc21b") (f (quote (("default") ("build" "syn" "bindgen" "rust2go-cli"))))))

(define-public crate-rust2go-0.3.7 (c (n "rust2go") (v "0.3.7") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-cli") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1dwf2i1hjd2sbj42bg1l4byq5zxdmb0hbrn1p5xncajq4ikvnch8") (f (quote (("default") ("build" "syn" "bindgen" "rust2go-cli"))))))

(define-public crate-rust2go-0.3.8 (c (n "rust2go") (v "0.3.8") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 0)) (d (n "rust2go-cli") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "rust2go-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1g1ih0gpkl7jna06a7hc3ckcbm8vhiglw0c59760lvi36hgw9pw3") (f (quote (("default") ("build" "syn" "bindgen" "rust2go-cli"))))))

