(define-module (crates-io ru st rust-forth-interactive-compiler) #:use-module (crates-io))

(define-public crate-rust-forth-interactive-compiler-0.1.0 (c (n "rust-forth-interactive-compiler") (v "0.1.0") (d (list (d (n "rust-forth-compiler") (r "^0.3.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.2") (d #t) (k 0)))) (h "0b80sy6iyr04g2ai511pvwg5mshap8h4j4gmh16p40d4ckvgny69")))

(define-public crate-rust-forth-interactive-compiler-0.1.1 (c (n "rust-forth-interactive-compiler") (v "0.1.1") (d (list (d (n "rust-forth-compiler") (r "^0.3.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.2") (d #t) (k 0)))) (h "0j106kl96yshq5a8ls74k192v549vbzbix0zxm7pnqzvdypagfnc")))

(define-public crate-rust-forth-interactive-compiler-0.1.2 (c (n "rust-forth-interactive-compiler") (v "0.1.2") (d (list (d (n "rust-forth-compiler") (r "^0.4.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.2") (d #t) (k 0)))) (h "10sgzpdk8nm9srhjdlij4i2c67fz0i1lzqa7ga4i86vsqknqxyp8")))

(define-public crate-rust-forth-interactive-compiler-0.1.3 (c (n "rust-forth-interactive-compiler") (v "0.1.3") (d (list (d (n "rust-forth-compiler") (r "^0.4.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.2") (d #t) (k 0)))) (h "0w3x1nzj0k6ka0kdrk49kic784p9bi9vhbkf68y5nrkzwa6cf0am")))

(define-public crate-rust-forth-interactive-compiler-0.1.4 (c (n "rust-forth-interactive-compiler") (v "0.1.4") (d (list (d (n "rust-forth-compiler") (r "^0.4.2") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.2") (d #t) (k 0)))) (h "0yi27ahzm6abfmgmd8dpha4r070g2anm2hg3g6k7s9dp0lnnkida")))

(define-public crate-rust-forth-interactive-compiler-0.1.5 (c (n "rust-forth-interactive-compiler") (v "0.1.5") (d (list (d (n "rust-forth-compiler") (r "^0.5.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)))) (h "0y3md2cqx3h0dyk7xgnbaw6wjz386fmbv94cmlvx9xl2b2ajc22w")))

(define-public crate-rust-forth-interactive-compiler-0.1.6 (c (n "rust-forth-interactive-compiler") (v "0.1.6") (d (list (d (n "rust-forth-compiler") (r "^0.5.2") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)))) (h "14ggmq098n21j8hjz927mnhf35xn9lk3bwk7jxfqx7d7dfhr92rd")))

(define-public crate-rust-forth-interactive-compiler-0.1.7 (c (n "rust-forth-interactive-compiler") (v "0.1.7") (d (list (d (n "rust-forth-compiler") (r "^0.5.3") (f (quote ("enable_reflection"))) (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)))) (h "1rhyjacp2m4493qx4yij4pjdk6d86ihiah7y9iblqg62zaspxvgw")))

