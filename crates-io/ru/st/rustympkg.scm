(define-module (crates-io ru st rustympkg) #:use-module (crates-io))

(define-public crate-rustympkg-0.1.0 (c (n "rustympkg") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "rustympkglib") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dk1hba0249351wzbnjrzrpa4jw9cqy2l8zrbnwwyvg5qb79l9gg")))

(define-public crate-rustympkg-0.1.1 (c (n "rustympkg") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "rustympkglib") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "133niyqdxygwhjci1vdvc7cidmz49hk939qind9bal9bb1m6y6l6")))

