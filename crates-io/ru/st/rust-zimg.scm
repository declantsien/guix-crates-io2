(define-module (crates-io ru st rust-zimg) #:use-module (crates-io))

(define-public crate-rust-zimg-0.1.0 (c (n "rust-zimg") (v "0.1.0") (d (list (d (n "image") (r "^0.9.1") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "multipart") (r "^0.9.1") (f (quote ("server" "iron"))) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.35") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0lmq5207m8px2i2f23hgf57zr2415vax315ypcgx5yvqxmr64ha7")))

