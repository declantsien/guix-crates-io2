(define-module (crates-io ru st rustsec-test-crate) #:use-module (crates-io))

(define-public crate-rustsec-test-crate-0.1.0 (c (n "rustsec-test-crate") (v "0.1.0") (h "1rkx8b46nyqx8ac15xd93brikbkmyv8vv3bpwqqixhlld6wfkj4r") (y #t)))

(define-public crate-rustsec-test-crate-1.0.0 (c (n "rustsec-test-crate") (v "1.0.0") (h "0515186kivlqa2gvnp714kdp2ch2i8y1wg0md24xq77yv0x6c7nh") (y #t)))

