(define-module (crates-io ru st rust-worker-build) #:use-module (crates-io))

(define-public crate-rust-worker-build-0.0.1 (c (n "rust-worker-build") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1i3ngh40n6yaj7lz0q2i9m4finhzk5l7wxnicncv1kix8zafhzbw") (y #t)))

(define-public crate-rust-worker-build-0.0.2 (c (n "rust-worker-build") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0viawndclms2idyy6yn1b0nrnqpv103ybi9y4pvj4350ysvd8s4p") (y #t)))

