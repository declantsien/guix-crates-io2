(define-module (crates-io ru st rust-sugiyama) #:use-module (crates-io))

(define-public crate-rust-sugiyama-0.1.0 (c (n "rust-sugiyama") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1mx929z3akawzwhyl80ydz3f17xds7i19p4as3d1m37mymkxmrmp")))

(define-public crate-rust-sugiyama-0.2.0 (c (n "rust-sugiyama") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0by26ljbd03rv77pjzl107dsqbhy8ys23kc7g2f75ffgl4a3ikl8")))

