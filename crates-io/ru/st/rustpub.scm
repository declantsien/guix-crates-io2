(define-module (crates-io ru st rustpub) #:use-module (crates-io))

(define-public crate-rustpub-0.1.0 (c (n "rustpub") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1gk7958s4bq651m9bbi0fhiz9fhk86ypyyn48i647k1xsay1w8pf") (y #t)))

(define-public crate-rustpub-0.1.1 (c (n "rustpub") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "198b8cq7gyfwvr7xdilri6byzp7c6rjj9ddvc1wcmjz2j2qrak9j") (y #t)))

(define-public crate-rustpub-0.1.2 (c (n "rustpub") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rustpub-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1xy0q66ghml3iyjwznf6p9p0hs31hvvss511ya1lcp18m9xzxvqf")))

(define-public crate-rustpub-0.1.3 (c (n "rustpub") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rustpub-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0gxpxq4vapss1l86hailbxjg6qlr3hg3r5inag3m5vhq7zik5vcz")))

(define-public crate-rustpub-0.1.4 (c (n "rustpub") (v "0.1.4") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rustpub-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0mxw7nb8ni6kfkk7dgsyd1jq4bz796agx6rnmp217q1a81b2p6m9")))

(define-public crate-rustpub-0.1.5 (c (n "rustpub") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rustpub-macro") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)))) (h "1fhjv8riiwxw7p4q0pb77yqrryx2gmv18a4iqza8p9b38i56pqxs")))

(define-public crate-rustpub-0.1.6 (c (n "rustpub") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rustpub-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)))) (h "07fbzmiag6gwxhpwd67vxcf1z6hwqdm4pv278ddz8d5k6hdjwns2")))

(define-public crate-rustpub-0.1.7 (c (n "rustpub") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rustpub-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)))) (h "021a3n62hlqf6bng8gpxkxrclvgshpz7wwh0dl3m52b0q2jmcd05") (f (quote (("simple") ("default" "simple") ("complex"))))))

(define-public crate-rustpub-0.1.8 (c (n "rustpub") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "rustpub-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)))) (h "09wxw6y5n897qnigs5865lmj2h81zwrb031rmwd66hghwbgxmgih") (f (quote (("simple") ("default" "simple") ("complex"))))))

