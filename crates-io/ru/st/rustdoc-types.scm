(define-module (crates-io ru st rustdoc-types) #:use-module (crates-io))

(define-public crate-rustdoc-types-0.1.0 (c (n "rustdoc-types") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15l9drkbplq1gfz0qg2wmyzfh8rn1l8zpym53h9nqrp405sncr5c") (y #t)))

(define-public crate-rustdoc-types-0.2.0 (c (n "rustdoc-types") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fdpaaqc4jghphlfrk2gcliz3vm2qk3av43bvd4plyls8fdjk8jj") (y #t)))

(define-public crate-rustdoc-types-0.3.0 (c (n "rustdoc-types") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07nbwsaqpl5lchm76jggskv7xqmsaaah48f5768sapbgf7hg9am8")))

(define-public crate-rustdoc-types-0.4.0 (c (n "rustdoc-types") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "065x9728naai24s8blibbrv4cgx6ck4gdpxp0jzyg668y1m415jd")))

(define-public crate-rustdoc-types-0.5.0 (c (n "rustdoc-types") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12q2hidv4b11564pf52qfc7s0gqzmqbv9smhp9s2gv39m1kdc9y8")))

(define-public crate-rustdoc-types-0.6.0 (c (n "rustdoc-types") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17fh4hccl457nd05gbl2s1qpk1w4v8kpih6gdmmljgfq80cwx8r2")))

(define-public crate-rustdoc-types-0.7.0 (c (n "rustdoc-types") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sim77kda83qk6zaal767cwvyc5730dzvdvx0yyn1y5lqcl8v0l3")))

(define-public crate-rustdoc-types-0.8.0 (c (n "rustdoc-types") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0maaysfqaajvjr9j9zqrwwbamgqgbj4255qrjy9i3q7xk51rjg21")))

(define-public crate-rustdoc-types-0.9.0 (c (n "rustdoc-types") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0z4kp9sxir2wr3xdcbk8h9ckg1m0hg19b77a2fp5wip02dzs372v")))

(define-public crate-rustdoc-types-0.10.0 (c (n "rustdoc-types") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0j2pnniqk9rjbr184wmkd505sljk6byh09h1y5dp2f4xjxb3znaw")))

(define-public crate-rustdoc-types-0.11.0 (c (n "rustdoc-types") (v "0.11.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zwcc15606sczglyhrxrdnr49rzayz3dxnnffzjxclhhapjjwygs")))

(define-public crate-rustdoc-types-0.12.0 (c (n "rustdoc-types") (v "0.12.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19iaqrwqmbaa71gb8r45vp2p6a3pafr1ix9hrmpqpv7bfv1n18mk")))

(define-public crate-rustdoc-types-0.13.0 (c (n "rustdoc-types") (v "0.13.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zhic8jk413lcl1bllci73ysp4x3kmizmr47wkvf537qzy4ijfbv")))

(define-public crate-rustdoc-types-0.13.1 (c (n "rustdoc-types") (v "0.13.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dmy815vmldh5axdkj0mhzwgh6lj9x6plv4dnd1rns93gbg4r2h8")))

(define-public crate-rustdoc-types-0.14.0 (c (n "rustdoc-types") (v "0.14.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a2c34c1cy4ms6a95jnjrbrvb0r9xaibkaw1hybbf1j0kbgyd2pb")))

(define-public crate-rustdoc-types-0.15.0 (c (n "rustdoc-types") (v "0.15.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j0qcrxsy5j21sdcaivklsc14r4rj8jlf7q0qx7ps43j27gyrla3")))

(define-public crate-rustdoc-types-0.16.0 (c (n "rustdoc-types") (v "0.16.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ci0nwf0wmv4diy4p835423ygbpsdgikclv4c3a661lm9k77w37p")))

(define-public crate-rustdoc-types-0.17.0 (c (n "rustdoc-types") (v "0.17.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01rrvipimpm612i8z373gyz3y72pqbiqih12awvxmdrqz2cb87x6")))

(define-public crate-rustdoc-types-0.18.0 (c (n "rustdoc-types") (v "0.18.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1hq1g8rcnx9v7mgb232lzj3yaj7rajgy02r75n6hi6nq8f5vcibp")))

(define-public crate-rustdoc-types-0.19.0 (c (n "rustdoc-types") (v "0.19.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04nzbbp5vx6bhr0wgv5bjfqfbi7cpm38801856r8bryikh75aqpx")))

(define-public crate-rustdoc-types-0.20.0 (c (n "rustdoc-types") (v "0.20.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "008mn8vmfxfnlp6754w6gdcq2i2xnvfgq4c71wgn7q89is29pxgm")))

(define-public crate-rustdoc-types-0.21.0 (c (n "rustdoc-types") (v "0.21.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19gzq288877f72i48dqdmxxn6gc5l90xwz5aifpy2c03ya58sxcq")))

(define-public crate-rustdoc-types-0.22.0 (c (n "rustdoc-types") (v "0.22.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03r9xyhn8ymz2ajqi8bs86s2w1p0j17a0lcqbjnfm039a33i4yjw")))

(define-public crate-rustdoc-types-0.23.0 (c (n "rustdoc-types") (v "0.23.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "095jdf44p0b02xq0n9vx1qpnrfgdqkd8nsbmp61r0s4x0q4nyr6c")))

(define-public crate-rustdoc-types-0.24.0 (c (n "rustdoc-types") (v "0.24.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n95f4rm9klxcm6850l9mfvc2xmgpsdy9f9zfrihx00if0abj109")))

(define-public crate-rustdoc-types-0.25.0 (c (n "rustdoc-types") (v "0.25.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g2gp0xpqlby9v9s6nrjcxik0wlx68ddblqw6kidkkizcca695k0")))

