(define-module (crates-io ru st rustme) #:use-module (crates-io))

(define-public crate-rustme-0.1.0 (c (n "rustme") (v "0.1.0") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "058ccya9bnj4ph1as39lzi5cmfyqwch4pwfkmrd7n9fl6lqbrqxz")))

(define-public crate-rustme-0.1.1 (c (n "rustme") (v "0.1.1") (d (list (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "1pml69rh20ygy48bmm45d969j1z18m9v43h39dbqn9h9g54bpsaw")))

