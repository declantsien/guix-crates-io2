(define-module (crates-io ru st rusty_core) #:use-module (crates-io))

(define-public crate-rusty_core-0.0.1 (c (n "rusty_core") (v "0.0.1") (h "0smh67brm71cqbm5qxsl6vd6nr5bk2rfcvpzb3hfa3iqkkq7bd18")))

(define-public crate-rusty_core-0.1.0 (c (n "rusty_core") (v "0.1.0") (h "0k8j2phxxwhjad7f2p42fpsg38bax62sw6sbvmcg3fgz0d27167q")))

(define-public crate-rusty_core-0.2.0 (c (n "rusty_core") (v "0.2.0") (d (list (d (n "nalgebra-glm") (r "^0.6.0") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0d2ripi8n3nfvbpf504c2s8klrgnln06wzgdyjfhqxkx3133f50w")))

(define-public crate-rusty_core-0.3.0 (c (n "rusty_core") (v "0.3.0") (d (list (d (n "nalgebra-glm") (r "^0.6.0") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1apdwfxbn70q0a0aw1al1hvq1y8rcp4bzjf66iq6fws2wjwi2lh4")))

(define-public crate-rusty_core-0.9.0 (c (n "rusty_core") (v "0.9.0") (d (list (d (n "nalgebra-glm") (r "^0.6.0") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1akr33g8hrfs9mmfs13nivqmqk3iplsyyfb8ilghj6bpij1529mn")))

(define-public crate-rusty_core-0.10.0 (c (n "rusty_core") (v "0.10.0") (d (list (d (n "nalgebra-glm") (r "^0.6.0") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1q18slh339jc6kls3c704zm8q42qdxl8gwkjh868ss13njssmbi5")))

(define-public crate-rusty_core-0.11.0 (c (n "rusty_core") (v "0.11.0") (d (list (d (n "nalgebra-glm") (r "^0.6.0") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0fabyca26iv2fwiw95ndd7xglxlzg68g9pnnqzcsbjjpaqkzvvhc")))

