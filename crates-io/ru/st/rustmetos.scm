(define-module (crates-io ru st rustmetos) #:use-module (crates-io))

(define-public crate-rustmetos-1.0.0 (c (n "rustmetos") (v "1.0.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "ini") (r "^1.3.0") (d #t) (k 0)) (d (n "rustmetos_core") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1kz3gxmhr75fks6l1zkpm84c18dxxmlffb355ywhf4fq540kav39")))

