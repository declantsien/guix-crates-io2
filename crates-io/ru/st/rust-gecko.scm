(define-module (crates-io ru st rust-gecko) #:use-module (crates-io))

(define-public crate-rust-gecko-0.0.0 (c (n "rust-gecko") (v "0.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "191anpgscmfjg1i8h4jrr1w6cl4vznw946knfvsvpar6wvsjkm67")))

(define-public crate-rust-gecko-0.0.1 (c (n "rust-gecko") (v "0.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "03nwaslcyggflxg3h0sd3i0jyhlikbf2fwgy3c3w96wjwdq47ix9")))

(define-public crate-rust-gecko-0.1.0 (c (n "rust-gecko") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "11c4lzkq3v2yrggqhjsd8bl5nzdkryfb6pfwbdz2z2fx6np9lp40")))

