(define-module (crates-io ru st rust_iso4217) #:use-module (crates-io))

(define-public crate-rust_iso4217-0.1.0 (c (n "rust_iso4217") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1p074l48q2aqbqcdl9ljmn6y0cdxpaq7icdzqijfy5bp18pd83ly")))

(define-public crate-rust_iso4217-0.1.1 (c (n "rust_iso4217") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "093gpg86qwsck546n1wgv38jqh5wwc28wfk5vxpzj7b1fd4br9g8")))

