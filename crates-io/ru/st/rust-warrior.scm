(define-module (crates-io ru st rust-warrior) #:use-module (crates-io))

(define-public crate-rust-warrior-0.1.0 (c (n "rust-warrior") (v "0.1.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "00g14lrnd9379wkz3pdrdkr1r8qri7kbjc1zx3kpzg7k5pxsialp")))

(define-public crate-rust-warrior-0.1.1 (c (n "rust-warrior") (v "0.1.1") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1fb0kasi8p0bqx5cpd1kymh868sz95pyd3sr9abzvv6d8b8ljsid")))

(define-public crate-rust-warrior-0.2.0 (c (n "rust-warrior") (v "0.2.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "12wyhlz5qfhmjj8y1jhvhjgm4fxrknk8k34sh4hvsbia0zdcxic3")))

(define-public crate-rust-warrior-0.3.0 (c (n "rust-warrior") (v "0.3.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0rdmsglkpca4swnbfnqrqlfl8ndmxrvb07ddvyml8mr8my79536v")))

(define-public crate-rust-warrior-0.3.1 (c (n "rust-warrior") (v "0.3.1") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1kkx14vgsc585bqynpdyx80z2crvhm8mjgwpw5gmx6qqs7xvpzyj")))

(define-public crate-rust-warrior-0.3.2 (c (n "rust-warrior") (v "0.3.2") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "187j78q7nb6qz9imzbcdrajf2vds10r20ljpvxv2r23pzw423mq1")))

(define-public crate-rust-warrior-0.3.3 (c (n "rust-warrior") (v "0.3.3") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0lv8g1qracabzjkafz3jlaczsqa5pyaamamarjflxjpl77biriwq")))

(define-public crate-rust-warrior-0.4.0 (c (n "rust-warrior") (v "0.4.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "17m7kbar39bd27ypay26is9vs9akk19hi87pc5g7bcaiybl5npc2")))

(define-public crate-rust-warrior-0.5.0 (c (n "rust-warrior") (v "0.5.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1zkk2c6ikx46k23nwspa6i2hqy1g21q5kl8scafrx6c2w8l5sscn")))

(define-public crate-rust-warrior-0.6.0 (c (n "rust-warrior") (v "0.6.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1cxnsfydqn40m1i3bw18i1zzsblpxcmyjf6hbrvzw3raxpkrk2in")))

(define-public crate-rust-warrior-0.7.0 (c (n "rust-warrior") (v "0.7.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0xyidqi4ip2qfdqlzx05b6x9rj43rbaffmgbws47cd7rn4yndfy6")))

(define-public crate-rust-warrior-0.8.0 (c (n "rust-warrior") (v "0.8.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "04i2inznc7wxj9zrf0vk470zasv8dig5gp8x75c18yvzahsxq5n9")))

(define-public crate-rust-warrior-0.8.1 (c (n "rust-warrior") (v "0.8.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "03xywq52hja574h0lg5bal84jw5g6vw1gkrm1wa87f77v5dwzh7g")))

(define-public crate-rust-warrior-0.8.2 (c (n "rust-warrior") (v "0.8.2") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "specs") (r "^0.15.0") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "06clwvsjb56rhr6y9ak1h67qr8yny90n0sm44ihbrx5l3niya666")))

(define-public crate-rust-warrior-0.9.0 (c (n "rust-warrior") (v "0.9.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.15") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gncjqbzi7aklzp4qv34rl6fi7izk1njgb987i5qmk3gnzlh9him")))

(define-public crate-rust-warrior-0.9.1 (c (n "rust-warrior") (v "0.9.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.15") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1mq4na215p5rm5svwhq251k232jr03xvgbcqfwmp4i0iqgas359b")))

(define-public crate-rust-warrior-0.9.2 (c (n "rust-warrior") (v "0.9.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.16") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hw3y7p66amkzhs1q86a0sj7xx086266d1fbwi6jhxi60f9grdvj")))

(define-public crate-rust-warrior-0.9.3 (c (n "rust-warrior") (v "0.9.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.17") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0cdlihav6gdjzz37cbiwn5g66sydqbr64arz03f01c6b3q2krsxl")))

(define-public crate-rust-warrior-0.10.0 (c (n "rust-warrior") (v "0.10.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.17") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0h72zwk3yhgd5p1bq5gdlzial0axcxzz3nf1cgwd1fhxy5fp1mnz")))

(define-public crate-rust-warrior-0.11.0 (c (n "rust-warrior") (v "0.11.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.17") (f (quote ("specs-derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "09716agfljaa7lw5wicvmnm4cwpcb4liihc0zbdkcvm5gqdgcmjk")))

(define-public crate-rust-warrior-0.12.0 (c (n "rust-warrior") (v "0.12.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ikyqm6zzlz5yld47k4j79n7i50knd7n9dn3pa6nw40ma962qrjs")))

(define-public crate-rust-warrior-0.12.1 (c (n "rust-warrior") (v "0.12.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0wfvpi4cl75244cir6l1ybyhn475j1b840lv3ryz9v6prvy2msyb")))

(define-public crate-rust-warrior-0.12.2 (c (n "rust-warrior") (v "0.12.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1agyibdavrd31zfnr1n5qg4fla35cp778ysrmjgwld1nqmgrnnh9")))

(define-public crate-rust-warrior-0.13.0 (c (n "rust-warrior") (v "0.13.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "ncurses") (r "^5.101") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bsljszd6k8pc80p8xljcpkrqd0v8wbi9cyzyf72cwi7jxz4339n")))

(define-public crate-rust-warrior-0.14.0 (c (n "rust-warrior") (v "0.14.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "ncurses-lite") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "059br3dgfs77pclv1hy1fsad7ds77gw5xs11vqpka6ipdfz35w67") (s 2) (e (quote (("ncurses" "dep:ncurses-lite"))))))

