(define-module (crates-io ru st rusty_agent) #:use-module (crates-io))

(define-public crate-rusty_agent-0.1.0 (c (n "rusty_agent") (v "0.1.0") (d (list (d (n "futures") (r "~0.3") (d #t) (k 0)) (d (n "zmq") (r "~0.9") (d #t) (k 0)))) (h "13b50a9rljhfqrkhmh456m90ln2rp5h03iplb764m5aq9i9n0xqa")))

(define-public crate-rusty_agent-0.1.1 (c (n "rusty_agent") (v "0.1.1") (d (list (d (n "futures") (r "~0.3") (d #t) (k 0)) (d (n "zmq") (r "~0.9") (d #t) (k 0)))) (h "0vcc4pb52331mfd1igb7p3j9xnflcksrvx55k4nqjxbhdx9xzqqz")))

