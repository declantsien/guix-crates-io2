(define-module (crates-io ru st rust-sasa) #:use-module (crates-io))

(define-public crate-rust-sasa-0.1.0 (c (n "rust-sasa") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rstar") (r "^0.12.0") (d #t) (k 0)))) (h "166y7g7cfa939ljv4ily4qj4259i863vhsz3wrv558i55xpgg5z7") (y #t)))

(define-public crate-rust-sasa-0.1.1 (c (n "rust-sasa") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rstar") (r "^0.12.0") (d #t) (k 0)))) (h "1ai7qv1ihn9lngd2j6cijm00hc51ysi0lig98q2vyvdmlnj82r9j")))

(define-public crate-rust-sasa-0.2.0 (c (n "rust-sasa") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rstar") (r "^0.12.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8.2") (d #t) (k 0)))) (h "0j70y8fvisqaq9aqvn16lcam5brj6dmh0rk3gfsi7znppdblf9p8")))

(define-public crate-rust-sasa-0.2.1 (c (n "rust-sasa") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rstar") (r "^0.12.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8.2") (d #t) (k 0)))) (h "1c805gj50822142fi043ycymfyc2y999w0nm1pv1dmnvz8hlhbw8")))

(define-public crate-rust-sasa-0.2.2 (c (n "rust-sasa") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rstar") (r "^0.12.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8.2") (d #t) (k 0)))) (h "0ns9k2c0x6p9q4x5j2q7cpj147fd2302123hc15bs8vs594ln41y")))

