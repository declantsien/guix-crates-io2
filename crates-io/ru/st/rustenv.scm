(define-module (crates-io ru st rustenv) #:use-module (crates-io))

(define-public crate-rustenv-0.0.1 (c (n "rustenv") (v "0.0.1") (h "0gg0hyrxy42nrg0hmbcr20wyb5aylv70gam4wsw4gnvnqxd7yjpv")))

(define-public crate-rustenv-0.0.2 (c (n "rustenv") (v "0.0.2") (h "0sx4a1yx3285rkaxq42w7fhn1kmk3crdhk72q1xqlzg63ns63vrj") (f (quote (("write") ("default"))))))

(define-public crate-rustenv-1.0.0 (c (n "rustenv") (v "1.0.0") (h "1j959wgb9c6xjsprz02insf1861imfy6qpxmggxqa9gszvz8iyy1") (f (quote (("write") ("read") ("default") ("all" "read" "write"))))))

