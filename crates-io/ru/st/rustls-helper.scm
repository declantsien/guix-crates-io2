(define-module (crates-io ru st rustls-helper) #:use-module (crates-io))

(define-public crate-rustls-helper-0.1.0 (c (n "rustls-helper") (v "0.1.0") (d (list (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)))) (h "04aynlbklqzq9xbn69has1h5ldjm3gbd612f7bx0nmhqvk2c8f3j") (y #t)))

(define-public crate-rustls-helper-0.1.1 (c (n "rustls-helper") (v "0.1.1") (d (list (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)))) (h "1kqd0vyy0idn4nmbh8lszjfslvk3lszlh6m4d58rk1asbyyha0zp") (y #t)))

(define-public crate-rustls-helper-0.1.2 (c (n "rustls-helper") (v "0.1.2") (d (list (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)))) (h "0warl9z3ncrq1dvips3i49fx2h5m6j6zhky3lq33png9ix4r0yfm") (y #t)))

(define-public crate-rustls-helper-0.1.3 (c (n "rustls-helper") (v "0.1.3") (d (list (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)))) (h "0kqq8k6kcfayhq4mp7f9znawjpl11lbv8y043a2fgly33y9kqvj2") (y #t)))

(define-public crate-rustls-helper-0.2.0 (c (n "rustls-helper") (v "0.2.0") (d (list (d (n "rustls") (r "^0.21") (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)))) (h "19vpxq9fmxn39hkwam2ik5adqbz1s13cr47h6f275qhniny6qkzk") (y #t)))

(define-public crate-rustls-helper-0.2.1 (c (n "rustls-helper") (v "0.2.1") (d (list (d (n "rustls") (r "^0.21") (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)))) (h "0a2cxwglm0nqlqw0adrn1s75fl3llsmz48an216yafdrqc750jj2") (y #t)))

