(define-module (crates-io ru st rusty-todo) #:use-module (crates-io))

(define-public crate-rusty-todo-0.1.0 (c (n "rusty-todo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "1rp1bliqx82pbd9bi08vjawhd7d66kvfcj9kyk5wlxciwss6szwh")))

