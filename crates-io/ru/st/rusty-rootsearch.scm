(define-module (crates-io ru st rusty-rootsearch) #:use-module (crates-io))

(define-public crate-rusty-rootsearch-0.1.1 (c (n "rusty-rootsearch") (v "0.1.1") (d (list (d (n "num-dual") (r "^0.5.2") (d #t) (k 0)))) (h "0zwzik6hrqp7pcjlx692h5zmcgjp7lj6hgjasnxg1gwm0s5d6mxv")))

(define-public crate-rusty-rootsearch-0.1.2 (c (n "rusty-rootsearch") (v "0.1.2") (d (list (d (n "num-dual") (r "^0.5.2") (d #t) (k 0)))) (h "0g8sinbhxh89ylr4i3y5cwmcflgawgdrqr97mvv88hrdl1zvam0c")))

(define-public crate-rusty-rootsearch-0.1.3 (c (n "rusty-rootsearch") (v "0.1.3") (d (list (d (n "num-dual") (r "^0.5.2") (d #t) (k 0)))) (h "02xr8xs6ffx58npc6i7vw50qdvshkphpn6qiy9vyj1pcq3rgaimp")))

(define-public crate-rusty-rootsearch-0.1.4 (c (n "rusty-rootsearch") (v "0.1.4") (d (list (d (n "num-dual") (r "^0.5.2") (d #t) (k 0)))) (h "1556b5h3fc57hhji2g0k6px9ncrh3w9xsbcm8c2nvq15ragnzw09")))

(define-public crate-rusty-rootsearch-0.2.0 (c (n "rusty-rootsearch") (v "0.2.0") (d (list (d (n "num-dual") (r "^0.5.2") (d #t) (k 0)))) (h "0b2qp12jndarqc345i7z0i8mjc0csqxmbsbpcyav6kyfwnngiwy8")))

(define-public crate-rusty-rootsearch-0.2.1 (c (n "rusty-rootsearch") (v "0.2.1") (d (list (d (n "num-dual") (r "^0.5.3") (d #t) (k 0)))) (h "0mhfrf0v5c3z3g8gyym6y5fap6r8wx7fjqvcgzzs3nznni07hilv")))

(define-public crate-rusty-rootsearch-0.3.0 (c (n "rusty-rootsearch") (v "0.3.0") (d (list (d (n "num-dual") (r "^0.7.1") (d #t) (k 0)))) (h "1a2zkdmqd7jaz2d1kxvr65czmarbfclwc6ax1zn2wh8i86xjzwrj")))

(define-public crate-rusty-rootsearch-0.3.1 (c (n "rusty-rootsearch") (v "0.3.1") (d (list (d (n "num-dual") (r "^0.7.1") (d #t) (k 0)))) (h "0wgd1pijc4mxdqg9ms0bbg2w99wm6mgy8558dajaa7wnb3i5irps")))

(define-public crate-rusty-rootsearch-0.4.0 (c (n "rusty-rootsearch") (v "0.4.0") (d (list (d (n "num-dual") (r "^0.7.1") (d #t) (k 0)))) (h "03z83mvaqnz54iqz74hb4s5qki65yipdqyd9sbk861ijh3nilfvp")))

