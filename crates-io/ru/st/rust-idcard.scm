(define-module (crates-io ru st rust-idcard) #:use-module (crates-io))

(define-public crate-rust-idcard-0.1.0 (c (n "rust-idcard") (v "0.1.0") (h "1rj7gp1az0pj6lg92h9jw719py87l2ymp282dsszr3nyxnxfn8za")))

(define-public crate-rust-idcard-0.1.1 (c (n "rust-idcard") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04r9yqkr1ijqz2azd5jy3laafhswifd6grn8dff5fyb3hmc9dzz6")))

(define-public crate-rust-idcard-0.1.2 (c (n "rust-idcard") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i8nhnk5dvxg75d1nzzmdikd1diasrnh3sh9dk17jxcwnypq2n2k")))

