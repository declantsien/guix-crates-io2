(define-module (crates-io ru st rustimate-templates) #:use-module (crates-io))

(define-public crate-rustimate-templates-0.0.1 (c (n "rustimate-templates") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3.40") (d #t) (k 0)) (d (n "maud") (r "^0.21.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rustimate-core") (r "^0.0.1") (d #t) (k 0)) (d (n "rustimate-service") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jjyngda1ibihw3wwpds4dcfxk62sac4k4hj5708091g1p9kyp0r")))

(define-public crate-rustimate-templates-0.1.0 (c (n "rustimate-templates") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "maud") (r "^0.21.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "rustimate-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rustimate-service") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0lhqpd7c2v5634q1chkikkm5084i77r74g1rdlir85p93d402268")))

