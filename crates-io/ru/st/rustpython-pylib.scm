(define-module (crates-io ru st rustpython-pylib) #:use-module (crates-io))

(define-public crate-rustpython-pylib-0.3.0 (c (n "rustpython-pylib") (v "0.3.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "rustpython-compiler-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rustpython-derive") (r "^0.3.0") (d #t) (k 0)))) (h "0002asdi1jr4qfld8hshsd2jnzja58yqdlagayjkmy6xylq6b4xr") (f (quote (("freeze-stdlib"))))))

(define-public crate-rustpython-pylib-0.3.1 (c (n "rustpython-pylib") (v "0.3.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "rustpython-compiler-core") (r "^0.3.1") (d #t) (k 0)) (d (n "rustpython-derive") (r "^0.3.1") (d #t) (k 0)))) (h "1dc64m7ga05kfv7fcmkrc6mx6yz44c7sb8ci5a5gqddy04j9ppvb") (f (quote (("freeze-stdlib"))))))

