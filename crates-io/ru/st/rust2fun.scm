(define-module (crates-io ru st rust2fun) #:use-module (crates-io))

(define-public crate-rust2fun-0.1.0 (c (n "rust2fun") (v "0.1.0") (d (list (d (n "rust2fun_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1pqws9m7p36yqk1l46kiiamlfkfwwimxqd1sfnj3zqz8059zi2s2") (f (quote (("std") ("default" "std"))))))

(define-public crate-rust2fun-0.2.0 (c (n "rust2fun") (v "0.2.0") (d (list (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rust2fun_macros") (r "^0.2.0") (d #t) (k 0)))) (h "1gpnk1ily3mhbdfyvg6jgdffi6zn3qpnkfd7pn7h3hh4k0gg0n2r") (f (quote (("std") ("default" "std"))))))

(define-public crate-rust2fun-0.2.1 (c (n "rust2fun") (v "0.2.1") (d (list (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rust2fun_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0zhp4yjajvgdagfyxkg8n3949jvrg52ppifgl6xhvcfly6swgbi8") (f (quote (("std") ("default" "std"))))))

