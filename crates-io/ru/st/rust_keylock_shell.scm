(define-module (crates-io ru st rust_keylock_shell) #:use-module (crates-io))

(define-public crate-rust_keylock_shell-0.2.0 (c (n "rust_keylock_shell") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "fern") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^0.3") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.2") (d #t) (k 0)))) (h "05h63jl0akf5b1mxgkclc5024bpw4sg7qpv9fhsv6wwgnidlc1py")))

(define-public crate-rust_keylock_shell-0.2.1 (c (n "rust_keylock_shell") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "fern") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^0.3") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.2.1") (d #t) (k 0)))) (h "0ld2rphg2lsa4w8rf8v6h0lyzyy2pk2afdcvmckmfvnv2fak8rk6")))

(define-public crate-rust_keylock_shell-0.3.0 (c (n "rust_keylock_shell") (v "0.3.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "fern") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.3") (d #t) (k 0)))) (h "0g89q2rbq2a3fi69wgy76zy8qgxf8yfa84d96bxxyi8bxiqzr873")))

(define-public crate-rust_keylock_shell-0.4.0 (c (n "rust_keylock_shell") (v "0.4.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "fern") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.4") (d #t) (k 0)))) (h "1mp5090s29wdc7cyv87fqxkkbsidzbcq2dszwq8skz3ylm1clgq6")))

(define-public crate-rust_keylock_shell-0.5.0 (c (n "rust_keylock_shell") (v "0.5.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "fern") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.5") (d #t) (k 0)))) (h "0mqmya63vnxr4fcihp7l8wj87z9ihd9pj7c2yrwm311f7jk6akhx")))

(define-public crate-rust_keylock_shell-0.6.0 (c (n "rust_keylock_shell") (v "0.6.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "fern") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.6.0") (d #t) (k 0)))) (h "1z6w26304b4jlijajn34b49smbx85bdmy4ij676fs9xxx339xwzk")))

(define-public crate-rust_keylock_shell-0.7.0 (c (n "rust_keylock_shell") (v "0.7.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "fern") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^0.4") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.7") (d #t) (k 0)))) (h "0gzdmw7ay393i3csqp2i4fs270mfz6gnm3k7cf4s2pwgdvwzv3ps")))

(define-public crate-rust_keylock_shell-0.8.0 (c (n "rust_keylock_shell") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^2.1") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.8") (d #t) (k 0)))) (h "0bhhy0581jkvn1blrw37zfb6a799c0gan8kzmbw998nqp8f002ca")))

(define-public crate-rust_keylock_shell-0.8.2 (c (n "rust_keylock_shell") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^2.1") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.8.2") (d #t) (k 0)))) (h "1cbz3x8mjs1pc1hfmw5z71q7x1f5gy2qdp5zr02gv5cgb518sib8")))

(define-public crate-rust_keylock_shell-0.9.1 (c (n "rust_keylock_shell") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.9") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "1fqdjrb58p41x39ah49sw2231dvc61hagsn20cpclzn4fsv7pj7i") (y #t)))

(define-public crate-rust_keylock_shell-0.10.0 (c (n "rust_keylock_shell") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.10.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "0swbms11f10pajjsywzafwp6pk2ajqmdr28vm8j2ixry6dkc6fp9")))

(define-public crate-rust_keylock_shell-0.11.0 (c (n "rust_keylock_shell") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^3.0") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.11") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "1dhvpilhi6yx8bc0hc0wis93plvjiqysnk6w67vp66mddidlcl3h")))

(define-public crate-rust_keylock_shell-0.12.0 (c (n "rust_keylock_shell") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.12") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "14ljjpxhlank36m5gpnxq1k31l3fv7slgs85xhn7x3n30qx2xs36")))

(define-public crate-rust_keylock_shell-0.13.0 (c (n "rust_keylock_shell") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.13") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "0bhy1683qczf4cn6zhsnzn9ia5f5mnvw418bn5nljn2ah7gblvna")))

(define-public crate-rust_keylock_shell-0.14.0 (c (n "rust_keylock_shell") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.14") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "11khv2idw78km4whzmbk4mz7510yy9d9l6faj3bd2amspcg0d58g")))

(define-public crate-rust_keylock_shell-0.15.0 (c (n "rust_keylock_shell") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "1mq8318nv6dhfbrs2114l5130h0lrdgdx85xqhwz2lab1sjgiwz7")))

(define-public crate-rust_keylock_shell-0.15.1 (c (n "rust_keylock_shell") (v "0.15.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "rust_keylock") (r "^0.15") (d #t) (k 0)) (d (n "termcolor") (r "^1.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "0nm31vzsggyjcgda87ry0agx43ibz4n0pml7c3q32xwpv3fz65mq")))

