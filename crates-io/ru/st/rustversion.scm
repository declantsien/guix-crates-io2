(define-module (crates-io ru st rustversion) #:use-module (crates-io))

(define-public crate-rustversion-0.1.3 (c (n "rustversion") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.25") (f (quote ("full"))) (d #t) (k 0)))) (h "158mxvmn1kxxskvb3l4sj4abkcs315byli3i52rr831424ljsxbl")))

(define-public crate-rustversion-0.1.4 (c (n "rustversion") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s3ib2paa5gq17x4qsmjmnsw68z7b5d5av1wsiqcrihmqb7kk0dl")))

(define-public crate-rustversion-1.0.0 (c (n "rustversion") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rarcqxx4apm7fzma98ig96nngv67wfi6pf1b19vxwsfgybr33y4")))

(define-public crate-rustversion-1.0.1 (c (n "rustversion") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "11krxgi7j6h5crhs6ws06wwzjvgdqaazvli805xja5vyi6ykh19s")))

(define-public crate-rustversion-1.0.2 (c (n "rustversion") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xkr1g792w728py2qpg2zj0vfviv2xzmxkkd9w6035l9d5ss3fxk")))

(define-public crate-rustversion-1.0.3 (c (n "rustversion") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ni9kvqij4vaqyyh21f9q932jhz5ynhi6v7v8rc6h7p5avlcbgdr")))

(define-public crate-rustversion-1.0.4 (c (n "rustversion") (v "1.0.4") (d (list (d (n "trybuild") (r "^1.0.35") (d #t) (k 2)))) (h "1kas7n19n097fjjhdwrv36yjjvih0j5lkqzx2vyxiln6dl1jlpfb")))

(define-public crate-rustversion-1.0.5 (c (n "rustversion") (v "1.0.5") (d (list (d (n "trybuild") (r "^1.0.35") (d #t) (k 2)))) (h "1250m7ymrhp3c5xfmliskmknhf23r7x3cirxy9wmrdwbfnfr1cv1")))

(define-public crate-rustversion-1.0.6 (c (n "rustversion") (v "1.0.6") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gxj6skypbk0wlbks3pdqb0lclpwbzmyv9xbqkijsvk6zbl3ik7j") (r "1.31")))

(define-public crate-rustversion-1.0.7 (c (n "rustversion") (v "1.0.7") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1krzk0w1ii52km06qkggwszj4i4836ybbk0wka989lpm533zg9d0") (r "1.31")))

(define-public crate-rustversion-1.0.8 (c (n "rustversion") (v "1.0.8") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1n6qb80hpc3gw34cls3bszdy600kgxm26kb1qxdypq801i7svj14") (r "1.31")))

(define-public crate-rustversion-1.0.9 (c (n "rustversion") (v "1.0.9") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1a6nlrrnfbacn5pzg3m3311anhngcxs8kbvsbynh71ngni47wiwp") (r "1.31")))

(define-public crate-rustversion-1.0.10 (c (n "rustversion") (v "1.0.10") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ih87ilx5fmnscxdipnrrm1zm83xx4c0di9y63ibcw1qa64yly23") (r "1.31")))

(define-public crate-rustversion-1.0.11 (c (n "rustversion") (v "1.0.11") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0w4da3xx21ih64lr1f210gfvj4ahmady03v665h515l922gfi0sm") (r "1.31")))

(define-public crate-rustversion-1.0.12 (c (n "rustversion") (v "1.0.12") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "01mzns4b7vfcxsyf63ck68gachqcbqzsfs6iwzrv6j449p70hcjg") (r "1.31")))

(define-public crate-rustversion-1.0.13 (c (n "rustversion") (v "1.0.13") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0pxx0mxigrvzsbpkw54wjyx4jfm85bcss60dcpwk2b53c6dvscfw") (r "1.31")))

(define-public crate-rustversion-1.0.14 (c (n "rustversion") (v "1.0.14") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1x1pz1yynk5xzzrazk2svmidj69jhz89dz5vrc28sixl20x1iz3z") (r "1.31")))

(define-public crate-rustversion-1.0.15 (c (n "rustversion") (v "1.0.15") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0iyy66mldq0z7h6n2zm6fw2bndw04pifhv5s7xda8xzj668nzbw0") (r "1.31")))

(define-public crate-rustversion-1.0.16 (c (n "rustversion") (v "1.0.16") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1c6zy65i4jmhn2raxpb3p6wfbyh5hjcmi8z6d67jga0yl38p8909") (r "1.31")))

(define-public crate-rustversion-1.0.17 (c (n "rustversion") (v "1.0.17") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1mm3fckyvb0l2209in1n2k05sws5d9mpkszbnwhq3pkq8apjhpcm") (r "1.31")))

