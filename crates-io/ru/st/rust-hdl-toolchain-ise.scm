(define-module (crates-io ru st rust-hdl-toolchain-ise) #:use-module (crates-io))

(define-public crate-rust-hdl-toolchain-ise-0.1.0 (c (n "rust-hdl-toolchain-ise") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-toolchain-common") (r "^0.1") (d #t) (k 0)))) (h "0qz59id5cdi5sgjr4cn6ly85732cx7vmjxkcy8da56371jl6h20g") (y #t)))

