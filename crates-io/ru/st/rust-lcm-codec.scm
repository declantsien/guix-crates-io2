(define-module (crates-io ru st rust-lcm-codec) #:use-module (crates-io))

(define-public crate-rust-lcm-codec-0.2.0 (c (n "rust-lcm-codec") (v "0.2.0") (h "0bjf85mbbd2jhl9fgpnv1v2pzmzsb61zqf8x04r15xiafh98iqnr")))

(define-public crate-rust-lcm-codec-0.2.1 (c (n "rust-lcm-codec") (v "0.2.1") (h "1isw31w2vim2n2hj27nz2n97rcnssyiwbcn496m8vvcbrb20bp2q")))

