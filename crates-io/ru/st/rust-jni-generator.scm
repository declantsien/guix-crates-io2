(define-module (crates-io ru st rust-jni-generator) #:use-module (crates-io))

(define-public crate-rust-jni-generator-0.1.0 (c (n "rust-jni-generator") (v "0.1.0") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "rust-jni") (r "^0.1.0") (d #t) (k 0)))) (h "125202577p04hix0021xbkd1zahkmqgz0hdrybkvzs8fc1gl4404")))

