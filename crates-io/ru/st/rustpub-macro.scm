(define-module (crates-io ru st rustpub-macro) #:use-module (crates-io))

(define-public crate-rustpub-macro-0.1.0 (c (n "rustpub-macro") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "11n28brbzgbi9qc003ngr3bdvzqsrkqm9mav0ywwwbwwqhsmmfk5")))

(define-public crate-rustpub-macro-0.1.1 (c (n "rustpub-macro") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "17kv9134v0ns7492zkg6kj1n9msmhdxi04yj49c7b4rh1qv9wxx4")))

(define-public crate-rustpub-macro-0.1.2 (c (n "rustpub-macro") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1438qk4vldnjh6l6b5yiv449ibg65xigffq3qd0f560ryha9j8j8")))

(define-public crate-rustpub-macro-0.1.3 (c (n "rustpub-macro") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "013hhws1kwnmr0dm6qjnad0mdk8qr96b6y519gf6byi250jwznpn")))

(define-public crate-rustpub-macro-0.1.4 (c (n "rustpub-macro") (v "0.1.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "13qyi3ds02jh4hq90gg7vhiv5qvbm40rpw5w180k5bn0l221shlh")))

