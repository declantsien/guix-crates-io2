(define-module (crates-io ru st rust-bitfield) #:use-module (crates-io))

(define-public crate-rust-bitfield-0.1.0 (c (n "rust-bitfield") (v "0.1.0") (h "19lqx4919j7dlv4d81a7cacm33c5dvzx3985vhykhp1jjrmgb2fy") (y #t)))

(define-public crate-rust-bitfield-0.1.1 (c (n "rust-bitfield") (v "0.1.1") (h "02fizlzb5rbm1dgzpca2wzv2i0528x1hlhsc47lq57f3giz3djcx")))

