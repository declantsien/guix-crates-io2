(define-module (crates-io ru st rusty_state) #:use-module (crates-io))

(define-public crate-rusty_state-0.1.0 (c (n "rusty_state") (v "0.1.0") (h "1841f80s8ibd4yv7yl6wbj8cp9ljjqdx4gvly6aa9i7iraqv1yb0")))

(define-public crate-rusty_state-0.1.1 (c (n "rusty_state") (v "0.1.1") (h "0x2lr6062czpp6543in13aysahfvp2mqx5rr3habq0gg8n3b20j5")))

