(define-module (crates-io ru st rustycat) #:use-module (crates-io))

(define-public crate-rustycat-0.1.0 (c (n "rustycat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "128c0g02d59zgbn3ivjz2m7a4sh6qc9gr1w0lw53vdcx76gi4j9w") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "chrono/serde") ("build-binary" "dep:clap" "serde"))))))

