(define-module (crates-io ru st rustbust) #:use-module (crates-io))

(define-public crate-rustbust-0.1.0 (c (n "rustbust") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "1db16ikv92q8yd7fy8fa9bhk9niknbjg6spgsbm46rv0zp9cqywb")))

