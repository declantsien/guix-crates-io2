(define-module (crates-io ru st rust_lzss) #:use-module (crates-io))

(define-public crate-rust_lzss-0.1.0 (c (n "rust_lzss") (v "0.1.0") (d (list (d (n "bit-vec") (r "~0.5") (d #t) (k 0)) (d (n "byteorder") (r "~1") (d #t) (k 0)))) (h "1gmkcybhmmcakf877jzj6nc7j1z9ih2x6x73h80g8xjfz59ksi3l") (y #t)))

(define-public crate-rust_lzss-0.1.1 (c (n "rust_lzss") (v "0.1.1") (d (list (d (n "bit-vec") (r "~0.5") (d #t) (k 0)) (d (n "byteorder") (r "~1") (d #t) (k 0)))) (h "02yca19k3ga8nmw19f8yw2lwcs1h78xmcgh7cplqyvizlzcjw0pz")))

