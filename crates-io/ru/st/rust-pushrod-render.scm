(define-module (crates-io ru st rust-pushrod-render) #:use-module (crates-io))

(define-public crate-rust-pushrod-render-0.1.0 (c (n "rust-pushrod-render") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "1lix4crjzshxxpdcjm5659diqzhmv7ghimmvmbzanpazqc9d0a6z") (y #t)))

(define-public crate-rust-pushrod-render-0.1.1 (c (n "rust-pushrod-render") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "10w5r2p465qsfs1xsa4qm321kdlbr69rp24bcxxphyykx5ryri8g") (y #t)))

(define-public crate-rust-pushrod-render-0.1.2 (c (n "rust-pushrod-render") (v "0.1.2") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "1hl7dpd6xk89ab7h28jllniqvl2q7nsvrfq06rw4xw25mqcbizdc") (y #t)))

(define-public crate-rust-pushrod-render-0.1.3 (c (n "rust-pushrod-render") (v "0.1.3") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "1a2vyk6s0gkfqbb13vfnqiq2dzp89rqgqab5v646z88ifdszx6di") (y #t)))

(define-public crate-rust-pushrod-render-0.1.4 (c (n "rust-pushrod-render") (v "0.1.4") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "14i91zry46wli7hsb6fbkyb493l81fkc17gzga7vjpsp84yw8xas") (y #t)))

(define-public crate-rust-pushrod-render-0.1.5 (c (n "rust-pushrod-render") (v "0.1.5") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "1210kfvw1wf519q517rc9424372l8l93ksnllh9ffic5hnp230af") (y #t)))

(define-public crate-rust-pushrod-render-0.1.6 (c (n "rust-pushrod-render") (v "0.1.6") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "0wjqm1fymc4g7bw2xb2mhrswdc4il3ydby08q1c28a07sjcbxqi4") (y #t)))

(define-public crate-rust-pushrod-render-0.1.7 (c (n "rust-pushrod-render") (v "0.1.7") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "0s1kc4a6rzbi1cgks43pbqr0nfcgywrikh5fbchsdrmnsdwckwlf") (y #t)))

(define-public crate-rust-pushrod-render-0.1.8 (c (n "rust-pushrod-render") (v "0.1.8") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "05nv3n6k2f2cbjv6547a75bf3v1ar803pazpyg110by4xbk5ymb7") (y #t)))

(define-public crate-rust-pushrod-render-0.1.9 (c (n "rust-pushrod-render") (v "0.1.9") (d (list (d (n "sdl2") (r "^0.32") (d #t) (k 0)))) (h "1d39g17n19xiv0c881ww0m86jh6afrfqk2y4ia1qlv2745rbd75r") (y #t)))

