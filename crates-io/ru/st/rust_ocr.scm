(define-module (crates-io ru st rust_ocr) #:use-module (crates-io))

(define-public crate-rust_ocr-0.1.0 (c (n "rust_ocr") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Media_Ocr" "Graphics" "Graphics_Imaging" "Globalization" "Storage" "Foundation" "Storage_Streams" "Foundation_Collections"))) (d #t) (k 0)))) (h "0f6zzlpahzk2glvvm3yr5imzqvrgrjzklkd4l33zwk4y0h7ygc4p")))

(define-public crate-rust_ocr-0.1.1 (c (n "rust_ocr") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Media_Ocr" "Graphics" "Graphics_Imaging" "Globalization" "Storage" "Foundation" "Storage_Streams" "Foundation_Collections"))) (d #t) (k 0)))) (h "0q5kn2djlffpdipb4kckgpjldrp8nx89nyfyg0gwlxabgr0xrfnk")))

(define-public crate-rust_ocr-0.1.2 (c (n "rust_ocr") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Media_Ocr" "Graphics" "Graphics_Imaging" "Globalization" "Storage" "Foundation" "Storage_Streams" "Foundation_Collections"))) (d #t) (k 0)))) (h "1yfxmzdwg2q342jpks1hlv8dyr3ia9i115knri92dslkygvw5mnh")))

(define-public crate-rust_ocr-0.1.3 (c (n "rust_ocr") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Media_Ocr" "Graphics" "Graphics_Imaging" "Globalization" "Storage" "Foundation" "Storage_Streams" "Foundation_Collections"))) (d #t) (k 0)))) (h "1vyfzdxrpyhidzx638s6b21pikpngpb55rg4bdcc3fyj5vj7rvzx")))

(define-public crate-rust_ocr-0.1.4 (c (n "rust_ocr") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Media_Ocr" "Graphics" "Graphics_Imaging" "Globalization" "Storage" "Foundation" "Storage_Streams" "Foundation_Collections"))) (d #t) (k 0)))) (h "13b22s3g82fpkd80wxnairrip89dfi7hvjksmipygapj75dn8rgq")))

(define-public crate-rust_ocr-0.1.5 (c (n "rust_ocr") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Media_Ocr" "Graphics" "Graphics_Imaging" "Globalization" "Storage" "Foundation" "Storage_Streams" "Foundation_Collections"))) (d #t) (k 0)))) (h "0rvbn8hp5in9jb21wmvli5xsxkwbfq9cxzgzkgkk8z0bd9scmjlp")))

