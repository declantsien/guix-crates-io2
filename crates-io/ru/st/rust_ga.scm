(define-module (crates-io ru st rust_ga) #:use-module (crates-io))

(define-public crate-rust_ga-0.1.0 (c (n "rust_ga") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gg8cvswnaz9l7j8y9ig0qzbkqnk1q47x8igd2ba2fb4wcpilwj7")))

(define-public crate-rust_ga-0.1.1 (c (n "rust_ga") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0amdrrvs6nqj24p4sg9jivj9x1fi0lb85civfdpi86ry22qva25w")))

(define-public crate-rust_ga-0.1.2 (c (n "rust_ga") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0rfn27kp57ircdhiz4v580mazf4g745rf6rnp9hnm96h5z06sgjs")))

(define-public crate-rust_ga-0.1.3 (c (n "rust_ga") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0lybbbias5zf534z7hcprmg0d4lcxkznlqsl513rg0kzsnjdw7gd")))

(define-public crate-rust_ga-0.1.4 (c (n "rust_ga") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1pdgmy02ma47mh1qxx9j3vsx8q2i82p7hrv7x5g81krz30z3g4lm")))

(define-public crate-rust_ga-0.1.5 (c (n "rust_ga") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0zsf5jwfajyym6w4rpqrz1sq7rsaqsklk1p78iavlv00yszzjxh2")))

(define-public crate-rust_ga-0.1.6 (c (n "rust_ga") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04d56i02gv9nihyx7gdgrvr90vcqkbzjnv5ai1vala1yzmbq2fqz")))

(define-public crate-rust_ga-0.1.7 (c (n "rust_ga") (v "0.1.7") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1rqq4dmr3xwkyc4lxqkmwxri2yj093iwz83hxbbxqhck4cnncv82")))

(define-public crate-rust_ga-0.1.8 (c (n "rust_ga") (v "0.1.8") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0g3lpc0cdrfqp9j57g8g1mh8jw4aiqb4lpwj04ky4bx6swm7n07n")))

