(define-module (crates-io ru st rustversion-msrv) #:use-module (crates-io))

(define-public crate-rustversion-msrv-0.99.16 (c (n "rustversion-msrv") (v "0.99.16") (d (list (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "17glsrsxzzabyr2dhpmfbqiyfzs62gal9igjyj1wxw5i6br7ajig") (r "1.31")))

