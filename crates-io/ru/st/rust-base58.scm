(define-module (crates-io ru st rust-base58) #:use-module (crates-io))

(define-public crate-rust-base58-0.0.1 (c (n "rust-base58") (v "0.0.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "05xsxg4i3aba193945sbxqjvbs7y5n2y17scicvh5rcsrvhfm0mj")))

(define-public crate-rust-base58-0.0.2 (c (n "rust-base58") (v "0.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "09car77jyl0r58bfwidd9k376hbdiqmakr24fdqh31ax0n7j6rl3")))

(define-public crate-rust-base58-0.0.4 (c (n "rust-base58") (v "0.0.4") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0fa4y2jjjmg1a0cr3gz4z8rkic0hx2vx5nm23za9lwf6rlgvj4xk")))

