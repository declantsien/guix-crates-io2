(define-module (crates-io ru st rusty_vainfo) #:use-module (crates-io))

(define-public crate-rusty_vainfo-0.1.0 (c (n "rusty_vainfo") (v "0.1.0") (d (list (d (n "libva-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1r4i3dmbs150dhj552k0rg9518gab4psn6rj4cx83vs6rkfz1wwm")))

(define-public crate-rusty_vainfo-0.1.1 (c (n "rusty_vainfo") (v "0.1.1") (d (list (d (n "libva-sys") (r "^0.1.1") (d #t) (k 0)))) (h "03lgkb04vk8993g2g6gbl428zyk6hckr5ylbipamcnyn1mfsp7dl")))

(define-public crate-rusty_vainfo-0.1.2 (c (n "rusty_vainfo") (v "0.1.2") (d (list (d (n "libva-sys") (r "^0") (d #t) (k 0)))) (h "0v9rmgjmzx1r1nl9pm122v03gm4znxdschcwrdnif7w207icjnj3")))

(define-public crate-rusty_vainfo-0.1.3 (c (n "rusty_vainfo") (v "0.1.3") (d (list (d (n "libva-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0m0p8ndszbg186nicn2fr4gf220z2yn17f71710cgsrmmlgiqwlr")))

(define-public crate-rusty_vainfo-0.1.4 (c (n "rusty_vainfo") (v "0.1.4") (d (list (d (n "libva-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1y6asv6d4jkivvj6sy8xdlfm4nnxnsm0wp1rkcqj9981bjy38zhs")))

