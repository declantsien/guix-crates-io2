(define-module (crates-io ru st rust-locale) #:use-module (crates-io))

(define-public crate-rust-locale-0.1.0 (c (n "rust-locale") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gkxzq3h7fxxi8vj03hncsadgkrgwmqdx7jhxqmh9b28myg9ad3d")))

(define-public crate-rust-locale-0.1.1 (c (n "rust-locale") (v "0.1.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sw6n3jfw9wghfiw65zlnfqd6jag4gryp7va27bp6r8n843adrsa")))

(define-public crate-rust-locale-0.1.2 (c (n "rust-locale") (v "0.1.2") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dx7ikxy6k3x52pfsfciz52sznf9n6bksl1zlgxkj4wq4rfx0pxg")))

(define-public crate-rust-locale-0.1.3 (c (n "rust-locale") (v "0.1.3") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kab8zp5wpjgwlvy9l3nxq4g0pwf475ha4m3cqik9ijx91z386q4")))

(define-public crate-rust-locale-0.1.4 (c (n "rust-locale") (v "0.1.4") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "009fkwm461hkbjhxwd5pvqn06rdc5vid8j6jql5ypzf9g1lm2ig1")))

