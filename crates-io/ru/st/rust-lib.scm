(define-module (crates-io ru st rust-lib) #:use-module (crates-io))

(define-public crate-rust-lib-0.1.0 (c (n "rust-lib") (v "0.1.0") (h "12j17h1nffsac7axa1vgb34q4rl0vyhamk088mhzhma3v93wbakh")))

(define-public crate-rust-lib-0.1.1 (c (n "rust-lib") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nbpn4h0a288lra8lzi3cs0s0xsy09b2pcd8flwzwi1z91zfqm0a") (y #t)))

