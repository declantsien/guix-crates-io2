(define-module (crates-io ru st rustytrader) #:use-module (crates-io))

(define-public crate-RustyTrader-0.1.0 (c (n "RustyTrader") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "polars") (r "^0.38.0") (d #t) (k 0)))) (h "0pg92vmva6pxissgdwbw1c2k6zslywaifhcswhcn4zvc0g5qsnvm")))

