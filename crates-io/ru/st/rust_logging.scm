(define-module (crates-io ru st rust_logging) #:use-module (crates-io))

(define-public crate-rust_logging-1.0.0 (c (n "rust_logging") (v "1.0.0") (d (list (d (n "isatty") (r "^0.1.9") (d #t) (k 0)))) (h "0pn8k54vqisnnsg2sz0mdlcp1s9x6cihghn7kwpysxrc2d4wslj1")))

(define-public crate-rust_logging-1.1.0 (c (n "rust_logging") (v "1.1.0") (d (list (d (n "isatty") (r "^0.1.9") (d #t) (k 0)))) (h "0hhy840hbx3lv5fi74j9gzygs3ri2nlxijkkl1lxrpbbippcab1r")))

