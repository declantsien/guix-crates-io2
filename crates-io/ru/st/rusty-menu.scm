(define-module (crates-io ru st rusty-menu) #:use-module (crates-io))

(define-public crate-rusty-menu-0.1.0 (c (n "rusty-menu") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)))) (h "1la8q8kc699wwzvwybw8myy5zqfvym5m5a1qbl43n66xd2r61cvd")))

(define-public crate-rusty-menu-0.1.1 (c (n "rusty-menu") (v "0.1.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)))) (h "17lhqvj6cjai0nmchr0fc5aig0yywd3q5i8i8j1gmvm6h6r6bla6")))

(define-public crate-rusty-menu-0.1.3 (c (n "rusty-menu") (v "0.1.3") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)))) (h "0rzrvsfgycrb6gw80w814wm7zycwfxz56iqf9whb434m7chgfbah")))

