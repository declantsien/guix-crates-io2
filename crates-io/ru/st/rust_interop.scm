(define-module (crates-io ru st rust_interop) #:use-module (crates-io))

(define-public crate-rust_interop-0.0.1 (c (n "rust_interop") (v "0.0.1") (d (list (d (n "rust_interop_derive") (r "^0.0.1") (d #t) (k 0)))) (h "1wda4wwzvh3n1xpnq8bw9mycpvx1imn1abxmjsblyhrcqsrsihy3") (y #t)))

(define-public crate-rust_interop-0.0.2 (c (n "rust_interop") (v "0.0.2") (h "07swgxszn6p5phagbjx7whfp11z6g0r5a8yjx7ghl5bdswgf1d84") (y #t)))

