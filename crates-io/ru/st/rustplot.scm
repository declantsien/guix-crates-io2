(define-module (crates-io ru st rustplot) #:use-module (crates-io))

(define-public crate-rustplot-0.1.0 (c (n "rustplot") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0") (f (quote ("png"))) (d #t) (k 0)) (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0l5ry1wr43s1j7z99qh5h9s66qlmr2v27i63y4nk34k4s3yp2yp9")))

