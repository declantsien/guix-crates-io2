(define-module (crates-io ru st rustc_codegen_spirv-types) #:use-module (crates-io))

(define-public crate-rustc_codegen_spirv-types-0.4.0-alpha.13 (c (n "rustc_codegen_spirv-types") (v "0.4.0-alpha.13") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nq9772qjca3lcq3hriqn01ffc7y62597hc84m7spsrzaswr2kna")))

(define-public crate-rustc_codegen_spirv-types-0.4.0-alpha.14 (c (n "rustc_codegen_spirv-types") (v "0.4.0-alpha.14") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gkak8cpmviiingi84a5q5i4nfqmqm5j0dd82wajfsmnps65ljr3")))

(define-public crate-rustc_codegen_spirv-types-0.4.0-alpha.15 (c (n "rustc_codegen_spirv-types") (v "0.4.0-alpha.15") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wkl3wli90c0p63pmssvadbm49jwd0f9rbkjl0dx7bsfzm5kc7na")))

(define-public crate-rustc_codegen_spirv-types-0.4.0-alpha.16 (c (n "rustc_codegen_spirv-types") (v "0.4.0-alpha.16") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b5dq30smkiydv8zszpwz10p0idql1zam5aknbjyvdl7pa3lwvgy")))

(define-public crate-rustc_codegen_spirv-types-0.4.0-alpha.17 (c (n "rustc_codegen_spirv-types") (v "0.4.0-alpha.17") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x4srq0k7ysf2kp4j0zprc7n61n2c69xvqr6774icf5r3mv48dqp")))

(define-public crate-rustc_codegen_spirv-types-0.4.0 (c (n "rustc_codegen_spirv-types") (v "0.4.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06qc8dan3g2my0nvzp966zd987na7gfs17yf7m80n97h9hafq2zh")))

(define-public crate-rustc_codegen_spirv-types-0.5.0 (c (n "rustc_codegen_spirv-types") (v "0.5.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bjliyiaf2fcyvjfkyh0zyf0lcbp2cn99yy7ik2z76wvn33fs2ik")))

(define-public crate-rustc_codegen_spirv-types-0.6.0 (c (n "rustc_codegen_spirv-types") (v "0.6.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06s10gqqllf5dcilvkjvz7xb21biilprby824llnw0mssv5apqp7")))

(define-public crate-rustc_codegen_spirv-types-0.6.1 (c (n "rustc_codegen_spirv-types") (v "0.6.1") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z43bq9wykcpb20lh0cqni443jlpxv0ifjyknm5118d7cgky34y3")))

(define-public crate-rustc_codegen_spirv-types-0.7.0 (c (n "rustc_codegen_spirv-types") (v "0.7.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qkkmvzaw47y6j0qslf1wkmscfflqj1jc6zaxi6j1rz2zkbhk4rk")))

(define-public crate-rustc_codegen_spirv-types-0.8.0 (c (n "rustc_codegen_spirv-types") (v "0.8.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19qdbjjs1imykq7nn98v4l32xmj6s4r8yn77awzjqw5l4w3l2nxl")))

(define-public crate-rustc_codegen_spirv-types-0.9.0 (c (n "rustc_codegen_spirv-types") (v "0.9.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "043px5vyml96d1zc647nzqj01r2safk3y2f22wka1lk8pl8kfcqk")))

