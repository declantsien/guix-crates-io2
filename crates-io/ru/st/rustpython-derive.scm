(define-module (crates-io ru st rustpython-derive) #:use-module (crates-io))

(define-public crate-rustpython-derive-0.1.0 (c (n "rustpython-derive") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "rustpython-bytecode") (r "^0.1.0") (d #t) (k 0)) (d (n "rustpython-compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "00f14ykbsq19xri0i11na88k8jyqrkkrvj7q6s7blkqxh820cwij")))

(define-public crate-rustpython-derive-0.1.1 (c (n "rustpython-derive") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "rustpython-bytecode") (r "^0.1.1") (d #t) (k 0)) (d (n "rustpython-compiler") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0sv73vppzk6izi2zyp3s3rgp430z0b0axjdkq65k9xqyh7qbbw8g") (f (quote (("default" "proc-macro-hack"))))))

(define-public crate-rustpython-derive-0.1.2 (c (n "rustpython-derive") (v "0.1.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustpython-bytecode") (r "^0.1.1") (d #t) (k 0)) (d (n "rustpython-compiler") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qan9dchl6hfqd7j7djfxra6m5sr9inkv6z9vcgkzcqapl6cbs10")))

(define-public crate-rustpython-derive-0.3.0 (c (n "rustpython-derive") (v "0.3.0") (d (list (d (n "rustpython-compiler") (r "^0.3.0") (d #t) (k 0)) (d (n "rustpython-derive-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "184ncxy1d8zfdq2b3rl26kjnk5f98yzb1pbqqaii79aqdpw9bhvg")))

(define-public crate-rustpython-derive-0.3.1 (c (n "rustpython-derive") (v "0.3.1") (d (list (d (n "rustpython-compiler") (r "^0.3.1") (d #t) (k 0)) (d (n "rustpython-derive-impl") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1sx8v3rjafnjpmicwf5xvyvh5r875jqmfx1d5si9zibk6ndw5jhg")))

