(define-module (crates-io ru st rust_transit_rabbit) #:use-module (crates-io))

(define-public crate-rust_transit_rabbit-0.1.0 (c (n "rust_transit_rabbit") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lapin") (r "^0.34.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rust_transit") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_transit_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1alxc2l0zcv0xbd5q2wp2rhqkx3p4gq30ydhqrl7p2fp9n7d8g85")))

