(define-module (crates-io ru st ruster_unsafe) #:use-module (crates-io))

(define-public crate-ruster_unsafe-0.1.0 (c (n "ruster_unsafe") (v "0.1.0") (d (list (d (n "libc") (r ">= 0.1") (d #t) (k 0)))) (h "1f5mlng94addydpp07w6alixabwshf31xch7fa3kiivv3l7fgisz")))

(define-public crate-ruster_unsafe-0.2.0 (c (n "ruster_unsafe") (v "0.2.0") (d (list (d (n "libc") (r ">= 0.1") (d #t) (k 0)))) (h "0rs09081wc9w83yb9pnkyxlsr3pii2adgw12nym09gv89l1mcdr1")))

(define-public crate-ruster_unsafe-0.3.0 (c (n "ruster_unsafe") (v "0.3.0") (d (list (d (n "libc") (r ">= 0.1") (d #t) (k 0)))) (h "0fs00f2iizhfj3igzvv7g0p5dzkzs891rbmv7zlalcx3ic38kyl6")))

(define-public crate-ruster_unsafe-0.4.0 (c (n "ruster_unsafe") (v "0.4.0") (d (list (d (n "libc") (r ">= 0.1") (d #t) (k 0)) (d (n "libc") (r ">= 0.1") (d #t) (k 1)))) (h "0wi8ahx8lm86c9cygc2zlm6bny5lqjxlx6lrynvnjqnwq198g5xz")))

