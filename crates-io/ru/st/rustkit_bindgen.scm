(define-module (crates-io ru st rustkit_bindgen) #:use-module (crates-io))

(define-public crate-rustkit_bindgen-0.0.1 (c (n "rustkit_bindgen") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rustkit-clang-sys") (r "^0.24") (f (quote ("runtime" "clang_8_0"))) (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kqkchngibkyfxgkhl0sjgrk6czpxk688qv54z6ms5wqmqgwh1b7")))

