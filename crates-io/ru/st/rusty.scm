(define-module (crates-io ru st rusty) #:use-module (crates-io))

(define-public crate-rusty-0.0.2 (c (n "rusty") (v "0.0.2") (h "1gvsidlwhb1pad84prwnsabhndfy2zhq4xab39fqkykf2pfn0w2p") (y #t)))

(define-public crate-rusty-0.1.0 (c (n "rusty") (v "0.1.0") (h "1rqv9j5r75vsv26qqz263myx9waqc3xw13kga0avdmpzw5mjnmnl") (y #t)))

(define-public crate-rusty-0.2.0 (c (n "rusty") (v "0.2.0") (d (list (d (n "cpython") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1wgrdikg9q8jq26y9v8b2hr7ylan2fkkdlcln9y54597yyg0lbqj") (y #t)))

(define-public crate-rusty-0.3.0 (c (n "rusty") (v "0.3.0") (d (list (d (n "cpython") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "174cpqm2ah784b1k69f95jm3mh90a59kyyfjfad1hpqbfavrgc7c") (y #t)))

