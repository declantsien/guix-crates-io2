(define-module (crates-io ru st rust_graphics_window) #:use-module (crates-io))

(define-public crate-rust_graphics_window-0.1.0 (c (n "rust_graphics_window") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rust_graphics_library_loader") (r "^0.1") (d #t) (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)))) (h "0qaz4xgwf571a3s9fgj1m9bgrh57akfafna5r3hb4f4k000wwjsp")))

(define-public crate-rust_graphics_window-0.1.1 (c (n "rust_graphics_window") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rust_graphics_library_loader") (r "^0.1") (d #t) (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)))) (h "045rjhmqxdi2nn73v7jfgg395b4nkipzhfgcprb4kbw1jx3cj6n0") (f (quote (("verbose_log") ("debug_derive"))))))

(define-public crate-rust_graphics_window-0.1.2 (c (n "rust_graphics_window") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rust_graphics_library_loader") (r "^0.1") (d #t) (k 0)) (d (n "rust_graphics_log") (r "^0.1") (d #t) (k 0)))) (h "0l6yxnf9s13vy5p64a0vh2ivbc0dzj419wih29b40v7lwc21x9c3") (f (quote (("verbose_log") ("debug_derive"))))))

