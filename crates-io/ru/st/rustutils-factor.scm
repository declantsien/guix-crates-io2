(define-module (crates-io ru st rustutils-factor) #:use-module (crates-io))

(define-public crate-rustutils-factor-0.1.0 (c (n "rustutils-factor") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16dwiwvnzy5f5gn6pv3xxbg8l99ax3amacgss86yxnvayvl85nhl") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

