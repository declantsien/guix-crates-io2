(define-module (crates-io ru st rusty-rescuetime) #:use-module (crates-io))

(define-public crate-rusty-rescuetime-0.1.0 (c (n "rusty-rescuetime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.19.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "0899sacgnkvzzmbcs69bxmqd8aabng3wfygsdi22fv4q2yab7p9a")))

