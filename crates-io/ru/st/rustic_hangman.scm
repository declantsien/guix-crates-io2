(define-module (crates-io ru st rustic_hangman) #:use-module (crates-io))

(define-public crate-Rustic_Hangman-0.1.0 (c (n "Rustic_Hangman") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "13jvynchry4x7yr6c6fr4q1q19n560nw7l8d5hprcpamdf1aiz0z")))

(define-public crate-Rustic_Hangman-0.1.1 (c (n "Rustic_Hangman") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "19k4v5x6ggksyhij84nkzk4464xdi737s0ss8azfda2s26gq256p")))

