(define-module (crates-io ru st rustbasic-macro) #:use-module (crates-io))

(define-public crate-rustbasic-macro-0.0.1 (c (n "rustbasic-macro") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02xd0pimrq5ri4m1prmpa51wrq0c3i61s07mba299qal74xx4qrg")))

(define-public crate-rustbasic-macro-0.0.2 (c (n "rustbasic-macro") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0bicvpxnxjj8mmg2xmsf3xh19ack7rpq28pny75w8l6svj9wxbn9")))

(define-public crate-rustbasic-macro-0.0.3 (c (n "rustbasic-macro") (v "0.0.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "15wdlwvhccs5d9kgmbgy5w2pn1q4k09vscvza15ac75kbh81px20")))

(define-public crate-rustbasic-macro-0.0.4 (c (n "rustbasic-macro") (v "0.0.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0l25b1y7zm3p9cxw92mfnjxj0ysr4kiwz2wp05j74mcc52bb49jc")))

(define-public crate-rustbasic-macro-0.0.5 (c (n "rustbasic-macro") (v "0.0.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhcwcbxgh72vz3k7rx7wld509ncamsb778zp6mcirrmrrvcl2hm")))

(define-public crate-rustbasic-macro-0.0.6 (c (n "rustbasic-macro") (v "0.0.6") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1ngn8gwqg5c30xcfxa20p77d1jn4bjkn1wxdcrd3y4nz4lb7p0c2")))

(define-public crate-rustbasic-macro-0.0.7 (c (n "rustbasic-macro") (v "0.0.7") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1z3q456w6gx0axyfvv2964rblmswvagln585lh20fsm8nkpwj07s")))

(define-public crate-rustbasic-macro-0.0.8 (c (n "rustbasic-macro") (v "0.0.8") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1k38rmvgd08skiba4wraxjsz9hpchlxjzdja5y0zwd92wcdmnjmf")))

(define-public crate-rustbasic-macro-0.0.9 (c (n "rustbasic-macro") (v "0.0.9") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1w29gyhj2rik3nj57g056n3lqlb7xmmhk37ck0h9lgm71syxy23c")))

(define-public crate-rustbasic-macro-0.0.10 (c (n "rustbasic-macro") (v "0.0.10") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "19fsk7k2p0345c06q2ah3lrhrr7l214l25zh3awm3zs943yirdm1")))

