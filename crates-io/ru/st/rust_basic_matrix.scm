(define-module (crates-io ru st rust_basic_matrix) #:use-module (crates-io))

(define-public crate-rust_basic_matrix-0.0.1 (c (n "rust_basic_matrix") (v "0.0.1") (h "1z56y6i5693mncqwp2sn0b7yng019db6q80h2110gphsn1kxjxxi")))

(define-public crate-rust_basic_matrix-0.0.2 (c (n "rust_basic_matrix") (v "0.0.2") (h "0g5las05iijfj2849gdzskddgq5cfw3aw4y22cbgkqxsh7fa4f95")))

(define-public crate-rust_basic_matrix-0.0.3 (c (n "rust_basic_matrix") (v "0.0.3") (h "1r7c7973wc6l3imc4z2hq7mdh2npgdsrjnsmd54byil8pri1pc2k")))

(define-public crate-rust_basic_matrix-0.0.4 (c (n "rust_basic_matrix") (v "0.0.4") (h "0v94w0gmbpqkd6aqm07z7vwsgrry92004lnllm0psasgh76r780i")))

