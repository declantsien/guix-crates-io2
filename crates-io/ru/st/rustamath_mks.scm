(define-module (crates-io ru st rustamath_mks) #:use-module (crates-io))

(define-public crate-rustamath_mks-0.1.0 (c (n "rustamath_mks") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)))) (h "1fm9l68dwqqpgksh8zab1gpzqi00haikhx6g5l1yi8j1xaj076dz")))

(define-public crate-rustamath_mks-0.1.1 (c (n "rustamath_mks") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)))) (h "18jj6x3c9ryb838dskfl2d0b4c21xy76vxzvxq9aajzbxl91grga")))

