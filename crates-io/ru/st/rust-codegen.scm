(define-module (crates-io ru st rust-codegen) #:use-module (crates-io))

(define-public crate-rust-codegen-0.1.0 (c (n "rust-codegen") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)))) (h "0lxij710smg0i7nrqhhimnlvhxkgpzcviv9whhjq94zivn27c0af")))

(define-public crate-rust-codegen-0.1.1 (c (n "rust-codegen") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)))) (h "0vsk4vifrk37yq7gyg50a9cwq0pmnmvgb1x6hg13mw113cmky8pw")))

