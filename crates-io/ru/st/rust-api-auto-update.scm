(define-module (crates-io ru st rust-api-auto-update) #:use-module (crates-io))

(define-public crate-rust-api-auto-update-1.0.0 (c (n "rust-api-auto-update") (v "1.0.0") (d (list (d (n "actix-web") (r "^4.0") (d #t) (k 0)) (d (n "clokwerk") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)) (d (n "version-compare") (r "^0.1.1") (d #t) (k 0)))) (h "025rcrfrqj1bdc043b58dha25g7g9jpn6xk7xz8rlbl23hs5bc1f")))

