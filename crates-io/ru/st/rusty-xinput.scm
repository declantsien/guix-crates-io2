(define-module (crates-io ru st rusty-xinput) #:use-module (crates-io))

(define-public crate-rusty-xinput-1.0.0 (c (n "rusty-xinput") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "simple_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wvirnbgyb4mbqaq53n2d6msyzl1k40c2kyb9ikgg1w4qks95h3r")))

(define-public crate-rusty-xinput-1.0.1 (c (n "rusty-xinput") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "simple_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06zg113g2wpd7r5fzri0yi4z14a5zd6gxw6j2gmfbcaxfjbgcn1s")))

(define-public crate-rusty-xinput-1.1.0 (c (n "rusty-xinput") (v "1.1.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "simple_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "076j24ghwrw6xc06bp02zap7vxqxhrg8j2g2n7p9q0xp3bvcjrq1")))

(define-public crate-rusty-xinput-1.2.0 (c (n "rusty-xinput") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "simple_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fxk5lkkjk09k8k3az2lli4kkr6zr6mq9871rhacmf9fqd5nbanj")))

(define-public crate-rusty-xinput-1.2.1 (c (n "rusty-xinput") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15d2rq5qqklpj8wbnimg8sl5fxs581hj1jxzdzk4mbrp6wafjcpp")))

(define-public crate-rusty-xinput-1.3.0 (c (n "rusty-xinput") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wvywizfl6s3235snjllzabkcviq0lbr9j7n4zcqvr71c8mmqcy3")))

