(define-module (crates-io ru st rustcord-sys) #:use-module (crates-io))

(define-public crate-rustcord-sys-0.1.0 (c (n "rustcord-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0w9g62jsbrsnqrvygzrdda66490bq4ih29n87ag7xhj5q08fn63z")))

(define-public crate-rustcord-sys-0.2.0 (c (n "rustcord-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "01yv64r1s7zcfyyq692fh5wp9xnb5kyhp27jh1rmcmzxd8980wvc")))

(define-public crate-rustcord-sys-0.2.1 (c (n "rustcord-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0z9ml2kcf2kb0cm979dsr9vb2qlkj5v36n0xd52a6zy989idd9lp")))

(define-public crate-rustcord-sys-0.2.2 (c (n "rustcord-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)))) (h "0cfv3mv8f23spj0n2byx6ca1dh63pd39450qj7rjlikscx8mbbqy")))

(define-public crate-rustcord-sys-0.2.4 (c (n "rustcord-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0s66j8mq82inanxpwkwwqs1a4skpz0m4wvjbv1m36niiss78q0vm")))

