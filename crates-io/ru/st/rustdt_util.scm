(define-module (crates-io ru st rustdt_util) #:use-module (crates-io))

(define-public crate-rustdt_util-0.2.0 (c (n "rustdt_util") (v "0.2.0") (h "1j0xp48h7grikx4c39imgz96jnl1fpd2ns74agp9bri1sjh1ryah") (f (quote (("test_utils") ("default" "test_utils"))))))

(define-public crate-rustdt_util-0.2.1 (c (n "rustdt_util") (v "0.2.1") (h "1b48yz9cq7viiymj36yi6bpkf2bmhgyqmsxfmagmchcxlm1ra96f") (f (quote (("test_utils") ("default" "test_utils")))) (y #t)))

(define-public crate-rustdt_util-0.2.2 (c (n "rustdt_util") (v "0.2.2") (h "08wcar1wh4ryfd42095l75yq1qa8ysrmfwz1q673cg4rh44wzhf7") (f (quote (("test_utils") ("default" "test_utils"))))))

(define-public crate-rustdt_util-0.2.3 (c (n "rustdt_util") (v "0.2.3") (h "1zar5i8k2k9xjp7g7swi4jpmbq5wcbyca1anvpi8nxfqi65gmzvw") (f (quote (("test_utils") ("default" "test_utils"))))))

