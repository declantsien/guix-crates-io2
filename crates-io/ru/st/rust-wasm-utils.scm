(define-module (crates-io ru st rust-wasm-utils) #:use-module (crates-io))

(define-public crate-rust-wasm-utils-0.3.0 (c (n "rust-wasm-utils") (v "0.3.0") (d (list (d (n "clippy") (r "= 0.0.174") (o #t) (d #t) (k 0)))) (h "1jvzh52ndnmf6f0cxrhp9hj98jjpv923zm69lkydm1xn2727q9zv") (f (quote (("lint" "clippy"))))))

