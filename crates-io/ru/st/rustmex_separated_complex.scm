(define-module (crates-io ru st rustmex_separated_complex) #:use-module (crates-io))

(define-public crate-rustmex_separated_complex-0.1.0 (c (n "rustmex_separated_complex") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.1") (d #t) (k 0)))) (h "0a9j5k9nrq88g72q4rgz3y0w6infrw28p6dqgfq1nzknza754pqd")))

(define-public crate-rustmex_separated_complex-0.2.0 (c (n "rustmex_separated_complex") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.2") (d #t) (k 0)))) (h "1h6xg6qz2dw48f5gs1318xwhnivl9qc7hdvaxnhwxy24hvr5dmxx")))

