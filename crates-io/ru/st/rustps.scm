(define-module (crates-io ru st rustps) #:use-module (crates-io))

(define-public crate-rustps-0.1.0 (c (n "rustps") (v "0.1.0") (d (list (d (n "algori") (r "^0.6.1") (d #t) (k 0)))) (h "1i0nrj21g2zzk7hkpkg8wbxbyf8m67k5jbwrjz1dvljx58pk3i0c")))

(define-public crate-rustps-0.2.0 (c (n "rustps") (v "0.2.0") (d (list (d (n "algori") (r "^0.6.1") (d #t) (k 0)))) (h "0dignmir96sxf6y3aq5zsgf360s5gm713n48qq15pzrf9znkw4mi")))

(define-public crate-rustps-0.3.0 (c (n "rustps") (v "0.3.0") (d (list (d (n "algori") (r "^0.6.1") (d #t) (k 0)))) (h "1qyxww0izx50r7kriri5xqdg71yladjkb6r65mbp0xir6clbbr5m")))

(define-public crate-rustps-0.4.0 (c (n "rustps") (v "0.4.0") (d (list (d (n "algori") (r "^0.6.1") (d #t) (k 0)))) (h "04fn3p8l8f0qcyxc8m3qh7k75k691fcbs4qqjg9iwa0gbmrbw4df")))

