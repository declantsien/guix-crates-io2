(define-module (crates-io ru st rustutils-arch) #:use-module (crates-io))

(define-public crate-rustutils-arch-0.1.0 (c (n "rustutils-arch") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "1hyddh6ls66lfprvvhbrd1pr6wg5md698v1651gzx36byc37z3f4")))

