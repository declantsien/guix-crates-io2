(define-module (crates-io ru st rust-world) #:use-module (crates-io))

(define-public crate-Rust-WORLD-0.1.0 (c (n "Rust-WORLD") (v "0.1.0") (h "1bzrph95ffcg5kf5bqr766335k13jd2f8nchgx7qgc9712df5wfd")))

(define-public crate-Rust-WORLD-0.1.1 (c (n "Rust-WORLD") (v "0.1.1") (d (list (d (n "rsworld") (r "^0.1.0") (d #t) (k 0)) (d (n "rsworld-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1hqzi1rglnv1p40x6k178jmj76qnkz6f5ckgyc5x246a70cnzzva")))

