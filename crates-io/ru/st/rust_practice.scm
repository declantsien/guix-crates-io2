(define-module (crates-io ru st rust_practice) #:use-module (crates-io))

(define-public crate-rust_practice-0.1.0 (c (n "rust_practice") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1jppz8kfww2k7xhi8pphb43hcn386b54zcp65n852020v6h5apkb")))

(define-public crate-rust_practice-0.1.1 (c (n "rust_practice") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0ryvcnr9yrbjv6gmipwqpa3sylgk4q6vwd34nq7x2sg8mfg12325")))

