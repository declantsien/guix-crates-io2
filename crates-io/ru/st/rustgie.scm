(define-module (crates-io ru st rustgie) #:use-module (crates-io))

(define-public crate-rustgie-0.0.1 (c (n "rustgie") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "gzip" "deflate" "brotli" "cookies"))) (d #t) (k 0)) (d (n "rustgie_types") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("serde" "serde-well-known"))) (d #t) (k 0)))) (h "0qv23jf7ph39q6pa1isrk0p8nxb2nv7ri71ys9pgymd84v8bg412")))

(define-public crate-rustgie-0.1.0 (c (n "rustgie") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "gzip" "deflate" "brotli" "cookies"))) (d #t) (k 0)) (d (n "rustgie_types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)))) (h "0i0fgs3x3j00d3vi213ni148i029jhafsg34xpsd5yzyxg3a397l")))

(define-public crate-rustgie-0.1.1 (c (n "rustgie") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "gzip" "deflate" "brotli" "cookies"))) (d #t) (k 0)) (d (n "rustgie_types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)))) (h "0i0wc0g6wvcv0w3v8slw03h97wzwagd1crj2m5229qlfdjvfinb4")))

(define-public crate-rustgie-0.2.0 (c (n "rustgie") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.13") (f (quote ("json" "gzip" "deflate" "brotli" "cookies"))) (d #t) (k 0)) (d (n "rustgie_types") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "0xl75x1gdw1h43bfd63kkrdmyb6blsv6kcg91mnzbd6bvgam2kgq") (f (quote (("rustls" "reqwest/rustls"))))))

