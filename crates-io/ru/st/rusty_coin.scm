(define-module (crates-io ru st rusty_coin) #:use-module (crates-io))

(define-public crate-rusty_coin-0.1.0 (c (n "rusty_coin") (v "0.1.0") (h "16vvdqcdg30ffd1bxgcsd80kh2f1y7nwqqwbig45dzlkg4vdwrbm")))

(define-public crate-rusty_coin-0.2.3 (c (n "rusty_coin") (v "0.2.3") (h "0lpj3cvfnrhvvk0d7659nxvj24h2qx4h3cvgn7i5b396w23jrz2b")))

(define-public crate-rusty_coin-0.2.4 (c (n "rusty_coin") (v "0.2.4") (h "0d89q1fw2p3kz9zk5a1ag2hnkrslkwnp8ac1dixzdf99zcz7fgrs")))

