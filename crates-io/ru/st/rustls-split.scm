(define-module (crates-io ru st rustls-split) #:use-module (crates-io))

(define-public crate-rustls-split-0.1.0 (c (n "rustls-split") (v "0.1.0") (d (list (d (n "rustls") (r "^0.19") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (d #t) (k 0)))) (h "0b83icx1szf20wsx8sackrnjjjf2pg3s7p919530ncqnl1py2b39")))

(define-public crate-rustls-split-0.1.1 (c (n "rustls-split") (v "0.1.1") (d (list (d (n "rustls") (r "^0.19") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (d #t) (k 0)))) (h "13bv54x7pjdvjhipqnbk07bb83ghnar6bnq4g9zxayxqqq75nzlx")))

(define-public crate-rustls-split-0.1.2 (c (n "rustls-split") (v "0.1.2") (d (list (d (n "rustls") (r "^0.19") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (d #t) (k 0)))) (h "0gr5m032il37vvsykp14jivkyyqx7x6pscqfav884d2i2sm26lb0")))

(define-public crate-rustls-split-0.2.1 (c (n "rustls-split") (v "0.2.1") (d (list (d (n "rustls") (r "^0.19") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (d #t) (k 0)))) (h "0dv8qd5kbsq5d9lrwcqafxag37jazvbp3qb2ld2qsk8b948kzmm9")))

(define-public crate-rustls-split-0.2.2 (c (n "rustls-split") (v "0.2.2") (d (list (d (n "rustls") (r "^0.19") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (d #t) (k 2)))) (h "19hsq4kq5cy8wic10cm8fqsh00p791h68g3w5dshbc7x5jspkc3z")))

(define-public crate-rustls-split-0.3.0 (c (n "rustls-split") (v "0.3.0") (d (list (d (n "rustls") (r "^0.20") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.0") (d #t) (k 2)) (d (n "webpki") (r "^0.21") (d #t) (k 2)))) (h "1ls71xcgs42zm9qx8mfvv891pjijh7rldxygg8h9ss5l2ab2r03q")))

