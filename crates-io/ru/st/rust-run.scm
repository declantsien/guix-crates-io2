(define-module (crates-io ru st rust-run) #:use-module (crates-io))

(define-public crate-rust-run-0.0.0 (c (n "rust-run") (v "0.0.0") (h "1vvp872pdb829qzvil195qbldf5c41jnspmis2068xxxw4301pfw")))

(define-public crate-rust-run-0.0.1 (c (n "rust-run") (v "0.0.1") (h "16r9rq0cbw0ip8rv8lr99mxmgx4iw5xhlqkgrr5130yzbzfa7q9w")))

