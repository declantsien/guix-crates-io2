(define-module (crates-io ru st rust_sfp) #:use-module (crates-io))

(define-public crate-rust_sfp-0.1.0 (c (n "rust_sfp") (v "0.1.0") (d (list (d (n "unisocket") (r "^0.1.0") (d #t) (k 0)))) (h "1wjvxnx7437lz6w53ljzplfxnflhxxj5y2mgsrd2zb417bvgmsrw")))

(define-public crate-rust_sfp-0.2.0 (c (n "rust_sfp") (v "0.2.0") (d (list (d (n "unisocket") (r "^0.1.0") (d #t) (k 0)))) (h "075nyfr9l2612kp0kn5820h76idwqr8syjwdzwl564v4chqc7jck")))

(define-public crate-rust_sfp-0.3.0 (c (n "rust_sfp") (v "0.3.0") (d (list (d (n "unisocket") (r "^0.1.0") (d #t) (k 0)))) (h "1f1alrw8w0wwwsl0dn4w0m2qp64d1lhcclv51snj3zwb9lq02gfa")))

(define-public crate-rust_sfp-0.4.0 (c (n "rust_sfp") (v "0.4.0") (d (list (d (n "unisocket") (r "^0.1.0") (d #t) (k 0)))) (h "0zrx0ph63xqdhwh0glhcj28bnjkb1wddbzxjrysj9sh0ivd5yr9w")))

(define-public crate-rust_sfp-0.5.0 (c (n "rust_sfp") (v "0.5.0") (d (list (d (n "unisocket") (r "^1.0.0") (d #t) (k 0)))) (h "1sg319fjfpiyfbnj6vyfcmzkvs2whx274gn33si6268k44ak4zp4")))

