(define-module (crates-io ru st rusty-ls) #:use-module (crates-io))

(define-public crate-rusty-ls-0.1.0 (c (n "rusty-ls") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rb64nqj0wizc4gariqd32fmzspw9p44xmi21dnnhia9yayvxmba")))

(define-public crate-rusty-ls-0.1.1 (c (n "rusty-ls") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1yjfkpkd66idbrqa8v7bdffijqs68jwqwyll9hd5n4mdpx612g6l")))

(define-public crate-rusty-ls-0.1.2 (c (n "rusty-ls") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02sxgm5lb8rk962crhfdyi3rl9i8sdid4bsf4mrxw37hhqcxbfg8")))

(define-public crate-rusty-ls-0.1.3 (c (n "rusty-ls") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17a8i61ydd9dsxk4qz6aq93y9lmcgp9y2j80q712www7wza44vnq")))

