(define-module (crates-io ru st rustup-find) #:use-module (crates-io))

(define-public crate-rustup-find-0.1.0 (c (n "rustup-find") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.1") (d #t) (k 0)))) (h "09vnp633pq87a5c42sy7fxhmm3qr4x6lc628lk8pg6d43gbk9fwv")))

(define-public crate-rustup-find-0.1.2 (c (n "rustup-find") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.2") (d #t) (k 0)))) (h "043y3hw060cb0x3iq1pxrz7xrryr0bpv0ap49931axf2djp7fdkr")))

