(define-module (crates-io ru st rust_examples) #:use-module (crates-io))

(define-public crate-rust_examples-0.1.0 (c (n "rust_examples") (v "0.1.0") (h "0byif2m7dwgdb3mjbl32f87sx9v7hv96nxfk1qq1lq53a3rbc1pg")))

(define-public crate-rust_examples-0.1.1 (c (n "rust_examples") (v "0.1.1") (h "0bpvamxag21b7a5zp0cvnx4rl9v60c1l0af7ix5ncnn3hpb8pkz2")))

(define-public crate-rust_examples-0.1.2 (c (n "rust_examples") (v "0.1.2") (h "0hqxfq906n22gfy1wigxsfvbhi1fsz0z3imbvlwfnm2r4mws0lbx")))

(define-public crate-rust_examples-0.1.3 (c (n "rust_examples") (v "0.1.3") (h "1x3zgkwc8vqz50fgz8rk93hzbazq7my459darw48dqilwp144m62")))

(define-public crate-rust_examples-0.1.4 (c (n "rust_examples") (v "0.1.4") (h "1pq5zar4cmv8w9x3czh7pa61sfmmbpfjhdrh0381fsw1dajkgqsr")))

(define-public crate-rust_examples-0.1.5 (c (n "rust_examples") (v "0.1.5") (h "0ld3dxx71gr0448mb4g1lxwys81fm6f7p5zq8m0g2h1c6afva1a5")))

(define-public crate-rust_examples-0.1.6 (c (n "rust_examples") (v "0.1.6") (h "0wni99g1a1jlyijq9qvwx5fq5bxm677jgvmnllgc7cy49gmisnr7")))

(define-public crate-rust_examples-0.1.7 (c (n "rust_examples") (v "0.1.7") (h "1rjxljqxwh17hnavlz16zrkxv00q7s34y84img14gkg60bxhixfl")))

(define-public crate-rust_examples-0.1.8 (c (n "rust_examples") (v "0.1.8") (h "08qp6043r3l49ixcay6vy339flp2r8ixdqma0sqriyncz1j8b7cf")))

(define-public crate-rust_examples-0.1.9 (c (n "rust_examples") (v "0.1.9") (h "149z4zw2lpsjg0gp21zvwy5lmal13g5682vrkbyk7h1mw0fk15rk")))

(define-public crate-rust_examples-0.1.10 (c (n "rust_examples") (v "0.1.10") (h "0nmzh6nayc3s5314g6gwmyrkv7abxqvqw5x6rp5lfx9qql6aw92n")))

(define-public crate-rust_examples-0.1.11 (c (n "rust_examples") (v "0.1.11") (h "1cbvnjq2rcx997l3m64paxsp1zw7v1nhb3g7fy607ygb3fd438ir")))

