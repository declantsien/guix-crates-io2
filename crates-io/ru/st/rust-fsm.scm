(define-module (crates-io ru st rust-fsm) #:use-module (crates-io))

(define-public crate-rust-fsm-0.2.0 (c (n "rust-fsm") (v "0.2.0") (d (list (d (n "rust-fsm-dsl") (r "^0.3.0") (d #t) (k 0)))) (h "0j6s21s2z649557w14wqx2d3n6pij0z22giv3c4w4gz80i27r8cr")))

(define-public crate-rust-fsm-0.3.0 (c (n "rust-fsm") (v "0.3.0") (d (list (d (n "rust-fsm-dsl") (r "^0.3.0") (d #t) (k 0)))) (h "1dpw2cmwq8x16hpb4gprpz0as383vd605gjlnsbgb4vxzxpfzff1")))

(define-public crate-rust-fsm-0.4.0 (c (n "rust-fsm") (v "0.4.0") (d (list (d (n "rust-fsm-dsl") (r "^0.4.0") (d #t) (k 0)))) (h "1awh06qbbchbgzn9g2qa2fs1w5a246gdkgwj7vkz7wd3n4abhy02")))

(define-public crate-rust-fsm-0.5.0 (c (n "rust-fsm") (v "0.5.0") (d (list (d (n "rust-fsm-dsl") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1lxy1xa838pb8871abk34lyjw16sy8hwzavg2dhp7m2xbxk2vpik") (f (quote (("std") ("dsl" "rust-fsm-dsl") ("default" "std" "dsl"))))))

(define-public crate-rust-fsm-0.6.0 (c (n "rust-fsm") (v "0.6.0") (d (list (d (n "rust-fsm-dsl") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0rcbcwyvcwmrx9day950dxmi8vm2awyc90rbfxrwjlqg39ry97iv") (f (quote (("std") ("dsl" "rust-fsm-dsl") ("default" "std" "dsl"))))))

(define-public crate-rust-fsm-0.6.1 (c (n "rust-fsm") (v "0.6.1") (d (list (d (n "rust-fsm-dsl") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "03c6da0y4p6n1c9kn5nkv0gjfzqgb9rb1yx24jnlagi52pkps782") (f (quote (("std") ("dsl" "rust-fsm-dsl") ("default" "std" "dsl"))))))

(define-public crate-rust-fsm-0.6.2 (c (n "rust-fsm") (v "0.6.2") (d (list (d (n "rust-fsm-dsl") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "0i210r20fq44mpf7vwx78xdnrn14ay18c0qz7vsydrl4jcggqva9") (f (quote (("std") ("dsl" "rust-fsm-dsl") ("default" "std" "dsl"))))))

