(define-module (crates-io ru st rust_io) #:use-module (crates-io))

(define-public crate-rust_io-0.1.0 (c (n "rust_io") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0124vql9071ghwaw37z6481ibx24dmrs162cq1k7rwbnij2fxa6b")))

(define-public crate-rust_io-0.2.0 (c (n "rust_io") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0alrx5s8d82xmr1dzy4l54j2biaf39x6h76z5g8yfd43y19w53ar")))

(define-public crate-rust_io-0.3.0 (c (n "rust_io") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17q5zywhw0niszpj7k9j990xaqhzxpvz7afv8wdzqlxf0jlkyxdq")))

(define-public crate-rust_io-0.4.0 (c (n "rust_io") (v "0.4.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v878hf5y8wsai6m5c2d5psxhx57kb565bdvlwh142lcm261visw")))

(define-public crate-rust_io-0.5.0 (c (n "rust_io") (v "0.5.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ffqncamay58dk6hah81fq7dnw0j3qq390vmmz4gpgrgkcwn67gw")))

(define-public crate-rust_io-0.6.0 (c (n "rust_io") (v "0.6.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10vl9y8km66l8h9pd6krz8p9g5bjvhykicw5d6k0d2k0bn9v47lr")))

