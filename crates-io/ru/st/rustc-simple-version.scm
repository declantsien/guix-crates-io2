(define-module (crates-io ru st rustc-simple-version) #:use-module (crates-io))

(define-public crate-rustc-simple-version-0.1.0 (c (n "rustc-simple-version") (v "0.1.0") (h "1vfns2sfm8j02pccwsgm0m4pnbp88cw0g2h6i4m4zx669ycl1hfj")))

(define-public crate-rustc-simple-version-0.1.1 (c (n "rustc-simple-version") (v "0.1.1") (h "1ipssvp2zilg3s3yzf9l7kqj800pfy51wbdp0wfm0b02lfq7i58g")))

