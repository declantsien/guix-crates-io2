(define-module (crates-io ru st rust-tools) #:use-module (crates-io))

(define-public crate-rust-tools-0.1.0 (c (n "rust-tools") (v "0.1.0") (h "093pfcx3ixir0v5620c3w12adl2vrxa7f7gm9sylpqx2lvm18kpa") (f (quote (("default"))))))

(define-public crate-rust-tools-0.2.0 (c (n "rust-tools") (v "0.2.0") (d (list (d (n "pico-args") (r "^0.3") (k 0)))) (h "00zvypq6vpd3c203jlbz85cmximlmzgv647w818vf7w12jmf6r6p") (f (quote (("default"))))))

(define-public crate-rust-tools-0.3.0 (c (n "rust-tools") (v "0.3.0") (h "0gx4y3dq8y2apnsw1zgf3fv1s18k917r44bh0rsk00k8dsv0052f") (f (quote (("default"))))))

(define-public crate-rust-tools-0.3.1 (c (n "rust-tools") (v "0.3.1") (h "1vvip2fjqmsfwzqswxkzyhwgpb2p7m3yh2l9gqlbrwshxm7q9dby") (f (quote (("default"))))))

(define-public crate-rust-tools-1.0.0 (c (n "rust-tools") (v "1.0.0") (h "0s85xs13h61fd3q5b233q2wb3qxjkiprv2413k6390v48vajyx6c") (f (quote (("default")))) (r "1.61")))

(define-public crate-rust-tools-1.0.1 (c (n "rust-tools") (v "1.0.1") (h "0cdmr5w6hqgbsng1jiwf337sigibllpwdyaaq1mz2amysmp4dqnp") (f (quote (("default")))) (r "1.61")))

(define-public crate-rust-tools-1.0.2 (c (n "rust-tools") (v "1.0.2") (h "154sdg8j9vjg18gvg0m0a3wrb9xygdcdc2fkpa9n5i4jc4lyc3jn") (f (quote (("default")))) (r "1.61")))

(define-public crate-rust-tools-1.0.3 (c (n "rust-tools") (v "1.0.3") (h "1i0av2lq0sid9agipg4clbvjvh7spi6jjvlxbipnznipsh5zyhff") (f (quote (("default")))) (r "1.65")))

