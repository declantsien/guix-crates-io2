(define-module (crates-io ru st rust-15-puzzle-cli) #:use-module (crates-io))

(define-public crate-rust-15-puzzle-cli-0.1.0 (c (n "rust-15-puzzle-cli") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (d #t) (k 0)))) (h "1zh8fpcmvgpb3mv86cdhm5ghasbxmjzgnj55x36nrr8j82cl439j")))

(define-public crate-rust-15-puzzle-cli-0.1.1 (c (n "rust-15-puzzle-cli") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (d #t) (k 0)))) (h "16q3fqyiwh3q6xsj87wi0gjayqmp4ms21lgfx4l7lll18ni212ll")))

(define-public crate-rust-15-puzzle-cli-0.2.0 (c (n "rust-15-puzzle-cli") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.9.1") (d #t) (k 0)))) (h "0dr0ljxby0pqfn1xqci2f50sc69dy5m77qxm16kqs53h63xca0lb")))

