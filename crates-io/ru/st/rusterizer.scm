(define-module (crates-io ru st rusterizer) #:use-module (crates-io))

(define-public crate-rusterizer-0.1.0 (c (n "rusterizer") (v "0.1.0") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.1") (d #t) (k 0)))) (h "1hgna4mi9y3jrbcd237m75xrgza2g9l7bzqzqqzp55fi0hdqsixi")))

(define-public crate-rusterizer-0.1.1 (c (n "rusterizer") (v "0.1.1") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.1") (d #t) (k 0)))) (h "0mq2kfi811wrjp6qrs3v1i973jczrjd111klxd7djzfq3dbmpn02")))

(define-public crate-rusterizer-0.1.2 (c (n "rusterizer") (v "0.1.2") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.1") (d #t) (k 0)))) (h "1d4ai9mman8qplcb6hm7nhvxy9p8pigs8c039njg8zkagzq97ihp")))

(define-public crate-rusterizer-0.1.3 (c (n "rusterizer") (v "0.1.3") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.1") (d #t) (k 0)))) (h "1dfwyhqzgjchcflifxg3mq0mz2rja4ymscr8hs8agpiyl5wkvmmh")))

(define-public crate-rusterizer-0.1.4 (c (n "rusterizer") (v "0.1.4") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.1") (d #t) (k 0)))) (h "068mg7x3g8h6li8hrl9qasb7di3sdk8j246w298zwp14kr6j7lvz")))

(define-public crate-rusterizer-0.1.5 (c (n "rusterizer") (v "0.1.5") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.1") (d #t) (k 0)))) (h "06gf4r69cs7gsrq4jd8f7wbdxrnh0m8yzin76cc9wmk00bdqz8gb")))

(define-public crate-rusterizer-0.1.6 (c (n "rusterizer") (v "0.1.6") (d (list (d (n "glium") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "tobj") (r "^0.1.1") (d #t) (k 0)))) (h "0h5riy7rw08lwwh8qq837994clxklkmmy6601fw3bjcxg4r00brx")))

