(define-module (crates-io ru st rust_demangle) #:use-module (crates-io))

(define-public crate-rust_demangle-0.1.0 (c (n "rust_demangle") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.13.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.18") (d #t) (k 0)))) (h "01invfvqv124ikqgdcwxr0hmxv0j8x9ibrpxib0p7p2cpj768xcb")))

