(define-module (crates-io ru st rustlings-fix) #:use-module (crates-io))

(define-public crate-rustlings-fix-0.1.0 (c (n "rustlings-fix") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "1zrl8y9lzs99pj4q80fwk7k2shc43ja5mr1gbgxy41z9774iwsgq")))

(define-public crate-rustlings-fix-0.1.1 (c (n "rustlings-fix") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "0r22c114hx9ig4yckcfgq68cccxfaxigbv7xk3x8gdavdnsj512n")))

(define-public crate-rustlings-fix-0.1.2 (c (n "rustlings-fix") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "1v7c8lqp0j3ii2p0a1w8nwqjms3hkgqyrc8683syvfv29qw8hsz3")))

