(define-module (crates-io ru st rust-domain) #:use-module (crates-io))

(define-public crate-rust-domain-0.0.1-alpha0 (c (n "rust-domain") (v "0.0.1-alpha0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "app_dirs2") (r "^2.3") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1k6sfx1fxn0n84v98xx6s6f3c46yvzghklib4139322sj1gc286y")))

