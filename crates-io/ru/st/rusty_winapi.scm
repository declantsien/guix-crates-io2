(define-module (crates-io ru st rusty_winapi) #:use-module (crates-io))

(define-public crate-rusty_winapi-0.1.1 (c (n "rusty_winapi") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("impl-default" "oleauto"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0218fg9p79wbdq3kpw97pzlz2zqzan9h1706skqrgjkqslq7zhng")))

