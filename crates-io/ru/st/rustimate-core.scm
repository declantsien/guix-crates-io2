(define-module (crates-io ru st rustimate-core) #:use-module (crates-io))

(define-public crate-rustimate-core-0.0.1 (c (n "rustimate-core") (v "0.0.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "built") (r "^0.3.2") (d #t) (k 1)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1lcnk92fy42vjhs7b40jcqnaim7b41kdzyqnwilmkj4m8dx92yfc")))

(define-public crate-rustimate-core-0.1.0 (c (n "rustimate-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "built") (r "^0.3.2") (d #t) (k 1)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0x7wnlir9cjf8l6k9dvgng993h9qx1r06cgs73xjqk3i4xqcjy3x")))

