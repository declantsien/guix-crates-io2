(define-module (crates-io ru st rust-fp-categories) #:use-module (crates-io))

(define-public crate-rust-fp-categories-0.0.1 (c (n "rust-fp-categories") (v "0.0.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "08zsz5xc6k2z2wdzasa9jammr3g2g0f95133zvhxkp0fl9hzv7gj")))

(define-public crate-rust-fp-categories-0.0.2 (c (n "rust-fp-categories") (v "0.0.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0fb1n7w6k0dalb2ipmmiaki89qq0visjddbdjw11r1f2nvpifkba")))

(define-public crate-rust-fp-categories-0.0.3 (c (n "rust-fp-categories") (v "0.0.3") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1b9zwa7p06k93drqgys8x96c790agdcx2p7yz4amc2wjn57byyk6")))

(define-public crate-rust-fp-categories-0.0.4 (c (n "rust-fp-categories") (v "0.0.4") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "0v5z589sgh7xvgb6qnn4w727qbiw6mi8d0rgb8h2pcj9dgpq1niz")))

(define-public crate-rust-fp-categories-0.0.5 (c (n "rust-fp-categories") (v "0.0.5") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "01gl9np62hv1a8lnhxb14x033vviqg0vznr576x9d9yf46i81vsi")))

