(define-module (crates-io ru st rust_gps) #:use-module (crates-io))

(define-public crate-rust_gps-1.1.2 (c (n "rust_gps") (v "1.1.2") (d (list (d (n "nmea") (r "^0.0.7") (d #t) (k 0)) (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0wwqasd04vbih6w0l5cw1gxbjnh8ar87z1pssvzxppwpyqdkiaa8")))

