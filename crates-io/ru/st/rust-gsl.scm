(define-module (crates-io ru st rust-gsl) #:use-module (crates-io))

(define-public crate-rust-GSL-0.4.9 (c (n "rust-GSL") (v "0.4.9") (h "18bzh7xzidmzz8vnrd1g9qfcj1w2mfrhkknrr17gfh98drl3ymnn")))

(define-public crate-rust-GSL-0.4.10 (c (n "rust-GSL") (v "0.4.10") (h "0f3640kfkd433lgaqayz7irxvdfn3lvhg73vr0f67v77qnr5hs85")))

