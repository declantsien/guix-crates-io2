(define-module (crates-io ru st rust_black_tree) #:use-module (crates-io))

(define-public crate-rust_black_tree-0.1.0 (c (n "rust_black_tree") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "07k1x0md15advnqfszdxk685dcq5y16nbpmgbih4vfvrvfrq4mka") (y #t)))

(define-public crate-rust_black_tree-0.1.1 (c (n "rust_black_tree") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "1ix93lqp0xns04nw0psxhi8v00bxdyqzi62mc6ygssg9pz54hins")))

(define-public crate-rust_black_tree-0.1.2 (c (n "rust_black_tree") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0ahl4rc5db13f5i54s5mbpyp5ggsvxh9dmsr5jhwwksh3x07mfgn")))

(define-public crate-rust_black_tree-0.1.3 (c (n "rust_black_tree") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0bhcnsmfk3l54k3jv3iild5n139pi5s2kal5vl8sfxpi50nlg2kl")))

(define-public crate-rust_black_tree-0.1.4 (c (n "rust_black_tree") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "17hnv8wyfrn28ka2z1hq99650jnlfp557g36i7d4cj767fcw704v")))

(define-public crate-rust_black_tree-0.1.5 (c (n "rust_black_tree") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "isatty") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0lc7ywhwdvi0hmqslckks3z44q1if0y8r51vs0w7y04icbd91v8r")))

