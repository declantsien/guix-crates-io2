(define-module (crates-io ru st rustgenpass) #:use-module (crates-io))

(define-public crate-rustgenpass-0.3.0 (c (n "rustgenpass") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "rpassword") (r "^7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1fq4k70plnardabcy78lx9kxplliis3ib1arv3bibcqg0ivid16d")))

(define-public crate-rustgenpass-0.4.0 (c (n "rustgenpass") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "rpassword") (r "^7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0jf7m9q9anf2yrpzgk16hwr4cl8ifr0fkyz381g7vbvizikj0g02")))

(define-public crate-rustgenpass-0.5.0 (c (n "rustgenpass") (v "0.5.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "18k7m53qrx0rgwjyarq910l43sy4cwz3wx50002mzk39fhb306jg")))

