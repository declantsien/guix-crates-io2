(define-module (crates-io ru st rustonomicon_optima) #:use-module (crates-io))

(define-public crate-rustonomicon_optima-0.1.0 (c (n "rustonomicon_optima") (v "0.1.0") (d (list (d (n "finitediff") (r "^0.1.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1kp2i4zqwnz0ralh8xmdb9yzirdbf9qsvrhqbmvmhx9mac1sg0p9")))

(define-public crate-rustonomicon_optima-0.1.1 (c (n "rustonomicon_optima") (v "0.1.1") (d (list (d (n "finitediff") (r "^0.1.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0dmmsi1mlcny68vghdlczz2vfj8kasvzx83mykrjc64z3c89bppc")))

(define-public crate-rustonomicon_optima-0.1.2 (c (n "rustonomicon_optima") (v "0.1.2") (d (list (d (n "finitediff") (r "^0.1.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0p0ph8lsd3jkdmv9jc2xyfnkn84hm41dj1mxnnabfrirly804jj6")))

