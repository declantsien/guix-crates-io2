(define-module (crates-io ru st rustpp) #:use-module (crates-io))

(define-public crate-rustpp-0.1.0 (c (n "rustpp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "029g1wz2l1bzm509x6ibw65xfp6xz3vnqzgwa87mimq9r2rxn63b") (y #t)))

(define-public crate-rustpp-0.1.1 (c (n "rustpp") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12biis9rib6n44pxbf9bf4nzn5dx0kaqxhyi9cdpqpvhga8jrz69") (y #t)))

(define-public crate-rustpp-0.1.2 (c (n "rustpp") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07jijfc9bwcilhn3433ilx240ggbcg85fm835ppgri55w9k5wzqz") (y #t)))

(define-public crate-rustpp-0.1.3 (c (n "rustpp") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w67s3smaggj1pn73q1ldf63mg72zwm6nmz993p0yrmink7sdh1a")))

(define-public crate-rustpp-0.1.4 (c (n "rustpp") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gl6jvdm6m5lq9by4nn3k6wnn95axynd6m2bgn9nzfbd2hj4w61y")))

