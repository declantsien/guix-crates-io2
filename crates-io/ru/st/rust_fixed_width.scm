(define-module (crates-io ru st rust_fixed_width) #:use-module (crates-io))

(define-public crate-rust_fixed_width-1.0.0 (c (n "rust_fixed_width") (v "1.0.0") (d (list (d (n "pad") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1dmyarwnsa51579i5c2x74glk2xb1lk77l3plfrljs5yl6gglr7b") (f (quote (("default" "all") ("all" "pad"))))))

(define-public crate-rust_fixed_width-1.0.1 (c (n "rust_fixed_width") (v "1.0.1") (d (list (d (n "pad") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "05f3pjwxlijq3kkp93hsxr5p7l1h2ravd7sb7ig5gcm86a6xhs73") (f (quote (("default" "all") ("all" "pad"))))))

