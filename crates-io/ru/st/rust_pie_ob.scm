(define-module (crates-io ru st rust_pie_ob) #:use-module (crates-io))

(define-public crate-rust_pie_ob-0.1.0 (c (n "rust_pie_ob") (v "0.1.0") (d (list (d (n "rust_ob") (r "^2.5.0") (d #t) (k 0)))) (h "047s4ggs024vv68clbz18fhaql5my6rwjh0l2hciv7sba1bm1lfw") (y #t)))

(define-public crate-rust_pie_ob-0.2.0 (c (n "rust_pie_ob") (v "0.2.0") (d (list (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.32.0") (d #t) (k 2)) (d (n "rust_ob") (r "^2.5.1") (d #t) (k 0)))) (h "1z5dz6q6pvm99gib5675gkrwjy7ygdajp3q4d9hznsw5m97fmh4r")))

(define-public crate-rust_pie_ob-0.3.0 (c (n "rust_pie_ob") (v "0.3.0") (d (list (d (n "rust_decimal") (r "^1.32.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.32.0") (d #t) (k 2)) (d (n "rust_ob") (r "^2.5.1") (d #t) (k 0)))) (h "08pgpfvzn0500yvp0rkaxmdscpb0nwg5hahh4w18jbaikdsbqs2p")))

