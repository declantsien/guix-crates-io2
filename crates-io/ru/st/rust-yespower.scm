(define-module (crates-io ru st rust-yespower) #:use-module (crates-io))

(define-public crate-rust-yespower-0.1.0 (c (n "rust-yespower") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bj0w0ihxyfr5c2mcm84h337rpyn87cwn9wd5hwwa8r87s3gx3di")))

