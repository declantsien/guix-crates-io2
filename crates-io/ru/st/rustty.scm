(define-module (crates-io ru st rustty) #:use-module (crates-io))

(define-public crate-rustty-0.1.0 (c (n "rustty") (v "0.1.0") (d (list (d (n "nix") (r "^0.3.6") (d #t) (k 0)))) (h "112bffyilm2gjn9v1rxxxk7kh93d0dhj8x0mrkbc34pa2ghbyff4")))

(define-public crate-rustty-0.1.1 (c (n "rustty") (v "0.1.1") (d (list (d (n "nix") (r "^0.3.6") (d #t) (k 0)))) (h "0y17iz3iizmz78l7mfair3gsjkqxcdql798mnn2d44i06z64z16s")))

(define-public crate-rustty-0.1.2 (c (n "rustty") (v "0.1.2") (d (list (d (n "nix") (r "^0.3.6") (d #t) (k 0)))) (h "12jjzkdcddjxzmn3ph29and8a53rx41za85z2izajhc30b6mrr1f")))

(define-public crate-rustty-0.1.3 (c (n "rustty") (v "0.1.3") (d (list (d (n "nix") (r "^0.3.8") (d #t) (k 0)))) (h "0jv6wm57g462p58nxmyb9fld7r18133d7xqgybf4n6x90zb2hb1h")))

(define-public crate-rustty-0.1.4 (c (n "rustty") (v "0.1.4") (d (list (d (n "nix") (r "^0.3.8") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "0xq5bvn3yrdgcrbw5sdnavkpvhg1khmqsffg68fnspvb1kqla5q3")))

(define-public crate-rustty-0.1.5 (c (n "rustty") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.1.12") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)) (d (n "term") (r "^0.2.11") (d #t) (k 0)))) (h "09x0gr097kpjijirmw52v3avdq5wnq3k5qwj83qrx3zmncfw22w2")))

(define-public crate-rustty-0.1.6 (c (n "rustty") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.1.12") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)) (d (n "term") (r "^0.2.11") (d #t) (k 0)))) (h "19x1cgh59bqb90dasf68c1rhx3a9kiqbmyavr9341mixszi902zf")))

(define-public crate-rustty-0.1.7 (c (n "rustty") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.1.12") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)) (d (n "term") (r "^0.2.11") (d #t) (k 0)))) (h "1zqgf19d3jim3dl1djp6bgwna205rfgvblb746a5rqv2pni6l8ph")))

(define-public crate-rustty-0.1.8 (c (n "rustty") (v "0.1.8") (d (list (d (n "lazy_static") (r "^0.1.12") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)) (d (n "term") (r "^0.2.11") (d #t) (k 0)))) (h "197zbqrzljjwlr7f7zd00hqb64qalsbzq3c08mcnq89vjvxfywma")))

(define-public crate-rustty-0.1.9 (c (n "rustty") (v "0.1.9") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "1gscyl2yhpy86ll9v4n68zcal9iph941ajd44r959w9pp4x19maj")))

(define-public crate-rustty-0.1.10 (c (n "rustty") (v "0.1.10") (d (list (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1jng10mf3dyw319165ymmahzbk50hi5yrl0ky52y1536x2calk0p")))

(define-public crate-rustty-0.1.11 (c (n "rustty") (v "0.1.11") (d (list (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0r3r8lvl8m1bs3nvp5fr54alk3r8y2z2chvwzr0baj1ji21dr250")))

(define-public crate-rustty-0.1.12 (c (n "rustty") (v "0.1.12") (d (list (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1c0ppncci3j0p33jk6nxl8xfj5bix3584in3ggqglrkm1ppdrxkr")))

