(define-module (crates-io ru st rust_math) #:use-module (crates-io))

(define-public crate-rust_math-0.1.0 (c (n "rust_math") (v "0.1.0") (h "1i5wlmd5rk45iydiqx5cmqi6fv0wqknbd70ra68qfcaygf6fjp42")))

(define-public crate-rust_math-0.1.1 (c (n "rust_math") (v "0.1.1") (h "0yrhl8idyr2qns7n59jr93yzwwig86r74c3nj9033b318hhb5qyp")))

(define-public crate-rust_math-0.1.2 (c (n "rust_math") (v "0.1.2") (h "11339akq70cp7k1ksrrfnsak4b5bkp946iam127gg7fk8zgnn4s3")))

(define-public crate-rust_math-0.2.2 (c (n "rust_math") (v "0.2.2") (h "0sx7azb4wvv8fllzn0x8gnzic7zv2mkxwxbxvkbkc10gar7x7gaq") (y #t)))

(define-public crate-rust_math-0.2.3 (c (n "rust_math") (v "0.2.3") (h "1daxav0mqpdrij0hpfxzv6i03fbnxij8mjywkh60im16dh32mfym")))

(define-public crate-rust_math-0.2.4 (c (n "rust_math") (v "0.2.4") (h "1x700jxw01kcza2ai90807vvpgqsizirvy2hfzhqydbbfglxd7c4")))

(define-public crate-rust_math-0.2.5 (c (n "rust_math") (v "0.2.5") (h "1y11l2qyvb2n8h8crh95xwya3sv697im2vi7xmjh5syiyf8biz56")))

(define-public crate-rust_math-0.2.6 (c (n "rust_math") (v "0.2.6") (h "0mpfn4yc384mm1cjiy9a64fmvx21jf7k4ab3dbqsjkcc5n0p85x3")))

(define-public crate-rust_math-0.3.6 (c (n "rust_math") (v "0.3.6") (h "0lx6pmvh17vzwqr6if37l2kpsfsij1pxg5f77csq8gkpd4gfz4js")))

(define-public crate-rust_math-0.2.7 (c (n "rust_math") (v "0.2.7") (h "0mvqwfm50314691rbzlxf1n4pvig4dy79dcr9s257rkhw0qwnrvc") (y #t)))

(define-public crate-rust_math-0.3.7 (c (n "rust_math") (v "0.3.7") (h "1zrl65s52pzap40ls0jlwzrfxmwwh33kirihjr7nlx2mmp4v132z")))

