(define-module (crates-io ru st rust-typed) #:use-module (crates-io))

(define-public crate-rust-typed-0.1.0 (c (n "rust-typed") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1y5i6rm31wknjld6x4b1imbi61f939ajwcha298rma7jp83g8j4f")))

(define-public crate-rust-typed-0.1.1 (c (n "rust-typed") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1yfmc9h1mhl8xyami5sbcvdga793d08svgsm3zkwj6lap1ifg7x7")))

