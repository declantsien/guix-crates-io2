(define-module (crates-io ru st rustmaton) #:use-module (crates-io))

(define-public crate-rustmaton-0.2.1 (c (n "rustmaton") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0zjcj7p8gc18lh9780a2favb4r39d4d5z4k13hvivh1s0spcq939")))

(define-public crate-rustmaton-0.2.2 (c (n "rustmaton") (v "0.2.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0nxfq6yz5hhp8rgyacwdf970ncqaz7xgjag4nlj85pxqizychkk3")))

(define-public crate-rustmaton-0.2.3 (c (n "rustmaton") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0whqc80hnyqqxk8y8n9my8sgs1fxz644jw0ynvpgkaxczzyxfawd")))

