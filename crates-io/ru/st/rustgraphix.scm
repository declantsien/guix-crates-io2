(define-module (crates-io ru st rustgraphix) #:use-module (crates-io))

(define-public crate-RustGraphix-0.1.0 (c (n "RustGraphix") (v "0.1.0") (h "0pym9jla3zqy1fc6hckn5iq42a2kqljy1jfhi7zgw60694mk7dyn")))

(define-public crate-RustGraphix-0.2.0 (c (n "RustGraphix") (v "0.2.0") (h "1wi4nvrk7gvwmdfaz2jfzmv485gf85apy3xbkd8j0p86lkq3ny1c")))

(define-public crate-RustGraphix-0.2.1 (c (n "RustGraphix") (v "0.2.1") (h "1bdh33xpd7rwf0vzp3y59dky9c2sv3l3fhms1ayqzr1paf2p9j2s")))

