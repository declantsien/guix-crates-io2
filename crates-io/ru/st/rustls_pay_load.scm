(define-module (crates-io ru st rustls_pay_load) #:use-module (crates-io))

(define-public crate-rustls_pay_load-0.1.0 (c (n "rustls_pay_load") (v "0.1.0") (d (list (d (n "rustls") (r "^0.20.8") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-rustls") (r "^0.23.4") (d #t) (k 2)))) (h "03jvv8rk4lzzv7fp1cfsmncvx29l0xs26vdb2d4hr7hcl00c2ana")))

(define-public crate-rustls_pay_load-0.1.2 (c (n "rustls_pay_load") (v "0.1.2") (d (list (d (n "rustls") (r "^0.20.8") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-rustls") (r "^0.23.4") (d #t) (k 2)))) (h "0jm199wbgcfqb06s122ay7yjwncjjg47v3yfr5r6c1xgyw603xj9")))

