(define-module (crates-io ru st rust_hello_test) #:use-module (crates-io))

(define-public crate-rust_hello_test-0.1.1 (c (n "rust_hello_test") (v "0.1.1") (h "0gnjg2yknwhwcyg72cwqyya9hr7fcyl8vh5f9j2fgdbqf8f36z40")))

(define-public crate-rust_hello_test-0.1.2 (c (n "rust_hello_test") (v "0.1.2") (h "12v1pjlay4wfs8vidwk08xagv691hvcc5nlxkzi805n88xly5z2a")))

(define-public crate-rust_hello_test-0.1.3 (c (n "rust_hello_test") (v "0.1.3") (h "0cvxjk80y2b15gkfrc02gqs52y8w5c8hfh5ixkw5r6hzdvcipa96")))

(define-public crate-rust_hello_test-0.1.4 (c (n "rust_hello_test") (v "0.1.4") (h "1m0kj55j536jv585sy1k0h95f01mk2yijm1qfm55pm1f0prn8a9x")))

(define-public crate-rust_hello_test-0.1.5 (c (n "rust_hello_test") (v "0.1.5") (h "10z5a0mnqrbsj2wjjp5i1syg600wzlvn01afsn09jwp6fgfsfx7b")))

(define-public crate-rust_hello_test-0.2.0 (c (n "rust_hello_test") (v "0.2.0") (h "1gg8r6vfcamry19ha2ppf60zyncbr1z4szd8qi61gfj4pvhmziia")))

(define-public crate-rust_hello_test-0.2.1 (c (n "rust_hello_test") (v "0.2.1") (h "1qpd5rg5vg8fa6g1m9z8yl078r0dp5csax57vzjlflxhad8ry2gf")))

