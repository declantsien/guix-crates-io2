(define-module (crates-io ru st rust_jsc) #:use-module (crates-io))

(define-public crate-rust_jsc-0.1.0 (c (n "rust_jsc") (v "0.1.0") (d (list (d (n "rust_jsc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.0") (f (quote ("patches"))) (d #t) (k 0)))) (h "0fsgn0s2bk19aambq7nj8zb8m2xm0j8d1a81794cpqh5zd2jghpy")))

(define-public crate-rust_jsc-0.1.2 (c (n "rust_jsc") (v "0.1.2") (d (list (d (n "rust_jsc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.2") (f (quote ("patches"))) (d #t) (k 0)))) (h "0z5d9y17j75zvclcwhg40g8kcgm2xx1ai777zcnwlqg10kpknq31")))

(define-public crate-rust_jsc-0.1.3 (c (n "rust_jsc") (v "0.1.3") (d (list (d (n "rust_jsc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.3") (f (quote ("patches"))) (d #t) (k 0)))) (h "03hwqy9nbmpys600411lgxpnz3x5w1hrlwf9ayh6xgxpq8vkwfv4")))

(define-public crate-rust_jsc-0.1.4 (c (n "rust_jsc") (v "0.1.4") (d (list (d (n "rust_jsc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.4") (f (quote ("patches"))) (d #t) (k 0)))) (h "0n689zkdqwihyxbbqp08f01r1c9gsi6mbr3q279lbmncrb5rf1fj")))

(define-public crate-rust_jsc-0.1.5 (c (n "rust_jsc") (v "0.1.5") (d (list (d (n "rust_jsc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.5") (f (quote ("patches"))) (d #t) (k 0)))) (h "0fhd18gr42ny1vvg53cigc013mi27aijphcha6a9x706yc9xipc8")))

(define-public crate-rust_jsc-0.1.6 (c (n "rust_jsc") (v "0.1.6") (d (list (d (n "rust_jsc_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.5") (f (quote ("patches"))) (d #t) (k 0)))) (h "08idil51cx3xc3n39ibcah9qbsgwwpijlv9cvp36mzw6lw6kvxsw")))

(define-public crate-rust_jsc-0.1.7 (c (n "rust_jsc") (v "0.1.7") (d (list (d (n "rust_jsc_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.5") (f (quote ("patches"))) (d #t) (k 0)))) (h "008hccwibrasvq2v6ys1i2c208sk623pbi9nkvs53ry3dd19c266")))

(define-public crate-rust_jsc-0.1.8 (c (n "rust_jsc") (v "0.1.8") (d (list (d (n "rust_jsc_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.6") (f (quote ("patches"))) (d #t) (k 0)))) (h "1fwqipac2wilykd3zmgs5871r8acnxz76yfxdwg7x7idaxpxn7l0")))

(define-public crate-rust_jsc-0.1.9 (c (n "rust_jsc") (v "0.1.9") (d (list (d (n "rust_jsc_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.6") (f (quote ("patches"))) (d #t) (k 0)))) (h "17kyiypn0w7z9dx0mh78fd58hx6idvwxs0ykxcjgdp8lv32910bn")))

(define-public crate-rust_jsc-0.1.10 (c (n "rust_jsc") (v "0.1.10") (d (list (d (n "rust_jsc_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "rust_jsc_sys") (r "^0.1.6") (f (quote ("patches"))) (d #t) (k 0)))) (h "1fmj7i6wqpy77hlk7qfvb1fj6q5x7wc2vcz8bqywqbwgqb95da1s")))

