(define-module (crates-io ru st rustogram) #:use-module (crates-io))

(define-public crate-rustogram-0.1.0 (c (n "rustogram") (v "0.1.0") (h "0w4fni2b9iimzxgifd879r2jzn9g0kampl5k6widsj5nm5dq7mmx")))

(define-public crate-rustogram-0.1.1 (c (n "rustogram") (v "0.1.1") (h "0xwaz43bplafwyv7x9sr1ip8yhbvh2322sin8pz0b7567fbf8vas")))

(define-public crate-rustogram-0.1.2 (c (n "rustogram") (v "0.1.2") (h "010201mq47wqnxal8qg54m14sc9rx4v65hg09c9crqx29jwq68ik")))

(define-public crate-rustogram-0.1.3 (c (n "rustogram") (v "0.1.3") (h "1ywbkb6y2my60r1qj9857fcjjlf2hrh3vskc7h21sspsqm69i6bn")))

(define-public crate-rustogram-0.1.4 (c (n "rustogram") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ii10xll11qclkvcm1x77ksdn1r9jvin5i6aic2kvgn9wfnahn83")))

