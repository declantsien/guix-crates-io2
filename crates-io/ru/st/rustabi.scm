(define-module (crates-io ru st rustabi) #:use-module (crates-io))

(define-public crate-rustabi-6.1.0 (c (n "rustabi") (v "6.1.0") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "hex-literal") (r "^0.1.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "s-types") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "0hbk4wcjhshgbm7zirjy1xqacjqwxzqbrcnlvk5msca2j6a7azwb") (f (quote (("backtrace" "error-chain/backtrace"))))))

