(define-module (crates-io ru st rustasm6502) #:use-module (crates-io))

(define-public crate-rustasm6502-0.1.0 (c (n "rustasm6502") (v "0.1.0") (h "10m8wb4rq8kpsp1699vjx5c25dl2dwdp9mynw42igb78hzspf8n4")))

(define-public crate-rustasm6502-0.1.1 (c (n "rustasm6502") (v "0.1.1") (h "0i2wbxqkx46j1fdh989mvz00kjwf47pjypppw1j9wsdnaymd3wbp")))

(define-public crate-rustasm6502-0.1.2 (c (n "rustasm6502") (v "0.1.2") (h "0plg5pq43yzia9mvxvi3k36cys0vhb5spjzljrk6xq65j0252j3p")))

(define-public crate-rustasm6502-0.1.3 (c (n "rustasm6502") (v "0.1.3") (h "06jxyybix7la1h2fd6swfqpynn5hcspc6yi594zjzflqvgvcc5cq")))

(define-public crate-rustasm6502-0.1.4 (c (n "rustasm6502") (v "0.1.4") (h "12sps0ah2cxh3dhs2jr4msshnzfm8937kzd83j4iigknjwjibmlm")))

