(define-module (crates-io ru st rust-hsluv) #:use-module (crates-io))

(define-public crate-rust-hsluv-0.1.1 (c (n "rust-hsluv") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kdi6gcf3k0ylcz25slggwqbly372dww448z76l2w1w9xzbl3ry2")))

(define-public crate-rust-hsluv-0.1.2 (c (n "rust-hsluv") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kszm0vxiffwx0687nfl5z09lllp8bsj95h7a28l44jrm8n8y5y2")))

(define-public crate-rust-hsluv-0.1.3 (c (n "rust-hsluv") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1m1sfj2bbz35sjn8i1ssg8xlj9rh8wkj1fz1qwxx9vr6yy4bznxj")))

(define-public crate-rust-hsluv-0.1.4 (c (n "rust-hsluv") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12rp3v3vaya8fcwclfc5kly61pj6lsr80vs4b9sxikc54d7kgqpg")))

