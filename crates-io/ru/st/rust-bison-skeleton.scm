(define-module (crates-io ru st rust-bison-skeleton) #:use-module (crates-io))

(define-public crate-rust-bison-skeleton-0.1.0 (c (n "rust-bison-skeleton") (v "0.1.0") (h "0shqsg40msdshlvd60ynl21zr5hmkci9d2kniddvvh1ynf27h92l")))

(define-public crate-rust-bison-skeleton-0.2.0 (c (n "rust-bison-skeleton") (v "0.2.0") (h "1zg2ay6j3qa6sl8xlqwi7kzrpzfpsq8x7b57l8fgd9padsr0ksi9")))

(define-public crate-rust-bison-skeleton-0.3.0 (c (n "rust-bison-skeleton") (v "0.3.0") (h "081v4339zdnn7mqgc9yqqsmd0g4ydzwwaa22s6yk4aspvfypr9as")))

(define-public crate-rust-bison-skeleton-0.4.0 (c (n "rust-bison-skeleton") (v "0.4.0") (h "13j62aj73m3q5yy7vs6w516d57cbc0d687zxfvsjcpv2qfxs57mr")))

(define-public crate-rust-bison-skeleton-0.5.0 (c (n "rust-bison-skeleton") (v "0.5.0") (h "18naqc8jrq7nn5mqvy84r1g24g81vfnap7f6fily49bnlly9jqmk")))

(define-public crate-rust-bison-skeleton-0.6.0 (c (n "rust-bison-skeleton") (v "0.6.0") (h "1ynqmwjrvwhrg4zd5gcjy575dhlic3gp07nl1rh9cxrl7vg4jv1m")))

(define-public crate-rust-bison-skeleton-0.7.0 (c (n "rust-bison-skeleton") (v "0.7.0") (h "0vlkqvvw57k63bkizncv27jclzpacfqlknizcg7li5rz72v81dsc")))

(define-public crate-rust-bison-skeleton-0.8.0 (c (n "rust-bison-skeleton") (v "0.8.0") (h "0lzinvljnz9w4wx9irq4xzkgp78c1zp7ywkvp3lrzw4ifkb7i0ij")))

(define-public crate-rust-bison-skeleton-0.9.0 (c (n "rust-bison-skeleton") (v "0.9.0") (h "0hds6ng9qyq43agdzf7d60xkj6y8jywdgkkys6z59hs4iaag5m7g")))

(define-public crate-rust-bison-skeleton-0.10.0 (c (n "rust-bison-skeleton") (v "0.10.0") (h "1g22anrxhabspyf6k4gziarjhl0fbr85nl27p2592n4mn70xbqwj")))

(define-public crate-rust-bison-skeleton-0.11.0 (c (n "rust-bison-skeleton") (v "0.11.0") (h "1mlnf48yxywcvhbl5v76l52rxi24d2vmb0is186vi0yx1d5vvfyh")))

(define-public crate-rust-bison-skeleton-0.12.0 (c (n "rust-bison-skeleton") (v "0.12.0") (h "1hli7va8yibph9nna21k0wyi6sxyz59bk7yxhi28c4xcs381rxf1")))

(define-public crate-rust-bison-skeleton-0.13.0 (c (n "rust-bison-skeleton") (v "0.13.0") (h "04sj8hcvkpl57jxh1ns6snlc1jwhv96h44p3sq0ihy01iy792ja9")))

(define-public crate-rust-bison-skeleton-0.14.0 (c (n "rust-bison-skeleton") (v "0.14.0") (h "1c6sdx0nn47hzcj3q9p8rzrlcj5z8hy5937202rch7561cd23n05")))

(define-public crate-rust-bison-skeleton-0.15.0 (c (n "rust-bison-skeleton") (v "0.15.0") (h "0awsk7hsy3jzy2zspn2fdw7456hswkr1zk9alnpjs8d94apqr32a")))

(define-public crate-rust-bison-skeleton-0.16.0 (c (n "rust-bison-skeleton") (v "0.16.0") (h "1djh407rsj5f7vkirwv3qgbq0r558qvyilmarz3n8svi7hq4naax")))

(define-public crate-rust-bison-skeleton-0.17.0 (c (n "rust-bison-skeleton") (v "0.17.0") (h "02bb3syjvz0pfr1rrsc801cb8zdaj4i40yrngfkghx9x2g9sg9n3")))

(define-public crate-rust-bison-skeleton-0.18.0 (c (n "rust-bison-skeleton") (v "0.18.0") (h "0hia2bzhavh2dkflpnqsf9k8bkhyb7y4lnc6v5g04am2mil1njiz")))

(define-public crate-rust-bison-skeleton-0.19.0 (c (n "rust-bison-skeleton") (v "0.19.0") (h "02klalw5kaqxdpqkd7yp8nfskw0grs2gqq2hxnnh2q0gn133p3hn")))

(define-public crate-rust-bison-skeleton-0.20.0 (c (n "rust-bison-skeleton") (v "0.20.0") (h "0kcccrbf4lkp4lbg9mb1xrd58v1x539nsdlm5sm2n25kpxfddg2g")))

(define-public crate-rust-bison-skeleton-0.21.0 (c (n "rust-bison-skeleton") (v "0.21.0") (h "00z3aiz16ksixf8nmbf4xkmfxp6c31v0a1k8pfj25w1q1jb64gjw")))

(define-public crate-rust-bison-skeleton-0.22.0 (c (n "rust-bison-skeleton") (v "0.22.0") (h "0qm12fpkcss9fbyvf50pcjgna18lzwb9sx8zbjvxqgm3yl9caywg")))

(define-public crate-rust-bison-skeleton-0.23.0 (c (n "rust-bison-skeleton") (v "0.23.0") (h "1bczbr11nx4jznm6sa3s24qzdvy2bxvx6vhdgxqv3i22wacyrxh4")))

(define-public crate-rust-bison-skeleton-0.24.0 (c (n "rust-bison-skeleton") (v "0.24.0") (h "0rzkdw49wpmlf21hm82crm3b01aqb67h65ycqji63ncfsrk5kc5s")))

(define-public crate-rust-bison-skeleton-0.25.0 (c (n "rust-bison-skeleton") (v "0.25.0") (h "1gagrv57s96lcg4y93i5g7snhca2sy0kx0byr25zvl6gkpbnh944")))

(define-public crate-rust-bison-skeleton-0.26.0 (c (n "rust-bison-skeleton") (v "0.26.0") (h "1l2qqbl6fd64igw0rzcxfmld7s8z5g98plbiy1ag3w7879ddzs9c")))

(define-public crate-rust-bison-skeleton-0.27.0 (c (n "rust-bison-skeleton") (v "0.27.0") (h "14ybps0yfzb1wng31z6dgmfzv4wcha8hg0dzbkr0dckx2n6y7gba")))

(define-public crate-rust-bison-skeleton-0.28.0 (c (n "rust-bison-skeleton") (v "0.28.0") (h "1kkrl2fni7xq0fg8f3mg7j509hwhv7pfk5rx5vpql4xm1l86gczw")))

(define-public crate-rust-bison-skeleton-0.29.0 (c (n "rust-bison-skeleton") (v "0.29.0") (h "18c5sgigxw1jj8k3jsk8c5wbbk2p1hffp1vif3dzwq1kfbm3qdkl")))

(define-public crate-rust-bison-skeleton-0.30.0 (c (n "rust-bison-skeleton") (v "0.30.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "08x1i8jhbx7vfw2f76a9j51kxygal1sga4d6vl9c9hmj8j3hkf3d") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.31.0 (c (n "rust-bison-skeleton") (v "0.31.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "0g9321r4x5v7i716hhkcwcb1jbs552qjzgy5s9bhn8js39j963ly") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.32.0 (c (n "rust-bison-skeleton") (v "0.32.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "0shmdc93x4vnhxj20rkiglqv94r4xm5fqw3927i9nhzylfrxx7hh") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.33.0 (c (n "rust-bison-skeleton") (v "0.33.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "1rfks8nxyzc2sbrlc7fl2fixqmwimkgflvn384nrm3k235wj3zyg") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.34.0 (c (n "rust-bison-skeleton") (v "0.34.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "04scdpj4l4vj3ls73qcf2ybxpr41yvla83m91hm2n2di0km5klzw") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.35.0 (c (n "rust-bison-skeleton") (v "0.35.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "0rnyiyr7vxaf3pbavk5hl07zs1c3ai5mbaldk1gpb9c38wnrqgsh") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.36.0 (c (n "rust-bison-skeleton") (v "0.36.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "1dfghcncr8s14fqxdla8i2a2zym91z6wlvl70qsa1dj7pl02bpqs") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.37.0 (c (n "rust-bison-skeleton") (v "0.37.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "0s22j25kqzgk2xak8i7fplvffjdbfl0aqnh7swvq06kn6ckal8dq") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.38.0 (c (n "rust-bison-skeleton") (v "0.38.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.4") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "1kysgzhavk41vh6m7si3bs00z5in3l3hv67xzsrlv2j0v675vzaf") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.39.0 (c (n "rust-bison-skeleton") (v "0.39.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.7") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "0isr425ansx48ff539haarfza4nkrg3iaaj2wcfs349h010bsqgr") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.40.0 (c (n "rust-bison-skeleton") (v "0.40.0") (d (list (d (n "jemallocator") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "pprof") (r "^0.7") (f (quote ("flamegraph"))) (o #t) (d #t) (k 0)))) (h "0rxxqfi28wz8lnsg3qys26s51ikhrz4xhhh9bx4nw164x61m6yiz") (f (quote (("examples" "jemallocator" "pprof") ("dummy-parser") ("default"))))))

(define-public crate-rust-bison-skeleton-0.41.0 (c (n "rust-bison-skeleton") (v "0.41.0") (h "12mrgczqxgg73kzj9mp5yck3dc9z86qa0qdx5virc0fvqpzcpsj4")))

