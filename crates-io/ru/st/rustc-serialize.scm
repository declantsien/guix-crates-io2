(define-module (crates-io ru st rustc-serialize) #:use-module (crates-io))

(define-public crate-rustc-serialize-0.1.0 (c (n "rustc-serialize") (v "0.1.0") (h "040bxgxlxlxh4j2028mlf91wj1n8nk5hbb84lrmk4ip7q7r0xfs3")))

(define-public crate-rustc-serialize-0.1.1 (c (n "rustc-serialize") (v "0.1.1") (h "1k12n5jzgl4gbk61sd96hxh45y0jz5aja331w0szddnbnh8ydp70")))

(define-public crate-rustc-serialize-0.1.2 (c (n "rustc-serialize") (v "0.1.2") (h "14k7zadr94s89lyl7zrca2blvrql220h02kkaiqkw1v17k9aahpn")))

(define-public crate-rustc-serialize-0.1.3 (c (n "rustc-serialize") (v "0.1.3") (h "0qqczf06692f5rv8m4lkl7yr6m8xb1zmzv61aii5c5dnwzzbc3bh")))

(define-public crate-rustc-serialize-0.1.4 (c (n "rustc-serialize") (v "0.1.4") (h "0rrbzz1j3iap4znz4wj74j5z63zy8617zn9qbv12h02735jadymh")))

(define-public crate-rustc-serialize-0.1.5 (c (n "rustc-serialize") (v "0.1.5") (h "17gxanapxvkl8i9y8xikhaia7y8zqwkasgf18qvh77158k9xvqd9")))

(define-public crate-rustc-serialize-0.1.6 (c (n "rustc-serialize") (v "0.1.6") (h "18i14gxhz6j138q36w29s2vjy0ppvjx5ia2bxsmik2jyfp3dm1cw") (y #t)))

(define-public crate-rustc-serialize-0.2.0 (c (n "rustc-serialize") (v "0.2.0") (h "0hhchmq455y2vchsfw4bzsz0kl11b7npsbpix2sijp109c00jppa")))

(define-public crate-rustc-serialize-0.2.1 (c (n "rustc-serialize") (v "0.2.1") (h "1hb0rb2brp1s1wsi51469qyw6lvr2hpj09xfv2n2wiza6h52n6qf")))

(define-public crate-rustc-serialize-0.2.2 (c (n "rustc-serialize") (v "0.2.2") (h "1ymyw5cga6ymvsqwfaaa02fjmpcxy4hws22ij38nvnz0iv0hipbx")))

(define-public crate-rustc-serialize-0.2.3 (c (n "rustc-serialize") (v "0.2.3") (h "07w1gd1kp7kpiscy01nlhzf0x6ksd34yl5nryywimx1b2y77w9q9")))

(define-public crate-rustc-serialize-0.2.4 (c (n "rustc-serialize") (v "0.2.4") (h "0v1cdbfdg52flgy20an80djlsakgz6wkzvzgi0194xdak415p33y")))

(define-public crate-rustc-serialize-0.2.5 (c (n "rustc-serialize") (v "0.2.5") (h "18rk5ba1jpg5c0p2s4ai5zlcyfkl4y6yzgpg7pzsdbl5d734pz0i")))

(define-public crate-rustc-serialize-0.2.6 (c (n "rustc-serialize") (v "0.2.6") (h "0lvvm355gaa4xgkphkzbq6g177g9pwz0il1990r0hfcy2jm5cxsn")))

(define-public crate-rustc-serialize-0.2.7 (c (n "rustc-serialize") (v "0.2.7") (h "0226w9bwcn804ynr5hycw91cndycy3v92dd1mkgklj6djviibcrc")))

(define-public crate-rustc-serialize-0.2.8 (c (n "rustc-serialize") (v "0.2.8") (h "0j6x4347x273h93chj68gz57883qidl96nd97bgj8cjvnwwdlsnn")))

(define-public crate-rustc-serialize-0.2.9 (c (n "rustc-serialize") (v "0.2.9") (h "0rj03rfixncklqracsd0yhihkslhp3kvwzh8wvbjhc254242b2px")))

(define-public crate-rustc-serialize-0.2.10 (c (n "rustc-serialize") (v "0.2.10") (h "19flfqdxpcbqlyjqd7lfdjvd2q1y83a1agb1wn57mmdgk8v5lnsx")))

(define-public crate-rustc-serialize-0.2.11 (c (n "rustc-serialize") (v "0.2.11") (h "0i8al7hl0jhjckb4lbm3hx0aljpd704rg082skjcwxhwj80xw8qi")))

(define-public crate-rustc-serialize-0.2.12 (c (n "rustc-serialize") (v "0.2.12") (h "1d098abh3sww6b3hmb6mbx5bi3z4czq2hmvpyzfgvd8wx5kvmvmc")))

(define-public crate-rustc-serialize-0.2.13 (c (n "rustc-serialize") (v "0.2.13") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "155kgl9csv73fjy77x4z0ry3lav5ck3cqj5yhf035522hwqdg5v7")))

(define-public crate-rustc-serialize-0.2.14 (c (n "rustc-serialize") (v "0.2.14") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "1d8l5h9j3m4qgs2282srhiazvanba4kx97h476xv2fwmxqg2kn1w")))

(define-public crate-rustc-serialize-0.2.15 (c (n "rustc-serialize") (v "0.2.15") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "1lgvn00rxi46djv8p9s58mlgz8lw48c328v088h6mh8vaygry9la")))

(define-public crate-rustc-serialize-0.3.0 (c (n "rustc-serialize") (v "0.3.0") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "18s6r0cns15gxnwki0mhha860zvzisg3s8l5rapjkb839h7l4cqb")))

(define-public crate-rustc-serialize-0.3.1 (c (n "rustc-serialize") (v "0.3.1") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "1vz6zjdlvwnmcc0555ij1b8j39i7qgsq37574zrw5c276fgdll2a")))

(define-public crate-rustc-serialize-0.3.2 (c (n "rustc-serialize") (v "0.3.2") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "05n14ghwlp5k0c1n505877sp858bz4la6d4wyfsad3myn4scibsk")))

(define-public crate-rustc-serialize-0.3.3 (c (n "rustc-serialize") (v "0.3.3") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "1pcvkjghhrz1qnk1hmlqd11mgri0zy97pfhcnwjcmp499k46w160")))

(define-public crate-rustc-serialize-0.3.4 (c (n "rustc-serialize") (v "0.3.4") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "10l5clw2fh01mdq7j1zlz1wmhxh55h4hwq10hs83jpv0grg0549l")))

(define-public crate-rustc-serialize-0.3.5 (c (n "rustc-serialize") (v "0.3.5") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "11l96hdgzi4qhfsx59g442c106djdpp8q1mgxsci5b5337vq6cib")))

(define-public crate-rustc-serialize-0.3.6 (c (n "rustc-serialize") (v "0.3.6") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "0071v1bi3f4i43c9fjb1r4n5gw3hpim7ax2hanmgrjgy30lw8cbd")))

(define-public crate-rustc-serialize-0.3.7 (c (n "rustc-serialize") (v "0.3.7") (d (list (d (n "rand") (r "^0.2") (d #t) (k 2)))) (h "07n0ayybwcafg1nia51whcrf912xcgy37v2w5rd1wh5p0gw93xkc")))

(define-public crate-rustc-serialize-0.3.9 (c (n "rustc-serialize") (v "0.3.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0c224qxqka3l5hyg2ixb70qjg1yi98j9isi7vsjnjpbr5fh3lsqr")))

(define-public crate-rustc-serialize-0.3.10 (c (n "rustc-serialize") (v "0.3.10") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rcrqwfgjdi92g7f7xkmm8fy74i8gj3yl01zf92lmlzyqn884cn5")))

(define-public crate-rustc-serialize-0.3.11 (c (n "rustc-serialize") (v "0.3.11") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0281avr6lv3bfb6sa2k4kw2isc0gqb50pp3z5nmshiyh12dd9rfl")))

(define-public crate-rustc-serialize-0.3.12 (c (n "rustc-serialize") (v "0.3.12") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "18gs2knyd6nnn8d2pgz69dj5dxichqbxbanl05cnxn0ndw1m2p49")))

(define-public crate-rustc-serialize-0.3.13 (c (n "rustc-serialize") (v "0.3.13") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1fbgi64ipg7rq757y8n8i1f8d4k656k52jjy9p4n9bsiarmq6fdp")))

(define-public crate-rustc-serialize-0.3.14 (c (n "rustc-serialize") (v "0.3.14") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1s5vnccx821i65ybhh8g74hi02qlwqx72wchy7cr6ii3qbjylirv")))

(define-public crate-rustc-serialize-0.3.15 (c (n "rustc-serialize") (v "0.3.15") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1wi58lihg4zfr5a3pifj8vivzph0612574xw5692yw7b0z6dk8ds")))

(define-public crate-rustc-serialize-0.3.16 (c (n "rustc-serialize") (v "0.3.16") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0mj490xa67j5z9km2k59fzc4f1cxp6nwr6syi037vr6acim58j0s")))

(define-public crate-rustc-serialize-0.3.17 (c (n "rustc-serialize") (v "0.3.17") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "018abcrw559xabc94i4li1cy7irfrn3jmqjk7zcq2n1x7vvi4zjl")))

(define-public crate-rustc-serialize-0.3.18 (c (n "rustc-serialize") (v "0.3.18") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1w3zibf94c2qhbd624hqak3ibgmrpll179y0b2f8m7sprlc1by4w")))

(define-public crate-rustc-serialize-0.3.19 (c (n "rustc-serialize") (v "0.3.19") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "06v9c6mlp4g73mmyf4nfa7ld6izmd3ywisdg0vbipj2rwpkf8nb1")))

(define-public crate-rustc-serialize-0.3.20 (c (n "rustc-serialize") (v "0.3.20") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0jbpd4ib3wj4xngi46z9pqq2b7g1xbck3w38j36kipngbx3fa48c")))

(define-public crate-rustc-serialize-0.3.21 (c (n "rustc-serialize") (v "0.3.21") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "064qmyr2508qf78dwcpiv25rfjp9h9vd0wrj4mmwgppjg4fgrydz")))

(define-public crate-rustc-serialize-0.3.22 (c (n "rustc-serialize") (v "0.3.22") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0ywmsdk4l4q68hkw37743cf8kl7dp71p6317h14v82zji734cx93")))

(define-public crate-rustc-serialize-0.3.23 (c (n "rustc-serialize") (v "0.3.23") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1c6zw8cwqsba91dkgcfbimmisdn42imkny7ar4056inn6s2f8k38")))

(define-public crate-rustc-serialize-0.3.24 (c (n "rustc-serialize") (v "0.3.24") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1nkg3vasg7nk80ffkazizgiyv3hb1l9g3d8h17cajbkx538jiwfw")))

(define-public crate-rustc-serialize-0.3.25 (c (n "rustc-serialize") (v "0.3.25") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "00c494bsxjqjvc15h9x2nkgwl6bjdp9bmb9v0xs4ckv0h33lp0zy")))

