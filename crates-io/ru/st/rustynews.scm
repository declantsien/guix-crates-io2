(define-module (crates-io ru st rustynews) #:use-module (crates-io))

(define-public crate-rustynews-0.1.0 (c (n "rustynews") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.17") (d #t) (k 0)))) (h "1bflfv0p5z34c3bxfcyiidxsyb74905vyg40pkiqrwafz39bjax0")))

(define-public crate-rustynews-0.1.1 (c (n "rustynews") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.17") (d #t) (k 0)))) (h "0xcr83midpbc3if1593jc0fa4fwra0qfw8i7ij04z4agklwmnaib")))

