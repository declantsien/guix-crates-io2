(define-module (crates-io ru st rust_wek) #:use-module (crates-io))

(define-public crate-rust_wek-0.1.0 (c (n "rust_wek") (v "0.1.0") (d (list (d (n "rust_keien") (r "^0.1.0") (d #t) (k 0)) (d (n "sort_rust") (r "^0.2.0") (d #t) (k 0)))) (h "12ik7c37nsidw90svm6cqy6bghqk0kc9rngj5frj9kwa22c80s0g")))

(define-public crate-rust_wek-0.2.0 (c (n "rust_wek") (v "0.2.0") (d (list (d (n "rust_keien") (r "^0.1.0") (d #t) (k 0)) (d (n "sort_rust") (r "^0.2.0") (d #t) (k 0)))) (h "1li1834bflr5yrsnd9d9fd6xnk9agp8i11y98lhfcvmkxih90pw0")))

