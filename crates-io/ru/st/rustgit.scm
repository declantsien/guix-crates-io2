(define-module (crates-io ru st rustgit) #:use-module (crates-io))

(define-public crate-rustgit-1.0.0 (c (n "rustgit") (v "1.0.0") (d (list (d (n "coolssh") (r "^1.1.0") (d #t) (k 0)) (d (n "lmfu") (r "^1.0.3") (f (quote ("litemap" "hashmap" "arcstr"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "106spdc4af2nvc3gzilxk0pfmrcb4ag90by6pq4jnyfdzrnwb413")))

(define-public crate-rustgit-1.0.1 (c (n "rustgit") (v "1.0.1") (d (list (d (n "coolssh") (r "^1.1.0") (d #t) (k 0)) (d (n "lmfu") (r "^1.0.3") (f (quote ("litemap" "hashmap" "arcstr"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "0ml9cxg0gwrz5nvsdlnicwfvfcmv83g162sk8mqpw64kzizb427d")))

(define-public crate-rustgit-1.1.0 (c (n "rustgit") (v "1.1.0") (d (list (d (n "coolssh") (r "^1.2.0") (d #t) (k 0)) (d (n "lmfu") (r "^1.3.0") (f (quote ("litemap" "hashmap" "arcstr" "json"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "1rq3hakah0z0lds82ysvhr2x3wf0x4phqjpya92700sqi84fjvpk")))

(define-public crate-rustgit-1.1.1 (c (n "rustgit") (v "1.1.1") (d (list (d (n "coolssh") (r "^1.2.0") (d #t) (k 0)) (d (n "lmfu") (r "^1.3.1") (f (quote ("litemap" "hashmap" "arcstr" "json"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "1c08hdz9pf1jd840ba4z922j92959j0y582fxff7rs3rxpg7ban3")))

