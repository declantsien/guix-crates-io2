(define-module (crates-io ru st rustic_hal) #:use-module (crates-io))

(define-public crate-rustic_hal-0.1.0 (c (n "rustic_hal") (v "0.1.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1dn2pm0j5ci7k1zm41fmis9glw3pwsnxddsy4h378qskmm4zdrvw") (f (quote (("unstable" "serde_derive") ("default" "serde_codegen"))))))

(define-public crate-rustic_hal-0.1.1 (c (n "rustic_hal") (v "0.1.1") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "04r8i8hbmvq38k936j2xrn2v852ajqxsbp7h9dnxvh0p0l3m87q4") (f (quote (("unstable" "serde_derive") ("default" "serde_codegen"))))))

(define-public crate-rustic_hal-0.1.2 (c (n "rustic_hal") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1avbicd01pk844ihr43sgnk57zz8k69g92hrsmp5i9gr22kpzpcn") (y #t)))

(define-public crate-rustic_hal-0.1.3 (c (n "rustic_hal") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gmzns7vs65d7rqd21qmfvcnyydkq40ps09vk30q5789xfp7wck1")))

(define-public crate-rustic_hal-0.2.0 (c (n "rustic_hal") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k9v31ap5kpijx0zx8bi5x2kf5gf8xqdn5csrrbdjiqvs43cs71b")))

