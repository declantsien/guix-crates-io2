(define-module (crates-io ru st rustreexo) #:use-module (crates-io))

(define-public crate-rustreexo-0.1.0 (c (n "rustreexo") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "15w9ys5726mz0w64dl2347pbmmppc4sclc6fnfdxiqf3vsvwllj8") (f (quote (("with-serde" "serde") ("default"))))))

