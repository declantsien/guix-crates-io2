(define-module (crates-io ru st rusty_wordle) #:use-module (crates-io))

(define-public crate-rusty_wordle-1.0.0 (c (n "rusty_wordle") (v "1.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10csqjlk4wz30dr01r8z2jyqa91xp57w79b3486b21ryim4y1znh")))

(define-public crate-rusty_wordle-1.0.1 (c (n "rusty_wordle") (v "1.0.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xg7x4jxks8gxhd5ym4vx9b5ppmbz8gf8iiy5nrrfb7dxving10s")))

(define-public crate-rusty_wordle-1.0.2 (c (n "rusty_wordle") (v "1.0.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0i9g4dxhj424rqggmjaxfrb6k3b19kndrdwvz71cf8268l3gy3w4")))

(define-public crate-rusty_wordle-1.0.3 (c (n "rusty_wordle") (v "1.0.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gg0c9ja7k98nzcljmymz1yf8k64jbi07lwkak8z617h5qjwk1lp")))

