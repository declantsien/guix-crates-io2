(define-module (crates-io ru st rust-dwm-status) #:use-module (crates-io))

(define-public crate-rust-dwm-status-0.1.0 (c (n "rust-dwm-status") (v "0.1.0") (d (list (d (n "chan") (r "^0.1.19") (d #t) (k 0)) (d (n "chan-signal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.0") (d #t) (k 0)))) (h "08gdav5cq8hczhz0p5m4dxpbxjbczkvlpy8pds68mpn0yjssz7ap")))

(define-public crate-rust-dwm-status-0.1.1 (c (n "rust-dwm-status") (v "0.1.1") (d (list (d (n "chan") (r "^0.1.19") (d #t) (k 0)) (d (n "chan-signal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.1") (d #t) (k 0)))) (h "0bcg3d9ry7zfc5sl36lx3vfrnrpimhlby5kxc1bmjycvcyv936zw")))

(define-public crate-rust-dwm-status-0.2.0 (c (n "rust-dwm-status") (v "0.2.0") (d (list (d (n "chan") (r "^0.1.19") (d #t) (k 0)) (d (n "chan-signal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.1") (d #t) (k 0)))) (h "0znxn92jkd3zz06f0k5g5fz7pv73wsq688490mwhgfhcn1y7mzbm")))

(define-public crate-rust-dwm-status-0.3.0 (c (n "rust-dwm-status") (v "0.3.0") (d (list (d (n "chan") (r "^0.1.19") (d #t) (k 0)) (d (n "chan-signal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.2.1") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.1") (d #t) (k 0)))) (h "00hqgxm4wjq0ckqywgb561q99ljd4ca6x08bvhj8db38w6iqyx5s")))

(define-public crate-rust-dwm-status-0.3.1 (c (n "rust-dwm-status") (v "0.3.1") (d (list (d (n "chan") (r "^0.1.19") (d #t) (k 0)) (d (n "chan-signal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.2.1") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.1") (d #t) (k 0)))) (h "12kf5gw69ms04s0zinrhbwpbx9sylxw2jqb0pj7r2lkalxpd8agi")))

(define-public crate-rust-dwm-status-0.4.0 (c (n "rust-dwm-status") (v "0.4.0") (d (list (d (n "chan") (r "^0.1.19") (d #t) (k 0)) (d (n "chan-signal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.2.1") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.1") (d #t) (k 0)))) (h "03jq1siwgzlcwn3f1ay559swhhwzkvfbn5wmfwhrw9i8my48rs3g")))

(define-public crate-rust-dwm-status-0.5.0 (c (n "rust-dwm-status") (v "0.5.0") (d (list (d (n "chan") (r "^0.1.19") (d #t) (k 0)) (d (n "chan-signal") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.2") (d #t) (k 0)))) (h "1mk3qr09wh32jdkyv5awcdalkf7sb5b262clvy91h1jg6m6hc6k4")))

