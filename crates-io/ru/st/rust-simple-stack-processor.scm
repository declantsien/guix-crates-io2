(define-module (crates-io ru st rust-simple-stack-processor) #:use-module (crates-io))

(define-public crate-rust-simple-stack-processor-0.2.0 (c (n "rust-simple-stack-processor") (v "0.2.0") (h "1f6s1i6n28jzq9g13vm882rb39kaakpi7gbpl7x9rjnxcszchpjk")))

(define-public crate-rust-simple-stack-processor-0.3.0 (c (n "rust-simple-stack-processor") (v "0.3.0") (h "1r6ihiqrynbyjv40zxxnn3fr4jpfvsg9lymylwrsgcy0n9km5b8z")))

(define-public crate-rust-simple-stack-processor-0.3.1 (c (n "rust-simple-stack-processor") (v "0.3.1") (h "0aavhl21b7ybzbhb84kx52r9cs85ilbiiq2hh0vri9sgw945ilm2")))

(define-public crate-rust-simple-stack-processor-0.4.0 (c (n "rust-simple-stack-processor") (v "0.4.0") (h "1q8wj1rxg76bdqw296xg2q3zzy614wzxya4f6vi50vqs880bb4cp")))

(define-public crate-rust-simple-stack-processor-0.5.0 (c (n "rust-simple-stack-processor") (v "0.5.0") (h "05qff6gp8x303ryszi1r62cqbbk5ixmknppdzw540h4033wmgl6w")))

(define-public crate-rust-simple-stack-processor-0.6.0 (c (n "rust-simple-stack-processor") (v "0.6.0") (h "1nw2mdxkaidk5pnz8ary205jak5fglaph7gyddbfakxnbgz68mwh")))

(define-public crate-rust-simple-stack-processor-0.7.0 (c (n "rust-simple-stack-processor") (v "0.7.0") (h "0gimw6p7j0v006pmk84zn7bn94cgxcr98fjnmqgmr6s6vn9q2krv")))

