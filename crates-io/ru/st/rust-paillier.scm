(define-module (crates-io ru st rust-paillier) #:use-module (crates-io))

(define-public crate-rust-paillier-0.1.0 (c (n "rust-paillier") (v "0.1.0") (d (list (d (n "gmp") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0w1z6gnc62bnyxk3xc6c109d2bhgy9knkzbicqqgwb882c978akp")))

