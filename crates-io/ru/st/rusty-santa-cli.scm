(define-module (crates-io ru st rusty-santa-cli) #:use-module (crates-io))

(define-public crate-rusty-santa-cli-0.1.0 (c (n "rusty-santa-cli") (v "0.1.0") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rprompt") (r "^1.0.3") (d #t) (k 0)) (d (n "rusty-santa") (r "^0.1") (d #t) (k 0)))) (h "05jxhlxl0qgj0jy5skncfps1xf64hrhaj908xsmgzdd62pqzmyn1")))

