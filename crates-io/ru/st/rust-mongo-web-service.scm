(define-module (crates-io ru st rust-mongo-web-service) #:use-module (crates-io))

(define-public crate-rust-mongo-web-service-0.1.0 (c (n "rust-mongo-web-service") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.1") (f (quote ("tokio-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ki2dxqqxci0l7zj5zlxc9z651g3y1zxmxa91gm1sy2pyixj1pgn")))

