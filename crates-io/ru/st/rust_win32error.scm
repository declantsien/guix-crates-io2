(define-module (crates-io ru st rust_win32error) #:use-module (crates-io))

(define-public crate-rust_win32error-0.7.0 (c (n "rust_win32error") (v "0.7.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0775jr2vc5c67c3m82ykcj4svzw7ylp15mrsj2gj34ix9hs3l6rd")))

(define-public crate-rust_win32error-0.8.0 (c (n "rust_win32error") (v "0.8.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)))) (h "12s1wlzhpywvaaqn2n8y4d7aswpcqapa2zh4l7fydv35icv1b7y3")))

