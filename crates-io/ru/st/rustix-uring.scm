(define-module (crates-io ru st rustix-uring) #:use-module (crates-io))

(define-public crate-rustix-uring-0.1.0 (c (n "rustix-uring") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.98") (k 0)) (d (n "rustix") (r "^0.37") (f (quote ("io_uring" "mm" "time"))) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.4") (d #t) (k 2)))) (h "044iirz1hmwqzg8wdf1mnmgrsvrhm6n1xa1zkfy0hv67l3vvnc6w") (f (quote (("std" "rustix/std") ("default" "std"))))))

(define-public crate-rustix-uring-0.1.1 (c (n "rustix-uring") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.98") (k 0)) (d (n "rustix") (r "^0.37") (f (quote ("io_uring" "mm" "time"))) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.4") (d #t) (k 2)))) (h "03d7sfcnjnbyj2rh50cr6rsfh9sf8wmm4g26mjxydmik169pm85a") (f (quote (("std" "rustix/std") ("default" "std"))))))

(define-public crate-rustix-uring-0.2.0 (c (n "rustix-uring") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.0") (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 2)) (d (n "rustix") (r "^0.38.18") (f (quote ("io_uring" "mm"))) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.5") (d #t) (k 2)))) (h "1knai58byj4dvj7br54fch2wm4rviphy24i8w7jx7yrrcsrhq61g") (f (quote (("std" "rustix/std" "bitflags/std") ("default" "std")))) (r "1.63")))

