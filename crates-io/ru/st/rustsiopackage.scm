(define-module (crates-io ru st rustsiopackage) #:use-module (crates-io))

(define-public crate-rustsIoPackage-0.0.1 (c (n "rustsIoPackage") (v "0.0.1") (h "0vi004fnpgps35cma61mb01nh2dqnr3xqkysnpbfy9bkphfsx5jk")))

(define-public crate-rustsIoPackage-0.0.2 (c (n "rustsIoPackage") (v "0.0.2") (h "1qyb10amx1jpyzx651p0ymjrw8zl0mgynlw7ysbic69jww4b9hkj")))

