(define-module (crates-io ru st rustzx-z80) #:use-module (crates-io))

(define-public crate-rustzx-z80-0.14.0 (c (n "rustzx-z80") (v "0.14.0") (d (list (d (n "paste") (r "^1") (d #t) (k 2)))) (h "10wp8ggjqsda8dsnf9h7rb5vvi5v20y445kdpzai6gqksyzyfr4z")))

(define-public crate-rustzx-z80-0.15.0 (c (n "rustzx-z80") (v "0.15.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0pjns5ims2544q7xp1s7687kkzn5r2dhc9bd7ky43p1cjn2dzkz9")))

(define-public crate-rustzx-z80-0.16.0 (c (n "rustzx-z80") (v "0.16.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "1qp774hix5ran5dr7hw3xmmfimnyxy2w7dzx0x7div1lnsmfwx9c")))

