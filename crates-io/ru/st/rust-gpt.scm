(define-module (crates-io ru st rust-gpt) #:use-module (crates-io))

(define-public crate-rust-gpt-0.0.1 (c (n "rust-gpt") (v "0.0.1") (d (list (d (n "async-trait") (r ">=0.1") (d #t) (k 0)) (d (n "once_cell") (r "~1.17") (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 2)))) (h "09ak9srw5xfr8xqc6w68m5m5chicxsh27a2l6ljk6wq6clv7qw22")))

(define-public crate-rust-gpt-0.0.2 (c (n "rust-gpt") (v "0.0.2") (d (list (d (n "async-trait") (r ">=0.1") (d #t) (k 0)) (d (n "once_cell") (r "~1.17") (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "tokio") (r ">=1.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1rnkq40964qc3x3dn749llfc4pik73wm8mw6ijp4fady3wih45cc")))

(define-public crate-rust-gpt-0.0.3 (c (n "rust-gpt") (v "0.0.3") (d (list (d (n "async-trait") (r ">=0.1") (d #t) (k 0)) (d (n "once_cell") (r "~1.17") (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "tokio") (r ">=1.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ybpzpqcl148k6ch4kfv04s9brbhzaj13ckvdqvi2i1xq7fx305k")))

