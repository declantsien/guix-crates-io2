(define-module (crates-io ru st rusty_utils) #:use-module (crates-io))

(define-public crate-rusty_utils-0.1.0 (c (n "rusty_utils") (v "0.1.0") (h "1iyn9my2w5ql2rnv9skyqxl8j1c1c9inj3v3lc0ys9pkbnrip1i9")))

(define-public crate-rusty_utils-0.1.1 (c (n "rusty_utils") (v "0.1.1") (h "0maklil34pyxnj8qhr5whlmp7pfhi9z24pxs6glrilxwlycvycw8")))

