(define-module (crates-io ru st rustql_common) #:use-module (crates-io))

(define-public crate-rustql_common-0.1.0 (c (n "rustql_common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "09ynn7wv0nhqdmb3njh5rwjbpfn17wg02jhcg5jv3cbs375fz9z2")))

