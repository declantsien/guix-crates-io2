(define-module (crates-io ru st rust_transit_kafka) #:use-module (crates-io))

(define-public crate-rust_transit_kafka-0.1.0 (c (n "rust_transit_kafka") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rdkafka") (r "^0.23") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "rust_transit") (r "^0.1.0") (d #t) (k 0)) (d (n "rust_transit_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "15ph519g0nf13xgk5qr497ibywz39xq31il4d225z2q00q8s3d96")))

