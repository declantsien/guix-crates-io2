(define-module (crates-io ru st rust-usbtmc) #:use-module (crates-io))

(define-public crate-rust-usbtmc-0.1.1 (c (n "rust-usbtmc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "rusb") (r "^0.8.0") (d #t) (k 0)))) (h "1cfzywvglb4xl8n92jnr7zgddankglxayhiwhww9v7z0srfqpwh8") (y #t)))

(define-public crate-rust-usbtmc-0.1.2 (c (n "rust-usbtmc") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "rusb") (r "^0.8.0") (d #t) (k 0)))) (h "07qrvq789x50j3p20dydajf3nxym6lch0d9pzwkw5c303yl7wg30")))

