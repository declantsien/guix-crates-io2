(define-module (crates-io ru st rust_argp) #:use-module (crates-io))

(define-public crate-rust_argp-0.0.0 (c (n "rust_argp") (v "0.0.0") (h "1s8mb6pbm4l5qfm1dm2mxb4dnyl1v2v2iscp6w5ajnrw4cwyana5")))

(define-public crate-rust_argp-0.0.1 (c (n "rust_argp") (v "0.0.1") (h "12ajyj5rj8zhzwh5yj3agzwdyjap31hkm40i8jhq9kasvsrv3bma")))

(define-public crate-rust_argp-0.0.2 (c (n "rust_argp") (v "0.0.2") (h "04qx001p49rk4wxsjvjvpsd4wa331jkdk9x67bsplayy4mj5wqja")))

