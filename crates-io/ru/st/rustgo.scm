(define-module (crates-io ru st rustgo) #:use-module (crates-io))

(define-public crate-rustgo-0.1.0 (c (n "rustgo") (v "0.1.0") (h "09bz7cxz5dvfy861har5q3g84m22rfm2gsn97m2jfdqhm6z3m86r") (y #t)))

(define-public crate-rustgo-1.0.0 (c (n "rustgo") (v "1.0.0") (h "1mhzpvdglgv5j7xfck6ds9jsz7mzp6kpk6jj9w3lz0i0kaywh4lm")))

