(define-module (crates-io ru st rustacean) #:use-module (crates-io))

(define-public crate-rustacean-0.1.0 (c (n "rustacean") (v "0.1.0") (h "0baafgnfk1jrqqg6cmy18brdwpd8dr56qh1mc085walzsxnkicjn")))

(define-public crate-rustacean-0.1.1 (c (n "rustacean") (v "0.1.1") (h "1l9sqh5zvngl8i2wv6pmiqgp350fkcvp8fqyazpsxr5r1i954csf")))

(define-public crate-rustacean-0.1.2 (c (n "rustacean") (v "0.1.2") (h "0jc5wcfqvb384b0406jskl2fk2jdbpp1h4mck05v1i6p1ryi9jj8")))

(define-public crate-rustacean-0.1.3 (c (n "rustacean") (v "0.1.3") (h "0akrq7b84nx0m4xxwiix8rpg7dpmm4xy28cj9l3p6kacl179s3p1")))

(define-public crate-rustacean-0.1.4 (c (n "rustacean") (v "0.1.4") (h "171mas9kdwr81ih2i4z048h61fjwdjwdmiafg8sndhjmrr7f1hi3")))

