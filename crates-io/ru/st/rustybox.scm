(define-module (crates-io ru st rustybox) #:use-module (crates-io))

(define-public crate-RustyBox-0.1.0 (c (n "RustyBox") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm" "serde"))) (k 0)))) (h "1dicj0wx9xnrrza4ml2194fqx1r59b77sx50a3hqlxlqpydawxnn") (y #t)))

