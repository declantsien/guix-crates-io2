(define-module (crates-io ru st rustkell) #:use-module (crates-io))

(define-public crate-rustkell-0.1.0 (c (n "rustkell") (v "0.1.0") (h "102rmyarx583yhnma1qmnzryy4lf0z6m6yhbi0ddmamjbwnn613a")))

(define-public crate-rustkell-0.1.1 (c (n "rustkell") (v "0.1.1") (h "1z9gnp3d4h86dwrblpk5zrjvc47b8z8sa388pgfbffdgz83vhmnw")))

(define-public crate-rustkell-0.1.2 (c (n "rustkell") (v "0.1.2") (h "1gbgl4kxn85f3jx0pcvfw5v9sxwvfd03vwaimsjwqk2m7aca8m16")))

(define-public crate-rustkell-0.1.3 (c (n "rustkell") (v "0.1.3") (h "0b6vpapx7gcn6sbaziyzlyvfm6vyksbfqkrwj00r8d1ncis80r83")))

(define-public crate-rustkell-0.2.0 (c (n "rustkell") (v "0.2.0") (h "1vshnrgzvr7m24lb3j7dkmxxv6ybik78y9xpg75f4zc8vqg4qyh7")))

(define-public crate-rustkell-0.2.1 (c (n "rustkell") (v "0.2.1") (h "1xrbk4jvgc9snrnmd1in7zlkk82mz5yzp97ql50iy9xyh3lpv12r")))

(define-public crate-rustkell-0.2.2 (c (n "rustkell") (v "0.2.2") (h "0wg8pjlwg5zxqhsjh0wybkahw553l3fw1x79mi497jv5bgpqlnxq")))

