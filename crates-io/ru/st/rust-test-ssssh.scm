(define-module (crates-io ru st rust-test-ssssh) #:use-module (crates-io))

(define-public crate-rust-test-ssssh-0.1.0 (c (n "rust-test-ssssh") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0wkwyh0gblgkrbyckg7bq5x7459dnmp55bx4n90mz3sbvid0k5vw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-rust-test-ssssh-0.1.1 (c (n "rust-test-ssssh") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)))) (h "1ksckp3l7ps0s840rf80p2wywljjq6cy4sxzls5hjnj7l04adicg") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-rust-test-ssssh-0.1.2 (c (n "rust-test-ssssh") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)))) (h "06lzncgypnqflyqgyjyqmvn2c72p22s74zmf7jhgy5p716czb656") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-rust-test-ssssh-0.1.3 (c (n "rust-test-ssssh") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)))) (h "17dji4brrm0axxlzjmgw3wkskqg3lcpk9fwqn2fg38jdayy26kjy") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

