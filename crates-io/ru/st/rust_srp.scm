(define-module (crates-io ru st rust_srp) #:use-module (crates-io))

(define-public crate-rust_srp-0.1.0 (c (n "rust_srp") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0c4cm0m4fs509plsl6f925mq5rk3vlrwwjflv3zgspkk88bh30pg")))

(define-public crate-rust_srp-0.1.1 (c (n "rust_srp") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "07nh0hgli2n6lzbjb8w9z121syrsx16pig5w9pcibn0lf2z9vllf")))

(define-public crate-rust_srp-0.1.2 (c (n "rust_srp") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "16v2c4p7v8q8axavd4pgnhxd6ms9lkzkzk1s5zzmdl0r7xp8g8pb")))

(define-public crate-rust_srp-0.1.3 (c (n "rust_srp") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "1i53l3gy8y6aww8g149q14jw3b9idzcipzlkj57ip6icxiln4n31")))

(define-public crate-rust_srp-0.1.4 (c (n "rust_srp") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "1vwahm84c7h7xmq1hxb7019dhhkfy1cxchpylvaav8sx3c9hm597")))

(define-public crate-rust_srp-0.1.5 (c (n "rust_srp") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0w71h058is6rxqryzw2ji0nq5ynxbf87yagypy3hx3bai6y54p7k")))

(define-public crate-rust_srp-0.1.6 (c (n "rust_srp") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0wc5vaaypmxyjb13k3d3q60g3jngwhv49g6f9ndrjjqzvkbz43nm")))

(define-public crate-rust_srp-0.1.7 (c (n "rust_srp") (v "0.1.7") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "02li4mxyqvjv9s70a181hxnzf3byg9mz4qgxkrk67npbmlwc4gns")))

(define-public crate-rust_srp-0.1.8 (c (n "rust_srp") (v "0.1.8") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (f (quote ("num-bigint" "rand" "serde"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)))) (h "0dl5blqvghkwpqwbwd8b1civ4xa63mw465g5bs6gmj0jr086v8wg")))

