(define-module (crates-io ru st rustman) #:use-module (crates-io))

(define-public crate-rustman-0.1.0 (c (n "rustman") (v "0.1.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "0cbgq4lq8myajbqb1g88khhx7yxxwhfjah69yz0jj45fm8kipi22") (y #t)))

(define-public crate-rustman-0.1.1 (c (n "rustman") (v "0.1.1") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "16wai0lwmdh9jwd62dd2h8n9pjggh68j96kg96k5xa92dar0a7as")))

(define-public crate-rustman-0.1.2 (c (n "rustman") (v "0.1.2") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "0xq6qdzg5w3p9q9pcigs0l4m88amdsy5m28nq1dsf6pi1jjwjd3w")))

(define-public crate-rustman-0.1.3 (c (n "rustman") (v "0.1.3") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0ydl9g6rsbwkmgznhsqhsgk4sx0yf6i0d4cn4axmy1nj9x9p7dna")))

(define-public crate-rustman-0.1.4 (c (n "rustman") (v "0.1.4") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0z5dfmk2lvlh562gy7giisn41c77mray7p92d8c7hz1lyylmqkl7")))

(define-public crate-rustman-0.1.5 (c (n "rustman") (v "0.1.5") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "13cbgdvi0p30a1bfsl458pqdjkhdpjisbnm6zp29siwg7wy0c6kb")))

(define-public crate-rustman-0.1.6 (c (n "rustman") (v "0.1.6") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0w1khhxajpfa4fgsfkcrk02kjzjimi7z6cr7ysbq3rg6w98yld08")))

(define-public crate-rustman-0.1.7 (c (n "rustman") (v "0.1.7") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0xdcp3zzj7dm21ggip696zaj2akxw2bhaw0j0xqdnyqjxmfvqagx")))

(define-public crate-rustman-0.1.8 (c (n "rustman") (v "0.1.8") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0bnhlzi4v9bv27ja2xs916s1yck328nihi8iq47m5zyd1z5j7f73")))

(define-public crate-rustman-0.1.9 (c (n "rustman") (v "0.1.9") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0zh9hdy9jicahvxjvapif0dn8a4kyvb910adk0bp4ndnb72zwqr0")))

(define-public crate-rustman-0.2.0 (c (n "rustman") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "122k2bmw47axrjh7nfv1751xdzfb40a2a37v904njkix39mn2jng")))

(define-public crate-rustman-0.2.1 (c (n "rustman") (v "0.2.1") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "09sla7y05gpwqdladjnjajlqgv897m9kixcsiyh31myl0m7l6wrp")))

(define-public crate-rustman-0.2.2 (c (n "rustman") (v "0.2.2") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "0m89l0f3mmp792mid8slwn3akd7k4adxnzdqg6j6kczqzxhxc5yp")))

(define-public crate-rustman-0.2.3 (c (n "rustman") (v "0.2.3") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "0dnddfkhpx8s9d9ca7m479p36m1sz7l2hiwa187xj8ljmmp19m8f")))

(define-public crate-rustman-0.3.0 (c (n "rustman") (v "0.3.0") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "05dk4438k3dv3xlghzr8mysnlni8512libf71nx9gk66q6pfx3bc") (y #t)))

(define-public crate-rustman-0.3.1 (c (n "rustman") (v "0.3.1") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "1a6d8qkr16rp4rxaddhsai43r1bk88jhmj7dfvhxsl6zx6j1yh2q") (y #t)))

(define-public crate-rustman-0.3.2 (c (n "rustman") (v "0.3.2") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "15sh15ncljfpsymhvangn4i7dd2y38nyizr6d4m8zdkg1kjv1cfg") (y #t)))

(define-public crate-rustman-0.3.3 (c (n "rustman") (v "0.3.3") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "0s9jb43ppam0z5jn5f2alj0xy13z6syc1hbjxr9saakqch91g89n") (y #t)))

(define-public crate-rustman-0.4.0 (c (n "rustman") (v "0.4.0") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "http_req") (r "^0.5.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "1caz1rlq3yrn1zmbrxn5ljwa9hhfs9qfm7vw9n29j9xg7xnjxnnq")))

(define-public crate-rustman-0.4.1 (c (n "rustman") (v "0.4.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "http_req") (r "^0.6.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "1s5bz70cn5gqb138qjgcsfsycjs98p7ygyv6b1dikb59jl8109w6")))

(define-public crate-rustman-0.4.2 (c (n "rustman") (v "0.4.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "http_req") (r "^0.6.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "unchained") (r "^0.1.3") (d #t) (k 0)))) (h "1j69k2z6wdhc6kizia9pr4mhrxkgsgjxwccwcw2s6swjq44w0plj")))

(define-public crate-rustman-0.5.0 (c (n "rustman") (v "0.5.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1za5i0w3yapln6mmh1gx1xsgmzvp1i76zmfd4agyg9mwlxd5wwyr")))

(define-public crate-rustman-0.5.1 (c (n "rustman") (v "0.5.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "08vqcqxi5cpqd2yq0jrx0cv737qm24gvq9d9wi8la02phz6hpcyh")))

(define-public crate-rustman-0.5.2 (c (n "rustman") (v "0.5.2") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "14kx4gywyk73jswq8y81xiap0il10ixwbdgp2y1i9klncfkfgnlj")))

(define-public crate-rustman-0.5.3 (c (n "rustman") (v "0.5.3") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0v04ydf64wgh2b0l6q8xx2nd2hxb9sc025jniijqjm45f2ivap52")))

(define-public crate-rustman-0.6.0 (c (n "rustman") (v "0.6.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "08x6yzf197nw6wf6lpafdpalmc00r5s79dmkykgzza3g5rmkrf2k")))

