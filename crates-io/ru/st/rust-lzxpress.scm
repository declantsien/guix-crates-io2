(define-module (crates-io ru st rust-lzxpress) #:use-module (crates-io))

(define-public crate-rust-lzxpress-0.1.0 (c (n "rust-lzxpress") (v "0.1.0") (h "0smcjrfwqk0qmkzs23al2clf95mqw7sl68dq4n5v4cp4iq2rdkgg")))

(define-public crate-rust-lzxpress-0.1.1 (c (n "rust-lzxpress") (v "0.1.1") (h "0zrz0a9yxjacsas44d4dhdrhd9fb34cxjd5ydwh1mmpc9w6vkxxj")))

(define-public crate-rust-lzxpress-0.5.0 (c (n "rust-lzxpress") (v "0.5.0") (h "06p15nsdkk4n35mb8qd31cq9hkyp3azx9yqwnk7fc3mnvzw9wdb4")))

(define-public crate-rust-lzxpress-0.6.0 (c (n "rust-lzxpress") (v "0.6.0") (h "07zpz6x3d2aigd7c33np7i32z687jsqvxa14xyzpchbm76var6bg")))

(define-public crate-rust-lzxpress-0.6.5 (c (n "rust-lzxpress") (v "0.6.5") (h "0ckqj5anjwnv72xr6xykyckwbcz8r77f14k8zgpwkj2i3ylgr0vk")))

(define-public crate-rust-lzxpress-0.7.0 (c (n "rust-lzxpress") (v "0.7.0") (h "07nly7rnb9pwkjjv8z8j8v8h2mbj6cvmimcx0hdg9hv7gzz3zapa")))

(define-public crate-rust-lzxpress-0.7.1 (c (n "rust-lzxpress") (v "0.7.1") (h "0r2mpjj07n0hczyh98nzbb9s5m32r75yi0wkrs2s4k853qmdwy0f")))

