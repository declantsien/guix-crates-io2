(define-module (crates-io ru st rustsynth) #:use-module (crates-io))

(define-public crate-rustsynth-0.1.0 (c (n "rustsynth") (v "0.1.0") (d (list (d (n "rustsynth-sys") (r "^0.1") (d #t) (k 0)))) (h "0h4x09ckwvkzxhrk18p739cgfajp1madgsnq4ggz9rh293qxgzlf")))

(define-public crate-rustsynth-0.1.1 (c (n "rustsynth") (v "0.1.1") (d (list (d (n "rustsynth-sys") (r "^0.1") (d #t) (k 0)))) (h "1c26c1gbdf742y763c115h2wn4mxjcr6j3kv4cxicacfgv43x9v9")))

(define-public crate-rustsynth-0.2.0 (c (n "rustsynth") (v "0.2.0") (d (list (d (n "rustsynth-sys") (r "^0.1") (d #t) (k 0)))) (h "0v9x9i7ddq4k2imm9wazhc7b64w8n8c3avwr28cys3lhkqp4kw6w")))

(define-public crate-rustsynth-0.2.1 (c (n "rustsynth") (v "0.2.1") (d (list (d (n "rustsynth-sys") (r "^0.1") (d #t) (k 0)))) (h "0p5v38gd067707gx1fzaik44022pa0b806gqk93cy1f0fyanqiwr")))

(define-public crate-rustsynth-0.3.0 (c (n "rustsynth") (v "0.3.0") (d (list (d (n "half") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "rustsynth-sys") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zw7y564zkrf76j92avgphk9f27rnv7bmbnwb0w4393riicjsfym") (f (quote (("f16-pixel-type" "half"))))))

(define-public crate-rustsynth-0.4.0 (c (n "rustsynth") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "half") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "rustsynth-derive") (r "^0.4") (d #t) (k 0)) (d (n "rustsynth-sys") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11q4ydnwg8245sgx7hbnv1g6gga155775pizqm43q9bfwwgi5vq2") (f (quote (("vsscript-functions" "rustsynth-sys/vsscript-functions") ("vapoursynth-functions" "rustsynth-sys/vapoursynth-functions") ("default" "vapoursynth-functions" "vsscript-functions")))) (s 2) (e (quote (("f16-pixel-type" "dep:half"))))))

