(define-module (crates-io ru st rusty-hook) #:use-module (crates-io))

(define-public crate-rusty-hook-0.1.0 (c (n "rusty-hook") (v "0.1.0") (h "0sp8czh78l9sr5w1v6m1dng1kfwhmg1bgj4z4xmqvipb4m086q6g")))

(define-public crate-rusty-hook-0.1.1 (c (n "rusty-hook") (v "0.1.1") (h "0v12v13dxq99wnnpl7z22vanrf8nrmzh76x1vr69rcgykrb47nfy")))

(define-public crate-rusty-hook-0.1.2 (c (n "rusty-hook") (v "0.1.2") (h "13835awg6y5ccpa6nn7b965dpqwh2gv58gl178wbl99gg8svz5hn")))

(define-public crate-rusty-hook-0.4.0 (c (n "rusty-hook") (v "0.4.0") (d (list (d (n "ci_info") (r "^0.3.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "05mjy3fpylvn7c87xvs97c6xmn97fcpmcicm5gq3slb28i7vg2z0")))

(define-public crate-rusty-hook-0.4.1 (c (n "rusty-hook") (v "0.4.1") (d (list (d (n "ci_info") (r "^0.4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0pvmpv4p1na3p0krr7j04xxdsgl158l1cg0nhnqwlly92a4nq7sr")))

(define-public crate-rusty-hook-0.5.0 (c (n "rusty-hook") (v "0.5.0") (d (list (d (n "ci_info") (r "^0.4.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.4.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 1)))) (h "05ja1hpfjlqx8q2v38lrwbdr6rjigvlvgmg4rblmy4na54azsk7w") (y #t)))

(define-public crate-rusty-hook-0.5.1 (c (n "rusty-hook") (v "0.5.1") (d (list (d (n "ci_info") (r "^0.4.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.4.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 1)))) (h "16qsnn0ns1hlbvkl9f1sf05c0nahkah40kz6jf62v69g0jjiwna2")))

(define-public crate-rusty-hook-0.6.5 (c (n "rusty-hook") (v "0.6.5") (d (list (d (n "ci_info") (r "^0.4.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.4.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 1)))) (h "1a4n5aa3xkka3qlrbc3dmrid53qa12mha7j3nxrmni3s7fki2y5a")))

(define-public crate-rusty-hook-0.7.0 (c (n "rusty-hook") (v "0.7.0") (d (list (d (n "ci_info") (r "^0.5.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.5.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 1)))) (h "0cgq4ya0ww234gk8mfz3ar238zb4wlp142xn2ia702rv072q9lfh")))

(define-public crate-rusty-hook-0.7.1 (c (n "rusty-hook") (v "0.7.1") (d (list (d (n "ci_info") (r "^0.5.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.5.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 1)))) (h "1c098y0276mfpkmh1q81mc41y857cr086ky9qyq3jqszs8cwbs4i")))

(define-public crate-rusty-hook-0.8.0 (c (n "rusty-hook") (v "0.8.0") (d (list (d (n "ci_info") (r "^0.6.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.6.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 1)))) (h "05dfgamxzd5n9isnipchcvaq1fvsnn7pslz5iz7ljr961yvazl3i")))

(define-public crate-rusty-hook-0.8.1 (c (n "rusty-hook") (v "0.8.1") (d (list (d (n "ci_info") (r "^0.6.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.6.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "00ygafnnby0kcnfnvhid9pjkgryw42a3mxbbcbgihk55576hdz1v")))

(define-public crate-rusty-hook-0.8.2 (c (n "rusty-hook") (v "0.8.2") (d (list (d (n "ci_info") (r "^0.7.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.7.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "0pmq23vacjr4zw02zls04i7vnyrhppz2ydmha02h9kmnwwiqilci")))

(define-public crate-rusty-hook-0.8.3 (c (n "rusty-hook") (v "0.8.3") (d (list (d (n "ci_info") (r "^0.7.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.7.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "076sxh694120anfbqlamr07q1c4c91lxbfnm2rkrgr2n48x2qg41")))

(define-public crate-rusty-hook-0.8.4 (c (n "rusty-hook") (v "0.8.4") (d (list (d (n "ci_info") (r "^0.7.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.7.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "1ajzn1w5pr1xgcfkgy4z6nvlrylz90vg02bzbd3libyghpysc4w7")))

(define-public crate-rusty-hook-0.9.0 (c (n "rusty-hook") (v "0.9.0") (d (list (d (n "ci_info") (r "^0.7.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.7.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "1dip47nm7g2cra5f7msgd4n0q4ncbyzqlpc55g0hxq8wdd5xmn0y")))

(define-public crate-rusty-hook-0.9.1 (c (n "rusty-hook") (v "0.9.1") (d (list (d (n "ci_info") (r "^0.7.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.7.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "1kswdyn53mvrag34zpk79p6w3y9p905vsghp2yqpskgw3izdgmr1")))

(define-public crate-rusty-hook-0.9.2 (c (n "rusty-hook") (v "0.9.2") (d (list (d (n "ci_info") (r "^0.7.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.7.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 1)))) (h "0hrv4c4056wfqx4l13kvm24068ki7jvydn71xppb49vaxjvgrf3j")))

(define-public crate-rusty-hook-0.9.3 (c (n "rusty-hook") (v "0.9.3") (d (list (d (n "ci_info") (r "^0.8.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.8.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 1)))) (h "1db4z3ngjgibwb4fgby87sbfv7y0zymbdbhl01csli5qk92xs25v")))

(define-public crate-rusty-hook-0.9.4 (c (n "rusty-hook") (v "0.9.4") (d (list (d (n "ci_info") (r "^0.8.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.8.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 1)))) (h "1flzrimxvq5sdh0difs4mxc4mqki157qszb2v0g8f9z54c1c29ca")))

(define-public crate-rusty-hook-0.10.0 (c (n "rusty-hook") (v "0.10.0") (d (list (d (n "ci_info") (r "^0.8.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.8.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nias") (r "^0.4.0") (d #t) (k 0)) (d (n "nias") (r "^0.4.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 1)))) (h "0kjvs7p608zqv7syzwlgnxvcn62wa042k9dip6fmhw18x66bzxcb")))

(define-public crate-rusty-hook-0.10.1 (c (n "rusty-hook") (v "0.10.1") (d (list (d (n "ci_info") (r "^0.8.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.8.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nias") (r "^0.4.0") (d #t) (k 0)) (d (n "nias") (r "^0.4.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 1)))) (h "0r3irq8fiqz9spm7slgw1by7cw06gcx69spy8v0d1g1prwwq19y4")))

(define-public crate-rusty-hook-0.10.3 (c (n "rusty-hook") (v "0.10.3") (d (list (d (n "ci_info") (r "^0.9.2") (d #t) (k 0)) (d (n "ci_info") (r "^0.9.2") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nias") (r "^0.4.0") (d #t) (k 0)) (d (n "nias") (r "^0.4.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 1)))) (h "06cyv5568w16hbfj134wl8qxary6z794br5bjky272a8m0dkbliy")))

(define-public crate-rusty-hook-0.11.0 (c (n "rusty-hook") (v "0.11.0") (d (list (d (n "ci_info") (r "^0.10.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.10.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nias") (r "^0.5.0") (d #t) (k 0)) (d (n "nias") (r "^0.5.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 1)))) (h "1q8rrw1b9janpzs9j9wna9gl4k0dfha0g52jzkviq3x96p9wrwlz")))

(define-public crate-rusty-hook-0.11.1 (c (n "rusty-hook") (v "0.11.1") (d (list (d (n "ci_info") (r "^0.10.0") (d #t) (k 0)) (d (n "ci_info") (r "^0.10.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nias") (r "^0.5.0") (d #t) (k 0)) (d (n "nias") (r "^0.5.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 1)))) (h "1x0gmwx4xkvl6kjg10kvlm10h34vgysy7987hy8swqyfm1rqn4r7")))

(define-public crate-rusty-hook-0.11.2 (c (n "rusty-hook") (v "0.11.2") (d (list (d (n "ci_info") (r "^0.10.1") (d #t) (k 0)) (d (n "ci_info") (r "^0.10.1") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nias") (r "^0.5.0") (d #t) (k 0)) (d (n "nias") (r "^0.5.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 1)))) (h "18r1mh2br76b6zwkvch01xi9rhj9fknmh7l5vnx1qzmyc6zfkkln")))

