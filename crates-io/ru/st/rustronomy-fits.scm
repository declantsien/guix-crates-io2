(define-module (crates-io ru st rustronomy-fits) #:use-module (crates-io))

(define-public crate-rustronomy-fits-0.1.0 (c (n "rustronomy-fits") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 2)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "progressing") (r "^3") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rustronomy-core") (r "^0.1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "0yr98vqy4qwfhmlhpk2x52wsmrpzhadf490blzibf3gkdy9hdbif")))

(define-public crate-rustronomy-fits-0.2.0 (c (n "rustronomy-fits") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 2)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "progressing") (r "^3") (d #t) (k 2)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "rustronomy-core") (r "^0.1") (d #t) (k 0)))) (h "134412k9vha68r2dkn1bb8152qg40lz3sxadnqiazyflsqmf0zry")))

