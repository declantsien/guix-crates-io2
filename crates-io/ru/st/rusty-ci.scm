(define-module (crates-io ru st rusty-ci) #:use-module (crates-io))

(define-public crate-rusty-ci-0.1.0 (c (n "rusty-ci") (v "0.1.0") (d (list (d (n "rusty-yaml") (r "^0.1.2") (d #t) (k 0)))) (h "0ix6vmygq1rz939imlgxj47i3ry51nkmcjaiwzlb3lzvr1lj6h0b")))

(define-public crate-rusty-ci-0.1.1 (c (n "rusty-ci") (v "0.1.1") (d (list (d (n "rusty-yaml") (r "^0.1.2") (d #t) (k 0)))) (h "0x8aycpir83h6h2yk8cgvflzxzi992az22cjxh6qqnc4xijrbvxb")))

(define-public crate-rusty-ci-0.1.2 (c (n "rusty-ci") (v "0.1.2") (d (list (d (n "rusty-yaml") (r "^0.1.2") (d #t) (k 0)))) (h "04wnh6z6i3kps60ds6wcwzpcs3mzcnf1q53bmz9z19pkgga4x2nj")))

(define-public crate-rusty-ci-0.2.0 (c (n "rusty-ci") (v "0.2.0") (d (list (d (n "rusty-yaml") (r "^0.1.2") (d #t) (k 0)))) (h "1l6lhd61npndwjvjw60hgaagqlwidmbqxfq292yxr6hnim5x1z3a")))

(define-public crate-rusty-ci-0.2.1 (c (n "rusty-ci") (v "0.2.1") (d (list (d (n "rusty-yaml") (r "^0.2.0") (d #t) (k 0)))) (h "0r62w06jvfh88w6wn38mxar6hwpj3b271c2wrw9x25xdy4fb6y8a")))

(define-public crate-rusty-ci-0.2.2 (c (n "rusty-ci") (v "0.2.2") (d (list (d (n "rusty-yaml") (r "^0.2.1") (d #t) (k 0)))) (h "0ar0h5nrg93j96r3g89wry1vs1jvqf2l6qd2qvcs4g9njn1azlsa")))

(define-public crate-rusty-ci-0.2.3 (c (n "rusty-ci") (v "0.2.3") (d (list (d (n "rusty-yaml") (r "^0.2.1") (d #t) (k 0)))) (h "0vgnchqzz8s604736vsr4qmc7p0bdbfgcrd25294lzkn5dznh9m5") (y #t)))

(define-public crate-rusty-ci-0.2.4 (c (n "rusty-ci") (v "0.2.4") (d (list (d (n "rusty-yaml") (r "^0.2.1") (d #t) (k 0)))) (h "1r4hv7xbzi8ph0z2d3hp80icgnjy2z64s5ib37zrs1z5hhyhcck1")))

(define-public crate-rusty-ci-0.2.5 (c (n "rusty-ci") (v "0.2.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "17bafyjs2rg78rzp9q63595imwc10gpawipygwrqq52vbm7kz22f")))

(define-public crate-rusty-ci-0.2.6 (c (n "rusty-ci") (v "0.2.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1r3y2g5pmfxymgg3vv4v9qdsbszxcvxxkcwvaz3p532pnhdsxyc1")))

(define-public crate-rusty-ci-0.3.0 (c (n "rusty-ci") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0307304i60k9q73pj94j2ss2z4hvfl8lxgq91mxdl27ja81wvcaz")))

(define-public crate-rusty-ci-0.4.0 (c (n "rusty-ci") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0jv50gd1nd7zkab7vfdlisnw232r9av0b9k9277jmqs9x16mv0x7")))

(define-public crate-rusty-ci-0.4.1 (c (n "rusty-ci") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "15gjf7lg26vksd2rlb9y6pzrvdwzif78a3zjzhbcvxlrxmh2v03p")))

(define-public crate-rusty-ci-0.4.2 (c (n "rusty-ci") (v "0.4.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "16p0y1a9y8wqra3bmpcpisy2rcycmgw3h7xz04qv7hk4p7f3qll9")))

(define-public crate-rusty-ci-0.4.3 (c (n "rusty-ci") (v "0.4.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "01b7mgf01yqy0fmlc2am619n960kkf5jwkfbqc9lbg9rfyvn3fcr")))

(define-public crate-rusty-ci-0.4.4 (c (n "rusty-ci") (v "0.4.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1is0zj1wbj15p03c52ms4ys0hzajnfmxclkkqbc3lc6nqb6sm3jc")))

(define-public crate-rusty-ci-0.4.5 (c (n "rusty-ci") (v "0.4.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0b8y6i5j57k88b6g80hfp6k7k75pgpls6m3zzsv26fdvk193p9bk")))

(define-public crate-rusty-ci-0.4.6 (c (n "rusty-ci") (v "0.4.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1db5xv0mvw0z9m1vmgaf10yp8wxjgpm1cjl51xfmplbpapdy6rvd")))

(define-public crate-rusty-ci-0.4.7 (c (n "rusty-ci") (v "0.4.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.3.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "06vgc3azy6kmry21ly8i6lcvil1v7d0ms5bpycpylymh5d174dqm")))

(define-public crate-rusty-ci-0.5.2 (c (n "rusty-ci") (v "0.5.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0zfpf0wh8l037b624hdbdjr1n19qphgv9xch3chj792pcafx5czz")))

(define-public crate-rusty-ci-0.5.3 (c (n "rusty-ci") (v "0.5.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1vkk6c4sv7319xq9sc44bh9i8ym1ihk4s36x69mk3jsbbahq91va")))

(define-public crate-rusty-ci-0.5.4 (c (n "rusty-ci") (v "0.5.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "168m565mxy95brdx0k2zv7l7n6swbrk67q1wz9qaxpfrbh7ak7zh")))

(define-public crate-rusty-ci-0.5.5 (c (n "rusty-ci") (v "0.5.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "020bnjsxgpi4f5rjh70mxgf330nl44is6h06280rrlla45l15awj")))

(define-public crate-rusty-ci-0.5.6 (c (n "rusty-ci") (v "0.5.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0354z7lk24gkj8rmzc9rib2ng40vszlx3xm024d4vs9kxcp05swf")))

(define-public crate-rusty-ci-0.5.7 (c (n "rusty-ci") (v "0.5.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "13q7yb5zbjm539q0vbx49d4zxcgyrnip1a20r7c32fy69jschxyj")))

(define-public crate-rusty-ci-0.5.8 (c (n "rusty-ci") (v "0.5.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1xghdidv9fz8mlwip0qwgy2qby3w4jrdbvgizwfxa0hh7xq5w683")))

(define-public crate-rusty-ci-0.6.0 (c (n "rusty-ci") (v "0.6.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0r1dh34v5s6g4zgk59p0vs90k3annbjd0ga3vsvixlfrhzjkz3vv")))

(define-public crate-rusty-ci-0.6.1 (c (n "rusty-ci") (v "0.6.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "08xx0dv2p5w4ak16cgmsps9885dddq1jrimpjgllqbrgw7343g37")))

(define-public crate-rusty-ci-0.6.2 (c (n "rusty-ci") (v "0.6.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0nh029abc1f8rvd9djwivdwa33ii3cjbd9ca99m3cxg2461fqm2h")))

(define-public crate-rusty-ci-0.6.3 (c (n "rusty-ci") (v "0.6.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0jxxx25ky4j7h339q66l5dkh707dskrw6q77jlnv5lnq3avm66ry")))

(define-public crate-rusty-ci-0.6.4 (c (n "rusty-ci") (v "0.6.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1kdrgjq0a48xi01b1f7jwmbd0l9kimvjkaa5x7swm0y7mqpdbq95")))

(define-public crate-rusty-ci-0.6.5 (c (n "rusty-ci") (v "0.6.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0pyvv2p40w41g9lfd7bdvn78qbn40zq96d68j4v1hv41ij03fv6g")))

(define-public crate-rusty-ci-0.7.1 (c (n "rusty-ci") (v "0.7.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "02db1ajlli48nfrnkhxdmw10i7chf8laywb5a4wmg15la8asa2zm")))

(define-public crate-rusty-ci-0.7.2 (c (n "rusty-ci") (v "0.7.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1l108b5m8844cn5c29jd550cxw35l4axrjfjzbaz11ikhn4gl1fz")))

(define-public crate-rusty-ci-0.7.4 (c (n "rusty-ci") (v "0.7.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "19q0yhkz7cdlraggx8r0lbfsbg4xy0l4p6x4h8lanfc8dsi7ima8")))

(define-public crate-rusty-ci-0.8.0 (c (n "rusty-ci") (v "0.8.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1ggslm5c45n8anxl5wc275n4n23vsv2w7l175kmrr4gg7nrc7kcd")))

(define-public crate-rusty-ci-0.8.1 (c (n "rusty-ci") (v "0.8.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1n243a478nkb85lfpj4bhhr92cgv3vbhi15plfiwjsr0n5pikbxj")))

(define-public crate-rusty-ci-0.8.2 (c (n "rusty-ci") (v "0.8.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1s62587j5icpgk646aw5zrp0g71778w7rx4np7d085qgndqw5n4q")))

(define-public crate-rusty-ci-0.8.3 (c (n "rusty-ci") (v "0.8.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1yc15hp3yq20rjpkzgvf2q5583vgv2h44i7sz7fxkzh8pali7wfx")))

(define-public crate-rusty-ci-0.8.4 (c (n "rusty-ci") (v "0.8.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1227qq7hs4ibbm9n03lp4pbiwwh05l7h5wpb0ylwkz7wdb7c0v2q")))

(define-public crate-rusty-ci-0.8.5 (c (n "rusty-ci") (v "0.8.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "15sp9hfzaljpmh5pmnghyg7l9cmqiqi2amq5c6kwa44vpqpv1hh0")))

(define-public crate-rusty-ci-0.9.0 (c (n "rusty-ci") (v "0.9.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)))) (h "07hlkb3sffqypl2n201xdga9skdp48pdwyz8rgk8z83awwkdfzgd")))

(define-public crate-rusty-ci-0.9.1 (c (n "rusty-ci") (v "0.9.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)))) (h "0bal75891r8flhl7c2fbzra15ih9zsfns56kxha6kih7ykqwlazb")))

(define-public crate-rusty-ci-0.9.2 (c (n "rusty-ci") (v "0.9.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)))) (h "07ni2wxzamic6ainc73zdil45jlpziwxsjgyfax2hzlh01z31pkr")))

(define-public crate-rusty-ci-0.9.3 (c (n "rusty-ci") (v "0.9.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)))) (h "1widd822i09z32j3ylx4kvn6xmr3xhkhprqn1va25xjv5ngjl29m")))

(define-public crate-rusty-ci-0.9.4 (c (n "rusty-ci") (v "0.9.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)))) (h "1na6c26s1nfj17pxp78fv5xf40rfnk0w6fcwyf2xrxpdnxrzx8fj")))

(define-public crate-rusty-ci-0.9.5 (c (n "rusty-ci") (v "0.9.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "os_info") (r "^1.1.1") (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)))) (h "0ng43a5gq5zm1iwi5qymwf1lcsnzlzli9vwvxnh3lrzgjizfpf6y")))

(define-public crate-rusty-ci-0.9.6 (c (n "rusty-ci") (v "0.9.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "os_info") (r "^1.1.1") (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rusty-yaml") (r "^0.4.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.9") (d #t) (k 0)))) (h "1r5fdmwljy9ghhp3xichkkv2xzcqpwmn2nqb9wbhs01csyd5j4vh")))

