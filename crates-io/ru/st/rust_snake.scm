(define-module (crates-io ru st rust_snake) #:use-module (crates-io))

(define-public crate-rust_snake-0.1.0 (c (n "rust_snake") (v "0.1.0") (d (list (d (n "piston") (r "^0.52.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.39.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.77.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.117.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0kjksdqgd87a93y1kl605j6iklfb5ijawnpwmd87i12br09rqbhz")))

(define-public crate-rust_snake-0.1.1 (c (n "rust_snake") (v "0.1.1") (d (list (d (n "piston") (r "^0.52.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.39.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.77.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.117.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0k9brra1w71rrd4vjf41rdm5y42ka32y2q19by3c6ndxqrrx6vxy")))

