(define-module (crates-io ru st rust_bmp) #:use-module (crates-io))

(define-public crate-rust_bmp-0.1.0 (c (n "rust_bmp") (v "0.1.0") (h "1vdfk77y6cpyl079q0z64385kvvx73gqn2vjcgyaq1smyw1ag3k9") (y #t)))

(define-public crate-rust_bmp-0.1.1 (c (n "rust_bmp") (v "0.1.1") (h "05z0a5q6ldk2xbfikcbz2r9280f9d3jvcfrala6r3gwn5xkg1irh") (y #t)))

