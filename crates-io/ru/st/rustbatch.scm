(define-module (crates-io ru st rustbatch) #:use-module (crates-io))

(define-public crate-rustbatch-0.1.0 (c (n "rustbatch") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "0173hbdd2dxf3r68hra3pb2cqhmycja3gcig9d09745qppq99cba") (y #t)))

(define-public crate-rustbatch-0.1.1 (c (n "rustbatch") (v "0.1.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "1m7w6z6qvsfmxvdag8gv8b3zrf7hhg8jbyl6z15ymidbdc2nfx9v") (y #t)))

(define-public crate-rustbatch-0.1.2 (c (n "rustbatch") (v "0.1.2") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "0hgcj02fvpf82pb76hhbdgs1ijri8m5x9vd51fc5a6wp3dyk8px9") (y #t)))

(define-public crate-rustbatch-0.2.0 (c (n "rustbatch") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "0f9jlagl48nhnshg6319w81lib8ivqj3g5jgkj95chpvy1nhzl3n")))

(define-public crate-rustbatch-0.2.1 (c (n "rustbatch") (v "0.2.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "04q1f03w3xif1naiakwmpk5wawcipr7klzqnasm73xpwqm5pkavr")))

(define-public crate-rustbatch-0.2.2 (c (n "rustbatch") (v "0.2.2") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "1sy450qlzdyxsks6b9lg9lvib29yqvfc0rj4v0qzfzr04vyaawp9")))

(define-public crate-rustbatch-0.3.0 (c (n "rustbatch") (v "0.3.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "1rjvv5sf0sxbmg8m1icn709ajg46j3jcb80zhpq2gwblw25hns5p")))

(define-public crate-rustbatch-0.3.1 (c (n "rustbatch") (v "0.3.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "18j1n2p3v05hlqbdfxdfc5adkvy1g0vr5dd1mvc9gbgbn9bx9r1p") (y #t)))

(define-public crate-rustbatch-0.4.0 (c (n "rustbatch") (v "0.4.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.2") (d #t) (k 0)))) (h "0567w291y0w7v00qkqzy6fzfvq2a795j3415dzxa8m8ssb29vssk")))

