(define-module (crates-io ru st rust_wbc) #:use-module (crates-io))

(define-public crate-rust_wbc-0.1.0 (c (n "rust_wbc") (v "0.1.0") (d (list (d (n "rust_keien") (r "^0.1.0") (d #t) (k 0)) (d (n "sort_rust") (r "^0.2.0") (d #t) (k 0)))) (h "1nwj8cy2j0jk5dx1dszmfkkchkn2d43k4gc9mnnp1kx75151y5ss")))

(define-public crate-rust_wbc-0.2.0 (c (n "rust_wbc") (v "0.2.0") (d (list (d (n "rust_keien") (r "^0.1.0") (d #t) (k 0)) (d (n "sort_rust") (r "^0.2.0") (d #t) (k 0)))) (h "115z1d5rgnxz48mn5n1arh2nnig0f3bmk92b8hhh18cdr16yzz6i")))

(define-public crate-rust_wbc-0.3.0 (c (n "rust_wbc") (v "0.3.0") (d (list (d (n "rust_keien") (r "^0.1.0") (d #t) (k 0)) (d (n "sort_rust") (r "^0.2.0") (d #t) (k 0)))) (h "0s17v0s2yyj13cccqfi9wbx07d2wx1scd3lqvrllbx64lww166ip")))

