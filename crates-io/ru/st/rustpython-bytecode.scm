(define-module (crates-io ru st rustpython-bytecode) #:use-module (crates-io))

(define-public crate-rustpython-bytecode-0.1.0 (c (n "rustpython-bytecode") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vcmsslaax50q7alcihlwmny7pbdjjq82n2l10qg7c1g0fr1r29q")))

(define-public crate-rustpython-bytecode-0.1.1 (c (n "rustpython-bytecode") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04rx0x1hk26rbvdx3ic8rsi2vcq6jww47lxzzvk1aizzw537cy6h")))

(define-public crate-rustpython-bytecode-0.1.2 (c (n "rustpython-bytecode") (v "0.1.2") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lz4-compress") (r "^0.1.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jp8i3zwplpdgfbmx1ismxl9vs1akz84pk4j2zn2c2b887ajbba7")))

