(define-module (crates-io ru st rust_object_format) #:use-module (crates-io))

(define-public crate-rust_object_format-0.1.0 (c (n "rust_object_format") (v "0.1.0") (d (list (d (n "rof_rs_core") (r "^0.1.0") (d #t) (k 0)) (d (n "rof_rs_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1ynfn0x4b2x8zd85nvaabdi2imgb0hm0n4wrkvmyac5gvvspphih") (y #t)))

