(define-module (crates-io ru st rust_ssg) #:use-module (crates-io))

(define-public crate-rust_ssg-2.1.7 (c (n "rust_ssg") (v "2.1.7") (d (list (d (n "geml") (r "^1.1.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bcxdylpkzw1qhvsla2sn7bhm3d68r3ibgc3l50y0rywm5rpglwj")))

(define-public crate-rust_ssg-2.1.8 (c (n "rust_ssg") (v "2.1.8") (d (list (d (n "geml") (r "^1.1.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1s08hp3q9ayqpbd6dzdpqj0ns664i1mal6ix81sxq6h6z84md8hr")))

