(define-module (crates-io ru st rust_search) #:use-module (crates-io))

(define-public crate-rust_search-0.1.0 (c (n "rust_search") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cx2hfdg7hg5smx9dzw9ar8fbs0ayiw37pmvbqxxiwmm8j1hvr9h")))

(define-public crate-rust_search-0.1.1 (c (n "rust_search") (v "0.1.1") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19j6vd584mls8zhfxn4b8nfwl3l2qzfk78gdv1w03jvr887ygk98")))

(define-public crate-rust_search-0.1.2 (c (n "rust_search") (v "0.1.2") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1znwvxsfv8npq7bkj3dwbxvln4cm0rvwkdhdqs7llfs9c2dbwv8y")))

(define-public crate-rust_search-0.1.3 (c (n "rust_search") (v "0.1.3") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bbw9djk42ymplzbwgxb7gdb6jii01ckrpc6kw0g7iqnvfy0vq7l")))

(define-public crate-rust_search-0.1.4 (c (n "rust_search") (v "0.1.4") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "141d1yy9q57sclhpmv8ddjb43xxfb9c68cmwnh9rnk2g0pyix9d3")))

(define-public crate-rust_search-1.0.0 (c (n "rust_search") (v "1.0.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a8l6p93p3s5fk146943cggkzfqs0v98v5yxx7m0qwl7sld6zhqh")))

(define-public crate-rust_search-2.0.0 (c (n "rust_search") (v "2.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0is5cly77f2g0d9p4nfgwxaca8hfl99pivvwzccavkxvqbf4w5b0")))

(define-public crate-rust_search-2.1.0 (c (n "rust_search") (v "2.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "0y130815v9jsg12zrbnbfcync26y45jz0qz6vp4qklj50bi7nzfj")))

