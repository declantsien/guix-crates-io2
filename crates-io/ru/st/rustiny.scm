(define-module (crates-io ru st rustiny) #:use-module (crates-io))

(define-public crate-Rustiny-0.1.0 (c (n "Rustiny") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wrvgw0zqim0qnk2pgxy9d5brz97y2gmnsazw62737a28mdrci0m")))

(define-public crate-Rustiny-0.1.1 (c (n "Rustiny") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zw9f0952m5n6ny17906p8jd2n6w7qk8dl0djnha3dlidzwx6jrk")))

(define-public crate-Rustiny-0.1.11 (c (n "Rustiny") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fh9f8gslzhgyskiijpmplhh2kzfl9k30wp655bkdvrxq1cil1g2")))

(define-public crate-Rustiny-0.1.12 (c (n "Rustiny") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11asl1ly1cr50likvvzcxd0m5wrvabc3ahm48k1vcsm9cysvdb7s")))

