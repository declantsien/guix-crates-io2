(define-module (crates-io ru st rustc_tools_util) #:use-module (crates-io))

(define-public crate-rustc_tools_util-0.1.0 (c (n "rustc_tools_util") (v "0.1.0") (h "0gk69wm5wrwy6rnmin1ip8iyl87nnv92xn8j0s4c4hb5gzjxw2a5")))

(define-public crate-rustc_tools_util-0.1.1 (c (n "rustc_tools_util") (v "0.1.1") (h "1p5rbsmcaczzbsdy2ifjq3j609vmqjvqnxz4d8ir7j50vxgakidk")))

(define-public crate-rustc_tools_util-0.2.0 (c (n "rustc_tools_util") (v "0.2.0") (h "1vj4ymv29igs7n52m12k138zbsn5k5d7ya4sys6lig7sx7ddl9dp")))

(define-public crate-rustc_tools_util-0.2.1 (c (n "rustc_tools_util") (v "0.2.1") (h "0rk3rn45kb5pzlf5k5ig3nww2z385dsala28wsrl45a25b74i3sr") (f (quote (("deny-warnings"))))))

(define-public crate-rustc_tools_util-0.3.0 (c (n "rustc_tools_util") (v "0.3.0") (h "17q29zmid8zbkyd2cpfg4imc527mdq20y65nxz670jvw69v9984b") (f (quote (("deny-warnings"))))))

