(define-module (crates-io ru st rusty-html-macros) #:use-module (crates-io))

(define-public crate-rusty-html-macros-0.1.0 (c (n "rusty-html-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)))) (h "0ykypy0fb17pk4qw30l9gl6zyan4dfjgn02qclp76gr0861kvhcv")))

