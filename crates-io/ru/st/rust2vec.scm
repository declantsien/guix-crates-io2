(define-module (crates-io ru st rust2vec) #:use-module (crates-io))

(define-public crate-rust2vec-0.1.0 (c (n "rust2vec") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)))) (h "12n77f5zb3haj4jb4afwsficm68hg479hs5xsll51ln108sdqpzw")))

(define-public crate-rust2vec-0.2.0 (c (n "rust2vec") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)))) (h "14k0fng98wi5bcv9fyfa3pbn5f8lsq3m4izf226apyjg0mf88k09")))

(define-public crate-rust2vec-0.2.1 (c (n "rust2vec") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)))) (h "1kimvj3x7gn9d1mbgq1r8vww4hx083wa1hfx83vvqm1p55p7936y")))

(define-public crate-rust2vec-0.3.0 (c (n "rust2vec") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)))) (h "13az3k9gp2gd1sfbwb7jqgsqp08jpp736142l3aql4f4rghigmhq")))

(define-public crate-rust2vec-0.3.1 (c (n "rust2vec") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "11r9fm48f2jvqyc2qmsdxad2p4bw4lcgaqkffjikzj8rlacndlvk")))

(define-public crate-rust2vec-0.3.2 (c (n "rust2vec") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "07dv1747yfrwmfr8w1xqybik40cysjl4wbn62v5mff1zvcpykp28")))

(define-public crate-rust2vec-0.4.0 (c (n "rust2vec") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "0gasirl3c7n8yx5xknj9w7xirddnhyaplhdlmsy4wki685fnw2j3")))

(define-public crate-rust2vec-0.5.0 (c (n "rust2vec") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 0)) (d (n "reductive") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "00wczw8ic3fbymm4kkjyzjg154zy77bksxg8lm4zb48b68pccp04")))

(define-public crate-rust2vec-0.5.1 (c (n "rust2vec") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 0)) (d (n "reductive") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1jdz24bi7cx9yn09f9rw5q27l6662nvwvi3rp0lpq9dccj1sza3x")))

(define-public crate-rust2vec-0.5.2 (c (n "rust2vec") (v "0.5.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 0)) (d (n "reductive") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0lm4yjif2jj3am5q30xq642brx69nm9gkh58ljaragk5x4axr4l3")))

