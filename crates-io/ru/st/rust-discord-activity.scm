(define-module (crates-io ru st rust-discord-activity) #:use-module (crates-io))

(define-public crate-rust-discord-activity-0.1.0 (c (n "rust-discord-activity") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0910b7dl09rr4bqri5wbp83qyjg87ld56h087gginf7mi2hl2y09")))

(define-public crate-rust-discord-activity-0.1.1 (c (n "rust-discord-activity") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "07b48an9c2gfvqj229xm67k6nvrb7nr886a39kxmvfsrb9gzhi3k")))

(define-public crate-rust-discord-activity-0.1.2 (c (n "rust-discord-activity") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0fmrlzdv3x2npfqmw431y8rzjnd97jskly3mbsaiiq6qr88vgym6")))

(define-public crate-rust-discord-activity-0.2.0 (c (n "rust-discord-activity") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ckcapc14bc99lvd1lpd3c9nnf8449s70wyjhggl5lmwsbf455bd")))

(define-public crate-rust-discord-activity-0.3.0 (c (n "rust-discord-activity") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1piv0mpanhwqgykdrmrmhs5fjl18lsqb77v31xmsz9ncp0y1fkik")))

