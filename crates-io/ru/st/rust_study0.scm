(define-module (crates-io ru st rust_study0) #:use-module (crates-io))

(define-public crate-rust_study0-0.1.0 (c (n "rust_study0") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "17ii3bbavhjf2082bgixjldbccy3aap1zz2m22d1wfx292ymr6az")))

(define-public crate-rust_study0-0.1.1 (c (n "rust_study0") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1fk03csm00v5a6yb3k9pqph1syasyz5vv37rvr46mxpnrp9gf8zz")))

