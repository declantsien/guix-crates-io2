(define-module (crates-io ru st rustronomy-core) #:use-module (crates-io))

(define-public crate-rustronomy-core-0.1.0 (c (n "rustronomy-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1gnkjqhqwmv5v0jfi19848gd38p1dh49hfqjds0fkdgrm871yvm4")))

(define-public crate-rustronomy-core-0.2.0 (c (n "rustronomy-core") (v "0.2.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0g7wgsqcbpsyhy4dx9921pq416w2732hnz813cjn93axndcvxa8f")))

(define-public crate-rustronomy-core-0.2.1 (c (n "rustronomy-core") (v "0.2.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12y5niiqk7fg1lb47g440r1a1w7rrf93i4k37k3nmc8wp5mprwid")))

(define-public crate-rustronomy-core-0.3.0 (c (n "rustronomy-core") (v "0.3.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0sw80cbr2ab5gz7b8y64kn2q7wy221dgkpyx941h4bnlbjr40wg6")))

(define-public crate-rustronomy-core-0.4.0 (c (n "rustronomy-core") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11v48zkqfwwndy0v8r32n49g3wak1r4ybx11chqk9pky36szvr88")))

(define-public crate-rustronomy-core-0.4.1 (c (n "rustronomy-core") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0aymj7sd9y4kng32b455a8481wf1czhazzgx8f0hfxbr39jfzjzs")))

(define-public crate-rustronomy-core-0.4.2 (c (n "rustronomy-core") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "16q8yk46lzn179faid0wa3wykca1c9k4d247y077vvrmj91ks62a")))

(define-public crate-rustronomy-core-0.5.0 (c (n "rustronomy-core") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1nd2k0l0iwna5qw7qxdqs5vr7m233b8gls1jfdd7s71x88d35g3y")))

(define-public crate-rustronomy-core-0.5.1 (c (n "rustronomy-core") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0i07bzlvc538dwj9bh563ix4jrdd2sdqr1rlhp42j03jfpy26aib")))

