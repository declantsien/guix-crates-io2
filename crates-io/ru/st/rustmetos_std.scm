(define-module (crates-io ru st rustmetos_std) #:use-module (crates-io))

(define-public crate-rustmetos_std-0.1.0 (c (n "rustmetos_std") (v "0.1.0") (d (list (d (n "configparser") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustmetos_api") (r "^1.0.0") (d #t) (k 0)) (d (n "rustmetos_core") (r "^1.0.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0c9nqlhhj1alqlj14mr49qc9bx83f9ky2195a8b5l745f22jwn5c")))

(define-public crate-rustmetos_std-0.2.0 (c (n "rustmetos_std") (v "0.2.0") (d (list (d (n "configparser") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustmetos_api") (r "^1.0.0") (d #t) (k 0)) (d (n "rustmetos_core") (r "^1.0.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1yd98fhm57vc5hv5fy28vfcph6fkdcg8918qah9fs523mhcffhx7")))

(define-public crate-rustmetos_std-0.3.0 (c (n "rustmetos_std") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "configparser") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustmetos_api") (r "^1.0.0") (d #t) (k 0)) (d (n "rustmetos_core") (r "^1.0.0") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0dfq2sj5b2pzwq3yvjss6gpj9blvgrj4cgp75lwfazwmcviblfqi")))

