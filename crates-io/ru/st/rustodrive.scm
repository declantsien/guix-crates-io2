(define-module (crates-io ru st rustodrive) #:use-module (crates-io))

(define-public crate-rustodrive-0.1.0 (c (n "rustodrive") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (d #t) (k 0)))) (h "1mb7mqcrr3yc47j51nnhpam7fv4hb25jz7gzgiywbljqlfr84v3v") (f (quote (("mock-socket"))))))

