(define-module (crates-io ru st rust_hawktracer_proc_macro) #:use-module (crates-io))

(define-public crate-rust_hawktracer_proc_macro-0.1.0 (c (n "rust_hawktracer_proc_macro") (v "0.1.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1gg8xvm5k25858r9n72idqcdb03ikwwyh11an3db994lvwl21y70") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_proc_macro-0.2.0 (c (n "rust_hawktracer_proc_macro") (v "0.2.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.2") (o #t) (d #t) (k 0)))) (h "02sa71dnd2cc920ragzhs1z4pvg2jyccx1x73rmvxmlwvi09hz36") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_proc_macro-0.3.0 (c (n "rust_hawktracer_proc_macro") (v "0.3.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0n7rd5m03z35rn0zqa1p6wi2abpvnknnb5qmh943difrk7n6zr5x") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_proc_macro-0.4.0 (c (n "rust_hawktracer_proc_macro") (v "0.4.0") (d (list (d (n "rust_hawktracer_sys") (r "^0.4") (o #t) (d #t) (k 0)))) (h "13zi21rn8gvpyd8njfbadnpg6qg358c7x49b0zffpwx0rcyqdj7c") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

(define-public crate-rust_hawktracer_proc_macro-0.4.1 (c (n "rust_hawktracer_proc_macro") (v "0.4.1") (d (list (d (n "rust_hawktracer_sys") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1qfksscfv8rbbzv2zb0i9sbbqmig0dr0vrma3c1kzsfmpsynlqnb") (f (quote (("profiling_enabled" "rust_hawktracer_sys") ("pkg_config" "rust_hawktracer_sys" "rust_hawktracer_sys/pkg_config") ("generate_bindings" "rust_hawktracer_sys" "rust_hawktracer_sys/generate_bindings"))))))

