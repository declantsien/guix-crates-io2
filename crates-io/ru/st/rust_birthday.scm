(define-module (crates-io ru st rust_birthday) #:use-module (crates-io))

(define-public crate-rust_birthday-0.1.0 (c (n "rust_birthday") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1ic4fxc6mbyqcdmkvw7xz4lqmvya6hx2gp7wx73ww1c98pnznnng")))

(define-public crate-rust_birthday-0.1.1 (c (n "rust_birthday") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1jvy2911in25dik7h2s0miifms3drfrjdxgwbygnm6wr3igcls0r")))

