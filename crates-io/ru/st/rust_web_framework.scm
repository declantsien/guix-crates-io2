(define-module (crates-io ru st rust_web_framework) #:use-module (crates-io))

(define-public crate-rust_web_framework-0.1.0 (c (n "rust_web_framework") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)))) (h "0rz6l2bhn6c6014ciwy9v1pk119q7fyb0f026q38snwk19jxgdx0")))

(define-public crate-rust_web_framework-0.1.1 (c (n "rust_web_framework") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)))) (h "1xak60h07a19n28gp0rqj47iq9p3flj8aq0anvjdzimx67vzxzia")))

(define-public crate-rust_web_framework-0.1.2 (c (n "rust_web_framework") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)))) (h "15j88mhbj5l46camaqmhcj8baa0hzi604qn4swdbh635mm6fp560")))

(define-public crate-rust_web_framework-0.1.3 (c (n "rust_web_framework") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)))) (h "1r6c70fyqchvp3ifm7qn6k32a539j2wqqvg2i3z8axpg4ddrwwqa")))

(define-public crate-rust_web_framework-0.1.4 (c (n "rust_web_framework") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)))) (h "0mfl95lmj9kcvkyx6x2bvrm0zggb3p1kmhvcfrfh1mh03vszj5p3")))

(define-public crate-rust_web_framework-0.1.5 (c (n "rust_web_framework") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)))) (h "00g3hk4fb5pyjz5fkgd62pd4pygrycpghd9x7vfrvnzhsmnpvajr")))

(define-public crate-rust_web_framework-0.1.6 (c (n "rust_web_framework") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)))) (h "0m81wsc8lsdj7b0244vp909gbvja4lc0h9xzxcw6i3cmwymnj17x")))

