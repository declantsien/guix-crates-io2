(define-module (crates-io ru st rustypipes) #:use-module (crates-io))

(define-public crate-rustypipes-0.1.0 (c (n "rustypipes") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "unix-named-pipe") (r "^0.2.0") (d #t) (k 0)))) (h "0ba277gy997cnr4vp7ga6jggzhf9isa7m0xfychdffb1y1x9r2cp")))

(define-public crate-rustypipes-0.1.1 (c (n "rustypipes") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "unix-named-pipe") (r "^0.2.0") (d #t) (k 0)))) (h "1mhr47721c96d1d0vynkc2lmh077hzp1vw4ygwv0zdw1hldak0ky")))

