(define-module (crates-io ru st rustflow) #:use-module (crates-io))

(define-public crate-rustflow-0.1.0 (c (n "rustflow") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.55.1, <0.56.0") (d #t) (k 1)) (d (n "cc") (r ">=1.0.65, <2.0.0") (d #t) (k 1)) (d (n "image") (r ">=0.23.12, <0.24.0") (d #t) (k 0)))) (h "0j1g0hh2cifcn7w8fvdi7qsrq3kjdbv69n9c6l9zcbj884vysacv")))

