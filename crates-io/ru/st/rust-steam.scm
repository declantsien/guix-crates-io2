(define-module (crates-io ru st rust-steam) #:use-module (crates-io))

(define-public crate-rust-steam-0.1.0 (c (n "rust-steam") (v "0.1.0") (h "0rc69xvp1vxqm0ajb3w6gg6a97kyjm97hjhlhsvww50ilangwysa")))

(define-public crate-rust-steam-0.1.1 (c (n "rust-steam") (v "0.1.1") (h "00dw99fb4x1y1z2rwpvkzvhzm7czyy5yzyywvq1jnqnczckjxcwi")))

(define-public crate-rust-steam-0.1.2 (c (n "rust-steam") (v "0.1.2") (h "11kc1b9049rc68qdz8kdy2aj0637xl6h8l4b8jgh25s6kq7qizsr")))

(define-public crate-rust-steam-0.1.3 (c (n "rust-steam") (v "0.1.3") (h "1lvbbfw5lq29nvm4h8h4lfn3g8zjga1b0s1h7wydyqw9ldxxikhb")))

(define-public crate-rust-steam-0.1.4 (c (n "rust-steam") (v "0.1.4") (h "05mcjii9rix1203riz5bdy8ywgswf33w7np7vm7jlpf0mrl8gz32")))

(define-public crate-rust-steam-0.1.5 (c (n "rust-steam") (v "0.1.5") (h "00v52x1zpkip97pjcv5whqxzs00xzwgdw6wa0fam4i9jkx6a1ppv")))

(define-public crate-rust-steam-0.1.6 (c (n "rust-steam") (v "0.1.6") (h "0qvnqa62pb5w14zb3v7njmrywl7znb1k0laxfcp727l1jbfnll2p")))

(define-public crate-rust-steam-0.1.7 (c (n "rust-steam") (v "0.1.7") (h "16chwzd29mmnqhnsqbcpp22v6anpkk01ycc8n4dvpx8x3jdadhx9")))

(define-public crate-rust-steam-0.1.8 (c (n "rust-steam") (v "0.1.8") (h "1skv88ci4yqbpcj3lb65109rg3l3dl23f5z2m9cswarvkc2klh5n")))

(define-public crate-rust-steam-0.1.9 (c (n "rust-steam") (v "0.1.9") (h "05qv7sdq6b9akcnfhf2300w62jfb1g7rh1qm5fxyrn2llxn0hc2c")))

