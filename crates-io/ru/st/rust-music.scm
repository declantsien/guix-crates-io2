(define-module (crates-io ru st rust-music) #:use-module (crates-io))

(define-public crate-rust-music-0.1.0 (c (n "rust-music") (v "0.1.0") (d (list (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0v42yk8wbvqjjdyk50dv4c926bb0337vl2r8i80kabdkx25v6bya")))

(define-public crate-rust-music-0.1.1 (c (n "rust-music") (v "0.1.1") (d (list (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0iy53qwwczlpkg7s52kk5hjwcbn4075sfgjsgb8q8b83s70rnyk7") (y #t)))

(define-public crate-rust-music-0.1.2 (c (n "rust-music") (v "0.1.2") (d (list (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0xg19dkyipdckz7cwzsqm00sihyp5chrqzpg67p3pss9nzfif1ci")))

(define-public crate-rust-music-0.1.3 (c (n "rust-music") (v "0.1.3") (d (list (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "007bg0q4zb8wyp7cy05l2i9i2d2rq392f82i2gxvrpncsl2japka")))

(define-public crate-rust-music-0.1.4 (c (n "rust-music") (v "0.1.4") (d (list (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "170gs3xdap5dn38lzsxiak6gvp9j849whs6ym6y7482mnjvzqy8b") (f (quote (("composition"))))))

(define-public crate-rust-music-0.1.5 (c (n "rust-music") (v "0.1.5") (d (list (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0b30yhqszzrdah709ymfb8bfpci2z76g59wgwfazafn0jf4vy02s") (f (quote (("composition"))))))

(define-public crate-rust-music-0.1.6 (c (n "rust-music") (v "0.1.6") (d (list (d (n "midly") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1mkk7cndg4lnkb3nv618fz7xb6fsb410fdg4gz1r0xy5qxkrh31s") (f (quote (("composition"))))))

