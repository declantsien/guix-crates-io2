(define-module (crates-io ru st rust-faker) #:use-module (crates-io))

(define-public crate-rust-faker-0.1.0 (c (n "rust-faker") (v "0.1.0") (h "0x5xwiy8vrhczjfpxcp9c9598j1kv7y924vq01d9b06357iyclhr")))

(define-public crate-rust-faker-0.1.1 (c (n "rust-faker") (v "0.1.1") (h "1xxlar6z3zsfcnqp74cx3pfan95jv29z4z2r5h5kgakyn05ggcsb")))

(define-public crate-rust-faker-0.1.2 (c (n "rust-faker") (v "0.1.2") (h "1cziqq9gavdq3m9hhdj5klz9x5n86kvxm6vdagab4143hnags6wn")))

(define-public crate-rust-faker-0.1.3 (c (n "rust-faker") (v "0.1.3") (h "1mzan7mlraziwjzfrj248ji4niy7ki47f76vjr0kq9a9qs0m50v7")))

(define-public crate-rust-faker-0.1.4 (c (n "rust-faker") (v "0.1.4") (h "194md5802ca8w3lsd8kxn7ymxlz3j027z87l3v12iwpcr6n0i09y")))

(define-public crate-rust-faker-0.1.5 (c (n "rust-faker") (v "0.1.5") (h "1ki95wqp134rfwad0gysz9g8lprjrfaa0w9w39ngjynsxa0jlrk1")))

