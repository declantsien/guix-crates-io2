(define-module (crates-io ru st rustdoc-include) #:use-module (crates-io))

(define-public crate-rustdoc-include-0.1.2 (c (n "rustdoc-include") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "parse-display") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "yansi") (r "^1.0.1") (d #t) (k 0)))) (h "1b2i2d7ckcl3q4mqzpj98ifhwscyh5k2s5icz9b95ss0yafqfmv4")))

