(define-module (crates-io ru st rust_bench_parser) #:use-module (crates-io))

(define-public crate-rust_bench_parser-0.1.0 (c (n "rust_bench_parser") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "17yjhws3qi9j077nzpwi4n24s8c8wmxdd2m0a6hw42ssgpgpmv66")))

