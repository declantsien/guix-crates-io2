(define-module (crates-io ru st rust-audit-info) #:use-module (crates-io))

(define-public crate-rust-audit-info-0.1.0-rc0 (c (n "rust-audit-info") (v "0.1.0-rc0") (d (list (d (n "auditable-extract") (r "^0.1.0-rc0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4.1") (d #t) (k 0)))) (h "13llpyrkb1llpqpg5amzrcblhgab8skv83fs6hxvhhp1zfnb10cx")))

(define-public crate-rust-audit-info-0.1.0 (c (n "rust-audit-info") (v "0.1.0") (d (list (d (n "auditable-extract") (r "^0.1.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4.1") (d #t) (k 0)))) (h "0f14r3qcjrafwd7g6h6n17akp27wg9klq8vf183jch565hy5nq37")))

(define-public crate-rust-audit-info-0.2.0-alpha.1 (c (n "rust-audit-info") (v "0.2.0-alpha.1") (d (list (d (n "auditable-extract") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)))) (h "0vddpqci560swymz0zmgghpfcfxvw0ii7ivd96pflgxk7pw1aylr")))

(define-public crate-rust-audit-info-0.2.0 (c (n "rust-audit-info") (v "0.2.0") (d (list (d (n "auditable-extract") (r "^0.2.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)))) (h "06y5ib7z5ywbhwkppldirbxbs5bbfpaxc9mjzc7dv6awqzghvsvk")))

(define-public crate-rust-audit-info-0.3.0 (c (n "rust-audit-info") (v "0.3.0") (d (list (d (n "auditable-extract") (r "^0.3.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)))) (h "0zziq5bniwjcxvdpd2ndbgw0if1a0dh07h928gbd7k9bcp876r3a")))

(define-public crate-rust-audit-info-0.4.0 (c (n "rust-audit-info") (v "0.4.0") (d (list (d (n "auditable-extract") (r "^0.3.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5.3") (d #t) (k 0)))) (h "0566fr37vcxknhx6f1jmrw1m298igrlvxv2wsj3bl3yn8qsczba8")))

(define-public crate-rust-audit-info-0.4.1 (c (n "rust-audit-info") (v "0.4.1") (d (list (d (n "auditable-extract") (r "^0.3.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (f (quote ("std"))) (d #t) (k 0)))) (h "1ddnl96mhfrmv3mq4zgpfdds57kqrklb0rlwn6y15081bj1rhmcp")))

(define-public crate-rust-audit-info-0.5.0 (c (n "rust-audit-info") (v "0.5.0") (d (list (d (n "auditable-info") (r "^0.5.0") (k 0)))) (h "03gxy64pzs1hqbx3brj4g3ww4g8nq327wsd9pjxr1wrvlz4ajy87")))

(define-public crate-rust-audit-info-0.5.1 (c (n "rust-audit-info") (v "0.5.1") (d (list (d (n "auditable-info") (r "^0.5.0") (k 0)))) (h "0gyf6swr40njqm6psz0s4xziwfswlxc8pr9cssxalms4vh19fbb3")))

(define-public crate-rust-audit-info-0.5.2 (c (n "rust-audit-info") (v "0.5.2") (d (list (d (n "auditable-info") (r "^0.6.0") (k 0)))) (h "1p3p675d6ix4b90izf6z9fw9pa9nv2jf86mznp500v5jv8i19nkw")))

(define-public crate-rust-audit-info-0.5.3 (c (n "rust-audit-info") (v "0.5.3") (d (list (d (n "auditable-info") (r "^0.7.1") (k 0)))) (h "0r6f03i4l9m458q54xxfwjy85mmfn4n0wc2f52rfkd3mb46z14qp") (f (quote (("wasm" "auditable-info/wasm") ("default" "wasm"))))))

(define-public crate-rust-audit-info-0.5.4 (c (n "rust-audit-info") (v "0.5.4") (d (list (d (n "auditable-info") (r "^0.7.2") (k 0)))) (h "10yg9gh488l7kn2b7cynzaqkbl4yqwl8d5m2amxmrp1iifls34a6") (f (quote (("wasm" "auditable-info/wasm") ("default" "wasm"))))))

