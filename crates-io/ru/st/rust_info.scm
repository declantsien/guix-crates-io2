(define-module (crates-io ru st rust_info) #:use-module (crates-io))

(define-public crate-rust_info-0.1.0 (c (n "rust_info") (v "0.1.0") (h "0k2qlqszc9h2xz8az20brwaqyd9g7z3ig3bvy990byiix7ha9glb") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.1 (c (n "rust_info") (v "0.1.1") (h "169ad9z94bs19v3yyyh7gj0ckljq721nx4kdhg0sx8zc7z1v0apw") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.2 (c (n "rust_info") (v "0.1.2") (h "01c2xkpdkrvfwar85kx2zk3ka0x4szpz0zpga22vrbii4lkx0kbg") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.3 (c (n "rust_info") (v "0.1.3") (h "11a48j3nbsxp25mxy1rzlnmgqqdawqkr1b0s29lnwksip2hgaj20") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.4 (c (n "rust_info") (v "0.1.4") (h "0q1lr009wk4x8ljacapg191s5f2lxkcc269al10ahxy746s2wmrr") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.5 (c (n "rust_info") (v "0.1.5") (h "0rw8sflrsx599xqjpi3pf6xb6wcfhv9frg8mfb83cfzgdbbf8hnw") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.6 (c (n "rust_info") (v "0.1.6") (h "0xg4qmd3gnddbdy11r05j0zj3hz0p0zh07h5pmvg1fffls2akhm9") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.7 (c (n "rust_info") (v "0.1.7") (h "0hypwb8mwn9c42cykq6rr4jwf1cvr25vr1rxpb2q2vrjyacip139") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.8 (c (n "rust_info") (v "0.1.8") (h "1lpkrjnj55m0gvfp1dl3xa71hq80w0zzcyc1fvclma00pswm1438") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.9 (c (n "rust_info") (v "0.1.9") (h "0awddby4lvi06klcnjblpdi9kmvdh5zvri2sc9111xzi13zqychx") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.10 (c (n "rust_info") (v "0.1.10") (h "141xzx5zjkmfjmjkn3g8ckyzz232id3bvlvkply4y7zdg3isp1z7") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.11 (c (n "rust_info") (v "0.1.11") (h "1va58nrsffsfrg2zj287k6k1j0bih56vnyyvli8ivd9a3dbjm0nj") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.12 (c (n "rust_info") (v "0.1.12") (h "0fn36ry6ns4wkmnj55pkxx0grg88xzk55bvy09qxalpqahm1mqiz") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.13 (c (n "rust_info") (v "0.1.13") (h "1a699agckcn3fdf8g5h5cd4gjcasm17r8wj0cpf5f039qr6nzliq") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.14 (c (n "rust_info") (v "0.1.14") (h "0x0lrxqc2gn01nafkw5hx1wy3by3gracci6nn0i0crvbpw2lfbya") (f (quote (("default"))))))

(define-public crate-rust_info-0.1.15 (c (n "rust_info") (v "0.1.15") (h "03v8yjbfbkkhacybl2m9fgxyxyj7jqmxm5wklizscax0dpv85dsx") (f (quote (("default"))))))

(define-public crate-rust_info-0.2.0 (c (n "rust_info") (v "0.2.0") (h "1agq4dbwxpq4q699ly61zynfyyarjbikj4dck40n8q337w6nbn75") (f (quote (("default"))))))

(define-public crate-rust_info-0.2.1 (c (n "rust_info") (v "0.2.1") (h "1684sx1jjzsbcjk393vynbk1h584wlis7y4yyf9dzljnffdl6imy")))

(define-public crate-rust_info-0.2.2 (c (n "rust_info") (v "0.2.2") (h "1nwplpvp40yvdwyjd2h1phh9nk6jrnsbxc4549fcj21c0ajh8kkf")))

(define-public crate-rust_info-0.3.0 (c (n "rust_info") (v "0.3.0") (h "0gbp3rhw622fyhfyiwq05v5b70n1knf06rik16pzzxvdk4miz55y")))

(define-public crate-rust_info-0.3.1 (c (n "rust_info") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0bqxrd2sfq6dxs9c6y5h3gc72v6ymhlvbvd3fj6vh0v7g6yhdd82")))

(define-public crate-rust_info-0.3.2 (c (n "rust_info") (v "0.3.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0g84k1pb3z7cgxild3f81mi8l7az02bjx9xk8wrl7r0m7plra542")))

