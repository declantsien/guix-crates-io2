(define-module (crates-io ru st rust-libretro-example-core) #:use-module (crates-io))

(define-public crate-rust-libretro-example-core-0.1.0 (c (n "rust-libretro-example-core") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rust-libretro") (r "^0.1.1") (f (quote ("log" "unstable-env-commands"))) (d #t) (k 0)))) (h "1bkcjqbby10cba7ilz03fq4z4y77asdid38wgiz82482nld5qliz")))

(define-public crate-rust-libretro-example-core-0.1.1 (c (n "rust-libretro-example-core") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.117") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rust-libretro") (r "^0.1.3") (f (quote ("log" "unstable-env-commands"))) (d #t) (k 0)))) (h "08j4scfkn3cvzf6a8ps6mlvcqrhrzmq8q40kkjsdsgniyfyc62ak")))

(define-public crate-rust-libretro-example-core-0.1.4 (c (n "rust-libretro-example-core") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("release_max_level_debug"))) (d #t) (k 2)) (d (n "rust-libretro") (r "^0.1.4") (f (quote ("log" "unstable-env-commands"))) (d #t) (k 0)))) (h "12ms9zci9kywlhci2v7aqalpdk272y770wclm5mqk37ah3rvzs5s")))

(define-public crate-rust-libretro-example-core-0.1.5 (c (n "rust-libretro-example-core") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("release_max_level_debug"))) (d #t) (k 2)) (d (n "rust-libretro") (r "^0.1.5") (f (quote ("log" "unstable-env-commands"))) (d #t) (k 0)))) (h "0k1h4r7ni1r74llf0p1d0521jhaaks6h68z9lcpcdnbinhlcz4aj")))

(define-public crate-rust-libretro-example-core-0.2.3 (c (n "rust-libretro-example-core") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("release_max_level_debug"))) (d #t) (k 2)) (d (n "rust-libretro") (r "^0.2.3") (f (quote ("log" "unstable-env-commands"))) (d #t) (k 0)))) (h "187f5xr3hdr6vxzf6qy3yfxw6dkqlzcgc8d8yf0psvb53yhvl419")))

(define-public crate-rust-libretro-example-core-0.3.1 (c (n "rust-libretro-example-core") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("release_max_level_debug"))) (d #t) (k 2)) (d (n "rust-libretro") (r "^0.3.1") (f (quote ("log" "unstable-env-commands"))) (d #t) (k 0)))) (h "1j02ydx74jnahgbchj572vikvs8hjgajr43vihb8rmg04b6dz4yv")))

(define-public crate-rust-libretro-example-core-0.3.2 (c (n "rust-libretro-example-core") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rust-libretro") (r "^0.3.1") (f (quote ("log" "unstable-env-commands"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("release_max_level_debug"))) (d #t) (k 2)))) (h "19irnbbmfsjdvrq8y01rhxx5x5ghlk2r5zbdhmph8k6jl4dhyd6x")))

