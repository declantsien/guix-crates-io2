(define-module (crates-io ru st rustyline-nightly) #:use-module (crates-io))

(define-public crate-rustyline-nightly-2.0.0-alpha-20180628 (c (n "rustyline-nightly") (v "2.0.0-alpha-20180628") (d (list (d (n "assert_matches") (r "^1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "minwindef" "processenv" "winbase" "wincon" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12ynxn6ppmpslj23qj3ppmkbmnpyi3nlcg192ypvaqc4dllhs4zc") (y #t)))

