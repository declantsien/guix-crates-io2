(define-module (crates-io ru st rust_lib_example) #:use-module (crates-io))

(define-public crate-rust_lib_example-0.1.0 (c (n "rust_lib_example") (v "0.1.0") (d (list (d (n "is-odd") (r "^1.0") (d #t) (k 0)))) (h "175csmlbcpzbd1h9qlcd5b6aa88ijj4hq8w9s3m36imi22xck0s0")))

(define-public crate-rust_lib_example-0.2.0 (c (n "rust_lib_example") (v "0.2.0") (d (list (d (n "is-odd") (r "^1.0") (d #t) (k 0)))) (h "0mzwrwq79h7i4k1pnznbvlqyqflkiampqnpscjw2x8fnwclp2q1a")))

