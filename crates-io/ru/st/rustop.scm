(define-module (crates-io ru st rustop) #:use-module (crates-io))

(define-public crate-rustop-1.0.0 (c (n "rustop") (v "1.0.0") (h "0zv7vfjyiz1vlqb27k669fvlrznfywsn65ghhlrv1g7jcb686kgx")))

(define-public crate-rustop-1.0.1 (c (n "rustop") (v "1.0.1") (h "13rcl5mwd5s04hfm97qj8fwa8663iv3irs787y9iwf7bfh46rm2m")))

(define-public crate-rustop-1.0.2 (c (n "rustop") (v "1.0.2") (h "12k3bkwpzxzpysbqpl3i9x6dqdir9hshqafp58260gy1lb264zsl")))

(define-public crate-rustop-1.0.3 (c (n "rustop") (v "1.0.3") (h "16j08j7fki8iwvbl313rrp0w9ivr9adivfmbk5crz5l39xsw7j2l")))

(define-public crate-rustop-1.0.4 (c (n "rustop") (v "1.0.4") (h "09ika84skxv1p341i77ybgdn6d44ijd0m0s61pwbh5iqs0jddd20")))

(define-public crate-rustop-1.0.5 (c (n "rustop") (v "1.0.5") (h "1m3spj37s0ypik5hwjy5bzr9mjfdgbqi1bfgrjs5k8b3v06bn6hb")))

(define-public crate-rustop-1.1.0 (c (n "rustop") (v "1.1.0") (h "0x1jpiv99hkzmsmckagqssl2mf3lk8dyi0zkd2fng7bcyhrpidfc")))

(define-public crate-rustop-1.1.1 (c (n "rustop") (v "1.1.1") (h "1v5r30yxr64xw5cypdnac324y22ga18plf3d4ra2b157mcbz79jr")))

(define-public crate-rustop-1.1.2 (c (n "rustop") (v "1.1.2") (h "12bf77sx69a6xldnxbwi0q9wnd4kq4mhgzm2ansar2aw9vm7piya")))

(define-public crate-rustop-1.1.3 (c (n "rustop") (v "1.1.3") (h "0k4hj80d1cxzkqffb8z7m25nih9dgx1m3p3dpn72zd10vk04ig1y")))

(define-public crate-rustop-1.1.4 (c (n "rustop") (v "1.1.4") (h "0bxjqwd916wfs84hxz09nqfx1lg10pwhss46j9rwxa1kcs96lnhb")))

