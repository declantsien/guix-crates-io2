(define-module (crates-io ru st rustsysfetch) #:use-module (crates-io))

(define-public crate-rustsysfetch-0.1.0 (c (n "rustsysfetch") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "0bak2yl9ra18j641ijlzvqh97cdqnv4fazai47nzlp92630aal4f")))

(define-public crate-rustsysfetch-0.1.1 (c (n "rustsysfetch") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "1sv9iy39q0n2wsrccwc6psp2j79vj3i382c04lyhp8yjqnp3g0sh")))

(define-public crate-rustsysfetch-0.1.2 (c (n "rustsysfetch") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "0jzdklrppv34asqzmimf0ngdgrq4kvb3zq4bk1cka118n100m6j7")))

(define-public crate-rustsysfetch-0.1.4 (c (n "rustsysfetch") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "1sk6a17x85j9bcdqyai4vhr74b081lky621iffmsc4i5apv3m20y")))

(define-public crate-rustsysfetch-0.1.5 (c (n "rustsysfetch") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "0azrnnq3jhwp4zvm4pmlpdj126smh2949bpayyf9zvqn0sz084yd")))

(define-public crate-rustsysfetch-0.1.6 (c (n "rustsysfetch") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (d #t) (k 0)))) (h "1rbc7nkk0jm88ggcardr26fsx59zi9c1gs2w48hj7656w79hqzmq")))

