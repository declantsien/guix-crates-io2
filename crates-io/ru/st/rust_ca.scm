(define-module (crates-io ru st rust_ca) #:use-module (crates-io))

(define-public crate-rust_ca-0.1.0 (c (n "rust_ca") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "gif") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "13qg18kx55691a367fsifsyc6vkh41q1dg5qgwamvyi4xx0bwshc")))

(define-public crate-rust_ca-0.2.0 (c (n "rust_ca") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "gif") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "0kbv10nf9jsfij6i4ia2vwba9g31qivk52gmpcfjlgprmig31bqn")))

(define-public crate-rust_ca-0.2.1 (c (n "rust_ca") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "gif") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "0x0dfhw6w7vg3zmh0l32yzrgyw8mlqxlhn1fyj1pnnbbvhjzm5bw")))

(define-public crate-rust_ca-0.2.2 (c (n "rust_ca") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "gif") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "11jj2j0anspis6vcnmkgi220iii9zrkxbw77zmwr8c39a27wix0i")))

