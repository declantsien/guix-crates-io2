(define-module (crates-io ru st rust-pulsectl-fork) #:use-module (crates-io))

(define-public crate-rust-pulsectl-fork-0.2.7 (c (n "rust-pulsectl-fork") (v "0.2.7") (d (list (d (n "libpulse-binding") (r "^2.18.1") (d #t) (k 0)))) (h "1cbc064hz7ylgb4ar2127skmjpyg7jahjb9f30c5iqirp2nnj5fr")))

(define-public crate-rust-pulsectl-fork-0.2.8 (c (n "rust-pulsectl-fork") (v "0.2.8") (d (list (d (n "libpulse-binding") (r "^2.22.0") (d #t) (k 0)))) (h "0a8ndqq7w8gfbf30jyax9d4hqy4p7xs9200v7x8rlqddlyn8lmyj")))

(define-public crate-rust-pulsectl-fork-0.2.11 (c (n "rust-pulsectl-fork") (v "0.2.11") (d (list (d (n "libpulse-binding") (r "^2.22.0") (d #t) (k 0)))) (h "1ldbbxni16ba2gk233mv37jgy63d62gx86scc8xdi8dldra0mxdn")))

(define-public crate-rust-pulsectl-fork-0.2.12 (c (n "rust-pulsectl-fork") (v "0.2.12") (d (list (d (n "libpulse-binding") (r "^2.22.0") (d #t) (k 0)))) (h "07p82kszsslfay9qj0zhcvd4p0qsfl80qchiifll59nnkmlr1g6w")))

