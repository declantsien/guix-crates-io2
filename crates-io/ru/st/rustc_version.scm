(define-module (crates-io ru st rustc_version) #:use-module (crates-io))

(define-public crate-rustc_version-0.1.0 (c (n "rustc_version") (v "0.1.0") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "13ahxmjx3r3j1nvz3dgsp124qxgqz33dshnyz1i86llgl8a5p1gr") (y #t)))

(define-public crate-rustc_version-0.1.1 (c (n "rustc_version") (v "0.1.1") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1zxjzf6rjjxpdz7lvlzvnldnh9qp004i5ixq1v6qnkpdpw9v4viw") (y #t)))

(define-public crate-rustc_version-0.1.2 (c (n "rustc_version") (v "0.1.2") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1bl0n2ahczaa55h0g7xpkm63zwspw0z0sbf1az9650032fj08a7l") (y #t)))

(define-public crate-rustc_version-0.1.3 (c (n "rustc_version") (v "0.1.3") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "00ski9fcvs3mjpyaaj11lp6xdnwm35w1v7fbkc2rwh5ywfli7664") (y #t)))

(define-public crate-rustc_version-0.1.4 (c (n "rustc_version") (v "0.1.4") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0v9rwp5ka01jmg4z2z7419ij97yvki84pdvmmsx38axpkmr12rgw") (y #t)))

(define-public crate-rustc_version-0.1.5 (c (n "rustc_version") (v "0.1.5") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1szllm4jm5kj66hz1dg2zchmsyz7hk46hlspmvg0sd4jhj70dxc4") (y #t)))

(define-public crate-rustc_version-0.1.6 (c (n "rustc_version") (v "0.1.6") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "13dbh3bysk60kiwrpd03m3rl0v517xx8aavaw0ppjk5mpx2ncwd2") (y #t)))

(define-public crate-rustc_version-0.1.7 (c (n "rustc_version") (v "0.1.7") (d (list (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1160jjsqhqr25cvhr48hmpp8v61bjvjcnxzb0cyf4373lmp3gxf5")))

(define-public crate-rustc_version-0.2.0 (c (n "rustc_version") (v "0.2.0") (d (list (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "1ylgmzpixqjmnb47fq5a0agj088rmjd8zcjjpd85v6vwbwklw48y")))

(define-public crate-rustc_version-0.2.1 (c (n "rustc_version") (v "0.2.1") (d (list (d (n "semver") (r "^0.6") (d #t) (k 0)))) (h "0sfakdfa4b6xi534sq5ba5i9in3igkdyq204jm95v3fqf1v3lx5r")))

(define-public crate-rustc_version-0.2.2 (c (n "rustc_version") (v "0.2.2") (d (list (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "16mhhrv7g45rhibkia8pvrz9jddlhgc7ycxlmi71r366215a0jm5")))

(define-public crate-rustc_version-0.2.3 (c (n "rustc_version") (v "0.2.3") (d (list (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "02h3x57lcr8l2pm0a645s9whdh33pn5cnrwvn5cb57vcrc53x3hk")))

(define-public crate-rustc_version-0.3.0 (c (n "rustc_version") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "10f1w1k6jqbzspqwfqhpdia5xmcy52l1az73yz8xcr27nh0l5jb5")))

(define-public crate-rustc_version-0.3.1 (c (n "rustc_version") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "1ip1cjmb69ax0vlqj8bcrnkddcv7p471chyx6c56sjy3hks666hq")))

(define-public crate-rustc_version-0.3.2 (c (n "rustc_version") (v "0.3.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "11cbix6q7jcnxb76h4xg38bfp04rgl7ii1392zdpn29lzhf42z9r")))

(define-public crate-rustc_version-0.3.3 (c (n "rustc_version") (v "0.3.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "semver") (r "^0.11") (d #t) (k 0)))) (h "1vjmw7xcdri0spsf24mkpwpph853wrbqppihhw061i2igh4f5pzh")))

(define-public crate-rustc_version-0.4.0 (c (n "rustc_version") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "semver") (r "^1.0") (d #t) (k 0)))) (h "0rpk9rcdk405xhbmgclsh4pai0svn49x35aggl4nhbkd4a2zb85z")))

