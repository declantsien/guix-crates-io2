(define-module (crates-io ru st rust-wasm-prelude) #:use-module (crates-io))

(define-public crate-rust-wasm-prelude-0.3.0 (c (n "rust-wasm-prelude") (v "0.3.0") (d (list (d (n "clippy") (r "= 0.0.174") (o #t) (d #t) (k 0)) (d (n "memalloc") (r "^0.1.0") (d #t) (k 0)))) (h "0h9wm3y1nfb4f208d2lczqr224l1av88yqh5vxfbayrf0z4qk63x") (f (quote (("lint" "clippy"))))))

