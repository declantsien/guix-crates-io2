(define-module (crates-io ru st rustubble) #:use-module (crates-io))

(define-public crate-rustubble-0.1.0 (c (n "rustubble") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1paczbzs6yympy2lx4qjfh39jgwqqs0b4zs58gx7z40x1c5fg8dw")))

(define-public crate-rustubble-0.1.1 (c (n "rustubble") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1z4x78nfz51p869mpxkxygn6lakhgadb8sq136pdg9i56irln7nv")))

(define-public crate-rustubble-0.1.2 (c (n "rustubble") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1bd7wd1amvn3ynbryld3055hvvp3znxak2rw03q9iklpfy483m5g")))

(define-public crate-rustubble-0.1.3 (c (n "rustubble") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0phd2a8krcy1r9n5qfnj68s2jyw0p6scn6cr7bgp1rwjpdc53alk")))

