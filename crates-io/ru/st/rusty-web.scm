(define-module (crates-io ru st rusty-web) #:use-module (crates-io))

(define-public crate-rusty-web-0.0.1 (c (n "rusty-web") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1hii3aby95s2pscr2xcrplralkzgi5fbi0d2a4w7kcianfxmmps9")))

(define-public crate-rusty-web-0.0.2 (c (n "rusty-web") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "07xzmmsvkwxjs8a7xmnb8vmx8f77j2p92h2pv7di4812iskn1dl0")))

