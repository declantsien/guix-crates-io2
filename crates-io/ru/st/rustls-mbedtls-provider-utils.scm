(define-module (crates-io ru st rustls-mbedtls-provider-utils) #:use-module (crates-io))

(define-public crate-rustls-mbedtls-provider-utils-0.1.0 (c (n "rustls-mbedtls-provider-utils") (v "0.1.0") (d (list (d (n "mbedtls") (r "^0.12.1") (f (quote ("std"))) (k 0)) (d (n "rustls") (r "^0.22.1") (k 0)))) (h "13h40cx0fpszc4dbpnj6iq9n92b4f3iq0dhwbgpafzpvaixkk0p9")))

(define-public crate-rustls-mbedtls-provider-utils-0.1.1 (c (n "rustls-mbedtls-provider-utils") (v "0.1.1") (d (list (d (n "mbedtls") (r "^0.12.1") (f (quote ("std"))) (k 0)) (d (n "rustls") (r "^0.22.1") (k 0)))) (h "1644cp0sp1cix17m3l4sf8sc3kiby5n4cvp4263k3smp0yn40r7a")))

(define-public crate-rustls-mbedtls-provider-utils-0.2.0 (c (n "rustls-mbedtls-provider-utils") (v "0.2.0") (d (list (d (n "mbedtls") (r "^0.12.3") (f (quote ("std"))) (k 0)) (d (n "rustls") (r "^0.23.5") (f (quote ("std"))) (k 0)))) (h "1p41mgnfk15ffn1bv7d6ghfa0np0gmv8gkk2wzp5nyfyc5bf2czc")))

