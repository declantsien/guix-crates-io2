(define-module (crates-io ru st rust-rocket) #:use-module (crates-io))

(define-public crate-rust-rocket-0.1.0 (c (n "rust-rocket") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "103nz2vrh0m4170lnxqm1l14gjjj6zb01wzbp5fnk89bb1vki4nb")))

(define-public crate-rust-rocket-0.1.1 (c (n "rust-rocket") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0by4dnjr669pzfrah3vpbsgs6d92rd0csaar048wkgx23mvq1f8g")))

(define-public crate-rust-rocket-0.2.0 (c (n "rust-rocket") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "045qjxykz8zh327imc963iday5951157azhjlak96jr1rwpnkygb")))

(define-public crate-rust-rocket-0.3.0 (c (n "rust-rocket") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0za8jj1q8aj1cflkiyq89867gwi6fiygc7zvm55fdy1sjzmz9lsv")))

(define-public crate-rust-rocket-0.3.1 (c (n "rust-rocket") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0gq2qn5nq3m4nw02439kk3nd86gh319p6a2523z4shl4yfdxvijx")))

(define-public crate-rust-rocket-0.3.2 (c (n "rust-rocket") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1s2y6vhff1mn8lxa63n2wy2mnjpfmwm8im6vjl6y9797w01ym15y")))

(define-public crate-rust-rocket-0.3.4 (c (n "rust-rocket") (v "0.3.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "10fnmhzcidi31nh8xxfpiq9h59vh3k4ycnff49gwp3b7bbj8iy8x")))

(define-public crate-rust-rocket-0.3.5 (c (n "rust-rocket") (v "0.3.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0ydzvsjwra5p6na6cxjpsw3v79ar5d4n1lxg8wrjsxx8b0g207h3")))

(define-public crate-rust-rocket-0.7.1 (c (n "rust-rocket") (v "0.7.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0p3viw2zwfs3zcxc049nqsd89hcpfyb7ygr7w8bkhk8n17g8dldv")))

(define-public crate-rust-rocket-0.7.2 (c (n "rust-rocket") (v "0.7.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0qlaj78sdqhqgymnyabjman9jfifrr4yf6gzwnd9p4c4rs1kw8hi")))

