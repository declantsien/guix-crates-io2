(define-module (crates-io ru st rustometry) #:use-module (crates-io))

(define-public crate-rustometry-0.1.0 (c (n "rustometry") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "02z2ggwxg6a43v2r0b2ahb3caak9fvbsj2g5wy6m1n0n7d2l121r")))

(define-public crate-rustometry-0.1.1 (c (n "rustometry") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "18ra4mhcgd9n39a286s20mbm3wg1pzw44x8dlwjq6xyh730dh9xg")))

(define-public crate-rustometry-0.1.2 (c (n "rustometry") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "11fwa62ydrqzzlqpz1p6j7xxb5fg9qx2fnsdma8lpqrrr2i43ah1")))

