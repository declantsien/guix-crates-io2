(define-module (crates-io ru st rust-modules) #:use-module (crates-io))

(define-public crate-rust-modules-0.1.0 (c (n "rust-modules") (v "0.1.0") (h "0afx35413a3gj4vbiiz2xzxp41rgnkplmyqyqwvwivi002463dbx")))

(define-public crate-rust-modules-0.1.1 (c (n "rust-modules") (v "0.1.1") (h "00g2igfpx9ibhn3q174dc3p8skqaibbfwyi1ymcciz05z98q4fsx")))

