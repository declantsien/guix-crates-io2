(define-module (crates-io ru st rustrings) #:use-module (crates-io))

(define-public crate-rustrings-1.0.0 (c (n "rustrings") (v "1.0.0") (h "1hb8nscza9vx2lykkbcama29jv4bdwyaxmkzlxgyc6mqfr2fmsnq")))

(define-public crate-rustrings-1.0.1 (c (n "rustrings") (v "1.0.1") (h "1vdmhgd0h1hl0y7h12wi91l22japvy21bngpw2ms2s567320hyw8")))

(define-public crate-rustrings-1.0.2 (c (n "rustrings") (v "1.0.2") (h "1fk0251x1yr9ld1d32cyklqbzmyp517fqj0yw49va3x8236l6gf9")))

