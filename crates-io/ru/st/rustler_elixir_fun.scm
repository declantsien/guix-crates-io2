(define-module (crates-io ru st rustler_elixir_fun) #:use-module (crates-io))

(define-public crate-rustler_elixir_fun-0.1.0 (c (n "rustler_elixir_fun") (v "0.1.0") (d (list (d (n "rustler") (r "^0.25.0") (d #t) (k 0)) (d (n "rustler_stored_term") (r "^0.1.0") (d #t) (k 0)) (d (n "rustler_sys") (r "^2.2.0") (d #t) (k 0)))) (h "14wrrday4simhw16lav621vvjz8vrdlha0j60k781wbh7p1ynldn")))

(define-public crate-rustler_elixir_fun-0.1.1 (c (n "rustler_elixir_fun") (v "0.1.1") (d (list (d (n "rustler") (r "^0.25.0") (d #t) (k 0)) (d (n "rustler_stored_term") (r "^0.1.0") (d #t) (k 0)) (d (n "rustler_sys") (r "^2.2.0") (d #t) (k 0)))) (h "0hsjnwdnl0qh13p7cph25i3myjcf99dg7g78qcnjx5l91dnm47ln")))

(define-public crate-rustler_elixir_fun-0.2.0 (c (n "rustler_elixir_fun") (v "0.2.0") (d (list (d (n "rustler") (r "^0.25.0") (d #t) (k 0)) (d (n "rustler_stored_term") (r "^0.1.0") (d #t) (k 0)) (d (n "rustler_sys") (r "^2.2.0") (d #t) (k 0)))) (h "1vbha851aq0925id91f9jn0rwzzjxy51ghrlr9yi96k1jpj480yn")))

(define-public crate-rustler_elixir_fun-0.3.0 (c (n "rustler_elixir_fun") (v "0.3.0") (d (list (d (n "rustler") (r "^0.25.0") (d #t) (k 0)) (d (n "rustler_stored_term") (r "^0.1.0") (d #t) (k 0)) (d (n "rustler_sys") (r "^2.2.0") (d #t) (k 0)))) (h "0js2m0f1fp6gpzvszy99bh31dl2jmib90brcxa0lssx7lfx34bzx")))

