(define-module (crates-io ru st rustutils-rmdir) #:use-module (crates-io))

(define-public crate-rustutils-rmdir-0.1.0 (c (n "rustutils-rmdir") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "0xhj6n6c0s1b2c2nqbiznhpcajmpxpy9hb1kq2xvd742ws5aqhsw")))

