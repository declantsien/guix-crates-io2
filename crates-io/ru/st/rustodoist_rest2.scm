(define-module (crates-io ru st rustodoist_rest2) #:use-module (crates-io))

(define-public crate-rustodoist_rest2-0.0.1 (c (n "rustodoist_rest2") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1a4qwjwksyi360alx80612kgl9awnp542nlp2gp18yab25ssgqls")))

(define-public crate-rustodoist_rest2-0.0.2 (c (n "rustodoist_rest2") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ial1r8xr0r4cx6g529vfhv5s36kwblv7xp39swa9jh0a8nmrf5x")))

(define-public crate-rustodoist_rest2-0.0.3 (c (n "rustodoist_rest2") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ryamra8vmwdp4ill96zwky6xclwmw1960119jx3sp65n7s4k1pr")))

