(define-module (crates-io ru st rustorm_codegen) #:use-module (crates-io))

(define-public crate-rustorm_codegen-0.1.0 (c (n "rustorm_codegen") (v "0.1.0") (d (list (d (n "dao") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1nvb70f2vwrb17yjqc36wbqh8fzz1i74yxvpprqjhb3mxk35lnsj")))

(define-public crate-rustorm_codegen-0.1.1 (c (n "rustorm_codegen") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "19bkcpw0b32gzj3iyj3w47r8nsd6avrk5cdxx9iizj71xhrhwjv3")))

(define-public crate-rustorm_codegen-0.1.2 (c (n "rustorm_codegen") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0i8xa0j3li6nyb02vvzv5b2fwj4mbqi1dq32dq7zf4347v460hnl")))

(define-public crate-rustorm_codegen-0.1.3 (c (n "rustorm_codegen") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1g0fm2pyv96fs6azzb2grmhph16bg55n049s76763kjjryma919d")))

(define-public crate-rustorm_codegen-0.1.8 (c (n "rustorm_codegen") (v "0.1.8") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.1.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "11fd876jcbijfgzh6l2c7sibvr5qgh1a2sjs66lsy0c5fmsl082m")))

(define-public crate-rustorm_codegen-0.2.0 (c (n "rustorm_codegen") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1jxg8463h7y6iz2j8v4k07p46f2qqc9mlgk1mz2mbrycsbpq485b")))

(define-public crate-rustorm_codegen-0.3.0 (c (n "rustorm_codegen") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1iwp08q17qxq983r7v8l47rjc2a6pivyl7hwvy94m1w249jpdfdy")))

(define-public crate-rustorm_codegen-0.3.1 (c (n "rustorm_codegen") (v "0.3.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0sph5n520y6f13hw4f48sj4ah2cfs0284ha6dz8ybivg5k26w1zh")))

(define-public crate-rustorm_codegen-0.5.0 (c (n "rustorm_codegen") (v "0.5.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1igrmzpxzqvvsjfp5k98zh56q3kim9b0ypzk1fjyh3l2cf1c9pj7")))

(define-public crate-rustorm_codegen-0.18.0 (c (n "rustorm_codegen") (v "0.18.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.18.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1dbxyw4yinkq1h7cfbdrvz69733474xhf8805x60qs9d9nhpcyil")))

(define-public crate-rustorm_codegen-0.20.0 (c (n "rustorm_codegen") (v "0.20.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rustorm_dao") (r "^0.20.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1mjh6rsg2rnqj3lnacxxyj7ilwj3mjb7lw0w5nclmbwrg4i5zby8")))

