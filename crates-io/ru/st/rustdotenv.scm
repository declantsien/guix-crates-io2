(define-module (crates-io ru st rustdotenv) #:use-module (crates-io))

(define-public crate-rustdotenv-0.1.0 (c (n "rustdotenv") (v "0.1.0") (d (list (d (n "envfile") (r "^0.2.1") (d #t) (k 0)))) (h "17x2niifm3v72rrw3sslg2zwyap599vy3nb91bpm53qgnd7071bp")))

(define-public crate-rustdotenv-0.1.1 (c (n "rustdotenv") (v "0.1.1") (d (list (d (n "envfile") (r "^0.2.1") (d #t) (k 0)))) (h "0fcfxzhyjh7hgrdqs012fmzdsa3763dkqlzdaig0qxm0k0qzxin5")))

(define-public crate-rustdotenv-0.1.2 (c (n "rustdotenv") (v "0.1.2") (d (list (d (n "envfile") (r "^0.2.1") (d #t) (k 0)))) (h "1p36khs9gfg9ygwxvdd28a1hsffd4pnp6dvznl3kdfa7xngygz4m")))

