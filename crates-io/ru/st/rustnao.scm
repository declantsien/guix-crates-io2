(define-module (crates-io ru st rustnao) #:use-module (crates-io))

(define-public crate-rustnao-0.1.0 (c (n "rustnao") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1k4kv1maazd40l17qpp3kbz5yqm10gf9d7vn8niggbwa1cjjnqr5")))

(define-public crate-rustnao-0.1.1 (c (n "rustnao") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0s85iq0sxabr62nharfb534din6kn789pbhjfkvhwwfkmnrl80yi")))

(define-public crate-rustnao-0.1.2 (c (n "rustnao") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0mxri1qm0mmfz72nc0zcpkrrmbsc3njir7hyp6a9fa004vyzmjz4")))

(define-public crate-rustnao-0.1.3 (c (n "rustnao") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0x1yl7pmp8lhvdbfdm920vzpdsw7dc7m3xmhrpwhxkkknplw47wj")))

(define-public crate-rustnao-0.1.4 (c (n "rustnao") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1hzcmjmhsdl3j9w4zyrzppj55bbbvsvvv4spbcychd31f59gvdrm")))

(define-public crate-rustnao-0.2.0-rc.1 (c (n "rustnao") (v "0.2.0-rc.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0m6mbpvzmqr1ni1ng4c3h2ld7cf5qhdx06yrfpgj0a6rpv49n1nb")))

(define-public crate-rustnao-0.2.0 (c (n "rustnao") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1x5ixwjan1qsy3sc5iz0gx3czgv5nl5mlqhla36kwxahzx4xlfs3")))

(define-public crate-rustnao-0.2.1 (c (n "rustnao") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "160lxaqdxcdypyqhrn0jvbvkss4srvbhzmx4mlw50p22fly8xhyn")))

(define-public crate-rustnao-0.3.0-alpha.1 (c (n "rustnao") (v "0.3.0-alpha.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.1") (f (quote ("blocking" "json" "default-tls" "default-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1ig910qidyjzm67lllky1dhjzidvpfsmdq3pn0l7xjlznm1wvjhw")))

