(define-module (crates-io ru st rustofi) #:use-module (crates-io))

(define-public crate-rustofi-0.1.0 (c (n "rustofi") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "06a32f0w91ys1sd8kfxb5cfp2vw53p30lr5cds8ajdp2zl1jrkdj")))

(define-public crate-rustofi-0.1.1 (c (n "rustofi") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0lcxrbcph1lpw58lzy0na6vjcw8x2xq4rn4kmw7ycan8y0lc59lz")))

(define-public crate-rustofi-0.1.5 (c (n "rustofi") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "043gynq7gk788c1w29klgrslp9191w3gkbdb61i9k92w9cmmrzfr")))

(define-public crate-rustofi-0.1.6 (c (n "rustofi") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0vbyrp4zx5diiiiglf5y5rn02isax2gw8xj5v6c0j4afh4pxxqb2")))

(define-public crate-rustofi-0.2.0 (c (n "rustofi") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0z68kjlcw2rl8klc9smah4rb9kmcb46bypaii4y2vhqx3ki8yrkk") (y #t)))

(define-public crate-rustofi-0.2.1 (c (n "rustofi") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0zic2fqa0z00xrxb7917rz3r378k2prpph2mrivjfidgaz56j01s") (y #t)))

(define-public crate-rustofi-0.2.2 (c (n "rustofi") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pickledb") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 2)) (d (n "subprocess") (r "^0.1.18") (d #t) (k 0)))) (h "0zpb1k44wnyq3ymijbqk835xw5591pzfn52243imhgmkhwb53vrj")))

