(define-module (crates-io ru st rust-ad) #:use-module (crates-io))

(define-public crate-rust-ad-0.1.0 (c (n "rust-ad") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wrdzaiyjqnd9jy28d9lwchklk1w2g0ay7kzagxbskmfzvwm5a4m")))

(define-public crate-rust-ad-0.2.0 (c (n "rust-ad") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r8n4s7w13ixxf9dkdninh485830m4axm3w7x4bfngr47lakffr3")))

(define-public crate-rust-ad-0.3.0 (c (n "rust-ad") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07z7jv8phckxf552qxywwyrqr151aps03n5fbs122y4cixhivybv")))

(define-public crate-rust-ad-0.4.0 (c (n "rust-ad") (v "0.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.2.0") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "117pprsxalgxannqpgfbbb30ld5nfcl118wq77ykr3ysbfjz5gi9")))

(define-public crate-rust-ad-0.4.1 (c (n "rust-ad") (v "0.4.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-ad-core") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02jmnyms93rhr0nvwzysbc2prcwrspy96qaignsagk6skz5hmb13")))

(define-public crate-rust-ad-0.4.2 (c (n "rust-ad") (v "0.4.2") (d (list (d (n "rust-ad-core") (r "^0.2.2") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r2g1wx595dg37rfw6fiwsfsgsrrfhl8jj8i4rf2r82ampx3nkmk")))

(define-public crate-rust-ad-0.4.3 (c (n "rust-ad") (v "0.4.3") (d (list (d (n "rust-ad-core") (r "^0.2.3") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wdgvnpn3b515hcd2mb8rpl42prn8njn83wi3g4y4nnd406xyd4j")))

(define-public crate-rust-ad-0.4.4 (c (n "rust-ad") (v "0.4.4") (d (list (d (n "rust-ad-core") (r "^0.2.4") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12fx2bc8qgg9nb7kxg4zwzfwzafna6m5shfp2yrsfjg0hpz5z7j4")))

(define-public crate-rust-ad-0.5.0 (c (n "rust-ad") (v "0.5.0") (d (list (d (n "rust-ad-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1al6c0661gc2rq81yha98mzfqydb8jyxsa65n9c43nfbndbncaaa")))

(define-public crate-rust-ad-0.5.1 (c (n "rust-ad") (v "0.5.1") (d (list (d (n "rust-ad-core") (r "^0.3.1") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jq1m9s7zcd6c5k2p0dazdks7ml0linjnj46bwv889c6dwk2gd51")))

(define-public crate-rust-ad-0.6.0 (c (n "rust-ad") (v "0.6.0") (d (list (d (n "rust-ad-core") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g1r3zd37zzwmanvxxazwq1bmm1yp7adwcw1hzw0amj7rddbs2jg")))

(define-public crate-rust-ad-0.6.1 (c (n "rust-ad") (v "0.6.1") (d (list (d (n "rust-ad-core") (r "^0.4.1") (d #t) (k 0)) (d (n "rust-ad-macros") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fdb99wr6sl5cn3yynpj64jryaiygzrphzayqnzl480w1h0k403k")))

(define-public crate-rust-ad-0.6.2 (c (n "rust-ad") (v "0.6.2") (d (list (d (n "rust-ad-macros") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qz1isq6fm4a0swyx6wrjj9my8y5w6q0za4pbsaalcjz88sxk9x0")))

(define-public crate-rust-ad-0.7.0 (c (n "rust-ad") (v "0.7.0") (d (list (d (n "rust-ad-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12p0pih541g4vlck4nms2nk7gjxhwqyy7i62f3jb01cwhv3c72nm")))

(define-public crate-rust-ad-0.7.1 (c (n "rust-ad") (v "0.7.1") (d (list (d (n "rust-ad-macros") (r "^0.7.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12xdsb5sdhby82vfbf1z1w5k6yzrxcpdcanc7j2wcg0ccxwl38nf")))

(define-public crate-rust-ad-0.7.2 (c (n "rust-ad") (v "0.7.2") (d (list (d (n "rust-ad-macros") (r "^0.7.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vhdwzs93pwd40irlwn84hjrmgh1v0466avpcr9q8d4shdks2cai")))

(define-public crate-rust-ad-0.8.0 (c (n "rust-ad") (v "0.8.0") (d (list (d (n "rust-ad-macros") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02f1k0g7rrxhnj8ff19y9hzwlnc2c07ghnim2cyx2j4k2rx5xz93")))

