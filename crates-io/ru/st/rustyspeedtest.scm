(define-module (crates-io ru st rustyspeedtest) #:use-module (crates-io))

(define-public crate-rustyspeedtest-0.1.0 (c (n "rustyspeedtest") (v "0.1.0") (h "0i671fnghqwsqdqfq9cb6sa622mw3ndbsk7hnflj3q42qp6bj176")))

(define-public crate-rustyspeedtest-0.1.1 (c (n "rustyspeedtest") (v "0.1.1") (h "1ydxpgaq8vipjqrpnbjhs4fghyswxzx3ksf1458dxf5b17a429n1")))

