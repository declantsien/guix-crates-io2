(define-module (crates-io ru st rust2pickle) #:use-module (crates-io))

(define-public crate-rust2pickle-0.0.1 (c (n "rust2pickle") (v "0.0.1") (d (list (d (n "serde-pickle") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.2") (d #t) (k 0)))) (h "19qxqk9lj261g355mf27w9gk7cjd0qi4kkvffknrwk8g7a1gyb3m")))

(define-public crate-rust2pickle-0.0.2 (c (n "rust2pickle") (v "0.0.2") (d (list (d (n "serde-pickle") (r "^0.6.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.2") (d #t) (k 0)))) (h "1403w586lz5plw1n3qfxzm3n0nj3k5rndsd2q913fwzlj5d8fs6z")))

