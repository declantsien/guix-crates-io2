(define-module (crates-io ru st rustdct) #:use-module (crates-io))

(define-public crate-rustdct-0.1.0 (c (n "rustdct") (v "0.1.0") (d (list (d (n "rand") (r "~0.3") (d #t) (k 2)) (d (n "rustfft") (r "^2.0") (d #t) (k 0)))) (h "0094vrncl3h2nkjkbmvjw3879c480v8yaf37x0xj1pw5a8m5bh5y")))

(define-public crate-rustdct-0.1.1 (c (n "rustdct") (v "0.1.1") (d (list (d (n "rand") (r "~0.3") (d #t) (k 2)) (d (n "rustfft") (r "^2.0") (d #t) (k 0)))) (h "0qgzz1b78mlw16r1j1alrk7n8mf1ssrnsjlpybs7ynlll70wlz2v")))

(define-public crate-rustdct-0.1.2 (c (n "rustdct") (v "0.1.2") (d (list (d (n "rand") (r "~0.3") (d #t) (k 2)) (d (n "rustfft") (r "^2.0") (d #t) (k 0)))) (h "0wj4lwx2ks0kf4ypsrmi0fdp76h5n5qiqvm4ihxkmnjf8xynhh78")))

(define-public crate-rustdct-0.1.3 (c (n "rustdct") (v "0.1.3") (d (list (d (n "rand") (r "~0.3") (d #t) (k 2)) (d (n "rustfft") (r "^2.0") (d #t) (k 0)))) (h "1vhq963svf73f7h3bfs3gq2v2vi592w04r0kkd4aw71hi99dx2mm")))

(define-public crate-rustdct-0.2.0 (c (n "rustdct") (v "0.2.0") (d (list (d (n "rand") (r "~0.3") (d #t) (k 2)) (d (n "rustfft") (r "^2.0") (d #t) (k 0)))) (h "14nnp8mr31kj1y4pbpbnb7h2x303m4ysi4qgik3prqgq8m8nzizz") (y #t)))

(define-public crate-rustdct-0.2.1 (c (n "rustdct") (v "0.2.1") (d (list (d (n "rand") (r "~0.3") (d #t) (k 2)) (d (n "rustfft") (r "^2.0") (d #t) (k 0)))) (h "0mqfwwv52s0nqnics2nnypwgrawks5cjnhqmkc705xhi5nr1qni2")))

(define-public crate-rustdct-0.3.0 (c (n "rustdct") (v "0.3.0") (d (list (d (n "rand") (r "~0.4") (d #t) (k 2)) (d (n "rustfft") (r "^2.1") (d #t) (k 0)))) (h "022p355qkkjp4ad832ws82jxg5dsxy4y5pmix2wpnar7i5m8bxz0")))

(define-public crate-rustdct-0.4.0 (c (n "rustdct") (v "0.4.0") (d (list (d (n "rand") (r "~0.4") (d #t) (k 2)) (d (n "rustfft") (r "^3") (d #t) (k 0)))) (h "0z8bg39g364jn51hyfxpa8b0gaasr4scvnsb2716ikxlfiv1ckgg")))

(define-public crate-rustdct-0.5.0 (c (n "rustdct") (v "0.5.0") (d (list (d (n "rand") (r "~0.4") (d #t) (k 2)) (d (n "rustfft") (r "~4") (d #t) (k 0)))) (h "1mx5zx1j2lm9aqwjjknylnzapjskfdn0zg2kcc0w70vkfyx41fna")))

(define-public crate-rustdct-0.5.1 (c (n "rustdct") (v "0.5.1") (d (list (d (n "rand") (r "~0.4") (d #t) (k 2)) (d (n "rustfft") (r "~4") (d #t) (k 0)))) (h "0645fqjcs2mwyq0gdwn51yxph95hzs4i9dr2gavlgi8pgwx85czs")))

(define-public crate-rustdct-0.6.0 (c (n "rustdct") (v "0.6.0") (d (list (d (n "rand") (r "~0.4") (d #t) (k 2)) (d (n "rustfft") (r "^5") (d #t) (k 0)))) (h "03gq6gddhb4mmsrkqqh68sp2lr732awrh56vvahlv9lap42vbp7s")))

(define-public crate-rustdct-0.7.0 (c (n "rustdct") (v "0.7.0") (d (list (d (n "rand") (r "~0.4") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)))) (h "0fm6l4ggalhmmipnv3h06spc6ibyacin9ih7wv9xs3nf782lf0k2")))

(define-public crate-rustdct-0.7.1 (c (n "rustdct") (v "0.7.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustfft") (r "^6") (d #t) (k 0)))) (h "0lcm1191xx8wizima5j3n25fs90x58v3q1kwg6cbzafn0m8maqcb")))

