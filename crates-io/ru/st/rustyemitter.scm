(define-module (crates-io ru st rustyemitter) #:use-module (crates-io))

(define-public crate-RustyEmitter-2.1.0 (c (n "RustyEmitter") (v "2.1.0") (h "1zmkcmq2gp3ilfxpadyxga45s40a7s285q1wil0cz336sxdga89a")))

(define-public crate-RustyEmitter-2.2.0 (c (n "RustyEmitter") (v "2.2.0") (h "0bv62c9nwhvg177mh7gzkl77y7kiw5qm3jcmzj9mjx5q0h5f67s2")))

