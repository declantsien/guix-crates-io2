(define-module (crates-io ru st rustamodb) #:use-module (crates-io))

(define-public crate-rustamodb-0.0.1 (c (n "rustamodb") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.32.0") (d #t) (k 0)))) (h "0xmkix6jsraszina5xlpqahqwvk5fkvcjy8caarh9d5zl279m37x")))

(define-public crate-rustamodb-0.0.2 (c (n "rustamodb") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.32.0") (d #t) (k 0)))) (h "0kp2clyhx4si66khh8fndkp80ay43wikssbi5rj4cfrj8x3al034")))

(define-public crate-rustamodb-0.0.3 (c (n "rustamodb") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.32.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.32.0") (d #t) (k 0)))) (h "01cm3ssjbjibrgl2pm7k8f4zbniyzdfdgsk2mv17ql00fn942gil")))

(define-public crate-rustamodb-0.0.4 (c (n "rustamodb") (v "0.0.4") (d (list (d (n "lazy_static") (r "~1.2.0") (d #t) (k 0)) (d (n "rusoto_core") (r "~0.32.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "~0.32.0") (d #t) (k 0)))) (h "0737ykzgv0myzsydwnvv3p0n6478c4jb37a0k7lgxfcr1q1plb0s")))

(define-public crate-rustamodb-0.0.5 (c (n "rustamodb") (v "0.0.5") (d (list (d (n "lazy_static") (r "~1.2.0") (d #t) (k 0)) (d (n "rusoto_core") (r "~0.32.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "~0.32.0") (d #t) (k 0)))) (h "1dbvv8kh8smd4jbvjlnqdgslx8zl820k5s56swjic7whrksc8cdd")))

(define-public crate-rustamodb-0.0.6 (c (n "rustamodb") (v "0.0.6") (d (list (d (n "lazy_static") (r "~1.2.0") (d #t) (k 0)) (d (n "rusoto_core") (r "~0.32.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "~0.32.0") (d #t) (k 0)))) (h "15x6pi8pg11ca3ihj03x1b42q31mcxy2x1wwsy9qpgz4sbddrh33")))

