(define-module (crates-io ru st rustchord) #:use-module (crates-io))

(define-public crate-rustchord-0.1.0 (c (n "rustchord") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.116.0") (d #t) (k 2)))) (h "1csci2j8609hs7cbfz6v4mz7kwrcknbp0an79k0sdz8ppbj1isa7")))

(define-public crate-rustchord-0.2.0 (c (n "rustchord") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.116.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "189wff3jq6zl2an9pkjslavh50mk22lg4w47skdwxm28icb606za")))

(define-public crate-rustchord-0.2.1 (c (n "rustchord") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.116.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "04dgs80cygwp13m40zir910f16l06ga3jkq1h4bjfjjgp63ga8a3")))

(define-public crate-rustchord-0.3.0 (c (n "rustchord") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.116.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1han8i9bvxf5ays2blvc1sz63k0znnvyqba1yjsnvy4b6f7bc74l")))

(define-public crate-rustchord-0.3.1 (c (n "rustchord") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.116.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0g78lcfyj40ahb8valspf20dl2wdjxizyidh1ydwxkfzfx4hgw9i")))

(define-public crate-rustchord-0.4.0 (c (n "rustchord") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.116.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1bkpddx7baiyqb5v003aqycr2wbnpdycg4dvl0352bh91vmbiajp")))

(define-public crate-rustchord-0.4.1-alpha.0 (c (n "rustchord") (v "0.4.1-alpha.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.121.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0q4wq5fb718dzc59yrzxg35yjjqq6xvpl772l8v2qy4sd2x5kya7")))

(define-public crate-rustchord-0.5.0 (c (n "rustchord") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.121.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0mwfawhddqqpy7vz2b43imsmdm2dp1k4f7y5d3a52z3jw3p3z3l5")))

