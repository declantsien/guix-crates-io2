(define-module (crates-io ru st rust_lsp) #:use-module (crates-io))

(define-public crate-rust_lsp-0.6.0 (c (n "rust_lsp") (v "0.6.0") (d (list (d (n "languageserver-types") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustdt-json_rpc") (r "^0.3.0") (d #t) (k 0)) (d (n "rustdt_util") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "10q74q2gv3glclrb508mhh00vrqa23cbkkm39qjvrh027xcrva3k")))

(define-public crate-rust_lsp-0.6.1 (c (n "rust_lsp") (v "0.6.1") (d (list (d (n "languageserver-types") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustdt-json_rpc") (r "^0.3.0") (d #t) (k 0)) (d (n "rustdt_util") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "01yhg8bh32a8wdxymaxmmd25fjy03s5kzgh926hzl7xsilcczy2b")))

