(define-module (crates-io ru st rustlate) #:use-module (crates-io))

(define-public crate-rustlate-0.1.0 (c (n "rustlate") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.2.1") (d #t) (k 0)))) (h "0rss58sw0wndz6bkk9n1g188x93qvi14wihngdvli9p4s51d3jji")))

(define-public crate-rustlate-0.2.0 (c (n "rustlate") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.2.1") (d #t) (k 0)))) (h "05fsf6g4afjdwj7j84isi48kwk1s4gca6hs093zr8ic09v54s1kd")))

(define-public crate-rustlate-1.0.0 (c (n "rustlate") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.2.1") (d #t) (k 0)))) (h "00qhzv62c0gjbj29z9h9dzpxpvkhbqkxfbpzidspdag3ynsc139p")))

