(define-module (crates-io ru st rustfmt_ignore) #:use-module (crates-io))

(define-public crate-rustfmt_ignore-0.4.10 (c (n "rustfmt_ignore") (v "0.4.10") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "globset") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "same-file") (r "^1.0.4") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0qw87mbcwq51s55ca99gfzh621p2ajbjsy3233xvf1r6zzl7fwc5") (f (quote (("simd-accel" "globset/simd-accel"))))))

