(define-module (crates-io ru st rusty-cli) #:use-module (crates-io))

(define-public crate-rusty-cli-0.1.0 (c (n "rusty-cli") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.1") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)))) (h "1a6lkzc3zi1i1zwf32q1bxj4bxr1avxcf8mwya33knbfarlgxkly")))

