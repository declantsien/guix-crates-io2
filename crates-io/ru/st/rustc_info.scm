(define-module (crates-io ru st rustc_info) #:use-module (crates-io))

(define-public crate-rustc_info-0.0.1 (c (n "rustc_info") (v "0.0.1") (h "09zarg9q7q4pjizax4awllij066kl9j1g2b1p6xccp9kvkjlm46v") (y #t)))

(define-public crate-rustc_info-0.0.2 (c (n "rustc_info") (v "0.0.2") (h "0mql23czhigvd6vkfnijdixmmfpqlc2hg32kr69g2js5489sr0ad") (y #t)))

