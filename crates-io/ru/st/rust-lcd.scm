(define-module (crates-io ru st rust-lcd) #:use-module (crates-io))

(define-public crate-rust-lcd-0.1.0 (c (n "rust-lcd") (v "0.1.0") (h "04clgfd9858ncbakggyr37n3cbx8xqla16nyavwak5z349xpp88q")))

(define-public crate-rust-lcd-0.1.1 (c (n "rust-lcd") (v "0.1.1") (h "1qrnkb5dq7a18j23kzp3v1pzh1vk8v4zdpg5465rjvsi1rg7d9mz")))

