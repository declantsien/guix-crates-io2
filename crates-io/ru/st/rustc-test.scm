(define-module (crates-io ru st rustc-test) #:use-module (crates-io))

(define-public crate-rustc-test-0.1.0 (c (n "rustc-test") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0nbiv13nlacyz76shy2kkp4l6vvl1vs5gndjwwhi4pp92ql5lakv") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-rustc-test-0.1.1 (c (n "rustc-test") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0pmdp2iv8jfv2g4vhd3g0di7qx84kpc868mlxpgrkvz89zk57fqd") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-rustc-test-0.1.2 (c (n "rustc-test") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0aq1sl47xiakjb213fdr23hqg2w44hl58pi0gxfk0vgf5hjbn8wj") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-rustc-test-0.1.3 (c (n "rustc-test") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0s72m4cjl2vmhcs74dh1diycl9w23b3sv3d2wfvnz1c1j914q6bm") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-rustc-test-0.1.4 (c (n "rustc-test") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0x9878wxb8lqp97d6414sxl7nk0cnp8qqm4yvbhg8bfdqcmpr2js") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-rustc-test-0.1.5 (c (n "rustc-test") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "02gs9nsh4ba67gnfwsgzh7svnhw9axxml2va8jarzr6grw92s1cr") (f (quote (("capture") ("asm_black_box")))) (y #t)))

(define-public crate-rustc-test-0.2.0 (c (n "rustc-test") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ilb5g0nzrzyw97lfxhhdl483fdw5ipy1gd8gms6619r9nb1fic3") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-rustc-test-0.3.0 (c (n "rustc-test") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0a27mlcg0ck0hgsdvwk792x9z1k1qq1wj091f1l5yggbdbcsnx5w") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-rustc-test-0.3.1 (c (n "rustc-test") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1fsr9rnlsch5gygxhz0mq3d02vzrrf0jgbwcihhaz0xfn1kpkk5a") (f (quote (("capture") ("asm_black_box"))))))

