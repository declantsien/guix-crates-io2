(define-module (crates-io ru st rust_lldp) #:use-module (crates-io))

(define-public crate-rust_lldp-0.1.0 (c (n "rust_lldp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pcap") (r "^0.9") (f (quote ("capture-stream"))) (d #t) (k 0)) (d (n "rshark") (r "^0.0.1") (d #t) (k 0)))) (h "1hn27abl7pnpnc40fx27yyxh1dfpqi6hc721dyilc3p75ypbwwns")))

(define-public crate-rust_lldp-0.1.1 (c (n "rust_lldp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "pcap") (r "^0.9") (f (quote ("capture-stream"))) (d #t) (k 0)) (d (n "rshark") (r "^0.0.1") (d #t) (k 0)))) (h "17qjqsrww6jb6viji0q1d0m26gribzdx4l2s3qs5yfg7ys7wx465")))

