(define-module (crates-io ru st rusty-junctions-macro) #:use-module (crates-io))

(define-public crate-rusty-junctions-macro-0.1.0 (c (n "rusty-junctions-macro") (v "0.1.0") (d (list (d (n "rusty-junctions-client-api-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty-junctions-client-api-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rusty-junctions-library-generation-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1016y7wj7sqpbdpzqkwcm72nrcfg83m910vy5qxsvchgcxqiach7")))

