(define-module (crates-io ru st rusty_pipe_viewer) #:use-module (crates-io))

(define-public crate-rusty_pipe_viewer-0.1.0 (c (n "rusty_pipe_viewer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "0g884rmq10qlxasfh34lzi16lpshnllzhjhy97pi18aanbx0g6b6")))

