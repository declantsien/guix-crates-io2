(define-module (crates-io ru st rust-diskinfo) #:use-module (crates-io))

(define-public crate-rust-diskinfo-0.0.1 (c (n "rust-diskinfo") (v "0.0.1") (d (list (d (n "kstat") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j503yi526qksvyxrxh4c08lgmipgan3l8si4431ld8gkzgmybdn") (f (quote (("unstable"))))))

