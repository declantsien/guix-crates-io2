(define-module (crates-io ru st rust-acd) #:use-module (crates-io))

(define-public crate-rust-acd-0.1.0 (c (n "rust-acd") (v "0.1.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "mime") (r "^0.1") (d #t) (k 0)) (d (n "multipart") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1r0lpwh5a0hhw596v81f1malbs4r3dpzncp7224bba8iwnzbl3za")))

