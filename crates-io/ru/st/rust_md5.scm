(define-module (crates-io ru st rust_md5) #:use-module (crates-io))

(define-public crate-rust_md5-0.2.0 (c (n "rust_md5") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0dv0ssx8qn09g56if1j5aj17fg9ck7gqny1yhgcwp5j82cfizbdl")))

