(define-module (crates-io ru st rustic-logger) #:use-module (crates-io))

(define-public crate-rustic-logger-0.1.0 (c (n "rustic-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "nickel") (r "^0.11.0") (d #t) (k 0)))) (h "1w1644yr1rmvzmj9lybq8jysz4bdhwp4937v4yg5xviy9n0wqlwg")))

(define-public crate-rustic-logger-0.1.1 (c (n "rustic-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "12ls6k4zzkvlyjn47sjm6iy7qpgj0s0x3vqxpaa9sq0zbl0n8gi8")))

(define-public crate-rustic-logger-0.1.2 (c (n "rustic-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "0d5d4z33j1xabcznpmn1qiciqgf820bzcyc76zp8naa34vv8sa2h")))

