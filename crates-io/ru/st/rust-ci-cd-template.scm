(define-module (crates-io ru st rust-ci-cd-template) #:use-module (crates-io))

(define-public crate-rust-ci-cd-template-0.1.0 (c (n "rust-ci-cd-template") (v "0.1.0") (h "19nyqsaq2dc1gr45vfa7bcla6219vlnrcml8cxs5pjcl56xm8pqq") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.1 (c (n "rust-ci-cd-template") (v "0.1.1") (h "0vvby4wf1p4whzfxhpcg7r3d8fprsffq9hwdimkhwwr8j04al3m1") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.2 (c (n "rust-ci-cd-template") (v "0.1.2") (h "0gkkw8ps2ngb5ig8qjycz1fpmf0rxz7zb807nc2b9844xn4hixjr") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.3 (c (n "rust-ci-cd-template") (v "0.1.3") (h "1rl7k38lp5m02y3gy3h2prwh9sksndrd3f0a8isj1wc9m3ldrdjj") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.4 (c (n "rust-ci-cd-template") (v "0.1.4") (h "09z7kffmg0h8spflpgip93hmn16v1zd36xxqi6zg2n7kch12cvfb") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.5 (c (n "rust-ci-cd-template") (v "0.1.5") (h "1yiwfgya6pr990pn0lpq6i96ygnzb4z4cbd9s5h0hkidqzc36vnx") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.6 (c (n "rust-ci-cd-template") (v "0.1.6") (h "1j5lmlkgb8s0fxs8cad55qdz8ipnipxlbgy4v4nlwk3v9dz1hask") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.7 (c (n "rust-ci-cd-template") (v "0.1.7") (h "01rdp9aqx15zffyaxr94sll89g6ri60d59svj8a9xiaw61fxq2jc") (r "1.59.0")))

(define-public crate-rust-ci-cd-template-0.1.8 (c (n "rust-ci-cd-template") (v "0.1.8") (h "1pd9r97q0hzhmlfq437z90l3x6h4hl268l0knp4p6wz3jfsrb7p1") (r "1.59.0")))

