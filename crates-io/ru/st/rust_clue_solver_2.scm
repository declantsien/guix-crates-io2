(define-module (crates-io ru st rust_clue_solver_2) #:use-module (crates-io))

(define-public crate-rust_clue_solver_2-0.1.0 (c (n "rust_clue_solver_2") (v "0.1.0") (h "1hi64svxbcw2p0qcfg7izmjbpxmkhjnsx0n1wan9k63z0jgilrxz")))

(define-public crate-rust_clue_solver_2-0.1.1 (c (n "rust_clue_solver_2") (v "0.1.1") (h "1xzjj11nyg0915wdb1skm67pn0l0np32m54hjpcg1286mnh371ks")))

(define-public crate-rust_clue_solver_2-0.1.2 (c (n "rust_clue_solver_2") (v "0.1.2") (h "1dvaa63nkin06hsxzgz1k87i2yk7m6n4s5g5j8j3yv2fb0sxi04q")))

