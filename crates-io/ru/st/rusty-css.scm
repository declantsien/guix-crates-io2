(define-module (crates-io ru st rusty-css) #:use-module (crates-io))

(define-public crate-rusty-css-0.1.0 (c (n "rusty-css") (v "0.1.0") (d (list (d (n "bevy_reflect") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2") (d #t) (k 0)))) (h "0m53dac37v93ykp0raq8wk75jj3i7dvr8x7w1jdyw5didjjp9bvc")))

(define-public crate-rusty-css-0.1.3 (c (n "rusty-css") (v "0.1.3") (d (list (d (n "bevy_reflect") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.38") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window" "Document"))) (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1hzhy49ah7wj466nfphnwixm1dp6wnpjr3h6wxxd1jh5bdhjcibk")))

