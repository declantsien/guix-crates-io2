(define-module (crates-io ru st rustlex) #:use-module (crates-io))

(define-public crate-rustlex-0.1.0 (c (n "rustlex") (v "0.1.0") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "07rhg1l9wz0z4dn22px5xs1janc3skh3s771p0h20nca7pk1ag9c") (y #t)))

(define-public crate-rustlex-0.1.1 (c (n "rustlex") (v "0.1.1") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "1r4a56jcgzcjgfb06f8k9wq3pc179vhmxwa4awxc0l8gz2if17kz") (y #t)))

(define-public crate-rustlex-0.1.2 (c (n "rustlex") (v "0.1.2") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "0dgzr210cjz9r34knd39h3xd5lalcx8pgp9glbak5g5c7j53j3ml") (y #t)))

(define-public crate-rustlex-0.1.3 (c (n "rustlex") (v "0.1.3") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "13dxgmwbz4j098l7jzyi0ph1bsxx3dc0vaabl7n3i4p3bhx3ai7d") (y #t)))

(define-public crate-rustlex-0.1.4 (c (n "rustlex") (v "0.1.4") (h "1pmw91208c0vh693ngn4ppf0ansqcq6jgpx1czr26n8j2ni5idsv") (y #t)))

(define-public crate-rustlex-0.2.0 (c (n "rustlex") (v "0.2.0") (h "1y6lh14rl9ypk1214irvrmqaayd4s00yh86nglwacxnmxpkhxgnp") (y #t)))

(define-public crate-rustlex-0.2.1 (c (n "rustlex") (v "0.2.1") (h "0jvmnl0rcmizn20666rw2nb58k0fr3240b47s1l0skbvmnjy5msd") (y #t)))

(define-public crate-rustlex-0.2.2 (c (n "rustlex") (v "0.2.2") (h "11r2wkmiy00vgz0hsbkp3974mh2kc4a0zmy3x97q351dqfznd03r") (y #t)))

(define-public crate-rustlex-0.2.3 (c (n "rustlex") (v "0.2.3") (h "1gw53h1h2gfrc4cjaxa7bc8maps99c05lz9a2pk6q4rp6w0c3cd2") (y #t)))

(define-public crate-rustlex-0.2.4 (c (n "rustlex") (v "0.2.4") (h "144pniv6x1jp9c0g9w8jwq0y8c8bwh2jhcv2751ailbrq6aznvx4") (y #t)))

(define-public crate-rustlex-0.2.5 (c (n "rustlex") (v "0.2.5") (h "1qsadvdnh04v29xwcz4qgrhv7l13ijnmb36gccqqsavq8sbpy4wm") (y #t)))

(define-public crate-rustlex-0.2.6 (c (n "rustlex") (v "0.2.6") (h "0dsskrvyqz0nf9wzxiqiq2afj6qqp5lign5zbcl41p6jlgkiq5j6") (y #t)))

(define-public crate-rustlex-0.3.0 (c (n "rustlex") (v "0.3.0") (d (list (d (n "rustlex_codegen") (r "*") (d #t) (k 0)))) (h "1933xa770vdgj84xp1azig2c1vcdc0f3kmzp2j77bm1ansaq5vs4") (y #t)))

(define-public crate-rustlex-0.3.2 (c (n "rustlex") (v "0.3.2") (d (list (d (n "rustlex_codegen") (r "*") (d #t) (k 0)))) (h "0zpzfynz4scyvr40zn6imq0j4d9bbb0lvanzzkqi8k1hx56lc22m") (y #t)))

(define-public crate-rustlex-0.3.3 (c (n "rustlex") (v "0.3.3") (d (list (d (n "rustlex_codegen") (r "^0.3.3") (d #t) (k 0)))) (h "073dg8zf3i9y2bqzxcbkjlp6scf5cpv36p5hqn1sgmb6v1fak3wh") (y #t)))

(define-public crate-rustlex-0.3.4 (c (n "rustlex") (v "0.3.4") (d (list (d (n "rustlex_codegen") (r "^0.3.4") (d #t) (k 0)))) (h "0gspnd0vzabm12lhh8ki3mjp3hvgzn0062lfv3ij8gh3f1mgqm1s")))

(define-public crate-rustlex-0.4.0 (c (n "rustlex") (v "0.4.0") (d (list (d (n "rustlex_codegen") (r "^0.4.0") (d #t) (k 0)))) (h "0y6j9bvgy1x3x9pgg1kap851w69dbc1g2yz53j7h55mzwpmgk6nc")))

