(define-module (crates-io ru st rustls-cert-reloadable) #:use-module (crates-io))

(define-public crate-rustls-cert-reloadable-0.1.0 (c (n "rustls-cert-reloadable") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1j9cb978dhakaxxf7lm653k7jylrag9ncrvxj3wfv7xk76y0pfq9")))

