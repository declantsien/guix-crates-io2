(define-module (crates-io ru st rust-debugging-locks) #:use-module (crates-io))

(define-public crate-rust-debugging-locks-0.8.9 (c (n "rust-debugging-locks") (v "0.8.9") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "1163a3csg48fr0l0anqb4pmny0jvqjh4a0nxbk0qnfgp4pq2zcgy")))

