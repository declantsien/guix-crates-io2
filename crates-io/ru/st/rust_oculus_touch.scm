(define-module (crates-io ru st rust_oculus_touch) #:use-module (crates-io))

(define-public crate-rust_oculus_touch-0.0.3 (c (n "rust_oculus_touch") (v "0.0.3") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "14c90zccg67pzp8yi0mlz4qp37gan75z4ayl1bw261ac8yicgbm5")))

(define-public crate-rust_oculus_touch-0.0.4 (c (n "rust_oculus_touch") (v "0.0.4") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "15sc6npxylm7mcqwzmiigpifrp4q5fy3p8q9mjk9p586zx1sgln7")))

(define-public crate-rust_oculus_touch-0.0.5 (c (n "rust_oculus_touch") (v "0.0.5") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "06g643b2hfw42gc8hvdp64rdsc9f4z7fw18bcv75nc1xp9xx3xzf")))

(define-public crate-rust_oculus_touch-0.0.6 (c (n "rust_oculus_touch") (v "0.0.6") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0a0vg860z0h9qcvqcad3gz16qgnsjq5rnvvnhfcpxkrw4dql4wmw")))

(define-public crate-rust_oculus_touch-0.1.0 (c (n "rust_oculus_touch") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0jqq5x7wwa0p121gz6gw42n2bdqfprn208b25mn4h3sv3qs3b91c")))

