(define-module (crates-io ru st rust-openai-lib) #:use-module (crates-io))

(define-public crate-rust-openai-lib-0.1.0 (c (n "rust-openai-lib") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sxp9vymappw395in1rim6h9c90yqvv3hk7zhnswdzl89xvw0nyy")))

