(define-module (crates-io ru st rust-abci) #:use-module (crates-io))

(define-public crate-rust-abci-0.1.0 (c (n "rust-abci") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "grpc") (r "0.*") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "tls-api") (r "0.*") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)) (d (n "websocket") (r "^0.20.1") (d #t) (k 0)))) (h "0zid7960152lcd3vj9k8mxaipl2q5psl1zk67psj9y0vaqxvd2w2")))

