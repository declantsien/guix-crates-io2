(define-module (crates-io ru st rust_rts) #:use-module (crates-io))

(define-public crate-rust_rts-1.1.0 (c (n "rust_rts") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "eager") (r "^0.1") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)) (d (n "rts_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1.5") (d #t) (k 0)))) (h "02w7fbbccc56ayp95kam0aprpj5q3ilpyr1lc49ylq3zldjcrd0p") (y #t)))

