(define-module (crates-io ru st rust-codecov) #:use-module (crates-io))

(define-public crate-rust-codecov-0.1.0 (c (n "rust-codecov") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0fsc97izh7j1dslf1ih5ww51vmzsxb298gkp7dagv0c0n8bqilbg")))

