(define-module (crates-io ru st rust-lexer) #:use-module (crates-io))

(define-public crate-rust-lexer-0.1.0 (c (n "rust-lexer") (v "0.1.0") (h "042cny2rdqggaippcb9si04nj88gxpp86ipsmq702s3ap3cc0kyy")))

(define-public crate-rust-lexer-0.2.0 (c (n "rust-lexer") (v "0.2.0") (h "0vfcm2w6rdcvvvx3vm3ph7iddhp353a6bmdfp8v92n1qcf8d3qky")))

