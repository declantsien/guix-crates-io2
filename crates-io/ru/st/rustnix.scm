(define-module (crates-io ru st rustnix) #:use-module (crates-io))

(define-public crate-rustnix-0.0.1 (c (n "rustnix") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^2.0.5") (d #t) (k 2)) (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.135") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("env"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dbp84jdsjaz3vka4a60360lvkkbypqgyqy76qdrnckg9fyp9cs9")))

