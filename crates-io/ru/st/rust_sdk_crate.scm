(define-module (crates-io ru st rust_sdk_crate) #:use-module (crates-io))

(define-public crate-rust_sdk_crate-0.1.0 (c (n "rust_sdk_crate") (v "0.1.0") (h "1m6rwpf0sbzkp9brqi8anwf0alj1p8127wn4j6mhz0z2zpx2bsbx")))

(define-public crate-rust_sdk_crate-0.1.1 (c (n "rust_sdk_crate") (v "0.1.1") (h "09rskbmc9b01c7wy0fpr9h4mg00fkrmlqp4r9gkidgpmzqr3rmr7")))

