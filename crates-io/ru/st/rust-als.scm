(define-module (crates-io ru st rust-als) #:use-module (crates-io))

(define-public crate-rust-als-0.1.0 (c (n "rust-als") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("rayon" "inline-more" "ahash"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0wz2bn73vn1bcanpvndxpw92ap5k4lb7j4h754bfp7y1wrh2qn9i")))

