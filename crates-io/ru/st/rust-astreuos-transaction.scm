(define-module (crates-io ru st rust-astreuos-transaction) #:use-module (crates-io))

(define-public crate-rust-astreuos-transaction-0.2.0 (c (n "rust-astreuos-transaction") (v "0.2.0") (d (list (d (n "astro-format") (r "^0.2.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "0vw9v29pcgxyp3pqsaq06mcv8j46w7fj92fcf76y6bsif5g7m3ds") (y #t)))

