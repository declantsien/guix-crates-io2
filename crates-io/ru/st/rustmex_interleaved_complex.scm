(define-module (crates-io ru st rustmex_interleaved_complex) #:use-module (crates-io))

(define-public crate-rustmex_interleaved_complex-0.1.0 (c (n "rustmex_interleaved_complex") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.1") (d #t) (k 0)))) (h "05pfybjym744zsfp7gk356lpvfvw247rp0q4jl95krjg6b8nbrd7")))

(define-public crate-rustmex_interleaved_complex-0.2.0 (c (n "rustmex_interleaved_complex") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.2") (d #t) (k 0)))) (h "1kq47bpcvgy98w2dq8lr61sc4yl1lhx0593jvvizjm3hxaa7f3yd")))

