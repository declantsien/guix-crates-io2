(define-module (crates-io ru st rust-scrypt) #:use-module (crates-io))

(define-public crate-rust-scrypt-0.1.0 (c (n "rust-scrypt") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0d3jwx2vgnz0rkfvjchkbp66ynkc63jnxqnjgiimj040fgnphgrk") (f (quote (("dev" "clippy"))))))

(define-public crate-rust-scrypt-1.0.0 (c (n "rust-scrypt") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "00c86k4jc7mr4d3bw8lb7nhxd8kw4zhg99k6dk39wz35l6g6l45q") (f (quote (("dev" "clippy"))))))

(define-public crate-rust-scrypt-1.1.0 (c (n "rust-scrypt") (v "1.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0hn7arq3jzyyx2v8iiq6r48d2mp2f16schwp1zmf8x7rva6qr463") (f (quote (("dev" "clippy"))))))

(define-public crate-rust-scrypt-1.2.0 (c (n "rust-scrypt") (v "1.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "0g6wj52cnihx8sfpn1kdp4bfly91g4da5yr2nbnm485y5693ppz0") (f (quote (("dev" "clippy"))))))

(define-public crate-rust-scrypt-1.3.0 (c (n "rust-scrypt") (v "1.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "0as65v8r4zyix7q1nxl5h6g3fjhgmkl8xiprxj7mg5iq71ji5qn2") (f (quote (("dev" "clippy"))))))

