(define-module (crates-io ru st rust_md5_updated) #:use-module (crates-io))

(define-public crate-rust_md5_updated-0.2.0 (c (n "rust_md5_updated") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0y0scfdvw2f6vvhfrqpnakpbb6i4z2fxcq1iaygm2ldsa97kgsy7")))

