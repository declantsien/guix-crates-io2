(define-module (crates-io ru st rust-aws-sns) #:use-module (crates-io))

(define-public crate-rust-aws-sns-0.1.0 (c (n "rust-aws-sns") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 0)) (d (n "rusoto_sns") (r "^0.46.0") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l59l0rmgwgwwgl7d267hvhdn6l1n2z8hfirr861d83hjiybglhm")))

(define-public crate-rust-aws-sns-0.2.0 (c (n "rust-aws-sns") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 0)) (d (n "rusoto_sns") (r "^0.46.0") (d #t) (k 0)) (d (n "rust-aws-sns") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1x5vnxs9h8mprq67zb6x4b3ns9f13941q73jy66phygjxpmbaqk8")))

(define-public crate-rust-aws-sns-0.3.0 (c (n "rust-aws-sns") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 0)) (d (n "rusoto_sns") (r "^0.46.0") (d #t) (k 0)) (d (n "rust-aws-sns") (r "^0.2.0") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "124mshwr2s0sna3fq5g1j5rw6m0gkdx5ifq9qmwyjk8jy2804wdn")))

(define-public crate-rust-aws-sns-0.4.0 (c (n "rust-aws-sns") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 0)) (d (n "rusoto_sns") (r "^0.46.0") (d #t) (k 0)) (d (n "rust-aws-sns") (r "^0.2.0") (d #t) (k 2)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0gzcnd9pzfqbr1i9alxivygx598z3wi4i9395yddh9131k714asd")))

