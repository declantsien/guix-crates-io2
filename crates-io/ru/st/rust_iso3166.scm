(define-module (crates-io ru st rust_iso3166) #:use-module (crates-io))

(define-public crate-rust_iso3166-0.1.0 (c (n "rust_iso3166") (v "0.1.0") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ba8afj852w5yh71vwwbs3gjmjmqn68xr0l3inc5ihncrpd6cwyz") (y #t)))

(define-public crate-rust_iso3166-0.1.1 (c (n "rust_iso3166") (v "0.1.1") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0x88jlay31g241y3bjhkhyh1h3f94rah7k8i4gz3k43dad05pxc8") (y #t)))

(define-public crate-rust_iso3166-0.1.2 (c (n "rust_iso3166") (v "0.1.2") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0arvqpylq27n0s3fzgvfi0swgizics2rj33jy267wrrsj12rk1a6") (y #t)))

(define-public crate-rust_iso3166-0.1.3 (c (n "rust_iso3166") (v "0.1.3") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0g3cckrqmm7zcn1fc0hql5a5rnbz7q32bm63v7zpisma4ffc5jcs") (y #t)))

(define-public crate-rust_iso3166-0.1.4 (c (n "rust_iso3166") (v "0.1.4") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "11ja3jmsf4dz2k0kppgc8saf19jlkbapsdpk6mclk5bfrwij165p") (y #t)))

(define-public crate-rust_iso3166-0.1.5 (c (n "rust_iso3166") (v "0.1.5") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "00rmwd78r25z6r3nbw7cymyqkldsqb19pmihkdri444sl2wl6dz0")))

(define-public crate-rust_iso3166-0.1.6 (c (n "rust_iso3166") (v "0.1.6") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "03b23fr6ry0dmkdh6f3ws2id3drvdy8cd9nwf27q4y1z62fd8f3x")))

(define-public crate-rust_iso3166-0.1.7 (c (n "rust_iso3166") (v "0.1.7") (d (list (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "11ihv4rqysm8bc6x3fh51n2hm1dyip7zl1dyrfg17skr7cfx8n1f")))

(define-public crate-rust_iso3166-0.1.8 (c (n "rust_iso3166") (v "0.1.8") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "01xqff07hbc98q19fwwnaynp7bwajcknkzrc6gha504v018jzmxj")))

(define-public crate-rust_iso3166-0.1.9 (c (n "rust_iso3166") (v "0.1.9") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "19m6bv01cj3ga7y04y4zaz9l098bv8h57ldb37277nmnag2lr82a")))

(define-public crate-rust_iso3166-0.1.10 (c (n "rust_iso3166") (v "0.1.10") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1y0f5n236w5ikkq3xkm2q9zvl8wfk36gbv0px7qz7lp58qzz46bf")))

(define-public crate-rust_iso3166-0.1.11 (c (n "rust_iso3166") (v "0.1.11") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0wcz7fqbqp0zkdklzzsp939jzwll7bs9kl1m9lv6idr6ywvg8inc")))

(define-public crate-rust_iso3166-0.1.12 (c (n "rust_iso3166") (v "0.1.12") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0jq5yck4x30dlmd9ih6qfzpd5bjhmlxmax4cv2l4h228jixnfxp6")))

(define-public crate-rust_iso3166-0.1.13 (c (n "rust_iso3166") (v "0.1.13") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "140a03g5faki3lq1hzvvkjmza6sysnq6qdhsfsj8rvqpnpm2ccfx")))

