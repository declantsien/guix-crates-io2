(define-module (crates-io ru st rust-cli-curry) #:use-module (crates-io))

(define-public crate-rust-cli-curry-0.1.0 (c (n "rust-cli-curry") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "1kfn9v6pz1i20paay45jj7rlnf41w3wwmm8hg7x2spc368kppm8l")))

