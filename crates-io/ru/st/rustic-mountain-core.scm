(define-module (crates-io ru st rustic-mountain-core) #:use-module (crates-io))

(define-public crate-rustic-mountain-core-0.1.0 (c (n "rustic-mountain-core") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.10") (f (quote ("js"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0dhfky9gg92kcgw7byr9hh89l3r2lg8my3dqvqnljbdjyr5lqd1c")))

