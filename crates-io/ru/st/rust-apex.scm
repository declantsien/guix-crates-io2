(define-module (crates-io ru st rust-apex) #:use-module (crates-io))

(define-public crate-rust-apex-0.1.0 (c (n "rust-apex") (v "0.1.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8.4") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "18j88a36gk2178mfs8lz4ixgi69sjbarmf4ypg3n3h1hc435gc2r")))

(define-public crate-rust-apex-0.2.0 (c (n "rust-apex") (v "0.2.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0dm0wv5lnail4x5kbdn0r65i6n0lkiw5d7hf9mmykc00snbqvcij")))

(define-public crate-rust-apex-0.3.0 (c (n "rust-apex") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1srd0n7zpici99spn0y9ncqgkw90j4dvb7i0fxl3yr031f5lhx38")))

(define-public crate-rust-apex-0.3.1 (c (n "rust-apex") (v "0.3.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00m1xrnrwkvn91q1mvim70b7dlxna17yr7npmszkim9wdw9lcirj")))

(define-public crate-rust-apex-0.3.2 (c (n "rust-apex") (v "0.3.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cjz45q2ppwrwifik5h2lfyg6w8b71chl4g53cmy4g54q53w3vcz")))

(define-public crate-rust-apex-0.3.3 (c (n "rust-apex") (v "0.3.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ymf29zvw8rplr7wxvsfyr5b90zslad0wsg5vw2y48pd92jhfqdk")))

