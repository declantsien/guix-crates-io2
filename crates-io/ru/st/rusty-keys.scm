(define-module (crates-io ru st rusty-keys) #:use-module (crates-io))

(define-public crate-rusty-keys-0.0.1 (c (n "rusty-keys") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)) (d (n "uinput-sys") (r "^0.1") (d #t) (k 0)))) (h "0fa2r3ba6hnyywhr5vf1fwglm0s5ymk4kikbks984nq0nahx2vgj") (f (quote (("udev" "libudev") ("default" "udev"))))))

(define-public crate-rusty-keys-0.0.2 (c (n "rusty-keys") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)) (d (n "uinput-sys") (r "^0.1") (d #t) (k 0)))) (h "014q04id12qd9gd1plj2hm6z2y0i59qhw8yjz3ak15662zy4gjsk") (f (quote (("udev" "libudev") ("default" "udev"))))))

