(define-module (crates-io ru st rusty_brainfck) #:use-module (crates-io))

(define-public crate-rusty_brainfck-0.1.0 (c (n "rusty_brainfck") (v "0.1.0") (h "0lx6qr61jjsnbfd7avs4b5nijghzq4qbzwryyx78kb16r1mjx6sd") (f (quote (("tinix" "alloc") ("no_alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-rusty_brainfck-0.1.1 (c (n "rusty_brainfck") (v "0.1.1") (h "0r14g3j231h6w1hphckm6pdzfklm8ic2z9ac8hdhak26ypb53897") (f (quote (("tinix" "alloc") ("no_alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-rusty_brainfck-0.1.2 (c (n "rusty_brainfck") (v "0.1.2") (h "0p9fgr9apcb43v2hkybscn4hs8gf5paccr2g7dajykbv12w33vh0") (f (quote (("tinix" "alloc" "no_std") ("no_std") ("no_alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-rusty_brainfck-0.1.3 (c (n "rusty_brainfck") (v "0.1.3") (h "0k3nhnzfckq6djwdjzxr602cpcpal4sg5lwqwfvlv8gr4fcy6by1") (f (quote (("tinix" "alloc" "no_std") ("no_std") ("no_alloc") ("default" "alloc") ("alloc"))))))

