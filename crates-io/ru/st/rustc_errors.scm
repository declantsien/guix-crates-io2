(define-module (crates-io ru st rustc_errors) #:use-module (crates-io))

(define-public crate-rustc_errors-0.1.0 (c (n "rustc_errors") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bym69cwxzn191zikzx6gq4r3dlsp7vyl936zwxv1zm4xciq4hgg")))

(define-public crate-rustc_errors-0.0.0 (c (n "rustc_errors") (v "0.0.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a5rgp257wql8y6xs0cmi21prngci0lbk5vnn4ddnrk0f5cly6bs")))

(define-public crate-rustc_errors-0.0.1 (c (n "rustc_errors") (v "0.0.1") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zndf7gmk6dvxwrcipy3hs6pfwnhy022azx0wvnvc8v42s3bmw28")))

(define-public crate-rustc_errors-0.0.2 (c (n "rustc_errors") (v "0.0.2") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "186mca2jvmbjk5ffn3bnfiqsbfxzx473z9nwp1184fkqd04sfv7k")))

(define-public crate-rustc_errors-0.1.1 (c (n "rustc_errors") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "191c1cqbvw28w0kwc4k4z6f4rb8xhdzx76vg4r5bxgsajlqwbkf2")))

(define-public crate-rustc_errors-0.1.2 (c (n "rustc_errors") (v "0.1.2") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "synchapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0g14n42w9qai4lf8f0w2gqwfwimln234yvcy290jqvc184c9na80")))

