(define-module (crates-io ru st rustfire) #:use-module (crates-io))

(define-public crate-rustfire-0.1.0 (c (n "rustfire") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "libflate") (r "^1.2.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)))) (h "0zccslz6ddb8wz6yi8dpzriwszwab00sb0ghpl59726aqbal5gvn")))

(define-public crate-rustfire-0.1.1 (c (n "rustfire") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "libflate") (r "^1.2.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)))) (h "0jbghysydw5sxj6wdz4nn6w1x15a6z51kb3nc5cwfm2bqxahy691")))

(define-public crate-rustfire-0.2.0 (c (n "rustfire") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "libflate") (r "^1.2.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)))) (h "0g329pwdy583fwsrnn6vaa4x60dz5migglhjbn886mg4klsgxha9")))

