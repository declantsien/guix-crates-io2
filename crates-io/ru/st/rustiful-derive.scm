(define-module (crates-io ru st rustiful-derive) #:use-module (crates-io))

(define-public crate-rustiful-derive-0.1.0 (c (n "rustiful-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.10") (d #t) (k 0)) (d (n "clippy") (r "^0.0.123") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rustiful") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1jswql8h3g50rz79fwzlf233c8hm3mr8zibvijf6qz26b8icmz7q") (f (quote (("uuid") ("dev" "clippy") ("default"))))))

