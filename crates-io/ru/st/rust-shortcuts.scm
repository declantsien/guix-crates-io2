(define-module (crates-io ru st rust-shortcuts) #:use-module (crates-io))

(define-public crate-rust-shortcuts-0.1.1 (c (n "rust-shortcuts") (v "0.1.1") (h "1wjrcvkk7gw13nabg4pzrhr04z017i1gsf5g5yzwf27b2dkz72gd")))

(define-public crate-rust-shortcuts-0.1.2 (c (n "rust-shortcuts") (v "0.1.2") (h "03appmrdq4j5mb9k6lsxfrjvsniksa5mwkmi1hkkgvx11xk581ma")))

(define-public crate-rust-shortcuts-0.1.3 (c (n "rust-shortcuts") (v "0.1.3") (h "0iv947r7warhfmkh8q8v7y6b010m65n9mkb90l8dyabr2k1ybd2f")))

