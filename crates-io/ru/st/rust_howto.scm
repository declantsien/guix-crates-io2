(define-module (crates-io ru st rust_howto) #:use-module (crates-io))

(define-public crate-rust_howto-0.1.0 (c (n "rust_howto") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.19.0") (d #t) (k 0)))) (h "12b285zyl3jrkhlpq3axpx5xzhh1f7f0c0nd3n1jj89iv698y1lg") (r "1.75")))

(define-public crate-rust_howto-0.1.1 (c (n "rust_howto") (v "0.1.1") (d (list (d (n "cargo_toml") (r "^0.19.0") (d #t) (k 0)))) (h "04kyqwcxbgly4r51j5hf880m86qicdfmx29kyb4s7gh7sc5abwpb") (r "1.75")))

