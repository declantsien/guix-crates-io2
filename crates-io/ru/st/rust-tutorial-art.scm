(define-module (crates-io ru st rust-tutorial-art) #:use-module (crates-io))

(define-public crate-rust-tutorial-art-0.1.0 (c (n "rust-tutorial-art") (v "0.1.0") (h "0lsr2fdyncr2d11k032mrzfglzfjimvqik1prk9lxzk492ms8ysa") (y #t)))

(define-public crate-rust-tutorial-art-0.1.1 (c (n "rust-tutorial-art") (v "0.1.1") (h "1lfp7c000lnggmc62613agb8jgq9vfr6s9hh0nhxw37j9lfbiw6m")))

