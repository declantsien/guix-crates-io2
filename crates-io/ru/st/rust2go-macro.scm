(define-module (crates-io ru st rust2go-macro) #:use-module (crates-io))

(define-public crate-rust2go-macro-0.0.1 (c (n "rust2go-macro") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1pmnqzpyxx595ay2ndyqy1zrmly1chddr5h48lyi0s4fdky14rgl")))

(define-public crate-rust2go-macro-0.0.2 (c (n "rust2go-macro") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vz7yhgr8dvq9qbvcgsgbis7136mvirasp4w7qa7ik1i5m7yjf3p")))

(define-public crate-rust2go-macro-0.1.0 (c (n "rust2go-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z0lvs0ni000fwvvhfaap2madzlvrdzvfqz3zrq3knjgqi5fv0lz")))

(define-public crate-rust2go-macro-0.2.0 (c (n "rust2go-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0cw7ga1rky1fw6kk6yx1mzpaw6bwnjdq8jfjy7c5f3lrgf3rhzvi")))

(define-public crate-rust2go-macro-0.3.0 (c (n "rust2go-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0yvmaxag56lbwdb6iw1x11kx5prp12rhi7iz5mc5wivp272pz92n")))

(define-public crate-rust2go-macro-0.3.1 (c (n "rust2go-macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kkbgwyg4m2cx5bjcg2xhkicqd121qqi5isdnkg4a75v11n1swyb")))

(define-public crate-rust2go-macro-0.3.2 (c (n "rust2go-macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lhjya6gbvrhkx3w7avq4gkc7wwrg3blaf6d6ivm3smxsla4w76h")))

(define-public crate-rust2go-macro-0.3.3 (c (n "rust2go-macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "030i07jaj2812gb496jj3zcsfb9p98726ic3shj73zy9xl1nq8z2")))

(define-public crate-rust2go-macro-0.3.4 (c (n "rust2go-macro") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rust2go-common") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0j60b035wnamw3mbgfj1x56r7mn98xg0hg8c97yfpv6zfb8mbsmc")))

