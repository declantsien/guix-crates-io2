(define-module (crates-io ru st rust-libindy-wrapper) #:use-module (crates-io))

(define-public crate-rust-libindy-wrapper-0.2.11 (c (n "rust-libindy-wrapper") (v "0.2.11") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 2)))) (h "0ybc3xky61xz0244lpqqns5gcqamnk06j4p6qcyapzari91k9553") (y #t)))

(define-public crate-rust-libindy-wrapper-0.2.12 (c (n "rust-libindy-wrapper") (v "0.2.12") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 2)))) (h "10gpdzzcx0zw53g7dbzim00nvib31n986g9lakryzmfg452akc0q") (y #t)))

(define-public crate-rust-libindy-wrapper-0.2.13 (c (n "rust-libindy-wrapper") (v "0.2.13") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 2)))) (h "03a1s2g1nzwj39549bflsk6nbfj6in1clgihs1sq15kqdyf7wnna")))

