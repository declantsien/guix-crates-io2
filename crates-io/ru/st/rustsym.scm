(define-module (crates-io ru st rustsym) #:use-module (crates-io))

(define-public crate-rustsym-0.1.0 (c (n "rustsym") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "07qfzg4v3z8s7aa1jcm4wwirm2hz5zb23bq21wvdipd7smb1pyjx")))

(define-public crate-rustsym-0.2.0 (c (n "rustsym") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0p4badcw8g6cvk6q36ah7cj21i93r359rvli20pmldxrsl0y2f9h") (y #t)))

(define-public crate-rustsym-0.2.2 (c (n "rustsym") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0p67diqrd8rphfrjjg3c8b1w31xzqalkwfy6ryh5gfh5315c19ck")))

(define-public crate-rustsym-0.3.0 (c (n "rustsym") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0blxd6ncx2c0bg5c8sbkpgply165yj08sqwb2a080bya0asshmx6")))

(define-public crate-rustsym-0.3.1 (c (n "rustsym") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.4") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1yc7spqspnz8fpg4vz70kild86xz3x7v15b5xlk8ksm23jjibwwz")))

(define-public crate-rustsym-0.3.2 (c (n "rustsym") (v "0.3.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.4") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "11aaiy7281ngmlvaz3599q0gfxi5lz9fiwgg11aw9xnhwhvky8vq")))

