(define-module (crates-io ru st rustc-holidays) #:use-module (crates-io))

(define-public crate-rustc-holidays-0.1.0 (c (n "rustc-holidays") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("std"))) (k 1)) (d (n "holiday") (r "^0.1.2") (d #t) (k 1)))) (h "1r2kzjzd24kc8laqw8va0qkwragffscg8798dzba0wdy0wb8kgpq") (f (quote (("slow_down") ("default" "slow_down"))))))

