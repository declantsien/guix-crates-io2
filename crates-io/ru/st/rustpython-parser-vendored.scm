(define-module (crates-io ru st rustpython-parser-vendored) #:use-module (crates-io))

(define-public crate-rustpython-parser-vendored-0.3.0 (c (n "rustpython-parser-vendored") (v "0.3.0") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (k 0)))) (h "06xms9f2vny78bb9d9g4yqalx86rbqacg8gkff0f6pzysxi31bhp") (f (quote (("location") ("default"))))))

(define-public crate-rustpython-parser-vendored-0.3.1 (c (n "rustpython-parser-vendored") (v "0.3.1") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (k 0)))) (h "13qwhrnjk8kfm26rfv6nasq7hqvwp3bsi5fb57hlqplvyfc37r7n") (f (quote (("location") ("default")))) (r "1.72.1")))

