(define-module (crates-io ru st rustacean-review) #:use-module (crates-io))

(define-public crate-rustacean-review-0.1.0 (c (n "rustacean-review") (v "0.1.0") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)))) (h "1ndgcbryz03ralzvgkkfp45v2m75iq17lnx08icj6cz05cxkvaxi") (y #t)))

(define-public crate-rustacean-review-1.0.0 (c (n "rustacean-review") (v "1.0.0") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0a9niapdydzacbdkwwssm9bzr1wmss4dwr21863cgs7w4db92fd4") (y #t)))

(define-public crate-rustacean-review-1.0.1 (c (n "rustacean-review") (v "1.0.1") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1g5a18l440gxss3czl0ksswy0d8wqbwr9agvh4qhxz4ddkjqzfp1") (y #t)))

(define-public crate-rustacean-review-2.0.0 (c (n "rustacean-review") (v "2.0.0") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0d9hiwi9ilfcwxp2blwr4k688f1gws10fw6hn3rsz0hg56507s5x") (y #t)))

(define-public crate-rustacean-review-2.1.0 (c (n "rustacean-review") (v "2.1.0") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1cagazlx77h3vaw8m1bcz8k6wmikzv3l6cnamhbyz2s7gi3wnqc0") (y #t)))

(define-public crate-rustacean-review-2.2.0 (c (n "rustacean-review") (v "2.2.0") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1n4df9dvbj9383w51vn40dzyw1gkl6v7zgz5772ybmy4xmr0pmcg") (y #t)))

(define-public crate-rustacean-review-10.10.10 (c (n "rustacean-review") (v "10.10.10") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0yfpar9dxl1npxmd8ardvpy8q31jrbfl6pp426mwzc0bv54bnfwc") (y #t)))

