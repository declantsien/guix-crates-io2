(define-module (crates-io ru st rust-libretro-sys-proc) #:use-module (crates-io))

(define-public crate-rust-libretro-sys-proc-0.2.3 (c (n "rust-libretro-sys-proc") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0lwyac3qz1m05bdq6yjxwshgxsj3fgmwhiifmj2ckyk57j399kyi")))

(define-public crate-rust-libretro-sys-proc-0.3.0 (c (n "rust-libretro-sys-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "1v5lc6bl8s1jdxq0x7qzlf3qrv6kgbh4wsf7kfy10hq9ykr1zhmb")))

(define-public crate-rust-libretro-sys-proc-0.3.1 (c (n "rust-libretro-sys-proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "00l6cmac1b0cd5g4r9i8bxn8clf12rhwlcwzrds8jy979sn8fb7x")))

(define-public crate-rust-libretro-sys-proc-0.3.2 (c (n "rust-libretro-sys-proc") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0afrq9zwhh32v0fs6w75jwiv478282m2f9r99kbpx0i3cpj0mmys")))

