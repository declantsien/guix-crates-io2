(define-module (crates-io ru st rustlangbr) #:use-module (crates-io))

(define-public crate-rustlangbr-0.1.0 (c (n "rustlangbr") (v "0.1.0") (h "01gzkinyr5fckjbhjihji6z49sz32msymvmbdr8b32bgpdis1v4w")))

(define-public crate-rustlangbr-0.1.1 (c (n "rustlangbr") (v "0.1.1") (h "0z2gbslr5gvclcy6anbw9v6g62193mdlzibv9ndkkaqd1x33n461")))

(define-public crate-rustlangbr-0.1.2 (c (n "rustlangbr") (v "0.1.2") (h "0pv80v26izajmd42fakr10pvq14zysssgzd4dyvyja94l31msznd")))

(define-public crate-rustlangbr-0.1.3 (c (n "rustlangbr") (v "0.1.3") (h "1lg095vjm3icam96f0whaxa8d89j14095xzgyhvbicbfy1bxrs9v") (y #t)))

(define-public crate-rustlangbr-0.1.5 (c (n "rustlangbr") (v "0.1.5") (h "167815g8daij7cqfg53pijgz2zac4snyifpmz9lbasblda6qscnb")))

