(define-module (crates-io ru st rustc-hash) #:use-module (crates-io))

(define-public crate-rustc-hash-1.0.0 (c (n "rustc-hash") (v "1.0.0") (h "114bf72466bl63i5hh8fgqfnhihs0w1m9c9jz505095agfixnvg0")))

(define-public crate-rustc-hash-1.0.1 (c (n "rustc-hash") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "1f4cnbcmz2c3zjidqszc9c4fip37ch4xl74nkkp9dw291j5zqh3m")))

(define-public crate-rustc-hash-1.1.0 (c (n "rustc-hash") (v "1.1.0") (h "1qkc5khrmv5pqi5l5ca9p5nl5hs742cagrndhbrlk3dhlrx3zm08") (f (quote (("std") ("default" "std"))))))

(define-public crate-rustc-hash-1.2.0 (c (n "rustc-hash") (v "1.2.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1gr9aaljclg1w9826cy7ad64wh3qw2v9321vpjvan2cnzqvysl1l") (f (quote (("std") ("default" "std")))) (y #t) (s 2) (e (quote (("rand" "dep:rand" "std"))))))

