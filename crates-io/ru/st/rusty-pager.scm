(define-module (crates-io ru st rusty-pager) #:use-module (crates-io))

(define-public crate-rusty-pager-0.1.0 (c (n "rusty-pager") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "101zvm8jvknnfprmyd1smwwcvnf9lsh6hkg2b2y8dj5xpvgn7n5v")))

(define-public crate-rusty-pager-0.1.1 (c (n "rusty-pager") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "177p89mm1y3mkx2ln679k2l7dm79wm9fj6dl746y07rladv729v7")))

