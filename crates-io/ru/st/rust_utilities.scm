(define-module (crates-io ru st rust_utilities) #:use-module (crates-io))

(define-public crate-rust_utilities-0.1.0 (c (n "rust_utilities") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "02dwfy63cvbjkq2a0ynr9ga6xinjq8a1ii51j0f24y8047z9b6in") (f (quote (("sha" "sha-1" "sha2" "hex") ("full" "crypto" "sha") ("default" "full") ("crypto")))) (y #t)))

(define-public crate-rust_utilities-0.2.0 (c (n "rust_utilities") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "19wxvqxalv9gji9p3nnnvv5q92nc6nsb6z2dsav9fc7djrjcf945") (f (quote (("sha" "sha-1" "sha2" "hex" "crypto") ("full" "crypto" "sha" "jsonwebtoken") ("default" "full") ("crypto")))) (y #t) (s 2) (e (quote (("jsonwebtoken" "dep:jsonwebtoken" "chrono" "crypto"))))))

