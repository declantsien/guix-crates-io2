(define-module (crates-io ru st rust-main-abel-test) #:use-module (crates-io))

(define-public crate-rust-main-abel-test-0.1.0 (c (n "rust-main-abel-test") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "060mbhhxqcm7fgsgvln69gabgih2clbw3jc8s5slsgnjcx9zwqgs")))

