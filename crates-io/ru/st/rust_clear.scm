(define-module (crates-io ru st rust_clear) #:use-module (crates-io))

(define-public crate-rust_clear-0.1.0 (c (n "rust_clear") (v "0.1.0") (d (list (d (n "crossterm") (r ">=0.18.2, <0.19.0") (d #t) (k 0)) (d (n "term_size") (r ">=0.3.2, <0.4.0") (d #t) (k 0)))) (h "0rz2d8m79v0a4iv3h6zs61hvac4my4cbl9rhbji6pwc54wj8bjn3")))

