(define-module (crates-io ru st rust_lisp) #:use-module (crates-io))

(define-public crate-rust_lisp-0.1.0 (c (n "rust_lisp") (v "0.1.0") (h "0p8zsry9ylc8blyy3a7k7xkadclms89p7w62lwlw0crk9nvii3sa")))

(define-public crate-rust_lisp-0.2.0 (c (n "rust_lisp") (v "0.2.0") (h "1qlwa309mdn06vnhik7ycp1907h3qs0bw1yy94w207h4lz0kx7z7")))

(define-public crate-rust_lisp-0.3.0 (c (n "rust_lisp") (v "0.3.0") (h "1ip2dyv6v5wdzwwn2na4lgqr5iznzgscczzx1wcf6hgbpf5663xm")))

(define-public crate-rust_lisp-0.4.0 (c (n "rust_lisp") (v "0.4.0") (h "13ycc7j4cv9rrmlnqbdlp6sjp3szmfk7p7mw6i8gamldmhimrh4h")))

(define-public crate-rust_lisp-0.5.0 (c (n "rust_lisp") (v "0.5.0") (h "1wim80w0r1zikhjaf7d8pzgq3ma2gjfrhwsl5rc6csykbw7pswgy")))

(define-public crate-rust_lisp-0.6.0 (c (n "rust_lisp") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "00b8f8kg2pxykpjx2g1dvm21sywc18zxnwg582y9xag0lc1c7kdk") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.7.0 (c (n "rust_lisp") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "00v2mgbp7448nrh95yb69d90cimd7jh6brl82643ci29ja077sa1") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.7.1 (c (n "rust_lisp") (v "0.7.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1jhhv33w3drx00axxagxdm1f0cz0a2pz9rgyps58vcl4whc7cpwz") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.7.2 (c (n "rust_lisp") (v "0.7.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1mwdbwdxhx6ly153xf2xi4yz296ddbm7vvssl323kdqddswvfn7x") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.8.0 (c (n "rust_lisp") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "173hancg3zak34cx2rkj9hb51s089wxn9rbswknmzwryqfyzifqv") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.9.0 (c (n "rust_lisp") (v "0.9.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0mw35fnc62bjhpfnbbpasfhgl6sgmblz5a0xbwjbmcj9x6bh5pmc") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.10.0 (c (n "rust_lisp") (v "0.10.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0az1x36xg5s8fpgpcbg8wxy7dbxy36796zf8kwxass4gqyil6k1a") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.11.0 (c (n "rust_lisp") (v "0.11.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1gqq00sn2d8qcnkw3896mh41lnsnbpxbn6lab6418lkncrp34vgi") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.12.0 (c (n "rust_lisp") (v "0.12.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "15vlharlv9fgidj90fjawy1zb5ahhqicr6v8gnkw7jlalf6kqb4c") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.13.0 (c (n "rust_lisp") (v "0.13.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "11i8hg362q1s48641960csdbmswa1c4yiyzblfnjwvy3ag80wxgn") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.13.1 (c (n "rust_lisp") (v "0.13.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0psyl8dyk38pfjb84lwxjq0fsik508lb5jba0k6gy1ap0zv035jz") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.14.0 (c (n "rust_lisp") (v "0.14.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "13dk3x3qrq63zjwlhmdwd8x51pgv86lfhl7k9f91cm26ddzvlmr4") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.15.0 (c (n "rust_lisp") (v "0.15.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1pkq9dy9hcsm32v98apknq54n8hxf7hyxcgay9id44dcibqciax4") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.15.1 (c (n "rust_lisp") (v "0.15.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "07b0c8846vp9k8aa6wpv914sp2hb5ahz143x3392psi71qkk0wcf") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.16.0 (c (n "rust_lisp") (v "0.16.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17gh0q8v9mv8wg9qblngj9a4wri194pgdxb6k8q2bdbxpfpya0sy") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.16.1 (c (n "rust_lisp") (v "0.16.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1vz6wlw6dxph4p02f2j7r1b5jngkxzk8nab9z2q7a5m8749bbl9j") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.17.0 (c (n "rust_lisp") (v "0.17.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1cpfhax7qzfvf9xljbm5bbfxz3d70s0p3gz2bv1qq4ahg3shq1bk") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.17.1 (c (n "rust_lisp") (v "0.17.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1rzrlhfm5v040gzzqd298r8zrnv2038r7s0rirh377j07j0mkwcm") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

(define-public crate-rust_lisp-0.18.0 (c (n "rust_lisp") (v "0.18.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1vh8w2d1s21xj5pzaavk4fgbfm7l83jsyp68bd31a2mnrr777igm") (f (quote (("i8") ("i64") ("i16") ("i128") ("f64") ("bigint" "num-bigint" "num-traits"))))))

