(define-module (crates-io ru st rust-eratos) #:use-module (crates-io))

(define-public crate-rust-eratos-0.1.0 (c (n "rust-eratos") (v "0.1.0") (h "19nafdkz11zbz1fmlxjhaksyn5pwg4lm2kpflp87njf06h8d40mm") (y #t)))

(define-public crate-rust-eratos-0.1.1 (c (n "rust-eratos") (v "0.1.1") (h "10j36k8ym9s2hy58vzj8rmqsyicxvba423x1dvnblrldv04l6grl")))

