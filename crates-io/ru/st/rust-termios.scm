(define-module (crates-io ru st rust-termios) #:use-module (crates-io))

(define-public crate-rust-termios-0.0.1 (c (n "rust-termios") (v "0.0.1") (d (list (d (n "termios-sys") (r "*") (d #t) (k 0)))) (h "1vsy6cyp3wpbznlzncq8821akr9mjbqjj5zb7yl5ws1jxas2wrv1")))

(define-public crate-rust-termios-0.0.2 (c (n "rust-termios") (v "0.0.2") (d (list (d (n "termios-sys") (r "*") (d #t) (k 0)))) (h "14pbwc69lgz532ib2z5rapz9q49dgsir773nfzr8ijmqfrla3kyw")))

