(define-module (crates-io ru st rustmap) #:use-module (crates-io))

(define-public crate-rustmap-0.1.0 (c (n "rustmap") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "12vrqwzrhpvylc9agkna78jh07x3vxmhcm0yl761w1aih61mlanf")))

(define-public crate-rustmap-0.1.1 (c (n "rustmap") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0c1g7wfcvdy39npg9w18whn6gd9sv53k1dribl15pxqyljls79qw")))

(define-public crate-rustmap-0.1.2 (c (n "rustmap") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "07wg2g3rrnhngcqpqb95078jkl9v8v40dg38r7cwwzlf7jrn5vr0")))

