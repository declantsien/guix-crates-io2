(define-module (crates-io ru st rustydns) #:use-module (crates-io))

(define-public crate-rustydns-0.1.1 (c (n "rustydns") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "mini-moka") (r "^0.10.3") (f (quote ("sync"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread" "net" "io-util"))) (d #t) (k 0)))) (h "10jbmk007k00gpzaff36nzj8ykg2s809pp0k17x4z9nfn461vn1y")))

(define-public crate-rustydns-0.1.2 (c (n "rustydns") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "mini-moka") (r "^0.10.3") (f (quote ("sync"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread" "net" "io-util"))) (d #t) (k 0)))) (h "04d40iw91krplwvp11h3rb6r6b13g0zal65vm5wl5yd1xwvmhq7s")))

