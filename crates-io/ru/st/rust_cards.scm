(define-module (crates-io ru st rust_cards) #:use-module (crates-io))

(define-public crate-rust_cards-0.1.0 (c (n "rust_cards") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "187zjig99r9r69mqj16nzkxqyd9j3x74z3k155yjypsip88i7g7v")))

(define-public crate-rust_cards-0.1.1 (c (n "rust_cards") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "180hbprjhsrd414aapzvbsn68d3qy4ihvyc3sx2p87wj790209vk")))

(define-public crate-rust_cards-0.1.2 (c (n "rust_cards") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1352gmam4f5pgqcl3jypi76w9nk5gynhbl61ixkff6sna3rf9pvf")))

