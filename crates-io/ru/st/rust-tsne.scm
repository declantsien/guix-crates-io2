(define-module (crates-io ru st rust-tsne) #:use-module (crates-io))

(define-public crate-rust-tsne-0.1.0 (c (n "rust-tsne") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1mkmb65nqjiwq758iwp0sy5m0nswcq9qj82dmir95idlwn41jp57") (y #t)))

(define-public crate-rust-tsne-0.1.1 (c (n "rust-tsne") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.13.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jl2504wxhwxjlp3g3bp4pxz2d51d6dg98gmc2ijz76afdd6w3wn") (y #t)))

(define-public crate-rust-tsne-0.1.2 (c (n "rust-tsne") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.13.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01yiapjrr8drbfy790l9kziib29l24c0agdsi0v5d7m3j8sxrfxx") (y #t)))

(define-public crate-rust-tsne-0.0.0 (c (n "rust-tsne") (v "0.0.0") (d (list (d (n "ndarray") (r "^0.13.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14m7yggxffjv2szscadri56v9wy0dz72ibv4wk75m9d7j52q4dzc") (y #t)))

