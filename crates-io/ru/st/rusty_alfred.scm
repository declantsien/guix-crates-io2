(define-module (crates-io ru st rusty_alfred) #:use-module (crates-io))

(define-public crate-rusty_alfred-0.1.0 (c (n "rusty_alfred") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "032pgjlhdzj0bymwkzdzc5rvj2lxjdfwr2qwzs4y06mn7gwixqjf")))

(define-public crate-rusty_alfred-0.1.1 (c (n "rusty_alfred") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "15lciq7qzawmg8ikjk1nhsy0bz33dqyq6aa8bagsfzm9l66959n9")))

