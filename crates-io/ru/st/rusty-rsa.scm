(define-module (crates-io ru st rusty-rsa) #:use-module (crates-io))

(define-public crate-rusty-rsa-0.1.0 (c (n "rusty-rsa") (v "0.1.0") (d (list (d (n "glass_pumpkin") (r "^1.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0bvz2k9rl7s5g3a1cy97wyjwpszybavlhrbcsfc55wgc0scy9h8i")))

