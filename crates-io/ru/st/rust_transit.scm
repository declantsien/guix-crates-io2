(define-module (crates-io ru st rust_transit) #:use-module (crates-io))

(define-public crate-rust_transit-0.1.0 (c (n "rust_transit") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rust_transit_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1ygfdsbgy52gznj6iza5s2dphwffz1w7bgp3fy418jzsx59l7a70")))

