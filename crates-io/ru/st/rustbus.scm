(define-module (crates-io ru st rustbus) #:use-module (crates-io))

(define-public crate-rustbus-0.1.0 (c (n "rustbus") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.16.0") (d #t) (k 0)))) (h "1mjcya597g14wvps302qrm5vyi2k6z0b9v17m1d4nhgdkc3p750p")))

(define-public crate-rustbus-0.1.1 (c (n "rustbus") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1biv6rasks2mlc4vf1v3grfswy3dpkfchwnnzvzarxisyrjnafxj")))

(define-public crate-rustbus-0.2.0 (c (n "rustbus") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1pi0qayvfhg4fwb2hh91ap6gjmkkly30z5jbwnf8b61a28sz7rkc")))

(define-public crate-rustbus-0.3.0 (c (n "rustbus") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "126a9626xplnpzf7gmwdbr2lagbyv3f7ypv6ygv59ypqpnqah9z1")))

(define-public crate-rustbus-0.3.1 (c (n "rustbus") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0hp6pw0s7xfkbfd8z4r8s4p7c8iw8yrqc2vpn0gvm109bl2dp0fr")))

(define-public crate-rustbus-0.3.2 (c (n "rustbus") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0xws54y8zdgzyk6g77nw2w016mzri9sxl8bqcg0nf95c0vap74h5")))

(define-public crate-rustbus-0.4.0 (c (n "rustbus") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1hvhiiij1xzg8kl2pkpn1b8g5phxhbmlw45g0bk31qbjsr8ksd01")))

(define-public crate-rustbus-0.4.1 (c (n "rustbus") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "072i513jxvjrxgqy7grrbp9i094vcab4n2lqwx96phk9aak91skk")))

(define-public crate-rustbus-0.5.1 (c (n "rustbus") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1xim6ix95cplk6hh0klsc0hl3kiik17s3w9pzp7kk70c490vq7rd")))

(define-public crate-rustbus-0.6.0 (c (n "rustbus") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1mr174qx1r88vbjvrjbvs36lcii1gsz22j77r6jlh5r9kbmy459r")))

(define-public crate-rustbus-0.7.0 (c (n "rustbus") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0z6jdrkyx67w176ya4nadr2v1k8bl66kxpa93wa9rd0f24pd9pbc")))

(define-public crate-rustbus-0.7.1 (c (n "rustbus") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1dvakbh80jll11fyj9dqhssxaqpdhvx6w5mzn7jvhmmxpml0n91b")))

(define-public crate-rustbus-0.8.0 (c (n "rustbus") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0czz854b7rmblcfyacvz3hw540i2aic2100jwvwwazjjdhp8c9rw")))

(define-public crate-rustbus-0.9.0 (c (n "rustbus") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0gkvz8szz2kankjd8yl862gyc29j38p39hiqz8mgggf7diz8lxwa")))

(define-public crate-rustbus-0.9.1 (c (n "rustbus") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1z04q0ag6pxx48pwg1ryf6gv4lzlcmnw6z78nyh5lk1llb2pxiay")))

(define-public crate-rustbus-0.9.2 (c (n "rustbus") (v "0.9.2") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "nix") (r ">=0.17.0, <0.18.0") (d #t) (k 0)))) (h "1bcfw8mxha2ckxzx7f3sv1gm7qqza84y4iccxph0vd2ca5fsjdfs")))

(define-public crate-rustbus-0.10.0 (c (n "rustbus") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "1a98k5707zzr6nbrghwmq8kqcg4852w10d3znqiqb2p74pwch7pz")))

(define-public crate-rustbus-0.11.0 (c (n "rustbus") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "0zff5bnqvafqq6vhb2bs1c536y160z6w5pw626csrqsqw4m9zwhv")))

(define-public crate-rustbus-0.12.0 (c (n "rustbus") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0l6ksb37f2hv93xqfv1kpqv1k38gpc5x2cajkwhpw5iaa637j9m4")))

(define-public crate-rustbus-0.14.0 (c (n "rustbus") (v "0.14.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.2.0") (d #t) (k 0)))) (h "06yhlgvrrgdbx4vzi7g3qm225if77d5z7wawvf7sxw71xprfdawy")))

(define-public crate-rustbus-0.15.0 (c (n "rustbus") (v "0.15.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0g8j1wb9v9yyd18h68a7jfynls26x0q4d3smx7ahzg6pxjxhk4xz")))

(define-public crate-rustbus-0.16.0 (c (n "rustbus") (v "0.16.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1vr9vkdjqrv7anfnxsqx79k6sxnk1q023rkfk1bkl4x4wd68h5gx")))

(define-public crate-rustbus-0.17.0 (c (n "rustbus") (v "0.17.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.4.0") (d #t) (k 0)))) (h "0hl5k6fzqnh9k1bxx8x5q0kcqivj9m47msqv4v4j4mkzfmf6fv6r")))

(define-public crate-rustbus-0.18.0 (c (n "rustbus") (v "0.18.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.24") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hnczzky0hmzs4var6c81v2dxhs7bqr2i74cjxnlrys0941ar32s")))

(define-public crate-rustbus-0.19.0 (c (n "rustbus") (v "0.19.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11m5lxd9skyrb1p5ci8szkx0z7l9ikk58ym925qplkiklid9ya2p")))

(define-public crate-rustbus-0.19.1 (c (n "rustbus") (v "0.19.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bj348ln8s2gpq2si85m2hgin7dlsc9kar8z70madcmjbhdjxwrp")))

(define-public crate-rustbus-0.19.2 (c (n "rustbus") (v "0.19.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nsplc773yvy58nhv2livx48wd9wps1l7196qwgv6yx6kgr473i0")))

(define-public crate-rustbus-0.19.3 (c (n "rustbus") (v "0.19.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "rustbus_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10gjc9klmnb80pbz0rxy45vdqyrv4aicvzmrz9bns30x4hgcam20")))

