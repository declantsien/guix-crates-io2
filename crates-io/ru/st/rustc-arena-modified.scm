(define-module (crates-io ru st rustc-arena-modified) #:use-module (crates-io))

(define-public crate-rustc-arena-modified-0.1.0 (c (n "rustc-arena-modified") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 2)))) (h "14pp0v4flghxj7vfxwkv5zp82yjbrvl44943majbw848ynald087") (f (quote (("slab"))))))

(define-public crate-rustc-arena-modified-0.1.1 (c (n "rustc-arena-modified") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 2)))) (h "1zlssl1vhz50afa4ja1gciifvinfq6g5f0cfaw1ac6cyp4126v7c") (f (quote (("slab"))))))

