(define-module (crates-io ru st rusty-forest) #:use-module (crates-io))

(define-public crate-rusty-forest-0.1.0 (c (n "rusty-forest") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "termion") (r "^1.0.0") (d #t) (k 0)))) (h "1d11cy5zv3wmk3q2kx15vklj2mj19yf8f44zzdld2n5c7j24yqbx")))

(define-public crate-rusty-forest-0.1.1 (c (n "rusty-forest") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "termion") (r "^1.0.0") (d #t) (k 0)))) (h "02vmiyys2yps5fx5gv1k4x7wpqb3wx3sixg517mvcqpadpa8mni1")))

(define-public crate-rusty-forest-0.1.2 (c (n "rusty-forest") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "termion") (r "^1.0.0") (d #t) (k 0)))) (h "1ajwcl7kvsvbggil0fpmsm18mrrj90jps5d3ss8c1pq5nish5gvj")))

