(define-module (crates-io ru st rust2xml) #:use-module (crates-io))

(define-public crate-rust2xml-0.0.1 (c (n "rust2xml") (v "0.0.1") (d (list (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "syn-serde") (r "^0.2") (d #t) (k 0)))) (h "0ixyi0vb4y1mdg5mwa1pd9a2s2dlv5vvyv3c0gn39h01g68i75dj")))

