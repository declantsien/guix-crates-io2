(define-module (crates-io ru st rustarium) #:use-module (crates-io))

(define-public crate-rustarium-0.0.5 (c (n "rustarium") (v "0.0.5") (d (list (d (n "local-ip") (r "^0.1") (d #t) (k 0)))) (h "19yz7550d1c40hv9y427w48jsm29anifkgqhr6y6rsmcc5wrjc4n")))

(define-public crate-rustarium-0.1.1 (c (n "rustarium") (v "0.1.1") (d (list (d (n "local-ip") (r "^0.1") (d #t) (k 0)))) (h "06zslslx3d7rcck8f5sn3d87kdf63m1ib5hb8wys0w3pixb7mvp6")))

(define-public crate-rustarium-0.1.2 (c (n "rustarium") (v "0.1.2") (d (list (d (n "local-ip") (r "^0.1") (d #t) (k 0)))) (h "0nm48gds85kkp6zdlkrfsjg2zg912aw889zwgh19kp4hgc1wgiaf")))

(define-public crate-rustarium-0.1.3 (c (n "rustarium") (v "0.1.3") (d (list (d (n "local-ip") (r "^0.1") (d #t) (k 0)))) (h "10cky2sv7c70b36jg2fk15p8nz98p9fqxs6mb8iypdlxrdqd46li")))

