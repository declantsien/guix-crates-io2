(define-module (crates-io ru st rust-fr) #:use-module (crates-io))

(define-public crate-rust-fr-0.1.0 (c (n "rust-fr") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dl8z359kfdv4bfa36mbz5kmswg7nahw8v61rfw6savbvlm2nx3z") (y #t)))

(define-public crate-rust-fr-1.0.0 (c (n "rust-fr") (v "1.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.2") (d #t) (k 2)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ivavl0brivdipj8p3pnnc8259wh46x5n25w38wkra9042q7n4l0") (y #t)))

(define-public crate-rust-fr-1.0.1 (c (n "rust-fr") (v "1.0.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.2") (d #t) (k 2)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14fh47xb6b3yq4ccyx0rdfh0dn6v4cmip4gfkm7aqrcbq6h79z5b")))

