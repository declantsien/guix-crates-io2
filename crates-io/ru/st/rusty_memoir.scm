(define-module (crates-io ru st rusty_memoir) #:use-module (crates-io))

(define-public crate-rusty_memoir-0.1.0 (c (n "rusty_memoir") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (d #t) (k 0)))) (h "1r5i85q9ziak9xy2gima0z4w8nnbrpdn8xplnqynhb9xbyal3mqb") (y #t)))

