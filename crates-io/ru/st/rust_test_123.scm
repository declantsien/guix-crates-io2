(define-module (crates-io ru st rust_test_123) #:use-module (crates-io))

(define-public crate-rust_test_123-0.1.0 (c (n "rust_test_123") (v "0.1.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 0)))) (h "1wjl6gkk845ch5bixq1y4xxwnhaqh0ywj8n8jz843mnlq7xqy3hd")))

(define-public crate-rust_test_123-0.1.1 (c (n "rust_test_123") (v "0.1.1") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 0)))) (h "00lbiq3b9m9v5l0alhsp4d9i84k6jn4ipy1qw2isggnhkx4b4769")))

