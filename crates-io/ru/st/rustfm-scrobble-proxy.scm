(define-module (crates-io ru st rustfm-scrobble-proxy) #:use-module (crates-io))

(define-public crate-rustfm-scrobble-proxy-1.1.2 (c (n "rustfm-scrobble-proxy") (v "1.1.2") (d (list (d (n "attohttpc") (r "^0.19") (f (quote ("form"))) (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "mockito") (r "^0.28") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wrapped-vec") (r "^0.3") (d #t) (k 0)))) (h "0d565wxnd51s2lbs099mn97a698v3p7a8r89ivnjh5rnw18pdvci")))

(define-public crate-rustfm-scrobble-proxy-1.1.3 (c (n "rustfm-scrobble-proxy") (v "1.1.3") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("form"))) (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "mockito") (r "^0.31") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wrapped-vec") (r "^0.3") (d #t) (k 0)))) (h "10hag4sqm19yylbzasl8vw7j9w9cbsnmxzlpq2jfxr5pz3pj51hz")))

(define-public crate-rustfm-scrobble-proxy-2.0.0 (c (n "rustfm-scrobble-proxy") (v "2.0.0") (d (list (d (n "attohttpc") (r "^0.25") (f (quote ("form"))) (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "mockito") (r "^0.31") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wrapped-vec") (r "^0.3") (d #t) (k 0)))) (h "1ghjr57ipq9wj9c4ik6i2fzbs6msrm058ycm0rbhp67vh5c4wq7y")))

