(define-module (crates-io ru st rust-shell) #:use-module (crates-io))

(define-public crate-rust-shell-0.1.0 (c (n "rust-shell") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "07fr30zi4vlyacgca25makkrsam62773110ldbwx36prb34p968a") (y #t)))

(define-public crate-rust-shell-0.1.1 (c (n "rust-shell") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0wz027i810wg1dr2nj7ppvgzh36k9c4awgif5vbl3ih6wvddnxnv") (y #t)))

(define-public crate-rust-shell-0.2.0 (c (n "rust-shell") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0q0lhgaxk64v2rwqrpdlcaj9icacaz3nrid00y4l0v554y4j75py")))

