(define-module (crates-io ru st rustere) #:use-module (crates-io))

(define-public crate-rustere-0.1.0 (c (n "rustere") (v "0.1.0") (h "1lm639h5jcl87pdnfpc607hl4bwkxqjmy3qm1k0ydk5vsach03fj")))

(define-public crate-rustere-0.2.0 (c (n "rustere") (v "0.2.0") (h "0qxa2a5v7sp098w6agy9ciqxrsrrh020f65mgacik85nrildpas3")))

(define-public crate-rustere-0.3.0 (c (n "rustere") (v "0.3.0") (h "1idjm9bywprx75471p86rnpcbr0s618lyk29mpll7vg63n74p08k")))

