(define-module (crates-io ru st rust_multicodec) #:use-module (crates-io))

(define-public crate-rust_multicodec-0.1.0 (c (n "rust_multicodec") (v "0.1.0") (d (list (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)))) (h "1w561543agb42pqvli37b0i9mii3npbb6accdxyzx6v2z1y4c2sg")))

(define-public crate-rust_multicodec-0.2.0 (c (n "rust_multicodec") (v "0.2.0") (d (list (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)))) (h "098dwj19ji7wvm23ldjhf0yyqqcci3xq225lr53v5d7g74305bq3")))

