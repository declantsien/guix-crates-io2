(define-module (crates-io ru st rust-timeit) #:use-module (crates-io))

(define-public crate-rust-timeit-0.2.0 (c (n "rust-timeit") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "0h1j0r3ymidrbn7adxfznqsshv58l6vwhv9cnic97fkldn8b8mc7")))

(define-public crate-rust-timeit-0.3.0 (c (n "rust-timeit") (v "0.3.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "1ziz43bcpl5bz5vzb35jy14sq392v2yr8aqifk6xk0mi3lia5si8")))

