(define-module (crates-io ru st rusty-forkfork) #:use-module (crates-io))

(define-public crate-rusty-forkfork-0.3.0 (c (n "rusty-forkfork") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0mfqw30rama21m371zvb86nffs7d3mkjv5f4f485h4x25aaxx2dp") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

(define-public crate-rusty-forkfork-0.4.0 (c (n "rusty-forkfork") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0662hjcvaxw7kcawjmvkhs9sc7n74kjba6hj8c0hryx2vzs5ms3w") (f (quote (("timeout" "wait-timeout") ("default" "timeout"))))))

