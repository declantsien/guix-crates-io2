(define-module (crates-io ru st rust_hls_macro_lib) #:use-module (crates-io))

(define-public crate-rust_hls_macro_lib-0.1.0 (c (n "rust_hls_macro_lib") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "extra-traits" "printing" "clone-impls" "derive" "visit" "visit-mut" "fold"))) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "03g5jjbij56rszwrapflrdxx5jjab30gjwpm4d8ra9frddnqs6ym")))

