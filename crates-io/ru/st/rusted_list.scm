(define-module (crates-io ru st rusted_list) #:use-module (crates-io))

(define-public crate-rusted_list-0.1.0 (c (n "rusted_list") (v "0.1.0") (h "1zylpcydfjd81z0dgnd9c08jk2l6m86lpvxgna9xw12qq08hql44") (y #t)))

(define-public crate-rusted_list-0.1.1 (c (n "rusted_list") (v "0.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1lkq1a67v9vwrf5q5vrhqyvpwygwwv0iqsmd73vmkw47yygj1k53") (y #t)))

(define-public crate-rusted_list-1.0.0 (c (n "rusted_list") (v "1.0.0") (h "1fvgwp10yb5sv31qwxknb0vz48cb7v0yq077wkqwnjbxjjncnmms")))

(define-public crate-rusted_list-1.1.0 (c (n "rusted_list") (v "1.1.0") (h "0ci8ynwnc75vb455yj6adlpfw0h4lki81ps32zdpslw0a14x0bjl")))

