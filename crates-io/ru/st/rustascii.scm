(define-module (crates-io ru st rustascii) #:use-module (crates-io))

(define-public crate-rustascii-0.1.0 (c (n "rustascii") (v "0.1.0") (h "1jjcp68j2djlcw4kmr5m3f6j6l7pij7rsq11238v88c561l77z8q")))

(define-public crate-rustascii-0.1.1 (c (n "rustascii") (v "0.1.1") (h "09gbnkcaxh844c9b9xg8bwn6dck5zxm6l5zikc0li2nwyfjl53kv")))

(define-public crate-rustascii-0.1.2 (c (n "rustascii") (v "0.1.2") (h "0iav7470gb5islrqr1bai1d0j8nawk1azsfriihc36ryyk7qd6yk")))

