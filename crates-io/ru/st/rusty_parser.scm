(define-module (crates-io ru st rusty_parser) #:use-module (crates-io))

(define-public crate-rusty_parser-0.1.0 (c (n "rusty_parser") (v "0.1.0") (d (list (d (n "rusty_parser_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0vak4n0s8p3lzbx6f8xl5b40nh1c78bplna40ik0cwi284v5yqsz") (y #t)))

(define-public crate-rusty_parser-0.1.1 (c (n "rusty_parser") (v "0.1.1") (h "1d82s527s9mbkzh7xq2mphs7w1vv19fkj4p7xnallqll2m4hv2qr")))

(define-public crate-rusty_parser-0.1.4 (c (n "rusty_parser") (v "0.1.4") (h "1qlwdrp93j02mq6wlxcl15d873zbmc3b23g97bl2y8w28fikm6g9")))

(define-public crate-rusty_parser-0.1.6 (c (n "rusty_parser") (v "0.1.6") (h "1vb7d5mpc19nmac76lpb76aw59wyhvkbxx7qqmv8d26dh8clgih9")))

(define-public crate-rusty_parser-0.1.12 (c (n "rusty_parser") (v "0.1.12") (h "0wlqx99bxx3bmw45qclvh23z27nk0n1vcs1ljwgj751lwx6migvr")))

