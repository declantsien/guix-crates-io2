(define-module (crates-io ru st rusty-p4-proto) #:use-module (crates-io))

(define-public crate-rusty-p4-proto-0.1.0-alpha.1 (c (n "rusty-p4-proto") (v "0.1.0-alpha.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "grpcio") (r "^0.5.0-alpha.4") (f (quote ("prost-codec"))) (d #t) (k 0)) (d (n "grpcio-compiler") (r "^0.5.0-alpha.2") (f (quote ("prost-codec"))) (d #t) (k 1)) (d (n "prost") (r "^0.5") (d #t) (k 0)) (d (n "prost-types") (r "^0.5") (d #t) (k 0)))) (h "1kaqmi3d6x1vdvvbdasd4h4kjjcp4w999lhadn0yiq04sbicm955")))

(define-public crate-rusty-p4-proto-0.1.0-alpha.2 (c (n "rusty-p4-proto") (v "0.1.0-alpha.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.5") (d #t) (k 0)) (d (n "prost-types") (r "^0.5") (d #t) (k 0)) (d (n "tonic") (r "^0.1.0-alpha.1") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1.0-alpha.3") (f (quote ("rustfmt"))) (d #t) (k 1)))) (h "0plswn84wciss0w8fvhig32g3g3nsc7fjhbh2ynk4dbac3hfn4g1")))

(define-public crate-rusty-p4-proto-0.1.0-alpha.3 (c (n "rusty-p4-proto") (v "0.1.0-alpha.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.1.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1.0") (d #t) (k 1)))) (h "01c1s87hdv6358k0v5wdkbag9x9nybf11r1zqjyd2aww0yfix1wr")))

