(define-module (crates-io ru st rust2fun_laws) #:use-module (crates-io))

(define-public crate-rust2fun_laws-0.1.0 (c (n "rust2fun_laws") (v "0.1.0") (d (list (d (n "rust2fun") (r "^0.1.0") (d #t) (k 0)))) (h "1s31z87ixv5pvfn853pymb4azwqsd6vcpgw8lmali3zssrjx497d")))

(define-public crate-rust2fun_laws-0.2.0 (c (n "rust2fun_laws") (v "0.2.0") (d (list (d (n "rust2fun") (r "^0.2.0") (d #t) (k 0)))) (h "1namkafrs7cj2fkj98kgbryqyj6wn2lg65bkmdwmmr3yq0630jyl")))

(define-public crate-rust2fun_laws-0.2.1 (c (n "rust2fun_laws") (v "0.2.1") (d (list (d (n "rust2fun") (r "^0.2.1") (d #t) (k 0)))) (h "1fgh740nzpsg77126g3rvpy45ldgyl12x3kx5f324y0jb7hsxh66")))

