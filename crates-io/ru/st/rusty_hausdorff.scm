(define-module (crates-io ru st rusty_hausdorff) #:use-module (crates-io))

(define-public crate-rusty_hausdorff-0.1.0 (c (n "rusty_hausdorff") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.2") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "00vnkjpmwyv8ps7nqgqlbjqcdnzqgkqbndiw32k08vr3azxpsjfy")))

(define-public crate-rusty_hausdorff-0.2.0 (c (n "rusty_hausdorff") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.2") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "186ia2ys54qx7zzaa4gn0pll5ncp0vgn513n4ndhgsmkd4amjl0f")))

