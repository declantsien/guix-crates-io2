(define-module (crates-io ru st rust-blind-rsa-signatures) #:use-module (crates-io))

(define-public crate-rust-blind-rsa-signatures-0.1.0 (c (n "rust-blind-rsa-signatures") (v "0.1.0") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hmac-sha512") (r "^0.1") (f (quote ("traits" "sha384"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rsa") (r "^0.3") (f (quote ("expose-internals"))) (d #t) (k 0)))) (h "0r1nbah0rlizjcw0557m254gz1k2j6aw9qa7nfwc2d3jqvcy1rdr") (y #t)))

