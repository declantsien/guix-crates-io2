(define-module (crates-io ru st rusty_planner) #:use-module (crates-io))

(define-public crate-rusty_planner-0.1.0 (c (n "rusty_planner") (v "0.1.0") (d (list (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1vwnx0kn6p27cjmbwa09yxfgbdw9j8sf7k5vl00sxv8214sjzfhn")))

(define-public crate-rusty_planner-0.1.1 (c (n "rusty_planner") (v "0.1.1") (d (list (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "06cjbz04iw118llf2xwznjw709iwfjjql2incvg5ib1biv21yzm3")))

(define-public crate-rusty_planner-0.1.2 (c (n "rusty_planner") (v "0.1.2") (d (list (d (n "rusty_agent") (r "~0.1.0") (o #t) (d #t) (k 0)))) (h "0yvy4ydwq2chnksc1rvr1yq52wv35qpavpakd5rbz2cdk4apjl6g") (s 2) (e (quote (("multi_agent" "dep:rusty_agent"))))))

