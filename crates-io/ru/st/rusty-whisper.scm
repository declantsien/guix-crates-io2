(define-module (crates-io ru st rusty-whisper) #:use-module (crates-io))

(define-public crate-rusty-whisper-0.1.0 (c (n "rusty-whisper") (v "0.1.0") (h "1cznzy3sxsbhiyi6glvf98x6wa2jjcxjx0ynfabj2z0qmxcriq67") (y #t)))

(define-public crate-rusty-whisper-0.1.1 (c (n "rusty-whisper") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.3") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20.21") (d #t) (k 0)))) (h "0p4yip4rm1nifzw3hk75hy4d94i4knwmkypamlmkl5s17bas1jls")))

(define-public crate-rusty-whisper-0.1.2 (c (n "rusty-whisper") (v "0.1.2") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.3") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20.21") (d #t) (k 0)))) (h "1dbs3lfy42k3b87zbchhdsixnw1vxamlz30ngkqazcg42l9lhv2x")))

(define-public crate-rusty-whisper-0.1.3 (c (n "rusty-whisper") (v "0.1.3") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.3") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20.22") (d #t) (k 0)))) (h "12pm6cq7v06k658s3slv4pbnx0g2h1gf76xhv71qb326mi9j1w2c")))

