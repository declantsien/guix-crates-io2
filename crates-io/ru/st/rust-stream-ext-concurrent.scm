(define-module (crates-io ru st rust-stream-ext-concurrent) #:use-module (crates-io))

(define-public crate-rust-stream-ext-concurrent-1.0.0 (c (n "rust-stream-ext-concurrent") (v "1.0.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "more-asserts") (r "^0.3.1") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0vl6lak3kq743bgcd97f350fib1n95h7w7y8r5m30536d3ivkjrg")))

