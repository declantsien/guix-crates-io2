(define-module (crates-io ru st rust2fun_macros) #:use-module (crates-io))

(define-public crate-rust2fun_macros-0.1.0 (c (n "rust2fun_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qljy215jiyjrf7r7rrbjnqskfqqzdvgn4cxm1hk5gg4dsih7yj3")))

(define-public crate-rust2fun_macros-0.2.0 (c (n "rust2fun_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hvwvswk8bwjqx65y08i339xy433y7a84b68w833r26vfvf67g7p")))

(define-public crate-rust2fun_macros-0.2.1 (c (n "rust2fun_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vvjwv2fxkiaxcfjc7zhzs3j949jnsbchldld4ydsa60wdw3p9q7")))

