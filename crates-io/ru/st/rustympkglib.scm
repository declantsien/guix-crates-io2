(define-module (crates-io ru st rustympkglib) #:use-module (crates-io))

(define-public crate-rustympkglib-0.1.0 (c (n "rustympkglib") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tree-sitter") (r "^0.16") (d #t) (k 0)))) (h "0dd4ah7bwwy14k5qad6cm8qdi3rfd8y1xam3b8c9wgl1jbn79sp3")))

(define-public crate-rustympkglib-0.1.1 (c (n "rustympkglib") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tree-sitter") (r "^0.19") (d #t) (k 0)) (d (n "tree-sitter-bash") (r "^0.19") (d #t) (k 0)))) (h "05ia4y1ws6n8kilhksmmwz9kim593a7cxy4i8byx1iacki7a19ir")))

