(define-module (crates-io ru st rustii) #:use-module (crates-io))

(define-public crate-rustii-0.1.0 (c (n "rustii") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.2") (d #t) (k 0)))) (h "0z1h0jzr7cdxh69bz8npiiql3b3dzsjcknmzizvjf5hynyxpdl9p") (y #t)))

(define-public crate-rustii-0.1.1 (c (n "rustii") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.2") (d #t) (k 0)))) (h "0ihw1l8hlyjzlza45b3g01dynzyqwgxgdxa4f5syp9kb3jsirxkk") (y #t)))

