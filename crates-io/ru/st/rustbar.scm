(define-module (crates-io ru st rustbar) #:use-module (crates-io))

(define-public crate-rustbar-0.1.0 (c (n "rustbar") (v "0.1.0") (h "1jlhqrrgr87l97fyq90hi4jvbd1gq6g6r5452i8pbdxzdfnmw876")))

(define-public crate-rustbar-0.2.0 (c (n "rustbar") (v "0.2.0") (h "0jrwmyirpig9jnvm1s6p2v11h1nbjf9vansnvhl89j0baaqysbzv")))

