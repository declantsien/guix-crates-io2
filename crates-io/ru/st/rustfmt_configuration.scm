(define-module (crates-io ru st rustfmt_configuration) #:use-module (crates-io))

(define-public crate-rustfmt_configuration-1.0.0 (c (n "rustfmt_configuration") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "rustfmt-config_proc_macro") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syntax_pos") (r "^610.0.0") (d #t) (k 0) (p "rustc-ap-syntax_pos")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ks87w72sq7rg4w8cn6lckq007xhw7s8vv5kgsbp1n3mk8xp9ys3")))

