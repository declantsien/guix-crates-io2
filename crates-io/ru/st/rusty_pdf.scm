(define-module (crates-io ru st rusty_pdf) #:use-module (crates-io))

(define-public crate-rusty_pdf-0.1.0 (c (n "rusty_pdf") (v "0.1.0") (d (list (d (n "imagesize") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("chrono_time" "nom_parser" "embed_image"))) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)))) (h "0rbzwnydzfb3kb3yx56r1mb2cdf3hpvn1rp35ybn7pvlqyg0a1x0")))

(define-public crate-rusty_pdf-0.1.1 (c (n "rusty_pdf") (v "0.1.1") (d (list (d (n "imagesize") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("chrono_time" "nom_parser" "embed_image"))) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)))) (h "1av52in878vpgwv6riksmcfh0ha0kiwmm24v91avwkxlzkvn3sl5")))

(define-public crate-rusty_pdf-0.2.0 (c (n "rusty_pdf") (v "0.2.0") (d (list (d (n "imagesize") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("chrono_time" "nom_parser" "embed_image"))) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)))) (h "0cap44sr97wd2ik16sgclnxa75px69ywmk8akrlp2qjfj10cdn8m")))

(define-public crate-rusty_pdf-0.21.0 (c (n "rusty_pdf") (v "0.21.0") (d (list (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "imagesize") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("chrono_time" "nom_parser" "embed_image"))) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "0x1flh9f681vyjkp5hi3y35n7fz3ddig4i3iv6l0y92sb0wr3hk2")))

