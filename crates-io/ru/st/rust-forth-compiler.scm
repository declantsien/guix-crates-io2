(define-module (crates-io ru st rust-forth-compiler) #:use-module (crates-io))

(define-public crate-rust-forth-compiler-0.3.1 (c (n "rust-forth-compiler") (v "0.3.1") (d (list (d (n "rust-simple-stack-processor") (r "^0.2.0") (d #t) (k 0)))) (h "1spcndj7482ybxdrbld06r011ac2agk3ycj0yx4rzss2fzhaif1k")))

(define-public crate-rust-forth-compiler-0.4.0 (c (n "rust-forth-compiler") (v "0.4.0") (d (list (d (n "rust-forth-tokenizer") (r "^0.0.5") (d #t) (k 0)) (d (n "rust-simple-stack-processor") (r "^0.2.0") (d #t) (k 0)))) (h "0d59hji8wnsbmpbpa5rcf8f7ypbqpjx4mij4v97pvak2cn9cm0nw") (f (quote (("track-forth-compiler-metadata"))))))

(define-public crate-rust-forth-compiler-0.4.1 (c (n "rust-forth-compiler") (v "0.4.1") (d (list (d (n "rust-forth-tokenizer") (r "^0.0.5") (d #t) (k 0)) (d (n "rust-simple-stack-processor") (r "^0.2.0") (d #t) (k 0)))) (h "08x6w7ib0g3rry0175vviy7d00jmb4w2f4isi07l09qrb2bd5w79") (f (quote (("track-forth-compiler-metadata"))))))

(define-public crate-rust-forth-compiler-0.4.2 (c (n "rust-forth-compiler") (v "0.4.2") (d (list (d (n "rust-forth-tokenizer") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-simple-stack-processor") (r "^0.2.0") (d #t) (k 0)))) (h "0p4qhlyar4jinr2gbi2dkq1i334p40h9yay86n1mvzbhc36lcy22") (f (quote (("track-forth-compiler-metadata"))))))

(define-public crate-rust-forth-compiler-0.5.0 (c (n "rust-forth-compiler") (v "0.5.0") (d (list (d (n "rust-forth-tokenizer") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-simple-stack-processor") (r "^0.6.0") (d #t) (k 0)))) (h "0xp5z2z3mw8ipdwamlqnm4rpsbv7ns9xvqq82wcnv4h7jcr0bxzi") (f (quote (("track-forth-compiler-metadata"))))))

(define-public crate-rust-forth-compiler-0.5.1 (c (n "rust-forth-compiler") (v "0.5.1") (d (list (d (n "rust-forth-tokenizer") (r "^0.1.1") (d #t) (k 0)) (d (n "rust-simple-stack-processor") (r "^0.6.0") (d #t) (k 0)))) (h "0gg6qc2ybya7fl2szdgpyksn2fwhhs7rqmb0vnql76l2shisp4x7") (f (quote (("track-forth-compiler-metadata"))))))

(define-public crate-rust-forth-compiler-0.5.2 (c (n "rust-forth-compiler") (v "0.5.2") (d (list (d (n "rust-forth-tokenizer") (r "^0.1.2") (d #t) (k 0)) (d (n "rust-simple-stack-processor") (r "^0.7.0") (d #t) (k 0)))) (h "050drkpm7ws8x99kvbk8q5vjxcyn2nzmy5mr8pf64djril6wybb6") (f (quote (("track-forth-compiler-metadata"))))))

(define-public crate-rust-forth-compiler-0.5.3 (c (n "rust-forth-compiler") (v "0.5.3") (d (list (d (n "rust-forth-tokenizer") (r "^0.2.0") (d #t) (k 0)) (d (n "rust-simple-stack-processor") (r "^0.7.0") (d #t) (k 0)))) (h "1klrwv7sgr48dfrsxj3hzvd46kzfgvmndfbcvcl137arnjwyabbj") (f (quote (("enable_reflection"))))))

