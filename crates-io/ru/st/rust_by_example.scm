(define-module (crates-io ru st rust_by_example) #:use-module (crates-io))

(define-public crate-rust_by_example-0.1.0 (c (n "rust_by_example") (v "0.1.0") (h "1ky01vi376ad7jbyk0wjmh2yi733rbmipkv1rky3rnwg9kq7c30a")))

(define-public crate-rust_by_example-0.2.0 (c (n "rust_by_example") (v "0.2.0") (d (list (d (n "ferris-says") (r "^0.2") (d #t) (k 0)))) (h "1j5qypdbh0hmwm8gnnh7vs734ll95x8by4v3srj5klhshdvflm9x")))

