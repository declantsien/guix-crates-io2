(define-module (crates-io ru st rustiny_computer_graphic) #:use-module (crates-io))

(define-public crate-rustiny_computer_graphic-0.1.0 (c (n "rustiny_computer_graphic") (v "0.1.0") (d (list (d (n "rustiny_fixed_point") (r "^0.1.10") (d #t) (k 0)) (d (n "rustiny_linear_algebra") (r "^0.1.5") (d #t) (k 0)) (d (n "rustiny_number") (r "^0.1.10") (d #t) (k 0)))) (h "0m5hiwbyjasxarki2cymkkws43bynkxkyw2prvbrc254i9krl7v9")))

(define-public crate-rustiny_computer_graphic-0.2.0 (c (n "rustiny_computer_graphic") (v "0.2.0") (d (list (d (n "rustiny_fixed_point") (r "^0.2.0") (d #t) (k 0)) (d (n "rustiny_linear_algebra") (r "^0.2.0") (d #t) (k 0)) (d (n "rustiny_number") (r "^0.2.0") (d #t) (k 0)))) (h "080k1nm6j27z4vssil65k0gq3pbigwracbs3jl6as0k00396gywl")))

