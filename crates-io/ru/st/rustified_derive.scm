(define-module (crates-io ru st rustified_derive) #:use-module (crates-io))

(define-public crate-rustified_derive-0.5.3 (c (n "rustified_derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0pf45fz4873svn8xnh380ichfw8ma157rzrn947d4scv4nc34vlk")))

