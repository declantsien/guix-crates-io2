(define-module (crates-io ru st rustigo) #:use-module (crates-io))

(define-public crate-rustigo-1.1.0 (c (n "rustigo") (v "1.1.0") (h "1znid9adxxgffxdw6mb9h51prczs8qmrv58xbyhw6iw6897cka82")))

(define-public crate-rustigo-1.1.1 (c (n "rustigo") (v "1.1.1") (h "1yaw9adygd8zfgvn0pmn1sgv3jzjklhmlsps1wlz0dzykwa7885g")))

(define-public crate-rustigo-1.1.3 (c (n "rustigo") (v "1.1.3") (h "0mgkbqvvv7dasr2n985jfpvfb7r34bljp66zpp5kk47bm4a9m05j")))

