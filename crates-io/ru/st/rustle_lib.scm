(define-module (crates-io ru st rustle_lib) #:use-module (crates-io))

(define-public crate-rustle_lib-0.0.1-alpha (c (n "rustle_lib") (v "0.0.1-alpha") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "swc") (r "^0.232.54") (d #t) (k 0)) (d (n "swc_common") (r "^0.29.10") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.94.14") (d #t) (k 0)) (d (n "swc_ecma_codegen") (r "^0.127.24") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.122.20") (d #t) (k 0)))) (h "1v5a3pqb6y6j2vclhp6hpa6mbw1xk696qzmhsjv9jwv8jwy1yh6s") (y #t)))

