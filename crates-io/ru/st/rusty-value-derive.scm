(define-module (crates-io ru st rusty-value-derive) #:use-module (crates-io))

(define-public crate-rusty-value-derive-0.1.0 (c (n "rusty-value-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04nfiac07ll5rjjjkc08ky1bby06iv8grlb02achck34vf2rs4v3")))

(define-public crate-rusty-value-derive-0.1.1 (c (n "rusty-value-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yys295pjdjii87j66y0gvg0bkakdgh785jsfbbvkrcpzaf3n1dq")))

