(define-module (crates-io ru st rustaferry_render_hal) #:use-module (crates-io))

(define-public crate-rustaferry_render_hal-0.1.0 (c (n "rustaferry_render_hal") (v "0.1.0") (h "0lnxq822kc0j1lkw5g5hlnk093bdayqfj4zcrgyjrdi5zbiinrmx")))

(define-public crate-rustaferry_render_hal-0.1.1 (c (n "rustaferry_render_hal") (v "0.1.1") (h "0lamq72cb1cwnqx1akjqph2spyhfph0hhbgbm16nzw07c761vg3f")))

(define-public crate-rustaferry_render_hal-0.2.0 (c (n "rustaferry_render_hal") (v "0.2.0") (h "13pmj22f7b0l89pfdmh3ydhy09r18h597kyjm4ra0hf7idmxaaj5")))

(define-public crate-rustaferry_render_hal-1.0.0 (c (n "rustaferry_render_hal") (v "1.0.0") (h "1z6hb366scqf6j6mqfc48szz401785lrfwa99b79sv0589gm64j2")))

(define-public crate-rustaferry_render_hal-0.1.3 (c (n "rustaferry_render_hal") (v "0.1.3") (h "07npwrpbijf1anz93g597c2dr7n6xa1nvjbaxq4n3vf2a9lx46gf")))

