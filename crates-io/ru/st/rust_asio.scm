(define-module (crates-io ru st rust_asio) #:use-module (crates-io))

(define-public crate-rust_asio-0.1.0 (c (n "rust_asio") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1vmz7j77kfvmrif8qgypch88w5dc3qsvxggi0wkbmxh1nlvvk736") (f (quote (("developer"))))))

(define-public crate-rust_asio-0.1.1 (c (n "rust_asio") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1lf29af38cazyfsr88krrmsv1sqpqr1fb2b3fgsx0wq154gfkpli") (f (quote (("developer"))))))

(define-public crate-rust_asio-0.1.2 (c (n "rust_asio") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0akhviv04pqzmhz1hg7v4rhg6bln2b44hhnkvl8c6rx3241sp99a") (f (quote (("developer"))))))

(define-public crate-rust_asio-0.2.0 (c (n "rust_asio") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0357vxihhvvw5fkkfycjni5bm03vm95226b867wbx8gpmig1gay6") (f (quote (("developer"))))))

(define-public crate-rust_asio-0.2.1 (c (n "rust_asio") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0f7qpwmfzqb1kqr6jj341l5l087r4hlpa3vc02wkn73a9x56bd6q") (f (quote (("developer"))))))

(define-public crate-rust_asio-0.3.0 (c (n "rust_asio") (v "0.3.0") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1lahj7rmkyjh0j0kbbjzmrd3yxvk2m9lylfi708wq48xnbi8r1rh")))

(define-public crate-rust_asio-0.3.1 (c (n "rust_asio") (v "0.3.1") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0kak2vd2srkfmqc8zc99a0vqzll6saziyvwj6sm0mz3mcf3lm5hy") (f (quote (("asio_no_signal_set") ("asio_no_epoll_reactor"))))))

(define-public crate-rust_asio-0.3.2 (c (n "rust_asio") (v "0.3.2") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ivpw2irla703nndyrhwrc2qz5qppsg4vm7bn1m72kmd9a2zscb5") (f (quote (("asio_no_signal_set") ("asio_no_epoll_reactor")))) (y #t)))

(define-public crate-rust_asio-0.3.3 (c (n "rust_asio") (v "0.3.3") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "03rv5c81m07mjqqdk36qk9rgbgnnnfymdrcf7xzikkk0y5g27275") (f (quote (("asio_no_signal_set") ("asio_no_epoll_reactor"))))))

(define-public crate-rust_asio-0.3.4 (c (n "rust_asio") (v "0.3.4") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09jq6sw0imq27djgljbmn20r2kn2lwbv7r66nwppkwyy4g8d2g8p") (f (quote (("asio_no_signal_set") ("asio_no_epoll_reactor"))))))

(define-public crate-rust_asio-0.4.0 (c (n "rust_asio") (v "0.4.0") (d (list (d (n "context") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thread-id") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1b7r53348pqzafynl8caq7qpfmy272hv5gzvcdgwlg23583y0nqx") (f (quote (("default" "context"))))))

(define-public crate-rust_asio-0.5.0 (c (n "rust_asio") (v "0.5.0") (d (list (d (n "context") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0b0zqdka7lmrcj2464cf2qfh6jkfjlp7ayq36c3i3n43p6pqrz7s") (f (quote (("timerfd") ("signalfd") ("pipe") ("kqueue") ("epoll") ("devpoll") ("default" "context" "termios" "epoll" "kqueue" "devpoll" "signalfd" "timerfd" "pipe"))))))

(define-public crate-rust_asio-0.5.1 (c (n "rust_asio") (v "0.5.1") (d (list (d (n "context") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0r14imw8ijpaff37d11qbn097l64dagg4bc7f5bdw7c92fsnklnx") (f (quote (("timerfd") ("signalfd") ("pipe") ("kqueue") ("epoll") ("devpoll") ("default" "context" "termios" "epoll" "kqueue" "devpoll" "signalfd" "timerfd" "pipe"))))))

(define-public crate-rust_asio-0.5.2 (c (n "rust_asio") (v "0.5.2") (d (list (d (n "context") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1s7f1x4kjxxkd010hbybhfix360v3a2zckda60cm2y9031zkvdsz") (f (quote (("timerfd") ("signalfd") ("pipe") ("kqueue") ("epoll") ("devpoll") ("default" "context" "termios" "epoll" "kqueue" "devpoll" "signalfd" "timerfd" "pipe"))))))

(define-public crate-rust_asio-0.5.3 (c (n "rust_asio") (v "0.5.3") (d (list (d (n "context") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1aqns9zghhnjkimgxfx3jz9ab188qhk44izc1vwsm7p2s27k5gsd") (f (quote (("timerfd") ("signalfd") ("pipe") ("kqueue") ("epoll") ("devpoll") ("default" "context" "termios" "epoll" "kqueue" "devpoll" "signalfd" "timerfd" "pipe"))))))

(define-public crate-rust_asio-0.6.0 (c (n "rust_asio") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "context") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "termios") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "0jlvafk09kdk8imdcxc1cqx21zdq3r9f33sn1dacfmp7dk6qhz1h") (f (quote (("windows") ("timerfd") ("signalfd") ("pipe") ("macos" "openssl" "openssl-sys" "termios") ("linux" "openssl" "openssl-sys" "termios") ("kqueue") ("epoll") ("default" "context" "epoll" "kqueue" "pipe" "timerfd" "signalfd"))))))

