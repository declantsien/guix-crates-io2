(define-module (crates-io ru st rust_storage_interface_library) #:use-module (crates-io))

(define-public crate-rust_storage_interface_library-0.1.0 (c (n "rust_storage_interface_library") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1vn83065635gkzmi39180plf01i5ywp73rgab0i6flbk8ijsadkr")))

(define-public crate-rust_storage_interface_library-0.1.1 (c (n "rust_storage_interface_library") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1c6h4fb6sm6zaq8s2ab8qygzhc9rrrdgx3wjbcl3ggg4n216rnwc")))

(define-public crate-rust_storage_interface_library-0.1.2 (c (n "rust_storage_interface_library") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0v8nfcz1d1cps5rx33i7mhrvhsfzpp84pnx06jcfgi0dl99l6jdm")))

(define-public crate-rust_storage_interface_library-0.1.3 (c (n "rust_storage_interface_library") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0hs440jv9jjvhp3zjkfjmaaz3jq2asmfgsrbrigiwganb20k5iq7")))

(define-public crate-rust_storage_interface_library-0.1.4 (c (n "rust_storage_interface_library") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ci1qrw9wwvvg8hrp388pnh51c2zjq1x2zk2vpzmha6sdwdajafw")))

(define-public crate-rust_storage_interface_library-0.1.5 (c (n "rust_storage_interface_library") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04b0zk83q7lf0sgig8vk6j8y8r33j5d46ix1wkr8ck9hcfv39bvc")))

(define-public crate-rust_storage_interface_library-0.1.6 (c (n "rust_storage_interface_library") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "043dr338ijz7a0yi667g5dz2dsrig8bcnsbjy48mipylnfyj0cmq")))

(define-public crate-rust_storage_interface_library-0.1.7 (c (n "rust_storage_interface_library") (v "0.1.7") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "136jsm06cmm72jp8sr8cp0zqdiss6vcn0p773shb1zr8rlziyj9s")))

(define-public crate-rust_storage_interface_library-0.1.8 (c (n "rust_storage_interface_library") (v "0.1.8") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1svm8bssx85zsjvgbwkig64vlxlj7j11f1d3s7gc0k5fm5j4q1qf")))

(define-public crate-rust_storage_interface_library-0.1.9 (c (n "rust_storage_interface_library") (v "0.1.9") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1lzk2rq98m2hfd1yb3gzbhp3dzysa0plx3p3vdz1y8wc4zj2vc08")))

(define-public crate-rust_storage_interface_library-0.1.10 (c (n "rust_storage_interface_library") (v "0.1.10") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1zngycfdi1dsm580axfhxyjzzcpm4ccyzrsnmxqf5ws635vgpidx")))

(define-public crate-rust_storage_interface_library-0.1.11 (c (n "rust_storage_interface_library") (v "0.1.11") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0n7y8f0gsi0ssa6g16l1x358jf82ryp2876kbsjzy61wr0n7ff9n")))

(define-public crate-rust_storage_interface_library-0.1.12 (c (n "rust_storage_interface_library") (v "0.1.12") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1l6zr709m4f9mj1r2xgh2yjfak8snpbmq0zby0l6yq6sdjjdya2k")))

(define-public crate-rust_storage_interface_library-0.1.13 (c (n "rust_storage_interface_library") (v "0.1.13") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1mhvnqi71q45fz42wbp2vdfpgcapyxdhcqk98yd1kzzn1ic22qfw")))

(define-public crate-rust_storage_interface_library-0.1.14 (c (n "rust_storage_interface_library") (v "0.1.14") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1klr12xw5r40rrib358pvhv5fi5xm870w2qjf6banc2b9j9155qm")))

(define-public crate-rust_storage_interface_library-0.1.15 (c (n "rust_storage_interface_library") (v "0.1.15") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1i7az963sbg7bvkl712kdzh1w4lqgz65d9r2syqdcin0l3fz6f47")))

(define-public crate-rust_storage_interface_library-0.1.16 (c (n "rust_storage_interface_library") (v "0.1.16") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "0izc327sp9ychnksjf23fhnz3wvm639wzmy3r3pc3v13arhdhysf")))

(define-public crate-rust_storage_interface_library-0.1.17 (c (n "rust_storage_interface_library") (v "0.1.17") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "024hf62s7wb9ipg4aj1v2hdpfrmxsx76954df3lm9dpgz3c9b19m")))

(define-public crate-rust_storage_interface_library-0.1.18 (c (n "rust_storage_interface_library") (v "0.1.18") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1n8fwhcfrpb5i06snqbq28s4qkygivybm7vwslrd0ap3cs1x6ijf")))

(define-public crate-rust_storage_interface_library-0.1.20 (c (n "rust_storage_interface_library") (v "0.1.20") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1ibp0k4wphbs60p93js6sx4slbp76qnv1f11z6jmwjwwxp753azc")))

(define-public crate-rust_storage_interface_library-0.1.21 (c (n "rust_storage_interface_library") (v "0.1.21") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1lv2vdqp3dbm3nraipl3v2wgp1j91rjhxg2792pxah6ck8hyv4ba")))

(define-public crate-rust_storage_interface_library-0.1.22 (c (n "rust_storage_interface_library") (v "0.1.22") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "0v0hw27pipvvccm83531wyxhvxwgxp5bbyz277cfx1mmfhw9c094")))

(define-public crate-rust_storage_interface_library-0.1.23 (c (n "rust_storage_interface_library") (v "0.1.23") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1a37bgnzlh50i1i467h16wzy9yk4xyykj0zw9yqllfmhsd0x78c7")))

(define-public crate-rust_storage_interface_library-0.1.24 (c (n "rust_storage_interface_library") (v "0.1.24") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1f520mxv563vka67yq5mkx5lblzjrycb220wslrribxmq2c4zr0p")))

(define-public crate-rust_storage_interface_library-0.1.25 (c (n "rust_storage_interface_library") (v "0.1.25") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "009ds9ccl9sw2gwfplh8gyxamzmcp3f1s56igzxym75sk7m1whrs")))

(define-public crate-rust_storage_interface_library-0.1.26 (c (n "rust_storage_interface_library") (v "0.1.26") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "01zwnm4qn99ci9w0c6bxhk8iznj4zf8bxkhr9i7s8vljrfs05snv")))

(define-public crate-rust_storage_interface_library-0.1.27 (c (n "rust_storage_interface_library") (v "0.1.27") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1ab4wwsi3f489hssz0hx41svi5yyabhryg9c3ynig7abwahg2sbc")))

(define-public crate-rust_storage_interface_library-0.1.28 (c (n "rust_storage_interface_library") (v "0.1.28") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1cza2497xf9q3f0yyc4y44p65x94b0ihdyjhdgzfwr05gdqaqny4")))

(define-public crate-rust_storage_interface_library-0.1.29 (c (n "rust_storage_interface_library") (v "0.1.29") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1d0l9q0bfvh61x50134n7cq0dyjb14z9rrmx4ki42jv7z13y5wkh")))

(define-public crate-rust_storage_interface_library-0.1.30 (c (n "rust_storage_interface_library") (v "0.1.30") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "0wbammib9pkcf51rf6mzgvswv0wylcapqjgb7rkn4v03vnjfnqh8")))

