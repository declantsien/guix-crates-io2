(define-module (crates-io ru st rustls-symcrypt) #:use-module (crates-io))

(define-public crate-rustls-symcrypt-0.1.0 (c (n "rustls-symcrypt") (v "0.1.0") (d (list (d (n "cargo-llvm-cov") (r "^0.6.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "rustls") (r "^0.23.0") (f (quote ("ring" "tls12" "std"))) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 2)) (d (n "rustls-webpki") (r "^0.102.2") (d #t) (k 0)) (d (n "symcrypt") (r "^0.2.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (d #t) (k 2)))) (h "1a51z1g370fl6pz6dys9s34gv1adcjrxd2plfpc5lqzkmrxd8ywl") (f (quote (("x25519") ("default") ("chacha"))))))

