(define-module (crates-io ru st rustpython-sre_engine) #:use-module (crates-io))

(define-public crate-rustpython-sre_engine-0.3.1 (c (n "rustpython-sre_engine") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)))) (h "14qd2ngrp8rh8k1qdjn3hbsj7r56dqqr0vlwris4lhb5a84bpwxv")))

