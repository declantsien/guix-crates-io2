(define-module (crates-io ru st rust-crlf) #:use-module (crates-io))

(define-public crate-rust-crlf-1.0.0 (c (n "rust-crlf") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.14") (d #t) (k 0)))) (h "1nz1rfilr5qsqj446yi2hky5fzs1h1j04bvbr72qzk2ag7g2wmyv")))

