(define-module (crates-io ru st rusty-html) #:use-module (crates-io))

(define-public crate-rusty-html-0.1.0 (c (n "rusty-html") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)))) (h "0cdcif8m75zp28n76bzg7r43kbhjy6yvrqkfx5hw4rsi73d7k9rw")))

(define-public crate-rusty-html-0.1.1 (c (n "rusty-html") (v "0.1.1") (d (list (d (n "rusty-html-macros") (r "^0.1") (d #t) (k 0)))) (h "0mxpi7p957gm6d5my50sc6xmkvjdmp6v9l1mygibl51x8w1nbqvs")))

(define-public crate-rusty-html-0.1.2 (c (n "rusty-html") (v "0.1.2") (d (list (d (n "rusty-html-macros") (r "^0.1") (d #t) (k 0)))) (h "02na5qi4f3rkjym81fc4bccrmi0r99pwk6kha1x5b7092bssjryr")))

