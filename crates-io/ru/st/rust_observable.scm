(define-module (crates-io ru st rust_observable) #:use-module (crates-io))

(define-public crate-rust_observable-0.1.0 (c (n "rust_observable") (v "0.1.0") (d (list (d (n "rust_observable_literal") (r "^0.1.0") (d #t) (k 0)))) (h "1bwb3fb90ni1g6m3dkhkxiya2jxfd1k6n32mdvgd00qdkc5whgqj")))

(define-public crate-rust_observable-0.1.1 (c (n "rust_observable") (v "0.1.1") (d (list (d (n "rust_observable_literal") (r "^0.1.0") (d #t) (k 0)))) (h "0j16pfqcpb9n2gk225psiql5x77rqxjwzfql66lfc8ixa1cd0gcd")))

(define-public crate-rust_observable-0.1.2 (c (n "rust_observable") (v "0.1.2") (d (list (d (n "rust_observable_literal") (r "^0.1.0") (d #t) (k 0)))) (h "09wn6fvg08yy9gwwgnyg6g6rghl2qr6za6hcxnjjadhfdhhsbs9i")))

(define-public crate-rust_observable-0.2.0 (c (n "rust_observable") (v "0.2.0") (d (list (d (n "rust_observable_literal") (r "^0.1.0") (d #t) (k 0)))) (h "1rvh87i21cs56kw1lp766sv7vv9g4f9h01chmf4x75islcg2vd19")))

(define-public crate-rust_observable-0.2.1 (c (n "rust_observable") (v "0.2.1") (d (list (d (n "rust_observable_literal") (r "^0.1.0") (d #t) (k 0)))) (h "1ijxz224vw79m9h3wnc4x63l03h8k6fdl0s88f2qp00jk6xqa3ax")))

