(define-module (crates-io ru st rustore) #:use-module (crates-io))

(define-public crate-rustore-0.1.0 (c (n "rustore") (v "0.1.0") (h "0k8s3zjlfzb5qy0j6klc42b88xigh0dwcqii3x69aq5fdzdk035f") (y #t)))

(define-public crate-rustore-0.1.1 (c (n "rustore") (v "0.1.1") (h "1rapga83wdnjy9lp70fd6vz8r6gs9r8cz81rlflg4szy47yl2xfz")))

