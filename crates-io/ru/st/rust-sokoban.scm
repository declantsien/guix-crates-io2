(define-module (crates-io ru st rust-sokoban) #:use-module (crates-io))

(define-public crate-rust-sokoban-0.1.0 (c (n "rust-sokoban") (v "0.1.0") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "specs") (r "^0.17.0") (f (quote ("specs-derive"))) (d #t) (k 0)))) (h "074d99hr8fbzf153s4a1n27hdflj727b46g0265vk9kfy32d82w2")))

(define-public crate-rust-sokoban-0.1.1 (c (n "rust-sokoban") (v "0.1.1") (d (list (d (n "ggez") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20.0") (f (quote ("mint"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "specs") (r "^0.17.0") (f (quote ("specs-derive"))) (d #t) (k 0)))) (h "0pz2jkk5dpp88m4xx20540hmf5ljpv9svaw952dkmbz49nmbx1d6")))

