(define-module (crates-io ru st rust_learn) #:use-module (crates-io))

(define-public crate-rust_learn-0.1.0 (c (n "rust_learn") (v "0.1.0") (h "1h2ynp1yxph0jr1xy7dsmw6i9b3sfc3354aqlzzpw3rx6zidjiz8")))

(define-public crate-rust_learn-0.1.1 (c (n "rust_learn") (v "0.1.1") (h "0wmsiaqc1a752gm5z6g8j9zkjpf27a50sjr5l1pgxaxpj9p9i65d")))

