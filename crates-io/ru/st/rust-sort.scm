(define-module (crates-io ru st rust-sort) #:use-module (crates-io))

(define-public crate-rust-sort-0.1.0 (c (n "rust-sort") (v "0.1.0") (h "18ig76jnqa30fa572kg3vlsnm4ikygr6m3fgsmi5j4q210vz7cmz")))

(define-public crate-rust-sort-0.1.1 (c (n "rust-sort") (v "0.1.1") (d (list (d (n "utils") (r "^0.0.1") (d #t) (k 2)))) (h "1idprhg3h2m2h198h2hq3ish9vb2zjcpqsi4qh5wwrjpw9g2vi5p")))

(define-public crate-rust-sort-0.1.2 (c (n "rust-sort") (v "0.1.2") (d (list (d (n "utils") (r "^0.0.1") (d #t) (k 2)))) (h "0zsqlkdiclchhy42m2qwgycqk1axcxv53d69kfm8p9ib9sz74p3s")))

(define-public crate-rust-sort-0.1.3 (c (n "rust-sort") (v "0.1.3") (d (list (d (n "utils") (r "^0.0.1") (d #t) (k 2)))) (h "11068g18qikwfic9kdn0xsfk81ahc9h7wi23dz9d5za2bjx44mmh")))

(define-public crate-rust-sort-0.1.4 (c (n "rust-sort") (v "0.1.4") (d (list (d (n "utils") (r "^0.0.1") (d #t) (k 2)))) (h "00ql43lafvkzgf16llj9ljmcghvy7mbmvadzml8sdzsqx7gmyz0s")))

