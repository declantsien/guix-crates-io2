(define-module (crates-io ru st rust-hdl-fpga-support) #:use-module (crates-io))

(define-public crate-rust-hdl-fpga-support-0.1.0 (c (n "rust-hdl-fpga-support") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.31.0") (d #t) (k 0)))) (h "15x88q2wfz0d6iw3lh0cvbrhzp5g6cxzmvd7fzx0pr1r7wlf8g86")))

(define-public crate-rust-hdl-fpga-support-0.2.0 (c (n "rust-hdl-fpga-support") (v "0.2.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.32.0") (d #t) (k 0)))) (h "113rg83kizicmxbkdp9scib4r655a4qm695yr5a372xnlfr0c4gk")))

(define-public crate-rust-hdl-fpga-support-0.3.0 (c (n "rust-hdl-fpga-support") (v "0.3.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.33.0") (d #t) (k 0)))) (h "0z0rk6ijlla24mazvry339277j09fv4k0ayrrdyizhmxx5c2jz9f")))

(define-public crate-rust-hdl-fpga-support-0.4.0 (c (n "rust-hdl-fpga-support") (v "0.4.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.34.0") (d #t) (k 0)))) (h "15zzffhmlm1rfs5kwziw48wxmm26635fj6wm3ync3il5h9whjhmk")))

(define-public crate-rust-hdl-fpga-support-0.5.0 (c (n "rust-hdl-fpga-support") (v "0.5.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.35.0") (d #t) (k 0)))) (h "0bb2ldmfxllzglz5cfl28rr0vbcsiz5lzdxz60zyx5acydq95hyr")))

(define-public crate-rust-hdl-fpga-support-0.5.1 (c (n "rust-hdl-fpga-support") (v "0.5.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.35.0") (d #t) (k 0)))) (h "1nfmp400hgf427nav7dkp3cwrlba3p3ncq49ppwlgrgyx0qj32ns")))

(define-public crate-rust-hdl-fpga-support-0.5.2 (c (n "rust-hdl-fpga-support") (v "0.5.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.35.0") (d #t) (k 0)))) (h "1mnjpzgzh3q6fn69gsa5pldbzm3ysr65ncdsv0rdd5gvhscwlzwa")))

(define-public crate-rust-hdl-fpga-support-0.6.0 (c (n "rust-hdl-fpga-support") (v "0.6.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.36.0") (d #t) (k 0)))) (h "0mb8r3qmmw70r0iv7sw1k2gm85vhfij7b2gvg0laq5m7n9lxjwj2")))

(define-public crate-rust-hdl-fpga-support-0.7.0 (c (n "rust-hdl-fpga-support") (v "0.7.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.37.0") (d #t) (k 0)))) (h "0r3nzjcscdqfbrag04a6jc5q5s4q0f870wqm3ass0qi366vlrjpq")))

(define-public crate-rust-hdl-fpga-support-0.7.1 (c (n "rust-hdl-fpga-support") (v "0.7.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.37.0") (d #t) (k 0)))) (h "1s8nyznc2i7m7y5h6bnbhdzgx1vqmr6mfzb9qr5s5q1ns8hyp8pq")))

(define-public crate-rust-hdl-fpga-support-0.8.0 (c (n "rust-hdl-fpga-support") (v "0.8.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)))) (h "1qcl95iq2xbx7nahxrfpxzprc59n9h0zqj69wc0vh9bmkmbdj3qz")))

(define-public crate-rust-hdl-fpga-support-0.8.2 (c (n "rust-hdl-fpga-support") (v "0.8.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)))) (h "09ynhbl1ylmm4zn4cg5y8dcw3vzff7n4l4kc6w880np46xgxjvw2")))

(define-public crate-rust-hdl-fpga-support-0.8.3 (c (n "rust-hdl-fpga-support") (v "0.8.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)))) (h "1186h3zyql318jvbpqxz5k8icxlmy9dkmi0y4jjj08iapk6ihbvh")))

(define-public crate-rust-hdl-fpga-support-0.8.4 (c (n "rust-hdl-fpga-support") (v "0.8.4") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)))) (h "0ic3ywnibs4wbrcvihf5yivvg0y4b2qfr8k2xyz0lamcah1sl9bx")))

(define-public crate-rust-hdl-fpga-support-0.8.5 (c (n "rust-hdl-fpga-support") (v "0.8.5") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)))) (h "00ak3nm076652mxyhcnya6pr5y7zsybad1aq8by5s3gvsajnwx6a")))

(define-public crate-rust-hdl-fpga-support-0.8.6 (c (n "rust-hdl-fpga-support") (v "0.8.6") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)))) (h "1017id2bb1fsdw15z1hf089ff5yf902fbvizka6y1qj8avkp83gd")))

(define-public crate-rust-hdl-fpga-support-0.11.0 (c (n "rust-hdl-fpga-support") (v "0.11.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.41.0") (d #t) (k 0)))) (h "0cb572pwva36xradyzrcszlqksa183dx6awgw5562sbcwns7vshb")))

(define-public crate-rust-hdl-fpga-support-0.11.1 (c (n "rust-hdl-fpga-support") (v "0.11.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.41.0") (d #t) (k 0)))) (h "1kql9gqlraam6racs79lk576mws5ng84vhj3rbi0glc61khpcyaa")))

(define-public crate-rust-hdl-fpga-support-0.42.0 (c (n "rust-hdl-fpga-support") (v "0.42.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0") (d #t) (k 0)))) (h "00ykmlm3rxbj1gf5632zkpax04y3bfjvv39203dsywb7gi598ljl")))

(define-public crate-rust-hdl-fpga-support-0.43.0 (c (n "rust-hdl-fpga-support") (v "0.43.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.43.0") (d #t) (k 0)))) (h "0vqrajx0qsfr47amsbw203b8y3cbln1kv3d4gqkmppwgrlq68y5s")))

(define-public crate-rust-hdl-fpga-support-0.44.2 (c (n "rust-hdl-fpga-support") (v "0.44.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.44.2") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.44.2") (d #t) (k 0)))) (h "055qp8m83ai9042fd0k0csm47izwiq6j5m3fi4jch2cq3y4m8bgk")))

(define-public crate-rust-hdl-fpga-support-0.45.0 (c (n "rust-hdl-fpga-support") (v "0.45.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.45.0") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.45.0") (d #t) (k 0)))) (h "11j7cp6f003r77hdzgjxc3gg098ymxax67pldmizjbi3p6l8j2rg")))

(define-public crate-rust-hdl-fpga-support-0.45.1 (c (n "rust-hdl-fpga-support") (v "0.45.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.45.1") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.45.1") (d #t) (k 0)))) (h "1iyxkqcz85vkf96d7f9zhlzarrab333pkkb03mvxgl6qajd9l4by")))

(define-public crate-rust-hdl-fpga-support-0.46.0 (c (n "rust-hdl-fpga-support") (v "0.46.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.46.0") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.46.0") (d #t) (k 0)))) (h "0y5mxrnn0xd83d4x0nv5ybz6ikxzk2wj3zz0kagvlmsnn0mm2j7y")))

