(define-module (crates-io ru st rust-spice-derive) #:use-module (crates-io))

(define-public crate-rust-spice-derive-0.6.0 (c (n "rust-spice-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1j7r3b43rsiwdmp1xdq77lhb68664dgsmpwllhfh1nyxlmqp582c")))

(define-public crate-rust-spice-derive-0.6.1 (c (n "rust-spice-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1mj30iyc2jify7fh6np357ng37pc8xl62pqfgdv4dhwlqmvr5qym")))

(define-public crate-rust-spice-derive-0.6.3 (c (n "rust-spice-derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "197wj17gwqkrdmc7qhc5f60diqi80w3507iyk5grq24wxbnnwcvb") (y #t)))

(define-public crate-rust-spice-derive-0.6.4 (c (n "rust-spice-derive") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0pagi2gkf7d8z42pr4aqpwvqgalp7p3acc4qxb4xdm0b7jp7j6mp")))

(define-public crate-rust-spice-derive-0.6.10 (c (n "rust-spice-derive") (v "0.6.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0632qncyjvz3gyz4rr1in3qigkfraqvv1jx4azcj5l389nwdy478")))

(define-public crate-rust-spice-derive-0.7.0 (c (n "rust-spice-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1irzv2vzqa4q23bpdhd18mcs8nd1748dvac4msqh2bs9bbcizfwh")))

(define-public crate-rust-spice-derive-0.7.1 (c (n "rust-spice-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yd4bgqmd73clp64n2z0sn4ca5y6wybwg9p1i59awag2als8k3z4")))

(define-public crate-rust-spice-derive-0.7.2 (c (n "rust-spice-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dyz0qbdwbzwm21h4lwddx79lkz7jp2mfvaz43ncpnq0gf0f7zkr")))

(define-public crate-rust-spice-derive-0.7.3 (c (n "rust-spice-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bk6nmkrqz4qi4yqf4pzzq374b5igjm00mbiihfgz148l2fdzqc3")))

(define-public crate-rust-spice-derive-0.7.4 (c (n "rust-spice-derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ymq501w0fai9b6zbf4ii8niwga1hrxnynbz39w7zsdz0nvyvxb1")))

(define-public crate-rust-spice-derive-0.7.5 (c (n "rust-spice-derive") (v "0.7.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ijizpc36a0pbrqiprylgdxffacslmzk9rdx9aq006zhlk36vh4d")))

(define-public crate-rust-spice-derive-0.7.6 (c (n "rust-spice-derive") (v "0.7.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixvbmxypr01bf7s1c8jn48dyk1bzhf2qivq92lxd0lrw4g94iv4")))

