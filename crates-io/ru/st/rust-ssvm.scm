(define-module (crates-io ru st rust-ssvm) #:use-module (crates-io))

(define-public crate-rust-ssvm-0.0.1 (c (n "rust-ssvm") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "evmc-client") (r "^7.4.1") (d #t) (k 0) (p "ssvm-evmc-client")) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0r1vdkhyhfjh5rhl0rdmyd5dmwhg3nnq27pw2bqankwr49709zhx") (y #t)))

(define-public crate-rust-ssvm-0.0.2 (c (n "rust-ssvm") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "evmc-client") (r "^7.4.1") (d #t) (k 0) (p "ssvm-evmc-client")) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "06399zyy3pi6hqz93g41yms4xp43krwxwn5ac5p3bj2schciywf4") (y #t)))

(define-public crate-rust-ssvm-0.0.3 (c (n "rust-ssvm") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "evmc-client") (r "^7.4.1") (d #t) (k 0) (p "ssvm-evmc-client")) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0s8x8sk2si4rfs6q2p6abzfm9yvk66z11ssg7v6pgh2gskpbzac6") (y #t)))

(define-public crate-rust-ssvm-0.0.4 (c (n "rust-ssvm") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "evmc-client") (r "^6.3.1") (d #t) (k 0) (p "ssvm-evmc-client")) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0vh9cq3bpaj1119xkmyb3rf1bb3zhpqbgz7hbh1czm28kipq2kpg") (y #t)))

(define-public crate-rust-ssvm-0.0.1-rc1 (c (n "rust-ssvm") (v "0.0.1-rc1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "evmc-client") (r "^6.3.1-rc1") (d #t) (k 0) (p "ssvm-evmc-client")) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1nkk9iahfs8gcczcnzbmgw4b467rk58v1sznyav8q1zxvj7jammc") (y #t)))

(define-public crate-rust-ssvm-0.1.0-rc1 (c (n "rust-ssvm") (v "0.1.0-rc1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "evmc-client") (r "^6.3.1-rc3") (d #t) (k 0) (p "ssvm-evmc-client")) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0rq4fcpvic0p6c0czwvrks7mszzagivqyv6249kwk4ffl5ps2ydy") (y #t)))

(define-public crate-rust-ssvm-0.1.0-rc2 (c (n "rust-ssvm") (v "0.1.0-rc2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "evmc-client") (r "^6.3.1-rc4") (d #t) (k 0) (p "ssvm-evmc-client")) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1ccpll2dymzg42s7mhjz21p1xnvpailwhr0ml8hy6dpyrs0yrajw")))

