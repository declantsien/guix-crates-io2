(define-module (crates-io ru st rusty_audio) #:use-module (crates-io))

(define-public crate-rusty_audio-1.0.0 (c (n "rusty_audio") (v "1.0.0") (d (list (d (n "rodio") (r "^0.9.0") (d #t) (k 0)))) (h "14figfdgaglj8a9dg7h94crasx2cpx8s41cpzdshkydaqblw4wis")))

(define-public crate-rusty_audio-1.1.0 (c (n "rusty_audio") (v "1.1.0") (d (list (d (n "rodio") (r "^0.9.0") (d #t) (k 0)))) (h "11f3pxi80nczdfmlqjjpj6jqn3wws2cf9sjj4k8l1yk3c6737i7c")))

(define-public crate-rusty_audio-1.1.1 (c (n "rusty_audio") (v "1.1.1") (d (list (d (n "rodio") (r "^0.10.0") (d #t) (k 0)))) (h "14li67697dcfvg7472qzbarpfrrzwn702p68hy3fcklzcq36gfh7")))

(define-public crate-rusty_audio-1.1.2 (c (n "rusty_audio") (v "1.1.2") (d (list (d (n "rodio") (r "^0.10.0") (d #t) (k 0)))) (h "1wbf6icny83xmzi20d5m8zj2kmlqprwxrvm59x3wxyirbg61bc2d")))

(define-public crate-rusty_audio-1.1.3 (c (n "rusty_audio") (v "1.1.3") (d (list (d (n "rodio") (r "^0.10.0") (d #t) (k 0)))) (h "0fgqb8pmkkidp9sx32f32rld9myshgrqrbb2zkkjzrsbnl7syg2z")))

(define-public crate-rusty_audio-1.1.4 (c (n "rusty_audio") (v "1.1.4") (d (list (d (n "rodio") (r "^0.10.0") (d #t) (k 0)))) (h "1hn9dgb93hch7iiajmvrcf4dwydgbiy2861g94ymz8zxi9c4i3pq")))

(define-public crate-rusty_audio-1.1.5 (c (n "rusty_audio") (v "1.1.5") (d (list (d (n "rodio") (r "^0.11.0") (d #t) (k 0)))) (h "17p7bw3l5mfn0ybdh78ibyhi7dh4v83lc8dhz8h2x4d01b1bk1z9")))

(define-public crate-rusty_audio-1.2.0 (c (n "rusty_audio") (v "1.2.0") (d (list (d (n "rodio") (r "^0.13.0") (d #t) (k 0)))) (h "1fcj99w4skgxjkhvylyajfw542zv5bara1ra1834k96rgpnrm554")))

(define-public crate-rusty_audio-1.2.1 (c (n "rusty_audio") (v "1.2.1") (d (list (d (n "rodio") (r "^0.13.0") (d #t) (k 0)))) (h "02rcks1w0smg4rjnfpqyjpp4zwv98gvvxsa4ivablp9dj05ama7w")))

(define-public crate-rusty_audio-1.3.0 (c (n "rusty_audio") (v "1.3.0") (d (list (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "08cvsj0mz3bgh9ak5f41s92bhdrjin3hmwvh3vgzbl1f0slvw4pd")))

(define-public crate-rusty_audio-1.4.0 (c (n "rusty_audio") (v "1.4.0") (d (list (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1mld8qf699dgsgrxqdxn67xkxyzr1p4bgdvmqp1h4bczaipyzf2d")))

(define-public crate-rusty_audio-1.4.1 (c (n "rusty_audio") (v "1.4.1") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1lahc2n7s80i2lsp90aqjhqbv3lzqipqw0ws6dkg9ylzx98sc4p3")))

