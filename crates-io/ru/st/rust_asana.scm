(define-module (crates-io ru st rust_asana) #:use-module (crates-io))

(define-public crate-rust_asana-0.1.0 (c (n "rust_asana") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q2c4cs3n7w23isgg2ap100asqriviwvx5gcc46a965r8f55nl11")))

