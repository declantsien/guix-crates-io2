(define-module (crates-io ru st rustywind_vite) #:use-module (crates-io))

(define-public crate-rustywind_vite-0.1.0 (c (n "rustywind_vite") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "rustls") (r "^0.22") (d #t) (k 0)) (d (n "rustywind_core") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (d #t) (k 0)))) (h "09ns4hldvgkjs7kkdsz10y72r7mzh3rqsnl2lh2mbh9njff80myd")))

