(define-module (crates-io ru st rust-sl) #:use-module (crates-io))

(define-public crate-rust-sl-0.2.0 (c (n "rust-sl") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1f6kpi8mp494hp90yy4l3rkw9hllycv7978cksq976rfq7mhzbsv")))

(define-public crate-rust-sl-0.2.1 (c (n "rust-sl") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "15q9h00j7nymnk3vxiyrydsyjgzdfd5f4qzanwfkx18j8ra69ahj")))

(define-public crate-rust-sl-0.2.2 (c (n "rust-sl") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fyyycimzi3mh3f7i7cjii0xvcqzvql709gjqaphsmkhw8q0kmz2")))

(define-public crate-rust-sl-0.2.3 (c (n "rust-sl") (v "0.2.3") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1mqbbln7sb75xxy0rq60ms6hkcarknxz1kl51d65k4c86qdaklmx")))

