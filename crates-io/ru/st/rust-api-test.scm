(define-module (crates-io ru st rust-api-test) #:use-module (crates-io))

(define-public crate-rust-api-test-0.1.0 (c (n "rust-api-test") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ffp63pmbms4k1zzzb37sicl0c9gr6j963jv5ajrxqmqajw550bp") (y #t)))

(define-public crate-rust-api-test-1.1.0 (c (n "rust-api-test") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qk3isffgwq9x5z7ldml2n46cwi1p9lda2b995syzz53jikxf2jy") (y #t)))

(define-public crate-rust-api-test-1.1.1 (c (n "rust-api-test") (v "1.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nw40pms52bbiqv9xgpjm2cxqx07lypaj6mppg8g1bfb6n2b2fni") (r "1.76.0")))

