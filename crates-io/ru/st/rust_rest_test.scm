(define-module (crates-io ru st rust_rest_test) #:use-module (crates-io))

(define-public crate-rust_rest_test-0.1.0 (c (n "rust_rest_test") (v "0.1.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "curl") (r "^0.4.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0af7pvlvnqyr3qfis6xzjjrq8l4dwy01agc4mmi62mfl2grrqir6")))

(define-public crate-rust_rest_test-0.1.1 (c (n "rust_rest_test") (v "0.1.1") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "curl") (r "^0.4.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09kzg6s0dnk24smqn5ybzhn09ipwz0ach4bxprmx844mcbvpnv4c")))

