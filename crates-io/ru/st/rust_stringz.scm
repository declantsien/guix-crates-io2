(define-module (crates-io ru st rust_stringz) #:use-module (crates-io))

(define-public crate-rust_stringz-0.1.0 (c (n "rust_stringz") (v "0.1.0") (h "1liig01m02706r86ag6ma6ymxq6vfqs0vfxhvfj29ydw8b374ni8")))

(define-public crate-rust_stringz-0.2.0 (c (n "rust_stringz") (v "0.2.0") (h "0nm5p1l258gby1hm75syg5nw22krjna4sqj0437igax32jhjf0lx")))

(define-public crate-rust_stringz-1.0.0 (c (n "rust_stringz") (v "1.0.0") (h "167f32cgxrrxw6anw6333mib26acjqsbgq7s8sq97nyzvv1mgv5a")))

