(define-module (crates-io ru st rust_is_even) #:use-module (crates-io))

(define-public crate-rust_is_even-0.1.0 (c (n "rust_is_even") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02y79ln85ps2ifxkc0ybgxcrjgrp51sx52z72slll38gswrylpjn")))

