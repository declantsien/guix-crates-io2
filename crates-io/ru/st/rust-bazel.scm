(define-module (crates-io ru st rust-bazel) #:use-module (crates-io))

(define-public crate-rust-bazel-0.1.0 (c (n "rust-bazel") (v "0.1.0") (d (list (d (n "mycalculator") (r "^0.1.0") (d #t) (k 0)))) (h "0n0qpjjcqxaj9bbn1v8z8lifbvkgkzb1yapd5zhyninfh5ij1m08") (r "1.74")))

(define-public crate-rust-bazel-0.2.0 (c (n "rust-bazel") (v "0.2.0") (d (list (d (n "mycalculator") (r "^0.2.0") (d #t) (k 0)))) (h "1cns83qjbl05hhj6zqpljk2mxp3w8sc894c0nndkbzj8yvm6r2xp") (r "1.74")))

