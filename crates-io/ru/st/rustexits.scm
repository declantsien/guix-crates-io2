(define-module (crates-io ru st rustexits) #:use-module (crates-io))

(define-public crate-rustexits-0.1.0 (c (n "rustexits") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (k 1)))) (h "0463a4cyij6w2dyb97ycdnwkfmrh1r3pcgivk55j0ay1va2bzm2y") (y #t)))

(define-public crate-rustexits-0.1.1 (c (n "rustexits") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (k 1)))) (h "04l1hz28hbijzm4prvmc9wyzsdldsx3r9j4igji7zr8wzb68xv5d")))

