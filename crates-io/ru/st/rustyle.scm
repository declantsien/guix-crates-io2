(define-module (crates-io ru st rustyle) #:use-module (crates-io))

(define-public crate-rustyle-0.1.0 (c (n "rustyle") (v "0.1.0") (d (list (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)))) (h "0i1a2d6bk83dp7sglplh13ip23pkib8bfznynr4ib5c3mymcav2b")))

