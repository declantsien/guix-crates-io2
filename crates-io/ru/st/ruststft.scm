(define-module (crates-io ru st ruststft) #:use-module (crates-io))

(define-public crate-ruststft-0.3.0 (c (n "ruststft") (v "0.3.0") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "strider") (r "^0.1.3") (d #t) (k 0)))) (h "1xrdj6sgrq655mpk0va4bax2iybv7pk32la3isi81s6rf2m3yrp7")))

(define-public crate-ruststft-0.3.1 (c (n "ruststft") (v "0.3.1") (d (list (d (n "apodize") (r "^1.0.0") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "strider") (r "^0.1.3") (d #t) (k 0)))) (h "0dr5r5bxrz1bmggax93qqy8c9v1iw3jw4l1hna25hz5xk4k15isz")))

