(define-module (crates-io ru st rusty-junctions-library-generation-proc-macro) #:use-module (crates-io))

(define-public crate-rusty-junctions-library-generation-proc-macro-0.1.0 (c (n "rusty-junctions-library-generation-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hbdscv3mx8rf25gvkn64i6jhjf38m4jm9pyih6zq78fbbpyhx9i")))

