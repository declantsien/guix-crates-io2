(define-module (crates-io ru st rustmex-entrypoint) #:use-module (crates-io))

(define-public crate-rustmex-entrypoint-0.1.0 (c (n "rustmex-entrypoint") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cww7ysanpgm6hwkpzz8nbs3ha0g0b4p342qm86gsm6l5b1wvrn3")))

(define-public crate-rustmex-entrypoint-0.1.1 (c (n "rustmex-entrypoint") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xhl3kpk65wd26kqbp7jcdvmdmphpxq6q463fyxydxp0lqf3asjv")))

(define-public crate-rustmex-entrypoint-0.2.0 (c (n "rustmex-entrypoint") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z5nbrs7h0gai7va64kzhwa8diszwyzkp6db7ip8c4d2cplkf2hg")))

(define-public crate-rustmex-entrypoint-0.3.0 (c (n "rustmex-entrypoint") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01m27x6vlsbqhfkvpv5ny70dk1v6127rzn9yycnranwcyp6b8xsk")))

(define-public crate-rustmex-entrypoint-0.4.0 (c (n "rustmex-entrypoint") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pb0zyvv5dm1az3qs75111gb3cqjxx217vxbr7y46d7gm2zdqvki")))

(define-public crate-rustmex-entrypoint-0.4.1 (c (n "rustmex-entrypoint") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k2793cr8n8ifqbv3f5y6ckgxqq35k59bk4vrpbrdrbxz75ivgfj") (y #t)))

(define-public crate-rustmex-entrypoint-0.4.2 (c (n "rustmex-entrypoint") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wnwc77whaxd7sdzyw6vfg66ch0s383skkyw8mq3hd4h6mrrhq8i")))

