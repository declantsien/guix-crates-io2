(define-module (crates-io ru st rustutils-false) #:use-module (crates-io))

(define-public crate-rustutils-false-0.1.0 (c (n "rustutils-false") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "18617yk49xn1v08w2kbl8xszazfrqmqbfnzhy64r9kmy7ckk28lh")))

