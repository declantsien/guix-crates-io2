(define-module (crates-io ru st rusty_oge-cli) #:use-module (crates-io))

(define-public crate-rusty_oge-cli-0.2.0 (c (n "rusty_oge-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rusty_oge") (r "^1.0") (d #t) (k 0)))) (h "007xym3gnry7vfdmi42b4n4pasjc2g14ff11wnxihhl9dajc1gxw")))

(define-public crate-rusty_oge-cli-0.3.0 (c (n "rusty_oge-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rusty_oge") (r "^1.0") (d #t) (k 0)))) (h "1rzah78nff967v0irg2bsbdld4mw6jdi3blfkfcli1zpr1ri8ph6")))

(define-public crate-rusty_oge-cli-0.5.0 (c (n "rusty_oge-cli") (v "0.5.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("error-context" "cargo" "derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rusty_oge") (r "^1.3.0") (d #t) (k 0)))) (h "03nyf8apqrk7kx0dyhzcvcb870cq8rgidq7gj9nf3hbdnjp5y4yh")))

(define-public crate-rusty_oge-cli-0.6.0 (c (n "rusty_oge-cli") (v "0.6.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("error-context" "cargo" "derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rusty_oge") (r "^1.4.0") (d #t) (k 0)))) (h "1rrj5c66klzfvylrggh07m2bb0ycfx8jw263q39amjfc25n7sfw6")))

(define-public crate-rusty_oge-cli-0.7.0 (c (n "rusty_oge-cli") (v "0.7.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("error-context" "cargo" "derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rusty_oge") (r "^1.5.0") (d #t) (k 0)))) (h "0x9vvqajj5mdbshziipi7g6r40qvyq37pqyisqj5wpmf773k481p")))

