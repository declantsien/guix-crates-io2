(define-module (crates-io ru st rust-hdl-hls) #:use-module (crates-io))

(define-public crate-rust-hdl-hls-0.44.2 (c (n "rust-hdl-hls") (v "0.44.2") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.44.2") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.44.2") (d #t) (k 0)))) (h "12274yam0h8x2fhv93vp9jfysgdhivqcvb5734q36532izr89p9r")))

(define-public crate-rust-hdl-hls-0.45.0 (c (n "rust-hdl-hls") (v "0.45.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.45.0") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.45.0") (d #t) (k 0)))) (h "0pqqcib5gbzrfhnaq4zxyiwhgwwv1ilw9bdzsrk2gvgsvih5r0av")))

(define-public crate-rust-hdl-hls-0.45.1 (c (n "rust-hdl-hls") (v "0.45.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.45.1") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.45.1") (d #t) (k 0)))) (h "1akdq367i1pcdcnkqfr429cz3m201pxrazbyc591l49l8ca2w00r")))

(define-public crate-rust-hdl-hls-0.46.0 (c (n "rust-hdl-hls") (v "0.46.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.46.0") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.46.0") (d #t) (k 0)))) (h "0dwbxx3g6hgk39i20q3l8m9zc7xb93vnsb2ggb8gziscn5fsxjgw")))

