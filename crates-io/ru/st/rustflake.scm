(define-module (crates-io ru st rustflake) #:use-module (crates-io))

(define-public crate-rustflake-0.1.0 (c (n "rustflake") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)))) (h "0b227gp007lrhazlmil55fvkfcxli5p5jxy0x20caysrpf96s4bj")))

(define-public crate-rustflake-0.1.1 (c (n "rustflake") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)))) (h "155csmqan1df3mf2dzikydqr93m7rncwgbafnf0l0x2rgcya5liz")))

