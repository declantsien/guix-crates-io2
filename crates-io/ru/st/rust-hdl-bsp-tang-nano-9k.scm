(define-module (crates-io ru st rust-hdl-bsp-tang-nano-9k) #:use-module (crates-io))

(define-public crate-rust-hdl-bsp-tang-nano-9k-0.0.1 (c (n "rust-hdl-bsp-tang-nano-9k") (v "0.0.1") (d (list (d (n "rust-hdl") (r "^0.45.1") (f (quote ("fpga"))) (d #t) (k 0)) (d (n "rust-hdl-core") (r "^0.45.1") (d #t) (k 0)))) (h "1ni8f7sljppay3l29q0ghjn7y73gcnxqkz8g8kmvc4fcb1lvcv66") (f (quote (("yowasp") ("default"))))))

