(define-module (crates-io ru st rust-lcm-codegen) #:use-module (crates-io))

(define-public crate-rust-lcm-codegen-0.2.0 (c (n "rust-lcm-codegen") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1xi2yzb131kwp9qibjsmpn43kgdgcn4wlf55p4zz0l3df38in1sx")))

(define-public crate-rust-lcm-codegen-0.2.1 (c (n "rust-lcm-codegen") (v "0.2.1") (d (list (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0p5xbdpswqnlqskwz2i17nfrhksqmnr28ifx2vif56b0vqpvn3xr")))

