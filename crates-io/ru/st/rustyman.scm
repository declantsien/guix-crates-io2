(define-module (crates-io ru st rustyman) #:use-module (crates-io))

(define-public crate-rustyman-0.1.0 (c (n "rustyman") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "1bwjfgl9vp8zcvh0r11dyn8mryy9wlpjcgl79wmpk1qh81p80i87")))

(define-public crate-rustyman-0.2.0 (c (n "rustyman") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "04r1psjb1jllwvch4qjayfaxr9ghigbv06ygq0bja6mrqbqfaird")))

(define-public crate-rustyman-0.2.2 (c (n "rustyman") (v "0.2.2") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0l0yh53dcpddsdg0060d8sndbmy4nz2anl4kagzb9s3fl1m9d37c")))

(define-public crate-rustyman-0.2.3 (c (n "rustyman") (v "0.2.3") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1gcnhgvlvac5i2jvh53ma74nipn8vd9fcgmjsrwjxccwyh60201l")))

(define-public crate-rustyman-0.3.0 (c (n "rustyman") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1viqh2324wg0nc1qfy03a1jnigfgd6zdyyjy4qlj7yz5c3krl00y") (r "1.63.0")))

