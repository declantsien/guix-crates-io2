(define-module (crates-io ru st rust-openfmb-ops-protobuf) #:use-module (crates-io))

(define-public crate-rust-openfmb-ops-protobuf-1.0.1 (c (n "rust-openfmb-ops-protobuf") (v "1.0.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)))) (h "0679z29ciln82qlqi6yhq3zk3pg3dshn59v6qn3xn8g8dpzks5il")))

(define-public crate-rust-openfmb-ops-protobuf-1.0.2 (c (n "rust-openfmb-ops-protobuf") (v "1.0.2") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "1rjzmfr83nva60cz33xs40ry5lzb7j56qh4303rz054gkr77qwfw")))

