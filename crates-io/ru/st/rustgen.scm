(define-module (crates-io ru st rustgen) #:use-module (crates-io))

(define-public crate-rustgen-0.1.0 (c (n "rustgen") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0dlaxs6sw2p7iyw6iad23n0x5f6rj0rwh6xhdljkq1jw8c757n17")))

(define-public crate-rustgen-0.1.1 (c (n "rustgen") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1nxn5n8mm45cbrbcn9i7whz29nmf8pd6116c3i8bpfq61pslgmr8")))

(define-public crate-rustgen-0.2.0 (c (n "rustgen") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "12vx365yvy74minhlj60dpq4h6ybr4plq1j1gqj421l3alx07zix")))

(define-public crate-rustgen-0.2.1 (c (n "rustgen") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "05ggdzs8v2zi8w7bl3sv0r2adh6g90r8wkk27knczv6dr8pp0h5l")))

(define-public crate-rustgen-0.2.2 (c (n "rustgen") (v "0.2.2") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1zwzpbjp1qd1vbb8c3ziglwksznqxh28ra0hn6m54nclljjjg9zl")))

(define-public crate-rustgen-0.2.3 (c (n "rustgen") (v "0.2.3") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1q21pr3gdar5vgzc814g9bfsp2w711pkx20kx7gvwbrrgnv45xhh")))

(define-public crate-rustgen-0.2.4 (c (n "rustgen") (v "0.2.4") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "02c2fg6xbcg6ci87z8005l9lvsgkpc813jbsaxhdl8nyqaa5ak53")))

(define-public crate-rustgen-0.2.5 (c (n "rustgen") (v "0.2.5") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "convert_case") (r "0.4.*") (d #t) (k 0)) (d (n "handlebars") (r "3.5.*") (d #t) (k 0)) (d (n "regex") (r "1.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)))) (h "0bp3jpypla9897al978fq8icm8gc2326j1wjpqjjzfi27wyfygyr")))

(define-public crate-rustgen-0.2.6 (c (n "rustgen") (v "0.2.6") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "convert_case") (r "0.4.*") (d #t) (k 0)) (d (n "handlebars") (r "3.5.*") (d #t) (k 0)) (d (n "regex") (r "1.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)))) (h "1h2mc2d0zswl4rcdwq6ijl2k1yizw6frmba4yqw8sj8pbvspyi60")))

