(define-module (crates-io ru st rusty-peg) #:use-module (crates-io))

(define-public crate-rusty-peg-0.0.1 (c (n "rusty-peg") (v "0.0.1") (d (list (d (n "regex") (r "^0.1.30") (d #t) (k 0)))) (h "1javsfajph8rr42hl81sqsmbsx8vi6hkac2x0rz3q3c4ifq14sdq")))

(define-public crate-rusty-peg-0.0.2 (c (n "rusty-peg") (v "0.0.2") (d (list (d (n "regex") (r "^0.1.30") (d #t) (k 0)))) (h "09bwn8j2cg14iifr50a2b9l43ci7lkcgg66v859nzgx81b7fkka1")))

(define-public crate-rusty-peg-0.0.3 (c (n "rusty-peg") (v "0.0.3") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0v0mqqx7inpan5mhw7mxrvgahfxz5ysfdz779y2k2n39jygck2bp")))

(define-public crate-rusty-peg-0.4.0 (c (n "rusty-peg") (v "0.4.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0nlzh3k2qfdsc6a64pdxm6jc9gsc0v0g5y9k7i6d2i3s4q9hmwh0")))

(define-public crate-rusty-peg-0.2.0 (c (n "rusty-peg") (v "0.2.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "04dqrmhykq6d0530illifmylmsz0xlrqmcnh9hv9ah5kxrhnm94c")))

(define-public crate-rusty-peg-0.3.0 (c (n "rusty-peg") (v "0.3.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0qrsx3p9ldq522zspg2ci762pvsfnbccm8kzxa7631gkkxc6apfz")))

