(define-module (crates-io ru st rust_readability) #:use-module (crates-io))

(define-public crate-rust_readability-0.1.0 (c (n "rust_readability") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "punkt") (r "^1.0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "wordsworth") (r "0.1.*") (d #t) (k 0)))) (h "1q5h5mgpyx6nk7f43xpq61q5sbq5ljxhp0ncqmda0x0dnj9n8sqv")))

(define-public crate-rust_readability-0.1.1 (c (n "rust_readability") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "punkt") (r "^1.0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "wordsworth") (r "0.1.*") (d #t) (k 0)))) (h "1nzh0rfiqhpbghy2vzjhjvqczhgcsybw8pfhbmyl8kdjjl38f78x")))

(define-public crate-rust_readability-0.1.2 (c (n "rust_readability") (v "0.1.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "punkt") (r "^1.0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stop-words") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "wordsworth") (r "0.1.*") (d #t) (k 0)))) (h "05d0zprh6kbd512a1b5ias331iibqz81v2cashgggf1d7i4kjnk9")))

