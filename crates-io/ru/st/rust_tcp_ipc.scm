(define-module (crates-io ru st rust_tcp_ipc) #:use-module (crates-io))

(define-public crate-rust_tcp_ipc-0.2.0 (c (n "rust_tcp_ipc") (v "0.2.0") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1psfxc5d5p19prf1rpldyi9x7986s6s6j90czqnqw642pz67rs98")))

(define-public crate-rust_tcp_ipc-0.2.1 (c (n "rust_tcp_ipc") (v "0.2.1") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1bjr3yh4qrk9m5jnpk6y34br57wqp39zrx3ywhdp389crlqiy4g3")))

(define-public crate-rust_tcp_ipc-0.2.2 (c (n "rust_tcp_ipc") (v "0.2.2") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "0h4mdasxm8y8c53azl9rfczm9ky1r37v41wman0qd6ar6nphkhjf")))

(define-public crate-rust_tcp_ipc-0.2.3 (c (n "rust_tcp_ipc") (v "0.2.3") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1f5k9ajam2z81qsyvnvayfyr2g9vcphhskx9vlf0gkzv18j7an7v")))

(define-public crate-rust_tcp_ipc-0.2.4 (c (n "rust_tcp_ipc") (v "0.2.4") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "0b3j8li4j58cliryq39m4p9zvz0i2jxkrdxdyansj57kdl918grk")))

(define-public crate-rust_tcp_ipc-0.2.5 (c (n "rust_tcp_ipc") (v "0.2.5") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "04b7mkxban5x51qrd2h3y1dw7wnr5dpzm9blfdniwpvw4jmn10yx")))

(define-public crate-rust_tcp_ipc-0.2.6 (c (n "rust_tcp_ipc") (v "0.2.6") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1lhz73kax2yj7bjkvciv8g5dsvw6jgax6hjwqg52wylmbcrjz0wc")))

(define-public crate-rust_tcp_ipc-0.2.7 (c (n "rust_tcp_ipc") (v "0.2.7") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1r0148llnilpvp5z0850mkascyw9ayl640a5zcyzrjlwy1vgz8pi")))

(define-public crate-rust_tcp_ipc-0.2.8 (c (n "rust_tcp_ipc") (v "0.2.8") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1qi1xdas60h1agjhzj0sni0f8l9s3r3qdrwdqi92hmrm6ba71d1x")))

(define-public crate-rust_tcp_ipc-0.2.9 (c (n "rust_tcp_ipc") (v "0.2.9") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1i6ln9y7lqsmd8fjmwnn4n1bbziq9bxjibhwrdjg6z9y1hh205mw")))

(define-public crate-rust_tcp_ipc-0.3.0 (c (n "rust_tcp_ipc") (v "0.3.0") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "1z3znf404k14jrk3fwy0jnnvy30r3yqbcv1k79kpfjcclk114av0")))

(define-public crate-rust_tcp_ipc-0.3.1 (c (n "rust_tcp_ipc") (v "0.3.1") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 2)))) (h "0f22ba7nzb2r5a76vclry3lf3g60sf4g0cnndl1zxxk99kbpwnki")))

