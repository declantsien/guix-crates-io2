(define-module (crates-io ru st rustfinity) #:use-module (crates-io))

(define-public crate-rustfinity-0.1.0 (c (n "rustfinity") (v "0.1.0") (h "08c41bmjjfw9kn2vbq9qizi0j7msncxrbxq1y5pv52sgkij21jhj") (y #t)))

(define-public crate-rustfinity-0.2.0 (c (n "rustfinity") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dload") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y8p7ihxd4xdlcny5dq4d2jnpaisp5bggb8qz05w28dzxc81smda")))

(define-public crate-rustfinity-0.2.1 (c (n "rustfinity") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dload") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03f0ihmd5n0m304a5r9clyrbmwhgd4xn5iy9jdn1h1xw50pi5sx5")))

(define-public crate-rustfinity-0.2.2 (c (n "rustfinity") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dload") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wnyind4nvbkl46q41j74jr0sm8wwxv7rbxljzv3s44l4p7jyrln")))

(define-public crate-rustfinity-0.2.3 (c (n "rustfinity") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dload") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1br9qiwskv28fs4jif3anfzi9j5ngrx3y6m3mzldbvqfybsyz0j5")))

(define-public crate-rustfinity-0.2.4 (c (n "rustfinity") (v "0.2.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dload") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikfkqgmvil710av0vyhqf9vb8s9mvzx1nn2l083vvhzji1hpxkl")))

(define-public crate-rustfinity-0.2.5 (c (n "rustfinity") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dload") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0230spsngdfij5nb9g1x0y0gp638gd3yk1clasfd62w0ysfgmcii")))

(define-public crate-rustfinity-0.2.6 (c (n "rustfinity") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dload") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a2cg0ngpkb1slma1gzkqx0k9j54y44x8lfc49m01nzhm9z15l50")))

