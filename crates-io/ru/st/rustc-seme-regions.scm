(define-module (crates-io ru st rustc-seme-regions) #:use-module (crates-io))

(define-public crate-rustc-seme-regions-0.1.0 (c (n "rustc-seme-regions") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.4.12") (d #t) (k 2)))) (h "1ncqzbh0gpknjl58brid0465hm5492d0lkcy9kamzl02c3f8xzlf")))

(define-public crate-rustc-seme-regions-0.2.0 (c (n "rustc-seme-regions") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.4.12") (d #t) (k 2)))) (h "1gmh2j23cjdly6lgqm55pim8rbxz5n8rmq4j79mdcjx31i18jk67")))

(define-public crate-rustc-seme-regions-0.3.0 (c (n "rustc-seme-regions") (v "0.3.0") (d (list (d (n "petgraph") (r "^0.4.12") (d #t) (k 2)))) (h "1chabqqp7xqp2jdpdgn1zj66iwkvl109iqcyhf02limarps973cs")))

(define-public crate-rustc-seme-regions-0.4.0 (c (n "rustc-seme-regions") (v "0.4.0") (d (list (d (n "petgraph") (r "^0.4.12") (d #t) (k 2)))) (h "0bbz4k2r0qbpbm26br8pkh67ra1dgl64njn98v0r75yklqpwgxw1")))

