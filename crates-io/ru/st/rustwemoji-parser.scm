(define-module (crates-io ru st rustwemoji-parser) #:use-module (crates-io))

(define-public crate-rustwemoji-parser-0.1.0 (c (n "rustwemoji-parser") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "rustwemoji") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "macros"))) (o #t) (d #t) (k 0)))) (h "1c63gfsrih9phrnm43sw1abpshlrag645axdc7k8pwbfkagmkrax") (f (quote (("discord") ("default") ("async")))) (s 2) (e (quote (("tokio" "dep:tokio" "async") ("async-std" "dep:async-std" "async"))))))

(define-public crate-rustwemoji-parser-0.1.1 (c (n "rustwemoji-parser") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "rustwemoji") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "macros"))) (o #t) (d #t) (k 0)))) (h "15q6y18z6d2i7zhhv50vq94k9231vmd4k44r849byys02andhpki") (f (quote (("discord") ("default") ("async")))) (s 2) (e (quote (("tokio" "dep:tokio" "async") ("async-std" "dep:async-std" "async"))))))

