(define-module (crates-io ru st rusty-gql-macro) #:use-module (crates-io))

(define-public crate-rusty-gql-macro-0.1.0-alpha (c (n "rusty-gql-macro") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("fs" "io-std" "io-util" "rt-multi-thread" "sync" "signal" "macros"))) (d #t) (k 2)))) (h "1nv0yqcv7hbfp5cf2w3x05fyi1yhdww2dfw8gfn4c2wb7xzfca9j")))

(define-public crate-rusty-gql-macro-0.1.0 (c (n "rusty-gql-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("fs" "io-std" "io-util" "rt-multi-thread" "sync" "signal" "macros"))) (d #t) (k 2)))) (h "0zshc19rrng9kpn6yklcxzmpgx5p8xfmv8jhila65dna070w7y2v")))

(define-public crate-rusty-gql-macro-0.1.1 (c (n "rusty-gql-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("fs" "io-std" "io-util" "rt-multi-thread" "sync" "signal" "macros"))) (d #t) (k 2)))) (h "1bkyjzjbkzmqh2y6zx8asid04a79qpg1jan1vch3rsdcvk0b7xhc")))

(define-public crate-rusty-gql-macro-0.1.2 (c (n "rusty-gql-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("fs" "io-std" "io-util" "rt-multi-thread" "sync" "signal" "macros"))) (d #t) (k 2)))) (h "0jfz1lkysmgs7wxpqidm6af2k5085aiqfpybzsab3ziwvyb8138w")))

