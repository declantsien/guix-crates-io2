(define-module (crates-io ru st rust-anonfiles-api) #:use-module (crates-io))

(define-public crate-rust-anonfiles-api-0.1.0 (c (n "rust-anonfiles-api") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1g2ddknl9ba9s91xfk5f0cmcb5mqs33ny0dsl6ymwbl0v1n8zq6j")))

(define-public crate-rust-anonfiles-api-0.1.1 (c (n "rust-anonfiles-api") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.43") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "17zs3w8nxab6g1l8765jdprary7fhfmawk6w9v8bns2vmxnw5gzg")))

