(define-module (crates-io ru st rusty_qjs) #:use-module (crates-io))

(define-public crate-rusty_qjs-0.0.1 (c (n "rusty_qjs") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xfyd4jdqr5azr9p4cqvrrgdiavabvfcal2rhg56csslld4zhgz0") (f (quote (("local"))))))

