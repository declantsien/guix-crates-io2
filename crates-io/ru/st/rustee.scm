(define-module (crates-io ru st rustee) #:use-module (crates-io))

(define-public crate-rustee-0.1.0 (c (n "rustee") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "069ibcf7wd79b0za5w1ypaylpxnapq9qvhf77ciznpxy43ckqbnw")))

