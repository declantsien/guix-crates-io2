(define-module (crates-io ru st rustc-ap-proc_macro) #:use-module (crates-io))

(define-public crate-rustc-ap-proc_macro-40.0.0 (c (n "rustc-ap-proc_macro") (v "40.0.0") (d (list (d (n "rustc-ap-rustc_errors") (r "^40.0.0") (d #t) (k 0)) (d (n "rustc-ap-syntax") (r "^40.0.0") (d #t) (k 0)) (d (n "rustc-ap-syntax_pos") (r "^40.0.0") (d #t) (k 0)))) (h "1zrif9cc0nl5qj6f2whi8xss39d2bcvlmbqwirhjascas5yh2vyr")))

