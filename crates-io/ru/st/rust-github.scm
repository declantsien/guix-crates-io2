(define-module (crates-io ru st rust-github) #:use-module (crates-io))

(define-public crate-rust-github-0.1.0 (c (n "rust-github") (v "0.1.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cph594yjipwgpskxz10z0kizqgahavrfmjpnphbpfyqv3hrh9gb")))

(define-public crate-rust-github-0.1.1 (c (n "rust-github") (v "0.1.1") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1q69lxrf7jlfxvzjqacdxi3dv8pkxl42h225zjksc47vhqqci9fv")))

