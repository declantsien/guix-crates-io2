(define-module (crates-io ru st rustmex_octave) #:use-module (crates-io))

(define-public crate-rustmex_octave-0.1.0 (c (n "rustmex_octave") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.1") (d #t) (k 0)))) (h "1zvayfqg402qh7qdv7blmv3544nyvxlpsq46pp1n8z3p5qsb5vkm") (l "mexoctave")))

(define-public crate-rustmex_octave-0.1.2 (c (n "rustmex_octave") (v "0.1.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.2") (d #t) (k 0)))) (h "09f2xm2pxadi0m305za4k1gh7wbqlv2waz7048ykgxq6gh8i4pzz") (l "mexoctave")))

