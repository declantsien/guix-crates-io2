(define-module (crates-io ru st rustracing) #:use-module (crates-io))

(define-public crate-rustracing-0.1.0 (c (n "rustracing") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0gmpv1hfalc09qjpfsd6im926h6hll9l4369wfnh5l2inhkf3xa7")))

(define-public crate-rustracing-0.1.1 (c (n "rustracing") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0c2yi02qyh9vbncplgi58hd2h7xaxv1y93n0ypjk3x1kzx3xkwyf")))

(define-public crate-rustracing-0.1.2 (c (n "rustracing") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1i4c1ka1qz9dfkw5ljkc01dfp4w7vnzh1ls18qaakzqsc9z0wgq5")))

(define-public crate-rustracing-0.1.3 (c (n "rustracing") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1283v6k0ggw1rixnv1yanh1hcvv8zhl1p9jify6dda3mdhc54h6k")))

(define-public crate-rustracing-0.1.4 (c (n "rustracing") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "12i3ir8h31id0mqxkrzrzr057ijw37zrsl67sh7k41fza326z6jq")))

(define-public crate-rustracing-0.1.5 (c (n "rustracing") (v "0.1.5") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0iky94hp8zffs47pv63pq4nayjld3320005k32xw9vhbl2fb3nll") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.1.6 (c (n "rustracing") (v "0.1.6") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "04q16n2hl4mayis3ll3y3jqqisw6b4b5kd7i97r5bidkqcdiilgh") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.1.7 (c (n "rustracing") (v "0.1.7") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "15ss293pbayj0j6pk2liphns6jfbsghyf6637i9dsxcwz016ylv5") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.1.8 (c (n "rustracing") (v "0.1.8") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1vrqhh01h813jgvnrm60ma2x6d7cyssvr6h4m2rk1q3mfp3w38nx") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.2.0 (c (n "rustracing") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0s027vk60ksgj1m681hl07mdf0asc2d5vwj77ka1fv04zsv6kaay") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.1.9 (c (n "rustracing") (v "0.1.9") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0hy9ny6blqpjzpvhabfp2bnz7splkj4vcwwbxx34fiij65bv74ff") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.1.10 (c (n "rustracing") (v "0.1.10") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "04jzkbq4dyjzd40pdpla6m18kwy9gai0h8j4wizw504gqijcx9yv") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.1.11 (c (n "rustracing") (v "0.1.11") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0v19jq1pkwb5nwfb1kfh2arvk0f4f1w9h1d62xrz49ji8bka233z") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.2.1 (c (n "rustracing") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1p1xfnmbzcxjmvx5d0601gms30qmw5jj2pivxhxky30lzsydncqh") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.3.0 (c (n "rustracing") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0q73sxkly1aqilpxlp9q3vn9m36557mq7nisvahg7naiyzqfh11m") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.4.0 (c (n "rustracing") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0171kylh6ihpghkw2hkb3svsvqbz90fspxnhh2bljabwrhdxhpz2") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.5.0 (c (n "rustracing") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "09kfzkk5jdxv35n574nii2l4p8qm21bm4y4xsb4flqn0yk4mpf97") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.5.1 (c (n "rustracing") (v "0.5.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "199n7nqxnfyds50a1bmd5ksgw96780g8jayyd545fph91jqj4j54") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

(define-public crate-rustracing-0.6.0 (c (n "rustracing") (v "0.6.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "trackable") (r "^1.2") (d #t) (k 0)))) (h "007wsfx01y7lcqls5hs924iyarajnkp1d4mfblgw3bxqgz9zb4qp") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

