(define-module (crates-io ru st rusty_backtest) #:use-module (crates-io))

(define-public crate-rusty_backtest-0.0.1 (c (n "rusty_backtest") (v "0.0.1") (h "1rfjblf319bpmyh63q55ikk7cniqf5jh63ggpkcldv9ya8lcp6n9")))

(define-public crate-rusty_backtest-0.0.2 (c (n "rusty_backtest") (v "0.0.2") (h "0zzvqakir1fiz8kan9yds1vksikqqg9l6hnvbb8qnxds6qrsw25r")))

(define-public crate-rusty_backtest-0.0.3 (c (n "rusty_backtest") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01akl9f9kwbrj7ygl29lmvra12rdz10ryxlk60p5y955l636jv2n")))

