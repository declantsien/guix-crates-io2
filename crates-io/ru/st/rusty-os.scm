(define-module (crates-io ru st rusty-os) #:use-module (crates-io))

(define-public crate-rusty-os-0.1.0 (c (n "rusty-os") (v "0.1.0") (h "1arli2g2hwqla5pdfl4va3njv9n3vpk05rwif493sykbm44yh7a3")))

(define-public crate-rusty-os-1.0.0 (c (n "rusty-os") (v "1.0.0") (d (list (d (n "bootloader") (r "^0.9.23") (d #t) (k 0)))) (h "16ycbz6n6mmxswgfcjginl3s9gwp12z8yjmr503gd2h1pqq0l2ll")))

(define-public crate-rusty-os-1.1.0 (c (n "rusty-os") (v "1.1.0") (d (list (d (n "bootloader") (r "^0.9.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.0") (d #t) (k 0)) (d (n "volatile") (r "^0.2.6") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.2") (d #t) (k 0)))) (h "1i5s4f8vvgd81d8nbw80fln4lm3d2k5vymyrz7s07f4g6avzwy1a")))

