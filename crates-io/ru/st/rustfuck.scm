(define-module (crates-io ru st rustfuck) #:use-module (crates-io))

(define-public crate-rustfuck-0.1.0 (c (n "rustfuck") (v "0.1.0") (h "1mpbp7k0d1n6adajq04b7mbhpq2ssnk1rx80xqs7yzh9qjdvrwvg")))

(define-public crate-rustfuck-0.1.1 (c (n "rustfuck") (v "0.1.1") (h "1wgc80ig4xwszjdfzj7ljr0z9x3qhr7ikkmr09bfvgy16mlx5l7x")))

(define-public crate-rustfuck-0.1.2 (c (n "rustfuck") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "0p57mhzja2vz6xj19j5g86x91z2a4pmdivlbh5zzdbxn0fa9bmjy")))

(define-public crate-rustfuck-0.1.3 (c (n "rustfuck") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0vb4kvvvn2lhcpcm5w578l67z4g8gg3zii5bcv5zf16i2a86i58c") (y #t)))

