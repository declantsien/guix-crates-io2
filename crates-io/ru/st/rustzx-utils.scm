(define-module (crates-io ru st rustzx-utils) #:use-module (crates-io))

(define-public crate-rustzx-utils-0.14.0 (c (n "rustzx-utils") (v "0.14.0") (d (list (d (n "rustzx-core") (r "^0.14") (d #t) (k 0)))) (h "01qhkgcr3x6wjckz2fj95xymm62bkxzp8hc8lq0a7dga8p7dxz1d") (f (quote (("std") ("default"))))))

(define-public crate-rustzx-utils-0.15.0 (c (n "rustzx-utils") (v "0.15.0") (d (list (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustzx-core") (r "^0.15") (d #t) (k 0)))) (h "1kxzq79na6vjs5xl52m94fvx8snwsyvlwlza3ad4jshfwpx596d3") (f (quote (("std" "log" "flate2") ("default"))))))

(define-public crate-rustzx-utils-0.16.0 (c (n "rustzx-utils") (v "0.16.0") (d (list (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rustzx-core") (r "^0.16.0") (d #t) (k 0)))) (h "12150cm2zm9nd39hll5zq5hyib2h088hgp5aw3w0sf6d5qvb0nvc") (f (quote (("std" "log" "flate2") ("default"))))))

