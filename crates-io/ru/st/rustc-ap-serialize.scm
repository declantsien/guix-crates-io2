(define-module (crates-io ru st rustc-ap-serialize) #:use-module (crates-io))

(define-public crate-rustc-ap-serialize-1.0.0 (c (n "rustc-ap-serialize") (v "1.0.0") (h "18s6zfky6hyz25xlzmxrw21akzr01na5sdn1jimnhkdhyj4vbarb")))

(define-public crate-rustc-ap-serialize-2.0.0 (c (n "rustc-ap-serialize") (v "2.0.0") (h "1kfnm52gl1kv2irmfylpc135v2xnin92lfqxab1yd1qxmcgdjnz1")))

(define-public crate-rustc-ap-serialize-3.0.0 (c (n "rustc-ap-serialize") (v "3.0.0") (h "0j8ny86clg13pm07jrh9grbgpnghicr0sb978600wi4i2sh6xh73")))

(define-public crate-rustc-ap-serialize-4.0.0 (c (n "rustc-ap-serialize") (v "4.0.0") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0dz8b5q0zlgvc717za1zj9w4l6lh2q4wficgrzaacxyzipr2wp3v")))

(define-public crate-rustc-ap-serialize-5.0.0 (c (n "rustc-ap-serialize") (v "5.0.0") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0yqnkqldf1sj4cwdkabhqif0hzbrhlg3a62s943ngy97z4jkx45s")))

(define-public crate-rustc-ap-serialize-6.0.0 (c (n "rustc-ap-serialize") (v "6.0.0") (h "1v2ip38x2kasa8fpiw20fdcgaal4p4wiwa514nvgl2njjd50rg87")))

(define-public crate-rustc-ap-serialize-7.0.0 (c (n "rustc-ap-serialize") (v "7.0.0") (h "1qflarf2l3bscrwqg7smm4bwhp09wag1fg49nxgwfyaqq3gjphgc")))

(define-public crate-rustc-ap-serialize-8.0.0 (c (n "rustc-ap-serialize") (v "8.0.0") (h "18jvih46xwfsyhyg1w051jnknra6z2hwzziglksk690lnnifcv9r")))

(define-public crate-rustc-ap-serialize-9.0.0 (c (n "rustc-ap-serialize") (v "9.0.0") (h "1p1hiwxf6zc2j910592ax4h62hn74gdizyrrkwq3y6wmwirb4hph")))

(define-public crate-rustc-ap-serialize-10.0.0 (c (n "rustc-ap-serialize") (v "10.0.0") (h "01qfrmyngi37ya0kf3m76dm59day8az75iminixm0xv8c0p38sjn")))

(define-public crate-rustc-ap-serialize-11.0.0 (c (n "rustc-ap-serialize") (v "11.0.0") (h "14snpf7l9qffwb3iy2cjwbc6d0vybm0ln2f954wfs41dx9pmg1vs")))

(define-public crate-rustc-ap-serialize-12.0.0 (c (n "rustc-ap-serialize") (v "12.0.0") (h "0176d74smjs4h6sjbf33ksq6z4x845b2dxrhz0mmqrwqw98pykkb")))

(define-public crate-rustc-ap-serialize-13.0.0 (c (n "rustc-ap-serialize") (v "13.0.0") (h "0jlsgl6fk6433h4zps5gkiwbq2vcjkfknfhan4q75ci8ljf63jqq")))

(define-public crate-rustc-ap-serialize-14.0.0 (c (n "rustc-ap-serialize") (v "14.0.0") (h "0ccj9fvmgzl8sya1p7mgcbyc1nvymnb0xnrql0x9z0zrfk4kal7h")))

(define-public crate-rustc-ap-serialize-15.0.0 (c (n "rustc-ap-serialize") (v "15.0.0") (h "1hr69afnxa5lfmz2s2dllsqh3iv61dwlld2rb15qzk5rar1crfkf")))

(define-public crate-rustc-ap-serialize-16.0.0 (c (n "rustc-ap-serialize") (v "16.0.0") (h "1qpwlmgxynjn6pzlp6v2r24w3hiapi3dkpp5cqp8m37cfjx3bh15")))

(define-public crate-rustc-ap-serialize-17.0.0 (c (n "rustc-ap-serialize") (v "17.0.0") (h "1wb8fbi9kmkns2wszfwh27yv3kqcpcx7jl3m0804dlm3hhq8rm5v")))

(define-public crate-rustc-ap-serialize-18.0.0 (c (n "rustc-ap-serialize") (v "18.0.0") (h "1izha4s6ljl3if39kwzxpi4m3ad4hpg76n0xjzx0707h7g2d77i9")))

(define-public crate-rustc-ap-serialize-19.0.0 (c (n "rustc-ap-serialize") (v "19.0.0") (h "0lidcrr923kkl633v3l1awnh2f6ijwb0jz9z0z8aghqrkc3nfq72")))

(define-public crate-rustc-ap-serialize-20.0.0 (c (n "rustc-ap-serialize") (v "20.0.0") (h "1sw3c2cjs2hh5akfmp40wpfdbqb8d6a2d44g0np79syh72h37yna")))

(define-public crate-rustc-ap-serialize-21.0.0 (c (n "rustc-ap-serialize") (v "21.0.0") (h "0rd92nzjffm747ki983h3b15jvggm676k5px9ydqmjmh347mzy5q")))

(define-public crate-rustc-ap-serialize-22.0.0 (c (n "rustc-ap-serialize") (v "22.0.0") (h "1g0649barplzqqlf4g22p4af2b6ayhg3rzfhgc347g5l61kddym8")))

(define-public crate-rustc-ap-serialize-23.0.0 (c (n "rustc-ap-serialize") (v "23.0.0") (h "0a62a14p7fhcrf3dvhinwsw137j4yn507vyd3zl93f5xwzcycg0i")))

(define-public crate-rustc-ap-serialize-24.0.0 (c (n "rustc-ap-serialize") (v "24.0.0") (h "1ap9vb1ilyxpq006czi2sxw5j1fsdx6xkywckaqqdhgc7riwd4f5")))

(define-public crate-rustc-ap-serialize-25.0.0 (c (n "rustc-ap-serialize") (v "25.0.0") (h "02w0xcysiqfgbdsgz639yjnfh7vqd3f5jxdf7gipfrws5f39qhpi")))

(define-public crate-rustc-ap-serialize-26.0.0 (c (n "rustc-ap-serialize") (v "26.0.0") (h "01cxq26vgkad984pjfajaczv0dpvl0j3mii25nn0cbkd4b4apamj")))

(define-public crate-rustc-ap-serialize-27.0.0 (c (n "rustc-ap-serialize") (v "27.0.0") (h "10kgx4lygm3pigc13xj1wcmair7p886jpicpn4h7ssrjcpk8xq3k")))

(define-public crate-rustc-ap-serialize-28.0.0 (c (n "rustc-ap-serialize") (v "28.0.0") (h "1biv4i4i78qwx8g2spgfdcbsqyip94d69ld6dc8ysxdqfz30phan")))

(define-public crate-rustc-ap-serialize-29.0.0 (c (n "rustc-ap-serialize") (v "29.0.0") (h "0yrs5ivr95c2kj9b7mvx6w19b91wrqswydlrd6vc8hgz8nj5yx70")))

(define-public crate-rustc-ap-serialize-30.0.0 (c (n "rustc-ap-serialize") (v "30.0.0") (h "1fv9l6z8y8263r79dzf7y816dmw59jisiwnfbgqq83xbga6pv9xp")))

(define-public crate-rustc-ap-serialize-31.0.0 (c (n "rustc-ap-serialize") (v "31.0.0") (h "1big7ryjmck7n8yjxckvfx0p3m2p5fyvzzdfnkhzy18i022r2hdn")))

(define-public crate-rustc-ap-serialize-32.0.0 (c (n "rustc-ap-serialize") (v "32.0.0") (h "1l9wpg231k8q9dr388a77i3rpmb5c006n5423fbdpn6p3yfp359g")))

(define-public crate-rustc-ap-serialize-33.0.0 (c (n "rustc-ap-serialize") (v "33.0.0") (h "0ys4l5biadrbc90d4vs8iqq4j715710zh9i2l4xp32n4f6vd8ay5")))

(define-public crate-rustc-ap-serialize-34.0.0 (c (n "rustc-ap-serialize") (v "34.0.0") (h "0y7spm8cwj6qrpc5jagnl0krr8an51fdlnmbi2y7mdrszxf9ilxf")))

(define-public crate-rustc-ap-serialize-35.0.0 (c (n "rustc-ap-serialize") (v "35.0.0") (h "19a0zbvggl30aj43q0455v8g5qqsb26hpzhd20amfqry1br7jb2b")))

(define-public crate-rustc-ap-serialize-36.0.0 (c (n "rustc-ap-serialize") (v "36.0.0") (h "1w2i2m0n8vf4jbgbbhapnasn632inw2jjcqgc16r939wzphxkkwi")))

(define-public crate-rustc-ap-serialize-37.0.0 (c (n "rustc-ap-serialize") (v "37.0.0") (h "0qg88w8x4k9941dhvjp7qd52lhp45jslih5cmp0a2a7gsnyybyci")))

(define-public crate-rustc-ap-serialize-38.0.0 (c (n "rustc-ap-serialize") (v "38.0.0") (h "1p29ag8bsgbxf514qc7mnsanms2jxbna6bsh6vb4yhilsx0ssxj4")))

(define-public crate-rustc-ap-serialize-39.0.0 (c (n "rustc-ap-serialize") (v "39.0.0") (h "1wqdj3cl66c2ddzbbhq4mc9h6hkb2wv5dyzg61x5i9dm63sl2h4b")))

(define-public crate-rustc-ap-serialize-40.0.0 (c (n "rustc-ap-serialize") (v "40.0.0") (h "02vyfdmx95yqw5k6flszwibkq1wcs2chgqa8zg9wc7nynfqjglj3")))

(define-public crate-rustc-ap-serialize-41.0.0 (c (n "rustc-ap-serialize") (v "41.0.0") (h "0y88fp7b4qsrnsrbp8k5001n9drqm4xaybq5p44r9cq2rb5fy03p")))

(define-public crate-rustc-ap-serialize-42.0.0 (c (n "rustc-ap-serialize") (v "42.0.0") (h "04l3qv88yl55b61dsaqxz2pvmf5z4ymm001qbg4cnda36nmr1pc5")))

(define-public crate-rustc-ap-serialize-43.0.0 (c (n "rustc-ap-serialize") (v "43.0.0") (h "0ikk8n5xbj1f12j13zwp8qvdp7abnmwjb3cfk5yhp0xb569s3r0q")))

(define-public crate-rustc-ap-serialize-44.0.0 (c (n "rustc-ap-serialize") (v "44.0.0") (h "1z1s9yjhbkq6h4mmkvwcarp7ld5vkx1l07li0mzwv7d8h8wvhlzs")))

(define-public crate-rustc-ap-serialize-45.0.0 (c (n "rustc-ap-serialize") (v "45.0.0") (h "0bd2xb8gim1450j8c05f692ccigg3zm4bl9ky70aa4n2pxli2b1i")))

(define-public crate-rustc-ap-serialize-46.0.0 (c (n "rustc-ap-serialize") (v "46.0.0") (h "0npav2r8hf0iwsd1hy29ik8hr716g9jgagzpfqyd6h0dm1vp2hi9")))

(define-public crate-rustc-ap-serialize-47.0.0 (c (n "rustc-ap-serialize") (v "47.0.0") (h "00j7x2njjij8vrpzsba5rx7pds57rli86fv7z1l47glhjrvwvbfg")))

(define-public crate-rustc-ap-serialize-48.0.0 (c (n "rustc-ap-serialize") (v "48.0.0") (h "1llqlkbgprdwzan2yklvf7768p381xfba1r74sv4qm62dszzk6ia")))

(define-public crate-rustc-ap-serialize-49.0.0 (c (n "rustc-ap-serialize") (v "49.0.0") (h "1phajk8d873npzfq2isysk0yfirlyqhddzj2fy850ql3qzkkjnnh")))

(define-public crate-rustc-ap-serialize-50.0.0 (c (n "rustc-ap-serialize") (v "50.0.0") (h "0l9a42801lh41fp235caf765izix2nqb2z4wqdf8p2ykqnj7b6ak")))

(define-public crate-rustc-ap-serialize-51.0.0 (c (n "rustc-ap-serialize") (v "51.0.0") (h "1bpv4g8grq4a1vw044qm5s98c56pcpjvdlg4r9nhlk7aqw4nd0x5")))

(define-public crate-rustc-ap-serialize-52.0.0 (c (n "rustc-ap-serialize") (v "52.0.0") (h "0bpdis2y7pfqbf388iyn7cx3f6s3xdkq57p8n1lhb4axm6hs1b1q")))

(define-public crate-rustc-ap-serialize-53.0.0 (c (n "rustc-ap-serialize") (v "53.0.0") (h "0pbdqi5fwjrp5bp1nwki2i34x2wmwxhhw76z9vip3vql4bh5jqac")))

(define-public crate-rustc-ap-serialize-54.0.0 (c (n "rustc-ap-serialize") (v "54.0.0") (h "06ibivcnwdp6qabx4xhca3d0wksz3ji5gb6llrx9g1madnybr5l2")))

(define-public crate-rustc-ap-serialize-55.0.0 (c (n "rustc-ap-serialize") (v "55.0.0") (h "01fs7pg4j9bp5jd2knz3vs9ipwaxbsaklj2ldsbx6yrzs1ygii9j")))

(define-public crate-rustc-ap-serialize-56.0.0 (c (n "rustc-ap-serialize") (v "56.0.0") (h "0k0019yjfvdi268nw6152xbdy0lh274w886ikr827si091yw24dv")))

(define-public crate-rustc-ap-serialize-57.0.0 (c (n "rustc-ap-serialize") (v "57.0.0") (h "0n68azvycj4dmp2a9rgnsrhg7xc5hh98dhwdnshjmnk6hbhyy5c4")))

(define-public crate-rustc-ap-serialize-58.0.0 (c (n "rustc-ap-serialize") (v "58.0.0") (h "1aq99s4ij2w44jpi5gc69ylixkvh4aky7n666w5gp8lh2i6l08hz")))

(define-public crate-rustc-ap-serialize-59.0.0 (c (n "rustc-ap-serialize") (v "59.0.0") (h "15vgfcxdb9n6khijg7ndyx053kngr0izv4z3v5bwczzisjjd1s9l")))

(define-public crate-rustc-ap-serialize-60.0.0 (c (n "rustc-ap-serialize") (v "60.0.0") (h "1djf9is47qbkk7wrb6q4q9kjyk8cjgqml6604v7y6mxvv5pxwfkk")))

(define-public crate-rustc-ap-serialize-61.0.0 (c (n "rustc-ap-serialize") (v "61.0.0") (h "0g037fkbnmsrcnvvqf5bbmq0wk8kmmll56c4n60hprqbd5cp1bjb")))

(define-public crate-rustc-ap-serialize-62.0.0 (c (n "rustc-ap-serialize") (v "62.0.0") (h "1789gg1jkjg2m2mwwp20y25z595yifwj8wwb2fsaz41iacgfx2qp")))

(define-public crate-rustc-ap-serialize-63.0.0 (c (n "rustc-ap-serialize") (v "63.0.0") (h "08hxwz14nnhm5gsyhw7pjpgdshg5fw54hl3qzimydz0ffs35lcc9")))

(define-public crate-rustc-ap-serialize-64.0.0 (c (n "rustc-ap-serialize") (v "64.0.0") (h "0c5kcgd6f2d8lzq46n3yh1d2afs6pf8fk362y6fsvakn5p299dgy")))

(define-public crate-rustc-ap-serialize-65.0.0 (c (n "rustc-ap-serialize") (v "65.0.0") (h "0dk9mf9k8cl3ihyfqy4x6ls20g1fk615aym7x87gamh66pzqxrg5")))

(define-public crate-rustc-ap-serialize-66.0.0 (c (n "rustc-ap-serialize") (v "66.0.0") (h "0k32w79qshzcd8xhq6ip3hsln474yc0mfifllimkx8lj09w254sx")))

(define-public crate-rustc-ap-serialize-67.0.0 (c (n "rustc-ap-serialize") (v "67.0.0") (h "1vf5vykzwh85yv6lp8f0awc395wip5gsw0pjmhf7jb9kzi0fw4jj")))

(define-public crate-rustc-ap-serialize-68.0.0 (c (n "rustc-ap-serialize") (v "68.0.0") (h "12fww89kymkm66gx6y8fbxnka5q2h8a13ngg6s0bzgdcwgxj46rl")))

(define-public crate-rustc-ap-serialize-69.0.0 (c (n "rustc-ap-serialize") (v "69.0.0") (h "1sylsqhx718q27ixyn86yix8hblvv6dvschmiv19ybilrxaqi8iw")))

(define-public crate-rustc-ap-serialize-70.0.0 (c (n "rustc-ap-serialize") (v "70.0.0") (h "1anaxhb1qj85d64r9swf1hqlfbhvh3z9i1macn9c0h8xk9f08jqj")))

(define-public crate-rustc-ap-serialize-71.0.0 (c (n "rustc-ap-serialize") (v "71.0.0") (h "16j1sdl8ga6z1sfjhwhwwilnp1akxw9q6n6qgrsgb8alps6kixfw")))

(define-public crate-rustc-ap-serialize-72.0.0 (c (n "rustc-ap-serialize") (v "72.0.0") (h "1k2sn2sc67d0ws218dhjgkglhkiwc0vh4azn5m44fs5psh2s3gq0")))

(define-public crate-rustc-ap-serialize-73.0.0 (c (n "rustc-ap-serialize") (v "73.0.0") (h "0q6fd8zfixmz9z9m1syiar5d60ml13zy5spscdgwlp95mq0kk539")))

(define-public crate-rustc-ap-serialize-74.0.0 (c (n "rustc-ap-serialize") (v "74.0.0") (h "0c6r7a37g4i8cnmp67g9869p37csvz6nnj6yvc3xhv39n1pvwgva")))

(define-public crate-rustc-ap-serialize-75.0.0 (c (n "rustc-ap-serialize") (v "75.0.0") (h "1a7fj1vn3qbpb0w3z6vr9vl7philcl9qg4wjprp9v498pnca4ghb")))

(define-public crate-rustc-ap-serialize-76.0.0 (c (n "rustc-ap-serialize") (v "76.0.0") (h "05wc5v1ls08zmcx3ql4jxi615lpzzy8h0j2livh4xy7wkyn9g5nx")))

(define-public crate-rustc-ap-serialize-77.0.0 (c (n "rustc-ap-serialize") (v "77.0.0") (h "09xy3zdyn97wqhzlh3zskvqzf3b3f7jba3jrqr81xf6i8346xz99")))

(define-public crate-rustc-ap-serialize-78.0.0 (c (n "rustc-ap-serialize") (v "78.0.0") (h "097vn1rg7wjbfy4k1kna40584n0wnmcgsxc7bp4bi12wnfyb92ml")))

(define-public crate-rustc-ap-serialize-79.0.0 (c (n "rustc-ap-serialize") (v "79.0.0") (h "07kbrijd148n3s1afim5a5nyyw20cmfjwy64fz8da4kqg0dgmkn3")))

(define-public crate-rustc-ap-serialize-80.0.0 (c (n "rustc-ap-serialize") (v "80.0.0") (h "0pc4axdg7acqgrzsg8h79iv1b56jgcjnp2jzagjs8933h14gyyd6")))

(define-public crate-rustc-ap-serialize-81.0.0 (c (n "rustc-ap-serialize") (v "81.0.0") (h "12l9k1j459sxs153mm91lr3zm402pc1idzxxi7hczixcryhi049b")))

(define-public crate-rustc-ap-serialize-82.0.0 (c (n "rustc-ap-serialize") (v "82.0.0") (h "1bc81hwa3fx0cgij7yvzrfi0x3aadlj6is8fz3bi1dm32dbzd3yf")))

(define-public crate-rustc-ap-serialize-83.0.0 (c (n "rustc-ap-serialize") (v "83.0.0") (h "1jp2rnb3h3nbird6hm5mqpwz7ynwh701k5kdrpcb3chwn8z9mj4r")))

(define-public crate-rustc-ap-serialize-84.0.0 (c (n "rustc-ap-serialize") (v "84.0.0") (h "1iikylaa74ffnk4jyd0cs9d2gq6lpqmywsyr2vvi2f85x8ia1j82")))

(define-public crate-rustc-ap-serialize-85.0.0 (c (n "rustc-ap-serialize") (v "85.0.0") (h "0xhzhzwmw4fccrqbp8nyr32wgq3sjqwjz2b5nhzmzjx3w22y6nmp")))

(define-public crate-rustc-ap-serialize-86.0.0 (c (n "rustc-ap-serialize") (v "86.0.0") (h "0myqc4z208jz23s7a8v8lc201q0rh213d4a2j1ddvzw8pc0h469j")))

(define-public crate-rustc-ap-serialize-87.0.0 (c (n "rustc-ap-serialize") (v "87.0.0") (h "04w4fnf3i2rayhqn0ffvm9rzd8fzr7yq2imn95f77ggh07vb1rrx")))

(define-public crate-rustc-ap-serialize-88.0.0 (c (n "rustc-ap-serialize") (v "88.0.0") (h "1p03z2v6hd17ji7vff5i732494kfrxbgx3bqj6zj79ywyvdwaymn")))

(define-public crate-rustc-ap-serialize-89.0.0 (c (n "rustc-ap-serialize") (v "89.0.0") (h "1ifvqw1fjs48jsd3hj2bqw0069hz6q5rs9d4ss623icrwvi02k5w")))

(define-public crate-rustc-ap-serialize-90.0.0 (c (n "rustc-ap-serialize") (v "90.0.0") (h "0zbzcdjdw07919awd7f1iw4rc031asxjnny0icqn63489gw6i882")))

(define-public crate-rustc-ap-serialize-91.0.0 (c (n "rustc-ap-serialize") (v "91.0.0") (h "1sml38ys5l7wd4fwyzyfjgyjpx46b3ndy6zmzn4dlzyqw4y7x2nh")))

(define-public crate-rustc-ap-serialize-92.0.0 (c (n "rustc-ap-serialize") (v "92.0.0") (h "0yvnrhhdrxs80hjzwxx5nq4ij8d7w3w0iw8r6qrd423k48ql1jy0")))

(define-public crate-rustc-ap-serialize-93.0.0 (c (n "rustc-ap-serialize") (v "93.0.0") (h "1dl8qfggyry1mj9vh7kpl0vfcq892d3qpwsg8pvqfcsnfhgnghl1")))

(define-public crate-rustc-ap-serialize-94.0.0 (c (n "rustc-ap-serialize") (v "94.0.0") (h "086mgkjjg6d1j68bxym6inpwypqvy8naalcc5fxxxb8yh64qzrq6")))

(define-public crate-rustc-ap-serialize-95.0.0 (c (n "rustc-ap-serialize") (v "95.0.0") (h "1zsd2z7jvn4z2hkgmkmvw05q12ph7fj1nv1705w5y4kkh9lzj40b")))

(define-public crate-rustc-ap-serialize-96.0.0 (c (n "rustc-ap-serialize") (v "96.0.0") (h "1fq22qdb4k5a02vsa3d00l4mqii2hbxi2rkqdwfraz29363yq2fv")))

(define-public crate-rustc-ap-serialize-97.0.0 (c (n "rustc-ap-serialize") (v "97.0.0") (h "0l5rv6d0prc4i7gpsb7dyv91bbhql0npyrjyx95rhamqp1lrh7r5")))

(define-public crate-rustc-ap-serialize-98.0.0 (c (n "rustc-ap-serialize") (v "98.0.0") (h "0jw4p35gq6jmd1i7r1vmi4qyqys8v3f0i718k0c4spm774hxnm1q")))

(define-public crate-rustc-ap-serialize-99.0.0 (c (n "rustc-ap-serialize") (v "99.0.0") (h "1j8crydcldzf7wsl6g89lgnmr7d4f9266c3hfr9c1xzlfpr23d35")))

(define-public crate-rustc-ap-serialize-100.0.0 (c (n "rustc-ap-serialize") (v "100.0.0") (h "1vaj2g31zgn92ym0kkdlx3ryh6izck6czcvwywb8fsbqjjvfa9mz")))

(define-public crate-rustc-ap-serialize-101.0.0 (c (n "rustc-ap-serialize") (v "101.0.0") (h "18iqswffybn5m67iwjmsl3nfj03l7k10l5ym376gvkq2fvpi7ngf")))

(define-public crate-rustc-ap-serialize-102.0.0 (c (n "rustc-ap-serialize") (v "102.0.0") (h "09mzmsnprpw5vpbihj5fcicp75wdxxbjgr91g30idh5p55wbfi8p")))

(define-public crate-rustc-ap-serialize-103.0.0 (c (n "rustc-ap-serialize") (v "103.0.0") (h "0pjyg385w91z7g59pf2fwf8z1a4dsvap4s1951j7v32kps6k6v9m")))

(define-public crate-rustc-ap-serialize-104.0.0 (c (n "rustc-ap-serialize") (v "104.0.0") (h "1jg0lzikxcj0k971lgrvykn006d0q8llp0cw15gmyx5swqfpwm55")))

(define-public crate-rustc-ap-serialize-105.0.0 (c (n "rustc-ap-serialize") (v "105.0.0") (h "0jn3h446jr3z1jiaprr1785sff130j2akimvq53wsqfwhxjs36xr")))

(define-public crate-rustc-ap-serialize-106.0.0 (c (n "rustc-ap-serialize") (v "106.0.0") (h "0chl3d231khhqp1an8ml51hhdhczyy4zf04wvw9bhs6nq962zj5w")))

(define-public crate-rustc-ap-serialize-107.0.0 (c (n "rustc-ap-serialize") (v "107.0.0") (h "1hpzx29v35gjyf70czg6xvrska6ajqv18gxis86yzsw23dxvlvwy")))

(define-public crate-rustc-ap-serialize-108.0.0 (c (n "rustc-ap-serialize") (v "108.0.0") (h "00j34jyz6af1w9qwpfn8dg0mfkp13f41z4prl06dprqllk0ri9ah")))

(define-public crate-rustc-ap-serialize-109.0.0 (c (n "rustc-ap-serialize") (v "109.0.0") (h "1jyv19xbn6b7fsdkc6lyyyyvr9mjnr5y6w3w2nn1sknfs1h3rph8")))

(define-public crate-rustc-ap-serialize-110.0.0 (c (n "rustc-ap-serialize") (v "110.0.0") (h "1bj5g5dvmca8v85vzf0vrwzcna4zsiljyv2nnxx0qszh97wprr52")))

(define-public crate-rustc-ap-serialize-111.0.0 (c (n "rustc-ap-serialize") (v "111.0.0") (h "15axf7vs6cipc30h889ljwylxba54wjm2j72dd0qdiw5rslyd3pm")))

(define-public crate-rustc-ap-serialize-112.0.0 (c (n "rustc-ap-serialize") (v "112.0.0") (h "0fdv14plqns8s955zjlqs1qz6dg1qx7yczrqapydvmi9mz0xj5w2")))

(define-public crate-rustc-ap-serialize-113.0.0 (c (n "rustc-ap-serialize") (v "113.0.0") (h "0yfq4ajnp1ym9cmcsqrq82pm5nimkbqz2mjnhj5blp6nhxdaa111")))

(define-public crate-rustc-ap-serialize-114.0.0 (c (n "rustc-ap-serialize") (v "114.0.0") (h "14f9gmsm4jrdrz5a9igjbrnn6h9512a6j93xbp37a02fbs6g4psf")))

(define-public crate-rustc-ap-serialize-115.0.0 (c (n "rustc-ap-serialize") (v "115.0.0") (h "0agbkqkbq8k1i0qz9xn0f5wyi6d5gzkhkm61682vmx8s3lknpan7")))

(define-public crate-rustc-ap-serialize-116.0.0 (c (n "rustc-ap-serialize") (v "116.0.0") (h "0j2b810hdnyh3w70q2321jhzpxr1dawrj0z831w8yyndmd7dx9wm")))

(define-public crate-rustc-ap-serialize-117.0.0 (c (n "rustc-ap-serialize") (v "117.0.0") (h "007fvwky1zbsbv04vjvihr7i1in6wzk1jnbqaxkz9nlk03vrpd3h")))

(define-public crate-rustc-ap-serialize-118.0.0 (c (n "rustc-ap-serialize") (v "118.0.0") (h "13mwkf28dfxpx9ap4jmh3h31zwm3d9jfxi9c6b4p20rqg62c3w79")))

(define-public crate-rustc-ap-serialize-119.0.0 (c (n "rustc-ap-serialize") (v "119.0.0") (h "10f6q84ns2sk4aw625nh1bl3v2cjwv325jnrs5q01wj3bnmwzykr")))

(define-public crate-rustc-ap-serialize-120.0.0 (c (n "rustc-ap-serialize") (v "120.0.0") (h "128nvi278280rka0n1daqv8yn2fvflqmgazkaldvlihb63cpkf96")))

(define-public crate-rustc-ap-serialize-121.0.0 (c (n "rustc-ap-serialize") (v "121.0.0") (h "0my4pfgb9cm0is5j9g1brrk0q5y6xy6389xmjsi1lfm3ifjj2xzg")))

(define-public crate-rustc-ap-serialize-122.0.0 (c (n "rustc-ap-serialize") (v "122.0.0") (h "0s4hkqd9g54ghj18fxnib07ffx3yp6yb60jac82da4bz169lvg6n")))

(define-public crate-rustc-ap-serialize-123.0.0 (c (n "rustc-ap-serialize") (v "123.0.0") (h "1rf68xkd1y7bnhqmmwr4cczypa2ckwcdvj4z067zs9my56pamisd")))

(define-public crate-rustc-ap-serialize-124.0.0 (c (n "rustc-ap-serialize") (v "124.0.0") (h "0jy6p932z6yrr5dss21bsgmzbw06sikhx9xrhxc35giq0fh45amx")))

(define-public crate-rustc-ap-serialize-125.0.0 (c (n "rustc-ap-serialize") (v "125.0.0") (h "00b42mgw5v5lmd8flhdxnpdzh6lb5g4sdilsf18jcjzw46k2cb3v")))

(define-public crate-rustc-ap-serialize-126.0.0 (c (n "rustc-ap-serialize") (v "126.0.0") (h "0s8szi24ba77bg1rzmz14b65mbmahr93c91zkxqz49xgzjgkyqhb")))

(define-public crate-rustc-ap-serialize-127.0.0 (c (n "rustc-ap-serialize") (v "127.0.0") (h "0zzarq1vfl1qdnnf0461fa8gwcdv3c2v0vcsapi10mfgv9fbv8am")))

(define-public crate-rustc-ap-serialize-128.0.0 (c (n "rustc-ap-serialize") (v "128.0.0") (h "1hql2wfaf6f9mw785m5d77k47v0m8pci4n3sl92mqf98ghjp5ayi")))

(define-public crate-rustc-ap-serialize-129.0.0 (c (n "rustc-ap-serialize") (v "129.0.0") (h "0ilvxm8gcma9s9m02wzhn4hhavlnbnr498p5kzqlj50wgbwxbqz8")))

(define-public crate-rustc-ap-serialize-130.0.0 (c (n "rustc-ap-serialize") (v "130.0.0") (h "1hiz233zpkrw37rmdk8p4x9mg32j51x4a53g2sg6bvswan5qvysg")))

(define-public crate-rustc-ap-serialize-131.0.0 (c (n "rustc-ap-serialize") (v "131.0.0") (h "0l8hq576vqq2if45dh2ijz787mnqbnvfqkf55yg5l77a0dvm33jn")))

(define-public crate-rustc-ap-serialize-132.0.0 (c (n "rustc-ap-serialize") (v "132.0.0") (h "142ckffcivs9k2jpm4lic8zw0jlps33f4h225chwrd1c5gkx14pa")))

(define-public crate-rustc-ap-serialize-133.0.0 (c (n "rustc-ap-serialize") (v "133.0.0") (h "1amb73fibf9syp7fp6im135g7f3ddn1s7m0s16fbh84g8m34xa52")))

(define-public crate-rustc-ap-serialize-134.0.0 (c (n "rustc-ap-serialize") (v "134.0.0") (h "0nrbrpswl2svw3awi8hmk3mpimz05fa9mlxivnzn7yr4czjmjzfw")))

(define-public crate-rustc-ap-serialize-135.0.0 (c (n "rustc-ap-serialize") (v "135.0.0") (h "01dvhvlln1hqwhx8h54309ffxvdiyn1g70vxqbfsii34xh4r42fk")))

(define-public crate-rustc-ap-serialize-136.0.0 (c (n "rustc-ap-serialize") (v "136.0.0") (h "155hccn30jq36q3wdj6vq1xamrbcwq65hqqzg155a94n9mxmb5hx")))

(define-public crate-rustc-ap-serialize-137.0.0 (c (n "rustc-ap-serialize") (v "137.0.0") (h "02blwxfkva4zbkx5xjnlx0waynmmihnrm0j9x8hdvf89wbnz3qj9")))

(define-public crate-rustc-ap-serialize-138.0.0 (c (n "rustc-ap-serialize") (v "138.0.0") (h "1yxs6xfk8zz2nfb81vvp7gaii665yv2zsxy406j5lmnczhan6mqh")))

(define-public crate-rustc-ap-serialize-139.0.0 (c (n "rustc-ap-serialize") (v "139.0.0") (h "0y4v41ml39fjb2d2mj8a5z7dlrq5lib27dr64bg7g29l0hfvxhci")))

(define-public crate-rustc-ap-serialize-140.0.0 (c (n "rustc-ap-serialize") (v "140.0.0") (h "0fhlzn0cx4fqvndbca1c993kbpff4y2jdrv6nbml84sjw324c1ja")))

(define-public crate-rustc-ap-serialize-141.0.0 (c (n "rustc-ap-serialize") (v "141.0.0") (h "09qvp7j5d8b97bimjs2z9d3gsxp507p6rxchkxzgcnrmsfgsj1rr")))

(define-public crate-rustc-ap-serialize-142.0.0 (c (n "rustc-ap-serialize") (v "142.0.0") (h "1f8ql1xc19rlj2xlzy47qjhcgb4jn34jcl18i2aq9mzyxg2z29ph")))

(define-public crate-rustc-ap-serialize-143.0.0 (c (n "rustc-ap-serialize") (v "143.0.0") (h "0a5gb19fv133r2faxgkkmyckmhnns7wqgrxbd9f3bqs04aqj66iv")))

(define-public crate-rustc-ap-serialize-144.0.0 (c (n "rustc-ap-serialize") (v "144.0.0") (h "08hqcb5gb7vkn48a01p1yixdbz9jvcvl0vgarnr9k6vgv8jrp01g")))

(define-public crate-rustc-ap-serialize-145.0.0 (c (n "rustc-ap-serialize") (v "145.0.0") (h "14674i6w4asffzx8bd4qybkx3q2sln0daznjhcls9cv82v0fj7vf")))

(define-public crate-rustc-ap-serialize-146.0.0 (c (n "rustc-ap-serialize") (v "146.0.0") (h "0ivx3zplf6y1xg0wwgxmg1l875zi4jnqvss157swwczbabzyqx5c")))

(define-public crate-rustc-ap-serialize-147.0.0 (c (n "rustc-ap-serialize") (v "147.0.0") (h "13wx9b73nds53j2ap2y35sdswblzns2w65v1ad14wwkdq70r7r5x")))

(define-public crate-rustc-ap-serialize-148.0.0 (c (n "rustc-ap-serialize") (v "148.0.0") (h "1j5jibwyh8kij7igabdsdbp2ccy578acc6pzs9cgrhfldaxzgimx")))

(define-public crate-rustc-ap-serialize-149.0.0 (c (n "rustc-ap-serialize") (v "149.0.0") (h "0dn7z02pfl1ibp0sw7ks8xj2c5lyg5h980vqjxzm9968pb86j8b9")))

(define-public crate-rustc-ap-serialize-150.0.0 (c (n "rustc-ap-serialize") (v "150.0.0") (h "0sd90q4aj1km3vbn21w6h655xf8zv99fhk2kh09g59syg46gkqkc")))

(define-public crate-rustc-ap-serialize-151.0.0 (c (n "rustc-ap-serialize") (v "151.0.0") (h "1qqzzkn6f28pakbsd1syh2ivnrkpz0q2ibx2p7ihiaw7hcj9yhql")))

(define-public crate-rustc-ap-serialize-152.0.0 (c (n "rustc-ap-serialize") (v "152.0.0") (h "078j84jfzc4iqnhi6qb1x42xd77ycxrjmrgpp22vw5hmgpd0hqn7")))

(define-public crate-rustc-ap-serialize-153.0.0 (c (n "rustc-ap-serialize") (v "153.0.0") (h "0hagdmy05hzn0mkh15saaa9bqwjb62fab16ri7v3ixcnsixwyvcx")))

(define-public crate-rustc-ap-serialize-154.0.0 (c (n "rustc-ap-serialize") (v "154.0.0") (h "0vl2adml7gw2cn60x92wz0d0j3ddr32wmrir03jbz75shcm32f0h")))

(define-public crate-rustc-ap-serialize-155.0.0 (c (n "rustc-ap-serialize") (v "155.0.0") (h "1lrwbmvqxv1filcg1wkk48qi2hzwyclshhsn3vhaz1414d5psbcf")))

(define-public crate-rustc-ap-serialize-156.0.0 (c (n "rustc-ap-serialize") (v "156.0.0") (h "0w6v6qgf5irnf89jd6q5dfvncv9zvdlkqnbgvd6sdwdzjl2p1ir7")))

(define-public crate-rustc-ap-serialize-157.0.0 (c (n "rustc-ap-serialize") (v "157.0.0") (h "15v8c6wpm0h89zj68bj9rbgamvz3pminfy3097pf3d8rhl8i1xkq")))

(define-public crate-rustc-ap-serialize-158.0.0 (c (n "rustc-ap-serialize") (v "158.0.0") (h "0cc86rbp3c5ym9xkmrq29679w4hkcaf2jkavrn71mhpbb0ldvxs9")))

(define-public crate-rustc-ap-serialize-159.0.0 (c (n "rustc-ap-serialize") (v "159.0.0") (h "0pfr72j7saqb0csf9k2fp6f1417bzyfyd9zqz1cc71pmkhs00z49")))

(define-public crate-rustc-ap-serialize-160.0.0 (c (n "rustc-ap-serialize") (v "160.0.0") (h "1d8s1jqbhz32n3l26yjbv3gdd3c1igdian60p4mjl3kfjics62jv")))

(define-public crate-rustc-ap-serialize-161.0.0 (c (n "rustc-ap-serialize") (v "161.0.0") (h "1c8x8q53lbla6mbdf5zfjbj25g7d5z42kh2afxalbjismwwfx89f")))

(define-public crate-rustc-ap-serialize-162.0.0 (c (n "rustc-ap-serialize") (v "162.0.0") (h "04a34ghza61zzzb4vm0lhqipxnpx251l6qvrmm6f9hg0rnkl1ibc")))

(define-public crate-rustc-ap-serialize-163.0.0 (c (n "rustc-ap-serialize") (v "163.0.0") (h "0qpriz3296h57x9hv542wqfqr6aay0r9m4wca05702larxlbwh0w")))

(define-public crate-rustc-ap-serialize-164.0.0 (c (n "rustc-ap-serialize") (v "164.0.0") (h "0jsrys2akk4gyrg45kw3s5ag89h60cg20qzjzqpl65gik0sz55g0")))

(define-public crate-rustc-ap-serialize-165.0.0 (c (n "rustc-ap-serialize") (v "165.0.0") (h "0w0w64f6bn77x1inbcgk0j3cxrwf3s5cigf05abgpvfvf6zc45kf")))

(define-public crate-rustc-ap-serialize-166.0.0 (c (n "rustc-ap-serialize") (v "166.0.0") (h "1338qxvx5y10m83065y5d3mwg38ksn5250mxf6ixaq8b8nhz1hcf")))

(define-public crate-rustc-ap-serialize-167.0.0 (c (n "rustc-ap-serialize") (v "167.0.0") (h "1hz1pbsyhawpg2q7g9b6sxkvy6kzlgji0h72979pkcs36hdgjd7a")))

(define-public crate-rustc-ap-serialize-168.0.0 (c (n "rustc-ap-serialize") (v "168.0.0") (h "1pb3f7wawx8m88sqxwh3vgmp1w24l4z8agxhck9a7bsrixgi0irx")))

(define-public crate-rustc-ap-serialize-169.0.0 (c (n "rustc-ap-serialize") (v "169.0.0") (h "1sgrk2p32wmzlzy78npc6xvq0pnfxz4khwr0vj7wvik7q9m4pyxn")))

(define-public crate-rustc-ap-serialize-170.0.0 (c (n "rustc-ap-serialize") (v "170.0.0") (h "0483n37c3ykd0k7qjpj3y7hx151403bl00ih9k0bcqvrgd60w1zy")))

(define-public crate-rustc-ap-serialize-171.0.0 (c (n "rustc-ap-serialize") (v "171.0.0") (h "07mqxgf3giplbg5rdp3j91lp9zmrqh2cifzc7la8g88ns5n3xjqs")))

(define-public crate-rustc-ap-serialize-172.0.0 (c (n "rustc-ap-serialize") (v "172.0.0") (h "0iz1a656lm03arcblqs7hsk3yjpnmnv4x8dgisqry0pkaj8yxnm1")))

(define-public crate-rustc-ap-serialize-173.0.0 (c (n "rustc-ap-serialize") (v "173.0.0") (h "0kznlxmh3rwg4xm69vxryrq35z3y6a5ni5lc5paks1fbj5wh3d45")))

(define-public crate-rustc-ap-serialize-174.0.0 (c (n "rustc-ap-serialize") (v "174.0.0") (h "10w9ngbp1q3gzj7hclhznk59487cz5hz72dxjy69d2k9dl40ldsx")))

(define-public crate-rustc-ap-serialize-175.0.0 (c (n "rustc-ap-serialize") (v "175.0.0") (h "14p82n1y0cn3pr57zz6ljgn5xlf1k9z03b1q9gbw9qldvad0f20g")))

(define-public crate-rustc-ap-serialize-176.0.0 (c (n "rustc-ap-serialize") (v "176.0.0") (h "0p7lidmmad5l4zpm182l4s26h9cd5z7sa0vv84hlxishgwfzvhrr")))

(define-public crate-rustc-ap-serialize-177.0.0 (c (n "rustc-ap-serialize") (v "177.0.0") (h "0syl595hawral7h41l3i2gh0y8fhnnjifyn1kav78v559qgl88f2")))

(define-public crate-rustc-ap-serialize-178.0.0 (c (n "rustc-ap-serialize") (v "178.0.0") (h "1r7113fillkk0vg8870gr6k9rl5d2xqlni61jy5ljwxvmjxn5f5c")))

(define-public crate-rustc-ap-serialize-179.0.0 (c (n "rustc-ap-serialize") (v "179.0.0") (h "0cmdff3dbq5xncj3qbw3bfrf79jrh497j8pjg0akxwzzr1jl5far")))

(define-public crate-rustc-ap-serialize-180.0.0 (c (n "rustc-ap-serialize") (v "180.0.0") (h "1j4iv83vghks1b7y0q2587ca6x82zrslfg7517cf2hmc5q6fhybp")))

(define-public crate-rustc-ap-serialize-181.0.0 (c (n "rustc-ap-serialize") (v "181.0.0") (h "1arj6fvng22k7jk4f8fb1jiqamrmaaqlvdwnxx67pama1hw0vq7g")))

(define-public crate-rustc-ap-serialize-182.0.0 (c (n "rustc-ap-serialize") (v "182.0.0") (h "0arr58zygnh9c6d3irw61925aaimxib1mpacvqrf4iifvkcpj98q")))

(define-public crate-rustc-ap-serialize-183.0.0 (c (n "rustc-ap-serialize") (v "183.0.0") (h "0fd4aj71yrwq2d3v8d4q0gzjcv0vrcvdl163753m632m7frrq8gs")))

(define-public crate-rustc-ap-serialize-184.0.0 (c (n "rustc-ap-serialize") (v "184.0.0") (h "03cc1jllf6w21fdhry8vgfa755y5nx23jn9krfjc87qaapqg4j19")))

(define-public crate-rustc-ap-serialize-185.0.0 (c (n "rustc-ap-serialize") (v "185.0.0") (h "09hzk486lckkmzk780djcqj07r3nm0880gwp9w7v69g28jlfhz01")))

(define-public crate-rustc-ap-serialize-186.0.0 (c (n "rustc-ap-serialize") (v "186.0.0") (h "1fcqlq1m9vcmbpn7kmx4bmki3w1rp1dxz8m4yv1rngwhxgplywjx")))

(define-public crate-rustc-ap-serialize-187.0.0 (c (n "rustc-ap-serialize") (v "187.0.0") (h "1mxqvj7j5jcv5xsl3c5i43zn9xyl6j3hnl3kg9wkvwvb3m8kpa89")))

(define-public crate-rustc-ap-serialize-188.0.0 (c (n "rustc-ap-serialize") (v "188.0.0") (h "1zsz9kkb3kvyqwzjbda42mnyqgwqcy4ikkkbmyxj3mhm55lxsbry")))

(define-public crate-rustc-ap-serialize-189.0.0 (c (n "rustc-ap-serialize") (v "189.0.0") (h "1mhjz78pca19w1hg6wijax8vga77yyc586mbr8pf3l1phn2l6hqr")))

(define-public crate-rustc-ap-serialize-190.0.0 (c (n "rustc-ap-serialize") (v "190.0.0") (h "07qxl8mpfa4lb9iipgh6dh7k7254zp1bjvcvnryq4i278hfc8bs5")))

(define-public crate-rustc-ap-serialize-191.0.0 (c (n "rustc-ap-serialize") (v "191.0.0") (h "14gjyjb92smk0q3czyhh3k54jwzd6m74hqx598j997qr5912pgjp")))

(define-public crate-rustc-ap-serialize-192.0.0 (c (n "rustc-ap-serialize") (v "192.0.0") (h "0a5acwmk3fdxl25jdrkma2l5cw69kmyziagy0824whydfly23hg4")))

(define-public crate-rustc-ap-serialize-193.0.0 (c (n "rustc-ap-serialize") (v "193.0.0") (h "1766hgclc7ak6d9p4vvbxjwqppi86bv2d298gqrnvnpy31rqlhg1")))

(define-public crate-rustc-ap-serialize-194.0.0 (c (n "rustc-ap-serialize") (v "194.0.0") (h "1mr68pzpffkd9grl8j9fw5syw3ki35rjkh0n8crca200zghwyg3y")))

(define-public crate-rustc-ap-serialize-195.0.0 (c (n "rustc-ap-serialize") (v "195.0.0") (h "186x1n88vl8fsdyvlamjhi99mgpq37ig68f5sck0ay5m2v0dnbcm")))

(define-public crate-rustc-ap-serialize-196.0.0 (c (n "rustc-ap-serialize") (v "196.0.0") (h "1bf7n71xi1ca3gpw1h3s3r8cik4gqv6vwbfdd2hwy64qa6g71l7n")))

(define-public crate-rustc-ap-serialize-197.0.0 (c (n "rustc-ap-serialize") (v "197.0.0") (h "05is5lycjsqr4xb3yx955gxm6qxbq7arx05nnf4m7zb63zssmxbx")))

(define-public crate-rustc-ap-serialize-198.0.0 (c (n "rustc-ap-serialize") (v "198.0.0") (h "0zdi6189hy7b579iwhlc2w5mzcl9bcpl287g0kyichhy49ww3ba1")))

(define-public crate-rustc-ap-serialize-199.0.0 (c (n "rustc-ap-serialize") (v "199.0.0") (h "0wiw40mf7x90mq840jgylkfn08ahxqkd3fnwfdi4anqjbpgp2vmf")))

(define-public crate-rustc-ap-serialize-200.0.0 (c (n "rustc-ap-serialize") (v "200.0.0") (h "0qxp2d9r5ld0j0spjxid5xh71jkj4mvjngwfch65cwfizjjrryqb")))

(define-public crate-rustc-ap-serialize-201.0.0 (c (n "rustc-ap-serialize") (v "201.0.0") (h "041fjwcb8n8sl76515gw22shd2zb4abz8gha7vnvx3jpb8hcn7ak")))

(define-public crate-rustc-ap-serialize-202.0.0 (c (n "rustc-ap-serialize") (v "202.0.0") (h "1vrjg24wl60mqrpq92mblsk4hgb7n80jqw60cgiqb60lcakfasaw")))

(define-public crate-rustc-ap-serialize-203.0.0 (c (n "rustc-ap-serialize") (v "203.0.0") (h "10vjcpj3b47vvqx88imz62khhvj664883dm5hp9w26ikccq2dj67")))

(define-public crate-rustc-ap-serialize-204.0.0 (c (n "rustc-ap-serialize") (v "204.0.0") (h "1qyl121xdq853vhiy7hy7yvynyxiabpif2s36clb5plhqmaja6jn")))

(define-public crate-rustc-ap-serialize-205.0.0 (c (n "rustc-ap-serialize") (v "205.0.0") (h "1093zvib241xs6vns1vp0bfs0db33d8z8sh147crlwcm0phg7w1m")))

(define-public crate-rustc-ap-serialize-206.0.0 (c (n "rustc-ap-serialize") (v "206.0.0") (h "12xaivf2frrmj2v6fmllzy3ipiiv4czghnsd23dvwss61lyxbhnm")))

(define-public crate-rustc-ap-serialize-207.0.0 (c (n "rustc-ap-serialize") (v "207.0.0") (h "0ajyvv12z7c4jbzm9a5rcd52l85nb2wmqknjvqg8x79wmydd4lxm")))

(define-public crate-rustc-ap-serialize-208.0.0 (c (n "rustc-ap-serialize") (v "208.0.0") (h "00rdsmnxkyw9rx9gfwln158xjzrh1s0lfb300k88p0rkknqx3xyx")))

(define-public crate-rustc-ap-serialize-209.0.0 (c (n "rustc-ap-serialize") (v "209.0.0") (h "0zww37adp003ldfpaa8likz6ry9gnx9gqlbym8nahf5hfpqzh24b")))

(define-public crate-rustc-ap-serialize-210.0.0 (c (n "rustc-ap-serialize") (v "210.0.0") (h "00k6mfs9vlvpj7pn6w2593jd81ijvy5q1m47x8hw0kys8mic00rq")))

(define-public crate-rustc-ap-serialize-211.0.0 (c (n "rustc-ap-serialize") (v "211.0.0") (h "0zsvaqcypfkwhhh6kbl8jncwzzhnivlgw5pvv3m4asn2q3116ixd")))

(define-public crate-rustc-ap-serialize-212.0.0 (c (n "rustc-ap-serialize") (v "212.0.0") (h "0041ixbcdzz2i5wrhlqpg2b306a24h8j5vznwjm68rdgnrk92g4j")))

(define-public crate-rustc-ap-serialize-213.0.0 (c (n "rustc-ap-serialize") (v "213.0.0") (h "175r52x35x6w1cj2vmgvailx5c64hdni7dzxipxhi7md5ya1fs28")))

(define-public crate-rustc-ap-serialize-214.0.0 (c (n "rustc-ap-serialize") (v "214.0.0") (h "1nrzjq91kn9w4zxdkkkh9b0p0qwf6h1qfzvr3mdbwhs8w8swxqr3")))

(define-public crate-rustc-ap-serialize-215.0.0 (c (n "rustc-ap-serialize") (v "215.0.0") (h "1mi0cpsvm39vf1v78ak3amh1kwkns8d36lgszwg4haxf8azqbx0r")))

(define-public crate-rustc-ap-serialize-216.0.0 (c (n "rustc-ap-serialize") (v "216.0.0") (h "0843jajin1i9a879nn9ysrwcv0s03cs62bgmaa6sn8bvnpxrcbq2")))

(define-public crate-rustc-ap-serialize-217.0.0 (c (n "rustc-ap-serialize") (v "217.0.0") (h "14qma38x8q9am2lfadjl0v3mr3zvy4lqachb9gin270nnwrsavwl")))

(define-public crate-rustc-ap-serialize-218.0.0 (c (n "rustc-ap-serialize") (v "218.0.0") (h "14v8amzx96y193g9x668zafsy9rl4pvh12h0bkrgbjyp5h8b3kzz")))

(define-public crate-rustc-ap-serialize-219.0.0 (c (n "rustc-ap-serialize") (v "219.0.0") (h "0w96qfiwgbz415mgaf9hc119hm8kmvx95kvd0l7ah8mh5c7gyb7f")))

(define-public crate-rustc-ap-serialize-220.0.0 (c (n "rustc-ap-serialize") (v "220.0.0") (h "1hnjvmhf1zp231r7pya9b22hr672w56ngxnvn2skax4q3kmymjdv")))

(define-public crate-rustc-ap-serialize-221.0.0 (c (n "rustc-ap-serialize") (v "221.0.0") (h "1yb3ai7i4ml3bg1lcf2pqjkydwf8pmydax5cl15llc7yr4mxqzg2")))

(define-public crate-rustc-ap-serialize-222.0.0 (c (n "rustc-ap-serialize") (v "222.0.0") (h "1n3iqwy1skk3l40w0hcm24qg14xcnq0a4ngvzfgk91rwgvw09bi4")))

(define-public crate-rustc-ap-serialize-223.0.0 (c (n "rustc-ap-serialize") (v "223.0.0") (h "134sgp3l6d3c357ilazyqi0bl3x98xphi0giyj08b2js29p0qk4g")))

(define-public crate-rustc-ap-serialize-224.0.0 (c (n "rustc-ap-serialize") (v "224.0.0") (h "1j9dh3xa4c4ihhkj0ddww70kch7vnx0qs8vxzhp1gvl07845a2ph")))

(define-public crate-rustc-ap-serialize-225.0.0 (c (n "rustc-ap-serialize") (v "225.0.0") (h "1lpm9q31g0kc1qdqvmli7b05k80vr1d4gvqp0bh9g25xgqznjjrh")))

(define-public crate-rustc-ap-serialize-226.0.0 (c (n "rustc-ap-serialize") (v "226.0.0") (h "1cd0rwqnb78gw335rycvz7nigwksl43j1rxmd9mfvlka4m0493gs")))

(define-public crate-rustc-ap-serialize-227.0.0 (c (n "rustc-ap-serialize") (v "227.0.0") (h "1xfsmf263akpm0zrdyvqfr6daq5h49xagwnajpr5byar0fdd9q2n")))

(define-public crate-rustc-ap-serialize-228.0.0 (c (n "rustc-ap-serialize") (v "228.0.0") (h "11cf4krw7falnzsl5faw1b4g755rgs3yczwmgpjdvj9imq9qdmax")))

(define-public crate-rustc-ap-serialize-229.0.0 (c (n "rustc-ap-serialize") (v "229.0.0") (h "0k2zf7kcq5pzwj246xkrv12vzicyd6ks45clicsmkkv28hqak7ns")))

(define-public crate-rustc-ap-serialize-230.0.0 (c (n "rustc-ap-serialize") (v "230.0.0") (h "0n76jn35zds7hjs8nk86v5kyd0jh413mfam97kiljjwsgcb7drln")))

(define-public crate-rustc-ap-serialize-231.0.0 (c (n "rustc-ap-serialize") (v "231.0.0") (h "0gbmsah4zxqb2jajklj7scwm941kjxj7lwv5n1447qjcc04pp4vx")))

(define-public crate-rustc-ap-serialize-232.0.0 (c (n "rustc-ap-serialize") (v "232.0.0") (h "1c71ww6kcpiiib3rljjb8hvqg5kf1vpwplxk52jj45knm2dxwj36")))

(define-public crate-rustc-ap-serialize-233.0.0 (c (n "rustc-ap-serialize") (v "233.0.0") (h "1rmig2p8mwldngwj0dj59zndn876y8nhg0i45banv3yizzsygfxi")))

(define-public crate-rustc-ap-serialize-234.0.0 (c (n "rustc-ap-serialize") (v "234.0.0") (h "10icmycyhi2dz6907vlbq31ibcpmdg5hvnqipa1w0fqgz3wgsh1b")))

(define-public crate-rustc-ap-serialize-235.0.0 (c (n "rustc-ap-serialize") (v "235.0.0") (h "1micqlmzwbfjbqc62xcdv82w36dmyi0vqxbrfsahy4n0288fp6ya")))

(define-public crate-rustc-ap-serialize-236.0.0 (c (n "rustc-ap-serialize") (v "236.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0nf8n2sn9vq5xwa8kic74g383qzwk36a4w81s2xhii5ampwvw6a4")))

(define-public crate-rustc-ap-serialize-237.0.0 (c (n "rustc-ap-serialize") (v "237.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0acrwca148y9w75ch0l0xlvacrwsdy1nc9d160zjv56lb2v8sqsp")))

(define-public crate-rustc-ap-serialize-238.0.0 (c (n "rustc-ap-serialize") (v "238.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1ik17sdylmfhc8ck2wn22gb0djsv79s0fcydmbcwm2xs0kq81q38")))

(define-public crate-rustc-ap-serialize-239.0.0 (c (n "rustc-ap-serialize") (v "239.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "11r1iwwh5jfkpdp2bpg4syynbrwqwk5qpfc0gb964xsifi46d1p4")))

(define-public crate-rustc-ap-serialize-240.0.0 (c (n "rustc-ap-serialize") (v "240.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1in4wg3qb0j0jr89bjk0zzqa5r3y4y22aqkbdfkcihfjjfijvymg")))

(define-public crate-rustc-ap-serialize-241.0.0 (c (n "rustc-ap-serialize") (v "241.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0h7zf6k5jiz6gl426rlfprsvqs9qlpqbm9mc1ik2vcxjm8wbiwyn")))

(define-public crate-rustc-ap-serialize-242.0.0 (c (n "rustc-ap-serialize") (v "242.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "15z1c1h0aywzfp36vbwsxpzlvv8l01xqvw86nlvr2640sirwsh5z")))

(define-public crate-rustc-ap-serialize-243.0.0 (c (n "rustc-ap-serialize") (v "243.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1q1c24xkym0bwpbh8nwl0y4xxbspay1gqi7gnxs161rvrjqy1sml")))

(define-public crate-rustc-ap-serialize-244.0.0 (c (n "rustc-ap-serialize") (v "244.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0x096m1h86jc8c8d6rh0r3hm9r130m2hl3bfcivj757w5ki85vbp")))

(define-public crate-rustc-ap-serialize-245.0.0 (c (n "rustc-ap-serialize") (v "245.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1s8rawa8ga7xvjafhx9ihm013ffpyd7am17qhp3qps8lxm2d20gj")))

(define-public crate-rustc-ap-serialize-246.0.0 (c (n "rustc-ap-serialize") (v "246.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0jk0wf65rhvlnnkm6zrmww141kcgxj964h8ngrw66c4qachb33kz")))

(define-public crate-rustc-ap-serialize-247.0.0 (c (n "rustc-ap-serialize") (v "247.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0n800kl30v2g5q3sdbavdqq9pdwlb1ylvzn1kjnvbgipnzmrx7yg")))

(define-public crate-rustc-ap-serialize-248.0.0 (c (n "rustc-ap-serialize") (v "248.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0yimp8jj6l509ivdq1pjr83hkg3jxwxcayh0ndbyw1ys21vcd4na")))

(define-public crate-rustc-ap-serialize-249.0.0 (c (n "rustc-ap-serialize") (v "249.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "16p1gbavyczhdh50cawb729j9jd62imb4cwy9p8jbnkz2nzjkbdd")))

(define-public crate-rustc-ap-serialize-250.0.0 (c (n "rustc-ap-serialize") (v "250.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1lpcxv40anpbp4vagkq8zar5iwj10wrmk6rkdrpa7rxmbla26rjx")))

(define-public crate-rustc-ap-serialize-251.0.0 (c (n "rustc-ap-serialize") (v "251.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1nqnwps2fmq8xq9nwcw87c57mwhdz118wip2fmcbj6ssr9a1d5g5")))

(define-public crate-rustc-ap-serialize-252.0.0 (c (n "rustc-ap-serialize") (v "252.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1d08lsa4ii8wz4dwj3f6y5jz9jgvx6878vwiqhma381lqh62ncg7")))

(define-public crate-rustc-ap-serialize-253.0.0 (c (n "rustc-ap-serialize") (v "253.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1l7wqfqd9v78wzdyzcrpc1i309vlxpbvkrs52ssl4x35pkaj2b2r")))

(define-public crate-rustc-ap-serialize-254.0.0 (c (n "rustc-ap-serialize") (v "254.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "067k0zm5qcd318r19kjynyksdj5g5dasxb6dl5zlr9hh5lvj0f2v")))

(define-public crate-rustc-ap-serialize-255.0.0 (c (n "rustc-ap-serialize") (v "255.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0yd8zclih1dwq0hwc90d4xi5vfqvwdbfnl9prwa9854caihivf8w")))

(define-public crate-rustc-ap-serialize-256.0.0 (c (n "rustc-ap-serialize") (v "256.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1pz0wxyhp60y89bzad874jmi93xyy2ci0flmq5mv1zwqwlxn9i5i")))

(define-public crate-rustc-ap-serialize-257.0.0 (c (n "rustc-ap-serialize") (v "257.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0zqjgjq48xvg7w295yjzxw71b11h9samirchfnbknm7pg9mvd939")))

(define-public crate-rustc-ap-serialize-258.0.0 (c (n "rustc-ap-serialize") (v "258.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0zkf2iws0wjmm62ynipgmrpq1fs51rwcybrjwv7ng3px19d9fiq0")))

(define-public crate-rustc-ap-serialize-259.0.0 (c (n "rustc-ap-serialize") (v "259.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "063ckd8r5kwl0l5argd431cx1bynnxz3bb3r3iazbahhfhzqzffz")))

(define-public crate-rustc-ap-serialize-260.0.0 (c (n "rustc-ap-serialize") (v "260.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1kj64l9ldb8cxzyl2bdm3m94p22isk02p0ryvyy4493hsmj4jfw4")))

(define-public crate-rustc-ap-serialize-261.0.0 (c (n "rustc-ap-serialize") (v "261.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0cx47pzr7pyha4r83wdxbxk92ya9i7pmy6lgmn0kic7kr2wj1qh4")))

(define-public crate-rustc-ap-serialize-262.0.0 (c (n "rustc-ap-serialize") (v "262.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "117f2rfznpqx00dnx94n1avnis92zgm1hbhmks4capimkzrpivkf")))

(define-public crate-rustc-ap-serialize-263.0.0 (c (n "rustc-ap-serialize") (v "263.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0gpwdh0c8fnjr2gv9h2iqsl5qpj717rvvx2qp1s6y7pv9xwrqhdn")))

(define-public crate-rustc-ap-serialize-264.0.0 (c (n "rustc-ap-serialize") (v "264.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0nvrx8szr65kblz91yqvj09g1dxnk7jspndhb1ghdwfjb2mq5pq6")))

(define-public crate-rustc-ap-serialize-265.0.0 (c (n "rustc-ap-serialize") (v "265.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "05yd9d1g79zqin8k2850vkay4cni4nnidfb8iwf62izzqdv09y15")))

(define-public crate-rustc-ap-serialize-266.0.0 (c (n "rustc-ap-serialize") (v "266.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1059x0qndh3i72yb9lcpy7g5q6z1cndg62fp4c25c298kl2d5dhl")))

(define-public crate-rustc-ap-serialize-267.0.0 (c (n "rustc-ap-serialize") (v "267.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1m39vrb0hvp043ahc9rnjqaykl9jm0l0yj83l54czj4r605vjw3y")))

(define-public crate-rustc-ap-serialize-268.0.0 (c (n "rustc-ap-serialize") (v "268.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "15m51nnqi847yrm59fj901r0khlzhjrnwdmdz0v8c3c6xvjqpcwh")))

(define-public crate-rustc-ap-serialize-269.0.0 (c (n "rustc-ap-serialize") (v "269.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "18hry3dq0zfb6dkpnfci7vxk9pa55xryf4a4if6dawcxwxv778xv")))

(define-public crate-rustc-ap-serialize-270.0.0 (c (n "rustc-ap-serialize") (v "270.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "14fzgnwf7fjivws16yndlqix9hhdxrvyk9nnpya4nkjhpvp1hcx4")))

(define-public crate-rustc-ap-serialize-271.0.0 (c (n "rustc-ap-serialize") (v "271.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1rgz5xjparjv5y7lv68y14c4kbvh11kxq8jsk6d53n4rzpkliwz5")))

(define-public crate-rustc-ap-serialize-272.0.0 (c (n "rustc-ap-serialize") (v "272.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1qpipwqgsq5qa369m74li4q1zn82a1wjwzqww4ihr916124rh9x7")))

(define-public crate-rustc-ap-serialize-273.0.0 (c (n "rustc-ap-serialize") (v "273.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0jcklxfg10wbc35w29n9jjkdkw9id9xi01qavicmm4snljgb28i9")))

(define-public crate-rustc-ap-serialize-274.0.0 (c (n "rustc-ap-serialize") (v "274.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0v2p2d8xzdwgrvwg103ik3akj3hpqc3ahv7d1d6556mg2k8c6v6i")))

(define-public crate-rustc-ap-serialize-275.0.0 (c (n "rustc-ap-serialize") (v "275.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "06v9v4vxr9rywh5wv1zfxa09c8s32c0392040wvi64mvsp4wnkfw")))

(define-public crate-rustc-ap-serialize-276.0.0 (c (n "rustc-ap-serialize") (v "276.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "14n49r2ik2p8rl7w8padirp5wqkmgavfi1r4bdfr3hnfnbjpg79x")))

(define-public crate-rustc-ap-serialize-277.0.0 (c (n "rustc-ap-serialize") (v "277.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1zxgjjxl4a5g6dzhg3rwy0c62b4jwnxs80zgk3mb9xdlv0w1glb5")))

(define-public crate-rustc-ap-serialize-278.0.0 (c (n "rustc-ap-serialize") (v "278.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "16byylcs983kc4cpsrwhak9gm9nx3a5h23hvk2jninr365cb1cgd")))

(define-public crate-rustc-ap-serialize-279.0.0 (c (n "rustc-ap-serialize") (v "279.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0mv5l4d7wi82s7pd828081jp358c530kcn8qkfn6bckclyfkp4k5")))

(define-public crate-rustc-ap-serialize-280.0.0 (c (n "rustc-ap-serialize") (v "280.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "09aqhhb6k4n24xzmag3b8rl7im0k18z1ivmrca7kiikvzxw79069")))

(define-public crate-rustc-ap-serialize-281.0.0 (c (n "rustc-ap-serialize") (v "281.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1mmkf2xzf5nz9hrkmfam7rl6yl37l0nayrwhrs7ijpp9zlzl3vrc")))

(define-public crate-rustc-ap-serialize-282.0.0 (c (n "rustc-ap-serialize") (v "282.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1kjv1r75sbm4b9k9fhvc6aagarngi1951cm64zrp8d430ddz87m4")))

(define-public crate-rustc-ap-serialize-283.0.0 (c (n "rustc-ap-serialize") (v "283.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "197iy5ppyz0h5qjh4bjcb8ypvglyfx0yxlrgmfsfk3sn30q7mvnc")))

(define-public crate-rustc-ap-serialize-284.0.0 (c (n "rustc-ap-serialize") (v "284.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1gdjzlvppa3fn8nrf7micjk10jwqmhwklz4arrdj26jq4g2b1v4n")))

(define-public crate-rustc-ap-serialize-285.0.0 (c (n "rustc-ap-serialize") (v "285.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "01pgxx8zmg3dz1pi5nhmgig00bgpi7jxqc3xxakpcrpsip6hl7fy")))

(define-public crate-rustc-ap-serialize-286.0.0 (c (n "rustc-ap-serialize") (v "286.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "008bkzpfvpskj4zmxvadjvkk918ccindqfv46cak4zapv8cr06i2")))

(define-public crate-rustc-ap-serialize-287.0.0 (c (n "rustc-ap-serialize") (v "287.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0lpx25m8z13a56lg1ypxk6azickcj1vm314sp29snbfb4rs5166k")))

(define-public crate-rustc-ap-serialize-288.0.0 (c (n "rustc-ap-serialize") (v "288.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1c2q4mvymxxxi211sbwc5jfab0rklysc0y6j5ja9mq3nrgl0f9qr")))

(define-public crate-rustc-ap-serialize-289.0.0 (c (n "rustc-ap-serialize") (v "289.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1kffbjcqqnnlvdrn6mxncbgv4x910cr47mmbgl4qjr88s8z1pjs9")))

(define-public crate-rustc-ap-serialize-290.0.0 (c (n "rustc-ap-serialize") (v "290.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1bsaqsg5ksabrnyi5fk3nylq24mijpqjx8hmpncfawkqimx81l8m")))

(define-public crate-rustc-ap-serialize-291.0.0 (c (n "rustc-ap-serialize") (v "291.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1w0w9bciq2hq85p1frsfb0s76h5k76zs2i96gzn1f8b2j87kx40f")))

(define-public crate-rustc-ap-serialize-292.0.0 (c (n "rustc-ap-serialize") (v "292.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "04rl0by0sfzynwhb5y9gv72fl1cljwsw5pxkp56cf26ajy32cyhm")))

(define-public crate-rustc-ap-serialize-293.0.0 (c (n "rustc-ap-serialize") (v "293.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0pm11arfnzbqviiq35812g857kg0srik0z6f4sfh52zfwlmz7pan")))

(define-public crate-rustc-ap-serialize-294.0.0 (c (n "rustc-ap-serialize") (v "294.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0h8k1majj5sfk7i7fzlrsa3fhzpqzvqs9478w62yzczm6migv1i1")))

(define-public crate-rustc-ap-serialize-295.0.0 (c (n "rustc-ap-serialize") (v "295.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "15w5wcmrpj9p98f7rm601k0hd8lkjrcg51902xzfp5cs5m39hxyr")))

(define-public crate-rustc-ap-serialize-296.0.0 (c (n "rustc-ap-serialize") (v "296.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0w1xdgk0igrj618qy4ii7s4p7khsbl9hkpp1k742zc1scd7991ay")))

(define-public crate-rustc-ap-serialize-297.0.0 (c (n "rustc-ap-serialize") (v "297.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "17afaifpqqiswc89xbr4f4lz4ikkdbykhw19wcif7qhpwq83bwf8")))

(define-public crate-rustc-ap-serialize-298.0.0 (c (n "rustc-ap-serialize") (v "298.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0wq5sxnslz610zb45sgkjcak9rdzbb95pziq34y9m8w18hapy0ms")))

(define-public crate-rustc-ap-serialize-299.0.0 (c (n "rustc-ap-serialize") (v "299.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "13p8iz3iy0b4ddbli8y7kzglb7vwnwa18b2dh6z0xs646y0jxgsr")))

(define-public crate-rustc-ap-serialize-300.0.0 (c (n "rustc-ap-serialize") (v "300.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1nhd38f87icqi0065vnbkydvmadsc5c34i8cqp660h7i1i7vsh0y")))

(define-public crate-rustc-ap-serialize-301.0.0 (c (n "rustc-ap-serialize") (v "301.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0w2jsq3ad8ci2lb4ymxbssqq4qpnl601zis5bx87fzrngr5q3w8m")))

(define-public crate-rustc-ap-serialize-302.0.0 (c (n "rustc-ap-serialize") (v "302.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "068kqgpxbk6m8hdfw67pms1359x8x2kvdd6bcf1rvdhsidx1g3fm")))

(define-public crate-rustc-ap-serialize-303.0.0 (c (n "rustc-ap-serialize") (v "303.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1m55nlwgh2r3zmmkf5iw84216gz7fki0zfqcfy4xzrkj7xdfqj2w")))

(define-public crate-rustc-ap-serialize-304.0.0 (c (n "rustc-ap-serialize") (v "304.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "14al8s1n1j0vav0kv83li6q7w7yzq3vqg7382n1cilvj9hfiaviv")))

(define-public crate-rustc-ap-serialize-305.0.0 (c (n "rustc-ap-serialize") (v "305.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0hps1lmll62435nrgng300wllm9zw1nr7jlhf5bbyqhzjsj1iz0i")))

(define-public crate-rustc-ap-serialize-306.0.0 (c (n "rustc-ap-serialize") (v "306.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0drpnfffp8ycm077f35p6vm0bkp66r3hfdx7j9sn8mp9c60hwb2v")))

(define-public crate-rustc-ap-serialize-307.0.0 (c (n "rustc-ap-serialize") (v "307.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "0j9m16j4dxh2kz6b3r8krn4553mfi2c8rpkhq4hdx3x29a7qhqh5")))

(define-public crate-rustc-ap-serialize-308.0.0 (c (n "rustc-ap-serialize") (v "308.0.0") (d (list (d (n "smallvec") (r "^0.6.5") (f (quote ("union"))) (d #t) (k 0)))) (h "1fppd3csihda96nd856f5r8xyy1q9d149r95cms5bmk2462hkjh0")))

(define-public crate-rustc-ap-serialize-309.0.0 (c (n "rustc-ap-serialize") (v "309.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0m9sr0zkd5v3miwkgmimx3jfwqrvbvx4920rwzfxrvjy3znn3kva")))

(define-public crate-rustc-ap-serialize-310.0.0 (c (n "rustc-ap-serialize") (v "310.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0mf08pgybzbkj0kn9crv97baillkmldwa97xmbi9xzhc4nrg5sbg")))

(define-public crate-rustc-ap-serialize-311.0.0 (c (n "rustc-ap-serialize") (v "311.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "07agqkviyvx5wqy1s30cvv8z47scmgzadz976n4k9a98bz4pqi4l")))

(define-public crate-rustc-ap-serialize-312.0.0 (c (n "rustc-ap-serialize") (v "312.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "03kkzljjahi0fwgg6mdp95yv2sf1kwm857i3x1mvsay0kx07sac0")))

(define-public crate-rustc-ap-serialize-313.0.0 (c (n "rustc-ap-serialize") (v "313.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "03knjdgh271iz2lxk0yv457ji7jc685g2sai5iy283196kzrs6vn")))

(define-public crate-rustc-ap-serialize-314.0.0 (c (n "rustc-ap-serialize") (v "314.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "02d7ih24i7azwa4xyc28kwscl72rsfnd57mqrq5mjm10n5x4rsk8")))

(define-public crate-rustc-ap-serialize-315.0.0 (c (n "rustc-ap-serialize") (v "315.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0yqpchqqgnjxpjm2kppifkjrm6hg60h808qqmkr53ggfj12pqjl0")))

(define-public crate-rustc-ap-serialize-316.0.0 (c (n "rustc-ap-serialize") (v "316.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1hfhhaimcp894b667lbylwjhkcnl6rhdw4gvsv059mr40w10fnln")))

(define-public crate-rustc-ap-serialize-317.0.0 (c (n "rustc-ap-serialize") (v "317.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1kcxi7nb8yhxp5gjcj8d4vpy8j4120lgqwnm29ss0s1h05ljfwla")))

(define-public crate-rustc-ap-serialize-318.0.0 (c (n "rustc-ap-serialize") (v "318.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1f01f9jc4x249c9qii17ni52lwzkxysp0ffq59zk5vh89xpw7ali")))

(define-public crate-rustc-ap-serialize-319.0.0 (c (n "rustc-ap-serialize") (v "319.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mpx8990bi2b81wc6mwb41865walnkwxrilr0vwvr0xk75ci9kld")))

(define-public crate-rustc-ap-serialize-320.0.0 (c (n "rustc-ap-serialize") (v "320.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "14v3k66lywvz07h74qlslkdxalxp5qhcl7zh2z2cfn6aksnpmijw")))

(define-public crate-rustc-ap-serialize-321.0.0 (c (n "rustc-ap-serialize") (v "321.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0s6wgwrarwk276k1443dk6ygb6xjnrs25nz6z1y61wfdhdcwwbbf")))

(define-public crate-rustc-ap-serialize-322.0.0 (c (n "rustc-ap-serialize") (v "322.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0nci7jqpyi0dyx6p3j3qhlxk8vx8jgv6c9whgj6isk0syxq7gw2r")))

(define-public crate-rustc-ap-serialize-323.0.0 (c (n "rustc-ap-serialize") (v "323.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1srfg8bqzzmgpii4blwyix3c698a4sn2il7m3p6macd8p8n38f8f")))

(define-public crate-rustc-ap-serialize-324.0.0 (c (n "rustc-ap-serialize") (v "324.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0vgfpv440ws7phjx7x1nwp7hf3y830x5phmhs4psc3g94yln73c2")))

(define-public crate-rustc-ap-serialize-325.0.0 (c (n "rustc-ap-serialize") (v "325.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1gm009j9bg1n9q8hcrcycairlhlvjyn8jkxvz279y3d7ais96fn1")))

(define-public crate-rustc-ap-serialize-326.0.0 (c (n "rustc-ap-serialize") (v "326.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "18rd65gdvf87xbkj6smcfml83priv1fx3f7xcqhw25s0zk12gz2i")))

(define-public crate-rustc-ap-serialize-327.0.0 (c (n "rustc-ap-serialize") (v "327.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xbc1pm9d5sdm7gpwgkmcl7km6as40ikhq2l6ndxnqm91ik7gicp")))

(define-public crate-rustc-ap-serialize-328.0.0 (c (n "rustc-ap-serialize") (v "328.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "17w4b1lnx61hzrs8m0bl0dsjw2l4gzziysxnsncbklj6lymqi8yw")))

(define-public crate-rustc-ap-serialize-329.0.0 (c (n "rustc-ap-serialize") (v "329.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "180ynn7y805ai0qad0117lkifhgagfarbkmazgi30dms31mn8dfj")))

(define-public crate-rustc-ap-serialize-330.0.0 (c (n "rustc-ap-serialize") (v "330.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0si4vwq01krx7fmilxbl2zpmw279kp3gy4ww2g7whngxj9gclb0d")))

(define-public crate-rustc-ap-serialize-331.0.0 (c (n "rustc-ap-serialize") (v "331.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "15w0dixh34lphpsgbfal3l6v0vhmz5dakjk3s8z5172w9sv2psng")))

(define-public crate-rustc-ap-serialize-332.0.0 (c (n "rustc-ap-serialize") (v "332.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0sxik1iy34h5q2bckifkarr968nr2pxy963qf5ly4vhk16gzv1b9")))

(define-public crate-rustc-ap-serialize-333.0.0 (c (n "rustc-ap-serialize") (v "333.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1w5g88mdr46n7pkvrml3kwadijq8k0j8isbwjb068pj5pnnajrzs")))

(define-public crate-rustc-ap-serialize-334.0.0 (c (n "rustc-ap-serialize") (v "334.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1y3hi4z5p73gil24dmpd7k089awgivfnfnwazk3h445814wjyqvm")))

(define-public crate-rustc-ap-serialize-335.0.0 (c (n "rustc-ap-serialize") (v "335.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qcpgciapzhzvwryrnc6nhwh7cmzw89qg8zadk4bfscsqmi341fa")))

(define-public crate-rustc-ap-serialize-336.0.0 (c (n "rustc-ap-serialize") (v "336.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0w83s492p3cqs9k9gj7hnvbagn7cq54cxa1wb7nwm6afyj1ddxmv")))

(define-public crate-rustc-ap-serialize-337.0.0 (c (n "rustc-ap-serialize") (v "337.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1dazrjcwj7g2clyp0d809rwl7gn3pqnl95klk6fli36f40aw8xap")))

(define-public crate-rustc-ap-serialize-338.0.0 (c (n "rustc-ap-serialize") (v "338.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ih034ls5csbsnq0zim977icl5hyyl3kfkk2ql5882d3l8b6q25y")))

(define-public crate-rustc-ap-serialize-339.0.0 (c (n "rustc-ap-serialize") (v "339.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0c07dkgm8bcszmnrz8jlvgqwd3m66kxnjqlh0yi200xw3sw96i6y")))

(define-public crate-rustc-ap-serialize-340.0.0 (c (n "rustc-ap-serialize") (v "340.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xpfi5sc5skzn2ygfxd5wppj3g7216hcg8m4xajmlrradyl53jqb")))

(define-public crate-rustc-ap-serialize-341.0.0 (c (n "rustc-ap-serialize") (v "341.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1giqqf7793bidz6pr73mcw0dj2xp1iygjifipfcg43a2b5hj43sf")))

(define-public crate-rustc-ap-serialize-342.0.0 (c (n "rustc-ap-serialize") (v "342.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qix05sq8vdgzaagis36r5z8h9ax78zjg28rwq9j77dci7rkpfqp")))

(define-public crate-rustc-ap-serialize-343.0.0 (c (n "rustc-ap-serialize") (v "343.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0dzfywc67gpcq7n2g3hzw7vzqv17wlk3f596vjlnanvc18db9kmh")))

(define-public crate-rustc-ap-serialize-344.0.0 (c (n "rustc-ap-serialize") (v "344.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mn7sg2qv41zmbql9q960klf7dq1z9bmn9464w0b20xvzb10gdag")))

(define-public crate-rustc-ap-serialize-345.0.0 (c (n "rustc-ap-serialize") (v "345.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ai5nkh5arpbs6yzmfqkgsn1yfl9znjjnk8n4vh9azw99p0r6nv2")))

(define-public crate-rustc-ap-serialize-346.0.0 (c (n "rustc-ap-serialize") (v "346.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1v4bnf5m46s276lg8fjsa85agpf8y9hc673ps6ddqfz3r71cga2z")))

(define-public crate-rustc-ap-serialize-347.0.0 (c (n "rustc-ap-serialize") (v "347.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cvxknkm2sq64w3hv8yl5p33kn02ni7931rjq6bb05wnd0dqc7dh")))

(define-public crate-rustc-ap-serialize-348.0.0 (c (n "rustc-ap-serialize") (v "348.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0w7gx3yqr7369p8cjdrng73yjzmpmbrjl6p7k8zw1vmjb31dmsz0")))

(define-public crate-rustc-ap-serialize-349.0.0 (c (n "rustc-ap-serialize") (v "349.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0f7dharzjrmxqp4hld12s6pnph70vxklyi3y44w10s2vgs8gxhnl")))

(define-public crate-rustc-ap-serialize-350.0.0 (c (n "rustc-ap-serialize") (v "350.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1shb5x4bd35yqlg8nmb7bc7flqfxfwxmns646mkqbi653zm1nry0")))

(define-public crate-rustc-ap-serialize-351.0.0 (c (n "rustc-ap-serialize") (v "351.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13cj144b8bd9pk582k7h2jkxbm064gkmy2vs2d7gpn0ia1smvrfn")))

(define-public crate-rustc-ap-serialize-352.0.0 (c (n "rustc-ap-serialize") (v "352.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0lckgg5fl4c41m7171qpx9bch5c6xcv00dxff76m1wcyisfr8qxk")))

(define-public crate-rustc-ap-serialize-353.0.0 (c (n "rustc-ap-serialize") (v "353.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xqs0dc03csx0vw6193i8sdcp3ybzs5rx8qbfmbv6hi2c2pgibjm")))

(define-public crate-rustc-ap-serialize-354.0.0 (c (n "rustc-ap-serialize") (v "354.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ymd2ylzg59fbsqa305qnfi0491z94fm3z0mzirnpk25sr9gd641")))

(define-public crate-rustc-ap-serialize-355.0.0 (c (n "rustc-ap-serialize") (v "355.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qp445gd09mlmnfzjhbxlfwpd99incwzrzbma9g667bi198gbk8j")))

(define-public crate-rustc-ap-serialize-356.0.0 (c (n "rustc-ap-serialize") (v "356.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ln9d4b3h58kpdh4raxpxgsmlwx8ww5sir7w5dfisb851anj5ca5")))

(define-public crate-rustc-ap-serialize-357.0.0 (c (n "rustc-ap-serialize") (v "357.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0lpx07g4w5svma39svjd07d8yrchs502zf9ivipvsyxyj51nc2p9")))

(define-public crate-rustc-ap-serialize-358.0.0 (c (n "rustc-ap-serialize") (v "358.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11pid88nrvpyp4z3lhnq17zmsdwh7qsm68ms9c7pl2v84qpil86p")))

(define-public crate-rustc-ap-serialize-359.0.0 (c (n "rustc-ap-serialize") (v "359.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0wbn0jn06kdwfcrrksfzw48iknsf5ixvrz3zk0isnxqmj71gwii4")))

(define-public crate-rustc-ap-serialize-360.0.0 (c (n "rustc-ap-serialize") (v "360.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11cydbvj58j5ydk7b645prs5nf7wyb3naacp9yjga31iw714j0hs")))

(define-public crate-rustc-ap-serialize-361.0.0 (c (n "rustc-ap-serialize") (v "361.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0dl199lhc4mxpd57x266m8kzwq6y1kgaihviciwpmldzx7ywsvb8")))

(define-public crate-rustc-ap-serialize-362.0.0 (c (n "rustc-ap-serialize") (v "362.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1rclng9dbsfcq3afybsqka63s3599wmmsyh82r7d9551h795lp46")))

(define-public crate-rustc-ap-serialize-363.0.0 (c (n "rustc-ap-serialize") (v "363.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0fldqk6mibbhl2mhwhdarigqlzak8g3i5mn4357zrgdqw9l0j4lb")))

(define-public crate-rustc-ap-serialize-364.0.0 (c (n "rustc-ap-serialize") (v "364.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1k0hm3ik4a5vvfhifqapwabwrjwv30hhagk1gd1a83rkjgidp6s7")))

(define-public crate-rustc-ap-serialize-365.0.0 (c (n "rustc-ap-serialize") (v "365.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cjgh9mriy64hh9yvrh9l8326diw5affizxvwd79as7qnsk34myr")))

(define-public crate-rustc-ap-serialize-366.0.0 (c (n "rustc-ap-serialize") (v "366.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1yfzb3wrc8adm9w8ssspfahdfk0lbm8f44mm1dfhpsizwrr72g69")))

(define-public crate-rustc-ap-serialize-367.0.0 (c (n "rustc-ap-serialize") (v "367.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ahqa954immiri4sdvs7s0qqzdhvldrnvl3s3w0042khc5pvr36b")))

(define-public crate-rustc-ap-serialize-368.0.0 (c (n "rustc-ap-serialize") (v "368.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1dhmnr2imxc26lnn44svvmfn1kcjizcv60q39hfaib8lm54a2cnq")))

(define-public crate-rustc-ap-serialize-369.0.0 (c (n "rustc-ap-serialize") (v "369.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1h6aipssxx3cfmwdxxdw535xllwcf0qr5hf6l0mgaz0q6qy2w4wx")))

(define-public crate-rustc-ap-serialize-370.0.0 (c (n "rustc-ap-serialize") (v "370.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0a8770k4b0v8nynga0dfiwkiwgi0njkvfikqfvw25c4zcd7vam1x")))

(define-public crate-rustc-ap-serialize-371.0.0 (c (n "rustc-ap-serialize") (v "371.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "158sjj5h6wqq4a90wj9hb9g9dj86hhif06qgfz5q8n2k48k2ncf9")))

(define-public crate-rustc-ap-serialize-372.0.0 (c (n "rustc-ap-serialize") (v "372.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "14mhx4vrj03jhiq2iq3s83500yi9nzzvqp0va2vg4sma7fim84d4")))

(define-public crate-rustc-ap-serialize-373.0.0 (c (n "rustc-ap-serialize") (v "373.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0mf7fy57r5czzjp3i87nxly11hapwa9k6ccarfxhhklyiha0na8c")))

(define-public crate-rustc-ap-serialize-374.0.0 (c (n "rustc-ap-serialize") (v "374.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "09sbz6dai5lh71zxzkrnfszlj03nh14yyhjwy4qfxsjsga91djms")))

(define-public crate-rustc-ap-serialize-375.0.0 (c (n "rustc-ap-serialize") (v "375.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "07k7hs2q7k4w9wqqbdazlwfjjpcvl3x0yshyfj03qr6sfvdyhni6")))

(define-public crate-rustc-ap-serialize-376.0.0 (c (n "rustc-ap-serialize") (v "376.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13ai88ld1px4zy8kawvz3kkyrqcb1qk8k164das0gpa40wq8g498")))

(define-public crate-rustc-ap-serialize-377.0.0 (c (n "rustc-ap-serialize") (v "377.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "171ih5z5b7jjj2lcsfwi45id6cyi5sl3c3aazpj4w25bqw8z8i0m")))

(define-public crate-rustc-ap-serialize-378.0.0 (c (n "rustc-ap-serialize") (v "378.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1dz702dj6larhcxqh0vls2kryknaqsayd327z8yrzdj4zirdw6hg")))

(define-public crate-rustc-ap-serialize-379.0.0 (c (n "rustc-ap-serialize") (v "379.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "08x2711nq3q1g4f13gm30f8n4f2zdfbf12y098hm6ls48nad821j")))

(define-public crate-rustc-ap-serialize-380.0.0 (c (n "rustc-ap-serialize") (v "380.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1f4yxkmf5khz72q4k8454c3n2p5lvhx77qylaacna5iw9jqi0xd2")))

(define-public crate-rustc-ap-serialize-381.0.0 (c (n "rustc-ap-serialize") (v "381.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0x25bzvwxqvaqvld6m9kiqiaa82963qk83ppxq8c5ip20jg1l8qq")))

(define-public crate-rustc-ap-serialize-382.0.0 (c (n "rustc-ap-serialize") (v "382.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1y38yrrx8yd66jca41db3jdpjmnapmcfmkanjr5mmp6al6fdc75w")))

(define-public crate-rustc-ap-serialize-383.0.0 (c (n "rustc-ap-serialize") (v "383.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1vqhliw0zix9yxspwp66z7n1zk79y6mds0cdsf8g1q6abdvjq2xv")))

(define-public crate-rustc-ap-serialize-384.0.0 (c (n "rustc-ap-serialize") (v "384.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "01z3l5qajmnj9648wxhpn6cj0y6j143gz6mhgsdaa1qsxjwg5c6m")))

(define-public crate-rustc-ap-serialize-385.0.0 (c (n "rustc-ap-serialize") (v "385.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ykh1m3rc8h44rgnihqg8ydg8379psdka0ry39xlg1plw0n0jzl8")))

(define-public crate-rustc-ap-serialize-386.0.0 (c (n "rustc-ap-serialize") (v "386.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16fkk2g5d5dz3ghld63rqhnhk994bdyplyfa37h9lpqc8f88mx40")))

(define-public crate-rustc-ap-serialize-387.0.0 (c (n "rustc-ap-serialize") (v "387.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0yqxv9chrssb651h30an1jz46nfsv2ciia1wdc9vibx2palccv3f")))

(define-public crate-rustc-ap-serialize-388.0.0 (c (n "rustc-ap-serialize") (v "388.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0r4d3cl3sd3ri3rs5bb6rjv3xp2rjg6267vnxdffcf0aah33fh4j")))

(define-public crate-rustc-ap-serialize-389.0.0 (c (n "rustc-ap-serialize") (v "389.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0zpky05wagisi1aibv4rv2nhvzl273rq6kwm4wb3862xjm45chfi")))

(define-public crate-rustc-ap-serialize-390.0.0 (c (n "rustc-ap-serialize") (v "390.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nx44a6fksxbklzsqm711dg4qxxa8j4cxz1vd64ndyrnzi4py1cb")))

(define-public crate-rustc-ap-serialize-391.0.0 (c (n "rustc-ap-serialize") (v "391.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1gj10v72rwmjlm47s1v6ag8d4ibk50pg1azw4m1zs1qn5hwr3h6m")))

(define-public crate-rustc-ap-serialize-392.0.0 (c (n "rustc-ap-serialize") (v "392.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0m23hvwyvj9v2sk6llcr3yj7fpz69qbm34zvwrr8bnsrhydlylr9")))

(define-public crate-rustc-ap-serialize-393.0.0 (c (n "rustc-ap-serialize") (v "393.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0bjs21bvjkg81362ld1l38xbx50y4a8b21mbj1nrw4yqvrqvinfd")))

(define-public crate-rustc-ap-serialize-394.0.0 (c (n "rustc-ap-serialize") (v "394.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1xypzph91p5smw5hjaxbj3skih9q2aq9maw5pm3xnw3z3z9q9s2y")))

(define-public crate-rustc-ap-serialize-395.0.0 (c (n "rustc-ap-serialize") (v "395.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1jxsm2c9z01aivlp1w50plw07kp3f7cmnby6z2ab8zrymf55iw0l")))

(define-public crate-rustc-ap-serialize-396.0.0 (c (n "rustc-ap-serialize") (v "396.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0fzhwvmwpfc7w9rl7lijw7xa0x2av7rlb8fj4ydnmasjly7qznak")))

(define-public crate-rustc-ap-serialize-397.0.0 (c (n "rustc-ap-serialize") (v "397.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "03vka0qffjfy6mzfjb4br7yfygfp3iq2ca9kgxk58pc70b6lm7ql")))

(define-public crate-rustc-ap-serialize-398.0.0 (c (n "rustc-ap-serialize") (v "398.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "04sla1hn5jbqg4cdwmyhfk9q4v2bdz93271wy9hzpx6gxv7myxnm")))

(define-public crate-rustc-ap-serialize-399.0.0 (c (n "rustc-ap-serialize") (v "399.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0hxl1ma15kzsvh9wqxz7wmf12s5jnd4i0gdigim83ngv2hzbp57y")))

(define-public crate-rustc-ap-serialize-400.0.0 (c (n "rustc-ap-serialize") (v "400.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1l1qd4ckic06qxv3vgkz9sc2537q0ghipm7a3wxdk6c4y6grdmn6")))

(define-public crate-rustc-ap-serialize-401.0.0 (c (n "rustc-ap-serialize") (v "401.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1d7k80pjcjlcqvi7q7p668dsklicl5ip8wah2yvpb3zfb3b1dbfr")))

(define-public crate-rustc-ap-serialize-402.0.0 (c (n "rustc-ap-serialize") (v "402.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0nnqh3iibh84916gpgkafyg794d9ch4c4sims3val9cwp1zg4xrk")))

(define-public crate-rustc-ap-serialize-403.0.0 (c (n "rustc-ap-serialize") (v "403.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1bl6g3avlf76hrr1bcklcfi8qpkc7i602k5d6cfzali7nn78w81x")))

(define-public crate-rustc-ap-serialize-404.0.0 (c (n "rustc-ap-serialize") (v "404.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "084gc27gp9rpq1nlm3s4k8gcjmq7p4z9k50hlqv18h7w1vsc734c")))

(define-public crate-rustc-ap-serialize-405.0.0 (c (n "rustc-ap-serialize") (v "405.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "10ffvvc15278h36cf6b77apynrdxjgm5av93ifzh8d7swnfh7m7b")))

(define-public crate-rustc-ap-serialize-406.0.0 (c (n "rustc-ap-serialize") (v "406.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "169jkmj473djnlnj31z2faz0kqs497wahbmqpyflhm3zywdr1fwy")))

(define-public crate-rustc-ap-serialize-407.0.0 (c (n "rustc-ap-serialize") (v "407.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0lvzn798yssn4fl4m8rxygkn7m6q0zkcyi3m23yv14p8mq5cc2fg")))

(define-public crate-rustc-ap-serialize-408.0.0 (c (n "rustc-ap-serialize") (v "408.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0pl3644fkqa8wq3cgwcfwhwd27v5zx7hy3r7svd5vj4rx0l5w49s")))

(define-public crate-rustc-ap-serialize-409.0.0 (c (n "rustc-ap-serialize") (v "409.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i4p1zip0fsh2xcxd8c0a54s2i333phb35dc8wjh1pbjwrpj8y2f")))

(define-public crate-rustc-ap-serialize-410.0.0 (c (n "rustc-ap-serialize") (v "410.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1lip50ja7papikqharlhglw5nydqcylidb7lp6vyzla9x6w0088j")))

(define-public crate-rustc-ap-serialize-411.0.0 (c (n "rustc-ap-serialize") (v "411.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1hp330zbmgc1497i0v0jdv5qizvzim6ijq0mgy64jas42fd2b6z0")))

(define-public crate-rustc-ap-serialize-412.0.0 (c (n "rustc-ap-serialize") (v "412.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "08wc28vgm40pjp1xvqr6wmjffhkl7h7c6m7v5s08hdxk7m63v6rr")))

(define-public crate-rustc-ap-serialize-413.0.0 (c (n "rustc-ap-serialize") (v "413.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11i5x2ndla0rg3948dc32611g94hnpad5f96liyx10xd2kqbh33j")))

(define-public crate-rustc-ap-serialize-414.0.0 (c (n "rustc-ap-serialize") (v "414.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mfz67bisbl57r4m3mgl3wyl6fhhqxbxrs4c8ri65bclb7fg7cw1")))

(define-public crate-rustc-ap-serialize-415.0.0 (c (n "rustc-ap-serialize") (v "415.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0pcmqdiznq7gz5b6q4sz2l84fj60dsvldldimgj0y6vphnfv3rii")))

(define-public crate-rustc-ap-serialize-416.0.0 (c (n "rustc-ap-serialize") (v "416.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mrnq0rw6ds8vsycdb6abrv33f1pn6d7xy4q8zmpvr3wvk48mjfy")))

(define-public crate-rustc-ap-serialize-417.0.0 (c (n "rustc-ap-serialize") (v "417.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cbgggry5blpg3p40zfgjwvclakmspl8fv3w01rzwsks3542n61i")))

(define-public crate-rustc-ap-serialize-418.0.0 (c (n "rustc-ap-serialize") (v "418.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cnxjpycprjqkzjzff8kl57i151ympgwzmxwjmji4bqj0dyhsh9b")))

(define-public crate-rustc-ap-serialize-419.0.0 (c (n "rustc-ap-serialize") (v "419.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11nww8jk6s6spdy01gkiwjyq8mipjlxw08kz0p8pvg8xg0cggwjf")))

(define-public crate-rustc-ap-serialize-420.0.0 (c (n "rustc-ap-serialize") (v "420.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16marvhqil5lk251vzky6rm3xiqk2fgqs3cgpbmsyrj2z3a4vm69")))

(define-public crate-rustc-ap-serialize-421.0.0 (c (n "rustc-ap-serialize") (v "421.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1pa3k1aava427cg7xiq9pw7bzbk2f60si03vxg6sccgp36bz8amd")))

(define-public crate-rustc-ap-serialize-422.0.0 (c (n "rustc-ap-serialize") (v "422.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0h6bqnw3bw5jz2vl3dka1wwsqq0wfn5886j2s1m9v6hi2p78fnlc")))

(define-public crate-rustc-ap-serialize-423.0.0 (c (n "rustc-ap-serialize") (v "423.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0bwr8xil3k4d37vh39j29wgfqyfphfkbf9zchksq2val23p4m577")))

(define-public crate-rustc-ap-serialize-424.0.0 (c (n "rustc-ap-serialize") (v "424.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1jid0cp48blyw57ch67j6v4p7m1s0cs3dm0mgs2pm5xy7w87i329")))

(define-public crate-rustc-ap-serialize-425.0.0 (c (n "rustc-ap-serialize") (v "425.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1j77ks3fmayfkrhjfbd2v9q4j7sfvk9s7yl7gy89p7a627w77i9m")))

(define-public crate-rustc-ap-serialize-426.0.0 (c (n "rustc-ap-serialize") (v "426.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "109wlicijgxz2jq3fk6djdh20wphcdk2i7icqvpvf5zkjzwj2qys")))

(define-public crate-rustc-ap-serialize-427.0.0 (c (n "rustc-ap-serialize") (v "427.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nmlmx40k8s42nyqywr8mml94j1v7xfqd41rkiina56hc9i8wvhr")))

(define-public crate-rustc-ap-serialize-428.0.0 (c (n "rustc-ap-serialize") (v "428.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1da4szb9ik2mm5vlmzawabvjvygc2lr0ds3q918qic1x6wphzdz9")))

(define-public crate-rustc-ap-serialize-429.0.0 (c (n "rustc-ap-serialize") (v "429.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0k1469r586b5wjzffgpq95bnkgr0yq1rq9gxahsak7lnhmdky595")))

(define-public crate-rustc-ap-serialize-430.0.0 (c (n "rustc-ap-serialize") (v "430.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0gkcnha9rrdpfcqmm3xv9pndhfab0v980bxlb5qqra6gy2dm97mg")))

(define-public crate-rustc-ap-serialize-431.0.0 (c (n "rustc-ap-serialize") (v "431.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "09xw22ni7lz7b6nza19gvi50hc3r2190lz2bblryvr7lbfw48i6h")))

(define-public crate-rustc-ap-serialize-432.0.0 (c (n "rustc-ap-serialize") (v "432.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cx9hb8mjrg9k1kgqy17jqfv0wfmlcak0wgafmshl42hcvl1n7cb")))

(define-public crate-rustc-ap-serialize-433.0.0 (c (n "rustc-ap-serialize") (v "433.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nimg8jf9s1i16kqjxr20agg85532ki01i0rw34j3qa2h6k9z193")))

(define-public crate-rustc-ap-serialize-434.0.0 (c (n "rustc-ap-serialize") (v "434.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qbwdyvzvqch7ihqq6a0jbvp4ihmfpnyx8201ac7n72qbn6v5hn3")))

(define-public crate-rustc-ap-serialize-435.0.0 (c (n "rustc-ap-serialize") (v "435.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "043xsb72p2vr01klm1cdsrd1skwqfflpniwzg9dvi3h9niyal15q")))

(define-public crate-rustc-ap-serialize-436.0.0 (c (n "rustc-ap-serialize") (v "436.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1mp0radwahpdz61g3r222vhc3558b8i10qd6by3gp6q11a87n147")))

(define-public crate-rustc-ap-serialize-437.0.0 (c (n "rustc-ap-serialize") (v "437.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0az37mhyq02hii4r44a96dqi5k6f3m69i27r1mky4bxzqicjcy9a")))

(define-public crate-rustc-ap-serialize-438.0.0 (c (n "rustc-ap-serialize") (v "438.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11m5ic6gvk6jrmwkrpapzv3yf4xl3040hvla5yiwm7zk8f6wchzz")))

(define-public crate-rustc-ap-serialize-439.0.0 (c (n "rustc-ap-serialize") (v "439.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "07hri4mjf3giflfvbjhbzgrrx7rv8xqsfhjih97wpa912lp8vip5")))

(define-public crate-rustc-ap-serialize-440.0.0 (c (n "rustc-ap-serialize") (v "440.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1hzpi1rvxly53dkg2g2gj40a4qgd9qcaww1ym89rxvzhrnh8jn1x")))

(define-public crate-rustc-ap-serialize-441.0.0 (c (n "rustc-ap-serialize") (v "441.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "01myi6y04938590w7syd7dbmpy70nlarf119am4y1ndjjcxysx9z")))

(define-public crate-rustc-ap-serialize-442.0.0 (c (n "rustc-ap-serialize") (v "442.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1h10fl7yxab69zxvxzi10km03qls0z3960c7a9hq5k4pmh5mmymc")))

(define-public crate-rustc-ap-serialize-443.0.0 (c (n "rustc-ap-serialize") (v "443.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "100w2m335fcci378nhszkmwc7d2ygp9wgcfgp41yc2q5n9rhxjjn")))

(define-public crate-rustc-ap-serialize-444.0.0 (c (n "rustc-ap-serialize") (v "444.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "18sqih1izlsz546s2l8mbq4j3pwmsl33qigk51iz5g34rd180ahm")))

(define-public crate-rustc-ap-serialize-445.0.0 (c (n "rustc-ap-serialize") (v "445.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1pzrnyh8mxmpqcvl51bicrhiw6v0zs76v4dbj655jqq3hsvjwaw7")))

(define-public crate-rustc-ap-serialize-446.0.0 (c (n "rustc-ap-serialize") (v "446.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "105q4hgyskx3f7ivnvqrg0vbii1h5b00n1nqgkf95hl4ifjw50fp")))

(define-public crate-rustc-ap-serialize-447.0.0 (c (n "rustc-ap-serialize") (v "447.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0jwxgk18j2xxk69c832agd3l93b47y3g641r6n16jrsfrv7mam4k")))

(define-public crate-rustc-ap-serialize-448.0.0 (c (n "rustc-ap-serialize") (v "448.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1md84b995w10pbhdd3bw2yf4z0pj2ky78vkd9zlkklad1wd3fvqf")))

(define-public crate-rustc-ap-serialize-449.0.0 (c (n "rustc-ap-serialize") (v "449.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "147r3r86pj5hh0f7harxlp7xg3iqqramw6mzf1da81w0yk6bs3gv")))

(define-public crate-rustc-ap-serialize-450.0.0 (c (n "rustc-ap-serialize") (v "450.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0hibpnkd1h9jxs8gmqhh14ild86s50006ylhvzyyibxsg0781kj9")))

(define-public crate-rustc-ap-serialize-451.0.0 (c (n "rustc-ap-serialize") (v "451.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "152jabmla15z0cfp74rpn58m4y7rdv086scsickzlxkvw9qjlgaq")))

(define-public crate-rustc-ap-serialize-452.0.0 (c (n "rustc-ap-serialize") (v "452.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0mb6glrp45b4jryx1idx9arkbb7gd9avn067dfh1zzd752b0qmy7")))

(define-public crate-rustc-ap-serialize-453.0.0 (c (n "rustc-ap-serialize") (v "453.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "15hdc2xxgdmr1hnibqh59xdwd4qxc534lz2y4r7rmnidfr9sisfj")))

(define-public crate-rustc-ap-serialize-454.0.0 (c (n "rustc-ap-serialize") (v "454.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "10fd1ylqadgxdli94i6m8v7anm0rch76w9w1xf6lq88ak1g51pk5")))

(define-public crate-rustc-ap-serialize-455.0.0 (c (n "rustc-ap-serialize") (v "455.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0cbjilh6mcykk0ll7nqifirmn419yy9da1r2dlgl9b6dpmm3djqj")))

(define-public crate-rustc-ap-serialize-456.0.0 (c (n "rustc-ap-serialize") (v "456.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0y30sqixyfnj0dycvwlqhcp57lsczwf1vl0hzvjw98bdma1ngbix")))

(define-public crate-rustc-ap-serialize-457.0.0 (c (n "rustc-ap-serialize") (v "457.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1pdkf4vg2vl3bln0q2izg1y9dc25kjihf7h1bmzw39qljr3v63fa")))

(define-public crate-rustc-ap-serialize-458.0.0 (c (n "rustc-ap-serialize") (v "458.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1428x2mazgjkhvh8v05inmf9jzllvjl80229idfrw3p5sz9i3aix")))

(define-public crate-rustc-ap-serialize-459.0.0 (c (n "rustc-ap-serialize") (v "459.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "046cn1spwpmwnsy8s6zaq3kawgcjcj4y5dkbkaxfar65v7h1z1rd")))

(define-public crate-rustc-ap-serialize-460.0.0 (c (n "rustc-ap-serialize") (v "460.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0cw32dvs0dmsaqn66l9yz7ilkwwx9aa8ka44nmhcyb7my4xy5qwq")))

(define-public crate-rustc-ap-serialize-461.0.0 (c (n "rustc-ap-serialize") (v "461.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1fzflyaalxs0z2664cfxfydzs89ms4cisdy20q6lxkrwbcz9nsmf")))

(define-public crate-rustc-ap-serialize-462.0.0 (c (n "rustc-ap-serialize") (v "462.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13md5pgz54sk4daxb05h24gz1snf8hyqm8z57ivr7z28yz8ibdy3")))

(define-public crate-rustc-ap-serialize-463.0.0 (c (n "rustc-ap-serialize") (v "463.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0lqj58zgwixc7lwxfhb418hw2nhm869j6w1wpivjwrvdy9v6pai5")))

(define-public crate-rustc-ap-serialize-464.0.0 (c (n "rustc-ap-serialize") (v "464.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0n8bd89ipgq03apb21pgc5kxg9gf18bmdw2k1hz45ii92cpz7xsj")))

(define-public crate-rustc-ap-serialize-465.0.0 (c (n "rustc-ap-serialize") (v "465.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0n775ivz129k2n519xfdnygjqip2h2jicci52xbm3zrcxff72ab0")))

(define-public crate-rustc-ap-serialize-466.0.0 (c (n "rustc-ap-serialize") (v "466.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0x7284b7nqzd31j29jpyb8a9vv0ws8hx3mwpng29f8nxyyxjk488")))

(define-public crate-rustc-ap-serialize-467.0.0 (c (n "rustc-ap-serialize") (v "467.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0iqds0wwzd5ylx4fb6b2z30wmq2lmjgb583mz2z5qiri2jpz6qk1")))

(define-public crate-rustc-ap-serialize-468.0.0 (c (n "rustc-ap-serialize") (v "468.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "19dr4959zc4b55cmvzb4l0cgfml4971wvfgp7nk29b1iix4mkpp0")))

(define-public crate-rustc-ap-serialize-469.0.0 (c (n "rustc-ap-serialize") (v "469.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1is5nd78c3wl5yfpxv0rj7g27cb2lw88hk3gk06l9aq5qxjan5x7")))

(define-public crate-rustc-ap-serialize-470.0.0 (c (n "rustc-ap-serialize") (v "470.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11cbmmbwivv1x6lizbkydhd6alh6jznqsr6i0l1g6yckvmwnba2h")))

(define-public crate-rustc-ap-serialize-471.0.0 (c (n "rustc-ap-serialize") (v "471.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cjblzccccd6axvcdyr4cjdz9gjwzg0dx3r654frjf7lhspwj2hz")))

(define-public crate-rustc-ap-serialize-472.0.0 (c (n "rustc-ap-serialize") (v "472.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1l1f10pyinnyyjmzwxmcjrpqlhbkf8dhad08x2w6j26f4fi7gi6g")))

(define-public crate-rustc-ap-serialize-473.0.0 (c (n "rustc-ap-serialize") (v "473.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13mmh71kwmqy0cmbv7nbr7164cxcbayhp3xi89nkqk8grnq1d4ys")))

(define-public crate-rustc-ap-serialize-474.0.0 (c (n "rustc-ap-serialize") (v "474.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0gs88fl0ajwfka83daw0yw6szl34xajci6pbhq9s4sd45ivhb1cd")))

(define-public crate-rustc-ap-serialize-475.0.0 (c (n "rustc-ap-serialize") (v "475.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1axfsw13kr795nyp4hryd5l93djrky45p160654yljii2pf2163s")))

(define-public crate-rustc-ap-serialize-476.0.0 (c (n "rustc-ap-serialize") (v "476.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1b4vfpa1w7iw0g376b2azv7hjdbyr9bd43xqk6546m6knmkxgizn")))

(define-public crate-rustc-ap-serialize-477.0.0 (c (n "rustc-ap-serialize") (v "477.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0cj68npj05kxvgy7y3i2kpmi4p2n97k32q3cyj9h7ny05x6cwmcc")))

(define-public crate-rustc-ap-serialize-478.0.0 (c (n "rustc-ap-serialize") (v "478.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11zb1fqy73hp9p3cnsfcmwjf92bzmvv90n7ci9sv5sd9piff9vbh")))

(define-public crate-rustc-ap-serialize-479.0.0 (c (n "rustc-ap-serialize") (v "479.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0zjsali06dv6j20ady6dc59r0c3zwvjp6618nr1pa0rjw32w6fqm")))

(define-public crate-rustc-ap-serialize-480.0.0 (c (n "rustc-ap-serialize") (v "480.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "156qixla794lii0xvs9aavbvmqxg05c41nf5mvws27crj08m6rm4")))

(define-public crate-rustc-ap-serialize-481.0.0 (c (n "rustc-ap-serialize") (v "481.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1laz23l3smrvhly5grbsfj3x407imr6l27vcq0iswsh1pyw3wrdc")))

(define-public crate-rustc-ap-serialize-482.0.0 (c (n "rustc-ap-serialize") (v "482.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qcd0cli5qggnm3bywz3j41cgx58mbdahd6rd4gwxnlhlnmvcc5s")))

(define-public crate-rustc-ap-serialize-483.0.0 (c (n "rustc-ap-serialize") (v "483.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0hgx1qb2i3gs5zpp25xw9f3g7xdi3jk7m439n5wm2mkb2527f6m5")))

(define-public crate-rustc-ap-serialize-484.0.0 (c (n "rustc-ap-serialize") (v "484.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0agm2khcnghp2rqdbm93m2c33ha696yrv11mnpy0hq0735dcm829")))

(define-public crate-rustc-ap-serialize-485.0.0 (c (n "rustc-ap-serialize") (v "485.0.0") (d (list (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "09x14jx0j05sm28piplqfra6v5x1z6anwpzpw5mklsas9l810xmw")))

(define-public crate-rustc-ap-serialize-486.0.0 (c (n "rustc-ap-serialize") (v "486.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1d7k3l64iy4363rrhjv65b3zb6lw6vs9ncr4wgg8sfzacxzfmrlx")))

(define-public crate-rustc-ap-serialize-487.0.0 (c (n "rustc-ap-serialize") (v "487.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0p0cvdrvfnnzv233301vwlksbs12wq39gi5asxqph11cdq9d65c7")))

(define-public crate-rustc-ap-serialize-488.0.0 (c (n "rustc-ap-serialize") (v "488.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nj2s6rrlcsdqvxgwdsaz9l9a0fddlbfn13g4qbfqymxry38lqkw")))

(define-public crate-rustc-ap-serialize-489.0.0 (c (n "rustc-ap-serialize") (v "489.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11clzha9cb7dix6w38n9idgkb36qrryaray0nagjpsn0951ym3rf")))

(define-public crate-rustc-ap-serialize-490.0.0 (c (n "rustc-ap-serialize") (v "490.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "18i1k66miq8hgbm00x947xrwbbn2cji24abzx7kw92pv4zw7nz07")))

(define-public crate-rustc-ap-serialize-491.0.0 (c (n "rustc-ap-serialize") (v "491.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "00hm7jdfafslz2y1qxvc7kbhwnrii7nb65bqy8pg6p6125f48r4l")))

(define-public crate-rustc-ap-serialize-492.0.0 (c (n "rustc-ap-serialize") (v "492.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0vb2vvlcl7gqkwlzwx6s4jlb50wi05my0llffx8r0krwjm7pz6yq")))

(define-public crate-rustc-ap-serialize-493.0.0 (c (n "rustc-ap-serialize") (v "493.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11ghgxzrmw98x0zn47jw1n6a9rngd8baxp7l27s8zz1lpzj64wl7")))

(define-public crate-rustc-ap-serialize-494.0.0 (c (n "rustc-ap-serialize") (v "494.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qkvizbmz41ay73xa4r23g4gk7qrfx7gphx8l2rfsxam54ncz70c")))

(define-public crate-rustc-ap-serialize-495.0.0 (c (n "rustc-ap-serialize") (v "495.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0j7z2ygr92zb1s9fiis2bfqmzj1i0gh6p2m27rffdyvpkivkbfsc")))

(define-public crate-rustc-ap-serialize-496.0.0 (c (n "rustc-ap-serialize") (v "496.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1bdb3hxscbb28gc7p3dw3bw8xy1v70f9zq47rrl52myyc5hp8mww")))

(define-public crate-rustc-ap-serialize-497.0.0 (c (n "rustc-ap-serialize") (v "497.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0v21yjisrvivr3cscf4jjwysg9my4lsncd52s8w9m9nii0ap58gn")))

(define-public crate-rustc-ap-serialize-498.0.0 (c (n "rustc-ap-serialize") (v "498.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1y5wfha2cpzljmdaq0f4arg0mq39mk5dc2ws9vrqgvhqmsiiwzm9")))

(define-public crate-rustc-ap-serialize-499.0.0 (c (n "rustc-ap-serialize") (v "499.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1q4rwhc37whkg8mnl2s71dvfzr673gcpjhw2x8sics6zn5r2a4fq")))

(define-public crate-rustc-ap-serialize-500.0.0 (c (n "rustc-ap-serialize") (v "500.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "172ysbf8sgdg3rm1jz7m4l8ybjl9jryi3hyx795s8kyrd33babfv")))

(define-public crate-rustc-ap-serialize-501.0.0 (c (n "rustc-ap-serialize") (v "501.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0kkdqxfsc8w5l4n1lbp8i5zsnxykrl8np436992f2mf2hjmwdy6h")))

(define-public crate-rustc-ap-serialize-502.0.0 (c (n "rustc-ap-serialize") (v "502.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "196dslz55ckz97j5mjrf1j6lwm1gzg503nca2iwwd2x78pqvqin3")))

(define-public crate-rustc-ap-serialize-503.0.0 (c (n "rustc-ap-serialize") (v "503.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0z0mll8flypqhwch5p21rm3665bplcjxzrabkc9vz9gcvc40shfh")))

(define-public crate-rustc-ap-serialize-504.0.0 (c (n "rustc-ap-serialize") (v "504.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "17gj44f48yryd7h95lm9x4flzvqznl258xny0dk0gqckfd8np7dl")))

(define-public crate-rustc-ap-serialize-505.0.0 (c (n "rustc-ap-serialize") (v "505.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nl1dh6ydkzyb6w7cja6q50xks1nf05f3jqar481gpdshdrlkic9")))

(define-public crate-rustc-ap-serialize-506.0.0 (c (n "rustc-ap-serialize") (v "506.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0vbcs451l4i9if8i19gjll57yrq7dhy9j4amsr8a0k3qr9f2ic2k")))

(define-public crate-rustc-ap-serialize-507.0.0 (c (n "rustc-ap-serialize") (v "507.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "10rh97v7573b0qjxpwvx870cskljiz5dn2pykklrm7326nywzmgp")))

(define-public crate-rustc-ap-serialize-508.0.0 (c (n "rustc-ap-serialize") (v "508.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0nhgfyl8hsgcmn7aznn0pxz2kk7sjw4yqf0j362b7n9gr8m5kwxg")))

(define-public crate-rustc-ap-serialize-509.0.0 (c (n "rustc-ap-serialize") (v "509.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qxilm1jym13ak8frbrzymsnfzvg581i05ah83maajdwbca70q8c")))

(define-public crate-rustc-ap-serialize-510.0.0 (c (n "rustc-ap-serialize") (v "510.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qhi705xm8rqn0dgqi2qfvzgr5y92c29vv2y9bfrrlxbh3rs59g4")))

(define-public crate-rustc-ap-serialize-511.0.0 (c (n "rustc-ap-serialize") (v "511.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1hg4vy31f15hhljzawkv9svfhzzvai4ygl7vwyjh6rx4px645yyd")))

(define-public crate-rustc-ap-serialize-512.0.0 (c (n "rustc-ap-serialize") (v "512.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nqbbglrfhzf2gdq804s8lgbw31lsxy36ja7zg13g81lpdr8r4qa")))

(define-public crate-rustc-ap-serialize-513.0.0 (c (n "rustc-ap-serialize") (v "513.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0gifrvylsi08n96giy7nd0p8pxm2nqp77gglgla6x6d3d60yia1s")))

(define-public crate-rustc-ap-serialize-514.0.0 (c (n "rustc-ap-serialize") (v "514.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16vv5v4r4c7cjfrg3nw3prylpsl3zdbgkrg8iljv6x5zxc6klm8d")))

(define-public crate-rustc-ap-serialize-515.0.0 (c (n "rustc-ap-serialize") (v "515.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1857m3xxrp3ggdjfsy8dfzzv57m8hvaf03cqk5f0hjg6r9ylvmpi")))

(define-public crate-rustc-ap-serialize-516.0.0 (c (n "rustc-ap-serialize") (v "516.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "00g32lbj9ya1nz2mi35cqjwp1gplmrp8x2iifncqa4izcl1f2hp3")))

(define-public crate-rustc-ap-serialize-517.0.0 (c (n "rustc-ap-serialize") (v "517.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xm64sjc2sj8rpxcmc7yq73mcivicbm024qhj469r71x8zpkngfw")))

(define-public crate-rustc-ap-serialize-518.0.0 (c (n "rustc-ap-serialize") (v "518.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0dscrih72dy3h7v5nnr4m2q46zdbgymnxx81s6bjb73clg7l76q1")))

(define-public crate-rustc-ap-serialize-519.0.0 (c (n "rustc-ap-serialize") (v "519.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0paj74lw2jn7rnh662wc5wpb2jfanhlry5vls4dj508xk92f1w0a")))

(define-public crate-rustc-ap-serialize-520.0.0 (c (n "rustc-ap-serialize") (v "520.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1zxq8f8vyh44yxl93j10lq94s8pa53477zxbxs0b3c610mhcsrix")))

(define-public crate-rustc-ap-serialize-521.0.0 (c (n "rustc-ap-serialize") (v "521.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1fpdkqf9fvm0125cblbs7zly5v8przalx0j977nrh69wp1ywh37f")))

(define-public crate-rustc-ap-serialize-522.0.0 (c (n "rustc-ap-serialize") (v "522.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ac63944dm5ilasr1p85qg00h4sfpannyhyvdk81vv6zwqa5zdxq")))

(define-public crate-rustc-ap-serialize-523.0.0 (c (n "rustc-ap-serialize") (v "523.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1pqwcprri84dg0m062a1rhkrcz19kwbwckajk1rn37b7xp38dc1j")))

(define-public crate-rustc-ap-serialize-524.0.0 (c (n "rustc-ap-serialize") (v "524.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qds69nwv334j3b6cqg7hd9w5gnnc9g46fip6kz9azkwwr7nwran")))

(define-public crate-rustc-ap-serialize-525.0.0 (c (n "rustc-ap-serialize") (v "525.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1d8pf70kp9rch9ch8vmi1l96wa7qfg90qp09jlm16xcj4z5vjh4w")))

(define-public crate-rustc-ap-serialize-526.0.0 (c (n "rustc-ap-serialize") (v "526.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0cwp6gbvcp20szaf625314r9v2h715rdfgaf5rlmspzhfwi1as61")))

(define-public crate-rustc-ap-serialize-527.0.0 (c (n "rustc-ap-serialize") (v "527.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "10j27ks38wld154vw619pgz6b6h4qdpbc4w10ikfqfjrfqhc7gp3")))

(define-public crate-rustc-ap-serialize-528.0.0 (c (n "rustc-ap-serialize") (v "528.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0gzwhiskg0livvh2qxbyskxydnlxsi4pd3g6ci4cfgwxappgnc5v")))

(define-public crate-rustc-ap-serialize-529.0.0 (c (n "rustc-ap-serialize") (v "529.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1clwdd2ai9n96gx4dksb5ggichv8cgqbcd9qvhhmsn2v52zl0sgq")))

(define-public crate-rustc-ap-serialize-530.0.0 (c (n "rustc-ap-serialize") (v "530.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "107j97mkamr0ghaaz75xh8iw0q29h2bas1hp289m02kl5z0n96na")))

(define-public crate-rustc-ap-serialize-531.0.0 (c (n "rustc-ap-serialize") (v "531.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16xllk40lwc842i4gv9mfs4dzi23znrx6am9bh8x2glyj0vxf5dx")))

(define-public crate-rustc-ap-serialize-532.0.0 (c (n "rustc-ap-serialize") (v "532.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0r70hibqmbr74fw2lrms6cicgm0gqw35mpka20jxk7bcvm0v5656")))

(define-public crate-rustc-ap-serialize-533.0.0 (c (n "rustc-ap-serialize") (v "533.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1608pz84x3dzinpqrmjffhj8g86szdih5qfkvb1lq3zh4rgacglm")))

(define-public crate-rustc-ap-serialize-534.0.0 (c (n "rustc-ap-serialize") (v "534.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1hw0mp25gyk67iwyy3iacwncccvvyk5kg3pcmzn2h87car03pi3m")))

(define-public crate-rustc-ap-serialize-535.0.0 (c (n "rustc-ap-serialize") (v "535.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0kaah6lirm0dd9fkxsvkprgcdwzl5vid4kpgi1xd29r7n9f9nk2f")))

(define-public crate-rustc-ap-serialize-536.0.0 (c (n "rustc-ap-serialize") (v "536.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0x6zfazvsnrkdrhjdx0k2z22qj6xww5xcz6r96pvqsdc2zldbgcs")))

(define-public crate-rustc-ap-serialize-537.0.0 (c (n "rustc-ap-serialize") (v "537.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11ykx0hwzh1sl5w76q6ngf9yv73kyxc8n32k13msk5jsff1cp8yk")))

(define-public crate-rustc-ap-serialize-538.0.0 (c (n "rustc-ap-serialize") (v "538.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0j9qqm5sx0hzyqz7krhdzlx764wsvd8b7sjybljyj38qr25mlrai")))

(define-public crate-rustc-ap-serialize-539.0.0 (c (n "rustc-ap-serialize") (v "539.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1n87csdyp12r5qgsgykq74lfb9fn2khk5x3kdd0p44rq0fslfxva")))

(define-public crate-rustc-ap-serialize-540.0.0 (c (n "rustc-ap-serialize") (v "540.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qj88mbbss14w723a3fm5yqhqi5avc509k2kn53klk426i9f353z")))

(define-public crate-rustc-ap-serialize-541.0.0 (c (n "rustc-ap-serialize") (v "541.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1x2kycpmlwmwcw5r0nm91pd52qxyly57b79sgvc2bs0gbb9jiqwa")))

(define-public crate-rustc-ap-serialize-542.0.0 (c (n "rustc-ap-serialize") (v "542.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "08n6csrhyxgq7jjj7kpkg4iasbwmzwd5jxxs8mqlyy3pl1gjj3jm")))

(define-public crate-rustc-ap-serialize-543.0.0 (c (n "rustc-ap-serialize") (v "543.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1p3n2qdga428ncxbwjjz81izp2y0inhgairwfs2xncpzyqa0abr5")))

(define-public crate-rustc-ap-serialize-544.0.0 (c (n "rustc-ap-serialize") (v "544.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "00iin9scs2mjaqz38jfjpkxs3x912glv3pl68qippwj524a56xlc")))

(define-public crate-rustc-ap-serialize-545.0.0 (c (n "rustc-ap-serialize") (v "545.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1978vih0gdgbhw7gn47rsfvghs4gvialh1aq7wp0yiz3qzhgmzgr")))

(define-public crate-rustc-ap-serialize-546.0.0 (c (n "rustc-ap-serialize") (v "546.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "00j69mif34l26lx2ac8v0w1585smiycd30ps7w1h37h8ya1kfrv1")))

(define-public crate-rustc-ap-serialize-547.0.0 (c (n "rustc-ap-serialize") (v "547.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "027kg3sh4jjahr80yv4lc6vyi6v0jq74jfb8ip8r2nxzihliv5fs")))

(define-public crate-rustc-ap-serialize-548.0.0 (c (n "rustc-ap-serialize") (v "548.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1f7zwfmy65q1ffzaf4l90ixvwri885p2b3zbz2xbi33ja995qcdw")))

(define-public crate-rustc-ap-serialize-549.0.0 (c (n "rustc-ap-serialize") (v "549.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ry18d02z4q7hd7bdbmn6dhxjz953k3dhdv9sij9nbsgk7f7a6ic")))

(define-public crate-rustc-ap-serialize-550.0.0 (c (n "rustc-ap-serialize") (v "550.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1d3a02jy2kvksc0akfpr6gvcaghysl0vkkgi94v96c95b9srb0r2")))

(define-public crate-rustc-ap-serialize-551.0.0 (c (n "rustc-ap-serialize") (v "551.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0dxwm98l21ga1gpng4bf0v7rknpna34yfz14077wrfhvvks0419i")))

(define-public crate-rustc-ap-serialize-552.0.0 (c (n "rustc-ap-serialize") (v "552.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "18vh3z423c5kh1zyv08zxqy55v69a1sv4jnqc1bvjrzb8378j6bg")))

(define-public crate-rustc-ap-serialize-553.0.0 (c (n "rustc-ap-serialize") (v "553.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1y523hy7kffx8hg72lnj4na7frmc8jghxrvgm98yb22cid8gg9gy")))

(define-public crate-rustc-ap-serialize-554.0.0 (c (n "rustc-ap-serialize") (v "554.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0v349wnn2x6clxx5k5m8bi7j9qzlq8kgprls2hxxn2r5fvl1hcs5")))

(define-public crate-rustc-ap-serialize-555.0.0 (c (n "rustc-ap-serialize") (v "555.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1cahbk7rpprbxz59v4wjb1zm8hjp4mz257in2jxkx831ja4ggdbb")))

(define-public crate-rustc-ap-serialize-556.0.0 (c (n "rustc-ap-serialize") (v "556.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1gxphsi5q710ccajn7zs4h40zq1irj9h18li6bqr2g6v1jsmq27k")))

(define-public crate-rustc-ap-serialize-557.0.0 (c (n "rustc-ap-serialize") (v "557.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qphifpfi2mbm1ad0yp9jm9gf487fhrhfmw1jbs60plpcm8yi1bq")))

(define-public crate-rustc-ap-serialize-558.0.0 (c (n "rustc-ap-serialize") (v "558.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1yqpy2cwiyvjg8pcl3lv5yga92n2c8iiqqpq3pr1562vw84ai7xr")))

(define-public crate-rustc-ap-serialize-559.0.0 (c (n "rustc-ap-serialize") (v "559.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1129q06yqfn0afs0xm8hvi1h7a1z33wh6qb4w5vjr3mlqzh1m0si")))

(define-public crate-rustc-ap-serialize-560.0.0 (c (n "rustc-ap-serialize") (v "560.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "12cp4cvim77phiqr555r1c3xabikdmffdx5ns881nvlwxbl8djqk")))

(define-public crate-rustc-ap-serialize-561.0.0 (c (n "rustc-ap-serialize") (v "561.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0pp4i7kcbpqck4kx3jmwqrwjy4ad4iz59vlgld7dqi5w0s404c4q")))

(define-public crate-rustc-ap-serialize-562.0.0 (c (n "rustc-ap-serialize") (v "562.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16ai9ccdlrx3vdh860fhqfg9hcdgpmhnc6sswk306371v6dx07dm")))

(define-public crate-rustc-ap-serialize-563.0.0 (c (n "rustc-ap-serialize") (v "563.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "113c36vjmvcb4x0wkrmcrk6nir7x71dd7pgajh4n1i9pnp0c0qsz")))

(define-public crate-rustc-ap-serialize-564.0.0 (c (n "rustc-ap-serialize") (v "564.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1vnnwj4bshhlx3cyc4zxy3yx64bvg3gq40h1cbbsigi7bizspyzb")))

(define-public crate-rustc-ap-serialize-565.0.0 (c (n "rustc-ap-serialize") (v "565.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "05cmh06bhcsvhy5mx4lmaqrg0ah2jdnnvlivgm4i7j4q6p8shi9z")))

(define-public crate-rustc-ap-serialize-566.0.0 (c (n "rustc-ap-serialize") (v "566.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16cd70fjdrw36avv8lqlm4r65lq2k28w40hbddcxgmbw0rvcfa50")))

(define-public crate-rustc-ap-serialize-567.0.0 (c (n "rustc-ap-serialize") (v "567.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "17vmf743hzg1bdc6bsi7zmf7rjkbh1d1dzg5i8z3dk1sdxpkwr7b")))

(define-public crate-rustc-ap-serialize-568.0.0 (c (n "rustc-ap-serialize") (v "568.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0y5pzwi5mgm30jb3m1v6jcq6356h6s13ix0snhwx1hpfa46321hb")))

(define-public crate-rustc-ap-serialize-569.0.0 (c (n "rustc-ap-serialize") (v "569.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i2x3cvbr64ywivzkr06gy2axpnik338xbmx8z5nddyz9fbkk9j6")))

(define-public crate-rustc-ap-serialize-570.0.0 (c (n "rustc-ap-serialize") (v "570.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "06rhn46rfl5ragdwsjmwhpy22jw7jpmxdh7kjd26kp4l90chyflm")))

(define-public crate-rustc-ap-serialize-571.0.0 (c (n "rustc-ap-serialize") (v "571.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1kryay4dc5yd5l6y4q1p6k2cidh39p99jli0jc4bz67cs36kpw5y")))

(define-public crate-rustc-ap-serialize-572.0.0 (c (n "rustc-ap-serialize") (v "572.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "02pibz4dx11rvxskbn1gbw8zvapn7yjyc96bbagq4l593824bzsf")))

(define-public crate-rustc-ap-serialize-573.0.0 (c (n "rustc-ap-serialize") (v "573.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0d0jp2d0v6vhracgd1dvn816cyqgz2fvj6siqmicdvbgqz2an271")))

(define-public crate-rustc-ap-serialize-574.0.0 (c (n "rustc-ap-serialize") (v "574.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "125yfarykq4a39jn30qhm0irq8qxjr2m6rs09l2gv0iciy0g7n8z")))

(define-public crate-rustc-ap-serialize-575.0.0 (c (n "rustc-ap-serialize") (v "575.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "14d0kb6hw75z690a6fwdxfbflfbysqks2yskcy7l6bzzibqifzil")))

(define-public crate-rustc-ap-serialize-576.0.0 (c (n "rustc-ap-serialize") (v "576.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0r1zlab47vpq5g8nh2m5c7dqfdim5q73hgqpcawhylcjrjdc9gqh")))

(define-public crate-rustc-ap-serialize-577.0.0 (c (n "rustc-ap-serialize") (v "577.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0s1pkwa41ic4hc15kbfgxvcym3yhs4bw7gsys6w8psxxpcmc9mkf")))

(define-public crate-rustc-ap-serialize-578.0.0 (c (n "rustc-ap-serialize") (v "578.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0147dg5cfhx4jvsvjr72z4wwgwhacmjzjs46pmb2cbaq5bvnrafw")))

(define-public crate-rustc-ap-serialize-579.0.0 (c (n "rustc-ap-serialize") (v "579.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1crlx9axnf9j2agmcdpinfpdl3gv76dsxk1azc75h9ybd8i3z2pq")))

(define-public crate-rustc-ap-serialize-580.0.0 (c (n "rustc-ap-serialize") (v "580.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "15h2mdz7svfry3dja7xlpsr4g0vdymwcl2k6l995bdm9dpbic8bq")))

(define-public crate-rustc-ap-serialize-581.0.0 (c (n "rustc-ap-serialize") (v "581.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1p2skc7q9wy841hx97gnvp4wxm59pn4ixccfpk02qpg63ca9i7qy")))

(define-public crate-rustc-ap-serialize-582.0.0 (c (n "rustc-ap-serialize") (v "582.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "151h83dlzj3b90dmijnlr9sbknchhbpmbw7kl7an26gprzld5p32")))

(define-public crate-rustc-ap-serialize-583.0.0 (c (n "rustc-ap-serialize") (v "583.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1xpl03pfnvqvwalsl2wcbkzkcq9xz2cp8r2nkk9xx7pp3hiyxf9b")))

(define-public crate-rustc-ap-serialize-584.0.0 (c (n "rustc-ap-serialize") (v "584.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0rqrcvlj8zzwx41hzfpj9dvc5nm2cnxynz44i571fzg8jiwy6ib3")))

(define-public crate-rustc-ap-serialize-585.0.0 (c (n "rustc-ap-serialize") (v "585.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "15xax5q79gg71ms45yjmjjv9caz63xvrhmm2a8vs5nr77kazw6ns")))

(define-public crate-rustc-ap-serialize-586.0.0 (c (n "rustc-ap-serialize") (v "586.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0dj55wvc2fsrlqps1bdf16qyaq3xmfmhd7lqqziajnycvqskhaqy")))

(define-public crate-rustc-ap-serialize-587.0.0 (c (n "rustc-ap-serialize") (v "587.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1c58jvzhirrjaxh3k3a9n8n6rfzmrg6d3r4nzx5dra59p33mhx2z")))

(define-public crate-rustc-ap-serialize-588.0.0 (c (n "rustc-ap-serialize") (v "588.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0kak18kc4jc6f7wrinaz6bsw4qrpfmplfyyi2gzx61b5i2md9vlj")))

(define-public crate-rustc-ap-serialize-589.0.0 (c (n "rustc-ap-serialize") (v "589.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0baplsawq2yna473mahyr34zvyhp6pkhs6p0aivjfj0b7gbcsdm9")))

(define-public crate-rustc-ap-serialize-590.0.0 (c (n "rustc-ap-serialize") (v "590.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1as8lcaqfsp2d21jjvdvsfpj5s4z5wxjwm7xlkn0n4nd5cj4258p")))

(define-public crate-rustc-ap-serialize-591.0.0 (c (n "rustc-ap-serialize") (v "591.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0347mvgy8686kvzh10xqxnsvqsvvl16ghr9iqd93yka929nm7zfm")))

(define-public crate-rustc-ap-serialize-592.0.0 (c (n "rustc-ap-serialize") (v "592.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "03rsbwip4wkgg6a99kb0hn2miji5ipvzj4b41627w8sycrnq1b3y")))

(define-public crate-rustc-ap-serialize-593.0.0 (c (n "rustc-ap-serialize") (v "593.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "111wix3rjv0crjw3fnn7b9klm2wasnagzybdplf3aw22j5ghvl16")))

(define-public crate-rustc-ap-serialize-594.0.0 (c (n "rustc-ap-serialize") (v "594.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1l2pi223h0x7kw0xvwg67l60cvgmf9mzg6bnfm5p03zhsz4m23fx")))

(define-public crate-rustc-ap-serialize-595.0.0 (c (n "rustc-ap-serialize") (v "595.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xk4n9x02phz3295gnvq7xplpd881ajwavfddq7a5x3vs7g02002")))

(define-public crate-rustc-ap-serialize-596.0.0 (c (n "rustc-ap-serialize") (v "596.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0z5xggn9qqblh7zfzbj4w6mz8z5ggf95llvjbj7ykgch3n9f4207")))

(define-public crate-rustc-ap-serialize-597.0.0 (c (n "rustc-ap-serialize") (v "597.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0cqgwif1qif5ka453sa92padn6191vghazyh5fwarcv4gz5cwri4")))

(define-public crate-rustc-ap-serialize-598.0.0 (c (n "rustc-ap-serialize") (v "598.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0xvy6ird5q2865fq0xjxm5zpj1vnkkzprr6ynmwsvsphybzcqsli")))

(define-public crate-rustc-ap-serialize-599.0.0 (c (n "rustc-ap-serialize") (v "599.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0d24q6j3g64v7vwy36fj5kgbg1xxvs1zjww8c2f0aqh8w74lvm59")))

(define-public crate-rustc-ap-serialize-600.0.0 (c (n "rustc-ap-serialize") (v "600.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0q4llb8qq728w2y3jpy7mrjf1cbj2jngk4lw9gdvzial3jg0rrl1")))

(define-public crate-rustc-ap-serialize-601.0.0 (c (n "rustc-ap-serialize") (v "601.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0mjmy6cwq803ily3a7098yjbcwhj3j5lgs7phxhn4nh92mfplx31")))

(define-public crate-rustc-ap-serialize-602.0.0 (c (n "rustc-ap-serialize") (v "602.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ajanvsxh8hgjxqk9nlgn02wmpqph4yd25vwk1bhv8pv81vjyva9")))

(define-public crate-rustc-ap-serialize-603.0.0 (c (n "rustc-ap-serialize") (v "603.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "00w94agxzkfppcv9sa62s69w7dvqcx0f7f6kgdfi770p54s9knyr")))

(define-public crate-rustc-ap-serialize-604.0.0 (c (n "rustc-ap-serialize") (v "604.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "03y2qim260dfrjdbwqrr7c64v9mp3js35j42ika3dvb26idhf1lw")))

(define-public crate-rustc-ap-serialize-605.0.0 (c (n "rustc-ap-serialize") (v "605.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ylnl8hc4hcw9fnp31dzvpa0n865q6i8hgk48z5lmbl5crppg7an")))

(define-public crate-rustc-ap-serialize-606.0.0 (c (n "rustc-ap-serialize") (v "606.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "085saayyjv8pd6272mb5ksn7pf6skd1zdp7q0p686ibgx1094rwj")))

(define-public crate-rustc-ap-serialize-607.0.0 (c (n "rustc-ap-serialize") (v "607.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0c047v80d90zqp5cdqqdwfa7fnfd9kp609q6jnd6cjg6749x0y9x")))

(define-public crate-rustc-ap-serialize-608.0.0 (c (n "rustc-ap-serialize") (v "608.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0jw3l8x78p28lrrx55wscvj95nnfia296ww296qf706z1r9wna6l")))

(define-public crate-rustc-ap-serialize-609.0.0 (c (n "rustc-ap-serialize") (v "609.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0lc0bz98ijam6h25idc5539fh6r16g9ilx139nqwln4gdp02ggjz")))

(define-public crate-rustc-ap-serialize-610.0.0 (c (n "rustc-ap-serialize") (v "610.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0bj3s5kgwrjy1r83s99k2d61y9nahscyfrlqami8sh1ma7pxc75v")))

(define-public crate-rustc-ap-serialize-611.0.0 (c (n "rustc-ap-serialize") (v "611.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "16nyja025z9mzvkayzh1y0s4im4j5jg221gri77ykgh5b3v5rax2")))

(define-public crate-rustc-ap-serialize-612.0.0 (c (n "rustc-ap-serialize") (v "612.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "19259a4bb1c1jv29dly5vpx5zpzlg13fpzx93v0s0c9m6d65cqk5")))

(define-public crate-rustc-ap-serialize-613.0.0 (c (n "rustc-ap-serialize") (v "613.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0hd3z3snv18b2j1dywv3zxkdldy4vr7h91r9xx4f2fw72agvf2vx")))

(define-public crate-rustc-ap-serialize-614.0.0 (c (n "rustc-ap-serialize") (v "614.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0rif1an4kbi4m746719b50kh8fkhsxlzf41a4c137mxzass5vbzg")))

(define-public crate-rustc-ap-serialize-615.0.0 (c (n "rustc-ap-serialize") (v "615.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1wb551rf0x0vrc8vkr5k0pmn7gf7ib5npx3lkya7lrmifbizh9cz")))

(define-public crate-rustc-ap-serialize-616.0.0 (c (n "rustc-ap-serialize") (v "616.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qcvd7l1gyr58v3gj99hcbkf0cx6rbjzi10xanm49xgi01mr4403")))

(define-public crate-rustc-ap-serialize-617.0.0 (c (n "rustc-ap-serialize") (v "617.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1lzyd7yqglzi7j881c3blfhnc2nbzrinbwfv26byasx5gq93fwq1")))

(define-public crate-rustc-ap-serialize-618.0.0 (c (n "rustc-ap-serialize") (v "618.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1adgwg0pgzswmg28pqrxdqq4cqiazx382zy2b8akk0spxgc1z562")))

(define-public crate-rustc-ap-serialize-619.0.0 (c (n "rustc-ap-serialize") (v "619.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ghf71qfdhg46wb1fppyd7bb7xcrsc0vqmjsvc3ry6mx256qnf39")))

(define-public crate-rustc-ap-serialize-620.0.0 (c (n "rustc-ap-serialize") (v "620.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1vhbg09gjaw18rhm38p81x4bj90cs4dcrwq59nl7wvfljfamsg27")))

(define-public crate-rustc-ap-serialize-621.0.0 (c (n "rustc-ap-serialize") (v "621.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0c8s1b5hzfgd8xsmn023rwm9sxnask0fnl1549y4vsjvarpa9sqs")))

(define-public crate-rustc-ap-serialize-622.0.0 (c (n "rustc-ap-serialize") (v "622.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1z13g76zdgpmyapcm927s54dzg29l5zc45dkihrxhg9h8pmc5l0n")))

(define-public crate-rustc-ap-serialize-623.0.0 (c (n "rustc-ap-serialize") (v "623.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "194pm1bs4b2c46fdb0qyj5gz42lmzxgdylakhwdk41270hnpinja")))

(define-public crate-rustc-ap-serialize-624.0.0 (c (n "rustc-ap-serialize") (v "624.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1b5175j0kv52f1hm6nfmfzm3hr4ymh0jnwg7z43qnv5kcfz29da4")))

(define-public crate-rustc-ap-serialize-625.0.0 (c (n "rustc-ap-serialize") (v "625.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0wy89717ps4xkphb77vi55xzj7bw95vzbwbfbg34nqpcpqgrwrks")))

(define-public crate-rustc-ap-serialize-626.0.0 (c (n "rustc-ap-serialize") (v "626.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1r32aiidv1mjfn8mvn0fcykmnjsxh7wbx743y60xfkynhq8xf91z")))

(define-public crate-rustc-ap-serialize-627.0.0 (c (n "rustc-ap-serialize") (v "627.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "164afs0jhfq3xxcbsnymmj08z5waii5cg96pif0fsngps84k4ak6")))

(define-public crate-rustc-ap-serialize-628.0.0 (c (n "rustc-ap-serialize") (v "628.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qsw6k2fi1y4g3i02jz2m3a3zdqaf25c098kqnk7yxfz8siqd4aq")))

(define-public crate-rustc-ap-serialize-629.0.0 (c (n "rustc-ap-serialize") (v "629.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i3l6a126zzq6k1r2f6wm4li5nplbds66h85frwqhbyjc6cwrj36")))

(define-public crate-rustc-ap-serialize-630.0.0 (c (n "rustc-ap-serialize") (v "630.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1b9fw4rrbrq1n4vh2fbcl82zn0gwmzb5bkci3nr9jfizq9y18xlk")))

(define-public crate-rustc-ap-serialize-631.0.0 (c (n "rustc-ap-serialize") (v "631.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qf3lcn5ss1aw6ylqil9d57rfly65hpl8m8nv11qjgbx8l26mbvr")))

(define-public crate-rustc-ap-serialize-632.0.0 (c (n "rustc-ap-serialize") (v "632.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1nbr5ii36px8n2kyk7ncp2k7ykif7ywnmywnaknbgbjpjqc6fwkh")))

(define-public crate-rustc-ap-serialize-633.0.0 (c (n "rustc-ap-serialize") (v "633.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1s483f7c2aizpy7bxghfpgsgy4vm2wfk6pw2wrlq0fbi72vlrih9")))

(define-public crate-rustc-ap-serialize-634.0.0 (c (n "rustc-ap-serialize") (v "634.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "152qgs65cqs73kc18c4bm8jah6g1rxyqfgw477p8xrj08vgv406i")))

(define-public crate-rustc-ap-serialize-635.0.0 (c (n "rustc-ap-serialize") (v "635.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13zsabgc6g0mm4g9nfgcv25q7dm6krwnn242xqsv5c064ryza5vq")))

(define-public crate-rustc-ap-serialize-636.0.0 (c (n "rustc-ap-serialize") (v "636.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0h3hwrp759nkadsmc2rn0cfi7f8ysfdpgljpxlp1894rq21636p8")))

(define-public crate-rustc-ap-serialize-637.0.0 (c (n "rustc-ap-serialize") (v "637.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1vb2r2658ia22pb5i34vfixy1vdca6sm465axbxjd1nsays309ya")))

(define-public crate-rustc-ap-serialize-638.0.0 (c (n "rustc-ap-serialize") (v "638.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1sl19jfz4d9lmxp4kps1r7gwdwgr6ahxh4wfj70ayb50hfijpc8h")))

(define-public crate-rustc-ap-serialize-639.0.0 (c (n "rustc-ap-serialize") (v "639.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1b5mdyc9xnyvxjx5p5kwyq7qkvps8igrs099vpsr1jp2hqsdv6ki")))

(define-public crate-rustc-ap-serialize-640.0.0 (c (n "rustc-ap-serialize") (v "640.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "17drxfnwsxc8vv6ndnr40145hpiah8s65cq5wrmixmzqmfmimwj2")))

(define-public crate-rustc-ap-serialize-641.0.0 (c (n "rustc-ap-serialize") (v "641.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1207h942fr1g1bli0k4z0kyjb969v2rl16dyc1mxxp3kxys6kcqf")))

(define-public crate-rustc-ap-serialize-642.0.0 (c (n "rustc-ap-serialize") (v "642.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0hfbz1163wmdriklmj005dgpkrhbjnanlcscvab3vz2j0aniwfp1")))

(define-public crate-rustc-ap-serialize-643.0.0 (c (n "rustc-ap-serialize") (v "643.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1zmd9lrcsk8a090yjrxnm844j9v5ymaqb5s9imdnlizwhjnzs5my")))

(define-public crate-rustc-ap-serialize-644.0.0 (c (n "rustc-ap-serialize") (v "644.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "11rmwr2x7g86qnnkq0c12jzangb9zkdhw8smgg1ihsai2762kc77")))

(define-public crate-rustc-ap-serialize-645.0.0 (c (n "rustc-ap-serialize") (v "645.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "154vg3znjavs0m7zih9w9pazpjf4i79x73j82w12d2sl3a9mgn4d")))

(define-public crate-rustc-ap-serialize-646.0.0 (c (n "rustc-ap-serialize") (v "646.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "132zfq7jac59vwkzh5qc2kbr19x3h3a6nr7ni7d0r07ni89y0f98")))

(define-public crate-rustc-ap-serialize-647.0.0 (c (n "rustc-ap-serialize") (v "647.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "05crh79mhmmaqw62kw1ya06f9h7ikg6fscgj9lyj9l7idci7sc95")))

(define-public crate-rustc-ap-serialize-648.0.0 (c (n "rustc-ap-serialize") (v "648.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "04xi7fpfsfp0xcz2k7xk6i57lhlv2z2lx29kz85xcl23qq6y5iq1")))

(define-public crate-rustc-ap-serialize-649.0.0 (c (n "rustc-ap-serialize") (v "649.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "126lay683306ydrfla4afmxzhbyld8admxpkv75yg92l28gqjn8c")))

(define-public crate-rustc-ap-serialize-650.0.0 (c (n "rustc-ap-serialize") (v "650.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0106zj7gyc9c8qi5aw4bsibllzlk37l1lpk7yfxr62hgpl974mp8")))

(define-public crate-rustc-ap-serialize-651.0.0 (c (n "rustc-ap-serialize") (v "651.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i1jygbxbrr16l2s5mzmqmzaix40lwpqiyq3fddc9n4qj1jhlldg")))

(define-public crate-rustc-ap-serialize-652.0.0 (c (n "rustc-ap-serialize") (v "652.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13bnzvfxs29aqxqivzr0dsm77myg7aac3d0in7xzagysgn2flav4")))

(define-public crate-rustc-ap-serialize-653.0.0 (c (n "rustc-ap-serialize") (v "653.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0wv1wqnf323q8cnqvjf4ym2j2g1ixj9j22zjdzy99wbim8im2iqi")))

(define-public crate-rustc-ap-serialize-654.0.0 (c (n "rustc-ap-serialize") (v "654.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1vwfa3q4f9k0nfryr53jnwmf8vhaq7ijbgw8449nx467dr98yvkm")))

(define-public crate-rustc-ap-serialize-655.0.0 (c (n "rustc-ap-serialize") (v "655.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0gra4gd0v0fffivngim2mwmdx4xzdwmgmv2l8dxf4qq15vn9znln")))

(define-public crate-rustc-ap-serialize-656.0.0 (c (n "rustc-ap-serialize") (v "656.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "19krcs1hyh1lwwx3r0vf7fw63g4p9fk1i1g36lp28y7ngbja4b2v")))

(define-public crate-rustc-ap-serialize-657.0.0 (c (n "rustc-ap-serialize") (v "657.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1kpbihmn93zzh45i93jfd9s8x5wrygpsa0ldi1csr3qdxrpw8igy")))

(define-public crate-rustc-ap-serialize-658.0.0 (c (n "rustc-ap-serialize") (v "658.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ljw8k13y8nm67k5bgag6m5dmc4mim8dl6khl9666bn19fnw4khv")))

(define-public crate-rustc-ap-serialize-659.0.0 (c (n "rustc-ap-serialize") (v "659.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1ihxpq80805lgng3p9lf2bjzvvb255817g0j4x939nb9aqq5m2vn")))

(define-public crate-rustc-ap-serialize-660.0.0 (c (n "rustc-ap-serialize") (v "660.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0hibxvghdmc9mp05y128vm6mx7956dirs4a9r16222lgz8xzq63b")))

(define-public crate-rustc-ap-serialize-661.0.0 (c (n "rustc-ap-serialize") (v "661.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "09lsh1dsfib5f8cy5a0v93qqhwi7l6casjl8x8yz5hd7rqbhzgvf")))

(define-public crate-rustc-ap-serialize-662.0.0 (c (n "rustc-ap-serialize") (v "662.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.0") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0pdd146gkygifijvnjk13vzp516kk776ic1p7p59qsajhfqzjh7r")))

