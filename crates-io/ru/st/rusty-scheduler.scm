(define-module (crates-io ru st rusty-scheduler) #:use-module (crates-io))

(define-public crate-rusty-scheduler-0.1.0 (c (n "rusty-scheduler") (v "0.1.0") (h "01nhklfg7qbarsgr47k9djlpa7vs69qcnk656jgfi2cafgv7w90m")))

(define-public crate-rusty-scheduler-0.1.1 (c (n "rusty-scheduler") (v "0.1.1") (h "0gk34ycd83xkvmiisxh764w0zq189388f639qcrcqvbxvj9lv19s")))

