(define-module (crates-io ru st rustsbi) #:use-module (crates-io))

(define-public crate-rustsbi-0.0.1 (c (n "rustsbi") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.6") (d #t) (k 0)))) (h "1fc70anv999p5ws8rsaagwvsiycyzl2m62cxai29g8fhrbik9hhp")))

(define-public crate-rustsbi-0.0.2 (c (n "rustsbi") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.6") (d #t) (k 0)))) (h "1j9jlx70vh5i9fb653qpx4vr0gldg54aj3zag2jnwr9wpcd93mcs")))

(define-public crate-rustsbi-0.1.0 (c (n "rustsbi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.6") (d #t) (k 0)))) (h "1phwvgx2nan0ksbg4b5qhn3n0h2lh5903qfz0c3lvjqs3gb29ijh")))

(define-public crate-rustsbi-0.1.1 (c (n "rustsbi") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "1l11fm15praq6hy448bp4r8wf7ylwskbbfj1zvxf7575d935902p")))

(define-public crate-rustsbi-0.2.0-alpha.1 (c (n "rustsbi") (v "0.2.0-alpha.1") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0sxz90j97nniqcvj7n8sq26zjycwi9419pydcq3rlcs1k3z991jl")))

(define-public crate-rustsbi-0.2.0-alpha.2 (c (n "rustsbi") (v "0.2.0-alpha.2") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0gm2h7v3j6168ymiwdcc7lr8cag5l1ddhkgxyh03xdsih5yd60l6")))

(define-public crate-rustsbi-0.2.0-alpha.3 (c (n "rustsbi") (v "0.2.0-alpha.3") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "0xd63aji4n33z4gn095z61sjlvvfaxpznxrra6p2hqgaprvpjxd3")))

(define-public crate-rustsbi-0.2.0-alpha.4 (c (n "rustsbi") (v "0.2.0-alpha.4") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "1y3rinlcxwd2izvmha6rcmmw0gbq1ii7sjcrx26il8h8l2a8038p")))

(define-public crate-rustsbi-0.2.0-alpha.5 (c (n "rustsbi") (v "0.2.0-alpha.5") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "1v19x2s30f5lnckhw4qwfa15hibhj1sz3pgvfbxrvjllwvbppshr")))

(define-public crate-rustsbi-0.2.0-alpha.6 (c (n "rustsbi") (v "0.2.0-alpha.6") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "1l421wz69rhzwwn70nb38q91rfnn4ks1z350fhds1pxljh568l5k")))

(define-public crate-rustsbi-0.2.0-alpha.7 (c (n "rustsbi") (v "0.2.0-alpha.7") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.7") (d #t) (k 0)))) (h "1rhhir19xs5akl5adxlyadvinbj6q60afvm77p3s8pa584jkfq1g")))

(define-public crate-rustsbi-0.2.0-alpha.8 (c (n "rustsbi") (v "0.2.0-alpha.8") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "1vi5n5258hw9jc8vd0dsnmxwmz7sixlk7bjcz7g08kdr4bmb7ck3")))

(define-public crate-rustsbi-0.2.0-alpha.9 (c (n "rustsbi") (v "0.2.0-alpha.9") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "0ybk8n2pmixfvgkq00zand65782i09x8nswmbx33k0r4fb1i6qrz") (y #t)))

(define-public crate-rustsbi-0.2.0-alpha.10 (c (n "rustsbi") (v "0.2.0-alpha.10") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "1y3gdrj6l3fw57njzjl9lx6g1vay765c7kjdkcsm1qf2dc4xvfz7")))

(define-public crate-rustsbi-0.2.0 (c (n "rustsbi") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "1ckm1q3v8x2380z02d7mnmxkzcmkkb2v1i738b7hsnlgyb7v1s4z") (y #t)))

(define-public crate-rustsbi-0.2.1 (c (n "rustsbi") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "0a5h68fa5560dlpr2d3bwf287h4pydb3vsk5kmwsz4g8kgd8ssh5")))

(define-public crate-rustsbi-0.2.2 (c (n "rustsbi") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)))) (h "0pcb57hqrmslspmq610vwzwx9ay5nymygd04d0zssgmg7vqg37q3") (f (quote (("guest") ("default"))))))

(define-public crate-rustsbi-0.3.0-alpha.1 (c (n "rustsbi") (v "0.3.0-alpha.1") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.8") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.8") (d #t) (k 0)))) (h "00c2a7n1p43s92q49gwvb1irdbdcw9c6vflzacym34xq8i3nx6ij") (f (quote (("legacy" "embedded-hal" "nb") ("guest") ("default"))))))

(define-public crate-rustsbi-0.3.0-alpha.2 (c (n "rustsbi") (v "0.3.0-alpha.2") (d (list (d (n "riscv") (r "^0.8") (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.2") (d #t) (k 0)))) (h "0nbdif5dqddwn88qvs42zw2j92pr35pz5syxdivk67n225a695sn") (f (quote (("legacy") ("default"))))))

(define-public crate-rustsbi-0.3.0-alpha.4 (c (n "rustsbi") (v "0.3.0-alpha.4") (d (list (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.3") (d #t) (k 0)))) (h "1wqm6qc5r3s1s9hv92rqpb6h5zmnr4yqvw6y5mkf9ay1qrn83z8g") (f (quote (("legacy") ("default"))))))

(define-public crate-rustsbi-0.3.0-rc.1 (c (n "rustsbi") (v "0.3.0-rc.1") (d (list (d (n "riscv") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.4") (d #t) (k 0)))) (h "171glxck2yzh3pp2d38kwvjgfz90s667z6v522j4japz5vjb04xz") (f (quote (("legacy" "sbi-spec/legacy" "singleton") ("default" "machine")))) (y #t) (s 2) (e (quote (("singleton" "dep:riscv" "machine") ("machine" "dep:riscv"))))))

(define-public crate-rustsbi-0.3.0-rc.2 (c (n "rustsbi") (v "0.3.0-rc.2") (d (list (d (n "riscv") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.4") (d #t) (k 0)))) (h "0xc1baf6db0a3sk9rlyr765xj3qjccjxnrhxc5hffswm1y91k94r") (f (quote (("legacy" "sbi-spec/legacy" "singleton") ("default" "machine")))) (s 2) (e (quote (("singleton" "dep:riscv" "machine") ("machine" "dep:riscv"))))))

(define-public crate-rustsbi-0.3.0 (c (n "rustsbi") (v "0.3.0") (d (list (d (n "riscv") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.4") (d #t) (k 0)))) (h "00sla0478d7wyygkj6pbfrz67szzfbzq6xziv8f13zgba07xnwcn") (f (quote (("legacy" "sbi-spec/legacy" "singleton") ("default" "machine")))) (s 2) (e (quote (("singleton" "dep:riscv" "machine") ("machine" "dep:riscv"))))))

(define-public crate-rustsbi-0.3.1 (c (n "rustsbi") (v "0.3.1") (d (list (d (n "riscv") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.4") (d #t) (k 0)))) (h "0za8x4hc8yg1j68kc8v0majbih9p64wwi7wplafzjxhkmbx8valj") (f (quote (("legacy" "sbi-spec/legacy" "singleton") ("default" "machine")))) (s 2) (e (quote (("singleton" "dep:riscv" "machine") ("machine" "dep:riscv"))))))

(define-public crate-rustsbi-0.3.2-rc.2 (c (n "rustsbi") (v "0.3.2-rc.2") (d (list (d (n "riscv") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.5") (d #t) (k 0)))) (h "0927cs413ikj8gil68qylcp37vd4bydbiac1nyqq7wmh3qnhbx4p") (f (quote (("sbi_2_0") ("legacy" "sbi-spec/legacy" "singleton") ("default" "machine")))) (s 2) (e (quote (("singleton" "dep:riscv" "machine") ("machine" "dep:riscv"))))))

(define-public crate-rustsbi-0.3.2 (c (n "rustsbi") (v "0.3.2") (d (list (d (n "riscv") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.5") (d #t) (k 0)))) (h "0fzrbh0m8a4xx39jcmakh7csq48ixq610gcm3z4dvlgcbkgwcx4c") (f (quote (("sbi_2_0") ("legacy" "sbi-spec/legacy" "singleton") ("default" "machine")))) (s 2) (e (quote (("singleton" "dep:riscv" "machine") ("machine" "dep:riscv"))))))

(define-public crate-rustsbi-0.4.0-alpha.1 (c (n "rustsbi") (v "0.4.0-alpha.1") (d (list (d (n "riscv") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "rustsbi-macros") (r "^0.0.0") (d #t) (k 0)) (d (n "sbi-rt") (r "^0.0.3") (f (quote ("integer-impls"))) (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.7") (d #t) (k 0)))) (h "0kw2r8v7g6kbz3jaww1js82l3i0p9qswjiinwhhv4jjg0xm4nx50") (f (quote (("default")))) (s 2) (e (quote (("machine" "rustsbi-macros/machine" "dep:riscv") ("forward" "dep:sbi-rt"))))))

(define-public crate-rustsbi-0.4.0-alpha.2 (c (n "rustsbi") (v "0.4.0-alpha.2") (d (list (d (n "riscv") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "rustsbi-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "sbi-rt") (r "^0.0.3") (f (quote ("integer-impls"))) (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.7") (d #t) (k 0)))) (h "0yqnyh5ggb8kf1qm8f6zfhs3xa7xp7gyiz5k0g3wwbgn93hqhyp3") (f (quote (("default")))) (s 2) (e (quote (("machine" "rustsbi-macros/machine" "dep:riscv") ("forward" "dep:sbi-rt"))))))

(define-public crate-rustsbi-0.4.0-alpha.3 (c (n "rustsbi") (v "0.4.0-alpha.3") (d (list (d (n "riscv") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "rustsbi-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "sbi-rt") (r "^0.0.3") (f (quote ("integer-impls"))) (o #t) (d #t) (k 0)) (d (n "sbi-spec") (r "^0.0.7") (d #t) (k 0)))) (h "1hs0g9q6b0hjpjg99cn48sh5gg1h6fbbmhlcny39akba5z3caiyf") (f (quote (("default")))) (s 2) (e (quote (("machine" "rustsbi-macros/machine" "dep:riscv") ("forward" "dep:sbi-rt"))))))

