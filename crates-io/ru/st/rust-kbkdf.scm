(define-module (crates-io ru st rust-kbkdf) #:use-module (crates-io))

(define-public crate-rust-kbkdf-1.0.0 (c (n "rust-kbkdf") (v "1.0.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 2)))) (h "029vj49cr3kpxdh9q0hdzvsvsgppyrnlg1r5pw61qkcrxnf04cp9") (y #t)))

(define-public crate-rust-kbkdf-1.0.1 (c (n "rust-kbkdf") (v "1.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 2)))) (h "09aj7i1j47cw0jrfg1qzjypjhfa1g6cq9dd6zfmsbp0zpddh9fqx") (y #t)))

(define-public crate-rust-kbkdf-1.1.0 (c (n "rust-kbkdf") (v "1.1.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "typenum") (r "^1.15") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.6") (d #t) (k 0)))) (h "15l5ka7qqmrpv7cy06im0z1b89pwh7idd3kjj3zabdvpdl34n5gp")))

