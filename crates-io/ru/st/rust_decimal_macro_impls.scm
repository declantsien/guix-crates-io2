(define-module (crates-io ru st rust_decimal_macro_impls) #:use-module (crates-io))

(define-public crate-rust_decimal_macro_impls-0.11.0 (c (n "rust_decimal_macro_impls") (v "0.11.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.11.0") (d #t) (k 0)))) (h "1c84b7ix7zwfp68vgx4valcszsvnph602rwwjxgcqyaq69h0xqwv")))

(define-public crate-rust_decimal_macro_impls-0.11.1 (c (n "rust_decimal_macro_impls") (v "0.11.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rust_decimal") (r "^0.11.1") (d #t) (k 0)))) (h "04v7d8lv0jrdma54wdylnk0lmamh5gnqpa8zkf77ss3cpi5vifgc")))

(define-public crate-rust_decimal_macro_impls-1.0.0 (c (n "rust_decimal_macro_impls") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.0") (d #t) (k 0)))) (h "0gha8azhi4lwjagh3mfavimrqm5ps8591xzwdfqg3n1k63x1vnlr")))

(define-public crate-rust_decimal_macro_impls-1.0.1 (c (n "rust_decimal_macro_impls") (v "1.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.1") (d #t) (k 0)))) (h "0qh1p2bnqa5gp7rhgihcpdc5yf53vrab9m6da1p0gyvrzdq3zfqv")))

(define-public crate-rust_decimal_macro_impls-1.0.2 (c (n "rust_decimal_macro_impls") (v "1.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.2") (d #t) (k 0)))) (h "197ha7xwj7b16jv7n6c8lzvdpa7zywjq0z42v1wfjh2fd0dnxhyr")))

(define-public crate-rust_decimal_macro_impls-1.0.3 (c (n "rust_decimal_macro_impls") (v "1.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.0.3") (d #t) (k 0)))) (h "0iz4nc58cpf5ynpjgig3cb57jjl50lvvl5lh3wnyglp9gh4y4n28")))

(define-public crate-rust_decimal_macro_impls-1.1.0 (c (n "rust_decimal_macro_impls") (v "1.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.1.0") (d #t) (k 0)))) (h "09m31wyz9829mcyk1mb5rx5lq3h0p7xp8ypsmw6526jm54n7n89c")))

(define-public crate-rust_decimal_macro_impls-1.2.0 (c (n "rust_decimal_macro_impls") (v "1.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.2.0") (d #t) (k 0)))) (h "1kcmfhl5qznabigb29cc2ibn3184cprkrvvh5g93846yzxvampg4")))

(define-public crate-rust_decimal_macro_impls-1.2.1 (c (n "rust_decimal_macro_impls") (v "1.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.2.1") (d #t) (k 0)))) (h "1qkmw7dvr391p1cgc1q7b2r9cafc1f9pdx54h8rfaj13gyywz957")))

(define-public crate-rust_decimal_macro_impls-1.3.0 (c (n "rust_decimal_macro_impls") (v "1.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.3.0") (d #t) (k 0)))) (h "0cc96av90fimn43dsxxa4qpcywyr2yfanq4p3qv0v65yxsrv4vdq")))

(define-public crate-rust_decimal_macro_impls-1.4.0 (c (n "rust_decimal_macro_impls") (v "1.4.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.0") (d #t) (k 0)))) (h "0xilg371h0hza4sca4sk4f6lzqyn95vlnh0crarjar81iz1fw6x3")))

(define-public crate-rust_decimal_macro_impls-1.4.1 (c (n "rust_decimal_macro_impls") (v "1.4.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.1") (d #t) (k 0)))) (h "13s6sc8bfjn19ccypzv9yh2hf11s7j74m1j7gnhcvc3875ipysr9")))

(define-public crate-rust_decimal_macro_impls-1.5.0 (c (n "rust_decimal_macro_impls") (v "1.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.5.0") (d #t) (k 0)))) (h "1sx2rsgpclxxlxf3s8bh63kkf38yn879vnvqrzn0gwjv39wczgpm")))

(define-public crate-rust_decimal_macro_impls-1.6.0 (c (n "rust_decimal_macro_impls") (v "1.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.6.0") (d #t) (k 0)))) (h "08m47607xyk4r9hbmk5vxkip6nj6a68m22zkiawskp7xq10ay6sr")))

(define-public crate-rust_decimal_macro_impls-1.7.0 (c (n "rust_decimal_macro_impls") (v "1.7.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.7.0") (d #t) (k 0)))) (h "13aagz9rrfgrvjd3jdnj01dw8cqab4cwj43p87lcaglwjsin12xk")))

