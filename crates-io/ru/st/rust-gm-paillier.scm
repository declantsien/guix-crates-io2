(define-module (crates-io ru st rust-gm-paillier) #:use-module (crates-io))

(define-public crate-rust-gm-paillier-0.1.0 (c (n "rust-gm-paillier") (v "0.1.0") (d (list (d (n "gmp") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1m8harcacswakmh5hrv8aandj33hvp1j3m1yhlyp0r3f9ckxww1k")))

