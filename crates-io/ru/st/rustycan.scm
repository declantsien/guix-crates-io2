(define-module (crates-io ru st rustycan) #:use-module (crates-io))

(define-public crate-rustycan-0.1.0 (c (n "rustycan") (v "0.1.0") (h "0k13lqhwnksmxzqnnl331ymjbmfyqrrihhsslj1gssm1mrj2sr8l")))

(define-public crate-rustycan-0.1.1 (c (n "rustycan") (v "0.1.1") (h "04phyr0c7pagz91c7jk38gsyfq6v55bfrs0507s7hg2nm5fp3xwn")))

(define-public crate-rustycan-0.1.2 (c (n "rustycan") (v "0.1.2") (h "1d0h2ggqdsmi2p0ayajkplcvnhmhr0zg21c58yr2dfjq83rpcr0q")))

(define-public crate-rustycan-0.1.3 (c (n "rustycan") (v "0.1.3") (h "143281nj1vy3lq374j0qhwh7q0bfqblqa3xs7ijmyr0lkifqpgvb")))

