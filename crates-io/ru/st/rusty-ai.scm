(define-module (crates-io ru st rusty-ai) #:use-module (crates-io))

(define-public crate-rusty-ai-0.1.0 (c (n "rusty-ai") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "01q4wk9yp7ybvl7bjxn567c1vj76x01v839h1dscbyhs89ald9iv")))

(define-public crate-rusty-ai-0.1.1 (c (n "rusty-ai") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0x3mqjsmhikdnwfzl8j7zqbz7na8qlfbqiflxvm7kqzj18v4a3xz")))

(define-public crate-rusty-ai-0.1.2 (c (n "rusty-ai") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1h05s6hdwcda6s95x1p8qppzaxzdglzyfbbj69zq4iarxa645w4w")))

(define-public crate-rusty-ai-0.1.3 (c (n "rusty-ai") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0idqy592wq0wppd90bm8y4dfs11zj9b02lk9i7dhqz71851cc5r5")))

(define-public crate-rusty-ai-0.1.4 (c (n "rusty-ai") (v "0.1.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "15r5gjzs60xydxz5z4z250kdgrvansb7wkwdwzgk0vwy7x2cd0n0")))

(define-public crate-rusty-ai-0.1.5 (c (n "rusty-ai") (v "0.1.5") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "177ifxh9x6h8ndbcml4a3s70bhig8dyiy58r9qv1fbg7p8gg7r12")))

