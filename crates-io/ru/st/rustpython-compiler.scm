(define-module (crates-io ru st rustpython-compiler) #:use-module (crates-io))

(define-public crate-rustpython-compiler-0.1.0 (c (n "rustpython-compiler") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustpython-bytecode") (r "^0.1.0") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.1.0") (d #t) (k 0)))) (h "0qkwablbrfvv47szp6xhjjsz04bficzh5m32fyq8jifzi207jh2b")))

(define-public crate-rustpython-compiler-0.1.1 (c (n "rustpython-compiler") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustpython-bytecode") (r "^0.1.1") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.1.1") (d #t) (k 0)))) (h "0sxmcsvdqg9wk7xff5rx4cz8ygvdwcpyk0zvixna7igwmjw33cza")))

(define-public crate-rustpython-compiler-0.1.2 (c (n "rustpython-compiler") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustpython-bytecode") (r "^0.1.1") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.1.1") (d #t) (k 0)))) (h "0873zgsf5pijz32l62mrfaga8xznj4d24za0h54xxlyphvfk5fr4")))

(define-public crate-rustpython-compiler-0.3.0 (c (n "rustpython-compiler") (v "0.3.0") (d (list (d (n "rustpython-codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "rustpython-compiler-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.3.0") (d #t) (k 0)))) (h "1irlskaa3x35mv19w0cbc3iwjfm3lbnpdpik4dhdks0k8v9m11by")))

(define-public crate-rustpython-compiler-0.3.1 (c (n "rustpython-compiler") (v "0.3.1") (d (list (d (n "rustpython-codegen") (r "^0.3.1") (d #t) (k 0)) (d (n "rustpython-compiler-core") (r "^0.3.1") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.3.1") (d #t) (k 0)))) (h "18wzzddsw8wv28im5s5nm5bhn0zqgdpwl3y311l2k83qc4iljj5d")))

