(define-module (crates-io ru st rustpat) #:use-module (crates-io))

(define-public crate-rustpat-0.5.0-alpha.3 (c (n "rustpat") (v "0.5.0-alpha.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "170ckp5gni18ciyhlb97rnpqb5c2i58n2n38chajnd8ij31wc994")))

(define-public crate-rustpat-0.5.0 (c (n "rustpat") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0k822j2z4gwi47cifg8pwvk3w12ziasxghh57jdihklfqxwm3f6k")))

(define-public crate-rustpat-0.5.1 (c (n "rustpat") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05z0zyla90pfk9p2k6mw0xzh51j968g2zid3z5xw2qdal2vq1v6w")))

(define-public crate-rustpat-0.6.0 (c (n "rustpat") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03v81gdjvyqnhgclwyhlbasf7bhflz5v1mml8lwcym81f13y0ar3")))

(define-public crate-rustpat-0.6.1 (c (n "rustpat") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1l07209f17h5z0xwjal2g64kwh992sd3ahvbi6lhrnf9n5s83ibv")))

