(define-module (crates-io ru st rust-randomx) #:use-module (crates-io))

(define-public crate-rust-randomx-0.1.0 (c (n "rust-randomx") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0hqil9vfkx7b3x447griq09wq3ii9y5c0gld5502rsmfrhzsg1i3")))

(define-public crate-rust-randomx-0.2.0 (c (n "rust-randomx") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1y8f9h0vppk2qq4szxc2lqvivhjv9ci347wzm9m7ysqw64r4gaac")))

(define-public crate-rust-randomx-0.3.0 (c (n "rust-randomx") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "10xqyjqjc8yygxy0nsb0gd1gn81r05n6qnsd2r03m9m91v0bz86v")))

(define-public crate-rust-randomx-0.3.1 (c (n "rust-randomx") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0jg49v3f542p6km030afjg6hil2qhn3nj137hjlq3nzmbm33fwk2")))

(define-public crate-rust-randomx-0.4.0 (c (n "rust-randomx") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sgikwalg3bhzgsj4q76vayxvzl8r3w4r31bq18ys71imn1kad2s")))

(define-public crate-rust-randomx-0.4.1 (c (n "rust-randomx") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mgn1qp83vhs8npnwvqpfdybwbxnb092rvg8ihcgb80cqksgjjxs")))

(define-public crate-rust-randomx-0.5.0 (c (n "rust-randomx") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1z8hw46w5f2iwiq89libqmb6nqw1xhw6a5lfw9vamcf98pkaicn2")))

(define-public crate-rust-randomx-0.5.1 (c (n "rust-randomx") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "016jnz5gykwvy8ycv6kv909yfn6k2yx58x3ls99jyrcasjv3r572")))

(define-public crate-rust-randomx-0.5.2 (c (n "rust-randomx") (v "0.5.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1r5zsak764kv9q1y2g0lf6vrzh2qqnmpsm7kfs864wn1wl2l36v4")))

(define-public crate-rust-randomx-0.5.3 (c (n "rust-randomx") (v "0.5.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sca5pqbdr6p9b72s1i9zi0w5ki9pb4kadrv7amjz2ha941g4i8c")))

(define-public crate-rust-randomx-0.5.4 (c (n "rust-randomx") (v "0.5.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01bkhdyxy4501a2lh1c8yzzyxcyj51iggk9hq85b4g5ddd8n2831")))

(define-public crate-rust-randomx-0.5.5 (c (n "rust-randomx") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n73wcp5c9py2k5dzbpjwhwj5dcv7l0zvwwqi7qf6amkfg2jpqvb")))

(define-public crate-rust-randomx-0.5.6 (c (n "rust-randomx") (v "0.5.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1j0cyygzz8syi4w1qdsq96brshj4d6jv6yq5b0qk28vdc81441wq")))

(define-public crate-rust-randomx-0.5.7 (c (n "rust-randomx") (v "0.5.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dbyg5h973c2a18438pl9jq3dz97hbc7i8ksdq49qvycdyjw9346")))

(define-public crate-rust-randomx-0.6.0 (c (n "rust-randomx") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ndak1iy68nvpqqhbv0c2384r9irsmrr5d78csamkns5x3aw40lg")))

(define-public crate-rust-randomx-0.7.0 (c (n "rust-randomx") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17wpmj4zay2jj4ys4dpm4qifzy6vxj8n3y77ak1pyk23pqjrnkki")))

(define-public crate-rust-randomx-0.7.1 (c (n "rust-randomx") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "139ixl42ky5ly036a5p4jxazc4p7vmpnl4898m8741sa0g7xnyra")))

(define-public crate-rust-randomx-0.7.2 (c (n "rust-randomx") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ih34drc3j26yz4vbcz9v9yjpci77kfqs1qjs4hfjgjhs93rsnkb")))

(define-public crate-rust-randomx-0.7.3 (c (n "rust-randomx") (v "0.7.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1y1hy83jyybv4wgg138rlc0jghb88ngmxi6hr9fwmqnqax2s5xga")))

(define-public crate-rust-randomx-0.7.4 (c (n "rust-randomx") (v "0.7.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "14rhn13wy8jma6xjkpb7d4nmhy1xx36cwp5wmmcl3ylpm1ym9b3i")))

(define-public crate-rust-randomx-0.7.5 (c (n "rust-randomx") (v "0.7.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ig9r844jw2x0iwgkf4qz1pk6p54qi0cqfyjq49q10i7lghl5jvs")))

