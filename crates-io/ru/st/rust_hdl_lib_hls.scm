(define-module (crates-io ru st rust_hdl_lib_hls) #:use-module (crates-io))

(define-public crate-rust_hdl_lib_hls-0.44.0 (c (n "rust_hdl_lib_hls") (v "0.44.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust_hdl_lib_core") (r "^0.44.0") (d #t) (k 0)) (d (n "rust_hdl_lib_widgets") (r "^0.44.0") (d #t) (k 0)))) (h "1ky4mml5slbbxmi69x2skw0n1w33y1ax0j28zg7wmy80yi1xycii")))

