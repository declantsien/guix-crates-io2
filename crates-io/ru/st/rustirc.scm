(define-module (crates-io ru st rustirc) #:use-module (crates-io))

(define-public crate-rustirc-0.1.0 (c (n "rustirc") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-util" "net" "sync" "rt" "signal" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.0") (f (quote ("codec"))) (d #t) (k 0)))) (h "1kyj327xd2nh0zw8k12pxz6qy47mx08fkhc3lk45p3z408ylzbk0")))

