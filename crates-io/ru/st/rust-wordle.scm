(define-module (crates-io ru st rust-wordle) #:use-module (crates-io))

(define-public crate-rust-wordle-0.2.3 (c (n "rust-wordle") (v "0.2.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1kvgv52w3m75hyqgmzmj7a7x2nsyg82hiy937bzly6lz4w1qw6v9") (y #t)))

(define-public crate-rust-wordle-0.2.4 (c (n "rust-wordle") (v "0.2.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1npds5frmiz16bkzz9sa0ad8v8lkb4dvw9c7bqrfnrv3isp0mpig") (y #t)))

