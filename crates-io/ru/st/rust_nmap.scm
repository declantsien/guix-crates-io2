(define-module (crates-io ru st rust_nmap) #:use-module (crates-io))

(define-public crate-rust_nmap-0.1.0 (c (n "rust_nmap") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s6rpwzqyms5k1idrpq42fhkx630f2z6wzxbvfsaaq1rdrfxhq52")))

(define-public crate-rust_nmap-0.1.1 (c (n "rust_nmap") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qqzzc53h4ygi5gphml1iyxkacjkh4gcgw77lwfx44q5hy0dfs9c")))

(define-public crate-rust_nmap-0.1.2 (c (n "rust_nmap") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j6nb9k2igh56fsd2jjlkjyb8b5bcabj63d118n71xjcia1ci0pz")))

(define-public crate-rust_nmap-0.1.3 (c (n "rust_nmap") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gcpmdlxbxfzwc41hv035s79gwl3d3cb3l9kn09l67yn6rga7753")))

