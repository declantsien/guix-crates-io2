(define-module (crates-io ru st rusty_style) #:use-module (crates-io))

(define-public crate-rusty_style-0.1.0 (c (n "rusty_style") (v "0.1.0") (h "0sjf6ipfm7ksa16jpcbhvy2750v3nd93lg35c9vv696f42w9b90w")))

(define-public crate-rusty_style-0.1.1 (c (n "rusty_style") (v "0.1.1") (h "1vrhy3mha7j4ay7ds34ga2fg2as3rr45hb8h8iw4a07ahz0ranr1")))

(define-public crate-rusty_style-0.1.2 (c (n "rusty_style") (v "0.1.2") (h "0hm5xnz8zlwxlb4xagip62d4cn5d57xkg94lmfwblnm7s1vnbni9")))

