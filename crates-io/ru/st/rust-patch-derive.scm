(define-module (crates-io ru st rust-patch-derive) #:use-module (crates-io))

(define-public crate-rust-patch-derive-0.1.0 (c (n "rust-patch-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0dbzp4jd1w7aa9adzkrmd72f7wvk8n6s1678i8ygcz309jgd9y2q")))

(define-public crate-rust-patch-derive-0.1.1 (c (n "rust-patch-derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "021i9vk49cmqzfpkhjgg3icbjp8y9xnz6li0xgk4xd8fpkl3522j")))

(define-public crate-rust-patch-derive-0.1.2 (c (n "rust-patch-derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1bn6pm3vpp5yy2q7c6lfcwz2b23app1wanbzg64f9n3i7qvxm2v3")))

(define-public crate-rust-patch-derive-0.1.3 (c (n "rust-patch-derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "02qghb6jxc0yx0qddc81vckmyhqyqcaa328yrvgf7hx7l087d4mr")))

