(define-module (crates-io ru st rust-toolchain) #:use-module (crates-io))

(define-public crate-rust-toolchain-0.0.0 (c (n "rust-toolchain") (v "0.0.0") (h "16yfddzsymrixh5lpgs1srs0mkd3jsa4knga2b810hq4q0z7q7cb")))

(define-public crate-rust-toolchain-0.17.0 (c (n "rust-toolchain") (v "0.17.0") (h "1lsiaayjcm3fnkkiir0cyiixsd779mgg62gr5fk534wmryik30ks") (y #t)))

(define-public crate-rust-toolchain-0.18.0 (c (n "rust-toolchain") (v "0.18.0") (h "1pcb41z2248nxzpdhy4gghh09qnn5nxsliw8cpc436ajsngkc8g2") (y #t)))

(define-public crate-rust-toolchain-0.20.0 (c (n "rust-toolchain") (v "0.20.0") (h "129x0p0wd3ghgv10bkj07254s07ac1hz79riv7df9cvk3lhhjr85") (y #t)))

(define-public crate-rust-toolchain-0.21.0 (c (n "rust-toolchain") (v "0.21.0") (h "0xazgk23sccaiq595waxvsjbrkkmp2xxi6mnwkjvjx0mmwmb64wj")))

(define-public crate-rust-toolchain-0.21.1 (c (n "rust-toolchain") (v "0.21.1") (h "0sc9mwm523yn14z4z3slz05385izbz1hsm04q8r15pyr1xnxlcap")))

