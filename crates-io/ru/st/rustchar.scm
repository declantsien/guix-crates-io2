(define-module (crates-io ru st rustchar) #:use-module (crates-io))

(define-public crate-rustchar-0.1.0 (c (n "rustchar") (v "0.1.0") (h "1xiq4xjv45sxs8lmj64s6647fbnw6alni5pzy952r2ip2lvrqmsn")))

(define-public crate-rustchar-0.1.1 (c (n "rustchar") (v "0.1.1") (h "1lvxzszr218md9prr0k13jsz79rrqaq3mb68mhfcn4jillzg882j")))

