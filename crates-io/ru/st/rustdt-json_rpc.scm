(define-module (crates-io ru st rustdt-json_rpc) #:use-module (crates-io))

(define-public crate-rustdt-json_rpc-0.2.0 (c (n "rustdt-json_rpc") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustdt_util") (r "^0.2.3") (d #t) (k 0)) (d (n "rustdt_util") (r "^0.2.3") (f (quote ("test_utils"))) (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "18lm07misr22yvsyg60aw80j9b2wyfgaz7gs6sixbxjld7b4070k")))

(define-public crate-rustdt-json_rpc-0.3.0 (c (n "rustdt-json_rpc") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustdt_util") (r "^0.2.3") (d #t) (k 0)) (d (n "rustdt_util") (r "^0.2.3") (f (quote ("test_utils"))) (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1g4c011lpzryqdf44cxjrcbdsq30dfnf2b22r9iv1sk8w1xx4cac")))

