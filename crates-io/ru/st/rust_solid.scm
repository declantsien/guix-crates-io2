(define-module (crates-io ru st rust_solid) #:use-module (crates-io))

(define-public crate-rust_solid-0.1.0 (c (n "rust_solid") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.6.0") (d #t) (k 0)) (d (n "actix-session") (r "^0.7.1") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)))) (h "044ay79xch980bzw4y9931vcaj2ha9j0mhdm4hsv5jr5g2xbl4xd") (r "1.56")))

