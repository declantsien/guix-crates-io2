(define-module (crates-io ru st rusty_password) #:use-module (crates-io))

(define-public crate-rusty_password-0.1.2 (c (n "rusty_password") (v "0.1.2") (d (list (d (n "bcrypt") (r "^0.12.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0rqwz6l2rs4b03w5nnljsrc1k6lb4w404l542cimq96va6ybhm50")))

