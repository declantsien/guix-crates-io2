(define-module (crates-io ru st rustbase-scram) #:use-module (crates-io))

(define-public crate-rustbase-scram-0.6.1 (c (n "rustbase-scram") (v "0.6.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "1hivla0fqmwpky7hzm2jv1bchslz0hlqc9kgajg1j91arygl95wh")))

