(define-module (crates-io ru st rust-fake-database) #:use-module (crates-io))

(define-public crate-rust-fake-database-0.1.0 (c (n "rust-fake-database") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (f (quote ("derive" "chrono"))) (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (d #t) (k 0)))) (h "17dijy1j8pm9m0cglf8qz6cjdrl7f2cwfynzm80q4dqpirz1snj0") (y #t)))

