(define-module (crates-io ru st rusty_pool) #:use-module (crates-io))

(define-public crate-rusty_pool-0.1.0 (c (n "rusty_pool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "1r3zy6j5b9mkra28cw2bribs1a4h09rb8mrmgzwfxigga53fabaj")))

(define-public crate-rusty_pool-0.2.0 (c (n "rusty_pool") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "00m3flnh775qpdb8iblnhf5bpsmapyja9di9vs5knncsr9l3n4zd")))

(define-public crate-rusty_pool-0.3.0 (c (n "rusty_pool") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "1m4qk0a0d1px2b1bmzr0gsmjzcy3irz8rg787bfmvsryry8fsgvw")))

(define-public crate-rusty_pool-0.3.1 (c (n "rusty_pool") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "0m19l6mc85f88l8ll9rwfkb29a542alam9kw96fhn9jpky3d4wrp")))

(define-public crate-rusty_pool-0.3.2 (c (n "rusty_pool") (v "0.3.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)))) (h "0v1znlwcldxvnrv0cgv9dg9wfxqarrkm1r8f66bfrnx84icxaf2p")))

(define-public crate-rusty_pool-0.4.0 (c (n "rusty_pool") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0787kh91h27fabc2z3ksvjq6jcpky58aiawvwj8ihw19b2agrmsw") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-rusty_pool-0.4.1 (c (n "rusty_pool") (v "0.4.1") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0dl0jybkd2jvfk365153vwr3n1fqbvwzlqaagcwca6sp0vninas9") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-rusty_pool-0.4.2 (c (n "rusty_pool") (v "0.4.2") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1qb6ys9yp44gailqzbbm2gava227vhsq9l75m8pkyd863hzpkn11") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-rusty_pool-0.4.3 (c (n "rusty_pool") (v "0.4.3") (d (list (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1kp1a5gwpc4p0s0xp1239yx1p2i9rshkga31rqjpz945nhcr392h") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-rusty_pool-0.5.0 (c (n "rusty_pool") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.12") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0f5hylk64m3c157c6cwvb3b78dx88pb1rf7gbcxyznh15j53hiq0") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-rusty_pool-0.5.1 (c (n "rusty_pool") (v "0.5.1") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.12") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0fqkf8miahi58dmhlr4qdwymv8d22hdllg7sha442f22sphiqbgi") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-rusty_pool-0.6.0 (c (n "rusty_pool") (v "0.6.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.12") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.12") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1pm2n5s098aj9wzzxmiwvm87j8khzww4s821rk5ihl1mfrw1p81k") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-rusty_pool-0.7.0 (c (n "rusty_pool") (v "0.7.0") (d (list (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.21") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.21") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1zvn93786m80lpfawyl7rzr8d8y77y4bh17a2yddhrny43dnrlsf") (f (quote (("default" "async") ("async" "futures"))))))

