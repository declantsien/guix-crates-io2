(define-module (crates-io ru st rustaceans) #:use-module (crates-io))

(define-public crate-rustaceans-0.1.0 (c (n "rustaceans") (v "0.1.0") (h "1imklhj2v3wqk924jrs1yzxrlrayh1nyi7vrhgryjar31l6m6aqh")))

(define-public crate-rustaceans-0.2.0 (c (n "rustaceans") (v "0.2.0") (h "0zh0lylvbv0w6j2r85fk0fm0jspdnglr1mdxiiwh2n7hgwicmjpa")))

