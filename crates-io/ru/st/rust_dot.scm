(define-module (crates-io ru st rust_dot) #:use-module (crates-io))

(define-public crate-rust_dot-0.1.0 (c (n "rust_dot") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)))) (h "1xs5hm8n7mn9bv5bfmn4iz2pjm8a02lhxiha4d91bajgal95nfvm")))

(define-public crate-rust_dot-0.2.0 (c (n "rust_dot") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)))) (h "11vizknylyi6h7f9h1qqd0hs0hm78bgdis0z86ch0gb4dhaf0hc9")))

(define-public crate-rust_dot-0.3.0 (c (n "rust_dot") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)))) (h "0cpbrn5zqlb5d961llxz9aflkf1cxxpxggwxf3n3srf5abhb9383")))

(define-public crate-rust_dot-0.4.0 (c (n "rust_dot") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)))) (h "07ls1l3h3rriy0vb8vv83j9inx11wz9ar9gqfrpk54zw7ygr3c4y")))

(define-public crate-rust_dot-0.5.0 (c (n "rust_dot") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)))) (h "1m7n06zsri2df2pipz40v7v09g35c41nz8kmcy37m2hlpgq7mlhl") (y #t) (r "1.56")))

(define-public crate-rust_dot-0.5.1 (c (n "rust_dot") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)))) (h "0d5dw0ynlixxlsvwgzdbi4mvqkgx4rpx79ns1jdxq5j6sf1q2csi") (r "1.56")))

(define-public crate-rust_dot-0.6.0 (c (n "rust_dot") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)))) (h "12brawm6gwn8j47pcf9w1b75z050kyr6mvrh36mbskrlvzk39z9z") (r "1.56")))

