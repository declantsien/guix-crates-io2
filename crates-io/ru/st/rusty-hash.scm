(define-module (crates-io ru st rusty-hash) #:use-module (crates-io))

(define-public crate-rusty-hash-1.0.0 (c (n "rusty-hash") (v "1.0.0") (d (list (d (n "memmap") (r "^0.2.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "1v6hv51izj6knnfql9pl56ms3b2f2agkh3r3q8dpblikkdlv2prn")))

(define-public crate-rusty-hash-1.1.0 (c (n "rusty-hash") (v "1.1.0") (d (list (d (n "memmap") (r "^0.2.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "0iqxbgc3mxd7r95kyz580628dqzbsa7340wnpfsw3rgr5ixvwvy8")))

