(define-module (crates-io ru st rustcat) #:use-module (crates-io))

(define-public crate-rustcat-0.0.2 (c (n "rustcat") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0ffi7hc1zy04z8mdjb538p4bfyzic5vbyq9vmpcnd4320slj73im")))

(define-public crate-rustcat-0.0.3 (c (n "rustcat") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1vj3qck8jcywg975wwcichppfzb7dj5gq1hnj03gz55s5wxyynzd")))

(define-public crate-rustcat-0.0.4 (c (n "rustcat") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1fmqydsb9xds3bjyf4ycpm98l2adqj02g8f8m8y4rimccvdqpz8k")))

(define-public crate-rustcat-0.0.5 (c (n "rustcat") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "06p4fsy66rn7y3phw39mf035w3mdhi8flhak0mdsqccwzpyvw0gq")))

(define-public crate-rustcat-1.0.0 (c (n "rustcat") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0bkb1ggzlbq1ak6jy0r6yrhx7cjymplbxk8mkn3n6l8661z63zl5")))

(define-public crate-rustcat-1.0.1 (c (n "rustcat") (v "1.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0b73bvmxjsp2wnwjv1bpcnrv87kgk1dlz5x56hr1k0yljh7q6858")))

(define-public crate-rustcat-1.1.0 (c (n "rustcat") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0ly99c58lyj3wjsg4aaxjr2319hv9jp8d2faz4n3hlyz8qrfr8dd")))

(define-public crate-rustcat-1.1.1 (c (n "rustcat") (v "1.1.1") (d (list (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0vv50b3v67d0ifclfdi9zmz41gxqp8rpqybzav73mli7irb4pxkq")))

(define-public crate-rustcat-1.1.2 (c (n "rustcat") (v "1.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0s1hqyz4yxzvy8hznyl2saaz2wkjd2izlp4xvhwwspvdfpqmwxbd")))

(define-public crate-rustcat-1.2.0 (c (n "rustcat") (v "1.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "07y7nybd83vx7r8sxz67hj1v1m85na84aym6as8drfv007hm63aq")))

(define-public crate-rustcat-1.3.0 (c (n "rustcat") (v "1.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "0bj356b7chbzpq4k5w3nvibhklmdmghyi1125mymj7hxf6j621dg")))

(define-public crate-rustcat-3.0.0 (c (n "rustcat") (v "3.0.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "0x2l2qkm3h3yi62zdcc1msvf4yzdfkbhjkam8810vqi0izqzb4vp")))

