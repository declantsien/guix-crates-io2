(define-module (crates-io ru st rust-forth-tokenizer) #:use-module (crates-io))

(define-public crate-rust-forth-tokenizer-0.0.1 (c (n "rust-forth-tokenizer") (v "0.0.1") (h "0pgi11k6n6da5rxj2v8ls09vrck47slfjxfp7azajasxk4q75k9r")))

(define-public crate-rust-forth-tokenizer-0.0.2 (c (n "rust-forth-tokenizer") (v "0.0.2") (h "1lksvfx9s3q55xb4wsa34ckcazmis5irlgb6d0fmlac7fs4764wn")))

(define-public crate-rust-forth-tokenizer-0.0.3 (c (n "rust-forth-tokenizer") (v "0.0.3") (h "0lkwsx46c3p0qsnfaixjnh3l31hi0j22nkm45i3ibfx1bcr1rmgr")))

(define-public crate-rust-forth-tokenizer-0.0.5 (c (n "rust-forth-tokenizer") (v "0.0.5") (h "0976mfpzy5yy71d8k7dww1w4vrqhvh1r2x9sszmb3a6jw5fxq4va")))

(define-public crate-rust-forth-tokenizer-0.0.6 (c (n "rust-forth-tokenizer") (v "0.0.6") (h "05fqdzz14hl1p4gxq8rfd798kzpdgv1gwllsl4d3f0k9l7cwjifb")))

(define-public crate-rust-forth-tokenizer-0.1.0 (c (n "rust-forth-tokenizer") (v "0.1.0") (h "17cqrzb4d2067b0if6c0w8z10nphv0kpp29njqymb89k55qa1y70")))

(define-public crate-rust-forth-tokenizer-0.1.1 (c (n "rust-forth-tokenizer") (v "0.1.1") (h "1dfr63m8vl5bkys2xvsxhraiwg047g2kcanqw40sskhw7qyckpvl")))

(define-public crate-rust-forth-tokenizer-0.1.2 (c (n "rust-forth-tokenizer") (v "0.1.2") (h "0kr76bmj03cznljl46ahqzra5lyv0nin2p2a8in2x3i7xjd0a1r2")))

(define-public crate-rust-forth-tokenizer-0.2.0 (c (n "rust-forth-tokenizer") (v "0.2.0") (h "0pk6s7ba8zkbin352ppr9d7y3m557wg79rqin8n2mxqcqpihaaj5")))

