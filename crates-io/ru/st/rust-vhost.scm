(define-module (crates-io ru st rust-vhost) #:use-module (crates-io))

(define-public crate-rust-vhost-0.1.0 (c (n "rust-vhost") (v "0.1.0") (h "00s7q2j2fs20srq7q24yi2diprzb62m5yx19i2fr91pd5h459kbw")))

(define-public crate-rust-vhost-0.1.1 (c (n "rust-vhost") (v "0.1.1") (h "14v5fkva7dwcv31iwpds33z5r4wv52c66cx78yrpcdw8vqfgw1l0")))

(define-public crate-rust-vhost-0.1.2 (c (n "rust-vhost") (v "0.1.2") (h "0hm2pqpq67cxkikq194yxj2r888nqdl87q1bkbxszyrfvp7dr8di")))

(define-public crate-rust-vhost-0.1.3 (c (n "rust-vhost") (v "0.1.3") (h "04692fmgsz8dwmv23fabxas36r60i2wbvlsabaxk2n35s3iwjsfn")))

