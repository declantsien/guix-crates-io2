(define-module (crates-io ru st rust-hdl-bsp-alchitry-cu) #:use-module (crates-io))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.1.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.1.0") (d (list (d (n "rust-hdl-core") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-test-core") (r "^0.1") (d #t) (k 2)) (d (n "rust-hdl-toolchain-icestorm") (r "^0.1") (d #t) (k 0)) (d (n "rust-hdl-widgets") (r "^0.1") (d #t) (k 2)) (d (n "rust-hdl-yosys-synth") (r "^0.1") (d #t) (k 0)))) (h "1lbp9vmg1a0qzd3yg16ym2wzy5cm7pzgkapmcwpk9ydgibmqp1zx") (y #t)))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.2.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.2.0") (d (list (d (n "rust-hdl") (r "^0.31.1") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.1.0") (d #t) (k 0)))) (h "0vxg39xcb2bm0bq6m5nbn9jdgxpdvzpz3kd67k2yb5hh7bfd3jrz")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.4.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.4.0") (d (list (d (n "rust-hdl") (r "^0.33.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.3.0") (d #t) (k 0)))) (h "0q6ivqsdhjj38h6wnqn6pycbc8gxb62imv8vsdssz6b8fp3lqs6r")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.5.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.5.0") (d (list (d (n "rust-hdl") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.4.0") (d #t) (k 0)))) (h "18njx1r8g4mrfqljzh04c7xwmk5v7l0jx37l1zj47a1gzqzjhigb")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.6.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.6.0") (d (list (d (n "rust-hdl") (r "^0.35.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.5.0") (d #t) (k 0)))) (h "18pkkr1nny4r42xyn1gcvjwsy0b2hg8dhlg2l42c4gybjipqrwsb")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.6.2 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.6.2") (d (list (d (n "rust-hdl") (r "^0.35.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.5.0") (d #t) (k 0)))) (h "16xa4nrfb385xsjdizpdqkdfri8n16lmjf6r6w1zpdwjcnbqpjj9")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.7.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.7.0") (d (list (d (n "rust-hdl") (r "^0.36.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.6.0") (d #t) (k 0)))) (h "1a7zdrsg7bl9dab3i1mq59bi4s342z4m3dil4hm8dm7jry5prhmg")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.8.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.8.0") (d (list (d (n "rust-hdl") (r "^0.37.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.7.0") (d #t) (k 0)))) (h "002yqrns3sn5dbn2xr1k9m8ipq9pg6gkgx0crchcia6i2187w5q5")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.8.1 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.8.1") (d (list (d (n "rust-hdl") (r "^0.37.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.7.0") (d #t) (k 0)))) (h "1h2vwrlcrs9lp4khgmgfbirdwd6fqda8pk2dlk2y8lyyilaqrsdb")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.9.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.9.0") (d (list (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.8.0") (d #t) (k 0)))) (h "0hvwrrl4fmr7n2mk7mbd0p98wp8xaqhki0jg6qbn19bhyvzwf75q")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.9.2 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.9.2") (d (list (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.8.0") (d #t) (k 0)))) (h "0pszjlvf6xb2fzir6xpcj42lzpx500fzkk5anxg2qj06l7yzl44j")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.9.4 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.9.4") (d (list (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.8.0") (d #t) (k 0)))) (h "0cvwns0ckn0n22138ggs99ivygq4nbd17nd3ad97iil45hv948cs")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.9.5 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.9.5") (d (list (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.8.0") (d #t) (k 0)))) (h "1956wzzyymxmhgjk64mbs2fizmvyjss9nz8dr9vvxnwhnvz20x37")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.9.6 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.9.6") (d (list (d (n "rust-hdl") (r "^0.38.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.8.0") (d #t) (k 0)))) (h "1yn2dsyhhjz3ickzhzqcipgfz9mx0c0l6j2m32p77vxmw9ms5qfx")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.12.1 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.12.1") (d (list (d (n "rust-hdl") (r "^0.41.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.11.0") (d #t) (k 0)))) (h "0gfxi6941gps8qi4nhz99kf93069g5ld1f4z6ddvhfwdwyal64pp")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.42.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.42.0") (d (list (d (n "rust-hdl") (r "^0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0") (d #t) (k 0)))) (h "0qhwhnbjalii99p0mvnsvcpxy784w3ichy292x1lm5hg9wqcz8q3")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.43.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.43.0") (d (list (d (n "rust-hdl") (r "^0.43.0") (d #t) (k 0)) (d (n "rust-hdl-fpga-support") (r "^0.43.0") (d #t) (k 0)))) (h "0yjsxd23p3icgs6r3642pszd3gfk6s14g2x187fqljvd2ccfcxcz")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.44.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.44.0") (d (list (d (n "rust-hdl") (r "^0.44.0") (f (quote ("fpga"))) (d #t) (k 0)))) (h "093kqiyaww58hqh4wmg48ardaq28vhql4hnl7ivlynh7d3wqp0ir")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.44.2 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.44.2") (d (list (d (n "rust-hdl") (r "^0.44.2") (f (quote ("fpga"))) (d #t) (k 0)))) (h "04z8i3kjr0pwpldj17nxsvf43znhms678mnpinn9lfw4pqgg0q7p")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.45.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.45.0") (d (list (d (n "rust-hdl") (r "^0.45.0") (f (quote ("fpga"))) (d #t) (k 0)))) (h "1zial990iq8wyhl6lza8nb2zzm6yr8qfx8jaqx03j7pji88ny0ph")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.45.1 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.45.1") (d (list (d (n "rust-hdl") (r "^0.45.1") (f (quote ("fpga"))) (d #t) (k 0)))) (h "14zbx3sc3vh8s8f9ffaa41p23nn6vqjgnj4k4fprsh0l23xyk82s")))

(define-public crate-rust-hdl-bsp-alchitry-cu-0.46.0 (c (n "rust-hdl-bsp-alchitry-cu") (v "0.46.0") (d (list (d (n "rust-hdl") (r "^0.46.0") (f (quote ("fpga"))) (d #t) (k 0)))) (h "11aagm9n9m25nf2v2mr6r1nz8i445wpy34gbcqqnhpcwbyb4rgwq")))

