(define-module (crates-io ru st rust-kepler-solver) #:use-module (crates-io))

(define-public crate-rust-kepler-solver-0.1.0 (c (n "rust-kepler-solver") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "07ka7bv4rsn9c5b6kc9qifih1dpykc9w9zkjhz51az8x6ja5ccqm")))

