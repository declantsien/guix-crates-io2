(define-module (crates-io ru st rustypath) #:use-module (crates-io))

(define-public crate-rustypath-0.1.0 (c (n "rustypath") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1clhvhm161kq9bdg1wdnv92ii82ai79m5immh2qccdbqw5y8aziv")))

(define-public crate-rustypath-0.1.1 (c (n "rustypath") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "19k5y8kcsmpgvhzrfbprx326cwkvck967qnvy5213b4dx546dw6v")))

(define-public crate-rustypath-0.2.0 (c (n "rustypath") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1c6rn4kfs0afhpshhrszakpmfkgxifigxhfr7p1pf83amvk5svgi")))

