(define-module (crates-io ru st rust-demo-rgrep) #:use-module (crates-io))

(define-public crate-rust-demo-rgrep-0.1.0 (c (n "rust-demo-rgrep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "16n1krpvc7z52y2m1rssp36zgq5yvw44pyc3nk5zzjfyf18akgqw")))

