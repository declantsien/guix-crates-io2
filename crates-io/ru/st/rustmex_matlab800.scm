(define-module (crates-io ru st rustmex_matlab800) #:use-module (crates-io))

(define-public crate-rustmex_matlab800-0.1.0 (c (n "rustmex_matlab800") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.1") (d #t) (k 0)))) (h "1v9im47pda0s1l1vd45m3c7vr81qis2z7n7z5ms85b91d74fccnc") (l "mex800")))

(define-public crate-rustmex_matlab800-0.1.2 (c (n "rustmex_matlab800") (v "0.1.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.2") (d #t) (k 0)))) (h "1vwzisnbhzwrlinszjz3mw25xwqfadbvb6ghqzy3z7zb83gvnpn7") (l "mex800")))

