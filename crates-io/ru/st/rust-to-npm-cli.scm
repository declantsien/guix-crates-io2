(define-module (crates-io ru st rust-to-npm-cli) #:use-module (crates-io))

(define-public crate-rust-to-npm-cli-0.4.10 (c (n "rust-to-npm-cli") (v "0.4.10") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust-to-npm") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "06z86jg3q156gx70p75md3vn7sj5miq2zzld8yafrzp4z3qj320r")))

(define-public crate-rust-to-npm-cli-0.4.11 (c (n "rust-to-npm-cli") (v "0.4.11") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust-to-npm") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1v8bhmz57gm2piawzzhhxxhx7785l36a3yfd5468qscvsm0lr4dj")))

(define-public crate-rust-to-npm-cli-0.4.12 (c (n "rust-to-npm-cli") (v "0.4.12") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust-to-npm") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0ax03x8pz4p25xk9yixhzfb78xn0hm1b6pqawfnwpsz02zkxk8mw")))

(define-public crate-rust-to-npm-cli-0.4.13 (c (n "rust-to-npm-cli") (v "0.4.13") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rust-to-npm") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0frlic9xy905cydca1wfqz5hd5agy2xk059lwnzihkghrwmqnrks")))

