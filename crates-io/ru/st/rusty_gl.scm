(define-module (crates-io ru st rusty_gl) #:use-module (crates-io))

(define-public crate-rusty_gl-0.1.0 (c (n "rusty_gl") (v "0.1.0") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "0fnb536i3zjlab8djnzsq8s3l2s8l1hv2xw8gdpf1i8yfrlaqllq")))

(define-public crate-rusty_gl-0.1.1 (c (n "rusty_gl") (v "0.1.1") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "14vh1min8v5gh1zy1gafhyiciazrmq7x4k2as81zqp8fim45zwr1")))

(define-public crate-rusty_gl-0.1.2 (c (n "rusty_gl") (v "0.1.2") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "1d47z21azbhcli6d54spd8cjyfn4kzy3nyl42n2ynql4d6s6fag9")))

(define-public crate-rusty_gl-0.1.3 (c (n "rusty_gl") (v "0.1.3") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "1pg6d0dy622a5vklbjbplyp1dmw7bs500r2qv9avfbnbhhx6bpp2")))

(define-public crate-rusty_gl-0.1.4 (c (n "rusty_gl") (v "0.1.4") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "1qc4di5w9xnp17i5l5jamzfdqx8zd4lyv8fyr3khyx5rdrnwl5ah")))

(define-public crate-rusty_gl-0.1.5 (c (n "rusty_gl") (v "0.1.5") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "1b93msfxnmxy4im5dxmkvj18mdmdj7rfs8vac3jr1l91j7r23glf")))

(define-public crate-rusty_gl-0.1.6 (c (n "rusty_gl") (v "0.1.6") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "174j3cr5ksvvbzj2997n1f7khw74da2kmm8sfjvbil2rhz1g3vpw")))

(define-public crate-rusty_gl-0.1.7 (c (n "rusty_gl") (v "0.1.7") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "08rflh25ah4bn92iggi37viv8g73wv0rnbbjwqbyb2m8gcnn64mp")))

