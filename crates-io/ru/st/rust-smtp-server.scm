(define-module (crates-io ru st rust-smtp-server) #:use-module (crates-io))

(define-public crate-rust-smtp-server-0.1.0 (c (n "rust-smtp-server") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.23.4") (d #t) (k 0)))) (h "1znksf96ym9h5pj81921fa1nl80769hppkvsh7y16xmcazw52g7n")))

(define-public crate-rust-smtp-server-0.1.1 (c (n "rust-smtp-server") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.23.4") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1") (d #t) (k 2)))) (h "0sajx98q3rkpf96gx8nbh6wy6kp26l7n73q8a8ca8107r3wvdbsm")))

