(define-module (crates-io ru st rust-pulsectl) #:use-module (crates-io))

(define-public crate-rust-pulsectl-0.2.0 (c (n "rust-pulsectl") (v "0.2.0") (d (list (d (n "libpulse-binding") (r "^2.14.0") (d #t) (k 0)))) (h "0amaiyjyk52kzhibyyd17ns3yv85r44xlnlmc82fxmy2c55b6smi")))

(define-public crate-rust-pulsectl-0.2.5 (c (n "rust-pulsectl") (v "0.2.5") (d (list (d (n "libpulse-binding") (r "^2.14.0") (d #t) (k 0)))) (h "0bbb7qjm3r7jiabvacw5cf16axhls23j0x9jq1vkdf0r3cw7wxmm")))

(define-public crate-rust-pulsectl-0.2.6 (c (n "rust-pulsectl") (v "0.2.6") (d (list (d (n "libpulse-binding") (r "^2.14.0") (d #t) (k 0)))) (h "1s4qygbnn83j333bk38xvdz2vpj7baykncblq1lfz617y353y4zh")))

