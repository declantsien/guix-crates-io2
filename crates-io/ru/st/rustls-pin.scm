(define-module (crates-io ru st rustls-pin) #:use-module (crates-io))

(define-public crate-rustls-pin-0.1.0 (c (n "rustls-pin") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "rustls") (r "^0.19.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)))) (h "1hw8w8p0g1j4zrxkn0i5wvnmd2i07is7d9waysa33dqib8vvqmdn")))

(define-public crate-rustls-pin-0.1.1 (c (n "rustls-pin") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "rustls") (r "^0.19.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)))) (h "1qml8phl22a0c9qmma13b1gfd5q27m5jf7129l9dppjm4vj7clmb")))

(define-public crate-rustls-pin-0.1.2 (c (n "rustls-pin") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "rustls") (r "^0.19.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "webpki") (r "^0.21.4") (d #t) (k 0)))) (h "1ng0jgqxd76c4l33avarpyqkswv3ckqs1q8lj1lxcm2lk45dxx0g")))

