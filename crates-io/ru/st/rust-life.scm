(define-module (crates-io ru st rust-life) #:use-module (crates-io))

(define-public crate-rust-life-0.0.3 (c (n "rust-life") (v "0.0.3") (d (list (d (n "image") (r "^0.6.0") (d #t) (k 0)) (d (n "num_cpus") (r "^0.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^0.1") (d #t) (k 0)))) (h "0pv2wr7bxs6sifgzbv46zpcmxcvl1sfx59paa0pfnbxbibp4hfmb") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.1.0 (c (n "rust-life") (v "0.1.0") (d (list (d (n "image") (r "^0.9.0") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "piston_window") (r "^0.42.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "02gnalz604sp5p2zhc6q61rjq2468r2fwc1srq0j9j8jfnpp3f2n") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.1.2 (c (n "rust-life") (v "0.1.2") (d (list (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "piston_window") (r "^0.47.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0qamdp3xmajgc9ql9h49n0jbhc8dfrynvsih5gh2dldz4ybpd1ci") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.1.3 (c (n "rust-life") (v "0.1.3") (d (list (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "piston_window") (r "^0.50.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.3") (d #t) (k 0)))) (h "1389yarqjvxivmrk8p4012r3682zsza1g3imnb5rs8phg6zj36vq") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.0 (c (n "rust-life") (v "0.2.0") (d (list (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "106hs708js09lpa13sampichhsn0ama8hazfxrb38nhyhhr7mdv0") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.1 (c (n "rust-life") (v "0.2.1") (d (list (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "08i77riac4iln1yzhk149z632h9zihpqxws9lwxr7qikjax8afic") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.2 (c (n "rust-life") (v "0.2.2") (d (list (d (n "image") (r "^0.12.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "19czwzp7ydaxvhpwk90ffa6bzdz0648z1vj8d35ln835cp27n9ym") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.3 (c (n "rust-life") (v "0.2.3") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.65.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.7.0") (d #t) (k 0)))) (h "0c4yqnzjpl89gr570pg9klwqa7528d734zlzl9b7z4bl1rk64a22") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.4 (c (n "rust-life") (v "0.2.4") (d (list (d (n "image") (r "^0.14.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.68.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.8.0") (d #t) (k 0)))) (h "0ighmnh3b6h4g7hf01vg0jw48w0f8ay20pgn2ija99s4h9ygw8fp") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.5 (c (n "rust-life") (v "0.2.5") (d (list (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.70.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.8.0") (d #t) (k 0)))) (h "0jnlzcn91l7csgvf035h2n3sidr84qavpvirs4nz43vgj1s54bim") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.6 (c (n "rust-life") (v "0.2.6") (d (list (d (n "image") (r "^0.17.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1qx8ngncs79k40hf6794m7rap82wd6am7ags99psnaz907hmivn3") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.7 (c (n "rust-life") (v "0.2.7") (d (list (d (n "image") (r "^0.17.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.73.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "17azphbnlfr6kdyxnisizq2isvvyc1db4gdgpzhvqiawxzmisj0w") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.9 (c (n "rust-life") (v "0.2.9") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.74.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "16w3j758nzp73wr7yj12n3jyffq53jpslfjfbdf266x22lyhcw5r") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.10 (c (n "rust-life") (v "0.2.10") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.76.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)))) (h "1azxgvnjhpl190mf47myjfmqqs21c1nhmw8wzr820kgnfpsapddi") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.11 (c (n "rust-life") (v "0.2.11") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.78.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)))) (h "049nlrcdv1jhj0y9c75xq1szwaw08rlsq7cx8d947gw95g0mdaa0") (f (quote (("unstable"))))))

(define-public crate-rust-life-0.2.12 (c (n "rust-life") (v "0.2.12") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.87.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "13xvga4ijwqqdlg16f39dy7p2vf35x21fypjjb42z54ks1dx9kgx") (f (quote (("unstable"))))))

