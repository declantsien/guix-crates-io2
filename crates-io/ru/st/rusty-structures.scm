(define-module (crates-io ru st rusty-structures) #:use-module (crates-io))

(define-public crate-rusty-structures-0.1.0 (c (n "rusty-structures") (v "0.1.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "evalexpr") (r "^8.1.0") (d #t) (k 0)) (d (n "fastmurmur3") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fdr6jpigpgxxxznlhvhqvfxnl3zb1xxnxwamajkwf5044bxd8n7")))

