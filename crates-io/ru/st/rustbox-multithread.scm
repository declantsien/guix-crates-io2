(define-module (crates-io ru st rustbox-multithread) #:use-module (crates-io))

(define-public crate-rustbox-multithread-0.8.1 (c (n "rustbox-multithread") (v "0.8.1") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "num") (r "^0.1.13") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.9") (d #t) (k 0)))) (h "17kz4lz9b8ls44a5ggyb6p5qssm3lylvy4ykh5brlgsjcfdp9kfd") (y #t)))

