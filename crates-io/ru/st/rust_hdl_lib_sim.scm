(define-module (crates-io ru st rust_hdl_lib_sim) #:use-module (crates-io))

(define-public crate-rust_hdl_lib_sim-0.44.0 (c (n "rust_hdl_lib_sim") (v "0.44.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "rust_hdl_lib_core") (r "^0.44.0") (d #t) (k 0)) (d (n "rust_hdl_lib_widgets") (r "^0.44.0") (d #t) (k 0)))) (h "1z5vs6clkh5dy2scmpwy57hh9f558cl8brygyfv7scpg5cnc9ahi")))

