(define-module (crates-io ru st rust-phone-number-geo) #:use-module (crates-io))

(define-public crate-rust-phone-number-geo-0.1.0 (c (n "rust-phone-number-geo") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10dqgr61mhaiypnrhyc5x11fs3va042bm2lr0x380rbj6s2j38f8") (y #t)))

(define-public crate-rust-phone-number-geo-0.1.1 (c (n "rust-phone-number-geo") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09sl2nmvw2jidq07nn5q37asrvv9dqarq694lksi3fnn83bwryvm") (y #t)))

(define-public crate-rust-phone-number-geo-0.1.2 (c (n "rust-phone-number-geo") (v "0.1.2") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05b08s04xg2v9wgpcy6qmz9shkjbby0yrjg8a2jfryihpbznzc5b")))

