(define-module (crates-io ru st rust-console-game-engine) #:use-module (crates-io))

(define-public crate-rust-console-game-engine-0.3.1 (c (n "rust-console-game-engine") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "19yxrl2pikp6s8m8vfc7qk5k1bnzfhi3yz2dmgm67sfrn3kk7bqj")))

(define-public crate-rust-console-game-engine-0.3.2 (c (n "rust-console-game-engine") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "1mlgx8w1vcah993r62gp872cam3khvca8yhk4141q4s49d17642p")))

(define-public crate-rust-console-game-engine-0.4.0 (c (n "rust-console-game-engine") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "0l8jp6hj4cbwl604zb2glcghxpc1j5ihzawl7pab7sd7lplhsl13")))

(define-public crate-rust-console-game-engine-0.5.0 (c (n "rust-console-game-engine") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "1a3l47jw0fhbwj3lzz49ckav68c1z6qk2byx2x5r6lmzg692yllp")))

(define-public crate-rust-console-game-engine-0.5.1 (c (n "rust-console-game-engine") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (k 1)))) (h "0q2ls84pw01bn34gk8003r2b3f2hz8vznajmv2z0j67v68nf1g4v")))

