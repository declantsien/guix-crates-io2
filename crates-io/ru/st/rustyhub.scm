(define-module (crates-io ru st rustyhub) #:use-module (crates-io))

(define-public crate-rustyhub-0.2.0 (c (n "rustyhub") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1zpgbggwq959d88nqbi67rkls958maqlvbxyp7rmmj8zvg02r23j")))

(define-public crate-rustyhub-0.3.0 (c (n "rustyhub") (v "0.3.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0srq7pfckrbcl9rdif0wrkhkpmw5bijyfd19k9cy2a1h12al79iq")))

