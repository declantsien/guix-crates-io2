(define-module (crates-io ru st rustring_builder) #:use-module (crates-io))

(define-public crate-rustring_builder-0.1.0 (c (n "rustring_builder") (v "0.1.0") (h "0m94v2gnc9g2x1aj0irmsjnc6kr2xkvzn777ndhgl6si5b4vmkvl")))

(define-public crate-rustring_builder-0.2.0 (c (n "rustring_builder") (v "0.2.0") (h "0bwdjl9bivakdysbdlw249ficz3ba4dg1g68flqpgpk3an8xvg3v")))

