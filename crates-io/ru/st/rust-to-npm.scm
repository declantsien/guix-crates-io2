(define-module (crates-io ru st rust-to-npm) #:use-module (crates-io))

(define-public crate-rust-to-npm-0.1.0 (c (n "rust-to-npm") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)))) (h "1x6ypxdxnxzckpdqxdfclkjg54xzmn3gjfw79r59xnvvd6m8n6cb")))

(define-public crate-rust-to-npm-0.1.8 (c (n "rust-to-npm") (v "0.1.8") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "18amnnzd8rykqr59590xg9ja3wmdh4d75iyrz9hzr41r5dw55zff")))

(define-public crate-rust-to-npm-0.2.0 (c (n "rust-to-npm") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1gbln9ibgx6209ja86ml7lkylbym15vbma8ay5agklx8r3rwlbm5")))

(define-public crate-rust-to-npm-0.2.1 (c (n "rust-to-npm") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1gvvcl2qqg76lkr4jpyx7138n05cgj73xb7jhknc6kvrrsjlzbw2")))

(define-public crate-rust-to-npm-0.2.2 (c (n "rust-to-npm") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1zrgfrj06ydlivn53bkx31fr66dds8frb7c0yj10ys269s5mbfbq")))

(define-public crate-rust-to-npm-0.2.3 (c (n "rust-to-npm") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1jfcak4s3dbvxvdnaai2n7hs96rql51k9vqh8gfdh5mi1z7ia5a8")))

(define-public crate-rust-to-npm-0.2.4 (c (n "rust-to-npm") (v "0.2.4") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0baadr28qa5lx1fwwwglmazbgwyx5amslh4w8mlrijj8j73nzk63")))

(define-public crate-rust-to-npm-0.2.5 (c (n "rust-to-npm") (v "0.2.5") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0i80makm91yfd4pg7yy2nq3ppdlpjh2wgj171plk2hm676pk3gfs")))

(define-public crate-rust-to-npm-0.2.6 (c (n "rust-to-npm") (v "0.2.6") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1ig8bvxs1m4i8yp5mgvy2zyfx5qfkmrs4lzv4f0a68hqq14922cl")))

(define-public crate-rust-to-npm-0.2.7 (c (n "rust-to-npm") (v "0.2.7") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0j9avq0qqzyal3p4bqnssg0i2gn7nq35fwzja3yb11gypj374j1l")))

(define-public crate-rust-to-npm-0.2.8 (c (n "rust-to-npm") (v "0.2.8") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "01jbyczydkfpibhnp4bl2dhf53hx02w79qkz4p3ax90942m6sdqc")))

(define-public crate-rust-to-npm-0.2.9 (c (n "rust-to-npm") (v "0.2.9") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1pj06xhfag4ni5fcbfwh99pqg7hv78ns4rk3qrwg5qr8qbckq0hg")))

(define-public crate-rust-to-npm-0.2.10 (c (n "rust-to-npm") (v "0.2.10") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0nlwkikycxcnb5qj82i6w453szcvh6v32333188zzm9nmcz64y70")))

(define-public crate-rust-to-npm-0.3.0 (c (n "rust-to-npm") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0rda64i2z6nr8nqlqnp6kpjv4bc6adi87j1km4bzd1ddgamiz2zh")))

(define-public crate-rust-to-npm-0.3.1 (c (n "rust-to-npm") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1zqhjiq5i6lylhfgzkzyiv4yqc6cg3scq7crb9cxh89hlz7sxggg")))

(define-public crate-rust-to-npm-0.3.2 (c (n "rust-to-npm") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1f203v8qyq4gjadfpx2kkpwac9y7y1g3rscgxilv43c2jv31m3hs")))

(define-public crate-rust-to-npm-0.3.3 (c (n "rust-to-npm") (v "0.3.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "01zjhwsswnw47jfq67mk1z3c1nnxx28nzivy5447j7rxk1da879a")))

(define-public crate-rust-to-npm-0.3.4 (c (n "rust-to-npm") (v "0.3.4") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "11cy39qcca2w671p9jd8k7kd9hs4khf35nd51b4pk7rhcmirqqxw")))

(define-public crate-rust-to-npm-0.3.5 (c (n "rust-to-npm") (v "0.3.5") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0qiqafyz0pq98dra7liiq41gia4wkql76lgyi334dap2qn4isl8w")))

(define-public crate-rust-to-npm-0.3.6 (c (n "rust-to-npm") (v "0.3.6") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1sz2wxksi8r8163a5diiqnmh21dha2d8cmn5dnps0xn8cz1mkqd7") (y #t)))

(define-public crate-rust-to-npm-0.3.7 (c (n "rust-to-npm") (v "0.3.7") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "15fc92n8vb2h1q8dqnaq45nrgqii2xkn8x1n53xmxg9jls2ab4mm")))

(define-public crate-rust-to-npm-0.3.8 (c (n "rust-to-npm") (v "0.3.8") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1b094fwvp6h5adm4w4cyy04kni32pbv22a4y7m89nj60z6wmip99") (y #t)))

(define-public crate-rust-to-npm-0.3.9 (c (n "rust-to-npm") (v "0.3.9") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0zr5bdmm1p3881n7r0a7z9lr9x8vrfk44x1yvrnxyk00ai4w2smp")))

(define-public crate-rust-to-npm-0.3.10 (c (n "rust-to-npm") (v "0.3.10") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "13dlz9df4hg8jd35djc0b8010brsq7xbwd5dsyip9gb5lz0cc9bi")))

(define-public crate-rust-to-npm-0.4.0 (c (n "rust-to-npm") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1yapkf3bk939ln84mgmb5jlb8rxfvi1wm6ahyq71irkbngzvnjfy")))

(define-public crate-rust-to-npm-0.4.1 (c (n "rust-to-npm") (v "0.4.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1hbl9idx5jgnw8ph293hfkd46gr1mfl8pknn77vkxzl3df20h48j")))

(define-public crate-rust-to-npm-0.4.2 (c (n "rust-to-npm") (v "0.4.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1y0f83gzihks6aws3s9y5ffinwpigvz6npmiapsrwc685x8i2v97")))

(define-public crate-rust-to-npm-0.4.3 (c (n "rust-to-npm") (v "0.4.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "047c7k3z3dncczs3vp00hrgc5kl3qdr5ph8xrdx9jzqaih9188fp")))

(define-public crate-rust-to-npm-0.4.4 (c (n "rust-to-npm") (v "0.4.4") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1q79ss81jfhvdlns8p6prjlysa592x8s6pprm0561ajv0aw4i49v")))

(define-public crate-rust-to-npm-0.4.5 (c (n "rust-to-npm") (v "0.4.5") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1iv80yqvsw0yy5v3sjyi19w0lyglpnj9fsvhjzyic2d1byx4qgz1")))

(define-public crate-rust-to-npm-0.4.6 (c (n "rust-to-npm") (v "0.4.6") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0z5bwi0izpz238q144bk7hv7cykx8xcawn8jskfwr6rv0mmnl0xw")))

(define-public crate-rust-to-npm-0.4.9 (c (n "rust-to-npm") (v "0.4.9") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "107gvyl01bgfm5xxc4xrzdyn95xjbpbw6ynm0lplm094n2lnibki")))

(define-public crate-rust-to-npm-0.4.10 (c (n "rust-to-npm") (v "0.4.10") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0i0bcrpkxgg6f53x9drcsxxinsz4xqxxlwzq0aby3ly1vgrl04wn")))

(define-public crate-rust-to-npm-0.4.11 (c (n "rust-to-npm") (v "0.4.11") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0821rz2qjsn58favk60p71x9zshfkbay7fd8wxswjmf3xk84lm41")))

(define-public crate-rust-to-npm-0.4.12 (c (n "rust-to-npm") (v "0.4.12") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0q49znz64cvz1542g3cr22h3m2w3jf56y5z4z1hk91vddvvqvp0y")))

(define-public crate-rust-to-npm-0.4.13 (c (n "rust-to-npm") (v "0.4.13") (d (list (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "02l3v1qgly6nswh27yi97b44qxr1c0dz3zn44l8bjvdirbwdfj4w")))

