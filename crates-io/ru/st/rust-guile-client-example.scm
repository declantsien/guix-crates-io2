(define-module (crates-io ru st rust-guile-client-example) #:use-module (crates-io))

(define-public crate-rust-guile-client-example-0.1.0 (c (n "rust-guile-client-example") (v "0.1.0") (d (list (d (n "rust-guile") (r "^0.1.2") (d #t) (k 0)))) (h "1m4kammv7i4lp5wga6lypbyfdp4l21sjfk4ii4ga1azns07dh2xf")))

(define-public crate-rust-guile-client-example-0.1.1 (c (n "rust-guile-client-example") (v "0.1.1") (d (list (d (n "rust-guile") (r "^0.1.5") (d #t) (k 0)))) (h "0879d7cw8nx5x7wkxqsx7106386myr7r6z40mx7n18n1wbrq3912")))

