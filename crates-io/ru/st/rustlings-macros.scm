(define-module (crates-io ru st rustlings-macros) #:use-module (crates-io))

(define-public crate-rustlings-macros-6.0.0-alpha.0 (c (n "rustlings-macros") (v "6.0.0-alpha.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)))) (h "0wgm4s6a66jy704ddvj1c5ad77bwlb1mdslb433srywdmbfg0x35")))

(define-public crate-rustlings-macros-6.0.0-beta.1 (c (n "rustlings-macros") (v "6.0.0-beta.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "0858mn860cksghc75b699l1fhj4dhylmyirf14m7jw1chhvngsza") (y #t)))

(define-public crate-rustlings-macros-6.0.0-beta.2 (c (n "rustlings-macros") (v "6.0.0-beta.2") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "1ijbx1dvfyr852svx55z3wwg454nmnq8mwpjyiv0mv73r59ry1kc") (y #t)))

(define-public crate-rustlings-macros-6.0.0-beta.3 (c (n "rustlings-macros") (v "6.0.0-beta.3") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "1j8hs1illnnmj5h2albgk0fw3jz606x70mq5mz1qkk2am19vrr53")))

(define-public crate-rustlings-macros-6.0.0-beta.4 (c (n "rustlings-macros") (v "6.0.0-beta.4") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "1m9dwcmmcv0ayav8la1hri6y0cb51lrxi9slvl8760may5f1laq7")))

(define-public crate-rustlings-macros-6.0.0-beta.5 (c (n "rustlings-macros") (v "6.0.0-beta.5") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "13zd7l1qykdsfahskzd9lx09i4qf5v6k4ppcci5p2l67bv5jkyn2")))

(define-public crate-rustlings-macros-6.0.0-beta.6 (c (n "rustlings-macros") (v "6.0.0-beta.6") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "0ld3iacl7ih3m0i2w8lbb92g6smcrxlc5h9vvds9mdpmrmrlw0r8")))

(define-public crate-rustlings-macros-6.0.0-beta.7 (c (n "rustlings-macros") (v "6.0.0-beta.7") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "03cllky6s49blqcxhqgxxjgzqgrdwkmc7x8s9snm6bw321lbyd0d")))

(define-public crate-rustlings-macros-6.0.0-beta.8 (c (n "rustlings-macros") (v "6.0.0-beta.8") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "0jd9rinc5flicdsdkb8v6pcqqrjxc6jmiq1d262nj96xbcra4fyf")))

(define-public crate-rustlings-macros-6.0.0-beta.9 (c (n "rustlings-macros") (v "6.0.0-beta.9") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.12") (f (quote ("parse" "serde"))) (k 0)))) (h "026wb0q8wcis1s25jqiwyc7f26jjnc9wnpmk60c6n7d175dk3cbz")))

