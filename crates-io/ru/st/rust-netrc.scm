(define-module (crates-io ru st rust-netrc) #:use-module (crates-io))

(define-public crate-rust-netrc-0.1.0 (c (n "rust-netrc") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1z81g9yy0ir0xkr9m36bxnj9vqd48xnc42ggxwlmhblzfa1var0a")))

(define-public crate-rust-netrc-0.1.1 (c (n "rust-netrc") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0s280kr7rxd0b6gpxlsgmryix1q6dw8kh4wgyzaxkfpxrfbjyrij")))

