(define-module (crates-io ru st rustcycles) #:use-module (crates-io))

(define-public crate-rustcycles-0.0.0 (c (n "rustcycles") (v "0.0.0") (h "05jqa4scb7pvchyjryhp0zp6c0ai20kwkjagxzkzkchlalx85w64")))

(define-public crate-rustcycles-0.0.1 (c (n "rustcycles") (v "0.0.1") (h "1869gnr9jyislfbs2fdfblpnbxcqwjq73wllf1bkfxsh8n5rqp7g")))

