(define-module (crates-io ru st rusty-sorter) #:use-module (crates-io))

(define-public crate-rusty-sorter-0.1.0 (c (n "rusty-sorter") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0dkdfn8n4864hx6jrpzm6lfb0fz6ca63id5hl0wbn99l3mg2blm9") (r "1.62")))

(define-public crate-rusty-sorter-0.1.1 (c (n "rusty-sorter") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1gblsl920855qf38162w0za6188yxm448maa79l16c5vsaj9sqr7") (r "1.62")))

