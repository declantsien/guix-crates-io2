(define-module (crates-io ru st rust_book_code) #:use-module (crates-io))

(define-public crate-rust_book_code-0.1.0 (c (n "rust_book_code") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07kb456avbx5bz1p6dwpg4sdlcd7p05rfmi38lrs7srfsnhwim80")))

(define-public crate-rust_book_code-0.2.0 (c (n "rust_book_code") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12k4ddl0p38v0pllx2ya21l62wdsndkv3bav1nkn7mnfyfcw1iwx") (y #t)))

(define-public crate-rust_book_code-0.2.1 (c (n "rust_book_code") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "017igbs0zyfdp5pm2dzg2zvg2phxgrc0s278474h5bpn5lmg8sx5")))

