(define-module (crates-io ru st rustmo-devices) #:use-module (crates-io))

(define-public crate-rustmo-devices-0.1.0 (c (n "rustmo-devices") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rustmo-server") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "1ha207i201v729qzgjci2w0wnlphm6lc9v0hcvcj4x2ca6ljypsq")))

(define-public crate-rustmo-devices-0.1.1 (c (n "rustmo-devices") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rustmo-server") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "0bbmnnfhf15m0p4b1f0pw8309ax2x0l71irs6z66rz0xnqrzi5y5")))

(define-public crate-rustmo-devices-0.1.2 (c (n "rustmo-devices") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rustmo-server") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "1a8y1bjixv01p43vka59ndpxk6sv0paafrwkb631jnkd1568w5g9")))

