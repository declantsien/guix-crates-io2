(define-module (crates-io ru st rust-jni) #:use-module (crates-io))

(define-public crate-rust-jni-0.1.0 (c (n "rust-jni") (v "0.1.0") (d (list (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "150iqnsbw64digcvx5nfhni5vknzgsmpcmldnkcc9jpyy6qihjq1") (f (quote (("libjvm"))))))

