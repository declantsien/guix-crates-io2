(define-module (crates-io ru st rusty_cc) #:use-module (crates-io))

(define-public crate-rusty_cc-0.1.0 (c (n "rusty_cc") (v "0.1.0") (h "1p3kmvvq7s5x5v4lmgvg52dddd9fnlcsrrhx0aszs10qimj5xxgc")))

(define-public crate-rusty_cc-0.1.1 (c (n "rusty_cc") (v "0.1.1") (h "0zxsn3cds7w2qy7yjcnlkrw31yk5b5wlw834zfgfgbjd8apsy6yq")))

(define-public crate-rusty_cc-0.1.2 (c (n "rusty_cc") (v "0.1.2") (h "031im28ah69ibf8pi1gjcr110qa3y8xz70m04skvnd57sj6h0p3j")))

