(define-module (crates-io ru st rustoken) #:use-module (crates-io))

(define-public crate-rustoken-0.0.0 (c (n "rustoken") (v "0.0.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "17p4zdnmrvxqc9ykxm9f5p0mgldm2k609sjzv3rhzxcpga9hixqi")))

