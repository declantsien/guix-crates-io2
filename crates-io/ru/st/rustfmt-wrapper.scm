(define-module (crates-io ru st rustfmt-wrapper) #:use-module (crates-io))

(define-public crate-rustfmt-wrapper-0.1.0 (c (n "rustfmt-wrapper") (v "0.1.0") (d (list (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.2.0") (d #t) (k 0)))) (h "1y53kpwh62nnh809iv76z9g92ks2ma2cmrrjc8jqng5inmzmfcvp")))

(define-public crate-rustfmt-wrapper-0.2.0 (c (n "rustfmt-wrapper") (v "0.2.0") (d (list (d (n "newline-converter") (r "^0.2.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.2.0") (d #t) (k 0)))) (h "0vj0yzj1dvzah8xlrjc3vkpjbpczra87xhlksppjpv08xqxrwwpd")))

(define-public crate-rustfmt-wrapper-0.2.1 (c (n "rustfmt-wrapper") (v "0.2.1") (d (list (d (n "newline-converter") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.4.0") (d #t) (k 0)))) (h "0h54ll62534cxb22g66kr18mfb18p5ip3k3qg43rkjawxpgwkbgi")))

