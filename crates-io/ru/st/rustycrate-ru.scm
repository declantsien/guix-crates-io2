(define-module (crates-io ru st rustycrate-ru) #:use-module (crates-io))

(define-public crate-rustycrate-ru-0.1.0 (c (n "rustycrate-ru") (v "0.1.0") (h "1aqi24vp1nmsmhsidmxdd1syg1npyclhx4r6mpqwciipxdwx88n5")))

(define-public crate-rustycrate-ru-0.1.1 (c (n "rustycrate-ru") (v "0.1.1") (h "0v68sy6d1kmq80q85aa2p03bs7ln141yzmmvalz96qjjfyb9mjq6")))

(define-public crate-rustycrate-ru-0.1.2 (c (n "rustycrate-ru") (v "0.1.2") (h "0496sr82blvyy1091h7wp79jh5vm1dn67r7y3xbn8666v9v022ng")))

