(define-module (crates-io ru st rust-fetch) #:use-module (crates-io))

(define-public crate-rust-fetch-0.1.0 (c (n "rust-fetch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "httpmock") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ngzns88cz4x9z66avdzbf6fkb9j26zxfzbqlqkscp8g0wahpyjz")))

