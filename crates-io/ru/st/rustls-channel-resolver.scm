(define-module (crates-io ru st rustls-channel-resolver) #:use-module (crates-io))

(define-public crate-rustls-channel-resolver-0.1.0 (c (n "rustls-channel-resolver") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.4.1") (f (quote ("rustls-0_21"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs"))) (d #t) (k 2)))) (h "1jya85cg71291z5gfmzf0shb6r1sylmjslqk2i63na6w8vsnn2ny")))

(define-public crate-rustls-channel-resolver-0.2.0 (c (n "rustls-channel-resolver") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.4.1") (f (quote ("rustls-0_22"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.22") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs"))) (d #t) (k 2)))) (h "0rp1z7wk6dlc6yrixns3zpbll1yvx2l7x6kaax8z0hj4410ikggz")))

(define-public crate-rustls-channel-resolver-0.3.0 (c (n "rustls-channel-resolver") (v "0.3.0") (d (list (d (n "actix-web") (r "^4.6.0") (f (quote ("rustls-0_23"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "rustls") (r "^0.23") (k 0)) (d (n "rustls") (r "^0.23") (d #t) (k 2)) (d (n "rustls-pemfile") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs"))) (d #t) (k 2)))) (h "1412mlk690119zjig32xg9bnlbcc8v33wwppk14npnjrfcj2mppy")))

