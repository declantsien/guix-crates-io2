(define-module (crates-io ru st rustposix_rup) #:use-module (crates-io))

(define-public crate-rustposix_rup-0.1.0 (c (n "rustposix_rup") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "grcov") (r "^0.8.19") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0n1n2g5d74nkwbgddk36nprxp168f92dnq3wk9yf3ixad40cinv7")))

