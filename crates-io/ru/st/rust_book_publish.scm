(define-module (crates-io ru st rust_book_publish) #:use-module (crates-io))

(define-public crate-rust_book_publish-0.1.0 (c (n "rust_book_publish") (v "0.1.0") (h "15345xgipaxx5vgbs71plcpingifrw24zlfnrbc44l3vlzwkm33p") (y #t)))

(define-public crate-rust_book_publish-0.2.0 (c (n "rust_book_publish") (v "0.2.0") (h "03nxv6f7r2954bhzqymljxnj0hz0wx3a53rr28clcb79jrvnb09s")))

