(define-module (crates-io ru st rust-bloomfilter) #:use-module (crates-io))

(define-public crate-rust-bloomfilter-0.0.1 (c (n "rust-bloomfilter") (v "0.0.1") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "1sam0mjas4zwxsws6vzkz04z13nvkqx781x037jq7i8kphij5yiq") (y #t)))

(define-public crate-rust-bloomfilter-0.0.2 (c (n "rust-bloomfilter") (v "0.0.2") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "0wh3kcizak4h843hyhwjplwf1l6k9gg4zvbydrbnqhcp4knbp60r") (y #t)))

(define-public crate-rust-bloomfilter-0.0.3 (c (n "rust-bloomfilter") (v "0.0.3") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "17cn5qy2bx37cvg749jaba5i50p0fandlfnhq14k9jqw32w434zm") (y #t)))

(define-public crate-rust-bloomfilter-0.0.4 (c (n "rust-bloomfilter") (v "0.0.4") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "0ch6h7xdscsh1a1a0fza3r23g0n4q66zvry40dhld0d0y1k0dlvm") (y #t)))

(define-public crate-rust-bloomfilter-1.0.0-beta (c (n "rust-bloomfilter") (v "1.0.0-beta") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "1v3an6rjp8fpidprfxy5bld77qjqkvwzxqvp83l3k7j1s8qssr63")))

(define-public crate-rust-bloomfilter-1.0.0-beta1 (c (n "rust-bloomfilter") (v "1.0.0-beta1") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "1wh1m9c6k11g34dgv49kfz65qhx0nbdj1q41x2rjwbk4mp0z5ldk")))

