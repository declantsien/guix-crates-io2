(define-module (crates-io ru st rusty-chip8) #:use-module (crates-io))

(define-public crate-rusty-chip8-0.1.0 (c (n "rusty-chip8") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bracket-terminal") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1gsf5aqbb2gxqj1z67p8f0751mjjh67kpiz9zp2k7dj76i2can23")))

(define-public crate-rusty-chip8-1.0.0 (c (n "rusty-chip8") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "bevy") (r "^0.9.0") (f (quote ("dynamic"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1b5jbm10mqac1n0y7zwcr1dz3qs4qxfnx8wmgb0nswks269jp8mg")))

(define-public crate-rusty-chip8-1.1.0 (c (n "rusty-chip8") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "bevy") (r "^0.9.0") (f (quote ("dynamic"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "08fsl6qg4za8mc29dvndf5a4r6y8j0xh4ll1kipr0h9d9r0k2c0n")))

