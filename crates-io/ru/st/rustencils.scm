(define-module (crates-io ru st rustencils) #:use-module (crates-io))

(define-public crate-rustencils-0.1.0 (c (n "rustencils") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)))) (h "01qqv4ncjad9n70nl5bgn7abdiny7xg8592i0i1wrp57bdhlm6qn")))

(define-public crate-rustencils-0.1.1 (c (n "rustencils") (v "0.1.1") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (d #t) (k 0)))) (h "14f8g318xv8yx4lf6dssamg97jiixlhrxa5nzal50n6hh3p6h7qp")))

(define-public crate-rustencils-0.1.2 (c (n "rustencils") (v "0.1.2") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (d #t) (k 0)))) (h "0ia2mpxqkcsnsm14amrx5qmxypy19pplj2sp2709x801c1vq1r29")))

(define-public crate-rustencils-0.1.3 (c (n "rustencils") (v "0.1.3") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (d #t) (k 0)))) (h "126fsccjfynd33r8fdz782n9m91y735fyplclfcczxvl1rz4n12x")))

(define-public crate-rustencils-0.1.4 (c (n "rustencils") (v "0.1.4") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (d #t) (k 0)))) (h "1h0ijrldxjzi0bsa542742vj4pp6y2pfgsjymkbpmpnldvnjzg2r")))

(define-public crate-rustencils-0.1.5 (c (n "rustencils") (v "0.1.5") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (d #t) (k 0)))) (h "1gqkxbwxz6gvclr4n1szds0gxf3b4v4zqh5495ma4vpiiz6zfdh2")))

(define-public crate-rustencils-0.1.6 (c (n "rustencils") (v "0.1.6") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (d #t) (k 0)))) (h "17mg82d91m515yxv0hpz70r0p711ybs4xd6dppgnjkxbmrms4kd7") (f (quote (("openblas" "ndarray-linalg/openblas") ("netlib" "ndarray-linalg/netlib") ("intel-mkl" "ndarray-linalg/intel-mkl") ("default" "openblas"))))))

