(define-module (crates-io ru st rustplotlib) #:use-module (crates-io))

(define-public crate-rustplotlib-0.0.1 (c (n "rustplotlib") (v "0.0.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rmp-serialize") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "1ac9bls1m6mn0n5d09p42l9hghrwvh9z367wynxm9hy0hczh0xj0")))

(define-public crate-rustplotlib-0.0.2 (c (n "rustplotlib") (v "0.0.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rmp-serialize") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "1f20jmngd1spzdij1g5rsfypa1w71qz2m99g10c87hcn50rv3xbm")))

(define-public crate-rustplotlib-0.0.3 (c (n "rustplotlib") (v "0.0.3") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "rmp-serialize") (r "^0.7.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "18pj6d73v1dm5k1ln700k0a0i276pqh5dyvzdxhzj44pc57i5qrj")))

(define-public crate-rustplotlib-0.0.4 (c (n "rustplotlib") (v "0.0.4") (h "12mqxca19q4hr0423ds9m20r9l6a3yzxl4mdha943zz4cyngf9j3") (f (quote (("default"))))))

