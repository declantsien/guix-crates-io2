(define-module (crates-io ru st rust-on-rv32i) #:use-module (crates-io))

(define-public crate-rust-on-rv32i-0.0.1 (c (n "rust-on-rv32i") (v "0.0.1") (h "1bz7m45j2al953lr4f41c5638mb9ld4v8nxi7s5svsc5i21mhrr7")))

(define-public crate-rust-on-rv32i-0.0.11 (c (n "rust-on-rv32i") (v "0.0.11") (h "0xhjpnarbdghcdxdfiwdv5i562wlnazx2fsxl1fajifgbb7vrrz5")))

(define-public crate-rust-on-rv32i-0.0.12 (c (n "rust-on-rv32i") (v "0.0.12") (h "1fzg8v1z798nciaiw5gyc9rb4r1ml2i9ks5dvpk31szvsqk4xx56")))

(define-public crate-rust-on-rv32i-0.0.13 (c (n "rust-on-rv32i") (v "0.0.13") (h "0ppfakq8c31499fjmvk42lwgdqrsh60ppsm2070dhxzkz6fsysvs") (f (quote (("uart") ("gpio") ("default" "uart" "gpio"))))))

(define-public crate-rust-on-rv32i-0.0.14 (c (n "rust-on-rv32i") (v "0.0.14") (h "0cw56qznqj29q0vzdy2h4vcg5di0rihfgzfzj99kbyj62nriwpyc")))

