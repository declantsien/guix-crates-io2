(define-module (crates-io ru st rustbox2) #:use-module (crates-io))

(define-public crate-rustbox2-0.0.1 (c (n "rustbox2") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.2.1") (d #t) (k 0)) (d (n "gag") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.13") (d #t) (k 0)) (d (n "termbox2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1ibdhg1lxj7cmy2hv6wlnyrmxif2fysizrp3gbgby7jdanwg5wq4")))

