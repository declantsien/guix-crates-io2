(define-module (crates-io ru st rust_weather) #:use-module (crates-io))

(define-public crate-rust_weather-1.0.0 (c (n "rust_weather") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("std" "color" "derive" "suggestions" "unicode" "cargo" "env" "wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "01n1m8w5kcyakcc2mmj8q4p2mniyvdb2w3ax5jfqcxnjpp1iqscx")))

(define-public crate-rust_weather-1.1.0 (c (n "rust_weather") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("std" "color" "derive" "suggestions" "unicode" "cargo" "env" "wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "04l9ii6x2nvb7bl59m2zlcw4i2nm2kp259dvlpvsljkhkijagz6g")))

(define-public crate-rust_weather-1.2.0 (c (n "rust_weather") (v "1.2.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("std" "color" "derive" "suggestions" "unicode" "cargo" "env" "wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0hk6pfhf7hihwdz7shqbd8c1vynca8m93p85l8blpl6r0c7i15g4")))

