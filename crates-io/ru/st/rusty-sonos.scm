(define-module (crates-io ru st rusty-sonos) #:use-module (crates-io))

(define-public crate-rusty-sonos-0.1.0 (c (n "rusty-sonos") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "0bg6n3zi7awd7mqwjkbm68n9y0i7bsjf0kn1sxy390yp5v63l836")))

(define-public crate-rusty-sonos-0.1.1 (c (n "rusty-sonos") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "1raszf2jw5aw1cqw5pb2i5f2xwqp7aw913xi3d3c5ygi7kbscyq0")))

(define-public crate-rusty-sonos-0.1.2 (c (n "rusty-sonos") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "10d0f2hbpl0is3ald7qva92yw0c8nwzbkk108klkax4g3w7cdip0")))

(define-public crate-rusty-sonos-0.2.2 (c (n "rusty-sonos") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "134yh6lcxcnxy3f54cd8z344m2cgp8h78r7wiiv8i8q7a18p3vdm") (y #t)))

(define-public crate-rusty-sonos-0.2.0 (c (n "rusty-sonos") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "08n6wncskv1jngxan4mlab8qigf22mgsdmdgzpg8n5sywz2a1sbn")))

(define-public crate-rusty-sonos-0.2.1 (c (n "rusty-sonos") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "042m5l11c8q3a9jxr83w3ig3rjzby8g1rjrw03rzpjqs7narxzdz")))

(define-public crate-rusty-sonos-0.2.3 (c (n "rusty-sonos") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "1d1klhsfgdkszaxvldclq48gg8fdkgngi61s8xyfh0albn6ydv38")))

(define-public crate-rusty-sonos-0.2.4 (c (n "rusty-sonos") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "0rxfx0ljmk10fmp77qj0v2d703akpvrml8ibg0mc5dzfrqjn7izd")))

(define-public crate-rusty-sonos-0.2.5 (c (n "rusty-sonos") (v "0.2.5") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "xml-builder") (r "^0.5.2") (d #t) (k 0)))) (h "1j6ar8npxmgi7k8wdndskklawh3brxf5ji53kmxxd4n1syr1r1b5")))

