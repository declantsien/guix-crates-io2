(define-module (crates-io ru st rusthtml) #:use-module (crates-io))

(define-public crate-rusthtml-0.1.0 (c (n "rusthtml") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1a91i8pvv31mxlvyh6psp8bqky5v0k5ld7cc2b4f1gqas3nylfdq")))

(define-public crate-rusthtml-0.2.1 (c (n "rusthtml") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1r8rcz7vfb60ivd1kns04x6qmzdh4h6yy4vdw01rdw682xjp8lki")))

(define-public crate-rusthtml-0.2.2 (c (n "rusthtml") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0hirc0qr1jfbsb6mgadm56isws830d1b8r83xmwb44s8d1hgvcph")))

(define-public crate-rusthtml-0.2.3 (c (n "rusthtml") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0625gpjpyr6j3nfdn1rfgyz605pq5rdvgf2h7xp5695krjm3n01i")))

(define-public crate-rusthtml-0.2.4 (c (n "rusthtml") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "12ifv2v5d5n2qbq5fjas0pnm5lbpin1631pxy9j11wqk9aanxc82")))

