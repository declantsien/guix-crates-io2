(define-module (crates-io ru st rust_redlock) #:use-module (crates-io))

(define-public crate-rust_redlock-0.1.0 (c (n "rust_redlock") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "0xd3p8c4z2m7cwmq7ry2gypl5yplyji6hpvli4zalcfrsmzrjivl")))

(define-public crate-rust_redlock-0.1.1 (c (n "rust_redlock") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "0pycbzf8qld7rcsaf8y1adh46jv348xgf7a5l5vwbgwyqvfl2iag")))

(define-public crate-rust_redlock-0.2.0 (c (n "rust_redlock") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "19wfg2p076qdzrayszbisd41xzkl9m6w05cgbxz84cncw18v65aj")))

(define-public crate-rust_redlock-0.2.1 (c (n "rust_redlock") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "1x4nh47q1qalksz7idb2jhkxmcfd1d2sp21bccbr3n5lm2lbfgxm")))

(define-public crate-rust_redlock-0.2.2 (c (n "rust_redlock") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "0jmid7858z00sgnpmkzr89ri6vv3d79nn4jlyb33rk36fby29f7v")))

(define-public crate-rust_redlock-0.3.0 (c (n "rust_redlock") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "0p1gbmw5jjpsgf20vijmq0yf3wnbb54n20szp01h31ndsd6ry9k4")))

(define-public crate-rust_redlock-0.3.1 (c (n "rust_redlock") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "1wwqp752yhs0zi2almcssm02yq6gsjy4k78h35x7r04594ppwcb4")))

(define-public crate-rust_redlock-0.3.2 (c (n "rust_redlock") (v "0.3.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "03sfz30pfb894qix5ma6m0npx7hiqr393rdgqs3fbwldzyhiq1qd")))

(define-public crate-rust_redlock-0.4.0 (c (n "rust_redlock") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "0pmwkdm32631bcw2jnzaa3ixrgi1dg0qchyhx93ypsfpn1f3farn")))

