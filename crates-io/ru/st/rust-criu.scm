(define-module (crates-io ru st rust-criu) #:use-module (crates-io))

(define-public crate-rust-criu-0.1.0 (c (n "rust-criu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.25.2") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.25.2") (d #t) (k 1)))) (h "1iwgrlndd50spcdh5bm6i7p8pqvvk5q9p1fvj0krsbaffdphhi8r")))

(define-public crate-rust-criu-0.2.0 (c (n "rust-criu") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.25.2") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.25.2") (d #t) (k 1)))) (h "0b0wjmx8dzlicdyzx742k8naaibfpwgyr30v6c9zk0j6yxab7hgv")))

(define-public crate-rust-criu-0.4.0 (c (n "rust-criu") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2.0") (d #t) (k 1)))) (h "1g5c6aaagfbap9lqnws2i7fcw4bs25rp04l5yicm6fdk0s2b4ds7")))

