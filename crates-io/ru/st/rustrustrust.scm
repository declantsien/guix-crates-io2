(define-module (crates-io ru st rustrustrust) #:use-module (crates-io))

(define-public crate-RUSTRUSTRUST-0.1.0 (c (n "RUSTRUSTRUST") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "1mj6kmci121fnn3kz9d6g0mlh0zjkrhm6lfc5kwwik8flaiwwpq7")))

(define-public crate-RUSTRUSTRUST-0.1.1 (c (n "RUSTRUSTRUST") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "0hwran405q4my1r5a1pg6nnxyzrl6iksvvwy8a04bkwyzv8lccm1")))

