(define-module (crates-io ru st rustmt) #:use-module (crates-io))

(define-public crate-rustmt-0.1.0 (c (n "rustmt") (v "0.1.0") (h "19c1gn53n9618ksw7g1k1g70ng6dhl4hmggkln2nrrygm9gkarsq")))

(define-public crate-rustmt-0.1.1 (c (n "rustmt") (v "0.1.1") (h "0x3ixixsq34xgx75i5alnz2isjfklgjkg7d9wys32r126y1v469x")))

(define-public crate-rustmt-0.1.2 (c (n "rustmt") (v "0.1.2") (h "09wvdfpbijm53pjbybddxnzazhniv5qwkgr55vhyqnj32vmp7zlk")))

