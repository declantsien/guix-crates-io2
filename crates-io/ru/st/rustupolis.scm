(define-module (crates-io ru st rustupolis) #:use-module (crates-io))

(define-public crate-rustupolis-0.0.2 (c (n "rustupolis") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "indextree-ng") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2.0") (d #t) (k 0)))) (h "0cv8qv4w1f8k6i3npb1wxxyydfas1b7iqhl5cjdnl6jy71pwq78w")))

