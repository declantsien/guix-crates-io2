(define-module (crates-io ru st rustutils-printenv) #:use-module (crates-io))

(define-public crate-rustutils-printenv-0.1.0 (c (n "rustutils-printenv") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "0fnnf9b6ha08nw7xckpvxasc5zcnkrs8xpvpny7drjvak24k6krh")))

