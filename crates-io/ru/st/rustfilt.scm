(define-module (crates-io ru st rustfilt) #:use-module (crates-io))

(define-public crate-rustfilt-0.1.0 (c (n "rustfilt") (v "0.1.0") (d (list (d (n "rustc-demangle") (r "^0.1.0") (d #t) (k 0)))) (h "1d0kjgk2gpf5x1g89i1l6gjhi2lw798xlsz6b0zxpimlxrzzasyb")))

(define-public crate-rustfilt-0.1.1 (c (n "rustfilt") (v "0.1.1") (d (list (d (n "rustc-demangle") (r "^0.1.2") (d #t) (k 0)))) (h "1wfci60qnjm6vnxpwh4llc7kfb5y5v1amc5w02kyw7xik5zz9kii")))

(define-public crate-rustfilt-0.2.0 (c (n "rustfilt") (v "0.2.0") (d (list (d (n "clap") (r "^2.21.1") (f (quote ("wrap_help"))) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.4") (d #t) (k 0)))) (h "12nivmhpcxgf9d8vvp8mzpz2d47z8i9k8adrvccxhwq1dp7sapqm")))

(define-public crate-rustfilt-0.2.1 (c (n "rustfilt") (v "0.2.1") (d (list (d (n "clap") (r "^2.21.1") (f (quote ("wrap_help"))) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.4") (d #t) (k 0)))) (h "1wli026xr94xnb7n1kl14mhsz64mc32xq83d6cj55yf8haqlimy8")))

