(define-module (crates-io ru st rusty-turing-machine) #:use-module (crates-io))

(define-public crate-rusty-turing-machine-0.1.0 (c (n "rusty-turing-machine") (v "0.1.0") (d (list (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0030q1b5vkbs4laxx3lydbda1h90vpnnlccp0mc4775n9yx3vhag")))

