(define-module (crates-io ru st rust-env-sorter) #:use-module (crates-io))

(define-public crate-rust-env-sorter-0.1.0 (c (n "rust-env-sorter") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)))) (h "16gfin89ymdc48gs2xgijwmqinkwvi5v31hlp8x53ywc0rf4iv9a") (f (quote (("windows-only") ("default" "windows-only")))) (y #t)))

(define-public crate-rust-env-sorter-0.1.1 (c (n "rust-env-sorter") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)))) (h "11hsgsnny4d763bdhy247pa68l72y5dp2833cnnf08289wj2hl2g") (f (quote (("windows-only") ("default" "windows-only"))))))

