(define-module (crates-io ru st rusty-yaml) #:use-module (crates-io))

(define-public crate-rusty-yaml-0.1.0 (c (n "rusty-yaml") (v "0.1.0") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "07w54hcyqa4hprx0ji8ri9kkq1acwbbssr1bhbjqxqgbh15sw4w5")))

(define-public crate-rusty-yaml-0.1.1 (c (n "rusty-yaml") (v "0.1.1") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "099snfdkiw5rgxwkwc48i58rswnhm0yqwsywjwpqks25hrw91n61")))

(define-public crate-rusty-yaml-0.1.2 (c (n "rusty-yaml") (v "0.1.2") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "13753za7j00x1vmd0blsr017fhxi8kq019nkfjq19c7mhp93dny1")))

(define-public crate-rusty-yaml-0.2.0 (c (n "rusty-yaml") (v "0.2.0") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "05hss4b9i87z5zkgm3c6fzcs2k3p2hr1g7iybdj3gd9l7d6pyw8y")))

(define-public crate-rusty-yaml-0.2.1 (c (n "rusty-yaml") (v "0.2.1") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "131v5v50dh4dwy9y5wm8fha2cjs3jbqxh6hkl8ki7kb799rkpbzc")))

(define-public crate-rusty-yaml-0.3.0 (c (n "rusty-yaml") (v "0.3.0") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1cn9hz4r4kli8pwd6kibwidg9435jjxs24dzianrra23qmzhlyc9")))

(define-public crate-rusty-yaml-0.3.3 (c (n "rusty-yaml") (v "0.3.3") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "12smnmq5gyfk0w9gw1w3mnhknn0f5wflgyr2n8m87fja4rqi8pi8")))

(define-public crate-rusty-yaml-0.4.0 (c (n "rusty-yaml") (v "0.4.0") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "14rjw3arp8zijkm0imphjnrabvjm53syzjz9yaq8h03a84zz2s5q")))

(define-public crate-rusty-yaml-0.4.1 (c (n "rusty-yaml") (v "0.4.1") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0gk0icvgp9jbj0a2kcm3qcrbk37anyb9v242lxz20bny81mh0qk7")))

(define-public crate-rusty-yaml-0.4.2 (c (n "rusty-yaml") (v "0.4.2") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "16ipd79adg3igci0lnj3bs7cbnk9c68781gpzi4vlk1yzrhrkbxv")))

(define-public crate-rusty-yaml-0.4.3 (c (n "rusty-yaml") (v "0.4.3") (d (list (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "180hq0hxmjd1pbq1pf7qan71y16ywr9h2da458r8i7qxbxnndffi")))

