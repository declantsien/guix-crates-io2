(define-module (crates-io ru st rust-gmp) #:use-module (crates-io))

(define-public crate-rust-gmp-0.2.3 (c (n "rust-gmp") (v "0.2.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0lyvm3p9yycjyw21v44zbnd2wd534xlkv6by5kxb9nvxd3rrvqvf")))

(define-public crate-rust-gmp-0.2.4 (c (n "rust-gmp") (v "0.2.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0znphs86z5i9a7bnkzhsw466a4cjgn8ndnm7zbff9xk1yyz63gym")))

(define-public crate-rust-gmp-0.2.5 (c (n "rust-gmp") (v "0.2.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1cp8y3divxx5wzvd3d3xjgw3q7ah5ih6a596ddxh4nz9wncb1c4v")))

(define-public crate-rust-gmp-0.2.6 (c (n "rust-gmp") (v "0.2.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1yhiii6y6d38q8q1709pj0cr7dsb81sc8w77q6ifvnjm2pwkj1bs")))

(define-public crate-rust-gmp-0.2.7 (c (n "rust-gmp") (v "0.2.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "070z508xi2f0g6klg95i30zc5rss0xmndcdly4ryc6lrz2y0ia7d")))

(define-public crate-rust-gmp-0.2.8 (c (n "rust-gmp") (v "0.2.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0x7z4b2b8xhy5xghv2ihprnccsmhcsq6x1bliz0wy31f8shyj0ca")))

(define-public crate-rust-gmp-0.2.9 (c (n "rust-gmp") (v "0.2.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0n7vjy1ggigdb7ll0y9s43gwjcqz5bgf7bpxhccrc36c5xwz0jwj")))

(define-public crate-rust-gmp-0.2.10 (c (n "rust-gmp") (v "0.2.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0p65kxdx5qybl53k0kygi7w00aijacfg746lkbilmkbv9gx57scg")))

(define-public crate-rust-gmp-0.3.0 (c (n "rust-gmp") (v "0.3.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1kkcdwv17b6sbz95i6gs71w0vy3hkfbz7h7v1dj7hiwxcifxr6bq")))

(define-public crate-rust-gmp-0.3.1 (c (n "rust-gmp") (v "0.3.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "0xpmvffrgy2g0n1yhw49nwzsx3ck7s1jyzph9bllip0ijdkr9fx3")))

(define-public crate-rust-gmp-0.3.2 (c (n "rust-gmp") (v "0.3.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "16rfcr8dnbkfnxgsqs7awvzb76ma0f910r71d4zsf2dkfxrxbmsc")))

(define-public crate-rust-gmp-0.4.0 (c (n "rust-gmp") (v "0.4.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0bqivwkrdykrpkn1kqyi700l241fzdf3cqdv7lxf26a79smkv2wr")))

(define-public crate-rust-gmp-0.5.0 (c (n "rust-gmp") (v "0.5.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "01nrwlhpjhfib8bsw0zlgzbh7r9xjmbqa67ym6b0nwymk24z5pf3")))

