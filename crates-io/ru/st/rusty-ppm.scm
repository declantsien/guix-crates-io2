(define-module (crates-io ru st rusty-ppm) #:use-module (crates-io))

(define-public crate-rusty-ppm-0.1.0 (c (n "rusty-ppm") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)))) (h "0dxlzs094awjhghl0nssq88738ahdvip73yxsr834jnn752d8j9l")))

(define-public crate-rusty-ppm-0.1.1 (c (n "rusty-ppm") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)))) (h "0401qnzaasl7h3iqfrws0422w1xcvjqpg0d6x5rrq3sw24psc29m")))

(define-public crate-rusty-ppm-0.2.0 (c (n "rusty-ppm") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)))) (h "0w8pnayks3fvgdcz1nh85wy8biw3a1f8s4fkrjmapgb2k9vnbxqm")))

(define-public crate-rusty-ppm-0.3.0 (c (n "rusty-ppm") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "simple-canvas") (r "^0.1.0") (d #t) (k 0)))) (h "1jy5p7x0bkp4xfcjg4cnzapm40iag0bpngbshbbkby8kw4p9w6zn")))

