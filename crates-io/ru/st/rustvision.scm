(define-module (crates-io ru st rustvision) #:use-module (crates-io))

(define-public crate-rustvision-0.1.0 (c (n "rustvision") (v "0.1.0") (h "07700a4zgw3mpw77z9495cij1p6bmaba7fdl98ik711sz6nnkhn4")))

(define-public crate-rustvision-0.1.1 (c (n "rustvision") (v "0.1.1") (h "0dkgrj2c5n8j8j87lsjywx5rsjpa833wdf8f78m7krhjapk9haca")))

