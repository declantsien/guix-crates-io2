(define-module (crates-io ru st rust_hawktracer) #:use-module (crates-io))

(define-public crate-rust_hawktracer-0.1.0 (c (n "rust_hawktracer") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "01c36sv4jw22zz4w3cc3222sk13rpdswq80hplwwkaf8qv3lqwqs") (f (quote (("profiling_enabled") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer-0.1.1 (c (n "rust_hawktracer") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "13nvrw1m35d8525p7lkijf06svjasvkdrflpisdkqhmq1yaqwgyp") (f (quote (("profiling_enabled") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer-0.2.0 (c (n "rust_hawktracer") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11sl2ghavz3g7s409ndc1ys2csaiav45mjby02i7bcj1vk9gkxgw") (f (quote (("profiling_enabled") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer-0.3.0 (c (n "rust_hawktracer") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0r6ndc4gl1mlvqy58zm696c7dsayyqg66890rijwvad90xm34r8s") (f (quote (("profiling_enabled") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer-0.3.1 (c (n "rust_hawktracer") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "03zyhj62ff0g0m36w91niwx0vq8kl1rh4rbzx865gryy3yp3san6") (f (quote (("profiling_enabled") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer-0.3.2 (c (n "rust_hawktracer") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0kkiifaw85ddnyf677hga7zzk26k0c055xwm92q91c1nmga1jn40") (f (quote (("profiling_enabled") ("generate_bindings" "bindgen")))) (y #t) (l "hawktracer")))

(define-public crate-rust_hawktracer-0.4.0 (c (n "rust_hawktracer") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0sp1d65vkndbfimzrdqibk5ycda6z8415645gkpsz97xmgxfpnh1") (f (quote (("profiling_enabled") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer-0.5.0 (c (n "rust_hawktracer") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "rust_hawktracer_normal_macro") (r "^0.2") (d #t) (k 0)) (d (n "rust_hawktracer_proc_macro") (r "^0.2") (d #t) (k 0)))) (h "04v5y0hzp3nd4qbnxczn717qmiq7vx4djn2fmp2y8znhrywa53d3") (f (quote (("profiling_enabled" "rust_hawktracer_normal_macro/profiling_enabled" "rust_hawktracer_proc_macro/profiling_enabled") ("generate_bindings" "rust_hawktracer_normal_macro/generate_bindings"))))))

(define-public crate-rust_hawktracer-0.6.0 (c (n "rust_hawktracer") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "rust_hawktracer_normal_macro") (r "^0.3") (d #t) (k 0)) (d (n "rust_hawktracer_proc_macro") (r "^0.3") (d #t) (k 0)))) (h "18p1rp54w3l0wmalmmc4nnjf11jjf0s02rix3y9719yds47jywrw") (f (quote (("profiling_enabled" "rust_hawktracer_normal_macro/profiling_enabled" "rust_hawktracer_proc_macro/profiling_enabled") ("generate_bindings" "rust_hawktracer_normal_macro/generate_bindings"))))))

(define-public crate-rust_hawktracer-0.7.0 (c (n "rust_hawktracer") (v "0.7.0") (d (list (d (n "rust_hawktracer_normal_macro") (r "^0.4") (d #t) (k 0)) (d (n "rust_hawktracer_proc_macro") (r "^0.4") (d #t) (k 0)))) (h "1h9an3b73pmhhpzc2kk93nh93lplkvsffysj0rp6rxi7p4lhlj73") (f (quote (("profiling_enabled" "rust_hawktracer_normal_macro/profiling_enabled" "rust_hawktracer_proc_macro/profiling_enabled") ("pkg_config" "rust_hawktracer_normal_macro/pkg_config" "rust_hawktracer_proc_macro/pkg_config") ("generate_bindings" "rust_hawktracer_normal_macro/generate_bindings"))))))

