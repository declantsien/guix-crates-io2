(define-module (crates-io ru st rustls-cert-read) #:use-module (crates-io))

(define-public crate-rustls-cert-read-0.1.0 (c (n "rustls-cert-read") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)))) (h "1h7d1klwlcvbqd9yp49x1kzyjg8ipqzhsnjbafmjs8hgfbl2c9qh")))

(define-public crate-rustls-cert-read-0.2.0 (c (n "rustls-cert-read") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.3") (d #t) (k 0)))) (h "0l057ldymi5sc4v8cfayajps5sqzlab1nb959h8vznwx2nsmc1gw")))

(define-public crate-rustls-cert-read-0.3.0 (c (n "rustls-cert-read") (v "0.3.0") (d (list (d (n "rustls-pki-types") (r "^1.3") (d #t) (k 0)))) (h "1m8riyrawwy4hfl6zqmr15r2izvsxjckza468xf39qsdmv2yhinx")))

