(define-module (crates-io ru st rustea) #:use-module (crates-io))

(define-public crate-rustea-0.1.0 (c (n "rustea") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "17178ipx4i3i58lrc3lvxmkw8f3n0lhzfs02j1jv5bcgsiq0j7xi")))

(define-public crate-rustea-0.1.1 (c (n "rustea") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "1gsf731afqrzlqqbxhxf0x7pg680h2zld7r55r30ypd7a73zyavg")))

(define-public crate-rustea-0.1.2 (c (n "rustea") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "1djn7q2bamfjdw04jll621yas9mjng1krsqbr5n24h07pc94g9pv")))

(define-public crate-rustea-0.1.3 (c (n "rustea") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "12833lgzk2h445vjcf9wppfnijxkpxl0lp6jgsmi9k5rcb4drwsd")))

(define-public crate-rustea-0.1.4 (c (n "rustea") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "0w6zwpr6kchnrq1ks154jx55b8808crvw9yqyhsxzq7mz77m82wc")))

(define-public crate-rustea-0.1.5 (c (n "rustea") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "1d408bmpbxfbikzp9bh5748rnn625vdm9c2rylyw4m5gixmp5n66")))

(define-public crate-rustea-0.1.6 (c (n "rustea") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "16bqsn5h5nf2fqqlvfyk149vrdpkj1x8p2vizfyy05qjfy9igz36")))

