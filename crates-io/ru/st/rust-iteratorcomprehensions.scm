(define-module (crates-io ru st rust-iteratorcomprehensions) #:use-module (crates-io))

(define-public crate-rust-iteratorcomprehensions-0.3.1 (c (n "rust-iteratorcomprehensions") (v "0.3.1") (h "1wyd46j1w904j1bra0c01h2bahh9125xffdk5iq8w3f00ha0ca3z")))

(define-public crate-rust-iteratorcomprehensions-0.3.2 (c (n "rust-iteratorcomprehensions") (v "0.3.2") (h "02yqh7ch7bq3g4475lac4njj16p3x93yg4mwx3jsgcy9mbdnfj2a")))

(define-public crate-rust-iteratorcomprehensions-0.3.3 (c (n "rust-iteratorcomprehensions") (v "0.3.3") (h "1ggbb969lc68grpcdsa6ypi5lhxjwznkhxacswr1mg5hwrm9jny1")))

(define-public crate-rust-iteratorcomprehensions-0.3.4 (c (n "rust-iteratorcomprehensions") (v "0.3.4") (h "11qbgnn8b33zzs09pxznfyajwacg6jhxvkbfwif17vpz0bhkmd1v")))

