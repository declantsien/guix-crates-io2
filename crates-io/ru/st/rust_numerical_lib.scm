(define-module (crates-io ru st rust_numerical_lib) #:use-module (crates-io))

(define-public crate-rust_numerical_lib-1.0.0 (c (n "rust_numerical_lib") (v "1.0.0") (d (list (d (n "cargo-emit") (r "^0.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "cfg") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "rustc-cfg") (r "^0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16g71pl6wsabfidhd6zlk9bm6zrkl7g9xadh3jiy3c5klyi3zk0i") (l "main")))

