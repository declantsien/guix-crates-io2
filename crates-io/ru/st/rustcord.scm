(define-module (crates-io ru st rustcord) #:use-module (crates-io))

(define-public crate-rustcord-0.1.0 (c (n "rustcord") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "rustcord-sys") (r "^0.1.0") (d #t) (k 0)))) (h "16v6ci9mggv9hdwkvhkscjr4hz24yfgi5fmw09sc5qvn8lk8vvhq")))

(define-public crate-rustcord-0.2.0 (c (n "rustcord") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "rustcord-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1xf828pd0mjy2f4y0kfgwhfsmfgd7fs8hxwbs91xc0ifmyi8dvhq")))

(define-public crate-rustcord-0.2.1 (c (n "rustcord") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "rustcord-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1wz2is3qr0fw00qzw0p2nbp3f49dlgfvd89cbvgp87h284qwfhx0")))

(define-public crate-rustcord-0.2.2 (c (n "rustcord") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "rustcord-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0cvk53dgzlsc1w2i83nbw0gv14zrnmvhpbzsvcwlvfppi0pqm0yl")))

(define-public crate-rustcord-0.2.3 (c (n "rustcord") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rustcord-sys") (r "^0.2.4") (d #t) (k 0)))) (h "1v781lryjdvjb7gzw31iy6cxiwl30k0bq3iwb4a57rjk95a2ha7r")))

(define-public crate-rustcord-0.2.4 (c (n "rustcord") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "rustcord-sys") (r "^0.2.4") (d #t) (k 0)))) (h "02h5a80nkklvm1wy7f7pz2liyzqgqqw0n2y7yphrw47cgxna9d9h")))

