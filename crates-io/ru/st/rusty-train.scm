(define-module (crates-io ru st rusty-train) #:use-module (crates-io))

(define-public crate-rusty-train-0.1.0 (c (n "rusty-train") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf" "svg"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "gdk") (r "^0.14") (d #t) (k 0)) (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.14") (f (quote ("v3_20"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "navig18xx") (r "^0.1.0") (d #t) (k 0)))) (h "1zh722b59hq1bnxp01v20ff18nc1bq122zz891vy21zzrdx7939m")))

