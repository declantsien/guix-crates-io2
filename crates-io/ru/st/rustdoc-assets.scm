(define-module (crates-io ru st rustdoc-assets) #:use-module (crates-io))

(define-public crate-rustdoc-assets-0.1.0 (c (n "rustdoc-assets") (v "0.1.0") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "117qzh6pmv0bjgm2l1gx8vd1k4p6whj0b89f6sf74a0dy0n72psb")))

(define-public crate-rustdoc-assets-0.1.1 (c (n "rustdoc-assets") (v "0.1.1") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "1xpfrpppg6f863xldkg5cnwfbrn6b5qcjrvm19ixxish2sqks5rw")))

(define-public crate-rustdoc-assets-0.1.2 (c (n "rustdoc-assets") (v "0.1.2") (d (list (d (n "fs_extra") (r "^1.1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "0643xjyjfmna0ik8cpi8rla0nqslqv1xk4k7rlpvknkx03y1cnga")))

(define-public crate-rustdoc-assets-0.2.0 (c (n "rustdoc-assets") (v "0.2.0") (d (list (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)))) (h "1vh3lb8syhm18g5845cp78dkm8ln8300nrh0kvpbsis4zx11gcck")))

(define-public crate-rustdoc-assets-0.2.1 (c (n "rustdoc-assets") (v "0.2.1") (d (list (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)))) (h "11x2s6iync83rqfdkmc2xafnqrxpafsyjhfa4vj3r18gc4hkxj1f")))

