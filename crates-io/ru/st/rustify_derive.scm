(define-module (crates-io ru st rustify_derive) #:use-module (crates-io))

(define-public crate-rustify_derive-0.1.0 (c (n "rustify_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1maw4ijqmqky5f7nxxw8xc85jfbxa977m41zkq9wa6x9xr55phwm")))

(define-public crate-rustify_derive-0.2.0 (c (n "rustify_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0923rdha6mzpyzwz4h8a1sbqs2m9mhvb77gxbr7hlsd4vfwfj1hj")))

(define-public crate-rustify_derive-0.3.0 (c (n "rustify_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1ra889h8bww0wrl0k78a51j1qqxla1dvw51qr5f2h7g92mnpr6mw")))

(define-public crate-rustify_derive-0.3.1 (c (n "rustify_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0827sq54l922w0kabn2r6khg1h951xlk6qrsd3i38pnh0acs15xx")))

(define-public crate-rustify_derive-0.4.0 (c (n "rustify_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1brfmq928y8in348wlxxr939ki1mmz1gsi8pn0frak658dxyy2lq")))

(define-public crate-rustify_derive-0.4.1 (c (n "rustify_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "170hs2dnq7q8fx9dx5wphpb74kx5gcxf9fsr7cmxpcddwib9yksy")))

(define-public crate-rustify_derive-0.4.2 (c (n "rustify_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "01pdq4har8sylh8368gi4kg1sv594r01x4b4n8k0mgs99a6r68bc")))

(define-public crate-rustify_derive-0.4.3 (c (n "rustify_derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1nbl5mcbnhbdrqiq8453kh75nlqkayzfn0047rgbk7rrs0jy919a")))

(define-public crate-rustify_derive-0.5.0 (c (n "rustify_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0wpxrha9vbmj5z9lm438rpvm642zdli5p22jfgib15185g78v8g1")))

(define-public crate-rustify_derive-0.5.1 (c (n "rustify_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1jf9r587208rkm09yznymsid06i0w3sq07hfifvnbarc9y225mr5")))

(define-public crate-rustify_derive-0.5.2 (c (n "rustify_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1j1lf72n2w3vxjikik0iz0zg7fj17wm1ibgd9diz814cq4v5a4sq")))

(define-public crate-rustify_derive-0.5.3 (c (n "rustify_derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0zghb17gp1ypn3h552ddzbfmjy49iibvs9xp4y136m6sf8kg6ibk")))

