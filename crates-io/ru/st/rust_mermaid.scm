(define-module (crates-io ru st rust_mermaid) #:use-module (crates-io))

(define-public crate-rust_mermaid-0.1.0 (c (n "rust_mermaid") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "02qmnqhgpnr080h2pfnd17v87irzk2h3hgpl1zgxh1szl64ls261")))

(define-public crate-rust_mermaid-0.1.1 (c (n "rust_mermaid") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0amh627a14wx39m89gsibiywl46y28mza8pw7ankxf5avdvbc1hr")))

