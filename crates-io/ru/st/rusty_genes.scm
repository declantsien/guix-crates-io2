(define-module (crates-io ru st rusty_genes) #:use-module (crates-io))

(define-public crate-rusty_genes-0.1.0 (c (n "rusty_genes") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "object-pool") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1yzrx18i7wssk3kz360ai04fcshdkyapnbhnap79fidfih9x23v9")))

(define-public crate-rusty_genes-0.1.1 (c (n "rusty_genes") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "object-pool") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1ism0l4qgacwk73wnq6sa0cbvsvlzy941752wjimzp80fz5ybhal")))

(define-public crate-rusty_genes-0.1.2 (c (n "rusty_genes") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "object-pool") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "17qnipm5isj6wn6v02fy3kav71av4sw5f3ycvhlmwq6rqqbak0lx")))

(define-public crate-rusty_genes-0.1.3 (c (n "rusty_genes") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "object-pool") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "1y79qcasl9xj9hjp1crdpjvcqi6jw4620rz1nnb46szws7yjz37s")))

