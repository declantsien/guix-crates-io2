(define-module (crates-io ru st rust-monster) #:use-module (crates-io))

(define-public crate-rust-monster-0.1.0 (c (n "rust-monster") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "05b050yjq3s8q9zsxnass8qyx2gqhdpi984wi4pckrh20i25xr15")))

