(define-module (crates-io ru st rust_pro) #:use-module (crates-io))

(define-public crate-rust_pro-0.1.0 (c (n "rust_pro") (v "0.1.0") (h "1g954jf3nsv3rya2vjxv6qi9x0ffqc0jmqypli0c7ldsdsdbyab0")))

(define-public crate-rust_pro-0.1.1 (c (n "rust_pro") (v "0.1.1") (h "1wacfdzs5qpfdhmw84xv9xqdjq65ab8fq4v5bgqgyg1hvzk8z90w")))

