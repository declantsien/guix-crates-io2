(define-module (crates-io ru st rustpy) #:use-module (crates-io))

(define-public crate-rustpy-0.0.0 (c (n "rustpy") (v "0.0.0") (d (list (d (n "pyo3") (r "^0.18") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.18") (d #t) (k 1)))) (h "0fxyq14091sgylinxk65p9miwjxrs67jq4r7yx3qn3smmi093b8n")))

(define-public crate-rustpy-0.1.0 (c (n "rustpy") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.18.1") (d #t) (k 1)))) (h "1z2znvnx9mkxbr02z00hsjp7bvn0rp47db634jcdga3sm14v7sia")))

(define-public crate-rustpy-0.2.0 (c (n "rustpy") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.18.1") (d #t) (k 1)))) (h "01461xx7yfzv87gj28vlpkn54vb1zsk0fg3zi0p7pjwsnbk52zc1")))

(define-public crate-rustpy-0.3.0 (c (n "rustpy") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.18.1") (d #t) (k 1)))) (h "1ljrycnclgn0d9vn5004yqimzwjz9rj5cskb3qrbm5p9bqm3v147")))

