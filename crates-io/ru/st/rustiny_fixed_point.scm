(define-module (crates-io ru st rustiny_fixed_point) #:use-module (crates-io))

(define-public crate-rustiny_fixed_point-0.0.1 (c (n "rustiny_fixed_point") (v "0.0.1") (d (list (d (n "rustiny_number") (r "^0.0.1") (d #t) (k 0)))) (h "1acjy3gb0kffvdp0560mrf161974xxs2sqbj8gmfqwss29hfcvg1")))

(define-public crate-rustiny_fixed_point-0.0.2 (c (n "rustiny_fixed_point") (v "0.0.2") (d (list (d (n "rustiny_number") (r "^0.0.2") (d #t) (k 0)))) (h "12l7s0g0nmiaxn4wxbkhxyy33fg8ga9il4vadyi8ljp1apxlzriz")))

(define-public crate-rustiny_fixed_point-0.1.0 (c (n "rustiny_fixed_point") (v "0.1.0") (d (list (d (n "rustiny_number") (r "^0.1.0") (d #t) (k 0)))) (h "0pybgsl0a34bakvaff4z5iayfgma9rapkwry1czlpf7cxvx62n32")))

(define-public crate-rustiny_fixed_point-0.1.1 (c (n "rustiny_fixed_point") (v "0.1.1") (d (list (d (n "rustiny_number") (r "^0.1.1") (d #t) (k 0)))) (h "17xk75fzd2p1m48l0lp28xar86s7vs8x9xhxwgjfvzgwrbh511dr")))

(define-public crate-rustiny_fixed_point-0.1.2 (c (n "rustiny_fixed_point") (v "0.1.2") (d (list (d (n "rustiny_number") (r "^0.1.1") (d #t) (k 0)))) (h "06p9hyqgd0ri9bp4n86wa4jw8s7sw9s7x61vfk6lz6bianp9bmwv")))

(define-public crate-rustiny_fixed_point-0.1.3 (c (n "rustiny_fixed_point") (v "0.1.3") (d (list (d (n "rustiny_number") (r "^0.1.2") (d #t) (k 0)))) (h "1dwqnzqsmapnq15b1z5dshh89q0kk5ss721f2v4qiqlaa6zmw1ra")))

(define-public crate-rustiny_fixed_point-0.1.4 (c (n "rustiny_fixed_point") (v "0.1.4") (d (list (d (n "rustiny_number") (r "^0.1.3") (d #t) (k 0)))) (h "1ys4cncz8mny067kq5jzpa4s6yk4nc6wahjqx9dzg0aj4piz0vcz")))

(define-public crate-rustiny_fixed_point-0.1.5 (c (n "rustiny_fixed_point") (v "0.1.5") (d (list (d (n "rustiny_number") (r "^0.1.4") (d #t) (k 0)))) (h "04ifagnagbxvh00ifsjdy5szlsinjck5j37fmw86g7nkm69mfdqv")))

(define-public crate-rustiny_fixed_point-0.1.6 (c (n "rustiny_fixed_point") (v "0.1.6") (d (list (d (n "rustiny_number") (r "^0.1.5") (d #t) (k 0)))) (h "013hpc1ix9vjjz4s8213cfrvz08kz8lpai9iz3di0s3kapq4f8ab")))

(define-public crate-rustiny_fixed_point-0.1.7 (c (n "rustiny_fixed_point") (v "0.1.7") (d (list (d (n "rustiny_number") (r "^0.1.6") (d #t) (k 0)))) (h "13rpifdvk7zqwvr6wr51myrn7dk9b7f4qzaxkw3s01iin2gz7l5f")))

(define-public crate-rustiny_fixed_point-0.1.8 (c (n "rustiny_fixed_point") (v "0.1.8") (d (list (d (n "rustiny_number") (r "^0.1.6") (d #t) (k 0)))) (h "1m8gcfadilvazaisnndm3rvv1rxzp46xxqvg0d5dzngyyhwgv2vb")))

(define-public crate-rustiny_fixed_point-0.1.9 (c (n "rustiny_fixed_point") (v "0.1.9") (d (list (d (n "rustiny_number") (r "^0.1.9") (d #t) (k 0)))) (h "13a4zwa18qmciprsp6qlf3cw7zyzx8ljcs8c5d8gji2fmchysqnq")))

(define-public crate-rustiny_fixed_point-0.1.10 (c (n "rustiny_fixed_point") (v "0.1.10") (d (list (d (n "rustiny_number") (r "^0.1.10") (d #t) (k 0)))) (h "0fgjcjcafg54ky0wfn87c9cbs0pr8j0wjv29wm8d8ggcjxqs7afr")))

(define-public crate-rustiny_fixed_point-0.1.11 (c (n "rustiny_fixed_point") (v "0.1.11") (d (list (d (n "rustiny_number") (r "^0.1.10") (d #t) (k 0)))) (h "160hwx4bmn6b7gkpzhgpkv8gqz2gx9sml62biblg6hvmi00gjgm6")))

(define-public crate-rustiny_fixed_point-0.2.0 (c (n "rustiny_fixed_point") (v "0.2.0") (d (list (d (n "rustiny_number") (r "^0.2.0") (d #t) (k 0)))) (h "1yq9h33nacwbhifabpmjriqdis600ds4460kan38dw9z52pgksyd")))

