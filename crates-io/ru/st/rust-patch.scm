(define-module (crates-io ru st rust-patch) #:use-module (crates-io))

(define-public crate-rust-patch-0.1.0 (c (n "rust-patch") (v "0.1.0") (d (list (d (n "rust-patch-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0nix300vr86yw964zcrpw6b9jyijyxjpl8pg2av7dgany82kc96h")))

(define-public crate-rust-patch-0.1.1 (c (n "rust-patch") (v "0.1.1") (d (list (d (n "rust-patch-derive") (r "=0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0rrw0y0h73zlj18s8fm5q3ycy54kwfzd19spgwwfh44rz921qc7l")))

(define-public crate-rust-patch-0.1.2 (c (n "rust-patch") (v "0.1.2") (d (list (d (n "rust-patch-derive") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "163yq2iq8cspqwn3jvnhmqd1flzbxb66chxmrl16s0ygkqhvlpi6")))

(define-public crate-rust-patch-0.1.3 (c (n "rust-patch") (v "0.1.3") (d (list (d (n "rust-patch-derive") (r "=0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0hzvhxh1va9c6mpihvx7b7mswsjgwrkcjig2s4vhsippbmzq6xj0")))

