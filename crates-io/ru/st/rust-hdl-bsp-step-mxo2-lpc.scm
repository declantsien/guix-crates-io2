(define-module (crates-io ru st rust-hdl-bsp-step-mxo2-lpc) #:use-module (crates-io))

(define-public crate-rust-hdl-bsp-step-mxo2-lpc-0.1.0 (c (n "rust-hdl-bsp-step-mxo2-lpc") (v "0.1.0") (d (list (d (n "rust-hdl") (r "^0.46.0") (f (quote ("fpga"))) (d #t) (k 0)))) (h "1vhxid181ixskvbq1yms7vimbp1va61l7kaafsafsrhrlcyhkcs4") (y #t)))

(define-public crate-rust-hdl-bsp-step-mxo2-lpc-0.1.1 (c (n "rust-hdl-bsp-step-mxo2-lpc") (v "0.1.1") (d (list (d (n "rust-hdl") (r "^0.46.0") (f (quote ("fpga"))) (d #t) (k 0)))) (h "1zm35fahnn73yl1vf6s1y6ndk813wjvrf9pvj45dc19617ybhp8s")))

(define-public crate-rust-hdl-bsp-step-mxo2-lpc-0.1.2 (c (n "rust-hdl-bsp-step-mxo2-lpc") (v "0.1.2") (d (list (d (n "rust-hdl") (r "^0.46.0") (f (quote ("fpga"))) (d #t) (k 0)))) (h "0kvskizs59lr7y43az09pgvkxpcn53jr94fhyfv15ynqldzbgkyd")))

