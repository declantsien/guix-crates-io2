(define-module (crates-io ru st rustpostal) #:use-module (crates-io))

(define-public crate-rustpostal-0.1.0 (c (n "rustpostal") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)))) (h "0zsp59dpd3a7s0qs9qkvw0bdx9pib3226paj10wm2dz169xhj45b")))

(define-public crate-rustpostal-0.1.1 (c (n "rustpostal") (v "0.1.1") (d (list (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)))) (h "1rqdpgqbydaa2x1r4bchkk23cczc7a4m2227pyj4dvdqysl1nw5n")))

(define-public crate-rustpostal-0.2.0 (c (n "rustpostal") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0kb2dmn3jymp1d4w42dkriyh83rm93f84zpc987l3xwg48ghnzk9")))

(define-public crate-rustpostal-0.3.0 (c (n "rustpostal") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "18lld3vva029h2i721f7bil19irx14whzy7y9n687dqfpiq6v1jc")))

