(define-module (crates-io ru st rustic_boards) #:use-module (crates-io))

(define-public crate-rustic_boards-0.1.0 (c (n "rustic_boards") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "inquire") (r "^0.6") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dgn3r260157c31nmbisankqmsysgsx4s87wbfnd9m2m66dwfv43")))

(define-public crate-rustic_boards-0.1.1 (c (n "rustic_boards") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "inquire") (r "^0.6") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kpn93wajz3n29nwy7g1qp7b6ai7w6dkmz4nvaxgk4waism54b5l")))

(define-public crate-rustic_boards-0.1.2 (c (n "rustic_boards") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "inquire") (r "^0.6") (f (quote ("date" "editor"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1scf5k0r5856ga76bdccwsd8azhygmaljb308ylrbcbn2dgxpcw6")))

