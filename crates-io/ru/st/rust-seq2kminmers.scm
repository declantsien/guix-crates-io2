(define-module (crates-io ru st rust-seq2kminmers) #:use-module (crates-io))

(define-public crate-rust-seq2kminmers-0.1.0 (c (n "rust-seq2kminmers") (v "0.1.0") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "nthash") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p3rcx8l8x6asvcrg1psgyydlbbm5r4rix69a0mx9fhixgc0jadq")))

