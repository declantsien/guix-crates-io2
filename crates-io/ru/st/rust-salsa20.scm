(define-module (crates-io ru st rust-salsa20) #:use-module (crates-io))

(define-public crate-rust-salsa20-0.1.0 (c (n "rust-salsa20") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1d2cnr88a5zkqjm5mfgj5rj3kimpvmp3j96z54y26357i7dm334x")))

(define-public crate-rust-salsa20-0.1.1 (c (n "rust-salsa20") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0za7vxmxhqcqx2axx8qs5pdmg8z4mi9wpx5fhlp7p8pq3f8xl9xf")))

(define-public crate-rust-salsa20-0.1.2 (c (n "rust-salsa20") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0dvhggcmas5cypqsissy4zvzd58pp2slp4l9ygsbhrcin01f312i")))

(define-public crate-rust-salsa20-0.2.0 (c (n "rust-salsa20") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1nc99c4f3mnfbbi6bhrb21gr4636ssls01qbrgqyhcjprypbkv16")))

(define-public crate-rust-salsa20-0.2.1 (c (n "rust-salsa20") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "167faxiwq57ljb7vd56rn4l51qlhyn8yq1s52qik9djw2xs9lbk2")))

(define-public crate-rust-salsa20-0.2.2 (c (n "rust-salsa20") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1naxld4dh351rg1p6di6ry0zn06rhdjj2aq0jpp8df1ljdfpm93z")))

(define-public crate-rust-salsa20-0.3.0 (c (n "rust-salsa20") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "02f55f3hrchfzipqgcr7sa19gr85gp6363d4nlifmq5sscxprf45")))

