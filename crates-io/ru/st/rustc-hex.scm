(define-module (crates-io ru st rustc-hex) #:use-module (crates-io))

(define-public crate-rustc-hex-1.0.0 (c (n "rustc-hex") (v "1.0.0") (h "07pff94vqc1mhrqp9i06xzayiad4xfx7588zkqsdw875lpkqrsqc")))

(define-public crate-rustc-hex-2.0.0 (c (n "rustc-hex") (v "2.0.0") (h "1khavsadw6kmcs2w394dxkgd69zvfz2p0mbq62h0ffc1qa035c6j") (f (quote (("std") ("default" "std"))))))

(define-public crate-rustc-hex-2.0.1 (c (n "rustc-hex") (v "2.0.1") (h "1f2sax4lb4883rx4sm9xil1187nc18ky30jzba108yhhhsib6fs0") (f (quote (("std") ("default" "std"))))))

(define-public crate-rustc-hex-2.1.0 (c (n "rustc-hex") (v "2.1.0") (h "1mkjy2vbn5kzg67wgngwddlk4snmd8mkjkql2dzrzzfh6ajzcx9y") (f (quote (("std") ("default" "std"))))))

