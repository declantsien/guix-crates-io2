(define-module (crates-io ru st rustutils-sync) #:use-module (crates-io))

(define-public crate-rustutils-sync-0.1.0 (c (n "rustutils-sync") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "1qxcn7fv5vgfchdm66gsryyzcj1rq3y6whrxsx3n6j562zf9l3kp")))

