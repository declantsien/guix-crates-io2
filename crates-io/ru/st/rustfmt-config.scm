(define-module (crates-io ru st rustfmt-config) #:use-module (crates-io))

(define-public crate-rustfmt-config-0.4.0 (c (n "rustfmt-config") (v "0.4.0") (d (list (d (n "rustc-ap-syntax") (r "^29.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0qw6kk6ffwzgm6h3hl27m4z2fxilapql129rpnl8b61rfz0ibc9b")))

