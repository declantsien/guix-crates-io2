(define-module (crates-io ru st rustup-available-packages) #:use-module (crates-io))

(define-public crate-rustup-available-packages-0.1.0 (c (n "rustup-available-packages") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.11") (d #t) (k 0)) (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "17nplha9584wp6y8hyaxjgbyqz664bcm3blbvqwc1395kxz1944s")))

