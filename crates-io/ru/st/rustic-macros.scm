(define-module (crates-io ru st rustic-macros) #:use-module (crates-io))

(define-public crate-rustic-macros-0.0.1 (c (n "rustic-macros") (v "0.0.1") (d (list (d (n "ic-cdk") (r "^0.9.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nq87zaik2q9alzjp7krxrba3zh313iy81kkmdcbb8l126adgibh") (r "1.65.0")))

(define-public crate-rustic-macros-0.1.0 (c (n "rustic-macros") (v "0.1.0") (d (list (d (n "ic-cdk") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0vxcmwmk9yasq4hwl6rzw9g8hsiibvsxx19sbvcl5b5lqw58spd8") (r "1.65.0")))

(define-public crate-rustic-macros-0.1.1 (c (n "rustic-macros") (v "0.1.1") (d (list (d (n "ic-cdk") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1b7hh7x0agk4gjqszvi7inn0d01lp8ly07d8k4jqqi1g3a47qi3v") (r "1.65.0")))

(define-public crate-rustic-macros-0.1.2 (c (n "rustic-macros") (v "0.1.2") (d (list (d (n "ic-cdk") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mr4dh2y1y15nqzxnrlyfnwzcd9zi09bd3zz4by5kgrqk3xc4pa4") (r "1.65.0")))

(define-public crate-rustic-macros-0.1.3 (c (n "rustic-macros") (v "0.1.3") (d (list (d (n "ic-cdk") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1vd9bgsw5jc58p9w8kbl8631ikh1ji04glz69xl6jy1rnz9ipp47") (r "1.65.0")))

(define-public crate-rustic-macros-0.1.4 (c (n "rustic-macros") (v "0.1.4") (d (list (d (n "ic-cdk") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ay11bjyp1cmff6v9zly4va0aksmi4skg48k7bg79fp88kvmkyig") (r "1.65.0")))

(define-public crate-rustic-macros-0.1.5 (c (n "rustic-macros") (v "0.1.5") (d (list (d (n "ic-cdk") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0viybc1x7fnv2kjasm358g1vya40q2xg5sf4vlaakyymzi35y4ry") (r "1.65.0")))

(define-public crate-rustic-macros-0.1.6 (c (n "rustic-macros") (v "0.1.6") (d (list (d (n "ic-cdk") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qa4v8p7ahzilmc40l0nmgwnxjwkqgfvyw90fzpzd9hr4asw938p") (r "1.65.0")))

