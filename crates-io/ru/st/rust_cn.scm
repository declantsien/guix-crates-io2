(define-module (crates-io ru st rust_cn) #:use-module (crates-io))

(define-public crate-rust_cn-0.1.2 (c (n "rust_cn") (v "0.1.2") (h "0rmpbl1axhj5x3bvm4axzzfd16cwrpnlhhl8zzz3bj38a48l3wi4")))

(define-public crate-rust_cn-0.1.3 (c (n "rust_cn") (v "0.1.3") (h "0yisx750qkzmxvd5ny27spfb6qf848bl84nd1nsqkr9psjjz3y6z")))

(define-public crate-rust_cn-0.1.5 (c (n "rust_cn") (v "0.1.5") (h "1226sb8jypan14ix8njk5ccd7ygnw5cf5r7v5lparlj81yhj38gp")))

(define-public crate-rust_cn-0.1.6 (c (n "rust_cn") (v "0.1.6") (h "1cj4xigjr2f94al4289rk0i1favmcpykz770liyvhagkynlk4k8a")))

