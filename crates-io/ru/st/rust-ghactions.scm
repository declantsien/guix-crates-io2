(define-module (crates-io ru st rust-ghactions) #:use-module (crates-io))

(define-public crate-rust-ghactions-0.1.0 (c (n "rust-ghactions") (v "0.1.0") (h "06nizswf55pxalc4p4gib4v9gdv29a01vw8izr7k8yp8wsx1gcim") (y #t)))

(define-public crate-rust-ghactions-0.1.1 (c (n "rust-ghactions") (v "0.1.1") (h "0dxb4k8br9ml75y7yv74dwaj3a00vqg4p7d23qy1wiwisdj9a1kg") (y #t)))

(define-public crate-rust-ghactions-0.1.2 (c (n "rust-ghactions") (v "0.1.2") (h "0hwc6v3q7dwhl7zcx6x93jg5qhc4f8a2l8ih87bvvn11xgn2b432") (y #t)))

(define-public crate-rust-ghactions-0.1.3 (c (n "rust-ghactions") (v "0.1.3") (h "0vq8bkvsy6kxadzhxybg6gvzx5iba0ky8j65qwm4izkr75yb88al") (y #t)))

(define-public crate-rust-ghactions-0.1.4 (c (n "rust-ghactions") (v "0.1.4") (h "01cwzyiskl20aqq58ba0m35h3c1sqvpvc01vjc74b3djrahf6vhn") (y #t)))

(define-public crate-rust-ghactions-0.1.5 (c (n "rust-ghactions") (v "0.1.5") (h "05dlbqdr99lqc0364pmyksb4nycabgn8xxwz1rpi6vgm45rx994x") (y #t)))

