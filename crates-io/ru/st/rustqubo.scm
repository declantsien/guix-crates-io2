(define-module (crates-io ru st rustqubo) #:use-module (crates-io))

(define-public crate-rustqubo-0.1.0 (c (n "rustqubo") (v "0.1.0") (d (list (d (n "annealers") (r "^0.1.0") (d #t) (k 0)) (d (n "classical_solver") (r "^0.1.0") (d #t) (k 0)) (d (n "katex-doc") (r "^0.1.0") (d #t) (k 2)) (d (n "pyo3") (r "^0.17") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "10r1x0rbdz72vzhxyjsh0ywynqc836lxhr220a3kf9k2yh9r1k8c") (f (quote (("python" "pyo3") ("default" "python"))))))

