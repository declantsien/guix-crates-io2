(define-module (crates-io ru st rustutils-uname) #:use-module (crates-io))

(define-public crate-rustutils-uname-0.1.0 (c (n "rustutils-uname") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "rustutils-runnable") (r "^0.1.0") (d #t) (k 0)))) (h "1r295kkl92zizd48n9s05gz01sl6dm9qf9a1wcb7m02h28avf1b2")))

