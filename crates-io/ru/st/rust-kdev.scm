(define-module (crates-io ru st rust-kdev) #:use-module (crates-io))

(define-public crate-rust-kdev-0.0.0 (c (n "rust-kdev") (v "0.0.0") (h "0gd76cws99pgryi2k3kfqnc1r3vhlikz48h35pm0wm2kj1xv7f4x") (y #t)))

(define-public crate-rust-kdev-0.0.1 (c (n "rust-kdev") (v "0.0.1") (h "0g4g6hx9mgi3xxdbhyb78zyn1q1hmvc8yf6mn01mg5a943sai3rh") (y #t)))

