(define-module (crates-io ru st rust-autojump) #:use-module (crates-io))

(define-public crate-rust-autojump-0.1.0 (c (n "rust-autojump") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.9") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "1m70rx6qy3csyy299k0cimf0klgb90lc029lfn3f96f74icd8mxq")))

(define-public crate-rust-autojump-0.1.1 (c (n "rust-autojump") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.9") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "0kw0qksy574nwqyf23p80108mcbrawh7wijsmk5z3kgawn5zy7lq")))

(define-public crate-rust-autojump-0.1.2 (c (n "rust-autojump") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.9") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "0s1k0qhcci70039spk6fxcxh6l6l47g4r8i8h089ajpjnk197fs9")))

(define-public crate-rust-autojump-0.1.3 (c (n "rust-autojump") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.9") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "078xiazpf3slyfmsdpsys9k4383883awyhx1q7bka7yh8906f0a1")))

