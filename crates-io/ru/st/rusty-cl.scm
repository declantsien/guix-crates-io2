(define-module (crates-io ru st rusty-cl) #:use-module (crates-io))

(define-public crate-rusty-cl-0.1.0 (c (n "rusty-cl") (v "0.1.0") (h "0rl83ymvjqwb84fk4mwzla8cfzj9dfvn71yvzmcs18cbzmqjhl38") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.1 (c (n "rusty-cl") (v "0.1.1") (h "1h4n9ivg7pl27r59v728nay10p0yn7s2fxmal6is3h4j91iw52i9") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.2 (c (n "rusty-cl") (v "0.1.2") (h "1n9gnmd85nn1p43skvnpvl0fivk5qxlaqal1kgpvs29kdx4lcfs5") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.3 (c (n "rusty-cl") (v "0.1.3") (h "0fvrvh58mblp0my7vkws2l8yq70fmkv2l6524hrq2642b84y9gsp") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.4 (c (n "rusty-cl") (v "0.1.4") (h "058sb6cmsvzgima4754iys6shd8m3nnx2xdngr1pwx2m5lcbfp1w") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.5 (c (n "rusty-cl") (v "0.1.5") (h "0d5lm7w1di2yr4a1bldx9375kfpgcdwjhwn03jvzxnfiwklr15vg") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.6 (c (n "rusty-cl") (v "0.1.6") (h "0s0mpy78jf4sfyy4l0ld5qyrhd9n3h3062xm0b186c0vd20dd071") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.7 (c (n "rusty-cl") (v "0.1.7") (h "1hapqkv6r99fc2r5bq1zpiy7az6fjsx9lna9nga91vhgrz79454p") (l "OpenCL")))

(define-public crate-rusty-cl-0.1.8 (c (n "rusty-cl") (v "0.1.8") (h "0nnv47q0kjzbkkivsijmz3raf2mdq84nd9sks2jb7rnk3cpnp9n2") (l "OpenCL")))

