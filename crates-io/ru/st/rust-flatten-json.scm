(define-module (crates-io ru st rust-flatten-json) #:use-module (crates-io))

(define-public crate-rust-flatten-json-0.1.0 (c (n "rust-flatten-json") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jw9h1iapmcb7n5cyj917ci11vji4l4pwrwcsvv1z4410z3n43bx")))

(define-public crate-rust-flatten-json-0.2.0 (c (n "rust-flatten-json") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wp967s3aqfzm1kybkqnmajhsfa8h4mlph3df0xb6sivsyq1jjr6")))

