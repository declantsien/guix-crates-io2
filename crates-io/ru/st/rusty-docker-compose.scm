(define-module (crates-io ru st rusty-docker-compose) #:use-module (crates-io))

(define-public crate-rusty-docker-compose-0.1.0 (c (n "rusty-docker-compose") (v "0.1.0") (h "0xf83q2yj5pmkchs72rsjbq5r72ah5x9a1vii4y70di5hjw18i1k")))

(define-public crate-rusty-docker-compose-0.2.0 (c (n "rusty-docker-compose") (v "0.2.0") (h "17vrhkw1d78pmi0xnk7ams9s2j6y902jnnj339mmawa2ynpr8a9a")))

(define-public crate-rusty-docker-compose-0.2.1 (c (n "rusty-docker-compose") (v "0.2.1") (h "0i631wpswx0gq9ffmh1i1ypfnhjsq0j5g0ri3vcs7rwyb9n9qn9n")))

(define-public crate-rusty-docker-compose-0.2.2 (c (n "rusty-docker-compose") (v "0.2.2") (h "0i7j1j3l0sisxgs92vphji90inx5yr7zfxncw8v84gg8b0wcfbv2")))

(define-public crate-rusty-docker-compose-0.3.0 (c (n "rusty-docker-compose") (v "0.3.0") (h "1fg23zx0z7ws2vxp99rdx64kpal2fjca223qs89k91synkl5s1nv")))

