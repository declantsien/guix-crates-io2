(define-module (crates-io ru st rust_hls_macro) #:use-module (crates-io))

(define-public crate-rust_hls_macro-0.2.0 (c (n "rust_hls_macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.45.1") (d #t) (k 0)) (d (n "rust_hls_macro_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kkq37nixg7r5y3c0k3m790pqkhvsbzgygsnrylnhbjhrshd2xjk")))

