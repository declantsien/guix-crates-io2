(define-module (crates-io ru st rusti-cal) #:use-module (crates-io))

(define-public crate-rusti-cal-0.1.0 (c (n "rusti-cal") (v "0.1.0") (h "1lkl38jx5dnx21i4d3p6rm439pjkr91x7x1dy6d4p6ph1nzyp9wn")))

(define-public crate-rusti-cal-0.2.0 (c (n "rusti-cal") (v "0.2.0") (h "08wa3iqlnm6krahpky2c8clp75rr2vxfwrwswxb6vjqmrynz5il9")))

(define-public crate-rusti-cal-0.3.0 (c (n "rusti-cal") (v "0.3.0") (h "1csfk37h3gcwxfbsx2b90gdixm2p79zchml70zixrqszmxys6izh")))

(define-public crate-rusti-cal-0.4.0 (c (n "rusti-cal") (v "0.4.0") (h "0hs5k75334h988jdhsf348cpjpwnf650c6w2mwl5xv96p0mp34kc")))

(define-public crate-rusti-cal-0.4.1 (c (n "rusti-cal") (v "0.4.1") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)))) (h "0d5xp42kfr92awr257ir8dwhpw9625xq11fwmnx1xlghh2zryi6n")))

(define-public crate-rusti-cal-0.4.2 (c (n "rusti-cal") (v "0.4.2") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)))) (h "1wkwh90bryw6cwyvjyiadxsj4bicmq8lbrfim6m1gp1dk6w09w47")))

(define-public crate-rusti-cal-0.4.3 (c (n "rusti-cal") (v "0.4.3") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "pure-rust-locales") (r "^0.5.6") (d #t) (k 0)))) (h "0bfcq5jrriprbfbmm9s3c3my2ymcikcq2kqqh2cyka43s4j7mj0d")))

(define-public crate-rusti-cal-0.4.4 (c (n "rusti-cal") (v "0.4.4") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "pure-rust-locales") (r "^0.5.6") (d #t) (k 0)))) (h "0wzhj9z98d5k68khna122gk17rrvfrwiqfdmc8q07n80fkiy292h")))

(define-public crate-rusti-cal-0.4.5 (c (n "rusti-cal") (v "0.4.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "pure-rust-locales") (r "^0.5.6") (d #t) (k 0)))) (h "06012cx5c473j1zd76pqz1a9spxz00iykf1cincwbxbd0gn5r6bz")))

(define-public crate-rusti-cal-1.0.0 (c (n "rusti-cal") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "pure-rust-locales") (r "^0.5.6") (d #t) (k 0)))) (h "1y6qxw35sb2nvc6rjsc2dwxi26j02x5k6yy8ky4cdwrjl1v99zx4")))

(define-public crate-rusti-cal-1.0.1 (c (n "rusti-cal") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "pure-rust-locales") (r "^0.5.6") (d #t) (k 0)))) (h "1infk2v3abfhckvzqgrbjiwqilrbdyd1crm7fw2gsh74bqv8ndks")))

(define-public crate-rusti-cal-1.1.0 (c (n "rusti-cal") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "locale_config") (r "^0.3.0") (d #t) (k 0)) (d (n "pure-rust-locales") (r "^0.5.6") (d #t) (k 0)))) (h "0ggah7z0n9m0p5ayz9wdifqcir2gg8890axifkv6jbgq940ks1a6")))

