(define-module (crates-io ru st rusty-rain) #:use-module (crates-io))

(define-public crate-rusty-rain-0.1.0 (c (n "rusty-rain") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "1r96xk5k96i927zahv6cw9881wgjdbgsl4dp5jdwhrlg2ww43hv8")))

(define-public crate-rusty-rain-0.2.0 (c (n "rusty-rain") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0iagrvibrg8xaf0lg94ycifmddsjvz9bzabafrn4hb5ngi74vib7")))

(define-public crate-rusty-rain-0.2.1 (c (n "rusty-rain") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0h5fmgb7yvypxf3qrjppj73gc4xm3j8vlrpyilg8jhzyvfp6w592")))

(define-public crate-rusty-rain-0.3.0 (c (n "rusty-rain") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ezemoji") (r "^0.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "1jp20q80wraby0aj38n1wrwq4ykwnlcm6hg9ixs3si4j9fwyy05r")))

(define-public crate-rusty-rain-0.3.1 (c (n "rusty-rain") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ezemoji") (r "^0.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0yja1dlds821ivn6gxw7lg2zabm1mgvqd6v3849jdkxk4gnnkk55")))

(define-public crate-rusty-rain-0.3.2 (c (n "rusty-rain") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ezemoji") (r "^0.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "064c4a8c1f0cal1fs2ki5v44lzam95dgvzyi0c5xdrps40lsfrlg")))

(define-public crate-rusty-rain-0.3.3 (c (n "rusty-rain") (v "0.3.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ezemoji") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "1zy9n7d7496zxhrbhxwmzj5zpnryrjj8x44mx9mz1lrisij2f2h1")))

(define-public crate-rusty-rain-0.3.4 (c (n "rusty-rain") (v "0.3.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ezemoji") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0bl4h3vxfzp3wj1lpfszxajf2019psn0m1dbxh8w28zn8kynfw2q")))

(define-public crate-rusty-rain-0.3.5 (c (n "rusty-rain") (v "0.3.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ezemoji") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0c29fbfawfbhzkzv2s1m65ff4569x7z7ymm1xlv2b8mzrczv2myv")))

(define-public crate-rusty-rain-0.3.6 (c (n "rusty-rain") (v "0.3.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ezemoji") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "0arnvicb4n8mb2gjmyw5cbzfgzh4yxkyjy0hzzbqnp3dsnr7bqsz")))

