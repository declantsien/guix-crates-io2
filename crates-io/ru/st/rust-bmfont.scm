(define-module (crates-io ru st rust-bmfont) #:use-module (crates-io))

(define-public crate-rust-bmfont-0.1.0 (c (n "rust-bmfont") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1xhyjzbrq98hycnny4wmw8hg0yf620q272w0g1r7nxjr8pp82sbi")))

(define-public crate-rust-bmfont-0.1.1 (c (n "rust-bmfont") (v "0.1.1") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1jpy0p020i34crl8q25l6cpi5zhr52b59kr36x00d63gjjxnassl")))

(define-public crate-rust-bmfont-0.1.2 (c (n "rust-bmfont") (v "0.1.2") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0kqiwb7vpc155k9i06w2xyw2xrrrsbsmssgfgm8zaj6ylpm2jqcb")))

(define-public crate-rust-bmfont-0.1.3 (c (n "rust-bmfont") (v "0.1.3") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0fc25xsqa2jz1bbwgaab96ficxqscci0gknwjv9y833sl7kg9mdi")))

(define-public crate-rust-bmfont-0.1.4 (c (n "rust-bmfont") (v "0.1.4") (d (list (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0wimd6586dg012dzj8pyqxc950rzh2w7nzzw8nyfpair6yg4c5q5")))

