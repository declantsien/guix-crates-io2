(define-module (crates-io ru st rustmex_matlab700) #:use-module (crates-io))

(define-public crate-rustmex_matlab700-0.1.0 (c (n "rustmex_matlab700") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.1") (d #t) (k 0)))) (h "168vxpp8fffs4shp5kjnz33mbm6nccc39qsz35h5nl4yxg5ssl6b") (l "mex700")))

(define-public crate-rustmex_matlab700-0.1.2 (c (n "rustmex_matlab700") (v "0.1.2") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustmex_core") (r "^0.2") (d #t) (k 0)))) (h "18p1r34lj6kd45n4s7qz06fvk6r3dzrqahlakl0k7r5smdvcp8sn") (l "mex700")))

