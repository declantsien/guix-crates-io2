(define-module (crates-io ru st rust-padbuster) #:use-module (crates-io))

(define-public crate-rust-padbuster-0.1.0 (c (n "rust-padbuster") (v "0.1.0") (d (list (d (n "aes") (r "^0.4.0") (d #t) (k 2)) (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "block-modes") (r "^0.5.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 2)))) (h "085bdr613imwa0a8bfbq9w3fckbcgspj2s6r6143g4cy3y7x9yyd")))

(define-public crate-rust-padbuster-0.1.1 (c (n "rust-padbuster") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 2)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.9.1") (d #t) (k 2)) (d (n "cbc") (r "^0.1.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 2)))) (h "0gjc6l92l71vdp20vfcyxrczy2707s0pqf91zd1f6mk4k2h4f38x")))

(define-public crate-rust-padbuster-0.1.2 (c (n "rust-padbuster") (v "0.1.2") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 2)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.9.1") (d #t) (k 2)) (d (n "cbc") (r "^0.1.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 2)))) (h "1fqbppwzj6iik67q3i1wmh1bwic0bypafa16sa777vg0s9qc1p0s")))

