(define-module (crates-io ru st rusty_themy) #:use-module (crates-io))

(define-public crate-rusty_themy-0.1.0 (c (n "rusty_themy") (v "0.1.0") (d (list (d (n "cssparser") (r "^0.29.6") (o #t) (d #t) (k 0)) (d (n "dark-light") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "lightningcss") (r "^1.0.0-alpha.40") (f (quote ("visitor"))) (o #t) (d #t) (k 0)))) (h "1h2mby4nlyybzhlkqlb9kxs40m2l67si65d0rfk44ymfqhswmprc") (f (quote (("gtk" "lightningcss" "cssparser" "dark-light") ("default" "gtk"))))))

