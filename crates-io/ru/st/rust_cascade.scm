(define-module (crates-io ru st rust_cascade) #:use-module (crates-io))

(define-public crate-rust_cascade-0.1.0 (c (n "rust_cascade") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1129rdyymwz2wlayjp0ipawfvvqrliy41k25w05jnb6cs0vlbcl0")))

(define-public crate-rust_cascade-0.1.1 (c (n "rust_cascade") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "073zmd8120cbw56wk84nkv7w9frbgw66pk3ax08sbdbxr91gs8qh")))

(define-public crate-rust_cascade-0.1.2 (c (n "rust_cascade") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "1j3mrxjwm8dlwqmb4a7h1ycrpagdr39vwjjp9ijdqc9hm6fymg8y")))

(define-public crate-rust_cascade-0.1.3 (c (n "rust_cascade") (v "0.1.3") (d (list (d (n "bitvec") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "0hyzwikc5xsn883ygdqwqzf8f5l5z3z3kckkgdm929dldimadjbk")))

(define-public crate-rust_cascade-0.2.0 (c (n "rust_cascade") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "192zrbjc8rgpm8qcj1nwg2n3qq3m86qkrvm8k55162vrwrcs572c")))

(define-public crate-rust_cascade-0.3.0 (c (n "rust_cascade") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.10.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "06wj7midz54zpcip3ra46prcq9mb19yw8x40691i8as19fca54cr")))

(define-public crate-rust_cascade-0.3.2 (c (n "rust_cascade") (v "0.3.2") (d (list (d (n "bitvec") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "0mz74plmd2y0kf4gz31pi9i7g19ibqw963v8af6z2gakm1y5ndsp")))

(define-public crate-rust_cascade-0.3.3 (c (n "rust_cascade") (v "0.3.3") (d (list (d (n "bit-vec") (r "^0.5.1") (d #t) (k 0)) (d (n "bit_reverse") (r "^0.1.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "1hm5q47w6a0yp9v1l7qcd3k5lk80wk79d7lz6dwckxxwpk92220r")))

(define-public crate-rust_cascade-0.3.4 (c (n "rust_cascade") (v "0.3.4") (d (list (d (n "bit-vec") (r "^0.5.1") (d #t) (k 0)) (d (n "bit_reverse") (r "^0.1.7") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "0fng7cs5g25an6ii9cn51rmiwwfhgml48pji3b91mawdsc04kzpk")))

(define-public crate-rust_cascade-0.4.0 (c (n "rust_cascade") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)))) (h "1a5jgblywzrxl9flir9a9j6x6sfing5ay2q5mlwdgqcl1508a23l")))

(define-public crate-rust_cascade-0.5.0 (c (n "rust_cascade") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)))) (h "1585jgd8krsifby8l53qh2lsjx17ppzz7xq8097d2avg3jdrk9kf")))

(define-public crate-rust_cascade-0.6.0 (c (n "rust_cascade") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0kc57nfjpq4rqrjmsdgkhf86d0vzk0x2gpfs4d4mx62viyx9nnws")))

(define-public crate-rust_cascade-1.0.0 (c (n "rust_cascade") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1i7i7cp38jzknqxz5179z40jbhx9ndfhicp6jsgm8arjn5qqwvcr")))

(define-public crate-rust_cascade-1.1.0 (c (n "rust_cascade") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "malloc_size_of_derive") (r "^0.1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "wr_malloc_size_of") (r "^0.1") (d #t) (k 0)))) (h "1d7zkzvxs5nyh5zrv42yik456fkg58isi6kqwnvwk0majc2lyjjw") (y #t)))

(define-public crate-rust_cascade-1.2.0 (c (n "rust_cascade") (v "1.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0kvcsqv1aqbzzga23k5dx91mj77slzzss1ykcydbf7hg66lig76h")))

(define-public crate-rust_cascade-1.3.0 (c (n "rust_cascade") (v "1.3.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1ajkc2bjvjf9sw4bms7bryc6h8zk8dyvywrsc2mlhhgrv51zy64y")))

(define-public crate-rust_cascade-1.4.0 (c (n "rust_cascade") (v "1.4.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0df3barl26xks8rxs52k4c739cv7ac112p8yxgqhfrhcqdb8897g") (f (quote (("builder"))))))

(define-public crate-rust_cascade-1.5.0 (c (n "rust_cascade") (v "1.5.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "09z13zalaz3bhn51gb8aqcqdqz6ixv8ahp7l8vvkcvdnw5crj904") (f (quote (("builder"))))))

