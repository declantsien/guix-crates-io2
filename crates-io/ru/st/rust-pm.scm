(define-module (crates-io ru st rust-pm) #:use-module (crates-io))

(define-public crate-rust-pm-0.0.1 (c (n "rust-pm") (v "0.0.1") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "1d0wbp904i8j2xp6z9jq7lqhfygwgxpvrjvksg8vsivcbdycwx6n")))

(define-public crate-rust-pm-0.0.2 (c (n "rust-pm") (v "0.0.2") (d (list (d (n "iron") (r "*") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)) (d (n "toml") (r "*") (d #t) (k 0)))) (h "1c3lddjbsp7qynra78gh4wz7cavvfxrqdpi6fg5s8cm7jdzspj34")))

