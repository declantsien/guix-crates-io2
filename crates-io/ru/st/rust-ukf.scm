(define-module (crates-io ru st rust-ukf) #:use-module (crates-io))

(define-public crate-rust-ukf-0.1.0 (c (n "rust-ukf") (v "0.1.0") (d (list (d (n "nalgebra") (r ">=0.30.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 2)) (d (n "ode_solvers") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "statrs") (r "^0.15.0") (d #t) (k 2)))) (h "0jb9310xxcsa048ydjjx0g3r8d27c0rmkg33847zxzkrx9p9vx40")))

