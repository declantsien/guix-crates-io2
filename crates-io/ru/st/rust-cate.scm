(define-module (crates-io ru st rust-cate) #:use-module (crates-io))

(define-public crate-rust-cate-0.1.0 (c (n "rust-cate") (v "0.1.0") (d (list (d (n "clap") (r "^2.13") (d #t) (k 0)))) (h "0rcx9cbwqqbiwbjj4qkcb0ppiyv9mbfxryhjdwm55ysv31krlhxy")))

(define-public crate-rust-cate-0.1.1 (c (n "rust-cate") (v "0.1.1") (d (list (d (n "clap") (r "^2.13") (d #t) (k 0)))) (h "1qpx5mhvck9483zw9fwj4iiffp965bgdhpknm5sq98ckgkyv56nw")))

(define-public crate-rust-cate-0.1.2 (c (n "rust-cate") (v "0.1.2") (d (list (d (n "clap") (r "^2.13") (d #t) (k 0)))) (h "1r7rk0wcx5csnv2f21k8fyfm3gzrnx2rw6i9w16c4l96rcpnwvqb")))

(define-public crate-rust-cate-0.1.3 (c (n "rust-cate") (v "0.1.3") (d (list (d (n "clap") (r "^2.13") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "016f8fxck7rr2k6ajhankih26k0dn5lb5nydhc5xzmns34szdqql")))

