(define-module (crates-io ru st rusty_v8_protryon) #:use-module (crates-io))

(define-public crate-rusty_v8_protryon-0.3.10 (c (n "rusty_v8_protryon") (v "0.3.10") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "08x3i4xmjjifcv355i86h5dbw2zjd2wv5sf05ximgcwhbb03pkjc")))

(define-public crate-rusty_v8_protryon-3.10.1 (c (n "rusty_v8_protryon") (v "3.10.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "0dkx7cjz5n4q6gfg8n83083ks30mpkian2allz4gcp01w0a0ia6v")))

(define-public crate-rusty_v8_protryon-3.10.2 (c (n "rusty_v8_protryon") (v "3.10.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "0y9npvgy2ip1ig1nblhk3d6vi7mrpzv9k1364vk65mmals817il6")))

(define-public crate-rusty_v8_protryon-3.10.3 (c (n "rusty_v8_protryon") (v "3.10.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "1m525mdcx6wk92vq9ahlf4js4179p36y7spxaprx2hwhxykr3x6b")))

(define-public crate-rusty_v8_protryon-3.10.4 (c (n "rusty_v8_protryon") (v "3.10.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cargo_gn") (r "^0.0.15") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)) (d (n "which") (r "^3.1.1") (d #t) (k 1)))) (h "0af04andh7lwkxxcj2sipm98ahn983nvvh1yx2y4zfqhfc49l3m7")))

