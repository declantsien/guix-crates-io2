(define-module (crates-io ru st rustic-jsonrpc) #:use-module (crates-io))

(define-public crate-rustic-jsonrpc-0.1.0 (c (n "rustic-jsonrpc") (v "0.1.0") (d (list (d (n "rustic-jsonrpc-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0gm986zcnryc7ap9xiclj54il0f6864bm8bglihdj4bqp6zg1y6q")))

(define-public crate-rustic-jsonrpc-0.1.1 (c (n "rustic-jsonrpc") (v "0.1.1") (d (list (d (n "rustic-jsonrpc-macro") (r "=0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "04959l112vkfqs6cznygz4v2v9fklb09b1ikvps7ia99vn48m9lz")))

(define-public crate-rustic-jsonrpc-0.1.2 (c (n "rustic-jsonrpc") (v "0.1.2") (d (list (d (n "rustic-jsonrpc-macro") (r "=0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1i3chv62zd6lg7h9598hwfs1szihzqddp7kx77kidj56jqfv5h5g")))

