(define-module (crates-io ru st rustysynth) #:use-module (crates-io))

(define-public crate-rustysynth-0.1.0 (c (n "rustysynth") (v "0.1.0") (h "1zlb3k75ryj1sd1m0hnmxd5m7h3jhmxasm8cdi9immcmsjbqd7ph")))

(define-public crate-rustysynth-0.9.0 (c (n "rustysynth") (v "0.9.0") (h "1dgnlh00vafwickcgk5kdd7m8hjp41w3dyhcdg30ffmjrqh6g42s")))

(define-public crate-rustysynth-0.9.1 (c (n "rustysynth") (v "0.9.1") (h "0m2sksbyjklqplis8b18vly86j2wdgn628mnpff11j0c6ihvi88q")))

(define-public crate-rustysynth-0.9.2 (c (n "rustysynth") (v "0.9.2") (h "1b2kvgpv0jkljrywymalqbk1smwzh0kmyr1x8q1b2lws5lqr921x")))

(define-public crate-rustysynth-1.0.0 (c (n "rustysynth") (v "1.0.0") (h "1a74vvnr40j8018higzpk68sbsnqnjyisf0sd4k4z9nsdb53blzd")))

(define-public crate-rustysynth-1.1.0 (c (n "rustysynth") (v "1.1.0") (h "0h4ab2yc3klj3gc08pn0nyk744wpf04r23bjf8dlyhphgpsyjwbp")))

(define-public crate-rustysynth-1.1.1 (c (n "rustysynth") (v "1.1.1") (h "1k5f5sqpi8anrq3npccvhwydr1dxfy9k806zx48xpwj5964g10vi")))

(define-public crate-rustysynth-1.1.2 (c (n "rustysynth") (v "1.1.2") (h "0kjgzblfxvf85d2ll97zynq3740rpwn8dkw4v6yv1b8vkz8cpf66")))

(define-public crate-rustysynth-1.2.0 (c (n "rustysynth") (v "1.2.0") (h "1zhhri62b7lm8fng8d3djwqmw5cvk0lcv2ygdaz40chzkisq8dx3")))

(define-public crate-rustysynth-1.2.1 (c (n "rustysynth") (v "1.2.1") (h "0sy6q62ffjgf0ymqrwafgqy2zwcyby2wqq97r4abl7fx9cn4j5mr")))

(define-public crate-rustysynth-1.3.0 (c (n "rustysynth") (v "1.3.0") (h "0pamipq943ahi0kq77awamrp8ck9ykl6v8h3p5nj11p9qll00lwh")))

(define-public crate-rustysynth-1.3.1 (c (n "rustysynth") (v "1.3.1") (h "1d8rlwsx4amqlfa2wismndl2ipxdb8kwja8z6d3zgr3m06wnycd2")))

