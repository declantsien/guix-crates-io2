(define-module (crates-io ru st rusty_pipes) #:use-module (crates-io))

(define-public crate-rusty_pipes-0.0.1 (c (n "rusty_pipes") (v "0.0.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0apk913dg44146xh5kxkj440r4lrvgw6l0396yka3rgvyxrx2fvb")))

