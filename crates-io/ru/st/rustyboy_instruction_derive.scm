(define-module (crates-io ru st rustyboy_instruction_derive) #:use-module (crates-io))

(define-public crate-rustyboy_instruction_derive-0.1.0 (c (n "rustyboy_instruction_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cdh07814w6dcyclmv862sp85vzswb7hi4s1mn6cpzi8sqas8ivw")))

