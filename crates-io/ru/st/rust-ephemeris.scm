(define-module (crates-io ru st rust-ephemeris) #:use-module (crates-io))

(define-public crate-rust-ephemeris-0.1.0 (c (n "rust-ephemeris") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "155snzrx4s379rgn2yh9n4w98gh6k71wilh1z0cs6mach5mjn2yk")))

(define-public crate-rust-ephemeris-0.1.0-aplpha (c (n "rust-ephemeris") (v "0.1.0-aplpha") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0zx13gwq0y5qqp7am7g2pz2jxivgvjg4p89yjgfcz632f69mr145")))

(define-public crate-rust-ephemeris-0.1.0-beta (c (n "rust-ephemeris") (v "0.1.0-beta") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1h1cwx7cm9ghv8qyfi8mq475y53f370jv20ydjny1px4fvb04p7y")))

