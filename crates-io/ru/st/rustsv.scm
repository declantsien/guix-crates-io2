(define-module (crates-io ru st rustsv) #:use-module (crates-io))

(define-public crate-rustsv-0.1.0 (c (n "rustsv") (v "0.1.0") (h "0w1dn8x21sm98r559spmhjxq6jib29mikhc2rk0dh43a6pqnrbr9") (f (quote (("std") ("nostd") ("default" "nostd" "std"))))))

(define-public crate-rustsv-0.1.1 (c (n "rustsv") (v "0.1.1") (h "0v0cx4ldqb5h7vk7zg01xbf4y8zq74f7lgdmq73xiq4a17x4vakb") (f (quote (("std") ("nostd") ("default" "nostd" "std"))))))

(define-public crate-rustsv-0.1.2 (c (n "rustsv") (v "0.1.2") (h "0cqgkl840rf73n0cm2nxgn0sqnzll6dfsi8i3grf4c5lx5m7aacq") (f (quote (("std") ("nostd") ("default" "nostd" "std"))))))

(define-public crate-rustsv-0.1.3 (c (n "rustsv") (v "0.1.3") (h "1yr38wp9vbd2vg49i3728nm6n8ij1vcngiv5ryi00vq1f4hg7h02") (f (quote (("std") ("nostd") ("default" "nostd" "std"))))))

(define-public crate-rustsv-0.1.4 (c (n "rustsv") (v "0.1.4") (h "0wapfpc6pjrw5r2qxhc26p6q33dyxzb61af5fgx5ir4myv5xba98") (f (quote (("std") ("nostd") ("default" "nostd" "std"))))))

(define-public crate-rustsv-0.1.5 (c (n "rustsv") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0rhs0f907zvdsbymmdrcc2ha5qvm0sl6kmxkg9yld48rfvfpidln") (f (quote (("http" "reqwest/blocking") ("default"))))))

