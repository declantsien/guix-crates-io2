(define-module (crates-io ru st rustc-cfg) #:use-module (crates-io))

(define-public crate-rustc-cfg-0.1.0 (c (n "rustc-cfg") (v "0.1.0") (h "0y782vhfdv7khcyhfr82igvfgqcb8krs04wazcarq5h21w84iipm")))

(define-public crate-rustc-cfg-0.1.1 (c (n "rustc-cfg") (v "0.1.1") (h "1axg2pps3wwfw5p8rx43ddzizph7x1ksdwpb3s3f1sqgkgvm8gl1")))

(define-public crate-rustc-cfg-0.1.2 (c (n "rustc-cfg") (v "0.1.2") (h "1barq8c6y5bvaqljxwbw2f6zv3pdi545nwalgcbmz581g232j6c2")))

(define-public crate-rustc-cfg-0.2.0 (c (n "rustc-cfg") (v "0.2.0") (h "05p5f8h446lfixpmy8ax3wmxq6p348i2gk638kkc9k02spz7v4rp")))

(define-public crate-rustc-cfg-0.3.0 (c (n "rustc-cfg") (v "0.3.0") (h "0147w6g894h8nxia8yhlvr9a2qplyw9ayc4ssmcy1xcbf6srd9an")))

(define-public crate-rustc-cfg-0.4.0 (c (n "rustc-cfg") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "0vqvjb04zzfp14dwvvzs7ps47qvq25xibk2xfgw394yhgkz23lla")))

(define-public crate-rustc-cfg-0.5.0 (c (n "rustc-cfg") (v "0.5.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fk3g0dqg8yyz6g7hb95vvfl6c0iqvqrgawam2jh700y8ig7mpwx")))

