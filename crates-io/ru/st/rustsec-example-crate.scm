(define-module (crates-io ru st rustsec-example-crate) #:use-module (crates-io))

(define-public crate-rustsec-example-crate-0.0.0 (c (n "rustsec-example-crate") (v "0.0.0") (h "18z2w92vdm3zjdc0aqf2b37hvw4wxxkj4qmmh5f21p7d0zz0k3yx") (y #t)))

(define-public crate-rustsec-example-crate-1.0.0 (c (n "rustsec-example-crate") (v "1.0.0") (h "03nqsip161rv9nyhyhac68fhh631rqr6dwasc81nhd9paf3lil52")))

(define-public crate-rustsec-example-crate-0.0.1 (c (n "rustsec-example-crate") (v "0.0.1") (h "0x9fyljjb6clnd4gl77sdhfz0y78zv604xkma6l5r2xnky7jzv62")))

