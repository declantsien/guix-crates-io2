(define-module (crates-io ru st rust-ipmi) #:use-module (crates-io))

(define-public crate-rust-ipmi-0.1.0 (c (n "rust-ipmi") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "1mzkx0wly5pyvkfsqns4222w66clq054xs1xkak7smrg9c65as03")))

(define-public crate-rust-ipmi-0.1.1 (c (n "rust-ipmi") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "14pkljpvrfn04sz5jcbc3p634np2v9lc2kcv5rlf87kbffb9hz0q")))

