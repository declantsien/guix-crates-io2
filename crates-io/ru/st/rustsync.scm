(define-module (crates-io ru st rustsync) #:use-module (crates-io))

(define-public crate-rustsync-0.2.0 (c (n "rustsync") (v "0.2.0") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1ailvx5bnsv63z6ljv2pfxr56i063g42xw9ds9i2n1g574fw194f")))

(define-public crate-rustsync-0.2.1 (c (n "rustsync") (v "0.2.1") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1n7wr01z724jxb1k7f8267lqyrybg27mijrq0afsd9lyw1lni06p")))

(define-public crate-rustsync-0.2.2 (c (n "rustsync") (v "0.2.2") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "125gw6dzrgi4cwmfypqq3p98zmczwq5q1pfr5pacwi829d3hdqwy")))

(define-public crate-rustsync-0.2.3 (c (n "rustsync") (v "0.2.3") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0rqzargnvn6jmlzbfm02akjqcicr3mvcl6jy5wlw3kjq548nms82")))

