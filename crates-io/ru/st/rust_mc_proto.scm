(define-module (crates-io ru st rust_mc_proto) #:use-module (crates-io))

(define-public crate-rust_mc_proto-0.1.0 (c (n "rust_mc_proto") (v "0.1.0") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "varint-rs") (r "^2.2.0") (d #t) (k 0)))) (h "15mdjvia1fzgnix8vdb6r7kr515y7sz1ak9h9h5ipk2m87bdhg2v")))

(define-public crate-rust_mc_proto-0.1.1 (c (n "rust_mc_proto") (v "0.1.1") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "varint-rs") (r "^2.2.0") (d #t) (k 0)))) (h "1i19sq51alh569liqzn6hbs8zpc7qphk1b5xaw14qzwjnwywb85z")))

(define-public crate-rust_mc_proto-0.1.2 (c (n "rust_mc_proto") (v "0.1.2") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "varint-rs") (r "^2.2.0") (d #t) (k 0)))) (h "19rhj4vnmdk5nq4r5lxhg5akg883wmyi32if3hl9x7q2my3psb2d")))

(define-public crate-rust_mc_proto-0.1.3 (c (n "rust_mc_proto") (v "0.1.3") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "varint-rs") (r "^2.2.0") (d #t) (k 0)))) (h "0imldlng1vgnrn2hd401nx2l3v7i23fwkfjfa0mxbfb0sm1899ia")))

(define-public crate-rust_mc_proto-0.1.4 (c (n "rust_mc_proto") (v "0.1.4") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)))) (h "0vs171gzk3cpsixscwq7i5d1226wh8nimcc1gq13bq9mi8qa656a")))

(define-public crate-rust_mc_proto-0.1.5 (c (n "rust_mc_proto") (v "0.1.5") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)))) (h "1pdafs09rrni6x54ws3372dxq10r8p2drrxnarimgk9gy5klbjip")))

(define-public crate-rust_mc_proto-0.1.6 (c (n "rust_mc_proto") (v "0.1.6") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "0lrkj6mxrh2lb96irph13jaiwfg6rcimdqjrws69didhniyy3pxq")))

(define-public crate-rust_mc_proto-0.1.7 (c (n "rust_mc_proto") (v "0.1.7") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (f (quote ("zlib"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "1nbgjl9v73ivnq72v50kk7bcz9n62mm06rmhhn9s5hqmq1y7bjnz")))

(define-public crate-rust_mc_proto-0.1.8 (c (n "rust_mc_proto") (v "0.1.8") (d (list (d (n "bytebuffer") (r "^2.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.30") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "0w6m59w278w5im9cby6phcvjm7fqj4zl0wnan908q0dpr8k6k8bi")))

