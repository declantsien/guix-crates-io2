(define-module (crates-io ru st rustz) #:use-module (crates-io))

(define-public crate-rustz-0.1.0 (c (n "rustz") (v "0.1.0") (h "0hgzgh2lszf3mb0pry6ma02rw0b5chzsklkdfr464vdb6c0wiwn7")))

(define-public crate-rustz-0.1.1 (c (n "rustz") (v "0.1.1") (h "1d4wqfz0ydvibs6h2d1gnhwj4axbpijcw2aipy8y860yvsm9xahc")))

(define-public crate-rustz-0.1.2 (c (n "rustz") (v "0.1.2") (h "0fi8kzkjxchd5dns18gw6p6whqi27kz9xvhk74brzxyrshf4bf0f")))

(define-public crate-rustz-0.1.3 (c (n "rustz") (v "0.1.3") (h "099hrv01w4lz1nxz222gn15by8lmr0yq3r8nqkn2sshm8kq2w56d")))

(define-public crate-rustz-0.1.4 (c (n "rustz") (v "0.1.4") (h "047kjvcrn27h268qiz9n1981y5ganq85splvycjknfi9l1xlva0q")))

