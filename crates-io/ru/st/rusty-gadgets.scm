(define-module (crates-io ru st rusty-gadgets) #:use-module (crates-io))

(define-public crate-rusty-gadgets-0.1.0 (c (n "rusty-gadgets") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "18xf4acry3lihpr08pqgahrihq2k7w8p954bhx1bilk1y2fmymxn")))

