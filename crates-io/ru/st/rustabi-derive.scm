(define-module (crates-io ru st rustabi-derive) #:use-module (crates-io))

(define-public crate-rustabi-derive-6.0.2 (c (n "rustabi-derive") (v "6.0.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rustabi") (r "^6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "186zs39qbw0267dlmjs1xyh5dgwfrd9dpp68j4hya8kn96jm8din")))

