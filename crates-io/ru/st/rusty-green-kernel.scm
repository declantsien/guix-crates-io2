(define-module (crates-io ru st rusty-green-kernel) #:use-module (crates-io))

(define-public crate-rusty-green-kernel-0.1.0 (c (n "rusty-green-kernel") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rusty-kernel-tools") (r "^0.1") (d #t) (k 0)))) (h "1d4iyqamwi7n9qjgd419ws7b2wjdvpfpk4h90mm8z53mpmnrmvf1")))

(define-public crate-rusty-green-kernel-0.1.1 (c (n "rusty-green-kernel") (v "0.1.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.14") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rusty-kernel-tools") (r "^0.1") (d #t) (k 0)))) (h "00bqcza7nwxx0rzhva0sfnb5vmx6p0v6di1jd3c9r3qsikhxsfrh")))

(define-public crate-rusty-green-kernel-0.2.0 (c (n "rusty-green-kernel") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "1.*") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1bjmsf55sygjl859357nmc8k2radwap1bd71yvgsr8r5r5ap4pfy")))

