(define-module (crates-io ru st rusto) #:use-module (crates-io))

(define-public crate-rusto-0.0.1 (c (n "rusto") (v "0.0.1") (d (list (d (n "codegen") (r "0.2.*") (d #t) (k 0)) (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "17a0rig1jihhzxm6i519smxdhb0nyfzsp490k0d5q04216jr7qdc")))

