(define-module (crates-io ru st rustysakari) #:use-module (crates-io))

(define-public crate-rustysakari-0.1.0 (c (n "rustysakari") (v "0.1.0") (h "1y61q73s6m17aab287d6w51i78f1ij9db4yrxz0l23mx3q2dz3f2")))

(define-public crate-rustysakari-0.1.1 (c (n "rustysakari") (v "0.1.1") (h "0pxxnc5an0zmjivnf2ngvcimmwb60r52j7aqy2973d65gsijfghv")))

