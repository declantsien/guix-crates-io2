(define-module (crates-io ru st rustc_version_runtime) #:use-module (crates-io))

(define-public crate-rustc_version_runtime-0.1.0 (c (n "rustc_version_runtime") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.2") (d #t) (k 1)) (d (n "semver") (r "^0.1") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 1)))) (h "0779rnii5nhvq7vdkzi0cw9493061ih64p8qhb96avsyfb5bzxm0")))

(define-public crate-rustc_version_runtime-0.1.3 (c (n "rustc_version_runtime") (v "0.1.3") (d (list (d (n "rustc_version") (r "^0.1.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.3") (d #t) (k 1)) (d (n "semver") (r "^0.1") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 1)))) (h "1lpv1vlwlb0g1bbalywa45znnsb1iibg5xxr3w39rjka0is1v0ch")))

(define-public crate-rustc_version_runtime-0.1.4 (c (n "rustc_version_runtime") (v "0.1.4") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "1qgr5jlks7n0qxmqq3yfqdq3bw5k9y3c7ygyk4rfkygsq0x69lly")))

(define-public crate-rustc_version_runtime-0.1.5 (c (n "rustc_version_runtime") (v "0.1.5") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "0i8ii76a8yj2g5lw1p34wihqq5x3ql7f3dk9dwq1ywypzbbyrs3d")))

(define-public crate-rustc_version_runtime-0.2.0 (c (n "rustc_version_runtime") (v "0.2.0") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "1rfyz0y25wsykwrs440nl2jwcgya9a1s0jxm99gb01sw2mf16xj0")))

(define-public crate-rustc_version_runtime-0.2.1 (c (n "rustc_version_runtime") (v "0.2.1") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "0bsq9419ax5c61jzc1fmz969qx0fq2qfann6j6zligqf4x9p26yk")))

(define-public crate-rustc_version_runtime-0.3.0 (c (n "rustc_version_runtime") (v "0.3.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "0787mz3zqkh7fmb88pxhag63y3qxlps58pmdnvq0m0p1pb98rl9d")))

