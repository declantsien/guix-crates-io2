(define-module (crates-io ru st rustamath_mnmz) #:use-module (crates-io))

(define-public crate-rustamath_mnmz-0.1.0 (c (n "rustamath_mnmz") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)))) (h "1g31syqxmsr2qh21rwa4vz86aaqrkwr4dncdby2xdnpqgb7gz6sf")))

(define-public crate-rustamath_mnmz-0.1.1 (c (n "rustamath_mnmz") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1") (d #t) (k 0)))) (h "1p9nwbh6c53v84pvxkrwszxy0i1kda0m2jx6jwx49mp6sz4a35ry")))

