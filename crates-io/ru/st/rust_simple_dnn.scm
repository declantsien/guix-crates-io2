(define-module (crates-io ru st rust_simple_dnn) #:use-module (crates-io))

(define-public crate-Rust_Simple_DNN-0.1.0 (c (n "Rust_Simple_DNN") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07c1yvvpjs9v93ai0zwrmvnxzwym0xwq8j18xvyzwr079j5gb0qp")))

(define-public crate-Rust_Simple_DNN-0.1.1 (c (n "Rust_Simple_DNN") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18fzlvg03z29ci0i62lji0vzan2pcdxps5z9dk64aap0i5pawbi7")))

(define-public crate-Rust_Simple_DNN-0.1.2 (c (n "Rust_Simple_DNN") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1999cj4hcmsy670qqgkpzs3z87q1rxhdxmm16q6l4i8h29x7sm7p")))

(define-public crate-Rust_Simple_DNN-0.1.3 (c (n "Rust_Simple_DNN") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0da5vs3q3fhbd931l4alxp0lcxp8b27f94r9c7a7lkb373vgfpj9")))

(define-public crate-Rust_Simple_DNN-0.1.4 (c (n "Rust_Simple_DNN") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0aikvhqvv0dsw6rh709d2d1x3cbhp15h0h4bnjasjqnalb9pyn1p")))

(define-public crate-Rust_Simple_DNN-0.1.5 (c (n "Rust_Simple_DNN") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dv3a5xwn87x0cfnxfhzdjd7ih3z3nxz58qin24kcqyy7h197s65")))

