(define-module (crates-io ru st rust_abf) #:use-module (crates-io))

(define-public crate-rust_abf-0.1.0 (c (n "rust_abf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1c020y41lbhn2r8by8963lih3qjxkr88ny9g6bd6b6fy81b7z2ky")))

(define-public crate-rust_abf-0.1.1 (c (n "rust_abf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0pfsfvg1jpzryb2mq4hz2da5yh5z3dk17swg61ny8zcb18j47kzs")))

(define-public crate-rust_abf-0.1.2 (c (n "rust_abf") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "03fg0h4cf4zlans3qvs93l4ia87bxcmsvrwbj928vlbzf7zp40a9")))

(define-public crate-rust_abf-0.1.3 (c (n "rust_abf") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "10al4p1yp2hscqgsc3nn26mc6s0cz0dqi56sm18jpyrzy40r1ryn")))

(define-public crate-rust_abf-0.1.4 (c (n "rust_abf") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0mqdh2xqh2nxcaf9nsmm3d36r7n1j7gjgsi25bajkd7dxdlpn1wp")))

(define-public crate-rust_abf-0.2.0 (c (n "rust_abf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0s2z8gc38081v10m4g09hgpyxyx63j2mbn6jm73vqx9f4m4zl8yp")))

(define-public crate-rust_abf-0.3.0 (c (n "rust_abf") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0mwdr7k7wbym94i383842apmgkv3d3xhrv99bw1wyzw70gd9j560")))

(define-public crate-rust_abf-0.4.0 (c (n "rust_abf") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1445ww8bi8dwjprfqb2cxzy7qbziyj92pjv3v502y1gzkm3mdgyf")))

(define-public crate-rust_abf-0.4.1 (c (n "rust_abf") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "163rwsyvzycgk39habz917wnqgwlmrwfn9wa3vck7iw4nwm9l5s4")))

(define-public crate-rust_abf-0.4.2 (c (n "rust_abf") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)))) (h "0jqk8g5mn3i893c86029jqh36xlr6n4ln3z73izbwrz65ysm8q3x")))

