(define-module (crates-io ru st rustup_prompt_helper) #:use-module (crates-io))

(define-public crate-rustup_prompt_helper-1.0.0 (c (n "rustup_prompt_helper") (v "1.0.0") (d (list (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0mifxa0kz1d5c7zsh8cvnkslva6skylsv7hy0g4igxz1mr78isys")))

(define-public crate-rustup_prompt_helper-1.0.1 (c (n "rustup_prompt_helper") (v "1.0.1") (d (list (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "082rgbclm9yifsg9v8nmx94qj45m8izlhh985v9l8gjv3khbx8ir")))

(define-public crate-rustup_prompt_helper-1.1.0 (c (n "rustup_prompt_helper") (v "1.1.0") (d (list (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "01k3hqzd57i7n987hpw8gjdk3lkc356ry47llkh7wmzi0ff3jczd")))

