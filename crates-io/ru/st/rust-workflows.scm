(define-module (crates-io ru st rust-workflows) #:use-module (crates-io))

(define-public crate-rust-workflows-0.2.1 (c (n "rust-workflows") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bi01l39yvf76kmxx8q46ikiq0y7wfikafpp02b3zjgy4xxyxiaz")))

(define-public crate-rust-workflows-0.3.0 (c (n "rust-workflows") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09v1hrsdzc54arpsjmy416lp2r0was0w371xak9kmhdav6l8rq24")))

(define-public crate-rust-workflows-0.3.1 (c (n "rust-workflows") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05s15mgvmxhwfwh3l03qw0ab5s1zfjgxf2sx86avbj72yhqvdvfn")))

(define-public crate-rust-workflows-0.4.0 (c (n "rust-workflows") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gk7l57hgbbzwbq87yk3g1kbir38a4kjz0wlpl1rijm4pk7dbc7r")))

(define-public crate-rust-workflows-0.4.1 (c (n "rust-workflows") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mwy8xak3qprvgvxy8micwqska0j2y1rvqkxjzyg69x2hbbysk19")))

(define-public crate-rust-workflows-0.4.2 (c (n "rust-workflows") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1almmb9740l58frcchrkqnlvgjk3nvndj6q4gc37dprffib9waql")))

(define-public crate-rust-workflows-0.4.3 (c (n "rust-workflows") (v "0.4.3") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0df78xfxzjzjyqqf0qlcsh0jvnlyszk4x6p3vjyddadca885rccc")))

(define-public crate-rust-workflows-0.4.4 (c (n "rust-workflows") (v "0.4.4") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b9z400nxbvlhlg4lw1y02sy05lijaigjdly991fjhzz6nv0wblf")))

(define-public crate-rust-workflows-0.4.5 (c (n "rust-workflows") (v "0.4.5") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rh8kvj461ksfd16mq5xgvb73afybckp6nkhx87k1xkvncglfygi")))

(define-public crate-rust-workflows-0.5.0 (c (n "rust-workflows") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lhkqrp449gxq80f4hkl8vvbiy9wcnx8imkddylf2ay9zy0i6vg9")))

(define-public crate-rust-workflows-0.6.0 (c (n "rust-workflows") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1w5nyz0j89x6qbqr9j1rvmjbqc3bcq27grrykn2nyrhbw4wbx6x9")))

(define-public crate-rust-workflows-0.7.0 (c (n "rust-workflows") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lp391548n6imp6vmw1b70587al2jbnj7ich5rzsjirxxyrzdx8n")))

(define-public crate-rust-workflows-0.7.1 (c (n "rust-workflows") (v "0.7.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10qsw2ph5ry0wmngnpvww54npl0lbwv4kjfwbp2mqchv7d5y96lm")))

(define-public crate-rust-workflows-0.8.0 (c (n "rust-workflows") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12yknpnkm5lhdxpmi93f81pv6f4hx8imfrhj0g6kvgwhh6zjjkbm")))

(define-public crate-rust-workflows-0.8.1 (c (n "rust-workflows") (v "0.8.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b0xddx0glcpzn2gdrlaw9hlx8566hhribqbplpx02ck1x7b7cvp")))

(define-public crate-rust-workflows-0.8.2 (c (n "rust-workflows") (v "0.8.2") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0blkf3i8kfrb9kla9qblq2jh811pklrhpf6d1kfcl395qkid3jfv")))

(define-public crate-rust-workflows-0.9.0 (c (n "rust-workflows") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14y9m5qaail5602lx2w0njrbrac23wwq19gvwr662hp4gahmqh1r")))

