(define-module (crates-io ru st rustico) #:use-module (crates-io))

(define-public crate-rustico-0.1.0 (c (n "rustico") (v "0.1.0") (h "0a6qaf2pxxbq5rcz6clql93xlvsjb9751f37b9iynwx0l8bq9d9d")))

(define-public crate-rustico-0.1.1 (c (n "rustico") (v "0.1.1") (h "0xlqyyzi9lgnh09rvrcl4dxp248hdh1mp88afa180n76fy1s9y1m")))

(define-public crate-rustico-0.1.2 (c (n "rustico") (v "0.1.2") (h "1jv5zmawpf6y84ydk0ilm0l71dn700rj26p9scwwsrf9lc9nnaqa")))

