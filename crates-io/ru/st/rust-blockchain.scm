(define-module (crates-io ru st rust-blockchain) #:use-module (crates-io))

(define-public crate-rust-blockchain-0.1.0 (c (n "rust-blockchain") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rust_sodium") (r "^0.10.2") (d #t) (k 0)))) (h "0hz5p5vdkm415f2japk6z4cssz4ljc4dsxcfyzsgic38x9wx1x7k")))

