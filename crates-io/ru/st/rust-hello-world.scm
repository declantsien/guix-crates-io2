(define-module (crates-io ru st rust-hello-world) #:use-module (crates-io))

(define-public crate-rust-hello-world-0.1.0 (c (n "rust-hello-world") (v "0.1.0") (h "0cyyqlbkb1zsm0q62s3kzp880an5gjzi119hbc2yjnx4y0wk732m")))

(define-public crate-rust-hello-world-0.1.1 (c (n "rust-hello-world") (v "0.1.1") (h "1lbqqsb37wm82jc2vg85411xdmzqalvvfc5ldzhs6fw86zifd74f")))

