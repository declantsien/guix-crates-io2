(define-module (crates-io ru st rust-parser) #:use-module (crates-io))

(define-public crate-rust-parser-0.1.0 (c (n "rust-parser") (v "0.1.0") (h "1g4fkfn68g8lp5q2zr9fv4g4sswqvz7hky0kbmz8mab3jq8bxdgw")))

(define-public crate-rust-parser-0.1.1 (c (n "rust-parser") (v "0.1.1") (h "1a90xbm4cxv4qi4x3zhzqirn8974c14llqaqy5yv89vbr56zwh1z")))

(define-public crate-rust-parser-0.1.2 (c (n "rust-parser") (v "0.1.2") (h "01by4b44ds0km4v6902f5cvcsnxmhnnh6lr2mfkjzix0pr2w8kfc")))

