(define-module (crates-io ru st rust-v4v) #:use-module (crates-io))

(define-public crate-rust-v4v-0.1.0 (c (n "rust-v4v") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1r6fps06pg2xy7xrp5qsjp40yiwa7q1hxij8f2jn8crd95mcjlp9")))

(define-public crate-rust-v4v-0.1.1 (c (n "rust-v4v") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "10ikddk9c554c0yf6qvqw7qln0v7vl68v5xnzfjm2j7rq072pqvs")))

(define-public crate-rust-v4v-0.2.0 (c (n "rust-v4v") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1jp00bzsd17l0apjfzlajc5si3g9naqffsymrr9dr55mbmx8axyz")))

