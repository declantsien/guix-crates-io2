(define-module (crates-io ru st rustymines) #:use-module (crates-io))

(define-public crate-rustymines-1.1.3 (c (n "rustymines") (v "1.1.3") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hj415f9mrjg7fnx2r52m3aja6gi1zg7y45lfkpmc9rz8grkyfzn") (y #t)))

(define-public crate-rustymines-1.1.4 (c (n "rustymines") (v "1.1.4") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17w1r591571pfm3pba8a7qw04ba6h015la8cldh10nydjn868iwa") (y #t)))

(define-public crate-rustymines-1.1.5 (c (n "rustymines") (v "1.1.5") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05sj8k2y437a916kcbg44cddbdh4gc0lh1dfp5p5cfdk2cphv6mn") (y #t)))

(define-public crate-rustymines-1.1.6 (c (n "rustymines") (v "1.1.6") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ysmncc2wdbd45vg07c9psyfqas33dlqb9j4n0dkyw5sbf2cnkfw")))

(define-public crate-rustymines-1.1.7 (c (n "rustymines") (v "1.1.7") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.2.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02h15kngkdkslf746z1bl070d3s9vvan58pqacc7jizg8l1gdl69")))

(define-public crate-rustymines-1.1.8 (c (n "rustymines") (v "1.1.8") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.2.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fdjihi3ck21nnlnhaizldzphdrh254rxr1bs38bvgbjw0lfb84z")))

(define-public crate-rustymines-1.1.9 (c (n "rustymines") (v "1.1.9") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17w9z88f3b2lqimd864hslqwial029cswbb42p8xradjz2cn1da3") (y #t)))

(define-public crate-rustymines-1.1.10 (c (n "rustymines") (v "1.1.10") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0j1wsjk2wd9vy33xrgl47l7jdxwvs9ifawsarbr1spl8avmlsvsh") (y #t)))

(define-public crate-rustymines-1.1.11 (c (n "rustymines") (v "1.1.11") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14x3nmzznsakmmqarqsa6ibd9xfdln2nahq3ah56711cjmik4ybl")))

(define-public crate-rustymines-1.1.12 (c (n "rustymines") (v "1.1.12") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b83rszhfa5qd7hcj6wv9vq9dar8ahspb9a2hzlc53fv42wfn1j5")))

(define-public crate-rustymines-1.1.13 (c (n "rustymines") (v "1.1.13") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1f6009pvb0irk1fn8zk2l83y1fd1xwd186arkba10wwnf8ydq5w2")))

(define-public crate-rustymines-1.1.14 (c (n "rustymines") (v "1.1.14") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bkcvg7zrkg5kh1hx9hbaf7yb2prq8h1byq196srh2hnsaf3af2c")))

(define-public crate-rustymines-1.1.15 (c (n "rustymines") (v "1.1.15") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.6") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vcf16iqk60a824hdsk0ci7x1xjz7r9812cyp7kq93m38pljqsbz")))

(define-public crate-rustymines-1.1.16 (c (n "rustymines") (v "1.1.16") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.7") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1axvwr4wa7k5pkspp2hzfijmbgwx9z5ia1sj2w6l870kw78jqqda")))

(define-public crate-rustymines-1.1.17 (c (n "rustymines") (v "1.1.17") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grid2d") (r "^0.3.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dkwxzzsnl717v271qqawz9f9c15x0hjlrhainmvm9hzgdn1di9j")))

