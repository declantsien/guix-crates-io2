(define-module (crates-io ru st rust-mnsm2i-ga) #:use-module (crates-io))

(define-public crate-rust-mnsm2i-ga-0.1.0 (c (n "rust-mnsm2i-ga") (v "0.1.0") (d (list (d (n "io") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0c558lgkc3hja0h4s22w5w3wskq97gijcimrqxbzfwgfxgvza1zz")))

