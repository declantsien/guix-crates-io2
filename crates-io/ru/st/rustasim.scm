(define-module (crates-io ru st rustasim) #:use-module (crates-io))

(define-public crate-rustasim-0.0.99 (c (n "rustasim") (v "0.0.99") (d (list (d (n "atomic-counter") (r "^1.0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "slog") (r "^2.5.2") (f (quote ("max_level_trace" "release_max_level_error"))) (d #t) (k 0)))) (h "0isac476gid11iadzcxbm36xsk3pnf1q6qmy5lffn6c6v9a6wgak")))

