(define-module (crates-io ru st rust00) #:use-module (crates-io))

(define-public crate-rust00-0.1.0 (c (n "rust00") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1q40sywjhpfasvwk5c55x41qkmljgarcl7jcwl4h5823dzq840db")))

(define-public crate-rust00-0.1.1 (c (n "rust00") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1fi5wf93nzz9wdqhy7fd5r65naqfr4158ww42nj2s9h0brpg9gii")))

(define-public crate-rust00-0.1.2 (c (n "rust00") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "17m3fy5nf4ifp353p1lyyicc8pp8ib56rnbl6yv2ziisj6bn16qc")))

