(define-module (crates-io ru st rustget) #:use-module (crates-io))

(define-public crate-rustget-0.1.0 (c (n "rustget") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "parallel-getter") (r "^0.2.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "12r6nwlcxpz9aw216lbqxp8svknw1y1gsk5y9v79d4aib4hg61bx") (y #t)))

(define-public crate-rustget-0.1.1 (c (n "rustget") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "parallel-getter") (r "^0.2.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "08d3mrjdsfj1ar31lypj8s84kh4y0540hmqhrx072zdi2p6si5c3")))

(define-public crate-rustget-0.1.2 (c (n "rustget") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "parallel-getter") (r "^0.2.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1wlsh6337xg9d6xifqmn7kcn1d9wxr362i5x9c4j0jrwqj73wz0x")))

