(define-module (crates-io ru st rust-strictmath) #:use-module (crates-io))

(define-public crate-rust-strictmath-0.1.0 (c (n "rust-strictmath") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1ac8f9b30mq6kx7d4qdl00zq7y683ym7jkfdwg7vi3g6cb01n3a2")))

(define-public crate-rust-strictmath-0.1.1 (c (n "rust-strictmath") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "13xhw3mzvzppazbpbs5pidgz678n58rbmzx3dcpzc9fy0jwilrbl")))

(define-public crate-rust-strictmath-0.1.2 (c (n "rust-strictmath") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0d7mpcfh9r20hqydnysfa07pzp45yzp145h7dvbqa8ps9jfmhp39")))

