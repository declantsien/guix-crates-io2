(define-module (crates-io ru st rustructure-macros) #:use-module (crates-io))

(define-public crate-rustructure-macros-0.2.0 (c (n "rustructure-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a3y20ljsbwablpssxqkdgl5hfnx52fzn2c9v98k418mr40xwx5r")))

