(define-module (crates-io ru st rustls-ffi) #:use-module (crates-io))

(define-public crate-rustls-ffi-0.8.2 (c (n "rustls-ffi") (v "0.8.2") (d (list (d (n "cbindgen") (r "^0.19.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "rustls") (r "=0.20") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "sct") (r "^0.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (d #t) (k 0)))) (h "06kqrvm1d5ps9pml26zdd2hm8hh20j6svwvqibpnx7m5rh3jg9cx") (f (quote (("no_log_capture")))) (l "rustls_ffi")))

(define-public crate-rustls-ffi-0.9.0 (c (n "rustls-ffi") (v "0.9.0") (d (list (d (n "cbindgen") (r "^0.19.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "rustls") (r "=0.20.4") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "sct") (r "^0.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (d #t) (k 0)))) (h "1z3x1aj5jmmpwx9pwwyapzw7gcxdsnycyjw5fk8wdmrw5ac4m172") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture")))) (l "rustls_ffi")))

(define-public crate-rustls-ffi-0.9.1 (c (n "rustls-ffi") (v "0.9.1") (d (list (d (n "cbindgen") (r "^0.19.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "rustls") (r "=0.20.4") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "sct") (r "^0.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (d #t) (k 0)))) (h "10mdfw13jh3nmm0gakc35gpgq82ljj02hfds466ds6pr85nyaqcq") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture")))) (l "rustls_ffi")))

(define-public crate-rustls-ffi-0.9.2 (c (n "rustls-ffi") (v "0.9.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.10") (d #t) (k 0)) (d (n "rustls") (r "=0.20.8") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "sct") (r "^0.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (d #t) (k 0)))) (h "17sr5w0wx8x889fvgj7bf9bmawcxb20ls1idv8ch8qi2a314isd1") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture")))) (l "rustls_ffi") (r "1.57")))

(define-public crate-rustls-ffi-0.10.0 (c (n "rustls-ffi") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.10") (d #t) (k 0)) (d (n "rustls") (r "=0.21.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (d #t) (k 0)) (d (n "sct") (r "^0.7") (d #t) (k 0)) (d (n "webpki") (r "^0.22") (d #t) (k 0)))) (h "0ncd530r15fmjqg4zzi6g8d7d2jv8m6k76q0q7jqapkfg7h1qc7q") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture")))) (l "rustls_ffi") (r "1.57")))

(define-public crate-rustls-ffi-0.11.0 (c (n "rustls-ffi") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rustls") (r "=0.21.5") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.3") (d #t) (k 0)) (d (n "rustls-webpki") (r "^0.101.0") (d #t) (k 0)) (d (n "sct") (r "^0.7") (d #t) (k 0)))) (h "145qcrnsr89cg152ji0ys5s3m20wypsr0427q8w4vyhqrdgq1bhd") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture")))) (l "rustls_ffi") (r "1.60")))

(define-public crate-rustls-ffi-0.12.0 (c (n "rustls-ffi") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pki-types") (r "^1") (f (quote ("std"))) (d #t) (k 0) (p "rustls-pki-types")) (d (n "regex") (r "^1.9.6") (d #t) (k 2)) (d (n "rustls") (r "^0.22") (f (quote ("ring"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 0)) (d (n "webpki") (r "^0.102.0") (f (quote ("std"))) (d #t) (k 0) (p "rustls-webpki")))) (h "1zl88yh495ns2kp511mijzfaz04z8gx5j1wdi5r8ijcghcaqyhjy") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture")))) (l "rustls_ffi") (r "1.61")))

(define-public crate-rustls-ffi-0.12.1 (c (n "rustls-ffi") (v "0.12.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pki-types") (r "^1") (f (quote ("std"))) (d #t) (k 0) (p "rustls-pki-types")) (d (n "regex") (r "^1.9.6") (d #t) (k 2)) (d (n "rustls") (r "^0.22") (f (quote ("ring"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 0)) (d (n "webpki") (r "^0.102.0") (f (quote ("std"))) (d #t) (k 0) (p "rustls-webpki")))) (h "0axdj9zm6kkzrb24s88n797fm1c6s1mnjrmncycwrk5drj6dqd3j") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture") ("capi")))) (l "rustls_ffi") (r "1.61")))

(define-public crate-rustls-ffi-0.13.0 (c (n "rustls-ffi") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pki-types") (r "^1") (f (quote ("std"))) (d #t) (k 0) (p "rustls-pki-types")) (d (n "regex") (r "^1.9.6") (d #t) (k 2)) (d (n "rustls") (r "^0.23.4") (f (quote ("ring" "std" "tls12"))) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 0)) (d (n "webpki") (r "^0.102.0") (f (quote ("ring" "std"))) (k 0) (p "rustls-webpki")))) (h "1jqm93jddc8r38a0jxr8rpsbnvc5a6fiinwbh5d1r2da2niz2nd7") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture") ("capi")))) (l "rustls_ffi") (r "1.61")))

(define-public crate-rustls-ffi-0.12.2 (c (n "rustls-ffi") (v "0.12.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pki-types") (r "^1") (f (quote ("std"))) (d #t) (k 0) (p "rustls-pki-types")) (d (n "regex") (r "^1.9.6") (d #t) (k 2)) (d (n "rustls") (r "^0.22") (f (quote ("ring"))) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2") (d #t) (k 0)) (d (n "webpki") (r "^0.102.0") (f (quote ("std"))) (d #t) (k 0) (p "rustls-webpki")))) (h "0xlzf766v5756k61ym03x8iizybnm7j3ra41a622592349plwa62") (f (quote (("read_buf" "rustls/read_buf") ("no_log_capture") ("capi")))) (l "rustls_ffi") (r "1.61")))

