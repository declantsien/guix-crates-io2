(define-module (crates-io ru st rusty_oge) #:use-module (crates-io))

(define-public crate-rusty_oge-1.0.0 (c (n "rusty_oge") (v "1.0.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1gw8xyaz14cyla7fcr1wccxna5250vrg76fb1p5q2jnm61ll7llh")))

(define-public crate-rusty_oge-1.0.1 (c (n "rusty_oge") (v "1.0.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "00by93l5hw9x73vhi5n5mwdmf12fhj4s4hgnrd824w90qglnvpip")))

(define-public crate-rusty_oge-1.1.0 (c (n "rusty_oge") (v "1.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1f3fcchr1chka0bw1b575j12r7f7wpyk143nq8z9zpff6n7ryjmx")))

(define-public crate-rusty_oge-1.2.0 (c (n "rusty_oge") (v "1.2.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "unrar") (r "^0.5.1") (d #t) (k 0)))) (h "18amnaw6i1k4izfb3axby4bdlvzb62gh2km43397b3fbcsrygbn4")))

(define-public crate-rusty_oge-1.3.0 (c (n "rusty_oge") (v "1.3.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unrar") (r "^0.5.1") (d #t) (k 0)))) (h "1bwkk2iw7sl4isswzlzcqz4yxyvh95qy47d66w06l21zijg1xalb")))

(define-public crate-rusty_oge-1.4.0 (c (n "rusty_oge") (v "1.4.0") (d (list (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unrar") (r "^0.5.1") (d #t) (k 0)))) (h "1056svsa0l06xijlgcpfhxipgvqmmgy5jcr6mmgbnlsfrl63z0fw")))

(define-public crate-rusty_oge-1.5.0 (c (n "rusty_oge") (v "1.5.0") (d (list (d (n "pest") (r "^2.7.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.7") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unrar") (r "^0.5.1") (d #t) (k 0)))) (h "07d0nhdzpxsszzg2j3z97149xdpgknijrgxxxi11qawb1ca9i3dp")))

