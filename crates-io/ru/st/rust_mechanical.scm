(define-module (crates-io ru st rust_mechanical) #:use-module (crates-io))

(define-public crate-rust_mechanical-0.0.1 (c (n "rust_mechanical") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "00k41h9n4fasiklh8k8nskjivkakq3yrwzm1y7xv4bq2a6p421v2") (y #t)))

(define-public crate-rust_mechanical-0.0.2 (c (n "rust_mechanical") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0g4c3kp4lbn67sk5rp8xi96l56i2yrh9ix2csabrvri0k8cvzz9j")))

