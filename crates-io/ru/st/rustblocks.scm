(define-module (crates-io ru st rustblocks) #:use-module (crates-io))

(define-public crate-rustblocks-0.1.0 (c (n "rustblocks") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ymxglpnycpg1nlzjz1h1vhd9crl6rw18q69jdazx11j9r89lb50")))

(define-public crate-rustblocks-0.2.0 (c (n "rustblocks") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nivvgaz9q7qh9rnw40y4mlj4ffx5lkapai5lv09y9ykdvz6qfgg")))

(define-public crate-rustblocks-0.2.1 (c (n "rustblocks") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07gy39bk38sfcgxp9gk661rwif4jnwyh5cb5xb8zw8dn22nxgak9")))

(define-public crate-rustblocks-0.2.2 (c (n "rustblocks") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "026yvcl3is34aqng3sbjh54yhqlhxklkh7xzhhw20w109q91pyzm")))

(define-public crate-rustblocks-0.3.0 (c (n "rustblocks") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0d6an8w293i81f2lizkwkgpbfdh28pbcqgjk3y1ck09h937wmsyv")))

(define-public crate-rustblocks-0.3.2 (c (n "rustblocks") (v "0.3.2") (d (list (d (n "blocks_lib") (r "^0.3.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal-light") (r "^1.2.0") (d #t) (k 0)))) (h "1jbbvn25wzsmpxy7llxg33z09cm6mmr3bjrl9xiq4mzds5zn23ng")))

(define-public crate-rustblocks-0.3.3 (c (n "rustblocks") (v "0.3.3") (d (list (d (n "blocks_lib") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal-light") (r "^1.2.0") (d #t) (k 0)))) (h "0kqyn5x7azxfsj7avmdqdyh78q2l0wjnnz5hqf1mhmk5xgh4sxzw")))

