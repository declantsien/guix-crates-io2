(define-module (crates-io ru st rust_dmx) #:use-module (crates-io))

(define-public crate-rust_dmx-0.3.0 (c (n "rust_dmx") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0dikzqs6fzxx50kpry025xafa6kzmz8jhyvlfr4p36h86g9j20fy")))

(define-public crate-rust_dmx-0.3.1 (c (n "rust_dmx") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "1bxkj1p0w12ivhi0kiamf49najf8j8vjxjx6hsmfyw1j3p0kl4w1")))

(define-public crate-rust_dmx-0.3.2 (c (n "rust_dmx") (v "0.3.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "1366298q2wqaf69bk7hbkgdwkrcf8rrkfsiy3lya53hadcbsisyg")))

(define-public crate-rust_dmx-0.3.3 (c (n "rust_dmx") (v "0.3.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "13sz3lw4bnifhham8hb25z1r36v8zsnhbssr4x0si47jajkbmgsp")))

(define-public crate-rust_dmx-0.3.4 (c (n "rust_dmx") (v "0.3.4") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0lakdh84yydf1g072v93nmha6y9k9csc6hjsw07m24zqdd3hsabj")))

(define-public crate-rust_dmx-0.4.0 (c (n "rust_dmx") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0gjrd4krp1ic827c45c44ki9wk12cpd7587877b0542j4ccc9vdr")))

(define-public crate-rust_dmx-0.5.0 (c (n "rust_dmx") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "typetag") (r "^0.2") (d #t) (k 0)))) (h "0j4kbzsvqvg4qlggv8ph710lgwxf9gmzqbfpi9skm8zbrcci6prl")))

