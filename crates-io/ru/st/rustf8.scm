(define-module (crates-io ru st rustf8) #:use-module (crates-io))

(define-public crate-rustf8-0.9.1 (c (n "rustf8") (v "0.9.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "00mbgqwf01knhx865xz52ky4q5a6baachljqv4wyx2igg00kg59z")))

