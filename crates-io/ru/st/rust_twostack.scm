(define-module (crates-io ru st rust_twostack) #:use-module (crates-io))

(define-public crate-rust_twostack-0.1.0 (c (n "rust_twostack") (v "0.1.0") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "rust_dynamic") (r "^0.3.0") (d #t) (k 0)))) (h "19dr90g2f2y0afkc1ikyp4gszkkxl1a6g6vcfbvysqx8lkl89iwr")))

(define-public crate-rust_twostack-0.2.0 (c (n "rust_twostack") (v "0.2.0") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "rust_dynamic") (r "0.*") (d #t) (k 0)))) (h "11pq3sdz026gp7xfvmq49w6gfjrs2hwmdll36w1vhjzgf222psyk")))

(define-public crate-rust_twostack-0.3.0 (c (n "rust_twostack") (v "0.3.0") (d (list (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "rust_dynamic") (r "0.*") (d #t) (k 0)))) (h "0kd9yx03gwqpq8fzfr8spgfs320shvwd8ripyqrcs7mxi6xhdpcl")))

(define-public crate-rust_twostack-0.11.0 (c (n "rust_twostack") (v "0.11.0") (d (list (d (n "nanoid") (r "0.4.*") (d #t) (k 0)) (d (n "rust_dynamic") (r "0.*") (d #t) (k 0)))) (h "031ky8zi8xmrl796ggz0d6yyklj0872nb23f6bjnv8nhp60ynzx1")))

(define-public crate-rust_twostack-0.12.0 (c (n "rust_twostack") (v "0.12.0") (d (list (d (n "nanoid") (r "0.4.*") (d #t) (k 0)) (d (n "rust_dynamic") (r "0.*") (d #t) (k 0)))) (h "18ldzs3k8v86izyy11xqkqfcd8nzwjb66r291bk0afgjyj2assih")))

(define-public crate-rust_twostack-0.13.0 (c (n "rust_twostack") (v "0.13.0") (d (list (d (n "nanoid") (r "0.4.*") (d #t) (k 0)) (d (n "rust_dynamic") (r ">=0.15") (d #t) (k 0)))) (h "012ip41vip1nfzg92z5666drkdjwg1slyb4axj5qzxwh423ariq6")))

(define-public crate-rust_twostack-0.14.0 (c (n "rust_twostack") (v "0.14.0") (d (list (d (n "nanoid") (r "0.4.*") (d #t) (k 0)) (d (n "rust_dynamic") (r ">=0.15") (d #t) (k 0)))) (h "0va7cjli265qkcmlk11i9qhm8b8p9c6mzx8jwvlk0z8fgnbmaghg")))

(define-public crate-rust_twostack-0.14.1 (c (n "rust_twostack") (v "0.14.1") (d (list (d (n "nanoid") (r "0.4.*") (d #t) (k 0)) (d (n "rust_dynamic") (r ">=0.15") (d #t) (k 0)))) (h "00wc3xf46i3k0fr27nqkm2375cyna3lds9bfhh3pdijjd6mmjz2k")))

