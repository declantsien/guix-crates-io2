(define-module (crates-io ru st rust-cloud-discovery) #:use-module (crates-io))

(define-public crate-rust-cloud-discovery-0.1.0 (c (n "rust-cloud-discovery") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rk0kpwrf0rdwwpxrgpwzhx1db3v4n3zzkwrdybgcyxq9xsnb6i6")))

(define-public crate-rust-cloud-discovery-0.2.0 (c (n "rust-cloud-discovery") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k1gaid5a7y9ghgbx0087lc0frcaqpvvfnrhq90nr11f0m39g98a")))

