(define-module (crates-io ru st rusty-leaf) #:use-module (crates-io))

(define-public crate-rusty-leaf-0.1.0 (c (n "rusty-leaf") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "darkdown") (r "^0.1.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "toml") (r "^0.8.9") (d #t) (k 0)))) (h "1v71wmaxf6kpmnyqcknm0b92i7fzdlqkqmqrgj7ni19pgfpgm604")))

