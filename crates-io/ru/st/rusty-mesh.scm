(define-module (crates-io ru st rusty-mesh) #:use-module (crates-io))

(define-public crate-rusty-mesh-0.1.0 (c (n "rusty-mesh") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "1dmwvnvq8jswbi97jkkxj79zk2b8cir828k1kjbsl0n2zwz7gilc")))

