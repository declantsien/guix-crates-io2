(define-module (crates-io ru st rust_combinatorics) #:use-module (crates-io))

(define-public crate-rust_combinatorics-0.0.1 (c (n "rust_combinatorics") (v "0.0.1") (h "0faa7zzhcbcs9x4cdh82cdkjp7d1i7k4xk5pw5n85vjd8v47myk0")))

(define-public crate-rust_combinatorics-0.1.0 (c (n "rust_combinatorics") (v "0.1.0") (h "0csi3fdsqslpfcdf70jzdh6l3z72a1ak7vncmrjl0kb2r6ngxgga")))

