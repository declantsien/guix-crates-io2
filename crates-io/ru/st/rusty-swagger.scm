(define-module (crates-io ru st rusty-swagger) #:use-module (crates-io))

(define-public crate-rusty-swagger-0.1.0 (c (n "rusty-swagger") (v "0.1.0") (d (list (d (n "mustache") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0gs68a7qv07acvn327kg36jmvk3znczk1xikldw775574xwi4f88")))

(define-public crate-rusty-swagger-0.1.1 (c (n "rusty-swagger") (v "0.1.1") (d (list (d (n "mustache") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0y46jsp0s057w2r3lsmzcf34d2qwc70ayc9p7nvqy6nhpmp0w4in")))

