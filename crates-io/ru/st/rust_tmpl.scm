(define-module (crates-io ru st rust_tmpl) #:use-module (crates-io))

(define-public crate-rust_tmpl-0.0.4 (c (n "rust_tmpl") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1h26kq6n6hpr9v93566xph30rsayd35kilwwxkridf900lnrai73") (f (quote (("default"))))))

(define-public crate-rust_tmpl-0.0.5 (c (n "rust_tmpl") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19jl6gammxwiyl6j64dzp52kn80zsa80rs48fi8j5fmdvvk8d4fx") (f (quote (("default"))))))

(define-public crate-rust_tmpl-0.0.6 (c (n "rust_tmpl") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04i0yk3lqdfzyxmrd3g2l0xmk5c00jia9j5wbjcjdmymplkpkmy7") (f (quote (("default"))))))

