(define-module (crates-io ru st rustboot) #:use-module (crates-io))

(define-public crate-rustboot-0.1.0 (c (n "rustboot") (v "0.1.0") (d (list (d (n "os_bootinfo") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "rlibc") (r "^1") (d #t) (k 0)) (d (n "usize_conversions") (r "^0.2.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.2.0-alpha-001") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.0") (d #t) (k 0)))) (h "1c85npi7d4ckcjgnm5dgba5vx0yfz2w0amvpcpir5qj0if58k2jg")))

