(define-module (crates-io ru st rustable) #:use-module (crates-io))

(define-public crate-rustable-0.1.0 (c (n "rustable") (v "0.1.0") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustbus") (r "^0.6.0") (d #t) (k 0)))) (h "0f7pgb64r1npz9sydlscrxryvz4fjfskkyaqvlrzfv8q2yyc31yg")))

(define-public crate-rustable-0.1.1 (c (n "rustable") (v "0.1.1") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rustbus") (r "^0.6.0") (d #t) (k 0)))) (h "1vdpwhs692ykf74pcfa5bgr6p2rvakpc055sy10vx1d8qx1dp8xl")))

(define-public crate-rustable-0.2.0 (c (n "rustable") (v "0.2.0") (d (list (d (n "nix") (r ">=0.18.0, <0.19.0") (d #t) (k 0)) (d (n "rustbus") (r ">=0.9.2, <0.10.0") (d #t) (k 0)))) (h "11g86dh393fny2d09dss97ci8blb9aihqbfjyr8x0xhhhxf9ka0r")))

(define-public crate-rustable-0.2.1 (c (n "rustable") (v "0.2.1") (d (list (d (n "nix") (r "^0.19.0") (d #t) (k 0)) (d (n "rustbus") (r "^0.9.2") (d #t) (k 0)))) (h "02ks9r0jf20pzhxsi3a89jgdlm485hsiaznn7pra8jbnpilaz53r")))

(define-public crate-rustable-0.2.2 (c (n "rustable") (v "0.2.2") (d (list (d (n "nix") (r "^0.19.0") (d #t) (k 0)) (d (n "rustbus") (r "^0.9.2") (d #t) (k 0)))) (h "1mwrzdqs30klhagdgc37650ljl7d87kym7q1h8nff8cwhv50i76c")))

(define-public crate-rustable-0.3.0 (c (n "rustable") (v "0.3.0") (d (list (d (n "async-rustbus") (r "^0.1.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-enum") (r "^0.1.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1wy1fy9dv4prwdsy3ckka3d2453p2bpgal9aqxq4n1hf3w221hiv")))

