(define-module (crates-io ru st rustup-version-name) #:use-module (crates-io))

(define-public crate-rustup-version-name-0.2.0 (c (n "rustup-version-name") (v "0.2.0") (d (list (d (n "toml") (r "^0.1.30") (d #t) (k 0)))) (h "0gzzms6qsfnbx6qlzcfg2akha039yj4km028mkigyj2izwbf3lwv")))

(define-public crate-rustup-version-name-0.2.1 (c (n "rustup-version-name") (v "0.2.1") (d (list (d (n "toml") (r "^0.1.30") (d #t) (k 0)))) (h "1dgjbkv9qfvdrb7bjxjf5yflpyxkbb3kdx44z56rhsmd22a1sdqw")))

