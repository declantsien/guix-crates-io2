(define-module (crates-io ru st rust-3d) #:use-module (crates-io))

(define-public crate-rust-3d-0.3.2 (c (n "rust-3d") (v "0.3.2") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1hs3l14pbpk95c9rpqdr9y302cw92dbny1s3q2wkisaivk218rf1")))

(define-public crate-rust-3d-0.4.0 (c (n "rust-3d") (v "0.4.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1mj3vdq4py6k07q6p0hxjr2fzwry5mgz4mal673lgpkzkbnklczv")))

(define-public crate-rust-3d-0.5.2 (c (n "rust-3d") (v "0.5.2") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0bkvgc73fvh463s812dgdiibdsp9kkbrm4sz4wwbq7z5r3zwrb3y")))

(define-public crate-rust-3d-0.5.4 (c (n "rust-3d") (v "0.5.4") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "09nnb9kqwj07rfbz2wrb8isx1fg94qfjamv37yv16ay44m2hwrqs")))

(define-public crate-rust-3d-0.9.0 (c (n "rust-3d") (v "0.9.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1yknjmjibh7j1l65r2nm6d6mb997nn3afwv5737kfa1qk14cwxzv")))

(define-public crate-rust-3d-0.11.0 (c (n "rust-3d") (v "0.11.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1acrmx3naarhzyhx6nw73lhj0ff3flg4w98pqxs7l97ar59r2r5n")))

(define-public crate-rust-3d-0.12.0 (c (n "rust-3d") (v "0.12.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0xacm13rdlc70pgrvpc455qf0f1f5z7yv3akfwz25h3lzkf9y12q")))

(define-public crate-rust-3d-0.14.1 (c (n "rust-3d") (v "0.14.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0q9bcb15scqv7k7xi8g2wgc8wkhyjcx3b1vqnhi6av8hnycp7mw2")))

(define-public crate-rust-3d-0.15.0 (c (n "rust-3d") (v "0.15.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0g7b9p6z4jacl7vishdmf60i2bb4fm5dxgpj5qa240vvilk32kcg")))

(define-public crate-rust-3d-0.16.0 (c (n "rust-3d") (v "0.16.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "17l8yxh00bs6znkz4xm9pcrw9d27px033n8sw0m5q5fkls2rpi0b")))

(define-public crate-rust-3d-0.18.0 (c (n "rust-3d") (v "0.18.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1sqh7ajmspr8czvbviacdipzyx95ffxz8j8hsyxaryw1vhrmwi26")))

(define-public crate-rust-3d-0.19.0 (c (n "rust-3d") (v "0.19.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1fv5sf846sdpr2kb7r3c71f2c0ilw0bvca09m84prj0y6cnwvn4h")))

(define-public crate-rust-3d-0.19.1 (c (n "rust-3d") (v "0.19.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1mmjhcx2pl860jqpjir4d4bwr2svi3l4wgj65pg0nxal8zj454gl")))

(define-public crate-rust-3d-0.20.1 (c (n "rust-3d") (v "0.20.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0rz5qakp71dy186qdhaxszqmkg00f2sf36y52lcrcn9hdxb3iia9")))

(define-public crate-rust-3d-0.20.2 (c (n "rust-3d") (v "0.20.2") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0bwk64pbf2ryrgiq0nrm1yf8ijnjrp4qyr7662q5dhqv8mlycay5")))

(define-public crate-rust-3d-0.21.0 (c (n "rust-3d") (v "0.21.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0y89qaq3wcfdl242pvgwz86iyig6f516fmi6w1d1qaqd13bndsq9")))

(define-public crate-rust-3d-0.22.0 (c (n "rust-3d") (v "0.22.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1kxdz09jphs1c6dfwhb6qywy6jz0qg0facaai04ss0flxkz3c297")))

(define-public crate-rust-3d-0.23.0 (c (n "rust-3d") (v "0.23.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0x3fxw2hv5w4hl3ryklkxk3lbxqhcg96xfqn8h7lma5z30qc615m")))

(define-public crate-rust-3d-0.24.0 (c (n "rust-3d") (v "0.24.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1n94ayimf2yqpxia18zmqyafl7drhk2cgil0baxcwdjgyf8pj15d")))

(define-public crate-rust-3d-0.24.1 (c (n "rust-3d") (v "0.24.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "14688gl7csadd6f19nhpwi8119fvmgzjr1w40qcxhjbmnalqyxs4")))

(define-public crate-rust-3d-0.25.0 (c (n "rust-3d") (v "0.25.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0js4swxqf4944y3fbz66hpnq9pa2c17cdv98h3p3334882x31f43")))

(define-public crate-rust-3d-0.25.1 (c (n "rust-3d") (v "0.25.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0b23gfavdpnsv88yfnxfcd144gmg5i9x7gnbj9k5vwi70hn5zm4s")))

(define-public crate-rust-3d-0.25.2 (c (n "rust-3d") (v "0.25.2") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1w3wyhbl8fll20mg0ypnvfbgp740ya0fvhbav3m4rbw9sxvr7f6l")))

(define-public crate-rust-3d-0.26.0 (c (n "rust-3d") (v "0.26.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0cs4cpxhzd3cxjq391lgyvf0n55xr4719fq8x792ngipwv9998s6")))

(define-public crate-rust-3d-0.27.0 (c (n "rust-3d") (v "0.27.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1xcdag4bfj45pl2h54l178swx58xksxq5b824xi71i5ymn5n6fn0")))

(define-public crate-rust-3d-0.28.0 (c (n "rust-3d") (v "0.28.0") (d (list (d (n "bitvec") (r "^0.16.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "18cdnf7sv3bx8ls0gx49aj6c948agyzida45rbw76fzh8qc45lxa")))

(define-public crate-rust-3d-0.28.1 (c (n "rust-3d") (v "0.28.1") (d (list (d (n "bitvec") (r "^0.16.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1z41ly47mw86zyy6idxzi5fkq01kjf91qafg7pmwhzvcyk17d56b")))

(define-public crate-rust-3d-0.29.0 (c (n "rust-3d") (v "0.29.0") (d (list (d (n "bitvec") (r "^0.16.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0ny8dfxyryjf4bb2gz366wxqz4g4vj4f1aalwsc80mw7y13wi9p5")))

(define-public crate-rust-3d-0.30.0 (c (n "rust-3d") (v "0.30.0") (d (list (d (n "bitvec") (r "^0.16.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "14fi02xknw07q404bqjan2qnqnx3vv22094cwjxcbx50cw2bnnmg")))

(define-public crate-rust-3d-0.31.0 (c (n "rust-3d") (v "0.31.0") (d (list (d (n "bitvec") (r "^0.16.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0rdn6wy69g6wqg4736j7vys3iba7i44qvxb91da4sjdfk1c2mqal")))

(define-public crate-rust-3d-0.32.0 (c (n "rust-3d") (v "0.32.0") (d (list (d (n "bitvec") (r "^0.17.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0n2b003vaczqszvl0k3mrdqdvi6a92l89kmafcqdqzxnqfn3ad44")))

(define-public crate-rust-3d-0.33.0 (c (n "rust-3d") (v "0.33.0") (d (list (d (n "bitvec") (r "^0.17.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0j05vzf3xzdyvrzc8196b00kr9x4ls453qsf26jr0lq5x9ycnrjl")))

(define-public crate-rust-3d-0.34.0 (c (n "rust-3d") (v "0.34.0") (d (list (d (n "bitvec") (r "^0.17.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1fz9bljbkqrvf86b9qsp1zmi86cnvzybkpizs6473igfq4gfiqbc")))

