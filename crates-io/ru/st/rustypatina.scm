(define-module (crates-io ru st rustypatina) #:use-module (crates-io))

(define-public crate-rustypatina-0.1.0 (c (n "rustypatina") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1dnws7dp6l0xak65iz1plc4flg7yzyksr09cq4khamdxhl979v88")))

