(define-module (crates-io ru st rustaferry_render_kit) #:use-module (crates-io))

(define-public crate-rustaferry_render_kit-0.1.0 (c (n "rustaferry_render_kit") (v "0.1.0") (d (list (d (n "rustaferry_render_hal") (r "^0") (d #t) (k 0)))) (h "0gqzkq5vbw65cb3krnz1jmvjqwcmv6scaj4982lr3fx8ksgh5z98")))

(define-public crate-rustaferry_render_kit-0.2.0 (c (n "rustaferry_render_kit") (v "0.2.0") (d (list (d (n "rustaferry_render_hal") (r "^0.1.3") (d #t) (k 0)))) (h "01m7n55mwjws2s82mm15h9ai4y57sijsbhbzpjd80laql2hl960r")))

