(define-module (crates-io ru st rustup-toolchain) #:use-module (crates-io))

(define-public crate-rustup-toolchain-0.1.0 (c (n "rustup-toolchain") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "15qcdi0y46jzi0bha9wmiyw6nm9jhb3hs44zfskj3h15xbd0c3l0")))

(define-public crate-rustup-toolchain-0.1.1 (c (n "rustup-toolchain") (v "0.1.1") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "public-api") (r "^0.24.2") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0hh5p3b0sm5j0glr399wb307kh1620ppbq76kjjr9xzpm7ji6mnf")))

(define-public crate-rustup-toolchain-0.1.2 (c (n "rustup-toolchain") (v "0.1.2") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "public-api") (r "^0.26.0") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "11pfryslv2f1hqyxr96qzddi476qlmz9lk411vjl96sj3p4ip1kp")))

(define-public crate-rustup-toolchain-0.1.3 (c (n "rustup-toolchain") (v "0.1.3") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "public-api") (r "^0.27.1") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1rh804vjcfsp7n0a891y2vhaanphg7fm3acgbff5liajqgs4rsgi")))

(define-public crate-rustup-toolchain-0.1.4 (c (n "rustup-toolchain") (v "0.1.4") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "public-api") (r "^0.27.3") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.8.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1a8dyyxhvdrj70w6lg4j646vj6lbkn8ck1a58z2h8p3as971rmdl")))

(define-public crate-rustup-toolchain-0.1.5 (c (n "rustup-toolchain") (v "0.1.5") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "public-api") (r "^0.30.0") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "07n2x9fgnr1r3pndnpzfw6zdik6mxvakjqxhkg0km7lpv0qaqnrb")))

(define-public crate-rustup-toolchain-0.1.6 (c (n "rustup-toolchain") (v "0.1.6") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "public-api") (r "^0.32.0") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.8.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "15fcw826mrgml07qw1h0ylgqvhj3wr7ggry2dwr9q63z8rh6yh34")))

