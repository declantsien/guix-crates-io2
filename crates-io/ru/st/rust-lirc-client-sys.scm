(define-module (crates-io ru st rust-lirc-client-sys) #:use-module (crates-io))

(define-public crate-rust-lirc-client-sys-0.1.0 (c (n "rust-lirc-client-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03vh1qdyr5xm8dbj9h5nmk072dzxvax6b68a9zb1iamkr3f0hr1b") (l "lirc")))

(define-public crate-rust-lirc-client-sys-0.2.0 (c (n "rust-lirc-client-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0y98ndi7lg8hshxfhwnl3lfygcc54zcp9034vwvf94wi3c1z3rdn") (l "lirc")))

