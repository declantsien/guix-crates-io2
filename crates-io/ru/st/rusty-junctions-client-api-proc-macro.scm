(define-module (crates-io ru st rusty-junctions-client-api-proc-macro) #:use-module (crates-io))

(define-public crate-rusty-junctions-client-api-proc-macro-0.1.0 (c (n "rusty-junctions-client-api-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v5ah0bhi1z5zqc9y2x314996y459ng1fbyy037whvf51fb8yj79")))

