(define-module (crates-io ru st rustiny_number) #:use-module (crates-io))

(define-public crate-rustiny_number-0.0.1 (c (n "rustiny_number") (v "0.0.1") (h "1dw9jd15lpp6wx2yxfgiwk8znk3gxkjbj7kc733vkld813h2x16l")))

(define-public crate-rustiny_number-0.0.2 (c (n "rustiny_number") (v "0.0.2") (h "0rvblc67dk4fbjndf5jmmf913c09zqf2cv2fxbikc2dmc6056k7x")))

(define-public crate-rustiny_number-0.1.0 (c (n "rustiny_number") (v "0.1.0") (h "0f4i3ps3dwn90mi20zpdylbrdn2gqzsqwrahfl5m6kdkjm5vwp4y")))

(define-public crate-rustiny_number-0.1.1 (c (n "rustiny_number") (v "0.1.1") (h "0zzfl5f8xr69vc7w7mabsa1k852iam088ivk1d58kjfbzj66q8i8")))

(define-public crate-rustiny_number-0.1.2 (c (n "rustiny_number") (v "0.1.2") (h "0l4pz68pvpgnfj43bh6rfhfpsfa6icnw6lvjjrqr4gxsqxjy7nrn")))

(define-public crate-rustiny_number-0.1.3 (c (n "rustiny_number") (v "0.1.3") (h "1vvmws39sfs6dgc1fxzx5bnm5hl02pigw0a72rlrdhx1asda5ks3")))

(define-public crate-rustiny_number-0.1.4 (c (n "rustiny_number") (v "0.1.4") (h "0lbqk78cmiaxa1l17ilfcilbhxj5ppyvw0h268fb703im3fpfnd9")))

(define-public crate-rustiny_number-0.1.5 (c (n "rustiny_number") (v "0.1.5") (h "1jd60shci7pzaxjmfb553zypxxwrjpn53zfdzb6avpilz82y75zs")))

(define-public crate-rustiny_number-0.1.6 (c (n "rustiny_number") (v "0.1.6") (h "0r8x33y63k2j2dch5aw9bqk64kwx05fyriak7f0g4c746vj2avlc")))

(define-public crate-rustiny_number-0.1.7 (c (n "rustiny_number") (v "0.1.7") (h "19nxwqs63ycx8sv7k80k2nwkgg7nfhqaxgq6yznxsj7pi2gd60al")))

(define-public crate-rustiny_number-0.1.8 (c (n "rustiny_number") (v "0.1.8") (h "19nywmcpp6x1zbc1dwn42afl8nzk03z4y8g0wb63iqyqn45b1vln")))

(define-public crate-rustiny_number-0.1.9 (c (n "rustiny_number") (v "0.1.9") (h "1hsjwfzczwxjzq25l3f9lw4sdnymwnbh5csbb9mvvp9khglgfvhh")))

(define-public crate-rustiny_number-0.1.10 (c (n "rustiny_number") (v "0.1.10") (h "0rlcbqsczffdmpk5blvbsfh5626jhaisi8cd1c994vg7m305rv7g")))

(define-public crate-rustiny_number-0.2.0 (c (n "rustiny_number") (v "0.2.0") (h "02gw8hyxy4ygjvpk2n7s95vpjpvx2hwsp2fdmc2x3nn74j9qzk1q")))

