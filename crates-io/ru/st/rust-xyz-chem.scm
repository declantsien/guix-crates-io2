(define-module (crates-io ru st rust-xyz-chem) #:use-module (crates-io))

(define-public crate-rust-xyz-chem-0.0.1 (c (n "rust-xyz-chem") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16xv2i59vafp3b71qyjrhkdip3inmyl7h0b4l4p9hyhvi07hp166") (y #t)))

(define-public crate-rust-xyz-chem-0.0.2 (c (n "rust-xyz-chem") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pn9f4iaz6v1vjinf8pfaajar0hzrx2dcgg7f4ld4y4jfg4pbj70") (y #t)))

