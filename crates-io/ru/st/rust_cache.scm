(define-module (crates-io ru st rust_cache) #:use-module (crates-io))

(define-public crate-rust_cache-0.1.0 (c (n "rust_cache") (v "0.1.0") (d (list (d (n "redis") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "168m4vjs2c1x98sx0pv5b873j6jga2n2l3r5f6j0p32x72kyg72a") (f (quote (("redis_integration" "redis") ("hash_map") ("filesystem" "rust-crypto") ("default" "hash_map") ("all" "hash_map" "filesystem" "redis_integration"))))))

(define-public crate-rust_cache-0.2.0 (c (n "rust_cache") (v "0.2.0") (d (list (d (n "redis") (r "^0.5, ^0.5.2") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "15s98s0gksgpsnfdch9iqj4c7xamngz8vd93w4xs3hb56qaka1lj") (f (quote (("redis_integration" "redis") ("hash_map") ("filesystem" "rust-crypto") ("default" "hash_map") ("all" "hash_map" "filesystem" "redis_integration"))))))

(define-public crate-rust_cache-0.3.0 (c (n "rust_cache") (v "0.3.0") (d (list (d (n "redis") (r "^0.5, ^0.5.2") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00f5iikz2yq9q22amn2ckxx9xbc6zs82znrf88z5g3xxsrwpvidk") (f (quote (("redis_integration" "redis") ("hash_map") ("filesystem" "rust-crypto") ("default" "hash_map") ("all" "hash_map" "filesystem" "redis_integration"))))))

