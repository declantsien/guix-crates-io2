(define-module (crates-io ru st rust-fuzzy-search) #:use-module (crates-io))

(define-public crate-rust-fuzzy-search-0.1.0 (c (n "rust-fuzzy-search") (v "0.1.0") (h "0w9sfkq2hzq6r8s1hz56sr1gzr9rhc3qfj02qi85i193ychd1yc4")))

(define-public crate-rust-fuzzy-search-0.1.1 (c (n "rust-fuzzy-search") (v "0.1.1") (h "1chvl47hq42r219yxs6r1dp4l19acy5ay145hpc5drgzaiq6amx1")))

