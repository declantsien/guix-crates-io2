(define-module (crates-io ru st rusty_timer) #:use-module (crates-io))

(define-public crate-rusty_timer-1.0.0 (c (n "rusty_timer") (v "1.0.0") (h "008xnch2ki2wlsvp1mdfp6pzgqx951rqncrm2dv3lbaavf5axwxq")))

(define-public crate-rusty_timer-1.0.1 (c (n "rusty_timer") (v "1.0.1") (h "1br3qfm8aa1vahyfxhhwph79vs4d0wpknbzjik0c3d6w13ivihji")))

(define-public crate-rusty_timer-1.0.2 (c (n "rusty_timer") (v "1.0.2") (h "1g3ilrssya54infa0i5x8ln0nrndfrvc19l9d35vn74bk5h0wz0f")))

(define-public crate-rusty_timer-1.0.3 (c (n "rusty_timer") (v "1.0.3") (h "02ninryyq0pfj1lirp6x6czg2d7rgsfwpfjbg9012p9qiwd38d4s")))

