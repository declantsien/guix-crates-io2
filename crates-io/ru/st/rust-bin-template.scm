(define-module (crates-io ru st rust-bin-template) #:use-module (crates-io))

(define-public crate-rust-bin-template-0.1.0 (c (n "rust-bin-template") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (f (quote ("humantime"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "149m4llcxfb2dspb9z8650dxnrd3kqqy64l8z4cf6xwk07d0dygn")))

