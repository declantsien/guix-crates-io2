(define-module (crates-io ru st rustomaton) #:use-module (crates-io))

(define-public crate-rustomaton-0.1.0 (c (n "rustomaton") (v "0.1.0") (d (list (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0nlci67611nnhs2qjvdxfibh9bgv4f6z4mr0bkfpva0v7qvssm2w")))

(define-public crate-rustomaton-0.2.0 (c (n "rustomaton") (v "0.2.0") (d (list (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0fy9r2h462gbdm0048v8zaz3q8gyr7x1fh4dl1bc23pqc18xajhj")))

(define-public crate-rustomaton-0.2.1 (c (n "rustomaton") (v "0.2.1") (d (list (d (n "logos") (r "^0.9.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "05wni4azkb6sa1ania2ijcra5qrlkxbd2j8vi728pw4spivw15ni")))

