(define-module (crates-io ru st rust-libcint) #:use-module (crates-io))

(define-public crate-rust-libcint-0.1.0 (c (n "rust-libcint") (v "0.1.0") (h "0gwpbdv4rf4cn0rjk9wpknhiviwz8adbhd5q9al1rawzyd617vpl")))

(define-public crate-rust-libcint-0.1.1 (c (n "rust-libcint") (v "0.1.1") (h "0h0gl0j38pzyy6gsz0hrdl4y5qdfwdjghqssywf06875p0sibxws")))

(define-public crate-rust-libcint-0.1.2 (c (n "rust-libcint") (v "0.1.2") (h "0kkvn42vhj84h9pc3pd9w8vsfxqbnp7wd3xblw4iwfp9lssm8si1")))

(define-public crate-rust-libcint-0.1.3 (c (n "rust-libcint") (v "0.1.3") (h "1cpb9g4nj5ak88v8r880b0g9wh7mxmdmj55qcr6ygq72wvk7yg4i")))

