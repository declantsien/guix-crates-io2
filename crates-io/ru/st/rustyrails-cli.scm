(define-module (crates-io ru st rustyrails-cli) #:use-module (crates-io))

(define-public crate-rustyrails-cli-0.1.0 (c (n "rustyrails-cli") (v "0.1.0") (d (list (d (n "cargo-generate") (r "^0.18.5") (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trycmd") (r "^0.14.19") (d #t) (k 2)))) (h "1jylh6a0k9pd1x19zcsvdkply1wi4vlijdii8vps7l30l73m9bsj")))

(define-public crate-rustyrails-cli-0.2.0 (c (n "rustyrails-cli") (v "0.2.0") (d (list (d (n "cargo-generate") (r "^0.18.5") (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trycmd") (r "^0.14.19") (d #t) (k 2)))) (h "0hpgrb6iyi9hxpvxc77k0zay09ffaiv067yvbpmky8klbr705bwp")))

(define-public crate-rustyrails-cli-0.2.1 (c (n "rustyrails-cli") (v "0.2.1") (d (list (d (n "cargo-generate") (r "^0.18.5") (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trycmd") (r "^0.14.19") (d #t) (k 2)))) (h "1pqkda4g744mlw5sl9jdqkpfck9rgn407bl89mk8rwspr4fpch30")))

(define-public crate-rustyrails-cli-0.2.2 (c (n "rustyrails-cli") (v "0.2.2") (d (list (d (n "cargo-generate") (r "^0.18.5") (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "~0.11") (d #t) (k 0)) (d (n "eyre") (r "^0.6.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.2") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trycmd") (r "^0.14.19") (d #t) (k 2)))) (h "08jvjjym2lyvvmwn53mlsyyzgxg6pxl6swvbyzrfv6vb6rrxahwy")))

