(define-module (crates-io ru st rust_runnables) #:use-module (crates-io))

(define-public crate-rust_runnables-0.1.0 (c (n "rust_runnables") (v "0.1.0") (h "1jgszsr0ihsql7jgd2zk7277qclh63lj72i81xbh19hlxgxh37hj")))

(define-public crate-rust_runnables-0.2.0 (c (n "rust_runnables") (v "0.2.0") (h "0j5s0fbg2xicxrwxp7jc0pia6wf8qzz75f0h0n6cba2kcvmah068")))

