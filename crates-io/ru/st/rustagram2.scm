(define-module (crates-io ru st rustagram2) #:use-module (crates-io))

(define-public crate-rustagram2-2.0.0 (c (n "rustagram2") (v "2.0.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "1kaq10dzrv2lvrsbyy93yqi7m1y33q1i5k0cv5z4cn1krkl3yypz")))

(define-public crate-rustagram2-2.0.1 (c (n "rustagram2") (v "2.0.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "1m8zhq6qp2r70m9yxdk0zj8rgjbi68dnhz0rqkv9xjfyhndknbkg")))

(define-public crate-rustagram2-2.1.0 (c (n "rustagram2") (v "2.1.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "0ckhfq319d9w8aysrk8w88hhdcjf3vvz170wa3rbl3zcbqqvzn6j")))

