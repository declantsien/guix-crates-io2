(define-module (crates-io ru st rustlex_fsa) #:use-module (crates-io))

(define-public crate-rustlex_fsa-0.3.3 (c (n "rustlex_fsa") (v "0.3.3") (d (list (d (n "bit-set") (r ">= 0.3.0") (d #t) (k 0)))) (h "1gczgz7ihcp7zn4iw4342jbds7h6r8z3m4fnfqqavzvpssjriww0") (y #t)))

(define-public crate-rustlex_fsa-0.3.4 (c (n "rustlex_fsa") (v "0.3.4") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)))) (h "1dr9zfx1qklxdkgyc1ac6dfwvsf19lmfjyxcyrmrm4vvcfkvgcw3")))

(define-public crate-rustlex_fsa-0.4.0 (c (n "rustlex_fsa") (v "0.4.0") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)))) (h "0wkyhghgs427sxvccdphciw130vllf78caci4fp7jx39m30xvmbs")))

