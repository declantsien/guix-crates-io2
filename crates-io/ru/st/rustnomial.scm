(define-module (crates-io ru st rustnomial) #:use-module (crates-io))

(define-public crate-rustnomial-0.0.1 (c (n "rustnomial") (v "0.0.1") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "18k08ng48hl4dhrsk1hvv99wgavqnvf6xld5hnjndcq48zrz314h") (f (quote (("default"))))))

(define-public crate-rustnomial-0.0.2 (c (n "rustnomial") (v "0.0.2") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0kgaixp8lviw7p13yvkaanqqa8j6786y3snl8vc0dnxaf2mja5lb") (f (quote (("default"))))))

(define-public crate-rustnomial-0.0.3 (c (n "rustnomial") (v "0.0.3") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "11l0kkzxvxi1m9vcasy72xcc18lhnjl3m50pwv7l9fb2h4fxw5nf") (f (quote (("default"))))))

(define-public crate-rustnomial-0.0.4 (c (n "rustnomial") (v "0.0.4") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "02f99grfiwlxvrbkk4iv9jnz2qy601yxgy7sp0iml355wjz5v5xb") (f (quote (("default"))))))

(define-public crate-rustnomial-0.0.5 (c (n "rustnomial") (v "0.0.5") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0insifrvvxvb1i95qi0bmdia39jlw8ba6h2qngrzz5gida5s6cpq") (f (quote (("default"))))))

(define-public crate-rustnomial-0.0.6 (c (n "rustnomial") (v "0.0.6") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0h8i8kzw0msw0bwspsilrzgpys2675k909wv8xn49xz9cg0x28pj") (f (quote (("default"))))))

(define-public crate-rustnomial-0.0.7 (c (n "rustnomial") (v "0.0.7") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "1ggm4zxai48k0lnh0mwcq54bhs94qgd2yk5iplg22al0yn8mhm6n") (f (quote (("default"))))))

(define-public crate-rustnomial-0.1.0 (c (n "rustnomial") (v "0.1.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "0awg30hih9nnd1c1hczq7jns9w2qh3wlzjzj96sa1cdi6dm8bq9s") (f (quote (("default"))))))

(define-public crate-rustnomial-0.2.0 (c (n "rustnomial") (v "0.2.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "1l1djlw1fpv5w5vjslh0av7bwadvcdy8dhf19f34rbqx0jz7m1xc") (f (quote (("default"))))))

(define-public crate-rustnomial-0.3.0 (c (n "rustnomial") (v "0.3.0") (d (list (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "1l6pwdzw0j5ckpkrfr9njvk388jdygjqv0wsi33ll0sn1ii18c7a") (f (quote (("sparse_poly_trim") ("default") ("array_polynomials"))))))

(define-public crate-rustnomial-0.3.1 (c (n "rustnomial") (v "0.3.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.6") (d #t) (k 0)))) (h "11kcqws4gkv12zpc8pvzfbdfd03l202gn406w17sbgkk91rk0nrg") (f (quote (("sparse_poly_trim") ("default") ("array_polynomials"))))))

(define-public crate-rustnomial-0.3.2 (c (n "rustnomial") (v "0.3.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.7") (d #t) (k 0)))) (h "09dfqcdfv4l6pwd3wwp8dzfr0fr5bv7s5wkvkq39p79zxkqwl7nr") (f (quote (("sparse_poly_trim") ("default") ("array_polynomials"))))))

