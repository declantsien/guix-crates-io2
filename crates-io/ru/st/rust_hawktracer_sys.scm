(define-module (crates-io ru st rust_hawktracer_sys) #:use-module (crates-io))

(define-public crate-rust_hawktracer_sys-0.1.0 (c (n "rust_hawktracer_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mrkvcxfh88p6d580v81yb8r0vwwcg7fncvbfbnh9cxkga2i1z01") (f (quote (("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer_sys-0.2.0 (c (n "rust_hawktracer_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1c3b71sdbjbwsbk9afzfj5g0xm65mj058mkmjm3vlph0if0c8hqv") (f (quote (("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer_sys-0.2.1 (c (n "rust_hawktracer_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1iynhr6z2s1jmwwpaz5mg0715jhf4gqv67xlsrx42x47j2rzi86m") (f (quote (("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer_sys-0.3.0 (c (n "rust_hawktracer_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1dc73ki09pha7qfsd085zh9id3l06imiaaflhm93dsgjbixf360p") (f (quote (("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer_sys-0.4.0 (c (n "rust_hawktracer_sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1jv7myd5c3ljjzk1s2hlhbr47z4wjsszwfq48fn1lzwkp3fkgfpa") (f (quote (("non-cargo") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer_sys-0.4.1 (c (n "rust_hawktracer_sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1v57w74di2b0l4f16cd123rv1s16l5bf23s9kgzgsbj16y8pk8dv") (f (quote (("pkg_config" "pkg-config") ("non-cargo") ("generate_bindings" "bindgen")))) (l "hawktracer")))

(define-public crate-rust_hawktracer_sys-0.4.2 (c (n "rust_hawktracer_sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.37.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "15acrj881y2g7cwsgf1nr22cixrknp8m4x08dkx1an6zf4q8bk37") (f (quote (("pkg_config" "pkg-config") ("non-cargo") ("generate_bindings" "bindgen")))) (l "hawktracer")))

