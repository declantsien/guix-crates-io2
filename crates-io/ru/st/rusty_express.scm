(define-module (crates-io ru st rusty_express) #:use-module (crates-io))

(define-public crate-rusty_express-0.2.0 (c (n "rusty_express") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "12c98lljaq34i6cywai2442s3cqnqn2qsajc207cvxz8zkab01sz")))

(define-public crate-rusty_express-0.2.1 (c (n "rusty_express") (v "0.2.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ixyqj6bh3xqvbymp3aaaw042imqbmzxwv3g510nfw9jgy7fysnl")))

(define-public crate-rusty_express-0.2.2 (c (n "rusty_express") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "09kixwmsqnz02z90w26qsmvmpagi58hn78nfpfmigmbr5r1l83sx")))

(define-public crate-rusty_express-0.2.3 (c (n "rusty_express") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1n89dpgrv1d7z0g0ajzh5k7l8qdm3nz8yy6ickk31yrhjnp0v827")))

(define-public crate-rusty_express-0.2.4 (c (n "rusty_express") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0aqp9p45mibj2aqq892p1qni0ixzj0v85m27b7i8l71yjwzq6c19")))

(define-public crate-rusty_express-0.2.5 (c (n "rusty_express") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1asgqdp35rw4970nvi85xvb43piwmx63yir2qsk88iljp2sgpi93") (y #t)))

(define-public crate-rusty_express-0.2.6 (c (n "rusty_express") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1vrjccgibag1sdp27pfl9xrn93jggw4f0pi2fhl0x12dqhb27ryl")))

(define-public crate-rusty_express-0.2.7 (c (n "rusty_express") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "01v64rswvgymy4ps0ggd6yglk1fg0zla150kcrvss5c9f18r77p8")))

(define-public crate-rusty_express-0.2.8 (c (n "rusty_express") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0mgj2b474178qfhs2p70y3663lzik8527xy3zynx7wpar6mqnb1k")))

(define-public crate-rusty_express-0.2.9 (c (n "rusty_express") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1f9za6kii5xyvn4028hwrqzwcc53ph2gwm9y2c8lyhm2ajrh4x1x") (f (quote (("session") ("default" "session"))))))

(define-public crate-rusty_express-0.3.0 (c (n "rusty_express") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1yfla0kni4858xz78n0cjdb65qcx3vs5si99nzga3nlpswk02ir0") (f (quote (("session") ("default" "session"))))))

(define-public crate-rusty_express-0.3.1 (c (n "rusty_express") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "19lmzypqrjb9fj8bcdb9jvrlmqkvkarijm3xfsl84z8si44c1ayd") (f (quote (("session") ("default" "session"))))))

(define-public crate-rusty_express-0.3.2 (c (n "rusty_express") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1y7366dch454kqglqrhpwnzjmgn232przwh5kg91cmwyi6kdkli9") (f (quote (("session") ("default" "session"))))))

(define-public crate-rusty_express-0.3.3 (c (n "rusty_express") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1cz09b21vsb715c4wmmy3c852g46scn257l68lbs1gg68sv888c5") (f (quote (("session") ("logger") ("default" "session" "logger")))) (y #t)))

(define-public crate-rusty_express-0.3.4 (c (n "rusty_express") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "05bc1fjrxx522m1d89qkbgly74hpyxz9371mirs0lw76rgx0h6qm") (f (quote (("session") ("logger") ("default" "session" "logger"))))))

(define-public crate-rusty_express-0.3.5 (c (n "rusty_express") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1dpafpvg004vf1y5lhj3is76h62y026q3699xj0mdjg4kx3112vr") (f (quote (("session") ("logger") ("default" "session" "logger"))))))

(define-public crate-rusty_express-0.3.6 (c (n "rusty_express") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "09c5wnd2d6brklz9jjyhnmxa9bbmpff2jn1c758sz9w9l8wmz067") (f (quote (("session") ("logger") ("default" "session" "logger"))))))

(define-public crate-rusty_express-0.4.1 (c (n "rusty_express") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1hhc4s517dkn1pkx0gc4s61lvfwn4chagl09nlkkf4cncrasap88") (f (quote (("session") ("logger") ("default" "session" "logger"))))))

(define-public crate-rusty_express-0.4.2 (c (n "rusty_express") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1k48q62mbbprwd01r0w8gph45cjb3r8l4wpcsrivg0i2wjhg6lcw") (f (quote (("session") ("logger") ("default" "session" "logger"))))))

(define-public crate-rusty_express-0.4.3 (c (n "rusty_express") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zhgdayavkdgf6ay160m0kharklg9d1kkrv5hkhqh9f6wg179292") (f (quote (("session") ("logger") ("default" "session" "logger"))))))

