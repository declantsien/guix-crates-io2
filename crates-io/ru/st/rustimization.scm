(define-module (crates-io ru st rustimization) #:use-module (crates-io))

(define-public crate-rustimization-0.1.0 (c (n "rustimization") (v "0.1.0") (d (list (d (n "cg-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lbfgsb-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "05fz581nyjr96vgh055qlfibl54bjs5baz0350p7c0g1dwma08nk")))

(define-public crate-rustimization-0.1.1 (c (n "rustimization") (v "0.1.1") (d (list (d (n "cg-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lbfgsb-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "1zpq24hb83kik51l6dhghk9d4g60daw8yp1795h81iyk4m045851")))

