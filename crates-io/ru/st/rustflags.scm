(define-module (crates-io ru st rustflags) #:use-module (crates-io))

(define-public crate-rustflags-0.1.0 (c (n "rustflags") (v "0.1.0") (h "004in4w3vqn5a49ixa25rd1v7nhs4hmqmb3agsx36kqk0ixswz11") (r "1.56")))

(define-public crate-rustflags-0.1.1 (c (n "rustflags") (v "0.1.1") (h "0zq2s1nanpg8bpi2ij3wkl06bl3wyrq53jlmy3nr799r32wb9dr7") (r "1.56")))

(define-public crate-rustflags-0.1.2 (c (n "rustflags") (v "0.1.2") (h "0aggxgnnxiw7wn0l6q065cjcvwjnlvfz62khmjhc0pmzz4ka5hg5") (r "1.56")))

(define-public crate-rustflags-0.1.3 (c (n "rustflags") (v "0.1.3") (h "0hwvjv0xfzxkgg24vwcmg8sabza4z9c42p2j83s2vixi54mj4pm4") (r "1.56")))

(define-public crate-rustflags-0.1.4 (c (n "rustflags") (v "0.1.4") (h "09n1qq7gpsp8f6w34i864hpcdjkssrn0dpy0lsis7bm1rp01wkz7") (r "1.56")))

(define-public crate-rustflags-0.1.5 (c (n "rustflags") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 2)))) (h "1b1hjdxg2drm5cq2gxbrrjdibn73c6kpbf3468ri97vmrvnfbmp6") (r "1.56")))

(define-public crate-rustflags-0.1.6 (c (n "rustflags") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 2)))) (h "1h1al0xhd9kzy8q8lzw6rxip5zjifxigfrm3blf462mmkwar5z6p") (r "1.74")))

