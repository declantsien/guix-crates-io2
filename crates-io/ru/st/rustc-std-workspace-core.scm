(define-module (crates-io ru st rustc-std-workspace-core) #:use-module (crates-io))

(define-public crate-rustc-std-workspace-core-0.1.0 (c (n "rustc-std-workspace-core") (v "0.1.0") (h "1vqjz6rf5sjhah63qn8wdnvkjh0adjw5axcvqs0rs55nqxz5cg3w")))

(define-public crate-rustc-std-workspace-core-1.0.0 (c (n "rustc-std-workspace-core") (v "1.0.0") (h "1309xhwyai9xpz128xrfjqkmnkvgjwddznmj7brbd8i8f58zamhr")))

