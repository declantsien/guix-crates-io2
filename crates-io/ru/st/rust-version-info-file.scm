(define-module (crates-io ru st rust-version-info-file) #:use-module (crates-io))

(define-public crate-rust-version-info-file-0.1.0 (c (n "rust-version-info-file") (v "0.1.0") (h "18x1mrw5hmf0kkj70m4gixi04aydphxzgmgpn4ywpviz0hf6lswz")))

(define-public crate-rust-version-info-file-0.1.1 (c (n "rust-version-info-file") (v "0.1.1") (h "1g1sxa8hbgn07q1caap9hz862mx74hdrnlk89jx1j5x90dc1sv2d")))

(define-public crate-rust-version-info-file-0.1.2 (c (n "rust-version-info-file") (v "0.1.2") (h "0h23ar2g0wzpg2kqcpryhn5n0bb9sc3kf54xv7ljdccwrk9vjyr5")))

(define-public crate-rust-version-info-file-0.1.3 (c (n "rust-version-info-file") (v "0.1.3") (d (list (d (n "regex") (r "^1.5") (d #t) (k 2)))) (h "12qliz8b1j0qqn7vznp2hd7vk93v9vqwnl8sky5925yq6aymxayk")))

(define-public crate-rust-version-info-file-0.1.4 (c (n "rust-version-info-file") (v "0.1.4") (d (list (d (n "regex") (r "^1.5") (d #t) (k 2)))) (h "1n0s47fx5chf5dmh87lzb62q210zw558b18xwma0zwz359i2lmg5")))

(define-public crate-rust-version-info-file-0.1.5 (c (n "rust-version-info-file") (v "0.1.5") (d (list (d (n "regex") (r "^1.5") (d #t) (k 2)))) (h "0hl6m48yc648g2is7l27b80h3g2ah3jnnifvq987yh0gqkmb2mdc")))

(define-public crate-rust-version-info-file-0.1.6 (c (n "rust-version-info-file") (v "0.1.6") (d (list (d (n "regex") (r "^1.5") (d #t) (k 2)))) (h "0awjb1px3yppi71cmx85s62y5kb2m21fn8xsd8nk9xjf4y0kli0q")))

(define-public crate-rust-version-info-file-0.1.7 (c (n "rust-version-info-file") (v "0.1.7") (d (list (d (n "regex") (r "^1.7") (d #t) (k 2)))) (h "0dw8a1ida02ycfk2dxxj76qhbxjgb7cr4yhjcffxk80l5k92s22i")))

(define-public crate-rust-version-info-file-0.1.8 (c (n "rust-version-info-file") (v "0.1.8") (d (list (d (n "regex") (r "^1.7") (d #t) (k 2)))) (h "1j1d73myrspjkpvmq1n0j858y35d2krhrmfzragzfhgrfi4d75pf") (r "1.58.0")))

(define-public crate-rust-version-info-file-0.1.9 (c (n "rust-version-info-file") (v "0.1.9") (d (list (d (n "regex") (r "^1.7") (d #t) (k 2)))) (h "18zmd5f9c7mbyy5rc449y9dj0qqms45s47ircjf9a4fcc720f91b") (r "1.58.0")))

