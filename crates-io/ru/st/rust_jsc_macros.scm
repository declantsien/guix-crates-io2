(define-module (crates-io ru st rust_jsc_macros) #:use-module (crates-io))

(define-public crate-rust_jsc_macros-0.1.0 (c (n "rust_jsc_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1vm9zm171pimcvip1lx6sy71fzg4wwcwhav1nv2bhlk46s3pbv0h")))

(define-public crate-rust_jsc_macros-0.1.2 (c (n "rust_jsc_macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "1nk01i31h33yabjn6m6f3qy68fhmnwnkd4vzam7bpp0wg6am2g1q")))

(define-public crate-rust_jsc_macros-0.1.3 (c (n "rust_jsc_macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "19ws00yqqg78zmjhcyjk9ks8fmmgzak2bx1zsi82rrzxp92jx8s1")))

(define-public crate-rust_jsc_macros-0.1.4 (c (n "rust_jsc_macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "0ab8q2z4lwv60fab5jklqswllrfp3w0vadx9bvcyd49cj2ymqg1y")))

