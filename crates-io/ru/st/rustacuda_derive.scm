(define-module (crates-io ru st rustacuda_derive) #:use-module (crates-io))

(define-public crate-rustacuda_derive-0.1.0 (c (n "rustacuda_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08plsbiin9n4f6l8i3wxpvs3j8bjiia61xjbshfcfq36mf805r2s")))

(define-public crate-rustacuda_derive-0.1.1 (c (n "rustacuda_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qh6iah802svfhnqza7vyxlzx45dm4b8jrlx9cha1cckn8l9d1zv")))

(define-public crate-rustacuda_derive-0.1.2 (c (n "rustacuda_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zmx63j4rfxcrxs6cjs9ppwlcmmnrpd4df532hjzrl51l5q8dkj3")))

