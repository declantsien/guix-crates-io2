(define-module (crates-io ru st rust-sctp) #:use-module (crates-io))

(define-public crate-rust-sctp-0.0.1 (c (n "rust-sctp") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sctp-sys") (r "*") (d #t) (k 0)))) (h "1lbnb40bj9n8xf6x5fwj7hy3zqx56a91bari0h4lwxdplvicqz36")))

(define-public crate-rust-sctp-0.0.2 (c (n "rust-sctp") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sctp-sys") (r "*") (d #t) (k 0)))) (h "158gfch8pi6fs78izbq6l4ramcfzlj8dsrrx2p7i15ah6gr1davn")))

(define-public crate-rust-sctp-0.0.3 (c (n "rust-sctp") (v "0.0.3") (d (list (d (n "libc") (r "0.1.*") (d #t) (k 0)) (d (n "sctp-sys") (r "*") (d #t) (k 0)))) (h "02lvfpk2bb1i9x0kf84xidmgj5xfcz1ypq1lzryprrlngi3i6ydx")))

(define-public crate-rust-sctp-0.0.4 (c (n "rust-sctp") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sctp-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "0jkp8r8slg6y7my0xfmlch8ywpj7kl1bbxm7jb7b1fjsfadswqmh")))

(define-public crate-rust-sctp-0.0.5 (c (n "rust-sctp") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sctp-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "0qpbcvjgkzvywxivzpkfzlv235wmgkyqdr7kab3xm4c7mzwdmlh3")))

