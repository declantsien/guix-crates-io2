(define-module (crates-io ru st rust-template-generated-bin) #:use-module (crates-io))

(define-public crate-rust-template-generated-bin-0.1.0 (c (n "rust-template-generated-bin") (v "0.1.0") (h "13f1zwjvfl2g5ac6pf2b162j1mj0rhbib8x63pb74irfmf9b16ki") (r "1.56.1")))

(define-public crate-rust-template-generated-bin-0.2.0 (c (n "rust-template-generated-bin") (v "0.2.0") (h "0jbh0p1fi4ljwygs955i0q7f1bpkxmybsfa9npfdvm92mz0igjpv") (r "1.59.0")))

(define-public crate-rust-template-generated-bin-0.3.0 (c (n "rust-template-generated-bin") (v "0.3.0") (h "055xdahx8h13gkaq773g0393jdapajyncl35kg5sg8pa88mdh3yc") (r "1.59.0")))

(define-public crate-rust-template-generated-bin-0.3.1 (c (n "rust-template-generated-bin") (v "0.3.1") (h "08b9cqvs13qcr95dfi92m7shq64f1sx0x2674cijd6ipl09xczmk") (r "1.59.0")))

(define-public crate-rust-template-generated-bin-0.3.2 (c (n "rust-template-generated-bin") (v "0.3.2") (h "072dbcv15dhcind485av5svzzg1frahsnql7fpc1i9474zq1hv13") (r "1.59.0")))

(define-public crate-rust-template-generated-bin-0.3.3 (c (n "rust-template-generated-bin") (v "0.3.3") (h "1cbavnikji7l9ny4bavj67mkl5k0hv6wmq0z6rzg5hfxc29r79ha") (r "1.59.0")))

(define-public crate-rust-template-generated-bin-0.3.4 (c (n "rust-template-generated-bin") (v "0.3.4") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cnwqf0gmicpz1zvfan20pzmwkj51xhrayqqgrhpi4isrip18vac") (r "1.59.0")))

(define-public crate-rust-template-generated-bin-0.3.5 (c (n "rust-template-generated-bin") (v "0.3.5") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "05n762sxlfw264hm1cs9w8sq8ifqj1cqwznpm3z2xnyxg6yp9rzd") (r "1.59.0")))

