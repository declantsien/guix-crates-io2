(define-module (crates-io ru st rusty-search) #:use-module (crates-io))

(define-public crate-rusty-search-0.1.0 (c (n "rusty-search") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 2)) (d (n "predicates") (r "^3.0.1") (d #t) (k 2)))) (h "0fs3fxcnm9am10n3glvssp5bhv0pm3gi7pf84kdcakxjvhjrb4l0")))

