(define-module (crates-io ru st rustssh) #:use-module (crates-io))

(define-public crate-rustssh-0.1.0 (c (n "rustssh") (v "0.1.0") (d (list (d (n "ssh2") (r "~0.9") (d #t) (k 0)))) (h "1yng01sclmirclp93znal4zpjjs9aqsc8qi82kwqngj2mcvrr993")))

(define-public crate-rustssh-0.1.1 (c (n "rustssh") (v "0.1.1") (d (list (d (n "ssh2") (r "~0.9") (d #t) (k 0)))) (h "0hpps7k2gxnhb3bbhh9kf6nm58bglhnhxhl42jn9b2786y5lxa85")))

(define-public crate-rustssh-0.1.2 (c (n "rustssh") (v "0.1.2") (d (list (d (n "ssh2") (r "~0.9") (d #t) (k 0)))) (h "1g8bapa0m357v3119x3dl07n66kj9nvaw60q9q2y0hpgs5xigmcf")))

