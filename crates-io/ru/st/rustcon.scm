(define-module (crates-io ru st rustcon) #:use-module (crates-io))

(define-public crate-rustcon-0.1.0 (c (n "rustcon") (v "0.1.0") (d (list (d (n "array-bytes") (r "^1.5.1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)))) (h "14arj63kfn7gxn5b3b0gl6hhg3kpsnb8s5bnl6hxr5zdvm3sbvz1") (r "1.65")))

