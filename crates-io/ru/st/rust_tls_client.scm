(define-module (crates-io ru st rust_tls_client) #:use-module (crates-io))

(define-public crate-rust_tls_client-0.1.0 (c (n "rust_tls_client") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hcfs86llbc7wq73i86y1mdzh5dpq2lymn3wlsc41yqzm0n1d2k5") (f (quote (("ubuntu") ("alpine"))))))

