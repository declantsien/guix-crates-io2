(define-module (crates-io ru st rust-crate-grrs-jesse) #:use-module (crates-io))

(define-public crate-rust-crate-grrs-jesse-0.1.0 (c (n "rust-crate-grrs-jesse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sgsvdm7a2lgq4lw13gj36gc247dcx0jvsfp3fw349i2ls767x07")))

(define-public crate-rust-crate-grrs-jesse-0.1.1 (c (n "rust-crate-grrs-jesse") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "0kc5aggbg0hvzshgp6fzkcwcmd1wr42lvpfq7rlgbjxhgj4nw2ca") (y #t)))

