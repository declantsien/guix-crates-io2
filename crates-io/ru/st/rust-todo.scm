(define-module (crates-io ru st rust-todo) #:use-module (crates-io))

(define-public crate-rust-todo-0.1.0 (c (n "rust-todo") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p7vwrlnyxfz0rdljm03crhfiq2k2xhnk5ikz4jl4kblklcgrac5")))

(define-public crate-rust-todo-0.2.0 (c (n "rust-todo") (v "0.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k690611acdnlfnfrgl71ppsb6dk8y4qfk3dspw7hgyya8vmsvhk")))

