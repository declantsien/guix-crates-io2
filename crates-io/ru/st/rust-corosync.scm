(define-module (crates-io ru st rust-corosync) #:use-module (crates-io))

(define-public crate-rust-corosync-0.1.0 (c (n "rust-corosync") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0f9hl45mbhvdd59cb37fypsz6pyhi28gzgh4x113lbcq5d9jmj3m")))

(define-public crate-rust-corosync-0.2.0 (c (n "rust-corosync") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1spw5xsp0pg0mflkbkp6adf7pa5xvs91wb609fmwpzpjnhxzq4nr")))

(define-public crate-rust-corosync-0.2.1 (c (n "rust-corosync") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0a634fb7w9sv6971ij00s7v3vbsgg8d2s2zb7w07hyfhrz98k6mw")))

(define-public crate-rust-corosync-0.2.2 (c (n "rust-corosync") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)))) (h "062c5xsbw7hkf9jzm7hczbd99kfq82i4zpr6yxhxp6wa5hsbjapj")))

(define-public crate-rust-corosync-0.2.3 (c (n "rust-corosync") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)))) (h "17w0dzr7j6x1xjrqn9dmhnvb8d9r488v660kfx8hfz92w81s3z2m")))

(define-public crate-rust-corosync-0.2.4 (c (n "rust-corosync") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)))) (h "0hhzbki95f4jns8qqr9q9xz4y3wk7c4lnki2cx2pch3hgwdyq02b")))

(define-public crate-rust-corosync-3.1.8-2-2fcda (c (n "rust-corosync") (v "3.1.8-2-2fcda") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)))) (h "0crpqgqaslz6k5yjff1zac06qm9khzknwrxmwzb2z3rnd5idlf3i")))

