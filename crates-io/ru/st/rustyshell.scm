(define-module (crates-io ru st rustyshell) #:use-module (crates-io))

(define-public crate-rustyshell-0.1.0 (c (n "rustyshell") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "console") (r "^0.11.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.3.1") (d #t) (k 0)))) (h "14bf4b6f4l6j0if5xln57441fl1gb4bm1n04pnj2lymcpj1g3b6j")))

