(define-module (crates-io ru be rubedo-macros) #:use-module (crates-io))

(define-public crate-rubedo-macros-0.1.0 (c (n "rubedo-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1bkcjzx1qjxa29qll43q9ahn2589mnam0dalz88r6i8lzgbbma5w")))

(define-public crate-rubedo-macros-0.1.1 (c (n "rubedo-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0d4ra8brsrjvwmnswazibrhpvknyfwkbf33xn56hpd3wdyd2652r")))

(define-public crate-rubedo-macros-0.2.0 (c (n "rubedo-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0zk921ldfkyd6hazk0ycqxd1vzf03fqz9kqgk3mfcpzdjrhg5lfm")))

(define-public crate-rubedo-macros-0.3.0 (c (n "rubedo-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "07nsv2np21b7sqljliprngrpl9frm4fd7i80bzd5m1q8da18lq4l")))

(define-public crate-rubedo-macros-0.3.1 (c (n "rubedo-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "11z8x88k9i3wrg48fw74vj9826g3qslz6q38yxks2nkqx1jlccyy")))

(define-public crate-rubedo-macros-0.3.2 (c (n "rubedo-macros") (v "0.3.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "150p3kj0fv5rpg2fcn8arzyycb5rd72h9kq4zsxdxnmfq5jlb79g")))

(define-public crate-rubedo-macros-0.3.3 (c (n "rubedo-macros") (v "0.3.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0x4cfkwxz0qqf2p6y09dihls09yls79z6ggnzprlk1a2m6j57pll")))

(define-public crate-rubedo-macros-0.3.4 (c (n "rubedo-macros") (v "0.3.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1wf41p4qfqlxd6qv4x222d5wc1iwbal87dd3i2jha4p2bxc3crc1")))

(define-public crate-rubedo-macros-0.3.5 (c (n "rubedo-macros") (v "0.3.5") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "18bwzkj7773927mmpfkzph969c15rfqgw3flpdkx18dcnrirz5c1")))

(define-public crate-rubedo-macros-0.3.6 (c (n "rubedo-macros") (v "0.3.6") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1fkqdsb7vfafqs4ybni6gaa372v1clfq7psa8sbmck3ic089pvv7")))

(define-public crate-rubedo-macros-0.4.0 (c (n "rubedo-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1xxpjhfs0gh0b3j9kiqfjwjyn8nln3jgky1c0sgiyr2gaax8mwrw")))

(define-public crate-rubedo-macros-0.4.1 (c (n "rubedo-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1ds027za5wsrnq1nm72q3iwbwfj5ff4n8kb94ypnqfsyd4xjvf8w")))

(define-public crate-rubedo-macros-0.4.2 (c (n "rubedo-macros") (v "0.4.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1xw4n8jk4gj6k4xi86wvz31gkhsi8svql4l6i5xd0amhq4kyhvvw")))

(define-public crate-rubedo-macros-0.4.3 (c (n "rubedo-macros") (v "0.4.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1rzd1gm6n78k080jbyw7f376lkfw4imfy6rs4y7py4h1gpp9r1fi")))

(define-public crate-rubedo-macros-0.4.4 (c (n "rubedo-macros") (v "0.4.4") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1vbvr7bcx1kpz8ma837j2034f0gjlsm3dzgwbr740vqgm6n1q3dc")))

(define-public crate-rubedo-macros-0.4.5 (c (n "rubedo-macros") (v "0.4.5") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "0pps8hk3vxjyzbpq2875f2pvs56c3i7i4kjazl78rxyd6cmh3m8j")))

(define-public crate-rubedo-macros-0.4.6 (c (n "rubedo-macros") (v "0.4.6") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "0xcsk0rf2zn27lsvrzp8hk6mn4w4n97w4kqrac62byhh70zzsdjw")))

(define-public crate-rubedo-macros-0.4.7 (c (n "rubedo-macros") (v "0.4.7") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "13k6p34qa4a865liyi1fdbvs29lkcw495mhpxnh427x7zn5zbyk5")))

(define-public crate-rubedo-macros-0.5.0 (c (n "rubedo-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "009bxcpd93ixqbv5jw7w2hyyvhsmj9zr7zsns29rw8l2bmr94jfq")))

(define-public crate-rubedo-macros-0.5.1 (c (n "rubedo-macros") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "06qby6fnccbgg6r1rq9hig80dna31lm88prqr68cwp422drqin4m")))

(define-public crate-rubedo-macros-0.5.2 (c (n "rubedo-macros") (v "0.5.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "0siig229qshp2lhpm50fmj32msnj9x23cxyhijz9kz5rps9rq91s")))

(define-public crate-rubedo-macros-0.5.3 (c (n "rubedo-macros") (v "0.5.3") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "1wkisxih5zx0s2hxbccpmkl7hc2mdxrj7scxm1aplgssqm2dfq74")))

