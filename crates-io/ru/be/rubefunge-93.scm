(define-module (crates-io ru be rubefunge-93) #:use-module (crates-io))

(define-public crate-rubefunge-93-0.0.1 (c (n "rubefunge-93") (v "0.0.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1nfxj3nlfvpxcp3ybs7d2p3zpx9cr9x5ml7mi39whnq1rbqp38xf")))

(define-public crate-rubefunge-93-0.0.2 (c (n "rubefunge-93") (v "0.0.2") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1qga4d5pphlwm03c1pmlmwkcbikz37g5852jq424ybc5jisafa02")))

(define-public crate-rubefunge-93-0.0.3 (c (n "rubefunge-93") (v "0.0.3") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1sari0riskbnj646i0spgnyn20jvh0pi5aniknhzb5sgfdgmy1nz")))

