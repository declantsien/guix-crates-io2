(define-module (crates-io ru si rusic) #:use-module (crates-io))

(define-public crate-rusic-0.0.2 (c (n "rusic") (v "0.0.2") (d (list (d (n "async_command") (r "^0.2.0") (d #t) (k 0)) (d (n "rusting") (r "^0.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "17in6wzdpb2jwl9l8w4653304pdyisx8wp7avhf7408jskjzyr5s")))

(define-public crate-rusic-0.0.3 (c (n "rusic") (v "0.0.3") (d (list (d (n "async_command") (r "^0.2.0") (d #t) (k 0)) (d (n "rusting") (r "^0.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1vvl2w842yzj9ngvdrq12rq4nnvsnhkqkqjm4hbc8qsqdqvydvsj")))

(define-public crate-rusic-0.0.4 (c (n "rusic") (v "0.0.4") (d (list (d (n "async_command") (r "^0.2.0") (d #t) (k 0)) (d (n "rusting") (r "^0.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0a5v86vgqggl52g8zwdvzrknc3x4cbqfvfj6safwrk67y99qmw83")))

(define-public crate-rusic-0.0.5 (c (n "rusic") (v "0.0.5") (d (list (d (n "async_command") (r "^0.2.0") (d #t) (k 0)) (d (n "rusting") (r "^0.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1wpd8ir5kl9w8913nslkn041a1zkr9bdk9vnsl33g6pkpg7gxsvr")))

