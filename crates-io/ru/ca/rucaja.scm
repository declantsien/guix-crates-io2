(define-module (crates-io ru ca rucaja) #:use-module (crates-io))

(define-public crate-rucaja-0.1.0 (c (n "rucaja") (v "0.1.0") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0l39hl29wj62qzkbg3skmbq53bmrgjsgac6dxv7c4ri8q9j98jbd")))

(define-public crate-rucaja-0.1.1 (c (n "rucaja") (v "0.1.1") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0ls9k6qxg2xmk83srhj99v3djchl48xidnkwifcqd8a53afv9kw2")))

(define-public crate-rucaja-0.1.2 (c (n "rucaja") (v "0.1.2") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "10wx2h4xk38fgaysn294fp1vwjx1w0xl2s4kdjcz2qp6ng7crvsw")))

(define-public crate-rucaja-0.1.3 (c (n "rucaja") (v "0.1.3") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0i2kiaj366j649k2gn3zakmwbqppcndq0ddgyafmxwbafpaa395h")))

(define-public crate-rucaja-0.1.4 (c (n "rucaja") (v "0.1.4") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0q3ilgbir47i3za9cwhdbzm27slyhaaiyq8fpll0mfxk4ayj0c5x")))

(define-public crate-rucaja-0.1.5 (c (n "rucaja") (v "0.1.5") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1knf10bd0kwfdx769ljqib6ijyk35r34w6xalk51aykp3q4l361j")))

(define-public crate-rucaja-0.1.6 (c (n "rucaja") (v "0.1.6") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "180g65rigdk5nw9k3if88dm4qfy5h6nn7c79na4wmsk5w3cyv2bi")))

(define-public crate-rucaja-0.1.7 (c (n "rucaja") (v "0.1.7") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "16052x4231fr2kajqdla3dcgpz573ayk1x5c1pv6l38d2nlz2flp")))

(define-public crate-rucaja-0.1.8 (c (n "rucaja") (v "0.1.8") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "08qxfzrs5jpgyqqj4y6bwdn7v65nvz830qg5skqqw3zk9ls7am8y")))

(define-public crate-rucaja-0.1.9 (c (n "rucaja") (v "0.1.9") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0n4v7am6s6wfq64mvcanrxr2ks3ljakphf684f7jzf4fnkyjksz8")))

(define-public crate-rucaja-0.2.0 (c (n "rucaja") (v "0.2.0") (d (list (d (n "jni-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0pyqyrh9721l8nxshhvaz0j8ms6rwq4in0y2rlcb046g1izf9kwn")))

(define-public crate-rucaja-0.3.0 (c (n "rucaja") (v "0.3.0") (d (list (d (n "jni-sys") (r "^0.2.2") (d #t) (k 0)))) (h "14gx68daaa63nnnn63hv34li8bwxmhjn9ml1zi0rdylfllf1b7ck")))

(define-public crate-rucaja-0.3.1 (c (n "rucaja") (v "0.3.1") (d (list (d (n "jni-sys") (r "^0.2.2") (d #t) (k 0)))) (h "14s55wh8kwqbis14z73js9fq69aswy6a1jmwknfd7hrdq3r6f1h2")))

(define-public crate-rucaja-0.3.2 (c (n "rucaja") (v "0.3.2") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0yhwxg96r3i5z9d8h71ab0br448y3dg6nakla3w09wzf2vy6yprv")))

(define-public crate-rucaja-0.3.3 (c (n "rucaja") (v "0.3.3") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "18qi7h819y09rhzmyg3dvql708jms9xnkpix8mzzqxn9n1az0wvj")))

(define-public crate-rucaja-0.4.0 (c (n "rucaja") (v "0.4.0") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "14f6y3hw4a9lrywcll5bl0mywsnxaf7rbq8s2mwmar84dgqkdi08")))

(define-public crate-rucaja-0.4.1 (c (n "rucaja") (v "0.4.1") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "12bbms6kk0bky5cyaz4ik8gp1xb3lbcwbpx9khbgs7mh20i1dk1y")))

(define-public crate-rucaja-0.4.2 (c (n "rucaja") (v "0.4.2") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0ikzskxa6qiw7nn43v44b0rd8208xafz7qi6wh5535hxknmbykni")))

(define-public crate-rucaja-0.4.3 (c (n "rucaja") (v "0.4.3") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1j4a56ldhhp0lvl4rfn8lg89jwr1vb03l041kr8i9aqlamaxndca")))

(define-public crate-rucaja-0.4.5 (c (n "rucaja") (v "0.4.5") (d (list (d (n "jni-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0dx3bfira4z5mirl93984sl4z6p9mcll70xvw02dy4n9x9lrcmnf")))

