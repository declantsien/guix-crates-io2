(define-module (crates-io ru _f ru_fetch) #:use-module (crates-io))

(define-public crate-ru_fetch-0.1.0 (c (n "ru_fetch") (v "0.1.0") (d (list (d (n "sysinfo") (r "^0.16.1") (d #t) (k 0)))) (h "0qg1wpfrf99qvq1q80dcgrwfvca2xpjz7vjiwk7c97966f3icvs1")))

(define-public crate-ru_fetch-0.1.1 (c (n "ru_fetch") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.1") (d #t) (k 0)))) (h "1p86xh7hnzdcwwgbz1dj68jgkwvfbar0cyfpqpiy35nwsnjppi60")))

(define-public crate-ru_fetch-0.2.0 (c (n "ru_fetch") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "09hjfczqjd7sl92hxncw96jfal58nsq5saa8mbzg7qbwql7d2k52")))

(define-public crate-ru_fetch-0.2.1 (c (n "ru_fetch") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1bkfm5wglbyc4vpqkq0g3jf84vm012jz9xa7j96af9phwjk5074c")))

(define-public crate-ru_fetch-0.2.2 (c (n "ru_fetch") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.16.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "13ns5b0x1z05lxam5pvwb15i3gi9mzgx0f1j7cp16xg508w8wk13")))

(define-public crate-ru_fetch-0.2.3 (c (n "ru_fetch") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0239w830ah2dhz51a3rkg2sfi1wb2g83k64nm9kg66x2z1yvjjyh")))

(define-public crate-ru_fetch-0.2.4 (c (n "ru_fetch") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.18.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1v4mhakh1s1mkg7xdx8h62qbdn0piz7r894v8ql5pyb3fx34p457")))

