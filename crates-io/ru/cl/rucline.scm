(define-module (crates-io ru cl rucline) #:use-module (crates-io))

(define-public crate-rucline-0.1.0 (c (n "rucline") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 2)) (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "pwner") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d2h1x37yk5g0csidcbp34i0zmizb8kh44n7809088n1v5b9finx") (f (quote (("serialize" "serde" "crossterm/serde") ("default"))))))

(define-public crate-rucline-0.2.1 (c (n "rucline") (v "0.2.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 2)) (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "pwner") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cc2v4lfrjawjbqgm53bj12pi6d1ygl6nrsp3zm52v4bkb6k055y") (f (quote (("serialize" "serde" "crossterm/serde") ("default"))))))

(define-public crate-rucline-0.2.2 (c (n "rucline") (v "0.2.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 2)) (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "pwner") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16if4f9zmvjlh8qx7k7bxlvs9f1m9v1vbrl4ac9kbimq9dlnbx3n") (f (quote (("serialize" "serde" "crossterm/serde") ("default"))))))

(define-public crate-rucline-0.3.0 (c (n "rucline") (v "0.3.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 2)) (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "pwner") (r "^0.1") (d #t) (k 2)) (d (n "quit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ic78krajmca6frwz2n6sbs4w0f0az2lqj2rhnb9a8dyh0mxn5xn") (f (quote (("serialize" "serde" "crossterm/serde") ("default"))))))

(define-public crate-rucline-0.3.1 (c (n "rucline") (v "0.3.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 2)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "pwner") (r "^0.1") (d #t) (k 2)) (d (n "quit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "05xm8is1qs0bab2a8d03zk96a5p36djannfc6h8aa4xyfp3xf3i5") (f (quote (("serialize" "serde" "crossterm/serde") ("default"))))))

(define-public crate-rucline-0.4.0 (c (n "rucline") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "pwner") (r "^0.1") (d #t) (k 2)) (d (n "quit") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "09nfsgb7x643pivmrq0br4v979r0k60ynbxs9rzg3vykvhy52dyk") (f (quote (("serialize" "serde" "crossterm/serde") ("default"))))))

