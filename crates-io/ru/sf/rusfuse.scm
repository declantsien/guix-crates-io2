(define-module (crates-io ru sf rusfuse) #:use-module (crates-io))

(define-public crate-rusfuse-0.0.1 (c (n "rusfuse") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1absjjj3a5gd5452mvvchk4hssf5bfcpb8468svw0rzyqdl2k0nq") (y #t)))

(define-public crate-rusfuse-0.0.2 (c (n "rusfuse") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0014vmikddd3m2dcw9gi1j7ivy32vkv4r3k6zxrw3gm5qipc79k8") (y #t)))

(define-public crate-rusfuse-0.0.3 (c (n "rusfuse") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0sqlda622xfh20i9d3l3pg66xdnvia1civ237br9w8fh85d6s6pl") (y #t)))

(define-public crate-rusfuse-0.0.4 (c (n "rusfuse") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "1nwmn4zkgdfh52sss1rjjqwyav9b30whqdjs01ifvsn32p7waw70")))

(define-public crate-rusfuse-0.0.9 (c (n "rusfuse") (v "0.0.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (f (quote ("max_level_debug" "release_max_level_error"))) (d #t) (k 0)))) (h "1cqzaj11svfw7cci9f9gy07fw7v6ykmy4x5d6vqsjy67vmw0wmkw")))

