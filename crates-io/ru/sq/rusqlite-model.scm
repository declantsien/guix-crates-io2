(define-module (crates-io ru sq rusqlite-model) #:use-module (crates-io))

(define-public crate-rusqlite-model-0.1.0 (c (n "rusqlite-model") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.24") (d #t) (k 0)) (d (n "rusqlite-model-derive") (r "^0.1") (d #t) (k 0)))) (h "0cr0kkky72dbbwy5jkwc4wdh94v67d0vm1j7igizk6wnnrx3xmhx")))

