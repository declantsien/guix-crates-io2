(define-module (crates-io ru sq rusqlite-macros) #:use-module (crates-io))

(define-public crate-rusqlite-macros-0.1.0 (c (n "rusqlite-macros") (v "0.1.0") (d (list (d (n "fallible-iterator") (r "^0.3") (d #t) (k 0)) (d (n "sqlite3-parser") (r "^0.12") (f (quote ("YYNOERRORRECOVERY"))) (k 0)))) (h "0bbgnp7pgk358js63666ix6a9p4fr4lgigaaf4av3aj45qksi4ix")))

(define-public crate-rusqlite-macros-0.2.0 (c (n "rusqlite-macros") (v "0.2.0") (d (list (d (n "fallible-iterator") (r "^0.3") (d #t) (k 0)) (d (n "litrs") (r "^0.4") (k 0)) (d (n "sqlite3-parser") (r "^0.12") (f (quote ("YYNOERRORRECOVERY"))) (k 0)))) (h "07y5887q4via4k7hdfxh61nmcwwz8r0bqlgxrk1p177lrkgz8cdp")))

