(define-module (crates-io ru sq rusqlite-from-row-derive) #:use-module (crates-io))

(define-public crate-rusqlite-from-row-derive-0.1.0 (c (n "rusqlite-from-row-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (d #t) (k 0)))) (h "090f2rhjs2ylywxg4fb0qchjkl7k8bp5wakci82hhf2nqp4hvhsi")))

(define-public crate-rusqlite-from-row-derive-0.2.0 (c (n "rusqlite-from-row-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "05njx7lbv470infxwsvp555dfhrq8zbsflhk6fsjbphslx0izmk8")))

(define-public crate-rusqlite-from-row-derive-0.2.1 (c (n "rusqlite-from-row-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1v2cn0iwd3f1sm8flvgibd6fkfyn92ki38k5r0p24gmm9kwxdllq")))

(define-public crate-rusqlite-from-row-derive-0.2.2 (c (n "rusqlite-from-row-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "16vfziw4k0m513rqlj9mc0v9gv9yc82qlrq78dyw1z1yyv8mvzbj")))

(define-public crate-rusqlite-from-row-derive-0.2.3 (c (n "rusqlite-from-row-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0v1s5v0ikzqg2mq93xmvpxbhp349bs37cw7gkv0djg0avxw7b5yr")))

(define-public crate-rusqlite-from-row-derive-0.2.4 (c (n "rusqlite-from-row-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1wc54qn4rrgli7n2l697g7j1ixij1sclbs16glzscajrjykbg8vk")))

