(define-module (crates-io ru sq rusqlite-from-row) #:use-module (crates-io))

(define-public crate-rusqlite-from-row-0.1.0 (c (n "rusqlite-from-row") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "rusqlite-from-row-derive") (r "=0.1.0") (d #t) (k 0)))) (h "17mbj5ianig5i4j2zm0gs6algllavlc8qprk4crdqgc86rjq3y7h")))

(define-public crate-rusqlite-from-row-0.2.0 (c (n "rusqlite-from-row") (v "0.2.0") (d (list (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "rusqlite-from-row-derive") (r "=0.2.0") (d #t) (k 0)))) (h "1xhfkjz7m0skw9rbsr8v7yhh31lly2nw3znpv4q06ag4kflbmkjx")))

(define-public crate-rusqlite-from-row-0.2.1 (c (n "rusqlite-from-row") (v "0.2.1") (d (list (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "rusqlite-from-row-derive") (r "=0.2.1") (d #t) (k 0)))) (h "0valfc1x2ljk7sg3xxfqg2hgl5x4bh32mkkhq915whxgc23lp4w0")))

(define-public crate-rusqlite-from-row-0.2.2 (c (n "rusqlite-from-row") (v "0.2.2") (d (list (d (n "rusqlite") (r ">=0.27, <=0.30") (d #t) (k 0)) (d (n "rusqlite-from-row-derive") (r "=0.2.2") (d #t) (k 0)))) (h "00wgsdksih158j80l8qqskqy8xikgadnncyzkdn6vhh5s8rcay64")))

(define-public crate-rusqlite-from-row-0.2.3 (c (n "rusqlite-from-row") (v "0.2.3") (d (list (d (n "rusqlite") (r ">=0.27, <=0.30") (d #t) (k 0)) (d (n "rusqlite-from-row-derive") (r "=0.2.3") (d #t) (k 0)))) (h "1448l8sxrhi5axnq2qrhnqchhc45jq79y5p4n6jlvk655ylwvci1")))

(define-public crate-rusqlite-from-row-0.2.4 (c (n "rusqlite-from-row") (v "0.2.4") (d (list (d (n "rusqlite") (r ">=0.27, <=0.31") (d #t) (k 0)) (d (n "rusqlite-from-row-derive") (r "=0.2.4") (d #t) (k 0)))) (h "0rdzxi2p9fgl7ch1mhhpwp52rmr96hhji20asfcp8v3pawl53hag")))

