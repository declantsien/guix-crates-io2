(define-module (crates-io ru sq rusqbin_lib) #:use-module (crates-io))

(define-public crate-rusqbin_lib-0.1.2 (c (n "rusqbin_lib") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9.14") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.4") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rnfqcjpwil1qd1ma8dqxj0h2ga6pvmm3qwb7k5qfvanly5arx73")))

