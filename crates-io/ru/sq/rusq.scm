(define-module (crates-io ru sq rusq) #:use-module (crates-io))

(define-public crate-rusq-0.1.0 (c (n "rusq") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1asbpj99471yssqqjk0nzrv2cmcvk5h3fxij24n8gjsr7dqawh7p")))

(define-public crate-rusq-0.1.1 (c (n "rusq") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1wwrb8ig00166g21mxnkl0s8vc6dyi18z5xr90lkyv3mbi37108p")))

