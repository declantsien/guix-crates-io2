(define-module (crates-io ru te rute) #:use-module (crates-io))

(define-public crate-rute-0.0.1 (c (n "rute") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1rwjigqp97684sspa86r71f6h64m5jrmjc0vq6vz0xka1r4aaws4")))

(define-public crate-rute-0.0.2 (c (n "rute") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0q2hl7amjgcb8i0lr5yqjx5inqdbddzjwvp73jmg6gg985aycdpw") (f (quote (("plugin-compatible"))))))

(define-public crate-rute-0.0.3 (c (n "rute") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1lngk2wvww2a6lmn4n59assayp5gmcqv7823akn558wsz6jmkq0i") (f (quote (("plugin-compatible"))))))

(define-public crate-rute-0.0.4 (c (n "rute") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1c6f7kdjrp55s7dfcvazv0dj2ipknb0zhzri86cywwmz0ccvzxvx") (f (quote (("plugin-compatible"))))))

(define-public crate-rute-0.0.5 (c (n "rute") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0l4x709iycw4f54lfx741kpjv41sx3a3akfzq4jd5i9ssq4l0map") (f (quote (("plugin-compatible"))))))

(define-public crate-rute-0.0.6 (c (n "rute") (v "0.0.6") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "04jckc9w2kn85srppd4y15nw3rrq1niwps0dwlc5rw5jdiiy0hnc") (f (quote (("plugin-compatible"))))))

