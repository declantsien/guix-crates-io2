(define-module (crates-io ru te rutenspitz) #:use-module (crates-io))

(define-public crate-rutenspitz-0.1.1 (c (n "rutenspitz") (v "0.1.1") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sjlswq8w4njqnx66qrkp57x2ivwwcqj7kg3ayc2hgwvdkghvq2i")))

(define-public crate-rutenspitz-0.1.2 (c (n "rutenspitz") (v "0.1.2") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m9ah4vcn23nas0ahjdf4y3y28p84dblw8h1lsgim4c0n1yv5721")))

(define-public crate-rutenspitz-0.2.0 (c (n "rutenspitz") (v "0.2.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vjs4qnjky0iznnllpqvaj3v5alaanm7falirx572lrasph5fm7x")))

(define-public crate-rutenspitz-0.2.1 (c (n "rutenspitz") (v "0.2.1") (d (list (d (n "arbitrary") (r "^0.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rutenspitz_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "1gicq804nkngj2hshwpi4lmyn79ngl61wivdjl7zjgqn39dchbhi")))

(define-public crate-rutenspitz-0.3.0 (c (n "rutenspitz") (v "0.3.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rutenspitz_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0c430xyxsfsklb8l1azwazicz356384jpvw4166glnxzavv6idcb")))

