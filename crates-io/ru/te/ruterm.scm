(define-module (crates-io ru te ruterm) #:use-module (crates-io))

(define-public crate-ruterm-0.2.0 (c (n "ruterm") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "00lbdqdq0nsqanf6x4qv409g4r9sv7fa8176z1gs97b1zf16sym7") (y #t)))

(define-public crate-ruterm-0.2.1 (c (n "ruterm") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "19k9m9f88zisgjqcvpx3q4s4d0rc6imnankhrcp7w0k9gh3mnfsr") (y #t)))

(define-public crate-ruterm-0.2.2 (c (n "ruterm") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0m0cj5icj7irha6iw7ycdw65ndmk94b70wcjj05xc8gfhbgdd99k")))

(define-public crate-ruterm-0.2.3 (c (n "ruterm") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1b2y966vl43a1p95laqfhacxi0gyqqzf6jz1ps2z3xbx26qfja85")))

(define-public crate-ruterm-0.2.4 (c (n "ruterm") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.153") (o #t) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0adf8zxdz9jbhfgv4p9gzllqvnvg7py5jsxvykv31h2qm1fk0m19") (f (quote (("style") ("render") ("default" "render" "style" "size")))) (s 2) (e (quote (("size" "dep:libc"))))))

(define-public crate-ruterm-0.2.5 (c (n "ruterm") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.153") (o #t) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "10sv5pgidifqj7xsd5gp4fhp4yzvlfd5a6xwhcr959k3rfc8fhh4") (f (quote (("style") ("render") ("default" "render" "style" "size")))) (s 2) (e (quote (("size" "dep:libc"))))))

(define-public crate-ruterm-0.2.6 (c (n "ruterm") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.153") (o #t) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "02zv78kxds1p9rnpabvbk0i7kv9h7y6lxx51bjmvplwr7rixn1kv") (f (quote (("view") ("render") ("default" "render" "view" "size")))) (y #t) (s 2) (e (quote (("size" "dep:libc"))))))

(define-public crate-ruterm-0.2.7 (c (n "ruterm") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.153") (o #t) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "12rqb0s8hq1l807zyvdakkzbidbq3w2rl71kmbxfcrqp30ip7949") (f (quote (("view") ("render") ("default" "render" "view" "size")))) (s 2) (e (quote (("size" "dep:libc"))))))

(define-public crate-ruterm-0.3.0 (c (n "ruterm") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.153") (o #t) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0cxx58syaag0pjl7z44gcbkvz0b7kr7krs5hbfh3pp8452mid88y") (f (quote (("view") ("render") ("default" "render" "view" "size")))) (s 2) (e (quote (("size" "dep:libc"))))))

(define-public crate-ruterm-0.3.1 (c (n "ruterm") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.153") (o #t) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1hd2a0dc90b3y600glzcmp5zb6a45gbz1lwr17y360l98pibxr14") (f (quote (("view") ("render") ("default" "render" "view" "size")))) (s 2) (e (quote (("size" "dep:libc"))))))

