(define-module (crates-io ru te rutenspitz_macro) #:use-module (crates-io))

(define-public crate-rutenspitz_macro-0.2.1 (c (n "rutenspitz_macro") (v "0.2.1") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dq846ibzjfhwmsh0smxw6l9j7244x8rh6fr7l3ffxn6qnbqcry0")))

