(define-module (crates-io ru _h ru_history) #:use-module (crates-io))

(define-public crate-ru_history-0.1.0 (c (n "ru_history") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bogobble") (r "^0.1.1") (d #t) (k 0)) (d (n "err_tools") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "str_tools") (r "^0.1.0") (d #t) (k 0)))) (h "1hggh0lkblmfrhja88v26qz0j86k9dscgfx75gyiqfzwjyq1h8iz")))

