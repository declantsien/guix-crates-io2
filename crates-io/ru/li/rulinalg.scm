(define-module (crates-io ru li rulinalg) #:use-module (crates-io))

(define-public crate-rulinalg-0.0.1 (c (n "rulinalg") (v "0.0.1") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)))) (h "1mgkzymy7ivpq5yrsscnvkl3kqlan3w03mnvgbi1i1hbipgajv38")))

(define-public crate-rulinalg-0.0.2 (c (n "rulinalg") (v "0.0.2") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)))) (h "1knx8pddapsq9cqgwl4sgl0ymk2msiwni5lnzldr5i241jmyp9bp")))

(define-public crate-rulinalg-0.1.0 (c (n "rulinalg") (v "0.1.0") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)))) (h "1fz974kifay203bp5hnhxbj9ykwnhiibbc8pc2dkj4p1129zxkg8")))

(define-public crate-rulinalg-0.2.0 (c (n "rulinalg") (v "0.2.0") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)))) (h "0fk35qx1k690dlrkcxwj58hw6ki70fblbizks1lr4vvq4ji0dl6k")))

(define-public crate-rulinalg-0.2.1 (c (n "rulinalg") (v "0.2.1") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (k 0)))) (h "1iwvk13hdi2wjypc2n7bffqid8vx6ralr1by3df98d1yrd6jldbj")))

(define-public crate-rulinalg-0.2.2 (c (n "rulinalg") (v "0.2.2") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (k 0)))) (h "135hd5jk1nz4h6xlh25zrxaldgrcjzr6zzb58xlinwbk3yyjd249")))

(define-public crate-rulinalg-0.3.0 (c (n "rulinalg") (v "0.3.0") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (k 0)))) (h "1wilx6kgpx2x3gkqvph3nj54rnsrwj44g72wxcv1wxaj11jg32kv")))

(define-public crate-rulinalg-0.3.1 (c (n "rulinalg") (v "0.3.1") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (k 0)))) (h "1fb3mw8ghfg83zv3g0x5l074ka43kxdl5k7aw9kjrzmxjlfc4qq1")))

(define-public crate-rulinalg-0.3.2 (c (n "rulinalg") (v "0.3.2") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (k 0)))) (h "1q4gyxa9bsk9g4x0q2awf6acxpzj70mc7vvl9di2vdcai50rnlbf")))

(define-public crate-rulinalg-0.3.3 (c (n "rulinalg") (v "0.3.3") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "19jrsfrkksyfkj078ycj5jha5mw37yz8nkmzli11ck9l5li1jmj7")))

(define-public crate-rulinalg-0.3.4 (c (n "rulinalg") (v "0.3.4") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0jc1plkjzsdz3f80plhil5hlniz611b8ah2yhv7kjaicq5cixvs9")))

(define-public crate-rulinalg-0.3.5 (c (n "rulinalg") (v "0.3.5") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "15h65274zc2ayyq4n41ihjbxpchf4q544pas5bdkqad42h4l015g")))

(define-public crate-rulinalg-0.3.6 (c (n "rulinalg") (v "0.3.6") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1dgk9kwfgsvqpxfi37bbyrinzqz5l2wcgx19pvk1v9rxvm59mr1z")))

(define-public crate-rulinalg-0.3.7 (c (n "rulinalg") (v "0.3.7") (d (list (d (n "matrixmultiply") (r "^0.1.8") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "11kzqld1n71gd2j1rbyq3xgmjmlvaiyg0afvdd1x9cny2wvwww2n")))

(define-public crate-rulinalg-0.4.0 (c (n "rulinalg") (v "0.4.0") (d (list (d (n "matrixmultiply") (r "^0.1.13") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1z5v8h2l08a4pyjq95x79z7pgnskfai4xgy53z97w7pc955jkhmx")))

(define-public crate-rulinalg-0.4.1 (c (n "rulinalg") (v "0.4.1") (d (list (d (n "matrixmultiply") (r "^0.1.13") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0dz1yzs4zal9hjjr02ykp0x29n6avya6wxlwjqz1pz0xbjymkc4r")))

(define-public crate-rulinalg-0.4.2 (c (n "rulinalg") (v "0.4.2") (d (list (d (n "csv") (r "^0.14.7") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 2)) (d (n "matrixmultiply") (r "^0.1.13") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0bxi7g6bknm3fsvappyk0zwdnn5kja75f322lxr1spk8r41a5b84") (f (quote (("io" "csv" "rustc-serialize"))))))

