(define-module (crates-io ru li rulid) #:use-module (crates-io))

(define-public crate-rulid-0.3.0 (c (n "rulid") (v "0.3.0") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "byteorder") (r "~0.5") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "01p79w98v8k140i3q2g7sqj1vhh7hrz86gq6vhrjjgvss2psd8rm")))

(define-public crate-rulid-0.3.1 (c (n "rulid") (v "0.3.1") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1038k1kymhin7jf3b5ng43pyn6kmmak7gzwixj01jmifbr38jppl")))

