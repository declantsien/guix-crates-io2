(define-module (crates-io ru zs ruzstd) #:use-module (crates-io))

(define-public crate-ruzstd-0.2.0 (c (n "ruzstd") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "04l4qrzm3j3yibykzk1k7mq3rfncdsdicl5iqp326vw6l5m0h21j")))

(define-public crate-ruzstd-0.2.1 (c (n "ruzstd") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "00r8ialp7y9pz2yxi30i3gvn0x7zh2677bap4lvbdypsw92nbbg6")))

(define-public crate-ruzstd-0.2.2 (c (n "ruzstd") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "twox-hash") (r "^1.6.0") (k 0)))) (h "004v030maj7lq152r9dl887s10xqwfxqkrj6g9y74dss911m2hix")))

(define-public crate-ruzstd-0.2.3 (c (n "ruzstd") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "twox-hash") (r "^1.6.0") (k 0)))) (h "17ljpqyk3xd9nxhv4xrc9dxqi99h77zvv02bsf2z5m08p8c61cqj")))

(define-public crate-ruzstd-0.2.4 (c (n "ruzstd") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "twox-hash") (r "^1.6.0") (k 0)))) (h "000iiqrhd9z83pzj3x4dfp3ky7pkv69iyjayvksab9pgb7ps1bcc")))

(define-public crate-ruzstd-0.3.0 (c (n "ruzstd") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (k 0)))) (h "14s48j6wgpr4iapvgvs34qg6ysy1zb8bwyy22nvq25r2mbs8vbpz")))

(define-public crate-ruzstd-0.3.1 (c (n "ruzstd") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (k 0)))) (h "1zkz8s4ws2h1ccmjrlm22v0hm31klqimvzll6hgw5npry1hyc5cs")))

(define-public crate-ruzstd-0.4.0 (c (n "ruzstd") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (k 0) (p "thiserror-core")) (d (n "twox-hash") (r "^1.6") (k 0)))) (h "1p4ghqzkq36dy1x1ijnk7jmml4wi3v9bkfzlbm2hsnkiz6wglgxc") (f (quote (("std" "thiserror/std") ("default" "std"))))))

(define-public crate-ruzstd-0.5.0 (c (n "ruzstd") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (f (quote ("display" "from"))) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "twox-hash") (r "^1.6") (k 0)))) (h "0ga8jciw7ka3mxrzl39skmsbdslajghzglcil10g0z4rh65fpi2q") (f (quote (("std" "derive_more/error") ("default" "std"))))))

(define-public crate-ruzstd-0.5.1 (c (n "ruzstd") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.5") (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (f (quote ("display" "from"))) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "twox-hash") (r "^1.6") (o #t) (k 0)))) (h "0jbd19382i8di04229b8n8i4ildy7pncn1shg6d2yd1faszmvhw9") (f (quote (("std" "derive_more/error") ("default" "hash" "std")))) (y #t) (s 2) (e (quote (("hash" "dep:twox-hash"))))))

(define-public crate-ruzstd-0.6.0 (c (n "ruzstd") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.5") (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (f (quote ("display" "from"))) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "twox-hash") (r "^1.6") (o #t) (k 0)))) (h "0yygqpar2x910lnii4k5p43aj4943hlnxpczmqhsfddmxrqa8x2i") (f (quote (("std" "derive_more/error") ("default" "hash" "std")))) (s 2) (e (quote (("hash" "dep:twox-hash"))))))

