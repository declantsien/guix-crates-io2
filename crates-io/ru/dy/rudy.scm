(define-module (crates-io ru dy rudy) #:use-module (crates-io))

(define-public crate-rudy-0.0.1 (c (n "rudy") (v "0.0.1") (d (list (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "12q28mw42mwyywla0j9jk8l69h31a4dw41bp3385b9r1jccdbdrl")))

(define-public crate-rudy-0.1.0 (c (n "rudy") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0hm0wy5x858xhbcvc4wm3yhwl10ksk7y89afyr1rhh4436ys3nbw")))

