(define-module (crates-io ru n- run-or-raise) #:use-module (crates-io))

(define-public crate-run-or-raise-0.1.0 (c (n "run-or-raise") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)) (d (n "termion") (r "^1.0.3") (d #t) (k 0)) (d (n "xcb") (r "^0.7.4") (d #t) (k 0)))) (h "14x91kkjps26j3b59aiyx65zghi93syk6xjrblh98skn484zc47x")))

(define-public crate-run-or-raise-0.2.0 (c (n "run-or-raise") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)) (d (n "xcb") (r "^0.7.4") (d #t) (k 0)))) (h "182qqhbmvyrdgvbp6j7mz58lhvjf5ql4qbq3ps5vyyvrwaiz6az7")))

(define-public crate-run-or-raise-0.2.1 (c (n "run-or-raise") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)) (d (n "xcb") (r "^0.7.4") (d #t) (k 0)))) (h "0j2mwh887h30g50g38rhscvpcxih1mydk8ammznxqfrqahvrbxf9")))

(define-public crate-run-or-raise-0.2.2 (c (n "run-or-raise") (v "0.2.2") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (d #t) (k 0)))) (h "1y24hm0qap80jckb8cl7clvg5ki95f2s6g52y16l7krp5vwqf4wn")))

(define-public crate-run-or-raise-0.3.0 (c (n "run-or-raise") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (d #t) (k 0)))) (h "1bp3nbjl564yrx9b6k08lmg9cvp6bazxqdqg1581dn0gh2zpsgfr")))

(define-public crate-run-or-raise-0.3.1 (c (n "run-or-raise") (v "0.3.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.7") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "xcb") (r "^0.8.2") (d #t) (k 0)))) (h "0qs6smpjik6yf2z1bb53x5bq2ync9pcl75d0yr17dn5gyvqc7cy7")))

(define-public crate-run-or-raise-0.3.3 (c (n "run-or-raise") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (d #t) (k 0)))) (h "1j201yh5dvy0nhn4s7wj0x99hlgqq8rml3i5rhqk5d31zy36n7cn")))

