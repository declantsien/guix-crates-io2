(define-module (crates-io ru n- run-down) #:use-module (crates-io))

(define-public crate-run-down-0.1.0 (c (n "run-down") (v "0.1.0") (d (list (d (n "assert-impl") (r "^0.1.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rsevents") (r "^0.2.0") (d #t) (k 0)))) (h "01q5nr6abbx1vsd4fxbn86p8h6jc2v9iv34kf88g60mimwadwc6y")))

(define-public crate-run-down-0.1.1 (c (n "run-down") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rsevents") (r "^0.2.0") (d #t) (k 0)))) (h "0pdd9n1n6g69kkygb9zbrn43rhi3lacrp85yi1d517ax2gswk75i")))

