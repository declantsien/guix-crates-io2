(define-module (crates-io ru n- run-loop) #:use-module (crates-io))

(define-public crate-run-loop-0.1.0 (c (n "run-loop") (v "0.1.0") (h "0ppp0rk3arksxvcky73lw7hcayh3591xigi1nr438899h8fvywjj") (y #t)))

(define-public crate-run-loop-0.1.1 (c (n "run-loop") (v "0.1.1") (h "09rq100d3qljq27ilar0jnx3myzmf6hvyf9d7w4axgqk5w977913")))

