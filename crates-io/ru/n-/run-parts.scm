(define-module (crates-io ru n- run-parts) #:use-module (crates-io))

(define-public crate-run-parts-0.1.0 (c (n "run-parts") (v "0.1.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "01q2wvwgh8r1w0qnnyjy4cfm4x8jcs41yrsazkfiyvixpgs7fbh3")))

(define-public crate-run-parts-0.2.0 (c (n "run-parts") (v "0.2.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "io-mux") (r "^1.0.1") (d #t) (k 0)) (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0wr35q2g7hh036zq6a9j6dmfg32ii9xdxm2wwwqfh3amv6i1a8cy")))

(define-public crate-run-parts-0.2.1 (c (n "run-parts") (v "0.2.1") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "io-mux") (r "^1.4.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0dgfm1msn9l7jmdhw03ar9ry2z36q8kz0bd7f9zkbkv4sd1wf0nq")))

(define-public crate-run-parts-0.2.2 (c (n "run-parts") (v "0.2.2") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "io-mux") (r "^1.4.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "01wlfk7y56936hd650kz7dbjn1y10i4f3y7dy0z5zsyyxj3gnbcj")))

