(define-module (crates-io ru n- run-command-on-aws-lambda) #:use-module (crates-io))

(define-public crate-run-command-on-aws-lambda-1.0.0 (c (n "run-command-on-aws-lambda") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)))) (h "028j5hhij4ywjvg0vhsyyws3155qyqzkn688byizbga3kcmnxbbk")))

(define-public crate-run-command-on-aws-lambda-1.1.0 (c (n "run-command-on-aws-lambda") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "async-process") (r "^1.3.0") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (d #t) (k 0)))) (h "0rsj7di4sfbz6mr0fziznxxqjcw6rdf8pkb3dkxiyw9559ghhc1k")))

