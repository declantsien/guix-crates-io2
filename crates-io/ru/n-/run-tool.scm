(define-module (crates-io ru n- run-tool) #:use-module (crates-io))

(define-public crate-run-tool-0.1.0 (c (n "run-tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1jpn2gr3f4ajb9b1qaxcag6hsgsvgnlh43g8ywvaa5m5jbpysqvc")))

(define-public crate-run-tool-0.2.0 (c (n "run-tool") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "07fmvfbf140bk5h46xfplb8vzc2bjafpzw54l62i73pzyd0a526f")))

(define-public crate-run-tool-0.3.0 (c (n "run-tool") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "17pi476x84qh8razhp2w4avkyc19z4chpcbfy4h7bh2ch0dwc169")))

(define-public crate-run-tool-0.4.0 (c (n "run-tool") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (f (quote ("colors"))) (k 0)))) (h "14p1fqkspaqrhpvpfdxipjqayx3p36y4nqkrg80xz5dff69rh8ks")))

(define-public crate-run-tool-0.5.0 (c (n "run-tool") (v "0.5.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (f (quote ("colors"))) (k 0)))) (h "04lfkw0zqhp29wywz1i7chg83bzqkvfzdbpkdyyi4lpvb1wqwflz")))

