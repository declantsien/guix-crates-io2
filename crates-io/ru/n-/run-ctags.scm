(define-module (crates-io ru n- run-ctags) #:use-module (crates-io))

(define-public crate-run-ctags-0.1.0 (c (n "run-ctags") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1g5b8l7l37d3d94339ja36l99sjqgdyy04s6qygmgj5zcnvpypp6")))

(define-public crate-run-ctags-0.1.1 (c (n "run-ctags") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "06hzi2sxd827mjl2x633dkaqbcnjh01q2ad8dfm3d0w6yjzxw824")))

(define-public crate-run-ctags-0.1.2 (c (n "run-ctags") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1zkigjdbyy5ri0bz2ihz9xnhh803xh1d1n9n69iyp9g9qpp09jky")))

(define-public crate-run-ctags-0.1.3 (c (n "run-ctags") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03jkxffq6kmiapar4gar2dlvzzpkz53lj4zyb83w76zvmarlznmy")))

(define-public crate-run-ctags-0.1.5 (c (n "run-ctags") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1lqh3g8qqwmxsg09sdn05fa1hcyf9acch7nk4c98fd29abyp7cc7")))

(define-public crate-run-ctags-0.1.6 (c (n "run-ctags") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1paa66ijznry5rmnm7lbm0fw59vglf1pw4rj68gzzm60g4d38vqz")))

(define-public crate-run-ctags-0.1.7 (c (n "run-ctags") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0qm6r13kiqikgxhy5zd9vfhc6pkwi2a7244dzmi563kh24hkqzd6")))

(define-public crate-run-ctags-0.1.8 (c (n "run-ctags") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1fa65yiznrlsqhywl47qyxy8dc31alg7jhf7xz7q4yl74l2z9bi1")))

(define-public crate-run-ctags-0.1.9 (c (n "run-ctags") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17qhzr7yqcwhnri1yl4gqwlx1r0mxh6b8mq8w86zxh7ylkdchj8i")))

(define-public crate-run-ctags-0.1.10 (c (n "run-ctags") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08ld6xrzb49wg1kwy7kcbr30wyym961alqaxpncnc9mkhkb28dvv")))

(define-public crate-run-ctags-0.1.11 (c (n "run-ctags") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0i8mfh7ck75yx12qsc8pk6di0sp7xv70h75py30ljmi01jpynljv")))

(define-public crate-run-ctags-0.1.12 (c (n "run-ctags") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0npfaz50cqqd4vwwq6f9xnl20jxps69m43mv3c37rlzhykibm67m")))

(define-public crate-run-ctags-0.1.13 (c (n "run-ctags") (v "0.1.13") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "0h4i84s9kag3r35lbl99152w39pz7a1qa2rghqp29i8cb5ydcw4s")))

(define-public crate-run-ctags-0.1.14 (c (n "run-ctags") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "07mrkx7jj79gd41mk66fah11q9rkpkmwlsxy31jh6zj62q26zh8b")))

