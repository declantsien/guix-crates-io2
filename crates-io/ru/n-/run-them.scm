(define-module (crates-io ru n- run-them) #:use-module (crates-io))

(define-public crate-run-them-0.1.0 (c (n "run-them") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0i94syib3m93drbyjibn1xfhxzmgp4iqcj3im3sa72nasfxzh2lf")))

