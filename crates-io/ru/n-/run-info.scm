(define-module (crates-io ru n- run-info) #:use-module (crates-io))

(define-public crate-run-info-0.4.0 (c (n "run-info") (v "0.4.0") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1nwhj3ahc7lsz0l5i63nyqgmil6a81fzhxn9bnwkbb1s2rri9sl1")))

(define-public crate-run-info-0.5.0 (c (n "run-info") (v "0.5.0") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0n6ilpinh37q8kgvqc261l979y5wn7kjapvx0ycliz1gqrlfaq7h")))

(define-public crate-run-info-0.5.1 (c (n "run-info") (v "0.5.1") (d (list (d (n "clap") (r "^2.11.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1c5rlr39xxjvbcwl0vxg4scysw0z63cbaxhvvjvd6v63pzllx625")))

