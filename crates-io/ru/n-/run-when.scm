(define-module (crates-io ru n- run-when) #:use-module (crates-io))

(define-public crate-run-when-1.0.0 (c (n "run-when") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.1") (d #t) (k 0)))) (h "09qbpq1ss8g89xd2m87a36amn9i9x2n32vl19fmsqa4lx6xlyriv")))

(define-public crate-run-when-1.0.1 (c (n "run-when") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.1") (d #t) (k 0)))) (h "17a6a6pgd1yfc6mp8hzkw1qv947qwhwas0m4rfrwb498q46aivpd")))

(define-public crate-run-when-1.0.2 (c (n "run-when") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.1") (d #t) (k 0)))) (h "0d7ac8hsrr6sxa09dnclp8diw69vwfwsw26c8mw9a3zsqqcpmq4f")))

(define-public crate-run-when-1.0.3 (c (n "run-when") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.1") (d #t) (k 0)) (d (n "simple_logger") (r "^2.2.0") (d #t) (k 0)))) (h "0bik9v5zcyrsmp1wjy3s2vkhlfsfan8snag2as0g93c7yd9li4zz")))

