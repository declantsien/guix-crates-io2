(define-module (crates-io ru bi rubic) #:use-module (crates-io))

(define-public crate-rubic-0.1.0 (c (n "rubic") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "0jaki11s8816gcayv2ilmmg5vfh6wx53ilq1b7wmdmw7m6276qpn")))

(define-public crate-rubic-0.1.1 (c (n "rubic") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "02w3wz4vdlldyjv9bxskll4dydadi7a1vdyfyjjmvcbxzrmpdv5l")))

