(define-module (crates-io ru bi rubik) #:use-module (crates-io))

(define-public crate-rubik-0.1.0 (c (n "rubik") (v "0.1.0") (h "0p9y24nmr83j397ix7shywyymjlas5c7xqil0ipj6m6pxxxszq1l")))

(define-public crate-rubik-0.1.1 (c (n "rubik") (v "0.1.1") (h "1kmgcjjqg0fvd18l0c4p9xvlgib1iki5jyqswl61750q8m6fnwxi")))

