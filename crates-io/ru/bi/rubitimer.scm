(define-module (crates-io ru bi rubitimer) #:use-module (crates-io))

(define-public crate-rubitimer-0.1.0 (c (n "rubitimer") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0da0kl2j9sp859hxk681g9mypg5snn3nkfzrmsw2nkak2qpr1myf")))

