(define-module (crates-io ru bi rubiks-moves) #:use-module (crates-io))

(define-public crate-rubiks-moves-0.0.1 (c (n "rubiks-moves") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "184i0bwpkhmd6dg8sci21gj43p5b0ws1llkk8fz27n7hlnj342gx")))

(define-public crate-rubiks-moves-0.0.2 (c (n "rubiks-moves") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "09cfcqa5pv252df8b9pyyzscqg6ixvdaxzwxyrwl830y3jnqn03d")))

(define-public crate-rubiks-moves-0.0.3 (c (n "rubiks-moves") (v "0.0.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1v8fk9qb404af4a62sy3adfdv3ni89pjydc1xarz6zshrzcfkf10")))

(define-public crate-rubiks-moves-0.0.4 (c (n "rubiks-moves") (v "0.0.4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0l7rpgi8gl0cfpp86jpk5j6dy0pw7sqrylhvi98185cjbmd482h8")))

