(define-module (crates-io ru bi rubin-cli) #:use-module (crates-io))

(define-public crate-rubin-cli-0.1.0 (c (n "rubin-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rubin") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "11s5xiysgkahhdbwmiavfvzypg8p741s78zxza39sir1m1xmaxhg")))

(define-public crate-rubin-cli-0.1.1 (c (n "rubin-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rubin") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19gck2z506mh8nv4kjm7vrnfjnn87hld1c9s3xdh7i1nyzr30qaj")))

(define-public crate-rubin-cli-0.2.0 (c (n "rubin-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rubin") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqik17qnhm9k0aj33qclwdx8sa7z1gy27k9i54vi6qm0c097cn6")))

