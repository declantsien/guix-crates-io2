(define-module (crates-io ru so rusolver) #:use-module (crates-io))

(define-public crate-rusolver-0.1.0 (c (n "rusolver") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19.5") (d #t) (k 0)))) (h "0i2hx34p29whm75zr14wz5dhqhvsh5a0n2syhysrcq722ixg4x5w") (y #t)))

(define-public crate-rusolver-0.1.1 (c (n "rusolver") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19.5") (d #t) (k 0)))) (h "12kay7pmxha4rp9c86807k3z41k81pz0jxxnx1xz3j6hxf1iw6k6")))

