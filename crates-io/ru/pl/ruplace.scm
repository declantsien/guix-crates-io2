(define-module (crates-io ru pl ruplace) #:use-module (crates-io))

(define-public crate-ruplace-0.1.0 (c (n "ruplace") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0p133988na67y5ykwwqmymq9ddgf6jdlyr0fblwf0chp0f7rbkb3")))

(define-public crate-ruplace-0.1.1 (c (n "ruplace") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0jhgbim872b1d3p24lnadwm43d8mb8z7c5i375yx4bvyb291bbzb")))

(define-public crate-ruplace-0.1.2 (c (n "ruplace") (v "0.1.2") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "15yn3kr2f8rl20vhg96rcmf42xhd24vjqzsl07pknaz9r2lbpsdd")))

(define-public crate-ruplace-0.1.3 (c (n "ruplace") (v "0.1.3") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "1rpxjz637s0as5rh8ljzxwiffj2m4jk5p0x94246zq0ab4nsk9mv")))

(define-public crate-ruplace-0.1.4 (c (n "ruplace") (v "0.1.4") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "18gr7m4fls355i98jalnrvx7jm9p2j56n3s16d4n7x7fp982lhac")))

(define-public crate-ruplace-0.2.0 (c (n "ruplace") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0d911qmqznx3lkpmsidwrj484b1wvyvrnrixn4xw7q99f52ngs44")))

(define-public crate-ruplace-0.2.1 (c (n "ruplace") (v "0.2.1") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "1s5x1wvslq9wk69nax8i16dx3xp4g23hqdz5z53by1rh4igpda95")))

