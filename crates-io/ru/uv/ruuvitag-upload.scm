(define-module (crates-io ru uv ruuvitag-upload) #:use-module (crates-io))

(define-public crate-ruuvitag-upload-0.2.0 (c (n "ruuvitag-upload") (v "0.2.0") (d (list (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.14") (d #t) (k 0)) (d (n "rumble") (r "^0.3") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0civjzm8zfpfp6k9vav2hvs8i5sx2bcl7kp52q6w2i1m12zbndnc")))

(define-public crate-ruuvitag-upload-0.3.0 (c (n "ruuvitag-upload") (v "0.3.0") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "directories") (r "^1.0") (d #t) (k 0)) (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.14") (d #t) (k 0)) (d (n "rumble") (r "^0.3") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "094glza09zi61a3l5dg7w3x0qk1ggn6q7xy80q9s24krv7508h37")))

