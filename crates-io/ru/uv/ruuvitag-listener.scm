(define-module (crates-io ru uv ruuvitag-listener) #:use-module (crates-io))

(define-public crate-ruuvitag-listener-0.1.0 (c (n "ruuvitag-listener") (v "0.1.0") (d (list (d (n "rumble") (r "^0.2.1") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "^0.2.0") (d #t) (k 0)))) (h "0nxw2z7c2vacn9da9ja5aya42qcyj2hpwr2ibyb3h6jccn9m89xk")))

(define-public crate-ruuvitag-listener-0.1.1 (c (n "ruuvitag-listener") (v "0.1.1") (d (list (d (n "rumble") (r "0.3.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.2.*") (d #t) (k 0)))) (h "0vlgffah8fam5zii5ih1mg155c0pa6xkdxzxj02cazny9835vf74")))

(define-public crate-ruuvitag-listener-0.2.0 (c (n "ruuvitag-listener") (v "0.2.0") (d (list (d (n "rumble") (r "0.3.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.2.*") (d #t) (k 0)))) (h "02i7c6dnmq52hsf8ykbgsz6a2j8pfhlgvpg0qk7b3dk2d59sfjis")))

(define-public crate-ruuvitag-listener-0.3.0 (c (n "ruuvitag-listener") (v "0.3.0") (d (list (d (n "rumble") (r "0.3.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.2.*") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)))) (h "0vmlvx6ipfq5352z0j8865xf4c74hzfrk81rha0x73qjrs7nb9wq")))

(define-public crate-ruuvitag-listener-0.3.1 (c (n "ruuvitag-listener") (v "0.3.1") (d (list (d (n "rumble") (r "0.3.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.2.*") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)))) (h "0a061vjzyd8hhwvi2k5hkp52hcgcx99jw6hj94d1n6nhhc4q72vf")))

(define-public crate-ruuvitag-listener-0.4.1 (c (n "ruuvitag-listener") (v "0.4.1") (d (list (d (n "btleplug") (r "0.4.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.4.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (k 0)))) (h "1mvcw246akn06cbfp7zlvws43yhd4rkswghkn6z31hhar3ng0sqr")))

(define-public crate-ruuvitag-listener-0.5.1 (c (n "ruuvitag-listener") (v "0.5.1") (d (list (d (n "btleplug") (r "0.4.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.4.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (k 0)))) (h "1b8iirg2nqhd2gn9paf3xig6ikih7igvdc4p5karaa8pxdd5fhqr")))

(define-public crate-ruuvitag-listener-0.5.2 (c (n "ruuvitag-listener") (v "0.5.2") (d (list (d (n "btleplug") (r "0.5.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.4.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (k 0)))) (h "02wqgfksbszwsbifcmx38y9gi7jn0rfb6rpkiqa1yz4ahhvw1cyd")))

(define-public crate-ruuvitag-listener-0.5.3 (c (n "ruuvitag-listener") (v "0.5.3") (d (list (d (n "btleplug") (r "0.5.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.4.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (k 0)))) (h "1j1g8n1y3zimvxhy6l2s10v07rx7lm2zpda9mc2lnhs7ak9iw77a")))

(define-public crate-ruuvitag-listener-0.5.4 (c (n "ruuvitag-listener") (v "0.5.4") (d (list (d (n "btleplug") (r "0.5.*") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.4.*") (d #t) (k 0)) (d (n "structopt") (r "0.3.*") (k 0)))) (h "1xva2wrxhb83slalyh5blf46w8x2qv119xfg4dfvxhsx0mwmymkz")))

(define-public crate-ruuvitag-listener-0.5.5 (c (n "ruuvitag-listener") (v "0.5.5") (d (list (d (n "btleplug") (r "0.5.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "0.5.*") (d #t) (k 0)))) (h "0ldgmmdklg56fji3krrlq35fij47pkv2p8h21if0vzbqa806i8n8")))

