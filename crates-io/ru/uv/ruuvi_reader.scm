(define-module (crates-io ru uv ruuvi_reader) #:use-module (crates-io))

(define-public crate-ruuvi_reader-0.1.0 (c (n "ruuvi_reader") (v "0.1.0") (d (list (d (n "btleplug") (r "^0.5.4") (d #t) (k 0)) (d (n "ruuvi-sensor-protocol") (r "^0.4.1") (d #t) (k 0)))) (h "1ysj8ngf9mbilfj49y56vxmmnkj2z4wfp5lym7230j1vqr6b20f3")))

