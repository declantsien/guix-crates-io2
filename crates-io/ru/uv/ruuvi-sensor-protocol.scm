(define-module (crates-io ru uv ruuvi-sensor-protocol) #:use-module (crates-io))

(define-public crate-ruuvi-sensor-protocol-0.1.0 (c (n "ruuvi-sensor-protocol") (v "0.1.0") (h "1hsz0z7r23i8xkiaz47f1laavjwyj3milci6zqxk4craa0v2wpdn")))

(define-public crate-ruuvi-sensor-protocol-0.1.1 (c (n "ruuvi-sensor-protocol") (v "0.1.1") (h "07lll76vf2nv7cx2jdnmp479bdwvirs7bsyjvvimhzsli1ylysg2")))

(define-public crate-ruuvi-sensor-protocol-0.2.0 (c (n "ruuvi-sensor-protocol") (v "0.2.0") (h "1vg0q5z7pyd8mwaqk1xzkb5hif3dn2ym70q4c62109h07fvk12mk")))

(define-public crate-ruuvi-sensor-protocol-0.3.0 (c (n "ruuvi-sensor-protocol") (v "0.3.0") (h "00phh9vcpj4aj4a5n1k800bnwn5477d1i3h8l1pfs843n9y37ffi") (f (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.4.0 (c (n "ruuvi-sensor-protocol") (v "0.4.0") (h "17lz3z3vg71431hn2py6vg3gxjd28hq13qqi173sb3nzv1fzpa8w") (f (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.4.1 (c (n "ruuvi-sensor-protocol") (v "0.4.1") (h "17cqwks85l61rcz9nvk1qm3bh6b4x830lkk17755smjmqgdla5cv") (f (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.5.0 (c (n "ruuvi-sensor-protocol") (v "0.5.0") (h "18cr3sgpkxvybvw1ibri7nv95lqn0r63mjww0klkcgm03kq4rgdn") (f (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.6.0 (c (n "ruuvi-sensor-protocol") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (o #t) (k 0)))) (h "01d4zhlnxjfgfxyy7fjm7qibbpjp46i1m221v1i4lva6jibb2qp1") (f (quote (("gateway" "hex" "serde" "serde_json") ("default" "std")))) (s 2) (e (quote (("std" "serde_json?/std")))) (r "1.60")))

(define-public crate-ruuvi-sensor-protocol-0.6.1 (c (n "ruuvi-sensor-protocol") (v "0.6.1") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0yhapjvj5v13pi2m5kp5hkd0bc76crhcq5i4h34izynvxy9cdmgd") (f (quote (("gateway" "hex" "serde" "serde_json") ("default" "std")))) (s 2) (e (quote (("std" "serde_json?/std")))) (r "1.60")))

