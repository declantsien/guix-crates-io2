(define-module (crates-io ru ch ruchei-route) #:use-module (crates-io))

(define-public crate-ruchei-route-0.1.4 (c (n "ruchei-route") (v "0.1.4") (d (list (d (n "futures-sink") (r "^0.3.30") (d #t) (k 0)))) (h "0m0gs6z92d4m9p5n6irc85lppx4vb4brpzw60zwzs7qzdkg1s9pl") (r "1.72")))

(define-public crate-ruchei-route-0.1.5 (c (n "ruchei-route") (v "0.1.5") (d (list (d (n "futures-sink") (r "^0.3.30") (d #t) (k 0)))) (h "0bsx9vqjfyhn38im5kr4p4c4dzcqnczqqz26zf063vs20bd4gvbs") (r "1.72")))

