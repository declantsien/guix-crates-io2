(define-module (crates-io ru ch ruchei-extra) #:use-module (crates-io))

(define-public crate-ruchei-extra-0.1.0 (c (n "ruchei-extra") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.30") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "1bbmv0mqhl0vlff29gzwks3wdg7yl3r8r29ymkhgsjdqaad3zgx4") (r "1.72")))

(define-public crate-ruchei-extra-0.1.1 (c (n "ruchei-extra") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.30") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)))) (h "1fmzmffcqnyaknj503f0cvnswd98p789ihjdrsvw729im52q4r0h") (r "1.72")))

