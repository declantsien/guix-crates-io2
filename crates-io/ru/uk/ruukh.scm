(define-module (crates-io ru uk ruukh) #:use-module (crates-io))

(define-public crate-ruukh-0.0.1 (c (n "ruukh") (v "0.0.1") (h "07grqyjicz67fzpqbv52qfn8n4vm3y37z5p7043xzznwxx9my63k")))

(define-public crate-ruukh-0.0.2 (c (n "ruukh") (v "0.0.2") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "ruukh-codegen") (r "^0.0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.21") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.21") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.0") (f (quote ("Node" "Element" "Comment" "Text" "Window" "Document" "MessagePort" "MessageChannel" "Event" "EventTarget"))) (d #t) (k 0)))) (h "0piwmmkwjfafjh22q5mbmc00lxvxg5g5ck3sj1585mg96kzf8g5s")))

(define-public crate-ruukh-0.0.3 (c (n "ruukh") (v "0.0.3") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)) (d (n "ruukh-codegen") (r "^0.0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.21") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.21") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.0") (f (quote ("Node" "Element" "Comment" "Text" "Window" "Document" "MessagePort" "MessageChannel" "Event" "EventTarget"))) (d #t) (k 0)))) (h "1jgpflqw8hvvp1cbiq6bawmswzjr9pzghnay6kx9d7jpln439b4b")))

