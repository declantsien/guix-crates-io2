(define-module (crates-io ru uk ruukh-codegen) #:use-module (crates-io))

(define-public crate-ruukh-codegen-0.0.1 (c (n "ruukh-codegen") (v "0.0.1") (h "128fri3jcjrla8jpvrc1wrqs7kni95hcwf7f947j4q5mdr7g4bn2")))

(define-public crate-ruukh-codegen-0.0.2 (c (n "ruukh-codegen") (v "0.0.2") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03qy2kjhjc91pvbqq8yqyf20qsp1xv64h5dhh1gma6xxs6s3hm2k")))

(define-public crate-ruukh-codegen-0.0.3 (c (n "ruukh-codegen") (v "0.0.3") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cpdlnchyrn4jmaryid078qbddl1ggms7xrqw7s6x1g8x6m9wxly")))

