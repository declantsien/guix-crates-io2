(define-module (crates-io ru ne runeterra-game-api) #:use-module (crates-io))

(define-public crate-runeterra-game-api-0.1.0 (c (n "runeterra-game-api") (v "0.1.0") (d (list (d (n "mockito") (r "^0.22.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "0inwpddlf9nvg4i2x5zdk84d3nv1ax4hym0s6dq75mnm65sihl0x")))

