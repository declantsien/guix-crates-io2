(define-module (crates-io ru ne runestick-macros) #:use-module (crates-io))

(define-public crate-runestick-macros-0.7.0 (c (n "runestick-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "runestick") (r "^0.7.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1l3r3yp1qlp8hcyfc67q1h0yj9qklp76ghdyz09d24w41lhr6pr2")))

(define-public crate-runestick-macros-0.8.0 (c (n "runestick-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "runestick") (r "^0.8.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "07dqrkndpmv80rmbq33nka2nhfbssa8a5c6iv9xbw46y49a9ap68")))

(define-public crate-runestick-macros-0.9.0 (c (n "runestick-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "runestick") (r "^0.9.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "1vbby7bzbad7lra4ivd9072i94jfpx3jfygcjxxzs90xx109mx16")))

(define-public crate-runestick-macros-0.9.1 (c (n "runestick-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "runestick") (r "^0.9.1") (d #t) (k 2)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "1gbd7wjzv86ch4n70a90asrzfzz7ki7k6dgn99p03295ywkkj9hj")))

