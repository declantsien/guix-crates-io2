(define-module (crates-io ru ne runestr-pancjkv) #:use-module (crates-io))

(define-public crate-runestr-pancjkv-0.1.0 (c (n "runestr-pancjkv") (v "0.1.0") (d (list (d (n "runestr") (r "^0.1.1") (d #t) (k 0)))) (h "1hr4xx4l6szak7a2cns7l3z9b7x4kkajvzi95pawjjm0hvpzxb60")))

(define-public crate-runestr-pancjkv-0.1.1 (c (n "runestr-pancjkv") (v "0.1.1") (d (list (d (n "runestr") (r "^0.1.1") (d #t) (k 0)))) (h "1a3b40qw8pxg4m2v4wyj23w05kwyx7p8q627xxxarwrd6yz6clr5")))

