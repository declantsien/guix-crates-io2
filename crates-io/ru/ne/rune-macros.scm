(define-module (crates-io ru ne rune-macros) #:use-module (crates-io))

(define-public crate-rune-macros-0.7.0 (c (n "rune-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "rune") (r "^0.7.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1s36zxbvr8hw40s02swlhdigfjsnckir4lfrsh4sv6ay4rb5jw0g")))

(define-public crate-rune-macros-0.8.0 (c (n "rune-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "rune") (r "^0.8.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "02f0lqnczdby8afvyc70naqn0ilpm3zwawllkacgx4l8dsvn0l40")))

(define-public crate-rune-macros-0.9.0 (c (n "rune-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rune") (r "^0.9.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "140jfxvks2yh6biq2fjsr7d14lacyf6bx9s2bvgbq1a90ry76d4c")))

(define-public crate-rune-macros-0.9.1 (c (n "rune-macros") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rune") (r "^0.9.1") (d #t) (k 2)) (d (n "syn") (r "^1.0.62") (f (quote ("full"))) (d #t) (k 0)))) (h "0q9hssdsj5g515lz3iigl6klpxj1zxm7jj96whxfb4ar4jx1zczd")))

(define-public crate-rune-macros-0.10.0 (c (n "rune-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.10.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0i2wqm8gdzqkqa30dch8fdjivhi10c4idsqinriz2pcinyddkmz2")))

(define-public crate-rune-macros-0.10.1 (c (n "rune-macros") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.10.1") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "1fdm7mzmq9zaspsaxvgwhz6cg0cfhyps0iqv538gx2sk93lak97h")))

(define-public crate-rune-macros-0.10.2 (c (n "rune-macros") (v "0.10.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.10.2") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "1dkrvg86hgsmywz68x0cx5dpzdv9j4qswrxn4w44qn7h3rr9dlkx")))

(define-public crate-rune-macros-0.10.3 (c (n "rune-macros") (v "0.10.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.10.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "1m4nckscqkxwrgx4zf5cxpmld9dm0mv9xrpnd392pby5q52i3k7r")))

(define-public crate-rune-macros-0.12.0 (c (n "rune-macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.12.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "04w7c8g38wlzzsgb9wgi8834wrv75g1cm3kl5haqwhcgniy4ifhd")))

(define-public crate-rune-macros-0.12.1 (c (n "rune-macros") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.12.1") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "1ba031298d776hp2n1rhabjkmcb67rc9aksqy00bags9lyw09rhp") (r "1.61")))

(define-public crate-rune-macros-0.12.2 (c (n "rune-macros") (v "0.12.2") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.12.2") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0f8qw19rvih5ys2vhhqhcypssjz8ivnbrcfm3rsh9h5b6l0fq9hg") (r "1.63")))

(define-public crate-rune-macros-0.12.3 (c (n "rune-macros") (v "0.12.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.12.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0i74sm7nk0n8pkds8r86df4ij06qny2kfbb1536ma598l2dqr9jj") (r "1.63")))

(define-public crate-rune-macros-0.12.4 (c (n "rune-macros") (v "0.12.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rune") (r "^0.12.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0s3pwdhr7kdxdlg9k0cb4jmin74mcvfnng9g208g3j52x8bnzip6") (r "1.63")))

(define-public crate-rune-macros-0.13.0 (c (n "rune-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "rune-core") (r "=0.13.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "00bxcy2qfjg5kzga7amix7wf53ix4mjgam88b830hjgndirad0x2") (r "1.70")))

(define-public crate-rune-macros-0.13.1 (c (n "rune-macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "rune-core") (r "=0.13.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1x6vp56x3m16cj3jrg059s4zi6j26rxglnkcjdfcfmwk902m81xw") (r "1.70")))

(define-public crate-rune-macros-0.13.2 (c (n "rune-macros") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "rune-core") (r "=0.13.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "13cvnhd5qdzwid971zdfddjhraxyqrylsgzha7iy4i2zbb6xxmrj") (r "1.74")))

