(define-module (crates-io ru ne runestick-http) #:use-module (crates-io))

(define-public crate-runestick-http-0.2.2 (c (n "runestick-http") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "runestick") (r "^0.2.2") (d #t) (k 0)) (d (n "runestick-json") (r "^0.2.2") (d #t) (k 2)))) (h "176yd6nvwcprzql1pvpvqm1mf5k74xpgyrij9dmapirgrdywh0d2")))

(define-public crate-runestick-http-0.3.0 (c (n "runestick-http") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (d #t) (k 0)) (d (n "runestick") (r "^0.3.0") (d #t) (k 0)) (d (n "runestick-json") (r "^0.3.0") (d #t) (k 2)))) (h "0dz4vjc6rwqp3m8km776k3syxfzr24y4zb3vkf0f0gpvafjg40k4")))

