(define-module (crates-io ru ne rune-alloc-macros) #:use-module (crates-io))

(define-public crate-rune-alloc-macros-0.13.0 (c (n "rune-alloc-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0pq9djklnmbr3alb20x3wvmpswz3fv36g9gpzbaf05zaq6fivd7y") (r "1.70")))

(define-public crate-rune-alloc-macros-0.13.1 (c (n "rune-alloc-macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "18wk2xwcd7lwxv23mkzggdb0h0w8ls00y7bkx5x02r6rrm4x618i") (r "1.70")))

(define-public crate-rune-alloc-macros-0.13.2 (c (n "rune-alloc-macros") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "08qh5pp96rlrk9p2046gzqhqvdy9c49mr1rg2w0l8i0nfiw9qsnb") (r "1.74")))

