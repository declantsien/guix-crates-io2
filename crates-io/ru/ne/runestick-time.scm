(define-module (crates-io ru ne runestick-time) #:use-module (crates-io))

(define-public crate-runestick-time-0.2.2 (c (n "runestick-time") (v "0.2.2") (d (list (d (n "runestick") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("time"))) (d #t) (k 0)))) (h "0b9d8489pdxr29zi5gf01sgm91rf3i8rn7nxim0n295pg26iq6yn")))

(define-public crate-runestick-time-0.3.0 (c (n "runestick-time") (v "0.3.0") (d (list (d (n "runestick") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("time"))) (d #t) (k 0)))) (h "126z3hhyakmbgzibcwqg97k5sdbi4wi502j93z364ad6fvkwr9r8")))

