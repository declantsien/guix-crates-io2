(define-module (crates-io ru ne runestone) #:use-module (crates-io))

(define-public crate-runestone-0.0.0 (c (n "runestone") (v "0.0.0") (d (list (d (n "bitcoin") (r "^0.30.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0syzvxyhfd5mhrjgjbqb7caqbq5c42216gh7w8zs95j9lkynj1fn")))

