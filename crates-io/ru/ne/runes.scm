(define-module (crates-io ru ne runes) #:use-module (crates-io))

(define-public crate-runes-0.1.0 (c (n "runes") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "0bvk992vqxvy0as04ik6hxwl2iz2njkvmqp4gr98d2jj8zsikrh7")))

(define-public crate-runes-0.1.1 (c (n "runes") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1psl5y9qxvy9idi9cmzanx3s2wzrm6w6dxq9nsr57flsp4dk9a6a")))

(define-public crate-runes-0.1.2 (c (n "runes") (v "0.1.2") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "07ar4j93fwmsbvkci6lq5vdirihn9x4z88ya1h56r6yylnrx2mq8")))

(define-public crate-runes-0.1.3 (c (n "runes") (v "0.1.3") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "0xj1z24sljlsnh1ybqsfr7w0mv3k66pr96qyl0lp0mlnbj5kg91j")))

(define-public crate-runes-0.1.4 (c (n "runes") (v "0.1.4") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1gzfdmdr66cif2ynyqkxy9h9frc8z3y19bk1b4dxlqc99x6k2kmi")))

(define-public crate-runes-0.1.5 (c (n "runes") (v "0.1.5") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "02r5vk4npgimjp8878559ld2zy3h2395gsg9cjq6sxyqmh06jali")))

(define-public crate-runes-0.1.6 (c (n "runes") (v "0.1.6") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "08nqsgnc3j72p2idfnq24hn8qc50bfixsqsb36l7kl5182ffk546")))

(define-public crate-runes-0.1.7 (c (n "runes") (v "0.1.7") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1kw458y05aqby1p3in48kb62va0ddksa78gy11wnpay5s3adx9l4")))

(define-public crate-runes-0.1.8 (c (n "runes") (v "0.1.8") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1b5xsbw764w1idsvl855z368a6f6rw17k0125fgsmg6llwn0pv34")))

(define-public crate-runes-0.1.9 (c (n "runes") (v "0.1.9") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1r3dl39di9xnvp4lr42w5mfdd35yzn3k9fh7jq51i98ni4ws7cbg")))

(define-public crate-runes-0.2.0 (c (n "runes") (v "0.2.0") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1j416w5ivglzgswmlzm1x53shasgpagxca6ni8p4lb0ij2g6bdv3")))

(define-public crate-runes-0.2.1 (c (n "runes") (v "0.2.1") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "0g1hp3niqxqx7i1dw4dzqnhz499lyk8p05ib3f8c69smqs38jyc2")))

(define-public crate-runes-0.2.2 (c (n "runes") (v "0.2.2") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "0ibiv2a3z64s3awvrxkhh8jkaykks5bzy71d936gr45673qg3l4f")))

(define-public crate-runes-0.2.3 (c (n "runes") (v "0.2.3") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "0pq1rq06ps18wx6dmmlfh2q1cshr6fnxc5bln7al6ryj14i3prz7")))

(define-public crate-runes-0.2.4 (c (n "runes") (v "0.2.4") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.31") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1ldh2l230fy0ndy72rd9khvymy87gn04cq3b93g9pdysfmv6kh8i")))

(define-public crate-runes-0.2.5 (c (n "runes") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.3") (f (quote ("unsafe_textures"))) (d #t) (k 2)))) (h "1b3bd4skycnniyxnxrkq884iizv10apx684bfriz89yjg939y19g")))

