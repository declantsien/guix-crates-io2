(define-module (crates-io ru ne rune-ssa) #:use-module (crates-io))

(define-public crate-rune-ssa-0.9.0 (c (n "rune-ssa") (v "0.9.0") (d (list (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)) (d (n "ordered-float") (r "^2.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "010ffnmcrzx65mbayqqcfa2yynbf0qr2yc9b59rvp9jd7rivgh7s")))

(define-public crate-rune-ssa-0.9.1 (c (n "rune-ssa") (v "0.9.1") (d (list (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)) (d (n "ordered-float") (r "^2.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "02qrfa9immdpw7j1lpsh07svnpv649ng855xa3glkrs7kz84zdjb")))

