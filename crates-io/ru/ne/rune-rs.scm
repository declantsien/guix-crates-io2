(define-module (crates-io ru ne rune-rs) #:use-module (crates-io))

(define-public crate-rune-rs-1.0.0 (c (n "rune-rs") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0qv0wlbz29l3gqwpykbw8vqvrn903603mmpkfnr5fnk45pwq4cf5") (y #t)))

