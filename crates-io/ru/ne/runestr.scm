(define-module (crates-io ru ne runestr) #:use-module (crates-io))

(define-public crate-runestr-0.1.0 (c (n "runestr") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0np1mg2qrrjrbz5iflw86jrnz02wahp9h2bfgv0v4rz4nb8k350y")))

(define-public crate-runestr-0.1.1 (c (n "runestr") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "178srvzm6h93kwnb1d4c2rhxscn7hda9l1rj404bphwdbd6jclxh")))

(define-public crate-runestr-0.1.2 (c (n "runestr") (v "0.1.2") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0dsrhzw9mplsw8djh9zpryys326vg12nypr99p8dz1balaw5m0s2")))

(define-public crate-runestr-0.1.3 (c (n "runestr") (v "0.1.3") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0zi1qdn0lcig964dqhl9ab2ww4hnjrhvn82mznggyn140phm941m")))

(define-public crate-runestr-0.1.4 (c (n "runestr") (v "0.1.4") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "023sqacbb8i9cvaygyzw1w357z20f64vm1z7rwf2ks1xjbil8xqp")))

(define-public crate-runestr-0.1.5 (c (n "runestr") (v "0.1.5") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "05ac3qzr7mx9lkpl7jm1l0b5aspn0x09n02xw5z01kqbjvmsgw9m")))

(define-public crate-runestr-0.1.6 (c (n "runestr") (v "0.1.6") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0sck8iqiyv9y3m181w0hliwj8kpls1bv7ik622bafmcp88h5bkyn")))

(define-public crate-runestr-0.1.7 (c (n "runestr") (v "0.1.7") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1lwpgqh0r2dxydda2ilvc27g3kd82sv34jn7mzsl122cc0y5cj97")))

(define-public crate-runestr-0.1.8 (c (n "runestr") (v "0.1.8") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0j972f01pkvp5k2p4gsgc9xvr82gabiz5c3anp91xa5v139zl6zm")))

(define-public crate-runestr-0.1.9 (c (n "runestr") (v "0.1.9") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1568w4z0mpbx195f39f5qp78z5slfwp6317qcr67cy4cxas5w7dr")))

(define-public crate-runestr-0.1.10 (c (n "runestr") (v "0.1.10") (d (list (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "19y439rp8xbw98j7gycr8bxmca1pd4r8nfxfw9a20ckj7fnks5h2")))

