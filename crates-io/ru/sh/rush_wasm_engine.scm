(define-module (crates-io ru sh rush_wasm_engine) #:use-module (crates-io))

(define-public crate-rush_wasm_engine-0.1.0 (c (n "rush_wasm_engine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-channel") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "rush_core") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "wasmer") (r "^4.1.1") (d #t) (k 0)) (d (n "wd_tools") (r "^0.8.12") (f (quote ("point-free" "sync"))) (d #t) (k 0)))) (h "0c017qp1xzfhraal3n808v5d6y8a67fl3fchllpxix17rz4yhqci") (f (quote (("rule-flow" "rush_core"))))))

