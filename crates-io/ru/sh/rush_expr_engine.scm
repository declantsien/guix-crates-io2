(define-module (crates-io ru sh rush_expr_engine) #:use-module (crates-io))

(define-public crate-rush_expr_engine-0.1.0 (c (n "rush_expr_engine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rush_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "wd_tools") (r "^0.8.12") (f (quote ("point-free" "sync"))) (d #t) (k 0)))) (h "1c3l3624876w7clky9yhw5km6xv84w0gka3yqdb5c17lb8mrrfkd")))

