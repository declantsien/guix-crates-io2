(define-module (crates-io ru sh rush-rs) #:use-module (crates-io))

(define-public crate-rush-rs-0.0.1 (c (n "rush-rs") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rush-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0i2l21p8hsagll3lfmjsr23x625p56zkzz8f0x5f6a8rwjd63p7j")))

(define-public crate-rush-rs-0.0.2-alpha.1 (c (n "rush-rs") (v "0.0.2-alpha.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "rush-sys") (r "^0.0.2-alpha.1") (d #t) (k 0)))) (h "055y7b5z4z63ckz864sz4r9ais4afi6dzxgjks9p0k6ry2gx6wqj")))

