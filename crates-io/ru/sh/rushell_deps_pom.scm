(define-module (crates-io ru sh rushell_deps_pom) #:use-module (crates-io))

(define-public crate-rushell_deps_pom-3.2.0-jeff.1 (c (n "rushell_deps_pom") (v "3.2.0-jeff.1") (h "0a8gjw3fk4xzk1b7j4r14vrli08ckgm439rx99zpxms34x3f9mm9")))

(define-public crate-rushell_deps_pom-3.2.0-jeff.2 (c (n "rushell_deps_pom") (v "3.2.0-jeff.2") (h "0jvwn7xpqm6zgywigq70pkdkcvrixk1mnc2imv60cqrqdr7z0jkv")))

