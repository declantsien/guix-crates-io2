(define-module (crates-io ru sh rush-interpreter-vm) #:use-module (crates-io))

(define-public crate-rush-interpreter-vm-0.1.0 (c (n "rush-interpreter-vm") (v "0.1.0") (d (list (d (n "rush-analyzer") (r "^0.1.0") (d #t) (k 0)))) (h "1cx9hwgq1jab31k310436hz3sppzyh3a1lfjn5vc70qmxk71nn7b")))

(define-public crate-rush-interpreter-vm-0.1.1 (c (n "rush-interpreter-vm") (v "0.1.1") (d (list (d (n "rush-analyzer") (r "^0.1.0") (d #t) (k 0)))) (h "0h0lnaxjnzshza4bmybrivkhgz1vqn1zifczjaw272iwdppc6c2d")))

(define-public crate-rush-interpreter-vm-0.1.2 (c (n "rush-interpreter-vm") (v "0.1.2") (d (list (d (n "rush-analyzer") (r "^0.1.0") (d #t) (k 0)))) (h "0aqr0ywdcpqiq30kzgrr9hypr246z15ccb1g6ml59cbxabfv4ijz")))

