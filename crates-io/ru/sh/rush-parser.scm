(define-module (crates-io ru sh rush-parser) #:use-module (crates-io))

(define-public crate-rush-parser-0.1.0 (c (n "rush-parser") (v "0.1.0") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)))) (h "04x2qvhg72yvbqkpvck24c5vhgm2mf442lz35bn6kawlg0a2qddd")))

(define-public crate-rush-parser-0.1.1 (c (n "rush-parser") (v "0.1.1") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)))) (h "028y7bzs0fyp3f6b0ds06i6lmjwyf3mvwnrgyrx7ggp0mr5bj3kg")))

