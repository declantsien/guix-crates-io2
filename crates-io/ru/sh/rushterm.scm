(define-module (crates-io ru sh rushterm) #:use-module (crates-io))

(define-public crate-rushterm-0.1.0 (c (n "rushterm") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1q2ffmrmbgkb7zixvfwp86fgrp3f296h2x8scm1nhky6rl84vznp") (y #t)))

(define-public crate-rushterm-0.1.1 (c (n "rushterm") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0q2cy707si08nl8x8daplml0h8g2gq2kjq2fpsm645v7p98z7f90") (y #t)))

(define-public crate-rushterm-0.1.2 (c (n "rushterm") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "05czf4ck3y1skr1dh50w5rz00wvpr5kpm4zyc1d7zpgi19rwi9fm") (y #t)))

(define-public crate-rushterm-0.1.3 (c (n "rushterm") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0h885rbj80j4bm04ayhihfl5c0q03bvj8jqx9lkivkz9dn556qin") (y #t)))

(define-public crate-rushterm-0.1.4 (c (n "rushterm") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "04ww1lrdp0s0b50zgpsczlwxpk4m5ynpfs9ph3cqdv824ipv547k") (y #t)))

(define-public crate-rushterm-0.1.5 (c (n "rushterm") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0iv5xvcf39v81wv0wwaba08xwk3hlfmir250f1zv36q9mn5w036v") (y #t)))

(define-public crate-rushterm-0.2.0 (c (n "rushterm") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1p33nlqlbqqpffwh2sbmadqw4nbypq9hf6181056vgghcd6bva5b") (y #t)))

(define-public crate-rushterm-0.2.1 (c (n "rushterm") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0pswhlnyz0dwp0z1znvkgswwrx0bqij7dk143cdkxk5hm2xkal18") (y #t)))

(define-public crate-rushterm-0.3.0 (c (n "rushterm") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1qxraq9dylf5ahv8g3gk4hyppgxk4wk1nfcgcvdsma3lg9qqlqmw") (y #t)))

(define-public crate-rushterm-0.3.1 (c (n "rushterm") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0dyccxh7mx6lca4vizrnyxnxm8zsj62p9k12npm5mgx7n1lm9jis") (y #t)))

(define-public crate-rushterm-0.3.2 (c (n "rushterm") (v "0.3.2") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0pl6khswlpxks5pm8g6dwsqlhqvy2scn3q3vd98micbd8gpk3wyw") (y #t)))

(define-public crate-rushterm-0.4.0 (c (n "rushterm") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0riiickvpjvzlk3hrwnf0dindjjyflvpnw81pbwrddlyg0qn4zz7") (y #t)))

(define-public crate-rushterm-0.4.1 (c (n "rushterm") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0r9kfczwkc2xv61mchcxjdd4kcw50whxqd4cjnrf9qs0qzgmizk8") (y #t)))

(define-public crate-rushterm-0.4.2 (c (n "rushterm") (v "0.4.2") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "1pv61xxgzgb0x3pn15dzfw98wipgnk7hmd27hw9rv2hylcg9f9y5") (y #t)))

(define-public crate-rushterm-0.5.0 (c (n "rushterm") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0a0cb7kiyfxcvyrd5lirgm9b6b7vswrlxcar59kihvb3ny1hxnmj") (y #t)))

(define-public crate-rushterm-0.5.1 (c (n "rushterm") (v "0.5.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "04ydqzpx81a2a43jrpgay1zyzld59n7a9jw9jf27c962qh234n4s") (y #t)))

(define-public crate-rushterm-0.5.2 (c (n "rushterm") (v "0.5.2") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "011x4rvsg271zzk6vdca8h7hfwj2b01ak7k43qjhd8wj68lxisv3") (y #t)))

(define-public crate-rushterm-0.6.0 (c (n "rushterm") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0pm3g1zn2p5yy90090qhxhfid3wgnpj3mcvn3nz3ddkfq6v1g7bh") (y #t)))

(define-public crate-rushterm-0.6.1 (c (n "rushterm") (v "0.6.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "111bidwkdddr4gxwdjwm4z77ww4xz31b06m740bb61k0hab17whc") (y #t)))

(define-public crate-rushterm-1.0.0 (c (n "rushterm") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0crvk3fpp54sapbnqx7ka57qcycl7ss447cym66g1lh3z2znfz3p") (y #t)))

(define-public crate-rushterm-1.0.1 (c (n "rushterm") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)))) (h "0lb94dvdqkhcqnhrb9bgm0kb7b7b6va5kwfv9hmjd61jyi9q9cqy") (y #t)))

(define-public crate-rushterm-1.1.0 (c (n "rushterm") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)))) (h "0195x2w3lb53dc6x0wymm7iksr7162mqprrqpvjxr1m6rgs0qs46") (y #t)))

(define-public crate-rushterm-1.1.1 (c (n "rushterm") (v "1.1.1") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)))) (h "181v5z5zna3ky1p7h230ncnn96zmvj3ig88hp6qdnm578gzqyjqj") (y #t)))

(define-public crate-rushterm-1.1.2 (c (n "rushterm") (v "1.1.2") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)))) (h "1d3kw0pd36aggcpm252v53qzmrmk2bra80xmw51iw9sglaba4jj2") (y #t)))

