(define-module (crates-io ru sh rush-analyzer) #:use-module (crates-io))

(define-public crate-rush-analyzer-0.1.0 (c (n "rush-analyzer") (v "0.1.0") (d (list (d (n "rush-parser") (r "^0.1.0") (d #t) (k 0)))) (h "0nqakkws296qn1a4h167rjdra6fwi0s7fnkfgws0cfkg8dlp5ifa")))

(define-public crate-rush-analyzer-0.1.1 (c (n "rush-analyzer") (v "0.1.1") (d (list (d (n "rush-parser") (r "^0.1.1") (d #t) (k 0)))) (h "0s3y3irjbmplx7m4hk73gc1mmxsh6ffp3zq9r73lp8kscmg5fb5d")))

