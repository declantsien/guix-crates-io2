(define-module (crates-io ru sh rusher) #:use-module (crates-io))

(define-public crate-rusher-0.0.1 (c (n "rusher") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0gpdcg9xnlbrn2a9bxq4vwsq5dc3cmxi00lk2vn9fcvzxd674v87")))

(define-public crate-rusher-0.0.0 (c (n "rusher") (v "0.0.0") (h "15nlg6b4avmdggwpyi4iaxw0v35pyj6bkgk3b93xxh1r9j0xwl63")))

