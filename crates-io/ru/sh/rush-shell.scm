(define-module (crates-io ru sh rush-shell) #:use-module (crates-io))

(define-public crate-rush-shell-0.1.0 (c (n "rush-shell") (v "0.1.0") (d (list (d (n "atty") (r "^0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "0lzji86kmp1cwn91lxl9q6hjw4c5qrdyb57hjdvikdwmmn1wa7dy") (y #t)))

(define-public crate-rush-shell-0.1.1 (c (n "rush-shell") (v "0.1.1") (d (list (d (n "atty") (r "^0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "085d0j4las0q5bam447zb9h5x44k9wzs49sydvdd60cmavmzdhj4")))

