(define-module (crates-io ru sh rush) #:use-module (crates-io))

(define-public crate-rush-0.0.1 (c (n "rush") (v "0.0.1") (h "1aylahzk8schdiwxd9ygijhk2c4fqdzn9gi4k2pq37ddhy1ik3rp") (y #t)))

(define-public crate-rush-0.1.0 (c (n "rush") (v "0.1.0") (h "0z0iikzd3vppz50jbhgh40jdsi584zz7klyldycvi8jlmlbb3jcz")))

(define-public crate-rush-0.1.2 (c (n "rush") (v "0.1.2") (h "1nmjcjrs6hr16bz77gfdxmd4xlqpr3wvkpdzckij56iv5wn6ncbm")))

(define-public crate-rush-0.1.3 (c (n "rush") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0vp9arbid1lqwjjbpfn47rl73q8n174mqwfp7x65yyhsk1r9qwxi")))

(define-public crate-rush-0.1.4 (c (n "rush") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "0fwkrl50cfva58hcrkxbiv9ai9dv7ddz16p08mjsjcvqj447kjsg")))

(define-public crate-rush-0.1.5 (c (n "rush") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1wx3f85jfpk43avgq126cnh4bz183rsv55zkh02dwyflkci3izlk")))

(define-public crate-rush-0.1.6 (c (n "rush") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1lfpfdn66y53gh2cgn5f57fa0vx273vkgz4idsx8ssm64bx63lyq")))

(define-public crate-rush-0.1.7 (c (n "rush") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "10c4bghkny2nzh3zhrmq1793k70902d7y4ir0mdmxrzv21hnzvyk")))

(define-public crate-rush-0.1.8 (c (n "rush") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "086jirnsm44sjxqbmmxzwlsc7cid091hs6gs33bgywh8z9js3nfn")))

(define-public crate-rush-0.1.9 (c (n "rush") (v "0.1.9") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "04kxz619caa8q20m1cid2ca21p3chgqh0bqssn5wi4a0r1wz0g25")))

(define-public crate-rush-0.1.10 (c (n "rush") (v "0.1.10") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0jq19czcm6hdzd06vvjqdzn0z90hfrsvary2s7wdv9b3gnnmw04f")))

(define-public crate-rush-0.1.11 (c (n "rush") (v "0.1.11") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "0mx1mm1am6naqki3gsw8qxv1vhsk696gm4chv924ypfh369792yk")))

