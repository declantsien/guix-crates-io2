(define-module (crates-io ru sh rush-sys) #:use-module (crates-io))

(define-public crate-rush-sys-0.0.1 (c (n "rush-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "const-cstr") (r "^0.1") (d #t) (k 0)))) (h "0ahvcw02kadk44x12vjn47kvshg30rs8vbrc6mdh907452dz3ghc")))

(define-public crate-rush-sys-0.0.2-alpha.1 (c (n "rush-sys") (v "0.0.2-alpha.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "const-cstr") (r "^0.1") (d #t) (k 0)))) (h "1bl74zg7n85357bk2lxcpzklb1zf8z4xyabx54vy26vysxx8ckp1")))

(define-public crate-rush-sys-0.0.2-alpha.2 (c (n "rush-sys") (v "0.0.2-alpha.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "const-cstr") (r "^0.1") (d #t) (k 0)))) (h "0j82wpjcwpl4vwmyd33rnx6qnack0qb9cg98j309z026kpkl1lgk")))

