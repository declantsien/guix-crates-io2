(define-module (crates-io ru is ruisa) #:use-module (crates-io))

(define-public crate-ruisa-0.0.1 (c (n "ruisa") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("aarch64_simd"))) (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "ruisa-path") (r "^0.0.1") (k 0)))) (h "08b8q2n2kkdf571bs0i2g2yf7nw1n0fixsdvmgjk6imbvi4jp993") (f (quote (("std" "ruisa-path/std") ("simd") ("png-format" "std" "png") ("no-std-float" "ruisa-path/no-std-float") ("default" "std" "simd" "png-format"))))))

