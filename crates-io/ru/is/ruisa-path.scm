(define-module (crates-io ru is ruisa-path) #:use-module (crates-io))

(define-public crate-ruisa-path-0.0.1 (c (n "ruisa-path") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (k 0)))) (h "1d99dzxyibv3ik3a2yb96k8da5277h91g17528x0lf7c5x97bhjq") (f (quote (("std") ("no-std-float" "libm") ("default" "std"))))))

