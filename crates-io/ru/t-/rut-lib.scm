(define-module (crates-io ru t- rut-lib) #:use-module (crates-io))

(define-public crate-rut-lib-0.1.0 (c (n "rut-lib") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ylnn5n6nw875wn156fx4n2kpv66giqizf47zbrfk31b5qmiphx9") (y #t)))

(define-public crate-rut-lib-0.1.1 (c (n "rut-lib") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lx7qw33j4jc72labhna61gdyivbvs8ghnj1j0xxri0mpgd2fqjh")))

(define-public crate-rut-lib-0.1.2 (c (n "rut-lib") (v "0.1.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-clippy" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00qbbn6v08q7rw0n7m2539ha012c9fsk76a5ffxillk0cf8vwbph")))

