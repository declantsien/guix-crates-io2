(define-module (crates-io ru pa rupantor) #:use-module (crates-io))

(define-public crate-rupantor-0.1.0 (c (n "rupantor") (v "0.1.0") (d (list (d (n "json") (r "^0.11.8") (d #t) (k 0)))) (h "1d2lna9361kmv4l98bn0gy3ysix8bpd59b5qcwzz0wnrvk2n1c68")))

(define-public crate-rupantor-0.2.0 (c (n "rupantor") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stringplus") (r "^0.1") (d #t) (k 0)))) (h "1p681azhry7rvchsa53lk6hvrjl0wiifv5zf8x6510vzk0sgjz8p")))

(define-public crate-rupantor-0.3.0 (c (n "rupantor") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stringplus") (r "^0.1") (d #t) (k 0)))) (h "0jvabzldk0d0gfsycpkwakm4rw8b8am4pfvdjjq2jl80hqlq1sq4")))

