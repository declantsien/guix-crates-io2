(define-module (crates-io ru pa rupamix) #:use-module (crates-io))

(define-public crate-rupamix-0.1.0 (c (n "rupamix") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pulse") (r "^2.28.1") (d #t) (k 0) (p "libpulse-binding")))) (h "0rknb8dqr7rnaiwpshqy00a6dgnajdmgr2amnz635485rqh0hlm5")))

(define-public crate-rupamix-0.3.0 (c (n "rupamix") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pulse") (r "^2.28.1") (d #t) (k 0) (p "libpulse-binding")))) (h "1flcpk33qlm9hbxbvlcqig3knb037fxrx4wfs9c9gn5z4fp0ny6k")))

(define-public crate-rupamix-0.4.0 (c (n "rupamix") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pulse") (r "^2.28.1") (d #t) (k 0) (p "libpulse-binding")))) (h "1hiqfknlksdx84smyxcvycr6xpxfks72xrs0n3izd4aj77nx7nll")))

(define-public crate-rupamix-0.5.0 (c (n "rupamix") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "pulse") (r "^2.28.1") (d #t) (k 0) (p "libpulse-binding")))) (h "1a3qy5j0wwjp0kh14182rgg75hd3j05qf8kbyhdl4fnfydr8nq26") (f (quote (("extractor" "colored"))))))

(define-public crate-rupamix-0.6.0 (c (n "rupamix") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "pulse") (r "^2.28.1") (d #t) (k 0) (p "libpulse-binding")))) (h "0i0ln780xdk92qknrpa2mwnq4xy5bj41nkahhc17wqsc49n2rnka") (f (quote (("extractor" "colored"))))))

(define-public crate-rupamix-0.7.0 (c (n "rupamix") (v "0.7.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "pulse") (r "^2.28.1") (d #t) (k 0) (p "libpulse-binding")))) (h "0za78c0jkrm5dhb6wyfpnk3g12dbd0hx069j0jf6gm1jgdr42q5i") (f (quote (("extractor" "colored"))))))

(define-public crate-rupamix-1.0.0 (c (n "rupamix") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "pulse") (r "^2.28.1") (d #t) (k 0) (p "libpulse-binding")))) (h "0fbiv6h4rr5wq2zbgf57wdabxhfbrpq32l0wz3bfi9nb9p3n8ws8") (f (quote (("extractor" "colored"))))))

