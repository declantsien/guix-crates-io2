(define-module (crates-io ru id ruiden) #:use-module (crates-io))

(define-public crate-ruiden-0.1.0 (c (n "ruiden") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9") (f (quote ("rtu"))) (k 0)) (d (n "tokio-serial") (r "^5") (d #t) (k 0)))) (h "01267kx1k9vybh9cgpng61b6mfl195063gx0bal4p1ffji0gpml3")))

(define-public crate-ruiden-0.1.1 (c (n "ruiden") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9") (f (quote ("rtu"))) (k 0)) (d (n "tokio-serial") (r "^5") (d #t) (k 0)))) (h "1ms38wfcbsp9gy3fh1icygkb1fm3xfzm004d0sx13sy502zhgkk1")))

(define-public crate-ruiden-0.2.0 (c (n "ruiden") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9") (f (quote ("rtu"))) (k 0)) (d (n "tokio-serial") (r "^5") (d #t) (k 0)))) (h "02r7ky2zcsvrqamd316xp24lbhsdchcknwgk1axa37rd8x933w9k")))

(define-public crate-ruiden-0.3.0 (c (n "ruiden") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9") (f (quote ("rtu"))) (k 0)) (d (n "tokio-serial") (r "^5") (d #t) (k 0)))) (h "13zms0sb12y22cphk1khb48ym7xc709dyyi012aj4flnk3sk8v2f")))

(define-public crate-ruiden-0.4.0 (c (n "ruiden") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9") (f (quote ("rtu"))) (k 0)) (d (n "tokio-serial") (r "^5") (d #t) (k 0)))) (h "0nvypb9i2kg83p354gg1nvfz535fszg6gj2n9npqy3h4jabrr9fp")))

