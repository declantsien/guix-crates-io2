(define-module (crates-io ru at ruatom) #:use-module (crates-io))

(define-public crate-ruatom-0.1.0 (c (n "ruatom") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ra65gjfvnsy5rc1jlv7579mv5bsn41hc4mh3i9n0ryp5ayk3f0k")))

