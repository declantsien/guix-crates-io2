(define-module (crates-io ru sm rusmo) #:use-module (crates-io))

(define-public crate-rusmo-0.1.0 (c (n "rusmo") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "08brig9d6y9v6b554zkyv5xclb8mmjxmi98wndhdb49rypk6s45m") (y #t)))

(define-public crate-rusmo-0.1.1 (c (n "rusmo") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "16ab21pqiiriackic5fg5qwaymlia2hc6dprjpm8mjmrxcq707h2") (y #t)))

(define-public crate-rusmo-0.1.2 (c (n "rusmo") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "163yj435dimzvssb605agcqww3aw6n3d53733h1w3zngb980j91w") (y #t)))

(define-public crate-rusmo-0.1.3 (c (n "rusmo") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "186rnhxm9mwhhg3yh92r0fvqxvpr4mbwlh6pfrmz905l4lmhyfjc") (y #t)))

(define-public crate-rusmo-0.1.4 (c (n "rusmo") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1y1wl3h946jjs9jp467yjxsh1hrkwwg7d00xxa81m4j9nvdcaim8") (y #t)))

(define-public crate-rusmo-0.1.5 (c (n "rusmo") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cmd_lib") (r "^0.6.5") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1n77jz461j7dj5zd05pmskd7hixsbpk97xr1vmcq4if1zn5sm3kr") (y #t)))

(define-public crate-rusmo-1.0.0 (c (n "rusmo") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cmd_lib") (r "^0.6.5") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1cvgpvwwfz8vsbmvfxd7icpb4zrbbhns6gvb6dwr9v6v5n873gwh")))

(define-public crate-rusmo-1.0.1 (c (n "rusmo") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cmd_lib") (r "^0.6.5") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "04f1r3hwm2hbmbg1mnlk99pbg4z72kkmx8b3q2vn104yfqy6c9hi")))

