(define-module (crates-io ru mm rummikub_solver) #:use-module (crates-io))

(define-public crate-rummikub_solver-0.1.0 (c (n "rummikub_solver") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "01j2yx4hvmv9i59s2p2jdd9y80ck24zsvlxvnbzsrxvgjgv45zg4")))

(define-public crate-rummikub_solver-0.1.1 (c (n "rummikub_solver") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "077p2nf6ynv06g7hy1811s7134yhlwrzwlxd1iiwkmg3ia4c781n")))

