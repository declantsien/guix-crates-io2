(define-module (crates-io ru wr ruwren-sys) #:use-module (crates-io))

(define-public crate-ruwren-sys-0.2.0 (c (n "ruwren-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yi9ibh0wjnxy3vh1r4ncbjb6721vcim8n9imhzk2bzy11har7zb") (l "wren")))

(define-public crate-ruwren-sys-0.2.1 (c (n "ruwren-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r2jh8fyaxha1xw35m49jvahvwq0i0hzwvqphfjmy1vqqi0x362z") (l "wren")))

(define-public crate-ruwren-sys-0.3.0 (c (n "ruwren-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02qjqc088ilkr49bzrb1ixd3z19gsbjrsrgspj02lrlvz5fyk904") (l "wren")))

(define-public crate-ruwren-sys-0.4.0 (c (n "ruwren-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07659wiyhdj49wdznkgdwv67fjxjijlw9aacbpgqpbp5j88c3ny3") (l "wren")))

(define-public crate-ruwren-sys-0.4.5 (c (n "ruwren-sys") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zwvnk47zkvsy1g4faz7hr7mqmvpbmzm0ipk4r329mdksfym4djf") (l "wren")))

(define-public crate-ruwren-sys-0.4.10 (c (n "ruwren-sys") (v "0.4.10") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03hnvqcxhmc8dw83d02yzqj2y20fs4pyv8182xfcdi3a1f0s303n") (l "wren")))

