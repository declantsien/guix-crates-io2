(define-module (crates-io ru wr ruwren-macros) #:use-module (crates-io))

(define-public crate-ruwren-macros-0.4.5 (c (n "ruwren-macros") (v "0.4.5") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0jszmavh5dj9d6ik427d9qwcpqkaqv9n8q851x95gz68hkcjhj2k")))

(define-public crate-ruwren-macros-0.4.6 (c (n "ruwren-macros") (v "0.4.6") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "08x51xj961l2mw74n8792rx52xg0smf07221g5gml38brly74j3p")))

(define-public crate-ruwren-macros-0.4.7 (c (n "ruwren-macros") (v "0.4.7") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xpzdnnb3cfh1yyln8d6mrdcny4jjgr18p6gb7wmw31li0qypyqi")))

(define-public crate-ruwren-macros-0.4.10 (c (n "ruwren-macros") (v "0.4.10") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0aw5z4ynpcr4p73ngnvfhszbxg83l000hyihzs9m2746b74j9bln")))

