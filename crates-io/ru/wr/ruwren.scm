(define-module (crates-io ru wr ruwren) #:use-module (crates-io))

(define-public crate-ruwren-0.2.0 (c (n "ruwren") (v "0.2.0") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0vqpbn8867qcnghp59s7makrp0dy67kd5p6infarjfc05xwz98hb")))

(define-public crate-ruwren-0.2.1 (c (n "ruwren") (v "0.2.1") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0ncwc6wnd9617f8gd4aqjvjp7z6g56jmpql85wy2vzygqf7r56ni")))

(define-public crate-ruwren-0.2.2 (c (n "ruwren") (v "0.2.2") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0r99sa83qdmsvjpdmgc730ksz895wyzzwg2w0s8cfjpgarqhrq5s")))

(define-public crate-ruwren-0.2.3 (c (n "ruwren") (v "0.2.3") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0wfnvldiqrdk01asixzs134ibfxqhsf9iiscbg6d3wd2kpa0369c")))

(define-public crate-ruwren-0.2.4 (c (n "ruwren") (v "0.2.4") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0szgjk1szs7cllcfh1nvla6hgi923kyicvc2af2m4gn0kg28xg6c")))

(define-public crate-ruwren-0.2.5 (c (n "ruwren") (v "0.2.5") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1rrgibhai2d58h9zqpm2i38iy2szv0rz78d2d1m4bb7ifq468b57")))

(define-public crate-ruwren-0.2.6 (c (n "ruwren") (v "0.2.6") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "10qbwfrfg1aiwkbbs9xy0rzs38n9ckjrwcb20j2lwcdj566l05sk")))

(define-public crate-ruwren-0.2.7 (c (n "ruwren") (v "0.2.7") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1alfdjzq9ngbi7hd4g70c28dvap0slzqvb3rf4sf1shi2bya5lh3")))

(define-public crate-ruwren-0.2.8 (c (n "ruwren") (v "0.2.8") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1gmr7nbi8a3mbx23wbxh66f5khx5pb5xvchn8ww7ai1wjcpyzw24")))

(define-public crate-ruwren-0.2.9 (c (n "ruwren") (v "0.2.9") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0gn0n62krg0jqj84nkps4zzma9f4w6fc2jyxp9gj5nf8w2z9pm69")))

(define-public crate-ruwren-0.2.10 (c (n "ruwren") (v "0.2.10") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "14dix08kc4x5sw8anzsh1xjjk5d1f176vzm29n8znci4drm0hkkq")))

(define-public crate-ruwren-0.2.11 (c (n "ruwren") (v "0.2.11") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1ziz3nd8dyv8qnhrdrrmwp86bxi0aq9z3q5wm808a3m6d7idz5kd")))

(define-public crate-ruwren-0.2.12 (c (n "ruwren") (v "0.2.12") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "04w33x0ry66711zaz2f9zila890vybjbi831vwlr4zkgi6z8axh8")))

(define-public crate-ruwren-0.2.13 (c (n "ruwren") (v "0.2.13") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "178cjs7gl3imxkkai8m06x67f8m1yjq42drlfk4swks8p2ik0kc0")))

(define-public crate-ruwren-0.2.14 (c (n "ruwren") (v "0.2.14") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1f3cl5s9cidbfj6dlm9ki4j14l41awq160npiz95dk0cp0bsw2ql") (y #t)))

(define-public crate-ruwren-0.2.15 (c (n "ruwren") (v "0.2.15") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "05sx442zfl4pmxli6lj3b1grs1s2852f9b12lvsi8cyfsz73032q")))

(define-public crate-ruwren-0.2.16 (c (n "ruwren") (v "0.2.16") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0bfq1696s8yw0c1wx5rys3id8lp054kifz3jkrks6d2qcm6nif6q")))

(define-public crate-ruwren-0.2.17 (c (n "ruwren") (v "0.2.17") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0r25napaaw3dckhsvrbd5d7vrm7nzli1yx6pn5yanvcs29cv5s8j")))

(define-public crate-ruwren-0.2.18 (c (n "ruwren") (v "0.2.18") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "15jm6shnwzf9i8hn39x90gphzqr39nmqrll2whgpqfpa8hpy459f") (y #t)))

(define-public crate-ruwren-0.2.19 (c (n "ruwren") (v "0.2.19") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0j7i6xpfhvn2inc35ni12q43jww0zn1pg0zz8f5q2xllzifqca38")))

(define-public crate-ruwren-0.2.20 (c (n "ruwren") (v "0.2.20") (d (list (d (n "ruwren-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0s316x78d1mf4n82934k1y37x39l715lzyphyq7mblarhfwmbz62")))

(define-public crate-ruwren-0.3.0 (c (n "ruwren") (v "0.3.0") (d (list (d (n "ruwren-sys") (r "^0.3") (d #t) (k 0)))) (h "0ylf7m8qd7z547znw4r1i9kymq1bsc5cxq47apbcfb6x9cjbkqw6")))

(define-public crate-ruwren-0.3.1 (c (n "ruwren") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.3") (d #t) (k 0)))) (h "0v0mj9nsnfx7g2307nymppqxhpj275vy5nyp6vb8vv35xlr2fcl5")))

(define-public crate-ruwren-0.3.2 (c (n "ruwren") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.3") (d #t) (k 0)))) (h "1kikmw1p1nfwb9qkvfwpbcf4a323sdk37d5p5aw1p3lk9z5m1z75")))

(define-public crate-ruwren-0.3.3 (c (n "ruwren") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.3") (d #t) (k 0)))) (h "07hl3ghniqyjvb4clmq1f52y5h8vzsncg8w85y41ws0arq4sn262")))

(define-public crate-ruwren-0.3.4 (c (n "ruwren") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.3") (d #t) (k 0)))) (h "0cssgjsy7znr2swizlh1h1y36ssn5zqpyxpljqvhcr0f17jg8x4k")))

(define-public crate-ruwren-0.3.5 (c (n "ruwren") (v "0.3.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.3") (d #t) (k 0)))) (h "0p556hj3zhkfq746kagmh5ri2i0p74zsvhiyzhg4licyrrrbvcny")))

(define-public crate-ruwren-0.4.0 (c (n "ruwren") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.4") (d #t) (k 0)))) (h "0g6jybb8x0lgw688z4s1zxwdf6sd4wshnr0w4l2s1hh7gn592aq5") (y #t)))

(define-public crate-ruwren-0.4.1 (c (n "ruwren") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.4") (d #t) (k 0)))) (h "0f27402l5aj3yr1bx5vf8zb91q44j4lzq9a1963zpxrc5dvhn5fk") (y #t)))

(define-public crate-ruwren-0.4.2 (c (n "ruwren") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.4") (d #t) (k 0)))) (h "1sbyysv7xrckgp9q7qkv6hm1nw1v07an7s99fjirlfg6gzgayclq")))

(define-public crate-ruwren-0.4.3 (c (n "ruwren") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.4") (d #t) (k 0)))) (h "0kz4zyha810g1sk3ljsyqddqvljkwi63jbfa3nij28nkgikm5g87") (y #t)))

(define-public crate-ruwren-0.4.4 (c (n "ruwren") (v "0.4.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-sys") (r "^0.4") (d #t) (k 0)))) (h "02wraa2gkjqwpk7c4r0q4hprjpvrzz4w3dds2i5jmraic8hmhxz6")))

(define-public crate-ruwren-0.4.5 (c (n "ruwren") (v "0.4.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-macros") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "ruwren-sys") (r "^0.4.5") (d #t) (k 0)))) (h "0n3b3c3ccr4w09hwv75sp9p227gd098kfbqddp1r0g9sc7pbfln8") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4.6 (c (n "ruwren") (v "0.4.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-macros") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "ruwren-sys") (r "^0.4.5") (d #t) (k 0)))) (h "0jbhxiyas296blzx3h54k2nd08skan07a1mnk5rckwc30mjqawkk") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4.7 (c (n "ruwren") (v "0.4.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-macros") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "ruwren-sys") (r "^0.4.5") (d #t) (k 0)))) (h "0ylffsgvf2r1xyp0hn7pgsif182f0ir10b25s9m64y4wpkbgzfx2") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4.8 (c (n "ruwren") (v "0.4.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-macros") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "ruwren-sys") (r "^0.4.5") (d #t) (k 0)))) (h "1h56rxhfdr2b6k8szs5aq1b2z6zkf7l1k5alcyxzzac9kc2y2rsl") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4.9 (c (n "ruwren") (v "0.4.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-macros") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "ruwren-sys") (r "^0.4.5") (d #t) (k 0)))) (h "0abj86pm7d5gjlba7v6k08pkx2fxnx7rkmcj16w5a2nn8dwjxfgg") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4.10 (c (n "ruwren") (v "0.4.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ruwren-macros") (r "^0.4.10") (o #t) (d #t) (k 0)) (d (n "ruwren-sys") (r "^0.4.10") (d #t) (k 0)))) (h "1azwrn88w3x3fdd9j336vayc8zjl45q62655pawwk6jyd8lsyi7d") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:ruwren-macros"))))))

