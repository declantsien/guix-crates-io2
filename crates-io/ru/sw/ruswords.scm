(define-module (crates-io ru sw ruswords) #:use-module (crates-io))

(define-public crate-ruswords-0.1.0 (c (n "ruswords") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "xvii") (r "^0.3") (d #t) (k 0)))) (h "16xbwpxw0crmpr8dcayb2gxc5l03mhq8c6hy9rwijcw1lm62lshy")))

