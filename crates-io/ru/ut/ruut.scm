(define-module (crates-io ru ut ruut) #:use-module (crates-io))

(define-public crate-ruut-0.1.0 (c (n "ruut") (v "0.1.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1flcgq9nicd459afvz42irb8qxi126k5brxrin8zp5p1b6dwmk7k")))

(define-public crate-ruut-0.2.0 (c (n "ruut") (v "0.2.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1rvzmkgh7hpnmhd7c2114zy2wrvbxz0n85427ainqiglgb623xlp")))

(define-public crate-ruut-0.3.0 (c (n "ruut") (v "0.3.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "05bm4ksv27kai3jdf9say412a4kf1klikxazjsl5073iwkny0v1z")))

(define-public crate-ruut-0.4.0 (c (n "ruut") (v "0.4.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0zx2sg7aj0xf44405jj4vbg16rs89zcdqxg29nvsf3r5zbirmb6g")))

(define-public crate-ruut-0.5.0 (c (n "ruut") (v "0.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0xx257pagf8a7c20ann15fv54qwdyfjc68pfkrlxdj9x09jfa7gm")))

(define-public crate-ruut-0.5.1 (c (n "ruut") (v "0.5.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0fsvy29ycr3wdpvyyadxkh7pz9fhqyy17zkpfhffa7p7x3fc016y")))

(define-public crate-ruut-0.6.0 (c (n "ruut") (v "0.6.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "json5") (r "^0.2.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "09x6sq04gx8w6x4pgagy7is7jzw8ck9i8kndbqvz1cj2c11hady9")))

(define-public crate-ruut-0.6.1 (c (n "ruut") (v "0.6.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "json5") (r "^0.2.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "04n3icskjk5p2sr6i38pc6gsvyh2qpisgl3lywx9xxbrsf38fiy4")))

(define-public crate-ruut-0.7.0 (c (n "ruut") (v "0.7.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "json5") (r "^0.2.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0ipapq354m92fbarnsh6f7fsas48w155b28qfkf56rj34xhp0v8v")))

(define-public crate-ruut-0.8.0 (c (n "ruut") (v "0.8.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "json5") (r "^0.2.5") (d #t) (k 0)) (d (n "render_as_tree") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0yf7d1z8w9rccy853g63mgj60hsjd3w20fi8lvml5h6in0lm92s5")))

