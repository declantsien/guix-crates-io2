(define-module (crates-io ru ut ruut-functions) #:use-module (crates-io))

(define-public crate-ruut-functions-0.0.1 (c (n "ruut-functions") (v "0.0.1") (d (list (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0jx75ij32xyh344rbcap7vgm5b6sgr78rrz92qw6dm5x20vxgip1")))

(define-public crate-ruut-functions-0.0.2 (c (n "ruut-functions") (v "0.0.2") (d (list (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "013brcwblnyll187nxg8z01zkm6q7ikvbi5i964yxvlln40yzmw1")))

(define-public crate-ruut-functions-0.0.3 (c (n "ruut-functions") (v "0.0.3") (d (list (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0gr406mfmncx83dj7gqjdljhdarycwbrksagahr84dw10i73mxyl")))

(define-public crate-ruut-functions-0.0.4 (c (n "ruut-functions") (v "0.0.4") (h "04xdyk9s8jg6k9h7rmi88pd9jm00gxlcp96jahqigvzissvh9yr2")))

(define-public crate-ruut-functions-0.0.5 (c (n "ruut-functions") (v "0.0.5") (h "1xx8c7wr26av6gx80nzafzgaq3ikx6kbnaysrv1712br0gr8irag")))

(define-public crate-ruut-functions-0.0.6 (c (n "ruut-functions") (v "0.0.6") (h "1zwwh4cla8j24q3ncwjvsw6kh2x23c21dsi639axzb3cp0831vnd")))

(define-public crate-ruut-functions-0.0.61 (c (n "ruut-functions") (v "0.0.61") (h "0wa4dkbss4p51qcshb3lvcrc45g8pyg3n9vyr5lbbqffrvrli32g")))

(define-public crate-ruut-functions-0.1.0 (c (n "ruut-functions") (v "0.1.0") (h "1wfawqqr6dnsviy7dk9za8fnyajzg9is75brn7dfp51b297mpr1p")))

(define-public crate-ruut-functions-0.1.1 (c (n "ruut-functions") (v "0.1.1") (h "100pmgf266bldg80fh2y92lm33cgrh01l9lak4ifjd0sd8pkap1k")))

(define-public crate-ruut-functions-0.1.2 (c (n "ruut-functions") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ruut-algebra") (r "^0.1.0") (d #t) (k 0)))) (h "0d0hz7r7ia0ifk6in9g8bd6h9jwpp2acbpyp8p9r54j3wm5zighh")))

