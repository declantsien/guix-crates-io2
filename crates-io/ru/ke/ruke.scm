(define-module (crates-io ru ke ruke) #:use-module (crates-io))

(define-public crate-ruke-0.1.0 (c (n "ruke") (v "0.1.0") (d (list (d (n "ruke_core") (r "^0.1.0") (d #t) (k 0)))) (h "1j0r6kj1aqp60pq4kpb98y3pmg9df1d2d59fr46cadyp4afihfyb") (y #t)))

(define-public crate-ruke-0.1.1 (c (n "ruke") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "13lggsjddwka07fn0nap0mgzz13la2dfpbzgrh78amdkr6b5c2jy") (y #t)))

(define-public crate-ruke-0.1.2 (c (n "ruke") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1k7sp41kfxxsi6pxp6hi384gqjric5sfzqbvhp7m3rn57zn580pc") (y #t)))

(define-public crate-ruke-0.1.3 (c (n "ruke") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "04l007w4mk7rfz7zzay6xry53xxqrai5kc6ds924dkx238gl8kq2") (y #t)))

(define-public crate-ruke-0.1.4 (c (n "ruke") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "112y5d3maa7ks7nvx6cn0l35ll7zdi6bvwk9mr69pg9v0gf24z86") (y #t)))

(define-public crate-ruke-0.1.5 (c (n "ruke") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0r7kj235zp1g7vs60b5hhl4zdxcms6qhfdhl2mk2q65c8z8jfajr") (y #t)))

(define-public crate-ruke-0.1.6 (c (n "ruke") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "14xa79pfjwsdy15vx2hnxczw75rfa9x8cp6bljsvx4c12yq04bbw") (y #t)))

(define-public crate-ruke-0.1.7 (c (n "ruke") (v "0.1.7") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1jq0h645ajj93nl6aaxipivwic6wm5azimli286yzplzxrgn8h6b") (y #t)))

(define-public crate-ruke-0.1.8 (c (n "ruke") (v "0.1.8") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0psnw8bj2qngii7d4h7hzi9lasb25qi10amqr67yqs9ly62kmpwb")))

(define-public crate-ruke-0.1.9 (c (n "ruke") (v "0.1.9") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "02f5g3c3s3wma697r5vxwvfdb966zx8bgmaly6g2ffm69h5xbfab")))

(define-public crate-ruke-0.1.10 (c (n "ruke") (v "0.1.10") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0a87z82ja912n4x81cz4al561rzqcwgx1qj7bqpdg8wbxabzpddi")))

