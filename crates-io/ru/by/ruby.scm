(define-module (crates-io ru by ruby) #:use-module (crates-io))

(define-public crate-ruby-0.0.1 (c (n "ruby") (v "0.0.1") (h "1bj4pcgayvab50vc3q6c40vp9i4pxn7vhl90yswsz6zqrnm3cq2c") (y #t)))

(define-public crate-ruby-0.0.2 (c (n "ruby") (v "0.0.2") (h "0bx7wwr7ky11m410r8cdwk0ayyhclnb1yckpjk1c5c6rymx09zsw") (y #t)))

(define-public crate-ruby-0.1.0 (c (n "ruby") (v "0.1.0") (h "1iji5y3lixp5g7qc96aa8zfnwcc5xw077l2kwzgig7c734vnpmpb") (y #t)))

(define-public crate-ruby-0.1.1 (c (n "ruby") (v "0.1.1") (h "0vi3mj3j2paa4vfx5xr07k6vb9ahlxyxbf2nmv9n08fk554i7aq4") (y #t)))

(define-public crate-ruby-0.1.2 (c (n "ruby") (v "0.1.2") (h "1x44ik1dc0bk5qhg98ikhya5ixhgqxrh7aymbwyj4myxs91gry2g") (y #t)))

