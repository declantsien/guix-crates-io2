(define-module (crates-io ru by ruby-prism) #:use-module (crates-io))

(define-public crate-ruby-prism-0.19.0 (c (n "ruby-prism") (v "0.19.0") (d (list (d (n "ruby-prism-sys") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "1rvi7bl6rq9ciz78c6w0z8p73prxqjiglak55vbz9y8qq85widab") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.20.0 (c (n "ruby-prism") (v "0.20.0") (d (list (d (n "ruby-prism-sys") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "08sp8d9h2qczy9wlmr9ydzqc4ndckbaspk4vw17q1zif8a6cspl8") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.21.0 (c (n "ruby-prism") (v "0.21.0") (d (list (d (n "ruby-prism-sys") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "1ii5as4q9nwpk7j4y5vlfsryqpiy2fdjn09wziixhyfdqinjfbh6") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.22.0 (c (n "ruby-prism") (v "0.22.0") (d (list (d (n "ruby-prism-sys") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "1asdjqfkmfrl084545c4qpawjzp87w4v8h6jsg29w2vs8jcqbixf") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.23.0 (c (n "ruby-prism") (v "0.23.0") (d (list (d (n "ruby-prism-sys") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "08vy6hr2c78bivrg96rmq09d1ldmnsra6jzbglg6645ssak24p4l") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.24.0 (c (n "ruby-prism") (v "0.24.0") (d (list (d (n "ruby-prism-sys") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "18jx0j30yvfvcdqjywb79p1q766df6k4y9n1p9nfrnkq22krqnc0") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.25.0 (c (n "ruby-prism") (v "0.25.0") (d (list (d (n "ruby-prism-sys") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "1nhi4mcmacjqn9p51dkgijwpyxh40341fnifrwpllmf7damx8342") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.26.0 (c (n "ruby-prism") (v "0.26.0") (d (list (d (n "ruby-prism-sys") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "0767h6vg0qi0ivg930rplxdygh92gyhf0fhn14479h173apbkpgk") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.27.0 (c (n "ruby-prism") (v "0.27.0") (d (list (d (n "ruby-prism-sys") (r "^0.27.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "0jxy52alz6g2lzh5dw22h579rwnbalvfg3g8a2snzp09famaq30j") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.28.0 (c (n "ruby-prism") (v "0.28.0") (d (list (d (n "ruby-prism-sys") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "0wn0p3j13bdb6wkw4q8gq17w9awmyjb2y2rnzbqs46pdh6f97i98") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.29.0 (c (n "ruby-prism") (v "0.29.0") (d (list (d (n "ruby-prism-sys") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 1)))) (h "1pghvf0j62i7wpd7a988w0679m2iy1y5ba11rh141z5snma501w6") (f (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

