(define-module (crates-io ru by ruby_inflector) #:use-module (crates-io))

(define-public crate-ruby_inflector-0.0.5 (c (n "ruby_inflector") (v "0.0.5") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mba6mvp1xj6aqgpdqj1y63fncg0w7w8nji99hi1msinmzip2n9d")))

(define-public crate-ruby_inflector-0.0.6 (c (n "ruby_inflector") (v "0.0.6") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ycgd7ym1bdlfwmwhfi94abzfzjvlq9l197lyiz7v3kk9fbwk2k3")))

(define-public crate-ruby_inflector-0.0.7 (c (n "ruby_inflector") (v "0.0.7") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1by7vwfskczv5wr2zxfrbcy95mj8yfcnhwrqn8s898v4s1ms3g9z")))

(define-public crate-ruby_inflector-0.0.8 (c (n "ruby_inflector") (v "0.0.8") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02q0f24gcq7mx152vqyg1748c1k7xzqffjcsanbh03y49v4gsl4g")))

(define-public crate-ruby_inflector-0.0.10 (c (n "ruby_inflector") (v "0.0.10") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1j9zqdr9bclhq7j58lnmpw021bdikp199bs7hnzahl0mk8ap68cv")))

(define-public crate-ruby_inflector-0.0.9 (c (n "ruby_inflector") (v "0.0.9") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0wmbag0rjcxb10ws7xprkmqxcxl8fwi0jwlcyhin6v03ray0lw90")))

