(define-module (crates-io ru by ruby-parser) #:use-module (crates-io))

(define-public crate-ruby-parser-0.0.0-dev1 (c (n "ruby-parser") (v "0.0.0-dev1") (d (list (d (n "codemap") (r "^0.1.3") (d #t) (k 0)) (d (n "memchr") (r "^2.2.3") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "16s1alg5q7x7p89v3ca1hvmw83j50na22s444d4ky4rxm0lwqhv5")))

