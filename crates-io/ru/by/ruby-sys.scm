(define-module (crates-io ru by ruby-sys) #:use-module (crates-io))

(define-public crate-ruby-sys-0.1.0 (c (n "ruby-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "088mh18r9zckdjy53776x219a9xy2vs9qa10lj6ba1fddq928d1c")))

(define-public crate-ruby-sys-0.1.1 (c (n "ruby-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "186knb040i68xsspm4p5pmd9zsmawkiwwq5zxnjx2akwgn4fg8i1")))

(define-public crate-ruby-sys-0.1.2 (c (n "ruby-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "1r3cksl3p1jgczq55mxwbn1nlmsmfr79a9mg2y068yzddl6fgjph")))

(define-public crate-ruby-sys-0.1.3 (c (n "ruby-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "16gs9xg026j73ys5qfgcglfsxx29c6nhz8gxi22fp8x8pg4k4mzc")))

(define-public crate-ruby-sys-0.1.4 (c (n "ruby-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "11n3lfpp1gphmh2m9gj2rvvsi68mmaylgr5xbz85qp0amrgclw21")))

(define-public crate-ruby-sys-0.1.5 (c (n "ruby-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "04iwcmz3142gs678y2d4aigs4xmp1z0fdhb4y9mksh6q6pna6zf2") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.0 (c (n "ruby-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1flx3gn73vrrsj2jjyw9kjg0yjwhlf0aqdrs95sambffwp311xcl") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-ruby-sys-0.2.1 (c (n "ruby-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1zdik07z9v92vsxxij3wnfw8hdr7b78gyfxcch5d6hjdkyhvic7y") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-ruby-sys-0.2.2 (c (n "ruby-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "05qkss4ip10614w0dgp8zyy3x7r0f6kw7x4hj3yak6bd8lqigdpc") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.3 (c (n "ruby-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1g8y227mjhqzdnacncixw7c48jab6p7lds06gl7s83fyqcw5xryq") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.4 (c (n "ruby-sys") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0bvs29hx3lknk6f1f988qwfdsh0ppjjrsir9a403dg1s8w6cy07m") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.5 (c (n "ruby-sys") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1ndb8igs2k2cf2m15qjxqqffan1wrb5614ansl2s1isi815ifcph") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.6 (c (n "ruby-sys") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0a27km0a0q04188h7ndfygrw02fnm1br6rlisvgsl5ga1dwhmryq") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.7 (c (n "ruby-sys") (v "0.2.7") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1lja32xw99470c6nm2xxm7xs8y4jvfnya4vnc8ndd23f8mnlxkbn") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.8 (c (n "ruby-sys") (v "0.2.8") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0h02l2yrph0mfqvw75qamw82i3mcg5hmjrq8xpga6rpg5c3yb663") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.9 (c (n "ruby-sys") (v "0.2.9") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1755qd4rrld6j1ii9892dfifdar94w6fm3x08jbzdd1d4mzbzbd4") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.10 (c (n "ruby-sys") (v "0.2.10") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1fykyh9nfmpn14b8nq78rlzwqpdhqhi5qsvvd9f041g34v009kvq") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.11 (c (n "ruby-sys") (v "0.2.11") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1vdfds4ji9gjy9xh4aspgcrax646m75741sgagwxzrym5qbk8c9q") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.12 (c (n "ruby-sys") (v "0.2.12") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1vhzhd670hgfa63ji3knnym1m4vvkaklckjxgda12l6j558pqk2h") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.13 (c (n "ruby-sys") (v "0.2.13") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0i0bv779ar5vw4gwl9l2b19ilnvkjw96apvchpylaaiddyqcy558") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.14 (c (n "ruby-sys") (v "0.2.14") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "17aqn9s4rm4icypkj0p89i8lwhlay8m789lk9dkm5dm8sgzhwl4w") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.15 (c (n "ruby-sys") (v "0.2.15") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1mnbn73qvvd00dl8xf1hlgj3823s8bfmrn3iv817x5s4hlkvxrlv") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.16 (c (n "ruby-sys") (v "0.2.16") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ay4hvk73vwq8r9h0aiwgn7lafqv18qiwvxkpq0l1243iwvik8ca") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.17 (c (n "ruby-sys") (v "0.2.17") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1i0my1ay629iccsbgksyj9g0igmbmx5dp64fnzlzygybb4ly3n9p") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.18 (c (n "ruby-sys") (v "0.2.18") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "05qvz8yfxa94y63fnfh03k8d5i2308klnjxjw54ak885c12k7jvg") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-ruby-sys-0.2.19 (c (n "ruby-sys") (v "0.2.19") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "13rcyzxssxh9vaq9dvxk6xq7pgnyccpc4wik619svdhb1c9zl8xm") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2.20 (c (n "ruby-sys") (v "0.2.20") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1iq4p9s0l4m82qw5c13b2l54jl40nmlf2n3237f36dz9gkqhj5g7") (f (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.3.0 (c (n "ruby-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "07hw7b676jxv5xz9r8l6qmgfrd0vm6wxzcrc57vp4daaj1miggvv") (f (quote (("unstable") ("default"))))))

