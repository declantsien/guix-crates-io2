(define-module (crates-io ru by ruby-prism-sys) #:use-module (crates-io))

(define-public crate-ruby-prism-sys-0.19.0 (c (n "ruby-prism-sys") (v "0.19.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1kx07jw5fnq5iq63bdmnik7q6v9wppvp56sc24avmx35h2n5mb6f") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.20.0 (c (n "ruby-prism-sys") (v "0.20.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1mdqfv9vlsf76rinqxwjg6hc4szj5xvdp5q7xkvagm6q4zqi8lb2") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.21.0 (c (n "ruby-prism-sys") (v "0.21.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1a3jif9s62p1q2vralgg3mmy7vrmbagf826mf15dfqxis33g9wnf") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.22.0 (c (n "ruby-prism-sys") (v "0.22.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "10h0v5wwd728drc59ii1vkrqrizjqp4rc5bd1gxib09jsxfqxp0p") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.23.0 (c (n "ruby-prism-sys") (v "0.23.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "02521pgkcackc0wbapgyqfp3vzm5zxl2z7b9yyax5g4cm2i19l9v") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.24.0 (c (n "ruby-prism-sys") (v "0.24.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0lgajj80qxx2d11gy2ywa8kw3v3g37lv9prv1r8bh7yd6msw2flq") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.25.0 (c (n "ruby-prism-sys") (v "0.25.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0ci85gy0qxb62g1c8217y43vb7ghiyzfisicg97s8nsbz2mljgch") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.26.0 (c (n "ruby-prism-sys") (v "0.26.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0yasp0w49wlip2kxyzcy6f1wgvqqczz2fgjcbqwl9dn2mr5mmvcb") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.27.0 (c (n "ruby-prism-sys") (v "0.27.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0i9myv56x0qymvkh6r9ihbi464xcpj048100lsxfbrg8v8y7mvl4") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.28.0 (c (n "ruby-prism-sys") (v "0.28.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0kvp5grfilrd7an0a7yfzvyj576fa83ybslwgcgfxb8k1w19iscr") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.29.0 (c (n "ruby-prism-sys") (v "0.29.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1k73qmfm3z5yx153z8x2jjws93nspyy45mkdh311n8gxvamnnfbp") (f (quote (("default" "vendored")))) (l "prism") (s 2) (e (quote (("vendored" "dep:cc"))))))

