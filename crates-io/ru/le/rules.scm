(define-module (crates-io ru le rules) #:use-module (crates-io))

(define-public crate-rules-0.0.1 (c (n "rules") (v "0.0.1") (h "1kys1sv5n4akn9qjna32ymsxd4x06xanqaj3s052jk4j2r1fc8cn")))

(define-public crate-rules-0.0.2 (c (n "rules") (v "0.0.2") (h "1y63hhps2f0wbamd4q294clrr15vma432wglcsyh2z38kxa75hm7")))

