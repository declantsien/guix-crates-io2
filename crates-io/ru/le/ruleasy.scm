(define-module (crates-io ru le ruleasy) #:use-module (crates-io))

(define-public crate-ruleasy-0.1.0 (c (n "ruleasy") (v "0.1.0") (h "1mz6w44lrwk542i2cvk9x7g3lvrz1v5z2v4qpl5m7iv2s27621mk")))

(define-public crate-ruleasy-0.1.1 (c (n "ruleasy") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "07rfslyj6wdc8iflyrx38xbnz9fczp1g9nix24nq287n8ssmvw4y")))

(define-public crate-ruleasy-0.1.2 (c (n "ruleasy") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1hi54idp557b50y6i0d8h272kprs72sq2am2g3xc4insp5fd58z7")))

(define-public crate-ruleasy-0.1.3 (c (n "ruleasy") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0hhl6f77fxbp5da9qxv4ia0bpwyfihsccjriljc8ca6igjx39cvl")))

