(define-module (crates-io ru le rulex-bin) #:use-module (crates-io))

(define-public crate-rulex-bin-0.1.0 (c (n "rulex-bin") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("std" "derive" "wrap_help"))) (k 0)) (d (n "miette") (r "^4.2.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rulex") (r "^0.1.0") (f (quote ("dbg" "miette"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1yx8drdydj4xsy8apgjl6aw3dvzhn7xyh0jiv9zbpv9827cj600w")))

(define-public crate-rulex-bin-0.1.1 (c (n "rulex-bin") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("std" "derive" "wrap_help"))) (k 0)) (d (n "miette") (r "^4.2.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rulex") (r "^0.2.0") (f (quote ("dbg" "miette"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "09nyklz6w36q504a4m8kfxn4icsicylrqh6vpr2rgmlz5vinf1wc")))

(define-public crate-rulex-bin-0.3.0 (c (n "rulex-bin") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("std" "derive" "wrap_help"))) (k 0)) (d (n "miette") (r "^4.2.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rulex") (r "^0.3.0") (f (quote ("dbg" "miette"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12q1kjwp96mvlknraqkwg49g1nkizm1ma3wq11vqg54isgkz04lz")))

(define-public crate-rulex-bin-0.4.0 (c (n "rulex-bin") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("std" "derive" "wrap_help"))) (k 0)) (d (n "miette") (r "^4.2.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rulex") (r "^0.4.0") (f (quote ("dbg" "miette"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0g0jx3icfz68lky2wk3mp4q5pmfng8xyl6v3chpqch9l6n5gb7r6")))

(define-public crate-rulex-bin-0.4.3 (c (n "rulex-bin") (v "0.4.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("std" "derive" "wrap_help"))) (k 0)) (d (n "miette") (r "^4.2.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rulex") (r "^0.4.3") (f (quote ("dbg" "miette"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13zj5vq2cf72c2bkjiis15hwfrilkzgn9paqf18y1w4vabrbvbwf")))

(define-public crate-rulex-bin-0.4.4 (c (n "rulex-bin") (v "0.4.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("std" "derive" "wrap_help"))) (k 0)) (d (n "miette") (r "^4.2.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rulex") (r "^0.4.4") (f (quote ("dbg" "miette"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gikjqkn5qh8sjyddsb78g1m2diyd26wj0knyqkqk9yn5js62z1c") (y #t)))

(define-public crate-rulex-bin-0.4.5 (c (n "rulex-bin") (v "0.4.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("std" "derive" "wrap_help"))) (k 0)) (d (n "miette") (r "^4.2.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "rulex") (r "^0.4.4") (f (quote ("dbg" "miette"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jh98h3590d1fy6j5ybx8an3d1g1pwrggmzlvpaj3xqaxamjsv6y")))

