(define-module (crates-io ru le rulex-macro) #:use-module (crates-io))

(define-public crate-rulex-macro-0.1.0 (c (n "rulex-macro") (v "0.1.0") (d (list (d (n "rulex") (r "^0.1.0") (d #t) (k 0)))) (h "1zq9n4ik9xzr4h8wns1cr5ywyzwjmmpnwh5ipkwfahav7jd8vldv")))

(define-public crate-rulex-macro-0.1.1 (c (n "rulex-macro") (v "0.1.1") (d (list (d (n "rulex") (r "^0.2.0") (d #t) (k 0)))) (h "1iz5d667lvif7xcgaimxym02m5r9k1x9jhgcz08a3a42ificscmv") (f (quote (("diagnostics") ("default" "diagnostics"))))))

(define-public crate-rulex-macro-0.3.0 (c (n "rulex-macro") (v "0.3.0") (d (list (d (n "rulex") (r "^0.3.0") (d #t) (k 0)))) (h "11361r07vixila1lgjn099xxzm2wyy1hlgfvrrbp2cgy956ypch4") (f (quote (("diagnostics") ("default"))))))

(define-public crate-rulex-macro-0.4.0 (c (n "rulex-macro") (v "0.4.0") (d (list (d (n "rulex") (r "^0.4.0") (d #t) (k 0)))) (h "0anx23cd2bh6pnhcvmv8qgqp1d1y5g619hbi36554p1q99rrra67") (f (quote (("diagnostics") ("default")))) (y #t)))

(define-public crate-rulex-macro-0.4.3 (c (n "rulex-macro") (v "0.4.3") (d (list (d (n "rulex") (r "^0.4.3") (d #t) (k 0)))) (h "1cwsx3xzxxg2n4slyk7i97xiap5kqknxnz1zcharam99mb53237w") (f (quote (("diagnostics") ("default")))) (y #t)))

(define-public crate-rulex-macro-0.4.4 (c (n "rulex-macro") (v "0.4.4") (d (list (d (n "rulex") (r "^0.4.4") (d #t) (k 0)))) (h "0yj17fw5bshwh6wz0f23gpv6c0yfhcq1x4a9nmilfwdg2mw1lrlj") (f (quote (("diagnostics") ("default"))))))

