(define-module (crates-io ru le rule30) #:use-module (crates-io))

(define-public crate-rule30-0.1.0 (c (n "rule30") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "17jcn0is8hlbfnapz0xccx1qrgzyv1n51asrjq7q6kmyrp8fci2b")))

(define-public crate-rule30-0.1.1 (c (n "rule30") (v "0.1.1") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "0r6yl7lsmnr95nsm6hy56hzgrmr1gpaq75n3y1h38ysjaha3k26w")))

(define-public crate-rule30-0.1.2 (c (n "rule30") (v "0.1.2") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "106wc6173x36rlgpjibsdnqf9cwdr3m4w15bd7xrraw9j4qs82i1")))

(define-public crate-rule30-0.1.3 (c (n "rule30") (v "0.1.3") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "110859jpdzrqa0ilax16jzcrw5rqz7wi3rd6rsx8b8fhfbblr7g2")))

(define-public crate-rule30-0.1.4 (c (n "rule30") (v "0.1.4") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "19lvz026g3b2pz7g9bx1rjngfv0vbd3nh0ccrwbafmz94azsm6xq")))

(define-public crate-rule30-0.2.0 (c (n "rule30") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "1wrcgj348nrkryb2gmsl63bfpy8dk6c8z36g7j5spzdwhm77j2p1")))

