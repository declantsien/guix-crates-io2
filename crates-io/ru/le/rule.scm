(define-module (crates-io ru le rule) #:use-module (crates-io))

(define-public crate-rule-0.1.0 (c (n "rule") (v "0.1.0") (h "073n2rqp6ym1207l8bwcsi84zbnr84spqyr5f305c3jmaqay39kz")))

(define-public crate-rule-0.1.1 (c (n "rule") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06hkrpkz2k8i2z52h315q3jvgqwixw5kghzvp7bkh3xs0sm5iasx")))

(define-public crate-rule-0.1.2 (c (n "rule") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s3qv7ynyszy5x7f3y4szx5dv5a0p5fchb5dgy5qjv49bikpkds9")))

(define-public crate-rule-0.1.3 (c (n "rule") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "067nfjgkjd41lmwp6nb6w4v4qma93jd4miykixzkdwpg4fm0g6ck")))

(define-public crate-rule-0.1.4 (c (n "rule") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "145nfmh5v6bzmmc5lgva4dnp7g69pl0g5yycgnsmvc05fb2cd9b1")))

(define-public crate-rule-0.1.5 (c (n "rule") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "000ylmynaa4x7a7kxhag3m1f2xbv73ndx5pkns13mmiq3pxyxirg")))

(define-public crate-rule-0.1.6 (c (n "rule") (v "0.1.6") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18cv0a76xsf02ndxaslq5b33wn3b777j1jfwqklggvnw9f7811bf")))

