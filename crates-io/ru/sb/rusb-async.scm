(define-module (crates-io ru sb rusb-async) #:use-module (crates-io))

(define-public crate-rusb-async-0.0.1-alpha (c (n "rusb-async") (v "0.0.1-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)))) (h "0f77g7998ivd0b2zcxh6lvijfbg3yrvz5s2bkp9nafld1c1lsr3y") (f (quote (("vendored" "rusb/vendored"))))))

