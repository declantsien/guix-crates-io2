(define-module (crates-io ru sb rusb) #:use-module (crates-io))

(define-public crate-rusb-0.4.0 (c (n "rusb") (v "0.4.0") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "0ilns0yjfsrmrgafxl56crdnh694jvwzb7mif06y492ka3rj6q17")))

(define-public crate-rusb-0.5.0 (c (n "rusb") (v "0.5.0") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "036hiasq77khwys0xxf9bakk0hmfrnnyj7xr01vdjswsh61l6h7z")))

(define-public crate-rusb-0.5.1 (c (n "rusb") (v "0.5.1") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "09xx0z6jwgpj8yniy79l8wryyfncfiz0anx18hl6237pap4lk5iv")))

(define-public crate-rusb-0.5.2 (c (n "rusb") (v "0.5.2") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "17j51nb377kw30xsn8y7i472lhjgbgj1c27qx0yadjs6jvvgvvi7")))

(define-public crate-rusb-0.5.3 (c (n "rusb") (v "0.5.3") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "1m8pnmzsn5agnqz0bs0k7qash1rhgskrmr94m8z3r8n2dfqf9i6m")))

(define-public crate-rusb-0.5.4 (c (n "rusb") (v "0.5.4") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "09ja18an4wwajhrpsqsngjnc91h5q7k7d09839s7vxy8vmb7h74w")))

(define-public crate-rusb-0.5.5 (c (n "rusb") (v "0.5.5") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0ah68hnr9v74dcwa3wgjp0a0d8af639idgvrlrwiibf7bwzal36i") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.6.0 (c (n "rusb") (v "0.6.0") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0jm61sxyi3dds5sx3hwjcx86b65svcfajgi4qnk5rnvcqvi9wjl8") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.6.2 (c (n "rusb") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1czks6jz6s7hwz7zwrj2x8676jwya3c9zh3yss2mh7z30xn5gizs") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.6.3 (c (n "rusb") (v "0.6.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "035ai1mcg2425ygqg60h5r1rrs0wj52mg2zha97y7mqiz22ajyrj") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.6.4 (c (n "rusb") (v "0.6.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0jpqikbv5f0zc768fn9ygs7c314h2bncadzjkv57ymzfd1rh7yk7") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.6.5 (c (n "rusb") (v "0.6.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1ipxssg1lgfdkjm4121c1rh1a9qkaxbpnm6lijglynb2b7a2rwzn") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.7.0 (c (n "rusb") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1haql9pvg3aph20k4dlsndh24x5wa13ami79hmphnwg4qryxqw64") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.8.0 (c (n "rusb") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0dxl1ck1k86kl446r71lqff2v6zz42s1l0acgr85flh9b542dwqj") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.8.1 (c (n "rusb") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1b80icrc7amkg1mz1cwi4hprslfcw1g3w2vm3ixyfnyc5130i9fr") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.9.0 (c (n "rusb") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "15078d0zi4pmighnng0xl8c56cim40xw2bh45bwpl055k8hm9d43") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.9.1 (c (n "rusb") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1q6fypmxh79nh49qd1k4lizcz3gwd0pb2djizcs9nn0wq8ss0fkh") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.9.2 (c (n "rusb") (v "0.9.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "usb-ids") (r "^1.2023.0") (d #t) (k 2)))) (h "1lf5hpvka5rr19bpww3mk8gi75xkr54gl79cf6za7cgr2ilw7a24") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.9.3 (c (n "rusb") (v "0.9.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "usb-ids") (r "^1.2023.0") (d #t) (k 2)))) (h "1gwpm8wmw4pbkga694n0vbkfh7m14p3b5dyb4pl2agq3nr4z3zs5") (f (quote (("vendored" "libusb1-sys/vendored"))))))

(define-public crate-rusb-0.9.4 (c (n "rusb") (v "0.9.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "usb-ids") (r "^1.2023.0") (d #t) (k 2)))) (h "1905rijhabvylblh24379229hjmkfhxr80jc79aqd9v3bgq9z7xb") (f (quote (("vendored" "libusb1-sys/vendored"))))))

