(define-module (crates-io ru sb rusb2snes) #:use-module (crates-io))

(define-public crate-rusb2snes-0.1.0 (c (n "rusb2snes") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19") (d #t) (k 0)))) (h "1w123pg4a8widvvv4nj93k0nr3y6463vfd6agf6v9j8jrjdmf3bp")))

(define-public crate-rusb2snes-0.1.1 (c (n "rusb2snes") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19") (d #t) (k 0)))) (h "09j8bx12zm70xvcl9g1kc115lf9w8lb4amjkri2xxh0vssa8b6lj")))

(define-public crate-rusb2snes-0.1.2 (c (n "rusb2snes") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19") (d #t) (k 0)))) (h "1wfkw8y3v0hccgc8v8xpqplk9p09ndr34mzvnfdyj6anvb78m749")))

(define-public crate-rusb2snes-0.1.3 (c (n "rusb2snes") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19") (d #t) (k 0)))) (h "06rp3nf6nwlnywffsk1ky42a5j5rc1nvwyf4b9f8d3rsj2gjwn99")))

(define-public crate-rusb2snes-0.1.4 (c (n "rusb2snes") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "tungstenite") (r "^0.20.0") (d #t) (k 0)))) (h "1qjx1g5g0kgsh74kqiqqalkplr7isf4dcgpnkkpzywway9fvlhkv")))

