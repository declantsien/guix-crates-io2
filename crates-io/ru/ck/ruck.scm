(define-module (crates-io ru ck ruck) #:use-module (crates-io))

(define-public crate-ruck-0.1.0 (c (n "ruck") (v "0.1.0") (h "02va33zvnlfmln4b2j6qx0y6np5q8xj9bwx7r94i57gd1nwn4q43")))

(define-public crate-ruck-0.2.0 (c (n "ruck") (v "0.2.0") (h "1ka945qacnsgdxsz5vkiwd0wd5r18rpwl568kb9prqn56h7kca5y")))

