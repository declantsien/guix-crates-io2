(define-module (crates-io ru ck ruckus-2d) #:use-module (crates-io))

(define-public crate-ruckus-2d-0.1.0 (c (n "ruckus-2d") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "02vr5sv481vkd05d26nxsgjdnsjysqs1khjjpgbzrymiwlq4qi3r")))

(define-public crate-ruckus-2d-0.1.1 (c (n "ruckus-2d") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "05bc809q41zsykp7pxrfyxn0lr1aqsf77mard8gbvaxhiy6h9hgn")))

