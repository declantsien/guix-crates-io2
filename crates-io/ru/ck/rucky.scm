(define-module (crates-io ru ck rucky) #:use-module (crates-io))

(define-public crate-rucky-0.1.0 (c (n "rucky") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.19") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.2.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "toml") (r "^0.2") (d #t) (k 2)))) (h "0js6ibi12p66dd42w0dk0f74bapmab0k3nzgp9rxnxd872qml1y7") (f (quote (("default"))))))

