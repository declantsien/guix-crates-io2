(define-module (crates-io ru tc rutcl) #:use-module (crates-io))

(define-public crate-rutcl-0.1.0 (c (n "rutcl") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "175z001hy0j0g746xcdq7703hr0rchjzn2c01m28iyc43x5gyh38")))

(define-public crate-rutcl-0.1.1 (c (n "rutcl") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0l24f9aih6cj031l1bmic7vs65rdf14irrnfsla6sggpn9z59i2j")))

(define-public crate-rutcl-0.1.2 (c (n "rutcl") (v "0.1.2") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0cmvb5i3x110isbw9ppanq0qxfd80ssz009iswn1ignp004jg747")))

(define-public crate-rutcl-0.1.3 (c (n "rutcl") (v "0.1.3") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "12fr5knx7mv7msh03y91fp2ab4sicvf846d1pbvnfvr3mj5wwf35")))

(define-public crate-rutcl-0.1.4 (c (n "rutcl") (v "0.1.4") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1lxx53l4775114qgyjpajnfnqy8rykxcvcs8l55yb1gqn447rsbd")))

(define-public crate-rutcl-0.1.5 (c (n "rutcl") (v "0.1.5") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1qyv610qvqxpyvs9ahlcghkjd4lip94nkxg5cr1pfy2bh0lj0rgk")))

(define-public crate-rutcl-0.1.6 (c (n "rutcl") (v "0.1.6") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0awy9y1iq4bngsfzrsz5j3vlcpnf5717075q7ls9bnpsqn22fssw")))

(define-public crate-rutcl-0.1.7 (c (n "rutcl") (v "0.1.7") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.176") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "00z102bikdphxsfkv0wai610ix4g37i923fcyl7cj27500cyqp8y") (s 2) (e (quote (("serde" "dep:serde"))))))

