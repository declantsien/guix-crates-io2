(define-module (crates-io ru mo rumos) #:use-module (crates-io))

(define-public crate-rumos-0.0.5 (c (n "rumos") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "brightness") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 0)))) (h "0hb2sqi1408jf9l715zjbrcm9ng5ghy5ga12xz2r0pdxjz0cwmrm")))

(define-public crate-rumos-0.0.6 (c (n "rumos") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 0)) (d (n "brightness") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 0)))) (h "1zs37fvsng1d0gaq3av7qal2gy2nc2rsmgyqkjdn91265qqv48sx")))

