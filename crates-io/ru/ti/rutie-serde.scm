(define-module (crates-io ru ti rutie-serde) #:use-module (crates-io))

(define-public crate-rutie-serde-0.1.0 (c (n "rutie-serde") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rutie") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b56qig01nscklqwrgdy8gazb2i5dgn46gplkivcqq1fgxxnmb03")))

(define-public crate-rutie-serde-0.1.1 (c (n "rutie-serde") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rutie") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09sbr7f3jb2mf39fr0f14wlp0dkwzbhd5bhdhq93ws0707m8lgfl")))

(define-public crate-rutie-serde-0.2.0 (c (n "rutie-serde") (v "0.2.0") (d (list (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rutie") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)))) (h "02g0siprw0sb0nk2jcxqiw4h3g4gzvyi1k5c4spyxclj00aa7z3m")))

(define-public crate-rutie-serde-0.3.0 (c (n "rutie-serde") (v "0.3.0") (d (list (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rutie") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)))) (h "09g462maksj6bycp0bin95v1fzbz0rf5bjbksxjqcwrijj970g36")))

