(define-module (crates-io ru ti rutile) #:use-module (crates-io))

(define-public crate-rutile-0.1.0 (c (n "rutile") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "17qk1y09iakf0sl3yh6rk917is06ral9ljh2gvklrbgypdmnhkm5")))

(define-public crate-rutile-0.1.1 (c (n "rutile") (v "0.1.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "13w7ai7kvd98rxrf1akqpahf7wnwszln9dk3dvvkhms90nn19q95")))

