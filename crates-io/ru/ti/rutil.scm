(define-module (crates-io ru ti rutil) #:use-module (crates-io))

(define-public crate-rutil-0.1.0 (c (n "rutil") (v "0.1.0") (h "05ws8bhpisahrziamsn55aby423xpc35hhbd5x3b64w6yr91nimz")))

(define-public crate-rutil-0.2.0 (c (n "rutil") (v "0.2.0") (h "19n8m17k76j9d234wnfkjjkvx3lv417jv5qmg8fz4n4xn8blaizq")))

