(define-module (crates-io ru ti rutilities) #:use-module (crates-io))

(define-public crate-rutilities-0.1.0 (c (n "rutilities") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p8jd1ksf7lxzk1xlprk3n734mmv23n1cysmij5xzlhs5nimzjn4") (y #t)))

(define-public crate-rutilities-0.2.0 (c (n "rutilities") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1cha7mshk7waavamz5ygvyc2agxbmc7z1k3lli9jljibx48577wa") (y #t)))

(define-public crate-rutilities-0.2.2 (c (n "rutilities") (v "0.2.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1ffwp82n2d7z4ir8lmw2mmn4b5aldrd02c45hf36ijqlih6cibiy") (y #t)))

(define-public crate-rutilities-0.2.3 (c (n "rutilities") (v "0.2.3") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0aqsy7gw3d4dl4nikksv0ix467ncvnhdj3dnbph9sf6j9cfnqn8w") (y #t)))

(define-public crate-rutilities-0.2.4 (c (n "rutilities") (v "0.2.4") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1ghis7n2d88a2lvfhpa1dzl23fgsvadw03mybh71d278k9wjvixx") (y #t)))

(define-public crate-rutilities-0.2.5 (c (n "rutilities") (v "0.2.5") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0gnhkwrnqrxgajhpv1yf5rhh3lw9jbya0vxcfrrw1wy8ih0irk6n")))

