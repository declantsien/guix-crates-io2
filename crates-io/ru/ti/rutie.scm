(define-module (crates-io ru ti rutie) #:use-module (crates-io))

(define-public crate-rutie-0.0.1 (c (n "rutie") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "188rfixfp67z3yq2c4wwq9b825196v3yl8d9bf240izxdw02npz9") (f (quote (("unstable") ("test") ("default")))) (l "ruby")))

(define-public crate-rutie-0.0.2 (c (n "rutie") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "190gdascbkqm48jjranbwkx1acmr026ncql0j9bzpbb89dj5pjjc") (f (quote (("unstable") ("test") ("default")))) (l "ruby")))

(define-public crate-rutie-0.0.3 (c (n "rutie") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0ng922f24s7jlflr6vwbvzcldnfz1p97m0k3b3s4j5parmaznyfp") (f (quote (("unstable") ("test") ("default")))) (l "ruby")))

(define-public crate-rutie-0.1.0 (c (n "rutie") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0vziv4a3ygv2882n3r6nwkgd079d1z9d4m7g11jgzl6zzh1mlsqn") (f (quote (("unstable") ("test") ("default")))) (l "ruby")))

(define-public crate-rutie-0.1.1 (c (n "rutie") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "00rdgfk0z4qssfp12sdf8f3x0i3cabn6b35alpz0yy785r13gf2a") (f (quote (("unstable") ("test") ("default")))) (y #t) (l "ruby")))

(define-public crate-rutie-0.1.2 (c (n "rutie") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "0796cgrcsw73fsb1d47nb74k801vk7i5idplpv0yd4nxk62nx1lg") (l "ruby")))

(define-public crate-rutie-0.1.3 (c (n "rutie") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "01anyazz0v7fwfwavpwlwbaiicwfl3acs4k3qpgbypxfl2c0zpls") (l "ruby")))

(define-public crate-rutie-0.1.4 (c (n "rutie") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "1gz64zr29mq1j8321fm08qvcj6q645yzxgq6cg7015z9hbxpcjbm") (l "ruby")))

(define-public crate-rutie-0.2.0 (c (n "rutie") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "0x6pvpfzvl0x7d7li1ffbxmkgvbfz1kpsw40yks1b1ab24q54qas") (l "ruby")))

(define-public crate-rutie-0.2.1 (c (n "rutie") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "1qakyc93hbnzkppnjkrvb5m1nbfhi38h113gaqxkxvd1iprgc420") (l "ruby")))

(define-public crate-rutie-0.2.2 (c (n "rutie") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "1s24ghhpsz6sq1crzswy5zlrswam6i39a2cqj8h1q7wz47cbfrri") (l "ruby")))

(define-public crate-rutie-0.3.0 (c (n "rutie") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "0sr23y4k36vij3z4k35k7nrpl07q9grcgrzrkww512sa4hl2azyv") (l "ruby")))

(define-public crate-rutie-0.3.1 (c (n "rutie") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "0q1iii39f0j3r6dwaqwg743ag7jiwz12bzaqfbncsq3wv5i1npdd")))

(define-public crate-rutie-0.3.2 (c (n "rutie") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "1f3q0wmaacw5p3c2824kxg2llwpb00bnz3ril5jrdx7sd05fkr3k") (l "ruby")))

(define-public crate-rutie-0.3.3 (c (n "rutie") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "0b19c9v5ynm4qnckj13rklvxjh79m0iqswqq38ccwpq5lxbjxk18") (l "ruby")))

(define-public crate-rutie-0.3.4 (c (n "rutie") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "1fva7myf270fk2hzr84x7xi84javrdp4v54saggqrc1pfc1yshmk") (l "ruby")))

(define-public crate-rutie-0.4.0 (c (n "rutie") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "0iq4sf8cail8n5fs3vzp30rxx82r9j51ys2i39hr41bmkhy7g9vl") (l "ruby")))

(define-public crate-rutie-0.4.1 (c (n "rutie") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "08cdm90dcqj14pbhv1wbas43dxxl30n5zv7gyhqlb6m8hxkk1l56") (l "ruby")))

(define-public crate-rutie-0.4.2 (c (n "rutie") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "04m15brbwzryqsgd1nb6lbrsa3iyfv217fnr31fbbifcyvljgapj") (l "ruby")))

(define-public crate-rutie-0.4.3 (c (n "rutie") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "1l7wvw791qrb2lygg19ip1n745zqla9vh2fbgawfbgx3dkvk3kk4") (l "ruby")))

(define-public crate-rutie-0.5.0 (c (n "rutie") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "17wiwr7z5646y08f5rb7ibjkxm9813acn9fjn31s2nar6iz5m71q") (l "ruby")))

(define-public crate-rutie-0.5.1 (c (n "rutie") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "1dg31rv4sk3di2f6g2xcp4lqhix6cdk4zl2v18vbfr5pwiqsvg16") (l "ruby")))

(define-public crate-rutie-0.5.2 (c (n "rutie") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.10") (d #t) (k 1)))) (h "0bzpz3qrdq3gzsc2r8qarl04g3a8lbvhdidcmg2mxm7dsy5c8m1g") (l "ruby")))

(define-public crate-rutie-0.5.3 (c (n "rutie") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "02mwia4xfpvxsjni7j000snakw03ciq33bvwwbfcq6xa36n7z22g") (l "ruby")))

(define-public crate-rutie-0.5.4 (c (n "rutie") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0bnilc88yq9qa6k09nqhar8sgq4kf81jjzrw6gmr57sfa7n6vqbi") (l "ruby")))

(define-public crate-rutie-0.5.5 (c (n "rutie") (v "0.5.5") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1a5kjxcwvs46rfksqzm430rapmjqqgv3cfy7lv5gksflllhxgh05") (l "ruby")))

(define-public crate-rutie-0.6.0-rc.1 (c (n "rutie") (v "0.6.0-rc.1") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1if98hxrsdj1rw4mkzlsagnq1cpl16k8dj92m6w5s8f82761fcbf") (l "ruby")))

(define-public crate-rutie-0.5.6 (c (n "rutie") (v "0.5.6") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1102j1cq3wvsh1wbfcpa1lj70q6j7s1ns1zmyvqd41a7sw6211jk") (l "ruby")))

(define-public crate-rutie-0.6.0-rc.2 (c (n "rutie") (v "0.6.0-rc.2") (d (list (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1q6svwmp05wbyy80vfs8ziyls1l4bp01w9gxhxy92ingx8mqn7s7") (l "ruby")))

(define-public crate-rutie-0.6.0 (c (n "rutie") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "0d7a6b5h6kdrkshhgrh3nbl6g4sfd8xnpgjp7mrdaq1fq8sbapk9") (l "ruby")))

(define-public crate-rutie-0.6.1 (c (n "rutie") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "0l3w06vdp2yiy18rfxz51ff5sg5jiy5dsms6ksykybnq0hvdzm4f") (l "ruby")))

(define-public crate-rutie-0.7.0 (c (n "rutie") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "1cg5p2jib7vbpkwfqcp3qbc01agcgrv3kn7z4k2nd7k70i2ig94r") (l "ruby")))

(define-public crate-rutie-0.8.0 (c (n "rutie") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "0pzjn9k6dds9f7b95jkja58x7f0qb7j8dcbynzfvf0h45vzky79k") (f (quote (("no-link")))) (l "ruby")))

(define-public crate-rutie-0.8.1 (c (n "rutie") (v "0.8.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "154pj8lvqw8rpfgrgzvxzg3ff9j8ywpwz21iw1dg6hz18268k0kf") (f (quote (("no-link")))) (l "ruby")))

(define-public crate-rutie-0.8.2 (c (n "rutie") (v "0.8.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "1362nz5ac464hc1fp5l8ngxql9yi0dki9h3rg82ika1cfknig0k7") (f (quote (("no-link")))) (l "ruby")))

(define-public crate-rutie-0.8.3 (c (n "rutie") (v "0.8.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.86") (d #t) (k 0)))) (h "0ii3wl95z4yk81pfc5c9rqsm39ir5a2fm385x01652y7vsid508d") (f (quote (("no-link")))) (l "ruby")))

(define-public crate-rutie-0.8.4 (c (n "rutie") (v "0.8.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "1b5jxgm2j7wf8jmn0yyq5fs0gp5bxfkck363cj1v8fcppd6dp5sx") (f (quote (("no-link")))) (l "ruby")))

(define-public crate-rutie-0.9.0 (c (n "rutie") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)))) (h "12k9gfdbq7rpigwwm895a9994iwdgfwgmbc06jg60c0c93vf9s75") (f (quote (("no-link")))) (l "ruby")))

