(define-module (crates-io ru re rurel) #:use-module (crates-io))

(define-public crate-rurel-0.1.0 (c (n "rurel") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ik0854141ykhdvqarlpyfkdwb7saymxwp9khn653aagl0vwvagh")))

(define-public crate-rurel-0.1.1 (c (n "rurel") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16vly8xgsmdc91jpnwsy56plaxwl2zbfpk0hny6qdpifqkh895xg")))

(define-public crate-rurel-0.1.2 (c (n "rurel") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0smmwzhcdpd8w9mqc4liha3nh5hxsbagz14xgbd2bk3kbnfz5gwv")))

(define-public crate-rurel-0.2.0 (c (n "rurel") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1h0shg5sbzaw3mz0mhc3gn9m2w5nqlknkxsvq611zkmf5m3h4gp1")))

(define-public crate-rurel-0.2.1 (c (n "rurel") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0zsmpvl2xmm8s697pdi4v60mfaigsg1kch4kywgydklzm9p4q7z6")))

(define-public crate-rurel-0.3.0 (c (n "rurel") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1x6ckx0yvmdn9sm8nc8jdcm1mrvn29yv2aijk0inicnnm1i5x6c7")))

(define-public crate-rurel-0.3.1 (c (n "rurel") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "01b7cl5b5ilb1snzfp0w894yrmq27llii57mla86cnhah1lyjkj4")))

(define-public crate-rurel-0.4.0 (c (n "rurel") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "120y4b7rh5f97w0nkw1fxas38935ggnpcnpcgyrnwr3mxfqd94fr")))

(define-public crate-rurel-0.5.0 (c (n "rurel") (v "0.5.0") (d (list (d (n "dfdx") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0s7ky8r4yb5wwzx7ylinfqpwxq0n87qj63ffpx7b63vmnryzk1my") (f (quote (("dqn" "dfdx") ("default"))))))

(define-public crate-rurel-0.5.1 (c (n "rurel") (v "0.5.1") (d (list (d (n "dfdx") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "176nxhpgvy7c80pb9mxv80vwrfmryq178b905zd8z4sfb7g68lhd") (f (quote (("dqn" "dfdx") ("default"))))))

