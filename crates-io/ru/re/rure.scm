(define-module (crates-io ru re rure) #:use-module (crates-io))

(define-public crate-rure-0.1.0 (c (n "rure") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "07bllrdp71aplylgpdmk0pbm2rrwpjmqawnkl83y3aajj1qzn912")))

(define-public crate-rure-0.1.1 (c (n "rure") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "0gcjl2dv6d642z09c5ldn24v1z5n9321cg5ax1iwkp8mvvlvg1bd")))

(define-public crate-rure-0.2.0 (c (n "rure") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)))) (h "1x9dflxx7mnx97vscn0yb5rpbrfhy84y4rjnfv9xaj2nfvby97x7")))

(define-public crate-rure-0.2.1 (c (n "rure") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0m768pkh8dy305h287k1qag6xk16wd5wbklq44q10krxc413n2bb")))

(define-public crate-rure-0.2.2 (c (n "rure") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0k529aly7knhfkxrvmiil0s47l2hzfniz2ipv88fxfkmbrchkppk")))

