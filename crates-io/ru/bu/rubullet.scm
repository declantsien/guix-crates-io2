(define-module (crates-io ru bu rubullet) #:use-module (crates-io))

(define-public crate-rubullet-0.1.0-alpha (c (n "rubullet") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rubullet-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1fxn2q1vigycwsx09pfx978knzw7wbayabi4ymk1xd5xabcjigmw")))

(define-public crate-rubullet-0.1.0-alpha-2 (c (n "rubullet") (v "0.1.0-alpha-2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rubullet-sys") (r "^0.1.0-alpha-2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1k36wrlw9r15dl6qzbzh3ym2ram8k2xambb27p0p7p27lm4g0kpm")))

(define-public crate-rubullet-0.1.0-alpha-3 (c (n "rubullet") (v "0.1.0-alpha-3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rubullet-sys") (r "^0.1.0-alpha-2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1mh04515x6hjkxb6a6w89wrm1ny6vvwzx5q93lrkv6h4niz77q7i")))

