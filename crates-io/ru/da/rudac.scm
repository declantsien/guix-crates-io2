(define-module (crates-io ru da rudac) #:use-module (crates-io))

(define-public crate-rudac-0.1.0 (c (n "rudac") (v "0.1.0") (h "0068d963cmv31fahdwbv99w8677j80idcmsy8ah8cil4lkmqbah4")))

(define-public crate-rudac-0.1.1 (c (n "rudac") (v "0.1.1") (h "0k7j79yyb46z1whz169vlcy1dqdf7bzh064f33mli07jc81xmy46")))

(define-public crate-rudac-0.2.0 (c (n "rudac") (v "0.2.0") (h "1bwlq5llp982v6nd9smghiz64wvr76xnjp9fvvar11mh6kb9m6zk")))

(define-public crate-rudac-0.2.1 (c (n "rudac") (v "0.2.1") (h "02srpyjycgigs0hgxiyr6s1bbh22narhzvjl1glarf74mk1yzwh5")))

(define-public crate-rudac-0.3.0 (c (n "rudac") (v "0.3.0") (h "0cjbw8gb9af8k32p21szk4a02wx52vfcnhs6hb82a6z2bdvrvybw")))

(define-public crate-rudac-0.4.0 (c (n "rudac") (v "0.4.0") (h "090j7mnags8i5mi68g3w2gs3yrm09yv2fjmf19ri5w9gv82ipf52")))

(define-public crate-rudac-0.4.1 (c (n "rudac") (v "0.4.1") (h "0i3xq4c6f13m4853i97rlmcakh70wyph0zrmap6ld847zyxgjgqz")))

(define-public crate-rudac-0.5.0 (c (n "rudac") (v "0.5.0") (h "161s2gxh0sm7ab68p968i0skw6l0pklqbh02krxa9zqx1ica9fp3")))

(define-public crate-rudac-0.6.0 (c (n "rudac") (v "0.6.0") (h "0y5wpcsqmj9l3zgrash98clydd9cdllh8zi37d61f89nsm2zkb7q")))

(define-public crate-rudac-0.6.1 (c (n "rudac") (v "0.6.1") (h "1q58zdglcfxjbg07wnb2srkifijcyz8sailx4awg7f8grk85ajf2")))

(define-public crate-rudac-0.7.0 (c (n "rudac") (v "0.7.0") (h "1ci5j85l255ldzyrxk45ldgn6ymvqigpi0ilkxdp3aq1a7g8nzzn")))

(define-public crate-rudac-0.8.0 (c (n "rudac") (v "0.8.0") (h "0m5rz99gfcrv2jr3ianyb4dfgkbhly1wi3xr7b093pjszwscphs5")))

(define-public crate-rudac-0.8.1 (c (n "rudac") (v "0.8.1") (h "1m66izsj9ps3fsql0668mzhc02rvfa9xns1vqwlw5pwlyffpqndp")))

(define-public crate-rudac-0.8.2 (c (n "rudac") (v "0.8.2") (h "0hir0ih39q7cbpf7pqwry4in1nswd929vwshqws1dbaiiby6g3ig")))

(define-public crate-rudac-0.8.3 (c (n "rudac") (v "0.8.3") (h "0r7gnvdy4ssx58j9s2ccck9xrgwzd6pfai0ll7v1jzpywhdzxjr4")))

