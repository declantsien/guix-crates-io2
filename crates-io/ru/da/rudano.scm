(define-module (crates-io ru da rudano) #:use-module (crates-io))

(define-public crate-rudano-0.1.0 (c (n "rudano") (v "0.1.0") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0z1gxwmkc9rdxr9lifjdpb2mr5isnlmjns1km1yrzh93cdwzdl7k")))

(define-public crate-rudano-0.1.1 (c (n "rudano") (v "0.1.1") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qw31myx43gii742qb36647bhp6n1qq8p3rprfbv35kfn94lgv29")))

