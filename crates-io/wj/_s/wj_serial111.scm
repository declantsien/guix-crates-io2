(define-module (crates-io wj _s wj_serial111) #:use-module (crates-io))

(define-public crate-wj_serial111-0.1.0 (c (n "wj_serial111") (v "0.1.0") (h "1ibxh7kpfi0v7v3sp7hqb8b2rq8lqdzzlrawp8zg4aq4f0kq96vi") (y #t)))

(define-public crate-wj_serial111-0.1.1 (c (n "wj_serial111") (v "0.1.1") (h "0jscs80zfphp42vl55nrn244dmvlsa54m5jg6087wjrrws0wcd0a") (y #t)))

