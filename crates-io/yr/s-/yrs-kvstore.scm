(define-module (crates-io yr s- yrs-kvstore) #:use-module (crates-io))

(define-public crate-yrs-kvstore-0.1.0 (c (n "yrs-kvstore") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lib0") (r ">=0.16") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (f (quote ("write" "union" "const_generics" "const_new"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yrs") (r ">=0.16") (d #t) (k 0)))) (h "1hqzvvq1nk92l77s5cgkgzrfyv54crlgakq43lglrbp41nx7m1my")))

(define-public crate-yrs-kvstore-0.2.0 (c (n "yrs-kvstore") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (f (quote ("write" "union" "const_generics" "const_new"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yrs") (r "^0.17") (d #t) (k 0)))) (h "0mikpfldmzj52896a40cx14hp4qx0pngwkqlyaxj2j5k24r1imqf")))

