(define-module (crates-io yr s- yrs-rocksdb) #:use-module (crates-io))

(define-public crate-yrs-rocksdb-0.1.0 (c (n "yrs-rocksdb") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lib0") (r ">=0.16") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rocksdb") (r ">=0.19") (d #t) (k 0)) (d (n "yrs") (r ">=0.16") (d #t) (k 2)) (d (n "yrs-kvstore") (r "^0.1") (d #t) (k 0)))) (h "0xl7j70q8zdnxlxqhpf5krzrg2a173f1vggr5k0xsj6xx0ramwig")))

(define-public crate-yrs-rocksdb-0.2.0 (c (n "yrs-rocksdb") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rocksdb") (r "^0.21") (d #t) (k 0)) (d (n "yrs") (r "^0.17") (d #t) (k 2)) (d (n "yrs-kvstore") (r "^0.2") (d #t) (k 0)))) (h "0yvifk4shf92pmmnabmykvc4hvjjdpma83rzg6jc1fj1j0j3008g")))

