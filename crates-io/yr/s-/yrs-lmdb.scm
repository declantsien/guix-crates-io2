(define-module (crates-io yr s- yrs-lmdb) #:use-module (crates-io))

(define-public crate-yrs-lmdb-0.1.0 (c (n "yrs-lmdb") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "lib0") (r ">=0.16") (d #t) (k 2)) (d (n "lmdb-rs") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "yrs") (r ">=0.16") (d #t) (k 2)) (d (n "yrs-kvstore") (r "^0.1") (d #t) (k 0)))) (h "047xvw4gbr84ryn5y89a4n0dj9kqyw65jjsdwd04j6kkdpb2wyci")))

(define-public crate-yrs-lmdb-0.2.0 (c (n "yrs-lmdb") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "lmdb-rs") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "yrs") (r "^0.17") (d #t) (k 2)) (d (n "yrs-kvstore") (r "^0.2") (d #t) (k 0)))) (h "0y4aa7kahflkr0n7r08jpgzffvhs3ydg0p2yc62q90canhpfbcs9")))

