(define-module (crates-io xw eb xwebtransport-core) #:use-module (crates-io))

(define-public crate-xwebtransport-core-0.1.0 (c (n "xwebtransport-core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)))) (h "0sjbnww4i6sbrsddi4adp12ixfnrx71r59ll0d6nh5d3188vfn5l")))

(define-public crate-xwebtransport-core-0.2.0 (c (n "xwebtransport-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)))) (h "0s62ikzphw1971v44hkz69cspyyjzljy2hb6zm8ii7yiknm2piqx")))

