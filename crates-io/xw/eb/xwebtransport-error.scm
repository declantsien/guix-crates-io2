(define-module (crates-io xw eb xwebtransport-error) #:use-module (crates-io))

(define-public crate-xwebtransport-error-0.1.0 (c (n "xwebtransport-error") (v "0.1.0") (d (list (d (n "xwebtransport-core") (r "^0.1") (k 0)))) (h "0r5w8vbscr8jsmjbzq5k6dkwfvv83w0vy1drpaahxrvv0d70kqg9")))

(define-public crate-xwebtransport-error-0.2.0 (c (n "xwebtransport-error") (v "0.2.0") (d (list (d (n "xwebtransport-core") (r "^0.2") (k 0)))) (h "156cfr72sp29qq9yp7nxcr412bvngh0y367g1gcr5mz3pdszwznq")))

