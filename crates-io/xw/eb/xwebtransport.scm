(define-module (crates-io xw eb xwebtransport) #:use-module (crates-io))

(define-public crate-xwebtransport-0.1.0 (c (n "xwebtransport") (v "0.1.0") (d (list (d (n "xwebtransport-web-sys") (r "^0.1") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwebtransport-wtransport") (r "^0.1") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0fq2d8sr0xvszp01kkdrd62ns5g8cannbh5yx1ypr0ijaf93mprz")))

(define-public crate-xwebtransport-0.2.0 (c (n "xwebtransport") (v "0.2.0") (d (list (d (n "xwebtransport-web-sys") (r "^0.2") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwebtransport-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "09pd264imcgdnic367h2n9m3vccqwlwp8kp6an79wap9imlgzm3f")))

(define-public crate-xwebtransport-0.3.0 (c (n "xwebtransport") (v "0.3.0") (d (list (d (n "xwebtransport-web-sys") (r "^0.3") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwebtransport-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0aqzs4bv14cpg1pxr0m0psxp13k7wk17mlsi8p2bb3vz6yrjx5wd")))

