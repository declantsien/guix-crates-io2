(define-module (crates-io xw eb xwebtransport-tests) #:use-module (crates-io))

(define-public crate-xwebtransport-tests-0.1.0 (c (n "xwebtransport-tests") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "xwebtransport-core") (r "^0.1") (k 0)) (d (n "xwebtransport-error") (r "^0.1") (k 0)))) (h "073rpssvg81k4glq62qrhwaks9kfzimr21wvb2mcwxysadl2qkfk")))

(define-public crate-xwebtransport-tests-0.2.0 (c (n "xwebtransport-tests") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "xwebtransport-core") (r "^0.2") (k 0)) (d (n "xwebtransport-error") (r "^0.2") (k 0)))) (h "027ywx1kmfzw8ga36ikvy5f2nk2zfwsvzkyx6gavkwxnz18xqvbz")))

