(define-module (crates-io xw as xwasmi-validation) #:use-module (crates-io))

(define-public crate-xwasmi-validation-0.1.0 (c (n "xwasmi-validation") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "xwasm") (r "^0.31") (k 0)))) (h "0bik434zam78r5c0n7zxrc3wvy9vbzvaikhhnl7ralk41ygj69p2") (f (quote (("std" "xwasm/std") ("default" "std") ("core" "hashbrown/nightly"))))))

