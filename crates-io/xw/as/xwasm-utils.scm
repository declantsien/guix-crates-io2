(define-module (crates-io xw as xwasm-utils) #:use-module (crates-io))

(define-public crate-xwasm-utils-0.6.1 (c (n "xwasm-utils") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "wabt") (r "^0.2") (d #t) (k 2)) (d (n "xwasm") (r "^0.31") (k 0)))) (h "0cy147wgkfnmhfsx4dd1l4cazdmhnw29ks9i8g95cl18sglzpnhw") (f (quote (("std" "xwasm/std" "log/std" "byteorder/std") ("default" "std"))))))

(define-public crate-xwasm-utils-0.6.2 (c (n "xwasm-utils") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "wabt") (r "^0.2") (d #t) (k 2)) (d (n "xwasm") (r "^0.31") (k 0)))) (h "10bby5651snvagw17rwjmy3pzjq8nrqx7yzflycy2sgk601g0dpj") (f (quote (("std" "xwasm/std" "log/std" "byteorder/std") ("default" "std"))))))

