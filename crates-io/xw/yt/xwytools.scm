(define-module (crates-io xw yt xwytools) #:use-module (crates-io))

(define-public crate-xwytools-0.1.0 (c (n "xwytools") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4") (d #t) (k 0)))) (h "0i5vbgbww8nsss6xzshcxd04hm8nm86xx0hf24nync3i59a7infc")))

