(define-module (crates-io xw in xwing-kem) #:use-module (crates-io))

(define-public crate-xwing-kem-0.1.0 (c (n "xwing-kem") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "pqcrypto-kyber") (r "^0.8.0") (d #t) (k 0)) (d (n "pqcrypto-traits") (r "^0.3.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2") (f (quote ("getrandom" "static_secrets"))) (d #t) (k 0)))) (h "1ldrl2mj74d9f29ackhpa7ykkhp4yp48zlikjfk3f5c51dbplr6a")))

