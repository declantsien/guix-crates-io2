(define-module (crates-io xw t- xwt-anchor) #:use-module (crates-io))

(define-public crate-xwt-anchor-0.1.0 (c (n "xwt-anchor") (v "0.1.0") (d (list (d (n "xwt-core") (r "^0.3") (k 0)))) (h "124fds8wmrh4q0zzin9bjrjg6rkaph19g98cgfa767x1dk7xdb2z")))

(define-public crate-xwt-anchor-0.2.0 (c (n "xwt-anchor") (v "0.2.0") (d (list (d (n "xwt-core") (r "^0.4") (k 0)))) (h "0sg0yb0xm7rhhc26y06jkx2jm499m801qms7ncfzyglzd6gpsch0")))

