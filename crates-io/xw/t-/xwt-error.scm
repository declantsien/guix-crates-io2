(define-module (crates-io xw t- xwt-error) #:use-module (crates-io))

(define-public crate-xwt-error-0.2.0 (c (n "xwt-error") (v "0.2.0") (d (list (d (n "xwt-core") (r "^0.2") (k 0)))) (h "0qs53qissad70sz1pw9pb1ks271yf9nhxjpr0wnswl11znrv06p1")))

(define-public crate-xwt-error-0.2.1 (c (n "xwt-error") (v "0.2.1") (d (list (d (n "xwt-core") (r "^0.2") (k 0)))) (h "08w2357wclr3gbp1mz6qwnfdvsa2xd1vpnbk3qdyymbq34q13447")))

(define-public crate-xwt-error-0.3.0 (c (n "xwt-error") (v "0.3.0") (d (list (d (n "xwt-core") (r "^0.3") (k 0)))) (h "09p7gnvdiagmmphkxz6chhsw91mf6fzy51zgc10lzqln41m4sl1k")))

(define-public crate-xwt-error-0.3.1 (c (n "xwt-error") (v "0.3.1") (d (list (d (n "xwt-core") (r "^0.3.1") (f (quote ("std"))) (k 0)))) (h "0amkav2d9p4qzisdkrn1ydnrszy4wsy0wdx0z2pdsxmzhmic0bwg")))

(define-public crate-xwt-error-0.4.0 (c (n "xwt-error") (v "0.4.0") (d (list (d (n "xwt-core") (r "^0.4.0") (f (quote ("std"))) (k 0)))) (h "0v4gjqsm2kd4prhwgpj0avfg9s5m59jv3ml08s1fvq4lpg9pr9j7")))

