(define-module (crates-io xw t- xwt-core) #:use-module (crates-io))

(define-public crate-xwt-core-0.2.0 (c (n "xwt-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)))) (h "0nbpshpbyj2qqchvjc6hjn2jxasgls57k12m24s3wc04h7zaqnxm")))

(define-public crate-xwt-core-0.2.1 (c (n "xwt-core") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)))) (h "1hzh3pnzl4f3ad03qbklf2lq56i355a4jz9npra7zfjjvr0jd7cz")))

(define-public crate-xwt-core-0.2.2 (c (n "xwt-core") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)))) (h "1psgsxwv7cs23n7pmq9f9q8d8mmy1cyy6ljlpbm0fy9hmzj4xx3v")))

(define-public crate-xwt-core-0.3.0 (c (n "xwt-core") (v "0.3.0") (h "1l8h8iqghqp9p4dfp9ilpj3184h7lgg58bcxvhxyl23l087k2z0d") (f (quote (("std" "alloc") ("error-in-core") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-xwt-core-0.3.1 (c (n "xwt-core") (v "0.3.1") (h "10lwv6sdnmlm959qsiz5hn9xhvmnn0af42gs8q4gn6qf7fbpgsjd") (f (quote (("std" "alloc") ("error-in-core") ("default" "std") ("alloc"))))))

(define-public crate-xwt-core-0.4.0 (c (n "xwt-core") (v "0.4.0") (h "032b55cvilmkppn5m6vhfi7bnlkbhfyw0whqly3m2hrrx2wwzikz") (f (quote (("std" "alloc") ("error-in-core") ("default" "std") ("alloc"))))))

(define-public crate-xwt-core-0.4.1 (c (n "xwt-core") (v "0.4.1") (h "1c44cx1h1ad1ch0a225q3dgga4ssvbx6pmhllijy67yc2w173wi2") (f (quote (("std" "alloc") ("error-in-core") ("default" "std") ("alloc"))))))

(define-public crate-xwt-core-0.4.2 (c (n "xwt-core") (v "0.4.2") (h "137dgwnd6j6xhcd9d71f36bfir9cs4gjvk6n2asqsfgrqb58myf1") (f (quote (("std" "alloc") ("error-in-core") ("default" "std") ("alloc"))))))

