(define-module (crates-io xw t- xwt-test-assets) #:use-module (crates-io))

(define-public crate-xwt-test-assets-0.1.0 (c (n "xwt-test-assets") (v "0.1.0") (d (list (d (n "xwt-test-assets-build") (r "^0.1") (k 1)))) (h "0w3hw8ca86dc585ybfaaga5d3zdsibbipsq5mpmsyp85r7g6v5ph")))

(define-public crate-xwt-test-assets-0.1.1 (c (n "xwt-test-assets") (v "0.1.1") (d (list (d (n "xwt-test-assets-build") (r "^0.2") (k 1)))) (h "06jdvx50cxg2b52zx3w4aldmwq7gbka9icdzzsc25s82z2yzx7jy")))

(define-public crate-xwt-test-assets-0.1.2 (c (n "xwt-test-assets") (v "0.1.2") (d (list (d (n "xwt-test-assets-build") (r "^0.3") (k 1)))) (h "0faalx91cih20c9ajw55blsvaf5bsmkk71sa4pnjdqvg7272fsgq")))

