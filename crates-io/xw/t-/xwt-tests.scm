(define-module (crates-io xw t- xwt-tests) #:use-module (crates-io))

(define-public crate-xwt-tests-0.2.0 (c (n "xwt-tests") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "xwt-core") (r "^0.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "17ds3g8qzkl2zcg6cs5dsdhsc7awadcl0vs96il1wq6difdwx7qy")))

(define-public crate-xwt-tests-0.2.1 (c (n "xwt-tests") (v "0.2.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "xwt-core") (r "^0.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "0mlmwjxccmbykk5ngmrm7drvx8pl1xqhybfxacc3asq4s9sa09v4")))

(define-public crate-xwt-tests-0.3.0 (c (n "xwt-tests") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "xwt-core") (r "^0.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "1l99miw7nciarrawa4pzsfpfhxg0wqv4ywa03xlwpyq7qdd8643b")))

(define-public crate-xwt-tests-0.3.1 (c (n "xwt-tests") (v "0.3.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "xwt-core") (r "^0.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "1lf2k4w853ykp85fnnrg0zm4x3ic93d32jqk9873c4aag6qlpgwb")))

(define-public crate-xwt-tests-0.3.2 (c (n "xwt-tests") (v "0.3.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "xwt-core") (r "^0.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "0x19izkmnvyx2604ni20j2f9adpa273jdqjfrlxm1w9lfnxbwl0a")))

(define-public crate-xwt-tests-0.4.1 (c (n "xwt-tests") (v "0.4.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "xwt-core") (r "^0.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "1lrsy8m67f5231h4bmafs9ghin45rwp36sx4xyynw4ng8zpp8lrd")))

(define-public crate-xwt-tests-0.4.2 (c (n "xwt-tests") (v "0.4.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "xwt-core") (r "^0.2.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "118svp3xm3i1k50q1mv49mbpj2hl3v1qggdnfccn4ymdwr1q4swc")))

(define-public crate-xwt-tests-0.4.3 (c (n "xwt-tests") (v "0.4.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "xwt-core") (r "^0.2.2") (k 0)) (d (n "xwt-error") (r "^0.2") (k 0)))) (h "0d4719p4rg8fx7klhaq1qwxsczn7mgd61cf4kq9wzjyqdyhcha24")))

(define-public crate-xwt-tests-0.5.0 (c (n "xwt-tests") (v "0.5.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "xwt-core") (r "^0.3") (k 0)) (d (n "xwt-error") (r "^0.3") (k 0)))) (h "0ja9pxn4b22iaxa7f2j38hnnq86fa38xqs21zq1c75dcj25cz3l7")))

(define-public crate-xwt-tests-0.6.0 (c (n "xwt-tests") (v "0.6.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "xwt-core") (r "^0.4") (k 0)) (d (n "xwt-error") (r "^0.4") (k 0)))) (h "1n0n49rfdhmjddpdqd3z461mm9acz6hamnvk6pk1vlbi0qqsm7yi")))

