(define-module (crates-io xw ii xwiimote-sys) #:use-module (crates-io))

(define-public crate-xwiimote-sys-0.1.0 (c (n "xwiimote-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "056322fcyy47wh076ybnmd5jlggkqpd2l1dn950ycif3zq2rkl6y")))

(define-public crate-xwiimote-sys-0.1.1 (c (n "xwiimote-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0cv9klg617i5x1wd922n256jgn1gzq02bb5v1z8b030s85ds3683")))

(define-public crate-xwiimote-sys-0.1.2 (c (n "xwiimote-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1xxxx5d3xa225c2dy6syjlgmrsa1lp44xs4hv1n31wlkg3c119l6")))

(define-public crate-xwiimote-sys-0.1.3 (c (n "xwiimote-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1l3793zx4lpfgvbj5lv47d25f0jq3dw4ibh155awc0x6j9495594")))

(define-public crate-xwiimote-sys-0.1.4 (c (n "xwiimote-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0g5kh8nryfsszrxdmx1z5fgfh45gy2gn2jbrq7agrs7palyacjqq")))

(define-public crate-xwiimote-sys-0.1.5 (c (n "xwiimote-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "141w0p66npdxkjq3pqj2lyy08na418piyf18j376bqil0dd8xgxk") (f (quote (("static")))) (l "xwiimote")))

(define-public crate-xwiimote-sys-0.1.6 (c (n "xwiimote-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "19rb5pa5gjrg7fsz3qdghjpmkzmn92wkj1qykq2s24zc80j2bq4r") (f (quote (("static")))) (l "xwiimote")))

