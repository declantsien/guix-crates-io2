(define-module (crates-io rp os rpos_drv) #:use-module (crates-io))

(define-public crate-rpos_drv-0.1.0 (c (n "rpos_drv") (v "0.1.0") (h "1jx335fzhacdafp1sxssy9wk46rxlln8cfsa2wgw9l52wqlvc035")))

(define-public crate-rpos_drv-0.2.0 (c (n "rpos_drv") (v "0.2.0") (d (list (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0yrjyqnjj2aqh9qiy2xnkhbynr8154bvi9hzvc40023g1756gdg9")))

