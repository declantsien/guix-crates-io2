(define-module (crates-io rp os rpos) #:use-module (crates-io))

(define-public crate-rpos-0.1.0-alpha.0 (c (n "rpos") (v "0.1.0-alpha.0") (h "1gyp3nw7ac2rpw1hpd9rv82226flla9clv6an7jivwga6cwdl99b") (y #t)))

(define-public crate-rpos-0.1.0 (c (n "rpos") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "1wir1hxpajxiwa0zwz06jl4j3f3dmhqai952cs8yhivpsf9c0fdz")))

(define-public crate-rpos-0.2.0-alpha.0 (c (n "rpos") (v "0.2.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "1kl7i62hm913jwfkfdp58zvrdjv2j5xwm55rz2f3p89bkh1klr7d") (y #t)))

(define-public crate-rpos-0.2.0 (c (n "rpos") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "0a8z2kpjclinvl9697rvzvxs1zs7mwyfbf8sgdx9bbcb51x4yrfh")))

(define-public crate-rpos-0.2.1 (c (n "rpos") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "0fbp32z6ilybi16g23n96kap0mcjvxyl44wzqnnqpzdqpspz1biy")))

(define-public crate-rpos-0.2.2 (c (n "rpos") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "11i5749lj7m7v75ggg6pmnvm34yq6yc9kzy5pjr1cgpq28swypd2")))

(define-public crate-rpos-0.3.0 (c (n "rpos") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "1k5y7mvflz34gbv4rzwhpwrdn96lkssk0zh8552n1lwdy793z3lx")))

