(define-module (crates-io rp in rping) #:use-module (crates-io))

(define-public crate-rping-0.1.0 (c (n "rping") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)))) (h "17pi2pd83bnls8144sbdvs909zvkrk5kna95ziw5aminrgx1vajg") (f (quote (("cli" "clap"))))))

(define-public crate-rping-0.1.1 (c (n "rping") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)))) (h "1x3m8ll49r4drkwy6jrjapgii67lpiddk1fl0l9kfizq8y65plly") (f (quote (("cli" "clap"))))))

(define-public crate-rping-0.1.2 (c (n "rping") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)))) (h "1isla8y9mlavb4ynrfh0l1ph4ykr9gp5bsnvh8cr3z4iyqfn81ap") (f (quote (("cli" "clap"))))))

(define-public crate-rping-0.1.3 (c (n "rping") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)))) (h "0snpv9zq8w93h798nvx78c7y3cvqwvzz02b225l6y7d6jyzqq60a") (f (quote (("cli" "clap"))))))

(define-public crate-rping-0.1.4 (c (n "rping") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)))) (h "1bzg68r96vjq933dr31kr93hj1fbc6zyikl31y6iw7hfdzhss3n1") (f (quote (("cli" "clap"))))))

(define-public crate-rping-0.1.5 (c (n "rping") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)))) (h "0pkaimk5p7d1nf7q28bsffkkrpxvy6180xc3cmwp7jkdbxc8dw9l") (f (quote (("cli" "clap"))))))

(define-public crate-rping-0.1.6 (c (n "rping") (v "0.1.6") (d (list (d (n "bump2version") (r "^0.1.3") (d #t) (k 2)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "socket2") (r "^0.5.5") (f (quote ("all"))) (d #t) (k 0)))) (h "03hmi54f3cp9ynw2sya35l0wkp2sb6w99kqmkmfx1ncgxzlx58bx") (f (quote (("cli" "clap"))))))

