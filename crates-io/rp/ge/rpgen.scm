(define-module (crates-io rp ge rpgen) #:use-module (crates-io))

(define-public crate-rpgen-0.1.1 (c (n "rpgen") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (f (quote ("paris"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "09g6fjzfzps9zsiz637jz0gss7b1r16bkd3245d09fih8s7k039k")))

