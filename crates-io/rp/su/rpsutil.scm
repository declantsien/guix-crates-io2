(define-module (crates-io rp su rpsutil) #:use-module (crates-io))

(define-public crate-rpsutil-0.0.1 (c (n "rpsutil") (v "0.0.1") (h "0wxygbic0ar9nxpx2wmjgg3kjf4klsp117kbyqwy8j7j8f4yj9jy")))

(define-public crate-rpsutil-0.0.2 (c (n "rpsutil") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12i50rqqga12r5i5zfmak3x78qjs3rng8j7f0x5j655gn0jyk1xa")))

(define-public crate-rpsutil-0.0.3 (c (n "rpsutil") (v "0.0.3") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zdqsp4cjbbg252kchhmy2nn8a29f7933kygkmai73rc84xsqamv")))

