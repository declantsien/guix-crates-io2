(define-module (crates-io rp ip rpipes) #:use-module (crates-io))

(define-public crate-rpipes-0.1.0 (c (n "rpipes") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.16") (f (quote ("wide"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1xwrphrg36k6lnaq0jarqnn5wl6cfy83acm8p3k63m24zn4kyfq2")))

(define-public crate-rpipes-0.1.1 (c (n "rpipes") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "pancurses") (r "^0.16") (f (quote ("wide" "win32"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1x14j9c0nll43hjxi2qppb082nz1g2aapnavx16qwn4jx0z185c4")))

