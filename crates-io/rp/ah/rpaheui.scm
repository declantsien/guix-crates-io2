(define-module (crates-io rp ah rpaheui) #:use-module (crates-io))

(define-public crate-rpaheui-0.0.1 (c (n "rpaheui") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "malachite-bigint") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "rustpython") (r "^0.3.1") (d #t) (k 0)) (d (n "rustpython-derive") (r "^0.3.1") (d #t) (k 0)))) (h "13dhlfx687ih0v9x00ynvpgr4ka398ayh9aghy6vxcy9mcjgi489") (f (quote (("default")))) (r "1.67.1")))

