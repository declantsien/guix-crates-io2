(define-module (crates-io rp tr rptree) #:use-module (crates-io))

(define-public crate-rptree-0.1.0 (c (n "rptree") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jhwcia6qfyg1gcrdxyya9bm25ck2fpx690j0qrrsxn915rsp4p4")))

(define-public crate-rptree-0.2.0 (c (n "rptree") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1rh7vh416b3s0ayjk8lpkqgyihd2dxh5y0mga33pfc2kw0c925rp")))

(define-public crate-rptree-0.3.0 (c (n "rptree") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0s15fpnr3x687a9p5kxi629n1j8ab66xj75dp1mqjsd7faa9k12f")))

(define-public crate-rptree-0.4.0 (c (n "rptree") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1c4yxxxy7041x1sxhpni6r8ic16axxk0xb4bz0ni8f838af96nmd")))

