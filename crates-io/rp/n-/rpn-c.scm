(define-module (crates-io rp n- rpn-c) #:use-module (crates-io))

(define-public crate-rpn-c-0.1.1 (c (n "rpn-c") (v "0.1.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "1czflmz4g8p2m8hgjgh41mxfk1g7305d3v53mxraqlm0421bn2vz")))

(define-public crate-rpn-c-0.1.2 (c (n "rpn-c") (v "0.1.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "1sgvjzffsy3jxs6m4vfw8ahva8c4bn1rcbsrgds0jcfr8zk3s89n")))

(define-public crate-rpn-c-0.1.3 (c (n "rpn-c") (v "0.1.3") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "13im427sngzw1hp6g49fwcdz0dhsp77vm1fwqn7fkkcx9ywjfgwd")))

(define-public crate-rpn-c-0.1.4 (c (n "rpn-c") (v "0.1.4") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)))) (h "0zzlf09s0rdb0r91h0d5ayj8fgmik7cpn22yjylbycaipl6aq2a0")))

(define-public crate-rpn-c-0.2.0 (c (n "rpn-c") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ramp") (r "^0.5.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0rn0bw7bgdd89s9yxpqf51cdrmcli0mkmilpqmpghq4gcgn9vhm6")))

(define-public crate-rpn-c-0.2.1 (c (n "rpn-c") (v "0.2.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ramp") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "04wqzl5rr5f87a5w39l483gm5b6ja3did5pslj9r2fg8awx89h5c")))

(define-public crate-rpn-c-0.2.2 (c (n "rpn-c") (v "0.2.2") (d (list (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ramp") (r "^0.6.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1y6cr15m2ypb9mjj92q5n02cfv2f92gggpml29jgqspp1r52gzvw")))

(define-public crate-rpn-c-0.2.3 (c (n "rpn-c") (v "0.2.3") (d (list (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ramp") (r "^0.7.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1vx7mhbhdphpkq326781l081z4fvpyzvjpm1rygbz0rwbnvvvak5")))

