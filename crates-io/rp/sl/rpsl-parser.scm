(define-module (crates-io rp sl rpsl-parser) #:use-module (crates-io))

(define-public crate-rpsl-parser-0.1.0 (c (n "rpsl-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)))) (h "12a3qbv7yaxd9593p56wqh3apvpsgbp0w07wzvkp8kr71f0n881x")))

(define-public crate-rpsl-parser-0.1.1 (c (n "rpsl-parser") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)))) (h "16apv8pshc1ia56ks883qybyb7c8zdn64nbj5pdpg33bri7wdxpx")))

(define-public crate-rpsl-parser-0.1.2 (c (n "rpsl-parser") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (d #t) (k 0)))) (h "1763igcbf5xk5d560g46didkv7hg17kjbcjj98rj8csb0zzdnfpq")))

(define-public crate-rpsl-parser-0.1.3 (c (n "rpsl-parser") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1ysrav9kskfimcr44md4dbl6w1gishw7mc7c55c33r14cjaa5vdb")))

(define-public crate-rpsl-parser-0.1.4 (c (n "rpsl-parser") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "0vrd317kcwnf27xhzx49kasyqahwqhchdh8fhp6l14s19c5gfwpd")))

