(define-module (crates-io rp sl rpsl-rs) #:use-module (crates-io))

(define-public crate-rpsl-rs-1.0.0 (c (n "rpsl-rs") (v "1.0.0") (d (list (d (n "codspeed-criterion-compat") (r "^2.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0wwx7rhb845yjr2wzmkgki05m7mhp3jl4d9mq50a3bm44zdhz1lz")))

(define-public crate-rpsl-rs-1.0.1 (c (n "rpsl-rs") (v "1.0.1") (d (list (d (n "codspeed-criterion-compat") (r "^2.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1lmhx6940687kfblf340a0g13iykc05azgp0i56crf3jc08c6c9m") (r "1.70")))

