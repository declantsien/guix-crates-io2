(define-module (crates-io rp at rpatch) #:use-module (crates-io))

(define-public crate-rpatch-0.1.0 (c (n "rpatch") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "patch") (r "^0.7.0") (d #t) (k 0)))) (h "02fl84nazmdcy5zfcnh6226vfbwjg929v6837hx5xxplz4ahkxbl")))

(define-public crate-rpatch-0.1.1 (c (n "rpatch") (v "0.1.1") (d (list (d (n "patch") (r "^0.7.0") (d #t) (k 0)))) (h "10qlyjpyxi46i3k7ca0abr0rg7fil3h77cy3gb93phs3balcwd2m")))

