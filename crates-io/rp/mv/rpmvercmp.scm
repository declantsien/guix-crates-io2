(define-module (crates-io rp mv rpmvercmp) #:use-module (crates-io))

(define-public crate-rpmvercmp-0.1.0 (c (n "rpmvercmp") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "1vxb4mq7dca2m9w9iww78hyxzhv1ihwn2xhjic81bkysnlm9awrv") (f (quote (("cmdline" "clap")))) (r "1.60")))

