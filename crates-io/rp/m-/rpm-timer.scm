(define-module (crates-io rp m- rpm-timer) #:use-module (crates-io))

(define-public crate-rpm-timer-0.0.1 (c (n "rpm-timer") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 0)))) (h "1ihdbqyqkvcyhjyasn3w6wbkw16569kj1vhq0jwzxi4n6p77i9q1")))

(define-public crate-rpm-timer-0.0.2 (c (n "rpm-timer") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 0)))) (h "11blvjql0jil7c8hk433kqa4fl5m8x2mj5q84s7nnjyycpfvl64q")))

(define-public crate-rpm-timer-0.0.3 (c (n "rpm-timer") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 0)))) (h "1g15dvqp5yrn4v1lyx8bagpfb1g41md7k0g85qz1pqy04czj4ix1")))

