(define-module (crates-io rp m- rpm-pkg-count) #:use-module (crates-io))

(define-public crate-rpm-pkg-count-0.1.0 (c (n "rpm-pkg-count") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1hncbmkdk71vgjd5zc3b11rhsgfjf83i8a3dg4m7pyjbavd2iyd9") (l "rpm")))

(define-public crate-rpm-pkg-count-0.2.0 (c (n "rpm-pkg-count") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (o #t) (d #t) (k 1)))) (h "0mqnxhapwd2g09fw737wiiswhfcx954awkdmdynrhkyq71kmrvn3") (f (quote (("default")))) (l "rpm") (s 2) (e (quote (("runtime" "dep:libloading") ("compile-time" "dep:pkg-config"))))))

(define-public crate-rpm-pkg-count-0.2.1 (c (n "rpm-pkg-count") (v "0.2.1") (d (list (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (o #t) (d #t) (k 1)))) (h "1jzh63l7k30l37s5gd2m2hvh8slzjaxw54s4xpcjnaqb8xfsq0sa") (f (quote (("default")))) (l "rpm") (s 2) (e (quote (("runtime" "dep:libloading") ("compile-time" "dep:pkg-config"))))))

