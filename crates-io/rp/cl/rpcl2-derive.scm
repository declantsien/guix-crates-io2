(define-module (crates-io rp cl rpcl2-derive) #:use-module (crates-io))

(define-public crate-rpcl2-derive-0.1.0 (c (n "rpcl2-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1csxv60ca38ag1xf3cr3cl83safk5vdxjs40i01mw7jpw5sdmnv5")))

(define-public crate-rpcl2-derive-0.2.0 (c (n "rpcl2-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mzmd7l86bdkkd6vx2ds0cqrbljxcds9br850a70mids4qjp06wk")))

