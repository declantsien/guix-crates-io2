(define-module (crates-io rp i_ rpi_memory_display) #:use-module (crates-io))

(define-public crate-rpi_memory_display-0.1.0 (c (n "rpi_memory_display") (v "0.1.0") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1iah4ma68jx5g0bzmimb8zy483fa2qla7ivcz1lbv6ss47kfdnzk")))

(define-public crate-rpi_memory_display-0.1.1 (c (n "rpi_memory_display") (v "0.1.1") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "13np29q0z5mn9mamx6bf5d0mc6y1d1arl45wmhmappsn2gd6njrz")))

(define-public crate-rpi_memory_display-0.1.2 (c (n "rpi_memory_display") (v "0.1.2") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "00yy1wx915w1dcmn9mn0ga7zr5nan167f0y3nfqibfs9snyi1dw3")))

