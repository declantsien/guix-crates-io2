(define-module (crates-io rp i_ rpi_window) #:use-module (crates-io))

(define-public crate-rpi_window-1.0.0 (c (n "rpi_window") (v "1.0.0") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1srswrmkvmkm9hr4lax3fj71fzvjrb9l9qqk3q91b533z0w9f7lp")))

(define-public crate-rpi_window-1.0.1 (c (n "rpi_window") (v "1.0.1") (d (list (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "04xyjzlsql8y3ladq2zcj0lhhm9pxab1i9aldxb8fsfbgg6qlqf3")))

