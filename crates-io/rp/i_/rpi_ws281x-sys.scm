(define-module (crates-io rp i_ rpi_ws281x-sys) #:use-module (crates-io))

(define-public crate-rpi_ws281x-sys-0.1.0 (c (n "rpi_ws281x-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1rs8qczzj1npdhniibsr5lz7lpwjy6m5g0wld607vbvaf39a6vkm") (y #t)))

(define-public crate-rpi_ws281x-sys-0.1.1 (c (n "rpi_ws281x-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0xlk0pwa77mhdgpl420cnkxv8syzfj8hpszmg7g6vy325jaj9lgr") (y #t)))

(define-public crate-rpi_ws281x-sys-0.1.2 (c (n "rpi_ws281x-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1gnd4ph4gwsb2208zhf20av1vfpm48zw4x57xl28pqszkzkp3nhn") (y #t)))

(define-public crate-rpi_ws281x-sys-0.1.3 (c (n "rpi_ws281x-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0nj9dr06a72jf1ay66c0m395rd4hnf8fbdblizxnxm4gn1k78ib3")))

(define-public crate-rpi_ws281x-sys-0.1.4 (c (n "rpi_ws281x-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0gxrcrx7ilzngz049im0d90fc95g3lm6x2dpb3a4ld9ssmb6fvcj") (y #t)))

(define-public crate-rpi_ws281x-sys-0.1.5 (c (n "rpi_ws281x-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0sn7kgpnavrkarxkjs0306nah6hd8bdbzj245ahcck618vij3754")))

