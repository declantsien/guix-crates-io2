(define-module (crates-io rp i_ rpi_ws281x-c) #:use-module (crates-io))

(define-public crate-rpi_ws281x-c-0.1.0 (c (n "rpi_ws281x-c") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "rpi_ws281x-sys") (r "^0.1.3") (d #t) (k 0)))) (h "068wlkp82fanl7pfgapw36x522fvmxydqjqclfnkszpqw6ddidhl") (y #t)))

(define-public crate-rpi_ws281x-c-0.1.1 (c (n "rpi_ws281x-c") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "rpi_ws281x-sys") (r "^0.1.3") (d #t) (k 0)))) (h "18nl5ighc7fyy9kix6vqfv7j81bmmf5psjl9zkyli07x9qxzq2d4") (y #t)))

(define-public crate-rpi_ws281x-c-0.1.2 (c (n "rpi_ws281x-c") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "rpi_ws281x-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0xv38sdbn7scy9arxxd0xcbh31vcmw6fm9b35h6kxnmggfikm2v3") (y #t)))

(define-public crate-rpi_ws281x-c-0.1.3 (c (n "rpi_ws281x-c") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "rpi_ws281x-sys") (r "^0.1.3") (d #t) (k 0)))) (h "07n23p10gnnjv6apkngk1qvdckjpzg2lmwa0q9q30cq34w35k785")))

(define-public crate-rpi_ws281x-c-0.1.4 (c (n "rpi_ws281x-c") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "rpi_ws281x-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1v2y0chy9kfq3kmrbaj8nfz3p7cg9gndb7466ypi8wikcgwgm7l9")))

(define-public crate-rpi_ws281x-c-0.1.5 (c (n "rpi_ws281x-c") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "rpi_ws281x-sys") (r "^0.1.3") (d #t) (k 0)))) (h "03lnj8h9m1yn91jj7rxsgdfskx2zb3917yyfp9ivbbmj9pb96bwa")))

