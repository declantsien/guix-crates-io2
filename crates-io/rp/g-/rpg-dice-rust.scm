(define-module (crates-io rp g- rpg-dice-rust) #:use-module (crates-io))

(define-public crate-rpg-dice-rust-1.1.0 (c (n "rpg-dice-rust") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "evalexpr") (r "^5.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x8ps98vl55ms5kwq5zkzw9wj6cm418vp08p6787hhjbg8hypb40")))

