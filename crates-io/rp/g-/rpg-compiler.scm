(define-module (crates-io rp g- rpg-compiler) #:use-module (crates-io))

(define-public crate-rpg-compiler-0.1.0 (c (n "rpg-compiler") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_colors") (r "^1") (d #t) (k 0)) (d (n "spinner") (r "^0.5.0") (d #t) (k 0)) (d (n "spinners") (r "^2") (d #t) (k 0)))) (h "095k96rpw9m29hhy6qihazxid5d24qf5l2gf87bxa71ijvzfmc5s")))

(define-public crate-rpg-compiler-0.1.1 (c (n "rpg-compiler") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_colors") (r "^1") (d #t) (k 0)) (d (n "spinner") (r "^0.5.0") (d #t) (k 0)) (d (n "spinners") (r "^2") (d #t) (k 0)))) (h "0yfbwl0n00i823y4rin4bxajvd9jkazb1rp5na2b0bjih4qfbs7r")))

