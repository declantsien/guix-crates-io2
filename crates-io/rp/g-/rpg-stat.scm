(define-module (crates-io rp g- rpg-stat) #:use-module (crates-io))

(define-public crate-rpg-stat-0.1.0 (c (n "rpg-stat") (v "0.1.0") (h "01n4va10lk7lm0q8l3xsdxjizlh98br71i9pqln06q2jxm0yxnp6")))

(define-public crate-rpg-stat-0.1.1 (c (n "rpg-stat") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g3id3fmbbg60bp1abx58v5f7jq4z9jjwlppjv058fvn27r4xlcb")))

(define-public crate-rpg-stat-0.1.2 (c (n "rpg-stat") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "049vk4vyvaycxl0ra2ga2wjp2x108dycp7il5lfn14svvhr927vd")))

(define-public crate-rpg-stat-0.1.3 (c (n "rpg-stat") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gzqw3bbahfjzfy7vl3w6mgj5znlk4i3n0jq9nmyzqr2y18j2lz4")))

(define-public crate-rpg-stat-0.1.4 (c (n "rpg-stat") (v "0.1.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "16qw8jdjhl4379kb7cvpydhnnzw12g6330a5n3ba2jawfqvj8wnh")))

(define-public crate-rpg-stat-0.1.5 (c (n "rpg-stat") (v "0.1.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "1v62kbwj5w9slwbbjy92w2zp0lpbap5a92ykz6290falr8gb2s96")))

(define-public crate-rpg-stat-1.0.0 (c (n "rpg-stat") (v "1.0.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "0a01hj28l2ba3pnvbyivk06qh0z2dw9gxlsjrxiz2k1dradyyr15")))

(define-public crate-rpg-stat-1.0.1 (c (n "rpg-stat") (v "1.0.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "0939d3ymfll5jgv6gpn3fml1pi81mbkxxy3yl2rxy2fkbp1q76a8")))

(define-public crate-rpg-stat-1.1.1 (c (n "rpg-stat") (v "1.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "0kvwdh197j0iydwa8ccxpxncp0gwir0yx1w8hl2d9lvrx8d2whqf")))

(define-public crate-rpg-stat-1.1.2 (c (n "rpg-stat") (v "1.1.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "022qpnrycdmr4lcq6wiva2y99wx3jcq5gzkgvd88dp6in9r7i0ma")))

(define-public crate-rpg-stat-1.1.3 (c (n "rpg-stat") (v "1.1.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "0hhpnrbz81khkrxiaz9lm3wi3g9xm2rwl03vjnjjlvjfag38af9j")))

(define-public crate-rpg-stat-1.1.4 (c (n "rpg-stat") (v "1.1.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "15jwfvg0i236sri5qgza3pawz5q407hq84mq64ampm2wb12wnsfv")))

(define-public crate-rpg-stat-1.1.5 (c (n "rpg-stat") (v "1.1.5") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)))) (h "1ig2hcx68mxm0nyqq8sn9lgky3had17zlvlgyalby585n38sryxn")))

(define-public crate-rpg-stat-1.1.7 (c (n "rpg-stat") (v "1.1.7") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zv3q57as52yfmq62h52rarli42zd6dbh7v32psa95w3gawbqrfr")))

(define-public crate-rpg-stat-1.1.8 (c (n "rpg-stat") (v "1.1.8") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "16981lmmgfwfr352y25jhb5jiyl4dpqzrlaxzb17f8ywgiybwwra")))

(define-public crate-rpg-stat-2.0.0 (c (n "rpg-stat") (v "2.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "056m3q5h98aklg4ciq6vmvh9spq8hq7phz9xfajkazrfnqjcc8if")))

(define-public crate-rpg-stat-2.0.1 (c (n "rpg-stat") (v "2.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "146xbj54axd8mjgzydl7bapng6afvnj86fknby8i5f17wbczfhp4")))

(define-public crate-rpg-stat-3.0.0 (c (n "rpg-stat") (v "3.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1swy95b79243aamjggm15br6w0h8yl3swfh1dh1wcb3x5lmadmh0")))

(define-public crate-rpg-stat-2021.12.7 (c (n "rpg-stat") (v "2021.12.7") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fltk") (r "^1.2.6") (o #t) (d #t) (k 0)) (d (n "fltk-form") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "fltk-form-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1yim346g26i9pj7dwmzpllpi9cf1l9zrg68y0j1yjg8pavh5321q") (f (quote (("makesvg" "svg") ("fltkform" "fltk" "fltk-form" "fltk-form-derive") ("default"))))))

(define-public crate-rpg-stat-2021.12.8 (c (n "rpg-stat") (v "2021.12.8") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fltk") (r "^1.2.6") (o #t) (d #t) (k 0)) (d (n "fltk-form") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "fltk-form-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "147gqzy6y3kzizin2f9gk5nrdpv6flbhfx4jr210r55mbj4yvzw5") (f (quote (("makesvg" "svg") ("fltkform" "fltk" "fltk-form" "fltk-form-derive") ("default"))))))

(define-public crate-rpg-stat-2021.12.9 (c (n "rpg-stat") (v "2021.12.9") (d (list (d (n "clap") (r "^2.3.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fltk") (r "^1.2.6") (o #t) (d #t) (k 0)) (d (n "fltk-form") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "fltk-form-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08dgsvxwaz81ialr5jp0811q71mzwxc903hx7gf8wk0c2mnwj0nr") (f (quote (("fltkform" "fltk" "fltk-form" "fltk-form-derive") ("default"))))))

(define-public crate-rpg-stat-2021.12.16 (c (n "rpg-stat") (v "2021.12.16") (d (list (d (n "clap") (r "^2.3.4") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fltk") (r "^1.2.6") (o #t) (d #t) (k 0)) (d (n "fltk-form") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "fltk-form-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1h8gb8v9k7c3xv5qsv5zvp8gqkm848cbn3sh0cpkymqwndbazxnr") (f (quote (("fltkform" "fltk" "fltk-form" "fltk-form-derive") ("default"))))))

