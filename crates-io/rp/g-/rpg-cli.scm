(define-module (crates-io rp g- rpg-cli) #:use-module (crates-io))

(define-public crate-rpg-cli-0.1.0 (c (n "rpg-cli") (v "0.1.0") (d (list (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lk6qf38rpg18r30f6ixhbha2fawan7ypryyb7b8gnhsjz7gh5c6")))

(define-public crate-rpg-cli-0.1.1 (c (n "rpg-cli") (v "0.1.1") (d (list (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01zws8dsv96ygacgm8p33qn47ivq026c9gxbwn6fzxargkl67lyg")))

(define-public crate-rpg-cli-0.2.0 (c (n "rpg-cli") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fdng0d496y8kr61dldi2lmprqkxa7yrh0w8sn5wrnj5zwpw8xh8")))

