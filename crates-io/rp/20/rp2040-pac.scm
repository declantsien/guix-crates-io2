(define-module (crates-io rp #{20}# rp2040-pac) #:use-module (crates-io))

(define-public crate-rp2040-pac-0.1.0 (c (n "rp2040-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "015qdbyvxmpaj2459ljdhralwf7z9394xxiykqbr7d18f3ljy269") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.1.1 (c (n "rp2040-pac") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0qqr0yvf0zgyqri812v7nrivnaxr5gi3pw1vvrhfm2w69wf5gkm8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.1.2 (c (n "rp2040-pac") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "100cfg88638n2p4jfxpwlys80my0s24nj72n40jpvz6qvqzdj95i") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.1.3 (c (n "rp2040-pac") (v "0.1.3") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0bsy9va2zp89kivn8589cvw551s1759f5am0gfrvja074jbn2ccn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.1.4 (c (n "rp2040-pac") (v "0.1.4") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0z244l1hnjlhkqj7w2vy268g9976dc1aspp4drj20fdz6haq9d6s") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.1.5 (c (n "rp2040-pac") (v "0.1.5") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0xjzs6lkgsdi5na2hfsyafrn21pbnir26pmgdz9hi7wy4cn24vjv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.1.6 (c (n "rp2040-pac") (v "0.1.6") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "08k623xpc698dq1pnzh3mhz4zyq7r1097gs14xj04kffcs20hn3g") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-rp2040-pac-0.2.0 (c (n "rp2040-pac") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0kyd4ks0qplbggz4r9140rv43qqyg19h68hb5mj9jpnrhx30k66l") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.2.1 (c (n "rp2040-pac") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gr5dkajdz3wn5hifal16xwqjddp60hl5b90rx50ff00cy9gq16v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.3.0 (c (n "rp2040-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0z7a0j4rq1k0vcicfypy8adxn4h9g2b7c7wwldqp275hbmni19hk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.4.0 (c (n "rp2040-pac") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0zixkxnizcap7f2ys8ldwjsf3zk9kjpplxnz1ng7qw8dnkxwm4li") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.5.0 (c (n "rp2040-pac") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0k3fm4fww6gcy7w7zwbmmqn9wzz4sih13l1m93sl7x8mb0vxin8j") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp2040-pac-0.6.0 (c (n "rp2040-pac") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0174sscs6bx7lkrpp6knggif9gj250h7xsy43pkbp9qcg8zwvjw3") (f (quote (("rt" "cortex-m-rt/device"))))))

