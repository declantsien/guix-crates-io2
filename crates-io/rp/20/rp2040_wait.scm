(define-module (crates-io rp #{20}# rp2040_wait) #:use-module (crates-io))

(define-public crate-rp2040_wait-0.1.0 (c (n "rp2040_wait") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.4") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (k 2)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rp2040-boot2") (r "^0.2.1") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.8.0") (f (quote ("rt" "critical-section-impl"))) (d #t) (k 0)) (d (n "rp2040-hal") (r "^0.8.0") (f (quote ("rt" "critical-section-impl"))) (d #t) (k 2)))) (h "16y6w1knb3yi275chl1ccwkpgry0pnjxrz4k64k5kq544bmx4jvx")))

