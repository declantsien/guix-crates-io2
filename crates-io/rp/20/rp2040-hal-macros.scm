(define-module (crates-io rp #{20}# rp2040-hal-macros) #:use-module (crates-io))

(define-public crate-rp2040-hal-macros-0.1.0 (c (n "rp2040-hal-macros") (v "0.1.0") (d (list (d (n "cortex-m-rt") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0piaczzlbrfdhidnqkg04xs1rzal3w3zjplrh6pf3vwpwiir0iw6")))

