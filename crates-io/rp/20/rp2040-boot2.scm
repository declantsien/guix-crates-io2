(define-module (crates-io rp #{20}# rp2040-boot2) #:use-module (crates-io))

(define-public crate-rp2040-boot2-0.1.2 (c (n "rp2040-boot2") (v "0.1.2") (d (list (d (n "crc-any") (r "^2.3") (d #t) (k 1)))) (h "0jk0k2x3gihvj1bbxg0b4asa82hpq22gqql4vw4fq60hqpf06ci3")))

(define-public crate-rp2040-boot2-0.2.0 (c (n "rp2040-boot2") (v "0.2.0") (d (list (d (n "crc-any") (r "^2.3") (d #t) (k 1)))) (h "0a3ifgwscdp0ds4z2zpnjancgjp7zqks1r9cz0wxdw1k67k0yj4b") (f (quote (("assemble"))))))

(define-public crate-rp2040-boot2-0.2.1 (c (n "rp2040-boot2") (v "0.2.1") (d (list (d (n "crc-any") (r "^2.3") (d #t) (k 1)))) (h "1n5b1xhcgrqs6qjhvkxnrppf2hx2aivdqn2b2jm7fq43kg23wxrw") (f (quote (("assemble"))))))

(define-public crate-rp2040-boot2-0.3.0 (c (n "rp2040-boot2") (v "0.3.0") (d (list (d (n "crc-any") (r "^2.3") (d #t) (k 1)))) (h "08dv9ndvdzyjz4wdlxcikf1m1s6wwi80027ldkihx59zyr2g74kw") (f (quote (("assemble"))))))

