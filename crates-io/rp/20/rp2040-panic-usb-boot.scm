(define-module (crates-io rp #{20}# rp2040-panic-usb-boot) #:use-module (crates-io))

(define-public crate-rp2040-panic-usb-boot-0.1.0 (c (n "rp2040-panic-usb-boot") (v "0.1.0") (d (list (d (n "bare-io") (r "^0.2.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "rp-hal") (r "^0.0.1") (f (quote ("2040"))) (k 0)))) (h "1h75b17g380gg0wxijl4v2r8hra0f7bkx63lh12i3m003gyvsn4l")))

(define-public crate-rp2040-panic-usb-boot-0.1.1 (c (n "rp2040-panic-usb-boot") (v "0.1.1") (d (list (d (n "bare-io") (r "^0.2.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "rp-hal") (r "^0.0.1") (f (quote ("2040"))) (k 0)))) (h "1fj560zc84ka4vijqqpi5kkwy5bdg384cykl8w6wykz14h02ydnj")))

(define-public crate-rp2040-panic-usb-boot-0.1.2 (c (n "rp2040-panic-usb-boot") (v "0.1.2") (d (list (d (n "bare-io") (r "^0.2.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "rp-hal") (r "^0.0.2") (f (quote ("2040"))) (k 0)))) (h "1c8rpd447qva73yqljbg6wx7jnvq8kjbim4i760fywx5xzkxjvq6")))

(define-public crate-rp2040-panic-usb-boot-0.2.0 (c (n "rp2040-panic-usb-boot") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 2)) (d (n "embedded-time") (r "^0.12.1") (d #t) (k 2)) (d (n "heapless") (r "^0.7.13") (d #t) (k 0)) (d (n "rp-pico") (r "^0.3.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.4.0") (d #t) (k 0)))) (h "0xblgxy4k7v0h9nh59wj0cijd64l3zad6prbrxgy6ad57yhnhqly")))

(define-public crate-rp2040-panic-usb-boot-0.3.0 (c (n "rp2040-panic-usb-boot") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 2)) (d (n "rp-pico") (r "^0.5.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.6.0") (k 0)))) (h "0vys6rw9bmx7m41a0jjqw0zphi55kwigi5dzm7d7g1h2zv5lsif4")))

(define-public crate-rp2040-panic-usb-boot-0.5.0 (c (n "rp2040-panic-usb-boot") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "rp2040-hal") (r "^0.8.0") (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 2)) (d (n "rp-pico") (r "^0.7.0") (d #t) (k 2)))) (h "0wz3l2q80gjbr4yvm4yh1fcy0b6hgpz62nrl25akv9p5f8m1sc8b")))

