(define-module (crates-io rp #{20}# rp2040) #:use-module (crates-io))

(define-public crate-rp2040-0.1.0 (c (n "rp2040") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "116pzhhaw914b1d8qvnj3z2m4bk9m1s2a88r4xh4wgia3k5g1mlf") (f (quote (("rt" "cortex-m-rt/device"))))))

