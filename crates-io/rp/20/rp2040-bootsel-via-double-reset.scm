(define-module (crates-io rp #{20}# rp2040-bootsel-via-double-reset) #:use-module (crates-io))

(define-public crate-rp2040-bootsel-via-double-reset-0.0.1 (c (n "rp2040-bootsel-via-double-reset") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "rp2040-hal") (r "^0.9.0") (d #t) (k 0)))) (h "0hh77a74958bqiq318pmbrrs0qgk962v2gpdiirbkq4daqvqb0n3")))

