(define-module (crates-io rp #{20}# rp2040-monotonic) #:use-module (crates-io))

(define-public crate-rp2040-monotonic-1.0.0 (c (n "rp2040-monotonic") (v "1.0.0") (d (list (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rp2040-pac") (r "^0.2.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "11w1rp1vf0qciijpy216i45998bqzdl1c3cs04vjmf2x0mvsjn7p")))

(define-public crate-rp2040-monotonic-1.0.1 (c (n "rp2040-monotonic") (v "1.0.1") (d (list (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rp2040-pac") (r "^0.2.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "011kwlsl0giwrnbs0skk03z21pvh8af7llbn9mq2vsfm2sq89ll7")))

(define-public crate-rp2040-monotonic-1.1.0 (c (n "rp2040-monotonic") (v "1.1.0") (d (list (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rp2040-pac") (r ">=0.2.0, <0.4") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "06lws6cd4wygsfhlrz8xn21xqapx4s6d2f9jpwl6rb8kp5r4wyrv")))

(define-public crate-rp2040-monotonic-1.2.0 (c (n "rp2040-monotonic") (v "1.2.0") (d (list (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rp2040-pac") (r ">=0.2.0, <0.5") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "0bvmmrd180ys5zygy9r817w1714q2kwjx5mlw07azljj94fs63y3")))

(define-public crate-rp2040-monotonic-1.3.0 (c (n "rp2040-monotonic") (v "1.3.0") (d (list (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rp2040-pac") (r ">=0.2.0, <0.6") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "1qv9bwfqvkwk1mizp1jm6dr5f1wj6dsf0b9qjysi6sv0bfaz67im")))

