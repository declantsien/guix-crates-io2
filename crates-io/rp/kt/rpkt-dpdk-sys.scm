(define-module (crates-io rp kt rpkt-dpdk-sys) #:use-module (crates-io))

(define-public crate-rpkt-dpdk-sys-0.1.0 (c (n "rpkt-dpdk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "000ynv3n57jk2bj43xzd6hfg6b2pbqvb7q2a22ax3maizh98nf3n") (r "1.70")))

