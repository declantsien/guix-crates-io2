(define-module (crates-io rp kt rpkt) #:use-module (crates-io))

(define-public crate-rpkt-0.1.0 (c (n "rpkt") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (d #t) (k 2)) (d (n "smoltcp") (r "^0.8") (d #t) (k 0)) (d (n "smoltcp") (r "^0.8") (d #t) (k 2)))) (h "083as89gb2hhpnlmzh8p0vgs1hm77nfisxinvmvxkxywi3a8iy4z") (r "1.70")))

