(define-module (crates-io rp kt rpkt-dpdk) #:use-module (crates-io))

(define-public crate-rpkt-dpdk-0.1.0 (c (n "rpkt-dpdk") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "run-dpdk-sys") (r "^0.1.0") (d #t) (k 0) (p "rpkt-dpdk-sys")) (d (n "run-packet") (r "^0.1.0") (o #t) (d #t) (k 0) (p "rpkt")) (d (n "smoltcp") (r "^0.8") (d #t) (k 2)))) (h "1qbh8xc9whdwyd7g8qvmm765yap0imb5yz0wi1lrqmgza728bxd4") (s 2) (e (quote (("multiseg" "dep:run-packet")))) (r "1.70")))

