(define-module (crates-io rp ml rpmlib) #:use-module (crates-io))

(define-public crate-rpmlib-0.0.1 (c (n "rpmlib") (v "0.0.1") (d (list (d (n "rpmlib-sys") (r "^0") (o #t) (d #t) (k 0)))) (h "1j42crsahhgk62cp7qvw4hbdam7a7dhydxjwf3v390741b8xnrfm") (f (quote (("default" "rpmlib-sys")))) (y #t)))

(define-public crate-rpmlib-0.1.0 (c (n "rpmlib") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rpmlib-sys") (r "^0.4") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "0j83nydnpx92xs2an7acjzffhfadmkjw4qs05w7vb37x1n88jc2r") (y #t)))

(define-public crate-rpmlib-0.1.1 (c (n "rpmlib") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rpmlib-sys") (r "^0.4") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "1nc1w41smqmz50xiqkyxb6ggg3bgk2wfxv28gd5k797qs5j99f56") (y #t)))

(define-public crate-rpmlib-0.1.2 (c (n "rpmlib") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "rpmlib-sys") (r "^0.4") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "076441bqv8hy1c5y1j5syabnpbydyqn1mnwp5by4yikvwwxymck2") (y #t)))

(define-public crate-rpmlib-0.99.0 (c (n "rpmlib") (v "0.99.0") (h "1nn0naw3nzfm12ffgq9n8ndqczrr1m6ijza7vak2bh61q9n9gc9y") (y #t)))

