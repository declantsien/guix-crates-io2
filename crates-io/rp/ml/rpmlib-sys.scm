(define-module (crates-io rp ml rpmlib-sys) #:use-module (crates-io))

(define-public crate-rpmlib-sys-0.1.0 (c (n "rpmlib-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "14mwphz6h8qkj0rxcmfabyql7i5pywf139h7p3g0d9qjv3kj2hkl") (y #t)))

(define-public crate-rpmlib-sys-0.2.0 (c (n "rpmlib-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "0mhf934wrrmcsxni73z2wfrz799gd2y6gr3lcgni8lh19nk7mrdp") (y #t)))

(define-public crate-rpmlib-sys-0.3.0 (c (n "rpmlib-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "01nhk3ss0z4rhbq4y6yc7m56kkvb9namjbcp2zc7xh1mh7zqdhyd") (f (quote (("rpmsign") ("rpmbuild") ("default")))) (y #t)))

(define-public crate-rpmlib-sys-0.4.0 (c (n "rpmlib-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "1crhj2610vd3rz5gcjm6qqg99rjrs3mlkadb24dvg2f907wy0vsk") (f (quote (("rpmsign") ("rpmbuild") ("default")))) (y #t)))

(define-public crate-rpmlib-sys-0.4.1 (c (n "rpmlib-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "1zna7qsc1in3a306jiwn0s4d9xqxnqa9axm0m7bgg41l87b4m8jb") (f (quote (("rpmsign") ("rpmbuild") ("default")))) (y #t)))

(define-public crate-rpmlib-sys-0.99.0 (c (n "rpmlib-sys") (v "0.99.0") (h "0q10ism2mydfgwqrc2sqncwal6c5j5fkij4lf4s1xa6zz55581wj") (y #t)))

