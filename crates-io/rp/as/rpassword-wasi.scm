(define-module (crates-io rp as rpassword-wasi) #:use-module (crates-io))

(define-public crate-rpassword-wasi-5.0.1 (c (n "rpassword-wasi") (v "5.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "winnt" "fileapi" "processenv" "winbase" "handleapi" "consoleapi" "minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ambshscss2z20gcfvkmyx1w8f6pwks1f4hyi8dyqg7jxjg3z4j4")))

(define-public crate-rpassword-wasi-5.0.2 (c (n "rpassword-wasi") (v "5.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "winnt" "fileapi" "processenv" "winbase" "handleapi" "consoleapi" "minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0x2qgklc74vjrisw5wigfa09gp4bmgxqya4lm7y3avfxnfy2ygid")))

(define-public crate-rpassword-wasi-5.0.3 (c (n "rpassword-wasi") (v "5.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "winnt" "fileapi" "processenv" "winbase" "handleapi" "consoleapi" "minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sjapzgk058dsva0hmp0768sahzwbp8ssb13ps7wqv1nmpxb4xm9")))

(define-public crate-rpassword-wasi-5.0.4 (c (n "rpassword-wasi") (v "5.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "winnt" "fileapi" "processenv" "winbase" "handleapi" "consoleapi" "minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1m76qwg2912glgg42zv2jaqmm5vqhwg4djy93rwvyic418kv0da1")))

