(define-module (crates-io rp cx rpcx_derive) #:use-module (crates-io))

(define-public crate-rpcx_derive-0.1.0 (c (n "rpcx_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "10yqh55cgv8irzgv4rhq9vf8wvr4mm4cyjjprkpiqdv11fgfw6wb")))

(define-public crate-rpcx_derive-0.1.1 (c (n "rpcx_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1q72k14g9r53hv37ci3jsifzbkm302y0vhpx6rm5iw1pqw1wbnk4")))

(define-public crate-rpcx_derive-0.1.2 (c (n "rpcx_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1vk3vkld6mzwaf3hlf2m21c62rligps23fi17i2i7kd3iqlxmgnr")))

(define-public crate-rpcx_derive-0.1.3 (c (n "rpcx_derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1s46xam6v0a54mhblnyzi84b5f4yn152x7r8dwvljinjqhhd2wv8")))

(define-public crate-rpcx_derive-0.2.0 (c (n "rpcx_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "04bpbchggx944lwm4xbrfm6r5kvgw4zsba16anndwb1hhq4ndkg2")))

(define-public crate-rpcx_derive-0.2.1 (c (n "rpcx_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qn7a3bkhv6v4zpzr5kji5gsl6706b9g5vz5vfmlifgnaiss67g6")))

(define-public crate-rpcx_derive-0.2.2 (c (n "rpcx_derive") (v "0.2.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1l2bb5hfli5s7xdsnpq3nlg89qqffz7liqf7g332aln2wy48fl5z")))

(define-public crate-rpcx_derive-0.3.0 (c (n "rpcx_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rpcx_protocol") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "02jm9xl0rrbixvhq7hlbznlyn04q7hlv9szvn3wzafd34b8z1j09")))

