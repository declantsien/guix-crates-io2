(define-module (crates-io rp ds rpds-pathtree) #:use-module (crates-io))

(define-public crate-rpds-pathtree-0.0.23 (c (n "rpds-pathtree") (v "0.0.23") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rpds") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "12f5nhjq5945klq946x2x0ghprx863v49w605s3pxhwkyi4xnwaj") (f (quote (("sync") ("default"))))))

(define-public crate-rpds-pathtree-0.1.0 (c (n "rpds-pathtree") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rpds") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0phil2nzfvr0pqvzn7z6dkv5r6wvy1fp4mwm9ik10iafbh5zxgaa") (f (quote (("sync") ("default"))))))

(define-public crate-rpds-pathtree-0.1.1 (c (n "rpds-pathtree") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rpds") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0mkykhp8l5jvrfjj95vckrb3d82wlac62ij9k39gxswm1jl54kwc") (f (quote (("sync") ("default"))))))

(define-public crate-rpds-pathtree-0.2.0 (c (n "rpds-pathtree") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rpds") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0a13160smwda3yq2rr1jx8pg6yzv7wflsd7g0rzdnraihkpwgggi") (f (quote (("sync") ("default"))))))

