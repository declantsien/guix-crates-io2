(define-module (crates-io rp -s rp-sys) #:use-module (crates-io))

(define-public crate-rp-sys-0.1.0 (c (n "rp-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1sfvjrrxhwzrv61pd1zqjqjb0vjril9kw1rbal3hcajzb0hq4zcl")))

(define-public crate-rp-sys-0.2.0 (c (n "rp-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1l8jwvbnaadfncilikf0y51pg3v8d0iwwx1040yzjkihpr7a1mgb")))

(define-public crate-rp-sys-0.3.0 (c (n "rp-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0n34vhrcp6jj7g11pjrj1j4vxnn4lkw55sbmm9a4vgyf36pjnajb")))

(define-public crate-rp-sys-0.4.0 (c (n "rp-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ybyp95xa444xygp3lc76cbfmd05wpj4ky1r08jpisq06x5rx61l")))

(define-public crate-rp-sys-0.5.0 (c (n "rp-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "045i89qpw4c3ny3lvcnjxlkmi6zw1pdadqmlana9jv1f0srd0bbs")))

(define-public crate-rp-sys-0.6.0 (c (n "rp-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "14xpfb036bcbbvw48vbhz1a06knnbzrhwbpr3av4brbkqjpif6xn")))

(define-public crate-rp-sys-0.7.0 (c (n "rp-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1yxyghfkw0ialr8hl1vrn80im0hpjxx74829214wcrdkj7z1fqfv")))

(define-public crate-rp-sys-0.8.0 (c (n "rp-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0bxsvh6grh6z6023i0kkrj5qqa7fzbgz411w2crd2z8v9pxj2r1i")))

(define-public crate-rp-sys-0.9.0 (c (n "rp-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1j5n73pn7xi3wn2s4gg3ina43i531j4pil9kkdb037crbd1iqir2")))

(define-public crate-rp-sys-0.10.0 (c (n "rp-sys") (v "0.10.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1d39q94yq7piig1z20hvfq0dw0b96nxn5wpkjfsqf2fm5y78j10c")))

(define-public crate-rp-sys-0.11.0 (c (n "rp-sys") (v "0.11.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "02dfz57bvyz354d01g207m3qzpi63nkk0x0341xsnrzvhcjmdwzd")))

(define-public crate-rp-sys-0.12.0 (c (n "rp-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fcdjj3fl3gj76cj8nvncxnq85rivbj83g7205b2s7kj2n6clzi9")))

(define-public crate-rp-sys-0.13.0 (c (n "rp-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "158ggnlghkbb4nsbnh3b3yqj2rwmv4l1ghbx8qld8yxkp8bd883n")))

(define-public crate-rp-sys-0.14.0 (c (n "rp-sys") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jfhjpj7yaylh8gkvfafzw8la3z6xx1kx2y6zacymvmmmmllzb7x")))

(define-public crate-rp-sys-0.15.0 (c (n "rp-sys") (v "0.15.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hc5psvgycs8y7szbmwxn7q5n6aijhvd66jpg18pbfd9zfiq61g5")))

(define-public crate-rp-sys-0.16.0 (c (n "rp-sys") (v "0.16.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sljmnb2bdwnzzymmlzjv8c8rfrgfw6qi8wpqygmndscfv2zvb5m")))

(define-public crate-rp-sys-0.17.0 (c (n "rp-sys") (v "0.17.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1paig1x5q29x346rlcz7hqvcp188wzrgld06w5lfwrls0s9kj63q")))

(define-public crate-rp-sys-0.18.0 (c (n "rp-sys") (v "0.18.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gc763sryllkaxb3kw7f3msy709g90j8jj9gb6s6kl8chxs8qb08")))

(define-public crate-rp-sys-0.19.0 (c (n "rp-sys") (v "0.19.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vl6b6r7hwi61q1x9b2bgw42a698szm4p5fyx0r4qab6xfn0ahj6")))

(define-public crate-rp-sys-0.20.0 (c (n "rp-sys") (v "0.20.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fqyl7vrp1bx0jwv1gn4wfq60lxf803zhli0kcx0gzqj7xwfjwwg")))

(define-public crate-rp-sys-0.21.0 (c (n "rp-sys") (v "0.21.0") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1znd9v4y7zmlk4dr0l60w1ykwkrrg2vycqsfyvwmfq92gylkacws")))

(define-public crate-rp-sys-0.22.0 (c (n "rp-sys") (v "0.22.0") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yh427ap59isv7mzjf1xxfgm9fapbf4fi9a80mlqvxpznqx7yw1b")))

(define-public crate-rp-sys-0.23.0 (c (n "rp-sys") (v "0.23.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "039f72si0wjsnfynpin864aifz7593cchhnd6gqzd703yxd6lgwv")))

(define-public crate-rp-sys-0.24.0 (c (n "rp-sys") (v "0.24.0") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zmaqycnnlw32wma56d7v3x0k8bp2y3zsv5xqrwy2h49gsfyn28l")))

(define-public crate-rp-sys-0.25.0 (c (n "rp-sys") (v "0.25.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nfbvhjfpqpyyghds39vzw6d78bb89kj38qn7mxa9y90fbbqx86p")))

(define-public crate-rp-sys-0.26.0 (c (n "rp-sys") (v "0.26.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cxmnsryj7d45hz7lpnr8kp3klfp7mmfmgm4lrbid9ivxmw12gxr") (f (quote (("mock" "lazy_static") ("default"))))))

(define-public crate-rp-sys-0.27.0 (c (n "rp-sys") (v "0.27.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hm3z1gp5ggv6i0wgjzgdyh76plhkd9c306vnbdm2gq4byz35cqb") (f (quote (("mock" "lazy_static") ("default"))))))

(define-public crate-rp-sys-0.28.0 (c (n "rp-sys") (v "0.28.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0crclrppz4v1k00ld71d8kasgrw8mq9g2h9ag7k8376cn6ww47df") (f (quote (("mock" "lazy_static") ("default"))))))

(define-public crate-rp-sys-0.28.1 (c (n "rp-sys") (v "0.28.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bq8g43zsafyjh9q8snh8kbci30sx05gc7r8jlwxsaj68503h44a") (f (quote (("mock" "lazy_static") ("default"))))))

