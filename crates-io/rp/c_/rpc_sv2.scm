(define-module (crates-io rp c_ rpc_sv2) #:use-module (crates-io))

(define-public crate-rpc_sv2-1.0.0 (c (n "rpc_sv2") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^1.1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc" "raw_value"))) (k 0)) (d (n "stratum-common") (r "^1.0.0") (f (quote ("bitcoin"))) (d #t) (k 0)))) (h "15gr7nrydiyj04q8n4bxkfbs2j6jlzl9yx9y2x6dxppa8dh4c2hx")))

