(define-module (crates-io rp gn rpgn) #:use-module (crates-io))

(define-public crate-rpgn-0.1.0 (c (n "rpgn") (v "0.1.0") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.26.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "12m7xgvmfwhdnixgn93fykdcxp5v27dd2i8043pyslsqj5yyblxl")))

(define-public crate-rpgn-0.2.0 (c (n "rpgn") (v "0.2.0") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.26.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "1yngard6mi8g0zz54n456mqr2w9sdyyddpz58hwxyzirc4x75q49")))

(define-public crate-rpgn-0.2.1 (c (n "rpgn") (v "0.2.1") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.26.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "18dm4wrwrr4xh4mxdclmrrkryfnxnrwpcygz9c53csk2jc3bxgig")))

(define-public crate-rpgn-0.2.3 (c (n "rpgn") (v "0.2.3") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.26.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "12av0p70mz3s740jm02n15mbr69vsmn47cavn3gbvgnn2pzv3519")))

(define-public crate-rpgn-0.2.4 (c (n "rpgn") (v "0.2.4") (d (list (d (n "dry-mods") (r "^0.1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.26.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "shakmaty") (r "^0.27.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "030q8v2jjcm49c2hdv7311vcv1m759a346pc3mi7rx63pmabf7qx")))

