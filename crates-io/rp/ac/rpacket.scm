(define-module (crates-io rp ac rpacket) #:use-module (crates-io))

(define-public crate-rpacket-0.1.0 (c (n "rpacket") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pgp") (r "^0.12.0-alpha.2") (d #t) (k 0)))) (h "1928nij193zjihazhvfmchww4kpk155yqpfzxhlmdiqb5kjii8wg")))

(define-public crate-rpacket-0.1.1 (c (n "rpacket") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pgp") (r "^0.12.0-alpha.2") (d #t) (k 0)))) (h "1jsz3rdnwyh0sr53lapqwzbcnh4izjh4przl7g68db65hmnbihp4")))

