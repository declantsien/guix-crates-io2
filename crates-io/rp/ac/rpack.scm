(define-module (crates-io rp ac rpack) #:use-module (crates-io))

(define-public crate-rpack-0.1.0 (c (n "rpack") (v "0.1.0") (h "0dywaqc1phnzqs70fjya2k6s10sl099f7ivfi8vjbkfdjlxyjyzw")))

(define-public crate-rpack-0.2.0 (c (n "rpack") (v "0.2.0") (h "07wy5ciqmqv4fzssl294jcm68bywvagm0n7bxq436hplm7iggx7k")))

(define-public crate-rpack-0.2.1 (c (n "rpack") (v "0.2.1") (h "1qsdm0idbh81l3lpmqvl0fmyfsi58ljmgfpjlvkdd7fwjf43wv8f")))

(define-public crate-rpack-0.2.2 (c (n "rpack") (v "0.2.2") (h "175gwh0a1c216c2p533kzd853x457zcz2c4k2srv9jhffppz8zfz")))

