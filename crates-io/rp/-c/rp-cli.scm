(define-module (crates-io rp -c rp-cli) #:use-module (crates-io))

(define-public crate-rp-cli-0.0.1 (c (n "rp-cli") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1dsph9rdyxphx5jlncm7glgdjnaihzbch0c6q3wcjlzkxm818vgm")))

(define-public crate-rp-cli-0.0.2 (c (n "rp-cli") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0myvl95i29kscyhpcm1yv7lrccjpyk39xs5y1vkgvj2g1ax7vg1b")))

(define-public crate-rp-cli-0.0.3 (c (n "rp-cli") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1syvcvqhdgbqg2zwrjw5i32rzpw33f7qzg65c7qr9an9iaqga7d3")))

(define-public crate-rp-cli-0.0.4 (c (n "rp-cli") (v "0.0.4") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1nw0v1z01krqd9gc6psp1w7k0z00ph2z4q83w2hbsyfcsy93vcac")))

