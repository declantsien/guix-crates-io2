(define-module (crates-io rp mb rpmbuild) #:use-module (crates-io))

(define-public crate-rpmbuild-0.1.0 (c (n "rpmbuild") (v "0.1.0") (h "1h2n8ckg21950jahwmmyifzm6vnkqi33bdnkgjrigdzapfim9q22") (y #t)))

(define-public crate-rpmbuild-0.0.0 (c (n "rpmbuild") (v "0.0.0") (h "1mzqck06l1l5pyb12ybk10fqsrlz5mp831dqc1zk6kcgnn2wprnq") (y #t)))

