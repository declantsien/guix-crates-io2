(define-module (crates-io rp pa rppal-mcp23s17) #:use-module (crates-io))

(define-public crate-rppal-mcp23s17-0.0.1 (c (n "rppal-mcp23s17") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rppal") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0djrbb3fbji2qr2fdj4qm72kchl2s9g6jsxgv8fbfh869gcffb68") (f (quote (("mockspi"))))))

(define-public crate-rppal-mcp23s17-0.0.2 (c (n "rppal-mcp23s17") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rppal") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0jzjp8md89s5b9pvj8p4mxgw260p1qah3nx0x61ky176ifnhihp0") (f (quote (("mockspi"))))))

(define-public crate-rppal-mcp23s17-0.0.3 (c (n "rppal-mcp23s17") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rppal") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1kxijdr8f1iw23avm0n7p79xh2q877h2cfy62wsp8gdflxwrj4b6") (f (quote (("mockspi"))))))

