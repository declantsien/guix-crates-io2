(define-module (crates-io rp pa rppal-dht11) #:use-module (crates-io))

(define-public crate-rppal-dht11-0.3.1 (c (n "rppal-dht11") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "rppal") (r "^0.13") (f (quote ("hal"))) (d #t) (k 0)))) (h "0knjcz4jzk1chmixyj18w728568f0a5c9n8a538z4qnzbr389n5w")))

(define-public crate-rppal-dht11-0.4.0 (c (n "rppal-dht11") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "rppal") (r "^0.13") (f (quote ("hal"))) (d #t) (k 0)))) (h "0b905b0cznn3fammx9sdzdb6lzywfiifyr06vq93ajyi2bk8ij5x")))

