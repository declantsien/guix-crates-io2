(define-module (crates-io rp pa rppal_w_frontend) #:use-module (crates-io))

(define-public crate-rppal_w_frontend-0.0.1 (c (n "rppal_w_frontend") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1zsyip9pb8dd4x0bgjsrd2frdmwcg55gjk55pcnvfyvj5zv3v0fq") (f (quote (("default"))))))

(define-public crate-rppal_w_frontend-0.0.2 (c (n "rppal_w_frontend") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "11a42ccp9abbr1gwcwpjk9hlnw67h6zybwbhl0dh27pgqj49zxdx") (f (quote (("default"))))))

(define-public crate-rppal_w_frontend-0.0.4 (c (n "rppal_w_frontend") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1xbllxkkiwbm1f69imk7cz05ayj379c7za5j4ivwqkighdfmqvdd") (f (quote (("default"))))))

