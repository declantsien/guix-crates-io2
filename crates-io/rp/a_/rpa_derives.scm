(define-module (crates-io rp a_ rpa_derives) #:use-module (crates-io))

(define-public crate-rpa_derives-0.1.0 (c (n "rpa_derives") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1wqcwimbzmbkrqnyw1byk7wp2a7v51rdr9n9mndd90yra4yh35f5")))

(define-public crate-rpa_derives-0.2.0 (c (n "rpa_derives") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1snf9snvc6hgmhrmxp610aymybxh84fj1h3jk724rig7madkwkak")))

(define-public crate-rpa_derives-0.3.0 (c (n "rpa_derives") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "07clq2xikc755bfz93kcsbq67fqi3b8bcqdbcrk9a4h09sq1aj42")))

(define-public crate-rpa_derives-0.3.1 (c (n "rpa_derives") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1f3kw06sf4l5dmwwz2cnkjm5pwcsn2ngv6lfnashs5vwk7v39ppz")))

(define-public crate-rpa_derives-0.3.2 (c (n "rpa_derives") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1mzngqkbh6h3xlw82g5iyshc8ssnciijyj63ah9kkgxydffsmjfl")))

(define-public crate-rpa_derives-0.4.0 (c (n "rpa_derives") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0s2pcsrmk6nzjp8zn9hlnhxs7w4lskkr8s763sj92lf4rd2mmchq")))

(define-public crate-rpa_derives-0.4.1 (c (n "rpa_derives") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0b6c1py4x14b0gqf24dfr9ck966j6qa9pfvikw4a82ci2vp2x09m")))

(define-public crate-rpa_derives-0.4.2 (c (n "rpa_derives") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1vx7l9mbq0w5w1shq5g4qxp7ynhaqv2h812vk42qn0by2ck40jbn")))

(define-public crate-rpa_derives-0.4.3 (c (n "rpa_derives") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)))) (h "18193q4ylx9dfz0l103dmr7w2pis1n0ksmph1270b0ki4s2f2slf")))

(define-public crate-rpa_derives-0.4.4 (c (n "rpa_derives") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0h346b41kk4qmf3bl7wjpliyj6wz3q7q27jql2b6r6rrbyl09gsh")))

(define-public crate-rpa_derives-0.4.5 (c (n "rpa_derives") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1sd03fm6mrbhlc5294n31b2jnq467izlxqdmhyxwm1fici0w101n")))

(define-public crate-rpa_derives-0.4.6 (c (n "rpa_derives") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "11n1sllzsmyciwalixswvfilqpl949q8zlifkiqwdbmdqkikbhb4")))

(define-public crate-rpa_derives-0.4.7 (c (n "rpa_derives") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0m39lqhdmic3yay5iwwb8xw6za7lzfsr00p6zyx5prqzf1whbm0w")))

(define-public crate-rpa_derives-0.4.8 (c (n "rpa_derives") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "147z8j0h1hsvbmbp1hfmpgfynx68q6n535byg8v08wkdpnys8pk0")))

(define-public crate-rpa_derives-0.5.0 (c (n "rpa_derives") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "14xnhd0a8xw307cdagaw1bjjz5qk8r1xwgj3j1zkxbc7jqmvd2m9")))

(define-public crate-rpa_derives-0.5.1 (c (n "rpa_derives") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1m5c0rm4prvlnzs03pa5kyx94mcybvbj64vr4ifn0z12sldg4s56")))

