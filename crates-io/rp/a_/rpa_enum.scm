(define-module (crates-io rp a_ rpa_enum) #:use-module (crates-io))

(define-public crate-rpa_enum-0.1.0 (c (n "rpa_enum") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "11niran47286j45p8hp2y1pd2arx94li88a7pa9g7233230gj3zi")))

(define-public crate-rpa_enum-0.1.1 (c (n "rpa_enum") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1fj8kgcdgwrvbhx765xhs0gn0lnckfs3ws6msb04hv047sz0nj9r")))

(define-public crate-rpa_enum-0.1.2 (c (n "rpa_enum") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "type-info") (r "^0.2.1") (d #t) (k 0)) (d (n "type-info-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0rw6531whmqlc7zb911dd52bw1j2nxsx0kr0w1n3bymgbna6s260")))

(define-public crate-rpa_enum-0.1.3 (c (n "rpa_enum") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0nxr8fp2ljw2pmzzkgnvps5nz0lp44p2r3pi256b3r66lryzq4b6")))

(define-public crate-rpa_enum-0.1.4 (c (n "rpa_enum") (v "0.1.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1p3qwjs4w0mrfai4g597gdpl4fql0j0bmimka4mb7i3lbykhrhs4")))

(define-public crate-rpa_enum-0.1.5 (c (n "rpa_enum") (v "0.1.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0vr97nm7divq348f03l7was1pg3d5r1jr0fxcwp3mqk8wwg29cgn")))

(define-public crate-rpa_enum-0.1.6 (c (n "rpa_enum") (v "0.1.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "17lk3fcx4csa90bqqkr729hwn53383qkwk8lp0rl05q3d7sg92in")))

