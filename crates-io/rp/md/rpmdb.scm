(define-module (crates-io rp md rpmdb) #:use-module (crates-io))

(define-public crate-rpmdb-0.1.0 (c (n "rpmdb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("fs"))) (t "cfg(unix)") (k 0)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1qx4mzfcry94q1dk5pc6dhphb0dmki70b0pvfph0shgx6a3vdss3")))

