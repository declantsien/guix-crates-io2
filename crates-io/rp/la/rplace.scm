(define-module (crates-io rp la rplace) #:use-module (crates-io))

(define-public crate-rplace-0.1.0 (c (n "rplace") (v "0.1.0") (h "00k44l22s353mj29dmfp2ln3yqwxyb9qf6w73ixfa2c7x5zcb5k4")))

(define-public crate-rplace-0.2.0 (c (n "rplace") (v "0.2.0") (h "12dhb5j2qyim4qsq5nismdpsn0vj5ph5qyhmjr06czd65357cypf")))

