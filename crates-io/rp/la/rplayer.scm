(define-module (crates-io rp la rplayer) #:use-module (crates-io))

(define-public crate-rplayer-0.1.0 (c (n "rplayer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)))) (h "1lnfdakaspb3rspsq3a1kzp47mxq9wyv9kawd729xmlclr7yc5zd")))

(define-public crate-rplayer-0.1.1 (c (n "rplayer") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rodio") (r "^0.13.1") (d #t) (k 0)))) (h "17xcsznxy5kw6angw5zqbr1i0m0482ggn1r7dqszyzql71ag2byg")))

