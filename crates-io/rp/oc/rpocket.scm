(define-module (crates-io rp oc rpocket) #:use-module (crates-io))

(define-public crate-rpocket-0.1.0 (c (n "rpocket") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (f (quote ("util"))) (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "mockito") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1421y23mrb3077kc4fky99ra27lf3hgpdlg9jcjpqvls7xq5xfxr") (f (quote (("multipart" "reqwest/multipart") ("default"))))))

