(define-module (crates-io rp lt rpltree) #:use-module (crates-io))

(define-public crate-rpltree-0.1.0 (c (n "rpltree") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rtshark") (r "^2.3.1") (d #t) (k 0)) (d (n "termtree") (r "^0.4.0") (d #t) (k 0)))) (h "1z2qzjspnw2652lyxcjnf87jx1xwh7b6x33qqq08p2qabqz1q7gl")))

