(define-module (crates-io rp ms rpmsign) #:use-module (crates-io))

(define-public crate-rpmsign-0.0.0 (c (n "rpmsign") (v "0.0.0") (h "1fr23slshhpmnf35v81zg2p9gww1bsbs8bqyf4hjnnlfap44z9bq") (y #t)))

(define-public crate-rpmsign-0.0.1 (c (n "rpmsign") (v "0.0.1") (h "1vml7cjhnhcffq6kdx07v6cvdhj94fqk9mm7l0x1g9iyr0132qhn") (y #t)))

