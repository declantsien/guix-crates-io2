(define-module (crates-io rp ms rpmspec) #:use-module (crates-io))

(define-public crate-rpmspec-0.1.0 (c (n "rpmspec") (v "0.1.0") (h "0ws0zdlz75giwid3npxb51c08jkv06mhwlkbhjzz0fky5kx0q9lf") (y #t)))

(define-public crate-rpmspec-0.2.0 (c (n "rpmspec") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "sailfish") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0f58l5xaavmcjlhhnpbw1nmpbv9dpcfvckk9x35aq7bhqjp06czn") (y #t)))

(define-public crate-rpmspec-0.3.0 (c (n "rpmspec") (v "0.3.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pkgspec") (r "^0.2") (d #t) (k 0)))) (h "0yblqc3ldzyva7xiv20varxgc087830sjpjiyfsgl144833ab3ai") (y #t)))

