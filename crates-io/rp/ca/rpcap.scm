(define-module (crates-io rp ca rpcap) #:use-module (crates-io))

(define-public crate-rpcap-0.2.0 (c (n "rpcap") (v "0.2.0") (d (list (d (n "bytepack") (r "^0.4.1") (d #t) (k 0)) (d (n "bytepack_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "14q0xjmvbky51nn9mk59wvpi2f506fnaf30hijfr16hhk4ds4h93")))

(define-public crate-rpcap-0.2.1 (c (n "rpcap") (v "0.2.1") (d (list (d (n "bytepack") (r "^0.4.1") (d #t) (k 0)) (d (n "bytepack_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1ps8qwh7c7v5s2l8i4fckw12rgkx6f5vg9xp678lpgdnwhwijmna")))

(define-public crate-rpcap-0.2.2 (c (n "rpcap") (v "0.2.2") (d (list (d (n "bytepack") (r "^0.4.1") (d #t) (k 0)) (d (n "bytepack_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1n8gx36m6606855dsski0wszfpcl68xn09rxa046cqgl0sipma4d")))

(define-public crate-rpcap-0.3.0 (c (n "rpcap") (v "0.3.0") (d (list (d (n "bytepack") (r "^0.4.1") (d #t) (k 0)) (d (n "bytepack_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "0jkfyfs4cmwizlhc8kn271c3gq5zib3j7rmc74b6c3rbmbs5pw04")))

(define-public crate-rpcap-1.0.0 (c (n "rpcap") (v "1.0.0") (d (list (d (n "bytepack") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1flrr069fkagcr8s94njlpsndd9r4bws6549myxhynzq1awvqj3l") (f (quote (("default"))))))

