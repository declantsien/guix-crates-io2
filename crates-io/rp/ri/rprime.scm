(define-module (crates-io rp ri rprime) #:use-module (crates-io))

(define-public crate-rprime-0.1.0 (c (n "rprime") (v "0.1.0") (h "1gs9cpjp5gbsln9c11my0cibg59zyj2ng6gjgbgvbdbbchdfsack")))

(define-public crate-rprime-1.0.0 (c (n "rprime") (v "1.0.0") (h "13pzgq2pz5in2qzfns25hrzza5dn59nrd9lcby62grqld33wcv4i")))

