(define-module (crates-io rp n_ rpn_calc_perl) #:use-module (crates-io))

(define-public crate-rpn_calc_perl-0.1.0 (c (n "rpn_calc_perl") (v "0.1.0") (h "1plil5mfcwv9flqgkxnm35mznnr3rk2hgzsnkgav300vn7nd8n7f")))

(define-public crate-rpn_calc_perl-0.1.1 (c (n "rpn_calc_perl") (v "0.1.1") (h "1pcbc87hsfphk31hxayfb1cx3mzf8hgnaljb1d9rxp8v98skb67h")))

