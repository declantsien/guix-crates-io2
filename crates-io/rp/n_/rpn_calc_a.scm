(define-module (crates-io rp n_ rpn_calc_a) #:use-module (crates-io))

(define-public crate-rpn_calc_a-0.1.0 (c (n "rpn_calc_a") (v "0.1.0") (h "0ahs82d9v07cb88x2ms29p8g7an0wyhlpnxvhf93z9sqn4m4h2ps") (y #t)))

(define-public crate-rpn_calc_a-0.1.1 (c (n "rpn_calc_a") (v "0.1.1") (h "038cwnpmy547drh6gha2fdlf00z7dx9x8c635a929wa2hkb2w7w1") (y #t)))

(define-public crate-rpn_calc_a-0.1.2 (c (n "rpn_calc_a") (v "0.1.2") (h "0nm7drxkf6xkvca5g9pna57w14a1gnjqf4rzwaqpycs4vmxn78yy") (y #t)))

