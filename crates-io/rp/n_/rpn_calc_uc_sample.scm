(define-module (crates-io rp n_ rpn_calc_uc_sample) #:use-module (crates-io))

(define-public crate-rpn_calc_uc_sample-0.1.0 (c (n "rpn_calc_uc_sample") (v "0.1.0") (h "1wmlxzp5a7p7qyhzvcw8azx6v579fc6ndgpf9x16zqk980vr0hy0") (y #t)))

(define-public crate-rpn_calc_uc_sample-0.1.1 (c (n "rpn_calc_uc_sample") (v "0.1.1") (h "1cms5aiy99zrrjrj92wqvcjipcb2gxp9pabx6wwpmv6wa2azavm9")))

(define-public crate-rpn_calc_uc_sample-0.1.2 (c (n "rpn_calc_uc_sample") (v "0.1.2") (h "1pcln2cazxbq886agf6h9c7ic8s7z4hxr434pirla7v2160ilizd")))

