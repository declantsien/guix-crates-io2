(define-module (crates-io rp n_ rpn_calc) #:use-module (crates-io))

(define-public crate-rpn_calc-0.1.0 (c (n "rpn_calc") (v "0.1.0") (h "1gz5wlxvsqg3m61hvsjhzifz5g45h9zblkckk8x61f555k2kv20j")))

(define-public crate-rpn_calc-0.1.1 (c (n "rpn_calc") (v "0.1.1") (h "1c6qfafjal2nxvv8l32dm0f03rj1cgzldijwvdbdppxamgfl0k1s")))

