(define-module (crates-io rp n_ rpn_lib) #:use-module (crates-io))

(define-public crate-rpn_lib-0.1.0 (c (n "rpn_lib") (v "0.1.0") (h "1ybdjg7v5ilqkcbhximj6jq0davdxwr01sjxazdcfas9am8vga8c")))

(define-public crate-rpn_lib-0.1.1 (c (n "rpn_lib") (v "0.1.1") (h "15sidbyjiqsnyfxdc4zkxfp7dk8gy1s022dw7z6f5bppgx4pj48b")))

(define-public crate-rpn_lib-0.1.2 (c (n "rpn_lib") (v "0.1.2") (h "02164jhlql3ravmwrin3n07wryn9pbik6ys0cxdw482hxpjp19ql")))

(define-public crate-rpn_lib-0.1.3 (c (n "rpn_lib") (v "0.1.3") (h "1lcm6i3p1l2v1aacwlx662bpdzj8d9cankggnl613ad1vi6w3ags")))

(define-public crate-rpn_lib-0.1.4 (c (n "rpn_lib") (v "0.1.4") (h "12kkskcbh9w9fl82l6l2lcjlj392sm7y7y0vggajlhnpi28p9wmb")))

(define-public crate-rpn_lib-0.1.5 (c (n "rpn_lib") (v "0.1.5") (h "06yh5nc8wz2nfn8b03q5k89bgxx10byhhl07iy0d4gyckgfsdg8j")))

(define-public crate-rpn_lib-0.1.6 (c (n "rpn_lib") (v "0.1.6") (h "1kf49yy5p1m2sw44kqp010knznpi2r78prxnx6a5n8z0528rzvl2")))

