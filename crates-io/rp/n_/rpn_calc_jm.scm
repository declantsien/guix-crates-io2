(define-module (crates-io rp n_ rpn_calc_jm) #:use-module (crates-io))

(define-public crate-rpn_calc_JM-0.1.0 (c (n "rpn_calc_JM") (v "0.1.0") (h "0jxb3g4xys0qw1l8mb29plgivrz744c0kc6q4nisgq3rpgfnawyr")))

(define-public crate-rpn_calc_JM-0.1.1 (c (n "rpn_calc_JM") (v "0.1.1") (h "00prywr7d6lvgkb1vb14f2vmsl1gc1x2j06fvz783mk704d6n7yc")))

