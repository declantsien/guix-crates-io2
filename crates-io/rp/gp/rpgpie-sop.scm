(define-module (crates-io rp gp rpgpie-sop) #:use-module (crates-io))

(define-public crate-rpgpie-sop-0.1.0 (c (n "rpgpie-sop") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pgp") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rpgpie") (r "^0.0.1") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "0m29lkrcbqmwh92h61aqzsaxypsx9ydrwaf5jbrm7m9way1qnlg9")))

(define-public crate-rpgpie-sop-0.2.0 (c (n "rpgpie-sop") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pgp") (r "^0.12.0-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rpgpie") (r "^0.0.2") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "1czzfvz7plicj0v71ppapw0y4g40sh6bnb6h27mm51rqlrpfd24b")))

(define-public crate-rpgpie-sop-0.2.1 (c (n "rpgpie-sop") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pgp") (r "^0.12.0-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rpgpie") (r "^0.0.4") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "0rbn60kc6rc8v1csfn4faydr0vbr5pj2gfcqqp4l75nl9cdpmcgf")))

(define-public crate-rpgpie-sop-0.3.0 (c (n "rpgpie-sop") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pgp") (r "^0.12.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rpgpie") (r "^0.0.5") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "1ipf6kr19k75ysfs2y4d9grym2f9var1br2m3g76vmxdyawf84rv")))

(define-public crate-rpgpie-sop-0.3.1 (c (n "rpgpie-sop") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "pgp") (r "^0.12.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "rpgpie") (r "^0.0.8") (d #t) (k 0)) (d (n "sop") (r "^0.7") (d #t) (k 0)))) (h "09v5vyb3w0bgcdk1hrw76g9xjhzccdh1j92cj3ldrkv34jag3rqc")))

