(define-module (crates-io rp s- rps-sys) #:use-module (crates-io))

(define-public crate-rps-sys-0.1.0 (c (n "rps-sys") (v "0.1.0") (h "0a4gawcvg87f7ica99vk220kdn8arzhvqilfqcdx4q6l51qz3lfn")))

(define-public crate-rps-sys-0.2.0 (c (n "rps-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0rnfwd88pw35r5fd6ij6r5qhrbhhcjcp7s6p0v13i6125pwryhyy")))

(define-public crate-rps-sys-0.3.0 (c (n "rps-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ysvmiy4v9i7xwk3jw6gjz46800c2jl6bnfc5mwk49cxzvycwd7z") (f (quote (("vk") ("d3d12") ("d3d11")))) (l "rps")))

(define-public crate-rps-sys-0.4.0 (c (n "rps-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18p7i2g52mmr1ks387lyikrxwmcb0hzlvibsnwzx86jj3149hrw2") (f (quote (("vk") ("d3d12") ("d3d11")))) (l "rps")))

