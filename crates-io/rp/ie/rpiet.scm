(define-module (crates-io rp ie rpiet) #:use-module (crates-io))

(define-public crate-rpiet-0.1.0 (c (n "rpiet") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "png") (r "~0.15.0") (d #t) (k 0)))) (h "13yr7hgvgzh9j9jl2lwgibi5h61q0qhfbl8w6v9bzldg929fy76p")))

(define-public crate-rpiet-0.2.0 (c (n "rpiet") (v "0.2.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "png") (r "~0.15.0") (d #t) (k 0)))) (h "00cgh6c6v6sdkk3z7qfvszd270lhlfrh0zb9685vnwygh8793r3g")))

(define-public crate-rpiet-0.3.0 (c (n "rpiet") (v "0.3.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "gif") (r "~0.10.3") (d #t) (k 0)) (d (n "png") (r "~0.15.0") (d #t) (k 0)))) (h "0bpsniw9sd0ymzm6qk9cmdxpi844qdhr3xdrcfrhbb2g908d00yv")))

