(define-module (crates-io rp -p rp-pac) #:use-module (crates-io))

(define-public crate-rp-pac-0.0.0 (c (n "rp-pac") (v "0.0.0") (h "1rpw6lz3zav1fhxj51fs9sp6f1lcyygma27lrl8dbyss2vr3xr53")))

(define-public crate-rp-pac-1.0.0 (c (n "rp-pac") (v "1.0.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)))) (h "14hnjfpjmhsj1b6girmdhrqlvyq4cbmcv60d7w5c43n994pd2a0d") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp-pac-2.0.0 (c (n "rp-pac") (v "2.0.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)))) (h "09y98s9977q4ynkqg718w5fk1751l5s6niz094zdg2rlkbqlppkm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp-pac-3.0.0 (c (n "rp-pac") (v "3.0.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)))) (h "1n2i9qyfyviwfrjnwjjlpx6dp2dmgmwxdv2cad49gjw9gkbbk135") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp-pac-4.0.0 (c (n "rp-pac") (v "4.0.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)))) (h "0599dh3qppkqsjf0b4gggimi52vq5188izm1zdlbcz9pv1n44vm7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp-pac-5.0.0 (c (n "rp-pac") (v "5.0.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)))) (h "0606ynb4zvcm57066xb9cwsf91srkdfwnk67f76jsjaz1a3nzp9k") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-rp-pac-6.0.0 (c (n "rp-pac") (v "6.0.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)))) (h "0dskfvinh22gyh6mb05ljlrr4yzzciwcgs85p0yjjsb2hi66q3zk") (f (quote (("rt" "cortex-m-rt/device"))))))

