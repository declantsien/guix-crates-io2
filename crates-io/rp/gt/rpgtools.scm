(define-module (crates-io rp gt rpgtools) #:use-module (crates-io))

(define-public crate-rpgtools-0.2.0-alpha (c (n "rpgtools") (v "0.2.0-alpha") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "01izbj9mmbppzl4k3gm1h8cqw91z3sjllrg8xzfzf8b221rbmmlb")))

(define-public crate-rpgtools-1.0.0 (c (n "rpgtools") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0fbfxf02ac6c08x6m8yksj2vr6nd0jqgfwxgnnydg266jw4widhb")))

(define-public crate-rpgtools-1.0.1 (c (n "rpgtools") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "17v12qf9bc8wqli8bpg2gyj9dg6h3j6shlwr3wfw71858hvhspck")))

(define-public crate-rpgtools-1.1.0 (c (n "rpgtools") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "19p2jigysqk4pf1v9xw3g677447vcwjzyaiv62jaiaxf34lcrwvz")))

(define-public crate-rpgtools-1.2.0 (c (n "rpgtools") (v "1.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "resvg") (r "^0.15") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5") (d #t) (k 0)) (d (n "usvg") (r "^0.15") (d #t) (k 0)))) (h "1hsgh3gsmp38bnmvjzc4261395005m4v9z3qqisnq7qnix994vi9")))

(define-public crate-rpgtools-1.2.2 (c (n "rpgtools") (v "1.2.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "resvg") (r "^0.23.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.6.6") (d #t) (k 0)) (d (n "usvg") (r "^0.23.0") (d #t) (k 0)))) (h "03vk91m7i8wak4f8fz89d1q9vj3p5x97kia1js48mq2wihm13dw7")))

(define-public crate-rpgtools-1.3.0 (c (n "rpgtools") (v "1.3.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.8.2") (d #t) (k 0)) (d (n "usvg") (r "^0.28.0") (d #t) (k 0)))) (h "01hhd0mqg5jyvmd84aa2bffzgni8kq9522np7hlkwi6avfm2r4vi")))

