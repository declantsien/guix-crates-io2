(define-module (crates-io rp lo rplotlib) #:use-module (crates-io))

(define-public crate-rplotlib-0.1.0 (c (n "rplotlib") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1wmgyz0a2v5l750yiyh3g0z6qs0yl6d4wsrj763738sn54w0pmc4")))

