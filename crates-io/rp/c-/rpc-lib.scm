(define-module (crates-io rp c- rpc-lib) #:use-module (crates-io))

(define-public crate-rpc-lib-0.1.0 (c (n "rpc-lib") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)))) (h "0xmpmqjb2isxaw9yv9jrfm37qg3kfhpwfhqiqcm2hjhbk3xl33rc")))

(define-public crate-rpc-lib-0.2.0 (c (n "rpc-lib") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)))) (h "117p8nl38j8618dgpdsvk0z8v5z0cri7vr738lljd9p3i4x9bg0b")))

(define-public crate-rpc-lib-0.3.0 (c (n "rpc-lib") (v "0.3.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0k672s2679a0p5bh8iqi4qffdq4qnwxjmpb90g9awg0qvld633c4")))

