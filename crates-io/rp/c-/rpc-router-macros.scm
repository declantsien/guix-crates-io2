(define-module (crates-io rp c- rpc-router-macros) #:use-module (crates-io))

(define-public crate-rpc-router-macros-0.0.2 (c (n "rpc-router-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gcbf3yzk8dddp9z36jkwvn5xaar0n8phwgac7gyk8bwkf19z1m1")))

(define-public crate-rpc-router-macros-0.0.3 (c (n "rpc-router-macros") (v "0.0.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a34wqa0qxrjkw7lpdjh8harrxdvw2khklbpsfpmzps9pxb9xfmn")))

(define-public crate-rpc-router-macros-0.1.0-rc.1 (c (n "rpc-router-macros") (v "0.1.0-rc.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "17jncg9ciya8wy4v6byix3d33qb9gdkk9p3zi34crbzjg80np5ng")))

(define-public crate-rpc-router-macros-0.1.0 (c (n "rpc-router-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1843nfcf83dzdwb3yxm55xp1a7kacv2shdgb2n3jpx5lr2q8mj")))

