(define-module (crates-io rp c- rpc-toy) #:use-module (crates-io))

(define-public crate-rpc-toy-0.1.0 (c (n "rpc-toy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ak6p8s7as9igaip2ny5s3byjga8dd0mj1yiyfbm0xbapjhzbn5z")))

(define-public crate-rpc-toy-0.1.1 (c (n "rpc-toy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1faykvjw86bgnsrp5q2nbw93xfcqr11s7h137dvhd5hwfwa4v7xz")))

(define-public crate-rpc-toy-0.1.2 (c (n "rpc-toy") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sayxyzipl53ln22q8a2p8bdfp15i44chgdja24w5n7nsj5r2kjc")))

