(define-module (crates-io rp c- rpc-core-net) #:use-module (crates-io))

(define-public crate-rpc-core-net-0.1.0 (c (n "rpc-core-net") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1zmg1n32g6r693v262sqsm7vnlcgr0l50m4q0427dslfrd6hgcpg") (r "1.56")))

(define-public crate-rpc-core-net-0.2.0 (c (n "rpc-core-net") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpc-core") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1xhqxrlzmkwnbcf9vgkhxk1r0gwlybzq7g905jmx2cmwmam25vfr") (r "1.56")))

(define-public crate-rpc-core-net-0.2.1 (c (n "rpc-core-net") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpc-core") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1if8h6xf0nwmpp7yg9yndi2nc4pcd3x8ycd8g6k751lgay8lakk4") (r "1.56")))

(define-public crate-rpc-core-net-0.2.2 (c (n "rpc-core-net") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpc-core") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "0aqd8xmj1q44866x2dlgfpz9ysf6ajdzlhz9d5jizhjzd3g6baym") (r "1.56")))

