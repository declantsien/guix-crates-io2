(define-module (crates-io rp c- rpc-toolkit-macro-internals) #:use-module (crates-io))

(define-public crate-rpc-toolkit-macro-internals-0.2.0 (c (n "rpc-toolkit-macro-internals") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1rq22wzrcpmmgcyf9qgxf9xz0x0mdyglxvy1x8m0mqjg5p7blmxa")))

(define-public crate-rpc-toolkit-macro-internals-0.2.1 (c (n "rpc-toolkit-macro-internals") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "10m2l01pi9q3bbhcwawsr3843ffaa5g2b1wdsk5fwaraw2xf5adj")))

(define-public crate-rpc-toolkit-macro-internals-0.2.2 (c (n "rpc-toolkit-macro-internals") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0hjznxcnbba5h2jm26f1ris479nwp5sqx6lwmg6sxzinp4hwxqnk")))

