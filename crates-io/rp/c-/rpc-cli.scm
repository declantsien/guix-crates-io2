(define-module (crates-io rp c- rpc-cli) #:use-module (crates-io))

(define-public crate-rpc-cli-0.1.0 (c (n "rpc-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rpc-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)))) (h "1n92xi240hr2p9iw29mxd0mkn92n7yv82kf92mly9lhwsyb41vnp")))

(define-public crate-rpc-cli-0.2.0 (c (n "rpc-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rpc-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)))) (h "0swvmsbhvqgx4r1yfs9ml82k9x4s7fcyw4k7mj4ldkcsyicc07pn")))

(define-public crate-rpc-cli-0.3.0 (c (n "rpc-cli") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "rpc-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0q1rrqz2d903j84jbs2c8bgaf3k6m0f7s296588rh9mm95k9qdvj")))

