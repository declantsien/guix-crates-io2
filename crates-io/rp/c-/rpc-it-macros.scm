(define-module (crates-io rp c- rpc-it-macros) #:use-module (crates-io))

(define-public crate-rpc-it-macros-0.0.0 (c (n "rpc-it-macros") (v "0.0.0") (h "1w9v5b54f7ld6amha160nr758795c8gqa8vrxs4987j76gjix82b")))

(define-public crate-rpc-it-macros-0.1.0 (c (n "rpc-it-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "06f5qpz6sblxp50nfk6kpry0dpgxkj4wk0xa1b5b4pzy4qq948xm") (y #t)))

(define-public crate-rpc-it-macros-0.1.1 (c (n "rpc-it-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0lrhv58hh5lb1ggfflkmcmbpw8d0n9l5yv44a8bm1w23asacwxad") (y #t)))

(define-public crate-rpc-it-macros-0.1.2 (c (n "rpc-it-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "08rxb7njp3057imjjs8618zk4yg2j8n6zfy91kr9000gh8sn203h") (y #t)))

(define-public crate-rpc-it-macros-0.1.3 (c (n "rpc-it-macros") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0i84bmw19b3y3skpy7i67x1pxxg9kry7qaiblxr3pjh64cpww78y") (y #t)))

(define-public crate-rpc-it-macros-0.1.4 (c (n "rpc-it-macros") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1n98kn524di23gak2izi5cjx4rfdhjxgp03352kfffcjvjr4wvmv")))

(define-public crate-rpc-it-macros-0.2.0 (c (n "rpc-it-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "04sff4mvcn4wic0y1pnjgagi977s9mlfl7v4lbql5b1pm356sabi")))

(define-public crate-rpc-it-macros-0.2.1 (c (n "rpc-it-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1baf8r5iynq9yah5qxmnw204kphb9j79wlnzln5fx1khqjagcvq2")))

(define-public crate-rpc-it-macros-0.2.3 (c (n "rpc-it-macros") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "11985y0hpr4x2w63k4di0dbh2fyx9fwpd7ffmvjs8cqjv4cfr0xn")))

(define-public crate-rpc-it-macros-0.10.0-alpha.1 (c (n "rpc-it-macros") (v "0.10.0-alpha.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0b41dm3jpfsx4qw6vw6cybp4nrr5rmh12rsy1vzbl6xh414y7idz") (f (quote (("extra-traits" "syn/extra-traits") ("default"))))))

