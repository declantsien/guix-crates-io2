(define-module (crates-io rp ls rpls) #:use-module (crates-io))

(define-public crate-rpls-0.1.0 (c (n "rpls") (v "0.1.0") (h "1fd7xm24vl3x7jh2fczr73ipa38ykjnvk6xzwlj53iiq3p4dwh5w") (y #t)))

(define-public crate-rpls-0.1.1 (c (n "rpls") (v "0.1.1") (h "1q9yxb3gxkzbyxy1x7ph9062vh2635g6b3j6jjhbfg8g9jp4lxn7") (y #t)))

(define-public crate-rpls-0.1.2 (c (n "rpls") (v "0.1.2") (h "0bza85qwqqghs08srcdgd6mi16cb0p5p17y11divgs0slnlr38bh")))

