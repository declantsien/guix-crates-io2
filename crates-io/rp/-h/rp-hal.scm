(define-module (crates-io rp -h rp-hal) #:use-module (crates-io))

(define-public crate-rp-hal-0.0.1 (c (n "rp-hal") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "rp2040") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1iw8l4mvyb31jg82d9vxdj45c819lwi303xfb4ack7x3j8jgcd16") (f (quote (("2040" "rp2040"))))))

(define-public crate-rp-hal-0.0.2 (c (n "rp-hal") (v "0.0.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "rp2040") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0a3bczljk51gmx9fzls64fln77fwn3skv7gv5rv6l7xsgjj74j9w") (f (quote (("2040" "rp2040"))))))

