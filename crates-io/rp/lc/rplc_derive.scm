(define-module (crates-io rp lc rplc_derive) #:use-module (crates-io))

(define-public crate-rplc_derive-0.1.0 (c (n "rplc_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0mi5ijrqi38j9ivmnsivbg4qb7dnfac54rxlkn80459agf1plm0j")))

(define-public crate-rplc_derive-0.3.0 (c (n "rplc_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0296a08y71kxi7i3bj38cl2y49mwg7r4r692xj2f5g4pdf0ycwk9")))

(define-public crate-rplc_derive-0.3.1 (c (n "rplc_derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.14.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1a37hb962ii2p6xrpdf6bwd5prprr55g81k8xxc6a7kigry4q5ba")))

