(define-module (crates-io rp ar rparse) #:use-module (crates-io))

(define-public crate-rparse-0.1.0 (c (n "rparse") (v "0.1.0") (h "1srll04i2zdx1x1blz54r2xj3ml3dxgpv1a1y5yqr1w5lahw2hdv")))

(define-public crate-rparse-0.1.1 (c (n "rparse") (v "0.1.1") (h "1ckr4qnkj4x06qina6n6hppnw4srgzsb8p7m001ayjrlmymb2v26")))

