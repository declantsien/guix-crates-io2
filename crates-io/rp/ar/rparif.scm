(define-module (crates-io rp ar rparif) #:use-module (crates-io))

(define-public crate-rparif-0.1.0 (c (n "rparif") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "httpmock") (r "^0.3") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1kg57crmvw2pwj46yzp5s5aha7iy6ag8y5y0nfqfr2z25hp89sh4")))

(define-public crate-rparif-0.1.1 (c (n "rparif") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "httpmock") (r "^0.6") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1y23k1vhyxqwycryksfs9pv2w8087lg14ppd9jnql2b0gwp8ixai")))

