(define-module (crates-io rp lu rplugin) #:use-module (crates-io))

(define-public crate-rplugin-0.1.0 (c (n "rplugin") (v "0.1.0") (h "181lw80apdll0knw0m76jm1hq8jgph9wcg38qb8cj8vvklril4nr")))

(define-public crate-rplugin-0.2.0 (c (n "rplugin") (v "0.2.0") (d (list (d (n "abi_stable") (r "^0.10.3") (d #t) (k 0)) (d (n "rplugin_macros") (r "^0.1") (d #t) (k 0)) (d (n "string_cache") (r "^0.8.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.14.6") (f (quote ("plugin-base"))) (d #t) (k 0)))) (h "1pyi63pzxbn81ffixnrnk8hsadp5zgmpz4l2yz5v3wwlwkvz5k0k")))

(define-public crate-rplugin-0.2.1 (c (n "rplugin") (v "0.2.1") (d (list (d (n "abi_stable") (r "^0.10.3") (d #t) (k 0)) (d (n "rplugin_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "string_cache") (r "^0.8.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.14.6") (f (quote ("plugin-base"))) (d #t) (k 0)))) (h "1fjxp87imc1wcjkpl9nwjsxy5rif9gcvlh3ir5m1fhghq0xql1fh")))

(define-public crate-rplugin-0.3.0 (c (n "rplugin") (v "0.3.0") (d (list (d (n "abi_stable") (r "^0.10.3") (d #t) (k 0)) (d (n "rplugin_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "string_cache") (r "^0.8.2") (d #t) (k 0)) (d (n "swc_common") (r "^0.15.0") (f (quote ("plugin-base"))) (d #t) (k 0)))) (h "0arvyy3r1ad5m16y3h4613f8gpsb89m2dh2w245yzv3mjzqdp1dn")))

