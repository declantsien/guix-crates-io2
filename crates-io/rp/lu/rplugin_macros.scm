(define-module (crates-io rp lu rplugin_macros) #:use-module (crates-io))

(define-public crate-rplugin_macros-0.1.0 (c (n "rplugin_macros") (v "0.1.0") (h "0g62qbg20ywjirq1q7vv5fbxqcxzbi74n4lpx8irdc747s3xhrrg")))

(define-public crate-rplugin_macros-0.1.1 (c (n "rplugin_macros") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zyxg8qlglv39nq0wv4sr47b751ix6dbxagrjyvqfv7hwd25dwpg")))

