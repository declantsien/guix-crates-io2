(define-module (crates-io rp ak rpak) #:use-module (crates-io))

(define-public crate-rpak-0.1.0 (c (n "rpak") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "19rl4m1vsvm70gk2fx2pbvgl88n1vwz6xv0qcn43a78gqjx59mjc") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-rpak-0.2.0 (c (n "rpak") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)))) (h "1jyf7chw6wfmhvyyn40bmka93v5difild118rmwy4v1ixn7xr6yl") (f (quote (("std" "byteorder/std") ("default" "std"))))))

