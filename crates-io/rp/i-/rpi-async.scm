(define-module (crates-io rp i- rpi-async) #:use-module (crates-io))

(define-public crate-rpi-async-0.1.0 (c (n "rpi-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("time"))) (d #t) (k 0)))) (h "03k0njk53hpfdvnyv6ahxkh5700cvaq8irv948b02s0h12n9y158")))

