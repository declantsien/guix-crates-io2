(define-module (crates-io rp i- rpi-st7789v2-driver) #:use-module (crates-io))

(define-public crate-rpi-st7789v2-driver-0.1.0 (c (n "rpi-st7789v2-driver") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)) (d (n "rppal") (r "^0.17.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("attributes"))) (d #t) (k 0)))) (h "1dfprpn2fh94p9a4gy3zbks9m36g0jyr4s9vx6psni9a4w6ym9sw") (s 2) (e (quote (("miette" "dep:miette")))) (r "1.76.0")))

