(define-module (crates-io rp i- rpi-derive-key) #:use-module (crates-io))

(define-public crate-rpi-derive-key-0.1.0 (c (n "rpi-derive-key") (v "0.1.0") (d (list (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "096c9kbcbadfbcdmzk0psdln2bgy015aqfj8rnlwr2f0fpz1bym8") (y #t)))

(define-public crate-rpi-derive-key-0.2.0 (c (n "rpi-derive-key") (v "0.2.0") (d (list (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0ij5fahsykscfq5xzkrpb6qpqrrq65wi0kb6n72480zsgyb4d21y") (r "1.65")))

(define-public crate-rpi-derive-key-0.2.1 (c (n "rpi-derive-key") (v "0.2.1") (d (list (d (n "hkdf") (r "^0.12") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0gp74xwkfvppzr6ha63xhnd68ys1ikzmipzwb4dyfz3mj6z4gs8g") (r "1.65")))

