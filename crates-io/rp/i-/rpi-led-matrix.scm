(define-module (crates-io rp i- rpi-led-matrix) #:use-module (crates-io))

(define-public crate-rpi-led-matrix-0.1.0 (c (n "rpi-led-matrix") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1955rcc3ml8kz5b614dm16g5jgs2flkxc8n3iq09zirgl6zsqc4g")))

(define-public crate-rpi-led-matrix-0.1.1 (c (n "rpi-led-matrix") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qwwwxh6986qvhbd2m8dzwrnqhls2qgqi4v7h5s19v2zg478v4gh")))

(define-public crate-rpi-led-matrix-0.1.2 (c (n "rpi-led-matrix") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i8ymmrszwd2lpqhxawg01jjqi8kn4mx2rm68rkm4x7s7z810k5a")))

(define-public crate-rpi-led-matrix-0.1.3 (c (n "rpi-led-matrix") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pgb7fsy0g1dybnqgf1lqs49f5ynij3y3vwvg5lbaxicscd66f4h")))

(define-public crate-rpi-led-matrix-0.1.4 (c (n "rpi-led-matrix") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zk11bha0h7ndaqd7hhrrlap6nm1g54hgw5c70snl96274yzj5gs")))

(define-public crate-rpi-led-matrix-0.1.5 (c (n "rpi-led-matrix") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pqndwfanh4v5h4xk5ac9ii6c9dl3lnhwzmlax2j33sbw6l92knf")))

(define-public crate-rpi-led-matrix-0.2.0 (c (n "rpi-led-matrix") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c7935p7spi255ijgc2v99vdg9q7higsd7dlxj5byqj1rid7va9r") (f (quote (("embeddedgraphics" "embedded-graphics") ("default" "embeddedgraphics") ("args" "clap"))))))

(define-public crate-rpi-led-matrix-0.2.1 (c (n "rpi-led-matrix") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "148hap4m70i367kmllkaqdxwja10zi17b1ixd1k6j5lgj2gl6a9y") (f (quote (("embeddedgraphics" "embedded-graphics") ("default" "embeddedgraphics") ("args" "clap"))))))

(define-public crate-rpi-led-matrix-0.2.2 (c (n "rpi-led-matrix") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dc3v04ls53fcsf24z03q4505hvr0r4k1mp6p4izm83lp4ml89sv") (f (quote (("embeddedgraphics" "embedded-graphics") ("default" "embeddedgraphics") ("args" "clap"))))))

(define-public crate-rpi-led-matrix-0.3.0 (c (n "rpi-led-matrix") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rpi-led-matrix-sys") (r "^0.1") (d #t) (k 0)))) (h "0v6c5nww2fa16rix5bpx8kwg29k532gwwlp5wbpzli7g0bjy3692") (f (quote (("embeddedgraphics" "embedded-graphics") ("default" "embeddedgraphics") ("c-stubs" "rpi-led-matrix-sys/c-stubs") ("args" "clap"))))))

(define-public crate-rpi-led-matrix-0.3.1 (c (n "rpi-led-matrix") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rpi-led-matrix-sys") (r "^0.1") (d #t) (k 0)))) (h "1607zk8fqysgalrfvc28w7m67r1d74rkd2jkw5my1plhxfm42v20") (f (quote (("embeddedgraphics" "embedded-graphics") ("default" "embeddedgraphics") ("c-stubs" "rpi-led-matrix-sys/c-stubs") ("args" "clap"))))))

(define-public crate-rpi-led-matrix-0.4.0 (c (n "rpi-led-matrix") (v "0.4.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("cargo"))) (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rpi-led-matrix-sys") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)))) (h "0w5nqnmxxjv0fvlqfrlypakz04hznv965jcpidvxfvarz5bkvlys") (f (quote (("stdcpp-static-link" "rpi-led-matrix-sys/stdcpp-static-link") ("embeddedgraphics" "embedded-graphics-core") ("default" "embeddedgraphics") ("c-stubs" "rpi-led-matrix-sys/c-stubs") ("args" "clap"))))))

