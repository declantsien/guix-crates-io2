(define-module (crates-io rp i- rpi-led-matrix-sys) #:use-module (crates-io))

(define-public crate-rpi-led-matrix-sys-0.1.0 (c (n "rpi-led-matrix-sys") (v "0.1.0") (d (list (d (n "copy_dir") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xrrmp33ryq4zyk3fmi06lanncm4r6d5pwyl7ijsh4a8mdw7zsk0") (f (quote (("default") ("c-stubs")))) (y #t) (l "rgbmatrixsys")))

(define-public crate-rpi-led-matrix-sys-0.1.1 (c (n "rpi-led-matrix-sys") (v "0.1.1") (d (list (d (n "copy_dir") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fqic73m0ywfcmn2gx9364s7alwdxzsz4bwq6riq4yv8aix5b89v") (f (quote (("default") ("c-stubs")))) (y #t) (l "rgbmatrixsys")))

(define-public crate-rpi-led-matrix-sys-0.1.2 (c (n "rpi-led-matrix-sys") (v "0.1.2") (d (list (d (n "copy_dir") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fpckzlh5b9sd10vbqzlg76931kc2aj15pf5317ld0m1cgp9kvrk") (f (quote (("default") ("c-stubs")))) (y #t) (l "rgbmatrixsys")))

(define-public crate-rpi-led-matrix-sys-0.1.3 (c (n "rpi-led-matrix-sys") (v "0.1.3") (d (list (d (n "copy_dir") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lj7m6na893vdwdy3r0q61dsk2rfrizwly7f2zqqlgxh8nc2fcry") (f (quote (("default") ("c-stubs")))) (l "rgbmatrixsys")))

(define-public crate-rpi-led-matrix-sys-0.1.4 (c (n "rpi-led-matrix-sys") (v "0.1.4") (d (list (d (n "copy_dir") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02v7hd49yzg1yw2xqq56f3qzax1kld1p5i5vk01zj6np19p68a3n") (f (quote (("default") ("c-stubs")))) (l "rgbmatrixsys")))

(define-public crate-rpi-led-matrix-sys-0.2.0 (c (n "rpi-led-matrix-sys") (v "0.2.0") (d (list (d (n "copy_dir") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w37m9g2j8nb6qram3wca5za0vjyz7pcvvj1rk1javmafy2l8r1c") (f (quote (("stdcpp-static-link") ("default") ("c-stubs")))) (l "rgbmatrixsys")))

(define-public crate-rpi-led-matrix-sys-0.2.1 (c (n "rpi-led-matrix-sys") (v "0.2.1") (d (list (d (n "copy_dir") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "024hzk9gk2l24m972ikz4179aj1gbiwj2j0kjfq5x0p8z0l1m2lw") (f (quote (("stdcpp-static-link") ("default") ("c-stubs")))) (l "rgbmatrixsys")))

