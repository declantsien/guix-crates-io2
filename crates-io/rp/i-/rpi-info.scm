(define-module (crates-io rp i- rpi-info) #:use-module (crates-io))

(define-public crate-rpi-info-0.1.0 (c (n "rpi-info") (v "0.1.0") (h "1h0vn4wp44smgnrfkbx6g11ray4svi1fkpqdkjp40axxfks02zj8")))

(define-public crate-rpi-info-0.2.0 (c (n "rpi-info") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "11zgw79v7ifadizx7qgzr0rcrzkwa4xhaai4150lwr7h8zrph0q0")))

(define-public crate-rpi-info-0.3.0 (c (n "rpi-info") (v "0.3.0") (d (list (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "1fz859zvy726zc2wqzhcn9226fnmw77k1x2r8zxl0wmb92ymb4qv")))

