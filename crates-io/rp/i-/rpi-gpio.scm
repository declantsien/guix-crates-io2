(define-module (crates-io rp i- rpi-gpio) #:use-module (crates-io))

(define-public crate-rpi-gpio-0.1.0 (c (n "rpi-gpio") (v "0.1.0") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0g5r7a800w97vdqcy0wwjv0w1mydnwwbs07hvirjnfkmsyrqwaby")))

(define-public crate-rpi-gpio-0.1.1 (c (n "rpi-gpio") (v "0.1.1") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "00fr0kgfal6520zpgsn4vgj3587rb0r7q9v4bc3hpih0j5fb9m5x")))

