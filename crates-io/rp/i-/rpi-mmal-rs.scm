(define-module (crates-io rp i- rpi-mmal-rs) #:use-module (crates-io))

(define-public crate-rpi-mmal-rs-0.0.1 (c (n "rpi-mmal-rs") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qaapqnp5n16rqsh20wsc2jcsallqnlm94a715jh2zd2imjy1fhj")))

(define-public crate-rpi-mmal-rs-0.0.2 (c (n "rpi-mmal-rs") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "062zviqlf46wpd3zydfn8jrcnasj4qx5j032g9m846aj8ch8zxqz")))

(define-public crate-rpi-mmal-rs-0.0.3 (c (n "rpi-mmal-rs") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zw65awlkn79lhhkv30p4zj16cq314y85r1s5mslgxdp9cblyrp4")))

