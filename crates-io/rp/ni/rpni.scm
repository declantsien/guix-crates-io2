(define-module (crates-io rp ni rpni) #:use-module (crates-io))

(define-public crate-rpni-0.1.0 (c (n "rpni") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "utf8-chars") (r "^1") (d #t) (k 0)))) (h "1wcwbb9yz14f1jjpaqis5vxpibjqm4yx9mcjdw42267r3kbz184h")))

(define-public crate-rpni-0.1.1 (c (n "rpni") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "utf8-chars") (r "^1") (d #t) (k 0)))) (h "1ivp1d6rypialrx7dapvr6dhf5v13fl86idkpxijxzx0zv9xq7g7")))

