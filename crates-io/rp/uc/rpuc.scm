(define-module (crates-io rp uc rpuc) #:use-module (crates-io))

(define-public crate-rpuc-0.2.0 (c (n "rpuc") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "rpu") (r "^0.2.0") (d #t) (k 0)))) (h "1m2fwgsvqgxp1d082qq512fg9zlzvrvmvmqyv6ib11wyvdg4i4la")))

(define-public crate-rpuc-0.2.1 (c (n "rpuc") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "rpu") (r "^0.2.0") (d #t) (k 0)))) (h "16y9969av66mjk5z7g5bzj819gsivmrk5zwp2swl6cqd6yybflss")))

(define-public crate-rpuc-0.2.2 (c (n "rpuc") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "rpu") (r "^0.2.2") (d #t) (k 0)))) (h "0knadm6f83liar05ab3f9gklcjq0vhlm1jbpmdsvpqabrccp0ms0")))

(define-public crate-rpuc-0.2.3 (c (n "rpuc") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "rpu") (r "^0.2.3") (d #t) (k 0)))) (h "10p7hvprqd4fk0q3i72z3k9h99ny4i1771pw7gydyqjkwhwp7w53")))

(define-public crate-rpuc-0.2.5 (c (n "rpuc") (v "0.2.5") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "rpu") (r "^0.2.5") (d #t) (k 0)))) (h "0yqwg9vpyxywn43p8znryncxfsr0c3ggwrw9axlgkf50c7jfh6bj")))

(define-public crate-rpuc-0.3.0 (c (n "rpuc") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.11") (d #t) (k 0)) (d (n "rpu") (r "^0.3.0") (d #t) (k 0)))) (h "1l1h32ypp1sj7h9n7c1gkm4y3sqdrpzs0d8n3275wvcmk4c71mby")))

