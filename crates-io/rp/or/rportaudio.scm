(define-module (crates-io rp or rportaudio) #:use-module (crates-io))

(define-public crate-rportaudio-0.1.0 (c (n "rportaudio") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1.39") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "1cm3zv2ybbikf98wmw6nw4v399dikkbcr1y3xa2awradi8wv28p2") (y #t)))

(define-public crate-rportaudio-0.1.1 (c (n "rportaudio") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "cmake") (r "^0.1.39") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "01yrc00hsdx4247gp742yp95zrr3n25crb7aq67g0pd7r2hxwn4k")))

