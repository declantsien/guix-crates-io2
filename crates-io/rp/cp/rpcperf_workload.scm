(define-module (crates-io rp cp rpcperf_workload) #:use-module (crates-io))

(define-public crate-rpcperf_workload-1.0.0 (c (n "rpcperf_workload") (v "1.0.0") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.2") (d #t) (k 0)) (d (n "pad") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "ratelimit") (r "^0.2.4") (d #t) (k 0)) (d (n "rpcperf_request") (r "^1.0.0") (d #t) (k 0)) (d (n "shuteye") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1q9wslaa09qc5v1pf81nxyx6qs2pqlpk04pfgvszq6d1h5c1q4c3") (y #t)))

(define-public crate-rpcperf_workload-2.0.0 (c (n "rpcperf_workload") (v "2.0.0") (d (list (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.2") (d #t) (k 0)) (d (n "pad") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "ratelimit") (r "^0.2.4") (d #t) (k 0)) (d (n "rpcperf_request") (r "^1.1.0") (d #t) (k 0)) (d (n "shuteye") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "190x5m8y6cdyiyfld5xfmq10hrgw6c2aapy3b7sfls77nkm0bbsa") (y #t)))

