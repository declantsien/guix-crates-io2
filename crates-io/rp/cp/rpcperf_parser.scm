(define-module (crates-io rp cp rpcperf_parser) #:use-module (crates-io))

(define-public crate-rpcperf_parser-1.0.0 (c (n "rpcperf_parser") (v "1.0.0") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "crc") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "08hacv10n870qff4lqw5z2z3f56arn6bkmw4jb210fbfjqhcrbii") (y #t)))

