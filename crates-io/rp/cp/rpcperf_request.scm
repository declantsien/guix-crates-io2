(define-module (crates-io rp cp rpcperf_request) #:use-module (crates-io))

(define-public crate-rpcperf_request-1.0.0 (c (n "rpcperf_request") (v "1.0.0") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "crc") (r "^1.2.0") (d #t) (k 0)))) (h "1ifrdqjmn8rkipna1n73494di9q6aigfs83cynh577jlk70bagg7") (y #t)))

(define-public crate-rpcperf_request-1.1.0 (c (n "rpcperf_request") (v "1.1.0") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)) (d (n "crc") (r "^1.2.0") (d #t) (k 0)))) (h "1p3041ydszfvi3xchqj1xikfq7zwss86i0hq2mfh7mvvgmbv527s") (y #t)))

