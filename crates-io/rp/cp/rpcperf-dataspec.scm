(define-module (crates-io rp cp rpcperf-dataspec) #:use-module (crates-io))

(define-public crate-rpcperf-dataspec-0.1.1 (c (n "rpcperf-dataspec") (v "0.1.1") (d (list (d (n "histogram") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1awbgl1jciavrqdap2avdx30mp2xgz3kil2c77nbvfpr0xlm3if3")))

(define-public crate-rpcperf-dataspec-0.2.0 (c (n "rpcperf-dataspec") (v "0.2.0") (d (list (d (n "histogram") (r "^0.8.3") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (o #t) (d #t) (k 0)))) (h "1xkrs2lxsjb0hmvjyv0h018jvvkxl30k7iyvmbdv6mywg4apypqf") (s 2) (e (quote (("utoipa" "dep:utoipa"))))))

