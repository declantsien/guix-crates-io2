(define-module (crates-io rp oo rpools) #:use-module (crates-io))

(define-public crate-rpools-0.2.2 (c (n "rpools") (v "0.2.2") (h "1f0iqipndkcnr9p5kdfdvh22n3zyb54j3d6v9h27vzm0pivdn7w8") (y #t)))

(define-public crate-rpools-0.2.3 (c (n "rpools") (v "0.2.3") (h "111qdlwiigmglfbivb8kr3916apkv3z0jj74nphlqpn0wqyay1j3") (y #t)))

(define-public crate-rpools-0.2.4 (c (n "rpools") (v "0.2.4") (h "0qv9gj5sr1p63bm1f2fzrrv2yab6wnww11ild5pm2m7m8qvkghf4")))

(define-public crate-rpools-0.3.0 (c (n "rpools") (v "0.3.0") (h "0ab72mrzp7dqc2bic4lrxk0b7rb7013k9nm1lcn2ib786yb5bw80")))

(define-public crate-rpools-0.3.1 (c (n "rpools") (v "0.3.1") (h "000bi562kiaw8l6amf3qnffdkq5by88kmh4iy8ws0xdpcinkrbv1")))

