(define-module (crates-io rp ro rprobe) #:use-module (crates-io))

(define-public crate-rprobe-0.1.0 (c (n "rprobe") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ajl2m9kkyyymikv8dyc6h4s4z5dp845ddv8da1gnyzppa8vbw81")))

(define-public crate-rprobe-0.1.1 (c (n "rprobe") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hjj41wjwhj6hk4rqf32397qpy58mzbfky1krwx4m57x64z5xsp2")))

(define-public crate-rprobe-0.2.0 (c (n "rprobe") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hl6k202i3lsanjwsybzzzlprqz61yjpvx2idrgcg7ac0kqzswzk")))

(define-public crate-rprobe-0.3.0 (c (n "rprobe") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wnb4nkjcha9i3vyv5nmwlpafw4bpld2ba287wnlrcgrkihvcwsy")))

(define-public crate-rprobe-0.3.1 (c (n "rprobe") (v "0.3.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10kvjczmwgllgd3y91z9pqhq1jczavsbg7cdpgqdb7djyn3jr0jq")))

(define-public crate-rprobe-0.3.2 (c (n "rprobe") (v "0.3.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lns87s7cqna4spixd6b7j1k53vkj9a15pnrxsc2gswzhwa74fbw")))

(define-public crate-rprobe-0.3.3 (c (n "rprobe") (v "0.3.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jc8v8f3hjjs6lz4w2pwg64hgvbb6lngk2gb876nbxg2gx4qc5x6")))

(define-public crate-rprobe-0.3.4 (c (n "rprobe") (v "0.3.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dv1xia5gcrd60qa64gcf5dril5lccpfl00swpis0kxrl97zv261")))

(define-public crate-rprobe-0.4.0 (c (n "rprobe") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hlxxx0sf9zj03dx55shqjvh81pndnzn6chza2ap0wqn5ziywa7f")))

(define-public crate-rprobe-0.4.1 (c (n "rprobe") (v "0.4.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mfhp9an1g4g7wpqspvxd78jk1h6h3dvg1gzm8czbhvbr7722fs9")))

(define-public crate-rprobe-0.4.2 (c (n "rprobe") (v "0.4.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kvv1p7qpvvxs4sdcp9gy5m9qi8qmclvi3ndhmnh91sb3calj5y6")))

(define-public crate-rprobe-0.5.0 (c (n "rprobe") (v "0.5.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04q7im6s673v977kzffylb2rhln2z0ybs3w9cjiqjkzbybs0qg9r")))

(define-public crate-rprobe-0.5.1 (c (n "rprobe") (v "0.5.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16ng9l9b43w5qb7yypjq79bhs2j5x1xsgwqyd6plqimyj8jbp6kj")))

(define-public crate-rprobe-0.5.2 (c (n "rprobe") (v "0.5.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p50yjps6swn4czm45zg6q0kmvj413pfll8vxljaprqxmzg4ghsp")))

(define-public crate-rprobe-0.5.3 (c (n "rprobe") (v "0.5.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g6vjc99f20awlczdg6b73aw107v071imp4rn6b5ghp9bw2vlvbj")))

(define-public crate-rprobe-0.5.4 (c (n "rprobe") (v "0.5.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1grmspfi24ak37wsr8idzxvszaky3rk7lwhiyi1lmvdyp5msjpjf")))

(define-public crate-rprobe-0.5.5 (c (n "rprobe") (v "0.5.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xywska16la5pbcjnh1m59adagzif1c1n5jxfi0zma5x4w2pmabi")))

(define-public crate-rprobe-0.5.6 (c (n "rprobe") (v "0.5.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cnc0bgiywj08797vs8n604acdjdmj7v5ps26acbf14p9vwx1iff")))

(define-public crate-rprobe-0.5.7 (c (n "rprobe") (v "0.5.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (d #t) (k 0)) (d (n "tokio") (r "^1.23.1") (f (quote ("full"))) (d #t) (k 0)))) (h "125c1n44pdlj6hsk2zd0x0kzkx4i43hlb9s1mqmi9q04rbl88wgj")))

