(define-module (crates-io rp ro rprofiler) #:use-module (crates-io))

(define-public crate-rprofiler-0.1.0 (c (n "rprofiler") (v "0.1.0") (d (list (d (n "flume") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0ml6094wvdpwd9qqkb50y3id901fjq93b6jlhf1bh3bbl80h8agg")))

(define-public crate-rprofiler-0.1.1 (c (n "rprofiler") (v "0.1.1") (d (list (d (n "flume") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0va804fzacwhrzqfkcl5rhjfmdrjrwhd9knwwv4fi172k7g3da2a")))

(define-public crate-rprofiler-0.2.0 (c (n "rprofiler") (v "0.2.0") (d (list (d (n "flume") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0dzg2xyrhq906gaf7as3vxw0x2f9zqvjl250v5kb3gygwxjw8qb2") (f (quote (("disable_profiling"))))))

