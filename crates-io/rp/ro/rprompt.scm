(define-module (crates-io rp ro rprompt) #:use-module (crates-io))

(define-public crate-rprompt-1.0.0 (c (n "rprompt") (v "1.0.0") (h "1rm8p93l19h4hwab8pkjqf5ad4gy8rzd93411bdwhgbwi5p8fg4y")))

(define-public crate-rprompt-1.0.1 (c (n "rprompt") (v "1.0.1") (h "0sqpg844k7lqk9p31ymyh3bvh6rlm1z7wdyy6ddzvnh2i2k7mp5x")))

(define-public crate-rprompt-1.0.2 (c (n "rprompt") (v "1.0.2") (h "03r93iz5idyxjl5mqlv1fivpvip1vgjr59wr7hfzla5y1af0l81p")))

(define-public crate-rprompt-1.0.3 (c (n "rprompt") (v "1.0.3") (h "09dndq5w0q4m117ikq3bq72hr0jddk4lbim1pwyax2l5qlmz608n")))

(define-public crate-rprompt-1.0.4 (c (n "rprompt") (v "1.0.4") (h "0r6x9zggdymry9g8ms3ffgbm9xbga5rvj5ch2p7mkf1129ix48qc")))

(define-public crate-rprompt-1.0.5 (c (n "rprompt") (v "1.0.5") (h "1y1haas18ahyhdvya84l10fqazv40zd5yzw5jvyaxqnsidsg91mk")))

(define-public crate-rprompt-2.0.0 (c (n "rprompt") (v "2.0.0") (d (list (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)))) (h "00lbid1swc2c6qcl0y58k2s3z0m6wq8ziv64iirik8zazdfbk904")))

(define-public crate-rprompt-2.0.1 (c (n "rprompt") (v "2.0.1") (d (list (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)))) (h "060nsmckayvgbf2wahjfs3lwsygadccw2nsgxg2jy9k91pmrhxl8")))

(define-public crate-rprompt-2.0.2 (c (n "rprompt") (v "2.0.2") (d (list (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "winnt" "fileapi" "processenv" "winbase" "handleapi" "consoleapi" "minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13mjv6l8093rd65vf2is27lrg7yxfckh2d8841s2hax3clandb22")))

(define-public crate-rprompt-2.1.0 (c (n "rprompt") (v "2.1.0") (d (list (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_Console" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1968vdlpzdx6q6v8k2jfcimxr2aj13yblakvwc6zk3hv7nlg9qlg")))

(define-public crate-rprompt-2.1.1 (c (n "rprompt") (v "2.1.1") (d (list (d (n "rtoolbox") (r "^0.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_Console" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04g6118ih306ka0dk54dg83x4nv5wzap7adwp0kg5fn6dwabq90d")))

