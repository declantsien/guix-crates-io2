(define-module (crates-io rp li rplidar_drv) #:use-module (crates-io))

(define-public crate-rplidar_drv-0.5.0 (c (n "rplidar_drv") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "rpos_drv") (r "^0.1.0") (d #t) (k 0)))) (h "1vhgamd6add2gzl4hpjfvppqfhyn50q0sjys72zb3ma12b1d4ia3")))

(define-public crate-rplidar_drv-0.6.0 (c (n "rplidar_drv") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "rpos_drv") (r "^0.2.0") (d #t) (k 0)))) (h "1alj5f781hxqr0qy8lcdnjhvkxirfw42pkyzlpfgir04y9p0cp91")))

