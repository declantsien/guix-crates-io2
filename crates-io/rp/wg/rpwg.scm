(define-module (crates-io rp wg rpwg) #:use-module (crates-io))

(define-public crate-rpwg-0.1.0 (c (n "rpwg") (v "0.1.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jnk33cg9845k5a4i4vniczyi64qaah03i6x946bm7b908vplhql")))

(define-public crate-rpwg-0.1.1 (c (n "rpwg") (v "0.1.1") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "044xgf5pd3v9rdy7jr2l79n1j4ga1163crr66i7mball4d66f9ra")))

(define-public crate-rpwg-0.1.2 (c (n "rpwg") (v "0.1.2") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kzaq65k2zc7qgw4xhpr25irisdqxxbml4d968k8r3ys5h15rav5")))

(define-public crate-rpwg-0.2.0 (c (n "rpwg") (v "0.2.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hx0g5rmdhmh3w608wdxg7vlspfhcinmijv0qx1r5617pcr9yy0l")))

(define-public crate-rpwg-0.3.0 (c (n "rpwg") (v "0.3.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pp9pnhzlbcqalw7xi03iqj26zjnp55nqy9da3b6n521zyhm5bha")))

(define-public crate-rpwg-0.4.0 (c (n "rpwg") (v "0.4.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ab0gnm6w1ka2ysng6zfr3lr01sq2q69wh581liy3bl54qwglm29")))

