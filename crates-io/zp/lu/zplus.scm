(define-module (crates-io zp lu zplus) #:use-module (crates-io))

(define-public crate-zplus-0.9.3-rc.1 (c (n "zplus") (v "0.9.3-rc.1") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.12") (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "0bdfxisjc1mni5063sd2ax6866brni0nzv47sbijn7bqimmk1gki")))

