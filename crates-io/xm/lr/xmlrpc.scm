(define-module (crates-io xm lr xmlrpc) #:use-module (crates-io))

(define-public crate-xmlrpc-0.1.0 (c (n "xmlrpc") (v "0.1.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "00g0bccdwhbhwwm9sp2ras8xh9xa78vqjbzf8ghkb5anaq690zq4")))

(define-public crate-xmlrpc-0.1.1 (c (n "xmlrpc") (v "0.1.1") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0vhlgn7bkiv8f23id6fikv2vcsd4qsybyi29mfhc0523w4c3x5xg") (y #t)))

(define-public crate-xmlrpc-0.1.2 (c (n "xmlrpc") (v "0.1.2") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1p86jrqqiic6la0vcxsb0g3r9jpf15byfk1r3as6gihhd6wfhcjn")))

(define-public crate-xmlrpc-0.2.0 (c (n "xmlrpc") (v "0.2.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1jphrscpd1l8p2vwqb78rvwww3amwqp4wxw5xb0is1wggq1gc37n")))

(define-public crate-xmlrpc-0.2.1 (c (n "xmlrpc") (v "0.2.1") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1xdnajkyqrm84qf8xvhl721zrkdc9x551x6ckb2dpmrlhxcmv0x9")))

(define-public crate-xmlrpc-0.3.0 (c (n "xmlrpc") (v "0.3.0") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0j1brpy2wp0angy2mpwz7gxi7vdhnl7myqn9a5zxc3fk2yzrcd8g")))

(define-public crate-xmlrpc-0.3.1 (c (n "xmlrpc") (v "0.3.1") (d (list (d (n "base64") (r "^0.2.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1yqff9xqyjp1505bs953rhv62wpnjqnl2ydgh4p9wvip218xasa4")))

(define-public crate-xmlrpc-0.3.2 (c (n "xmlrpc") (v "0.3.2") (d (list (d (n "base64") (r "^0.3.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1cb5nlmagyz392f1sqf3l1gpc7vn92lldwyb7a7yc27dxxcg3gh0")))

(define-public crate-xmlrpc-0.4.0 (c (n "xmlrpc") (v "0.4.0") (d (list (d (n "base64") (r "^0.3.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "0xvvrc69n2nyr0zzi351r6gjfq5w4yd8ym855jci1l6gwpgbjd5q")))

(define-public crate-xmlrpc-0.4.1 (c (n "xmlrpc") (v "0.4.1") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "19xnjgjk6n71ljmwzy956snnggxfag5z6265z63nxbg5ms7xrjwn")))

(define-public crate-xmlrpc-0.5.0 (c (n "xmlrpc") (v "0.5.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)))) (h "0i3n8qlvsvv4zddfkw7kwfcl6bbyg1wvb1z00kn2v1y60ly468ig")))

(define-public crate-xmlrpc-0.6.0 (c (n "xmlrpc") (v "0.6.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.2") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "1fm4rc8233w6wczy9zichvaviqv2gw8c9sas1svh52l94f13861a")))

(define-public crate-xmlrpc-0.7.0 (c (n "xmlrpc") (v "0.7.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0jqmswx2f0bh4hwhpwnh5iwganm54l601hh6ch6dpb69z9kgjj10")))

(define-public crate-xmlrpc-0.7.1 (c (n "xmlrpc") (v "0.7.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.0") (d #t) (k 0)))) (h "0gfmnallmiigwbfbyhj6d1bf6blykmrhvddwdsbz8xvjw220i6j6")))

(define-public crate-xmlrpc-0.8.0 (c (n "xmlrpc") (v "0.8.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "0s4vcm65ndiaw4szkvbj9bdn5xav45c6ib1wd2hz146k77xw4sak")))

(define-public crate-xmlrpc-0.9.0 (c (n "xmlrpc") (v "0.9.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "1gijks753alzp15x73vw2xcxycvk1kvhg79j6q05r4449rws63ph")))

(define-public crate-xmlrpc-0.10.0 (c (n "xmlrpc") (v "0.10.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "0anjif8liydh9pc69x73wj6wh7ijjncw89h50bba8w03ijzanj6z") (f (quote (("default" "reqwest"))))))

(define-public crate-xmlrpc-0.11.0 (c (n "xmlrpc") (v "0.11.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "1rzbm58pnbwfpp5cida9gcsngb8f9rm0rm5rgabc17pqvk79d90d") (f (quote (("default" "reqwest"))))))

(define-public crate-xmlrpc-0.11.1 (c (n "xmlrpc") (v "0.11.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "1sm8jij40qp1ggh7wnav4hfnzdr55smmlnkqc0f9vg0q4iwc1k1n") (f (quote (("default" "reqwest"))))))

(define-public crate-xmlrpc-0.12.0 (c (n "xmlrpc") (v "0.12.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "0cikcp9w993gcp77jp816msjcwckaxpx3q7aggnrjpww79gkikjy") (f (quote (("default" "reqwest"))))))

(define-public crate-xmlrpc-0.13.0 (c (n "xmlrpc") (v "0.13.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)))) (h "1cbvziad8hy1vp4fdylczn1qiw1xbnaiwr7g54pljxmr73ch9cgq") (f (quote (("http" "reqwest" "mime") ("default" "http"))))))

(define-public crate-xmlrpc-0.13.1 (c (n "xmlrpc") (v "0.13.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "iso8601") (r "^0.2.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0gq6sr1kvdlds27klsvg8q3m208zn1sfj0dgddxsmlgasiwqhxna") (f (quote (("http" "reqwest" "mime") ("default" "http"))))))

(define-public crate-xmlrpc-0.14.0 (c (n "xmlrpc") (v "0.14.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (f (quote ("blocking"))) (o #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "09a7zbkj8wnm1pxd5bmpc1n0gcbjpyyfzfmg1slrjjrpl47g1vr0") (f (quote (("tls" "reqwest/default-tls") ("http" "reqwest" "mime") ("default" "http" "tls"))))))

(define-public crate-xmlrpc-0.15.0 (c (n "xmlrpc") (v "0.15.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking"))) (o #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1661fyas6iw7rygrwbhgkrmrxlgri5n0g43xgm0ka52hk8mxzkd0") (f (quote (("tls" "reqwest/default-tls") ("http" "reqwest" "mime") ("default" "http" "tls"))))))

(define-public crate-xmlrpc-0.15.1 (c (n "xmlrpc") (v "0.15.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("blocking"))) (o #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1xviwks6c4w408kbi419wa8fn3lfbizz226qffxg04m4hqpcsdlb") (f (quote (("tls" "reqwest/default-tls") ("http" "reqwest" "mime") ("default" "http" "tls"))))))

