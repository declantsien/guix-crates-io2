(define-module (crates-io xm lp xmlparser-derive) #:use-module (crates-io))

(define-public crate-xmlparser-derive-0.1.0 (c (n "xmlparser-derive") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.0") (d #t) (k 2)) (d (n "xmlparser-derive-core") (r "^0.1.0") (d #t) (k 0)) (d (n "xmlparser-derive-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1p6r6aqhjw25xzb3m126nkdn5izzq26wppkzcx11dwdl4v9y7sxk")))

(define-public crate-xmlparser-derive-0.1.1 (c (n "xmlparser-derive") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.0") (d #t) (k 2)) (d (n "xmlparser-derive-core") (r "^0.1.0") (d #t) (k 0)) (d (n "xmlparser-derive-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1yxriqn5ng1d96sdr78n21sch2c6c0ap8k91ymb6m1vx7gvyhnm8")))

(define-public crate-xmlparser-derive-0.1.2 (c (n "xmlparser-derive") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.0") (d #t) (k 2)) (d (n "xmlparser-derive-core") (r "^0.1.0") (d #t) (k 0)) (d (n "xmlparser-derive-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1ngy66qngybwqr0vivi6qanggv5wj8k7qhhnfmw8b30qxj4c7lv3")))

(define-public crate-xmlparser-derive-0.1.3 (c (n "xmlparser-derive") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 2)) (d (n "xmlparser") (r "^0.13.0") (d #t) (k 2)) (d (n "xmlparser-derive-core") (r "^0.1.0") (d #t) (k 0)) (d (n "xmlparser-derive-utils") (r "^0.1.0") (d #t) (k 0)))) (h "064b17s9snx5n8g6pfjbcmyfmqfqziwhk6ljqzb415ljqa2n1xbg")))

