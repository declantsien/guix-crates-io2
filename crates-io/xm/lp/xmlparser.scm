(define-module (crates-io xm lp xmlparser) #:use-module (crates-io))

(define-public crate-xmlparser-0.1.0 (c (n "xmlparser") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-test") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)))) (h "0km8v2jmyxhk1qdi0gwk42gspr95aif2jsakcvihjvsdbgl2xj73")))

(define-public crate-xmlparser-0.1.1 (c (n "xmlparser") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-test") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)))) (h "1bkydi5l4drb02mxbvl86j00n0w38si9bjn1br8jr3ldk0d0sd3k")))

(define-public crate-xmlparser-0.1.2 (c (n "xmlparser") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-test") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)))) (h "1anl2lz0xxkcbzydk3jcr2v1gfhvr14f7sjiwxam1kwgwyvqryx4")))

(define-public crate-xmlparser-0.2.0 (c (n "xmlparser") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.3") (d #t) (k 2)))) (h "0bhg5s36sjlds09y90nnc2zkp7avpapj7k9rlj7rc3cv6zwl0npn")))

(define-public crate-xmlparser-0.3.0 (c (n "xmlparser") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.3") (d #t) (k 2)))) (h "0d1qhc60higrccmi0zafq9jv75anrzlsyw28x53ll4d7cf9vkh4l")))

(define-public crate-xmlparser-0.4.0 (c (n "xmlparser") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.3") (d #t) (k 2)))) (h "0642rw6j2x7zjmz064qzhaz26j4zq9b1695vzywmwi6ifvqsnirs")))

(define-public crate-xmlparser-0.4.1 (c (n "xmlparser") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "stderrlog") (r "^0.3") (d #t) (k 2)))) (h "1amakf52smyvh1n517zd64jhvm7j372v9d3gmi814wdxxlaln2m8")))

(define-public crate-xmlparser-0.5.0 (c (n "xmlparser") (v "0.5.0") (h "0lg8k8vi0xb71sn819629893lczvy1l9j9377grppkf6slgffz9i")))

(define-public crate-xmlparser-0.6.0 (c (n "xmlparser") (v "0.6.0") (h "1bw0bvkfih2yxjwm6bsfd1wb2lpp8xc4qbwhfdsj2b1rapqmmwqc")))

(define-public crate-xmlparser-0.6.1 (c (n "xmlparser") (v "0.6.1") (h "1fvjrhj73p2048b9lvlmm8rwm89b5nfsai7q7api5k0nk6gg4xga")))

(define-public crate-xmlparser-0.7.0 (c (n "xmlparser") (v "0.7.0") (h "1d6xh4dg69yaw6c1ngsgsqh4hkgfg9rp3djnfl02sgx820zsbsa1")))

(define-public crate-xmlparser-0.8.0 (c (n "xmlparser") (v "0.8.0") (h "1p7r8awinjksv2xd1xcaiccgai5mj2l3m5f1qlhkr10a330mcli2")))

(define-public crate-xmlparser-0.8.1 (c (n "xmlparser") (v "0.8.1") (h "0kg3cp5inzh6pj8a0ab9ycfzf39nk57xhifm44rzdvappclpz2np")))

(define-public crate-xmlparser-0.9.0 (c (n "xmlparser") (v "0.9.0") (h "1vhw7s5672557vdz9s5kkmv0j53xz0haakp6af8h3zxh1zq9bv7c")))

(define-public crate-xmlparser-0.10.0 (c (n "xmlparser") (v "0.10.0") (h "1sr1d602ng26lx07h3mqn3p3k88sg5d8v8rd1dmrc3fcbdn4j441")))

(define-public crate-xmlparser-0.11.0 (c (n "xmlparser") (v "0.11.0") (h "0spbddc1h4y5ncbgnvg4bgig17y7h7mvykj5lwn8lphv6ixb0pj7") (f (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.12.0 (c (n "xmlparser") (v "0.12.0") (h "1397vmn6p9g4vq6j5b3v7ll62v8nfrqmdx1dnvhwv0xnjs7i7pr8") (f (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13.0 (c (n "xmlparser") (v "0.13.0") (h "15xsi5z44l5d8ql2hc6wnqidlwr7nar2ab77s13i70cyic982sdi") (f (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13.1 (c (n "xmlparser") (v "0.13.1") (h "1mv2hg0k8cyap8qb05c1a8kz86zcdmf9wdp95nz41pys0c129d6c") (f (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13.2 (c (n "xmlparser") (v "0.13.2") (h "1x95247fshdljsrdlrcvvyb4vr39xysw7lg71wygc4bgbxjkwqaj") (f (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13.3 (c (n "xmlparser") (v "0.13.3") (h "1n73ymdxpdq30fgz698095zvh8k5r264rl6pcxnyyrr19nra4jqi") (f (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13.4 (c (n "xmlparser") (v "0.13.4") (h "09avy9adr1bqrdpbv9y4z5dy4kp3iavjk1zbdnlm5kni0z9pcqia") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-xmlparser-0.13.5 (c (n "xmlparser") (v "0.13.5") (h "1z8kkbbhq5mg8k02szi8cvivrfv88wajky4p182c84paz5dwf9ad") (f (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13.6 (c (n "xmlparser") (v "0.13.6") (h "1r796g21c70p983ax0j6rmhzmalg4rhx61mvd4farxdhfyvy1zk6") (f (quote (("std") ("default" "std"))))))

