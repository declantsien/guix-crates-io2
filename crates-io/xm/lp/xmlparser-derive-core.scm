(define-module (crates-io xm lp xmlparser-derive-core) #:use-module (crates-io))

(define-public crate-xmlparser-derive-core-0.1.0 (c (n "xmlparser-derive-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "xmlparser") (r "^0.13.0") (d #t) (k 0)))) (h "0v8z76p75i8li5g4wr53s49g6mavhzcgkygl662s2zyr5r3mz8zr")))

(define-public crate-xmlparser-derive-core-0.1.1 (c (n "xmlparser-derive-core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "xmlparser") (r "^0.13.0") (d #t) (k 0)))) (h "04h7pd38iflr2638w2vjj7lfjgg8nkg3s2y988zckb3qvfpbwa2c")))

(define-public crate-xmlparser-derive-core-0.1.2 (c (n "xmlparser-derive-core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "xmlparser") (r "^0.13.0") (d #t) (k 0)))) (h "1lnsa7anbkr5hwcxczdmy7lvbdmdfagj0akigdxn18hhh6wb3vw2")))

