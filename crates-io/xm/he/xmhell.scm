(define-module (crates-io xm he xmhell) #:use-module (crates-io))

(define-public crate-xmhell-0.1.0 (c (n "xmhell") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ib02iyfv5vjyap5yg1zfrbsb5ngmnbb25fzdhaf3ms73kqs2dis")))

