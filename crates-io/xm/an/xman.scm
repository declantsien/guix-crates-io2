(define-module (crates-io xm an xman) #:use-module (crates-io))

(define-public crate-xman-0.1.0 (c (n "xman") (v "0.1.0") (h "0nygvmdch47mrxkjfcy9akdf28vwmxaskhlmy9ic1xsafb2rmiq9")))

(define-public crate-xman-1.0.0 (c (n "xman") (v "1.0.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sg3ssbnnvwb38kxh9blf6w82vavr8xiv6gi62366bwqk0xafd7i")))

(define-public crate-xman-1.0.1 (c (n "xman") (v "1.0.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yi0rbvnh2wry5bgazdhrdqig0b7d7pcqi6nk3ibnnkl90pyhgfq")))

(define-public crate-xman-1.0.2 (c (n "xman") (v "1.0.2") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cikwziq5ca5n9z4f7k3v497115046ay8l0s6skdwb0r5qfgw6vh")))

(define-public crate-xman-1.0.3 (c (n "xman") (v "1.0.3") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0avfkzykjwmv0nr7wzjhykpx9kbiqk8gksmyys203is5d0gwrxn0")))

