(define-module (crates-io xm lt xmltree-parse_with_config) #:use-module (crates-io))

(define-public crate-xmltree-parse_with_config-0.10.3 (c (n "xmltree-parse_with_config") (v "0.10.3") (d (list (d (n "indexmap") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "04q1qrsfvdc67aicsa4dnpd2g6qcpkh5109ck37ifcavkbfa0jx5") (f (quote (("default") ("attribute-sorted") ("attribute-order" "indexmap"))))))

