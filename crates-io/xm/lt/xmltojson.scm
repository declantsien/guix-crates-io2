(define-module (crates-io xm lt xmltojson) #:use-module (crates-io))

(define-public crate-xmltojson-0.1.0 (c (n "xmltojson") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "196sry1ig935ab1smx2nnija6plwpwqn9xx3b7gx374apg94gn1n")))

(define-public crate-xmltojson-0.1.1 (c (n "xmltojson") (v "0.1.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06cd0wg38jh5qx03swbwv2sj71fzf9fncjk46inpp3zyvbksznm1")))

(define-public crate-xmltojson-0.1.3 (c (n "xmltojson") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.28.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fwv8givwdq18n1p4shh8x6i6qxcx1q4xn3h8als9dz3v9ryziim")))

