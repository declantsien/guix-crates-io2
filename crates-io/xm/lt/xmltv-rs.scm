(define-module (crates-io xm lt xmltv-rs) #:use-module (crates-io))

(define-public crate-xmltv-rs-0.0.1 (c (n "xmltv-rs") (v "0.0.1") (d (list (d (n "xml-builder") (r "^0.2.1") (d #t) (k 0)))) (h "0qkj5372by0v8a7lp0hlmzajfnm0ddcqvxrxnis045lx4skayxw3")))

(define-public crate-xmltv-rs-0.0.2 (c (n "xmltv-rs") (v "0.0.2") (d (list (d (n "xml-builder") (r "^0.2.1") (d #t) (k 0)))) (h "0s47k416flbzypi2119gzyspd19prkn2g1wmg0xbj5l33905nslk")))

(define-public crate-xmltv-rs-0.0.3 (c (n "xmltv-rs") (v "0.0.3") (d (list (d (n "xml-builder") (r "^0.2.1") (d #t) (k 0)))) (h "0ndwzmry483gxjyyqprlqbmq0vllxz968rx2iawhzhmzsrr817si")))

