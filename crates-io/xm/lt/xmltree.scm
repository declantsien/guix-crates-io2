(define-module (crates-io xm lt xmltree) #:use-module (crates-io))

(define-public crate-xmltree-0.2.1 (c (n "xmltree") (v "0.2.1") (d (list (d (n "tempfile") (r "~1.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.1") (d #t) (k 0)))) (h "0x1ah8maslc960pjb2ykvzhjg6s32pz7vr6h4llp2hg3585n0jb8")))

(define-public crate-xmltree-0.3.0 (c (n "xmltree") (v "0.3.0") (d (list (d (n "tempfile") (r "~1.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "1r4290mi7g1697qdrkbd0kbnpk9k31xhqqrmz6mz1c9lswhfb3gv")))

(define-public crate-xmltree-0.3.1 (c (n "xmltree") (v "0.3.1") (d (list (d (n "tempfile") (r "~1.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "1lmha8r7nkwhp4823p4yap9qcpzq5z1klaha32qcxrpvhc579mp5")))

(define-public crate-xmltree-0.3.2 (c (n "xmltree") (v "0.3.2") (d (list (d (n "tempfile") (r "~1.1") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "10i050rdiggc85whs5dbnvd7dgzkn64mppv124wv4fn5qwvrsaj7")))

(define-public crate-xmltree-0.4.0 (c (n "xmltree") (v "0.4.0") (d (list (d (n "xml-rs") (r "^0.4") (d #t) (k 0)))) (h "1v9076kdfvgdw9k8is4px31b8pkxwqy0j888iqwf2b7gwf5ilgwp")))

(define-public crate-xmltree-0.5.0 (c (n "xmltree") (v "0.5.0") (d (list (d (n "xml-rs") (r "^0.5") (d #t) (k 0)))) (h "1fl2jpa152fx441hz2fkv48w1lnwi4m1r4mz0wix6pl2p939yvr9")))

(define-public crate-xmltree-0.6.0 (c (n "xmltree") (v "0.6.0") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0lbj3qil5glw63ljwsblqzlj8rb4r84s3dxizdari64v7si8gxrd")))

(define-public crate-xmltree-0.6.1 (c (n "xmltree") (v "0.6.1") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1xd9njqlw36v1jw3k6g26irg6ljbs65kphhvasjmjsg2791vqshj")))

(define-public crate-xmltree-0.7.0 (c (n "xmltree") (v "0.7.0") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0cbla8x58vs1xb3r64vynxhkbm9ln62y9711fwipvwdqlr6bbkx9")))

(define-public crate-xmltree-0.8.0 (c (n "xmltree") (v "0.8.0") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0w0y0jz7lhxg05ca6ngfj0lj8sbrjh4vaqv13q7qaqkhs7lsx3pz")))

(define-public crate-xmltree-0.9.0 (c (n "xmltree") (v "0.9.0") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "061hsxl9v6q5jl47iykhzkp02hidfs79sfgz1vydh9743dsxbd98")))

(define-public crate-xmltree-0.10.0 (c (n "xmltree") (v "0.10.0") (d (list (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1pq4602zdi6p4yq522qnpb284yi174gsp6wdg922bnq0wvphg9kp")))

(define-public crate-xmltree-0.10.1 (c (n "xmltree") (v "0.10.1") (d (list (d (n "indexmap") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1cr2r4xcrl6fyrgpmssjjq7n5l1d7ybw50k5rgqg24q3rfnasszp") (f (quote (("default") ("attribute-order" "indexmap"))))))

(define-public crate-xmltree-0.10.2 (c (n "xmltree") (v "0.10.2") (d (list (d (n "indexmap") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1p8igrydh3w9mqiyay3wxnddmgbgzfs0a3df5rs38whksi1gsinh") (f (quote (("default") ("attribute-order" "indexmap"))))))

(define-public crate-xmltree-0.10.3 (c (n "xmltree") (v "0.10.3") (d (list (d (n "indexmap") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1jqzwhr1a5cknflsshhhjlllmd1xi04qdkjsls2bnmv5mxgagn6p") (f (quote (("default") ("attribute-order" "indexmap"))))))

