(define-module (crates-io xm to xmtool-sys) #:use-module (crates-io))

(define-public crate-xmtool-sys-0.1.0 (c (n "xmtool-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)))) (h "0hmvq64ldwmpgdw9swkgf8kg7wr5hp7hrlsaasmjhfqwq94hx72w")))

(define-public crate-xmtool-sys-0.1.1 (c (n "xmtool-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.17") (d #t) (k 1)))) (h "1h9wa8yfc4irs018z171qrnvcvjlcby7mjzijzkfmyn7dm1gpc88")))

(define-public crate-xmtool-sys-0.1.2 (c (n "xmtool-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.17") (d #t) (k 1)))) (h "07pcsd4nwh8vw8c83pd1a52a3b198zsgpx7xgfq0z7z3z7iky79w")))

(define-public crate-xmtool-sys-0.1.3 (c (n "xmtool-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.18") (d #t) (k 1)))) (h "1877b2bqvxnaq3m1zpbmrhb35l4n372kz53yys9rj60rc7dggx6z")))

(define-public crate-xmtool-sys-0.1.4 (c (n "xmtool-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.18") (d #t) (k 1)))) (h "02c5bvn40cib31bx04phaxdyl8aa3szmxxizd6zjk4s88r2jnmnp")))

(define-public crate-xmtool-sys-0.1.5 (c (n "xmtool-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cyberex") (r "^0.1.26") (d #t) (k 1)))) (h "1p553j0kln1haw4wa3xqvlahh2yjzjirw9a4wzas2sizdv1n095i")))

(define-public crate-xmtool-sys-0.1.6 (c (n "xmtool-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cyberex") (r "^0.3.4") (d #t) (k 1)))) (h "03k1jkj12vndbzshn7x1fmy5wfkpjy1q855vval1spymrrkyna75")))

(define-public crate-xmtool-sys-0.1.7 (c (n "xmtool-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cyberex") (r "^0.3.4") (d #t) (k 1)))) (h "1lg793yhnwl0jv921q89r8mdmyz8qvk5l32bckaxdf8gnigsvwrs")))

