(define-module (crates-io xm ac xmachine) #:use-module (crates-io))

(define-public crate-xmachine-0.1.0 (c (n "xmachine") (v "0.1.0") (h "096klz0brdyr3f28kws7c6pridi4bh6lzp7s34a6mgmdzr9gk3y7")))

(define-public crate-xmachine-0.1.1 (c (n "xmachine") (v "0.1.1") (h "0bgy3xfcvm95lkn23bsd7y9y50gsrd58n7fwmwvdh6rf063hgvxc")))

(define-public crate-xmachine-0.1.2 (c (n "xmachine") (v "0.1.2") (h "1b7q6apkdl2f38gf186yy4hl0n6wy5mz5yn221jrr71wg96zgpzv")))

(define-public crate-xmachine-0.1.3 (c (n "xmachine") (v "0.1.3") (h "0fkj09yjazbx04wvr426ig790261w4hv875863lgp1wkwz6gxnbr")))

(define-public crate-xmachine-0.1.4 (c (n "xmachine") (v "0.1.4") (h "071v65jv452c0l8l5mz2d2gn8fqsk4vl9pzrmv13gdg4p6pchcsh")))

(define-public crate-xmachine-0.1.5 (c (n "xmachine") (v "0.1.5") (h "01xzszgh1r30xndbnhkgpwmjrk7myxrrf0gnwkyp74y3ssxa0jrg")))

(define-public crate-xmachine-0.1.6 (c (n "xmachine") (v "0.1.6") (h "1lv6z3sgdqhyjnllpqj878bv92xn5c9qlrk1x9xv06psbxaj5kxk")))

(define-public crate-xmachine-0.1.7 (c (n "xmachine") (v "0.1.7") (h "0a5m6in2in61zic23ykh93lxl6jqd490fvh3x7jas0b5lchskb89")))

(define-public crate-xmachine-0.1.8 (c (n "xmachine") (v "0.1.8") (h "17cb8f9cw9jvh475grmgif7j85qld559n1bx5r1vpyn7hf6rxr7i")))

(define-public crate-xmachine-0.1.9 (c (n "xmachine") (v "0.1.9") (h "12xd0xp5n8xdm4cgi7pf3jm4k8c7sm0zxsj2fj41l82sa5zgf97b")))

(define-public crate-xmachine-0.1.10 (c (n "xmachine") (v "0.1.10") (h "010a27km00naf4dn91snjjxi5jg84s3hz5cifyxd7zc5d8wg89ag")))

(define-public crate-xmachine-0.1.11 (c (n "xmachine") (v "0.1.11") (h "0pm0djfwm3mnyg3xas0jz3sllr37zdbxwn22dkm42g1blcjib8km")))

(define-public crate-xmachine-0.1.12 (c (n "xmachine") (v "0.1.12") (h "04a640prm7y53ybwnlzzazq0saax9yiqm3a0zp9h2qxkyra3pvh0")))

(define-public crate-xmachine-0.1.13 (c (n "xmachine") (v "0.1.13") (h "02s955g2l2103nq04mci1hc659szpj9z0aq3h7gbbjflr4h85mlj")))

(define-public crate-xmachine-0.1.14 (c (n "xmachine") (v "0.1.14") (h "0ikq6gvs90j5mgcq1bnchsb3gk01rpyp5jafsxqh467n3xnllw6l")))

(define-public crate-xmachine-0.1.15 (c (n "xmachine") (v "0.1.15") (h "0wzingnm2v1qk0qk03lnwhknn9slzr7d7xsjd8zb9sbvf51fmnvc")))

(define-public crate-xmachine-0.1.16 (c (n "xmachine") (v "0.1.16") (h "00i5l2hxm8f951gzh8l1k79liprglhf0dcsmmgb4108v539crgb2")))

(define-public crate-xmachine-0.1.17 (c (n "xmachine") (v "0.1.17") (h "1fy0qj7s8g84mnrx8173xjxhj5ngfw2h20vvppigfmw6sinmzjp8")))

(define-public crate-xmachine-0.1.18 (c (n "xmachine") (v "0.1.18") (h "1gfs2bjfkwfvp3c56zmwhkxbhlw9b45h43iqn1drk2dx66cxr063")))

(define-public crate-xmachine-0.1.19 (c (n "xmachine") (v "0.1.19") (h "16739nxgsy3v35wpxlwrhpzcq2nx65zy8pak47jx9pxchif0njh2")))

(define-public crate-xmachine-0.2.1 (c (n "xmachine") (v "0.2.1") (h "09w6jfhf2igb49rfzq8imb7d6740aihh0xnxjxzw95xy2gvyhbs5")))

