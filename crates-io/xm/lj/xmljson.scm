(define-module (crates-io xm lj xmljson) #:use-module (crates-io))

(define-public crate-xmlJSON-0.1.0 (c (n "xmlJSON") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "01809qkamqpsjv3qswh9gl9z0sd5qxqsshrmnf63ra7kgrng0vh8")))

(define-public crate-xmlJSON-0.1.1 (c (n "xmlJSON") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "13p4xglm9d1rkb54rmhldiv6padrldny5kz749wh7x13ibywvprm")))

(define-public crate-xmlJSON-0.1.2 (c (n "xmlJSON") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "1089zpaz8ncxfi0qgbngssiy2c1s6ckgfmajbn89rxy48chl790w")))

(define-public crate-xmlJSON-0.1.3 (c (n "xmlJSON") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "xml-rs") (r "^0.2.2") (d #t) (k 0)))) (h "14dpj74ks44lk0l0yph968xjdv6c8rl8a20za58ykxmyvbh4hgcq")))

(define-public crate-xmlJSON-0.2.0 (c (n "xmlJSON") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "1d2rya045b19v5558svj9ssb8jfjfmfsfv9hlzbb8zl0l6kxkpfc")))

