(define-module (crates-io xm c4 xmc4800) #:use-module (crates-io))

(define-public crate-xmc4800-0.2.0 (c (n "xmc4800") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "10n4lbwhx59dw7rwwy4m1zi9ixz5qkbg76g02wx45743m74p41aq") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4800-0.2.1 (c (n "xmc4800") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "00l6fhf28zylzidxgfxrjb7p2ki6jfbb1h006j2yc5dcvks3pp70") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4800-0.4.0 (c (n "xmc4800") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0i37kz4yv4hwkk7l2257gm9dgmspj47wkc10hdjdm2agqahd6pnb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4800-0.5.0 (c (n "xmc4800") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16d39vryvpnplf0yj60i105vcrl339b1fhkyjdycm28di8gbpdzw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4800-0.6.0 (c (n "xmc4800") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02sjmykpbf4y70kabvbzfr848b9x89jkjx46i1c112jb7yccl4yw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4800-0.7.0 (c (n "xmc4800") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1prmb4vdxkh5js50czwvi5srkvl0qbrnjb89bxrrch7c9mmk8zbh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4800-0.7.1 (c (n "xmc4800") (v "0.7.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "09xkgqq5irv1428fl19z5yl004b7r5lmxvm4c57vj2yk923hf3gj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4800-0.7.2 (c (n "xmc4800") (v "0.7.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0k6g5csaxpa057d4hcwd1j44nwdbrm9xiiaaw2ad2i732iqfjipl") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4800-0.8.0 (c (n "xmc4800") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1x1vxfg5wibk4ylqbg17a4q12vyk5wzi2zlhinisjyrqj1m304w5") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4800-0.9.0 (c (n "xmc4800") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ci2l203lmaqlrblgz621xz018bijbrap7c9jizsmn2p3cn0xfjy") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4800-0.10.0 (c (n "xmc4800") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gqzd8vhnlyrwflrvlgnnhb3z1bbxwqjmm02gvvf2iy31g9pv8xf") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4800-0.11.0 (c (n "xmc4800") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07jg090mpn9phra8c5dxj131cvqsf9mn0ic9amqr7cx3rdn6h0h7") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4800-0.11.1 (c (n "xmc4800") (v "0.11.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "078rgwbmisq7hpjsvjn1ni6lrwxwmnqqsc61l11ycs4wa7y2qnnl") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4800-0.12.0 (c (n "xmc4800") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "193hz5mkvay21ynjr26frylwri6hgvlzj3177ydpw9z682vgnwmf") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

