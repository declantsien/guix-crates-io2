(define-module (crates-io xm c4 xmc4700) #:use-module (crates-io))

(define-public crate-xmc4700-0.1.0 (c (n "xmc4700") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11xwz006hlb9hxy63bd8gd559c3c7kdxm6zjd20by80629pgbykq") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4700-0.2.0 (c (n "xmc4700") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1rwxaa4mk0959mmss1ymlldgmkfwhjdg6js27z8cc576riphw724") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4700-0.1.1 (c (n "xmc4700") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vschzwwdi3grfzkv3hhcz99afxdxa5ckxawrszgb10h368ix4lx") (f (quote (("rt" "cortex-m-rt")))) (y #t)))

(define-public crate-xmc4700-0.3.0 (c (n "xmc4700") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19v244dmp5c0njwfwikmhpw2h8gw80jbqmg0xk8k9qbsfs4p781v") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4700-0.3.1 (c (n "xmc4700") (v "0.3.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0md5436hjmnk915gsg3db5a4458g90gv96vpjdhl700rqk3vw0x9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4700-0.4.0 (c (n "xmc4700") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0psvi3vc9f5mk5gz1s7ap0bqa6sa9aawcyka57zdyprgbfavcpfm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4700-0.5.0 (c (n "xmc4700") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0pz8g5fms6rd5nndnpfiim6ybmxs15n8faidkpjvr99pq9mzyqhy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4700-0.6.0 (c (n "xmc4700") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1phj3wm4ga0fn213kah01m3ppxm7vhyyqc8axjnkjljic4hv4b8d") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4700-0.7.0 (c (n "xmc4700") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13gsq52bc1avll3jjhs2ysy9dpdgqvxgi6wwvm66i7x0qjaz4fnf") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4700-0.8.0 (c (n "xmc4700") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "18gnpwbf8hqz6r7wxxkjms9ll8yq0h09mgn46n9fnwql1qzymg55") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4700-0.8.1 (c (n "xmc4700") (v "0.8.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pzxxyag26bm5mh5r9lm9hq9hddhy550z7ks7paivap9ds7yw757") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4700-0.8.2 (c (n "xmc4700") (v "0.8.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1szs2yipgx1h9hbkdk9jpgl151w1z3bf9wq2pcmw7mls7rcxzb1k") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4700-0.8.3 (c (n "xmc4700") (v "0.8.3") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00njn1g82ra0lin9h6cq73kcbfrld280n20ivvnrmqrwaf17nyzm") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4700-0.9.0 (c (n "xmc4700") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00wfil1cf5c439asi0zj6gk5x1wpx5a7iaqsyila8yg3h0b9m4v8") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4700-0.10.0 (c (n "xmc4700") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ysipl84sqmji66dvm0dwj1wq7cwv7nk6s1bs0dky8xwz599cbjf") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4700-0.10.1 (c (n "xmc4700") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0hn8wygcjv7vh1sbhq16wxpq9pn66v6h1g7ip0kzdlhqb667i3xn") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4700-0.11.0 (c (n "xmc4700") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ydin5shq747vd051jgsiknlpld0gqjfl153xgw2q6knckn5wzp7") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

