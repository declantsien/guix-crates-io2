(define-module (crates-io xm c4 xmc4300) #:use-module (crates-io))

(define-public crate-xmc4300-0.1.0 (c (n "xmc4300") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0m0fh07vwjb0shk764rmhcmpckws67zdbmfg9jy5k1ky7sayjgai") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4300-0.1.1 (c (n "xmc4300") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0awfd6x73il3cf7361qmgy2sjiqwba49r7mblf6hhf8c4wwc8am0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4300-0.4.0 (c (n "xmc4300") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0kmac5hsgh7cs9r6lwnvljr425s7kzlhl16dm607xzpls3yglvin") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4300-0.5.0 (c (n "xmc4300") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "038ssxni34wafqfr9yzx432y4pqfb7bq79ppjd1c2ryldlpsbgy3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4300-0.7.0 (c (n "xmc4300") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0mspxil6jb8cvsvinwaa377nhcn755lzyqmbzzc2h2rrzx9i0za4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4300-0.7.1 (c (n "xmc4300") (v "0.7.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qzf4k9pb4j1wwj6szh46zr9jh5vlspbmx3j5lwm7ndi5c1azn9n") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4300-0.7.2 (c (n "xmc4300") (v "0.7.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1lgia0sgaq45jm4kw5ibf6ybx1g56g9znwavr6l6alm360ab4hyj") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4300-0.8.0 (c (n "xmc4300") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ddzmhy2bvchrqpa6dsdvqxs8cv0jyfms6pw7dqxysdrr9v6k1il") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4300-0.9.0 (c (n "xmc4300") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fzbiqa2izl3qlna4nyadri3xfhx8lwmig5g01pdyv9mcplybd7r") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4300-0.10.0 (c (n "xmc4300") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13qqr2f144mbdchvzy0crncl9b97hl5k2r4vvyc9ifhv42s1rdhk") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4300-0.10.1 (c (n "xmc4300") (v "0.10.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19islcjrccr2gsfiay2mvia9bxqli8zgxbl8k22m6wmr2hyi12a1") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4300-0.11.0 (c (n "xmc4300") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "11kl7wdgs3wbs1rjiazzxyarwi20m2rc3dqwl3zavasn78szpy98") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

