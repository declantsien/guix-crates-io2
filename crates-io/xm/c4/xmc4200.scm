(define-module (crates-io xm c4 xmc4200) #:use-module (crates-io))

(define-public crate-xmc4200-0.1.0 (c (n "xmc4200") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0j115b6ccbycn1bmzycanqv2k8npql6qg5zrmpzdr4kfddki9rfz") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4200-0.1.1 (c (n "xmc4200") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ha2lbnnsrj9f7zi1qp1p9chrcvazip0005dc5psgizgk9vfbi71") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4200-0.1.2 (c (n "xmc4200") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1yyjcsv4c9687nv674jasab2xla4m4fzd3y005bzf8bzqd42qfl7") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4200-0.2.0 (c (n "xmc4200") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0sflngpjs55zm3kmih10wi0ymwnmm2mj11rqc7wdk49hvxikrij6") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4200-0.2.1 (c (n "xmc4200") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0w9n0d0xajkcg6hgf0ssqicbhaj1smg66ik2i70yaq8y2b066b34") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4200-0.4.0 (c (n "xmc4200") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16q23kf175dwxs141hcv3qif7gf0imdcwidd0nysbynfrdnjsf9b") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4200-0.5.0 (c (n "xmc4200") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16nn6wabqm0x6swy4bqrfszp8pdkkjxgnqxwxn3ndp3pxnyb7nmm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4200-0.6.0 (c (n "xmc4200") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "195jfx55z3kxxkacpp8q3bcq2drx5mkv7789va6dvc67n14nfqc9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4200-0.7.0 (c (n "xmc4200") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1a34ikaafgdkxkyn72gh27bmvminn694lwm8968kxg8z5b0bhx44") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4200-0.8.0 (c (n "xmc4200") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xypnrihvk8bj64ya48abvv2d5ys6kz38yxd4cwn73dkg3nbj3bc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4200-0.8.1 (c (n "xmc4200") (v "0.8.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "134sgh0hc068vmzqvfdkq48i0375wp84k9dfldmiabsbgfcwkf5w") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4200-0.8.2 (c (n "xmc4200") (v "0.8.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "041khxi3cy62im1vyv7na3ahb20wlsjrikghsfbg015854mlhyq1") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4200-0.9.0 (c (n "xmc4200") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pclnfcv64n57l93shf8krdvzsfqp42zzhgkcf68kp5cabhiqq94") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4200-0.10.0 (c (n "xmc4200") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xbwy7l6qf3jm2y43gy47g5rpw5nw0bjlfp4kafigvnjpp74aw1y") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4200-0.11.0 (c (n "xmc4200") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1nmwj8ihscsa9y5m62ngm926ia66li2d59aafw1568r22bq4gwps") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4200-0.12.0 (c (n "xmc4200") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0gqsmypzxi92p13r5fnsnqdpn16w317c6qfz856i58rzw4sp8r5k") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

