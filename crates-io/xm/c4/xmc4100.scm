(define-module (crates-io xm c4 xmc4100) #:use-module (crates-io))

(define-public crate-xmc4100-0.1.0 (c (n "xmc4100") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1h30znrfsb19fwykr7v1pgi9jjvb5r8q13jd5hngr47jw6dl761l") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4100-0.1.1 (c (n "xmc4100") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0n1x76pivagr99x80pqag2zxsjlbk0vnp8adsxzmb2adnvga9dni") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4100-0.2.0 (c (n "xmc4100") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14pr99rvgam1v7wqj3dbx1z19l1il84f7lzhlirr2cwrp09xqhpr") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4100-0.3.0 (c (n "xmc4100") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "03kdwg1m7bk14l3m6p8lljhvvkazyiagyx2vw49i3d54z508zvqv") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4100-0.3.1 (c (n "xmc4100") (v "0.3.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0xk7rr38sl4nssjmh1kjhbiddq1yr23nk048raf7z2q5ghqq4bzq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4100-0.4.0 (c (n "xmc4100") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1d26hyrqhvvc8x0s2qx5h1jcv2a5hd5jdmg6678vjdlh81bsh97r") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4100-0.5.0 (c (n "xmc4100") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0h3f9nhyxw81xcfk12zg1i7923adqbsldhi0hzr229brc1xr9v1j") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4100-0.6.0 (c (n "xmc4100") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "093ha9vgqkanzs4xi6a9nrmgxaqlr5y29cv6lrcyjyh1dmavh0qm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4100-0.7.0 (c (n "xmc4100") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pkqgq7bwvb61wrms8v0nzf6y19mh6nmvh1bw79ycczixhpaycpm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4100-0.8.0 (c (n "xmc4100") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kv3h39nqdsfyb905m42zp0i209iwnnrqf81fpylxhm0li1fwhmr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4100-0.8.1 (c (n "xmc4100") (v "0.8.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13brlm1b5p1n0wzq18z125ag2d2y4j82q0vw65lsnwzsajxa9z0y") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4100-0.8.2 (c (n "xmc4100") (v "0.8.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10zlcak9gqd5yygchwg89i1raqhwxvvk4skfdwhggs4h55rffmna") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4100-0.8.3 (c (n "xmc4100") (v "0.8.3") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0jqfpvka3yxs5rvjnyqf021y9y7aqqm50jxy5ds3inl7cj2321f3") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4100-0.9.0 (c (n "xmc4100") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pcbvpb6n2hfbp5kskg7klfv6dfngpznr8pxmljlln21lxbr7axf") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4100-0.10.0 (c (n "xmc4100") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02srj32dqc1jxvr98xf8vdrx80lv9nslfz4z019a505s22d1v8a8") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4100-0.11.0 (c (n "xmc4100") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z0n6dzz8z9i5cma86cj9jkzn8dg3sirjhbmbsgw1wia4mw3x02r") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4100-0.11.1 (c (n "xmc4100") (v "0.11.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0vsnz546rfrc0lqwgzjb4z5b5rfbw27q1jygj8rkx25nxcyac2q7") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4100-0.12.0 (c (n "xmc4100") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08c6g5pkl6rh2kjh3npr9qx2vj04q148c9djgfscqr8bsigq8pjl") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

