(define-module (crates-io xm c4 xmc4500) #:use-module (crates-io))

(define-public crate-xmc4500-0.1.0 (c (n "xmc4500") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gxv8p2qkyvc9ym3r013sarihb1wa5fhbwbrq062kzlxcp9bvi2h") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4500-0.1.1 (c (n "xmc4500") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1chq7s3mh1gdm5xf5rc1b8rx067r9dc6hab6zpvlif4x505vqyi3") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4500-0.2.0 (c (n "xmc4500") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1sj274ia7bd9yyb3kqv1mcya1iglsib28dbavwana0dp2x3ig052") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4500-0.3.0 (c (n "xmc4500") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19jjhxlc1dl1bq02762ngvbl8b05i6hydibw9805fglmp296bqz2") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-xmc4500-0.3.1 (c (n "xmc4500") (v "0.3.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "15cfmhjrbk9cf1zcnkka8v5q1x8s4dzz48g75603kdr2jrn31vj9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4500-0.4.0 (c (n "xmc4500") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "11q4asg8ph96k8xhdkdz17zgivhp5n1v6l6k91xzcafr8ism01hw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4500-0.5.0 (c (n "xmc4500") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0077b76qxkra8ayl8b8xjrk6504c4qbf8fzy312mbw2l4rffsxdc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4500-0.6.0 (c (n "xmc4500") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pf6ivf2ivaqyig1wx333qbf7gg8gp8rs23b52s86fv8j0ks3dvg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4500-0.7.0 (c (n "xmc4500") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "149xsdfrr3yzq4fdyy9mw10gjkcbhlxqd40spk9k7ylqfdw323jg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4500-0.8.0 (c (n "xmc4500") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0acgi9igi2apivpqs4smvr3110d69d7j06vwfn963540621ibj58") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4500-0.8.1 (c (n "xmc4500") (v "0.8.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mxcg5w42q2lrlgf2jxsbm8zqa8i9j3g8z9zcsr6kbzm5g4vlv6c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc4500-0.8.2 (c (n "xmc4500") (v "0.8.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1jz3j8fzi5ld24fw4csxjc629cqvpqna4dimxfwc1igyhb6gjjvs") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4500-0.8.3 (c (n "xmc4500") (v "0.8.3") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1yn8gvd0rq280h0685byfnfv5cywaaanm0m0y4mzagxv6k4xqrkl") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4500-0.9.0 (c (n "xmc4500") (v "0.9.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06dj4z9427xhaw66321b058z7gx5gz969kcqd1fxaqwlgjmyhhap") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4500-0.10.0 (c (n "xmc4500") (v "0.10.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0j2jfw9rdsrsdjk1w0pdq10fjplmhn0q3d9b3y9xhjrmpk3aj63a") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4500-0.11.0 (c (n "xmc4500") (v "0.11.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07gqp6zaq0i7jg0c8w88xd4932qcdgyi971d84cf3fn96hr9qj5g") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4500-0.11.1 (c (n "xmc4500") (v "0.11.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0wmgdq5gwfqnj4ql3r62gmbmaqcpn4mc37d0pix1kqld77cpnsj6") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

(define-public crate-xmc4500-0.12.0 (c (n "xmc4500") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "062llh7lj5ph6w110n41mjqfn7gf7wdr47965g1s3ikij06wgqmz") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section" "rt"))))))

