(define-module (crates-io xm ls xmlschema) #:use-module (crates-io))

(define-public crate-xmlschema-0.0.1 (c (n "xmlschema") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0l2ks1cid9bpmnn26qwkrc5wzj4d4dn2f51970adaz32m7ramfcd") (f (quote (("default")))) (r "1.67.1")))

