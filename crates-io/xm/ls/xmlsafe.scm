(define-module (crates-io xm ls xmlsafe) #:use-module (crates-io))

(define-public crate-xmlsafe-0.1.0 (c (n "xmlsafe") (v "0.1.0") (h "0h4vjh35zdrsc15j6fm4p4smaqln3f3498awqhgjm6dzjq9y3mv8") (y #t)))

(define-public crate-xmlsafe-0.2.0 (c (n "xmlsafe") (v "0.2.0") (h "0g3iq9akw4sqq4wk77awar6c0r1fi847fgf62f42qx6ga1fqrcri") (y #t)))

(define-public crate-xmlsafe-0.3.0 (c (n "xmlsafe") (v "0.3.0") (h "1cb5ych6pkmgdk1l9pf9kd0yaa953913afyc9yfjk3bzmqlrwckl") (y #t)))

(define-public crate-xmlsafe-0.4.0 (c (n "xmlsafe") (v "0.4.0") (h "16qdp9mc2x4y1x3777520rwnkss1ilrdqj5907kv6vmdrn3c67mh") (y #t)))

(define-public crate-xmlsafe-0.5.0 (c (n "xmlsafe") (v "0.5.0") (h "1yq6afgbg1x3n3p28j4pd86b22sbc20swvsdqapk6z2d5xvai29f") (y #t)))

(define-public crate-xmlsafe-0.5.1 (c (n "xmlsafe") (v "0.5.1") (h "12b69vlffmr1jxy0ccm4clvr5zcb22psi85p19wj52rpmj63p87b") (y #t)))

(define-public crate-xmlsafe-0.5.2 (c (n "xmlsafe") (v "0.5.2") (h "1c59s318mk6nwcj1fqq4dj83bs7p4c4jxia9xsn3krbd5sniz4pq") (y #t)))

(define-public crate-xmlsafe-0.5.3 (c (n "xmlsafe") (v "0.5.3") (h "0y2rix68ygxldsra4rq2bz0qh4ljpzl5yb09cxwpyhmxvcrrkyav") (y #t)))

