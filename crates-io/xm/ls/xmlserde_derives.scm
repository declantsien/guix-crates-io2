(define-module (crates-io xm ls xmlserde_derives) #:use-module (crates-io))

(define-public crate-xmlserde_derives-0.5.0 (c (n "xmlserde_derives") (v "0.5.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1lf5rm9b6zibi8a6nb3wpqwh8nynhjg827wqzkzmzy9kbzb21fqa")))

(define-public crate-xmlserde_derives-0.5.1 (c (n "xmlserde_derives") (v "0.5.1") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "169i8hwmh1j2rkbb1cj8v8b48g113ncyyhkvi07vnn9qyk9bj85b")))

(define-public crate-xmlserde_derives-0.5.2 (c (n "xmlserde_derives") (v "0.5.2") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1asiid3a7mvxg3f5f41x8c0jxy0kgs3zad9g87ia6fxpzp7d1j8f")))

(define-public crate-xmlserde_derives-0.5.3 (c (n "xmlserde_derives") (v "0.5.3") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0z8516glhw0imgd9fgllb6nyd4q1qy4fr2c1wzyyrgx5gy6pdbnx")))

(define-public crate-xmlserde_derives-0.5.4 (c (n "xmlserde_derives") (v "0.5.4") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "18mgz1k1j45jcdclmh7mhb61ii1mr2vay71vm8c28n9509qm06w8")))

(define-public crate-xmlserde_derives-0.5.5 (c (n "xmlserde_derives") (v "0.5.5") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0g7xfaahhra0bk1h2sz13mcymsyz657yk6fxqf4sjjssg6c1d0hb")))

(define-public crate-xmlserde_derives-0.6.0 (c (n "xmlserde_derives") (v "0.6.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1bwlwira5d0nmy6hs538rrv4rgpc8r26grs1945l2dfl542pparr")))

(define-public crate-xmlserde_derives-0.7.0 (c (n "xmlserde_derives") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1snij5qhyrm0ayh55jrgh85h380iv37ynh6ahjlhcswvgvzvvysz")))

(define-public crate-xmlserde_derives-0.7.1 (c (n "xmlserde_derives") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "07qvn6vakgcchxljzf0mhsx3znzysh1gzzqbv0cd78dp0vyb0867")))

(define-public crate-xmlserde_derives-0.7.2 (c (n "xmlserde_derives") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0snjf31nnqibvg1nlqp20i6zdlvi6vwha70yh7irwfqiblq1wszy")))

(define-public crate-xmlserde_derives-0.8.0 (c (n "xmlserde_derives") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0c7pplpil8wf4cx6cd9q9ak4snzf4734js1cb4ff7l3f7j9kap2f")))

(define-public crate-xmlserde_derives-0.8.1 (c (n "xmlserde_derives") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0hk3l7xr5qjmp2gf2j3r2fdfg9214qjpinw8frv5yq5yd27h5fg2")))

