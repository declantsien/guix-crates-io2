(define-module (crates-io xm ls xmlsec) #:use-module (crates-io))

(define-public crate-xmlsec-0.0.0 (c (n "xmlsec") (v "0.0.0") (h "156p8v8yqd45q5zihvk7jng85sxz6kjw4xv61w0g5jmcrnnz4w1c")))

(define-public crate-xmlsec-0.1.0 (c (n "xmlsec") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libxml") (r "^0.2.12") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0f2c4hx38h091qycp11cyvyi8lb2qdjlv0lnbyxx37d1sm5gw3ws")))

(define-public crate-xmlsec-0.1.1 (c (n "xmlsec") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libxml") (r "^0.2.12") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1zddci0gc2vkb66snqhb3ij2dw9mfwbmzppkpglc1321kjjsa9m4")))

(define-public crate-xmlsec-0.1.2 (c (n "xmlsec") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.2.14") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0niwj4g1r8r98lcwn6337cnn9pajvd284v8d0wsfr6x59jk0ghwv")))

(define-public crate-xmlsec-0.1.3 (c (n "xmlsec") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.2.14") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0miigjir8zbxll7ac6ghs203jlkix5lb8fdw4zja3nhylhn99nba")))

(define-public crate-xmlsec-0.2.0 (c (n "xmlsec") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.2.15") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nk32srm5rylji970fvlbvxr0kg05wgf78ydqdk78knwqpzsz7kp")))

(define-public crate-xmlsec-0.2.1 (c (n "xmlsec") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06rfh9x50p91s3xpz8yisdb2qa36dlhnzzc1534bw6nywkhrj5g5")))

(define-public crate-xmlsec-0.2.2 (c (n "xmlsec") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13qh9h80iw0siczvy8gx7nshhd2j9b3c0wfwp21hflb6y3r1784k")))

(define-public crate-xmlsec-0.2.3 (c (n "xmlsec") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pzia6cwkcypnmiiv1mvnxpvlhhh01y1rlcv4fy8w7iwzbb2q5l5")))

