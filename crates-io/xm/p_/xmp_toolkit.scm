(define-module (crates-io xm p_ xmp_toolkit) #:use-module (crates-io))

(define-public crate-xmp_toolkit-0.1.0 (c (n "xmp_toolkit") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "016c8ckyb00zyv7fvmbr3fxw238nrlb45xkkdy70pmbfvm1a1qkw")))

(define-public crate-xmp_toolkit-0.1.1 (c (n "xmp_toolkit") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "10cngbdw5q2795cl01p9932r0kyrncdlf9fl6kkjv7r68k0lxz4q")))

(define-public crate-xmp_toolkit-0.1.2 (c (n "xmp_toolkit") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0q6pqcf0g9qxs4sybd38jpxaw07wb6i7ljz8xsgbixy7ys5lp56w")))

(define-public crate-xmp_toolkit-0.1.3 (c (n "xmp_toolkit") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0g5iwsm5v2nsv7rqy0f4p5bp93wk3h4iisinf44118d9kg334axa")))

(define-public crate-xmp_toolkit-0.1.4 (c (n "xmp_toolkit") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0ppqr7k2l83rn104afrdm6krkxds3mld9xfc38bj51y5rb3ri85d")))

(define-public crate-xmp_toolkit-0.1.5 (c (n "xmp_toolkit") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0h3a9k1scgxgj9walgckdj5hglvr1sj7ipnpbc9r584429pq8hmi")))

(define-public crate-xmp_toolkit-0.1.6 (c (n "xmp_toolkit") (v "0.1.6") (d (list (d (n "bitflags") (r ">=1.2.1, <2.0.0") (d #t) (k 0)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "fs_extra") (r ">=1.1.0, <2.0.0") (d #t) (k 1)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "19qcaflwvkqqq37bp3yxmw6nvqkdh9mcpcmm54v584w606ivqh3d")))

(define-public crate-xmp_toolkit-0.1.7 (c (n "xmp_toolkit") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1f8kkb3y4b1lyfx2yn0icmbd5i4z0i47fnwaix8hz63k5c4gfqvf")))

(define-public crate-xmp_toolkit-0.1.8 (c (n "xmp_toolkit") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1gmqnsr13j6zrw8pvkvpflvvmx66gqsds5pg3gpgb9pmqsql4vcx")))

(define-public crate-xmp_toolkit-0.2.0 (c (n "xmp_toolkit") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "154qfazykp54qwrb3v42mvd4f9n7ynif7j6chmqi08k9awg1h12l")))

(define-public crate-xmp_toolkit-0.3.0 (c (n "xmp_toolkit") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1csv6cq0a70w45bvj7hjihvv651hzyda7ycfqjcc8m8i5cfbxyy5")))

(define-public crate-xmp_toolkit-0.3.1 (c (n "xmp_toolkit") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0lcs99yqf9fpv1524xrclzxfszqsxdn3p6rqzd7i8lm8h9d6139c")))

(define-public crate-xmp_toolkit-0.3.2 (c (n "xmp_toolkit") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "140csnq65nzk0irskdxx4grxap0kfd3bd5ak941qjgxpcgkmw4fh")))

(define-public crate-xmp_toolkit-0.3.3 (c (n "xmp_toolkit") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "186gri30i1n4psbmxx2a84q34zh8ybch0jvjk9nflfpk6z4s8aai")))

(define-public crate-xmp_toolkit-0.3.4 (c (n "xmp_toolkit") (v "0.3.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0ddn0n14ankj7ahbv4r9yqvdb41zsn7xi5w07lxfmgczw19kgb0k")))

(define-public crate-xmp_toolkit-0.3.6 (c (n "xmp_toolkit") (v "0.3.6") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1dznbmgqwgfklnmqqis7bv77cfd0w91hkba5d198drhxy8r388p9") (y #t)))

(define-public crate-xmp_toolkit-0.3.7 (c (n "xmp_toolkit") (v "0.3.7") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "13cfqfgi88q8blirgv5czh8v0kn6gg72v08gqkmjmviblkhx5zxx")))

(define-public crate-xmp_toolkit-0.3.8 (c (n "xmp_toolkit") (v "0.3.8") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1yqvbr181c6im9pv0kqpv8a6hkyywljrmsp753qrpvhmbssnndxc")))

(define-public crate-xmp_toolkit-0.4.0 (c (n "xmp_toolkit") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0pqdgpbpnf9gjqqi4d085sbvcy7ap8vh6z5qhwx7wawyximqch26") (r "1.54.0")))

(define-public crate-xmp_toolkit-0.5.0 (c (n "xmp_toolkit") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n23093pppy5w91b0d7y258ln9jcfhnh75hci80cynpmd6zdyjk2") (r "1.54.0")))

(define-public crate-xmp_toolkit-0.5.1 (c (n "xmp_toolkit") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x9q5ylc1ph94r4hmgi4lrns1321wx5h8n1h1lfhh5hbx1b0j48n") (r "1.54.0")))

(define-public crate-xmp_toolkit-0.5.2 (c (n "xmp_toolkit") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-ident") (r "=1.0.1") (d #t) (k 0)))) (h "0hpwh7sc2ijiq7g3bg3kl2hcxy1sq06fwisxrv4lf73darggrzz9") (r "1.54.0")))

(define-public crate-xmp_toolkit-0.5.3 (c (n "xmp_toolkit") (v "0.5.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jcr7y91hx3f5rq2hkwwifrqp9100j3gdqjyywlb0jw297w9w448") (r "1.54.0")))

(define-public crate-xmp_toolkit-0.6.0 (c (n "xmp_toolkit") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cjavc1qa78d3kkxf7jpb0cm4gq922ddv7hf5sqm5zbjkh03kii2") (r "1.56.0")))

(define-public crate-xmp_toolkit-0.7.0 (c (n "xmp_toolkit") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16m06c97i43w6g9p2gzfq2xf4fz7h91kfbvbal136fkkd5gvd5xp") (r "1.56.0")))

(define-public crate-xmp_toolkit-0.7.1 (c (n "xmp_toolkit") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zbgkfmrj1l9nwpnr8x92mllcjzxbhz0q7i19vjynb51ppzwy4mi") (r "1.56.0")))

(define-public crate-xmp_toolkit-0.7.2 (c (n "xmp_toolkit") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vbkzxm3frgg31fi6zs55j4rg00rihi5s4nc1bjj6lq7qidlm5qv") (r "1.56.0")))

(define-public crate-xmp_toolkit-0.7.3 (c (n "xmp_toolkit") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dk6k82zfbm4w0cyrsbg05qj8sq34maq2v9dsx68ahxkll6x0gjd") (r "1.56.0")))

(define-public crate-xmp_toolkit-0.7.4 (c (n "xmp_toolkit") (v "0.7.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zmfbgfshvf792p8yvdqva196h8x340m9n1s69sniw1mja92k63k") (r "1.56.0")))

(define-public crate-xmp_toolkit-0.7.5 (c (n "xmp_toolkit") (v "0.7.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s7ppx6i0cn4phgxc0g0yxxpbgqkfyqswi4xmh56jxrpdzb6i51g") (r "1.56.0")))

(define-public crate-xmp_toolkit-0.7.6 (c (n "xmp_toolkit") (v "0.7.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zvmyiay807z1yj36p38spgdirf29bp5x5gchh9lm49v7vrqapzm") (r "1.56.0")))

(define-public crate-xmp_toolkit-1.0.0 (c (n "xmp_toolkit") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19m7wxzdmff26q0zg8nmp7n2q5dllplwr2k4h4ry3gma0g56147f") (r "1.56.0")))

(define-public crate-xmp_toolkit-1.0.1 (c (n "xmp_toolkit") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0isg0xanhv29fh2ziwnd1yrsls89p0w50359lis9705nqb59i9c5") (r "1.56.0")))

(define-public crate-xmp_toolkit-1.0.2 (c (n "xmp_toolkit") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1na589lv08vp1pv7v7wc0n22sdvlcyr1ypvfm328pd8dvl2m1qd0") (r "1.56.0")))

(define-public crate-xmp_toolkit-1.0.3 (c (n "xmp_toolkit") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03d56avk0nvhx6mp9zx0xs3hf808wkcib90n8bp81vmih4xzjhzw") (r "1.56.0")))

(define-public crate-xmp_toolkit-1.1.0 (c (n "xmp_toolkit") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c5vb20slcd73zlrnyyr1xx0nwim9ikxlrakba0f4fxln9jgyxbl") (r "1.56.0")))

(define-public crate-xmp_toolkit-1.2.0 (c (n "xmp_toolkit") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10n86kjwriqigacmpf3yb57rmxi8fvwsn7glhv2r0g1bvvjq5c07") (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.60.0")))

(define-public crate-xmp_toolkit-1.2.1 (c (n "xmp_toolkit") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jkhhcgf1yax506wjqy4l0i9450hamv0k3cy1njjp2dw7a4h7qkh") (f (quote (("crt_static")))) (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.60.0")))

(define-public crate-xmp_toolkit-1.3.0 (c (n "xmp_toolkit") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wwiyhhl3xvivf8kw0j5d8gc853nb56a92qakrfk3g4y4phvqkvf") (f (quote (("crt_static")))) (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.60.0")))

(define-public crate-xmp_toolkit-1.4.0 (c (n "xmp_toolkit") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15dy0jgbki2fqda7rs5aykzsavsc8pa4qgk9mzh3ri8iyjgdx1cc") (f (quote (("crt_static")))) (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.64.0")))

(define-public crate-xmp_toolkit-1.5.0 (c (n "xmp_toolkit") (v "1.5.0") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04z0gkz9sqsp0z8l6dhqhns57iiszcsf87l3hzdn1448q9q84jy8") (f (quote (("crt_static")))) (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.66.0")))

(define-public crate-xmp_toolkit-1.6.0 (c (n "xmp_toolkit") (v "1.6.0") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02ypa4y11pafln0xfqc3bcj53c5bbfsnq09ra0j9vr2n2b64r2hn") (f (quote (("crt_static")))) (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.67.0")))

(define-public crate-xmp_toolkit-1.7.0 (c (n "xmp_toolkit") (v "1.7.0") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fcxc7x8b5lv8vc830si0f9a8cbigrjm7p8g0p023cvbywxlhdbv") (f (quote (("crt_static")))) (l "xmp_toolkit") (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.70.0")))

(define-public crate-xmp_toolkit-1.7.1 (c (n "xmp_toolkit") (v "1.7.1") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r "=1.0.83") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17av4y19glrzwblgrsk81r6azxany0ml197r86ah66mzx4d9kbw0") (f (quote (("crt_static")))) (l "xmp_toolkit") (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.70.0")))

(define-public crate-xmp_toolkit-1.7.2 (c (n "xmp_toolkit") (v "1.7.2") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r ">1.0.60, <=1.0.83") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p653ki7gb8l91i6v5x5c0ddx82h123kmw6ljz887xpwjv7ckjbi") (f (quote (("crt_static")))) (l "xmp_toolkit") (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.70.0")))

(define-public crate-xmp_toolkit-1.7.3 (c (n "xmp_toolkit") (v "1.7.3") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)) (d (n "cc") (r ">1.0.60, <=1.0.83") (d #t) (k 1)) (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "num_enum") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fwgmfl5dsjl453yc0v4iz9bwfmq1x4bp8v54i9qb51w5zb99c97") (f (quote (("crt_static")))) (l "xmp_toolkit") (s 2) (e (quote (("chrono" "dep:chrono")))) (r "1.70.0")))

