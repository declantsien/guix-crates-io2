(define-module (crates-io xm ob xmobet) #:use-module (crates-io))

(define-public crate-xmobet-0.1.0 (c (n "xmobet") (v "0.1.0") (d (list (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "random") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1rcxnmnk980i6qh40wn8cfizala5n1vzl23zqfzmimw8czw3zp7q")))

(define-public crate-xmobet-0.1.1 (c (n "xmobet") (v "0.1.1") (d (list (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "random") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "16ccxdv2xqfkpkcbn0v55cpbk0vmcbzk68qay2c0npqyvnfr7ds7")))

(define-public crate-xmobet-0.1.2 (c (n "xmobet") (v "0.1.2") (d (list (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "random") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1rn9f1fzwh9m44xkqrs0hcyqjhpfvllmpgsb92ygvbhr6wnc1i3n")))

(define-public crate-xmobet-0.1.3 (c (n "xmobet") (v "0.1.3") (d (list (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "random") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0scgivb54zs12zj6afkdmkjccjnc779f643k3chmdb4bg6gbnx4d")))

