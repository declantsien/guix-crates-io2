(define-module (crates-io xm od xmodmap-pke) #:use-module (crates-io))

(define-public crate-xmodmap-pke-0.1.0 (c (n "xmodmap-pke") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (d #t) (k 0)))) (h "0mg84nrp414xk4gmd6k5c8frmwadxy91k4blmlkfcpks32awmr7d")))

(define-public crate-xmodmap-pke-0.2.1 (c (n "xmodmap-pke") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (d #t) (k 0)))) (h "1y0d65m2nbl9jzbq34dzkv849phzjvcdxpclzb6ixjzkq20bfp9f")))

(define-public crate-xmodmap-pke-0.2.2 (c (n "xmodmap-pke") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (d #t) (k 0)))) (h "18ajg51bxysc2vmygpg49g49vxvgm9zcvklbh9v15bmdpxl6hhii")))

(define-public crate-xmodmap-pke-0.2.3 (c (n "xmodmap-pke") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (d #t) (k 0)))) (h "0miv3bjq6c3c248kbi045gpdkmscfv2j5y58nqqgigpi26d3qpfr")))

