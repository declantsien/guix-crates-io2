(define-module (crates-io xm od xmodem) #:use-module (crates-io))

(define-public crate-xmodem-0.1.0 (c (n "xmodem") (v "0.1.0") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.0") (d #t) (k 2)))) (h "0ac2qjpidsicarmpblicmaq55xip67wljrhx33vlvia18mag9gkk")))

(define-public crate-xmodem-0.1.1 (c (n "xmodem") (v "0.1.1") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.0") (d #t) (k 2)))) (h "0gbsprrriqcsd7nm4n106sz275xwnxqdh2iqbpbj7l9k1mkpjmik")))

(define-public crate-xmodem-0.1.2 (c (n "xmodem") (v "0.1.2") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.0") (d #t) (k 2)))) (h "0dw7qirqckvgk62gamggzy5nhjv67kxwksylsjacpmk0393vdwqj")))

(define-public crate-xmodem-0.1.3 (c (n "xmodem") (v "0.1.3") (d (list (d (n "crc16") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.0") (d #t) (k 2)))) (h "18mg4g40vl7nk7m8if1wbknc80lrh32wcgp0dg3vjgjjczdr38i5")))

