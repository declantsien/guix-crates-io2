(define-module (crates-io xm od xmodules) #:use-module (crates-io))

(define-public crate-xmodules-0.1.0 (c (n "xmodules") (v "0.1.0") (d (list (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "0npy3jbzq2xnca2n06w62sxwg83i50w18xm7cx7z875vz6j4w9b0") (y #t)))

(define-public crate-xmodules-0.1.1 (c (n "xmodules") (v "0.1.1") (d (list (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "0caj76s5fczn33gqaayi2piv5ajzv989l0zy4laxa4qibilv63qd") (y #t)))

(define-public crate-xmodules-0.1.2 (c (n "xmodules") (v "0.1.2") (d (list (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "11m933xi0i6dfxixj6y1i8aaxzz1vxcww3454b6z050afik8k79s")))

