(define-module (crates-io xm od xmodmap-pke-umberwm) #:use-module (crates-io))

(define-public crate-xmodmap-pke-umberwm-0.0.1 (c (n "xmodmap-pke-umberwm") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0ay87c16kdibk08ll512ybjrhs4fviwzmkms51yl5z12q8bwny9d") (l "X11")))

(define-public crate-xmodmap-pke-umberwm-0.0.2 (c (n "xmodmap-pke-umberwm") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "1qk9y2v9fa0bnjz73gca9v9p04c9hiamwd1yrl4kp017786an1d0") (l "X11")))

