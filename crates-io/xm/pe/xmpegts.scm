(define-module (crates-io xm pe xmpegts) #:use-module (crates-io))

(define-public crate-xmpegts-0.0.1 (c (n "xmpegts") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.1.26") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rtmp") (r "^0.0.6") (d #t) (k 0)))) (h "1p2xkg4vlj50ix9nqy1h11gjslv27iavwbfxi8ks2slqivmcncsg")))

(define-public crate-xmpegts-0.0.2 (c (n "xmpegts") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.1.26") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0g7d4h8kq4j3jb0ri8jbh3njp3didxqpl4615sm4vg7mi8g24jpm")))

(define-public crate-xmpegts-0.0.3 (c (n "xmpegts") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.1.27") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0sfv3zljjzz1lbl9cszy3fza29y4j2igf67akm23ga830zx3k76q")))

(define-public crate-xmpegts-1.0.0 (c (n "xmpegts") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.1.27") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1wfqw3pb56ysxyfrad1iags10msbjczl6pxwq40bfpwmwa71jy8k") (y #t)))

(define-public crate-xmpegts-0.1.0 (c (n "xmpegts") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.1.27") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0ik2y7mvm89xddif9pksf5sqsi182d3m64qsbqhq0pa6ns61jnvs")))

(define-public crate-xmpegts-0.1.1 (c (n "xmpegts") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "018yx4rj5nk14zv7kwx5618jj8xby5n2pvf7s97lmhrqd88nwiai")))

(define-public crate-xmpegts-0.2.0 (c (n "xmpegts") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "10894qiqbdihnir5vlxrspaqxzl1z0p3k5753546zj71b178vqrh")))

(define-public crate-xmpegts-0.2.1 (c (n "xmpegts") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0v7jzbk49jmnpinj5ml8mdqrkcnsq97vrv7978l91wzdvns6g7hi")))

(define-public crate-xmpegts-0.2.2 (c (n "xmpegts") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "0hqn9kwz9yzw3x6ixznnddysr8ksyjy4bwz29q9pmyacrhwp02d8")))

(define-public crate-xmpegts-0.2.3 (c (n "xmpegts") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "094dp3vfq5kyqvlny8f359rlc93v80h644w9x2inip35l80z2d7y")))

