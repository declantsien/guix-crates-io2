(define-module (crates-io xm ut xmutil) #:use-module (crates-io))

(define-public crate-xmutil-1.0.0 (c (n "xmutil") (v "1.0.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1v4r1rg8vyr9wq2wis9p56glabwia2ax4sszga1c422xx4254fjy")))

(define-public crate-xmutil-1.0.1 (c (n "xmutil") (v "1.0.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bsp7vl5x8y0h8dqs92a5gv8apprycdcw9irqra914pmgriqjkv7")))

(define-public crate-xmutil-1.1.0 (c (n "xmutil") (v "1.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "01dw84hi8jq4xla9sgh76ha6x7rfaj8z91ibc050931w4bvk5jxr")))

