(define-module (crates-io xm en xmenu) #:use-module (crates-io))

(define-public crate-xmenu-0.1.0 (c (n "xmenu") (v "0.1.0") (d (list (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1clsj58g8g9bljqdxspcbw1np93i0k2x8wj8jd29hj9m60a1idrp")))

