(define-module (crates-io xm u- xmu-login) #:use-module (crates-io))

(define-public crate-xmu-login-0.1.0 (c (n "xmu-login") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16wa55ps76c6xfd0w2c6bzvg5rgxssaj4n510l2ki3nqwsgg1qpb")))

(define-public crate-xmu-login-0.1.1 (c (n "xmu-login") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "056y20mwhd7bd0maa3lxf3rixxkn5zlxfxzjw37h9aglldv9b93a")))

