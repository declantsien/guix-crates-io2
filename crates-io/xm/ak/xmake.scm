(define-module (crates-io xm ak xmake) #:use-module (crates-io))

(define-public crate-xmake-0.1.0 (c (n "xmake") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "1l01lyzsbd8jn1ra1633hjfgsp85q84bigb3yrgr2rjzjbs7w6my")))

(define-public crate-xmake-0.2.0 (c (n "xmake") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0aicglyglh70a1kcbqiyqad4lcswbxgwq0gsv0nzy6sf3gj8c9dv")))

(define-public crate-xmake-0.2.1 (c (n "xmake") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 0)))) (h "0pg4hwqf6zvxkywalaxdy4q2pxwqqc6b236d424hi842p27s522m")))

