(define-module (crates-io xm l- xml-rs) #:use-module (crates-io))

(define-public crate-xml-rs-0.1.2 (c (n "xml-rs") (v "0.1.2") (h "0wq0yj2dsr1ppc7sl14q8cwbkaji6j6cx1bphqsjzdanf7dg3lgj") (y #t)))

(define-public crate-xml-rs-0.1.3 (c (n "xml-rs") (v "0.1.3") (h "1qv9y60mp6gcw457jq6fwhkh3pw1cg0h08gns9cilmw75d8h30ad") (y #t)))

(define-public crate-xml-rs-0.1.4 (c (n "xml-rs") (v "0.1.4") (h "16dimxp8dk1s9qh20agzk8b9vwr6dfkrlq57wsxs3j7p6q7xzda7") (y #t)))

(define-public crate-xml-rs-0.1.5 (c (n "xml-rs") (v "0.1.5") (h "0d5y8wn586lw4pw5z740wnr8z2pcgrhk1977nldnjsl8qra32x2q") (y #t)))

(define-public crate-xml-rs-0.1.6 (c (n "xml-rs") (v "0.1.6") (h "0q8x3nw89phnjppah7xj0hpzdk1cifgrc6v8cjjm5v9za148ir1y") (y #t)))

(define-public crate-xml-rs-0.1.7 (c (n "xml-rs") (v "0.1.7") (h "112j0fcvwcswli9wqh1ymqay3qz0fzlpw80hrpk57n0sja4mr08l") (y #t)))

(define-public crate-xml-rs-0.1.8 (c (n "xml-rs") (v "0.1.8") (h "1s1zm1dy5j7j2dpcl595wjm834dgjgkc71s8lww7cgx6lvji5q8h") (y #t)))

(define-public crate-xml-rs-0.1.9 (c (n "xml-rs") (v "0.1.9") (h "1vl0rl1fxhpp2cg7bxqsy4jzac9qal2wv92csjcxp0r3psy2li8v") (y #t)))

(define-public crate-xml-rs-0.1.10 (c (n "xml-rs") (v "0.1.10") (h "0hbl6qxqqwnsl7pyw5sajxc7r9g6ksgwx526xzm08hdlfbca3qjc") (y #t)))

(define-public crate-xml-rs-0.1.11 (c (n "xml-rs") (v "0.1.11") (h "0vbxcb9i4gswbs8x89675qa12d8rmy17d25azmhsya1f9b3zys3k") (y #t)))

(define-public crate-xml-rs-0.1.12 (c (n "xml-rs") (v "0.1.12") (h "1ah763nwn74kgz094bywm4nh70n54bwk0fr0j8wgi1apg0mi8qp9") (y #t)))

(define-public crate-xml-rs-0.1.13 (c (n "xml-rs") (v "0.1.13") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "0y0bl5x34pad2097imvbsfdc4xz51m1ss1djbh4053yzwc7xm58h") (y #t)))

(define-public crate-xml-rs-0.1.14 (c (n "xml-rs") (v "0.1.14") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1gg8wia7akmz34kr5xj4qyx6mq9rnqqnapz9pni6dsg29xich9bx") (y #t)))

(define-public crate-xml-rs-0.1.15 (c (n "xml-rs") (v "0.1.15") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "077qbizrj3159bm85y27d9hvgiyhlnzqw99y9sygbf4llfnm104w") (y #t)))

(define-public crate-xml-rs-0.1.16 (c (n "xml-rs") (v "0.1.16") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "0zghacg0cyzaxa755757w4xlrl0bvjap9z831r192a14r13dpdfv") (y #t)))

(define-public crate-xml-rs-0.1.17 (c (n "xml-rs") (v "0.1.17") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "05b9rb3k68rw7cal8s4sbfkdd945b1738qxd5qknlx8vi03wgqvx") (y #t)))

(define-public crate-xml-rs-0.1.18 (c (n "xml-rs") (v "0.1.18") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "090fhvbg5xy9sfyd8cznzcgabnhspiivnsgcq78xqn5pagpnbnd5") (y #t)))

(define-public crate-xml-rs-0.1.19 (c (n "xml-rs") (v "0.1.19") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "0nwzz2xkmpnmc2p376f6mhb248jb9m5cga09ss3sd6v02gc827pq") (y #t)))

(define-public crate-xml-rs-0.1.20 (c (n "xml-rs") (v "0.1.20") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "0k9ivskl11m89d5j15sx05r3h7parvj99pxv01sdwkqflmmvc9ys") (y #t)))

(define-public crate-xml-rs-0.1.21 (c (n "xml-rs") (v "0.1.21") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "136fi97ici6fn52c60zqaqv6v4js3vrqls64c712z82xqvrcplxh") (y #t)))

(define-public crate-xml-rs-0.1.22 (c (n "xml-rs") (v "0.1.22") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1qwy8ry7c8ahbyy49s82bax097x8nwrr2naqvwwv0x5brrcpd8l7") (y #t)))

(define-public crate-xml-rs-0.1.23 (c (n "xml-rs") (v "0.1.23") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "14biy5jn982lvmxrayz3iqhqh3hxqg631mr8iczhx0s9gws9k3wi") (y #t)))

(define-public crate-xml-rs-0.1.24 (c (n "xml-rs") (v "0.1.24") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "14cwd720wcwq5p4bxq4s4iad807bnhvkkcnrglxjry2yygys1p84") (y #t)))

(define-public crate-xml-rs-0.1.25 (c (n "xml-rs") (v "0.1.25") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1mvj28zca9pziiqcycybmhdnrbxqczmfyljg1fayq4lmcj21l6j4") (y #t)))

(define-public crate-xml-rs-0.1.26 (c (n "xml-rs") (v "0.1.26") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "068mcrpww6x7mlxcwr80hfhmr2vd6fgv59sanp9jvnr45gc8zb2b") (y #t)))

(define-public crate-xml-rs-0.2.0 (c (n "xml-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "10yxbs3z5gywjrsy0dsds51gm9ilg3rbqv6hkh32aacahqifn463") (y #t)))

(define-public crate-xml-rs-0.2.1 (c (n "xml-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "01llrgminsx6qiyj1lw469rc4wkyhdpyzdsjkizh37bfd25lzysj") (y #t)))

(define-public crate-xml-rs-0.2.2 (c (n "xml-rs") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)))) (h "1gwy8hny2643j2yxpkcs3n4gxbfhxk72wgz06cfxf0ps6kwk8z0d")))

(define-public crate-xml-rs-0.2.3 (c (n "xml-rs") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)))) (h "007qnvazx6yrld2pl02666a5683vysggyhwrhq88zsi7q66g5c9m") (y #t)))

(define-public crate-xml-rs-0.2.4 (c (n "xml-rs") (v "0.2.4") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)))) (h "02vagvn411nxi1kzbf8nn7gy2hjmxxfhnqwg2ls05j039y7j2zpf") (y #t)))

(define-public crate-xml-rs-0.3.0 (c (n "xml-rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)))) (h "0b5mkvbipc80z17fkkwsc57a0f81nwxg1sjvflw6ml2m9s3gf7pi") (y #t)))

(define-public crate-xml-rs-0.3.1 (c (n "xml-rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)))) (h "15fxwcm3j8rzqksnj731785mfnw0w0lcn35ycr4akv1mbq15wjiz") (y #t)))

(define-public crate-xml-rs-0.3.2 (c (n "xml-rs") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)))) (h "0vmj7xpb0kzlbl747lc3glrghigwli43c3v5pypdy1k4mqa10fxh") (y #t)))

(define-public crate-xml-rs-0.3.3 (c (n "xml-rs") (v "0.3.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)))) (h "1s5wldniv726ril30cwl3wlrv2cfyp90ckccnv927lgq0gk2jbr8") (y #t)))

(define-public crate-xml-rs-0.3.4 (c (n "xml-rs") (v "0.3.4") (d (list (d (n "bitflags") (r ">= 0.5, < 0.8") (d #t) (k 0)))) (h "1vz3ay999h3n0cx11vkfwh4zygc4vyk0v60aqyfj0y9ipnb4prv5") (y #t)))

(define-public crate-xml-rs-0.3.5 (c (n "xml-rs") (v "0.3.5") (d (list (d (n "bitflags") (r ">= 0.5, < 0.8") (d #t) (k 0)))) (h "1irf2djlihkj2r6xp39dk5dlc244vkwfg3p92nfxaav92bnmxcgj") (y #t)))

(define-public crate-xml-rs-0.3.6 (c (n "xml-rs") (v "0.3.6") (d (list (d (n "bitflags") (r ">= 0.5, < 0.8") (d #t) (k 0)))) (h "0qmm2nss16b0f46fp30s2ka8k50a5i03jlp36672qf38magc7iky")))

(define-public crate-xml-rs-0.3.7 (c (n "xml-rs") (v "0.3.7") (d (list (d (n "bitflags") (r ">= 0.5, < 0.8") (d #t) (k 0)))) (h "0jyskdxfvyvn6mnbcbhskvhaa8mqprpkbklk0nsi5bxfjrvsihv3") (y #t)))

(define-public crate-xml-rs-0.3.8 (c (n "xml-rs") (v "0.3.8") (d (list (d (n "bitflags") (r ">= 0.5, < 0.8") (d #t) (k 0)))) (h "0fzyr8892g9zry6f17pnfdaxlsaqmyx64wbxgagldik0haln94kj") (y #t)))

(define-public crate-xml-rs-0.4.0 (c (n "xml-rs") (v "0.4.0") (d (list (d (n "bitflags") (r ">= 0.5, < 0.8") (d #t) (k 0)))) (h "1fkc22xzmg2cdv9cg3rg1ypqdygcz4rcx1bw9k6awl7mw272w0l7") (y #t)))

(define-public crate-xml-rs-0.4.1 (c (n "xml-rs") (v "0.4.1") (d (list (d (n "bitflags") (r ">= 0.5, < 0.8") (d #t) (k 0)))) (h "0jmy0xp4f7rgdglcjd8i8vf64v2is942630pl449qrkspa4ycvml")))

(define-public crate-xml-rs-0.5.0 (c (n "xml-rs") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)))) (h "00zmrjxrc006nyshanffgzy175y74kc62j67s46hz8kh1akdgccd") (y #t)))

(define-public crate-xml-rs-0.6.0 (c (n "xml-rs") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)))) (h "03hlirrqinv3gsy67q8i5ja26anyqhrn1nzfm6h14hc0bg9r133n") (y #t)))

(define-public crate-xml-rs-0.6.1 (c (n "xml-rs") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)))) (h "08a6lydyf3cmknicf0hnqdwyz5i4hfq20rcpswhig5bbw495x571")))

(define-public crate-xml-rs-0.6.2 (c (n "xml-rs") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "18g7krn8zx8ifml83w91w2hvw437j5q3vaw4cvx3kryccj5860pl") (y #t)))

(define-public crate-xml-rs-0.7.0 (c (n "xml-rs") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1hp9kf80y9qm3aiqg5psyshqfkcrjgifbcm2c2nc5qlzs80vc71w")))

(define-public crate-xml-rs-0.7.1 (c (n "xml-rs") (v "0.7.1") (h "1wv7izl41jf3ylhqhw23y1h0m729v2g5k4mgfw72v4kmgvvawiin") (y #t)))

(define-public crate-xml-rs-0.8.0 (c (n "xml-rs") (v "0.8.0") (h "1db4v716rbpgjiasaim2s17rmvsfcq1qzwg6nji6mdf5k34i46sl")))

(define-public crate-xml-rs-0.8.1 (c (n "xml-rs") (v "0.8.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "0c905wsmk995xypxljpxzq6vv660r1pzgyrpsfiz13kw3hf0dzcs")))

(define-public crate-xml-rs-0.8.2 (c (n "xml-rs") (v "0.8.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "08vwh7nqdrzkx3d62qnm4i85aidk641h63664j4ypfqv89f6xdrb")))

(define-public crate-xml-rs-0.8.3 (c (n "xml-rs") (v "0.8.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "12ndxyhzxw2zdr76ql8nfdwb2vwhvdkrxwk4pbjafqfglmjv0zdh")))

(define-public crate-xml-rs-0.8.4 (c (n "xml-rs") (v "0.8.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)))) (h "18q048wk3jafgl59sa2m0qv4vk2sqkfcya4kznc5rxqkhsad7myj")))

(define-public crate-xml-rs-0.8.5 (c (n "xml-rs") (v "0.8.5") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "0wfm4ma4lbl1b6y6ajr20b8xixbxixac0k8904swwdkcnfgn0jrp")))

(define-public crate-xml-rs-0.8.6 (c (n "xml-rs") (v "0.8.6") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0amfgmph76q3p43vwy3apmgbg39la5sri5fvn86zn797dq7adqa0") (r "1.63")))

(define-public crate-xml-rs-0.8.7 (c (n "xml-rs") (v "0.8.7") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "18nxpxqhqbhxncii03gf56hvhyf4s3icdmlks3v7lznxph2037b9") (r "1.63")))

(define-public crate-xml-rs-0.8.8 (c (n "xml-rs") (v "0.8.8") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "03pqnq10s0b2pwh91gbbi9ba143jgjrxnl0xi7ngdzni5d7g282g") (r "1.63")))

(define-public crate-xml-rs-0.8.9 (c (n "xml-rs") (v "0.8.9") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1cwffnrl2n5rh487r7qn4vkhc9gyrhdqcjys7b1xk2vynrdw9z61") (r "1.63")))

(define-public crate-xml-rs-0.8.10 (c (n "xml-rs") (v "0.8.10") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1lpq7c9pxcdjiz1ib171k6f5hdgscgwl9asspvckqm2gl97a15fw") (r "1.63")))

(define-public crate-xml-rs-0.8.11 (c (n "xml-rs") (v "0.8.11") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "008b8aig9k5j24w03c5kax43l1331hss574v48jmbadza2am340n") (r "1.58")))

(define-public crate-xml-rs-0.8.12 (c (n "xml-rs") (v "0.8.12") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1s8dpi71wakzm6d3kbsrsiv9f9bihpgh21qbznfw0sxgbwmm9qpv") (y #t) (r "1.58")))

(define-public crate-xml-rs-0.8.13 (c (n "xml-rs") (v "0.8.13") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0bcjvrygagza3gk8khah2i5khwc0c1071xicd87b6dvaw453i3rd") (r "1.58")))

(define-public crate-xml-rs-0.8.14 (c (n "xml-rs") (v "0.8.14") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "072lwmk6y84gha04rr13z44y9daq3yfh6kgs7vv8wfh8274rv0sj") (r "1.58")))

(define-public crate-xml-rs-0.8.15-cvss-cries-wolf (c (n "xml-rs") (v "0.8.15-cvss-cries-wolf") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "055vws7nwq86mj4j9g9096ahbl5ivpx98kwzx9rhyiailsr9z5g0") (r "1.58")))

(define-public crate-xml-rs-0.8.15 (c (n "xml-rs") (v "0.8.15") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0dlkdx2xlfa1q8ksx4k8dcspbskd1w6gg4hwsap5hhndii5chmjs") (r "1.58")))

(define-public crate-xml-rs-0.8.16 (c (n "xml-rs") (v "0.8.16") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "18azkgi4r91a39dqbp172dzcbby3gdb42avmxv69km5mlyc0jhs7") (r "1.58")))

(define-public crate-xml-rs-0.8.17 (c (n "xml-rs") (v "0.8.17") (h "0i04nb24snjhm5c4c0x8ys9x8gw37niak0bkincwzrvbjbsnpvhy") (r "1.58")))

(define-public crate-xml-rs-0.8.18 (c (n "xml-rs") (v "0.8.18") (h "02jgfy3l25kx5lph8264lcflsfzls1yfwb0z8gd97vhannbpxdxs") (r "1.58")))

(define-public crate-xml-rs-0.8.19 (c (n "xml-rs") (v "0.8.19") (h "0nnpvk3fv32hgh7vs9gbg2swmzxx5yz73f4b7rak7q39q2x9rjqg") (r "1.58")))

(define-public crate-xml-rs-0.8.20 (c (n "xml-rs") (v "0.8.20") (h "14s1czpj83zhgr4pizxa4j07layw9wmlqhkq0k3wz5q5ixwph6br") (r "1.58")))

