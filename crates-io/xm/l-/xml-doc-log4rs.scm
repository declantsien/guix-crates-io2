(define-module (crates-io xm l- xml-doc-log4rs) #:use-module (crates-io))

(define-public crate-xml-doc-log4rs-0.2.0 (c (n "xml-doc-log4rs") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.1") (d #t) (k 0)) (d (n "log4rs-macros") (r "^1.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "19kw882n1v3z574gv2vrdbk27pppcml056js5r8wc7n9ndmcm5fd")))

