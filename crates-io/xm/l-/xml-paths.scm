(define-module (crates-io xm l- xml-paths) #:use-module (crates-io))

(define-public crate-xml-paths-0.1.0 (c (n "xml-paths") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0jps102bbjr8xyb6mql73djp4il6i3nc1jz3ddnsanfcn3cydmgj")))

(define-public crate-xml-paths-0.1.1 (c (n "xml-paths") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "0pc18jrm7zp7zpp8mqlmmnqzsliv3wcyxd83a7bqy3pam516hw1f")))

(define-public crate-xml-paths-0.1.2 (c (n "xml-paths") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "1z33qq13zn94qrsrbxsgkr68kwspb1j61hffyj0k51b6rh4abd32")))

