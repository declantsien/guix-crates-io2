(define-module (crates-io xm l- xml-stinks) #:use-module (crates-io))

(define-public crate-xml-stinks-0.1.0 (c (n "xml-stinks") (v "0.1.0") (d (list (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "quick-xml") (r "^0.28.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1pz1kbsdh5jik0dkf3h2hz9jkv8ssxn71idh50fzyv8d82r68dsz") (f (quote (("deserializer-static-generics"))))))

