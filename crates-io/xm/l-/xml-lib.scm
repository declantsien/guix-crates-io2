(define-module (crates-io xm l- xml-lib) #:use-module (crates-io))

(define-public crate-xml-lib-0.0.1 (c (n "xml-lib") (v "0.0.1") (h "0lbdya4fn1npp7jz3l18wfy8x9zsslbsjqky3sq3yx2pcgpcld5c")))

(define-public crate-xml-lib-0.0.2 (c (n "xml-lib") (v "0.0.2") (h "0bfgj9h41v7dsbdxjn1fzc49xslqbjnr219kfl92mfpsrp4lcz99")))

(define-public crate-xml-lib-0.0.3 (c (n "xml-lib") (v "0.0.3") (h "1h3vbdndsyg4npblrbmy23zi82d2i5wpybfk9h2rf220ds4c5zal")))

