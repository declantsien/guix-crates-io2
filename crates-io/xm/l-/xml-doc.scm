(define-module (crates-io xm l- xml-doc) #:use-module (crates-io))

(define-public crate-xml-doc-0.1.0 (c (n "xml-doc") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "1qik2r1lf96f2ncqwjr7mcmyvv3zx3ndjrml6ifbwz4spxcmb7li")))

(define-public crate-xml-doc-0.1.1 (c (n "xml-doc") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "1chfh56nf8jjwn0vn0qgcxy2ns5l3dp942dczvdmwr719iynwicl")))

(define-public crate-xml-doc-0.2.0 (c (n "xml-doc") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "0p16lx5sh04vszj3gy7dybghyjj5bx5a5qinfwajvd4v6nxgp98w")))

