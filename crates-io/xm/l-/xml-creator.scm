(define-module (crates-io xm l- xml-creator) #:use-module (crates-io))

(define-public crate-xml-creator-0.1.0 (c (n "xml-creator") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)))) (h "1jmx86p41dj8bcizx8zfwshl8yvf35xp8ca5vhi6bqykcphv6vmk") (y #t)))

(define-public crate-xml-creator-0.1.1 (c (n "xml-creator") (v "0.1.1") (d (list (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)))) (h "1biv4pj2v1x8n09ylcv7k0pd5qd16cl51787pshqqrkjfixhjiaw")))

