(define-module (crates-io xm l- xml-data-derive) #:use-module (crates-io))

(define-public crate-xml-data-derive-0.0.1 (c (n "xml-data-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1bpn4vasigqaa384csbfzpy1jwd3mifd729c22cjh7szigp2h3zj")))

