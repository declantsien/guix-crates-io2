(define-module (crates-io xm l- xml-rsl) #:use-module (crates-io))

(define-public crate-xml-rsl-0.1.0 (c (n "xml-rsl") (v "0.1.0") (h "0svzgga2j37k8dbsx02vkj83kag3wdqdcsd5bv3rjs543xd8c21r") (r "1.58")))

(define-public crate-xml-rsl-0.1.1 (c (n "xml-rsl") (v "0.1.1") (d (list (d (n "crypto-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "ssh2-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "tar-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "toml-rsl") (r "^0.1.0") (d #t) (k 0)))) (h "0n3901pp95ha5hjj1ry7nlafj8snkgcklv1rsylmm7ly2w336nc8") (r "1.58")))

(define-public crate-xml-rsl-0.2.0 (c (n "xml-rsl") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "jdks") (r "^0.1.3") (d #t) (k 0)) (d (n "ksre-tui") (r "^0.2.0") (d #t) (k 0)) (d (n "sdkman-cli-native") (r "^0.5.0") (d #t) (k 0)))) (h "1xl7alpd4sfsjxf1h8410b9hssj30ylf92rdnjjaawja2lzqrg4d") (f (quote (("ordered_attrs" "indexmap") ("bench"))))))

