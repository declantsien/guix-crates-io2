(define-module (crates-io xm l- xml-xls-parser) #:use-module (crates-io))

(define-public crate-xml-xls-parser-0.1.0 (c (n "xml-xls-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.2") (d #t) (k 0)))) (h "02vdw74h7lpxaqrs0zwlypnfv26dhhy55ghahnlgp55qpmjq0n7w")))

