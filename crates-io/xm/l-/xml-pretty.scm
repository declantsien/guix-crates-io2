(define-module (crates-io xm l- xml-pretty) #:use-module (crates-io))

(define-public crate-xml-pretty-0.1.0 (c (n "xml-pretty") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "xmlem") (r "^0.1.0") (d #t) (k 0)))) (h "1a07wlmpqx9k49wlxhrkr5nxmjcyrykzcnpkxymw3j7ycsmm2ssj")))

(define-public crate-xml-pretty-0.2.0 (c (n "xml-pretty") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "xmlem") (r "^0.1.0") (d #t) (k 0)))) (h "1g7fj65aq16r8x35xl1j29m9yfrqjyqjwd2w691mcc7h6bk1bg0r")))

(define-public crate-xml-pretty-0.2.1 (c (n "xml-pretty") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "xmlem") (r "^0.1.8") (d #t) (k 0)))) (h "0kjlkck70pa8qm27lndqi90pcz1fd4gi4hpwx695lbw0qd0kv3z5")))

(define-public crate-xml-pretty-0.2.2 (c (n "xml-pretty") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "xmlem") (r "^0.2.0") (d #t) (k 0)))) (h "1f8gs0nf63lrzaxhmf1b4zxs3bx4zc9az6smk35n815j2km4w3iw")))

(define-public crate-xml-pretty-0.2.3 (c (n "xml-pretty") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "xmlem") (r "^0.2.0") (d #t) (k 0)))) (h "0wbqjjdgdz852rlv0d3awzpjndrcivgrcshfzb918i2s6cpjzgrj")))

