(define-module (crates-io xm l- xml-attributes-derive) #:use-module (crates-io))

(define-public crate-xml-attributes-derive-0.1.0 (c (n "xml-attributes-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~0.2") (d #t) (k 0)) (d (n "quote") (r "~0.4") (d #t) (k 0)) (d (n "syn") (r "~0.12") (d #t) (k 0)))) (h "06iihsrzwrnr62fk5j8h8zcfn6ii2qvw1rqsk5m1crsj730nmzik")))

(define-public crate-xml-attributes-derive-0.1.1 (c (n "xml-attributes-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "~0.2") (d #t) (k 0)) (d (n "quote") (r "~0.4") (d #t) (k 0)) (d (n "syn") (r "~0.12") (d #t) (k 0)))) (h "128zj2fwi0101wb6vr5si3mkjpknci8wrp8i7m1iw2q33sdqjxqy")))

