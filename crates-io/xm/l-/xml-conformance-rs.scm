(define-module (crates-io xm l- xml-conformance-rs) #:use-module (crates-io))

(define-public crate-xml-conformance-rs-0.0.0 (c (n "xml-conformance-rs") (v "0.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "15kqijhx8dqfir1nncksm5x3yaswc0vjk8sqvq6wwl8qnxawhi13")))

