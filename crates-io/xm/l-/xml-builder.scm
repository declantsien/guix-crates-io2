(define-module (crates-io xm l- xml-builder) #:use-module (crates-io))

(define-public crate-xml-builder-0.1.0 (c (n "xml-builder") (v "0.1.0") (h "10dyq1kwjmmylmj4z47ydg1c28ky3da7nvy0l6c5903y5jk9bk3c")))

(define-public crate-xml-builder-0.1.1 (c (n "xml-builder") (v "0.1.1") (h "07wsm9x1lg50zsbnamyyra31p4jfkxg8mz0cgfwaqrcg8w54l2l9")))

(define-public crate-xml-builder-0.1.2 (c (n "xml-builder") (v "0.1.2") (h "0iv8cjh16gizh72wya3rcpjsp1awxikx16a90ksrxbyfrs8p1l6i")))

(define-public crate-xml-builder-0.1.3 (c (n "xml-builder") (v "0.1.3") (h "0mrpzpyxjz2ss6lh7inl7jz89z9gp9r6h3mm1xwxgbfybsixycsc")))

(define-public crate-xml-builder-0.1.4 (c (n "xml-builder") (v "0.1.4") (h "07gssjw1q3bl5agrrs0lygbddbc48y181iass2niw0v03qrc6x0z")))

(define-public crate-xml-builder-0.2.0 (c (n "xml-builder") (v "0.2.0") (h "010a84v4gdq5s73nc1ksb6rdj1ir35j5wdfbgff6dvs6rfvvczyh")))

(define-public crate-xml-builder-0.2.1 (c (n "xml-builder") (v "0.2.1") (h "1dmcylfsbd7mlpnqwbzpgsi17jbplkdagsyfxmswwih7kcnb30kh")))

(define-public crate-xml-builder-0.2.2 (c (n "xml-builder") (v "0.2.2") (h "0n4gaiifyfzvmxbr1gbzw3000n8k5jq4xfg09vjl0zhrdv7bzsnd")))

(define-public crate-xml-builder-0.2.3 (c (n "xml-builder") (v "0.2.3") (h "0nw76gs8kp30fpfgsk92f0hfpywpp1fwnyj88pdjmqlyn5k3j45a")))

(define-public crate-xml-builder-0.3.0 (c (n "xml-builder") (v "0.3.0") (h "0jy18ppz2f541mmshmpz7gh64aq2m62cw3cimhydk4crwxx8wa1r")))

(define-public crate-xml-builder-0.4.0 (c (n "xml-builder") (v "0.4.0") (h "0lpxgk88dlaqnvk7z2155ijh5znjm48hj6wql8rd1prjmimv1171")))

(define-public crate-xml-builder-0.5.0 (c (n "xml-builder") (v "0.5.0") (h "1v31rzvjy4rm5iirbmiss5j5g9995440ggfwss6x64lcxgmkxzva")))

(define-public crate-xml-builder-0.5.1 (c (n "xml-builder") (v "0.5.1") (h "1cq2rr2gsrj82f1qkq8hy4hc41v7phqk085p7vxs84zliqzmmdyc")))

(define-public crate-xml-builder-0.5.2 (c (n "xml-builder") (v "0.5.2") (h "0frkylldh541r6sn082h46p1aiga90v87isn83y0v07pdalg3i7g")))

