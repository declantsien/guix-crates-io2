(define-module (crates-io xm l- xml-data) #:use-module (crates-io))

(define-public crate-xml-data-0.0.1 (c (n "xml-data") (v "0.0.1") (d (list (d (n "quick-xml") (r "^0.17") (f (quote ("encoding"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "xml-data-derive") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1gw6q04s120vrpd7kj12yah42ydgfbk8a1amdja9w557vg4fbmgp") (f (quote (("derive" "xml-data-derive") ("default" "derive") ("_private-test"))))))

