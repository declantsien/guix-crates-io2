(define-module (crates-io xm l- xml-schema) #:use-module (crates-io))

(define-public crate-xml-schema-0.0.1 (c (n "xml-schema") (v "0.0.1") (d (list (d (n "xml-schema-derive") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.0.1") (d #t) (k 2)))) (h "0x3ann56f43dqr20xhnv91j2n1kcsvbkma9h6wvjdq45v6jg0fm1")))

(define-public crate-xml-schema-0.0.2 (c (n "xml-schema") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.0.2") (d #t) (k 2)) (d (n "yaserde") (r "^0.3") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.3") (d #t) (k 2)))) (h "0bl3nfdcckz6cyc392ir5bcmip1xmp8jhb1y381z0d587dpd2321")))

(define-public crate-xml-schema-0.0.3 (c (n "xml-schema") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.0.3") (d #t) (k 2)) (d (n "yaserde") (r "^0.3") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.3") (d #t) (k 2)))) (h "00wg4d9gnbvj4iws2jlh15scg2fl3k1550zygp1dij5fqqqcggak")))

(define-public crate-xml-schema-0.0.4 (c (n "xml-schema") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.0.4") (d #t) (k 2)) (d (n "yaserde") (r "^0.4") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "0976lik0m0zz25h0qy29w11299nl8fjmmzdk88q7iygabddafbc2")))

(define-public crate-xml-schema-0.0.5 (c (n "xml-schema") (v "0.0.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.0.5") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.0.5") (d #t) (k 2)) (d (n "yaserde") (r "^0.4") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "0fgbnhdvfv9z86qc5lh64d3hc8q2b3synxscfav7c5k6sk0h9902")))

(define-public crate-xml-schema-0.0.6 (c (n "xml-schema") (v "0.0.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.0.6") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.0.6") (d #t) (k 2)) (d (n "yaserde") (r "^0.4") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "1qz9q35l5cq2nqb4pspdk7s1p6amvrxfvlr0sh19sr0ba2gzyiy5")))

(define-public crate-xml-schema-0.0.7 (c (n "xml-schema") (v "0.0.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.0.7") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.0.7") (d #t) (k 2)) (d (n "yaserde") (r "^0.4") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "0gb32dbkz3mvzvbi0g33xhxn3h13pqsv0y5md6bky3lipw3fi182")))

(define-public crate-xml-schema-0.1.0 (c (n "xml-schema") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "yaserde") (r "^0.8") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.8") (d #t) (k 2)))) (h "0mr5fwn5rx61b94c23pkacd8flcnv8a460qzmc6m9mgzza5jfydn")))

(define-public crate-xml-schema-0.2.0 (c (n "xml-schema") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "yaserde") (r "^0.8") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.8") (d #t) (k 2)))) (h "0nnqccyp0pjkw1ngnbsdm38qx5p6pynkipc6042wki65w7iji6bq")))

(define-public crate-xml-schema-0.3.0 (c (n "xml-schema") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 2)) (d (n "xml-schema-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "xml-schema-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "yaserde") (r "^0.9") (d #t) (k 2)) (d (n "yaserde_derive") (r "^0.9") (d #t) (k 2)))) (h "1vcnbrjr9nn2b58xz46zpgx6v8l9gx77vb6p0pb0h7c9mswm36nw")))

