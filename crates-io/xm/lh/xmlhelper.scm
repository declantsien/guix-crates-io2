(define-module (crates-io xm lh xmlhelper) #:use-module (crates-io))

(define-public crate-xmlhelper-0.1.0 (c (n "xmlhelper") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "17izwi9xfrrvvq6yb8pfp6wr0c42s3x0zl1war4dfv242qj81ccd")))

