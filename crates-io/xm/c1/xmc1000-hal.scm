(define-module (crates-io xm c1 xmc1000-hal) #:use-module (crates-io))

(define-public crate-xmc1000-hal-0.1.0 (c (n "xmc1000-hal") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)) (d (n "xmc1000") (r "^0.1.0") (d #t) (k 0)))) (h "1zk4x5sci1dzky7vinkz694384qm1paaq1g4s8vmdw8j4in3wn0i") (f (quote (("rt" "xmc1000/rt"))))))

