(define-module (crates-io xm c1 xmc1100) #:use-module (crates-io))

(define-public crate-xmc1100-0.1.0 (c (n "xmc1100") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1y52s6s5bp4z7krgbw92zvf6jm3c93msl1i57rynyjqfbb7fa6fa") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-xmc1100-0.2.0 (c (n "xmc1100") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1jkgibapbbsy7yryi7vc6f8bhv2wydc1mz1mgp6ahkvldhhq9bgy") (f (quote (("rt" "cortex-m-rt/device"))))))

