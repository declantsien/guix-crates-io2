(define-module (crates-io xm c1 xmc1000) #:use-module (crates-io))

(define-public crate-xmc1000-0.1.0 (c (n "xmc1000") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0y12akvdjgpma9a4pcyjabm5dr0jq70b3arsf0m6651i0mwpq6a1") (f (quote (("rt" "cortex-m-rt/device"))))))

