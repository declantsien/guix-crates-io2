(define-module (crates-io xm lx xmlx) #:use-module (crates-io))

(define-public crate-xmlx-0.1.1 (c (n "xmlx") (v "0.1.1") (h "162wm40dcg86h9r1r962cb9xzn7ngk1i68vgjhqwf2yjpprxcb3q") (y #t)))

(define-public crate-xmlx-0.1.2 (c (n "xmlx") (v "0.1.2") (h "0x5b95289ffzmqn249zaf5kasarx478y2rqkh72594a56phg4x8b") (y #t)))

