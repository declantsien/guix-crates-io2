(define-module (crates-io xm pp xmpp-addr) #:use-module (crates-io))

(define-public crate-xmpp-addr-0.7.0 (c (n "xmpp-addr") (v "0.7.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p3xzzi7kyn76fsig0h2i9fi05x0ppzni6s3324g41hdad84fvzy") (f (quote (("stable") ("default" "stable" "serde"))))))

(define-public crate-xmpp-addr-0.8.0 (c (n "xmpp-addr") (v "0.8.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hylb596rvhjg7gskx3q5q5rd5b5lirhfa1px7gnlrlyzr8h1fcy") (f (quote (("stable") ("default" "stable" "serde"))))))

(define-public crate-xmpp-addr-0.8.1 (c (n "xmpp-addr") (v "0.8.1") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1q8nyryxxlv53wzdzvk0000kza6f6099rvvkhfh79hxrkb7987n6") (f (quote (("stable") ("default" "stable" "serde"))))))

(define-public crate-xmpp-addr-0.9.0 (c (n "xmpp-addr") (v "0.9.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0kjr1b2s7b1gbxpqbn335j77cf8l0ghm18p8ylgbmcl138ih61p0") (f (quote (("stable") ("default" "stable" "serde"))))))

(define-public crate-xmpp-addr-0.9.1 (c (n "xmpp-addr") (v "0.9.1") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "04h5016363qsd8py68nvdwg6hlw1z1annm4jmdq9x46diwz7kwhl") (f (quote (("stable") ("default" "stable" "serde"))))))

(define-public crate-xmpp-addr-0.10.0 (c (n "xmpp-addr") (v "0.10.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0f9vhhqh0d1w9w52p6749npm1za2qar3r29x6zpfy8fx539ricx7") (f (quote (("stable") ("default" "stable" "serde"))))))

(define-public crate-xmpp-addr-0.11.0 (c (n "xmpp-addr") (v "0.11.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0cdhwif4rs3mn23bsd939mky828v1kf4kw6h7mf2g95yhvr87r58") (f (quote (("stable") ("default" "stable"))))))

(define-public crate-xmpp-addr-0.11.1 (c (n "xmpp-addr") (v "0.11.1") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1yp98g064vhvhbw40mg5vcmck930kxi48cbjn7xhhha80cggmp4m") (f (quote (("stable") ("default" "stable"))))))

(define-public crate-xmpp-addr-0.12.0 (c (n "xmpp-addr") (v "0.12.0") (d (list (d (n "idna") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0hq853cb5db6jf31cw0jb5y2d37nc453pkzd45z88g14rg212dxl") (f (quote (("try_from") ("default") ("ascii_ctype"))))))

(define-public crate-xmpp-addr-0.13.0 (c (n "xmpp-addr") (v "0.13.0") (d (list (d (n "idna") (r "^0.1.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.5") (d #t) (k 0)))) (h "1mfq20z9psi3a3l7i1r2ric7p63ixpfp1w6iyf4wiyzn5bqj87z1") (f (quote (("try_from") ("default") ("ascii_ctype"))))))

(define-public crate-xmpp-addr-0.13.1 (c (n "xmpp-addr") (v "0.13.1") (d (list (d (n "idna") (r "^0.1.4") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.5") (d #t) (k 0)))) (h "03mngi3ha6pwl7ycnvn2jfi7k3dlbkk7xc8nnhl8rn38mjxqv7y9") (f (quote (("try_from") ("default"))))))

