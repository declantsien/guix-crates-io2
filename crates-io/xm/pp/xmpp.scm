(define-module (crates-io xm pp xmpp) #:use-module (crates-io))

(define-public crate-xmpp-0.1.0 (c (n "xmpp") (v "0.1.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "minidom") (r "^0.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.6") (d #t) (k 0)))) (h "006nczfjavw5gkhzxx88qjqk4pirbh50ysrglll85abw1hi3yxzn")))

(define-public crate-xmpp-0.2.0 (c (n "xmpp") (v "0.2.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "jid") (r "^0.2.0") (d #t) (k 0)) (d (n "minidom") (r "^0.4.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.12") (d #t) (k 0)) (d (n "sasl") (r "^0.4.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.3.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.1") (d #t) (k 0)) (d (n "xmpp-parsers") (r "^0.3.0") (d #t) (k 0)))) (h "02mm5as7qa0dhpcm937nkchng0zhb82524kbwiv40wp6mc0sn179") (f (quote (("insecure"))))))

(define-public crate-xmpp-0.3.0 (c (n "xmpp") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-xmpp") (r "^1.0.1") (d #t) (k 0)) (d (n "xmpp-parsers") (r "^0.15") (d #t) (k 0)))) (h "14i2nk2nwiixjcnzpi59aq034kk7kc8dgas6gqnvmb7j5546fg93")))

(define-public crate-xmpp-0.4.0 (c (n "xmpp") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tokio-xmpp") (r "^3.2") (d #t) (k 0)) (d (n "xmpp-parsers") (r "^0.19") (d #t) (k 0)))) (h "1vmi6767j28pdfsvs87xah26ar39142vk9lyghjc4gnw3dh5svd5") (f (quote (("default" "avatars") ("avatars"))))))

(define-public crate-xmpp-0.5.0 (c (n "xmpp") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tokio-xmpp") (r "^3.4") (d #t) (k 0)))) (h "1b6vhzxwcjkbk5vbcq10b3bs4lxmn3j25kf6fng4ymg7cdn8k7nl") (f (quote (("default" "avatars") ("avatars"))))))

