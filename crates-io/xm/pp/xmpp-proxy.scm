(define-module (crates-io xm pp xmpp-proxy) #:use-module (crates-io))

(define-public crate-xmpp-proxy-1.0.0 (c (n "xmpp-proxy") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "die") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("net" "rt" "rt-multi-thread" "macros" "io-util"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.22") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1616ix10rrb29qcn3ddzv8i6dl2qfvmq0hnc65k1q5c75cmp6dry")))

