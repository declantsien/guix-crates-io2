(define-module (crates-io xm pp xmpp-derive) #:use-module (crates-io))

(define-public crate-xmpp-derive-0.1.3 (c (n "xmpp-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde_codegen_internals") (r "^0.14.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.10") (f (quote ("full"))) (d #t) (k 0)) (d (n "xmpp-proto") (r "^0") (d #t) (k 0)))) (h "0bvbbvcxfqy7f8vajxy5cvlxlgk6lv31bkkiqvi1rv7gnawram77")))

