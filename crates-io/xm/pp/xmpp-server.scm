(define-module (crates-io xm pp xmpp-server) #:use-module (crates-io))

(define-public crate-xmpp-server-0.1.0 (c (n "xmpp-server") (v "0.1.0") (h "0vgz8haryw7g5nsqir6whc8cmgdkl4bc7xc6yfggk7qhgwnddp1b")))

(define-public crate-xmpp-server-0.1.1 (c (n "xmpp-server") (v "0.1.1") (h "0afyk77papr37dy7njwg5hzrp818vhkad8xipbzk1v7bhy8xc294")))

(define-public crate-xmpp-server-0.1.2 (c (n "xmpp-server") (v "0.1.2") (h "0xg4g61irb5c8vamgdqlcjasy8w4y7z5xl0my0bgjnqm7y6y5xhl")))

