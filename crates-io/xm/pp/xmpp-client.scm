(define-module (crates-io xm pp xmpp-client) #:use-module (crates-io))

(define-public crate-xmpp-client-0.1.0 (c (n "xmpp-client") (v "0.1.0") (h "199likaj0cvkkzz2mj5iybhwq4znjxp2b458vpkdf85p8bccya22")))

(define-public crate-xmpp-client-0.1.1 (c (n "xmpp-client") (v "0.1.1") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0") (d #t) (k 0)) (d (n "sasl") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0") (d #t) (k 0)) (d (n "tokio-io") (r "^0") (d #t) (k 0)) (d (n "tokio-tls") (r "^0") (d #t) (k 0)) (d (n "xmpp-proto") (r "^0") (d #t) (k 0)))) (h "0s7wnblfchb0r9pyicg664dhfkl0l5236zrik7ag092xxzcy1mj3")))

(define-public crate-xmpp-client-0.1.2 (c (n "xmpp-client") (v "0.1.2") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "native-tls") (r "^0") (d #t) (k 0)) (d (n "sasl") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0") (d #t) (k 0)) (d (n "tokio-io") (r "^0") (d #t) (k 0)) (d (n "tokio-tls") (r "^0") (d #t) (k 0)) (d (n "xmpp-proto") (r "^0") (d #t) (k 0)))) (h "073kl5ihmccs9cpb9a8xx49jpxbp5x6qzbp7iw4b0dd0bfky6fg1")))

