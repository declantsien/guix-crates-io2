(define-module (crates-io xm ld xmldecl) #:use-module (crates-io))

(define-public crate-xmldecl-0.1.0 (c (n "xmldecl") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.0") (d #t) (k 0)))) (h "0lg0qrna1bixa9zcyrrmsbx0n1i91gz883ypg7ccy41p42wa9hkl")))

(define-public crate-xmldecl-0.1.1 (c (n "xmldecl") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.0") (d #t) (k 0)))) (h "1346i8gxcakmq5b57zjkspld5wb22br7g5nhjx0p2md1zyj5bj78")))

(define-public crate-xmldecl-0.2.0 (c (n "xmldecl") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8.0") (d #t) (k 0)))) (h "1lgzymcpzm873ija94xgbdgfmsq8q4h3dvm3y5cp8ky9rf541szg")))

