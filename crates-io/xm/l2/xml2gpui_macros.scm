(define-module (crates-io xm l2 xml2gpui_macros) #:use-module (crates-io))

(define-public crate-xml2gpui_macros-0.1.0 (c (n "xml2gpui_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0mwj5iv0r8rbmf58vss2q9518564sj7g0hkbk3qw5jd5z950kfz1")))

