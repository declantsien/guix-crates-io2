(define-module (crates-io xm l2 xml2json) #:use-module (crates-io))

(define-public crate-xml2json-0.1.0 (c (n "xml2json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "1hnga9kcpi0v8q65yqldjk40f1zrk1b1nisjb79vqmx1xmk890rd")))

(define-public crate-xml2json-0.1.1 (c (n "xml2json") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "1iqrg8d1rmm7gw1bjps5rdd0fpc0gxx03dd9kipc26zbqwm33mnq")))

(define-public crate-xml2json-0.1.2 (c (n "xml2json") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "13dzalxv2hpavnla9qig42lj5qf18ry8z96razf54ngkybwrblqk")))

(define-public crate-xml2json-0.1.3 (c (n "xml2json") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "175k7ppadsrpvwcbzx438skkymgzqmf8p2z0ryxy1v7nd6h68nk4")))

