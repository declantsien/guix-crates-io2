(define-module (crates-io xm l2 xml2-src) #:use-module (crates-io))

(define-public crate-xml2-src-2.10.3-alpha (c (n "xml2-src") (v "2.10.3-alpha") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "dunce") (r "^1.0.3") (d #t) (k 0)))) (h "0fy3fsgkk45m8iw327vlmbmmq24fmm7y4vi216qxjpbwm0gbdrk7") (r "1.67")))

