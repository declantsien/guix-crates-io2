(define-module (crates-io xm l2 xml2-sys) #:use-module (crates-io))

(define-public crate-xml2-sys-2.10.3-alpha (c (n "xml2-sys") (v "2.10.3-alpha") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)) (d (n "xml2-src") (r "^2.10.3-alpha") (o #t) (d #t) (k 1)))) (h "09jynncvfhzl0p1xhivqrjwmb8kjga23kaqvh0hw8axr76x29imx") (f (quote (("static" "xml2-src") ("default") ("bindings")))) (l "xml2") (r "1.67")))

