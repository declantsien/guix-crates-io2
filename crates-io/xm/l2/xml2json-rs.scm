(define-module (crates-io xm l2 xml2json-rs) #:use-module (crates-io))

(define-public crate-xml2json-rs-1.0.3 (c (n "xml2json-rs") (v "1.0.3") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1sxxdjjbd0g65wgyh51gys1jqbhi6rim306rmc7x23xach7rd2c4") (y #t)))

(define-public crate-xml2json-rs-1.0.0 (c (n "xml2json-rs") (v "1.0.0") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1mr056pzjwgc5npp2swjcli6mpznsdds3q7sgs2cgiswxnw9iqgy")))

(define-public crate-xml2json-rs-1.0.1 (c (n "xml2json-rs") (v "1.0.1") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1skp80qyl6lr4fw3qx6hlhak5r78mppnal720bmx4xk14nqlzfn6")))

