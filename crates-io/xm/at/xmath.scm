(define-module (crates-io xm at xmath) #:use-module (crates-io))

(define-public crate-xmath-0.1.0 (c (n "xmath") (v "0.1.0") (d (list (d (n "glium") (r "*") (k 0)))) (h "0vcyzkikwkvvjiy0cyiq70v8ifkj35n5wibcyp6hd2binrxi2xam")))

(define-public crate-xmath-0.1.1 (c (n "xmath") (v "0.1.1") (d (list (d (n "glium") (r "^0.7") (k 0)))) (h "122ldfd6pqaiycn7amkjm12l28jk2gm9zd904wdznkjplindi8jz")))

(define-public crate-xmath-0.2.0 (c (n "xmath") (v "0.2.0") (d (list (d (n "glium") (r "^0.7") (k 0)))) (h "1i071nw78q4l09kixhgvk198pqj5ar48dc78jlslh2m8lf0h1y39")))

(define-public crate-xmath-0.2.1 (c (n "xmath") (v "0.2.1") (d (list (d (n "glium") (r "^0.8.2") (k 0)))) (h "0mvxc8qgd55kzxgygjpy6h7ph9qli2dhka9fc04ym4dfjmg30qxm")))

(define-public crate-xmath-0.2.2 (c (n "xmath") (v "0.2.2") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)))) (h "0ylm1c4g5gix1j6c8a722r3da1rb53iaq4lp1wc2yx59n1qlh2xy") (f (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2.3 (c (n "xmath") (v "0.2.3") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)))) (h "0x92ixz5khhjw5z2c72i2iwnzgq1w15r5x9wvv6cpqgxvzq1frf0") (f (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2.4 (c (n "xmath") (v "0.2.4") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)))) (h "1yqf5g7z176l8mw3mgzsgj9gwc5mjbq9iy2piiswv7g3bja6m0xd") (f (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2.5 (c (n "xmath") (v "0.2.5") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)))) (h "0p4al3q84136radjbjkkfs90plr08lzxhlwz4ynajhnpa79w7z0r") (f (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2.6 (c (n "xmath") (v "0.2.6") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)))) (h "0z9ymi6mfympfb5ylj93gvkxj5n974nw3py3m7qxfmq4lwl4yzrm") (f (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2.7 (c (n "xmath") (v "0.2.7") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)))) (h "1v1m9ckmpcr7pilkv0n45ii7wjvm7apjcn1m88nj26c33zsq9ajx") (f (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2.8 (c (n "xmath") (v "0.2.8") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)))) (h "0123wd3ja1l6rwn876kl24kdkmavdcssm8zcf0h47av8inky4w4g") (f (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2.9 (c (n "xmath") (v "0.2.9") (d (list (d (n "glium") (r ">=0.14.0, <0.33.0") (o #t) (k 0)))) (h "1rbkwbsr9lwgpjhkbdyz6vl1p808r0cy7n5bn9rn931d68gy9p0b") (f (quote (("glium-support" "glium"))))))

