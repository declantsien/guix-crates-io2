(define-module (crates-io xm ln xmlns) #:use-module (crates-io))

(define-public crate-xmlns-0.1.0 (c (n "xmlns") (v "0.1.0") (h "0y1i6668mmcmwd4fic7l0ajrnbbnfasxsyyry1hzz8fl4llx6mgl")))

(define-public crate-xmlns-0.1.1 (c (n "xmlns") (v "0.1.1") (h "0dy71w7p9wjrnxdnpkhv3vylr6v613nzmwf13hmick2mwkgysmjg")))

