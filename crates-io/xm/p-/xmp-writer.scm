(define-module (crates-io xm p- xmp-writer) #:use-module (crates-io))

(define-public crate-xmp-writer-0.1.0 (c (n "xmp-writer") (v "0.1.0") (h "1pya3xjabrhavycl54n7ljwppg6z8rsnpxlbn9rcj3wkpfxl5mwz")))

(define-public crate-xmp-writer-0.2.0 (c (n "xmp-writer") (v "0.2.0") (h "128r1sfrs9zafv5ndh71f3a06znabcb6rip9w4clpab4iw9vlhs5")))

