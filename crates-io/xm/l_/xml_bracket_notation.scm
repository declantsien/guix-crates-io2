(define-module (crates-io xm l_ xml_bracket_notation) #:use-module (crates-io))

(define-public crate-xml_bracket_notation-0.1.0 (c (n "xml_bracket_notation") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (f (quote ("encoding" "escape-html"))) (d #t) (k 0)))) (h "0kqh914xcsd5bmagxsyrf1xs6qnlvdr3nld4zpfl7sfqiaw8p8k9")))

