(define-module (crates-io xm l_ xml_sax) #:use-module (crates-io))

(define-public crate-xml_sax-0.0.1 (c (n "xml_sax") (v "0.0.1") (h "1fb1srjski1psgxmaj55697sbba8jf0hanc37akj05cd6n8vsyqf")))

(define-public crate-xml_sax-0.0.2 (c (n "xml_sax") (v "0.0.2") (h "008l06scc5si6y7l2rm9zwq5qksf8acm43ja1iqpjgq6ivydfnln")))

(define-public crate-xml_sax-0.0.3 (c (n "xml_sax") (v "0.0.3") (h "0w379z7x90z9djlasjcbj70hraadkykp6p8mchxzbla237q6nhih")))

(define-public crate-xml_sax-0.0.4 (c (n "xml_sax") (v "0.0.4") (h "1l8vfsn3hbxdl6gzrghfrkhr3ww4mvg5irb1qrcac8r28xg8qb85")))

(define-public crate-xml_sax-0.0.5 (c (n "xml_sax") (v "0.0.5") (h "01vw7c11grga5cvrmyakp9v5n66ppx9r8g17zsfsmq0hyyrqhxj3")))

(define-public crate-xml_sax-0.0.6 (c (n "xml_sax") (v "0.0.6") (h "1ykp4pisq8b6scg8b2ay8shl49b7pinn7mj51mpvdkvnaaiy4f6v")))

(define-public crate-xml_sax-0.1.0 (c (n "xml_sax") (v "0.1.0") (h "0397h4dqqy55acl9vm2bssqgg00zbmsgs76im22zwqw9pdwxyjrg")))

