(define-module (crates-io xm l_ xml_writer) #:use-module (crates-io))

(define-public crate-xml_writer-0.0.1 (c (n "xml_writer") (v "0.0.1") (h "0y2pih8gnrjarz1mp7njiavjn7qawjpzzqxw1k5kyhkndkj9imh7")))

(define-public crate-xml_writer-0.1.0 (c (n "xml_writer") (v "0.1.0") (h "03j1x98fi9dkjrbsg2czrf6cysfp19gf1k84bqjcxbwf76sg4lf6") (y #t)))

(define-public crate-xml_writer-0.1.1 (c (n "xml_writer") (v "0.1.1") (h "04vhfcwmjal2vid2yxrpzcgsz751gg7ijxmp41riw0v3mpd2547x")))

(define-public crate-xml_writer-0.1.2 (c (n "xml_writer") (v "0.1.2") (h "1rismv0jiqr7svgqs1c7fy2djf7dbj7wvhsna215pdsw7gbsjqgq")))

(define-public crate-xml_writer-0.1.3 (c (n "xml_writer") (v "0.1.3") (h "1ar8pp42bnfm37pr3rnvzcr9sdi425yvj520r8n5dfk573vrqi63")))

(define-public crate-xml_writer-0.1.4 (c (n "xml_writer") (v "0.1.4") (h "04vhlcz49fh507rfw58ki3jv1mrz3fb888chsq46h72r6ck1dnis")))

(define-public crate-xml_writer-0.2.0 (c (n "xml_writer") (v "0.2.0") (h "0ikhc9b2pcrcqx82f2v9b70zvjks0rcq7chymq5vv9zjipab0bsq")))

(define-public crate-xml_writer-0.2.1 (c (n "xml_writer") (v "0.2.1") (h "1f0qrz14ihjy2ljgihk7rkzlcsf6wpllggckfj3by476vy5s9491")))

(define-public crate-xml_writer-0.3.0 (c (n "xml_writer") (v "0.3.0") (h "0yqgi7vsm4r9vw0c9vfl1qc2f23mhyqd7x7wkz92bi69nb2js47w")))

(define-public crate-xml_writer-0.4.0 (c (n "xml_writer") (v "0.4.0") (h "13i5lf7szkplxjjlmwy1dzyr2fr88vfw8s4nkvw9ja9rv13shx1s")))

