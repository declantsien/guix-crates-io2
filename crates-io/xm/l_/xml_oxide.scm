(define-module (crates-io xm l_ xml_oxide) #:use-module (crates-io))

(define-public crate-xml_oxide-0.0.1 (c (n "xml_oxide") (v "0.0.1") (d (list (d (n "xml_sax") (r "^0.0.1") (d #t) (k 0)))) (h "1szqa4735zzvqcfv5b58h49i7nrmnybr8q8dndhh20zzd9pbkgz8")))

(define-public crate-xml_oxide-0.0.2 (c (n "xml_oxide") (v "0.0.2") (d (list (d (n "xml_sax") (r "^0.0.1") (d #t) (k 0)))) (h "0ysfabkpg5rsl2963gb7ssmz39mizybbsfc9039jx207i4n3sqg1")))

(define-public crate-xml_oxide-0.0.3 (c (n "xml_oxide") (v "0.0.3") (d (list (d (n "xml_sax") (r "^0.0.1") (d #t) (k 0)))) (h "1yf4kmak04wpk4czp12xli45ihmy6dwyphayxz3g30gdsk16hqfk")))

(define-public crate-xml_oxide-0.0.4 (c (n "xml_oxide") (v "0.0.4") (d (list (d (n "xml_sax") (r "^0.0.2") (d #t) (k 0)))) (h "1qjf4caag9wz2d4z5vf53jhn5n2iz0fd8c9bcgqpgj4qpjhjanv5")))

(define-public crate-xml_oxide-0.0.5 (c (n "xml_oxide") (v "0.0.5") (d (list (d (n "indextree") (r "^1.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "xml_sax") (r "^0.0.2") (d #t) (k 0)))) (h "0dshlxivg25k9zlpaj33h4y0bwradv3j954snahd0rmwrrn3mjlz")))

(define-public crate-xml_oxide-0.0.6 (c (n "xml_oxide") (v "0.0.6") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "xml_sax") (r "^0.0.3") (d #t) (k 0)))) (h "167h1m1w4zmzrr6qrm7hqiv5vybjk3p9f7pvdkacd5s2syqxdsbd")))

(define-public crate-xml_oxide-0.0.8 (c (n "xml_oxide") (v "0.0.8") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "xml_sax") (r "^0.0.5") (d #t) (k 0)))) (h "098qa7blvfl5ipyxa4myk0ppag7pvbyxj4q0brcixg8v241rvr6k")))

(define-public crate-xml_oxide-0.0.9 (c (n "xml_oxide") (v "0.0.9") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "xml_sax") (r "^0.0.6") (d #t) (k 0)))) (h "0gnnm0gdbrcmgyk0zv7gdydf7v0c7fwyxyn7dgxchxlnxnkvpwrx")))

(define-public crate-xml_oxide-0.1.0 (c (n "xml_oxide") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v10nidw1893dp6xh9pxan3h7jd9knlh6dk4s9mry30inb3370wz")))

(define-public crate-xml_oxide-0.1.1 (c (n "xml_oxide") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xzv8pz0fhja16mg5pkh4w9i5aa0hy9jz1nl5vlcwalhdzj98g93")))

(define-public crate-xml_oxide-0.2.0 (c (n "xml_oxide") (v "0.2.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cqq4l1jca67yc13jqygc9w4925hlh20837yx5sdxb68pxk8xf99")))

(define-public crate-xml_oxide-0.3.0 (c (n "xml_oxide") (v "0.3.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1if0bxns5n2i1nimnb03yi2wdg1dqzb9jfvjdf8208jk959wxd09")))

