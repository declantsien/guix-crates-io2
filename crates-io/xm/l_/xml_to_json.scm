(define-module (crates-io xm l_ xml_to_json) #:use-module (crates-io))

(define-public crate-xml_to_json-0.1.0 (c (n "xml_to_json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0d94sx08jq3f3xvdzrsjd9lqyg4y0fhga7gmmkxgyaddiykln3f9")))

