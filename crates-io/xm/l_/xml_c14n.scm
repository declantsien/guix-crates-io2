(define-module (crates-io xm l_ xml_c14n) #:use-module (crates-io))

(define-public crate-xml_c14n-0.1.0 (c (n "xml_c14n") (v "0.1.0") (d (list (d (n "libxml") (r "^0.3.3") (d #t) (k 0)))) (h "1c7hg8ibwqjq70ykgljvjidngfac4jdidnk09k169pbzhxchahnx")))

(define-public crate-xml_c14n-0.1.1 (c (n "xml_c14n") (v "0.1.1") (d (list (d (n "libxml") (r "^0.3.3") (d #t) (k 0)))) (h "112yv0nsxim6w5y56zc7j64dldx2scb7362nbmj1w99hyxxmg673")))

(define-public crate-xml_c14n-0.2.0 (c (n "xml_c14n") (v "0.2.0") (d (list (d (n "libxml") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1b5rhkc0s9bwgazdwjrqls3p2png1gpri9s6rq0jw48pkyq2lx81")))

(define-public crate-xml_c14n-0.3.0 (c (n "xml_c14n") (v "0.3.0") (d (list (d (n "libxml") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0p36cd79h9a39acrldyz3yfhngzya8cycsj7yr4y5x8hb4nhp4hh")))

