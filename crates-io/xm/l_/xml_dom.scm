(define-module (crates-io xm l_ xml_dom) #:use-module (crates-io))

(define-public crate-xml_dom-0.1.0 (c (n "xml_dom") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "091ysa7vaiz4szgga3jsr17bd5fra6v1wfh2c6ckpp8p5cp6fhq4")))

(define-public crate-xml_dom-0.1.1 (c (n "xml_dom") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1iqz3w1xywy63wa7y56rb99cnwhiysl2g0xvwy29dj18sqwbn5bz")))

(define-public crate-xml_dom-0.1.2 (c (n "xml_dom") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0nxfv2lsc255fhcw594wgyhirb295pagj5d1273ikdsr4mxvbw3a")))

(define-public crate-xml_dom-0.1.3 (c (n "xml_dom") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1k2phlh4k1a1z7gmn7avmh4f5j13r1mckq4ysqq7r5nc25d1p7pn")))

(define-public crate-xml_dom-0.1.4 (c (n "xml_dom") (v "0.1.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (o #t) (d #t) (k 0)))) (h "02688inw5k32aq18x3ksypvn6d01q1cq0a14zk8iilk5c5pgig9q") (f (quote (("quick_parser" "quick-xml"))))))

(define-public crate-xml_dom-0.2.0 (c (n "xml_dom") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0cc90qa3hm0f5aqzm1pjfh6gv9zjw1i3fxhwzpx185nxsqan60pn") (f (quote (("quick_parser" "quick-xml") ("default" "quick_parser"))))))

(define-public crate-xml_dom-0.2.1 (c (n "xml_dom") (v "0.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0h6wda1lqg2zinp3x6ys96f1dm9zvys4ag7qrvgpz9ixj35whww1") (f (quote (("quick_parser" "quick-xml") ("default" "quick_parser"))))))

(define-public crate-xml_dom-0.2.2 (c (n "xml_dom") (v "0.2.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "19zs276xmkgqgh2p33qkw1xkd68sq4ih4fpv5vwi91csi2bwcsbh") (f (quote (("quick_parser" "quick-xml") ("default" "quick_parser"))))))

(define-public crate-xml_dom-0.2.3 (c (n "xml_dom") (v "0.2.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "0bkh1jjjdshwp579l945sf2f0gwiijmlyk2r9xsrrj835pgxahdk") (f (quote (("quick_parser" "quick-xml") ("default" "quick_parser"))))))

(define-public crate-xml_dom-0.2.5 (c (n "xml_dom") (v "0.2.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "11vjv7195crq92q34d1ah0rrhimq0ma4z374ij5qzjwxmwjm0y4x") (f (quote (("quick_parser" "quick-xml") ("default" "quick_parser"))))))

(define-public crate-xml_dom-0.2.6 (c (n "xml_dom") (v "0.2.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "1h7p7cz9iblzd8c6fqdkaswmgb5bv2wwg73pr72kxsjn6rgnvl1a") (f (quote (("quick_parser" "quick-xml") ("default" "quick_parser"))))))

