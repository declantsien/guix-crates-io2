(define-module (crates-io xm l_ xml_tokens) #:use-module (crates-io))

(define-public crate-xml_tokens-0.1.0 (c (n "xml_tokens") (v "0.1.0") (h "02d74l8x7nndq8lrcfrky0h2x0h18dwrlqwn36z74k0qhg5bg42i")))

(define-public crate-xml_tokens-0.1.1 (c (n "xml_tokens") (v "0.1.1") (h "1dgg8p86m2vimy73hpn2r2cl82rcmscz7xg1zpqvh841n4sxfhzb")))

(define-public crate-xml_tokens-0.1.2 (c (n "xml_tokens") (v "0.1.2") (h "02kxx5nz2gkphymjp4dhllmkb2989cdp1k4sssk7sak3b1hzbyi0")))

(define-public crate-xml_tokens-0.1.3 (c (n "xml_tokens") (v "0.1.3") (h "0hyc0c5xf98g9j7sgv0dvcsvvdfwrbgyzdx8rc9ar157vaijqycb")))

(define-public crate-xml_tokens-0.1.4 (c (n "xml_tokens") (v "0.1.4") (h "1zqgcal1ykharykr9qb4pxnn0gdwjbzyh207hzgyyqyid3k3nw99")))

(define-public crate-xml_tokens-0.1.5 (c (n "xml_tokens") (v "0.1.5") (h "12x8dab1g4an994a61chd4c0ccp95yxc9f2zzwgqljyxxbxkyq4r")))

