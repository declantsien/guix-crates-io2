(define-module (crates-io xm l1 xml1) #:use-module (crates-io))

(define-public crate-xml1-0.1.0 (c (n "xml1") (v "0.1.0") (h "1dfzc43192zbs4m2sbprg97yb6xvajzh7w01b6qjs6lyh0fjbg8r") (y #t)))

(define-public crate-xml1-0.1.1 (c (n "xml1") (v "0.1.1") (h "07mk6syfz3swf12kqsq78a3cy9d678jbq77wcnlsjf3cvj9zwp69")))

(define-public crate-xml1-0.1.2 (c (n "xml1") (v "0.1.2") (h "1dxbzhv2kb7v4g99r8rczz82mkz8vmc0h4394c74fc55g3v2p89k")))

(define-public crate-xml1-0.1.3 (c (n "xml1") (v "0.1.3") (h "0fy8hkgl3apq7qr6528xf5y1yr31b2ar7f0385wfiix2j4kn6ds8")))

(define-public crate-xml1-0.1.4 (c (n "xml1") (v "0.1.4") (h "1kknir163p8ghq5533bmw5zxx5w8bw6giyx34v386034k6aj1xd0")))

(define-public crate-xml1-0.1.5 (c (n "xml1") (v "0.1.5") (h "1v218njw2xv8n41rbs7w1sdrvqv6fgv0vim7k71vcnkq96h4m8i5")))

(define-public crate-xml1-0.1.6 (c (n "xml1") (v "0.1.6") (h "1krbbifj3jadci3massyxgq8ggf9m8wm1agkmvnxp9hj3ahs6k66")))

