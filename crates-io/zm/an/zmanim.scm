(define-module (crates-io zm an zmanim) #:use-module (crates-io))

(define-public crate-zmanim-0.0.3 (c (n "zmanim") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sunrise") (r "^1.0") (d #t) (k 0)))) (h "1nr90x6ryb58r0qfglxx9lddi1wb5jbpbzvk81bxmicxc6rjm84f")))

(define-public crate-zmanim-0.0.4 (c (n "zmanim") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "sunrise") (r "^1.0.0") (d #t) (k 0)))) (h "03sxaiq7n12r2rw2vz3d8cd22lvpr9rc0cprp3bwd0y75ffzbj2b")))

(define-public crate-zmanim-0.0.5 (c (n "zmanim") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "sunrise") (r "^1.0.0") (d #t) (k 0)))) (h "1fijfkzbl5va9adfdyyimmaljp68hq1g6mdgq4x0j8iw0h5lpaqx")))

(define-public crate-zmanim-0.0.6 (c (n "zmanim") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "sunrise") (r "^1.0.0") (d #t) (k 0)))) (h "0nldh9l4nl3xrhp63hgs4f7dq434d5g4cn8hibkrqkhca7l6sl4h")))

(define-public crate-zmanim-0.0.7 (c (n "zmanim") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "sunrise") (r "^1.0.0") (d #t) (k 0)))) (h "0na3r0jm2ipqhf7dayy7blhf8mz2mzc68lvwa4js4vw7pp9p492a")))

