(define-module (crates-io zm -t zm-test) #:use-module (crates-io))

(define-public crate-zm-test-0.1.0 (c (n "zm-test") (v "0.1.0") (h "059qk3w8gw7yf3bdhahf086ra3z595l3wccs89478wpp4wz72dk7")))

(define-public crate-zm-test-0.1.1 (c (n "zm-test") (v "0.1.1") (h "1wpl3mp2nqdv0vyvcrd5g722rry82h4ng691yiqzd5dymq8bywsz")))

