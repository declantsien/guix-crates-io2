(define-module (crates-io zm q- zmq-sys) #:use-module (crates-io))

(define-public crate-zmq-sys-0.6.0 (c (n "zmq-sys") (v "0.6.0") (h "1f0avm74j67f3871qjafqjzx5d2a51c18q3m450izhfswvihb02i")))

(define-public crate-zmq-sys-0.6.1 (c (n "zmq-sys") (v "0.6.1") (h "12fxbfplfr3qc22zhrvajkzynk7nr0y0nfw7j69zr81iqy1bvfaq")))

(define-public crate-zmq-sys-0.6.2 (c (n "zmq-sys") (v "0.6.2") (h "1h9qx8bg4bf6qqh82ic71i2vv9v9cf0izfynvmzzaz03zackf6n2")))

(define-public crate-zmq-sys-0.6.4 (c (n "zmq-sys") (v "0.6.4") (h "10a172id8y3rk407xkhafziik5ps7s6l59i9h0xgqqlm6x0rqds2")))

(define-public crate-zmq-sys-0.6.5 (c (n "zmq-sys") (v "0.6.5") (h "1cmjdxlqfvyd8lwqas1a2ygizg7azsqldpd8g8bdqvsfjqhp9dyx")))

(define-public crate-zmq-sys-0.6.6 (c (n "zmq-sys") (v "0.6.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "04rb365k0vzrfi80samyspvlwda1fdj6jfragqvpw2yfynl387x2")))

(define-public crate-zmq-sys-0.7.0 (c (n "zmq-sys") (v "0.7.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0c5f2vam0mqa6z1zm6mdyqza8m27v767jb51g6iwqqs78ry2r67b")))

(define-public crate-zmq-sys-0.8.0 (c (n "zmq-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hyal31k9fd2kknci1nh1qfh537sxfww7wsvc4q7456lnr37z1bs")))

(define-public crate-zmq-sys-0.8.1 (c (n "zmq-sys") (v "0.8.1") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "1phdlwfp2vndcyi94c9mhvijmlv9ylz70xppp3cpjhlqb3yamwzc")))

(define-public crate-zmq-sys-0.8.2 (c (n "zmq-sys") (v "0.8.2") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "1cyw4pizyrkqzj6h0fpfry65z0lqhn08sgps9p2zzipk4lfjbk63")))

(define-public crate-zmq-sys-0.9.0 (c (n "zmq-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "1r11aryaahsm4yfvijdisnxr9mpqs882giycs236zqr3cq1724n9") (l "zmq")))

(define-public crate-zmq-sys-0.8.3 (c (n "zmq-sys") (v "0.8.3") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "0z03vvhaad43ghm8jz4drv80kv3ybgrnbi15mchfjhddjps0qxsb") (l "zmq")))

(define-public crate-zmq-sys-0.9.1 (c (n "zmq-sys") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "zeromq-src") (r "^0.1.6") (o #t) (d #t) (k 1)))) (h "09ga1dq2bch0kfhw8n3szasdlcsy8km03ri52s7sia05v2xv1kzg") (f (quote (("vendored" "zeromq-src")))) (l "zmq")))

(define-public crate-zmq-sys-0.11.0 (c (n "zmq-sys") (v "0.11.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "zeromq-src") (r "^0.1.7") (o #t) (d #t) (k 1)))) (h "0j1nkfd6pi1xjccyabpfxajj3wkd4s4b9m1f392mnkg2vm8jqfnk") (f (quote (("vendored" "zeromq-src")))) (l "zmq")))

(define-public crate-zmq-sys-0.12.0 (c (n "zmq-sys") (v "0.12.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "zeromq-src") (r "^0.2.1") (d #t) (k 1)))) (h "0r68zl40j9vmkrlc2il0snx3n1il6v1q39jjarzlsjs9fbf530wf") (l "zmq")))

