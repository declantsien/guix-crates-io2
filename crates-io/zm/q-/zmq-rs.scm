(define-module (crates-io zm q- zmq-rs) #:use-module (crates-io))

(define-public crate-zmq-rs-0.1.3 (c (n "zmq-rs") (v "0.1.3") (d (list (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "zmq-ffi") (r "^0.1.1") (d #t) (k 0)))) (h "1bjj4vcyaq32llqql2dz24qnnnv71x73rinqx332islswvjsgbqm")))

(define-public crate-zmq-rs-0.1.4 (c (n "zmq-rs") (v "0.1.4") (d (list (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "zmq-ffi") (r "0.1.*") (d #t) (k 0)))) (h "1wnkzf5rpsqj3l22mav6cq9rw2z6ywsmxv79dvbqqzvp9bc51sky")))

(define-public crate-zmq-rs-0.1.5 (c (n "zmq-rs") (v "0.1.5") (d (list (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "zmq-ffi") (r "0.1.*") (d #t) (k 0)))) (h "0xfr0kkz76gcfgprnv002212vjhahdgw4q5dwwbwzyn3dpd0nmg2")))

(define-public crate-zmq-rs-0.1.6 (c (n "zmq-rs") (v "0.1.6") (d (list (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "zmq-ffi") (r "0.1.*") (d #t) (k 0)))) (h "0dms5ngr6df57kpbi0vx9hf3lqb5nlw94b5b21jsq1hiia53av0m")))

(define-public crate-zmq-rs-0.1.7 (c (n "zmq-rs") (v "0.1.7") (d (list (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "zmq-ffi") (r "0.1.*") (d #t) (k 0)))) (h "1zfj15pgd0ws123w3nmgrwpgc9xax1msj3c56z8k8wxwfx2lg823")))

(define-public crate-zmq-rs-0.1.8 (c (n "zmq-rs") (v "0.1.8") (d (list (d (n "cfg-if") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "zmq-ffi") (r "0.1.*") (d #t) (k 0)))) (h "1f9f848k4zsng7y0y20wa6kvbnf5nhqrrpyypw2nmvmdkanic0dv")))

