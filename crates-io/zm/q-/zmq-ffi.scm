(define-module (crates-io zm q- zmq-ffi) #:use-module (crates-io))

(define-public crate-zmq-ffi-0.1.1 (c (n "zmq-ffi") (v "0.1.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "02p2vn93ik35wfqdzlww2ax5dkb50p6plsn4qqfpi597294yzkka")))

(define-public crate-zmq-ffi-0.1.2 (c (n "zmq-ffi") (v "0.1.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0n0v4a4p8rzm83qvygjmjx8j0mxwldh738hx294vawbpjsx16dbl")))

(define-public crate-zmq-ffi-0.1.3 (c (n "zmq-ffi") (v "0.1.3") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0317r8h0f3f1qlq9b1g1syp3md9k95jys6k286z04gj90wqb88y6")))

