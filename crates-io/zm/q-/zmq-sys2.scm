(define-module (crates-io zm q- zmq-sys2) #:use-module (crates-io))

(define-public crate-zmq-sys2-0.1.0 (c (n "zmq-sys2") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.0") (d #t) (k 0)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "zeromq-src") (r "^0.2.0") (d #t) (k 1)))) (h "0sw6jw7qq56dp8iy5cwmfj4v21rn11bky2qq82rgs882ac3gbmm6") (l "zmq")))

(define-public crate-zmq-sys2-0.2.0 (c (n "zmq-sys2") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "libsodium-sys-stable") (r "^1.0") (d #t) (k 0)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "zeromq-src") (r "^0.2.1") (d #t) (k 1)))) (h "0mxxzw47mw0q8hg6krv6sm1kdg98zab2m1fan006wk2c2hcj9cd3") (l "zmq")))

(define-public crate-zmq-sys2-0.3.0 (c (n "zmq-sys2") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "zeromq-src") (r "^0.2.1") (d #t) (k 1)))) (h "1n27cf2dylyn8n3pshhhlzbfj52hk5yqll6wyj66fjy1n8vr0zjz") (l "zmq")))

