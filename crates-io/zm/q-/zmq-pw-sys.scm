(define-module (crates-io zm q- zmq-pw-sys) #:use-module (crates-io))

(define-public crate-zmq-pw-sys-0.9.0 (c (n "zmq-pw-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "1msrcnw1pkakbjy18a3l5nar33g340psfp4ggy1vcx1kwn31yjpv")))

(define-public crate-zmq-pw-sys-0.9.1 (c (n "zmq-pw-sys") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0ppl35blbiiihn57zxk5nk5qgllp0vxxckb20bbnaaxqwifsn630")))

(define-public crate-zmq-pw-sys-0.9.2 (c (n "zmq-pw-sys") (v "0.9.2") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0wwr0apfxmzmld3zcyy365k68w9csbpvz7di9qhjlnazfhpxbnkm")))

(define-public crate-zmq-pw-sys-0.9.3 (c (n "zmq-pw-sys") (v "0.9.3") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1dhcjwlamww0gdylfkrw2gabid9y41xpqjfvv2xr9y7v69lza8kz")))

(define-public crate-zmq-pw-sys-0.9.4 (c (n "zmq-pw-sys") (v "0.9.4") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0vks60y556c1hrlc252a5xkhid6w2x2ld41nhn9hxcpd59cqahns")))

(define-public crate-zmq-pw-sys-0.9.5 (c (n "zmq-pw-sys") (v "0.9.5") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1yw8cfhgsw6jzaf9gnl134kcgb784s1mki4vhx4flyaldz6yf50j")))

(define-public crate-zmq-pw-sys-0.9.7 (c (n "zmq-pw-sys") (v "0.9.7") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0v21zqcd99jh8qb5i4szb0wbn9ahks0vwb2s5s5an10kjj8fkl18")))

(define-public crate-zmq-pw-sys-0.9.8 (c (n "zmq-pw-sys") (v "0.9.8") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "metadeps") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "09h1mwp9r8frdksk9dfpmizmazkmvxhlbfb5h0w9gir8g4bggk0i")))

