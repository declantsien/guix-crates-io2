(define-module (crates-io zm em zmem) #:use-module (crates-io))

(define-public crate-zmem-0.2.1 (c (n "zmem") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jbl97shfll3s70c7hqgx2lxkd9pp98w3qhgnbp9gvh927drvsk0")))

