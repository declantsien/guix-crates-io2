(define-module (crates-io js er jservice) #:use-module (crates-io))

(define-public crate-jservice-0.1.0 (c (n "jservice") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^0.7.5") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.5") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "syntex") (r "^0.32.0") (d #t) (k 1)))) (h "067q4dym72h2pn4z7hjbqx5jwxxfb2rmxfwa45zv0xblykzj2v0x")))

(define-public crate-jservice-0.1.1 (c (n "jservice") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^0.7.5") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.5") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "syntex") (r "^0.32.0") (d #t) (k 1)))) (h "1slc29y3m6xzffv8j7z79cfzcyvaxgdg1pmb7lbzgzpxnwv94fgr")))

(define-public crate-jservice-0.1.2 (c (n "jservice") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.33") (d #t) (k 1)))) (h "05lc2bs44bl9qzms3w10n6b7bsr9l74xyiidnw9bzp3z6pbrq3mv")))

(define-public crate-jservice-0.1.3 (c (n "jservice") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.35") (d #t) (k 1)))) (h "1d4yxb1zxkbpzc9zr1f8wq6r88imw9yld1697fzg624zsgajp1hh")))

(define-public crate-jservice-0.1.4 (c (n "jservice") (v "0.1.4") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0xd501k75bflg41wg0xh4f5hx02k8mshhzncdvy0zkgdlkhqzgwd")))

