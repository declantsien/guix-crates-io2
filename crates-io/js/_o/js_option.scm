(define-module (crates-io js _o js_option) #:use-module (crates-io))

(define-public crate-js_option-0.1.0 (c (n "js_option") (v "0.1.0") (d (list (d (n "serde_crate") (r "^1.0.125") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_crate") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2) (p "serde")) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "19l5lr7ccbbwb0qfryndfdanl7ja2hnjs9idj8cvg0zi6kf3djjp") (f (quote (("serde" "serde_crate") ("default" "serde"))))))

(define-public crate-js_option-0.1.1 (c (n "js_option") (v "0.1.1") (d (list (d (n "serde_crate") (r "^1.0.125") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_crate") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 2) (p "serde")) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0g2273zp51nik16s95zyxqhv5qh6ybdrhdh1cykr65bsjmri6hk8") (f (quote (("serde" "serde_crate") ("default" "serde"))))))

