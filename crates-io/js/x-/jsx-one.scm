(define-module (crates-io js x- jsx-one) #:use-module (crates-io))

(define-public crate-jsx-one-0.0.1 (c (n "jsx-one") (v "0.0.1") (d (list (d (n "swc_common") (r "^0.11.0") (f (quote ("tty-emitter"))) (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.46.0") (f (quote ("parser"))) (d #t) (k 0)))) (h "1xz2hyllr72vdrj3dpsm1x8hgi92wq652gl1fpagm89r8fsrs9al")))

