(define-module (crates-io js c- jsc-sys) #:use-module (crates-io))

(define-public crate-jsc-sys-0.1.0 (c (n "jsc-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07v4pkgm6n0yc2ndcyig91l30481k53aysyip1viszzikjv01z3s")))

(define-public crate-jsc-sys-0.1.2+r201969 (c (n "jsc-sys") (v "0.1.2+r201969") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1p6adcrfrm4xlw8xn0nijqbrhsi7s4hix3vj0rr615picschjwg3")))

