(define-module (crates-io js to jstorchtools) #:use-module (crates-io))

(define-public crate-jstorchtools-0.1.0 (c (n "jstorchtools") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.21.0") (d #t) (k 0) (p "ratatui")))) (h "1272rfy7kgqcjvh7spqw6l27wdgdh9h3cgppwpq557bada93aqxf")))

(define-public crate-jstorchtools-0.2.0 (c (n "jstorchtools") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.21.0") (d #t) (k 0) (p "ratatui")))) (h "09g83dw8932q6bf2yf3kpvfq984n814gb6mizgip58c45x387gm5")))

(define-public crate-jstorchtools-0.2.1 (c (n "jstorchtools") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.21.0") (d #t) (k 0) (p "ratatui")))) (h "19qgh1lrhc25mjbnciryidpcmkp62qa7wmdb7ccwngl9s775wwlp")))

