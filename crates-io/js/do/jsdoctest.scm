(define-module (crates-io js do jsdoctest) #:use-module (crates-io))

(define-public crate-jsdoctest-0.1.0 (c (n "jsdoctest") (v "0.1.0") (h "1vdla0vi3npf6vw5dkqpxi8mcjvqfx76aifgx1mp5r3yqwx037f2")))

(define-public crate-jsdoctest-0.2.0 (c (n "jsdoctest") (v "0.2.0") (h "09rixqnlb563vqm9682yqzb021x1923z87ppkdpqqfpvc87nqsqw")))

(define-public crate-jsdoctest-0.3.0 (c (n "jsdoctest") (v "0.3.0") (h "1v2gq8qyacmv3yri0c5pv98rbj1a5ga1hd2j6wkbcrbfp0b9gfap")))

(define-public crate-jsdoctest-0.4.0 (c (n "jsdoctest") (v "0.4.0") (h "1602lacc9br1jfxcwylv2c1rdi413qhhw2ry3g36r6600zmpn6jc")))

