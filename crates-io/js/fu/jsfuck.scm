(define-module (crates-io js fu jsfuck) #:use-module (crates-io))

(define-public crate-jsfuck-0.1.0 (c (n "jsfuck") (v "0.1.0") (h "13sngxrq76dibr879k59fcc039zv2rqn8lx0qy3xw7xhk76pba9c") (y #t)))

(define-public crate-jsfuck-1.0.0 (c (n "jsfuck") (v "1.0.0") (h "0wvx9vpbyxk5xh2134jxgh4y1s1vrg34r2y3sc0mk98vw6xmh7jh") (y #t)))

(define-public crate-jsfuck-1.0.1 (c (n "jsfuck") (v "1.0.1") (h "1na152knsxa4xqanzlyjsxz93h0bkj9qvzsxdqx7fmwrfg5irjr7") (y #t)))

(define-public crate-jsfuck-1.0.2 (c (n "jsfuck") (v "1.0.2") (h "070c6c9i6w8ch2jv9bvnckfnsib8nw733v6ag80r0bng3dpgjipx") (y #t)))

(define-public crate-jsfuck-1.0.3 (c (n "jsfuck") (v "1.0.3") (h "114ik1hh9a16q2vn1vxk90b19xlagi6jx9ikfcbcfax8j224yvzc") (y #t)))

(define-public crate-jsfuck-1.0.4 (c (n "jsfuck") (v "1.0.4") (h "06gx6j17ibjpdayfpkfarhrhk6w0y5yw1dad9vghkz9dnfhdlvmb") (y #t)))

(define-public crate-jsfuck-1.0.5 (c (n "jsfuck") (v "1.0.5") (h "0ff21pj3jszy7fs5yb39ka6sb3flvhfyzibasqrgfk4hnpa0fzjz") (y #t)))

(define-public crate-jsfuck-1.0.6 (c (n "jsfuck") (v "1.0.6") (h "1ib5pg93fzw2ibfd96i99swy63097nb1j4ijrii45lkdmk6mlhh3")))

