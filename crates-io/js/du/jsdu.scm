(define-module (crates-io js du jsdu) #:use-module (crates-io))

(define-public crate-jsdu-0.1.0 (c (n "jsdu") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "0fdh38327lqsivmhw7l1a2bmncaa2dvs84k075p2idid95xd5mf6")))

