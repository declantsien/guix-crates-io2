(define-module (crates-io js s_ jss_derive) #:use-module (crates-io))

(define-public crate-jss_derive-0.1.0 (c (n "jss_derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "enum_extract") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1z3430dihpj47a99512d1n6ghzb86r7v9w9y7vp6x7fvwjknn6jr")))

