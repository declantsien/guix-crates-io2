(define-module (crates-io js ti jstime) #:use-module (crates-io))

(define-public crate-jstime-0.5.2 (c (n "jstime") (v "0.5.2") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "0vvhgsjh4w05igdav2jzbyrdy827zrfm1zsfi75dpwg1kxymarhf")))

(define-public crate-jstime-0.6.0 (c (n "jstime") (v "0.6.0") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "0g4zn443x8zkjc5ibh85q65hnszfq1768d6azfky18r9l01n0f88")))

(define-public crate-jstime-0.6.2 (c (n "jstime") (v "0.6.2") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "0nx4cr2zcl1mbcypsh9w7f5mhmzbgr0s4pjjkv6jl22v5g90m1q2")))

(define-public crate-jstime-0.7.0 (c (n "jstime") (v "0.7.0") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "00vm06n61qsyp6mmiz5z0rf8fg1l2p6ar5vcpjfrq488gja4ddf2")))

(define-public crate-jstime-0.7.1 (c (n "jstime") (v "0.7.1") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "13ldpa1vsqgfcabzi29kcvwljy2kszfdp5l0qgjkhv9am09ql353")))

(define-public crate-jstime-0.8.0 (c (n "jstime") (v "0.8.0") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "1gp92f53jvr1blwpyq2cxh7il0l1baglfqxqi7v082pmlyijadjv")))

(define-public crate-jstime-0.7.2 (c (n "jstime") (v "0.7.2") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "1msrm4qvb10flak859i7z9ww4xsqn3bw0h3vw4rsn4z98aipzz6m")))

(define-public crate-jstime-0.8.1 (c (n "jstime") (v "0.8.1") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "1b146ix8qwb3i6510av9chmvyxya42q154956yzs9sip3fisrp25")))

(define-public crate-jstime-0.8.2 (c (n "jstime") (v "0.8.2") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "12lvs383ns1dnm0wjgi5qcmm83ahx2i1xg7fs2i2lq7hvy4388vk")))

(define-public crate-jstime-0.8.3 (c (n "jstime") (v "0.8.3") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "0519ccpny52c6hvjmb65mlgjmj26c5awzlygbxgcyy0f5v3acygz")))

(define-public crate-jstime-0.8.4 (c (n "jstime") (v "0.8.4") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "05zlcmca0ynlc6f1y17xwww2mn7ybcidshnpv72z24sgawf2c4ga")))

(define-public crate-jstime-0.8.5 (c (n "jstime") (v "0.8.5") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "0wrkbvy1g9ccxvgqlj3hzishs7kwkc6b68w9kbb6ldc4xrz2zf6s")))

(define-public crate-jstime-0.8.6 (c (n "jstime") (v "0.8.6") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "177vi07yqq1w6r1qyxbrb2vqf6sgz83abdf3i6xds87avj66h6kz")))

(define-public crate-jstime-0.8.7 (c (n "jstime") (v "0.8.7") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "1qx6cwq8p3xz6q10wpb4q9hj33085m4b3la9kw3l9hbspg1fxvif")))

(define-public crate-jstime-0.8.8 (c (n "jstime") (v "0.8.8") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "038m5nh4kkdhza93c1z3l30504kqxx9xg8ffjlyk82dmp9r2x8mn")))

(define-public crate-jstime-0.8.9 (c (n "jstime") (v "0.8.9") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "1q5yhwnvbw460dbjpdpczgmvzwv3nfkha7hfm5p04cq3jkj6chs8")))

(define-public crate-jstime-0.8.10 (c (n "jstime") (v "0.8.10") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "0i8gwsvvnhvgkyyk5zscsnarlwym050nikl147p6cgcnakvdlysn")))

(define-public crate-jstime-0.9.0 (c (n "jstime") (v "0.9.0") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "10bz01yanzn3bmdl6wynmlph4sggilb9yrfr6lmf2i8898408v06")))

(define-public crate-jstime-0.9.1 (c (n "jstime") (v "0.9.1") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)))) (h "15x6c8dgbkp89n056ry89g1c398if6gllp6pw29hsifhdmv7gfi4")))

(define-public crate-jstime-0.10.0 (c (n "jstime") (v "0.10.0") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1w6vjz49sz33dgbf2bqxmvg4ibqdrnc4bqp19wkx4cskzs7gl665")))

(define-public crate-jstime-0.11.0 (c (n "jstime") (v "0.11.0") (d (list (d (n "rusty_v8") (r "^0.8.1") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "12lfmz9dc1fwh30lidspsf5lrjnzwpp5lv8llx4bb57a2rqd9dxk")))

(define-public crate-jstime-0.12.1 (c (n "jstime") (v "0.12.1") (d (list (d (n "jstime_core") (r "^0.12.1-alpha.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1km4745bm9211m60dgq77jbw4m8g705kj35wp22s2dlw2l5zil3a")))

(define-public crate-jstime-0.12.2 (c (n "jstime") (v "0.12.2") (d (list (d (n "jstime_core") (r "^0.12.2-alpha.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1axghg4l98pz6lkph2n2w2slpb95sa9vlxacb75kgpajbnmqcjjh")))

(define-public crate-jstime-0.12.3 (c (n "jstime") (v "0.12.3") (d (list (d (n "jstime_core") (r "^0.12.3") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "17vjzx1ff5pzrijvqz99qb0pghjb9y8i7fmn43zbg2yg570qhvd9")))

(define-public crate-jstime-0.12.4 (c (n "jstime") (v "0.12.4") (d (list (d (n "jstime_core") (r "^0.12.4") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0bf43b8rhhf05fsgwsyrjgmf469vcwggk0i7i8v39w2arpmsnh4j")))

(define-public crate-jstime-0.12.5 (c (n "jstime") (v "0.12.5") (d (list (d (n "jstime_core") (r "^0.12.5") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "07xz9nkg9h316jh5wwkmwlpzcxng1wjqnj7vjav7hpvzmpbfjld4")))

(define-public crate-jstime-0.12.6 (c (n "jstime") (v "0.12.6") (d (list (d (n "jstime_core") (r "^0.12.6") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1s4g0hda9cmzr0chbiva182pg43aqayqbgbvnj2scakdi74gr3qc")))

(define-public crate-jstime-0.13.0 (c (n "jstime") (v "0.13.0") (d (list (d (n "jstime_core") (r "^0.13.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.0") (d #t) (k 1)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "09kxx7li99n7rnfgjmfascijq9il9ra4510d83ggn1ggbjcazp0i")))

(define-public crate-jstime-0.13.1 (c (n "jstime") (v "0.13.1") (d (list (d (n "jstime_core") (r "^0.13.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.1") (d #t) (k 1)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0xw9gh4wr5gpgdgx61srq1ya5fn6vwmz44fzcjn2bm1j663wi7jj")))

(define-public crate-jstime-0.13.2 (c (n "jstime") (v "0.13.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.2") (d #t) (k 1)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1mcp0fl6lqx86bqnmqqs5j62bkr0vzhj4d59nqkn6fxl0z3hvirc")))

(define-public crate-jstime-0.13.3 (c (n "jstime") (v "0.13.3") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.3") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.3") (d #t) (k 1)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "05rahiczzkq2l1hi3l8wz2maifg8jxvn4zzwgyb87sh6mf3a99f8")))

(define-public crate-jstime-0.13.4 (c (n "jstime") (v "0.13.4") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.4") (d #t) (k 0)) (d (n "jstime_core") (r "^0.13.4") (d #t) (k 1)) (d (n "rustyline") (r "^6.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "09x0ixr3v9va58cir2fawhix1yqavnpccg94jq1hc4802rlmadpc")))

(define-public crate-jstime-0.14.0 (c (n "jstime") (v "0.14.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.14.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.14.0") (d #t) (k 1)) (d (n "rustyline") (r "^6.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "08yf7z728r3qdx26w5d62fkncv1rwqkf4hsb3k3wrx4i1v086psa")))

(define-public crate-jstime-0.15.0 (c (n "jstime") (v "0.15.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.15.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.15.0") (d #t) (k 1)) (d (n "rustyline") (r "^6.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1fxqvmpqmri306ahw5d4988mja1yz4rbjcwh0viaps2fja6hgvll")))

(define-public crate-jstime-0.16.0 (c (n "jstime") (v "0.16.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.16.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.16.0") (d #t) (k 1)) (d (n "rustyline") (r "^7.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1rj8lcvjfxddjby6b3lxw8yh3y99hsy636bfn570qc1yhmld9bb3")))

(define-public crate-jstime-0.17.0 (c (n "jstime") (v "0.17.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.17.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.17.0") (d #t) (k 1)) (d (n "rustyline") (r "^7.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1dv29rn3cc07dir5fh3kzl46fni1vlhzhzhx0y7ivkya9zl4b9nk")))

(define-public crate-jstime-0.18.0 (c (n "jstime") (v "0.18.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.18.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.18.0") (d #t) (k 1)) (d (n "rustyline") (r "^7.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0x24gwwzbrh24r8g5zs9rh5rkfda6v6mn80gyjlkqv5vjcnd33wx")))

(define-public crate-jstime-0.19.0 (c (n "jstime") (v "0.19.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.19.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.19.0") (d #t) (k 1)) (d (n "rustyline") (r "^7.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "04dm52c2d3kyv4jdaqgk6cbzfj67milwandv587wgn4f4pavd386")))

(define-public crate-jstime-0.19.1 (c (n "jstime") (v "0.19.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.19.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.19.1") (d #t) (k 1)) (d (n "rustyline") (r "^7.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0ngqixkpx9lkcjfmxss14gr6hyl0zy8dmpjdw3inv2cxczxhp8zz")))

(define-public crate-jstime-0.20.0 (c (n "jstime") (v "0.20.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.20.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.20.0") (d #t) (k 1)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "17539dvvcbpclfg2zpfrx43afhk3lfwxpnglsh861ismw4asfwc2")))

(define-public crate-jstime-0.20.1 (c (n "jstime") (v "0.20.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.20.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.20.1") (d #t) (k 1)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0acbcs90bkbvr5mhw6wfq0jjpz416ff330mnsg0a3np41ipj682x")))

(define-public crate-jstime-0.21.0 (c (n "jstime") (v "0.21.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.21.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.21.0") (d #t) (k 1)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0cxwb7nz5kkywk2hhvp2p2j89qgvvmnr17l3505krbg5q6kji7kx")))

(define-public crate-jstime-0.22.0 (c (n "jstime") (v "0.22.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.22.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.22.0") (d #t) (k 1)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0jjgxvwasdjdf5gv2chx2dl9ajq1zzv5c7d3s4nmimm2y9pqncis")))

(define-public crate-jstime-0.22.1 (c (n "jstime") (v "0.22.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.22.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.22.1") (d #t) (k 1)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1wqqkj1k6pi1iflshh06vp8hdbpca82671b1lscb6n08ycfpisl5")))

(define-public crate-jstime-0.23.0 (c (n "jstime") (v "0.23.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.23.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.23.0") (d #t) (k 1)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "03d99z1mm3chdfksin0sw54ryxp7f56np3s7jnqviy9j3p97csph")))

(define-public crate-jstime-0.24.0 (c (n "jstime") (v "0.24.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.24.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.24.0") (d #t) (k 1)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "042nlp63r9ifvc1rnmirdmzafvh86p6gbbi08q8amrmxb8yn1pmh")))

(define-public crate-jstime-0.25.0 (c (n "jstime") (v "0.25.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.25.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.25.0") (d #t) (k 1)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0272h26hh0rvp4j3kbd8s8vywknb73bqkpcxvvpa45v66v0k86cf")))

(define-public crate-jstime-0.26.0 (c (n "jstime") (v "0.26.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.26.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.26.0") (d #t) (k 1)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1l2i6xzkcl9hg6sdr4v1rrjnd1yrilqqzlcyqrbim45zy2sxfkz4")))

(define-public crate-jstime-0.27.0 (c (n "jstime") (v "0.27.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.27.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.27.0") (d #t) (k 1)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0bwizw2x6gpqfvavfak35d7mqa2mqk18k2aws8zv8dcmylm6qnk5")))

(define-public crate-jstime-0.28.0 (c (n "jstime") (v "0.28.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.28.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.28.0") (d #t) (k 1)) (d (n "predicates") (r "^1.0.7") (d #t) (k 2)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0ppqyc882y58cll101dnxn7ay9ildhm6ij36wf14ybs7q4rncc46")))

(define-public crate-jstime-0.28.1 (c (n "jstime") (v "0.28.1") (d (list (d (n "assert_cmd") (r "^1.0.7") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.28.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.28.1") (d #t) (k 1)) (d (n "predicates") (r "^2.0.0") (d #t) (k 2)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "13am93q8nykrh49w4gzgpjimskq7vigrqr09lsqd03vx7ir4fwxk")))

(define-public crate-jstime-0.29.0 (c (n "jstime") (v "0.29.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.29.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.29.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.1") (d #t) (k 2)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1092vq65ql7r2hckasds9124f6dn6h828achngmy42d5y1c1vcs3")))

(define-public crate-jstime-0.30.0 (c (n "jstime") (v "0.30.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.30.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.30.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.1") (d #t) (k 2)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "14yz2yrsbb5y5fi87k9g0mvnfampb350vrm87z48lzhcs14vpndy")))

(define-public crate-jstime-0.30.1 (c (n "jstime") (v "0.30.1") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.30.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.30.1") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1qlydcikj4jl69a625n0i5ki7xb9b3vks5fl0ji0xg2704q4dc0n")))

(define-public crate-jstime-0.30.2 (c (n "jstime") (v "0.30.2") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.30.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.30.2") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1z9gjlsnmxh8wq4p24mdnr0qv7gfkb7hgw1w0p07gmf8jdqdy158")))

(define-public crate-jstime-0.31.0 (c (n "jstime") (v "0.31.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1qy8dd4qvmlmp6wqp1y552dhpm53pdaslgkcmrzy1c19bjzh9qnf")))

(define-public crate-jstime-0.31.1 (c (n "jstime") (v "0.31.1") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.1") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0yg1ir0fx0485rv0n1k1xq4nx9lbxki93firk07ln7wcx771dsq4")))

(define-public crate-jstime-0.31.2 (c (n "jstime") (v "0.31.2") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.2") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1dhz2mjd51z7k9cwmrb6hzzvr8nvvlsnb0p29wl0h9cidmh4p0jy")))

(define-public crate-jstime-0.31.3 (c (n "jstime") (v "0.31.3") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.3") (d #t) (k 0)) (d (n "jstime_core") (r "^0.31.3") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0h674nx8xi2nidn16qhnd3vxhjlvj0gyxqsixfjr62rn405l4vv8")))

(define-public crate-jstime-0.32.0 (c (n "jstime") (v "0.32.0") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "jstime_core") (r "^0.32.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.32.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "15x665zc79wrk3s1pyk2b5ipx37i8ky6svzy7fghm7i6lmy98m0y")))

(define-public crate-jstime-0.33.0 (c (n "jstime") (v "0.33.0") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.33.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.33.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1cbcvrxanjfms0pc27miad0ldrdjmgpy8flys8asrrz0zcmr6wmy")))

(define-public crate-jstime-0.34.0 (c (n "jstime") (v "0.34.0") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.34.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.34.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1y7f97jw5dlg5irabrifg3qs0l5pq4y4h8hznd4cdd75ndsvf5sw")))

(define-public crate-jstime-0.35.0 (c (n "jstime") (v "0.35.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.35.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.35.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0wryrgk0wvf0b9l2bf7jli0897y952kfbcmsbg0i60jpyjzhv4r3")))

(define-public crate-jstime-0.36.0 (c (n "jstime") (v "0.36.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.36.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.36.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0jxrrg59ywa9gp08smww0qf4visdnmij2n03rfdhf67a2j8dscaw")))

(define-public crate-jstime-0.37.0 (c (n "jstime") (v "0.37.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.37.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.37.0") (d #t) (k 1)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "08m0kb84fz643sd2sfd7jzfz56619mvq6dsnc0pr0n8avznq15jp")))

(define-public crate-jstime-0.38.0 (c (n "jstime") (v "0.38.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.38.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.38.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.0") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0i70sr1c5clv715kgg7xic9pdgglydwm4mfbyhy15973ijm4v3vq")))

(define-public crate-jstime-0.39.0 (c (n "jstime") (v "0.39.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.39.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.39.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "08bvd2dx62mb03dh18ldklmcp9pks8kkw813a56jwhwbrikkmczp")))

(define-public crate-jstime-0.40.0 (c (n "jstime") (v "0.40.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.40.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.40.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1j2qzypk3lv1bb0n8ggb7n9dbzr54l2iaz8ldpymdrh1f3h4imhi")))

(define-public crate-jstime-0.41.0 (c (n "jstime") (v "0.41.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.41.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.41.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0yp0npnv47nwl9kfy1sjqamn728v9n3r4pmcjxdyfmnlb268ig8m")))

(define-public crate-jstime-0.42.0 (c (n "jstime") (v "0.42.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.42.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.42.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0m6dnpnn72lrjwbhv05lqmk8gna2h64jr5f0dn1jbi93baj2i956")))

(define-public crate-jstime-0.43.0 (c (n "jstime") (v "0.43.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.43.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.43.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1bsh5mvfq9w7vd7fhgph4bb9ck5x3srajfqwxg7hv51c0g3iiksh")))

(define-public crate-jstime-0.44.0 (c (n "jstime") (v "0.44.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.44.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.44.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "11g93whzj6fzx6vladmldy2l25viakpz4a9k9r25l7jliny86ss7")))

(define-public crate-jstime-0.45.0 (c (n "jstime") (v "0.45.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.45.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.45.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0zmc3zma2qrjj397y6vbdd7xh7wpdsijvr4f4chidlhg2y2wcavs")))

(define-public crate-jstime-0.46.0 (c (n "jstime") (v "0.46.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.46.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.46.0") (d #t) (k 1)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "043g141nilfvqnsznyvhd4jb38k4y7knpghr6hsy19crkv76krxr")))

(define-public crate-jstime-0.47.0 (c (n "jstime") (v "0.47.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.47.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.47.0") (d #t) (k 1)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0fjpmjhgfwpdnkcskq0xyachzaixzqlx1rhrsz0r95qa3jd3z8gf")))

(define-public crate-jstime-0.48.0 (c (n "jstime") (v "0.48.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.48.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.48.0") (d #t) (k 1)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "12jzja16kjfwdqcckqx614kh91brf9m4gplw0h7n6k1y6sypdkl9")))

(define-public crate-jstime-0.49.0 (c (n "jstime") (v "0.49.0") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.49.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.49.0") (d #t) (k 1)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0x8xwlvdk032w0rpfn74lm73d5qpvj0rgc0lfnhmxg91qnj2dxlf")))

(define-public crate-jstime-0.50.0 (c (n "jstime") (v "0.50.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "jstime_core") (r "^0.50.0") (d #t) (k 0)) (d (n "jstime_core") (r "^0.50.0") (d #t) (k 1)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rustyline") (r "^14.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1afmbfk0dxlsdm1fpj65gmqqgzn0qfh98jwb5vyrmknymdw27l9i")))

