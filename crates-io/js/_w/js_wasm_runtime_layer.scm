(define-module (crates-io js _w js_wasm_runtime_layer) #:use-module (crates-io))

(define-public crate-js_wasm_runtime_layer-0.4.0 (c (n "js_wasm_runtime_layer") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "fxhash") (r "^0.2") (k 0)) (d (n "js-sys") (r "^0.3") (k 0)) (d (n "slab") (r "^0.4") (k 0)) (d (n "smallvec") (r "^1.11") (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (k 0)) (d (n "wasm_runtime_layer") (r "^0.4.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.118") (k 0)))) (h "07x90r3rhxa2yvk15amiw76cnddnsir1jjbygpdi47h920r14876") (s 2) (e (quote (("tracing" "dep:tracing"))))))

