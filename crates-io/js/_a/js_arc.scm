(define-module (crates-io js _a js_arc) #:use-module (crates-io))

(define-public crate-js_arc-0.1.0 (c (n "js_arc") (v "0.1.0") (d (list (d (n "async-channel") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0xx5ia0k2kfkssa21a4rr0kgvbx3jccvlr5wbb7mfq9sngc8z95c")))

