(define-module (crates-io js _t js_typify_gostruct) #:use-module (crates-io))

(define-public crate-js_typify_gostruct-0.1.0 (c (n "js_typify_gostruct") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "06qx4kmalc42pip3h60m03n7ca3bp1jl5rb20wfs1aq9yg99gg5i") (y #t)))

(define-public crate-js_typify_gostruct-0.1.1 (c (n "js_typify_gostruct") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "12k6j1j8qd5qn1rspsiw2mj3p73wh744pcd10mpqv88ip282idys") (y #t)))

(define-public crate-js_typify_gostruct-0.1.2 (c (n "js_typify_gostruct") (v "0.1.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0jp614irg5h14ah52k01zhdkfijdmqqwwnjq6zgql442mihc3wb0") (y #t)))

(define-public crate-js_typify_gostruct-0.2.0 (c (n "js_typify_gostruct") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "15ayq9n72ciyrm9py9y91d32hf9l83g3pxf7n0p0jmppqk22wg7s") (y #t)))

(define-public crate-js_typify_gostruct-0.3.0 (c (n "js_typify_gostruct") (v "0.3.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1qy7nnyszaprrks8zdk3qw73cd1ggji95zm9nhr18ksn82yj9a64") (y #t)))

(define-public crate-js_typify_gostruct-0.3.1 (c (n "js_typify_gostruct") (v "0.3.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0lfnpnbj626ld4gvy68aqpic6sjzddjawmm3fd1ibdrzvzr6yvk7") (y #t)))

(define-public crate-js_typify_gostruct-0.3.3 (c (n "js_typify_gostruct") (v "0.3.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0vz9lrbznjjj4hzf360xbxw67p67z37v33k18bfxfd3pkvjlx3i2") (y #t)))

(define-public crate-js_typify_gostruct-0.3.4 (c (n "js_typify_gostruct") (v "0.3.4") (h "04pcgmhpd68kh27334gbj4ivj9rdaw5fhiz6jdszvssmqlkc4rhc") (y #t)))

(define-public crate-js_typify_gostruct-1.0.0 (c (n "js_typify_gostruct") (v "1.0.0") (h "0snnwijrk6ybdr3y81nf0ly6rc43r8x3p8bw8rp565kv892vhqb7") (y #t)))

(define-public crate-js_typify_gostruct-2.0.0 (c (n "js_typify_gostruct") (v "2.0.0") (h "07qjdbdbwac721h0sm9v83k2q358bvc23l8qch7qa35gwg1ichpj") (y #t)))

(define-public crate-js_typify_gostruct-2.0.1 (c (n "js_typify_gostruct") (v "2.0.1") (h "161yzc3z6p1n3jpfx3688zy645nwjkd4p94x04wvn9b79rwxdzqx") (y #t)))

(define-public crate-js_typify_gostruct-2.0.2 (c (n "js_typify_gostruct") (v "2.0.2") (h "1c3rgpy9cm50wpj68cigrcgrp4902iijbgwmzs238w9x3hnrrmsn") (y #t)))

(define-public crate-js_typify_gostruct-2.1.0 (c (n "js_typify_gostruct") (v "2.1.0") (h "1gzh5s58lr510vdf924zq3q91fg1h5cv0khx45b9b3f5x4953zk3") (y #t)))

(define-public crate-js_typify_gostruct-2.1.1 (c (n "js_typify_gostruct") (v "2.1.1") (h "05k3zlwcwjfqa2pnz21vnbm5qlq3jppc18lnqky0a8iaqxpwgghw") (y #t)))

