(define-module (crates-io js he jshell) #:use-module (crates-io))

(define-public crate-jshell-0.1.0 (c (n "jshell") (v "0.1.0") (h "1g00dafr2xdl3396mph0dsdr9zsw9adjxxzp6ami3rz9wznykj22")))

(define-public crate-jshell-0.1.1 (c (n "jshell") (v "0.1.1") (h "1fyln1vh5mdr9zh18v8vwqnlbnxvx40qn6pbavashmwb4kcxcl91")))

