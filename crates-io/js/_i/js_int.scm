(define-module (crates-io js _i js_int) #:use-module (crates-io))

(define-public crate-js_int-0.1.0 (c (n "js_int") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0zrxy7sf6m6gb4jr9m0bgdx6zjl0ggcf6wal7q2r87c52mggczy2") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.1 (c (n "js_int") (v "0.1.1") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1l1jjjzhyf5z34rkial3jhif33d9lffj64llfvf3yjr7s35c3p8l") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.2 (c (n "js_int") (v "0.1.2") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1z7w1sz467yycj24ccaxzb2c89y2rn1ig5iwid6560wlyrf68a3s") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.3 (c (n "js_int") (v "0.1.3") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "09dcb6yv3j1cw8vv9g3cld7p9jgjzbip3ml9jvfw0y1dcyvdhds9") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.4 (c (n "js_int") (v "0.1.4") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0brsf20c70a0mmp4lllkdxfh944l943dhjjh02gkdw1v7z8w0zgr") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.5 (c (n "js_int") (v "0.1.5") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0cfzrcc6psqap25ah5ha0dmacmpskgykbq33b1ksv257f2rppavp") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.6 (c (n "js_int") (v "0.1.6") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "09f3833nha0p0fcy3p7qg8pfzrj13i2kmkc1xqkqk8v9nbqhh40d") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.7 (c (n "js_int") (v "0.1.7") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0g2s2p0a9d819yxmxn1j04lz9c9f51zgscz85c767lawi90iq021") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.8 (c (n "js_int") (v "0.1.8") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0rjgg2nxv8ahb5qj8kbb4rx0xin1svqw54k5glm2y4k40pb66aqv") (f (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1.9 (c (n "js_int") (v "0.1.9") (d (list (d (n "rocket_04") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1wljgn25m945bjimgqz0nx2lrc2lvrlll92zk3fdd89m6bsrfrxr") (f (quote (("std") ("lax_deserialize" "serde") ("default" "std"))))))

(define-public crate-js_int-0.2.0 (c (n "js_int") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1k1ar0wkb88nvm8cbzjxj1p8yjqp7axjaqpk70dphsx9g3h8kbpw") (f (quote (("std") ("lax_deserialize" "serde") ("default" "std"))))))

(define-public crate-js_int-0.2.1 (c (n "js_int") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1zbly9dfmlja7v5x8vqjxbipanwf6qzb4dip9d5qlmhrrndvmyny") (f (quote (("std") ("lax_deserialize" "serde") ("default" "std"))))))

(define-public crate-js_int-0.2.2 (c (n "js_int") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "02hn954yv5wksn26ck6lq19y3a0sswapf82hi65www5jf1agjdyr") (f (quote (("std") ("lax_deserialize" "float_deserialize") ("float_deserialize" "serde") ("default" "std"))))))

