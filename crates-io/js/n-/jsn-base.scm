(define-module (crates-io js n- jsn-base) #:use-module (crates-io))

(define-public crate-jsn-base-0.1.0 (c (n "jsn-base") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0p7y2bqk40ah7qi9s0jqg59s4h0fcfb5zs21wv4vwcjj3k5gvnp7")))

