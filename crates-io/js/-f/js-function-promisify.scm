(define-module (crates-io js -f js-function-promisify) #:use-module (crates-io))

(define-public crate-js-function-promisify-0.2.0 (c (n "js-function-promisify") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.23") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.0") (f (quote ("Window" "IdbFactory" "IdbRequest" "IdbOpenDbRequest"))) (d #t) (k 2)))) (h "06khqk6hqfwnvchlhg58v4bdqp2df1i5rkscrl48zry2y2xcmc34")))

(define-public crate-js-function-promisify-0.2.1 (c (n "js-function-promisify") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.23") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.0") (f (quote ("Window" "IdbFactory" "IdbRequest" "IdbOpenDbRequest"))) (d #t) (k 2)))) (h "1v0by7qzi97dzl8vzsm4na39qrnk6wnasifahnw0i68y6713ilrj")))

