(define-module (crates-io js ex jsexec) #:use-module (crates-io))

(define-public crate-jsexec-0.1.0 (c (n "jsexec") (v "0.1.0") (d (list (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 0)) (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.1") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.1") (d #t) (k 0)))) (h "09m8abf2p4rb7md9nl53pcjvaz1g0bqphmwkg0437hwgrskna8l9")))

(define-public crate-jsexec-0.1.1 (c (n "jsexec") (v "0.1.1") (d (list (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 0)) (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.1") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.1") (d #t) (k 0)))) (h "0qrvmlyca62i9vvwcpqq7y6q5w7b1g6md4fn95rbnbaqqya69r53")))

(define-public crate-jsexec-0.1.2 (c (n "jsexec") (v "0.1.2") (d (list (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 0)) (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.1") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.1") (d #t) (k 0)))) (h "1v08n03yx2rr43bakhwsj2fl7hd1kaq8fl58ivyihbckih899qbq")))

(define-public crate-jsexec-0.1.3 (c (n "jsexec") (v "0.1.3") (d (list (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 0)) (d (n "wasmer") (r "^3.0.1") (f (quote ("cranelift"))) (d #t) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.1") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.1") (d #t) (k 0)))) (h "037h4vwzv9ms52w5cdbnf0m006nry03icjyb91di1w3kxv1czd38")))

(define-public crate-jsexec-0.1.4 (c (n "jsexec") (v "0.1.4") (d (list (d (n "wasmer") (r "^3.0.2") (f (quote ("sys-default"))) (d #t) (k 0)) (d (n "wasmer") (r "^3.0.2") (f (quote ("sys-default"))) (d #t) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.2") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.2") (d #t) (k 0)))) (h "17vr44nh8xqyxivih26mwjsnqvvk6sincb8n6nfyhj4hm4krx6w1")))

(define-public crate-jsexec-0.1.5 (c (n "jsexec") (v "0.1.5") (d (list (d (n "wasmer") (r "^3.0.2") (f (quote ("sys" "wat" "cranelift"))) (k 0)) (d (n "wasmer") (r "^3.0.2") (f (quote ("sys" "wat" "cranelift"))) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.2") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.2") (d #t) (k 0)))) (h "0bp4lqrv2lr8zai8qj8qa7mmwi8nbswyjn1xl2k8172rkifbzbr3")))

(define-public crate-jsexec-0.1.6 (c (n "jsexec") (v "0.1.6") (d (list (d (n "wasmer") (r "^3.0.2") (d #t) (k 0)) (d (n "wasmer") (r "^3.0.2") (d #t) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.2") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.2") (d #t) (k 0)))) (h "0p89clh6vgxvab5g9hghppxa0s7mnk1nnd6k53007s6x77fgynhh")))

(define-public crate-jsexec-0.1.7 (c (n "jsexec") (v "0.1.7") (d (list (d (n "wasmer") (r "=3.0.2") (d #t) (k 0)) (d (n "wasmer") (r "=3.0.2") (d #t) (k 1)) (d (n "wasmer-compiler-cranelift") (r "^3.0.2") (d #t) (k 1)) (d (n "wasmer-wasi") (r "^3.0.2") (d #t) (k 0)))) (h "0v14bni1p3kxklq3sqp2cnp5im5lz2ib5araz7yrqq5cllw4x039")))

