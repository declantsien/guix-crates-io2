(define-module (crates-io js et jset) #:use-module (crates-io))

(define-public crate-jset-0.1.0 (c (n "jset") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1vzxvn6krmzigmykmx8fjrx7phk79vyjzs955p99wwrmp4kf87px")))

