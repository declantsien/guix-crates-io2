(define-module (crates-io js -s js-source-mapper) #:use-module (crates-io))

(define-public crate-js-source-mapper-0.1.0 (c (n "js-source-mapper") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0yg4rh7a7pcci4qkf665csri05irgg88ss53pszhv738kq8cqzs6")))

(define-public crate-js-source-mapper-0.1.1 (c (n "js-source-mapper") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02j859pa2zaik29id1dzn2nx9r7gs9f5qs8asqxypn4jj5fi0jry")))

(define-public crate-js-source-mapper-0.2.0 (c (n "js-source-mapper") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07qkr126dnwgs558pa7a5gy1qsjdfdsai96s54ql5xd3grdlh0mz")))

