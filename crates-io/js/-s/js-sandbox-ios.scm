(define-module (crates-io js -s js-sandbox-ios) #:use-module (crates-io))

(define-public crate-js-sandbox-ios-0.1.0 (c (n "js-sandbox-ios") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.238.0") (d #t) (k 0)) (d (n "js-sandbox-macros") (r "=0.2.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1b7fy7i9s2hw5jmyrvkwv4vhw4d37y9mbzl9g3phmmfn0gvx0gbf")))

