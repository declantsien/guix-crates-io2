(define-module (crates-io js -s js-sandbox-macros) #:use-module (crates-io))

(define-public crate-js-sandbox-macros-0.2.0-rc.0 (c (n "js-sandbox-macros") (v "0.2.0-rc.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vssfb41wcpq4442j9pafhak75bkzz2zb5n055w5lq9wqs6wg3ia")))

(define-public crate-js-sandbox-macros-0.2.0-rc.1 (c (n "js-sandbox-macros") (v "0.2.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ck8xdkif116ry773flqjap22rbr7kc7csc504mlrbcv0rs58i70")))

(define-public crate-js-sandbox-macros-0.2.0-rc.2 (c (n "js-sandbox-macros") (v "0.2.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (f (quote ("full"))) (d #t) (k 0)))) (h "1clz7dn2x5n1d4inf8mimwg0v0g5cni0qd2l30jggc9svx7h5jlf")))

