(define-module (crates-io js -s js-sandbox) #:use-module (crates-io))

(define-public crate-js-sandbox-0.1.0 (c (n "js-sandbox") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.56.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0j44bmz76wh70ngxsd4dl8cxp86p3y5d4wln543n3779vqk15b9p")))

(define-public crate-js-sandbox-0.1.1 (c (n "js-sandbox") (v "0.1.1") (d (list (d (n "deno_core") (r "^0.57.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1lsh5z97cafx8qdifziz1zcxjkc3m0l6bax1m83m5y73q47s6qkd")))

(define-public crate-js-sandbox-0.1.2 (c (n "js-sandbox") (v "0.1.2") (d (list (d (n "deno_core") (r "^0.61.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1qsdkzfm5v79acb4plqrrg9v6zi4c8179r9sd8aw49rrn5b16v48")))

(define-public crate-js-sandbox-0.1.3 (c (n "js-sandbox") (v "0.1.3") (d (list (d (n "deno_core") (r "^0.62.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0rlan38kk1lr983izbiik2m4dz5d71xlh9hf2jrxw9dpcj2sska8")))

(define-public crate-js-sandbox-0.1.4 (c (n "js-sandbox") (v "0.1.4") (d (list (d (n "deno_core") (r "^0.64.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0q381gm7hrmj1kz0igx6iipb16cp4m30iw1q933dz6gkydwp2d1s")))

(define-public crate-js-sandbox-0.1.5 (c (n "js-sandbox") (v "0.1.5") (d (list (d (n "deno_core") (r "^0.75.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "00wrykmr5pi98p8wv1ddqsxz50ks8z7y9mngj8lb40ahclpdr0nj")))

(define-public crate-js-sandbox-0.1.6 (c (n "js-sandbox") (v "0.1.6") (d (list (d (n "deno_core") (r "^0.84.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0a2i0x3pv3vmm95f2xrccjxzfr9i975ahdbhhwhivxgifaqjmhjl")))

(define-public crate-js-sandbox-0.2.0-rc.0 (c (n "js-sandbox") (v "0.2.0-rc.0") (d (list (d (n "deno_core") (r "^0.114") (d #t) (k 0)) (d (n "js-sandbox-macros") (r "=0.2.0-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kplzfxfx9dzb5m2jnaifndc0y9xm37klwsfn5ii05cnlwkk4w13")))

(define-public crate-js-sandbox-0.2.0-rc.1 (c (n "js-sandbox") (v "0.2.0-rc.1") (d (list (d (n "deno_core") (r "^0.178") (d #t) (k 0)) (d (n "js-sandbox-macros") (r "=0.2.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14vc15ga64kldyzwid7pnasx0qfkb5g35j7jvzxwp5kd966xcc89")))

(define-public crate-js-sandbox-0.2.0-rc.2 (c (n "js-sandbox") (v "0.2.0-rc.2") (d (list (d (n "deno_core") (r "^0.209.0") (d #t) (k 0)) (d (n "js-sandbox-macros") (r "=0.2.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "1yspf0q3n1wbxzh1lhgb3cl0jg0y8a2fw60b8nnaghjd8wlchxfj")))

