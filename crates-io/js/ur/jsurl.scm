(define-module (crates-io js ur jsurl) #:use-module (crates-io))

(define-public crate-jsurl-0.1.0 (c (n "jsurl") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("std" "preserve_order"))) (d #t) (k 0)))) (h "19gfm7anqc3imaf4flwqqk02r45hc9ifbsc9h2syix5qpgb3phph")))

