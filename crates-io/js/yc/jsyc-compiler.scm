(define-module (crates-io js yc jsyc-compiler) #:use-module (crates-io))

(define-public crate-jsyc-compiler-0.1.0 (c (n "jsyc-compiler") (v "0.1.0") (d (list (d (n "base64") (r "~0.10") (d #t) (k 0)) (d (n "resast") (r "~0.2.1") (d #t) (k 0)) (d (n "ressa") (r "~0.5.2") (d #t) (k 0)))) (h "1vhi8avqf5ja3r6ny9hjzzc6n7irs374nl17559c6kw8cql3jcrp")))

