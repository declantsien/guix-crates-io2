(define-module (crates-io js _p js_parser) #:use-module (crates-io))

(define-public crate-js_parser-0.1.0 (c (n "js_parser") (v "0.1.0") (d (list (d (n "aleph-syntax-tree") (r "^0.1.0") (d #t) (k 0)) (d (n "rslint_parser") (r "^0.3.1") (d #t) (k 0)))) (h "0c36w4mcqsghmjvzy5knch6ixsrmdp4n1f5ac61db8sz9p14jdjd")))

