(define-module (crates-io js _l js_lib) #:use-module (crates-io))

(define-public crate-js_lib-0.0.1 (c (n "js_lib") (v "0.0.1") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0r8faa4qqqygkjm2x23j2g1fih18y5ggmi439v4pzkml0kbxqff8")))

