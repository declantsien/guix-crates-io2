(define-module (crates-io js -j js-json-query) #:use-module (crates-io))

(define-public crate-js-json-query-0.3.0 (c (n "js-json-query") (v "0.3.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "v8") (r "^0.39.0") (d #t) (k 0)))) (h "0sg7a5k35x17ljym05fp300h3kmpw4hvgk9yddvxqa987w8l6sk2")))

(define-public crate-js-json-query-0.3.1 (c (n "js-json-query") (v "0.3.1") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "v8") (r "^0.39.0") (d #t) (k 0)))) (h "1g8ngsl9svqwaj3bqali8ss96rhj7kpvjzgpwfxpvy8479dy8wh8")))

(define-public crate-js-json-query-0.4.0 (c (n "js-json-query") (v "0.4.0") (d (list (d (n "argh") (r "^0.1.7") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "v8") (r "^0.39.0") (d #t) (k 0)))) (h "1jn4804ami5va96jy1kahd1nwfrdhnn3yglqf1rnw57hfhxcd2vy")))

