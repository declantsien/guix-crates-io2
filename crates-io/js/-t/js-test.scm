(define-module (crates-io js -t js-test) #:use-module (crates-io))

(define-public crate-js-test-1.0.0-alpha.1 (c (n "js-test") (v "1.0.0-alpha.1") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0gy6n17d5xsc7arancg10c7mlsy3xp8l8bjcfy8g8xm3y22l74qx")))

(define-public crate-js-test-1.0.0-alpha.2 (c (n "js-test") (v "1.0.0-alpha.2") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1jfad3jh38pblh396agq0gr292svy0xxmnyph6w799x1vn43gllh")))

(define-public crate-js-test-1.0.0-alpha.3 (c (n "js-test") (v "1.0.0-alpha.3") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0b98n5if0psrasqpd0xzj1bq8ab5l77c8yjnwa96i47pza8bh14b")))

(define-public crate-js-test-1.0.0-alpha.4 (c (n "js-test") (v "1.0.0-alpha.4") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1vz6q1g1ihq3cpbc442107azyx29fcz6yivndp2z4aw025n25673")))

(define-public crate-js-test-1.0.0-alpha.5 (c (n "js-test") (v "1.0.0-alpha.5") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1v6ydn6xb4f41xaq3911lqrv2a9y1ygv6xbh9plrfj37idbj700d")))

(define-public crate-js-test-1.0.0-alpha.6 (c (n "js-test") (v "1.0.0-alpha.6") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "12mdmai43hm7qcd8kpqd1a1l21rn1qi0sdz6ripi8mvz9g373mdg")))

(define-public crate-js-test-1.0.0-alpha.7 (c (n "js-test") (v "1.0.0-alpha.7") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "15axjjp82j4xch6j8qgq1k0hj4qs5zzi4k5anhb8zc0ai2r0js3m")))

(define-public crate-js-test-1.0.0-alpha.8 (c (n "js-test") (v "1.0.0-alpha.8") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1lmdydw0ij97f8ynyywxssg37gvg9mlhkzmi4rj50nqa33sr09a1")))

(define-public crate-js-test-1.0.0-alpha.9 (c (n "js-test") (v "1.0.0-alpha.9") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1si7wy787vqbfxh29k1wbaa0fdzzjmqfris7as836lgm2pain8y2")))

(define-public crate-js-test-1.0.0-alpha.10 (c (n "js-test") (v "1.0.0-alpha.10") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1baz3xjx3rjsfkgap66ba76m8p4q8ki83zzs9y6bq9xlzpf6mrfz")))

(define-public crate-js-test-1.0.0-alpha.11 (c (n "js-test") (v "1.0.0-alpha.11") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "03kc83268j77a00k0j9cc8wwb2553mvw2ql2mnf1g8frrvrqcivf")))

(define-public crate-js-test-1.0.0-alpha.12 (c (n "js-test") (v "1.0.0-alpha.12") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1qd2q6zll9yac3prm2yg9i3vgdmw6l81gx9qc90lal01j04g8br7")))

(define-public crate-js-test-1.0.0-alpha.13 (c (n "js-test") (v "1.0.0-alpha.13") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0j2wgcgm5yi1r04vg1zmhbr3kk72zwqh3l8di987z3pmilssv54f")))

(define-public crate-js-test-1.0.0-alpha.14 (c (n "js-test") (v "1.0.0-alpha.14") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0mdknjm00n5j27jn73l81kk4fglg6j3hwlhw4z1s48zzsg6fx32m")))

(define-public crate-js-test-1.0.0-alpha.15 (c (n "js-test") (v "1.0.0-alpha.15") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0y0c60z5jb9dv3fm28ljwfmsyf7hpwzinvpj2rbczqf940fbaqyc")))

