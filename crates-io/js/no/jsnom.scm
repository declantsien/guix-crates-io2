(define-module (crates-io js no jsnom) #:use-module (crates-io))

(define-public crate-jsnom-1.0.0 (c (n "jsnom") (v "1.0.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "14nz16q1mf57rri0arwqb7j84m7vcr7cgv4qjgvclnh41vhg95w5")))

(define-public crate-jsnom-1.0.1 (c (n "jsnom") (v "1.0.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1p78li19sb9zqn4p8k7jbi704wg0h8ygak3g5gc08lbais0w15q6")))

