(define-module (crates-io js -b js-bindgen) #:use-module (crates-io))

(define-public crate-js-bindgen-0.0.0 (c (n "js-bindgen") (v "0.0.0") (h "1ni9wkiqnk73gphqqxzpldrbkakpm5w11c6yy1xqa2rrmls05j03")))

(define-public crate-js-bindgen-0.0.1 (c (n "js-bindgen") (v "0.0.1") (h "1w9fjcqilzh5q0q50achz3i4jd338a8nffpn5w19bkbhdlf08b6q")))

(define-public crate-js-bindgen-0.0.2 (c (n "js-bindgen") (v "0.0.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "015rkljhzxlgpy4953wcf4vw98jrv0hpm51929f3xz2w2s0xk0b1")))

(define-public crate-js-bindgen-0.0.3 (c (n "js-bindgen") (v "0.0.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "0jv9fn3nf42gfhq4h3gfas45cax81l9sg5c9mxpzjndaszkznjx9")))

(define-public crate-js-bindgen-0.0.4 (c (n "js-bindgen") (v "0.0.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "13f534bav2wy8rhilz18s9j27ylnh4clclkzp92p3vwkzn8l4fwq")))

(define-public crate-js-bindgen-0.0.5 (c (n "js-bindgen") (v "0.0.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1jqljma3nxl45ppnhl7804k88pmnklaqfdblqgl2z80qpxp70c12")))

(define-public crate-js-bindgen-0.0.6 (c (n "js-bindgen") (v "0.0.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "0bdlr29m34whw817gvabhsmmlkarb8zq6fiyqprv62v114l9xi81")))

