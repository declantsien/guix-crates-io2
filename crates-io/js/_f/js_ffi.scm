(define-module (crates-io js _f js_ffi) #:use-module (crates-io))

(define-public crate-js_ffi-0.0.1 (c (n "js_ffi") (v "0.0.1") (d (list (d (n "cstring") (r "^0.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "woke") (r "^0.0.1") (d #t) (k 0)))) (h "0wkiyz2v8chn1dk3hhfalkgkg0xnqwyr1mkvskddgv0scw8r8csy")))

(define-public crate-js_ffi-0.0.2 (c (n "js_ffi") (v "0.0.2") (d (list (d (n "cstring") (r "^0.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "woke") (r "^0.0.1") (d #t) (k 0)))) (h "1nznx5vxsjvq98f4lmlzcikjqx6h9piwlbs409iwx3x27rvk5xn3")))

(define-public crate-js_ffi-0.0.4 (c (n "js_ffi") (v "0.0.4") (d (list (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "018rdzjdy8k25nxb8kra9c5884ifikqzmin66a1asxgp27msj2dc")))

(define-public crate-js_ffi-0.0.5 (c (n "js_ffi") (v "0.0.5") (d (list (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1bxnj6njp0s5drliya48375p9qsbd646yafx1q575zgafd3xkyrz")))

(define-public crate-js_ffi-0.0.6 (c (n "js_ffi") (v "0.0.6") (d (list (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1d3l632f2s9ddhgapvy4x0b3q47pir3231vp835xipk61qv9lqjb")))

(define-public crate-js_ffi-0.0.7 (c (n "js_ffi") (v "0.0.7") (d (list (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0xpb05bqw8rzhy9hkv1qifjsm9wglw6na5pqdvm7qvkg17nkcbn2")))

(define-public crate-js_ffi-0.0.8 (c (n "js_ffi") (v "0.0.8") (d (list (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1dzssfxv2jk934gp3mx9wf6m79qhnsfycnh2w092gjqfrsi4lfzi")))

(define-public crate-js_ffi-0.0.9 (c (n "js_ffi") (v "0.0.9") (d (list (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "1z4w84wdgrvhqyjdrc44b9yv6nbb1jg82iflvm34q46rf33gazd3")))

(define-public crate-js_ffi-0.0.10 (c (n "js_ffi") (v "0.0.10") (d (list (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "0bsj19442jbm9zsdfq8mwdfh0f4jmna4jbbxlnqhby3ra18yf83a")))

(define-public crate-js_ffi-0.0.11 (c (n "js_ffi") (v "0.0.11") (d (list (d (n "callback") (r "^0.0.2") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "wasm_common") (r "^0.0.1") (d #t) (k 0)) (d (n "woke") (r "^0.0.2") (d #t) (k 0)))) (h "07irsv5mrbz6j7ilzp6y6mki049mq35nv845ffi67mbq74w0csb0")))

(define-public crate-js_ffi-0.0.12 (c (n "js_ffi") (v "0.0.12") (d (list (d (n "callback") (r "^0.0.3") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "wasm_common") (r "^0.0.1") (d #t) (k 0)))) (h "0m16hba1j9qrgkpxh146xgygy2r40wjz35pd0zly81g03bb1z7j7")))

(define-public crate-js_ffi-0.0.13 (c (n "js_ffi") (v "0.0.13") (d (list (d (n "callback") (r "^0.0.5") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "0jmx2plq9w2vb397x47gkmxyvyxcwjq7rgnb9pqq9bil53ar1m85")))

(define-public crate-js_ffi-0.0.14 (c (n "js_ffi") (v "0.0.14") (d (list (d (n "callback") (r "^0.0.6") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "0wm5p2g81d28n3la9bqyc7x7slb9j0r0d290ca7s2qak391a92c7")))

(define-public crate-js_ffi-0.1.0 (c (n "js_ffi") (v "0.1.0") (d (list (d (n "callback") (r "^0.1.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "0cjaq7z7bwawq007cxwz11m1yfdl1dpi1in8jh8npj1ladj1fpk3")))

(define-public crate-js_ffi-0.1.1 (c (n "js_ffi") (v "0.1.1") (d (list (d (n "callback") (r "^0.2.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "0dppmkdzrdcmhf7ml3h48nr3dl7gy5vgqqf31fncrk4zqfd8dhs6")))

(define-public crate-js_ffi-0.1.2 (c (n "js_ffi") (v "0.1.2") (d (list (d (n "callback") (r "^0.2.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "1sk918vw8x4v9dx6sfxvvrd26511xqinail246w0mswxjxqaxl4m")))

(define-public crate-js_ffi-0.1.3 (c (n "js_ffi") (v "0.1.3") (d (list (d (n "callback") (r "^0.2.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0.4") (d #t) (k 0)) (d (n "web_common") (r "^0.0.1") (d #t) (k 0)))) (h "0314517rh7rajy89mava652lk52rwl44zdyabg05lrjwisri11zr")))

(define-public crate-js_ffi-0.2.0 (c (n "js_ffi") (v "0.2.0") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "callback") (r "^0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "web_common") (r "^0.0") (d #t) (k 0)))) (h "1inl49rjl7689yif2fjxgqr0zc76m1j61zamyamzpx6h20ssp9x0")))

(define-public crate-js_ffi-0.2.1 (c (n "js_ffi") (v "0.2.1") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "callback") (r "^0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "web_common") (r "^0.0") (d #t) (k 0)))) (h "0sy0p1dcwigcj1ifb7xaqzqw8drq7jffdiy5c6n174zvqij84q3g")))

(define-public crate-js_ffi-0.2.2 (c (n "js_ffi") (v "0.2.2") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "callback") (r "^0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "web_common") (r "^0.0") (d #t) (k 0)))) (h "0ib335990g1fvancsjqhb8vj24pkcj60ih10qksnnpybash23hk6")))

(define-public crate-js_ffi-0.3.0 (c (n "js_ffi") (v "0.3.0") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "callback") (r "^0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "web_common") (r "^0.0") (d #t) (k 0)))) (h "125qymxl8zx9s92f0wg1zhfylvzf7azj0fnpzdrla7rzgdmdz3vy")))

(define-public crate-js_ffi-0.3.1 (c (n "js_ffi") (v "0.3.1") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "callback") (r "^0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "web_common") (r "^0.0") (d #t) (k 0)))) (h "00w10bbflbmc34craczsln3j1awj8q5jk7sx72xcgxwb2msgsvbj")))

(define-public crate-js_ffi-0.3.2 (c (n "js_ffi") (v "0.3.2") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "callback") (r "^0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "web_common") (r "^0.0") (d #t) (k 0)))) (h "1hh6spv6wk3mp7jm8d30hvl4mmzfmvpm48d0c3q6mr51frmm4kdh")))

(define-public crate-js_ffi-0.5.1 (c (n "js_ffi") (v "0.5.1") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "1hcyyv2vmlq6grvx3hxic5mrzx25rd86w485kzgxwnkks1fynk3l")))

(define-public crate-js_ffi-0.5.2 (c (n "js_ffi") (v "0.5.2") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "06kvpajws0av7hwgwl61pgfdqddw46fz6b4z25b2b8iab4g2x4kd")))

(define-public crate-js_ffi-0.5.3 (c (n "js_ffi") (v "0.5.3") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "1ihwdzy3vj4krr68j8piwpsalbqjdj9ab0qf7csymrbv6fr6qx9z")))

(define-public crate-js_ffi-0.5.4 (c (n "js_ffi") (v "0.5.4") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "03bj3pfmyxslsfzn3aliyz79j00bcpp7jhaj5zyxh112r1krpw5i")))

(define-public crate-js_ffi-0.6.0 (c (n "js_ffi") (v "0.6.0") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0bs50wbkyh9s9xqr5ac2k8cagkscp25ln9mh3f2j09z6ps1nrzm4")))

(define-public crate-js_ffi-0.6.1 (c (n "js_ffi") (v "0.6.1") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "17yrvj85440bwcymiz2d8xpkf2g1l6fxxxlq11aiawd70nw84xnv")))

(define-public crate-js_ffi-0.6.2 (c (n "js_ffi") (v "0.6.2") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0136chb7vd9cki6kbr15b6nqirg2shs5gvybhg8mw814j5a5cazw")))

(define-public crate-js_ffi-0.6.3 (c (n "js_ffi") (v "0.6.3") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "1gf46zdq0ff7mh9368rjbk100a2qpvd2v4k0mvpna5nk3lq3g77c")))

(define-public crate-js_ffi-0.6.4 (c (n "js_ffi") (v "0.6.4") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0zav3x71dimd79khrvk4aqq3nck89a0j7fq5nphhgw9ysdjw6w3j")))

(define-public crate-js_ffi-0.6.5 (c (n "js_ffi") (v "0.6.5") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "19b2105800zbg4kjvrafsq6qkv2vvcfmfs2d73c461kxflgj8rxv")))

(define-public crate-js_ffi-0.6.6 (c (n "js_ffi") (v "0.6.6") (d (list (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "highlight") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "105l2fj24z644ym335fi60w3ak0d2c7br288h0pv5ng8hj8vxgwy")))

(define-public crate-js_ffi-0.6.7 (c (n "js_ffi") (v "0.6.7") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "11nck28gdnscrj8k6zih15xvsrj19cvkjxgkqn4kgncnb7n108m6")))

(define-public crate-js_ffi-0.6.8 (c (n "js_ffi") (v "0.6.8") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0h23b1x9whw7xnvvzcsin8y1lwjlbvhdsv9yd8yjxamqgsblps25")))

(define-public crate-js_ffi-0.6.9 (c (n "js_ffi") (v "0.6.9") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "1kgfzch9vzjm9mlqkkc5qlaz06arkq1kvw4j5iw7nk7plqmfhnnj")))

(define-public crate-js_ffi-0.7.0 (c (n "js_ffi") (v "0.7.0") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)) (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "13m2gclgfbqwsix1png51zd0vqvq3wnwxc5xg6nik9mk0qi4h8v0")))

(define-public crate-js_ffi-0.8.0 (c (n "js_ffi") (v "0.8.0") (d (list (d (n "cstring") (r "^0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "185vp3dqbf661w8gv9aimjrdga8n10gw16vxz6lv9ykz3bizgdia")))

