(define-module (crates-io js eq jseqio) #:use-module (crates-io))

(define-public crate-jseqio-0.1.0 (c (n "jseqio") (v "0.1.0") (d (list (d (n "ex") (r "^0.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "12pjycwhdgm6njjihpla4psb6kcyn0xncnm4g0wxpv0xn9plsm1a")))

(define-public crate-jseqio-0.1.1 (c (n "jseqio") (v "0.1.1") (d (list (d (n "ex") (r "^0.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1f43n96lnvnamxgh6xknj0wlk5g1rd56lp6rg4k2qnd0knlxgx49")))

(define-public crate-jseqio-0.1.2 (c (n "jseqio") (v "0.1.2") (d (list (d (n "ex") (r "^0.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "107447pz6ph9356p5lr5g6bf3210wwhrzcd0r4pmsidag1p7g1gb")))

