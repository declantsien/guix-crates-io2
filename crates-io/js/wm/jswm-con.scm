(define-module (crates-io js wm jswm-con) #:use-module (crates-io))

(define-public crate-jswm-con-0.1.0 (c (n "jswm-con") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "ipcon-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pij7kskm75gqkj18whb9fx725ak40r1c6y06hwkp1lg63jdvz8h") (f (quote (("default"))))))

(define-public crate-jswm-con-0.2.0 (c (n "jswm-con") (v "0.2.0") (d (list (d (n "error-stack") (r "^0.2.4") (d #t) (k 0)) (d (n "ipcon-sys") (r "^0.2") (d #t) (k 0)) (d (n "jlogger") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hck41incjp56pjz5rpbmx9kqc1bich6jlghzkpbk9rv2avl88sn") (f (quote (("default"))))))

