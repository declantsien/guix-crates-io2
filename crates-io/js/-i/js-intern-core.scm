(define-module (crates-io js #{-i}# js-intern-core) #:use-module (crates-io))

(define-public crate-js-intern-core-0.3.0 (c (n "js-intern-core") (v "0.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2.40") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.40") (d #t) (k 2)))) (h "15lpdg9m86fbbgalzj1hpcbwy0iwp7i1kpl9f5xmm8yp9dsm5qbs")))

(define-public crate-js-intern-core-0.3.1 (c (n "js-intern-core") (v "0.3.1") (d (list (d (n "wasm-bindgen") (r "^0.2.40") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.40") (d #t) (k 2)))) (h "1vrmng7zxy0a4az37b4n79f6s73098cdhzz5096ywimcpwfnq9gg")))

