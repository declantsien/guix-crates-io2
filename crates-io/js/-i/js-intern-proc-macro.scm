(define-module (crates-io js #{-i}# js-intern-proc-macro) #:use-module (crates-io))

(define-public crate-js-intern-proc-macro-0.3.0 (c (n "js-intern-proc-macro") (v "0.3.0") (d (list (d (n "js-intern-core") (r "= 0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "052pk1zlf3x6jhfbgpr1ngrn55dwwwniab7k77icf4n13rqac7jb")))

(define-public crate-js-intern-proc-macro-0.3.1 (c (n "js-intern-proc-macro") (v "0.3.1") (d (list (d (n "js-intern-core") (r "= 0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "18qkxmghqb8h5ichxqgrcwv0y0ihvl7w2320wj58548whifyig4f")))

