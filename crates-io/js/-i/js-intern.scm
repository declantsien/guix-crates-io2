(define-module (crates-io js #{-i}# js-intern) #:use-module (crates-io))

(define-public crate-js-intern-0.1.0 (c (n "js-intern") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.40") (d #t) (k 0)))) (h "0rg9a9qvnfxbhpx9gmlh3gnf9wdy717cnp7gy8zqwr8di8mvi7vb")))

(define-public crate-js-intern-0.2.0 (c (n "js-intern") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.40") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.40") (d #t) (k 2)))) (h "1f4pwjyxipqhlymrn8zpsi7i2yv9yx6hahm858d9qyimd3rpqlkc")))

(define-public crate-js-intern-0.2.1 (c (n "js-intern") (v "0.2.1") (d (list (d (n "wasm-bindgen") (r "^0.2.40") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.40") (d #t) (k 2)))) (h "1ybfy5q8w9nd5j7w24nii3zim01gz29nysqpid198gj5fq8iw0q3")))

(define-public crate-js-intern-0.3.1 (c (n "js-intern") (v "0.3.1") (d (list (d (n "js-intern-core") (r "= 0.3.1") (d #t) (k 0)) (d (n "js-intern-proc-macro") (r "= 0.3.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.40") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.40") (d #t) (k 2)))) (h "117xs2wf3fhsv2xkm8mf7xjaa5nl6l88j1z5axc2847c6sq0x6aw")))

