(define-module (crates-io js y_ jsy_mk_194) #:use-module (crates-io))

(define-public crate-jsy_mk_194-1.0.0 (c (n "jsy_mk_194") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0n3h1ym71q84jkihmvqqirfyzcf4qw4k5hi5pxj9dmn00krrm5kw") (r "1.66")))

(define-public crate-jsy_mk_194-1.0.1 (c (n "jsy_mk_194") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1wk0x9r11610815rmsykg9ygrjhlq5qd8f2v2a9nb4236rwr20ax") (r "1.66")))

(define-public crate-jsy_mk_194-1.0.2 (c (n "jsy_mk_194") (v "1.0.2") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0f4w84h7xb3gg41vzsc5qa2pm384wf1f225i3apdmzsc6ibkrbd5") (r "1.66")))

