(define-module (crates-io js xn jsxn) #:use-module (crates-io))

(define-public crate-jsxn-0.1.0 (c (n "jsxn") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "1kzy4s9rrjzq99z3x65r239aq3irsg9xi0gvj9k4zl8434ssjavj")))

(define-public crate-jsxn-0.1.1 (c (n "jsxn") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "1cr80ywykiyqf54clzd8rnlg4gglp89izig6k7ii62bf58c0lwzh")))

