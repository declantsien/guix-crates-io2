(define-module (crates-io js tz jstz_crypto) #:use-module (crates-io))

(define-public crate-jstz_crypto-0.1.0 (c (n "jstz_crypto") (v "0.1.0") (h "0bk3zckdh58fxj9f4s3ipkp4aq1sb5bkdf00c3sgkpnr1194q9m0")))

(define-public crate-jstz_crypto-0.1.1 (c (n "jstz_crypto") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "boa_gc") (r "^0.17.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tezos_crypto_rs") (r "^0.5.2") (k 0)))) (h "04yyxk22adknr6c8xf50liw8w94yr11bldwz9x16m1ba9s2aqsfh") (y #t)))

(define-public crate-jstz_crypto-0.1.0-alpha.0 (c (n "jstz_crypto") (v "0.1.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "boa_gc") (r "^0.17.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tezos_crypto_rs") (r "^0.5.2") (k 0)))) (h "136xcfy40fmn4m3bbd02bsq6fj5lfgz5r7ygvqpdsc8w5jk3bmw7")))

