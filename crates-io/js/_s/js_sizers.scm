(define-module (crates-io js _s js_sizers) #:use-module (crates-io))

(define-public crate-js_sizers-0.1.0 (c (n "js_sizers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "swc") (r "^0.87.2") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.88.1") (f (quote ("codegen" "minifier" "optimization" "parser" "react" "transforms" "typescript" "utils" "visit"))) (d #t) (k 0)))) (h "1mdhi7j7l722ilwvrvbka40cx8j5wkyq569gy8yv2kpb3sf8fla3")))

(define-public crate-js_sizers-0.1.1 (c (n "js_sizers") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "swc") (r "^0.87.2") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.88.1") (f (quote ("minifier"))) (d #t) (k 0)))) (h "1wr05pkfz06krvrmz694a3amzd4cck65xah2zbdvmlrdl7mc7b99")))

(define-public crate-js_sizers-0.1.2 (c (n "js_sizers") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "swc") (r "^0.87.2") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.88.1") (f (quote ("minifier"))) (d #t) (k 0)))) (h "0hbzm01s2wlq8rynsygfm0w65zhn88n5i6wjhnv0przrdjyi9vvh")))

(define-public crate-js_sizers-0.1.3 (c (n "js_sizers") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "swc") (r "^0.87.2") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.88.1") (f (quote ("minifier"))) (d #t) (k 0)))) (h "16f309cmbgxrwbd6y4yylhfr4knlmnhj4nh3y9w4mswm7clj45kn")))

(define-public crate-js_sizers-0.1.4 (c (n "js_sizers") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "swc") (r "^0.87.2") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.88.1") (f (quote ("minifier"))) (d #t) (k 0)))) (h "0cvkacyrkxh0999a0qvgadhwrpiv4h2vzcnc5wxfh101s7qs0wd7")))

(define-public crate-js_sizers-0.1.5 (c (n "js_sizers") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "swc") (r "^0.88.0") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.88.1") (f (quote ("minifier"))) (d #t) (k 0)))) (h "12w90k1pf7gvw31zdyzaxgk0jvxybmyipskf3gps4vb98p63c5mh")))

(define-public crate-js_sizers-0.1.6 (c (n "js_sizers") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "swc") (r "^0.119.9") (d #t) (k 0)) (d (n "swc_ecmascript") (r "^0.111.9") (f (quote ("minifier"))) (d #t) (k 0)))) (h "0alf35qj1fvlydmfyrjpc2n86w073jvdvwn0ga984j413hxha6xn")))

(define-public crate-js_sizers-0.2.0 (c (n "js_sizers") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "swc_core") (r "^0.43.23") (f (quote ("ecma_ast" "ecma_visit" "ecma_loader_node" "ecma_loader_lru" "ecma_utils" "ecma_minifier" "ecma_transforms" "__ecma_transforms" "ecma_transforms_react" "ecma_transforms_typescript" "ecma_parser" "ecma_parser_typescript" "cached" "base"))) (d #t) (k 0)))) (h "01gprh4dn6mkjids5zi9lkxs6h14m389wq1rfvwzkl7va8prirlb")))

