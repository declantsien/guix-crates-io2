(define-module (crates-io js -p js-promises) #:use-module (crates-io))

(define-public crate-js-promises-0.1.0 (c (n "js-promises") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "stdweb") (r "^0.4.19") (d #t) (k 0)))) (h "06wnqxhwg0yw5azf92hbd3hrq8zhsamzvw1a7gxs551kw9rcickw")))

