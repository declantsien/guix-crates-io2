(define-module (crates-io js on json_log) #:use-module (crates-io))

(define-public crate-json_log-0.1.0 (c (n "json_log") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ngg74v66ln742i01b7ln58nxr86nn1dspxzifqz5m3p4yhhv19r") (y #t)))

(define-public crate-json_log-0.1.1 (c (n "json_log") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ashvla58np3j3xacy3ih6sd7zbdfg25k74m5zymps6q0ngk1xh7")))

(define-public crate-json_log-0.2.1 (c (n "json_log") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11jxb9n0wbpdgrlq8qm996wm29ppbc5kp7hlnqq4mdnkv7r9md95")))

