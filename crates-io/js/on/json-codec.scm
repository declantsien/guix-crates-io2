(define-module (crates-io js on json-codec) #:use-module (crates-io))

(define-public crate-json-codec-0.1.0 (c (n "json-codec") (v "0.1.0") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "1ni2wb8kglflqgavim2hf57k3g847mkgchqbqn9n4fbkpm1zgmp4")))

(define-public crate-json-codec-0.1.1 (c (n "json-codec") (v "0.1.1") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "0wjc2dd2xb49wr74280f8c0qlws374j56s4cqxd7zsvqynskiv1x")))

(define-public crate-json-codec-0.2.0 (c (n "json-codec") (v "0.2.0") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "16l0lw12hw9ncnairdz1hc4hsl02mj4ya55qi1i94bmrqqwwwzgs")))

(define-public crate-json-codec-0.2.1 (c (n "json-codec") (v "0.2.1") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "121pxflwvyk8mp2dddcy4b47f7k8vllr54wi253cn2fkbxh2zzrr")))

(define-public crate-json-codec-0.3.0 (c (n "json-codec") (v "0.3.0") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "1q0xj9qlgv2p0cw4pplcdpscsa8s8082rjizh7zs33h93c12la29")))

(define-public crate-json-codec-0.4.0 (c (n "json-codec") (v "0.4.0") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "1fikzljkplw50fs812ql8i3hic2myd4zmxmwgxaj6jxj5dsf579p")))

(define-public crate-json-codec-0.4.1 (c (n "json-codec") (v "0.4.1") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "069458g62v7sk5kxks9abd8lqywcqnpy227pfkhmhj2bvb9rigwm")))

(define-public crate-json-codec-0.4.2 (c (n "json-codec") (v "0.4.2") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "0ns3ixpxkk4ggp6v81dr3qynrf4m25dfbnhwsz04x5zygryl64wc")))

(define-public crate-json-codec-0.5.0 (c (n "json-codec") (v "0.5.0") (d (list (d (n "quickcheck") (r ">= 0.2.21") (d #t) (k 2)))) (h "07q7lnqydspjy6va14kgvfm4jgskawgwdmsqry63y0ir971w2hgh")))

