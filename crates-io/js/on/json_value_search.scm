(define-module (crates-io js on json_value_search) #:use-module (crates-io))

(define-public crate-json_value_search-0.1.0 (c (n "json_value_search") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aw5zvljhzwyffvq08sjb2c1s22y04f470vy63gq16cb5mbr038z")))

(define-public crate-json_value_search-0.1.1 (c (n "json_value_search") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fbw0lni25wjfawmw1b7wqzzyj2nhglpjglg2nw28k1qrzwkbh4v")))

(define-public crate-json_value_search-0.1.2 (c (n "json_value_search") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ywfw68443khdkis2s3q6fihf994iq5jga86180r6a3zkvq1qqwc")))

(define-public crate-json_value_search-0.1.3 (c (n "json_value_search") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vyd7pgm3a9ngrlrhal7bs29ryq8n1wqj7xqypr1kf4938syrlny")))

(define-public crate-json_value_search-1.0.0 (c (n "json_value_search") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bcvxfkdrkmngak7sigg3ykwalpqw8qriwzs5v0d1jh183894sa5")))

(define-public crate-json_value_search-1.0.0-beta.1 (c (n "json_value_search") (v "1.0.0-beta.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jb82gd7hp3nf472vb999ylwr8f2ymr0mxzxx433p4pka2a8fryx")))

(define-public crate-json_value_search-1.0.1-beta.1 (c (n "json_value_search") (v "1.0.1-beta.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a17cfk10a7ynz7kb9ak0cz1xj7j9ajr53bss8kbjawb3ns5nydx")))

(define-public crate-json_value_search-1.0.1 (c (n "json_value_search") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b6jrs72pm60q9hfrsri8fb8nb5vsh1q21mnxldgajs3kvcd113z")))

