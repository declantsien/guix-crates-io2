(define-module (crates-io js on jsonify) #:use-module (crates-io))

(define-public crate-jsonify-0.1.0 (c (n "jsonify") (v "0.1.0") (h "1pvprapa4p5qx2nhzrpsj79pnaf2viyiygjks9haqvs3k69dh7kf") (y #t)))

(define-public crate-jsonify-0.2.0 (c (n "jsonify") (v "0.2.0") (h "0m1r5wyran2ak021xvcnvql14pkf85gg2nxkgwz6l5k3ya8q0xby") (y #t)))

