(define-module (crates-io js on jsonformat-cli) #:use-module (crates-io))

(define-public crate-jsonformat-cli-0.2.0 (c (n "jsonformat-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonformat") (r "^2.0.0") (d #t) (k 0)))) (h "1jycdzn9xa888prsawjhgxa42snkwf1llp2kf770y2ckvj45sjyh")))

