(define-module (crates-io js on jsona-openapi) #:use-module (crates-io))

(define-public crate-jsona-openapi-0.1.0 (c (n "jsona-openapi") (v "0.1.0") (d (list (d (n "jsona") (r "^0.1.4") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hymn4v5i93h5yg3gj67a68n2j6l23ais5bm0934wc7anakhjydw")))

(define-public crate-jsona-openapi-0.1.2 (c (n "jsona-openapi") (v "0.1.2") (d (list (d (n "jsona") (r "^0.1.8") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r9d5g72i74xml1rgmfvgrm20q6gw73vny3qazjx4vs54a61jasi")))

(define-public crate-jsona-openapi-0.1.4 (c (n "jsona-openapi") (v "0.1.4") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mjxwbwkm58n8mpx3vfhqv7aic33qarx2ah478rlrjqjf7spgpcv")))

(define-public crate-jsona-openapi-0.1.6 (c (n "jsona-openapi") (v "0.1.6") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18yzyx4zlp34j3kvvlhr6wi1hxrdiihvc2jpqnv7dpldnmyng4m9")))

(define-public crate-jsona-openapi-0.1.8 (c (n "jsona-openapi") (v "0.1.8") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0a15qnjg2jafj2j7csxzm1hsi87jgq0dx1gcablsb0qfwldfsvi3")))

(define-public crate-jsona-openapi-0.1.10 (c (n "jsona-openapi") (v "0.1.10") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02067zcxlrkzsfiqd1898d42lz2cwcij5p3zgfcl25sajqcv7z6w")))

(define-public crate-jsona-openapi-0.1.12 (c (n "jsona-openapi") (v "0.1.12") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b2q9hlcavgi624axm334c0vc270nliprrzp02q4g6lh9fg5jkc4")))

(define-public crate-jsona-openapi-0.1.14 (c (n "jsona-openapi") (v "0.1.14") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b9974msvmh9c5ir1zmgvdzs2fd7axf0dvacc53gx4krh1j6fnly")))

(define-public crate-jsona-openapi-0.1.16 (c (n "jsona-openapi") (v "0.1.16") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "177g2cmfjw90whhxcbxs9hkdfhq5n7mkjj7ahj05jngqiy2plbyv")))

(define-public crate-jsona-openapi-0.1.17 (c (n "jsona-openapi") (v "0.1.17") (d (list (d (n "jsona") (r "^0.1.10") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19ixa33rq0089lhswa5y23pryjc1yajrxf06ra94ndis8a0flq68")))

(define-public crate-jsona-openapi-0.2.0 (c (n "jsona-openapi") (v "0.2.0") (d (list (d (n "jsona") (r "^0.2.0") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g1ikzh1gdx78v6cajzp9a6y707i4sxzkv58f0m2p76cb2n53hvi")))

(define-public crate-jsona-openapi-0.2.1 (c (n "jsona-openapi") (v "0.2.1") (d (list (d (n "jsona") (r "^0.2.0") (d #t) (k 0)) (d (n "jsona_openapi_spec") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0959z2fs6nspc5czydrvbj346salazm90bnl8dak1w61nz64fqig")))

(define-public crate-jsona-openapi-0.3.0 (c (n "jsona-openapi") (v "0.3.0") (d (list (d (n "indexmap") (r "~1.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "insta") (r "^1.15") (d #t) (k 2)) (d (n "jsona") (r "^0.6") (d #t) (k 0)) (d (n "jsona-schema") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1cnjlilwmi49li1r3mpq3h0bxwyg160n38h3n5vjh8wp91vlk4n2")))

