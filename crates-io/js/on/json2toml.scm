(define-module (crates-io js on json2toml) #:use-module (crates-io))

(define-public crate-json2toml-0.1.1 (c (n "json2toml") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "0dsrmmn95rxjwxvzly56mc8ykbdhr2xhd9mbnq22l60xvvlwa6fk") (r "1.66")))

