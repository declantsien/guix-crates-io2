(define-module (crates-io js on json-resp-derive) #:use-module (crates-io))

(define-public crate-json-resp-derive-0.1.0 (c (n "json-resp-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "05nhy7bikns6rdamm2zikilnazrs7rb24wfy7pqkvjv56r75dbxd") (f (quote (("openapi") ("log"))))))

(define-public crate-json-resp-derive-0.1.1 (c (n "json-resp-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "151hkhndbiikzhch4w65xyhp97y27bfq9w7dc1zmv1jj62n5m4zp") (f (quote (("openapi") ("log"))))))

