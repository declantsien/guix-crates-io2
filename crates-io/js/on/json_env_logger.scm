(define-module (crates-io js on json_env_logger) #:use-module (crates-io))

(define-public crate-json_env_logger-0.1.0 (c (n "json_env_logger") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (k 0)) (d (n "kv-log-macro") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q8si2ja4zcnq1k6bxzbcw7995qc91gly0q570x3q2wjnjqkxf2h") (f (quote (("iso-timestamps" "chrono") ("default"))))))

(define-public crate-json_env_logger-0.1.1 (c (n "json_env_logger") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (k 0)) (d (n "kv-log-macro") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qicnvn2m66dl6xa5cvnxw37slkm7vqskd58sf3v2j04x90cabjf") (f (quote (("iso-timestamps" "chrono") ("default"))))))

