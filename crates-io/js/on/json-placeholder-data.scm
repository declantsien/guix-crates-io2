(define-module (crates-io js on json-placeholder-data) #:use-module (crates-io))

(define-public crate-json-placeholder-data-0.2.0 (c (n "json-placeholder-data") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (d #t) (k 0)))) (h "004m4xywrjmwnvk3m8grda0i2s0h4cs44f9hdq44csrqd4rgcif7") (r "1.64.0")))

(define-public crate-json-placeholder-data-0.2.1 (c (n "json-placeholder-data") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (d #t) (k 0)))) (h "0zm7hcpmwwcrfykkzy87xsh82b2107bw0jghv3q9f8n6b4fj4d37") (r "1.64.0")))

