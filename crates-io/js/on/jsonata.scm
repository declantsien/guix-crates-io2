(define-module (crates-io js on jsonata) #:use-module (crates-io))

(define-public crate-jsonata-0.0.0 (c (n "jsonata") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bumpalo") (r "^3.9.1") (f (quote ("collections" "boxed"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dtoa") (r "^1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (f (quote ("bumpalo"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "test-case") (r "^1.2.0") (d #t) (k 2)) (d (n "test-generator") (r "^0.3") (d #t) (k 2)))) (h "1a0r82f1xy4knhc207yyd8b2i7v78yxaavbgw4zzxzkiwk5gi2im")))

