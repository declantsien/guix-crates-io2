(define-module (crates-io js on json-rs) #:use-module (crates-io))

(define-public crate-json-rs-0.1.0 (c (n "json-rs") (v "0.1.0") (h "0lis4p3vzv7myr0cpr7va9czk823j86mbcmn50nqmz7gmi321ab1")))

(define-public crate-json-rs-0.1.1 (c (n "json-rs") (v "0.1.1") (h "0icfqzz817q4dbrkggz2yi8nri6rlybni3grrnp4qsc4iv049ixf")))

