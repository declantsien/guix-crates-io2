(define-module (crates-io js on jsonc_tools) #:use-module (crates-io))

(define-public crate-jsonc_tools-0.0.1 (c (n "jsonc_tools") (v "0.0.1") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "10arjbx0vs9xw2l0vgaazvggvxvbycg7yn02p5rhb11l23q1hmvx")))

