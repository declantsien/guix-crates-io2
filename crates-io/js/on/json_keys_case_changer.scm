(define-module (crates-io js on json_keys_case_changer) #:use-module (crates-io))

(define-public crate-json_keys_case_changer-0.1.0 (c (n "json_keys_case_changer") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1mpis74km7s4hvq1mlqpkvmwgw2khi19h5nzfxyszl68npqmylmw")))

(define-public crate-json_keys_case_changer-0.1.1 (c (n "json_keys_case_changer") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "15kgc20mf0f30bv57g9xwwkaricf35bc8yag5apv7a9skfy70d90")))

(define-public crate-json_keys_case_changer-0.1.2 (c (n "json_keys_case_changer") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "04wqylqccqy8qczijcdwi9s6ra4mk4afs7hwdmhrczy6d4pnav3i")))

(define-public crate-json_keys_case_changer-0.2.0 (c (n "json_keys_case_changer") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0rcbjzgd8k8kwn5d7npp4s1qiwmg4zklpxyih5s4v38xbvdy372y")))

