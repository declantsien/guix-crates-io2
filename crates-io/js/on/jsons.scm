(define-module (crates-io js on jsons) #:use-module (crates-io))

(define-public crate-jsons-0.1.0 (c (n "jsons") (v "0.1.0") (h "0l9ijj8jcr6my274gdyljriygxw2lsqpzda19cpxw7l7sc2ph7v8")))

(define-public crate-jsons-0.1.1 (c (n "jsons") (v "0.1.1") (h "1am65h77k1q1j9gs7gd5vnqk8qpjc20c641bnmqz2d2wrcb8krnf")))

(define-public crate-jsons-0.1.2 (c (n "jsons") (v "0.1.2") (h "0alb2bz61mn8vn1bynqbblfiazw8021kzq83frj9myc3kvhc8df1")))

(define-public crate-jsons-0.1.3 (c (n "jsons") (v "0.1.3") (h "1rfl22lrrylils90c8j9kg514ax1gs5dkd5l4h81qyil10c55aa1")))

(define-public crate-jsons-0.1.4 (c (n "jsons") (v "0.1.4") (h "1lphnvmbgiff0jfx3zv1j65s1d6j0xwl3jwyy2yv4kwfjqbhjkfj")))

(define-public crate-jsons-0.1.5 (c (n "jsons") (v "0.1.5") (h "1qdkngi7gi9kh7l8dr4phjia74x6ll6ydvzl2b3imqrzz4w1bh4g")))

