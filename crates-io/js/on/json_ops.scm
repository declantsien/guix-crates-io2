(define-module (crates-io js on json_ops) #:use-module (crates-io))

(define-public crate-json_ops-0.1.0 (c (n "json_ops") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (o #t) (d #t) (k 0)))) (h "0wgiyqcyn81wqdp84aylpqcfxnc6qj5zpwng7px5g6is0vwmqs7g")))

