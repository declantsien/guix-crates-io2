(define-module (crates-io js on json_schema) #:use-module (crates-io))

(define-public crate-json_schema-1.6.10 (c (n "json_schema") (v "1.6.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06hwndn34pwzzjiibxwaixyf690x30zjgn2s9l3hvd3cxvqk8p7p")))

(define-public crate-json_schema-1.6.16 (c (n "json_schema") (v "1.6.16") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "164yjbz4gq4dxqpkmd9zanl3h9wysk12df6bf6jcwjfs68mkhnj0")))

(define-public crate-json_schema-1.6.17 (c (n "json_schema") (v "1.6.17") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0crvzf8wakavss5nk17x39y4kjx2nay6c3iia7qn13bwy9mfn5z5")))

(define-public crate-json_schema-1.6.18 (c (n "json_schema") (v "1.6.18") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kxv01p50bj6kmhz1x8785ffyn9p400ab7j2wzy9nsx88415vrvi")))

(define-public crate-json_schema-1.6.19 (c (n "json_schema") (v "1.6.19") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ycwlkiw1338qb0wr2ch1an9fv9ms3j76pincncm1rlrjb7rfvs5")))

(define-public crate-json_schema-1.7.0 (c (n "json_schema") (v "1.7.0") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1swb62rlb19cy4nmhgqv2c810md47znzfmzpwfddbzjvayzzjfdi")))

(define-public crate-json_schema-1.7.4 (c (n "json_schema") (v "1.7.4") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jmvsv0h8vfxw467iqrvm801gzdyckr5iws8g2pgl8lximsg30ni")))

(define-public crate-json_schema-1.7.5 (c (n "json_schema") (v "1.7.5") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07m334f4ckkind8kzz478bhcsn1x89mym8djj9cjp7aa7jsgmfdq")))

