(define-module (crates-io js on json_macros) #:use-module (crates-io))

(define-public crate-json_macros-0.0.1 (c (n "json_macros") (v "0.0.1") (h "1j5wd525x5p5mjp9l02djyqdjhvb2sikh2xz7281mi17cdwas0mw")))

(define-public crate-json_macros-0.0.2 (c (n "json_macros") (v "0.0.2") (h "1xx9vsiwxqf6rfiki1ydflbs6hbg227ix2w54ryc05a86a0a9an1")))

(define-public crate-json_macros-0.0.2-pre (c (n "json_macros") (v "0.0.2-pre") (h "0311brkdpsvnc38x4mr09vlgy5821z84nqqvam8k72zvkhbzpvbc") (y #t)))

(define-public crate-json_macros-0.0.3 (c (n "json_macros") (v "0.0.3") (h "1l7skc95jklariclwznx1i104ldcw70ys9d5ngjl54b4y3khnwd1")))

(define-public crate-json_macros-0.0.4 (c (n "json_macros") (v "0.0.4") (h "1w3pphr24q1gzjzn79djfhb38lvw6n70aldwc89f21y6mn3nrv3n")))

(define-public crate-json_macros-0.0.5 (c (n "json_macros") (v "0.0.5") (h "1r4qx78d96mdz7gg27z453lw12ia93msgw7aj1syczcsf8ij7ncf")))

(define-public crate-json_macros-0.0.6 (c (n "json_macros") (v "0.0.6") (h "1bgr2034zv6kf0573v1g63gba1lhysdzvzqnnaj9dfgi76s3npmi")))

(define-public crate-json_macros-0.1.0 (c (n "json_macros") (v "0.1.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1ykh8ihrb2lpwlrzwf38njpv9mw4mm7cl71nwbzxdmsgfhr3bqn6") (y #t)))

(define-public crate-json_macros-0.1.1 (c (n "json_macros") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1k6pa96jcpnmmwrliz0iv5wkzk386h578b59gkcf8nkgkbrjkr95")))

(define-public crate-json_macros-0.1.2 (c (n "json_macros") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1rnjxc4cp4aa6kmkh756z9kfdznw4zppr7a0pkaqawfcdb1hm4fk")))

(define-public crate-json_macros-0.1.3 (c (n "json_macros") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "10il7pisbj1b7f1hp93jc5gfmx4b83ad3vwrh9wpmplnd8jawjlq")))

(define-public crate-json_macros-0.1.4 (c (n "json_macros") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0197znhbw32wjyagjmhaj17znd9gkv37vqsdn8akda6hdwnpj75m")))

(define-public crate-json_macros-0.2.0 (c (n "json_macros") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0dksqjqwqj96b0878jkgw7xmr3wmfj5pjjnhbq8f5ym0p6418f81")))

(define-public crate-json_macros-0.2.1 (c (n "json_macros") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0qhf8x01s2i1azvhgv6a2pcgd3fkhbs19nbdv636ww59384lpfq7")))

(define-public crate-json_macros-0.2.2 (c (n "json_macros") (v "0.2.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0ji0ii29c2x0dw848m6vkjvdlagzpipzphqvgx9hh1pmmwv70xsw")))

(define-public crate-json_macros-0.2.3 (c (n "json_macros") (v "0.2.3") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0igqyix1402wg11zvs3mfqrw93r8i3xwmf8qx9v69a8q31bkfald")))

(define-public crate-json_macros-0.2.4 (c (n "json_macros") (v "0.2.4") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "148wrxxcmrcmm112hb468jmicgmr0psq3mka1nxp9lj5iwcazpjf")))

(define-public crate-json_macros-0.2.5 (c (n "json_macros") (v "0.2.5") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0rllns52kaiikb9vxww99nrlldnfqna3zkzfywx8s7vhabjcpq39")))

(define-public crate-json_macros-0.2.6 (c (n "json_macros") (v "0.2.6") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "02jdkxagfck14chnp1zhgikr5cyc1yc3i0vafp4h9cv5pxk4kzg5")))

(define-public crate-json_macros-0.3.0 (c (n "json_macros") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 2)) (d (n "serde_json") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0gp5y7l739l4vi3s2m5vzqfskfx3dra9ba29gvhn5vza0k9rx5hq") (f (quote (("with-serde" "serde_json") ("with-rustc-serialize" "rustc-serialize") ("default" "with-rustc-serialize"))))))

(define-public crate-json_macros-0.3.1 (c (n "json_macros") (v "0.3.1") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 2)) (d (n "serde_json") (r "^0.6") (o #t) (d #t) (k 0)))) (h "16pa7ps20z294mwly470k867s6v4x62scvashr48grinyjawrmb6") (f (quote (("with-serde" "serde_json") ("with-rustc-serialize" "rustc-serialize") ("default" "with-rustc-serialize"))))))

(define-public crate-json_macros-0.3.2 (c (n "json_macros") (v "0.3.2") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 2)) (d (n "serde_json") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0jz3dak8p5gvsslfncwyfg6lwazbny8nhzy1xghf9asx548cvdl3") (f (quote (("with-serde" "serde_json") ("with-rustc-serialize" "rustc-serialize") ("default" "with-rustc-serialize"))))))

