(define-module (crates-io js on json-pointer) #:use-module (crates-io))

(define-public crate-json-pointer-0.1.0 (c (n "json-pointer") (v "0.1.0") (d (list (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0c7zxaxzyczc32xk0lmsb008fz7mq23hap9b0kcld6vgs79sisn4")))

(define-public crate-json-pointer-0.2.0 (c (n "json-pointer") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1mhs50djrs31bs8avfm0csips3m3p17ix8l4n874byg73nn0qsa8")))

(define-public crate-json-pointer-0.3.0 (c (n "json-pointer") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1d91rz18ih5a42kis3k3fh4aivm1mwjys1f0wqwls4j06i6ivav2")))

(define-public crate-json-pointer-0.3.1 (c (n "json-pointer") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1iagymywxyjkln1abcsrydz8nsy10ni3v71jzvwwlrdxbskihvby")))

(define-public crate-json-pointer-0.3.2 (c (n "json-pointer") (v "0.3.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1ldgvnssf6rna3j7qj0w4h3w43id2amq7f8ph9kicrp7p738prcb")))

(define-public crate-json-pointer-0.3.3 (c (n "json-pointer") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "07jsjc7ffv89cc434if0fpwmddhgrw2sa22jn19fb0ncm7nfnxfm")))

(define-public crate-json-pointer-0.3.4 (c (n "json-pointer") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "15r9kkj83h26lk2q9p6q4qpl3kr79789vqff2ci4i6ki9swl3s2z")))

