(define-module (crates-io js on jsonprima) #:use-module (crates-io))

(define-public crate-jsonprima-0.1.0 (c (n "jsonprima") (v "0.1.0") (h "0rg0vcq5473dhhkpkz3kx25kcjw1vdhcdq7y6nalh389fpv7vw10")))

(define-public crate-jsonprima-0.1.1 (c (n "jsonprima") (v "0.1.1") (h "0wjz024c1abiwl7aqz6rm1pm4f0dqrjjr6wqlam50js64wjlvwxy")))

(define-public crate-jsonprima-0.2.0 (c (n "jsonprima") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0sgafwkrq016yqndc5ynflm8mj8gm3clhmjpk3zd7khql733r79d")))

(define-public crate-jsonprima-0.3.0 (c (n "jsonprima") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0qzz991ad9ffqc00ja71bamksgay9d68flrswz9mhi8zlb4da97f")))

