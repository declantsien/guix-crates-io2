(define-module (crates-io js on json-digest-wasm) #:use-module (crates-io))

(define-public crate-json-digest-wasm-0.0.15 (c (n "json-digest-wasm") (v "0.0.15") (d (list (d (n "json-digest") (r "^0.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.121") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0w8wjighxbx1lv0sqzl1p5liajgb6n12v536qc8nvy27wxmcha8z")))

(define-public crate-json-digest-wasm-0.0.16 (c (n "json-digest-wasm") (v "0.0.16") (d (list (d (n "json-digest") (r "^0.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.81") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0n7hr40vq762y7cx8mz18zmy3arqp1qpmvh8ws2qcyz8lm2vrjn4")))

