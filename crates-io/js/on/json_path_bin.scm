(define-module (crates-io js on json_path_bin) #:use-module (crates-io))

(define-public crate-json_path_bin-0.1.5 (c (n "json_path_bin") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json_path") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19h29q52m0k573ypj5sfy1h1hd4d96bip6zmv2110igw6ypwn0cn")))

