(define-module (crates-io js on jsonnlp) #:use-module (crates-io))

(define-public crate-jsonnlp-0.0.1 (c (n "jsonnlp") (v "0.0.1") (d (list (d (n "restson") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0pyjm1jddfixfabb32gyffch8y7yvs4h98s8j34cifxnjpskqajp") (y #t)))

(define-public crate-jsonnlp-0.0.2 (c (n "jsonnlp") (v "0.0.2") (d (list (d (n "restson") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "07qhw482vihwxvc0ia2sr87pzd5ai9im3vj1hk1jp3z1xjshicam") (y #t)))

(define-public crate-jsonnlp-0.0.3 (c (n "jsonnlp") (v "0.0.3") (d (list (d (n "restson") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1vc9a085m766vsz6gl8kz25g8g2swz73pc9d1zg00cs8hbd9d1qz")))

(define-public crate-jsonnlp-0.0.4 (c (n "jsonnlp") (v "0.0.4") (d (list (d (n "restson") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0mqvp6jbffh944gf44c6695n8k55h7ack940fh2pa904d1fg1zg1")))

