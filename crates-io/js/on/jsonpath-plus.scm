(define-module (crates-io js on jsonpath-plus) #:use-module (crates-io))

(define-public crate-jsonpath-plus-0.1.0 (c (n "jsonpath-plus") (v "0.1.0") (d (list (d (n "chumsky") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jfg45ccr45j0hpqjz8r4f7fzm2hpxm2f52gmfyv26d76bh230s5")))

(define-public crate-jsonpath-plus-0.1.1 (c (n "jsonpath-plus") (v "0.1.1") (d (list (d (n "chumsky") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xkwz8aykqa32jhv88b8azv88c1ak4ppkbd2pkcskildmhb34s8a")))

(define-public crate-jsonpath-plus-0.1.2 (c (n "jsonpath-plus") (v "0.1.2") (d (list (d (n "chumsky") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w297vyf0n02kyzhvmy49kjfdi1s91y2ca94dblwgx07mv1rqxqg")))

(define-public crate-jsonpath-plus-0.1.3 (c (n "jsonpath-plus") (v "0.1.3") (d (list (d (n "chumsky") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v2byrhp5y03ba56p8j5nf7n5mf73h8iy0b4gsfhzr2lbn1m1ymn")))

(define-public crate-jsonpath-plus-0.1.4 (c (n "jsonpath-plus") (v "0.1.4") (d (list (d (n "chumsky") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dm4da6pxd21p39iazyfv2qsq1vk0jd2ilgnlxjgspni5afv1zx9") (f (quote (("spanned"))))))

(define-public crate-jsonpath-plus-0.1.5 (c (n "jsonpath-plus") (v "0.1.5") (d (list (d (n "chumsky") (r "^0.8") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "138hjd620gnprb8v361kls5sv2h1rdipaz09nkcayq093d85g5g3") (f (quote (("spanned"))))))

(define-public crate-jsonpath-plus-0.1.6 (c (n "jsonpath-plus") (v "0.1.6") (d (list (d (n "chumsky") (r "^0.8") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "039mj6znpg2ymzihpr1xbrrld13n5wswpp154clcgksm0c153xsz") (f (quote (("spanned"))))))

(define-public crate-jsonpath-plus-0.1.7 (c (n "jsonpath-plus") (v "0.1.7") (d (list (d (n "chumsky") (r "^0.8") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zrf2akaml5ksainahsm24mb0df9z8s0ndd24wa1g0ya3k6wp1xd") (f (quote (("spanned"))))))

(define-public crate-jsonpath-plus-0.1.8 (c (n "jsonpath-plus") (v "0.1.8") (d (list (d (n "chumsky") (r "^0.8") (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gs5kharh36rgc2xywnq40pfdiqc71khy5h0c72fvryd3y5jnvrl") (f (quote (("spanned"))))))

(define-public crate-jsonpath-plus-0.1.9 (c (n "jsonpath-plus") (v "0.1.9") (d (list (d (n "chumsky") (r "^0.8") (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ysjxbsazyka2zlx4jqcynbwx64j4q1wbp06b3fjp7ik9q3s3rpf") (f (quote (("spanned"))))))

