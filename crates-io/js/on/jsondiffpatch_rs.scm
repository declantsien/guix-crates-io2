(define-module (crates-io js on jsondiffpatch_rs) #:use-module (crates-io))

(define-public crate-jsondiffpatch_rs-0.1.0 (c (n "jsondiffpatch_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "jsondiff") (r "^0.1.0") (d #t) (k 0) (p "jsondiff_rs")) (d (n "jsonpatch") (r "^0.1.0") (d #t) (k 0) (p "jsonpatch_rs")) (d (n "jsonptr") (r "^0.1.0") (d #t) (k 0) (p "jsonptr_rs")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12lmkbj266wcbyqx9q7xk45x95k6cdhbdgqadifd4nb1iavxspqs")))

