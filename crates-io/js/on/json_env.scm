(define-module (crates-io js on json_env) #:use-module (crates-io))

(define-public crate-json_env-0.1.0 (c (n "json_env") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bgydpw6k4wyvqpw2yg7g14w66v3ghlg7nq59lblymakzwf1r2lq")))

(define-public crate-json_env-1.0.0 (c (n "json_env") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19gphn5kfx37136dqisiqgxsw0pnb5bhv62mjgbpfg3w76d2iy2y")))

(define-public crate-json_env-1.0.1 (c (n "json_env") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ckxx761fb9596m5rvsq5zqma0mz0vsnjma87lvrmnvccrx2sqln")))

(define-public crate-json_env-1.0.2 (c (n "json_env") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04rbb19c16j2grl9ma2ci374k3gcyzbfrfmqn6azq4bf6imlmv91")))

(define-public crate-json_env-1.0.3 (c (n "json_env") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vl3cri9bb21hqcl27iywp9vlq0wl9s7kxiyw09vzv90ic3n1fra")))

(define-public crate-json_env-1.0.7 (c (n "json_env") (v "1.0.7") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18yw4ynjhfn86brsfy177c1a4xpfhnp1cklhfm1rki2vn7j7zmvc")))

(define-public crate-json_env-1.1.0 (c (n "json_env") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14qzg8mqh6m7npspaiivz31ra2hph7rqrxkjmcxhi29jf1p20k5v")))

(define-public crate-json_env-1.1.11 (c (n "json_env") (v "1.1.11") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06q4b4rc7p7h8cb263z4yakhg50illmmk1ahjz0n2b1v4kwvajd4")))

(define-public crate-json_env-1.1.12 (c (n "json_env") (v "1.1.12") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qp4523i6rrchljr8ymax60xyj0f4g9i6r6jqnvvs8pdmgwd35rq")))

(define-public crate-json_env-1.1.13 (c (n "json_env") (v "1.1.13") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16h4qxhly9p3hlgnxnalmp0zl1vhs1czzdkfanmvm35b5kvf3kbh")))

(define-public crate-json_env-1.2.1 (c (n "json_env") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonpath-rust") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c41j0ilyjz7jh6dq7nply51w9371ncp4db4f2ab8i8kiaj3d10q")))

(define-public crate-json_env-1.3.0 (c (n "json_env") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jsonpath-rust") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wvlj0czqfssf81wfpar2z5zd87crbhnsksqiy7myikds726mp2l")))

