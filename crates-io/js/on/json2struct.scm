(define-module (crates-io js on json2struct) #:use-module (crates-io))

(define-public crate-json2struct-0.1.0 (c (n "json2struct") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1icrsmwfm7q9qf2rc6qdhxx4sdc3rvn5shnmz8589g2rkxskd2nc")))

(define-public crate-json2struct-0.1.1 (c (n "json2struct") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0zbks0rnwipa9688js62dffwzj7j7laqy1r221r1v9dsh0zn5hcr")))

(define-public crate-json2struct-0.1.2 (c (n "json2struct") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0ccfs6na50kdmijzwmzh9x859f7a2n2zazj6mky7d9gp958ahzn2")))

(define-public crate-json2struct-0.1.3 (c (n "json2struct") (v "0.1.3") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "10v5zd3sirbhrlmhdl69qmqaxnfw4v9wiy8k1wv66rx69jd0wf7c")))

(define-public crate-json2struct-0.1.4 (c (n "json2struct") (v "0.1.4") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1p5w10zxjj8073insly2m7njid330xq1flmw18qsa2yzzprrjjn3")))

(define-public crate-json2struct-0.1.5 (c (n "json2struct") (v "0.1.5") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1wari1zdaa75sfx4siyn7drgbaira50wnqfkkc4dxvk9cab9lfbd")))

(define-public crate-json2struct-0.1.6 (c (n "json2struct") (v "0.1.6") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0a70mwdsp3sxf19048jaqkxp1mcf3x8kqxn6pad84f7v4pcv8xhh")))

