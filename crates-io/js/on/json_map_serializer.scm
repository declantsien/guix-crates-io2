(define-module (crates-io js on json_map_serializer) #:use-module (crates-io))

(define-public crate-json_map_serializer-0.1.3 (c (n "json_map_serializer") (v "0.1.3") (d (list (d (n "impl_serialize") (r "^3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "111d8lnv53ch72fkc1n6yl2ilqqr0lw0zsvaibxr0lm6sqpkmmri") (y #t)))

