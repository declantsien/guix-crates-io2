(define-module (crates-io js on jsonrpc-reactor) #:use-module (crates-io))

(define-public crate-jsonrpc-reactor-0.1.0 (c (n "jsonrpc-reactor") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt" "sync" "time"))) (o #t) (d #t) (k 0)))) (h "0n9zmxzfvs3abi6069x5q60l9hldygrqhnf6jc3h3xjhrir28nvv") (f (quote (("std" "serde/std" "serde_json/std") ("reactor" "std" "tokio") ("default" "reactor" "std" "serde/default" "serde_json/default"))))))

(define-public crate-jsonrpc-reactor-0.1.1 (c (n "jsonrpc-reactor") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt" "sync" "time"))) (o #t) (d #t) (k 0)))) (h "1ir9mfpga5cp0vqjabs28yxwmsf28n4qkwgnkd9wcv7mcr174la6") (f (quote (("std" "serde/std" "serde_json/std") ("reactor" "std" "tokio") ("default" "reactor" "std" "serde/default" "serde_json/default"))))))

(define-public crate-jsonrpc-reactor-0.1.2 (c (n "jsonrpc-reactor") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt" "sync" "time"))) (o #t) (d #t) (k 0)))) (h "0pg3i3sah4zamwb29dbs37d98pzh2smk4xrrf6wv2qbxsyl8ii3a") (f (quote (("std" "serde/std" "serde_json/std") ("reactor" "std" "tokio") ("default" "reactor" "std" "serde/default" "serde_json/default"))))))

