(define-module (crates-io js on jsona-ast) #:use-module (crates-io))

(define-public crate-jsona-ast-0.1.0 (c (n "jsona-ast") (v "0.1.0") (d (list (d (n "jsona") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "04zy3s0bhdifmjlbvyfxhdas70nxzgijfwcxlgknf2cl3jf71b5j")))

