(define-module (crates-io js on jsonml) #:use-module (crates-io))

(define-public crate-jsonml-0.1.0 (c (n "jsonml") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0pydvxin7hjqczgrd4ay74xaly2lxjys3va0v042vl1x955kgmg4") (r "1.63")))

(define-public crate-jsonml-0.2.0 (c (n "jsonml") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "04aw5z843715p67w5brhilb5ra54f1bhvv71734yc7vvsyd941xp") (r "1.63")))

(define-public crate-jsonml-0.3.0 (c (n "jsonml") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1010glhxng110bp9p99rc5pm0p7zc3ryxsdws8lds4v6ialiczrc") (f (quote (("any-attribute-value-type" "serde_json")))) (r "1.63")))

(define-public crate-jsonml-0.4.0 (c (n "jsonml") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "003qn8vyvsmanxh0d03pyq4vixy6yqnsbjzdcmr8pj0jsrrgx2f1") (r "1.63")))

(define-public crate-jsonml-0.4.1 (c (n "jsonml") (v "0.4.1") (d (list (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.144") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0jnmipiqqhxvg98swpgfrn1hbrp3hz0apx7m1hbgwfbpcx56rz4f") (r "1.63")))

(define-public crate-jsonml-0.4.2 (c (n "jsonml") (v "0.4.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 2)) (d (n "serde_test") (r "^1.0.152") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1ggnklh37fqmdqfqyb14ymw3vf190fy100xs0ddsv6jg4xvl2n5n") (r "1.63")))

