(define-module (crates-io js on jsonbank) #:use-module (crates-io))

(define-public crate-jsonbank-0.1.0 (c (n "jsonbank") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yyl3vc0hmgnid93792v51f9k4vfhq16j6qd0ypgr244r5mqgmfv")))

(define-public crate-jsonbank-0.1.1 (c (n "jsonbank") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r02xqypq52x5js5bnv8kl5az3jhqa7ab6plxqa11qvfcfz8bsxz")))

(define-public crate-jsonbank-0.1.2 (c (n "jsonbank") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19xyisfh0a88n0gpv3y26xpafnz4kf734a9nfiwdsi2bb9z82vq5")))

(define-public crate-jsonbank-0.1.3 (c (n "jsonbank") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wvyjl1lbaw77dmmyydk2rdw0rjfbsrpp386zvrbp9ir7svyn89v")))

(define-public crate-jsonbank-0.1.4 (c (n "jsonbank") (v "0.1.4") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g5gkzvl0v6lf0xxlf0v7n31ghy09kzpqb1p3lhi1ak1sn1afsqx")))

(define-public crate-jsonbank-0.1.5 (c (n "jsonbank") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gcwc9f9kq7brbsjf8nhilv68w1graaq7akd5dxxvmnyc4q2xlwp")))

(define-public crate-jsonbank-0.1.6 (c (n "jsonbank") (v "0.1.6") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g87kj7kwccjld0bagj77ija17ls0ap8cpaqrikfs1qjj6xc6jkw")))

(define-public crate-jsonbank-0.1.7 (c (n "jsonbank") (v "0.1.7") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r9hvilly0bjch7w1xmvrik7chxcpv47dlid0nkvj5xh47h58dcf")))

(define-public crate-jsonbank-0.1.8 (c (n "jsonbank") (v "0.1.8") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "082mfjrhvpk6imyngfy83vw4mdj9g99ap0vx41xpbxprinkj19sq")))

(define-public crate-jsonbank-0.1.9 (c (n "jsonbank") (v "0.1.9") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wgvqb8g5g2mfr95svc7nvizrzhfq80qrs9crclxr75ww8pzfwzl")))

