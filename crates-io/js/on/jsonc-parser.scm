(define-module (crates-io js on jsonc-parser) #:use-module (crates-io))

(define-public crate-jsonc-parser-0.1.0 (c (n "jsonc-parser") (v "0.1.0") (h "0vrv79ip9xxywkbzshglmldbhbgb0rc11kncrdivlyhxyabjdp8d")))

(define-public crate-jsonc-parser-0.2.0 (c (n "jsonc-parser") (v "0.2.0") (h "18d7xvkljzrvz0c4ikhrcxbsxp0lfw93nksd72y9c0ddyzx368dm")))

(define-public crate-jsonc-parser-0.3.0 (c (n "jsonc-parser") (v "0.3.0") (h "18krscvfc7b3xdil9d7gpzf1s5v23wvsgb32dz4rksjkgg1va2df")))

(define-public crate-jsonc-parser-0.4.0 (c (n "jsonc-parser") (v "0.4.0") (h "16w46ac5qym8mall536c656jwf0qpxi558glpqhpcj4w9ilgv6jk")))

(define-public crate-jsonc-parser-0.5.0 (c (n "jsonc-parser") (v "0.5.0") (h "07c3d2qn2719lzf1353825v9i9lca53iwppp7z982gpj5a4bl8wv")))

(define-public crate-jsonc-parser-0.6.0 (c (n "jsonc-parser") (v "0.6.0") (h "11k8ba4mhyhnanwdfp0cyj2vc772f911r02b09k3492vycc0vfpc")))

(define-public crate-jsonc-parser-0.7.0 (c (n "jsonc-parser") (v "0.7.0") (h "0akjd4wd9wji1wazxmcg5hfq17qqics8hnvg0b287vrcqph00mhp")))

(define-public crate-jsonc-parser-0.8.0 (c (n "jsonc-parser") (v "0.8.0") (h "04dlv13xz4cx9bqnq4da2ij057l97jc9ldvnkq18syjjygjs870j")))

(define-public crate-jsonc-parser-0.9.0 (c (n "jsonc-parser") (v "0.9.0") (h "1klb8g2znczc6sygnr3s0y1w0arisg25szqabf1i24nl6wmsy3cv")))

(define-public crate-jsonc-parser-0.10.0 (c (n "jsonc-parser") (v "0.10.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0f3f7b5653ix7pw874akvgm12f2ppxlzccb1pxwnx4aj34xz2ca5")))

(define-public crate-jsonc-parser-0.11.0 (c (n "jsonc-parser") (v "0.11.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1ll0584l00cbp7zba7aw3dr8yp663234fprdl621ahhs613kq80c")))

(define-public crate-jsonc-parser-0.12.0 (c (n "jsonc-parser") (v "0.12.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1mx50rk3vk58g113545d6mh6dfxrmx7v2vzii17cqn8dc9ff8kl3")))

(define-public crate-jsonc-parser-0.12.1 (c (n "jsonc-parser") (v "0.12.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1iimvd29aw0yms305cw9pv4hwbbidqja15xay2n42bgkifz84ckr")))

(define-public crate-jsonc-parser-0.12.2 (c (n "jsonc-parser") (v "0.12.2") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "06rhdwgr7zw8y7rdmmwjk784wgfs9xd2ph0aqfg65zw5p2m40303")))

(define-public crate-jsonc-parser-0.13.0 (c (n "jsonc-parser") (v "0.13.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "022ymj71pvbs712bdrb20bfdgk87ysid52avdqhnbahs90h2yhvs")))

(define-public crate-jsonc-parser-0.14.0 (c (n "jsonc-parser") (v "0.14.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "11hiikb864wvmhybydppb5g8bm8dlxwjhx3yyd1irfc1hklb7scc")))

(define-public crate-jsonc-parser-0.15.0 (c (n "jsonc-parser") (v "0.15.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0lah7cq7i5lza9gvp2w1hbxl7qrl75sss6p9ablzdmd6pbmli25d")))

(define-public crate-jsonc-parser-0.15.1 (c (n "jsonc-parser") (v "0.15.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "12xxc9ynj76p05iq16rqlm0baizj8ywbgc451xf7fw17fjmywnfs")))

(define-public crate-jsonc-parser-0.16.0 (c (n "jsonc-parser") (v "0.16.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0djxdhb2axmzc0xs34csz7r178hzdf1jbgrgw0qkdc1jhsx30m0i")))

(define-public crate-jsonc-parser-0.17.0 (c (n "jsonc-parser") (v "0.17.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qh536zqydsasn6a1j8ijh1qq13gjc4jvwf6y5gkzhs9cgmsif0c") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.17.1 (c (n "jsonc-parser") (v "0.17.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zq5xk0zsxygripb913lx72ivbhb70bz39s3iga6k27w1frmbsrc") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.18.0 (c (n "jsonc-parser") (v "0.18.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18jydh9bbi0mb6m1nh5nv73ddzzaq0f6vm5a1ahx5hcc8qdl36mq") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.19.0 (c (n "jsonc-parser") (v "0.19.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10r7aff426jzxim0ziyjl81lvf9mhv2h07dyc4ld6kjc6b6v1frl") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.20.0 (c (n "jsonc-parser") (v "0.20.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jy4d6a6n9zd845qrfwcbq4jacvjw07cadb9q29jrwba23zq3zyc") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.21.0 (c (n "jsonc-parser") (v "0.21.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01wkq86xw3nzplck0mzfwdlh285b2dnh8n2kd2d6y81k0gj5661s") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.21.1 (c (n "jsonc-parser") (v "0.21.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gblaxf94da7hp9ynmx69r93qpgmbksd3z09b8jq8li3fq7a4mkv") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.22.0 (c (n "jsonc-parser") (v "0.22.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0aas0281kmckk60qxqahmcnbxzynczz05i3c30fn568x5z89q7zl") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.22.1 (c (n "jsonc-parser") (v "0.22.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0m1wm17z1l24zhizlzs77ig25af21snwkm2j2cwlvh5saknh51sk") (f (quote (("serde" "serde_json"))))))

(define-public crate-jsonc-parser-0.23.0 (c (n "jsonc-parser") (v "0.23.0") (d (list (d (n "indexmap") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1da16qr6iq6kfcq7dk3q3wvmv6bsy9d0a7cc4bc2335cr8hc69bp") (f (quote (("serde" "serde_json") ("preserve_order" "indexmap" "serde_json/preserve_order"))))))

