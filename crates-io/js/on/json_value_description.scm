(define-module (crates-io js on json_value_description) #:use-module (crates-io))

(define-public crate-json_value_description-0.1.0 (c (n "json_value_description") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "10qjzjb8m7517kx3x22mgwfn85ihrlvw70588qszdbym8kp758iv")))

(define-public crate-json_value_description-0.2.0 (c (n "json_value_description") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0rynyzxqvwnsclzrkjabbddywybamdjiwc3mzl7bsgm4klqzj25j")))

