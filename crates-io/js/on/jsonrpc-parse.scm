(define-module (crates-io js on jsonrpc-parse) #:use-module (crates-io))

(define-public crate-jsonrpc-parse-0.1.0 (c (n "jsonrpc-parse") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "httparse") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "1b2gb5bzhgi213wxqkvf8bm96qrn84k0nlqqlja8kzg6xd93d0b3")))

(define-public crate-jsonrpc-parse-0.1.1 (c (n "jsonrpc-parse") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "httparse") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "0jbh1f1vf8dfny9y7i7rzhn1jrlb25vrp213rn65wwv0dw1d25zi")))

(define-public crate-jsonrpc-parse-0.1.2 (c (n "jsonrpc-parse") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "httparse") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "0pa9qhk3y1l8p7dxazzab7b2n9rwiss97did8yxxki3wvspmm90s")))

