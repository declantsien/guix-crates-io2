(define-module (crates-io js on jsontp) #:use-module (crates-io))

(define-public crate-jsontp-0.1.0 (c (n "jsontp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "now"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02i2fxc94hz53rhwa1zkxxn1g5g75dnf0jsig1jy4i4yjpbq4wbv")))

(define-public crate-jsontp-0.1.1 (c (n "jsontp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "now"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s6a54smhy9n19m2ym5vh98gvvkqmj6nnldw7lpbjvwpvw4kl515")))

(define-public crate-jsontp-0.1.2 (c (n "jsontp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "now"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xgmnfx5khsqpmiq4m4p83v83iad20fpg0lm4k3iqvk5vhgdp5a9")))

(define-public crate-jsontp-0.1.3 (c (n "jsontp") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("alloc" "now"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "058n0yzyisawpfykkqw2qk6zj3vsr71hqyjpxni3dm3p091sc4kl")))

