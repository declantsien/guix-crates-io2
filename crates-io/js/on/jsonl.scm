(define-module (crates-io js on jsonl) #:use-module (crates-io))

(define-public crate-jsonl-0.1.0 (c (n "jsonl") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zg2l315g6x058p4d0qr0bg43w5wkhsxmskv0mjg75ndw8997ixm")))

(define-public crate-jsonl-0.2.0 (c (n "jsonl") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kp6fk1qncna2xah8yypkff96i8j4bs6qqq1bg293dwk8924sqhy")))

(define-public crate-jsonl-1.0.0 (c (n "jsonl") (v "1.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dv8h494skaaqgh10617jz92j6xajx9mvv1m9nr0dnlxxrh3yadr")))

(define-public crate-jsonl-1.1.0 (c (n "jsonl") (v "1.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rb3fygv52d4xy6mikccnc3r7zvbhzd1c7l18dnh1qmh7ddirwb4")))

(define-public crate-jsonl-1.2.0 (c (n "jsonl") (v "1.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09686rzz94w9zzxr4ard0jmaaikh49sg1vj3hdiwrz7mlsia61jz")))

(define-public crate-jsonl-1.3.0 (c (n "jsonl") (v "1.3.0") (d (list (d (n "mio") (r "^0.7.7") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rzwkn1cqs5xzpfnzbd16my22qa7bblf1h7vkqs8cvdsf9lsmg7j") (f (quote (("mio-tcp-stream" "mio" "parking_lot"))))))

(define-public crate-jsonl-2.0.0 (c (n "jsonl") (v "2.0.0") (d (list (d (n "mio") (r "^0.7.7") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0brbjfknkxb0xck3rqzij8vvmh9fingqj453nq35ylnjiz7wkv7j") (f (quote (("mio-tcp-stream" "mio" "parking_lot"))))))

(define-public crate-jsonl-3.0.0 (c (n "jsonl") (v "3.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "io-std" "net" "process"))) (o #t) (d #t) (k 0)))) (h "1fkx3nlbrsgb2zhlq3im81xb2nz5xsdpk7m4jphxncaz96m474bj")))

(define-public crate-jsonl-3.1.0 (c (n "jsonl") (v "3.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "io-std" "net" "process"))) (o #t) (d #t) (k 0)))) (h "10p1r0wwkl23vgdbz8cc3gl51sps9z0svf0d09b595blqh14bzvl")))

(define-public crate-jsonl-4.0.0 (c (n "jsonl") (v "4.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "io-std" "net" "process"))) (o #t) (d #t) (k 0)))) (h "0wgi8k89cqkc1bhjfn8kyjck9v34128c5f1kjfpg4z15128p75qa") (y #t)))

(define-public crate-jsonl-4.0.1 (c (n "jsonl") (v "4.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "io-std" "net" "process"))) (o #t) (d #t) (k 0)))) (h "103bks8zywmwlz5b8zavxcsnpbc0d8b9i1qr1jcc2kr38n7ykfhs")))

