(define-module (crates-io js on json_io) #:use-module (crates-io))

(define-public crate-json_io-0.1.0 (c (n "json_io") (v "0.1.0") (d (list (d (n "find_folder") (r "^0.1.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "0q5hqdw7wx73xzlgw669c5i08753ghfiai5y30shj9b1k7a5ygzs")))

(define-public crate-json_io-0.1.1 (c (n "json_io") (v "0.1.1") (d (list (d (n "find_folder") (r "^0.1.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "0kp7xc6y1017wjl8spnidwh1sybzbbhh3w9715sn8lnfsmj4hcng")))

(define-public crate-json_io-0.1.2 (c (n "json_io") (v "0.1.2") (d (list (d (n "find_folder") (r "^0.1.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "0d495m3if5sravxq3zxq8rdhyrr2gqnd1s268bbh3pp7b858vk6i")))

(define-public crate-json_io-0.2.0 (c (n "json_io") (v "0.2.0") (d (list (d (n "find_folder") (r "^0.1.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.19") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0i1rfqb5165naxha7yli664h5fj3m19kj8aanjj5dq0lq8wcnv76") (f (quote (("serde_serialization" "serde" "serde_json") ("rustc_serialization" "rustc-serialize") ("default" "rustc_serialization"))))))

(define-public crate-json_io-0.3.0 (c (n "json_io") (v "0.3.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0anx2iqmhn2q7k0z5xqsq8pkj0ngl76hq3k7a6wbfsbjlk16mlws") (f (quote (("serde_serialization" "serde" "serde_json") ("rustc_serialization" "rustc-serialize") ("default" "serde_serialization"))))))

