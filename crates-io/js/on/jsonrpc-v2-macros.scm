(define-module (crates-io js on jsonrpc-v2-macros) #:use-module (crates-io))

(define-public crate-jsonrpc-v2-macros-0.1.0 (c (n "jsonrpc-v2-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1flnxg8nqnsq9srdd1819qcd5h5iymk1kzglw70f14z7lqs5ki1y")))

