(define-module (crates-io js on jsonpath-rust) #:use-module (crates-io))

(define-public crate-jsonpath-rust-0.1.0 (c (n "jsonpath-rust") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19plx6zw0w0f8lkl9xm38h72f8ixjjzcjz274lihx6ldg3zvhjvp")))

(define-public crate-jsonpath-rust-0.1.1 (c (n "jsonpath-rust") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "190pvfn7gimlmc0wwb2kyn9a1zrp9fpj8f6sl47jv4z4n3ha6g7a")))

(define-public crate-jsonpath-rust-0.1.2 (c (n "jsonpath-rust") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "194p4nncc0jfk8bx2qccncs5gglckz91qybzhb6j8brpzsly7jcm")))

(define-public crate-jsonpath-rust-0.1.3 (c (n "jsonpath-rust") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zrcqxiifb5f7i4h9p7m601cnmfbq8i6jvfh441nr4xdw4jv89qq")))

(define-public crate-jsonpath-rust-0.1.4 (c (n "jsonpath-rust") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ix11g7prldj8gpkq40cjgwj5icmlb2ym2iq287gl5nywqrf71ns")))

(define-public crate-jsonpath-rust-0.1.5 (c (n "jsonpath-rust") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13a5qc11i6sqhivg5w4dg9kba2qzfcy6iws1fkw3hncd8fp4n3xy")))

(define-public crate-jsonpath-rust-0.1.6 (c (n "jsonpath-rust") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "066anqm9gzzr4kn5l5bl8kwdyiila1iibysa34rxa1vxpry1l4s1")))

(define-public crate-jsonpath-rust-0.2.0 (c (n "jsonpath-rust") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wl82ivl5yj7m21m7a5iwsqp05mvl97s5dbqp443yflm00vkq9ba")))

(define-public crate-jsonpath-rust-0.2.1 (c (n "jsonpath-rust") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01mf5is25732igza4ichwdic67c33lc8h980ip24xg5kwbnwkjxn")))

(define-public crate-jsonpath-rust-0.2.2 (c (n "jsonpath-rust") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14vlhxv5yxmaqydw1lhhjmz1nqkq1rv0pfvmalqrxfzg1ayv0kq4")))

(define-public crate-jsonpath-rust-0.2.3 (c (n "jsonpath-rust") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1awj9an6k0c0wr1kfv035ww3nwl61jmbbpyj5d4p0f9nb50wkfhj")))

(define-public crate-jsonpath-rust-0.2.5 (c (n "jsonpath-rust") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qm7fyys2vdx3l8x41z7hbpr27d24swz31ayl7j0rprp15kjygxs")))

(define-public crate-jsonpath-rust-0.2.6 (c (n "jsonpath-rust") (v "0.2.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b5py2p7cxnpnbfx7hpkziybvs1v9l07x2sch041mjiipziyfpj5")))

(define-public crate-jsonpath-rust-0.3.0 (c (n "jsonpath-rust") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fvshd4f3nnffq5w8h0b2706w4293pncwjh9dfm0853xpaijzspp")))

(define-public crate-jsonpath-rust-0.3.1 (c (n "jsonpath-rust") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1csadc9v2nybq2xzmsgww9ccs472z5f4g4pbsz01qjy550z5cm9b")))

(define-public crate-jsonpath-rust-0.3.2 (c (n "jsonpath-rust") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d81i836ssbdic78pnygghac1v0mc7fszvsmvxhi7nfjv57cwlaa")))

(define-public crate-jsonpath-rust-0.3.3 (c (n "jsonpath-rust") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hq5yirr0z5a959j7f8i0qdz2x3iyqdz5wbm8wfxxykxvgnxpdln")))

(define-public crate-jsonpath-rust-0.3.4 (c (n "jsonpath-rust") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11wlz9csmk3hn9vw9zyz98n6pf1fh63slwcvlsd613q2s2rghjbv")))

(define-public crate-jsonpath-rust-0.3.5 (c (n "jsonpath-rust") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0abqzgssg4vnv389v4c1p60a2qcpavwn88sp0kjhn9rxgixi5k06")))

(define-public crate-jsonpath-rust-0.4.0 (c (n "jsonpath-rust") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1c4yvkvrghny51zvwizccbg1ji5aav3zwgh5km8q7gfki1hvrb4n")))

(define-public crate-jsonpath-rust-0.5.0 (c (n "jsonpath-rust") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0qki2dvnfjdv2v909wacsdbd5kmf0r3rvvjhx0q88grr361hfs02")))

(define-public crate-jsonpath-rust-0.5.1 (c (n "jsonpath-rust") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0032bp43w6k1bl8h55m126cdf8xljj8p736f65gp3zvhpn2zxn0r")))

