(define-module (crates-io js on jsonpath_lib_polars_vendor) #:use-module (crates-io))

(define-public crate-jsonpath_lib_polars_vendor-0.0.1 (c (n "jsonpath_lib_polars_vendor") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0z8b92zhm1mrmb6fv95v2g5kqs6vmg5fl4zp3x3zf8knjia97ggl")))

