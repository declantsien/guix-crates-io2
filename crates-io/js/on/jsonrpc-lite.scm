(define-module (crates-io js on jsonrpc-lite) #:use-module (crates-io))

(define-public crate-jsonrpc-lite-0.1.0 (c (n "jsonrpc-lite") (v "0.1.0") (d (list (d (n "serde") (r "^0.7.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.2") (d #t) (k 0)))) (h "0d0652n5dv8djp5qpsh1czrd5mcflhnbxqzw3i6wb0ijwpwa2dpr")))

(define-public crate-jsonrpc-lite-0.2.0 (c (n "jsonrpc-lite") (v "0.2.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "19dqxsvvzapn8772my04yjcbkzfjcjfcpnyha059q18nmfbkd8n9")))

(define-public crate-jsonrpc-lite-0.3.0 (c (n "jsonrpc-lite") (v "0.3.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0bp43xaiyclv1sl7kbnsb9skq9jpi22gyhbynmihra8j3i9mbdrb")))

(define-public crate-jsonrpc-lite-0.4.0 (c (n "jsonrpc-lite") (v "0.4.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1krfs8hfzhyh4a75yy0rhrm0x7jk9hg2xqnw7cb44i61x35ln4n3")))

(define-public crate-jsonrpc-lite-0.4.1 (c (n "jsonrpc-lite") (v "0.4.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1liphg31mvzb2pk88hrar21wqdps6i812amcnsf5cpkwkdym8bcf")))

(define-public crate-jsonrpc-lite-0.5.0 (c (n "jsonrpc-lite") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m123bxf6knr64mr3d6x9qyqcx7p1k50clvs4xwdsjlq4rgj93d9")))

(define-public crate-jsonrpc-lite-0.6.0 (c (n "jsonrpc-lite") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "135qccg9s8n1p90zqp1bx503k11wvz23324ry95c3512m2mjhhdv")))

