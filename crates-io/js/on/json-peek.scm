(define-module (crates-io js on json-peek) #:use-module (crates-io))

(define-public crate-json-peek-0.0.1 (c (n "json-peek") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "1p9xx21dkv05hlqf2plms89q60awglr1grrsc77z1qjyq06qwy4z")))

(define-public crate-json-peek-0.0.2 (c (n "json-peek") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "0jwph9vpv4x43rakazk9f8icq68vkzb0j068gdfxrlcnvs4lgr1i")))

