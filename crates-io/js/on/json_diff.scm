(define-module (crates-io js on json_diff) #:use-module (crates-io))

(define-public crate-json_diff-0.1.0 (c (n "json_diff") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "16bqrc6035d5xxp9lm4apaqfyh3r1ksvmhc53810hx2brmi7d3rn")))

(define-public crate-json_diff-0.1.1 (c (n "json_diff") (v "0.1.1") (d (list (d (n "colored") (r "^1.9.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0vyssqi71vjn2f5b2yzv4cvm39nnmdv6sb3rxkqk60i77wcvb0jf")))

(define-public crate-json_diff-0.1.2 (c (n "json_diff") (v "0.1.2") (d (list (d (n "colored") (r "^1.9.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "162hygpmq8pkcs76imyaxjsm6dm90p3pw1vgg2lcnq04q3g2yxh1")))

