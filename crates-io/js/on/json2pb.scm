(define-module (crates-io js on json2pb) #:use-module (crates-io))

(define-public crate-json2pb-0.1.0 (c (n "json2pb") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "0ssz6dwi8ss814miqgs6i2q6gjkz4vias7sn1p2gllvqiqdgrqdn")))

(define-public crate-json2pb-0.1.1 (c (n "json2pb") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "176dg972hqx59p7ac96zz5ay0fb3gladym3h660nnpj8cxqlr7b9")))

(define-public crate-json2pb-0.1.2 (c (n "json2pb") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "1acw7r7gsijn6csjj2dy8sl43p87k79f2wd2rswk28fs9s5fp35m")))

(define-public crate-json2pb-0.2.0 (c (n "json2pb") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "0ah66nzssn24s9sz6b71zn1f84xlz4yng1kl5mfhaa1n96drfsm8")))

(define-public crate-json2pb-0.2.1 (c (n "json2pb") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "16sjcrmgb49zgi1fm62xjkk25n2q9ldsvgz1jhc36smvvylb4gyf")))

