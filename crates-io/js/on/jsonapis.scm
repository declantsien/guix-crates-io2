(define-module (crates-io js on jsonapis) #:use-module (crates-io))

(define-public crate-jsonapis-0.0.0 (c (n "jsonapis") (v "0.0.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0q3v642925ycdh8038cjxj791wnqriia4xfk3h93blipv9n3dnq6") (y #t)))

(define-public crate-jsonapis-0.0.1 (c (n "jsonapis") (v "0.0.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "00n689ab7y7qsgczmvv85xri2x1kif21c3543hvd638f0wyvzan5") (y #t)))

(define-public crate-jsonapis-0.0.2 (c (n "jsonapis") (v "0.0.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "04hf172gyp3kfd6j9xj6ggsbw1zkg2zixcahvxgmh40yczdcdsvl") (y #t)))

(define-public crate-jsonapis-0.0.3 (c (n "jsonapis") (v "0.0.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "096p0dhsf310qx03s49c84c451wyqgrr811sdy89ycbmpyrdznd1") (y #t)))

(define-public crate-jsonapis-0.0.4 (c (n "jsonapis") (v "0.0.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0bx7anxfbykk5mipg02yycn6lnhxrixvdvnw0lzngfiallqfkszi") (y #t)))

(define-public crate-jsonapis-0.0.5 (c (n "jsonapis") (v "0.0.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "09rkb8fr0pz7rl0vb9ml4lxjgb8dl48rcr9g6ipll27fab9krcn8") (f (quote (("default" "client") ("client" "reqwest" "url")))) (y #t)))

(define-public crate-jsonapis-0.0.6 (c (n "jsonapis") (v "0.0.6") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "16wwibx2c1n6hghzqdxvchhpik1yz32w6jk8q2ffjkwxgsaffydd") (f (quote (("default" "client") ("client" "reqwest" "url"))))))

(define-public crate-jsonapis-0.0.7 (c (n "jsonapis") (v "0.0.7") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "1wn2cac98n95f8s5rj7ikg5gd70m0ljfy38gqa4f6zr2gd5mg8yb") (f (quote (("default" "client") ("client" "reqwest" "url")))) (y #t)))

(define-public crate-jsonapis-0.0.8 (c (n "jsonapis") (v "0.0.8") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (o #t) (d #t) (k 0)))) (h "1zixapp18by13ykxjrwqs2xm0lbksvdfgy4klrhim47zwhx7fdph") (f (quote (("default" "client") ("client" "reqwest" "url"))))))

