(define-module (crates-io js on jsonrpc-macros-plus) #:use-module (crates-io))

(define-public crate-jsonrpc-macros-plus-7.1.1 (c (n "jsonrpc-macros-plus") (v "7.1.1") (d (list (d (n "jsonrpc-core") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-pubsub") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-tcp-server") (r "^7.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1v2q0b0r6z2d1y2z367sd2adcya72nlziscyb3dlfai4qj6d93bm")))

(define-public crate-jsonrpc-macros-plus-7.1.2 (c (n "jsonrpc-macros-plus") (v "7.1.2") (d (list (d (n "jsonrpc-core") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-pubsub") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-tcp-server") (r "^7.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1f4syv8bc1ch0qjqrjvhqpgcx055n55psi94fm6byxfl6rfpy533")))

(define-public crate-jsonrpc-macros-plus-7.1.3 (c (n "jsonrpc-macros-plus") (v "7.1.3") (d (list (d (n "jsonrpc-core") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-pubsub") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-tcp-server") (r "^7.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00n8llm5a4sg0w1fq7vcncxfkk7xch4gj24wh2538a2gzd7swpmz")))

(define-public crate-jsonrpc-macros-plus-7.1.4 (c (n "jsonrpc-macros-plus") (v "7.1.4") (d (list (d (n "jsonrpc-core") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-pubsub") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-tcp-server") (r "^7.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fpf30k5dakrxcnygymxslq30gwpcaaykc6da3k39ybwzfyd09al")))

