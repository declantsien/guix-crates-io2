(define-module (crates-io js on json-api-rocket) #:use-module (crates-io))

(define-public crate-json-api-rocket-0.1.0 (c (n "json-api-rocket") (v "0.1.0") (d (list (d (n "json-api") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1laxnymixa1a379as1jf88cz1qvcvq4kv9bp84ifbpx1q9nm2c8x")))

(define-public crate-json-api-rocket-0.2.0 (c (n "json-api-rocket") (v "0.2.0") (d (list (d (n "json-api") (r "^0.2") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00fg2whs4i3wx09b3700ykrb5jb149rvh3p6wpql57idzxdy9ad0")))

(define-public crate-json-api-rocket-0.3.0 (c (n "json-api-rocket") (v "0.3.0") (d (list (d (n "json-api") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vzpwsak9qyhj629qsndxv21yj4cdf3qkyd9z12diyh2x8bzx5hh")))

(define-public crate-json-api-rocket-0.4.0 (c (n "json-api-rocket") (v "0.4.0") (d (list (d (n "json-api") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i61vy5038idca2a8r1mhr3jnc66jyka2v5gf5rc7d20abqabfh9")))

(define-public crate-json-api-rocket-0.4.1 (c (n "json-api-rocket") (v "0.4.1") (d (list (d (n "json-api") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hm9fdl3ijjzmm8ydr5h4zhj5xy78q3hj0rz54nk4dwbjwn4pck8")))

