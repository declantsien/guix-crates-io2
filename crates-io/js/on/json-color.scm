(define-module (crates-io js on json-color) #:use-module (crates-io))

(define-public crate-json-color-0.1.0 (c (n "json-color") (v "0.1.0") (d (list (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "1vkdcpknm7x7xb15jm4dqr0ppfqrxvhaiqnsgn57h8m2nfbbcfpr")))

(define-public crate-json-color-0.2.0 (c (n "json-color") (v "0.2.0") (d (list (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "17pfqn5fkngqk9d7v58yrabys1brjm27mjw4hb0qyhk5jnf3dw8p")))

(define-public crate-json-color-0.3.0 (c (n "json-color") (v "0.3.0") (d (list (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "11vb1kjhy0127g359g4kzq3i5c4syazn6mhjsgxhcis9r83dc3s5")))

(define-public crate-json-color-0.4.0 (c (n "json-color") (v "0.4.0") (d (list (d (n "colored") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "0alpd6m56pasv9h67jza76fydrhvqq8kvp02vgih1hvzybm6r1sy")))

(define-public crate-json-color-0.5.0 (c (n "json-color") (v "0.5.0") (d (list (d (n "colored") (r "^1.4.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)))) (h "1gbvnb5l0ilvn4lc23agmkk1knyijyrq2zj57s53bfgm5mycy4c1")))

(define-public crate-json-color-0.6.0 (c (n "json-color") (v "0.6.0") (d (list (d (n "colored") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1jd6xvqg24ilycvc1k4441x23gmb90pk1jfv9gb23vdxrqj32q7d")))

(define-public crate-json-color-0.6.1 (c (n "json-color") (v "0.6.1") (d (list (d (n "colored") (r "^1.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1v2099cq8r34bcfhgm3cachpkjmgpf6qic9ysk4rwi97zl9agbrd")))

(define-public crate-json-color-0.6.2 (c (n "json-color") (v "0.6.2") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0146y6x425gdasjzgc4c6kw50ajn8nlpblwd6cjp70zv9xcpdp89")))

(define-public crate-json-color-0.7.0 (c (n "json-color") (v "0.7.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0cxkwzsd3sxs56iipi716dgb2cj18i7knis6hqaqirmb6msilmwq")))

(define-public crate-json-color-0.7.1 (c (n "json-color") (v "0.7.1") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1s5nfk2z3hnj7bnr45i701l2g7h0lcdy7hwcm4s75baw2xaqrp76")))

