(define-module (crates-io js on json2csv) #:use-module (crates-io))

(define-public crate-json2csv-0.1.0 (c (n "json2csv") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rust-flatten-json") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1815k07829n4gf96wzw67yz7zff1mpmz2v89gwh6xc5wg452b7f4")))

