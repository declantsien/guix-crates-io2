(define-module (crates-io js on jsonerr) #:use-module (crates-io))

(define-public crate-jsonerr-0.0.1 (c (n "jsonerr") (v "0.0.1") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0171nmj761n6bkdzaclj89p58b6chxa3rkf7gcp20af2cggf818s")))

(define-public crate-jsonerr-0.0.2 (c (n "jsonerr") (v "0.0.2") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ji46vb0hbxg02akgyhr7zl64p3h35c52ryqak48sgd7y5d44h35")))

(define-public crate-jsonerr-0.0.3 (c (n "jsonerr") (v "0.0.3") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z6r9lsj8qaxqknqgymihmqx2qsip438hzqlwi9b8h3brl5yxs2a")))

