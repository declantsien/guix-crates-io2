(define-module (crates-io js on jsonrpc-pubsub-examples) #:use-module (crates-io))

(define-public crate-jsonrpc-pubsub-examples-14.0.5 (c (n "jsonrpc-pubsub-examples") (v "14.0.5") (d (list (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "jsonrpc-ipc-server") (r "^14.0") (d #t) (k 0)) (d (n "jsonrpc-pubsub") (r "^14.0") (d #t) (k 0)) (d (n "jsonrpc-ws-server") (r "^14.0") (d #t) (k 0)))) (h "0rbkxc9z8xp3yyzndn81lhmcw1s563smqk52f5x3fspx9bsw4wg0")))

