(define-module (crates-io js on json_derive) #:use-module (crates-io))

(define-public crate-json_derive-0.0.1-alpha1 (c (n "json_derive") (v "0.0.1-alpha1") (d (list (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (d #t) (k 0)))) (h "1dwvl8f7mzcz9if0wg0w5kh8ryf8gfri3iw9bca2lijmjbs50f2c")))

