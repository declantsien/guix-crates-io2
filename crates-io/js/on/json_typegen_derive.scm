(define-module (crates-io js on json_typegen_derive) #:use-module (crates-io))

(define-public crate-json_typegen_derive-0.1.0 (c (n "json_typegen_derive") (v "0.1.0") (d (list (d (n "json_typegen_shared") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0b1vlzs7kr01xv5zb9z0kqc0iya3g4y8brdbm1rr3k9f98lzjdi5") (y #t)))

(define-public crate-json_typegen_derive-0.2.0 (c (n "json_typegen_derive") (v "0.2.0") (d (list (d (n "json_typegen_shared") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1c8xdskg3b08gyqbmkqdxdv03dy5j6y6bxmjj5vqhqpby71hiypd") (y #t)))

