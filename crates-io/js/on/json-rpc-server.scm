(define-module (crates-io js on json-rpc-server) #:use-module (crates-io))

(define-public crate-json-rpc-server-0.1.0 (c (n "json-rpc-server") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (f (quote ("client" "http1" "http2" "tcp" "server"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1qizlir5mvv6zba296jwib62z7iplbypn1iz441lskcv5n2r6np0")))

