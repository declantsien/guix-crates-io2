(define-module (crates-io js on json-event-parser) #:use-module (crates-io))

(define-public crate-json-event-parser-0.1.0 (c (n "json-event-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1yn1qi6gq64fh0b70dzi5f0liw9i4a9jbgc3xvjavfqkwxpaqv6v")))

(define-public crate-json-event-parser-0.1.1 (c (n "json-event-parser") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0xr9wbbp5cxci5bgmajs25l7yhkin0fg0j5vp764mdxf9ri2xw9j")))

(define-public crate-json-event-parser-0.2.0-alpha.1 (c (n "json-event-parser") (v "0.2.0-alpha.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "18ii3r0p1glwr2cjm2xa3z7nbp5692qk41lvbwfqzvkknc8sv8i0") (s 2) (e (quote (("async-tokio" "dep:tokio")))) (r "1.70")))

(define-public crate-json-event-parser-0.2.0-alpha.2 (c (n "json-event-parser") (v "0.2.0-alpha.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0ik3sii9wxrlk9zyfay1sf5frp22mlhhvcvs26g7d6cy6w1dspav") (s 2) (e (quote (("async-tokio" "dep:tokio")))) (r "1.70")))

(define-public crate-json-event-parser-0.2.0 (c (n "json-event-parser") (v "0.2.0") (d (list (d (n "clap") (r "^4") (d #t) (k 2)) (d (n "codspeed-criterion-compat") (r "^2.3.3") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0cg6ppqcsdqq9iwdvwz8plwdmbikbavhrn5rx9qcvflylzygll7q") (s 2) (e (quote (("async-tokio" "dep:tokio")))) (r "1.70")))

