(define-module (crates-io js on jsonpatch_rs) #:use-module (crates-io))

(define-public crate-jsonpatch_rs-0.1.0 (c (n "jsonpatch_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "jsonptr") (r "^0.1.0") (d #t) (k 0) (p "jsonptr_rs")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "081s6jnnwdghkgrmk0pq1r7ra6fai75yaprbzv60ffs5q9xg2ax6")))

