(define-module (crates-io js on json-type-derive) #:use-module (crates-io))

(define-public crate-json-type-derive-0.1.0 (c (n "json-type-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "0q58nxw7ga906v120pvi17i8q5phg1s3f5g5kqvgdcmjcj7ldhdg")))

