(define-module (crates-io js on json_keyquotes_convert) #:use-module (crates-io))

(define-public crate-json_keyquotes_convert-0.1.0 (c (n "json_keyquotes_convert") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1pxc5d1l408rv89n4fch97ghqg330634sp4nz79a5ahrf32qzvbb")))

(define-public crate-json_keyquotes_convert-0.1.1 (c (n "json_keyquotes_convert") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1paz3lll0g8f7hfk5jjn5m0dvyayc0wixxvp9krb4715jc9h5afv")))

(define-public crate-json_keyquotes_convert-0.2.0 (c (n "json_keyquotes_convert") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1w5yba9hpm0c7qg48qcfxmlrbrb98xmncmg8nfz8n4fzi1pgvmag")))

(define-public crate-json_keyquotes_convert-0.2.1 (c (n "json_keyquotes_convert") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0lici3msf2kgsy48xj2dlixxa50sr2h17zv4c5jad9zw8g5nik3s")))

(define-public crate-json_keyquotes_convert-0.2.2 (c (n "json_keyquotes_convert") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0kdiaw9qqjx3si5fg70vys13hkq9l779fk31qsrbvpkdpajvqnp9")))

(define-public crate-json_keyquotes_convert-0.2.3 (c (n "json_keyquotes_convert") (v "0.2.3") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0z1s00yiqx12bbn0n46ylhy4nxm18min3fdlkc1v8hkx4gs79xf6")))

