(define-module (crates-io js on json-rpc-types) #:use-module (crates-io))

(define-public crate-json-rpc-types-1.0.0-beta.1 (c (n "json-rpc-types") (v "1.0.0-beta.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1d29061wssdj2j0n14k69l8z7d6a9ls7v03vpwnkkri617hqv8fn")))

(define-public crate-json-rpc-types-1.0.0-beta.2 (c (n "json-rpc-types") (v "1.0.0-beta.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^1") (f (quote ("serde"))) (d #t) (k 0)))) (h "19diikqnhaz9hhg4j8ln9grjdb1gli6ibkzzgj606kygj8f8kgrk")))

(define-public crate-json-rpc-types-1.0.0-beta.3 (c (n "json-rpc-types") (v "1.0.0-beta.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^1") (f (quote ("serde"))) (d #t) (k 0)))) (h "110fn1gw343fy0mdlnfr9d5j4nvnz7rnkxxv5sax22nb62m3jhr4")))

(define-public crate-json-rpc-types-1.0.0-beta.4 (c (n "json-rpc-types") (v "1.0.0-beta.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0zh9pada7iizn4j17vfi9z727k57h5ihixh90ifp578m1qdc4w1i")))

(define-public crate-json-rpc-types-1.0.0 (c (n "json-rpc-types") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0gk3f67zz27w9bppb7f38pncx9han0sg7pvv38s20j00prvrdmjd") (y #t)))

(define-public crate-json-rpc-types-1.0.1 (c (n "json-rpc-types") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1zbydbnhdc4kva1mk848j142icrdzmg56rajcxijd66r1n1sxfvz")))

(define-public crate-json-rpc-types-1.0.2 (c (n "json-rpc-types") (v "1.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ysd1jahz5a58qs53jjznamcnxfr7km72a7slbc3rfpgcd1hv2gk")))

(define-public crate-json-rpc-types-1.0.3 (c (n "json-rpc-types") (v "1.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0zgp5ch0i7zlximb4vipr9lygk7cg10i79fg25754j2f9hq58gsl")))

(define-public crate-json-rpc-types-1.1.0 (c (n "json-rpc-types") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0pq0rnxsknxvsf8nshih64mvk7f7vrmgr1800mkz9f58m5fmvf18")))

(define-public crate-json-rpc-types-1.2.0 (c (n "json-rpc-types") (v "1.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1k9yfxzbld95lw7xwn2v7m0amx9mfwmdbdvnzpvxn5xg2wsx9fg4")))

(define-public crate-json-rpc-types-1.3.0 (c (n "json-rpc-types") (v "1.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^3") (f (quote ("serde"))) (d #t) (k 0)))) (h "066279jx34wvlqa88h8skwhpw51hg261l0jxmjklih8c5nl4sj3s")))

(define-public crate-json-rpc-types-1.3.1 (c (n "json-rpc-types") (v "1.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^3") (f (quote ("serde"))) (d #t) (k 0)))) (h "0s452wwky6hfhx969kyn5sz89i7lbzarjggnrylsj9jy2g13gir1")))

(define-public crate-json-rpc-types-1.3.2 (c (n "json-rpc-types") (v "1.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1y3fd7936bbrinamvhp74hl798nxnk4n3lir530z5ainq1vsz5p6")))

(define-public crate-json-rpc-types-1.3.3 (c (n "json-rpc-types") (v "1.3.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde-json-core") (r "^0.5") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1adb720kzynkjfbdljsr8i0fw57aw32nzai9c2r45wifwzr91pcw") (f (quote (("id-str-only") ("id-number-only"))))))

(define-public crate-json-rpc-types-1.3.4 (c (n "json-rpc-types") (v "1.3.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde-json-core") (r "^0.5") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "str-buf") (r "^3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1fgrvr5mrafnswjk1krk42hw355yrvvxijrranjri4q0h2jszc1r") (f (quote (("id-str-only") ("id-number-only"))))))

