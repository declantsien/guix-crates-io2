(define-module (crates-io js on jsonpick) #:use-module (crates-io))

(define-public crate-jsonpick-0.1.0 (c (n "jsonpick") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "11rfrivcw35r6m1y3czf356hp2mh21jsvpi6452x16m8ygvhwrbg")))

