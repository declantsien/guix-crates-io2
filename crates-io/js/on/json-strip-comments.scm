(define-module (crates-io js on json-strip-comments) #:use-module (crates-io))

(define-public crate-json-strip-comments-1.0.0 (c (n "json-strip-comments") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "1p9x7skh7ifxf5f4hpnk4pcg79sf1dygcnxx0qiq8b76wsxw0hml")))

(define-public crate-json-strip-comments-1.0.1 (c (n "json-strip-comments") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "1q8fjhp92x5hrkhs9h5kfm4infgli7yv1alvp4dzvg3v64r8a4va")))

(define-public crate-json-strip-comments-1.0.2 (c (n "json-strip-comments") (v "1.0.2") (d (list (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)))) (h "1vh4ks7wj78l3bbx0smsglklr8jvhadwarz4c44gij17jdwjklf3")))

