(define-module (crates-io js on json-pointer-simd) #:use-module (crates-io))

(define-public crate-json-pointer-simd-0.1.0 (c (n "json-pointer-simd") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "simd-json") (r "^0.13.4") (d #t) (k 0)))) (h "1zid32gd4xsqri9q0hz1pl8gdagbf8dm1p9mnkkkq28wf7blfs5a")))

(define-public crate-json-pointer-simd-0.2.0 (c (n "json-pointer-simd") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.4") (d #t) (k 0)))) (h "17vmp3w43psa7nn2lxgk9869wkkmm0r5vbwa6kfwy2dfidcg0jlp")))

(define-public crate-json-pointer-simd-0.3.0 (c (n "json-pointer-simd") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.4") (d #t) (k 0)))) (h "05qc3zzzfabspq1aqigs65g0c49alc31chx32sb7ahn9yjh4mhy2")))

(define-public crate-json-pointer-simd-0.3.1 (c (n "json-pointer-simd") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.4") (d #t) (k 0)))) (h "1vs0pgzvh98sja1a92fy21hf9bllsy2wg55vs99mc5axqadp93gr")))

