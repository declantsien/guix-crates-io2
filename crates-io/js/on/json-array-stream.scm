(define-module (crates-io js on json-array-stream) #:use-module (crates-io))

(define-public crate-json-array-stream-0.1.0 (c (n "json-array-stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "0kqvz31sqmliz9ssmzha9isnz8rnzycan0crx00mrcscdy3lxl22")))

