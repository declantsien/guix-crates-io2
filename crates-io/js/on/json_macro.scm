(define-module (crates-io js on json_macro) #:use-module (crates-io))

(define-public crate-json_macro-0.1.0 (c (n "json_macro") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "1rfi9ki9x0zxcn654l6zm44yif4i9hwl3d9xjiylbd1qnh2fwg8b")))

(define-public crate-json_macro-0.1.1 (c (n "json_macro") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)))) (h "1hd0wf7dmjrrdia5g6n3r67vvi6lx956wdhygagsf7261fhjis6a")))

