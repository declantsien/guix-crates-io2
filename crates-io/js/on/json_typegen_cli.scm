(define-module (crates-io js on json_typegen_cli) #:use-module (crates-io))

(define-public crate-json_typegen_cli-0.1.0 (c (n "json_typegen_cli") (v "0.1.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.7") (d #t) (k 0)))) (h "1x459lsbmpwyx9sxy9rsy8lflrlyg6s4v45323229ic00mzp6636")))

(define-public crate-json_typegen_cli-0.2.0 (c (n "json_typegen_cli") (v "0.2.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.2") (d #t) (k 0)))) (h "09rig7acyhsfcwmb4fiksd5w8g9iry5gpb8cxngydmqwgp4m2y68")))

(define-public crate-json_typegen_cli-0.3.0 (c (n "json_typegen_cli") (v "0.3.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)))) (h "04bwr37crasfpgm1myms5xjgrb1mb7jq8yr63pdlyvaqpzp6v2fg")))

(define-public crate-json_typegen_cli-0.3.1 (c (n "json_typegen_cli") (v "0.3.1") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)))) (h "1gn9w6zzjgxlz6k3pf4ag9q8labjpi2kgw0p2drw77ybbm9rr056")))

(define-public crate-json_typegen_cli-0.3.2 (c (n "json_typegen_cli") (v "0.3.2") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)))) (h "1ihfhyk729mwyw91mxs4z25kmwgwm6fjvj0n4xg0gjgl2wybgmz7")))

(define-public crate-json_typegen_cli-0.3.3 (c (n "json_typegen_cli") (v "0.3.3") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)))) (h "0wh3329695kx6mdfl61lrc5n29m8y9xl5yd0xiiay7grhaqxqj63")))

(define-public crate-json_typegen_cli-0.4.0 (c (n "json_typegen_cli") (v "0.4.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.4") (d #t) (k 0)))) (h "17cl4nc70jh1dlpjjyvgn1q7inl3v1pvdk3ag02zbssd8fmagb76")))

(define-public crate-json_typegen_cli-0.4.1 (c (n "json_typegen_cli") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.4") (d #t) (k 0)))) (h "1j8j4yykswqva8ch7sd488by2gsscazvpgfw1sjidd4izphwih2j")))

(define-public crate-json_typegen_cli-0.5.0 (c (n "json_typegen_cli") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.5") (d #t) (k 0)))) (h "0bq7qp98cyks3yl4ybi9hcys35s8clxyh95aipj6swmycgbiva5v")))

(define-public crate-json_typegen_cli-0.6.0 (c (n "json_typegen_cli") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.6") (d #t) (k 0)))) (h "1brlra28khc8f7k6wzqw3xcsy9mnagn7hbqm82nql6hsxzy4qyzv")))

(define-public crate-json_typegen_cli-0.7.0 (c (n "json_typegen_cli") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "json_typegen_shared") (r "^0.7") (d #t) (k 0)))) (h "1wzf6845r7n7q3i0n0k1wf7fpmrdi92yjn3n0wgvw9ppfbq63c89")))

