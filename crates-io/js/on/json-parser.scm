(define-module (crates-io js on json-parser) #:use-module (crates-io))

(define-public crate-json-parser-1.0.0 (c (n "json-parser") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02zryj0an6xf86v58aalhg0p0nsxmyfxci484avlakgafnnlprz4")))

(define-public crate-json-parser-1.0.1 (c (n "json-parser") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hj6ix4sdqcq4kn70w0p55r7r9z8fclz7dxmc1ipjvifh8ky5xnc")))

(define-public crate-json-parser-1.0.2 (c (n "json-parser") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a4z8xwcqhxyxk3c3llsha9w5i9bs5amc4v7s53a4as681skl97i")))

