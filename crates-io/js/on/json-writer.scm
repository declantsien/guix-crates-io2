(define-module (crates-io js on json-writer) #:use-module (crates-io))

(define-public crate-json-writer-0.2.0 (c (n "json-writer") (v "0.2.0") (d (list (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "ryu") (r "^1.0.9") (d #t) (k 0)))) (h "1jq7zyiw4dqj5vgak05fj2glygfcfhzkd472mcajiv0d3vd3yqjr")))

(define-public crate-json-writer-0.2.1 (c (n "json-writer") (v "0.2.1") (d (list (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "ryu") (r "^1.0.9") (d #t) (k 0)))) (h "08b84yv8340plalg4bnwkj9vvakihry5zysld5jawr9bqvnz2waw")))

(define-public crate-json-writer-0.3.0 (c (n "json-writer") (v "0.3.0") (d (list (d (n "itoa") (r "^1.0.6") (d #t) (k 0)) (d (n "ryu") (r "^1.0.13") (d #t) (k 0)))) (h "08wddy3bvgw38nhrgzzjvpkif3whrn0qkdp70cfdg5clkv9s6sm4")))

