(define-module (crates-io js on json-feed-model) #:use-module (crates-io))

(define-public crate-json-feed-model-0.1.0 (c (n "json-feed-model") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (k 0)))) (h "0s0hxdx9408ddqi75z3mdnw0zcxfvpnxrvvwfbjkpq3ir9qv9l9q") (f (quote (("std" "serde/std" "serde_json/std") ("default" "std") ("alloc" "serde/alloc" "serde_json/alloc")))) (y #t)))

(define-public crate-json-feed-model-0.1.1 (c (n "json-feed-model") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (k 0)))) (h "1v9i6nv06vsikhlx1mnw0fddxc3c6cvchg450s4rvpx4rgasy0vz") (f (quote (("std" "serde/std" "serde_json/std") ("default" "std") ("alloc" "serde/alloc" "serde_json/alloc")))) (y #t)))

(define-public crate-json-feed-model-0.2.0 (c (n "json-feed-model") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (k 0)))) (h "0ywyjncm2qaaawh954hysjm1j5ifb0wh1w9s50z3nxmkw5nx6fvj") (f (quote (("std" "serde/std" "serde_json/std") ("default" "std") ("alloc" "serde/alloc" "serde_json/alloc"))))))

