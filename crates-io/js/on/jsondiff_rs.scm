(define-module (crates-io js on jsondiff_rs) #:use-module (crates-io))

(define-public crate-jsondiff_rs-0.1.0 (c (n "jsondiff_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "197lx0j43wq03cwg16vpifz1raicdc118ny59rn8n2pxqx92cvx9")))

