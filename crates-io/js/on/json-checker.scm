(define-module (crates-io js on json-checker) #:use-module (crates-io))

(define-public crate-json-checker-0.1.0 (c (n "json-checker") (v "0.1.0") (d (list (d (n "cc") (r ">= 1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "0ikxbagss11mg45l5cil07qqcmzvbbrg8gi62bixqnnz36bgrfk6")))

(define-public crate-json-checker-0.1.1 (c (n "json-checker") (v "0.1.1") (d (list (d (n "cc") (r ">= 1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "1nqvp1cbzypvndbcyqh2ba9w0sg2168a3jnj28a408s1l5jznzhn")))

