(define-module (crates-io js on jsonp) #:use-module (crates-io))

(define-public crate-jsonp-1.0.0 (c (n "jsonp") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1zdjhdaqksj7hizklkx4vhrhsr9aa5g4icnhycmq6hx1a95w3bv8")))

(define-public crate-jsonp-1.0.1 (c (n "jsonp") (v "1.0.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1dk1wzkacfffyhmd47p0kb5pjfrzz0q1adcnyz9wss3zzh4fnzjl")))

(define-public crate-jsonp-1.0.2 (c (n "jsonp") (v "1.0.2") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0ji92cq6z5hfqj02agw5xr8qd1cg6hlqvmfvd8gw1a9npcm2j2rf")))

