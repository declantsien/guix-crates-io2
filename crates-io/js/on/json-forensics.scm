(define-module (crates-io js on json-forensics) #:use-module (crates-io))

(define-public crate-json-forensics-0.1.1 (c (n "json-forensics") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.82") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (o #t) (d #t) (k 0)))) (h "1kn5k5p1rrrmp8a9i6f99wmzxw3icdqacw13im049r0814l5ci37") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

