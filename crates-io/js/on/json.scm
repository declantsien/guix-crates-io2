(define-module (crates-io js on json) #:use-module (crates-io))

(define-public crate-json-0.1.0 (c (n "json") (v "0.1.0") (h "1i7w8yr4f1iydaj84g46bgmjcmy9liya5sd0i1w194mw9pn223kd")))

(define-public crate-json-0.1.1 (c (n "json") (v "0.1.1") (h "1dx76gvkn84zrrrbicllv6pwkw7yapkw5j2ywiwz9g0d1nsg852n")))

(define-public crate-json-0.2.0 (c (n "json") (v "0.2.0") (h "13q6f9d0bjnmrw4zaccx91m21l6rldmv0v3c0cs18mc0kx399wsz")))

(define-public crate-json-0.2.1 (c (n "json") (v "0.2.1") (h "0wpr9dicgwvlgr03zlhi84671md4bcal2hpcsfs7i5z73nz0lpff")))

(define-public crate-json-0.3.0 (c (n "json") (v "0.3.0") (h "0rj9ly55j739fryc0wn507ydg7wh6fm3kbxqdwjrchvggaaqsxsp")))

(define-public crate-json-0.3.1 (c (n "json") (v "0.3.1") (h "0l3rhdkj5v0v885ak9hwqsdnmyxaz0fk5mpwlaqqcw2x9hv1qb1c")))

(define-public crate-json-0.3.2 (c (n "json") (v "0.3.2") (h "0gcgfr9b90wlin9h5wyqkpgg3ggqpb3r6q8v29382gbip7an0jdk")))

(define-public crate-json-0.3.3 (c (n "json") (v "0.3.3") (h "1jq39q8v9mr53agvim93kdddm7n5c1v9xbx4knwfpyb6klw2icl0")))

(define-public crate-json-0.3.4 (c (n "json") (v "0.3.4") (h "0gdqcsd0jf9b761b1sgacy6m65cb1zrx5yavqcrqvrhjapjf4pnw")))

(define-public crate-json-0.4.0 (c (n "json") (v "0.4.0") (h "100hip2jg89r7mgfpnjd1q261qhlm9llz36ymipbmnhd5vcir4f8")))

(define-public crate-json-0.5.0 (c (n "json") (v "0.5.0") (h "0k4n65wp819k0xg17s2brgq6di3sqdpir4hslgjff5lr1xsvxa5c")))

(define-public crate-json-0.5.1 (c (n "json") (v "0.5.1") (h "08mqdm4brpw3j3qvag7hggznd42a9v8q1xhzbx3579qx8m0q8sxw")))

(define-public crate-json-0.6.0 (c (n "json") (v "0.6.0") (h "15ibw2mrns2cm02gcmknw2afwksg66ra3rz9d2zwhqjk35zsmnp7")))

(define-public crate-json-0.6.1 (c (n "json") (v "0.6.1") (h "11an563ya8g6hs6ci089s7j2lnxqy3pix4a1ingh7v0p5vng3ack")))

(define-public crate-json-0.7.0 (c (n "json") (v "0.7.0") (h "1ifq715dmqgjsiaa2wrsgm2s5z12z5h5501ryb5sfbs8b94rdzpw")))

(define-public crate-json-0.7.1 (c (n "json") (v "0.7.1") (h "1bfkj9myckdi8ra3630hpshli4rvanqwd33l2bs91wzwr6jjvkzj")))

(define-public crate-json-0.7.2 (c (n "json") (v "0.7.2") (h "0398hb5pcq63vag5frzvgmn19nm8khscd6pp23dmi94736xkwb79")))

(define-public crate-json-0.7.3 (c (n "json") (v "0.7.3") (h "0gnxj2zmavnkp25m99g2rhn1syv3rbf0ghfzvxk27nwh88dp1cwn")))

(define-public crate-json-0.7.4 (c (n "json") (v "0.7.4") (h "0lkgxdp00r0x3zgihhfrjgn5vcih5sfxm9w2nqbx03c4lflfv1ac")))

(define-public crate-json-0.8.0 (c (n "json") (v "0.8.0") (h "0kfbhpgfz6zafv62wmbx2zr0ghq5gha3afq5bh8gyb9k4qh38yfi")))

(define-public crate-json-0.8.1 (c (n "json") (v "0.8.1") (d (list (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "0spmn3lja50igbvsgxyvp3h3l291k5425rm71mrjdj9nra9ha88j")))

(define-public crate-json-0.8.2 (c (n "json") (v "0.8.2") (d (list (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "0baqdi950kf0vkzmrn6b3vys74h66k86lwxwgya53g8f4dnhd75q") (y #t)))

(define-public crate-json-0.8.4 (c (n "json") (v "0.8.4") (d (list (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "10d1rjq1swpxwwrpic37bi5qsmbmvr5yd6j40yf0rzjl541rwks5")))

(define-public crate-json-0.8.5 (c (n "json") (v "0.8.5") (d (list (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "14bfh2afas1vb05wm6p3ngj1h9w83dvf6l56c2d4v7lgj0mwvryj")))

(define-public crate-json-0.8.6 (c (n "json") (v "0.8.6") (d (list (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "0rcggvdi4gid9dh3d39f5xicza06i5913wqs54d9dncn7bc9y1km")))

(define-public crate-json-0.8.7 (c (n "json") (v "0.8.7") (d (list (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "0bdkxh9ipymhaz265p3s8r8l77v1da5cdy58652nwcm6272ifwrl")))

(define-public crate-json-0.8.8 (c (n "json") (v "0.8.8") (d (list (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "0nlgm2pgz4q699b6zc20h2ia5p2na25xbz5s6myar24pd42gr5r0")))

(define-public crate-json-0.9.0 (c (n "json") (v "0.9.0") (d (list (d (n "ftoa") (r "^0.1") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "0kdr7p9v8c4d8xbaak1lrl4d7dm6a4lqc2sifcgjv3a7b9ab1fp9")))

(define-public crate-json-0.9.1 (c (n "json") (v "0.9.1") (d (list (d (n "ftoa") (r "^0.1") (d #t) (k 0)) (d (n "itoa") (r "^0.1") (d #t) (k 0)))) (h "012al59j8jvi1892cgdfmmw45nw589f2avx0lynhw2s8g1shyw59")))

(define-public crate-json-0.10.0 (c (n "json") (v "0.10.0") (h "1x75136j575j0r8j5i1skxjvc9rjqjmz8h224lgm4hzq38lj6x2c")))

(define-public crate-json-0.10.1 (c (n "json") (v "0.10.1") (h "0pk3gdr5j5irih2qr5fz15ihg2v7s4l82lhrk8876bh0yg695gg6")))

(define-public crate-json-0.10.2 (c (n "json") (v "0.10.2") (h "162n8xjfzhp9khdmfih0pb1w70gx7b8ah4h45kmypil6harkjbbp")))

(define-public crate-json-0.10.3 (c (n "json") (v "0.10.3") (h "0p0n03b6kvx0c9j43vw1icrwvq7svr9qpgmx8dzqzhyvq084byzk")))

(define-public crate-json-0.11.0 (c (n "json") (v "0.11.0") (h "0wlwvw52s7ph8l37npfsn9r6ryn9cci1bxngfk52mkv7f511qnz3")))

(define-public crate-json-0.11.1 (c (n "json") (v "0.11.1") (h "1578qnsdgf1g9d4ghv0k5fp9h40pynfndsbl9lpj56lshxnc0jkl")))

(define-public crate-json-0.11.2 (c (n "json") (v "0.11.2") (h "038j19my7cwf5vgi555ggax9ramf6i7n8pyi8f5cbpxa59jn4zbp")))

(define-public crate-json-0.11.3 (c (n "json") (v "0.11.3") (h "1bxj84q6qjfkjdciczqq6gwcqdfkq5423fi7zkygf9rqs4z20jh9")))

(define-public crate-json-0.11.4 (c (n "json") (v "0.11.4") (h "11bwhb49rgxhv9rnk5dq5ka0hfwjb64kyp93bh56ks17vjfwfr4b")))

(define-public crate-json-0.11.5 (c (n "json") (v "0.11.5") (h "0ws65bhck0yvlqqpk72panfdp5gyxxgjbqm6ffa8yjbkwy2v4zjy")))

(define-public crate-json-0.11.6 (c (n "json") (v "0.11.6") (h "19zvrbxd9lgmgrl5fhk2gb0ssq6fimmvcdpv2dicq6xpnf5hwq17")))

(define-public crate-json-0.11.7 (c (n "json") (v "0.11.7") (h "1wrdfrbhl5rckrp9h7zrf35ljjx08zpv1p3nsyqlswc6z2hxky73")))

(define-public crate-json-0.11.8 (c (n "json") (v "0.11.8") (h "12s2hbs3rd78ppx3dwfq00qmwy2nxx30fkdl9p6qlsg5w8p49dqc")))

(define-public crate-json-0.11.9 (c (n "json") (v "0.11.9") (h "1r637c1dhzg7nn4l6xz47wg52qab7dyn74w8azqcg466280b5c62")))

(define-public crate-json-0.11.10 (c (n "json") (v "0.11.10") (h "1cgrq13jlbsngf8a4l6cdx95rzbkngh62awip04q4qg4wwnc0gfl")))

(define-public crate-json-0.11.11 (c (n "json") (v "0.11.11") (h "0k3crrqv0h925nqhkpknkr6nq1kxawgbi8x90pmcjcbwm6wncfnb")))

(define-public crate-json-0.11.12 (c (n "json") (v "0.11.12") (h "10z4l9kllxjgpl5p7qp3i5452r7z0ih49dj26953mvkpr7xg1srr")))

(define-public crate-json-0.11.13 (c (n "json") (v "0.11.13") (h "1glhqf8nzqnrfd9d1bhd60c68ddcssrd8h1swp64apqm0ia4il4s")))

(define-public crate-json-0.11.14 (c (n "json") (v "0.11.14") (h "1hj8c6xj5c2aqqszi8naaflmcdbya1i9byyjrq4iybxjb4q91mq1")))

(define-public crate-json-0.11.15 (c (n "json") (v "0.11.15") (h "1rg9jxf6bpbwis3ixd5ak8rp37him7n4z8awz4ssrxl6hyplbhlj")))

(define-public crate-json-0.12.0 (c (n "json") (v "0.12.0") (h "1hfa5iv2mqwk8g0qdjpsgl8hlpayprilx61a69b5sqdppsml3jmk")))

(define-public crate-json-0.12.1 (c (n "json") (v "0.12.1") (h "1ghx2gjfbgqg5lgy5xc0y7fkb2g2i8l12ri4zchqcvqj50d6cf4s")))

(define-public crate-json-0.12.2 (c (n "json") (v "0.12.2") (h "0qfwgs18v2cp8qa3r4nl0mlchcfjkkjva3s4ffry2wf2d29ckjsk")))

(define-public crate-json-0.12.3 (c (n "json") (v "0.12.3") (h "13xv5939sddsicbwk7fzd35dmwr95igkks7rrrjkz321qxbcwpwj") (y #t)))

(define-public crate-json-0.12.4 (c (n "json") (v "0.12.4") (h "1z9vybaaz3xq95bwmyln2ijmn4dmzj61xlz06jsc9dnzmxg2i3h7")))

