(define-module (crates-io js on jsonrpc-ws-client) #:use-module (crates-io))

(define-public crate-jsonrpc-ws-client-11.0.0 (c (n "jsonrpc-ws-client") (v "11.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^11.0") (d #t) (k 0)) (d (n "jsonrpc-core-client") (r "^11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "websocket") (r "^0.22") (d #t) (k 0)))) (h "04p72nfbq27my6lj46yd51gz5k7w5ix4i9xi5007p5ylmmg8r4sv")))

