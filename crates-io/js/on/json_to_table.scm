(define-module (crates-io js on json_to_table) #:use-module (crates-io))

(define-public crate-json_to_table-0.1.0 (c (n "json_to_table") (v "0.1.0") (d (list (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "tabled") (r "^0.9.0") (d #t) (k 0)))) (h "0milkyw388larhkd3lhl4w3lr2h9zk0yq43cya81wscnmdc7vscg") (f (quote (("color" "tabled/color"))))))

(define-public crate-json_to_table-0.2.0 (c (n "json_to_table") (v "0.2.0") (d (list (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)))) (h "06v6ilrp1436w4x4kcfhd1b9zksks2y30byg5s8akxqk4vj0k3s0") (f (quote (("color" "tabled/color"))))))

(define-public crate-json_to_table-0.3.0 (c (n "json_to_table") (v "0.3.0") (d (list (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)))) (h "0f6190vw9md63qv637fqfcxn9fw0im547c28lczm30qn9s32ld42") (f (quote (("color" "tabled/color"))))))

(define-public crate-json_to_table-0.3.1 (c (n "json_to_table") (v "0.3.1") (d (list (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)))) (h "0mgff77ch63p8qvczqzpb171cj4ii1xgv00calzpgcxfbx8k7gm0") (f (quote (("color" "tabled/color"))))))

(define-public crate-json_to_table-0.4.0 (c (n "json_to_table") (v "0.4.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0.11") (f (quote ("macros"))) (k 0)))) (h "0zm1z7qxl5zaygwsg3fq7xr1x3qx72m85cwzxlpdi10z40k3lqg9") (f (quote (("color" "tabled/color"))))))

(define-public crate-json_to_table-0.5.0 (c (n "json_to_table") (v "0.5.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0.12") (f (quote ("macros"))) (k 0)))) (h "0vmhn9wwmg0wsjz1yy7ziadbadqyrm0kgc71w42dqzmc1dsy6cz2") (f (quote (("color" "tabled/color"))))))

(define-public crate-json_to_table-0.6.0 (c (n "json_to_table") (v "0.6.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0.12") (f (quote ("macros"))) (k 0)))) (h "18c3p22scmbbkkw1kyzr8lnyzln2xi1z1n5awqf6xv9qarj13j9r") (f (quote (("color" "tabled/color"))))))

(define-public crate-json_to_table-0.7.0 (c (n "json_to_table") (v "0.7.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0.15") (f (quote ("std"))) (k 0)))) (h "06wj8cwhfp22r8iq5vhz4q1fn4x1kpksxzij21cn4f5ngiacg838") (f (quote (("macros" "tabled/macros") ("derive" "tabled/derive") ("ansi" "tabled/ansi"))))))

