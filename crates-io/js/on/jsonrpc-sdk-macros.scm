(define-module (crates-io js on jsonrpc-sdk-macros) #:use-module (crates-io))

(define-public crate-jsonrpc-sdk-macros-0.1.0 (c (n "jsonrpc-sdk-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dnkz8q65ix69xwvjg34svvijka1qcgrdbp7lsz9181gggy07xcx")))

(define-public crate-jsonrpc-sdk-macros-0.1.1 (c (n "jsonrpc-sdk-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y5jjd2igfdylfnwf4rgv70a4x366azz6mn8svc6zfb9nxlwh1b8")))

(define-public crate-jsonrpc-sdk-macros-0.1.2 (c (n "jsonrpc-sdk-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s13gnz7rbnkyfmysm7lagkzh84vmrl78npx4hafp0p1qlg6zsp9")))

(define-public crate-jsonrpc-sdk-macros-0.1.3 (c (n "jsonrpc-sdk-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15nig2l7i0xr7xqa0hnc8gp8vq56kxmwd98anqpwb0gf7x73f9xl")))

(define-public crate-jsonrpc-sdk-macros-0.1.4 (c (n "jsonrpc-sdk-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aggbrwy4nzwqsxcas94g8wcz2x9x34np0sp1micvn5vpym476h0")))

