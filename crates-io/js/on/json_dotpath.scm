(define-module (crates-io js on json_dotpath) #:use-module (crates-io))

(define-public crate-json_dotpath-0.1.0 (c (n "json_dotpath") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0vxc01c01m3fyssw6b66ap2q1dbbs42ck6j1s1r9p3cqph40dp86")))

(define-public crate-json_dotpath-0.1.1 (c (n "json_dotpath") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1smiz7aql9vmacry43cs53jhs5hx0yirc2jg47jy5nijs1yxlnis")))

(define-public crate-json_dotpath-0.1.2 (c (n "json_dotpath") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zd4zdhavl7m9af5fkyjsbpsai6mb700nzdgh4ji78h0gwa9lzgg")))

(define-public crate-json_dotpath-1.0.0 (c (n "json_dotpath") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nzp1a5aixkbf1fs00pydygm23palw3l7dyx6mfipa6kndyfrm0x")))

(define-public crate-json_dotpath-1.0.1 (c (n "json_dotpath") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fgi8fsab76kr6hxrf0h4cf3wvgzc124a9dk5q0dj1624ayi8az6")))

(define-public crate-json_dotpath-1.0.2 (c (n "json_dotpath") (v "1.0.2") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r3vw1l6fmfccch1lhv0913dnhn1g0va2ic722s5x2zpvdwdxrxv")))

(define-public crate-json_dotpath-1.0.3 (c (n "json_dotpath") (v "1.0.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rvv319iw19dsywmi01kji5wb9kj66p8w5q3s04shh5549n7x41y")))

(define-public crate-json_dotpath-1.1.0 (c (n "json_dotpath") (v "1.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06px5sirj507nb0cwv4k01d1ygayg6p1791dyv7g14amrzrzxp6v")))

