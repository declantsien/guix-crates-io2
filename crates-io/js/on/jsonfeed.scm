(define-module (crates-io js on jsonfeed) #:use-module (crates-io))

(define-public crate-jsonfeed-0.1.0 (c (n "jsonfeed") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0xsc7dnqbjr5dlvr3m8b2xii5pnnsxw7rk3mr095kp8aa7a1dzp0")))

(define-public crate-jsonfeed-0.1.1 (c (n "jsonfeed") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1nk41vr228zlwb8fac66vpfcwca8nnildh014nkj0xssing32nwn")))

(define-public crate-jsonfeed-0.1.2 (c (n "jsonfeed") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1832kln638yczyrw47fnn60svaq8icmbrd240fbf6x40i0w2m5mi")))

(define-public crate-jsonfeed-0.1.3 (c (n "jsonfeed") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1av5gmxdjmm77yy6wwpgdrpzjv1jpwkbyfz8lgqkgfy06a75rpkf")))

(define-public crate-jsonfeed-0.2.0 (c (n "jsonfeed") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dxbdf6nqjw85hlnwvd4sbf5sv59vbc9d4sd921si2iz6y4q5clp")))

