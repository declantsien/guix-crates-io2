(define-module (crates-io js on jsonvalid) #:use-module (crates-io))

(define-public crate-jsonvalid-0.1.0 (c (n "jsonvalid") (v "0.1.0") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "1vzn9ph7v6yf6nplpxw1j7rlg787hrv66845nl15zf5i8x766zad")))

(define-public crate-jsonvalid-0.1.1 (c (n "jsonvalid") (v "0.1.1") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.7.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.2.0") (d #t) (k 2)))) (h "159b3bvq0cisvgja8ghwqnlsvwckavpgbffbbpsw8pria8np018n")))

