(define-module (crates-io js on json5-to-json) #:use-module (crates-io))

(define-public crate-json5-to-json-0.1.0 (c (n "json5-to-json") (v "0.1.0") (d (list (d (n "json5") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ibl446mri67ixp75cz830am76aw7kw5jz5qbjz6mn90hpqgg6s2")))

(define-public crate-json5-to-json-0.1.1 (c (n "json5-to-json") (v "0.1.1") (d (list (d (n "json5") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1pa81cmhdmc7azprgh7sjdnyah47jq35xxq8v41qfgc3cdg3aica")))

(define-public crate-json5-to-json-0.1.2 (c (n "json5-to-json") (v "0.1.2") (d (list (d (n "json5") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y0z08qaplb1anjxv2hrxsnclqgnvjd5n31k23vchd1852fdwvf5")))

