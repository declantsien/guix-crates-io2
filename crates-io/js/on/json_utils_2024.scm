(define-module (crates-io js on json_utils_2024) #:use-module (crates-io))

(define-public crate-json_utils_2024-0.1.0 (c (n "json_utils_2024") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1f90waywll7d9zm6rk1m4ijza4sjrv3ckhfsszpbhhkjamsz5yar")))

(define-public crate-json_utils_2024-0.2.0 (c (n "json_utils_2024") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1d0pn4s0nc73c2xg2fcysipjmigq9y4yk8r1ci5djnyjmadi4n6l")))

