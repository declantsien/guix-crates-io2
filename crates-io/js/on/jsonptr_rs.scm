(define-module (crates-io js on jsonptr_rs) #:use-module (crates-io))

(define-public crate-jsonptr_rs-0.1.0 (c (n "jsonptr_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rhs1jsw2f5r3f3xg9d24xclpqhmgcjgdv9ys5v8k2b7q3ldz1r8")))

