(define-module (crates-io js on jsonfmt) #:use-module (crates-io))

(define-public crate-jsonfmt-0.1.0 (c (n "jsonfmt") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "09z0ciq4y32k5mn3ci4lpx9bd36nm9x947hwsavmj18l3nalzc9s")))

(define-public crate-jsonfmt-0.1.1 (c (n "jsonfmt") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0i767iz35syydxi13m70yk76i9b1a7rmkx9qv5p3y4sq7w0iy1hx")))

(define-public crate-jsonfmt-0.2.0 (c (n "jsonfmt") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "091hkd4z1z69nygab2hjcp98dap7327lcngwfn9pmqx71rm2mb8h")))

(define-public crate-jsonfmt-0.3.0 (c (n "jsonfmt") (v "0.3.0") (d (list (d (n "pico-args") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (f (quote ("preserve_order" "arbitrary_precision"))) (d #t) (k 0)))) (h "1dnmrcr2n5xs8kdhj9j9fq4di3vg3ai4j81g0kn2axxwh53hbipz")))

(define-public crate-jsonfmt-0.4.0 (c (n "jsonfmt") (v "0.4.0") (d (list (d (n "pico-args") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (f (quote ("preserve_order" "arbitrary_precision"))) (d #t) (k 0)))) (h "06rljx0wv6g1hzkf85vymnqmswh2lq1zqzg7877iycklrl6p16w7")))

(define-public crate-jsonfmt-0.4.1 (c (n "jsonfmt") (v "0.4.1") (d (list (d (n "pico-args") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (f (quote ("preserve_order" "arbitrary_precision"))) (d #t) (k 0)))) (h "11iczi4iyx802fvnsqzql4h0yg1jag8w76l2y4ajg5gypn97j3si")))

