(define-module (crates-io js on json-seq) #:use-module (crates-io))

(define-public crate-json-seq-0.1.0 (c (n "json-seq") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1jqpx43aw6fhgd0wsd1bfsxl0a3qaihfn6wz8bggi1fg1gshw4gk") (y #t)))

