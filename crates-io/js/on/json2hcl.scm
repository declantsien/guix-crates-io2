(define-module (crates-io js on json2hcl) #:use-module (crates-io))

(define-public crate-json2hcl-0.1.0 (c (n "json2hcl") (v "0.1.0") (d (list (d (n "hcl-rs") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pmj1g5g6gps7grwb4g6zwz1xm58yc07flmn5c9wxgs1wmd03n1l")))

