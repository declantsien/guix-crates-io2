(define-module (crates-io js on jsondata-derive) #:use-module (crates-io))

(define-public crate-jsondata-derive-0.1.0 (c (n "jsondata-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z61pyp4al7vf1hjnff6r5zcwkhvb6j18hj1sydjcs8ds67ml3n1")))

