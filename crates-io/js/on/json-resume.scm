(define-module (crates-io js on json-resume) #:use-module (crates-io))

(define-public crate-json-resume-0.1.0 (c (n "json-resume") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)) (d (n "serde_valid") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "04ak1vi3hh6swp12n597wffycbsrvxai75nqjxrdk22fa6a4yh67") (f (quote (("validate" "serde_valid") ("default" "validate"))))))

(define-public crate-json-resume-0.2.0 (c (n "json-resume") (v "0.2.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)) (d (n "serde_valid") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0rlz34wk151xgj4hykp6ln0r3fvc9jppk7brchmndzjp50ri9d2w") (f (quote (("validate" "serde_valid") ("side-projects") ("default" "validate" "side-projects"))))))

