(define-module (crates-io js on jsonrpc-client-core) #:use-module (crates-io))

(define-public crate-jsonrpc-client-core-0.1.0 (c (n "jsonrpc-client-core") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^7.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dyarf74yjdpdh6jnxgwmsz8g5msrbs9557m7hal3k9akvpxf71z")))

(define-public crate-jsonrpc-client-core-0.2.0 (c (n "jsonrpc-client-core") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^7.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h82pklm7kvq98gysc7szryn9vwbl3fyf1lc6m99nk3k7n54c89g")))

(define-public crate-jsonrpc-client-core-0.2.1 (c (n "jsonrpc-client-core") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^7.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11x5s94hcm5qjlap6wx0jkx6wvzwmwjkyxb9v469s0zaw7ddxfqs")))

(define-public crate-jsonrpc-client-core-0.3.0 (c (n "jsonrpc-client-core") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zw25wyzrdr64g6w2rk9jnhyzvaf2sam3d1214pcspkvglwvyhkm")))

(define-public crate-jsonrpc-client-core-0.4.0 (c (n "jsonrpc-client-core") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02g6nkp9llknvgh6mmwby6xvd7rw5ppgvfsnpqr6qchcgckiqzp7")))

(define-public crate-jsonrpc-client-core-0.5.0 (c (n "jsonrpc-client-core") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gn287r9s2yrw2siprv0n4hy2y96sbqzpc3zxq6gn83lhd4v577j")))

