(define-module (crates-io js on json_minimal) #:use-module (crates-io))

(define-public crate-json_minimal-0.0.0 (c (n "json_minimal") (v "0.0.0") (h "09kwvv5az56svzg2hqdycix42j59a3n1iv6zjwxjqqz1885hyh2k")))

(define-public crate-json_minimal-0.1.0 (c (n "json_minimal") (v "0.1.0") (h "1hrabgj65c4zm66y99ci2ilmbqkjdgjx2ix8zpwbcv04gdqg8zpr")))

(define-public crate-json_minimal-0.1.1 (c (n "json_minimal") (v "0.1.1") (h "040yq1j1d93hjj7rmgscxhyz70y81sl40m12a2pyzyyz4gp86vp2")))

(define-public crate-json_minimal-0.1.2 (c (n "json_minimal") (v "0.1.2") (h "1f2mnhngspxfqfczm6k0pb3yqxpwpckkwn2bl0gi8rhnm52sj8q4")))

(define-public crate-json_minimal-0.1.3 (c (n "json_minimal") (v "0.1.3") (h "13jd9cgdlga2ryfbgib88x8ip60npakhkac4s1jnvq5vqxaa28sz")))

