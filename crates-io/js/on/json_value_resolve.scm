(define-module (crates-io js on json_value_resolve) #:use-module (crates-io))

(define-public crate-json_value_resolve-0.1.0 (c (n "json_value_resolve") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i5i63wb7dbz67prhishg5r0g6qvywz1h9rfwqn9sa2g8vp7gf5d")))

(define-public crate-json_value_resolve-0.1.1 (c (n "json_value_resolve") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dn2wf48spqdygc5sz6rbcbwrsm84w4l6jnibaxvvam449pxvdw5")))

(define-public crate-json_value_resolve-0.1.2 (c (n "json_value_resolve") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08aaj0ww8cqwv854h2mfl6mqq37ql0dgbc1rih5y2wp9qcx02dki")))

(define-public crate-json_value_resolve-1.0.0 (c (n "json_value_resolve") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yvbjqkbvx5k02pd5siwlqzssjwhwg2v76z0zigydxlyaqkir9ls")))

(define-public crate-json_value_resolve-1.0.1 (c (n "json_value_resolve") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nmk3g2m463ha9dy3c40pflyblas0zp70zq83xjqpi4x7ng83prx")))

(define-public crate-json_value_resolve-1.0.2 (c (n "json_value_resolve") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11clns2pa05im6jf648f8s2y8lqy68i8ybgai4ma6s01wzqsywyn")))

(define-public crate-json_value_resolve-1.0.4-beta.5 (c (n "json_value_resolve") (v "1.0.4-beta.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h3nsdhbcwag674gqj8cm57ckzz5r6iwww2h710whq4ddbydc1g5")))

(define-public crate-json_value_resolve-1.0.4 (c (n "json_value_resolve") (v "1.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wwq8wr6kfjjx94wi7fxsxv5bzlcqi99gkhm02acr2l2sg0g1ahi")))

