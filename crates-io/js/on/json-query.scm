(define-module (crates-io js on json-query) #:use-module (crates-io))

(define-public crate-json-query-0.1.0 (c (n "json-query") (v "0.1.0") (d (list (d (n "jq-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0amvjqkyarmiyrcgwf0dzp3zjyw647vravgl0cws6zqjg3x7g8f2")))

(define-public crate-json-query-0.1.1 (c (n "json-query") (v "0.1.1") (d (list (d (n "jq-sys") (r "^0.1") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12nw10zh71x58v8z2nldlndr4cka3yfc9jxk5ywscx6pzqwsm7ga") (f (quote (("default" "bundled") ("bundled" "jq-sys/bundled"))))))

(define-public crate-json-query-0.2.0 (c (n "json-query") (v "0.2.0") (d (list (d (n "jq-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0929k83d91f0q1cdankznl038q18a445bssspz17hmmziwvp9dm9") (f (quote (("default") ("bundled" "jq-sys/bundled"))))))

(define-public crate-json-query-0.2.1 (c (n "json-query") (v "0.2.1") (d (list (d (n "jq-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1630l57nfa8iks57yqdnma3ww7d9h5qbadv18vpcym5n93zm6vpn") (f (quote (("default") ("bundled" "jq-sys/bundled"))))))

(define-public crate-json-query-0.3.0 (c (n "json-query") (v "0.3.0") (d (list (d (n "jq-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pd0hsy0g41zp3v0991a2424b7cgxmhf9slhv1zj4shws697mmf5") (f (quote (("default") ("bundled" "jq-sys/bundled"))))))

(define-public crate-json-query-0.3.1 (c (n "json-query") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "jq-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0assds7zg12mq6ly7v8m5fcw4lpl5mfcaca0rxibdq459iy69k0r") (f (quote (("default") ("bundled" "jq-sys/bundled"))))))

