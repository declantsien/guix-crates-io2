(define-module (crates-io js on json-split-aom) #:use-module (crates-io))

(define-public crate-json-split-aom-0.1.0 (c (n "json-split-aom") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1sbx07vjhfj5laqacpc58vnimwf848xk51zn0yrskpbcbrni96m1")))

