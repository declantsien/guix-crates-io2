(define-module (crates-io js on json-pest-parser) #:use-module (crates-io))

(define-public crate-json-pest-parser-0.1.0 (c (n "json-pest-parser") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "1wba9w4lz8cgprv8dx3w0w0p8wx7fcl0pr5baj9g8jix4vxl3qg6")))

(define-public crate-json-pest-parser-0.2.0 (c (n "json-pest-parser") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0zif9qryilamqzapch0rv5dl337dxlx6qfx0fa6pam1ynjw0bznz")))

(define-public crate-json-pest-parser-0.3.0 (c (n "json-pest-parser") (v "0.3.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "1vg85bzz0094a3pp6n5ll0fadjyqwayrg16m7g7a3vff5d5cii9v")))

