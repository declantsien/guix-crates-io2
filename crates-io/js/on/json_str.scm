(define-module (crates-io js on json_str) #:use-module (crates-io))

(define-public crate-json_str-0.2.0 (c (n "json_str") (v "0.2.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (f (quote ("with-serde"))) (k 0)) (d (n "serde") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "~0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "~0.7.0") (d #t) (k 0)))) (h "0wq72wp1yrb2g2m25p861gpbrjiy4pl53gbi0gra3llbyplnn4q1") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-json_str-0.2.1 (c (n "json_str") (v "0.2.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "json_macros") (r "^0.3") (f (quote ("with-serde"))) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "07jp666j7ziw0xixa36d5mk31c73hrwfsnkh2qd50qpxg43baw3k") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-json_str-0.2.2 (c (n "json_str") (v "0.2.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0igamikki2ddql5yxvcz26yrg4a1a1yn26l3vbpg9w6rs2nd2s41") (f (quote (("nightly-testing" "clippy"))))))

(define-public crate-json_str-0.2.3 (c (n "json_str") (v "0.2.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1qf2fykjhx3z9513n815j3qa7a8q26zma0121lzsccxpxky4crlk") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.3.0 (c (n "json_str") (v "0.3.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0460nmb4vfflnihia8d8wckpljg4f1wzdpzzlby9lpyry66bdhby") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.3.1 (c (n "json_str") (v "0.3.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1h0lhi8jslrlnbdvcnrna5k6560midh8fa273zabwhq8s8mbxdvl") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.3.2 (c (n "json_str") (v "0.3.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1g157m79aszh6650w0ryfk49jscr2h4s51a98y2fzdgzc5rdsxnh") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.3.3 (c (n "json_str") (v "0.3.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0w13y5jbhbw7fza5jc3f9r9k7y5fg1178rcx6rwxfc7ircj6qcmi") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.3.4 (c (n "json_str") (v "0.3.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0qdr2knjhlsla5sln3sqj1f6fvlhfydc97aaa9gy0bpw7l39kmkk") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.4.0 (c (n "json_str") (v "0.4.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0hs5g9harzi26lws81k35c2fi9aikqa7p31wx40gjnk9qnpaxds4") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.4.1 (c (n "json_str") (v "0.4.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0kch76vqi3n203nvrjz275yjxv7x391ngfdfsqqr9aszffpcbc2d") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.5.0 (c (n "json_str") (v "0.5.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0kgs3dvnbb0zp5ycpq3v9xs1qsxjxzkwcpq50sps751icb5kgnwj") (f (quote (("nightly-testing" "nightly" "clippy") ("nightly"))))))

(define-public crate-json_str-0.5.1 (c (n "json_str") (v "0.5.1") (h "079yxv5idgnjngxlksy6q8y6c0nmkds41lcs24kyf7vzbcf0fj21") (f (quote (("nightly"))))))

(define-public crate-json_str-0.5.2 (c (n "json_str") (v "0.5.2") (h "0rrkfdl8h2rmbwrqpmv3pn943gbbybj8zj1izd33q86xj32861zh") (f (quote (("nightly"))))))

