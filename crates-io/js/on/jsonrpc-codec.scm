(define-module (crates-io js on jsonrpc-codec) #:use-module (crates-io))

(define-public crate-jsonrpc-codec-0.1.0 (c (n "jsonrpc-codec") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "httparse") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "0hmblz9yrqaljawrn8czgjj8vy985hh2zr2x2iapf2kyy59imyv4")))

(define-public crate-jsonrpc-codec-0.1.1 (c (n "jsonrpc-codec") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "httparse") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "0kilz4mzv9k3l68xc08f018zj3k9x4hvzc8kpvxrnifr76k8naky")))

