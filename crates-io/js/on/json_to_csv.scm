(define-module (crates-io js on json_to_csv) #:use-module (crates-io))

(define-public crate-json_to_csv-0.1.0 (c (n "json_to_csv") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "08hx0klsr5drm6m6l6hncmg3i596y90vqr1jw9nq13jqwl538g5q")))

(define-public crate-json_to_csv-0.2.0 (c (n "json_to_csv") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1bqxq7wjbr89fgyff9yrzsmhssp2mfb54smnimrznhp3nwgjaj3w")))

