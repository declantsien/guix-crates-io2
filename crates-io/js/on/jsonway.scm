(define-module (crates-io js on jsonway) #:use-module (crates-io))

(define-public crate-jsonway-0.1.0 (c (n "jsonway") (v "0.1.0") (h "0pzlnqxr2yqnn2k2sh730w3bl39a5cfgcra1fww78907r1di7r74")))

(define-public crate-jsonway-0.1.1 (c (n "jsonway") (v "0.1.1") (h "1rxq4dnv7157xm220sm46ajnnfnq1l8gbxkpafjzn2qndwyakfws")))

(define-public crate-jsonway-0.1.2 (c (n "jsonway") (v "0.1.2") (h "1n76sw7ppjrvl10g41manq432bhnk79wadn3f926dbzxpk0fa6k5")))

(define-public crate-jsonway-0.1.3 (c (n "jsonway") (v "0.1.3") (h "0jmsyxiy2px88mcdzaypnnsay5lazkrrn8j4z7mjljvvm1mw9va5")))

(define-public crate-jsonway-0.1.4 (c (n "jsonway") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1szs12nhgnzrpiaxik2k6m1zz78ig5cd622b6llrjindwxp4qyz1")))

(define-public crate-jsonway-0.1.5 (c (n "jsonway") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "11d43gsg801kj8qnwwhm8ydv96nml9aw4zw5cfzrgyj58fdg097h")))

(define-public crate-jsonway-0.1.6 (c (n "jsonway") (v "0.1.6") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "02n8x6p8rlvma2ph0mjj7pnbwf3fv684sayikybrxns5yp9z9l65")))

(define-public crate-jsonway-0.1.7 (c (n "jsonway") (v "0.1.7") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "05bvq2c9q14f0v9z5k891dzzziaw8aqi168bz8h78jvmk4qj1mcy")))

(define-public crate-jsonway-0.1.8 (c (n "jsonway") (v "0.1.8") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0hnz6dq09dp6ady39g89krgivwscsn8sj6rbwwmmz49ybizdvfys")))

(define-public crate-jsonway-0.1.9 (c (n "jsonway") (v "0.1.9") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "17ncns87pvwj53p019q0p7nycrjp3q7mfyky6c9fp12iw62i2wm8")))

(define-public crate-jsonway-0.1.10 (c (n "jsonway") (v "0.1.10") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1xri6p9mfqyqs5qv4333h8pll3gmpjq2pynybf573g5c4mihwm78")))

(define-public crate-jsonway-0.1.11 (c (n "jsonway") (v "0.1.11") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "06jiy1v94vyw2z0jw3rwsz9pc89xi2g1nywqqbfa81ld3ipg53yf")))

(define-public crate-jsonway-0.2.0 (c (n "jsonway") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0wbx38ql34dhb86pc57bmj786nrlxy38wmw15j45h6vcbrx31lq2")))

(define-public crate-jsonway-0.2.1 (c (n "jsonway") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "16f8llcprra5mbm2ifnc45gffnijbhfa0sgpnfg6d3kxh1v8hn1l")))

(define-public crate-jsonway-0.3.0 (c (n "jsonway") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "16j0s2pih2lqw0q3hfxdacapyak4bajyilzz8xc3bxazzbsz0r5w")))

(define-public crate-jsonway-0.3.1 (c (n "jsonway") (v "0.3.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0mpcnw72gj37zqlmllij60hzw0y841jakzk08lydnwj2ddhzk7xc")))

(define-public crate-jsonway-0.3.2 (c (n "jsonway") (v "0.3.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1n72c88xhkkjy5a1gv0pg9cv464mayy0jy6j3hsarr40scvf1852")))

(define-public crate-jsonway-0.3.3 (c (n "jsonway") (v "0.3.3") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1k9da6va02grkbna9xv8q47gm95iz3jzzvi9zbn3kzjfz5w4mfkq")))

(define-public crate-jsonway-0.3.4 (c (n "jsonway") (v "0.3.4") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "10nfslsnvpspk4rff1110qacir5djrs3qf8g6m7akhafajypwzad")))

(define-public crate-jsonway-0.3.5 (c (n "jsonway") (v "0.3.5") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0s5rrcgpy0fd1h23lclplhm352l8ggmka746rssa3000q3yf2jff")))

(define-public crate-jsonway-1.0.0 (c (n "jsonway") (v "1.0.0") (d (list (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "0czypas66lwjdalkkmmbdi27jnykygki6gbal8hcxa7mrgwgq2nw")))

(define-public crate-jsonway-1.0.1 (c (n "jsonway") (v "1.0.1") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0q5a9prab554kinyy2krrbf4nz2m9avf23hgssdiznz3d61b6ibm")))

(define-public crate-jsonway-2.0.0 (c (n "jsonway") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pprwhjyc0g2l2xgjynv18729704n7w19lj9xyx5z41w8i4vgz7g")))

