(define-module (crates-io js on jsonrpc-core) #:use-module (crates-io))

(define-public crate-jsonrpc-core-0.1.0 (c (n "jsonrpc-core") (v "0.1.0") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.5") (d #t) (k 0)))) (h "1s7hw37fwpl7rhxjrrv45hpg8yzr4j4rgv0z39s553385ds7xanw")))

(define-public crate-jsonrpc-core-0.2.0 (c (n "jsonrpc-core") (v "0.2.0") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.5") (d #t) (k 0)))) (h "1x8s4hhw4jnxmbi16g7x1cs0arhxdv223afviwjy29bah9gpwj04")))

(define-public crate-jsonrpc-core-1.0.0 (c (n "jsonrpc-core") (v "1.0.0") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.10") (d #t) (k 0)))) (h "1qwadsxnn4dxlihpga122kmw2fv4db22xmaylkbc5r2gxcd735pj")))

(define-public crate-jsonrpc-core-1.0.1 (c (n "jsonrpc-core") (v "1.0.1") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.10") (d #t) (k 0)))) (h "1y4idhs133i8r7ny57ficlwqifqkqb9vhzvdh6llnwk1kjqb8q96")))

(define-public crate-jsonrpc-core-1.0.2 (c (n "jsonrpc-core") (v "1.0.2") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.10") (d #t) (k 0)))) (h "1l8faghqinsx15vl1wymkapzalj1jf7gd9gkhxsbw053lp8fbc53")))

(define-public crate-jsonrpc-core-1.1.0 (c (n "jsonrpc-core") (v "1.1.0") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.10") (d #t) (k 0)))) (h "0qmdaps2pdqvg8v0qahqp1a8zk1ybic96mmpylam1fq486jd7nx9")))

(define-public crate-jsonrpc-core-1.1.1 (c (n "jsonrpc-core") (v "1.1.1") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.10") (d #t) (k 0)))) (h "080bh3n3ksi6qfm1h96p7v0w0b7lr5k1r3kppbq6kw87m57n9khb")))

(define-public crate-jsonrpc-core-1.1.2 (c (n "jsonrpc-core") (v "1.1.2") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.13") (d #t) (k 0)))) (h "0yycm7l6kyy3vpq0z5igqqqv1qy2brv7yyf82x5rgc8pgzbjd7gx")))

(define-public crate-jsonrpc-core-1.1.3 (c (n "jsonrpc-core") (v "1.1.3") (d (list (d (n "serde") (r "^0.6.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6.13") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.28.0") (d #t) (k 1)))) (h "04j82ls063idh3dq9xq8mrm0mqjckba7k5k05m2vlwigsvvh5x4r") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-1.1.4 (c (n "jsonrpc-core") (v "1.1.4") (d (list (d (n "serde") (r "^0.6.14") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6.14") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.14") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.29.0") (d #t) (k 1)))) (h "1pf0v9vj8sak883kv8hp1l74daifvz1qkd6fi01a2qw6lzb2x9a5") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-1.2.0 (c (n "jsonrpc-core") (v "1.2.0") (d (list (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.29.0") (d #t) (k 1)))) (h "0gmkms83ya8gmmga3rjx879lq9zynzg60z7qdcr8widlbklwmx25") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.0.0 (c (n "jsonrpc-core") (v "2.0.0") (d (list (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.29.0") (d #t) (k 1)))) (h "17ar30i84rr378bxln09pb6rdi72fbgi7ddi4ab2qcz8rjdv5swy") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.0.1 (c (n "jsonrpc-core") (v "2.0.1") (d (list (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.30.0") (d #t) (k 1)))) (h "02zbrp9x9mj91a8d4vp0fj7gkk4wc172nmcz3hflrwbzw6wb9zjb") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.0.2 (c (n "jsonrpc-core") (v "2.0.2") (d (list (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.31.0") (d #t) (k 1)))) (h "0w0g6257spyl27nrrj30hh02inmvwallza358mcbi4y914m3mm47") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.0.4 (c (n "jsonrpc-core") (v "2.0.4") (d (list (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.32.0") (d #t) (k 1)))) (h "1z7z4gxh8n3ix7m8rqfys5azyawzbcl6wsy1jsmqcrc1frg6xdl8") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.0.5 (c (n "jsonrpc-core") (v "2.0.5") (d (list (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.33.0") (d #t) (k 1)))) (h "0d503kqgfnzbc4zpds4j81n44dmrv01sc0dr1sjw7krwxcxqmgsc") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.0.6 (c (n "jsonrpc-core") (v "2.0.6") (d (list (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.35.0") (d #t) (k 1)))) (h "0jnlxbzkv4x3nr0nkk2v6kna9nv3na4977swid59vn5fhdlxkav9") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.0.7 (c (n "jsonrpc-core") (v "2.0.7") (d (list (d (n "serde") (r "= 0.7.9") (d #t) (k 0)) (d (n "serde_codegen") (r "= 0.7.9") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "= 0.7.1") (d #t) (k 0)) (d (n "serde_macros") (r "= 0.7.9") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.33.0") (d #t) (k 1)))) (h "1k44pq1sdi0gm6y1simwhcwar4z7kqsqap06fqszf4q9j205cxci") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-2.1.1 (c (n "jsonrpc-core") (v "2.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.0") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1bgm7fi1kyycgrhn0mh3m47am6crnd71zlsxm8ya5391x3j7fi7c") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-3.0.0 (c (n "jsonrpc-core") (v "3.0.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1pmwhxgkrxhwdaa1g0acg7a8pfa6985rk2ysi643gfda174b64z9") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-3.0.1 (c (n "jsonrpc-core") (v "3.0.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1iadyg102adn2y7666r83f6w8xgnw6w2zp14vgz3q0jhw01xk2ws") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-3.0.2 (c (n "jsonrpc-core") (v "3.0.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0524iwh510vs3h64s7clljxivcxd5mrpp57kv8z8zwh71dhr8l1w") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-4.0.0 (c (n "jsonrpc-core") (v "4.0.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1lc0jsrqbzx4p2wvila6jp4dlab8xpfzbwlsbx9m066lq1972snm") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-5.0.0 (c (n "jsonrpc-core") (v "5.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "041hyhr93gq0363bcbdyyva7ykgwkb0z2qdrccqbnwisld087jlz") (f (quote (("reactor" "tokio-core") ("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-jsonrpc-core-6.0.0 (c (n "jsonrpc-core") (v "6.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0zsz84372v21wgdxn9y8c1n2x9bbb1gh0ldj038li79gb1drg1kv") (f (quote (("reactor" "tokio-core"))))))

(define-public crate-jsonrpc-core-7.0.0 (c (n "jsonrpc-core") (v "7.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cifxhnh6m392gcb4dzi2wpa05c251c31pd5qs9sm66nkcc3bay9")))

(define-public crate-jsonrpc-core-7.0.1 (c (n "jsonrpc-core") (v "7.0.1") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yjv39y1b15kcj2ka0i861izdasaf6k8nh7njzc4ygsdm1l2hqns")))

(define-public crate-jsonrpc-core-7.1.0 (c (n "jsonrpc-core") (v "7.1.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kzj79af671gvghsh1pza3w7q95ijy6q84kd8g0q6gazhkp5wglh")))

(define-public crate-jsonrpc-core-7.1.1 (c (n "jsonrpc-core") (v "7.1.1") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05fmphg9kk8bkzkbbzdx98djf6cv5f1kc3rps9k49aadjgwx1b5i")))

(define-public crate-jsonrpc-core-8.0.0 (c (n "jsonrpc-core") (v "8.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1chqx4bdww0fc1547x98xd5h32saird2c6idw092nn193drkgsja")))

(define-public crate-jsonrpc-core-8.0.1 (c (n "jsonrpc-core") (v "8.0.1") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g03r5mmzb0rz872ijanh02q69p5q792s26i4jj7k6g7yh23gy6x")))

(define-public crate-jsonrpc-core-9.0.0 (c (n "jsonrpc-core") (v "9.0.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19qi6nvjh9z8pqiyv1v9kv1jhxs3b20mxdhg760x60q863dbx72f")))

(define-public crate-jsonrpc-core-10.0.0 (c (n "jsonrpc-core") (v "10.0.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ckxzzib7ha6z21pp4a5i5d1hd77qlm8hc347cy4n7fwh4n1ybxn")))

(define-public crate-jsonrpc-core-10.0.1 (c (n "jsonrpc-core") (v "10.0.1") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1my5bzwnphl8vvbcnfn98a345i0v2bsfvcs1hgbdydd2zp1m4lbs")))

(define-public crate-jsonrpc-core-10.1.0 (c (n "jsonrpc-core") (v "10.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k6vfqckvjdz85vr81l7z7i3dl4mzxbsjh3lbyngbgmnz3syw5fw")))

(define-public crate-jsonrpc-core-11.0.0 (c (n "jsonrpc-core") (v "11.0.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10c5p8b6g7n8ifjii18c4krid9syg8p2y3r71n6i4602bvf3zf4p")))

(define-public crate-jsonrpc-core-12.0.0 (c (n "jsonrpc-core") (v "12.0.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hwibd7slc8cblgqhsmlxl0vfv0m829nn1w5whlhlw8kjxzwm398")))

(define-public crate-jsonrpc-core-12.1.0 (c (n "jsonrpc-core") (v "12.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1msvpr0dhigmrfn58w6prp7p9amqjwh6fwimqs4ksdzvjm6cy5h2")))

(define-public crate-jsonrpc-core-13.0.0 (c (n "jsonrpc-core") (v "13.0.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q6vjby0s6ch1zfc2y4mk3r6swj11gk9xbpng0v4xzl42n37klrl")))

(define-public crate-jsonrpc-core-12.2.0 (c (n "jsonrpc-core") (v "12.2.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13jxz037wlqp5raqwy6glycg6ydbg64pmk45jj8grlncimwn7pxq")))

(define-public crate-jsonrpc-core-13.1.0 (c (n "jsonrpc-core") (v "13.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05q2s3igf3k0a60qry3y74dq51adcnngnzlvw8754yahncg9ahnx")))

(define-public crate-jsonrpc-core-13.2.0 (c (n "jsonrpc-core") (v "13.2.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "038sljib8hy330x85d7bnc00g0p3khsrsj89lqc8drd7hg0ngmwi")))

(define-public crate-jsonrpc-core-14.0.0 (c (n "jsonrpc-core") (v "14.0.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0va2zlinzs1cmxljp3ihryi56nzprgkzymwmscpkz14xvx7l2d6g")))

(define-public crate-jsonrpc-core-14.0.1 (c (n "jsonrpc-core") (v "14.0.1") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00s0cljzs7ism6a76iqqzrbnw1mifgs0628n45mjx8a3isg2qfcv")))

(define-public crate-jsonrpc-core-14.0.2 (c (n "jsonrpc-core") (v "14.0.2") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jqf3r881pq09d2bp1b5zrv36g2xdv30nmhrpcq8ph3wmbhjg9sk")))

(define-public crate-jsonrpc-core-14.0.3 (c (n "jsonrpc-core") (v "14.0.3") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jj5k2c0sh7prigq0cc1gq5yv77sxj103vbhbv27qqqp6kgiwr9l")))

(define-public crate-jsonrpc-core-14.0.4 (c (n "jsonrpc-core") (v "14.0.4") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dgxz5kyrzyrknrqqvc4fafbc929mfazqmwl1hrv457lq0gm8g9m") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision")))) (y #t)))

(define-public crate-jsonrpc-core-14.0.5 (c (n "jsonrpc-core") (v "14.0.5") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w09vzrz762qzprvqy8wgpzwp4nb5rkjv7i2f985vvzi9236hfzy") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-14.1.0 (c (n "jsonrpc-core") (v "14.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v3zidsjjd5d8msl2644ljjjgxs71fdahrsipggb93rk09h5yli5") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-14.2.0 (c (n "jsonrpc-core") (v "14.2.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qkvgkr05sg0j25jqgw7zcw4r1agzg8gnfnrmw1rgyqz283p6x50") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-15.0.0 (c (n "jsonrpc-core") (v "15.0.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w4wwq9da84qrdvkjddfnbbs2sqy11qghv2vls48bm1ig9b142zk") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-15.1.0 (c (n "jsonrpc-core") (v "15.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ylig5lb8fkdlxqkw0r0fhjf8x07g64ka87chhy8kp1ykqvsci87") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-16.0.0 (c (n "jsonrpc-core") (v "16.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cwnwc6igpddx38qqdsbxynks0qr0ri9fgwl7119lgw4mk1w8iva") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-17.0.0 (c (n "jsonrpc-core") (v "17.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zrr6a1nxg8wdh9azvf9l9hylk0hjxa02yxkgrazymrj2d2rjmh7") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-17.1.0 (c (n "jsonrpc-core") (v "17.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pnr1wjz5804ldbnlliq0m0iglf4h3j941mxaagbcsd3vyv7linl") (f (quote (("default" "futures-executor" "futures") ("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-jsonrpc-core-18.0.0 (c (n "jsonrpc-core") (v "18.0.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sv5m6bxyscdqg8cfzlsm8f3vks3972zc9w475l4h19dxxmggxql") (f (quote (("default" "futures-executor" "futures") ("arbitrary_precision" "serde_json/arbitrary_precision"))))))

