(define-module (crates-io js on json-value) #:use-module (crates-io))

(define-public crate-json-value-0.1.0 (c (n "json-value") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "1whi170p92xq7w80sxpxa1qr5jsf66cz54ks0j71ji7lddlv6viw")))

(define-public crate-json-value-0.2.0 (c (n "json-value") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.74") (o #t) (d #t) (k 0)))) (h "03x3v6dabq6m5l95j4fi743il7lv6a277z6pf11k9ix4yb4y29x8") (f (quote (("default" "serde_json"))))))

(define-public crate-json-value-0.3.0 (c (n "json-value") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0.74") (o #t) (d #t) (k 0)))) (h "1mwphk124ckg3z240c08x47b9h9p97451qxy90cpifby5h7jn73s") (f (quote (("default" "serde_json"))))))

(define-public crate-json-value-0.3.1 (c (n "json-value") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1klvnivhx0558hnnmn1ggfcsbb4jr11m7ml94q2jl8kdjl19klik") (f (quote (("default"))))))

(define-public crate-json-value-0.3.2 (c (n "json-value") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1696ahxk21prw33xzzr7qzk3q7v5b6lbrfla41dbgi3mfq6aj0qm") (f (quote (("preserve_order" "serde_json/preserve_order") ("default" "preserve_order"))))))

(define-public crate-json-value-0.3.3 (c (n "json-value") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0hqix8ppadkzjxfj6495cn5b0jx7knlpyaslii9773308yp7q90a") (f (quote (("preserve_order" "serde_json/preserve_order") ("default" "preserve_order"))))))

