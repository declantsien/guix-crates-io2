(define-module (crates-io js on json2pdf-client) #:use-module (crates-io))

(define-public crate-json2pdf-client-0.1.0 (c (n "json2pdf-client") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pwnaxik3szrv0vnzl185al72w48iaf9xj63lwjnmw1469q8wqwy") (f (quote (("blocking" "reqwest/blocking"))))))

