(define-module (crates-io js on jsonbox) #:use-module (crates-io))

(define-public crate-jsonbox-0.0.1 (c (n "jsonbox") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.1.5") (d #t) (k 0)))) (h "1iqmagqn2hczlpxbslg6x4pi9mb926z1d2pq0048pj2faiy9pcfr")))

(define-public crate-jsonbox-0.0.2 (c (n "jsonbox") (v "0.0.2") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "mockito") (r "^0.20") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.1.5") (d #t) (k 0)))) (h "07wkizxwr48vd2z4m5sjadgrf00nwnszp8v1fxihnv0zcbw93q70")))

(define-public crate-jsonbox-0.0.3 (c (n "jsonbox") (v "0.0.3") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "mockito") (r "^0.20") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "1xqq4vkx9x4z30mpcbw2k3f618pp7xz0a70zn9vw8m34ppsi395q")))

(define-public crate-jsonbox-0.1.0 (c (n "jsonbox") (v "0.1.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "mockito") (r "^0.20") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "16666vx97hk3i7vqi3cqggqxw0z643vdriwjd483l79yxw0sn290")))

(define-public crate-jsonbox-0.1.1 (c (n "jsonbox") (v "0.1.1") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "mockito") (r "^0.20") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "0hgwki8cgjl39m409jy7hl1xmg2knjjyhidyb2var8v7dv285ng4")))

(define-public crate-jsonbox-0.1.2 (c (n "jsonbox") (v "0.1.2") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "mockito") (r "^0.20") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "17041lk9aw9l24spw4qjvyczc9qf2rdrbsa50q27dbmqqfp1b18f")))

(define-public crate-jsonbox-0.2.0 (c (n "jsonbox") (v "0.2.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "mockito") (r "^0.20") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)))) (h "0p8wkjqabvjk80szapmdr6n43qrg9wmq2yslqwj2qjc4hsw7f4ch")))

