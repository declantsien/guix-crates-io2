(define-module (crates-io js on json2arrow) #:use-module (crates-io))

(define-public crate-json2arrow-0.1.0 (c (n "json2arrow") (v "0.1.0") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05cazvgqwb5jgj7v8hiwjv2jcdkj38cv89nnj8lziyl3w9dw8q3c")))

(define-public crate-json2arrow-0.2.0 (c (n "json2arrow") (v "0.2.0") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p7fklp8lvgqsxdxhg35d3rx8a392fp6s9bh6537qnl07shfjyz7")))

(define-public crate-json2arrow-0.2.2 (c (n "json2arrow") (v "0.2.2") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "170gbvvl40v1qnag2g0bcvd7s31l8b5pgx0z4mxs1c2mfia7fz5r")))

(define-public crate-json2arrow-0.3.0 (c (n "json2arrow") (v "0.3.0") (d (list (d (n "arrow") (r "^4.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f6mmdb57xk52x5ma0bggmdv8s75zxjpnrji51wpzgpx15lyw0vl")))

(define-public crate-json2arrow-0.3.2 (c (n "json2arrow") (v "0.3.2") (d (list (d (n "arrow") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "168lfhfii9dwbyg0f3kw1qdsq86nhyivdmswlqs71plwv7ylgs12")))

(define-public crate-json2arrow-0.3.4 (c (n "json2arrow") (v "0.3.4") (d (list (d (n "arrow") (r "^9.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0783cjv1q5qqa4khf7d3qn0qrp3w40xn9638wl3d6abnlbgl6r41")))

(define-public crate-json2arrow-0.4.0 (c (n "json2arrow") (v "0.4.0") (d (list (d (n "arrow") (r "^13.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "10mjcppcrsy8cw1awj19n667hwz1p663ql19vjck7lqjdfvmpcp5")))

(define-public crate-json2arrow-0.4.1 (c (n "json2arrow") (v "0.4.1") (d (list (d (n "arrow") (r "^21.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0n0yrpnn33bcxnrx3x6lc0bnrhswmc6n6xdg46c0cxia04431rdf")))

(define-public crate-json2arrow-0.5.0 (c (n "json2arrow") (v "0.5.0") (d (list (d (n "arrow") (r "^23.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1yr799ad683pk3xgdfpb4gdw60lidbvnxpvbw2hrgzaz30b44ifi")))

(define-public crate-json2arrow-0.6.0 (c (n "json2arrow") (v "0.6.0") (d (list (d (n "arrow") (r "^23.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "16f36izgzg6arjprg3wj2mxjzwvl1ccg8awrkfy79h8g0xxn07yn")))

(define-public crate-json2arrow-0.6.1 (c (n "json2arrow") (v "0.6.1") (d (list (d (n "arrow") (r "^30.0.1") (d #t) (k 0)) (d (n "arrow-schema") (r "^30.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1ab7vjf0v8aw0yij40r08rxlk7gkf2zvvmn94r5zq1cgzsd9xz7a")))

(define-public crate-json2arrow-0.7.0 (c (n "json2arrow") (v "0.7.0") (d (list (d (n "arrow") (r "^32.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^32.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "12qbyabknhjqwxcql7jv4l4cw3i6l66509h5dldqwy9f2ngyhwa1")))

(define-public crate-json2arrow-0.7.2 (c (n "json2arrow") (v "0.7.2") (d (list (d (n "arrow") (r "^32.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^32.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1yqvvawc5vk16pdf4cs46r42r8177qbbsgc0lj6v1j273d77g3xv")))

(define-public crate-json2arrow-0.8.0 (c (n "json2arrow") (v "0.8.0") (d (list (d (n "arrow") (r "^34.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^34.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "11r0b3y6wsw2jbz7kzc07n70nvaa5714zpr4x08jb8h9i5ymi1sz")))

(define-public crate-json2arrow-0.8.1 (c (n "json2arrow") (v "0.8.1") (d (list (d (n "arrow") (r "^34.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^34.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "18ii6pfhg0f524y0i88nk6yyqxswqh146lvd0r7dz1rwmlkwfs02")))

(define-public crate-json2arrow-0.10.0 (c (n "json2arrow") (v "0.10.0") (d (list (d (n "arrow") (r "^36.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^36.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1zwql8nk59apzwlqi1vj0m182acg0n29ww1g0gyqzm8i6h6b4wcs")))

(define-public crate-json2arrow-0.11.0 (c (n "json2arrow") (v "0.11.0") (d (list (d (n "arrow") (r "^40.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^40.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "111j1rip8zzfjxphyqjaayik04qafyvy47r5w6fkwzi2757sa9ll")))

(define-public crate-json2arrow-0.12.0 (c (n "json2arrow") (v "0.12.0") (d (list (d (n "arrow") (r "^44.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^44.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1mxambsfhgxpk5jja1bbn17qzw727rdbaxnprhajfw1sajfkpxml")))

(define-public crate-json2arrow-0.12.1 (c (n "json2arrow") (v "0.12.1") (d (list (d (n "arrow") (r "^44.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^44.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1x9320d44aizfxvsn0w93cvlk09smyxnzy1n86mydsyfgi0qqgi2")))

(define-public crate-json2arrow-0.13.0 (c (n "json2arrow") (v "0.13.0") (d (list (d (n "arrow") (r "^46.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^46.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0nqj72hw4k6hqha8y96izp6rk3xiikj6jjs49iaa8n9wd0672yad")))

(define-public crate-json2arrow-0.14.0 (c (n "json2arrow") (v "0.14.0") (d (list (d (n "arrow") (r "^47.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^47.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1zdydckaxdzvav4ph8l10gxq1mbv76nckfd1glq0zh85g6zk5i40")))

(define-public crate-json2arrow-0.14.1 (c (n "json2arrow") (v "0.14.1") (d (list (d (n "arrow") (r "^47.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^47.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "09jms7yrph8nrhqwkp9hbv6fgmhckvvq7cwl0myqnvf2xwxnmalw")))

(define-public crate-json2arrow-0.17.1 (c (n "json2arrow") (v "0.17.1") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0rjk5lxxpx84nnyfly505y5h8gn8k7cikx1d38lymm84pm9ggsvh")))

(define-public crate-json2arrow-0.17.2 (c (n "json2arrow") (v "0.17.2") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "08jdpjc4nj5304g5r4gwsf9glwn1xj26g41rz9k7yfqpihf2pii1")))

(define-public crate-json2arrow-0.17.3 (c (n "json2arrow") (v "0.17.3") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "05sf7cmh7yv58r68w7ir3kma7iz5l4hpivgmkm2dqr46qv304rhp")))

(define-public crate-json2arrow-0.17.4 (c (n "json2arrow") (v "0.17.4") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1pkf7hj5rgrzwz75l7ij4rfrjfpqny3j40ppppd13qhzl65ghppv")))

(define-public crate-json2arrow-0.17.5 (c (n "json2arrow") (v "0.17.5") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1hm27yjrqbnf0qbbhngzyq10n5ccwl04vzfrpv8ldjnswfpd5542")))

(define-public crate-json2arrow-0.17.7 (c (n "json2arrow") (v "0.17.7") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.7") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0ibp7bxlawrzxw2avmg9zr09a6gifvsjnpbgbzr6k8ay6bz2vm49")))

(define-public crate-json2arrow-0.17.8 (c (n "json2arrow") (v "0.17.8") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.8") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0skza5y3cjp1a0mr4vav778ds2a09jwfy7f6akdb1qrz1ldsyr0p")))

(define-public crate-json2arrow-0.17.9 (c (n "json2arrow") (v "0.17.9") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.9") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1i4190l5z5g8m1y0prfrii39ykk568az7y5cs1n7dqzv9xcvwpq3")))

(define-public crate-json2arrow-0.17.10 (c (n "json2arrow") (v "0.17.10") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.10") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "15v2w3ah2imq3mg1lyw9anis4kbxwlsq14909x0q2v2f2q40nv07")))

(define-public crate-json2arrow-0.18.0 (c (n "json2arrow") (v "0.18.0") (d (list (d (n "arrow") (r "^51.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^51.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "02rdz13vgrb54y041ppija870dbv49k598w5wgzgdf56wg2gg7s2")))

