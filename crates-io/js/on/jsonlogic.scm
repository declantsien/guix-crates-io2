(define-module (crates-io js on jsonlogic) #:use-module (crates-io))

(define-public crate-jsonlogic-0.1.0 (c (n "jsonlogic") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dgzzbdh826ghbbnfdrh1jv3apl09y5lsv7pi35gxfb30mrf5a9y")))

(define-public crate-jsonlogic-0.1.1 (c (n "jsonlogic") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j5laaqs0g5kf793plggq97x8a2n68apsypp46krmmgcjz1pq5hx")))

(define-public crate-jsonlogic-0.1.2 (c (n "jsonlogic") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07nbxqdl65rykgvvc02k5mys446jdkfdk5aamvc3qiy28l5cs9vm")))

(define-public crate-jsonlogic-0.2.0 (c (n "jsonlogic") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pbfyk3bjr1wmylkzqka1ff2a8pf9x59jd94n5qm8h1lvffiqznv")))

(define-public crate-jsonlogic-0.3.0 (c (n "jsonlogic") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09nmirp5lc31x24ggh6kbv3yyb4lgh56jn5ac58p3abpbzhkdr75")))

(define-public crate-jsonlogic-0.4.0 (c (n "jsonlogic") (v "0.4.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18i2z30dism7gl0yk3s9x4z81s1i27nidxyld8hrxgzaisrqdipk")))

(define-public crate-jsonlogic-0.5.0 (c (n "jsonlogic") (v "0.5.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j4mp5l2wpp1n4a56p001fxdbwzazk0qja50biq24k2kgszq6byp")))

(define-public crate-jsonlogic-0.5.1 (c (n "jsonlogic") (v "0.5.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vsyljd7ni0d9a8nhazi75vj0kwmmqhswhixvmd6lc89c6f6kq7x")))

