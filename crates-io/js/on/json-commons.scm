(define-module (crates-io js on json-commons) #:use-module (crates-io))

(define-public crate-json-commons-0.1.0 (c (n "json-commons") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "14m3mdmkb2z113vpwnn7a2m3vfv2xn3x9hw1kj1dcp1kw9hvgj1v") (y #t)))

(define-public crate-json-commons-0.1.1 (c (n "json-commons") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0s9ldn92yc4yx9h6046n8a7wkdn1s81cjy0c2fyavc8hkssdvr7c")))

(define-public crate-json-commons-0.1.2 (c (n "json-commons") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "145dqra3k5z1p40ha2k964d0x4ic3ck7jzc797fg0sj414vgikj2") (y #t)))

(define-public crate-json-commons-0.1.3 (c (n "json-commons") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0nsmsghsb5pvg4q5gv69g0izjs0ddan2ch6mfgn5lvkdldlkj5x7")))

(define-public crate-json-commons-0.2.0 (c (n "json-commons") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0v1iff9bcfrl21d3dy75bvc1zz4vafvb2npm8k4rhhry4aji9k2j")))

(define-public crate-json-commons-0.3.0 (c (n "json-commons") (v "0.3.0") (d (list (d (n "bson") (r "^2.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1x5wxyv48mccgj3kavchgmvbkafhv8gnwbybsc0b4phy3prpjswy")))

