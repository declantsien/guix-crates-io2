(define-module (crates-io js on jsonrpc-base) #:use-module (crates-io))

(define-public crate-jsonrpc-base-0.1.0 (c (n "jsonrpc-base") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4"))) (o #t) (k 0)))) (h "0v6dz50h1n2jpzla5l1fdb43p1bkbsyx1514ay1ccv5zvhgjd9qn") (f (quote (("default" "uuid"))))))

(define-public crate-jsonrpc-base-0.2.0 (c (n "jsonrpc-base") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4"))) (o #t) (k 0)))) (h "05iv9d76pn81646v34mck5znmwk9h05bhbhhpg8wvy0cd00213qi") (f (quote (("std") ("default" "std" "uuid"))))))

