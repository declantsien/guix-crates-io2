(define-module (crates-io js on json-web-token) #:use-module (crates-io))

(define-public crate-json-web-token-0.1.0 (c (n "json-web-token") (v "0.1.0") (d (list (d (n "b64-url") (r "^0.2") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "09g5v1vqbnx8xpzi6z5hnfl2mgiqh91m89jnv1hkp5z2w6vryhq0")))

(define-public crate-json-web-token-0.2.0 (c (n "json-web-token") (v "0.2.0") (d (list (d (n "b64-url") (r "^0.5") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0ida7gn21qqcm5fhcfwzgrlfw3cpyf2vk8riwzhs2p8c4mgjnhgw")))

