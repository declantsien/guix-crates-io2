(define-module (crates-io js on json2file) #:use-module (crates-io))

(define-public crate-json2file-0.1.0 (c (n "json2file") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0nswv4dryrglcr4x5rq3xfibfxzg3rvhfc1nzxjsdib9d06qy4g6")))

(define-public crate-json2file-1.0.14 (c (n "json2file") (v "1.0.14") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1id2jmnrvcyxa9xv8506s53wg1g6j3kp56321wz4kwlfhj1kah9m")))

(define-public crate-json2file-1.0.15 (c (n "json2file") (v "1.0.15") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0i6jw6z54vy96wlawi1zmkjsld37kzy24v5s88016mdf6gp74l0b")))

(define-public crate-json2file-1.0.16 (c (n "json2file") (v "1.0.16") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1cjq03j9vmq46rg3v6gx136a1qi77w95pnryjm49dm9jyra74pav")))

(define-public crate-json2file-1.1.0 (c (n "json2file") (v "1.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0nhl56lahd1kd1g1a24qmxjld984mws6hfk9b534vafwzcdcdq3v")))

(define-public crate-json2file-1.2.1 (c (n "json2file") (v "1.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "0zzmjw4ls9446sn4w07zmmzvrbmiynp57gc3l8xx9sypjq1h8fhr")))

(define-public crate-json2file-1.3.0 (c (n "json2file") (v "1.3.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1xj0f4flp86b10liir4gmfs9r4v0p5lbqy4kpmkn852mhqzgdrqd")))

(define-public crate-json2file-1.4.0 (c (n "json2file") (v "1.4.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "11s5lzd1aci5ahfdich86xpwj5pryzyaci7i813w3264qyyjaziq")))

(define-public crate-json2file-1.4.1 (c (n "json2file") (v "1.4.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1wz0chk1kvjsidkzk71bk4s3jy4v5bd1qyyxlz8wf5dqnwlfx1i3")))

(define-public crate-json2file-1.4.2 (c (n "json2file") (v "1.4.2") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "0w5q88wsyj93x646q35fx8rr4ypnyhwqg8m6ig6j0p0lkx2p1xjh")))

(define-public crate-json2file-1.5.0 (c (n "json2file") (v "1.5.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1gkprv89savvsf091vgbiqyiv6cflzzhlbmnd5pfqw12kzc2gjss")))

(define-public crate-json2file-1.6.0 (c (n "json2file") (v "1.6.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.1") (d #t) (k 0)))) (h "1vlmj35isbplvv8nq424wlqrhp7ibxvk7nwf8267ggmkhjm6bvw9")))

(define-public crate-json2file-1.7.1 (c (n "json2file") (v "1.7.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.1") (d #t) (k 0)))) (h "1k07mwzy208575wa1gzzfa2p6q5chfam2jh0f4dhlm6q8qxvh073")))

(define-public crate-json2file-1.7.2 (c (n "json2file") (v "1.7.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.1") (d #t) (k 0)))) (h "1n9vyxz8hmdidgl9vwl73qqph33yg497s49cr0v2gj4yqvw54jp5")))

(define-public crate-json2file-1.7.3 (c (n "json2file") (v "1.7.3") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.1") (d #t) (k 0)))) (h "16cxlfd7s9v42v5h9agnfvs7v2pvjc3mslym02x0kvxl2ckxrx7d")))

(define-public crate-json2file-1.7.4 (c (n "json2file") (v "1.7.4") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.2") (d #t) (k 0)))) (h "118d90pldygigwci1in6ingh75p6rx5ay7lhf5nkznq6pbq725m3")))

(define-public crate-json2file-1.7.5 (c (n "json2file") (v "1.7.5") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.2") (d #t) (k 0)))) (h "0js45bnjmhxdslq6a5zagbishjp1i00pm6ha0xypnsw686kglpsl")))

(define-public crate-json2file-1.7.6 (c (n "json2file") (v "1.7.6") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.2") (d #t) (k 0)))) (h "0g05mhfdmss308w42grg6his7lm7g7k4cnvlmird2c9f8k8h6n89")))

(define-public crate-json2file-1.7.7 (c (n "json2file") (v "1.7.7") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.3") (d #t) (k 0)))) (h "1xf95ys8kpiz9xbidx5d6bnp1zc1g8zsin3fflybd6xcb87blypb")))

(define-public crate-json2file-1.7.8 (c (n "json2file") (v "1.7.8") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "unescaper") (r "^0.1.4") (d #t) (k 0)))) (h "0cymsb71m8m2xak4h8k48d5ddv057f1241hsn02cdz0q53spwsng")))

