(define-module (crates-io js on json-ext) #:use-module (crates-io))

(define-public crate-json-ext-0.1.0 (c (n "json-ext") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g4h2d77d45i3x96959sx9826dqifg6rmbmkcpxpwqq5q0l4qx3g")))

(define-public crate-json-ext-0.2.0 (c (n "json-ext") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1vdydyaf39qssd05d9x81i7icwg5bqn1i24cynz3vdyq5xrjik10")))

(define-public crate-json-ext-0.3.0 (c (n "json-ext") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0l1mrhl86nfvys23xx4hchv03yjx2bs5rd6bmprpc3yjqn7drfcn")))

