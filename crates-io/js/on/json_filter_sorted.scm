(define-module (crates-io js on json_filter_sorted) #:use-module (crates-io))

(define-public crate-json_filter_sorted-0.1.0 (c (n "json_filter_sorted") (v "0.1.0") (d (list (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x5sd070xgc6i60jb3gri6634699m546791sr97xmwa2lbby587p")))

(define-public crate-json_filter_sorted-0.1.1 (c (n "json_filter_sorted") (v "0.1.1") (d (list (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zrsnkrgfpw4ixrjgy7w63hlras6816xkp967z6c72p1gy0yny4v")))

