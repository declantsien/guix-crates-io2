(define-module (crates-io js on json-compare) #:use-module (crates-io))

(define-public crate-json-compare-0.0.1-pre (c (n "json-compare") (v "0.0.1-pre") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hk8q8jwyddjxx9y0d08w0byqjivj60pklchjv9yy3f2l861flrx")))

(define-public crate-json-compare-0.0.1 (c (n "json-compare") (v "0.0.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12q48sn37vdy68p1piyjkxlakrlal98ha3jgcqcd17rm2wdazmvf")))

