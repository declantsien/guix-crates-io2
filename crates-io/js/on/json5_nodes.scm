(define-module (crates-io js on json5_nodes) #:use-module (crates-io))

(define-public crate-json5_nodes-0.1.3 (c (n "json5_nodes") (v "0.1.3") (d (list (d (n "hashlink") (r "^0.7") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "173gd2kqjhbil8ijxqh3gn53d9m722q0z3z0kjp8wjaq6ks8wkmh")))

(define-public crate-json5_nodes-2.0.0 (c (n "json5_nodes") (v "2.0.0") (d (list (d (n "hashlink") (r "^0.7") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0l75vp3w6kyay3wm6ff5whszpf0k3ma1bw61fqgw1vv48s394q9p")))

(define-public crate-json5_nodes-2.0.1 (c (n "json5_nodes") (v "2.0.1") (d (list (d (n "hashlink") (r "^0.7") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0ixvcz3s7vh2w92mnrin6z15vj3hma1npdqy08dpxdalqkyipay6")))

