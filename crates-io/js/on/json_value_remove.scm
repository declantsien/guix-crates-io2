(define-module (crates-io js on json_value_remove) #:use-module (crates-io))

(define-public crate-json_value_remove-1.0.0 (c (n "json_value_remove") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14b8z14b25i18jlqp7wxq2jznnr9sxrll2709v0rlnnyfr3a4kam")))

(define-public crate-json_value_remove-1.0.1 (c (n "json_value_remove") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ca1x14h0mpig8fpw81vbgqfzb2dgy86w3f49c869cc494fz58bd")))

(define-public crate-json_value_remove-1.0.2-beta.1 (c (n "json_value_remove") (v "1.0.2-beta.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bzvp1rdb1wm5cjmkwxbyv2184h3r8av8a6jawrhhby7kjrash26")))

(define-public crate-json_value_remove-1.0.2 (c (n "json_value_remove") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "081x0z0hxrj6v9cgw3p8cikp5v2kjy28zd3d88kznxipkynidark")))

