(define-module (crates-io js on jsona_openapi_spec) #:use-module (crates-io))

(define-public crate-jsona_openapi_spec-0.1.0 (c (n "jsona_openapi_spec") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "12g227iriqyfvjfi484qzsrx0hhrdvr783i2iipfvfc5b4gc0k76") (y #t)))

(define-public crate-jsona_openapi_spec-0.1.2 (c (n "jsona_openapi_spec") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0r7dlns7kga1kln7gbaj3snvwxlhx2w399109lc5h5lz1a2fa4nc") (y #t)))

(define-public crate-jsona_openapi_spec-0.1.4 (c (n "jsona_openapi_spec") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0niniw5cv9v42w8f5ccyfbqp5vlhvnmxp0vncx4v08fbnml6qaxm") (y #t)))

(define-public crate-jsona_openapi_spec-0.1.6 (c (n "jsona_openapi_spec") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1xazd492yy2s94m52k3cq9fa14mca63l22p1idzw2gzpvgn2l9z5") (y #t)))

(define-public crate-jsona_openapi_spec-0.1.7 (c (n "jsona_openapi_spec") (v "0.1.7") (d (list (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kzaxbwk9fvfjakj7cxd9d8818h43zrh1zljq5z42xxy13w84lx6") (y #t)))

(define-public crate-jsona_openapi_spec-0.1.8 (c (n "jsona_openapi_spec") (v "0.1.8") (h "0phgwayqnhk8k4sdmw5a76zf0gpbz3q8ym4czb4bw4sg6q3n2ka8") (y #t)))

