(define-module (crates-io js on json-db) #:use-module (crates-io))

(define-public crate-json-db-0.1.0 (c (n "json-db") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r13riz5zfax0hvkd6vg4fq0cv9wxqkpy6276xhfg1k56a3vhf4m") (y #t)))

(define-public crate-json-db-0.1.1 (c (n "json-db") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nbpwfih3pi64b2d75a483fxsfqskhzjp2crq67ws0w1y68j0gni")))

(define-public crate-json-db-0.1.2 (c (n "json-db") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gfm4j78ncwmjwlfp58drw05qjn6ic6x1d8lv9cs5dkw08vdw31g")))

