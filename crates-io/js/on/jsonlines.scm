(define-module (crates-io js on jsonlines) #:use-module (crates-io))

(define-public crate-jsonlines-0.0.0 (c (n "jsonlines") (v "0.0.0") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "jsonl") (r "^4.0.1") (d #t) (k 0)))) (h "16lk49gm3jqkvgx3ia5slffrc2rwwl2zhwy2ad1qxnmcdrkf8734")))

(define-public crate-jsonlines-1.0.0 (c (n "jsonlines") (v "1.0.0") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0n3b778fxagyidg25gpsddiyy78qqj73zx9q8az8jb95k3swjwcs")))

