(define-module (crates-io js on jsonparser) #:use-module (crates-io))

(define-public crate-jsonparser-0.1.0 (c (n "jsonparser") (v "0.1.0") (h "1ab61sn8lxibicaxkhmrq5kq036z1hl9fcxld67g4g2ba7cqn5m8") (r "1.56")))

(define-public crate-jsonparser-0.1.1 (c (n "jsonparser") (v "0.1.1") (h "154hgs5k1w7mf3i2b2jbss7f39a1gky1i9jpqsplsf81vkjihxhc") (r "1.56")))

(define-public crate-jsonparser-0.1.2 (c (n "jsonparser") (v "0.1.2") (h "1mrp63bb660js01gmfjv194rfmk5nnc2m2adp44fi1h765hxyd0b") (r "1.56")))

(define-public crate-jsonparser-0.1.3 (c (n "jsonparser") (v "0.1.3") (h "1cmk8kl1l74v7mgfsnj36x4jz6cq20g8gg0cb8iwrqaw6w7rckng") (r "1.56")))

(define-public crate-jsonparser-0.1.4 (c (n "jsonparser") (v "0.1.4") (h "14vih29sxzxkwjmi4mjw872c1qzzj9g79yi6gyqdhxhj7ial1g3a") (r "1.56")))

(define-public crate-jsonparser-0.1.5 (c (n "jsonparser") (v "0.1.5") (h "03di1nim8cm4gp1iv07jaq7qsdqs7ny018q6lgvr8harrrjssvsa") (r "1.56")))

(define-public crate-jsonparser-0.1.6 (c (n "jsonparser") (v "0.1.6") (h "0x8l8spbbb4v38y4phg6m70c708fgzpgbd66n0v77qnnz98nhlr6") (r "1.56")))

(define-public crate-jsonparser-0.1.7 (c (n "jsonparser") (v "0.1.7") (h "1vycgss4ik8yj821f7l9a6lfv8fnmcl51pfkir4rar83q4w9pw41") (r "1.56")))

(define-public crate-jsonparser-0.1.8 (c (n "jsonparser") (v "0.1.8") (h "1pnc11c040zhwh2v0zfki3k9475693qf36p9xr93pcqbahnnpzc7") (r "1.56")))

(define-public crate-jsonparser-0.1.9 (c (n "jsonparser") (v "0.1.9") (h "09abxf5flgl56z23877k0gh8lwvzj9mw1gvasxdramyvsdczrly7") (r "1.56")))

(define-public crate-jsonparser-0.2.0 (c (n "jsonparser") (v "0.2.0") (h "1vqfsdialr5bdlqx7jyjr2k8xhdfjifs41m7sqc5ajpxadyac6qd") (r "1.56")))

(define-public crate-jsonparser-0.2.1 (c (n "jsonparser") (v "0.2.1") (h "1kzi1wdr1wdgkj74bnahv3h21bahpzhjxby6qk3blw377ajk8jlj") (r "1.56")))

