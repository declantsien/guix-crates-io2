(define-module (crates-io js on jsonnet-sys) #:use-module (crates-io))

(define-public crate-jsonnet-sys-0.5.0 (c (n "jsonnet-sys") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sr4zhg9flpxph6df08prk8jqryr3iwspl59q43nx0klfylrlcf6")))

(define-public crate-jsonnet-sys-0.6.0 (c (n "jsonnet-sys") (v "0.6.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vz64ylq7isylr0vksnadf7cvwvyn2s7585fdr3jhr0qiq9vyj19")))

(define-public crate-jsonnet-sys-0.17.0 (c (n "jsonnet-sys") (v "0.17.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pcs3mllz8fl9vpksjqybsyq6n51g3vgiw4j1l7z6d22yfypqd02") (l "jsonnet")))

