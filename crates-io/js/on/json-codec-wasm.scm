(define-module (crates-io js on json-codec-wasm) #:use-module (crates-io))

(define-public crate-json-codec-wasm-0.1.0 (c (n "json-codec-wasm") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "0qxf1jk4c1rfqdzx65l3fsg0xrmc0rs8qv1cq5zmxrfcajl445fd")))

