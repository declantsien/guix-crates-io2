(define-module (crates-io js on jsonit) #:use-module (crates-io))

(define-public crate-jsonit-0.1.0 (c (n "jsonit") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05iqzn5i4ngb6mhvw41hsl90sfks51sqzc0c2z5jh6gkv1kzjk58") (y #t)))

(define-public crate-jsonit-0.1.1 (c (n "jsonit") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06xacdahdizmdkc010fypzkl931qajqw7snqhxbzynlnvc174g50") (y #t)))

(define-public crate-jsonit-0.2.0 (c (n "jsonit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y490xy586pm7y8pdn0dbd3j6mvazwwl2wv8klz6rplls44763vy") (y #t)))

(define-public crate-jsonit-0.2.1 (c (n "jsonit") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p7piz9r8hcl29j4dr5qx4xmmbdmccvqii7yi4xyxrgh7ir37gva")))

(define-public crate-jsonit-0.2.2 (c (n "jsonit") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1myvgd9p9kyxrjf9jzqb007g6m5qi699k7jnr4g9gjlza77fz7wi")))

(define-public crate-jsonit-0.2.3 (c (n "jsonit") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sbzbirknrb99riill4bcxynbq484kqvvi7zbvnc7vya2qkj21bw")))

(define-public crate-jsonit-0.2.4 (c (n "jsonit") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h94rzy15by644fksgpz4hdhdhprc35awrjjzgdq6im53cbjx6ds")))

(define-public crate-jsonit-0.2.5 (c (n "jsonit") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11g95m9q4i4b0frynbb1rgkyg53fhwhq4gga7dfdd4xdrqzq5kf9")))

(define-public crate-jsonit-0.2.6 (c (n "jsonit") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fplmrwlj7vk7rrnpmkbqky64lxizja17phs845272xfgjhnjxjq")))

(define-public crate-jsonit-0.2.7 (c (n "jsonit") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08k3qmzs9f2wc8i35fs9ddqgcpkp9z7b8gw4pfnad4dafrks9fim")))

(define-public crate-jsonit-0.2.8 (c (n "jsonit") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01qwfdlh36vgrg9wmc9sqhzdgrwbd5341hwwcdifj91ihda4rw5q") (y #t)))

(define-public crate-jsonit-0.2.9 (c (n "jsonit") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a3r83pldnf15x5rxdc3qlv2ifjfyf4ypa7zf0nyl0a3zch2npsl")))

(define-public crate-jsonit-0.2.10 (c (n "jsonit") (v "0.2.10") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "fern") (r "^0.6.2") (f (quote ("colored"))) (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19l9nhb5g86mqib98cfc1z86q1ww096dvsl7vwazwn5s21pn9ajs")))

