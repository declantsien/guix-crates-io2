(define-module (crates-io js on json-generator) #:use-module (crates-io))

(define-public crate-json-generator-0.1.0 (c (n "json-generator") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "10jhbxckgpkxkhlpkvq52gynmgdi9rdzck52r3prakn07hp6liq5")))

(define-public crate-json-generator-0.1.1 (c (n "json-generator") (v "0.1.1") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0vhhvzy1f0h6l9fbxqaygfrsyaw2011i9ngsb5w1a59lia3vpy3k")))

(define-public crate-json-generator-0.1.2 (c (n "json-generator") (v "0.1.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0f2nssn05x9lh833r91nzpx90yi198falfjq442zgm442d603j53")))

(define-public crate-json-generator-0.1.3 (c (n "json-generator") (v "0.1.3") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1i65nra4nzbw2gh3af12g7rikn3rxhd42f4j1g6mxfrk3x58xzq1")))

(define-public crate-json-generator-0.1.4 (c (n "json-generator") (v "0.1.4") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1bdzf0vwqg2wbpms35z1g57yr4br19238cnhrzax2lvhy308ckdw")))

(define-public crate-json-generator-0.1.5 (c (n "json-generator") (v "0.1.5") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0a7c0s5yg8zql6yh1gdk25k8n5z788ifc2i1ka8zlm4lyxlhvq8l")))

(define-public crate-json-generator-0.1.6 (c (n "json-generator") (v "0.1.6") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "13vyqb23rp26nm559qa8cydzl6xrf9fhsyapdl3zhgmydqragwps")))

