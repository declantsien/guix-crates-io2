(define-module (crates-io js on jsonwebtoken-hs256) #:use-module (crates-io))

(define-public crate-jsonwebtoken-hs256-0.1.0 (c (n "jsonwebtoken-hs256") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nx8n8jfws917gb16yyc0nh08h3wx4fqrncliv1v7bnihmwvrb6h") (r "1.56.0")))

