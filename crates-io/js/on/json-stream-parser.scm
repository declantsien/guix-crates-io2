(define-module (crates-io js on json-stream-parser) #:use-module (crates-io))

(define-public crate-json-stream-parser-0.1.0 (c (n "json-stream-parser") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "073mywdvzfkhj3py23smb343n1kaj2dqy5h29kfyilkq0h79lbzq")))

(define-public crate-json-stream-parser-0.1.1 (c (n "json-stream-parser") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17wi6bznv21g17y4n9m8jgw1idl492k490kqvwwlq2khdjr3mkr7")))

(define-public crate-json-stream-parser-0.1.2 (c (n "json-stream-parser") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vk9jw1qghwqa80kcll7pymnqqc6i2cmy19x51jrwr0974y1f064")))

(define-public crate-json-stream-parser-0.1.3 (c (n "json-stream-parser") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0phvz8ddv7q0z2m1kivvwdaq5svmfkxwwbsgvscihgj6dkpfm8rn") (r "1.65.0")))

(define-public crate-json-stream-parser-0.1.4 (c (n "json-stream-parser") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ad628gjgb3cg98l2si845m062sn4hdz3z1989hf09z80lmsnw4a") (r "1.65.0")))

