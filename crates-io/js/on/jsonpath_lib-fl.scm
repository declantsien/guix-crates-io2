(define-module (crates-io js on jsonpath_lib-fl) #:use-module (crates-io))

(define-public crate-jsonpath_lib-fl-0.2.5 (c (n "jsonpath_lib-fl") (v "0.2.5") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0r76afyr3mms5qhnhhip2wwb5p0mq7p6x1fv3ygmgx71qaik64p8")))

(define-public crate-jsonpath_lib-fl-0.2.6 (c (n "jsonpath_lib-fl") (v "0.2.6") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1zqp1k10w82hbc1cp7bv876vmzb6i5wmpvgnpgqav4h9kx1m6di4")))

(define-public crate-jsonpath_lib-fl-0.3.6 (c (n "jsonpath_lib-fl") (v "0.3.6") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0frl70jnz93rzi7mbcvakmzswd054kagwvj3358rvvgy0wsydfjx")))

(define-public crate-jsonpath_lib-fl-0.3.7 (c (n "jsonpath_lib-fl") (v "0.3.7") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0zqhwl6pz6m7778y9frc4jbwrzzn1p7h3xhdixp3c98v4a0gkp1k")))

