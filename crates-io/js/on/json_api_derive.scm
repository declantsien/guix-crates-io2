(define-module (crates-io js on json_api_derive) #:use-module (crates-io))

(define-public crate-json_api_derive-0.1.0 (c (n "json_api_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16ip6iphs54fc1aksh9avyv479w6wnlypbg9wqbq4508vsav8322")))

