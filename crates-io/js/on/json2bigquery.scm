(define-module (crates-io js on json2bigquery) #:use-module (crates-io))

(define-public crate-json2bigquery-0.1.0 (c (n "json2bigquery") (v "0.1.0") (d (list (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_bigquery") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1k244ds041954g30jrs5xcs4n1nz85c7q7x7i4g7miijb4q77aib")))

(define-public crate-json2bigquery-0.1.1 (c (n "json2bigquery") (v "0.1.1") (d (list (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_bigquery") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0ajqghyx3rckhs3l8j4kfgkfdzi7xm946g0c5r7zng3rd51czpjp")))

