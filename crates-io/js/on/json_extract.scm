(define-module (crates-io js on json_extract) #:use-module (crates-io))

(define-public crate-json_extract-0.1.0 (c (n "json_extract") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1d7b8m9d5w6pppm6bx4z81k42k8f4sgk8894z11f7lwvjmf8qvnw") (y #t)))

(define-public crate-json_extract-0.1.1 (c (n "json_extract") (v "0.1.1") (d (list (d (n "json_extract") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1x4rjw5azxrmfvb128dkvdg68rb6nx4dpq2w78787fy2wk0ldh2q") (y #t)))

(define-public crate-json_extract-0.1.2 (c (n "json_extract") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1jkpxfn8w78b3dpw9ngg09knipd9sr5hniqvxijnlb79lqk0lz66") (y #t)))

(define-public crate-json_extract-0.1.3 (c (n "json_extract") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "14088y4afajvqwib8xicfzd7ppvwma36yi68rv5an8fcbl68w78a") (y #t)))

(define-public crate-json_extract-0.1.4 (c (n "json_extract") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0y3b5h6n0f090sr9b45b0khrbx3cg0yb4309kgjck3l7g5zslcj7")))

