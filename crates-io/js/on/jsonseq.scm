(define-module (crates-io js on jsonseq) #:use-module (crates-io))

(define-public crate-jsonseq-0.1.0 (c (n "jsonseq") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "11i128axm7y411w7d9ayyd7s3zndg9jxlc68jvxzbfbia0l3ihdz")))

(define-public crate-jsonseq-0.1.1 (c (n "jsonseq") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cyhph0rwcanm3ri2cfqfa1nj0hdbywji7bq1cgcbk8gmv08bhfb")))

