(define-module (crates-io js on json-tools) #:use-module (crates-io))

(define-public crate-json-tools-0.0.1 (c (n "json-tools") (v "0.0.1") (h "0smjx5s9z94hqjimlvmd1q70cvgwsmiaa4z2q5xaycksl16fqzsb")))

(define-public crate-json-tools-0.1.0 (c (n "json-tools") (v "0.1.0") (h "1a37xjcl5fma05b0akmidibrimg82fil9v6wvsdd9b5yjd1li0nf")))

(define-public crate-json-tools-0.1.1 (c (n "json-tools") (v "0.1.1") (h "1x38bhqs2l21mdcyc2ghblf5rqw5j8bz7ps3v7y5zi2nr5sssqgq")))

(define-public crate-json-tools-0.2.0 (c (n "json-tools") (v "0.2.0") (h "1j07f5mfpjk4g8q6pxmi0j9bd688v0q018x55w8mqh2d2cys9sc1")))

(define-public crate-json-tools-0.3.0 (c (n "json-tools") (v "0.3.0") (h "1fc1jwkccaaydzhiw4j6d2jf7hjm45l0zsn74yy07qsm6b96bjl6")))

(define-public crate-json-tools-1.0.0 (c (n "json-tools") (v "1.0.0") (h "09mm06qz45y2licw22ninm3d96vq4zd2xfrm2pwrky4h6n3y5v61")))

(define-public crate-json-tools-1.0.1 (c (n "json-tools") (v "1.0.1") (h "1p9pqx3c502mypga7xhk1gmbggpsb12s2qckqbf1ibbswjwyz59f")))

(define-public crate-json-tools-1.1.0 (c (n "json-tools") (v "1.1.0") (h "1pqgqjvsz6c9inackhy9k106qawjvnzn2wcj77c1954yrkkxrhb1")))

(define-public crate-json-tools-1.1.1 (c (n "json-tools") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0lyx7p80wn5si25zcsnfq2iyhq2y6dy4bfbxy5ar6bmxa1v1kmir")))

(define-public crate-json-tools-1.1.2 (c (n "json-tools") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1k25kfa3sxaw9knrk8a40q85j659xxa9z71m63kbcr0gic7xcgi3")))

