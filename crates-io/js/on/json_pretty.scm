(define-module (crates-io js on json_pretty) #:use-module (crates-io))

(define-public crate-json_pretty-0.1.0 (c (n "json_pretty") (v "0.1.0") (h "0xynm6fghn7ji8xcv5z80kjdmhvxqxszplww9fvihhz8mv8mg7cv")))

(define-public crate-json_pretty-0.1.1 (c (n "json_pretty") (v "0.1.1") (h "08d0cq5ld2acla31c9kwfkn4xxjxafdgydc2h4lmljva1wcj8r0k")))

(define-public crate-json_pretty-0.1.2 (c (n "json_pretty") (v "0.1.2") (h "0qm7qi3j90687sihd88ksa44fh2sllp3alg0wdh8djyjj3rf77ww")))

