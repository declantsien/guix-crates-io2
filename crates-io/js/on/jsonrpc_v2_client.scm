(define-module (crates-io js on jsonrpc_v2_client) #:use-module (crates-io))

(define-public crate-jsonrpc_v2_client-0.1.0 (c (n "jsonrpc_v2_client") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "08sl5z4m2qscl3xz9q72z27nic41r9lh8id452bpdbj69c74nnkb") (y #t)))

(define-public crate-jsonrpc_v2_client-0.2.0 (c (n "jsonrpc_v2_client") (v "0.2.0") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "1id7shlhsd352jr78a9n460r6bgxy5xd6ij5lzyk1x1hd2j6wc1v")))

(define-public crate-jsonrpc_v2_client-0.2.1 (c (n "jsonrpc_v2_client") (v "0.2.1") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "08hd31z9cw2i0jb4gmnc6grnbihsq9bgy1ws9yxp35p9331k1jl6")))

