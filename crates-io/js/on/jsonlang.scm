(define-module (crates-io js on jsonlang) #:use-module (crates-io))

(define-public crate-jsonlang-0.1.0 (c (n "jsonlang") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "semver") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)))) (h "1grqs6law5d8gihlbx5zm4sld0v7mcfzmd2qn6xlc4gwvhp5b1n2")))

