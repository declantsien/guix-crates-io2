(define-module (crates-io js on jsonresponder) #:use-module (crates-io))

(define-public crate-jsonresponder-0.1.0 (c (n "jsonresponder") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "1j9gd8qqimx2644xhkrh9ann8csq8db7dgvicw0py7vlmsr9ny95")))

