(define-module (crates-io js on jsonic) #:use-module (crates-io))

(define-public crate-jsonic-0.1.0 (c (n "jsonic") (v "0.1.0") (h "0milmpj4d2rcmlh6qjyjnsjkwbm3ydp4nkl0lmyh5xb2dhc372y7")))

(define-public crate-jsonic-0.1.1 (c (n "jsonic") (v "0.1.1") (h "12l9mnnp3krjrpzscajgxdspyq728cgbfbfkwhjf1i3kl9dk877q")))

(define-public crate-jsonic-0.1.2 (c (n "jsonic") (v "0.1.2") (h "0y3ijdl3bm4zx502xwqalpdiwhzyvi6z2nhn1aylzyg2pnyi487h")))

(define-public crate-jsonic-0.1.3 (c (n "jsonic") (v "0.1.3") (h "0srsfm0dcivhaykvycxapmradwm7pr2awcw9vnv4hmkqynynmrcl")))

(define-public crate-jsonic-0.1.4 (c (n "jsonic") (v "0.1.4") (h "113qdhd2cqv4rqkpnnfs6sx4d91wdlz7mp5sbnwy1jap0r813p56")))

(define-public crate-jsonic-0.1.5 (c (n "jsonic") (v "0.1.5") (h "0iphwix2aqr206paw327cspiiz35vl8rll3ffiz85m8ghp46a3iw")))

(define-public crate-jsonic-0.1.6 (c (n "jsonic") (v "0.1.6") (h "1rzd4jp3swqg4hlzbb4i6dxl34cfdw0igczwypsl4vh6sgcnnc1s")))

(define-public crate-jsonic-0.1.7 (c (n "jsonic") (v "0.1.7") (h "1ndk6bg7gjmhvg9lz865md1fff3y7ynbv9i9pifsfry90bvxh13m")))

(define-public crate-jsonic-0.1.8 (c (n "jsonic") (v "0.1.8") (h "0qq5x301xslnicdbpplcjc1458aqzidvgqi1d9kqfpgn5p0ljjzd")))

(define-public crate-jsonic-0.2.0 (c (n "jsonic") (v "0.2.0") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1drjdc8w7x20752nfsg0jgm21xwm6ndvyyxj455q5cmbg6ab8y6x")))

(define-public crate-jsonic-0.2.1 (c (n "jsonic") (v "0.2.1") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1322xcmymrkfak7bjsrqqf164vgc6yslf1cq2gpgv4c9pc4h4939")))

(define-public crate-jsonic-0.2.2 (c (n "jsonic") (v "0.2.2") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "03hwacvf6dm3085sfm6c2s1x0nvlvz4n06r9zv5mcbc63ncqz03r")))

(define-public crate-jsonic-0.2.3 (c (n "jsonic") (v "0.2.3") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0r7i0p6d37rjxksf28s8qw70i5iagnhl449hm6a9n2nh46hx5i3f")))

(define-public crate-jsonic-0.2.4 (c (n "jsonic") (v "0.2.4") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0bjvzb1fdf5kwmblpcykw0qqn0gw6l20m2w086lirabwlg8ql2q4")))

(define-public crate-jsonic-0.2.5 (c (n "jsonic") (v "0.2.5") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "19iq266rfdwhna4lgrmi6b5qaa35vyzy88n5ajdq7jn9zl40m9c9")))

(define-public crate-jsonic-0.2.6 (c (n "jsonic") (v "0.2.6") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1higvwmxy478z6jhywbf9hi5qwii87s0c57ic7ym9pkc5nxx6i0f")))

(define-public crate-jsonic-0.2.7 (c (n "jsonic") (v "0.2.7") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1jchx3rcgvcz3j38r99sryjslv3gp76hlk87g9bb6yy0cry0mwwl")))

(define-public crate-jsonic-0.2.8 (c (n "jsonic") (v "0.2.8") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1b4z3m1lkr551zlz78ig1nr1qzlcan799h08cvdq453vlq7bn64r")))

(define-public crate-jsonic-0.2.9 (c (n "jsonic") (v "0.2.9") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "1s6b5w1p2jx3vjx29ks01mh9xx1qgyng29clnsf3q8cxb46z0b0z")))

(define-public crate-jsonic-0.2.10 (c (n "jsonic") (v "0.2.10") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0xsq30qaz57z3cax8sagx8gvdakvc8kgfwb4rifa1sgcvl5g284v")))

(define-public crate-jsonic-0.2.11 (c (n "jsonic") (v "0.2.11") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "simd-json") (r "^0.13.8") (d #t) (k 2)) (d (n "sonic-rs") (r "^0.3") (d #t) (k 2)))) (h "1r1jsmqj47pr03k83nl0xpvannq1750i82ndlfjqmhvgqb9y11h3")))

(define-public crate-jsonic-0.2.12 (c (n "jsonic") (v "0.2.12") (d (list (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "simd-json") (r "^0.13.8") (d #t) (k 2)) (d (n "sonic-rs") (r "^0.3") (d #t) (k 2)))) (h "0zad212jzwpxjqpg9jxr30y97iq9pm786lx6ynahsws5hpnf05jn")))

