(define-module (crates-io js on jsonresume-renderer) #:use-module (crates-io))

(define-public crate-jsonresume-renderer-0.1.0 (c (n "jsonresume-renderer") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "1y9db5sisx0lbxm8cjpw589w0w8wlbc92lc6kw1240wlp6r6ybpm")))

(define-public crate-jsonresume-renderer-0.1.1 (c (n "jsonresume-renderer") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "116synsx4z7w2h56m5fd3zjh95p7jz80kwwb75fx91qxlr4sx563")))

(define-public crate-jsonresume-renderer-0.2.0 (c (n "jsonresume-renderer") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "0gj1ifpfws1kqp1csgiqh52gs16qx2bhx1i2l4hhgdmdbzm1figr")))

(define-public crate-jsonresume-renderer-0.3.0 (c (n "jsonresume-renderer") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)))) (h "1n03bh6sw6x5801d1js6n2nimi4yqbjr8y15jkp4v82smz443pvf")))

(define-public crate-jsonresume-renderer-0.4.0 (c (n "jsonresume-renderer") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 0)) (d (n "tera") (r "^1.6") (d #t) (k 0)))) (h "04ica6xhih99spysh8j67w3rgkk97agg3gwn9lc9rkyxa3mm23li")))

(define-public crate-jsonresume-renderer-0.5.0 (c (n "jsonresume-renderer") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13") (d #t) (k 0)) (d (n "tera") (r "^1.6") (d #t) (k 0)))) (h "1fgcb9hny4f2qdvvii9wadx5ppzfdmk3rifx82a71lfx4fj61092")))

