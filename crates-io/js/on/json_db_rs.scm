(define-module (crates-io js on json_db_rs) #:use-module (crates-io))

(define-public crate-json_db_rs-0.1.0 (c (n "json_db_rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07s5scw6zbgqrbpvrl06asrwr8q6kx5pm6m2bibijyl52q3924ad")))

