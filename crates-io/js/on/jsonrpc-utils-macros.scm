(define-module (crates-io js on jsonrpc-utils-macros) #:use-module (crates-io))

(define-public crate-jsonrpc-utils-macros-0.2.0-preview.1 (c (n "jsonrpc-utils-macros") (v "0.2.0-preview.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "130kvm2kbirfvkh9y646mj5faq80wyfsjkhv2c1d7qxk2mfm98js")))

(define-public crate-jsonrpc-utils-macros-0.2.0-preview.3 (c (n "jsonrpc-utils-macros") (v "0.2.0-preview.3") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0sh0bjrd1vh3xjhsvi1m6va2308mhkrkx9240y6v7m0359ng6xp5")))

(define-public crate-jsonrpc-utils-macros-0.2.0-preview.4 (c (n "jsonrpc-utils-macros") (v "0.2.0-preview.4") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "03b3ibjn1dbqq33yx1zizjpvhkdvff1g1h7b5hxh527ccm5ahk8x")))

(define-public crate-jsonrpc-utils-macros-0.2.0-preview.5 (c (n "jsonrpc-utils-macros") (v "0.2.0-preview.5") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19bvbn8dkm3p9x1pnk28vy3sfcq1hhvshhmpmdkvix6wfx8ns5mj")))

(define-public crate-jsonrpc-utils-macros-0.2.0 (c (n "jsonrpc-utils-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "08zqajvam1ivaadiw88c2xcqcjncyif9m1q68cng4cr8pg4d49w7")))

(define-public crate-jsonrpc-utils-macros-0.2.2 (c (n "jsonrpc-utils-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hzh57ff19jhqlhh0mvhmhicslxjpr31pjv9nd2gij38kk9rs5f5")))

(define-public crate-jsonrpc-utils-macros-0.2.3 (c (n "jsonrpc-utils-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13qsm2lp6i4v1576ddp35i4md8ziyxv3zhzrv1wf7k0sdwgka9mi")))

(define-public crate-jsonrpc-utils-macros-0.2.4 (c (n "jsonrpc-utils-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqqks2ckqma0v8q39nhnpsrnm954f37ir3bbxbhjjbydzk3rzil")))

