(define-module (crates-io js on jsonrpc-sdk-client) #:use-module (crates-io))

(define-public crate-jsonrpc-sdk-client-0.1.0 (c (n "jsonrpc-sdk-client") (v "0.1.0") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-prelude") (r "= 0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)))) (h "07rxrz3immfl0v1ky3j296zb0swy5dakc8fgci2spbv53h1kplgb")))

(define-public crate-jsonrpc-sdk-client-0.1.1 (c (n "jsonrpc-sdk-client") (v "0.1.1") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-prelude") (r "= 0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)))) (h "149f4bqm74szn822bcmii6wyagg326fx6p9i6ymd03vc74h90743")))

(define-public crate-jsonrpc-sdk-client-0.1.2 (c (n "jsonrpc-sdk-client") (v "0.1.2") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-prelude") (r "= 0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)))) (h "1lz6viv2i43ypw43czy4x0k6rakidjnzbjgjkd572pnjvaynz26z")))

(define-public crate-jsonrpc-sdk-client-0.1.3 (c (n "jsonrpc-sdk-client") (v "0.1.3") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-prelude") (r "= 0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)))) (h "07zz89wdbfy6mcwdba466f521d1r2zaw00p29a1d01f4m02hgfn7")))

(define-public crate-jsonrpc-sdk-client-0.1.4 (c (n "jsonrpc-sdk-client") (v "0.1.4") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-prelude") (r "= 0.1.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)))) (h "0gx1g5kcr6pwfz4f5zpwhg1v7ccw8ag0yl18jfi2mpzsk1l4q8f5")))

