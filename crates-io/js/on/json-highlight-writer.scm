(define-module (crates-io js on json-highlight-writer) #:use-module (crates-io))

(define-public crate-json-highlight-writer-0.1.0 (c (n "json-highlight-writer") (v "0.1.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "1zczxav2d98i8y1ziiyqdv18zs2rr6ykw2jv0jxjfjrpk8kx8jhz")))

(define-public crate-json-highlight-writer-1.0.0 (c (n "json-highlight-writer") (v "1.0.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "1ginq7pr1ifwwfcbas6d4gc0w05k60xqiac4sqf6rdya3bzcjska")))

(define-public crate-json-highlight-writer-1.1.0 (c (n "json-highlight-writer") (v "1.1.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "1a08876r3dqpaq121i83f3qsaslwg8dqn3d7qbpknzbwd0fy4l3k")))

