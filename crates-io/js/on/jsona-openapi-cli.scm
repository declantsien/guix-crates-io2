(define-module (crates-io js on jsona-openapi-cli) #:use-module (crates-io))

(define-public crate-jsona-openapi-cli-0.1.0 (c (n "jsona-openapi-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "05r7xm1jc9hisq8a7lbs50xknn4wjkkq7c7nz5i6hbimkzqkizsr")))

(define-public crate-jsona-openapi-cli-0.1.2 (c (n "jsona-openapi-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1lk1sdfan4qpmgmarcm57hwnm5jchk5dg4cgcnngm0rzzq8098dh")))

(define-public crate-jsona-openapi-cli-0.1.6 (c (n "jsona-openapi-cli") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0v574qkmn04sh97a1xyavc6abzd3qlpbwl92kanxaf8l3yshl7z2")))

(define-public crate-jsona-openapi-cli-0.1.8 (c (n "jsona-openapi-cli") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "062lk0g4rkz4dzb70pbc7g1rf0h5fvz1gn5kyjyb2d64yvgq9xzw")))

(define-public crate-jsona-openapi-cli-0.1.10 (c (n "jsona-openapi-cli") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kkik3if0szz5krqazn3w663sk3ix1vxipay0x231xcfgdnmg4pf")))

(define-public crate-jsona-openapi-cli-0.1.12 (c (n "jsona-openapi-cli") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "15jfby54hc65nq61dpndlxi5axhi6f69f6v9anm8fqsn8f65npgb")))

(define-public crate-jsona-openapi-cli-0.1.14 (c (n "jsona-openapi-cli") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0xkmlxz0m9q6ri90kcl1vqx328906r6xvkxm2rjv5rf317cbdhh5")))

(define-public crate-jsona-openapi-cli-0.1.16 (c (n "jsona-openapi-cli") (v "0.1.16") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0id2cxyi8wbiqmawwvnpvqfzjvnlgbqs1sc0yay1c65sdqwdrh9k")))

(define-public crate-jsona-openapi-cli-0.1.17 (c (n "jsona-openapi-cli") (v "0.1.17") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.1.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1f5c8wgkps2x0kish6flr910s0ml8fs5w5hbxdf2kpzy2pwgdxkp")))

(define-public crate-jsona-openapi-cli-0.2.0 (c (n "jsona-openapi-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0xn1ybv5p0dc1fgzl0h32jxc68549a26bscakh344yibb9kaj05h")))

(define-public crate-jsona-openapi-cli-0.2.1 (c (n "jsona-openapi-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "jsona-openapi") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0hrksbzwkcd6k3cd5amp68p258p6zjsr36pr6axv02508g555yw0")))

