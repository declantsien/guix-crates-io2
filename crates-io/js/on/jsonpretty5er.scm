(define-module (crates-io js on jsonpretty5er) #:use-module (crates-io))

(define-public crate-jsonpretty5er-0.1.0 (c (n "jsonpretty5er") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "json5format") (r "^0.2.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0kw1215ld4332bjwcljk6s3zn5svgl8cx7cbgijchw3zn6lycg1c")))

