(define-module (crates-io js on jsonrpc-types) #:use-module (crates-io))

(define-public crate-jsonrpc-types-0.1.0 (c (n "jsonrpc-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x5097n18cyz66lh2dakcdjmbkvs9k03ss7mmswj21azrn59hl12")))

(define-public crate-jsonrpc-types-0.2.0 (c (n "jsonrpc-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bi9jx03pg6d2i6af6icw2hdkw6bp0kl1fc26hzafl8814g72kc9")))

(define-public crate-jsonrpc-types-0.2.1 (c (n "jsonrpc-types") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07rrb5in6jj00fszqvxvdkcgxi85qni96pddyhb8y0rpi9ppm8am")))

(define-public crate-jsonrpc-types-0.3.0 (c (n "jsonrpc-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pi5b9n13b4xvf75jy7s3wzcddbd19rqsxrcpzl8r3ad5cmci4r1") (y #t)))

(define-public crate-jsonrpc-types-0.3.1 (c (n "jsonrpc-types") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dra2hbwrlwafs4qybb1dl6s3zg78wp7zcsliv9lizazq0240752") (y #t)))

(define-public crate-jsonrpc-types-0.3.2 (c (n "jsonrpc-types") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "116k9mdq5nwfccmp2vxd8qk08scwldwa5xim2ks6xnsc0x19r2d9")))

(define-public crate-jsonrpc-types-0.3.3 (c (n "jsonrpc-types") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cmjhqa886qb9brzxynq6s00vm6d5h6rgbywkpp1qgfh00dkn84d")))

