(define-module (crates-io js on json_in_type_derive) #:use-module (crates-io))

(define-public crate-json_in_type_derive-0.1.0 (c (n "json_in_type_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0rnhszfa88ryxl216630bazj5ia0apdqhrishfs0r505wgknilrb")))

(define-public crate-json_in_type_derive-0.1.1 (c (n "json_in_type_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "12wc7d4h4hrz5w3lgh3v7sr3cpv81g65ddla98hjkdzs2rrqgr2f")))

(define-public crate-json_in_type_derive-0.1.2 (c (n "json_in_type_derive") (v "0.1.2") (d (list (d (n "json_in_type") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1p0gk9ifqz3y97h67hr8ml2hwmy6yb1adqfjwb4as0ywcf1wficp")))

