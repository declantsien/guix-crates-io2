(define-module (crates-io js on json-decode) #:use-module (crates-io))

(define-public crate-json-decode-0.1.0 (c (n "json-decode") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1qs4kx5wwbkl0kcaz8ygkrqyp103wzqkh85jmyjr2rqqc65swqab")))

(define-public crate-json-decode-0.2.0 (c (n "json-decode") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0nwafjqzd10nd9z8929csdhs1ywy3yxp0m9hzwv7pbvkz4r94nxy")))

(define-public crate-json-decode-0.3.0 (c (n "json-decode") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1zl4f6a6g4vh88ryvny1kwiaxfwdny1p7rahs2r5zgb765jhi5ss")))

(define-public crate-json-decode-0.4.0 (c (n "json-decode") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "04n50xxjxbc59fp6yw0j1j81qg8gxwd6mglzvk99jy4yhdnairns")))

(define-public crate-json-decode-0.4.1 (c (n "json-decode") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0kxry3csnvqfbdvkndrc89qbn714wpqic342571zk69dzk2s9bfr")))

(define-public crate-json-decode-0.5.0 (c (n "json-decode") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "04yjyfrdv9jgv65q3l8kh1bzlcx4knz5dlmj66lz9rlbm55l8l04")))

(define-public crate-json-decode-0.6.0 (c (n "json-decode") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1vmnl4pr6vhyd735y6fwb3cg2g2a99jyibrplkwa8gg9mlwj3mwg")))

