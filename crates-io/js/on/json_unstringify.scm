(define-module (crates-io js on json_unstringify) #:use-module (crates-io))

(define-public crate-json_unstringify-1.0.0 (c (n "json_unstringify") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rncbli9zbdw07mz3h6vks6kc8y6jfmjrnvqd1vvqa101rgyj6is")))

(define-public crate-json_unstringify-1.0.1 (c (n "json_unstringify") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qf6cm5228arkbw7gwcmd9a4ml8sywxk2nhvqwpcxv1gsav2pnwz")))

(define-public crate-json_unstringify-1.0.2 (c (n "json_unstringify") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qmfzaqr0v229f1q3sjxyj0r0k3zzcansjiml8awkwjik2mx4wwl")))

(define-public crate-json_unstringify-1.0.3 (c (n "json_unstringify") (v "1.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kg6fm4x6rp5rai0qjxkn6izjfmmdk7wqb52zcdblynac6vcj6r2")))

