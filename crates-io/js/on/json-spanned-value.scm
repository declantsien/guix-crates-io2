(define-module (crates-io js on json-spanned-value) #:use-module (crates-io))

(define-public crate-json-spanned-value-0.1.0 (c (n "json-spanned-value") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18d0d1f7i48x1gqvqfinqc5jaj23xg0siaxg8ikg3ig6m5478g59") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-json-spanned-value-0.1.1 (c (n "json-spanned-value") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19hf0a46110j9qxkwrzlzn1vk3brl0q86a6gfixx9xwf5psnh2j7") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-json-spanned-value-0.1.2 (c (n "json-spanned-value") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fhcfsky5xwphmqyzcyvqh76awpanx038h90qj75xfxz77g47ddl") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-json-spanned-value-0.1.3 (c (n "json-spanned-value") (v "0.1.3") (d (list (d (n "codespan-reporting") (r "^0.9.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qlbqay6dvmfvk1ryz47qdwgzdcbdp2ykr1349ix7w9ylfbjl1rs") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-json-spanned-value-0.2.0 (c (n "json-spanned-value") (v "0.2.0") (d (list (d (n "codespan-reporting") (r "^0.9.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "058k4hci0cy859xm149r1x2qvhd69ywaa6h1bfyk095g1283pkm4") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-json-spanned-value-0.2.1 (c (n "json-spanned-value") (v "0.2.1") (d (list (d (n "codespan-reporting") (r "^0.9.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l591s5vwypz17iy1z3v0i16bf2805d0cv4j7ak0zxajn9hj58vb") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-json-spanned-value-0.2.2 (c (n "json-spanned-value") (v "0.2.2") (d (list (d (n "codespan-reporting") (r "^0.9.5") (d #t) (k 2)) (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0452h21w6dm4xb5k48lw2fzmm6fsi2nfvpip94s2pcmiwfj3yd5v") (f (quote (("preserve_order" "indexmap") ("default"))))))

