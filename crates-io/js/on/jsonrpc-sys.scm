(define-module (crates-io js on jsonrpc-sys) #:use-module (crates-io))

(define-public crate-jsonrpc-sys-0.1.0 (c (n "jsonrpc-sys") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1ngyvhnal7cjw3idwzxqfq2zy3avqznc685v9fg204zw77zyc9xz") (s 2) (e (quote (("unknown_params" "dep:serde_json" "serde_json/raw_value"))))))

(define-public crate-jsonrpc-sys-0.1.1 (c (n "jsonrpc-sys") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1h31dlwds721niypfrnwb7wwz2542i7gip6kr4fx1ma3c72wqxmr") (s 2) (e (quote (("unknown_params" "dep:serde_json" "serde_json/raw_value"))))))

(define-public crate-jsonrpc-sys-0.1.2 (c (n "jsonrpc-sys") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0f7cdzgl4d2qbg09zzw1a0ajfq71lvx5f57zvlhrriaiwbqhwyfh") (s 2) (e (quote (("utils" "dep:serde_json") ("unknown_params" "dep:serde_json" "serde_json/raw_value"))))))

(define-public crate-jsonrpc-sys-0.1.3 (c (n "jsonrpc-sys") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1szg55id64l9hsmjpzvxb5bzix8v2k2xa7w422452l2yn3kbpsaw") (s 2) (e (quote (("utils" "dep:serde_json") ("unknown_params" "dep:serde_json" "serde_json/raw_value"))))))

(define-public crate-jsonrpc-sys-0.1.4 (c (n "jsonrpc-sys") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0128ykg86ixqj6rhwa4z96dl2spycjwrls7fcmk008q3fa5a47lv") (s 2) (e (quote (("utils" "dep:serde_json") ("unknown_params" "dep:serde_json" "serde_json/raw_value"))))))

