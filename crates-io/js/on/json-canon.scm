(define-module (crates-io js on json-canon) #:use-module (crates-io))

(define-public crate-json-canon-0.1.0 (c (n "json-canon") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ryu-js") (r "^0.2.2") (k 0)) (d (n "serde") (r "^1.0.162") (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("std" "float_roundtrip"))) (k 0)))) (h "0h1pjai3s6yik6yzqysaw69dwjknfw3xlgmg8cj5x5f6a6rd1hzy")))

(define-public crate-json-canon-0.1.1 (c (n "json-canon") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ryu-js") (r "^0.2.2") (k 0)) (d (n "serde") (r "^1.0.162") (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("std" "float_roundtrip"))) (k 0)))) (h "1m36b8lwmzag9v68bnvq44ycqj2ghkjl8p2iizhigfwl05c0mfri")))

(define-public crate-json-canon-0.1.2 (c (n "json-canon") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ryu-js") (r "^0.2.2") (k 0)) (d (n "serde") (r "^1.0.162") (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("std" "float_roundtrip"))) (k 0)))) (h "00d10yc9ih76sqry4pyrnah86rg4382fs2w5jilbhz0wp963cnc7")))

(define-public crate-json-canon-0.1.3 (c (n "json-canon") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ryu-js") (r "^0.2.2") (k 0)) (d (n "serde") (r "^1.0.162") (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (f (quote ("std" "float_roundtrip"))) (k 0)))) (h "1nqfzidgikjfdxd851cpxn3yyclf82ai44qdrhddcixxl99y2yj4")))

