(define-module (crates-io js on json_logger) #:use-module (crates-io))

(define-public crate-json_logger-0.0.1 (c (n "json_logger") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0j20b0cwx13v3bg8azq3nlwlj33rg93mdgq1xw3vf8dxiflpm2jd")))

(define-public crate-json_logger-0.0.2 (c (n "json_logger") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "052w1clgjhjff4viqw6as6dfppdnw0y4lc8miprbksjf1vsdh63y")))

(define-public crate-json_logger-0.1.0 (c (n "json_logger") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12kzz65xf99z8gw3siz6m60067vqhkl87q2gpkzx8i2s644pn4ja")))

