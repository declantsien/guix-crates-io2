(define-module (crates-io js on jsonb-rs) #:use-module (crates-io))

(define-public crate-jsonb-rs-0.1.0 (c (n "jsonb-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "fast-float") (r "^0.2.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("preserve_order"))) (k 0)))) (h "157haal7vrhazii8z9a1s84d5g5yplzh1in93p8c0ik20rhdw0z7") (y #t) (r "1.60")))

