(define-module (crates-io js on json_rpc_v2) #:use-module (crates-io))

(define-public crate-json_rpc_v2-0.1.0 (c (n "json_rpc_v2") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync" "rt"))) (d #t) (k 0)))) (h "15gynjk36khh7ydm9cjn1zbjazpy22gr39njm71lzf7k9rgpabc4") (y #t)))

(define-public crate-json_rpc_v2-0.2.0 (c (n "json_rpc_v2") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "hyper") (r "^0.14.25") (f (quote ("full"))) (d #t) (k 2)) (d (n "json_rpc_v2_macro") (r "=0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0l22w07pmzbjy9c5dhyh4niq0r9j0krdarpidzhnq7zw26vjgi45") (y #t)))

