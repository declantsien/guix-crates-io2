(define-module (crates-io js on json_path) #:use-module (crates-io))

(define-public crate-json_path-0.1.0 (c (n "json_path") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gnpqxf4zbqga5x4x3nij1n9cb1wh5w63ji5xwagq51bz48l8p73")))

(define-public crate-json_path-0.1.1 (c (n "json_path") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s0smwsmk8nx2h7mmx1jgwpr0c1ygjsp8g7xrrfw36llgb7khixl")))

(define-public crate-json_path-0.1.2 (c (n "json_path") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j12ji1wjvlzhf4bhic7wwc6nhv55y7jl05bfv4gdx7wlv31y8hn")))

(define-public crate-json_path-0.1.3 (c (n "json_path") (v "0.1.3") (d (list (d (n "peekmore") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yqpnlix33bcqni5p4ni9alklrjqvgb39yfzjgv8ygn2qgxig1hb")))

(define-public crate-json_path-0.1.4 (c (n "json_path") (v "0.1.4") (d (list (d (n "peekmore") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xwnpn5705rwxrsc2cbpvifaczvfkgpyd0d2l26mhki4pxbrqql2")))

(define-public crate-json_path-0.1.5 (c (n "json_path") (v "0.1.5") (d (list (d (n "peekmore") (r "^1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yg2w3pqb2af81x9j3bdc4inan6367m18m9ms4yqsrac51zgqbri")))

