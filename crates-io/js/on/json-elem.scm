(define-module (crates-io js on json-elem) #:use-module (crates-io))

(define-public crate-json-elem-0.1.0 (c (n "json-elem") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "112gwac3g882558jivqpjfps42z0q1s4f8m1hl7y35qfsg9qpf2a")))

(define-public crate-json-elem-0.1.1 (c (n "json-elem") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "0m0rr2rwzxraf0cap0il8jf1h7jpkh4lmhwwzlp953zml41pbdw7")))

(define-public crate-json-elem-0.1.2 (c (n "json-elem") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "03ypcxcb8wcda984vbg46dhrdn1vjfl9fhzjzfib0bfpfbzixsih")))

(define-public crate-json-elem-0.1.3 (c (n "json-elem") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "1jqp0slkgqphjg5xzxajndk9xfxym2x74909vynk4avn3pi8d79x")))

(define-public crate-json-elem-0.1.4 (c (n "json-elem") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "06cyjfnsnf6r26amjlnszj6ps5dhrd4gk51cv4ii7mv2npzqrikp")))

(define-public crate-json-elem-0.1.5 (c (n "json-elem") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.3") (d #t) (k 2)))) (h "05pinspqc8miklgj45is95nw2i1gw96m3yfxvda37pcfka8f58hf") (y #t)))

