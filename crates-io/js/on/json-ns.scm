(define-module (crates-io js on json-ns) #:use-module (crates-io))

(define-public crate-json-ns-0.1.0 (c (n "json-ns") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.5") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1bw0zha6lrwxv42gxb98afkgpdm68bdlhw2y08ay3d2c9g4dskpf")))

