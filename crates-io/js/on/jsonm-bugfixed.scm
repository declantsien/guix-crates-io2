(define-module (crates-io js on jsonm-bugfixed) #:use-module (crates-io))

(define-public crate-jsonm-bugfixed-0.2.1 (c (n "jsonm-bugfixed") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1qyjwlhi7i7nbk23h33m3jrhbqhal41q5kcing0jigvvp7zqxkaw")))

