(define-module (crates-io js on jsonref) #:use-module (crates-io))

(define-public crate-jsonref-0.1.0 (c (n "jsonref") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1hxls0vhfip7fr5bsx75p1wfjmcbzf28yy74h1i7kgl9l0q0f0yb")))

(define-public crate-jsonref-0.1.1 (c (n "jsonref") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1qcnbw5yzlzfh5hxygi9sg31354kd447a0s6wsy40gl85d7jddmz")))

(define-public crate-jsonref-0.2.0 (c (n "jsonref") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "18526yrq4lcj0v5i2lang3l7irx3r2f4l2r2n4a1j8ixblaqlch2")))

(define-public crate-jsonref-0.3.0 (c (n "jsonref") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "charset"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1b42jh81asi4yc6hrdimk5l8crkcgf47sr0xvsmk2m895l5virsa")))

(define-public crate-jsonref-0.4.0 (c (n "jsonref") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json" "charset"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0fgz8asvpw0g0s73y38ij1xkmik3pxkrjdxqqscgapj36x5mnkf5")))

