(define-module (crates-io js on jsonrpc-http-server-plus) #:use-module (crates-io))

(define-public crate-jsonrpc-http-server-plus-7.1.0 (c (n "jsonrpc-http-server-plus") (v "7.1.0") (d (list (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^7.1") (d #t) (k 0)) (d (n "jsonrpc-server-utils") (r "^7.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0w4n6hgy5aliad3pv7grjlyg7r56vm2h6pm7g73gymcw39abwiqk")))

