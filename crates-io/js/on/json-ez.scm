(define-module (crates-io js on json-ez) #:use-module (crates-io))

(define-public crate-json-ez-0.1.0 (c (n "json-ez") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14dllvq8z3m4j2swgh8w92dd1bxcykdghni795msv9liwh10q17h") (y #t)))

(define-public crate-json-ez-0.2.0 (c (n "json-ez") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iqb99c26hd1m3sk7igszgpyca6537rqpjydrqfjm6afa4rhxnyj") (y #t)))

