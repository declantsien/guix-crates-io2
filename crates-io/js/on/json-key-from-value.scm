(define-module (crates-io js on json-key-from-value) #:use-module (crates-io))

(define-public crate-json-key-from-value-0.1.0 (c (n "json-key-from-value") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "03bmhfxxqbcw8wd9w5kl5alm00xa3slpxmmwv084iw0ynzcfrjm4")))

