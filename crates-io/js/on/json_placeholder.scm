(define-module (crates-io js on json_placeholder) #:use-module (crates-io))

(define-public crate-json_placeholder-0.1.0 (c (n "json_placeholder") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4svb2nqpwwa1n3k05najk5vmrjblcw3nf9b8am4zd9lx5cxh2b")))

