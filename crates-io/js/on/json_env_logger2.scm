(define-module (crates-io js on json_env_logger2) #:use-module (crates-io))

(define-public crate-json_env_logger2-0.2.0 (c (n "json_env_logger2") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "kv-log-macro") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15v6nmmkq9f79dlnzj4wqljc6lgmmdd33drd7irvhkjg7ip62w38") (f (quote (("iso-timestamps" "chrono") ("default"))))))

(define-public crate-json_env_logger2-0.2.1 (c (n "json_env_logger2") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (k 0)) (d (n "kv-log-macro") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("kv_unstable" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fxirs9xa15gaqb4nlkqcj565c29gsyw8pm76nnf7kf3kfnp2zkc") (f (quote (("iso-timestamps" "chrono") ("default"))))))

