(define-module (crates-io js on json-objects-to-csv) #:use-module (crates-io))

(define-public crate-json-objects-to-csv-0.1.0 (c (n "json-objects-to-csv") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "flatten-json-object") (r "^0.5.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zp7kwpiid9hrv3qjk41c327v8nknjks3vlnphv3246qg023kbnm")))

(define-public crate-json-objects-to-csv-0.1.1 (c (n "json-objects-to-csv") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "flatten-json-object") (r "^0.6.1") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1v5n2s7aa2951iwzzjcqmv7am7w089fsrw8dkywlk2in7bpa5wmw")))

(define-public crate-json-objects-to-csv-0.1.2 (c (n "json-objects-to-csv") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "flatten-json-object") (r "^0.6.1") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zvaq7xw67p6yalha8zch9mdq13722vw1m72ldd6d4sr9zwvnlkd")))

(define-public crate-json-objects-to-csv-0.1.3 (c (n "json-objects-to-csv") (v "0.1.3") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "flatten-json-object") (r "^0.6.1") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02cvax6v7zg9pbl4pcx09zs21ldjjpkk6mznfjxrk02lq6g7hfxg")))

