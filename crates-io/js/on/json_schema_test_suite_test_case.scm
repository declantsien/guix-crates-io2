(define-module (crates-io js on json_schema_test_suite_test_case) #:use-module (crates-io))

(define-public crate-json_schema_test_suite_test_case-0.1.0 (c (n "json_schema_test_suite_test_case") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wzb6xd18c7qcsk0wg1kaxnz0xc1szzpg6cahv0v4x71238pp4hf") (y #t)))

(define-public crate-json_schema_test_suite_test_case-0.2.0 (c (n "json_schema_test_suite_test_case") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09jlij14vnmbrqcjs8vb9zxpc235ghgrlr8vl056k0ingp9r5dbd")))

(define-public crate-json_schema_test_suite_test_case-0.3.0 (c (n "json_schema_test_suite_test_case") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18l69gmynkjk1m354xyjfdv6an2fwvx7459hnr0njaszwqhb0fzj")))

