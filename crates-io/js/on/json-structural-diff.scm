(define-module (crates-io js on json-structural-diff) #:use-module (crates-io))

(define-public crate-json-structural-diff-0.1.0 (c (n "json-structural-diff") (v "0.1.0") (d (list (d (n "console") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "difflib") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i90s0glwjhy3dad3ys3bsybc8knnfrcfxn10s9hgll47h6r9ir5") (f (quote (("colorize" "console"))))))

