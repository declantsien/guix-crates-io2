(define-module (crates-io js on json-toolkit) #:use-module (crates-io))

(define-public crate-json-toolkit-0.1.0 (c (n "json-toolkit") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0j4sq4hbq80fy4p0pcx09zca06758lbs9axr97ybsglw895m8xb8") (f (quote (("default" "serde" "json")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json") ("json" "dep:json")))) (r "1.60.0")))

(define-public crate-json-toolkit-0.1.1 (c (n "json-toolkit") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("display"))) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0a1wcn1n3jzw7lbgp1q8ww95wa9np5fqnf26spy9cc4nlydgp5iy") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "serde_json") ("json" "dep:json")))) (r "1.60.0")))

