(define-module (crates-io js on json_rpc_v2_macro) #:use-module (crates-io))

(define-public crate-json_rpc_v2_macro-0.1.0 (c (n "json_rpc_v2_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yqc4casvys90gg472x0mwvs8i3jn23zy2w5nwbsly1ixb2idn06") (y #t)))

