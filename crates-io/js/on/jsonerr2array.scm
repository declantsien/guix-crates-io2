(define-module (crates-io js on jsonerr2array) #:use-module (crates-io))

(define-public crate-jsonerr2array-0.0.1 (c (n "jsonerr2array") (v "0.0.1") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n3yglgaz4x2x5npmqm2rd6m46kh8f06n7h5bwfdqybmvbphnh94")))

(define-public crate-jsonerr2array-0.0.2 (c (n "jsonerr2array") (v "0.0.2") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iyy40hvpkxqk0fw6gnw6s4q508d987k2c0wycf2p3dsfprqp53s")))

