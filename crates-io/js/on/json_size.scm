(define-module (crates-io js on json_size) #:use-module (crates-io))

(define-public crate-json_size-0.1.0 (c (n "json_size") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "00xgljhizlv2x21wymjk8brzps97m1jlcgh9n4nn5b947zcl1xa7")))

(define-public crate-json_size-0.1.1 (c (n "json_size") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0vdx73yjk292r1vs9far5g9sv5p22v2my121990q4zzgjlgf8w5m")))

(define-public crate-json_size-0.1.2 (c (n "json_size") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0m8f8v1aw2ss8bl225d7xnxwbr8smdshwgdvby3v459nsvk4gxbn")))

(define-public crate-json_size-0.1.3 (c (n "json_size") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "11bhkbhirh5gp0dv5rz70wcfqaff2dj6cklx5ijpksyb7mkz6pzj")))

