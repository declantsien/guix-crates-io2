(define-module (crates-io js on json-cli) #:use-module (crates-io))

(define-public crate-json-cli-0.1.0 (c (n "json-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "18zd2acijzb8v5mlzw65jnsdhjslc9za4rk5f8mf8aqrmbj9w3js")))

(define-public crate-json-cli-0.1.1 (c (n "json-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1k96l7hxcjnj7qvb4av38h9rzwjprz6yhfddqb5l2qkf54vr0i4z")))

(define-public crate-json-cli-0.1.2 (c (n "json-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1894b5y9fksdp9ib96npipqx595knhdqpwynf6dszf1sk44kfvwf")))

(define-public crate-json-cli-0.1.3 (c (n "json-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1pvcz3gmagkmdg5mnfiz9vjkzbj0xr1hx7nx76va6ranmrjjlvgf")))

