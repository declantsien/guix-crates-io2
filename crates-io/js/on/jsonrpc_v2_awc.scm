(define-module (crates-io js on jsonrpc_v2_awc) #:use-module (crates-io))

(define-public crate-jsonrpc_v2_awc-0.1.0 (c (n "jsonrpc_v2_awc") (v "0.1.0") (d (list (d (n "awc") (r "^2.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xvh141lvb7zyr0wcbqf3x12jr3zxkx42znvfanicfjh9b93ghzp")))

