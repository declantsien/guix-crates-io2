(define-module (crates-io js on json-stream) #:use-module (crates-io))

(define-public crate-json-stream-0.1.0 (c (n "json-stream") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "02hp38lr9a9gpq1r0k6773wjlfiflvaaknv7wlip1b2hlizg75qj")))

(define-public crate-json-stream-0.1.1 (c (n "json-stream") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1j9gxkbnl1i57byfwjy5w84cfbxd2yii0d4d5hzddy1a8545d7f6")))

