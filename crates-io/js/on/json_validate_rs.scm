(define-module (crates-io js on json_validate_rs) #:use-module (crates-io))

(define-public crate-json_validate_rs-0.1.0 (c (n "json_validate_rs") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1d95432940xii3rk60k45hqy853rkxb6gf34xy7923jzdk3ab01n")))

(define-public crate-json_validate_rs-0.1.1 (c (n "json_validate_rs") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1ln5z9089b7nc1m7bnbxqcf1fd0ap97pdhy0drm02nadjmidckz0")))

(define-public crate-json_validate_rs-0.1.2 (c (n "json_validate_rs") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "14ncv6dfn72hgi763w23f2c38prjbkcg2grm1wlmi0cj897mlm6g")))

(define-public crate-json_validate_rs-0.1.3 (c (n "json_validate_rs") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1n1vhfsnwbsznw4wnb5vl0pdcdq61ylkhrmvplml4ypx315c236k")))

(define-public crate-json_validate_rs-0.1.4 (c (n "json_validate_rs") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1hfn0c7vbk731slj0m95i8mqnjgwa1kbl6vbwddf8drll8f8n2qg")))

(define-public crate-json_validate_rs-0.1.5 (c (n "json_validate_rs") (v "0.1.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "04fnmpkcs11r5ff3sgvd6jkhwvfg7gds817ph2f8wgg64hqbq8ps")))

(define-public crate-json_validate_rs-0.1.6 (c (n "json_validate_rs") (v "0.1.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1pa2b7mhpf9n93p5zhs1k7g3pkxffpcsvlbb9ypyc9hj49xx3bck")))

(define-public crate-json_validate_rs-0.1.7 (c (n "json_validate_rs") (v "0.1.7") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1q6n6vjjfwvgh8vyjf6nbxj74ajjwmpzs7m3a28xj869wif016di")))

(define-public crate-json_validate_rs-0.1.8 (c (n "json_validate_rs") (v "0.1.8") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0hqapwmdgzx3583fzjfs11iiymcpgcm00236w8w22631xyhvnhjb")))

(define-public crate-json_validate_rs-0.1.9 (c (n "json_validate_rs") (v "0.1.9") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "16mfl8wcnxa6r8w2klk8slzvv45sca3lh4ns0giq5wbzi6rn23b9")))

(define-public crate-json_validate_rs-0.2.0 (c (n "json_validate_rs") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1wwaf96m427fh43mqpabn218qvpmcbw6p6m3gjhk4gkwbk20icq4")))

(define-public crate-json_validate_rs-0.2.1 (c (n "json_validate_rs") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0dk936flprfj8fpy5ii79wh7jgky0d5aypz058d88zcpdgjpim84")))

(define-public crate-json_validate_rs-0.2.2 (c (n "json_validate_rs") (v "0.2.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0k42ki65k6v6frxjmrlmjca46vblr6pa4hfr31lg08akxyin7yhg")))

(define-public crate-json_validate_rs-0.2.4 (c (n "json_validate_rs") (v "0.2.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1lfhirmjyzlharkrprdabdhmakbbd1p2f691q8ml36j0w332vicr")))

(define-public crate-json_validate_rs-0.2.5 (c (n "json_validate_rs") (v "0.2.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0cyijlnhbly0qpiwdffji5yib4pcb8wv4kbchp6ffw8anr4cawx7")))

(define-public crate-json_validate_rs-0.2.6 (c (n "json_validate_rs") (v "0.2.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1krydmx1chkl19i4a6i9yr77nknnx8ayxknhbb4s0126n0snswy8")))

(define-public crate-json_validate_rs-0.2.7 (c (n "json_validate_rs") (v "0.2.7") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "08zskhr1i7zkhj1qakgxfa5sc31dafad51kyvmr0bpzv1cgz6n72")))

(define-public crate-json_validate_rs-0.2.8 (c (n "json_validate_rs") (v "0.2.8") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1frzsajnqznfgasb3xz8bffg24fbg98486ssmkbcd6rvi1jxx9b1")))

