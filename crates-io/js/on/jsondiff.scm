(define-module (crates-io js on jsondiff) #:use-module (crates-io))

(define-public crate-jsondiff-0.1.0 (c (n "jsondiff") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qq3pmi13iv37zy9cx3h99s8kf5y1hccz9wi4i780b5ja0y8aryp")))

