(define-module (crates-io js on jsonfilter) #:use-module (crates-io))

(define-public crate-jsonfilter-0.2.0 (c (n "jsonfilter") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1f86llwxzm8hf756fhxmfyxqi4x5vkzmflr3gj75dqwzjqmixr3r")))

