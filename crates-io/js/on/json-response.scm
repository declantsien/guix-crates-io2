(define-module (crates-io js on json-response) #:use-module (crates-io))

(define-public crate-json-response-1.0.0 (c (n "json-response") (v "1.0.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "routerify") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "stream-body") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "18qxzcr641xnhpjmphjmpbdhrq9sxfxfssq79579w3r3khdk9lki")))

