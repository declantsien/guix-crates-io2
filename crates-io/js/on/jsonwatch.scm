(define-module (crates-io js on jsonwatch) #:use-module (crates-io))

(define-public crate-jsonwatch-0.6.0 (c (n "jsonwatch") (v "0.6.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "ureq") (r "~0.11") (d #t) (k 0)))) (h "19zd3giyr731lnshrjnv33bq9qhjs5jzb316ijx29n9bl3nwb097")))

