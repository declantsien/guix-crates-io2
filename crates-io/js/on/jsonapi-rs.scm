(define-module (crates-io js on jsonapi-rs) #:use-module (crates-io))

(define-public crate-jsonapi-rs-0.1.0 (c (n "jsonapi-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "queryst") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "12hshb6dla4y8dvawn05lzd252kzrnhrk46aaqhgj5v8f9nw6ich") (r "1.56")))

(define-public crate-jsonapi-rs-0.1.1 (c (n "jsonapi-rs") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "queryst") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0793qalca17p7isr9jqmbpp1y7hm6vkxqv8g9llzfvmxi5n67k99") (r "1.56")))

