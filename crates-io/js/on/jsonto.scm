(define-module (crates-io js on jsonto) #:use-module (crates-io))

(define-public crate-jsonto-0.1.0 (c (n "jsonto") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "parsing"))) (o #t) (d #t) (k 0)) (d (n "synom") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "140c3pb12pbmvyysn5w8g7didq1as8b2y716myzcinighyi7dygp")))

(define-public crate-jsonto-0.1.1 (c (n "jsonto") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kl860rsdllwcpcml5xpni6sjmfw4vfmj6bx8a98f4yfildh536d")))

