(define-module (crates-io js on jsoncanvas) #:use-module (crates-io))

(define-public crate-jsoncanvas-0.1.0 (c (n "jsoncanvas") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1w78r5p6617d9bvsa2h55yzplssv0gjzvnmp8k30lwp8lpy187n1")))

(define-public crate-jsoncanvas-0.1.1 (c (n "jsoncanvas") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1a6ply4izk41yz7w34p89s0vwy1daz1ysl0wdaig1jm8fm4bbqz1")))

(define-public crate-jsoncanvas-0.1.2 (c (n "jsoncanvas") (v "0.1.2") (d (list (d (n "ambassador") (r "^0.3.6") (d #t) (k 0)) (d (n "hex_color") (r "^3.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_with") (r "^3.7") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0xydj18rp250pgn1ac5k3cvlaiy008yhgmy3n97igcpjgjhkrfxm")))

(define-public crate-jsoncanvas-0.1.3 (c (n "jsoncanvas") (v "0.1.3") (d (list (d (n "ambassador") (r "^0.3.6") (d #t) (k 0)) (d (n "hex_color") (r "^3.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_with") (r "^3.7") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0nd0nzpqrwvvyvzi3fvimwzzqrv0v39kw1csb5s0al1anpkjglaa")))

(define-public crate-jsoncanvas-0.1.4 (c (n "jsoncanvas") (v "0.1.4") (d (list (d (n "ambassador") (r "^0.3.6") (d #t) (k 0)) (d (n "hex_color") (r "^3.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_with") (r "^3.7") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1l544q1xs852jf3ka6fs10bxh36z7wzyc12sw1n3rp20zbv92p9q")))

(define-public crate-jsoncanvas-0.1.5 (c (n "jsoncanvas") (v "0.1.5") (d (list (d (n "ambassador") (r "^0.3.6") (d #t) (k 0)) (d (n "hex_color") (r "^3.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_with") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0r27bwzzlwl44kaagnva68f0m43g3qlfpjpdx91q4a141x4z1w1a")))

