(define-module (crates-io js on jsonpointer_flatten) #:use-module (crates-io))

(define-public crate-jsonpointer_flatten-0.1.0 (c (n "jsonpointer_flatten") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bfbrwww6v96xxdllcgy88i4ljqa4yd24fdbw2g4wrm1bx871rgy")))

(define-public crate-jsonpointer_flatten-0.1.1 (c (n "jsonpointer_flatten") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rsjxajw0pkbhg59rx2f3y2j6yfpi4vr5pmy4layvx1gnx5y3mv1")))

(define-public crate-jsonpointer_flatten-0.1.2 (c (n "jsonpointer_flatten") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00xgyl3bv4xq0mlpknn50wnbd6hiada8vbybhv06c0vx2k5zb6lc")))

(define-public crate-jsonpointer_flatten-0.1.3 (c (n "jsonpointer_flatten") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "009jvqqcmqsydsn9i7y9kbrfhsc2pbiwsmpqjwzaq3zxwq31y81s")))

