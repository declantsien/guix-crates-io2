(define-module (crates-io js on json-schema-tools) #:use-module (crates-io))

(define-public crate-json-schema-tools-0.1.0 (c (n "json-schema-tools") (v "0.1.0") (d (list (d (n "indexmap") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17iaiwlriqhn4aqccab12b9qf1nn6vgs0bj8lx55fila7wkz4ywm")))

