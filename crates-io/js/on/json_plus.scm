(define-module (crates-io js on json_plus) #:use-module (crates-io))

(define-public crate-json_plus-0.1.0 (c (n "json_plus") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "enumflags2") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.70") (d #t) (k 0)))) (h "1aqsrj8j0kpgqzy5j1bqhmblb119ia0pwkdkvwa62wwhy5014wr4")))

