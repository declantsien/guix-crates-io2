(define-module (crates-io js on jsonxf) #:use-module (crates-io))

(define-public crate-jsonxf-0.5.0 (c (n "jsonxf") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1dfn3q7q34knisrz7qkk97ylz8d073syqspxqxphqrg0vfsgl2sw")))

(define-public crate-jsonxf-0.6.0 (c (n "jsonxf") (v "0.6.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0cqh4ra2zjbh3zsx3pvnxnbsp64zq6s0c1nng4x23r73sm8dynxj")))

(define-public crate-jsonxf-0.7.0 (c (n "jsonxf") (v "0.7.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "10nw696ifwy2jkgc0g46016d01af153fvzdk7zpljznjwqdsr1vc")))

(define-public crate-jsonxf-0.9.0 (c (n "jsonxf") (v "0.9.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1kwxzqr2i0ry8ljzga5yld6s5y6k01qr7myw7rihd6alrkcp7yz1")))

(define-public crate-jsonxf-1.0.0 (c (n "jsonxf") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1wvpr78f20hqyalalibrq4j515f0kpcly62yljxa7mbd3l133hgi")))

(define-public crate-jsonxf-1.0.1 (c (n "jsonxf") (v "1.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0fg25bp9dnwgclq621y2ahzxbbh4dwaa096j70vd8kixgfbx71qx")))

(define-public crate-jsonxf-1.0.2 (c (n "jsonxf") (v "1.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1fbrv1mcxnin3bhhigq5dkd6axjfpdigdx1zpmnz17nmnydcvv4v")))

(define-public crate-jsonxf-1.1.0 (c (n "jsonxf") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3.4") (d #t) (k 0)))) (h "17gbklmv12ag43z8yamvwcq9a1kx8di6nglz6kwyxncjl5vm9jmz")))

(define-public crate-jsonxf-1.1.1 (c (n "jsonxf") (v "1.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3.4") (d #t) (k 0)))) (h "19iqy1nnprlsw1bgx9sz4q0k4ac8xhcpfxcaxl8dssjalng8imjh")))

