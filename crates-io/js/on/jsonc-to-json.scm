(define-module (crates-io js on jsonc-to-json) #:use-module (crates-io))

(define-public crate-jsonc-to-json-0.1.0 (c (n "jsonc-to-json") (v "0.1.0") (d (list (d (n "any-lexer") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16sx0an68v001xzpj4p73w2jda7j1ik6i8jnqh8pr7y0zyipsjxk")))

(define-public crate-jsonc-to-json-0.1.1 (c (n "jsonc-to-json") (v "0.1.1") (d (list (d (n "any-lexer") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n0m4aknhxg8ar7a2ha4b1qjck6cqzil9gx9nafi23gfq53irsqy")))

