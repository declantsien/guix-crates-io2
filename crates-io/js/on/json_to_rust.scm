(define-module (crates-io js on json_to_rust) #:use-module (crates-io))

(define-public crate-json_to_rust-0.2.0 (c (n "json_to_rust") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3.4") (d #t) (k 0)))) (h "0v4bbgr3iwc16lgwwf5203q2g20ds907584fm5nwih2xzny2mlw1")))

