(define-module (crates-io js on json-outer) #:use-module (crates-io))

(define-public crate-json-outer-0.1.0 (c (n "json-outer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "io-std" "process" "rt" "macros"))) (d #t) (k 0)))) (h "07qrwh010a3w35h4bbbvn9c98fhklaa0r6g7znybqi71pvvm0k6p")))

