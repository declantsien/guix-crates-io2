(define-module (crates-io js on json2tm) #:use-module (crates-io))

(define-public crate-json2tm-0.1.0 (c (n "json2tm") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "03xbc8kvs8pg2b5lm2yr6c5g8b213b5cnxsnpf52aaznjyafkyx7")))

(define-public crate-json2tm-0.1.1 (c (n "json2tm") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1hmqvddqnci1nbqis73an2bi35l3i3s59j8a67rq54qgy0w41kfm")))

(define-public crate-json2tm-0.1.2 (c (n "json2tm") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json_comments") (r "^0.2.1") (d #t) (k 0)) (d (n "plist") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "0czk4zb1pv2dcpr1ds55r1zx81k14s1mfa6fjja4729x77g96hf8")))

