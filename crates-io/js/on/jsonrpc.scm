(define-module (crates-io js on jsonrpc) #:use-module (crates-io))

(define-public crate-jsonrpc-0.2.0 (c (n "jsonrpc") (v "0.2.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "069zz8if5wasv47f98r689fdbmwhdc9hw3ry7kldiimxfbvcldky")))

(define-public crate-jsonrpc-0.2.1 (c (n "jsonrpc") (v "0.2.1") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "12z4bc4gx9i8fycnd39ccyfi76zysma14s851rxv32m1i90ang8b")))

(define-public crate-jsonrpc-0.3.0 (c (n "jsonrpc") (v "0.3.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "1vwqrm842rbkil7q18crwa8d3ai6d1cvqybgw3f08pyb8rp69zx6")))

(define-public crate-jsonrpc-0.3.1 (c (n "jsonrpc") (v "0.3.1") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "0l6jc0pwjynihxc0q97wbc2zcxzqpx43ix1d82h423fwfjn5mdax")))

(define-public crate-jsonrpc-0.3.2 (c (n "jsonrpc") (v "0.3.2") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "1gwlzw3r8bxh2dq3kqs24lxhcsm08f0dzj4hzlln9diwkj1164bh")))

(define-public crate-jsonrpc-0.3.3 (c (n "jsonrpc") (v "0.3.3") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "1s9bgz3fr3i281rjmp25i573vdm8r5c7nzvfpphi0y1y8j3z3whn")))

(define-public crate-jsonrpc-0.4.0 (c (n "jsonrpc") (v "0.4.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "085nwva8rc721jfy54zwyiib726bh7zxybnafzhnir233vj5p4kl")))

(define-public crate-jsonrpc-0.4.1 (c (n "jsonrpc") (v "0.4.1") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "1gk3yqxcpmpz0y7nvbc6ra7qjs4xr7q089n51m4a68yyj9s1bqjj")))

(define-public crate-jsonrpc-0.4.2 (c (n "jsonrpc") (v "0.4.2") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "04gp3y8yi5k8l7vxqc2fpv66idqldgi7is5vllfmr3jk95n9rxqz")))

(define-public crate-jsonrpc-0.5.0 (c (n "jsonrpc") (v "0.5.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)))) (h "1fwymp0rk0d5g4076yvf40d72ic9y23m4jkrfx07xa6jh1lpxl28")))

(define-public crate-jsonrpc-0.6.0 (c (n "jsonrpc") (v "0.6.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "1igmh110zvgap966crg75czw3b654wvnpc0lp628gzafn46zvpw0")))

(define-public crate-jsonrpc-0.6.1 (c (n "jsonrpc") (v "0.6.1") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "0y3qc7nlqwscyzz9dzk04v683nmcxydljawc0ph335dm6h2mhj90")))

(define-public crate-jsonrpc-0.6.2 (c (n "jsonrpc") (v "0.6.2") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "0ax4f3wdb8a73m9j09wfxkyg31miqbbmyy6w55gs5hfwranhxl64")))

(define-public crate-jsonrpc-0.7.0 (c (n "jsonrpc") (v "0.7.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "0p0ngw5wy76bxv10vd81pr233va1wqicry1lixpf4npql38jywsh")))

(define-public crate-jsonrpc-0.7.1 (c (n "jsonrpc") (v "0.7.1") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "1j6bfashpdl3zvjfdz1hmz0m0sw0hc2mrbgx9lhqx72q5hyv1s6f")))

(define-public crate-jsonrpc-0.7.2 (c (n "jsonrpc") (v "0.7.2") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "1z54xqgwc0ffkgdpp6m1vp39dc4n1w5nmnzf8fc3mfwblpi9vn7g")))

(define-public crate-jsonrpc-0.7.3 (c (n "jsonrpc") (v "0.7.3") (d (list (d (n "hyper") (r "^0.6") (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "1y88p9661y98jpsbfgxngsjz7rvlp5fihl92czp04s4pgs1qiair")))

(define-public crate-jsonrpc-0.7.4 (c (n "jsonrpc") (v "0.7.4") (d (list (d (n "hyper") (r "^0.6") (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "09cjhj8j2xafs8yb1swrrn3zd984p84wgfyvd746j5sl2zmngizb") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-jsonrpc-0.7.5 (c (n "jsonrpc") (v "0.7.5") (d (list (d (n "hyper") (r "^0.6") (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "1hn74qsccw9jbikpxnvy4ynkv0am81bwka87ncmbrigf7yaw2r45") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-jsonrpc-0.7.6 (c (n "jsonrpc") (v "0.7.6") (d (list (d (n "hyper") (r "^0.6") (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "1dkdah24am36k6anpkm8z34wigi8ap35aq6d7qv9v69mr4amxhrp") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-jsonrpc-0.8.0 (c (n "jsonrpc") (v "0.8.0") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "0dhhl66rvd4yrax6f86idm4qgkalscmhm39mv9b4qkxnc0779jb3") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

(define-public crate-jsonrpc-0.9.0 (c (n "jsonrpc") (v "0.9.0") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "strason") (r "^0.4") (d #t) (k 0)))) (h "1yq15r8sbfjr0kh2dvnlpx0i47r0xrv57r7gl01ljl7vq5rl51aw") (f (quote (("ssl" "hyper/ssl") ("default"))))))

(define-public crate-jsonrpc-0.10.0 (c (n "jsonrpc") (v "0.10.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0p0nd2g05jhw7z08rl5zwvm55chn04rbpqjwf3cgw58q5iwcqbci")))

(define-public crate-jsonrpc-0.9.1 (c (n "jsonrpc") (v "0.9.1") (d (list (d (n "hyper") (r "^0.9") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "strason") (r "^0.4") (d #t) (k 0)))) (h "1bn51gacjl4qr69254cf7ml9pl65711all65kybkj8r3wlrvdwz4") (f (quote (("ssl" "hyper/ssl") ("default"))))))

(define-public crate-jsonrpc-0.10.1 (c (n "jsonrpc") (v "0.10.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10i4sgwypamilgai0amj3gpy3ikpmcn19rp6lhnyrxnzp8wy1s34")))

(define-public crate-jsonrpc-0.10.2 (c (n "jsonrpc") (v "0.10.2") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04mvwksldlck3w8p52ysrghllr5mmz4f0ijpi8abbr6h8lf0cljn")))

(define-public crate-jsonrpc-0.11.0 (c (n "jsonrpc") (v "0.11.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "12fkglyhjh91klrn1ddi5pf0npgfk1hj1wg99nqwgsd4m1ak8vs3")))

(define-public crate-jsonrpc-0.12.0 (c (n "jsonrpc") (v "0.12.0") (d (list (d (n "base64-compat") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1xx9nvax5cbrd5xmsakp8nyf6gmfx0vyjj4hzf7xp606iaddc95d") (f (quote (("simple_http" "base64-compat") ("default" "simple_http"))))))

(define-public crate-jsonrpc-0.12.1 (c (n "jsonrpc") (v "0.12.1") (d (list (d (n "base64-compat") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1j3hwrvsy3yzga0iik50vmk8g9y9931i77aa3bpi4kf9iyvj713z") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64-compat") ("default" "simple_http" "simple_tcp"))))))

(define-public crate-jsonrpc-0.13.0 (c (n "jsonrpc") (v "0.13.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0f9nxrxm618vbimljrah9ssxaj4dl6i39a7c1yrjd90v60znp3gx") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("default" "simple_http" "simple_tcp"))))))

(define-public crate-jsonrpc-0.14.0 (c (n "jsonrpc") (v "0.14.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "socks") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "192gx4cgjzjsfnj6w86mdkjzfkr7kpqpdxdi4kipx8yrnpma73i4") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("proxy" "socks") ("default" "simple_http" "simple_tcp"))))))

(define-public crate-jsonrpc-0.14.1 (c (n "jsonrpc") (v "0.14.1") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "socks") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1wjk618k4yw9aw55zi7x3cflvqn9q1f1z35y8kqd67218xmz6a41") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("proxy" "socks") ("default" "simple_http" "simple_tcp"))))))

(define-public crate-jsonrpc-0.15.0 (c (n "jsonrpc") (v "0.15.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("json-using-serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "socks") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "06sqr1mr884842y36wfwa2p27s3k7smcf9vcnrs2b3r2z20hpv16") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("proxy" "socks") ("minreq_http" "base64" "minreq") ("default" "simple_http" "simple_tcp"))))))

(define-public crate-jsonrpc-0.16.0 (c (n "jsonrpc") (v "0.16.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("json-using-serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "socks") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "00np7n0qhbzfrmq6g8hm3lsq7dd5izm3l7fvavnpkyr24j6xxvrl") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("proxy" "socks") ("minreq_http" "base64" "minreq") ("default" "simple_http" "simple_tcp"))))))

(define-public crate-jsonrpc-0.17.0 (c (n "jsonrpc") (v "0.17.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("json-using-serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "socks") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1p9rr20c3y8ba3bb54w2a6vgv0aqa23qfi4p5l4jy28nsl292vd2") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("proxy" "socks") ("minreq_http" "base64" "minreq") ("default" "simple_http" "simple_tcp"))))))

(define-public crate-jsonrpc-0.18.0 (c (n "jsonrpc") (v "0.18.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("json-using-serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "socks") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1gz12rk9zvw9ab07pzak08yacnps1i103brwnznfyxqx6j6s6qin") (f (quote (("simple_uds") ("simple_tcp") ("simple_http" "base64") ("proxy" "socks") ("minreq_http" "base64" "minreq") ("default" "simple_http" "simple_tcp"))))))

