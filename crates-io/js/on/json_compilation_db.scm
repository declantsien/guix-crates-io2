(define-module (crates-io js on json_compilation_db) #:use-module (crates-io))

(define-public crate-json_compilation_db-0.1.0 (c (n "json_compilation_db") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0xik9aa91baalim5yxfxc9dzvapa6xy8kgw8wrr8i3n7mnk6rw1n")))

(define-public crate-json_compilation_db-0.2.0 (c (n "json_compilation_db") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "07fkpqazlrzkmqslcvni9apvz870pnjizi06dhhb54aqlygsk3mk")))

(define-public crate-json_compilation_db-0.3.0 (c (n "json_compilation_db") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16xdw27p9i6sa5zdjpg1827n5mv0w7v9rg9vdyv77wc4z70nga28")))

(define-public crate-json_compilation_db-1.0.0 (c (n "json_compilation_db") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shell-words") (r "^1.1") (d #t) (k 0)))) (h "1gpf1nsgarj6rby22cj1v000kpdn4mfb8yc1wvnj197igq34amqm")))

