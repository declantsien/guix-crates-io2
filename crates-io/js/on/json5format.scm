(define-module (crates-io js on json5format) #:use-module (crates-io))

(define-public crate-json5format-0.1.0 (c (n "json5format") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0fryin75j2f76a5r761a5qpn5prb35h33sqy20418pxiyxrz900p")))

(define-public crate-json5format-0.1.1 (c (n "json5format") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "03bn5m4cmb7khk3nivwkgg2np0pyzak1d8qxkp3vmiv1p7inhw6v")))

(define-public crate-json5format-0.1.2 (c (n "json5format") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "0nd29ys8qx4339699fnh0s2n1pycxlyvjkm7rgbbrr0bmlbpvbxv")))

(define-public crate-json5format-0.1.3 (c (n "json5format") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "10b0vw5mqwdpjiknl99pdy3f4yig1bxan9k6gplmvaf7a6jqvzj5")))

(define-public crate-json5format-0.1.4 (c (n "json5format") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "1qcryl3ka1vcik96cfzvlvhw9md3bjkhc41qsy1gmc5kdkvrcfn4")))

(define-public crate-json5format-0.1.5 (c (n "json5format") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "16mi1alhhgqz4vkky0dk19nl93cji29ipympxbfahn6956ahbjn3")))

(define-public crate-json5format-0.2.0 (c (n "json5format") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "03snz72729fbg5hk1hqzqsm1k7zaws2xn7pynshckli05r7dnbqd")))

(define-public crate-json5format-0.2.3 (c (n "json5format") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "1h1s333m43nlmwwf9j4jb5qv8qjhipqq1n0s6yag057v3sdkwm1k")))

(define-public crate-json5format-0.2.4 (c (n "json5format") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "0r00612czfhs00ha459wk4w58ws692gvz0c0wpk2k16sqdmi154m")))

(define-public crate-json5format-0.2.5 (c (n "json5format") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "16w6dd2sag3bl3jphsslrjvf5yqpr8qp3zk3wl26h7xshvgj7g8c")))

(define-public crate-json5format-0.2.6 (c (n "json5format") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.25") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)))) (h "00lrmrdv4hjscr25khvwx3arz3pvs2f35x4bc87g5a9gz1r4m2dj")))

