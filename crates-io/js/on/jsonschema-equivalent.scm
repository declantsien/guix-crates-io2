(define-module (crates-io js on jsonschema-equivalent) #:use-module (crates-io))

(define-public crate-jsonschema-equivalent-0.1.0 (c (n "jsonschema-equivalent") (v "0.1.0") (d (list (d (n "pathsep") (r "^0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^1") (d #t) (k 2)))) (h "0simbm00l6j7dsr92wak2bf5llddx6v07c8rhzpq395rn33wass4")))

