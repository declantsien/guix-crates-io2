(define-module (crates-io js on json-node) #:use-module (crates-io))

(define-public crate-json-node-0.1.0 (c (n "json-node") (v "0.1.0") (h "0zp1ybqzx81zxhfgk3j4mx9n20a6jcivz7fsvr38014hh41ajd1x")))

(define-public crate-json-node-0.1.1 (c (n "json-node") (v "0.1.1") (h "0cch2al566pz4j763jha514g93fwsz44isg6vj3gas00aa2n68rf")))

(define-public crate-json-node-0.1.2 (c (n "json-node") (v "0.1.2") (h "1d0w77fxdyn9cfixy2z9vr39n2zfs8bynw5zrq6qn4s1gjk7f5q1")))

(define-public crate-json-node-0.2.1 (c (n "json-node") (v "0.2.1") (h "1lh3iw79k4hhjrxixk40lmiwm5czljvazzflwq6klmcvdb2d0pcw")))

