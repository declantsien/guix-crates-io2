(define-module (crates-io js on jsonnet-rs) #:use-module (crates-io))

(define-public crate-jsonnet-rs-0.5.0 (c (n "jsonnet-rs") (v "0.5.0") (d (list (d (n "clap") (r "^2.18") (d #t) (k 2)) (d (n "jsonnet-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06ph4maflr39lbi9328nbh2lrsk3ky71iidf4fpb3vwl28x1aw5z")))

(define-public crate-jsonnet-rs-0.6.0 (c (n "jsonnet-rs") (v "0.6.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 2)) (d (n "jsonnet-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n3msmx1wp8m5npmbhll2x7gfajd4c2w24r6x379w7al8ag8b8yy")))

(define-public crate-jsonnet-rs-0.17.0 (c (n "jsonnet-rs") (v "0.17.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 2)) (d (n "jsonnet-sys") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08s7bvzp767wz4z98njl44wmyki1lbpw77jwamjlks9szjyrdcw2")))

