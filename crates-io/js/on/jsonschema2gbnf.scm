(define-module (crates-io js on jsonschema2gbnf) #:use-module (crates-io))

(define-public crate-jsonschema2gbnf-0.1.0 (c (n "jsonschema2gbnf") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gbnf") (r "^0.1.5") (d #t) (k 0)))) (h "053kv37yxd1ip6skd4h9kh71dblhnb0j8glaakqcwiwijhs8irmh")))

(define-public crate-jsonschema2gbnf-0.1.1 (c (n "jsonschema2gbnf") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gbnf") (r "^0.1.5") (d #t) (k 0)))) (h "1ac830wz8pvw2ym3sd7z0fklan3jiwf296lrb2yd8220rn58nihm")))

(define-public crate-jsonschema2gbnf-0.1.2 (c (n "jsonschema2gbnf") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gbnf") (r "^0.1.5") (d #t) (k 0)))) (h "0rqgakk315kvqii0gsir2ksgsmhwha57vcdp7v0179ifvwks7cdf")))

(define-public crate-jsonschema2gbnf-0.1.3 (c (n "jsonschema2gbnf") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gbnf") (r "^0.1.6") (d #t) (k 0)))) (h "1zrxbxgcpqv53c0854pcvamcsqxb531g53vxxlwf6v65wkaxiy17")))

(define-public crate-jsonschema2gbnf-0.1.4 (c (n "jsonschema2gbnf") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gbnf") (r "^0.1.6") (d #t) (k 0)))) (h "0njynzywj95wmlc82zbdqvwnvyppf1dl7k4pq90wy461q82qv0n6")))

