(define-module (crates-io js on json-ptr) #:use-module (crates-io))

(define-public crate-json-ptr-0.3.4 (c (n "json-ptr") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.25") (d #t) (k 0)))) (h "0scxdcfdpk658j42lz0i3qv4gcdx092dkp182s41n6by84i36s0k")))

(define-public crate-json-ptr-0.3.5 (c (n "json-ptr") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.25") (d #t) (k 0)))) (h "0545ljv1xpvs6xqw93p2yp2b9z7bsh8gqgl3445i0khlp8cy2pa1")))

(define-public crate-json-ptr-0.3.6 (c (n "json-ptr") (v "0.3.6") (d (list (d (n "serde_json") (r ">=1.0.25") (d #t) (k 0)))) (h "0nw14b473j82gazbqs2pc5in8cqax3fkdq2scl8jjcc9c0dzj61n")))

