(define-module (crates-io js on json-structural-diff-cli) #:use-module (crates-io))

(define-public crate-json-structural-diff-cli-0.1.0 (c (n "json-structural-diff-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "console") (r "^0.13") (d #t) (k 0)) (d (n "json-structural-diff") (r "^0.1.0") (f (quote ("colorize"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0vs6q37jmg9y07ircncfnjifyxp7rvi7pr9inlxr634bqdf33y83")))

