(define-module (crates-io js on json_config) #:use-module (crates-io))

(define-public crate-json_config-0.1.0 (c (n "json_config") (v "0.1.0") (h "19di2j7ikwwzjv6g0asg90mahjsg7r2hkfpanfgj97jr8acawx5n")))

(define-public crate-json_config-0.1.1 (c (n "json_config") (v "0.1.1") (d (list (d (n "build_script_file_gen") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "099yhg7nxc36n8jd5qwx621712wbdfra6pivn9cd2k4g3yv8v28m")))

(define-public crate-json_config-0.1.3 (c (n "json_config") (v "0.1.3") (d (list (d (n "json_config") (r "^0.1.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.6") (d #t) (k 1)))) (h "1p260k9y96a2hq7vk28shx3lra2iqajaszrb16a973bm1xpkwck6")))

(define-public crate-json_config-0.1.4 (c (n "json_config") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0.6") (d #t) (k 0)))) (h "1gmdhp6ravcx64jsnkr0kqvp9r8dyzxm4jd45vqhbpka1ip1bc8j")))

