(define-module (crates-io js on json-position-parser) #:use-module (crates-io))

(define-public crate-json-position-parser-0.0.1 (c (n "json-position-parser") (v "0.0.1") (h "16rlxalr4m3nfyrbg3mdq5spxng077mpb9f8zbxpaijxa0l33z35") (y #t)))

(define-public crate-json-position-parser-0.0.2 (c (n "json-position-parser") (v "0.0.2") (h "1ia0mnavjqv87fappmx07adhkwa8r0imsxrxhpf9d80bdbxlbs9b") (y #t)))

(define-public crate-json-position-parser-0.0.3 (c (n "json-position-parser") (v "0.0.3") (h "05s5gq7bv94x7a02mi1pm2skgm4cm07x8800hp3ab6dnb6di4hka")))

(define-public crate-json-position-parser-0.0.4 (c (n "json-position-parser") (v "0.0.4") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "02ii0vmfl67h1czm2gpdjpibchkjj8w3921lbmdaxgnxbdmy1if3")))

(define-public crate-json-position-parser-0.0.5 (c (n "json-position-parser") (v "0.0.5") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0zqqv4w9447sv3a2l6j1bf4bgy31xq5ild6n0qjj6sb793iyhdmx")))

(define-public crate-json-position-parser-0.0.6 (c (n "json-position-parser") (v "0.0.6") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "1s3x6igvn4vcikpc2v7g9am4yr3wq49jwlfr0fz94x5xfh4yzjql")))

(define-public crate-json-position-parser-0.0.8 (c (n "json-position-parser") (v "0.0.8") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0675qqpjwxs3gj5nzbxjbmxfa8wj6qjvi4ynb45pqi4cx2yj4g06") (y #t)))

(define-public crate-json-position-parser-0.0.9 (c (n "json-position-parser") (v "0.0.9") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "17ix6aqblz9li01d58k0nkwj58gjvr1fzg15sldayswlb4avrgk2")))

(define-public crate-json-position-parser-0.0.10 (c (n "json-position-parser") (v "0.0.10") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "1iy226bf1dznmvnk0rx3qv669a3npz8ddpr3hj41m43fp3219fd4") (y #t)))

(define-public crate-json-position-parser-0.0.11 (c (n "json-position-parser") (v "0.0.11") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0is72ym5ka0yv2jzfjfjfdwvrfidz3zpnja9lka8gx2f4xfcpw3j")))

