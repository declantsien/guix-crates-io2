(define-module (crates-io js on jsonpp) #:use-module (crates-io))

(define-public crate-jsonpp-2.0.0 (c (n "jsonpp") (v "2.0.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sjvm7cp4g1arws9x2b9572hngrwy7nsa72jd73aqcv88m53mipm")))

(define-public crate-jsonpp-2.0.1 (c (n "jsonpp") (v "2.0.1") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("unbounded_depth"))) (d #t) (k 0)))) (h "013dnf201is1gjadh64vphrmdkvgcgkrybsyvk5fap6f9yh7mgrr")))

