(define-module (crates-io js on jsonsv) #:use-module (crates-io))

(define-public crate-jsonsv-0.1.0 (c (n "jsonsv") (v "0.1.0") (d (list (d (n "jsonschema-valid") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0h0rmrnp3f1275ysf2i5nj6jm1zwhrgkvll5l3v8svz6xfhyarxy")))

(define-public crate-jsonsv-0.1.1 (c (n "jsonsv") (v "0.1.1") (d (list (d (n "jsonschema-valid") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0z69wx37ghhfk4ikb8ijvy8h45x0s8apddjw2lmc9xaw7fjv2qqn")))

(define-public crate-jsonsv-0.1.2 (c (n "jsonsv") (v "0.1.2") (d (list (d (n "jsonschema-valid") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1j8d41wmql8cm9k8w7p327ddsar7nr4kam6j76n2n0njphmfx1qx")))

