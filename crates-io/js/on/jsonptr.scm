(define-module (crates-io js on jsonptr) #:use-module (crates-io))

(define-public crate-jsonptr-0.0.1 (c (n "jsonptr") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0ahhq2is41s5lsgg1jriivjbq3diyp2wl6954i0z53i9r7ixskjg")))

(define-public crate-jsonptr-0.0.2 (c (n "jsonptr") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0gdz7gz42ls4sd7p5n8g0fclr5i1zy112vzlqb2gd2prrj98b381")))

(define-public crate-jsonptr-0.0.3 (c (n "jsonptr") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "13i26vyybrga54a0kngm8mknbxgh0d4m6xw9v84w30qwajw7f0kf")))

(define-public crate-jsonptr-0.0.4 (c (n "jsonptr") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1a6wbdl4jpwm22y9qn4yi2r6jvq7d7ys3947iskxqjislxx9qk43")))

(define-public crate-jsonptr-0.0.5 (c (n "jsonptr") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0lzavdwwbdbmfgk2ya8fzr3dafzsspb19gmmm4cvm1kb5754ghjw")))

(define-public crate-jsonptr-0.0.6 (c (n "jsonptr") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "14pbcl1xf008m7y1dknl2v9isl5j9x97rl5l4vhf641d26gyh09f")))

(define-public crate-jsonptr-0.0.7 (c (n "jsonptr") (v "0.0.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1rj26pcqmgrqyfq7mlsjiing18ia9vpjv76zl7wmp1i2dg4pzhcl")))

(define-public crate-jsonptr-0.0.8 (c (n "jsonptr") (v "0.0.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "055irkifqm8fnm0sw730kz11w90ddqsr657pa8z0kp7g7gm7vqdg")))

(define-public crate-jsonptr-0.1.0 (c (n "jsonptr") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0wqx5frbgiy1d6h7zig38fkn3j9vrm93xlqc05bbdz1156r2i1iz")))

(define-public crate-jsonptr-0.1.1 (c (n "jsonptr") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1y2a9rfc29mhxw0k0vsqaawv87nl68k4xivrd05vwxa7azayq6p2")))

(define-public crate-jsonptr-0.1.2 (c (n "jsonptr") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0xmbh336il5b9wl4081ynfcyz488pfl3mc2ivpsl455h4nvcjvxl")))

(define-public crate-jsonptr-0.1.3 (c (n "jsonptr") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0cfwdmqay0lhxjqs6zr9b2jpqqdn4pqqqh40kkk9qwypa838sx5j")))

(define-public crate-jsonptr-0.1.4 (c (n "jsonptr") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "16i61q0cxfv2lij6019jp4anmajwq94szzgx255cicxj2f6xpfqj")))

(define-public crate-jsonptr-0.1.5 (c (n "jsonptr") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uniresid") (r "^0.1.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "16gpm0cji8mwzv7m5vczrnpwsqiz118pxpm3yw2prs63gypa3rq4")))

(define-public crate-jsonptr-0.2.0 (c (n "jsonptr") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "08xpm29c6rqkknnxbf7hxj9gpin039cbqkzm1n8zdrn006y3pcbr") (f (quote (("std" "serde/std" "serde_json/std") ("default"))))))

(define-public crate-jsonptr-0.3.0 (c (n "jsonptr") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "03hvg9hp9wd769164nzmcyxv62565imr1xs1dwgkin03xjg4i8ca") (f (quote (("std" "serde/std" "serde_json/std") ("default")))) (y #t)))

(define-public crate-jsonptr-0.3.1 (c (n "jsonptr") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1xlan7hq9jygm80nmnyxwfpnldz7kjx86q878il750x4xc6njajy") (f (quote (("std" "serde/std" "serde_json/std") ("default")))) (y #t)))

(define-public crate-jsonptr-0.3.2 (c (n "jsonptr") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0sdaxpwm5yza8b0k8h4mkal4d9rhmff3n8iaqsgagz2991brx6c8") (f (quote (("std" "serde/std" "serde_json/std") ("default")))) (y #t)))

(define-public crate-jsonptr-0.3.3 (c (n "jsonptr") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0z81impfh81l52qh2n8mzg9pad5wma4vin7clfimhw0b9hyvc9qs") (f (quote (("std" "serde/std" "serde_json/std") ("default"))))))

(define-public crate-jsonptr-0.3.4 (c (n "jsonptr") (v "0.3.4") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0i8qqllm0f5chz8r15w7h0mkv7ajc5w8q03ydc1yy0ah50d022dj") (f (quote (("default")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.3.5 (c (n "jsonptr") (v "0.3.5") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0gdi7rf6ppdxlsaji5j5a04fmh3k5l449ld5p0v3zky75ap3nka7") (f (quote (("default")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.3.6 (c (n "jsonptr") (v "0.3.6") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0jaswb1vzgbr73c4d1ls9hl8yay2ydm21p38f32my5y3d2nabh8y") (f (quote (("default")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.0 (c (n "jsonptr") (v "0.4.0") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0r6cyjk8qz9gdcpyx8lzrg9nswq5cf8478nycbjk835s6qfslr3a") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.1 (c (n "jsonptr") (v "0.4.1") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "00f0xzlsripz0kwqs1lf5dzwdk0dg44b3mxf276zfzzd6hbkqprk") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.2 (c (n "jsonptr") (v "0.4.2") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "14266w02yzwa4720vrcb2hvdxkyqrzpqfmdxy5gdz3zr5lic9z7q") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.3 (c (n "jsonptr") (v "0.4.3") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "00wsslg0mkfczvwqi6clsihr4p04mi1h70fycpdscrkrajvmhcxa") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.4 (c (n "jsonptr") (v "0.4.4") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0syi8x8g93yvr0g4qzp8g5rn5nzblnx6167c1rqncc176my23vas") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.5 (c (n "jsonptr") (v "0.4.5") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "0fj6525j4c7x2cjbww6w30676ymfq5wl7wxs5qr3npk43dyiqfws") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.6 (c (n "jsonptr") (v "0.4.6") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1880sh7jvjw464wn0qvq7shhcnml9yq5zflcq41wxp1q905mzk3a") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

(define-public crate-jsonptr-0.4.7 (c (n "jsonptr") (v "0.4.7") (d (list (d (n "fluent-uri") (r "^0.1.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "uniresid") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "09s6bqjlkd1m5z9hi9iwjimiri7wx3fd6d88hara0p27968m4vhw") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde/std" "serde_json/std" "fluent-uri?/std"))))))

