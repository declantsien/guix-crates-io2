(define-module (crates-io js on jsonox) #:use-module (crates-io))

(define-public crate-jsonox-0.1.0 (c (n "jsonox") (v "0.1.0") (d (list (d (n "actix-cors") (r "^0.5.4") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "relative-path") (r "^1.3.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "16hrywvcvwhpbcv2wrgmlzswx0l993kqqddn3ib1vqc4lw4gnzfv")))

(define-public crate-jsonox-0.2.0 (c (n "jsonox") (v "0.2.0") (d (list (d (n "actix-cors") (r "^0.5.4") (d #t) (k 0)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "relative-path") (r "^1.3.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1rfa74wxhqrv7lrqz8l12ns0k1sd81pz6a4f8giaym62zx353ibw")))

