(define-module (crates-io js on jsonrpc2) #:use-module (crates-io))

(define-public crate-jsonrpc2-0.1.0 (c (n "jsonrpc2") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1s594ik1p6dcxyjqpl5rgwa1yb9wkgb65rva5c24bjdyn6gsnk3y")))

(define-public crate-jsonrpc2-0.1.1 (c (n "jsonrpc2") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1p4s7cs9fr48c2h4zrzanwdvkpr0gh77cszq0x9l3vbbs7s4h034")))

