(define-module (crates-io js on json-utils) #:use-module (crates-io))

(define-public crate-json-utils-0.2.7 (c (n "json-utils") (v "0.2.7") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "valico") (r "^3.0") (d #t) (k 0)))) (h "0zhc3vn2iwgzbwskfzz8h1h6kqf2zffb0iaaiq0xacfmrcp7pjnj")))

(define-public crate-json-utils-0.2.8 (c (n "json-utils") (v "0.2.8") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "valico") (r "^3.0") (d #t) (k 0)))) (h "0bs76pibsh5kcx2ayrhybyhki0klvs7hlvx0ssqgz2k4npl8jx1r")))

(define-public crate-json-utils-0.3.0 (c (n "json-utils") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "valico") (r "^3.1") (d #t) (k 0)))) (h "0ghnb6ihrkgyljjdglp814fpdr0m9dpj78b68bn0834sc4q4sci4")))

(define-public crate-json-utils-0.3.1 (c (n "json-utils") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "valico") (r "^3.1") (d #t) (k 0)))) (h "01hfyh9s4cwmxrv1gjm2damc33kh3h60v4awfii3xlq7xn9p9j9h")))

(define-public crate-json-utils-0.3.2 (c (n "json-utils") (v "0.3.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "valico") (r "^3.1") (d #t) (k 0)))) (h "0d5l0l3mjmjphg2369rn6sra2skyw7h3b61z02gi13fb8qrzy2j3")))

(define-public crate-json-utils-0.3.3 (c (n "json-utils") (v "0.3.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "valico") (r "^3.1") (d #t) (k 0)))) (h "1ji49xrkx2f14wvxk5lwr7k3cwvzlagi5lhbsfzrz8b21fs4691z")))

(define-public crate-json-utils-0.3.4 (c (n "json-utils") (v "0.3.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)) (d (n "valico") (r "^3.1") (d #t) (k 0)))) (h "078xj9x1lkld142jkj84401ivbrjshlyc7i2jmd0xaq044ynfcg3")))

(define-public crate-json-utils-0.3.5 (c (n "json-utils") (v "0.3.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "valico") (r "^3.1") (d #t) (k 0)))) (h "0jd33gw9i6qr5a3w42gdg25i568fv81ps5g232l8cf6p3r33w6y4")))

