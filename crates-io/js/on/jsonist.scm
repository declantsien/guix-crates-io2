(define-module (crates-io js on jsonist) #:use-module (crates-io))

(define-public crate-jsonist-0.0.1 (c (n "jsonist") (v "0.0.1") (h "0c4d1wjvlijpz83dvca7fi61g11bs3y6qfc9h0cdvdbkqzvj6nbg")))

(define-public crate-jsonist-0.0.2 (c (n "jsonist") (v "0.0.2") (h "1q868g91x2rg63wrvvwm1sghmdilsjxmbyzsx04775blhbjb1w9j")))

(define-public crate-jsonist-0.0.3 (c (n "jsonist") (v "0.0.3") (h "0v3ggwjjbz7xkyznd0gyx5q5l360il5iqhf1nfjaccdizlxwqxsb")))

