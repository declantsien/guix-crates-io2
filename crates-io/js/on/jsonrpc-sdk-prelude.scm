(define-module (crates-io js on jsonrpc-sdk-prelude) #:use-module (crates-io))

(define-public crate-jsonrpc-sdk-prelude-0.1.0 (c (n "jsonrpc-sdk-prelude") (v "0.1.0") (d (list (d (n "jsonrpc-core") (r "~10.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-macros") (r "= 0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1jq4kz4ngi208avids2qglp2cc7h3r49h0qdxyzh4ai139ha4k1v")))

(define-public crate-jsonrpc-sdk-prelude-0.1.1 (c (n "jsonrpc-sdk-prelude") (v "0.1.1") (d (list (d (n "jsonrpc-core") (r "~10.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-macros") (r "= 0.1.1") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1ab90mksminnxc4dyas81ivsp9lhbv7q6ij1x6k38k1rgjglimw4")))

(define-public crate-jsonrpc-sdk-prelude-0.1.2 (c (n "jsonrpc-sdk-prelude") (v "0.1.2") (d (list (d (n "jsonrpc-core") (r "~10.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-macros") (r "= 0.1.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1rsd53w03rrcbhrhzjkpbh3c65z1jz8rxdkb36w1yagscb5f595p")))

(define-public crate-jsonrpc-sdk-prelude-0.1.3 (c (n "jsonrpc-sdk-prelude") (v "0.1.3") (d (list (d (n "jsonrpc-core") (r "~10.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-macros") (r "= 0.1.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1s53ffmb00302w7n8w9hk3zghs8z35p1z4szmvqqknqfv3pfssgq")))

(define-public crate-jsonrpc-sdk-prelude-0.1.4 (c (n "jsonrpc-sdk-prelude") (v "0.1.4") (d (list (d (n "jsonrpc-core") (r "~10.1") (d #t) (k 0)) (d (n "jsonrpc-sdk-macros") (r "= 0.1.4") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "17xqrgwnd9iskk09gngfw56228pxdbjhajhfsk70xlkjkzbkms4p")))

