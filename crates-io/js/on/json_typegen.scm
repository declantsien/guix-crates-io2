(define-module (crates-io js on json_typegen) #:use-module (crates-io))

(define-public crate-json_typegen-0.1.0 (c (n "json_typegen") (v "0.1.0") (d (list (d (n "json_typegen_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "04g5qs4zh2v9x7cs1r6i50ldv72mjlmhlf1gcmpsvn3rn4nzhhvj")))

(define-public crate-json_typegen-0.1.1 (c (n "json_typegen") (v "0.1.1") (d (list (d (n "json_typegen_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "0fsf4dw0yz5ywppdj65aalapgbi7am61hkrxf0549jr0ddpa6lp1")))

(define-public crate-json_typegen-0.2.0 (c (n "json_typegen") (v "0.2.0") (d (list (d (n "json_typegen_derive") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)))) (h "0i4ay3k9rs3pbr4vwknd3kg4nd39vpsh7blaiip9dxmshvvpcf6q")))

(define-public crate-json_typegen-0.3.0 (c (n "json_typegen") (v "0.3.0") (d (list (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n7m7hmyx3x2mzimw3ysv2ydxvfwqz9kh297clhf6zvn64qbi3sq")))

(define-public crate-json_typegen-0.3.1 (c (n "json_typegen") (v "0.3.1") (d (list (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18hi1clj6nh69xyaxxmbmbdd6lg67a5wrl5hlggzhb71nmv8cdf3")))

(define-public crate-json_typegen-0.3.2 (c (n "json_typegen") (v "0.3.2") (d (list (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1a411p1xm2jjapadrrqfmbc9yw8svk3vivyxnxx4fr781ybdxwki")))

(define-public crate-json_typegen-0.3.3 (c (n "json_typegen") (v "0.3.3") (d (list (d (n "json_typegen_shared") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1raqb93n2hxyyl22kyjzq9iags7kdh8wmcg5fxcj2zgjhkjs97c5")))

(define-public crate-json_typegen-0.4.0 (c (n "json_typegen") (v "0.4.0") (d (list (d (n "json_typegen_shared") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dg2cqi2pq61566llxpph6v3sxa7rp2v1xgnsfyjr0d03h1g3g26")))

(define-public crate-json_typegen-0.4.1 (c (n "json_typegen") (v "0.4.1") (d (list (d (n "json_typegen_shared") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wphg037f3zlpc22dlg2racq399z2l0bsrxhbav2pingdw65iiym")))

(define-public crate-json_typegen-0.5.0 (c (n "json_typegen") (v "0.5.0") (d (list (d (n "json_typegen_shared") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0z8hibl2g7lybgy6ybh7zmblvvahv4j3yyvf3gyq587n61jc4p1h")))

(define-public crate-json_typegen-0.6.0 (c (n "json_typegen") (v "0.6.0") (d (list (d (n "json_typegen_shared") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cmxghg9rgcqr0fpxj42lr2i4rm733k3xbfspamnm0j3v6lxvjf9")))

(define-public crate-json_typegen-0.7.0 (c (n "json_typegen") (v "0.7.0") (d (list (d (n "json_typegen_shared") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0969y9gmlkfjbdr2xkyf4qn63xm8csc6ia2bzh0yvmxvgwv8hg1i")))

