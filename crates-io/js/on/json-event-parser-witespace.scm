(define-module (crates-io js on json-event-parser-witespace) #:use-module (crates-io))

(define-public crate-json-event-parser-witespace-0.1.0 (c (n "json-event-parser-witespace") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0vwhl85wyq3m36667nnsb7ys53gkmp41d9zr2cd3km3pwz1y3ba3")))

