(define-module (crates-io js on jsonice) #:use-module (crates-io))

(define-public crate-jsonice-0.1.0 (c (n "jsonice") (v "0.1.0") (d (list (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1yi4npfzi72b4mxw767cncjy67n424vscqkwgqv8n0b73drx8m2l")))

(define-public crate-jsonice-0.2.0 (c (n "jsonice") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0kwz889lag6n6hdz9w60rd6ssydz4h61c00qck82ir5qvqsb206r")))

(define-public crate-jsonice-0.2.1 (c (n "jsonice") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1i9fik4hw3rwq3l92cjzggn0nsm13j0y45gyghpnk1jjnmgixf40")))

