(define-module (crates-io js on json_comments) #:use-module (crates-io))

(define-public crate-json_comments-0.1.0 (c (n "json_comments") (v "0.1.0") (h "0zmg8jgyppvd7x9rfr6m5klca0vin5jsbxv7y2ah2v5hlmn0w6bl")))

(define-public crate-json_comments-0.2.0 (c (n "json_comments") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10v4c02hg7im4mii7rm2cbi2jcl5f4z5sjaq6j84kfrgfn4wc9db")))

(define-public crate-json_comments-0.2.1 (c (n "json_comments") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mapiispkg8i3zf99j6n023ay5al04a4zl3hmivlmfk8wfg47vj1")))

(define-public crate-json_comments-0.2.2 (c (n "json_comments") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0191qf6gaw3bcv13kx9xzwbfx71jv7gly5ds2l77bacvwpagxfwx")))

