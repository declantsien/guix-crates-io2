(define-module (crates-io js on jsonrpc-v1) #:use-module (crates-io))

(define-public crate-jsonrpc-v1-0.1.0 (c (n "jsonrpc-v1") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6") (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "0gzx4l6l55g3didpv6q248sffx0f6ziwxp7hq994wml2vyd3xibz") (f (quote (("ssl" "hyper/ssl") ("default" "ssl"))))))

