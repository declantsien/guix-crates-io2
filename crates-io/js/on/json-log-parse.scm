(define-module (crates-io js on json-log-parse) #:use-module (crates-io))

(define-public crate-json-log-parse-0.1.0 (c (n "json-log-parse") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h3qy39423is9zvqn72jkfsf9wjr0x4v92wxw0v876qhq0mar389") (y #t)))

(define-public crate-json-log-parse-0.1.1 (c (n "json-log-parse") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03gci309159ks0qcrd0n34sx9nhrrsd0xaql3gsvd62g1x810g8h")))

(define-public crate-json-log-parse-0.1.2 (c (n "json-log-parse") (v "0.1.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07qkvp82jk17vzq11si6bhzwzp2f35gcbmyripxs9p1dxykr5smd")))

(define-public crate-json-log-parse-0.2.0 (c (n "json-log-parse") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0habbdyx8wx7qaxisxmnis6p49a6zks797nv4fd6bzgc0ha4akz7")))

(define-public crate-json-log-parse-0.3.0 (c (n "json-log-parse") (v "0.3.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15qh3vpdpjb4dm2lc1rwl6bm0w7dj9xy2w4mrw01m4hggh7pan6c")))

