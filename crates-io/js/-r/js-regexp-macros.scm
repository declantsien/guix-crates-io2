(define-module (crates-io js -r js-regexp-macros) #:use-module (crates-io))

(define-public crate-js-regexp-macros-0.2.0 (c (n "js-regexp-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "1lj2g2jnkmg04qs59kkbdd04a3rsq09hprn5snyii898qdiczws3")))

(define-public crate-js-regexp-macros-0.2.1 (c (n "js-regexp-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0lkadbyn0k93rx4jkdc8fyil3h8bjdnj92lm0a916r9l1wmrzx28")))

