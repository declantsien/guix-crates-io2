(define-module (crates-io js -r js-regexp) #:use-module (crates-io))

(define-public crate-js-regexp-0.1.0 (c (n "js-regexp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "176myvvks4xc4czna6rdan1gyp9l33qjyy9gnvmmxhf8pw7yzj1h")))

(define-public crate-js-regexp-0.1.1 (c (n "js-regexp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "0qpvbr5lgcd8fahnwbmi1ycwvjhp49aj3w9x99v58bdqg0cr7mz8")))

(define-public crate-js-regexp-0.2.0 (c (n "js-regexp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "js-regexp-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "186j264a3w9hdwj9pqhpj0jmsd5nm9xkjbg1sh5cgdn0rf416ii9")))

(define-public crate-js-regexp-0.2.1 (c (n "js-regexp") (v "0.2.1") (d (list (d (n "js-regexp-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "0zq0sfy4d4inisz9rx56y0pvwnmh94xk6jdd2966rkrawpasgrq7")))

