(define-module (crates-io js -r js-resolve) #:use-module (crates-io))

(define-public crate-js-resolve-0.1.0 (c (n "js-resolve") (v "0.1.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0yb0c0rygkk7rpizapbr0cmcis9yil48l4rb3qyi36gs8sfz0lxk")))

(define-public crate-js-resolve-0.1.1 (c (n "js-resolve") (v "0.1.1") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "02fninaf61afbrj13wmd3id79vipy2qdpi0bwdbkknvwygzh665y")))

(define-public crate-js-resolve-0.1.2 (c (n "js-resolve") (v "0.1.2") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1i5xzv53ikyrcdr5q71q9ylzwl0m289sv18m4xa40dmfl03w9l70")))

(define-public crate-js-resolve-0.1.3 (c (n "js-resolve") (v "0.1.3") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "07x6gis9bx5swllkcj2v66gwrj533bpi687y4038n7l0iih2jv91")))

