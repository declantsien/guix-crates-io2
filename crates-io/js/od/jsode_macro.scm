(define-module (crates-io js od jsode_macro) #:use-module (crates-io))

(define-public crate-jsode_macro-0.1.0 (c (n "jsode_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "1k64s1ka0an1kd4bkh66gqcdb7crzbii7rcsrkp7jkxsiq1vb2m8")))

