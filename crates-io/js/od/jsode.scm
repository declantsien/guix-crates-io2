(define-module (crates-io js od jsode) #:use-module (crates-io))

(define-public crate-jsode-0.1.0 (c (n "jsode") (v "0.1.0") (d (list (d (n "jsode_macro") (r "^0.1") (d #t) (k 0)))) (h "1czp48yrdwsqj1svhs7imkddrn8qjj6wicjvsj7xjnf6sbc2aysd") (f (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.1.1 (c (n "jsode") (v "0.1.1") (d (list (d (n "jsode_macro") (r "^0.1") (d #t) (k 0)))) (h "0akripk28w9praza6zr7akvgx2j66m6q5ly35gyfdnan6pqb08wb") (f (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.1.2 (c (n "jsode") (v "0.1.2") (d (list (d (n "jsode_macro") (r "^0.1") (d #t) (k 0)))) (h "19y9ixjsm4qxhs9d2flasz1l9pan4s5ya2qz18drwr88k9vxk228") (f (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.1.3 (c (n "jsode") (v "0.1.3") (d (list (d (n "jsode_macro") (r "^0.1") (d #t) (k 0)))) (h "0yv1rkpk1h17vqp7qb5s75fba75gd966cdxms4jaypqpnlf7f9bb") (f (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.2.0 (c (n "jsode") (v "0.2.0") (d (list (d (n "jsode_macro") (r "^0.1") (d #t) (k 0)))) (h "1h5v8wrap16fjiasr82r4q7lsw694c4nk2kvk4wch8y0hgymk0fw") (f (quote (("unstable") ("macro") ("default" "macro"))))))

