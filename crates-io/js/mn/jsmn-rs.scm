(define-module (crates-io js mn jsmn-rs) #:use-module (crates-io))

(define-public crate-jsmn-rs-0.1.0 (c (n "jsmn-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "11cprbicfxkbzd0rhhrv6fzzs77xxdnyjgf8xl4w5rygwayjklmp") (f (quote (("strict") ("parent-links")))) (l "jsmn")))

(define-public crate-jsmn-rs-0.2.0 (c (n "jsmn-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "1y11kr0gik38z1kbp51799w0fpvbj014xdxr2w5d6iywf07rqm7q") (f (quote (("strict") ("parent-links")))) (l "jsmn")))

