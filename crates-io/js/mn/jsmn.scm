(define-module (crates-io js mn jsmn) #:use-module (crates-io))

(define-public crate-jsmn-0.1.0 (c (n "jsmn") (v "0.1.0") (h "0cq7h2j5wnkj190a6gwwqjm1lgiyibpl176j8585ivr0d1knchwa")))

(define-public crate-jsmn-0.1.1 (c (n "jsmn") (v "0.1.1") (h "1vngw5yln39h7npl3z63l7jgkchbpd59zq733jr3a3i07is8c152")))

(define-public crate-jsmn-0.1.2 (c (n "jsmn") (v "0.1.2") (h "1akwmfsmzspyd56bph45w80anmlhp7pvdxnnkijzly35mvkql573")))

(define-public crate-jsmn-0.1.3 (c (n "jsmn") (v "0.1.3") (h "1szngbxmfpa0k20h904vv5ai7wjjl7gjfxi813sy82fy3gp6ggaa")))

(define-public crate-jsmn-0.1.4 (c (n "jsmn") (v "0.1.4") (h "09s2iyawry14rwrmdrv1qnvnzcpx06zkfhinfs2v6m4vf0da579m")))

(define-public crate-jsmn-0.1.5 (c (n "jsmn") (v "0.1.5") (h "0rpyj2hcy72y5b7ypwz5j350bqfgf7ihijzwhsfmld91dm75yd03")))

