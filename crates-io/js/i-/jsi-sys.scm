(define-module (crates-io js i- jsi-sys) #:use-module (crates-io))

(define-public crate-jsi-sys-0.3.0-alpha.1 (c (n "jsi-sys") (v "0.3.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0slpc89drnfdba21kwwzz69g2ahmb0xk3i0izlqksfagv0ph2xi5") (l "jsi")))

(define-public crate-jsi-sys-0.3.0-alpha.2 (c (n "jsi-sys") (v "0.3.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0dg1y769lhrqy3g9z5kkm9ph1r079wvz36hlxjh9yqs7dj87j0q2") (l "jsi")))

(define-public crate-jsi-sys-0.3.0-alpha.4 (c (n "jsi-sys") (v "0.3.0-alpha.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "16j0p34gv8zwm5m1zagr2pkvnv7vw7krpy78zrixwmdy4pj0s5i7") (l "jsi")))

(define-public crate-jsi-sys-0.3.0-alpha.5 (c (n "jsi-sys") (v "0.3.0-alpha.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0pva0ls8qhbhgfwxn3051xk9jrwlz301wcjqz94dzqh5sxqhc832") (l "jsi")))

