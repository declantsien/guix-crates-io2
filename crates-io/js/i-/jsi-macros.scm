(define-module (crates-io js i- jsi-macros) #:use-module (crates-io))

(define-public crate-jsi-macros-0.3.0 (c (n "jsi-macros") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "quote"))) (d #t) (k 0)))) (h "1s910s89g4hci1shlpxz0p5r31f9bvvb4rlr2vr3vcxpa3gc2h14") (f (quote (("host-object-trace")))) (y #t)))

(define-public crate-jsi-macros-0.3.1-alpha.1 (c (n "jsi-macros") (v "0.3.1-alpha.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "quote"))) (d #t) (k 0)))) (h "0mnfsx8jraf397achb7swb0yg28ca1vm1r5gv3p3dm1m8v93gyl2") (f (quote (("host-object-trace")))) (y #t)))

(define-public crate-jsi-macros-0.3.1-alpha.4 (c (n "jsi-macros") (v "0.3.1-alpha.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "quote"))) (d #t) (k 0)))) (h "06i6wlj3gr3jl14v7k6m6zrmmxfyla7yx8c7vg8b2w3qsfrz5ska") (f (quote (("host-object-trace"))))))

(define-public crate-jsi-macros-0.3.1-alpha.5 (c (n "jsi-macros") (v "0.3.1-alpha.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "quote"))) (d #t) (k 0)))) (h "1ldj5cr5w4prpl37jr990j0ncxqkh6jrn0d1jnszg6zs9r5bvc3z") (f (quote (("host-object-trace"))))))

