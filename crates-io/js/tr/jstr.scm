(define-module (crates-io js tr jstr) #:use-module (crates-io))

(define-public crate-jstr-0.1.0 (c (n "jstr") (v "0.1.0") (h "07rz9qmqp4kqqshd41chqf8bvcf6h5m9d6ycz5chyha14ryy9pxr")))

(define-public crate-jstr-0.1.1 (c (n "jstr") (v "0.1.1") (h "0hkq11mx18f9cjqfx7068n7009wra6vfiw3blhvaa1jjq03ba8hl")))

