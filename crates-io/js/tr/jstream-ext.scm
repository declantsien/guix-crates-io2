(define-module (crates-io js tr jstream-ext) #:use-module (crates-io))

(define-public crate-jstream-ext-0.1.0 (c (n "jstream-ext") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1863qdgmhmwricm43mdid9kd4plf0jislkk96m59w14w1wg6lqb2") (f (quote (("sink") ("default" "sink"))))))

