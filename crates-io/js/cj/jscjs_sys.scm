(define-module (crates-io js cj jscjs_sys) #:use-module (crates-io))

(define-public crate-jscjs_sys-0.0.1 (c (n "jscjs_sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "clang") (r "^1.0.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "libz-sys") (r "^1.1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 1)))) (h "03w4wmgc5v5gig4936i8h9rygva14w576qzd0gmjd7wx0kr7mp3z") (y #t)))

(define-public crate-jscjs_sys-0.0.2 (c (n "jscjs_sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "0s83s15r6xc92p14ipf9d1v2smgyryaib1pcmmza7p0j9222nqkd") (l "jscjs")))

(define-public crate-jscjs_sys-0.0.3 (c (n "jscjs_sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1jw133gbx25bl9zksq5m8y1y45pfzf6by1rn0g9ss010vmsbmpf0") (l "glib-2.0")))

