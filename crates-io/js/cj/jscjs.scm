(define-module (crates-io js cj jscjs) #:use-module (crates-io))

(define-public crate-jscjs-0.0.1 (c (n "jscjs") (v "0.0.1") (d (list (d (n "jscjs_sys") (r "^0.0.1") (d #t) (k 0)) (d (n "jscjs_sys") (r "^0.0.1") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "10h1m61gq5zh9niih30y4qxxjq8qkk00r3990v2ivfkxd2sgv8l9")))

(define-public crate-jscjs-0.0.2 (c (n "jscjs") (v "0.0.2") (d (list (d (n "jscjs_sys") (r "^0.0.1") (d #t) (k 0)) (d (n "jscjs_sys") (r "^0.0.1") (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "03b8bs8bzmnzlf4nid2xgkhxz77gn8wba668padk0gk0j10qg5jd")))

