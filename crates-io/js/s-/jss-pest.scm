(define-module (crates-io js s- jss-pest) #:use-module (crates-io))

(define-public crate-jss-pest-0.4.0 (c (n "jss-pest") (v "0.4.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "14x0ynh9h8v64fdknmfcsj2wa24a30h1fjq447xwy9hg21rj183d")))

(define-public crate-jss-pest-0.4.1 (c (n "jss-pest") (v "0.4.1") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1vl57awk80naj7hihhqi3ihvjfsypjwfsi1j4n3szvp6ka3v9vir")))

(define-public crate-jss-pest-0.4.2 (c (n "jss-pest") (v "0.4.2") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "11fq9m7azm3dkn5b70cciipyrvsa6ki0jjm1ilr4kvj7g9nm4q25")))

