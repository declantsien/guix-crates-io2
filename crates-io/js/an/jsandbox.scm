(define-module (crates-io js an jsandbox) #:use-module (crates-io))

(define-public crate-jsandbox-0.1.0 (c (n "jsandbox") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.165.0") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.91.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (d #t) (k 0)))) (h "11kj0086nqak1znsp7a0z25ldlwd4lwajnv9rxajiz3j4f4vsp9c")))

(define-public crate-jsandbox-0.2.0 (c (n "jsandbox") (v "0.2.0") (d (list (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.126.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (d #t) (k 0)))) (h "0p9jgqrqhjjv2c28kx0cw7f8gb7i8r7x75n08sy5cb6zlx8w2abf")))

(define-public crate-jsandbox-0.3.0 (c (n "jsandbox") (v "0.3.0") (d (list (d (n "deno_core") (r "^0.204.0") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.126.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (d #t) (k 0)))) (h "1ddcnjrk6sl0n9dddg1ibw9q3c4462dzd4im8swq7rzjljxxq2b4")))

