(define-module (crates-io js -c js-canvas-rendering-context-2d) #:use-module (crates-io))

(define-public crate-js-canvas-rendering-context-2d-0.1.0 (c (n "js-canvas-rendering-context-2d") (v "0.1.0") (h "0bgxyxm3b5pw6g6n0nm0m0786jw9liaxbw1s1lbs7b4bxs0jknih")))

(define-public crate-js-canvas-rendering-context-2d-0.1.1 (c (n "js-canvas-rendering-context-2d") (v "0.1.1") (h "1hmqlhr668dfminpsjpc12lnd278hy8b530whfxy87b318izmx4s")))

(define-public crate-js-canvas-rendering-context-2d-0.2.0 (c (n "js-canvas-rendering-context-2d") (v "0.2.0") (h "09qsmha0p0z623ywp9j06zyv0bbnx9ckk5d86y1lsnxjksv6inz7")))

(define-public crate-js-canvas-rendering-context-2d-0.3.0 (c (n "js-canvas-rendering-context-2d") (v "0.3.0") (h "1rm9rraa5b0r9l0762605wbhpj317bgj50mz2w3swi7fdrjdqqsz")))

