(define-module (crates-io js #{8c}# js8call_lib) #:use-module (crates-io))

(define-public crate-js8call_lib-0.0.1 (c (n "js8call_lib") (v "0.0.1") (d (list (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1kskz3ziryk1p8x60nw2shmxkyz1180957xhm9lar5qajixhy7cm")))

(define-public crate-js8call_lib-0.0.2 (c (n "js8call_lib") (v "0.0.2") (d (list (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "14gfcwx8yjn1m3lixrs5qgm1adm91fn9fj1w0qgg30dz7yk3b1am")))

