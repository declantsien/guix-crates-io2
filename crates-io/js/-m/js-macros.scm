(define-module (crates-io js -m js-macros) #:use-module (crates-io))

(define-public crate-js-macros-0.1.0 (c (n "js-macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "123xm0w0gc1pp32h4kb1hkygldgqb1kshw23wm7xdk41xbr2c6yw")))

(define-public crate-js-macros-0.1.1 (c (n "js-macros") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "18ss0gwr7xr3r2caghgs2as18j8vld02rlifv8slz6f25hl01nm6")))

(define-public crate-js-macros-0.1.2 (c (n "js-macros") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1dql6ww5n1wspn0rx5pqbjbd00bmyz0zw3fb8zcibbf2nvjq7nnc")))

(define-public crate-js-macros-0.1.3 (c (n "js-macros") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "052gxcwhdhjz0hal5b5fdk30vwm7l584vkl3ggc7f986yki94bma")))

(define-public crate-js-macros-0.1.4 (c (n "js-macros") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "12hjv7ykkxxzag4rpbg35i7j8gamkaayxhbb39knsw379vagj6v4")))

(define-public crate-js-macros-0.1.5 (c (n "js-macros") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1aqadxkm7r3y0hzmiwnwi2a07336fp05kzfd3lb0kc3a554yknzg")))

