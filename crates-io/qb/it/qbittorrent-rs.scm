(define-module (crates-io qb it qbittorrent-rs) #:use-module (crates-io))

(define-public crate-qbittorrent-rs-0.1.3 (c (n "qbittorrent-rs") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "cookies" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0m6mw5a8qr4pfqwj9dls9l5vbfxv662914mrhdlh8mc61s08zsvh")))

