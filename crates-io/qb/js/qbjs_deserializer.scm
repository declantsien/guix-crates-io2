(define-module (crates-io qb js qbjs_deserializer) #:use-module (crates-io))

(define-public crate-qbjs_deserializer-0.0.1 (c (n "qbjs_deserializer") (v "0.0.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ijakwc0lq4rfmin9n6pflb60wz8mc0dss4vlwxf196q1k9cs1na")))

(define-public crate-qbjs_deserializer-0.0.2 (c (n "qbjs_deserializer") (v "0.0.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pr7mwllszs3nqjh0gzzpb497i4yg187fp6wcgjky07k1x4jnxvn")))

(define-public crate-qbjs_deserializer-0.0.3 (c (n "qbjs_deserializer") (v "0.0.3") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "191gm0blkd051prgqmc8ch938zhpdn0k7phph0z3f0nwlw0yxic0")))

(define-public crate-qbjs_deserializer-0.0.4 (c (n "qbjs_deserializer") (v "0.0.4") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jh3mmfzr7cba10kq14nkvpdq8rg2klw6yzb4mdz0wbq9fhsh5y4")))

(define-public crate-qbjs_deserializer-0.0.5 (c (n "qbjs_deserializer") (v "0.0.5") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07z98cny7fr5xd5l8a9mq641cv8s4fy7zawa5wibq2qbfzaa1yvp")))

