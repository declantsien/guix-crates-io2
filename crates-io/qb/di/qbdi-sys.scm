(define-module (crates-io qb di qbdi-sys) #:use-module (crates-io))

(define-public crate-qbdi-sys-0.1.0 (c (n "qbdi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0zq45jrcrcnq91b40j134zrbgaq9gawz3k1w844l8f93ian6jpw7") (y #t)))

(define-public crate-qbdi-sys-0.1.1 (c (n "qbdi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "05jy0hp9lhfpzh4avq3n7zc44vw1ys14zc61j1g069n2a11cr17g") (y #t)))

(define-public crate-qbdi-sys-0.1.2 (c (n "qbdi-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0qfjq875pf1dicjkav888bgspdx50z8mkpkxz9x01z8c7f50a86h")))

