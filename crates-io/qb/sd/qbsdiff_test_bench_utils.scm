(define-module (crates-io qb sd qbsdiff_test_bench_utils) #:use-module (crates-io))

(define-public crate-qbsdiff_test_bench_utils-0.1.0 (c (n "qbsdiff_test_bench_utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "globwalk") (r "^0.7") (d #t) (k 0)) (d (n "qbsdiff") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1qih45mgm6hwa4ysrzh0cgc1g4fnnf715n6c0jl71bbiix6lfni4")))

(define-public crate-qbsdiff_test_bench_utils-0.1.1 (c (n "qbsdiff_test_bench_utils") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "globwalk") (r "^0.7") (d #t) (k 0)) (d (n "qbsdiff") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "16r542h30lpm2gq2954hw06dh9wiwkidm3w4bnzx0k9fm3px7bdv")))

(define-public crate-qbsdiff_test_bench_utils-0.1.2 (c (n "qbsdiff_test_bench_utils") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "globwalk") (r "^0.8") (d #t) (k 0)) (d (n "qbsdiff") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02fwhvydh2dhv30jq01yvsngync8nkzgw4f68z6ab269vjkspmsb")))

(define-public crate-qbsdiff_test_bench_utils-0.1.3 (c (n "qbsdiff_test_bench_utils") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "globwalk") (r "^0.9") (d #t) (k 0)) (d (n "qbsdiff") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bwjp3j6whwqr1rhc3lhx3ndggaz1rqf8i5il9rg4kjxv0kpq878")))

