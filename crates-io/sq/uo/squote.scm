(define-module (crates-io sq uo squote) #:use-module (crates-io))

(define-public crate-squote-0.1.0 (c (n "squote") (v "0.1.0") (h "0dmrzhp39wm7ml2g7rbr51a257h44n27m738i2v1idcd6nb36gay")))

(define-public crate-squote-0.1.1 (c (n "squote") (v "0.1.1") (h "12jc1kqivfr0dcvbig7zcvd9y6mc9i44w2rba9yjmv8dmxkdd9bl")))

(define-public crate-squote-0.1.2 (c (n "squote") (v "0.1.2") (h "0zcz8irsmljmnn9jl6vhv35wb8jgnrkx4akdg7m5a94ys1zz3k0z")))

