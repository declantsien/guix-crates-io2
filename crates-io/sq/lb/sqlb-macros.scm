(define-module (crates-io sq lb sqlb-macros) #:use-module (crates-io))

(define-public crate-sqlb-macros-0.0.4 (c (n "sqlb-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "062sddql90izjq8ww2mrkgbj7mvrraj66ywnymcx8ggpsxgb5136")))

(define-public crate-sqlb-macros-0.0.5 (c (n "sqlb-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d20czamjghkvpd0d0vzvl2fzkd4n8hb0jmhc4w24f2a2dmj3w98")))

(define-public crate-sqlb-macros-0.3.0 (c (n "sqlb-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z5pbpz7j505s6pfw2x1ap93rbvw3vsq6b89pf66p94kyfs5plf0")))

(define-public crate-sqlb-macros-0.3.1 (c (n "sqlb-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "18y5086if6plg8a58yybfvmcqpp4y9lr9rp43mrjllvypl8acrm5")))

(define-public crate-sqlb-macros-0.3.2 (c (n "sqlb-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0fb7345ds1y8k073cd4bw0wxp463xq4zdgd72rcw1xl1zp2ki2b2")))

(define-public crate-sqlb-macros-0.4.0 (c (n "sqlb-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1jjdpks4hdnbdslnccks6h76x44d5wl6pcqcv03wq7nphibcd4k9")))

(define-public crate-sqlb-macros-0.5.0-alpha.1 (c (n "sqlb-macros") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "13z475dgkj6vdjnp303441kky8fh0v6c9rqbdvrcp846q579b60p")))

