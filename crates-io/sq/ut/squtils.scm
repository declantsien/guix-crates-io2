(define-module (crates-io sq ut squtils) #:use-module (crates-io))

(define-public crate-squtils-0.1.0 (c (n "squtils") (v "0.1.0") (d (list (d (n "convert_case") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sea-query") (r "^0.23") (f (quote ("derive"))) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0qvvsm53vdr18lpdx8g3sdy1p9cdj3mwh1jsxqkfhp3b9ghz523h")))

