(define-module (crates-io sq is sqisign) #:use-module (crates-io))

(define-public crate-sqisign-0.1.0 (c (n "sqisign") (v "0.1.0") (h "0kmrc32ckfzsln93jjsggv38c4s3g7j13srq4xs7a8vwlr7zc0xx")))

(define-public crate-sqisign-0.1.1 (c (n "sqisign") (v "0.1.1") (h "0yg89y3ggvyw0adps9cfmpcc52p150rd5agqwc7b9lrkq1711nnl")))

