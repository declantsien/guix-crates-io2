(define-module (crates-io sq a- sqa-ffmpeg) #:use-module (crates-io))

(define-public crate-sqa-ffmpeg-0.2.0 (c (n "sqa-ffmpeg") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "ffmpeg-sys") (r "^3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "sample") (r "^0.6.2") (d #t) (k 0)))) (h "12glg23sfwqq71d4w9gx8hzils46rwmab45ff1cckrkvvcswcdzr")))

(define-public crate-sqa-ffmpeg-0.2.1 (c (n "sqa-ffmpeg") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "ffmpeg-sys") (r "^3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "sample") (r "^0.6.2") (d #t) (k 0)))) (h "0313m7fbf8xjcw4yla34h02909fc5fjpjl54c14l0dw668gw8dr3")))

(define-public crate-sqa-ffmpeg-0.2.2 (c (n "sqa-ffmpeg") (v "0.2.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "ffmpeg-sys") (r "^3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "sample") (r "^0.6.2") (d #t) (k 0)))) (h "15mabrvxq80hzmxpl7xjcd7av8m7v4k09r3wg2jlsz9gb9kizgli")))

