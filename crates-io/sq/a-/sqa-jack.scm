(define-module (crates-io sq a- sqa-jack) #:use-module (crates-io))

(define-public crate-sqa-jack-0.1.0 (c (n "sqa-jack") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09d6xd7j2krra37l2fb2whwrgk7kriy1jrjgmsnavpcjc7zcv3m7")))

(define-public crate-sqa-jack-0.1.1 (c (n "sqa-jack") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05308nkmb3ijkf6bzw29m3w78mc4mi1yiayjan1xm1r25ih07fap")))

(define-public crate-sqa-jack-0.2.0 (c (n "sqa-jack") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00yrilmnsggrvls9g91iqy1wdnlxsw55nz4pnv075nnfznvyzhv8")))

(define-public crate-sqa-jack-0.3.0 (c (n "sqa-jack") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g12v7zzn927dj010vzp24mwzx4c36fvl2ik12d9g6ba6qzrch57")))

(define-public crate-sqa-jack-0.4.0 (c (n "sqa-jack") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mb4i50hill84m49wz3gg1r229i8jinrgrzlsyjss384jy34srlw") (f (quote (("test-xruns") ("default" "test-xruns"))))))

(define-public crate-sqa-jack-0.5.0 (c (n "sqa-jack") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mqpz841zvpbk4wwkc71pjr6q5sdrvm6lvb7xwdifznsggdv6g4p") (f (quote (("test-xruns") ("default" "test-xruns"))))))

(define-public crate-sqa-jack-0.5.1 (c (n "sqa-jack") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s9vlhwywf9r9bkmll39qmd034pypqpd15dycgp9lfdfry9pb6m8") (f (quote (("test-xruns") ("default" "test-xruns"))))))

(define-public crate-sqa-jack-0.5.2 (c (n "sqa-jack") (v "0.5.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06sd64i7zcna7mdb297dcipxk1y644ma46jj5bld8rz47a5x0mjr") (f (quote (("test-xruns") ("default" "test-xruns"))))))

(define-public crate-sqa-jack-0.6.0 (c (n "sqa-jack") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x5x4pvz6h4ippd0yh5cw7lac4ly47aiwqf18j5ysj8gcqc57p67") (f (quote (("test-xruns") ("default" "test-xruns"))))))

(define-public crate-sqa-jack-0.6.1 (c (n "sqa-jack") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0470asvfl90p5cspwxjvcr3l0hsvg4f7hprfz9vv1zv9syhqpzng") (f (quote (("test-xruns") ("default" "test-xruns"))))))

