(define-module (crates-io sq a- sqa-bounded-spsc-queue) #:use-module (crates-io))

(define-public crate-sqa-bounded-spsc-queue-0.2.0 (c (n "sqa-bounded-spsc-queue") (v "0.2.0") (h "0kd00dcnflvqy6n6w1v0dqkciwxra3lb7i88c37mzb2vxar5ha64") (f (quote (("nightly") ("default"))))))

(define-public crate-sqa-bounded-spsc-queue-0.2.1 (c (n "sqa-bounded-spsc-queue") (v "0.2.1") (h "00mr472rhndlgjd6d2i5sqznx93cw1pqlfbdaky9zhazic14bfg0") (f (quote (("nightly") ("default"))))))

