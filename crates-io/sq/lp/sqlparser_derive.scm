(define-module (crates-io sq lp sqlparser_derive) #:use-module (crates-io))

(define-public crate-sqlparser_derive-0.1.0 (c (n "sqlparser_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02wld3mr7h47rxz7grx26ww0qjsq9ahnicygy1q04hdig82xy96p")))

(define-public crate-sqlparser_derive-0.1.1 (c (n "sqlparser_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07knj4cvqd9r7jb7b6fzdifxipabv34bnzbcw1x7yk1n9b5pbzjm")))

(define-public crate-sqlparser_derive-0.2.0 (c (n "sqlparser_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qdxdiq82i1zwkzgsg2alsq9lm0lc1wqc4cbgyj366qasmbcn1w1")))

(define-public crate-sqlparser_derive-0.2.1 (c (n "sqlparser_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07q43xbbw4k8x4fzaj8nmms34vplj15dj8vr7q08gyhfvqfjx71y")))

(define-public crate-sqlparser_derive-0.2.2 (c (n "sqlparser_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("printing" "parsing" "derive" "proc-macro"))) (k 0)))) (h "0m05d4cxcsk1ljgy8zx79dibq62pdfbgp4zmfm9z2r2ma62y3ch1")))

