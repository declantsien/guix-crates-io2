(define-module (crates-io sq lp sqlpop) #:use-module (crates-io))

(define-public crate-sqlpop-0.1.0 (c (n "sqlpop") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.12.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.0") (d #t) (k 0)))) (h "0glzfqf8cm5ff3bs4gkrli40h1kvar1ss65x05zw4n7v4afkd4pc")))

(define-public crate-sqlpop-0.2.0 (c (n "sqlpop") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.12.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.0") (d #t) (k 0)))) (h "12d7595wc9ac3nwxs5rh554s1h2b3gmyvy7x9k885n9g1y5whpan")))

(define-public crate-sqlpop-0.3.0 (c (n "sqlpop") (v "0.3.0") (d (list (d (n "lalrpop") (r "^0.12") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12") (d #t) (k 0)))) (h "1wyhgmw3p3kxdxj3ladffk3ay38xi14b2phrcly1jmfnbl7vnmyq")))

