(define-module (crates-io sq lp sqlparser-lance) #:use-module (crates-io))

(define-public crate-sqlparser-lance-0.32.0 (c (n "sqlparser-lance") (v "0.32.0") (d (list (d (n "bigdecimal") (r "^0.3") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sqlparser_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)))) (h "05val58hkqgr069233qgsdhikyccgdv22anj4k1r8d8sm3a6js4d") (f (quote (("visitor" "sqlparser_derive") ("std") ("json_example" "serde_json" "serde") ("default" "std"))))))

