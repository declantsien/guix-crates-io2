(define-module (crates-io sq lp sqlplannertest) #:use-module (crates-io))

(define-public crate-sqlplannertest-0.0.0 (c (n "sqlplannertest") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1dixlxqw3m2jhkh1ihka1f4kr4ajnas8l42d41kmzd0vgs75rns8")))

(define-public crate-sqlplannertest-0.1.0 (c (n "sqlplannertest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "fs"))) (d #t) (k 0)))) (h "0ihxklpirlb1hcy90n4ma8vnbs5l2ql0n7rvb1h66dyiccg62lya")))

