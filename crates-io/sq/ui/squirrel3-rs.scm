(define-module (crates-io sq ui squirrel3-rs) #:use-module (crates-io))

(define-public crate-squirrel3-rs-0.1.0 (c (n "squirrel3-rs") (v "0.1.0") (h "161k1iqk9mamzx2jrskazcgvbykq2qhdlrcyba8qdx7rc2w9h111")))

(define-public crate-squirrel3-rs-0.1.1 (c (n "squirrel3-rs") (v "0.1.1") (h "1834hgb26cf4mldhl5hq030mlnnmhzhj73vck8pgaqwkmsw8hppp")))

(define-public crate-squirrel3-rs-0.1.2 (c (n "squirrel3-rs") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "wyhash") (r "^0.5.0") (d #t) (k 2)))) (h "18bmvdrymwx7y0krc9jxz24i72ph10s00kx3w2gahzwqf3ffc93n")))

