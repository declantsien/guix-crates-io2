(define-module (crates-io sq ui squirrel-rng) #:use-module (crates-io))

(define-public crate-squirrel-rng-0.1.1 (c (n "squirrel-rng") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "09jdcxk7fi2hq37s42v7iv716709axwyi2lgfg7512ia6fn77ajy")))

(define-public crate-squirrel-rng-0.2.0 (c (n "squirrel-rng") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "19mksikwvncsfszwa2215sq9illm9m4r2x0d9a7hk17qhjdfkbzk")))

(define-public crate-squirrel-rng-0.2.1 (c (n "squirrel-rng") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0panjb949m6i0ki3jqxaxhv0a95zrqs0nkjnqagz8kvjpq06dwrd")))

(define-public crate-squirrel-rng-0.2.2 (c (n "squirrel-rng") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "071zks4k8n4nngxdjff770sb3zhfsgxi1pahpw04v127w0jyih2x")))

