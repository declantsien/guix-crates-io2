(define-module (crates-io sq ui squirrel-sys) #:use-module (crates-io))

(define-public crate-squirrel-sys-0.1.0 (c (n "squirrel-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08v6n8ky6ynjdwfd7whh3iak2lqn42zk241ym626naljh760maac")))

(define-public crate-squirrel-sys-0.1.1 (c (n "squirrel-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1c1vr1kybijriqx2pcfrhjvblwxpn34kvwx4w5f12r4qp66ad96h")))

(define-public crate-squirrel-sys-0.2.0 (c (n "squirrel-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "057yjrryx7c6vj5vipkcyb50zwjwx4xbrdjsddbrdkh3wljc98wk")))

(define-public crate-squirrel-sys-0.2.1 (c (n "squirrel-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "04lxgrgrxbyb1rs1rxrlc6qfic6lpi33qyh7mixksnznr9f27jwl")))

