(define-module (crates-io sq ui squish_cli) #:use-module (crates-io))

(define-public crate-squish_cli-1.0.0 (c (n "squish_cli") (v "1.0.0") (d (list (d (n "ddsfile") (r "^0.2.3") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.1.15") (d #t) (k 0)) (d (n "png") (r "^0.12.0") (d #t) (k 0)) (d (n "squish") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "17b686ms5gxsn4xc7wz3xzqb1sw1x74wwr5qyypv1hx0xwx0izis")))

(define-public crate-squish_cli-2.0.0-beta1 (c (n "squish_cli") (v "2.0.0-beta1") (d (list (d (n "ddsfile") (r "^0.4.0") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.1.18") (d #t) (k 0)) (d (n "png") (r "^0.17.0") (d #t) (k 0)) (d (n "squish") (r "^2.0.0-beta1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1vwyg0cilly6zddi1iyp18viylmd4qa3sfrjvk3qxsnhb8704kbp") (f (quote (("rayon" "squish/rayon") ("default" "rayon"))))))

