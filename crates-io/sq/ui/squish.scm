(define-module (crates-io sq ui squish) #:use-module (crates-io))

(define-public crate-squish-1.0.0 (c (n "squish") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "1cmfylif0zj294jxscaf4mhpqm2r67cwi1g6q4f168ps1ch2qylx")))

(define-public crate-squish-2.0.0-beta1 (c (n "squish") (v "2.0.0-beta1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1644353n28qlicmzs9an5qr9ynm7lmwwfijniw6sazl58gxpzq5l")))

