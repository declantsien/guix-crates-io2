(define-module (crates-io sq ui squid) #:use-module (crates-io))

(define-public crate-squid-0.0.0 (c (n "squid") (v "0.0.0") (h "0vz4811ylhqnadjgr9f47n9x2s6ksi3lyi3n3g5wpwcz5lnz0zm2")))

(define-public crate-squid-0.0.1 (c (n "squid") (v "0.0.1") (h "14fa6davgigsbpaqvmqqnps929732q0ma6ckrk5dfjrsd1fia4f6")))

