(define-module (crates-io sq ui squirtle) #:use-module (crates-io))

(define-public crate-squirtle-0.1.0 (c (n "squirtle") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fjqjfzpjl4ij2zhla1s6b24lkyyil5y7hn727ifycql9bdj8k0w")))

(define-public crate-squirtle-0.2.0 (c (n "squirtle") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1za1vpcwpvkivmia9ahw3dinwdvdnizkl2f0hilk1xmjwgnbi30a")))

