(define-module (crates-io sq ui squishyid) #:use-module (crates-io))

(define-public crate-squishyid-0.1.0 (c (n "squishyid") (v "0.1.0") (h "1drbiz9q5lj1q22vyh84fp9mz9mlqqs9n78lk014v5rvc0p06m3k")))

(define-public crate-squishyid-0.1.1 (c (n "squishyid") (v "0.1.1") (h "0b0h6wd2mkdf5gh8xan98vkfiv8a71rwyn7g5v65lsbqd68pv1r6")))

