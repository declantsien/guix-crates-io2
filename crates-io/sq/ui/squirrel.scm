(define-module (crates-io sq ui squirrel) #:use-module (crates-io))

(define-public crate-squirrel-0.1.0 (c (n "squirrel") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "0z4c1djfp8jycragb5s1imlh44w8q2j3z3fwqckc2n2dnhb9wp3i") (f (quote (("use_unicode") ("use_double") ("default"))))))

