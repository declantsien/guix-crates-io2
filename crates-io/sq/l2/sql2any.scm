(define-module (crates-io sq l2 sql2any) #:use-module (crates-io))

(define-public crate-sql2any-0.1.2 (c (n "sql2any") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-map") (r "^2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio-rustls" "macros" "chrono" "rust_decimal" "postgres" "mysql"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "058gnclqq3bzw7d7xf4p4z4hka4s6ggpszl7nbmq8ljqyyf1akv3")))

