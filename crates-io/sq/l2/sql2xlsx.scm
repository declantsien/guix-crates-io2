(define-module (crates-io sq l2 sql2xlsx) #:use-module (crates-io))

(define-public crate-sql2xlsx-0.1.0 (c (n "sql2xlsx") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0wml42zcw2hklvdrnwk0bsp97c2c9llyw5sak9dfyp7cwc0p3fp8")))

