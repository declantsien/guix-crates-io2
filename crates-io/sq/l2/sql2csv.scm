(define-module (crates-io sq l2 sql2csv) #:use-module (crates-io))

(define-public crate-sql2csv-0.1.0 (c (n "sql2csv") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "postgres") (r "^0.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0c5rz4mpsipg8yxyi415a02n7g2l94i4aps5h0zfphlxi9ylwx6s")))

(define-public crate-sql2csv-0.1.1 (c (n "sql2csv") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "postgres") (r "^0.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "11dy9552jdlnizar3ib3x1dnjfqz2d3gahia5206l925rxa8rzf0")))

(define-public crate-sql2csv-0.2.0 (c (n "sql2csv") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "postgres") (r "^0.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1w8p4w95w70ydyydzyw6wvynbgvdvc0rsg49alpm45gahig0bmhh")))

(define-public crate-sql2csv-0.3.0 (c (n "sql2csv") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1dcadqg61alxx2jgmfy6wxqcsc483sfd975nzw74laxdkv4agaj4")))

(define-public crate-sql2csv-0.3.1 (c (n "sql2csv") (v "0.3.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1nkgzslq1hbffskyn429s7i4yzblkb4yg5b289a8lkyf2sv24v17")))

(define-public crate-sql2csv-0.4.0 (c (n "sql2csv") (v "0.4.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "050bn9zx06lc8jagqvps6pf7lnznw9s8160wlhkwh3jlz01h2s20")))

