(define-module (crates-io sq sh sqsh) #:use-module (crates-io))

(define-public crate-sqsh-0.1.0 (c (n "sqsh") (v "0.1.0") (h "0j7fvfx69h89s340g2a3xx0hd2i77qmysx2m2hssdr9fxx6pnczf")))

(define-public crate-sqsh-0.1.1 (c (n "sqsh") (v "0.1.1") (h "1cc84ilk4q83kgxamdslicqv5n8ha82xanpmz6lh6hpfd80fysfk")))

