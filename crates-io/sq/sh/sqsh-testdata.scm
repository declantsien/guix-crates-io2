(define-module (crates-io sq sh sqsh-testdata) #:use-module (crates-io))

(define-public crate-sqsh-testdata-0.1.0 (c (n "sqsh-testdata") (v "0.1.0") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "rand_chacha") (r "0.3.*") (d #t) (k 0)) (d (n "rand_distr") (r "0.4.*") (d #t) (k 0)))) (h "0fxaz1yy9gs0yxmi5vmybwmdjgkv5fvivg4jj8izh1nh1sfffsiq") (y #t)))

(define-public crate-sqsh-testdata-0.1.1 (c (n "sqsh-testdata") (v "0.1.1") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "rand_chacha") (r "0.3.*") (d #t) (k 0)) (d (n "rand_distr") (r "0.4.*") (d #t) (k 0)))) (h "11ya0ynxba6q7pi65jxv1vygkb763d1mi9k1vic4kgkkw8h5pvgf") (y #t)))

(define-public crate-sqsh-testdata-0.1.2 (c (n "sqsh-testdata") (v "0.1.2") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "rand_chacha") (r "0.3.*") (d #t) (k 0)) (d (n "rand_distr") (r "0.4.*") (d #t) (k 0)))) (h "17b0wlwsrp4gk2yyij9dcd7s4gda04f4d64i30cib4dsrylkqr62") (y #t)))

(define-public crate-sqsh-testdata-0.1.3 (c (n "sqsh-testdata") (v "0.1.3") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "clap") (r "3.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "rand_chacha") (r "0.3.*") (d #t) (k 0)) (d (n "rand_distr") (r "0.4.*") (d #t) (k 0)))) (h "0vqc0hkw4z1yll3a2fs24q4ypm1hxb4xxc6v8s3qg6bgygkh9319") (y #t)))

