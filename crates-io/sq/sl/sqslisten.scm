(define-module (crates-io sq sl sqslisten) #:use-module (crates-io))

(define-public crate-sqslisten-0.1.0 (c (n "sqslisten") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clokwerk") (r "^0.1.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.38.0") (d #t) (k 0)) (d (n "rusoto_sqs") (r "^0.38.0") (d #t) (k 0)))) (h "0zfqlmagzkf9vgngal2q61fc5hns5frjbhf36dznrjm3n678vg9v")))

(define-public crate-sqslisten-0.1.1 (c (n "sqslisten") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clokwerk") (r "^0.1.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.38.0") (d #t) (k 0)) (d (n "rusoto_sqs") (r "^0.38.0") (d #t) (k 0)))) (h "065f4lxycq4nbbr91w7ka793dzxw8045nfki78pkmf511nvzyr68")))

