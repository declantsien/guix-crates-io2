(define-module (crates-io sq uf squfof) #:use-module (crates-io))

(define-public crate-squfof-0.1.0 (c (n "squfof") (v "0.1.0") (d (list (d (n "is-prime-for-primitive-int") (r "^0.3.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "ring-algorithm") (r "^0.2.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)))) (h "006qzcka7zn486d4r9ij0q2hhir2d511mkdccxwbfdq4fwrxwhrz")))

(define-public crate-squfof-0.2.0 (c (n "squfof") (v "0.2.0") (d (list (d (n "is-prime-for-primitive-int") (r "^0.3.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "ring-algorithm") (r "^0.2.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)))) (h "0wyflh8da349fbcmrwkpng9sg5h861dmxli4rc09rclbccxz607d")))

(define-public crate-squfof-0.2.1 (c (n "squfof") (v "0.2.1") (d (list (d (n "is-prime-for-primitive-int") (r "^0.5.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ring-algorithm") (r "^0.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1zik056ggp86mrv430qbrkvrl6igmshya2zmxz6d9cs0lb5b8g9w")))

(define-public crate-squfof-0.2.3 (c (n "squfof") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "is-prime-for-primitive-int") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "ring-algorithm") (r "^0.4.3") (d #t) (k 0)))) (h "09cf9wf5a6zb4c2rhsparwwa9vw075iqq2yhxraqrz0qy65dsyh3")))

(define-public crate-squfof-0.2.4 (c (n "squfof") (v "0.2.4") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "is-prime-for-primitive-int") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "ring-algorithm") (r "^0.4.3") (d #t) (k 0)))) (h "0nydk7yr79zd2168r2z8h1vlajlvxq7i969qrzbbl8p6k47dn9p9")))

