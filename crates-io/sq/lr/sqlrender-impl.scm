(define-module (crates-io sq lr sqlrender-impl) #:use-module (crates-io))

(define-public crate-sqlrender-impl-0.1.0 (c (n "sqlrender-impl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "14r53am6ajqymjd5nvgjjkgjlqh6gj0g5ijznm8inkj80890man9") (f (quote (("test")))) (r "1.70")))

