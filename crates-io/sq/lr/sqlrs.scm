(define-module (crates-io sq lr sqlrs) #:use-module (crates-io))

(define-public crate-sqlrs-0.1.0 (c (n "sqlrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pi2zbqf0k00ml2b94vcbmifqpmk06ygqxps0hwj0pc67h64d2g2")))

