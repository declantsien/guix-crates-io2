(define-module (crates-io sq st sqstransfer) #:use-module (crates-io))

(define-public crate-sqstransfer-0.2.0 (c (n "sqstransfer") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (d #t) (k 0)) (d (n "rusoto_sqs") (r "^0.42.0") (d #t) (k 0)))) (h "1rp512q3irdz4lzq7cv97d4sck8gl1m6ncp1w1qkcnnpl1nrndhx")))

