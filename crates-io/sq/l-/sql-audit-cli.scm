(define-module (crates-io sq l- sql-audit-cli) #:use-module (crates-io))

(define-public crate-sql-audit-cli-0.1.0 (c (n "sql-audit-cli") (v "0.1.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "sql-audit") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.1") (f (quote ("postgres" "runtime-async-std-rustls" "macros" "json" "chrono"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0ncpibmwprad5g912cvgkmxvmcrzgw36bgz1qwqxi4aqdyj40mcz")))

