(define-module (crates-io sq l- sql-script-parser) #:use-module (crates-io))

(define-public crate-sql-script-parser-0.1.0 (c (n "sql-script-parser") (v "0.1.0") (h "03kxf6x2kchg5c1c8kx8zzlakbwyljbl56ps5da5b7bvacq6l8zr")))

(define-public crate-sql-script-parser-0.1.1 (c (n "sql-script-parser") (v "0.1.1") (h "1b5hjxyxrh9g9gz2skn3gpyxfl4ycv0gc4gvwnhjfy69nacm8y7v")))

(define-public crate-sql-script-parser-0.1.2 (c (n "sql-script-parser") (v "0.1.2") (h "1m2gzs7bv0bf6gy5wri5g6cbaacyd6xqkvcrz2jz0j5mbjzzh3r0")))

