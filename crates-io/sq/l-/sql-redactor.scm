(define-module (crates-io sq l- sql-redactor) #:use-module (crates-io))

(define-public crate-sql-redactor-0.1.0 (c (n "sql-redactor") (v "0.1.0") (d (list (d (n "sqlparser") (r "^0.32.0") (f (quote ("visitor"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "02vwr0ksi1n278964rgiiz7df1s2230k13qy6ivswj6kk4k16gy0")))

