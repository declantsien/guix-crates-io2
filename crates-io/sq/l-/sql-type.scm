(define-module (crates-io sq l- sql-type) #:use-module (crates-io))

(define-public crate-sql-type-0.1.0 (c (n "sql-type") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.1.1") (d #t) (k 0)))) (h "0kd9aqjj6fb3my7dp61s9rag5f9pf46jx8g0mbwc7a3ygfv723qs")))

(define-public crate-sql-type-0.1.1 (c (n "sql-type") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.1.1") (d #t) (k 0)))) (h "13366gpmz8kzw6w72dsvhz8a3kb1gqwkd4y1iyvi9pb7fbq48zhc")))

(define-public crate-sql-type-0.2.1 (c (n "sql-type") (v "0.2.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.2.1") (d #t) (k 0)))) (h "0s338njv663vg6wgf3zxgla8axzc7qsvr6ash1zl1s3nblvl78a6")))

(define-public crate-sql-type-0.3.0 (c (n "sql-type") (v "0.3.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.2.1") (d #t) (k 0)))) (h "10155rz6cpxvb56vmwa06k3zzyg0vk080jbz8jrmkgsqf8z1pgnb")))

(define-public crate-sql-type-0.3.1 (c (n "sql-type") (v "0.3.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.3") (d #t) (k 0)))) (h "0a8ny52n1awkmypbj8bw2rb6ds12464ki4j0sp3773hvnfysgccz")))

(define-public crate-sql-type-0.4.0 (c (n "sql-type") (v "0.4.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.4") (d #t) (k 0)))) (h "0pkl3fbmipjlbp4jyrlxag5k0f42m1x0c7gkx9f5sx8rj488gwkl")))

(define-public crate-sql-type-0.4.1 (c (n "sql-type") (v "0.4.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.4") (d #t) (k 0)))) (h "0dbwsjhf50ji3kxf28wq5m7b0cpsmfajb7jfr4wfnkagc0771jp7")))

(define-public crate-sql-type-0.4.2 (c (n "sql-type") (v "0.4.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.4") (d #t) (k 0)))) (h "0x349xffkwrz06ln01ixrfyq1hd8kvdzh30lv0252ds0k82gfrrr")))

(define-public crate-sql-type-0.4.3 (c (n "sql-type") (v "0.4.3") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.4") (d #t) (k 0)))) (h "1jh117wy7m4h7rj1xl3rrawpxs4j8v29cwaknz56f3272hhvz3na")))

(define-public crate-sql-type-0.5.0 (c (n "sql-type") (v "0.5.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.5") (d #t) (k 0)))) (h "0nkg2gn87ikxzm64r9csqc9w9acm1mp1vrhrcz8ksbaq7bqlbvix")))

(define-public crate-sql-type-0.6.0 (c (n "sql-type") (v "0.6.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.6") (d #t) (k 0)))) (h "0dzpi7b2n6l1nlxgfw9yqp109l7imphgnc8adgxij8c9hipdzhbw")))

(define-public crate-sql-type-0.7.0 (c (n "sql-type") (v "0.7.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.7") (d #t) (k 0)))) (h "04m8c2307kvjlmbhn5br1w7f9fcnnih329c038fn3i4a3qvy5p29")))

(define-public crate-sql-type-0.8.0 (c (n "sql-type") (v "0.8.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.8") (d #t) (k 0)))) (h "1z8rgn884zngdqfimprgwd1c6f22p14i8a7g201rwp2dimjd4cpi")))

(define-public crate-sql-type-0.8.1 (c (n "sql-type") (v "0.8.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.8") (d #t) (k 0)))) (h "0jrwsqsq6d1s5x3qafz0qgkfrajwx0giiccn6vh8197g71xd0wqp")))

(define-public crate-sql-type-0.9.0 (c (n "sql-type") (v "0.9.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.9") (d #t) (k 0)))) (h "06agb1330pshi4qnvb63l8gjv7bk4l5lyhx47rqdhaw1hkln80q9")))

(define-public crate-sql-type-0.10.0 (c (n "sql-type") (v "0.10.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.10.2") (d #t) (k 0)))) (h "0x6vrw6bnqy40hhc2kxxfiqaghd2325cd15hyc986xn9fhyrz7mq")))

(define-public crate-sql-type-0.10.1 (c (n "sql-type") (v "0.10.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.10.2") (d #t) (k 0)))) (h "1dglx5zbwkvydjymj57rj0ln1h2gp8nkd4vxp47avd8vp9ci9vqz")))

(define-public crate-sql-type-0.11.0 (c (n "sql-type") (v "0.11.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.11.0") (d #t) (k 0)))) (h "16il0v1brwvv298w19mlrv71l5czxm0vca1c3qz4lkqmxdzzrs6f")))

(define-public crate-sql-type-0.12.0 (c (n "sql-type") (v "0.12.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.12.0") (d #t) (k 0)))) (h "03f74542nfg059xbf7vwa4ri6jk5g40rg4la6idpc2bn0l3c52cg")))

(define-public crate-sql-type-0.13.0 (c (n "sql-type") (v "0.13.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.13.0") (d #t) (k 0)))) (h "1f00w9kwfzjklfyhs71f6nbir9gj8z8mhiliacb55srn3zg988a8")))

(define-public crate-sql-type-0.13.1 (c (n "sql-type") (v "0.13.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.13.0") (d #t) (k 0)))) (h "1l6fr2p3sddlszn9b2bgf2p9hql5gahzlmp2267p0h9qj7rkhkmq")))

(define-public crate-sql-type-0.14.0 (c (n "sql-type") (v "0.14.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.14.0") (d #t) (k 0)))) (h "1x92xskg4x6alz7c5qfir9ciqqx2n8yrgqmnfvn70kh8fw6nsyav")))

(define-public crate-sql-type-0.14.1 (c (n "sql-type") (v "0.14.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.14.1") (d #t) (k 0)))) (h "0mhabx39ss3vhqar0m6sn432y1169lsgf3pgf273mqj2qzvl40r3")))

(define-public crate-sql-type-0.14.2 (c (n "sql-type") (v "0.14.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.14.1") (d #t) (k 0)))) (h "0yh0q3y50kcgbqvklisaa2s6r8xc5cwbdz8ln8kpp25ks3ixnfqb")))

(define-public crate-sql-type-0.15.0 (c (n "sql-type") (v "0.15.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.15") (d #t) (k 0)))) (h "010cik39vw9z9xpnlrbzvskzvwlwj0s464cl6zw7qimpjx71cbba")))

(define-public crate-sql-type-0.15.1 (c (n "sql-type") (v "0.15.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.15") (d #t) (k 0)))) (h "1yy5mz836z1r8j2bccm8lryp5v1kqf22m5zzvhhr7ja6j8d6g3z5")))

(define-public crate-sql-type-0.15.2 (c (n "sql-type") (v "0.15.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.15") (d #t) (k 0)))) (h "1x97j6mp2p39h6ib3gmpd3nfw54w2krxh0mhcfyn8nvg4l00gd6j")))

(define-public crate-sql-type-0.15.3 (c (n "sql-type") (v "0.15.3") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.15.1") (d #t) (k 0)))) (h "09i1nac8rx20550rxqrj0csdp1a5r2xdinb1hapsdf2rj651gmcb")))

(define-public crate-sql-type-0.15.4 (c (n "sql-type") (v "0.15.4") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.16.0") (d #t) (k 0)))) (h "18d4l3y46s1adbb8w22490jraf1zs0ckvlj4ssjy932483ksbxbh")))

(define-public crate-sql-type-0.16.0 (c (n "sql-type") (v "0.16.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.17.0") (d #t) (k 0)))) (h "111v8zxl7dlidrnr8r0rp3r399nz18bp3h9aycg41xghnsq7pvzl")))

(define-public crate-sql-type-0.17.0 (c (n "sql-type") (v "0.17.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.18.0") (d #t) (k 0)))) (h "1sqs79y0hryfsnlbzm3zfbjwndb3d8cyyng61c2fnm7nv0140gr4")))

(define-public crate-sql-type-0.18.0 (c (n "sql-type") (v "0.18.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 2)) (d (n "sql-parse") (r "^0.18.0") (d #t) (k 0)))) (h "0mpcgrl96q9fpc63iwcdv6cvdfixld2lsmwc7px92i14yrg2ndi2")))

