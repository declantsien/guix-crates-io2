(define-module (crates-io sq l- sql-builder) #:use-module (crates-io))

(define-public crate-sql-builder-0.1.0 (c (n "sql-builder") (v "0.1.0") (h "0w0in192wllnbdz9c8jvwnw3qrd7jk80gkwby7cfzklvwqlb5g4s")))

(define-public crate-sql-builder-0.1.1 (c (n "sql-builder") (v "0.1.1") (h "0h4k6h4wyr9i8lfvlak6ralrnb3gf117qfcwzdzw4c19rjpmq8ww")))

(define-public crate-sql-builder-0.1.2 (c (n "sql-builder") (v "0.1.2") (h "1rrag0idxjsdqizxwl5ymgng1mfbb6sgd5s1ywwdz0k4vmhhn1a3")))

(define-public crate-sql-builder-0.1.3 (c (n "sql-builder") (v "0.1.3") (h "1h2kpcy8c9qx1n3v74kp472r9jy512fa8hlzzxgnlhzsmkf5gh93")))

(define-public crate-sql-builder-0.1.4 (c (n "sql-builder") (v "0.1.4") (h "1rj51a81xw3m3qh2fwb089zsb7n3iyiqzcrhy533vgnizpwz5cz8")))

(define-public crate-sql-builder-0.1.5 (c (n "sql-builder") (v "0.1.5") (h "0wnkvikgx7yhybn78blrqrbvn6jzyqcvjaayqly6l09h3ka6qi5g")))

(define-public crate-sql-builder-0.1.6 (c (n "sql-builder") (v "0.1.6") (h "19cmbwwx9ykr7q9xqzjrsyhm5q6y6hs38h64m901f7x4x6hshfy7")))

(define-public crate-sql-builder-0.1.7 (c (n "sql-builder") (v "0.1.7") (h "07s4xwrbyd6wi82ch6cgavm1196sxn22si0zvx5x2sinyb5i0vfg")))

(define-public crate-sql-builder-0.1.8 (c (n "sql-builder") (v "0.1.8") (h "16y1g2iyxgsiaq93kidb3c5g9n5ysssh5x5243wm391mv0m2xms0")))

(define-public crate-sql-builder-0.1.9 (c (n "sql-builder") (v "0.1.9") (h "1wmds15nvb1980lrb0qfp6swbqf87fwf6x94gpbaaygyrpk0rrwf")))

(define-public crate-sql-builder-0.1.10 (c (n "sql-builder") (v "0.1.10") (h "1nl6k0syn3krzsy228wjxs0za843fk3hawd97yznbpiq8gcmsf6a")))

(define-public crate-sql-builder-0.1.11 (c (n "sql-builder") (v "0.1.11") (h "08lwh98hyslm7zyagw8qg8bbrfvfvyyvvqs3h40xdnrhklaidhj2")))

(define-public crate-sql-builder-0.1.12 (c (n "sql-builder") (v "0.1.12") (h "16qvbfi7h8psgfb03pqy83wx9ld0l1nk21sx71nbfgqgfiqvqbvz")))

(define-public crate-sql-builder-0.1.13 (c (n "sql-builder") (v "0.1.13") (h "12i67g3b00k1z7riwpdip4m1y85y50p3lib28lwkc80z6r6krvjk")))

(define-public crate-sql-builder-0.1.14 (c (n "sql-builder") (v "0.1.14") (h "15569v3n3p7s568v8fgi1l6p5sb78g40mdg7q7wy8xdcjylz2qjj")))

(define-public crate-sql-builder-0.2.0 (c (n "sql-builder") (v "0.2.0") (h "0bvwlbb7sns6dlq62q25fs5j5crqkdmijj5d72l5f985j69bldml")))

(define-public crate-sql-builder-0.3.0 (c (n "sql-builder") (v "0.3.0") (h "0b5y82pnqzqnrzjagr3dpz5af0dd1fnynfjvzbwgpjf29szkcz8r")))

(define-public crate-sql-builder-0.4.0 (c (n "sql-builder") (v "0.4.0") (h "1f5hnkfk0ka1gcgzs4ahs42aqg2d4a1aapqsqyvjzqrzkj7f04yz")))

(define-public crate-sql-builder-0.5.0 (c (n "sql-builder") (v "0.5.0") (h "06xs57m1nr7as2qfh6q0gmvj7j5jdr6k8znvcq55znpgr5f2318g")))

(define-public crate-sql-builder-0.5.1 (c (n "sql-builder") (v "0.5.1") (h "0x7imbrkmyyx8hca1527ywadfby176y0rcmw9rybccqgny1xbvpa")))

(define-public crate-sql-builder-0.6.0 (c (n "sql-builder") (v "0.6.0") (h "0r8rv5higcpvmac3zbr8lxh88b5nvwk5a8c396q3nzpmfm2v1hbv")))

(define-public crate-sql-builder-0.7.0 (c (n "sql-builder") (v "0.7.0") (h "12smxyzlyd0fvhz5dfch33j3m8yafl4rsw37qvp3h8kfyn0wl0h3")))

(define-public crate-sql-builder-0.8.0 (c (n "sql-builder") (v "0.8.0") (h "0pdxbbvcvb353i4yswl9z7wa44gbf545npjxcsprm7rxp2hnrxa8")))

(define-public crate-sql-builder-0.8.1 (c (n "sql-builder") (v "0.8.1") (h "0hfrc8gbb32ji3mkvkgh3h4l8rza22zhzlck52i30ffpqsnih1j9")))

(define-public crate-sql-builder-0.9.1 (c (n "sql-builder") (v "0.9.1") (h "18adqdrwsvp28anipwmhiii97i8x4sfsxpl7fhanw04qnmqlgzwq")))

(define-public crate-sql-builder-0.10.0 (c (n "sql-builder") (v "0.10.0") (h "16li6zrl25cdnywl2rg10q6b3nrabw4g7k38ic0m3fmbhcglvwz8")))

(define-public crate-sql-builder-0.10.1 (c (n "sql-builder") (v "0.10.1") (h "18ywhpbzw3ja67aikhfdih1dal35rl2ml5h5sjdjbw6l4ij1izqw")))

(define-public crate-sql-builder-0.10.2 (c (n "sql-builder") (v "0.10.2") (h "0dl1xr6ji65qr3kf21s303y3y9z0zi38nna9vsc3ix5l83gkdrjp")))

(define-public crate-sql-builder-0.10.3 (c (n "sql-builder") (v "0.10.3") (h "0yk96q2xvll9zm02sam23rffvxjygf7ilyhsqgyzcmdldy7jw1h9")))

(define-public crate-sql-builder-0.10.4 (c (n "sql-builder") (v "0.10.4") (h "00gzvrh956x4zwbmn9b23sxff4rsd9fvixc0plrg6nwr0xvzxvjj")))

(define-public crate-sql-builder-0.11.0 (c (n "sql-builder") (v "0.11.0") (h "0bsqs6qd47gd2q5majkwgwrcmhzicyxhy226kn02cwg2k2h51ywc")))

(define-public crate-sql-builder-0.11.1 (c (n "sql-builder") (v "0.11.1") (h "1865r492lklddnfbmxxlixf4wpf2213b4bznmapp8vlpasv4x5vb")))

(define-public crate-sql-builder-0.11.2 (c (n "sql-builder") (v "0.11.2") (h "0i430ghys3s2bv5b1bryyp37r5h1bwr8zkhh4j56phx184mi0bll")))

(define-public crate-sql-builder-0.11.3 (c (n "sql-builder") (v "0.11.3") (h "1lcljsndf2l1sy0hrqi10gaggpcilgrpm5r4ccb600rlsdc43rnw")))

(define-public crate-sql-builder-0.11.4 (c (n "sql-builder") (v "0.11.4") (h "04z78dvlw8iky17py85ja281lq0vqp40pl3aikndmshqhdh1jsmk")))

(define-public crate-sql-builder-0.11.5 (c (n "sql-builder") (v "0.11.5") (h "0ndvvl26yrnr203l3fihc94458vypay21w4vwjx4f66xanlwvnn7")))

(define-public crate-sql-builder-0.11.6 (c (n "sql-builder") (v "0.11.6") (h "0sxfhk1vdqzg1wnjx3miqb05k5jda428dyfc70krskaa6dw9ax1i")))

(define-public crate-sql-builder-0.11.7 (c (n "sql-builder") (v "0.11.7") (h "1drbag97nbflym0b7dc6llvcmca4flzdf2kzv2pwy892j8p45jin")))

(define-public crate-sql-builder-0.11.8 (c (n "sql-builder") (v "0.11.8") (h "0zwk2zh1g81kgqslswh6w762d9f22yrsn0j9865ihjqayr1qwpbd")))

(define-public crate-sql-builder-0.11.9 (c (n "sql-builder") (v "0.11.9") (h "1j7bnbi9w1988zc7563092rls41z4xn7s3l2xa724kykd752z8qi")))

(define-public crate-sql-builder-0.11.10 (c (n "sql-builder") (v "0.11.10") (h "01w6p3q80yz30ixxayi9m1l2rijmmamg04yf9vrn76bjwkspb01k")))

(define-public crate-sql-builder-0.11.11 (c (n "sql-builder") (v "0.11.11") (h "1ghp339f2imj4znj3lcz10dkmy6i5lp8l8qh9k12wpawcwycf1hq")))

(define-public crate-sql-builder-0.12.0 (c (n "sql-builder") (v "0.12.0") (h "01k660gmvdkpdqrc9ygqcyy8gnhh0xirdh1s42vryf2xdcknhy36")))

(define-public crate-sql-builder-0.12.1 (c (n "sql-builder") (v "0.12.1") (h "00v1bg5dlc0pl5yhwv6f8q3hvmy3slm5fv9f1k912p26r3db33nj")))

(define-public crate-sql-builder-0.12.2 (c (n "sql-builder") (v "0.12.2") (h "0qsmy1f61cvz08ya42kiww506g61rinjpvgz7bzvq6ri0r9cq6mp")))

(define-public crate-sql-builder-0.13.0 (c (n "sql-builder") (v "0.13.0") (h "0i6sqp4s2lrrfj3ax2vq79k9pwy5nbg2ncwf9a0c6nz8maqrd7v5")))

(define-public crate-sql-builder-1.0.0 (c (n "sql-builder") (v "1.0.0") (h "0645fbkyic115ksf0fn03zdghvn9jq85qy5x462agqs251xk73d6")))

(define-public crate-sql-builder-1.1.0 (c (n "sql-builder") (v "1.1.0") (h "09h7k7widd36b435sb5aqpla0211mmakfp858kr19chzdrm2y99n")))

(define-public crate-sql-builder-1.1.1 (c (n "sql-builder") (v "1.1.1") (h "14hcmyzi497xlp34md2ys2a961m5h7m5wla75gbh12mgq81rn893")))

(define-public crate-sql-builder-2.0.0 (c (n "sql-builder") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i8vafkm74sg8vcjiir366p156s740rbvr0p7bj1byzrg6gk5nj7")))

(define-public crate-sql-builder-2.1.0 (c (n "sql-builder") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1w8ngvq5y36f5yhq9f8xb9kl09lnajv714ijli40s2xvbz29aa22")))

(define-public crate-sql-builder-2.2.0 (c (n "sql-builder") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "099c25s8fh85rf10l7zwy9i2bdnhv1q5s0yk2vi2lq3bqjcmsr2m")))

(define-public crate-sql-builder-2.3.0 (c (n "sql-builder") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m197mixqdnxvcm79j0726pqp6qxq8c5s28rxkxlj06c1kijp83y")))

(define-public crate-sql-builder-3.0.0 (c (n "sql-builder") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h8d71y9hlsds5g4w987lxn3hip2rncfihgi9k1jpikisi27wr4c")))

(define-public crate-sql-builder-3.0.1 (c (n "sql-builder") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ydj42ss8c2rccdbippribhzgm35adg295skpxw54syhbbxi581x")))

(define-public crate-sql-builder-3.0.2 (c (n "sql-builder") (v "3.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15kc10dbifh6ldv4lc4vs1r3rs5jg98f38xrdx7w7bfglc0na8dc")))

(define-public crate-sql-builder-3.0.3 (c (n "sql-builder") (v "3.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xqr7032c8glihg5x5i7lx22i8h9xd19wmd4x867aqanh8n8br9j")))

(define-public crate-sql-builder-3.1.0 (c (n "sql-builder") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00cr3424hlba86sx60p21a56x2srrp7ykb0jrhhwmwykqchwapmh")))

(define-public crate-sql-builder-3.1.1 (c (n "sql-builder") (v "3.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h5xp47zz9chv545lpmal51fq3z162z2f99mb4lhcbgcsaaqs05i")))

