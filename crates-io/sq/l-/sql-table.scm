(define-module (crates-io sq l- sql-table) #:use-module (crates-io))

(define-public crate-sql-table-0.1.0 (c (n "sql-table") (v "0.1.0") (d (list (d (n "inject") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1f9i2xi3qnkz58gmzjvms9i78d8ycrppvx3k40rx2qr6rpmpcgki") (y #t)))

(define-public crate-sql-table-0.1.1 (c (n "sql-table") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "sql-table-inject") (r "^0.1") (d #t) (k 0)))) (h "18qpnc8p6rrf8a9crrrgv4m2sacx9kwaxfw59p8yqzcpmnv8acjs")))

(define-public crate-sql-table-0.1.2 (c (n "sql-table") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "sql-table-inject") (r "^0.1") (d #t) (k 0)))) (h "0z5g0dk5x6bz0pj08jxv0wginngl08yg80sf4ji8vrcgzxrm9ngb")))

(define-public crate-sql-table-0.1.3 (c (n "sql-table") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "sql-table-inject") (r "^0.1") (d #t) (k 0)))) (h "13vbyvgvd5ya1plgygn5g77hhnmhla69y4gzjlmfdyi063xln4lg")))

(define-public crate-sql-table-0.1.4 (c (n "sql-table") (v "0.1.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "sql-table-inject") (r "^0.1") (d #t) (k 0)))) (h "0kk8rrlid5wx8p5xm3q34mw6mwnyd35d4kvly6fvdp35w1ycgf3i")))

