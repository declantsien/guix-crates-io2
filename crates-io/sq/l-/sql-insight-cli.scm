(define-module (crates-io sq l- sql-insight-cli) #:use-module (crates-io))

(define-public crate-sql-insight-cli-0.1.0 (c (n "sql-insight-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "sql-insight") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1fhc5pzqly6ym8n8cxq8s277ygwr64bsqj7nj2qhil7bclqzxdqf")))

(define-public crate-sql-insight-cli-0.1.1 (c (n "sql-insight-cli") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "sql-insight") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "14sx7dgzl06g9lhg401h3fzfxjg9hj37hgrbas4j1bfyw5vn1v99")))

