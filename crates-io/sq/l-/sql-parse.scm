(define-module (crates-io sq l- sql-parse) #:use-module (crates-io))

(define-public crate-sql-parse-0.1.0 (c (n "sql-parse") (v "0.1.0") (h "1vmcg3m8zns5lxk40f3a0dp1m0sslx71alj83iv06rkd16mlhl0m")))

(define-public crate-sql-parse-0.1.1 (c (n "sql-parse") (v "0.1.1") (h "1mbg6rc80j9dw25lk146dagd6dl9ksnql4cw99fqfq0ffjsy90n0")))

(define-public crate-sql-parse-0.2.0 (c (n "sql-parse") (v "0.2.0") (h "1vcry97k6h5swgwpkxgkgrl64yhd224mghxgdbc32iws35mkhsh4")))

(define-public crate-sql-parse-0.2.1 (c (n "sql-parse") (v "0.2.1") (h "1cpsdcwfd4vj2sg6zg8dx84aif99dr4p0i3x2l0i841f7m0n9l4z")))

(define-public crate-sql-parse-0.3.0 (c (n "sql-parse") (v "0.3.0") (h "0rx4swvi4sprp4dfwkhvmfi8fmjgli9pvv52jxv19kmrrik1kfjq")))

(define-public crate-sql-parse-0.4.0 (c (n "sql-parse") (v "0.4.0") (h "0fk1h69glaz4wf2ypa4msikvg6v0p3yjvaznvd41r85bvqkx489s")))

(define-public crate-sql-parse-0.5.0 (c (n "sql-parse") (v "0.5.0") (h "1cnjbxnc83ks0c63gm8hwv39q2bg3c8kiv6z51qx3rbfvl8nqdym")))

(define-public crate-sql-parse-0.6.0 (c (n "sql-parse") (v "0.6.0") (h "069k662qvxhkh1xz1yynxa9znwn69vgfwz7yalvn59hwga41xj4l")))

(define-public crate-sql-parse-0.7.0 (c (n "sql-parse") (v "0.7.0") (h "0awzch9ylh8wzfbbnzsxf1r3idsajrcrxjplbx4698r1xdw9l81b")))

(define-public crate-sql-parse-0.8.0 (c (n "sql-parse") (v "0.8.0") (h "11khl97xcmbivaczib63d2kxxqlimaj5c334sngxcgkj3y0q45kl")))

(define-public crate-sql-parse-0.9.0 (c (n "sql-parse") (v "0.9.0") (h "0z7pj3hhh7k0yppmvm82208aday64kx59yrjsc1d415fjxg6ww8y")))

(define-public crate-sql-parse-0.10.0 (c (n "sql-parse") (v "0.10.0") (h "14qll3hfywa4w9053n3i7nvs0006c9j544b6pwz96gg8x9wlka7w")))

(define-public crate-sql-parse-0.10.1 (c (n "sql-parse") (v "0.10.1") (h "1gv84fdlgffmczjx9ipjk7s8f3bvrdry0fljadw0b7ccr3q25s9v")))

(define-public crate-sql-parse-0.10.2 (c (n "sql-parse") (v "0.10.2") (h "012s8zvgmncqbihi7f0zhx0pc2fbca8qllfxdlim1vy3i9spjm4i")))

(define-public crate-sql-parse-0.11.0 (c (n "sql-parse") (v "0.11.0") (h "1fzc530a5170w7q74xy4qsb0ysmkxkx91v38pqr8h7vyfqql4kd0")))

(define-public crate-sql-parse-0.12.0 (c (n "sql-parse") (v "0.12.0") (h "00hpzjqgaw1gnn0ymyrhx4n76h2bly00njc9ziv30i787vka4cpw")))

(define-public crate-sql-parse-0.13.0 (c (n "sql-parse") (v "0.13.0") (h "05v0sj3knivxj9is7i4v5q1c7rwjafiaaczj5sdgf845bww01zr2")))

(define-public crate-sql-parse-0.14.0 (c (n "sql-parse") (v "0.14.0") (h "00499r39z2cqbfxlg5xaivms3ggxgyga3n9iz9s452almjdwx6zd")))

(define-public crate-sql-parse-0.14.1 (c (n "sql-parse") (v "0.14.1") (h "091yvg3kni3dcsl7cfssz316ldsn21h74lgvmm1qry3x14gpnmw9")))

(define-public crate-sql-parse-0.15.0 (c (n "sql-parse") (v "0.15.0") (h "0wrx266g2sk5z34q75nvv6h7cnpav52kv4y354gys9qmxzgfaa9k")))

(define-public crate-sql-parse-0.15.1 (c (n "sql-parse") (v "0.15.1") (h "0a42r8pfmx7al2hppjx6v814aznlzyxndjg0mbmy9n4jxzdszlp9")))

(define-public crate-sql-parse-0.15.2 (c (n "sql-parse") (v "0.15.2") (h "0mnri7pknsfjpvx00i9nny0k4cis8ris6rqj4a8kkwpp3k51y5av")))

(define-public crate-sql-parse-0.16.0 (c (n "sql-parse") (v "0.16.0") (h "0c7xms0lwsm9cjs936q4xdb2qz1n95367whbj2ispk8111p478nx")))

(define-public crate-sql-parse-0.17.0 (c (n "sql-parse") (v "0.17.0") (h "1g38zapq926vihm9ibnv0mklyfbjnhrf2ij8rvfhsvcs4x85422g")))

(define-public crate-sql-parse-0.18.0 (c (n "sql-parse") (v "0.18.0") (h "1xqv2c4ddigy15rfn2clps50cvilmafhrpw2ix77jmi8zb0vfml1")))

