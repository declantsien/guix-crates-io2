(define-module (crates-io sq l- sql-migration-sim) #:use-module (crates-io))

(define-public crate-sql-migration-sim-0.1.0 (c (n "sql-migration-sim") (v "0.1.0") (d (list (d (n "sqlparser") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0sa5rand7a8xx99w1rsrbf3j12qvii3a2qvfgqjx9hvi1ngmda4x")))

(define-public crate-sql-migration-sim-0.1.1 (c (n "sql-migration-sim") (v "0.1.1") (d (list (d (n "sqlparser") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "03qz5sm2icdhd2xxzd8gik9l0z8gy4shzq0q199p4k19ivfqgjk7")))

(define-public crate-sql-migration-sim-0.1.2 (c (n "sql-migration-sim") (v "0.1.2") (d (list (d (n "sqlparser") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1cf1gnnv2dp5iq0nj6nq0c251554df084yin6ghn1lgjk5rg95nz")))

(define-public crate-sql-migration-sim-0.1.3 (c (n "sql-migration-sim") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.195") (o #t) (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0clsax1531944s137g1r1vi5327b0674dlwfbiqfq3f7aqzjr22l") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "sqlparser/serde"))))))

(define-public crate-sql-migration-sim-0.1.5 (c (n "sql-migration-sim") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "sqlparser") (r "^0.45.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0v9912b2kylj7gdmx1y1h4kmqy0k6ic1kirhxjf9gwkihmyfsdlj") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "sqlparser/serde"))))))

(define-public crate-sql-migration-sim-0.1.6 (c (n "sql-migration-sim") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "sqlparser") (r "^0.45.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1aahf44wviymgl6jps91rbaym461nx5m0pbjvix3088zq03c17p4") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "sqlparser/serde"))))))

