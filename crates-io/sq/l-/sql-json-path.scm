(define-module (crates-io sq l- sql-json-path) #:use-module (crates-io))

(define-public crate-sql-json-path-0.1.0 (c (n "sql-json-path") (v "0.1.0") (d (list (d (n "jsonbb") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dagn18fndl11qlg8zrmlxi4a44zfzqicnaksidll0nvq7jh1n86")))

