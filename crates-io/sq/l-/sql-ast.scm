(define-module (crates-io sq l- sql-ast) #:use-module (crates-io))

(define-public crate-sql-ast-0.5.1-alpha-1 (c (n "sql-ast") (v "0.5.1-alpha-1") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "0p4wq6ch8y0ax6agmdarw7gs5qc6yk11xrl08nb17xz8izwy7gs1")))

(define-public crate-sql-ast-0.6.0 (c (n "sql-ast") (v "0.6.0") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "1qkryclrshpgvc0alvjv5bq1pw9gcng629i6zpbxz49r9wrv8s0x")))

(define-public crate-sql-ast-0.7.0 (c (n "sql-ast") (v "0.7.0") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "1mim06r3476h4mkrswwlxc1pk4101m0lxslmzwr0la1kbwxwykg1")))

(define-public crate-sql-ast-0.7.1 (c (n "sql-ast") (v "0.7.1") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "1sfi7fyfmfmznjgzp5s87vqd2q9zaadmp3msz6lnsgypby7npvcs")))

(define-public crate-sql-ast-0.7.2 (c (n "sql-ast") (v "0.7.2") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "0zs2i5jd5n8zxrcjfwsrbnz45q65nq02pj1pqn1ancllvzczvzmb")))

(define-public crate-sql-ast-0.7.3 (c (n "sql-ast") (v "0.7.3") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "1b5pvaxqs4vrv7hh0w106qc3nar367fv4xr2237ngabsr3jf61p0")))

(define-public crate-sql-ast-0.7.4 (c (n "sql-ast") (v "0.7.4") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "18f2k4lai3q4fvaigwmzk3arv7iqpfz6wlxphl1r3ci9vpnycgsd")))

(define-public crate-sql-ast-0.7.5 (c (n "sql-ast") (v "0.7.5") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "1ga75xndv2agw1la9y3pn1kw0fl2ixzkl8nk5387qwlgmpfigv6l")))

(define-public crate-sql-ast-0.8.0 (c (n "sql-ast") (v "0.8.0") (d (list (d (n "bigdecimal") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "14zr81b4iydx23q6mdl4zk68jb1sxk40haj22z637x166zkr4k22")))

