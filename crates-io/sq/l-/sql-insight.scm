(define-module (crates-io sq l- sql-insight) #:use-module (crates-io))

(define-public crate-sql-insight-0.1.0 (c (n "sql-insight") (v "0.1.0") (d (list (d (n "sqlparser") (r "^0.43.1") (f (quote ("visitor"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "05kbczc9l9aiaw394a9bdipsgia05yg82siyjwdm2vqzsgwgkvmd")))

(define-public crate-sql-insight-0.1.1 (c (n "sql-insight") (v "0.1.1") (d (list (d (n "sqlparser") (r "^0.43.1") (f (quote ("visitor"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0qzvsd52sh1gxa11m31dyhaj5iknhr7y20jmfk9g1gz0n29zgw08")))

