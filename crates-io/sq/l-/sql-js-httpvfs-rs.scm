(define-module (crates-io sq l- sql-js-httpvfs-rs) #:use-module (crates-io))

(define-public crate-sql-js-httpvfs-rs-0.0.1 (c (n "sql-js-httpvfs-rs") (v "0.0.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0qz2xs41fllkjkpasxzpgj8mwi1q03737lmkrf4ga46fbrsipgv3") (f (quote (("bundled"))))))

