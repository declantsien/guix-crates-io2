(define-module (crates-io sq l- sql-audit) #:use-module (crates-io))

(define-public crate-sql-audit-0.1.0 (c (n "sql-audit") (v "0.1.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.10") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.1") (f (quote ("postgres" "runtime-async-std-rustls" "macros" "json" "chrono"))) (d #t) (k 0)))) (h "1zfl12lmvp5iiln4qm0iaa7dki88clp812kvbfknl1gcxb35yf2p")))

