(define-module (crates-io sq lt sqltatic) #:use-module (crates-io))

(define-public crate-sqltatic-0.1.0-rc1 (c (n "sqltatic") (v "0.1.0-rc1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tera") (r "^1.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.16") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "0dws6zklfq6dad4l7iafhhr0rvrs7w71c8n5jmzb0fljsy780jg4")))

