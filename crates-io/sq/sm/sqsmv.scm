(define-module (crates-io sq sm sqsmv) #:use-module (crates-io))

(define-public crate-sqsmv-0.1.1 (c (n "sqsmv") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rusoto_core") (r "^0.45.0") (d #t) (k 0)) (d (n "rusoto_sqs") (r "^0.45.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0a8mizxgffjzp0zgrbs3ivbbw1qjw67gx4s0nqbqfk989m64v4rh")))

