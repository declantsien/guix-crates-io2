(define-module (crates-io sq ar sqare-series-crate) #:use-module (crates-io))

(define-public crate-sqare-series-crate-0.1.0 (c (n "sqare-series-crate") (v "0.1.0") (h "1490prs9a6v7pv9pai6r7rcrbpi902pn5ib6nkz5w99w2l9l8rrc")))

(define-public crate-sqare-series-crate-0.1.1 (c (n "sqare-series-crate") (v "0.1.1") (h "1ivlpyifq5ki9h9lqvz41l673j09wrhj3dm30alarg83qh1aa0gx")))

(define-public crate-sqare-series-crate-0.1.3 (c (n "sqare-series-crate") (v "0.1.3") (h "06z1f1vqhwyx5sl2k5qpbnwbpbhhi3lqqrd6albpagrvygm7ka9i")))

(define-public crate-sqare-series-crate-0.1.4 (c (n "sqare-series-crate") (v "0.1.4") (h "1n28xqcms36ksf6lgc79fa3ar4kv06d04rz1q3isg36fl2dmgkkm")))

