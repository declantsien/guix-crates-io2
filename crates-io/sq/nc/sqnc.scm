(define-module (crates-io sq nc sqnc) #:use-module (crates-io))

(define-public crate-sqnc-1.0.0 (c (n "sqnc") (v "1.0.0") (d (list (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1hcjn59qwc2i2zdry46yn7r6ks1dspr3nrgi9w34r0q3k71ck93y") (f (quote (("alloc")))) (r "1.66")))

