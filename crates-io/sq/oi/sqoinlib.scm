(define-module (crates-io sq oi sqoinlib) #:use-module (crates-io))

(define-public crate-sqoinlib-0.1.0 (c (n "sqoinlib") (v "0.1.0") (h "1c2ihfp91dqv7kdd0mslsbapjhp5i87lldjfjvlwcn3mbvfsrxjw")))

(define-public crate-sqoinlib-0.1.1 (c (n "sqoinlib") (v "0.1.1") (h "1l30jjwvqgpkbpjnifjxgngbq1k672d9q6yxdkalj3cd91mf3faq")))

