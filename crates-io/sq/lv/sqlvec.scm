(define-module (crates-io sq lv sqlvec) #:use-module (crates-io))

(define-public crate-sqlvec-0.0.1 (c (n "sqlvec") (v "0.0.1") (d (list (d (n "rusqlite") (r ">=0.27, <=0.30") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xai10lq0l62mld26q5d28hpw9ja8ivla7hn0a0zpfnfi6qrvd3c") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sqlvec-0.0.2 (c (n "sqlvec") (v "0.0.2") (d (list (d (n "rusqlite") (r ">=0.27, <=0.30") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16himn5dppbf47ckfjf680h5dy9slkmrcaxhfpbp7lv5jk3dvp92") (s 2) (e (quote (("serde" "dep:serde"))))))

