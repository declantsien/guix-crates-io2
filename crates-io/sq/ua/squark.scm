(define-module (crates-io sq ua squark) #:use-module (crates-io))

(define-public crate-squark-0.1.0 (c (n "squark") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "0igjly1k36vf6rmcq1qys447y19zgklqd9m6lx92i4ha0xl8bni8") (y #t)))

(define-public crate-squark-0.1.1 (c (n "squark") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "1kwpfdxpd5snwwv38lx4klxy5l9vp1nlvcfibamf5l5ci3a9vm6y")))

(define-public crate-squark-0.2.0 (c (n "squark") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "15759xnl4j0cfp5an4hpa1444bj8vmn11ldjacirdrhjzfx67zhf")))

(define-public crate-squark-0.3.0 (c (n "squark") (v "0.3.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "0rm7j6amk3h75612k3byb6zxnx1ab3j4xf581jcxc2d77cxwmmvh")))

(define-public crate-squark-0.3.1 (c (n "squark") (v "0.3.1") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "0asgcac5kg253znj36jd0g037g15q4jqqqcqdn8inp7nl7jgcy25")))

(define-public crate-squark-0.3.3 (c (n "squark") (v "0.3.3") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "1r55si8zfk1iydavyfmlw3j0md39dy6441lrsylb7g6p1gw4h745")))

(define-public crate-squark-0.4.0 (c (n "squark") (v "0.4.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "1900xn738q3ylvhh2a09si6gnn79cklky5n138smiv6i293apcnw")))

(define-public crate-squark-0.5.0 (c (n "squark") (v "0.5.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "0avygrnzznjpkyg1n01v6sjand2kkiy71hj6w9mcizz9z9658d3n")))

(define-public crate-squark-0.5.1 (c (n "squark") (v "0.5.1") (d (list (d (n "rand") (r "^0.5.0-pre.0") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "0n8q1fnjx839c2gxrqaqq7scjrz2qvd08v19xfvglbcachp0bkqx")))

(define-public crate-squark-0.6.0 (c (n "squark") (v "0.6.0") (d (list (d (n "rand") (r "^0.6.0-pre.0") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (d #t) (k 0)))) (h "1cl6ydg2jcmxky7rd6kcl8gf52bx3cayl9rh02swwdpz4b2gakw5")))

(define-public crate-squark-0.6.1 (c (n "squark") (v "0.6.1") (d (list (d (n "rand") (r "^0.6.0-pre.0") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "01mdqvhfq6nap2gyy1k5c68k10k03hk52gia3k490zwy8arz2gzl")))

(define-public crate-squark-0.6.2 (c (n "squark") (v "0.6.2") (d (list (d (n "rand") (r "^0.6.0-pre.0") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "04jrmwvnligiyr9af307h4x0yfq299y0dlghfal7vgigmq8chh4h")))

(define-public crate-squark-0.6.3 (c (n "squark") (v "0.6.3") (d (list (d (n "rand") (r "^0.6.0-pre.0") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0fjrhpigc7yahy6p51bsgrl5fpjmmxasyycadq57pgwskidssj1b")))

(define-public crate-squark-0.7.0 (c (n "squark") (v "0.7.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "= 0.6.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "0cisxpan8wsswbwn1h4yi06wf7wjrcjf3q64rjsnh800s06aha6f")))

(define-public crate-squark-0.7.1 (c (n "squark") (v "0.7.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (d #t) (k 0)))) (h "00j3w2ywivgqs834xsmzfgkxmp5rx3vvfkyb0p6rl4ja18xwbj54")))

