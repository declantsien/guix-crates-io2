(define-module (crates-io sq ua square_calculator) #:use-module (crates-io))

(define-public crate-square_calculator-0.1.0 (c (n "square_calculator") (v "0.1.0") (h "0sjxh0bnmv1wxf83psgzkfkqqr0s2q58rckgnq09w4ydvs2b87br")))

(define-public crate-square_calculator-0.1.1 (c (n "square_calculator") (v "0.1.1") (h "1bvrnr8i9d5rnsnaps1jnazp4g5gzc05qbxr6djf88pqx66w74ry")))

