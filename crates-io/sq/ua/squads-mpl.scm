(define-module (crates-io sq ua squads-mpl) #:use-module (crates-io))

(define-public crate-squads-mpl-1.3.0 (c (n "squads-mpl") (v "1.3.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.9") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)))) (h "0qafcaxw3za80pvakihd8mf5z7snryaj80zh163hk9swxizyr8hf") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-squads-mpl-1.3.1 (c (n "squads-mpl") (v "1.3.1") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "solana-program") (r "<1.14.24") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.1") (d #t) (k 0)))) (h "1imvz8v9c20klrf8j6q5gfb0jb4scc6zq7jzrrp60da8j36yzh81") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

