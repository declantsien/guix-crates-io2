(define-module (crates-io sq ua square-ox-derive) #:use-module (crates-io))

(define-public crate-square-ox-derive-0.1.0 (c (n "square-ox-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "square-ox") (r "^0.1.2") (d #t) (k 2)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "141sa82bgjqmbjzmqaf82nr8yhqrg8gb00k0y5gjkv1g9rqj1ifi")))

