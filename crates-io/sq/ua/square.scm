(define-module (crates-io sq ua square) #:use-module (crates-io))

(define-public crate-square-0.0.1 (c (n "square") (v "0.0.1") (d (list (d (n "cocoa") (r "^0.15") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "palette") (r "^0.3") (d #t) (k 0)))) (h "0r1scn71g2818xw13cisbrgbmc9zj8x8f4ga8cwp01h5ji6kv7qd")))

