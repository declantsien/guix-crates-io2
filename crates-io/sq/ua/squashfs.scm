(define-module (crates-io sq ua squashfs) #:use-module (crates-io))

(define-public crate-squashfs-0.1.0 (c (n "squashfs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.18.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "14b311xy6m5l0pkgy3ij6kgs16cgcrsa2yzn7ys1vpwkmbbaka59") (f (quote (("gzip-sqs"))))))

