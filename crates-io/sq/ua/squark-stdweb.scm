(define-module (crates-io sq ua squark-stdweb) #:use-module (crates-io))

(define-public crate-squark-stdweb-0.1.0 (c (n "squark-stdweb") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "squark") (r "^0.1.1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "1nc14n3ighwvsd9766am87r8ka82inqjkwx0h0wmfxxin0rlpds1")))

(define-public crate-squark-stdweb-0.2.0 (c (n "squark-stdweb") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "squark") (r "^0.2.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "0nq23q8apq4sd6qv6qwkxbfvghdqqamnn5nkvjsv96c5lakhnip2")))

(define-public crate-squark-stdweb-0.3.0 (c (n "squark-stdweb") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "squark") (r "^0.3.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "0j0wj9njdz8fqkfvzk2pbr7jl2dy9i15wc8v54zyajarkwfagsn8")))

(define-public crate-squark-stdweb-0.3.3 (c (n "squark-stdweb") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "squark") (r "^0.3.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "05db7w7b0shc7l0isqqpxgx572z2cizxwr3dvcpa515f3csrxgy1")))

(define-public crate-squark-stdweb-0.4.0 (c (n "squark-stdweb") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "squark") (r "^0.4.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "082mkp19z9zcy65l32m6wg21wvbdq8d1qi5i2nmmd6nd1w6vwvza")))

(define-public crate-squark-stdweb-0.5.0 (c (n "squark-stdweb") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "squark") (r "^0.5") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.2") (d #t) (k 0)))) (h "0da6a7gpnp6bbbfd9d1l2zs2agbajh4dfixw24ya657k3qqlibam")))

