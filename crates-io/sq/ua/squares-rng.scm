(define-module (crates-io sq ua squares-rng) #:use-module (crates-io))

(define-public crate-squares-rng-0.1.0 (c (n "squares-rng") (v "0.1.0") (h "0bs4c6a8grv3qv77mhvy4c8zym3x130lwvaak84iskyi2gn18r01")))

(define-public crate-squares-rng-0.1.1 (c (n "squares-rng") (v "0.1.1") (h "1aby6fn5y33bk8179z7javpk9jjh8afy87dx3pm362rjy67pcgc1")))

(define-public crate-squares-rng-0.2.0 (c (n "squares-rng") (v "0.2.0") (h "07fvassidc4969646n1pp5ygh6hv4k13xvxzydvdw8m9mi10iwy6")))

