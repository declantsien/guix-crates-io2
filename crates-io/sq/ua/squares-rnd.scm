(define-module (crates-io sq ua squares-rnd) #:use-module (crates-io))

(define-public crate-squares-rnd-1.0.0 (c (n "squares-rnd") (v "1.0.0") (h "0pxmgybmyihbll924l9842zdp5bff7m4z1b2p1sq6j72gy4x7kii")))

(define-public crate-squares-rnd-2.0.0 (c (n "squares-rnd") (v "2.0.0") (h "0kb1jz01xsik91hp5a17np4gyv8dp5fk4b1w3dknpd0a59z6mbns")))

(define-public crate-squares-rnd-3.0.0 (c (n "squares-rnd") (v "3.0.0") (h "1d6zpjhi1p35gklv050nbgnhgvmwnkg2hkdj2ps1d6rgp3a2ss5x")))

(define-public crate-squares-rnd-3.1.0 (c (n "squares-rnd") (v "3.1.0") (h "14ikb80ycnmd1hjqpy1r6qldmnyaywzd1dcx9jcj9h2xig6zg729")))

