(define-module (crates-io sq ua squat) #:use-module (crates-io))

(define-public crate-squat-0.0.0 (c (n "squat") (v "0.0.0") (h "0pvcfvzkcddgrm7nd6lrqjz7b2zri8n1zsv2syfqac5x2q2whrhg")))

(define-public crate-squat-0.0.0-0 (c (n "squat") (v "0.0.0-0") (h "1fb6ky1jxrcyv2lzndv2cy8y3hxv4sizz7vzd5k6wgiym328wck9")))

(define-public crate-squat-0.0.1-0 (c (n "squat") (v "0.0.1-0") (h "1svzwmw0q7x91pzy9vkmksfjmy5gz97jixildywhhv7pxhsnkhgm")))

(define-public crate-squat-0.0.1 (c (n "squat") (v "0.0.1") (h "1krbwkjqw4jmhaibmbxln34rqjip5s6z1hm9swg9vij0shmzjn5i")))

