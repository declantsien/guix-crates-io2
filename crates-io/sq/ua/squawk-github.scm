(define-module (crates-io sq ua squawk-github) #:use-module (crates-io))

(define-public crate-squawk-github-0.2.3 (c (n "squawk-github") (v "0.2.3") (d (list (d (n "jsonwebtoken") (r "^7") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vkc4sjl1glq22d56dji4wm6whvn293p0h6v5882qp75hqpniigj")))

(define-public crate-squawk-github-0.3.0 (c (n "squawk-github") (v "0.3.0") (d (list (d (n "jsonwebtoken") (r "^7") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pqbxbfqpk4rpds33dslvkjizx7k571r140bgg68mhqm2dly9vfm")))

