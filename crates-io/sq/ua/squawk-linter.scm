(define-module (crates-io sq ua squawk-linter) #:use-module (crates-io))

(define-public crate-squawk-linter-0.2.3 (c (n "squawk-linter") (v "0.2.3") (d (list (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "squawk-parser") (r "^0.2.3") (d #t) (k 0)))) (h "0jsl4a3r6w9sa20c1slgfxrcapwfcdm3jx531d3yw2znkz8l0b9g")))

(define-public crate-squawk-linter-0.3.0 (c (n "squawk-linter") (v "0.3.0") (d (list (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "squawk-parser") (r "^0.3.0") (d #t) (k 0)))) (h "15jppk6z95q6qnpjhgjd3czaf5j9qk1sw3r39v73w1rfk7larjnx")))

