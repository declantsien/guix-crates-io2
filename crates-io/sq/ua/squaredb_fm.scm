(define-module (crates-io sq ua squaredb_fm) #:use-module (crates-io))

(define-public crate-squaredb_fm-1.0.0-beta (c (n "squaredb_fm") (v "1.0.0-beta") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mz77hcharib6v8z9bja1lnmbnr4567ml0wrjichcf5g6a5jq9dk")))

