(define-module (crates-io sq ua squale) #:use-module (crates-io))

(define-public crate-squale-0.1.0 (c (n "squale") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.4") (d #t) (k 0)))) (h "12mh2jvy5xd0a7ril0p06gzffshddkq4vpl7ixj8g4079zgsw2b4") (r "1.67")))

(define-public crate-squale-0.1.1 (c (n "squale") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.4") (d #t) (k 0)))) (h "0l4vwdxlqv9xl6qmfxs4zymjinn6r64jixijdhxv4kr8k4kkhd0n") (r "1.67")))

(define-public crate-squale-0.1.2 (c (n "squale") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.4") (d #t) (k 0)))) (h "1y8zaf1gdamsy6g5mv5vd21zhvay695a9ldz3izkgnvw0s9bhp8f") (r "1.67")))

(define-public crate-squale-1.0.0 (c (n "squale") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.4") (d #t) (k 0)))) (h "0mcfm1r822gki860q0mh5ac7wg6xbanba2004l5w3nbp220y1d71") (r "1.67")))

(define-public crate-squale-1.0.1 (c (n "squale") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.0") (f (quote ("lite"))) (k 0)))) (h "0anjwrp9y4sqdglahvwj89jmhfk89f2xsmq82r0wzfyadx579has") (r "1.67")))

