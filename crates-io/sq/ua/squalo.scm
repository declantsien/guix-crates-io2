(define-module (crates-io sq ua squalo) #:use-module (crates-io))

(define-public crate-squalo-0.1.0 (c (n "squalo") (v "0.1.0") (d (list (d (n "polipo") (r "^0.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1qv5x4d4s5y4fx88q7i7pa53a9wjk5njnpf29a53kb7vqfig8z15")))

(define-public crate-squalo-0.1.1 (c (n "squalo") (v "0.1.1") (d (list (d (n "polipo") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1m3v8yfp1mzl54x1ik26wad28z3w8bcy9n76m51gq5lz1bwbbp49")))

(define-public crate-squalo-0.1.2 (c (n "squalo") (v "0.1.2") (d (list (d (n "polipo") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tungstenite") (r "^0.13.0") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0pxwr3h7ggm2hs70y1nawgvagjhw6qaigcdgffn4a7r7xrpw5lsx")))

(define-public crate-squalo-0.1.3 (c (n "squalo") (v "0.1.3") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.13") (d #t) (k 0)) (d (n "polipo") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0y6ppwf7q4jbfgx608ih09k5hh8vxy2bclqcgj51nz00a90ygvif")))

(define-public crate-squalo-0.1.4 (c (n "squalo") (v "0.1.4") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.13") (d #t) (k 0)) (d (n "polipo") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1n7gdcx8g459fv6l4yjlbk5784va692g64mvr7lj8f4yap94wzx9")))

(define-public crate-squalo-0.1.5 (c (n "squalo") (v "0.1.5") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.13") (d #t) (k 0)) (d (n "polipo") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1960r6kld3j44l1jwa7iqqx9mfwh5zjkvcffx37ghdzlya6ckqqw")))

(define-public crate-squalo-0.1.6 (c (n "squalo") (v "0.1.6") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.13") (d #t) (k 0)) (d (n "polipo") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1bgmriq7ng17k85qa0i8dq9z854gardzzmpskn752pj9ka70920n")))

