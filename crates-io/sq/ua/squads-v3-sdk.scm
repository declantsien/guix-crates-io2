(define-module (crates-io sq ua squads-v3-sdk) #:use-module (crates-io))

(define-public crate-squads-v3-sdk-0.1.0 (c (n "squads-v3-sdk") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "1djsz78bbz3ccjin8s47qi7xcxhp3zgf1dr4a1pyh3f2gjikyv02") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-squads-v3-sdk-0.1.1 (c (n "squads-v3-sdk") (v "0.1.1") (d (list (d (n "anchor-gen") (r "^0.3.1") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "1iv2li3y89y0d4k8h2qyv39gl7n0xi5xk4djsipi6p00jycbq8bl") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-squads-v3-sdk-0.1.2 (c (n "squads-v3-sdk") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "squads-mpl") (r "^1.3.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "02x9djhbyn7cy46b8s1l1pb28knwpifrys8h1m4x1wzcxxn1aa8g") (f (quote (("default"))))))

