(define-module (crates-io sq ua squash-sys) #:use-module (crates-io))

(define-public crate-squash-sys-0.8.0 (c (n "squash-sys") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1ylh3pwd45qb85f42syy6d655davh3daccfymjjby36klr7jd496") (f (quote (("wide-char-api") ("nightly" "lazy_static/nightly"))))))

(define-public crate-squash-sys-0.8.1 (c (n "squash-sys") (v "0.8.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0vz2nsb38p78ccrrcdbrf00d3yg8v6d4mna9v6vy7xp6p84blm51") (f (quote (("wide-char-api") ("nightly" "lazy_static/nightly")))) (y #t)))

(define-public crate-squash-sys-0.8.2 (c (n "squash-sys") (v "0.8.2") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "18157d57h9lm6qfm4h5fflfcjmvgv62y79g2gqspcjfjb31z4rfr") (f (quote (("wide-char-api") ("nightly" "lazy_static/nightly"))))))

(define-public crate-squash-sys-0.8.3 (c (n "squash-sys") (v "0.8.3") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1qm26scbydlgm5hkj2agjdllw1615cciq912rzk6jlwyc6204abv") (f (quote (("wide-char-api") ("nightly" "lazy_static/nightly"))))))

(define-public crate-squash-sys-0.9.0 (c (n "squash-sys") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0h4br2yyz94746wz5my30shyc6gsi4jc6fqmdrsbf6fqj7g9s7yv") (f (quote (("wide-char-api") ("nightly" "lazy_static/nightly"))))))

(define-public crate-squash-sys-0.10.0 (c (n "squash-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.28.0") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1vn7rxa2j21aj96xa2bzrrw6v5ldz1yjvw0ngfjr6r76180imwvn") (f (quote (("wide-char-api") ("nightly" "lazy_static/nightly"))))))

(define-public crate-squash-sys-1.0.0 (c (n "squash-sys") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0bffqjjs63ndkg0syc7rybmq5jvvjrgh8i92m41msfr11g562gj2") (f (quote (("wide-char-api")))) (l "libsquash0.8")))

(define-public crate-squash-sys-1.0.1 (c (n "squash-sys") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0vq49adn7ni8gspyn6q18w696ma7pwaxfrz5b62xjfri4rb4bd8y") (f (quote (("docs-rs")))) (l "libsquash0.8")))

(define-public crate-squash-sys-1.0.2 (c (n "squash-sys") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "07w236bq0ndk7ly2mjyvj400iql95lk430yf95f2zn0qrxkbxxia") (f (quote (("docs-rs")))) (l "libsquash0.8")))

