(define-module (crates-io sq ua squall_dot_io_resp) #:use-module (crates-io))

(define-public crate-squall_dot_io_resp-0.1.0 (c (n "squall_dot_io_resp") (v "0.1.0") (h "1qxmlj3yln4226jarqpq1fyzql0vz8hkgnpi22423c7357g9gmbx")))

(define-public crate-squall_dot_io_resp-0.1.1 (c (n "squall_dot_io_resp") (v "0.1.1") (h "1yjqzx95zpif5i755x4b3pa5ybbw1c0knf52zwp9v9g78hzpy1ji")))

(define-public crate-squall_dot_io_resp-0.1.2 (c (n "squall_dot_io_resp") (v "0.1.2") (h "1ny0nq1c99cx77ig5q76c2srfdrhiccqqqwc02f5k9c4g0n5vjjy")))

(define-public crate-squall_dot_io_resp-1.0.0 (c (n "squall_dot_io_resp") (v "1.0.0") (h "0df6apf5q87kfqb5z09sfcca4h98qbsfw1l2jinvpsnnddvi3lb6")))

