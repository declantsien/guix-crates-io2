(define-module (crates-io sq ua squaredb_enc) #:use-module (crates-io))

(define-public crate-squaredb_enc-1.0.0-beta (c (n "squaredb_enc") (v "1.0.0-beta") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "bcrypt") (r "^0.15.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (f (quote ("password-hash" "simple"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13llwaxl3631dcslxv2f2klzpzd9x5g0w5pkmxdsjczvrjn21qsc")))

