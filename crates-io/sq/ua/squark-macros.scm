(define-module (crates-io sq ua squark-macros) #:use-module (crates-io))

(define-public crate-squark-macros-0.1.0 (c (n "squark-macros") (v "0.1.0") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.1.1") (d #t) (k 0)))) (h "17diiiqd7y7zrq37n6bwwyqd057j2zfyi0341p59c8hlc9m68dlk")))

(define-public crate-squark-macros-0.2.0 (c (n "squark-macros") (v "0.2.0") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.2.0") (d #t) (k 2)))) (h "1sq00jy88fgwz2nngmfjcnhzq8z2skp9dssavsdd84ghlbbmvw2d")))

(define-public crate-squark-macros-0.3.0 (c (n "squark-macros") (v "0.3.0") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.3.0") (d #t) (k 2)))) (h "1ila9236jnxx3xbghm3iy6wccy561hv3597d06qpvdak80fs21nb")))

(define-public crate-squark-macros-0.4.0 (c (n "squark-macros") (v "0.4.0") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.4.0") (d #t) (k 2)))) (h "0wrz156k07jd0h6rs0k385w5rm9gjzymcsydlrk8khlpv34mi7k1")))

(define-public crate-squark-macros-0.5.0 (c (n "squark-macros") (v "0.5.0") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.5") (d #t) (k 2)))) (h "1ljfsd3kyv7j8rkrdqqavnz98d9mjwxzlxycvb78g0fg5lxfia0l")))

(define-public crate-squark-macros-0.5.1 (c (n "squark-macros") (v "0.5.1") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.5") (d #t) (k 2)))) (h "0a9xipvp2i56p5yjmn28mbi4r6f8rawlrmm3i0x90bvkvrnb06ky")))

(define-public crate-squark-macros-0.5.2 (c (n "squark-macros") (v "0.5.2") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.5") (d #t) (k 2)))) (h "12rx74rh3krkw2a5ms13j04vz8a4y91sfbgzdgz2n2iwdcdsl1n4")))

(define-public crate-squark-macros-0.5.3 (c (n "squark-macros") (v "0.5.3") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.5") (d #t) (k 2)))) (h "0qiyl4vy7mmmiykb31kfy4slih01iw7fdzk6q9v39cvijksz9pnl")))

(define-public crate-squark-macros-0.5.4 (c (n "squark-macros") (v "0.5.4") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.5") (d #t) (k 2)))) (h "1zlxmwxn5m5zxy00g0xxj7fy264y5pks7fnqqc8pjm77gf29ggzw")))

(define-public crate-squark-macros-0.6.0 (c (n "squark-macros") (v "0.6.0") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "squark") (r "^0.6.0") (d #t) (k 2)))) (h "1z9mbwvb6y7jjyf0nkfcdjrfmv829kp4xrw1vn6kyrpgik1r3cw1")))

(define-public crate-squark-macros-0.6.1 (c (n "squark-macros") (v "0.6.1") (d (list (d (n "pest") (r "^2.0.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.0") (d #t) (k 0)) (d (n "squark") (r "^0.6.0") (d #t) (k 2)))) (h "0xj30hi9imymjl4g6f8d4la5rnbsksaz7j49dx8ghw3yg0k1iskn")))

(define-public crate-squark-macros-0.7.0 (c (n "squark-macros") (v "0.7.0") (d (list (d (n "pest") (r "^2.0.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.0") (d #t) (k 0)) (d (n "squark") (r "^0.7.0") (d #t) (k 2)))) (h "0dibpgq6p24y9gbh8xy7fnfzaf14c8cxxh0jvm3k7xq0byhryc3n")))

