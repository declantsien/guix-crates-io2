(define-module (crates-io sq m_ sqm_parser) #:use-module (crates-io))

(define-public crate-sqm_parser-1.0.1 (c (n "sqm_parser") (v "1.0.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "03x75qg4g9gjcby1h06x91y4j9kcs5k3fjd3rkmpfqp1x7g1kvar")))

