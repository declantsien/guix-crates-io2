(define-module (crates-io sq ri sqrite) #:use-module (crates-io))

(define-public crate-sqrite-0.1.0 (c (n "sqrite") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0fvdn4kirc7i3rlj570qxkvyz2qxjxbd41bbdll912ypvi869i6h")))

(define-public crate-sqrite-0.2.0 (c (n "sqrite") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0rzfscdidhh0kn50xspgzbf6jlxnzyk8phc5knp6zy3jhkkifns6")))

(define-public crate-sqrite-0.3.0 (c (n "sqrite") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0rs2qdz77mjhhjgj8gc6bc4zr0xcq97fz11sydz44gzgc7fhqz9d")))

(define-public crate-sqrite-0.4.0 (c (n "sqrite") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1c276n43hfpmhlnxvpjfwizqnmvpwa2ja1n3j7fgyafzcs83100f")))

(define-public crate-sqrite-0.5.0 (c (n "sqrite") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0vmvv9rfhwk9xkysmfq3d8zlvxgl271cj40kmn0ahza2c2bnhvl7")))

(define-public crate-sqrite-0.6.0 (c (n "sqrite") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ynhvqk0r4nlsin28250rigans41kadwm63sxdb6f1lgkk7dhp3k")))

(define-public crate-sqrite-0.6.1 (c (n "sqrite") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0xvn9z5ka3yl8pf1mplw677lh1jgryph9l2j4d88r106r84vna51")))

(define-public crate-sqrite-0.6.2 (c (n "sqrite") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "08kin8mjwxhhf14rv5ihjx1wkiq9dpllb8lc8dxr24rzin7mi4np")))

(define-public crate-sqrite-0.7.0 (c (n "sqrite") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1chpr1lhkswlip3d88s0hamaji5sbj6mrawwkk080h6nb2i6rz2z")))

(define-public crate-sqrite-0.7.1 (c (n "sqrite") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0qdyfmakngp9zy9439mgam8aw2hg0q5d0za6icc1l9b9vd1ykgqf")))

(define-public crate-sqrite-0.8.0 (c (n "sqrite") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.64.0") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("fmt" "std" "ansi"))) (k 2)))) (h "1ppfqs80d6n7kmcky7kk7mrp2r2754hgcw5zx79218sxv4amzvn2")))

(define-public crate-sqrite-0.8.1 (c (n "sqrite") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.64.0") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("fmt" "std" "ansi"))) (k 2)))) (h "1b44jpd47qd55mq7jifqsc4fxgy9g5bnyj3yhrdcxwkiw17rh16j")))

(define-public crate-sqrite-0.8.2 (c (n "sqrite") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.64.0") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("fmt" "std" "ansi"))) (k 2)))) (h "1i7jv6vx6jdqfh63xmqf2prfx4xnkgvzqgpln3w2v3nrp9knzzx9")))

(define-public crate-sqrite-0.8.3 (c (n "sqrite") (v "0.8.3") (d (list (d (n "bindgen") (r "^0.64.0") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("fmt" "std" "ansi"))) (k 2)))) (h "023829b31fh97hajpp6j395n5djzvni8hwcafk1jm0y2wrvffr6z")))

(define-public crate-sqrite-0.8.4 (c (n "sqrite") (v "0.8.4") (d (list (d (n "bindgen") (r "^0.64.0") (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("fmt" "std" "ansi"))) (k 2)))) (h "0z0vfp3k7gm0wvisr97sackycb3p1q5s5g20rkrlq954v76d42jq")))

