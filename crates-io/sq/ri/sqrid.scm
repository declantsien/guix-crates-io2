(define-module (crates-io sq ri sqrid) #:use-module (crates-io))

(define-public crate-sqrid-0.0.1 (c (n "sqrid") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0x6si20wqcrblb95hbphk8hb8mb5ngfplw43psqr3asqv39h0jpz")))

(define-public crate-sqrid-0.0.2 (c (n "sqrid") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "0aif9aq75ygxxb95sqk4n8vq2g8f7mg7pnbvhj5fxnc8k0q65b1p")))

(define-public crate-sqrid-0.0.3 (c (n "sqrid") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0zp49j6vgg9nrhm9j1awwsv9nd5pqifxrpgap33qcq1pn94pz55q")))

(define-public crate-sqrid-0.0.4 (c (n "sqrid") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1w35sv1fhyvz7k73zivlzgq308qcwbz7zzgf23ns0wj4ykd27y6n")))

(define-public crate-sqrid-0.0.5 (c (n "sqrid") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1cssk59253hw8fap16k8ig2g1qhyd6sfgpsfym3bmk48bgm3d1gw")))

(define-public crate-sqrid-0.0.6 (c (n "sqrid") (v "0.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "16qd2zfp183b4jp11x9iklrdb392rg9p174qgk56jnjy7zfiqb05")))

(define-public crate-sqrid-0.0.7 (c (n "sqrid") (v "0.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "05xr70mf9s1w6ilr4gq6ysmx3bsnqry3mlmp9ircv635899qrkzv")))

(define-public crate-sqrid-0.0.8 (c (n "sqrid") (v "0.0.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0pli58200pvi098g8rlglm5scp96lnjx20kfa7pp2z6fn7jdb1km")))

(define-public crate-sqrid-0.0.9 (c (n "sqrid") (v "0.0.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1w5452v5lsm4crwl58188bswprafwyq01sycgviinycv8fmbxxir")))

(define-public crate-sqrid-0.0.10 (c (n "sqrid") (v "0.0.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "104jxz1klb70fkkfj17fgjc9bkr3al4x57b9dx999zl662fq7gmz")))

(define-public crate-sqrid-0.0.11 (c (n "sqrid") (v "0.0.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0679cxcrzx5z839znqfj3zarrx9narydqj7gpi6j56by4zsgipbb")))

(define-public crate-sqrid-0.0.12 (c (n "sqrid") (v "0.0.12") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1w3iclyg1rl1aq5hzyfd57wz7rg38nh2iv3pf65v5i5b60l8iz33")))

(define-public crate-sqrid-0.0.13 (c (n "sqrid") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0f1dpg6hsinqvkkbidgqxf5m6y5gcd8fyvsn1gdcqzbgnvwd11z6")))

(define-public crate-sqrid-0.0.14 (c (n "sqrid") (v "0.0.14") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1pibgbd1lyp30kdk898jpnxhlkchv6k8189gjzapq6kj13998jbz")))

(define-public crate-sqrid-0.0.15 (c (n "sqrid") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1ii623xaqgi367q5i8k5g9rz1ha9ln211cpsm2hjvcj553vn9prb")))

(define-public crate-sqrid-0.0.16 (c (n "sqrid") (v "0.0.16") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1wwws48hmg7x32hn7rx90p5qlwy6djrc8lr3wijsdr7ga4lj4kqd")))

(define-public crate-sqrid-0.0.17 (c (n "sqrid") (v "0.0.17") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "059l0qslccdj22prdaix0d3ff9g53pr2bihf7508ql0i9k0cfbx7")))

(define-public crate-sqrid-0.0.18 (c (n "sqrid") (v "0.0.18") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "02i2k09iqnv8q43vn57qcbxz5m9z8qxhxxkyaiidyqhggi1fzg22")))

(define-public crate-sqrid-0.0.19 (c (n "sqrid") (v "0.0.19") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "17rrbjldf25jgy72bqals0kqfbn6r648h0fr7snma8m15rfgryrp")))

(define-public crate-sqrid-0.0.20 (c (n "sqrid") (v "0.0.20") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0r68y8qpafrqjmrm9pcda7qaszbd313pvkkx91dfhaskricw1ys8")))

(define-public crate-sqrid-0.0.21 (c (n "sqrid") (v "0.0.21") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "12zhqpda37wcks3079w2h6j310maa2k6dl4cicyiv3zrlr1vsy7r")))

(define-public crate-sqrid-0.0.22 (c (n "sqrid") (v "0.0.22") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "09n1wi522bv0m0ck1556c5rskljbv0d1xdxyg1qw8bwfi12apgwj")))

(define-public crate-sqrid-0.0.23 (c (n "sqrid") (v "0.0.23") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0fjzk1glccm1683x008vwjfaz286mi5sg14pxxqq3k1y1pvp98fg")))

(define-public crate-sqrid-0.0.24 (c (n "sqrid") (v "0.0.24") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1mab593wnr9wdxq3mnybkvrzf5m9lvv141w465jw7r2vrl4qsbnq")))

