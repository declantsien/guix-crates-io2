(define-module (crates-io sq ri sqript) #:use-module (crates-io))

(define-public crate-sqript-0.1.0 (c (n "sqript") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "os-ext"))) (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (f (quote ("term" "process" "fs"))) (d #t) (k 0)))) (h "001a5vybrwhgrq44zqwh185x08zdmyd3v6b54id1yv5i4z1h0izi")))

