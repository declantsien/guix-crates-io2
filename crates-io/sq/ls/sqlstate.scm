(define-module (crates-io sq ls sqlstate) #:use-module (crates-io))

(define-public crate-sqlstate-0.0.0 (c (n "sqlstate") (v "0.0.0") (h "0dqg6vdwjwzmkbf76i7ivdqfl0k9vll6k1ivw2kv3mf7sbbv4v3x")))

(define-public crate-sqlstate-0.1.0 (c (n "sqlstate") (v "0.1.0") (d (list (d (n "sqlstate-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "1kvmsv50nqa25l5vnl9x0v6kcmkc0pyw7s9rr55razd6vfav0s51") (f (quote (("postgres"))))))

