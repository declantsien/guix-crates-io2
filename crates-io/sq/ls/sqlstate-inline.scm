(define-module (crates-io sq ls sqlstate-inline) #:use-module (crates-io))

(define-public crate-sqlstate-inline-0.1.0 (c (n "sqlstate-inline") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0c202ydi6f5l4084p98bcg8mxap4ik78b46vw37rv9wwm3ap1xl0") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror") ("serde" "dep:serde"))))))

(define-public crate-sqlstate-inline-0.1.1 (c (n "sqlstate-inline") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ya2nb7xr0qc89jp6g1l06ph8n247rv49j59xc5ipavw1k5icjki") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror") ("serde" "dep:serde"))))))

(define-public crate-sqlstate-inline-0.1.2 (c (n "sqlstate-inline") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1j55h6mckqi3mp8nylcjwynhpwjzsg7zwrs355j992avh4rhkd1y") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror") ("serde" "dep:serde"))))))

