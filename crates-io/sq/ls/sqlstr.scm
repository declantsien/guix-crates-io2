(define-module (crates-io sq ls sqlstr) #:use-module (crates-io))

(define-public crate-sqlstr-0.1.0 (c (n "sqlstr") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.7.3") (f (quote ("postgres" "runtime-tokio"))) (k 2)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "net"))) (k 2)))) (h "0fh87g7w28ycgw5iaabg562gxaqaycv8x4wl6f504942iir2wnyw") (f (quote (("std" "fmt") ("fmt")))) (r "1.70")))

