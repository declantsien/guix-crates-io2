(define-module (crates-io sq ls sqlstate-macros) #:use-module (crates-io))

(define-public crate-sqlstate-macros-0.0.0 (c (n "sqlstate-macros") (v "0.0.0") (h "0w9fqjq7rfvrmdjwp0xf6hg48yah160jrikbmsr3lq45j285qzjs")))

(define-public crate-sqlstate-macros-0.1.0 (c (n "sqlstate-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wxl6z2gapqgcxi3zjl774b0pcnz1ncfb605jlhqf9v3gnf0c0a0")))

