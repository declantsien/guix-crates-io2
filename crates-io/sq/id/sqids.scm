(define-module (crates-io sq id sqids) #:use-module (crates-io))

(define-public crate-sqids-0.1.0 (c (n "sqids") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.173") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "03zi976rvbpsm8f1zng38c354hcpx0axbghkz7hynfnb6n65j0z9")))

(define-public crate-sqids-0.1.1 (c (n "sqids") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.173") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0vrd89pw1i465xmj2ppw62cmq5f08kvpsdx44cfbj1diq25j7xkh")))

(define-public crate-sqids-0.2.0 (c (n "sqids") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1z7y9s0i26b3pvw5ncr9wdyxb2dciz5f9g6jz54fzpaqya60r2iv")))

(define-public crate-sqids-0.2.1 (c (n "sqids") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0d5q7brvwjjnjh8hc6rs80lzlb747r3d9g8hf2j9hfppk5gddbgv")))

(define-public crate-sqids-0.3.0 (c (n "sqids") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)))) (h "19827j4xayhv94r51jr7akr9i0c6y44c2yii87bs8s1699g6f58c")))

(define-public crate-sqids-0.3.1 (c (n "sqids") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "02bcpkqrbz0x4hw55k7cl12aqzw9ppkydlhrzwlrjpxg79fdwcfz")))

(define-public crate-sqids-0.4.0 (c (n "sqids") (v "0.4.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1pwfsfnh628w7v45fjl4p96vf6fxihx7l8xd59vqx0x9ign7jizs")))

(define-public crate-sqids-0.4.1 (c (n "sqids") (v "0.4.1") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "17b3hh66hnh1blns5g314qqrf9i3i7084bsv9sh0sksrmq88yclz")))

