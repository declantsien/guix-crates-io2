(define-module (crates-io sq lx sqlx-models-derive) #:use-module (crates-io))

(define-public crate-sqlx-models-derive-0.1.0 (c (n "sqlx-models-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1la2b9digp9794yh1yybpp4cvfjd1gnmpjzh4w6qj5c789jz8qlv") (y #t) (r "1.56")))

(define-public crate-sqlx-models-derive-0.1.1 (c (n "sqlx-models-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qc2gaxhlpr7s542gwqm981yzfqxsl4gl1w05iz6kinrvirmwx4j") (y #t) (r "1.56")))

(define-public crate-sqlx-models-derive-0.1.2 (c (n "sqlx-models-derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1q598s1nh4jj4w0x03q0fqaxg76v1vw2cn1fs0mvx860l6n51vjr") (r "1.56")))

(define-public crate-sqlx-models-derive-0.1.5 (c (n "sqlx-models-derive") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n0y1y4vy5vmg479343j8247j6s979gpfjcz34jgbgk7259cyg38") (r "1.56")))

(define-public crate-sqlx-models-derive-0.1.6 (c (n "sqlx-models-derive") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "03v90sz6apnj417rywn6nz4lycix1crmcv80chsxp6j9m2yq7i5f") (r "1.56")))

