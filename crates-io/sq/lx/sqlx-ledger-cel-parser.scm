(define-module (crates-io sq lx sqlx-ledger-cel-parser) #:use-module (crates-io))

(define-public crate-sqlx-ledger-cel-parser-0.0.1 (c (n "sqlx-ledger-cel-parser") (v "0.0.1") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1wa3bsiv0b14355jrvhq4m0p2g1i6pxsxwhb1m86cbmin10f98wk") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.0.4 (c (n "sqlx-ledger-cel-parser") (v "0.0.4") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1dymnmqyjwm5ag6hfdw1zm4ydc7938kgjr43pj5vq7hzjly4z8vl") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.0.5 (c (n "sqlx-ledger-cel-parser") (v "0.0.5") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1cz9mgv5n1zz32bb89za3wh3wvwgr97mc6z0f02b1x0bz5kyzfc8") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.1.0 (c (n "sqlx-ledger-cel-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "000ml9mvvy1zxmz6l1j0z91c1xl2zbagim5y8iirq3p2kk4vh5gs") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.1.1 (c (n "sqlx-ledger-cel-parser") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0xyr3dcyd2x93c0bgln8igj9lqgzfc46p9p0prfq1dqi44jzn8yp") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.2.0 (c (n "sqlx-ledger-cel-parser") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0y2p42yik59nn7jpfsg1mbhwyf5ccrbrz2hmvvjxsy16r1nvxh7v") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.2.1 (c (n "sqlx-ledger-cel-parser") (v "0.2.1") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "02gazgp4cvpzfl8n3hwfxiw339lzr9rww5ivwj08p8lhvcrfkm0z") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.3.0 (c (n "sqlx-ledger-cel-parser") (v "0.3.0") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "017bx5ws3bmv2slc4sy81b2jya4i29sbjid6iag7crdq7y71yabh") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.4.0 (c (n "sqlx-ledger-cel-parser") (v "0.4.0") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0anjnms3mvvhl2j20dl1qsckr8n6bkn3xkrb36f67l30giwx7vx2") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.0 (c (n "sqlx-ledger-cel-parser") (v "0.5.0") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0l5rw48nzi6s8a5ynlf999aiisxcg81h585idc2pl6kifnzmj166") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.1 (c (n "sqlx-ledger-cel-parser") (v "0.5.1") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "009mlh0zvn0k8qjiw81l5axs1dpkw287jmd0y95729vj2k8vyn83") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.2 (c (n "sqlx-ledger-cel-parser") (v "0.5.2") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0skd3108aazkr1ws8sybgpj44wp1k30sls2rciymb1kyjj9yx95h") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.3 (c (n "sqlx-ledger-cel-parser") (v "0.5.3") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0d2ammpim4alafgimbc6kqiw05ks13izjr8n1afz0lkgifb370vg") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.4 (c (n "sqlx-ledger-cel-parser") (v "0.5.4") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0zxyx9a0q84cig0556mw8z3nm6a2ki3sm4261s3wmi0m1xn9n6s8") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.5 (c (n "sqlx-ledger-cel-parser") (v "0.5.5") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1bf5jjrf642f58s3mjknm4n7jrjap3pp9igabbw854rfx4rfpjg8") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.6 (c (n "sqlx-ledger-cel-parser") (v "0.5.6") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0i7sq5zr74kjjpxfs231sj5jr0cxrmk3w5316nw4k53ffwgg9i0r") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.7 (c (n "sqlx-ledger-cel-parser") (v "0.5.7") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "194q8mi5ngcray2drzx6m2jsn12ylc9ch90ahpnsnyaj20h76zdn") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.8 (c (n "sqlx-ledger-cel-parser") (v "0.5.8") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0nw1jy40djdlq6j366iiz72sm3622cj4nfjw20l3d0ys9jk0c90w") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.9 (c (n "sqlx-ledger-cel-parser") (v "0.5.9") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1qcf56zyi0prp8v9qxrwrgbf7rm9f76nx5raysxx1zscvicd5z5y") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.10 (c (n "sqlx-ledger-cel-parser") (v "0.5.10") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1p3lz02wb38xk14fdafj1xnsc4x6ky3ka9wj2xg1cl5h8gvzq5bd") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.11 (c (n "sqlx-ledger-cel-parser") (v "0.5.11") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1n7r1j5kzrhwz1iw8ha24s9hmbxhca7wgncjd2r4jdiz52brck9v") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.5.12 (c (n "sqlx-ledger-cel-parser") (v "0.5.12") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0b2cmwx2hk20xxsbiagczp503lnkj97s21ns93jrfkar5mj2byh9") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.6.0 (c (n "sqlx-ledger-cel-parser") (v "0.6.0") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "1z0h4ww2091r2db9y732zldshkf5w9m99k8dw137cp96hynyrrlf") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.6.1 (c (n "sqlx-ledger-cel-parser") (v "0.6.1") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "19di00xb0ry5yni2mqh2kissml5w8xafyp0xhwdn5f3xg0lafa6h") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.0 (c (n "sqlx-ledger-cel-parser") (v "0.7.0") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "1i6vyk8qdzgijx93wyv3ddpxarf8ddy6w6a7si1dy1ydz256yn5i") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.1 (c (n "sqlx-ledger-cel-parser") (v "0.7.1") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "0a80xd5pf7fiwan5rydfch0ab465sif8fwlmpk71r95b3wg20n82") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.2 (c (n "sqlx-ledger-cel-parser") (v "0.7.2") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "1bj5mjfsm8n7w6s3i16zbmd0hbsrl1ri2sj71wzd7qqq6vrlkrhr") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.3 (c (n "sqlx-ledger-cel-parser") (v "0.7.3") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "1zhn04kjc400hxygr6fx4vwvkpfwkqjnmnhj12p4fdn3khmi072f") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.4 (c (n "sqlx-ledger-cel-parser") (v "0.7.4") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "1k77il4h55nlc65f2wii7ddaafc9y2kqbj75xpray55hm3gghh18") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.5 (c (n "sqlx-ledger-cel-parser") (v "0.7.5") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "1b2iwp6zblgxh7p6cqd5dyfnvry9xhzvq7mq1qhylbqf553j88c4") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.6 (c (n "sqlx-ledger-cel-parser") (v "0.7.6") (d (list (d (n "lalrpop") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "=0.19.9") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "=1.7.3") (d #t) (k 0)))) (h "1c8kmfr3kw27hgl60gzp2wixr8snx6b8gm6x5sp49016n3038jkl") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.7.7 (c (n "sqlx-ledger-cel-parser") (v "0.7.7") (d (list (d (n "lalrpop") (r "^0.19.12") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.12") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1zrcqg6h1hhqhnk9sydqbwkik525s0ak1wh58phndjchg2c59308") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.8.0 (c (n "sqlx-ledger-cel-parser") (v "0.8.0") (d (list (d (n "lalrpop") (r "^0.19.12") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.12") (f (quote ("lexer"))) (d #t) (k 0)))) (h "17y6y0gk5rg967vzfz6zniyqfldxnm4zjsmfmcdq4ryp4nhbhkm8") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.8.1 (c (n "sqlx-ledger-cel-parser") (v "0.8.1") (d (list (d (n "lalrpop") (r "^0.19.12") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.12") (f (quote ("lexer"))) (d #t) (k 0)))) (h "15qm5l00hf14cjvlrp49gvpl2jml2hqhrz5clff3s4zzlwx65v2n") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.8.2 (c (n "sqlx-ledger-cel-parser") (v "0.8.2") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0pqrd912sq78vhd55yh7n720kp4y71q8iisv69sa38kdgdm1agjb") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.8.3 (c (n "sqlx-ledger-cel-parser") (v "0.8.3") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0m9gs0qwk0s81bqcybzwaf0va36rgnbhha130bbmnnj8r9fbdn0x") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.8.4 (c (n "sqlx-ledger-cel-parser") (v "0.8.4") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0g7lv60kyi66hzr98yw6qmch74d2pcfi4n09cbz2v74zgv6inr59") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.8.5 (c (n "sqlx-ledger-cel-parser") (v "0.8.5") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1vf4729drakmsjrdnbvz3b0c2j69bzndqar61ndrs65qvbg95rii") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.8.6 (c (n "sqlx-ledger-cel-parser") (v "0.8.6") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0wricg7jlz79df0shdg5lzwbicnq2n91cgyyv7r048lpkwi6rspy") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.9.0 (c (n "sqlx-ledger-cel-parser") (v "0.9.0") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0ckdpwsw43j112cqvkxzcigc2hajjxckc6jmn3qxp8i1jyyrgx99") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.10.0 (c (n "sqlx-ledger-cel-parser") (v "0.10.0") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0vzznswy5zl9dwq1wpkyx44as4g68rdf41wvnb1j6f9l4ymx6m1x") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.11.0 (c (n "sqlx-ledger-cel-parser") (v "0.11.0") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1rcfcggcv3rakfn07pf885i2rshgikp5vb8bsnhm6l7fad2a9rvx") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.11.1 (c (n "sqlx-ledger-cel-parser") (v "0.11.1") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "1iz5n3jvsdhc5fnw2zjzvnjnr7qxpdmvp1pxdswiavpb5mj6cdcj") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.11.2 (c (n "sqlx-ledger-cel-parser") (v "0.11.2") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "0pv382mvkfzpmn5fnijpx19rmy1z37ca2ndwih0ariryg1bzqgpn") (f (quote (("fail-on-warnings"))))))

(define-public crate-sqlx-ledger-cel-parser-0.11.3 (c (n "sqlx-ledger-cel-parser") (v "0.11.3") (d (list (d (n "lalrpop") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)))) (h "17s246l9wlrvlq7ndxmdhnwdzqw7wv8npsnbz0xb5awpvli3dmid") (f (quote (("fail-on-warnings"))))))

