(define-module (crates-io sq lx sqlx-database-tester-wsf) #:use-module (crates-io))

(define-public crate-sqlx-database-tester-wsf-0.1.0 (c (n "sqlx-database-tester-wsf") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cfcpjl9s6v0j5janrk07h3grsicdlmxw8pz0srk4y16inf6zp28")))

(define-public crate-sqlx-database-tester-wsf-0.1.1 (c (n "sqlx-database-tester-wsf") (v "0.1.1") (d (list (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "04v6vckdfjkmsdizkcpdssn1kglljfc2jqaqd1mmm2yp1w73gzai")))

(define-public crate-sqlx-database-tester-wsf-0.1.2 (c (n "sqlx-database-tester-wsf") (v "0.1.2") (d (list (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "04gx2jmsi5ihdh9gf76wzcxrbmwz7374lfidymgqa0nbc890haj9")))

(define-public crate-sqlx-database-tester-wsf-0.1.3 (c (n "sqlx-database-tester-wsf") (v "0.1.3") (d (list (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "04mxv93x9d35mbs0w7qnfdbca5g01h60lym72rm2wi241jmb29y8")))

