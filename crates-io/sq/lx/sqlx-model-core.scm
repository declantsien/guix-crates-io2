(define-module (crates-io sq lx sqlx-model-core) #:use-module (crates-io))

(define-public crate-sqlx-model-core-0.0.1-beta.1 (c (n "sqlx-model-core") (v "0.0.1-beta.1") (d (list (d (n "simple_asn1") (r "^0.5.1") (d #t) (k 0)) (d (n "sqlx") (r "~0.5.5") (f (quote ("mysql" "runtime-tokio-native-tls" "offline"))) (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("full"))) (d #t) (k 0)))) (h "15klr52kkzq60sl3dy47vxwas1xsv8z8yrymdzcg6fykxdrl0mpj") (y #t)))

