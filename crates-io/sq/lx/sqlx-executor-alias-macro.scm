(define-module (crates-io sq lx sqlx-executor-alias-macro) #:use-module (crates-io))

(define-public crate-sqlx-executor-alias-macro-0.1.0 (c (n "sqlx-executor-alias-macro") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "sqlite"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "09dqm2bwrvr5w6ymsljr5xvb9pyipj3ikc0xs446zdd99rnc49hg")))

(define-public crate-sqlx-executor-alias-macro-0.2.0 (c (n "sqlx-executor-alias-macro") (v "0.2.0") (d (list (d (n "sqlx") (r "^0.7") (f (quote ("runtime-tokio-native-tls" "postgres" "sqlite"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0jzg1xyll7wfz8a4nrm5si45xfwiq7rqbpzmkxdg5rpldjnnqvmw")))

