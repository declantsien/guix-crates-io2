(define-module (crates-io sq lx sqlx-model-macros) #:use-module (crates-io))

(define-public crate-sqlx-model-macros-0.0.1-beta.1 (c (n "sqlx-model-macros") (v "0.0.1-beta.1") (d (list (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.5.1") (d #t) (k 0)) (d (n "sqlx") (r "~0.5.5") (f (quote ("mysql" "runtime-tokio-native-tls" "offline"))) (d #t) (k 0)) (d (n "sqlx-model-core") (r "^0.0.1-beta.1") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0gwrz934fb429a539l0f0af8jywzzvs9fcrdc5rsxn7yh1xrk3xg") (y #t)))

(define-public crate-sqlx-model-macros-0.0.1-beta.2 (c (n "sqlx-model-macros") (v "0.0.1-beta.2") (d (list (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "15y2982x6jj8jq42r91084d8xxhv9vjrplgqrkz9likkvdi982ky") (y #t)))

(define-public crate-sqlx-model-macros-0.0.1-beta.3 (c (n "sqlx-model-macros") (v "0.0.1-beta.3") (d (list (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0vaa2x6jlj69jbc3s6wmklm6gq70n9bmsjx3x0015v0x867nr6gd") (y #t)))

(define-public crate-sqlx-model-macros-0.0.1-beta.4 (c (n "sqlx-model-macros") (v "0.0.1-beta.4") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1hvhk7fqq4l76ws4c9wlh145pvsmslirj2qn0rpnzjizvc5pw9bp") (y #t)))

(define-public crate-sqlx-model-macros-0.0.1-beta.5 (c (n "sqlx-model-macros") (v "0.0.1-beta.5") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1fxmmlyam52vw29p8ci91p2z0l7pmcjj2r4f6imlzq8z6gdfm5ps") (y #t)))

(define-public crate-sqlx-model-macros-0.0.1-beta.6 (c (n "sqlx-model-macros") (v "0.0.1-beta.6") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "1z6nc9p730wm5fbkl66vwly0mvvzknvflsbnc33rhbagyxpbk6vp") (y #t)))

(define-public crate-sqlx-model-macros-0.0.2 (c (n "sqlx-model-macros") (v "0.0.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "14nygjhv5a5w469rbhyfp67s70pr1bcbdrzm4n3fz0sfrwvxbzz8") (y #t)))

(define-public crate-sqlx-model-macros-0.0.3 (c (n "sqlx-model-macros") (v "0.0.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0xps63zys3nah2m085jjrvvgvg9dziijzximjwkpifk7pkm58vd4") (y #t)))

(define-public crate-sqlx-model-macros-0.0.4 (c (n "sqlx-model-macros") (v "0.0.4") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.28") (d #t) (k 0)) (d (n "quote") (r "~1.0.9") (d #t) (k 0)) (d (n "syn") (r "~1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0ylzys02dw6dbap7zqjylfdppb05fdznlgfg9iimg2sr9wpfy85h")))

