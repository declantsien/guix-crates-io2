(define-module (crates-io sq lx sqlx-page) #:use-module (crates-io))

(define-public crate-sqlx-page-0.5.0 (c (n "sqlx-page") (v "0.5.0") (d (list (d (n "sqlx") (r "~0.5, <0.6") (k 0)))) (h "0sjhx4s3wvlkf9da8qf6wsicml91g0imwim4vw46yfrlyb7ca01c") (f (quote (("runtime-tokio-rustls" "sqlx/runtime-tokio-rustls") ("runtime-tokio-native-tls" "sqlx/runtime-tokio-native-tls") ("runtime-async-std-rustls" "sqlx/runtime-async-std-rustls") ("runtime-async-std-native-tls" "sqlx/runtime-async-std-native-tls") ("runtime-actix-rustls" "sqlx/runtime-actix-rustls") ("runtime-actix-native-tls" "sqlx/runtime-actix-native-tls") ("postgres" "sqlx/postgres") ("default" "runtime-tokio-native-tls" "postgres"))))))

