(define-module (crates-io sq lx sqlx-type) #:use-module (crates-io))

(define-public crate-sqlx-type-0.1.0 (c (n "sqlx-type") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.1") (d #t) (k 0)))) (h "1qdclp4siaxh2c71qvp2r4gpgxpz9rdcvydvvqc8yjsmig1cs3zj")))

(define-public crate-sqlx-type-0.1.1 (c (n "sqlx-type") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.1") (d #t) (k 0)))) (h "12xh1nparwkdf2rl8wc3gphw1m4xyv8drw7vcn6iv9db4hlcawxg")))

(define-public crate-sqlx-type-0.1.2 (c (n "sqlx-type") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.1.2") (d #t) (k 0)))) (h "04misk74nhqngi0qqyj9skiirjc4l4b04pwfzv6ak94b54cmpdwh")))

(define-public crate-sqlx-type-0.1.3 (c (n "sqlx-type") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.1.3") (d #t) (k 0)))) (h "0g4iyqga4advvwn05264pd4sd7xna7c9ny08lhkdq3k4z46d8xa3")))

(define-public crate-sqlx-type-0.1.4 (c (n "sqlx-type") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.1.3") (d #t) (k 0)))) (h "0a0nsvnng1vpjf7mmv833qs5dnrwffa70f09b855lwx2qpgj414d")))

(define-public crate-sqlx-type-0.1.5 (c (n "sqlx-type") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.1.5") (d #t) (k 0)))) (h "0h8y494jb723p7zc82pglsk1acc8ni8sdylsjcxajr200k07kyyr")))

(define-public crate-sqlx-type-0.2.0 (c (n "sqlx-type") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1qnak1bl6r3a5ygc0gp097nsbm2csm81fqrpv1skjv809dypfnpq")))

(define-public crate-sqlx-type-0.2.1 (c (n "sqlx-type") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.2.0") (d #t) (k 0)))) (h "107z7hwqy365cv1kn6sycg65qrc70lx46xll9qkdfw6lm5011xaz")))

(define-public crate-sqlx-type-0.2.2 (c (n "sqlx-type") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.2.2") (d #t) (k 0)))) (h "0s6853blcgabr0mc8546pfn34w124s14jk6rs6vk8jbh9rqx171b")))

(define-public crate-sqlx-type-0.2.3 (c (n "sqlx-type") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.2.3") (d #t) (k 0)))) (h "1d1n3yka4g2yv9i7nk42fvni0ihasi5m3q2p7ddp9676wrp6h14v")))

(define-public crate-sqlx-type-0.2.4 (c (n "sqlx-type") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.2.4") (d #t) (k 0)))) (h "1yzvrdink8ma4i08d9gdirgb1fkc1srw31qa0q1khg5an3dsb7dc")))

(define-public crate-sqlx-type-0.2.5 (c (n "sqlx-type") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.2.5") (d #t) (k 0)))) (h "0w2qw8awvhj3c5332lyjzg4iiydmnf03y8k8a6pd3sx2waxf2lxz")))

(define-public crate-sqlx-type-0.3.0 (c (n "sqlx-type") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.0") (d #t) (k 0)))) (h "03g3mnn1maml3hj1qchpd66l07fqm9call7c4lng3yhj9ss2zc9b")))

(define-public crate-sqlx-type-0.3.1 (c (n "sqlx-type") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1yaiyh9a5zv8qmiclvp3cfxm7gk364sp0ak2ggmliwhhiziyhh6a")))

(define-public crate-sqlx-type-0.3.2 (c (n "sqlx-type") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.2") (d #t) (k 0)))) (h "0q9jqzhl98w70273nywskjjif2ra93vw9h6xk69k21hl5c5lgff5")))

(define-public crate-sqlx-type-0.3.3 (c (n "sqlx-type") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.2") (d #t) (k 0)))) (h "0abz70cm139vgvx6332ggbx8kf14qgwprffpix36yfcjd6sqzc93")))

(define-public crate-sqlx-type-0.3.4 (c (n "sqlx-type") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.4") (d #t) (k 0)))) (h "10lwjw7jgibg3cwzwbha1vsrc8d1ai9ny715sqfl5sdh6723vil4")))

(define-public crate-sqlx-type-0.3.5 (c (n "sqlx-type") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.5") (d #t) (k 0)))) (h "14hhks0axb53bfhpd4hjwglc0w3yqb7gi87msb23qzxycc18rfd6")))

(define-public crate-sqlx-type-0.3.6 (c (n "sqlx-type") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.5") (d #t) (k 0)))) (h "0z5j2l3is33k0xzmd335fgsqviz9fkb69dpwjdzbqrhmrh34gqz7")))

(define-public crate-sqlx-type-0.3.7 (c (n "sqlx-type") (v "0.3.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.7") (d #t) (k 0)))) (h "1qj6f722c2pj8znznzv4aw8305sbvrhk8mypz7gpgnjgv4g8piy3")))

(define-public crate-sqlx-type-0.3.8 (c (n "sqlx-type") (v "0.3.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.8") (d #t) (k 0)))) (h "0gxw2szdvab4mshp2ncxgqa3x621fiamjsl4alvhqxvv8hmcp9wp")))

(define-public crate-sqlx-type-0.3.9 (c (n "sqlx-type") (v "0.3.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.9") (d #t) (k 0)))) (h "0ink3xzjzllxz3zr5r0wccgqkg819pmdj93j44blfjsgsvh6cdla")))

(define-public crate-sqlx-type-0.3.10 (c (n "sqlx-type") (v "0.3.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.10") (d #t) (k 0)))) (h "17gy1afz3j3f9lcyaap3fjkffwzpgzddr09ryxilz0licyd5zils")))

(define-public crate-sqlx-type-0.3.11 (c (n "sqlx-type") (v "0.3.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.10") (d #t) (k 0)))) (h "1frgwfl1jlqwhvcklqdv8qc22582fqqcm8gdczjr7q5rpz8as2r2")))

(define-public crate-sqlx-type-0.3.12 (c (n "sqlx-type") (v "0.3.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.10") (d #t) (k 0)))) (h "1ydc30x9lfx4zdgzj9bgkn7r2vgb6y21pvpkrxdyizdxh60pcliz")))

(define-public crate-sqlx-type-0.3.13 (c (n "sqlx-type") (v "0.3.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("mysql" "chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.13") (d #t) (k 0)))) (h "12pjyrxmh0pksjbssq97cwdh2a9xp1iwi5rxf9h883xmalc4kq2q")))

(define-public crate-sqlx-type-0.3.14 (c (n "sqlx-type") (v "0.3.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("chrono" "runtime-tokio-native-tls"))) (k 0)) (d (n "sqlx-type-macro") (r "^0.3.14") (d #t) (k 0)))) (h "1m4by8rv0linipx7h6fkc0mcajp37akaypms47j30zdsqb165n2c")))

(define-public crate-sqlx-type-0.3.15 (c (n "sqlx-type") (v "0.3.15") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("chrono" "runtime-tokio-native-tls" "mysql"))) (k 2)) (d (n "sqlx-type-macro") (r "^0.3.15") (d #t) (k 0)))) (h "1gf8nyrdkzw67hifr8sc4rqbwb2vcx9klcqxm7k7qlwxpihni1dk")))

