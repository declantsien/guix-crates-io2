(define-module (crates-io sq lx sqlx_insert_derive) #:use-module (crates-io))

(define-public crate-sqlx_insert_derive-0.1.0 (c (n "sqlx_insert_derive") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g3pmbji6akyg4j5d2dcgir0jsww57bjbsfz2ks2x3sxliiijgd4")))

(define-public crate-sqlx_insert_derive-0.1.1 (c (n "sqlx_insert_derive") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qb49yn7qxsw8y8n4ah1q13i5sjyzdn19s5kzsh43zm1mxab2l9v")))

