(define-module (crates-io sq lx sqlx-derive-with) #:use-module (crates-io))

(define-public crate-sqlx-derive-with-0.1.0 (c (n "sqlx-derive-with") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("macros" "runtime-tokio-rustls" "sqlite"))) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "19wnjv3k56wnnljqixcmxwx0jd1y5xf7s4c1h13208cszipr45m3")))

