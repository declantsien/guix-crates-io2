(define-module (crates-io sq lx sqlx-sqlhelper-proc-macros) #:use-module (crates-io))

(define-public crate-sqlx-sqlhelper-proc-macros-0.1.0 (c (n "sqlx-sqlhelper-proc-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gnchydqvv04m7yk2z3lrcsmz2bjiknjnqdnai6yycvxq09a56dm")))

