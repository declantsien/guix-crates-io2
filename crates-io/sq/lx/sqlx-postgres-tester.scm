(define-module (crates-io sq lx sqlx-postgres-tester) #:use-module (crates-io))

(define-public crate-sqlx-postgres-tester-0.1.0 (c (n "sqlx-postgres-tester") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cgb77wbv7afjgi4n2l63vkagyy2l6vgj139s51qbndjnhhskj3h")))

(define-public crate-sqlx-postgres-tester-0.1.1 (c (n "sqlx-postgres-tester") (v "0.1.1") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1d80n32h1xr5k8l36zimm62d6mf99iiysbg1n1j08z7a891jbskf")))

