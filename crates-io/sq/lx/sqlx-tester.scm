(define-module (crates-io sq lx sqlx-tester) #:use-module (crates-io))

(define-public crate-sqlx-tester-0.1.0 (c (n "sqlx-tester") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yzmvjrx7bnjf3mlqii8vy4ljvkvfbwavdwwr3h1fs1wxcxyi8m9")))

