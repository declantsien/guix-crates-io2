(define-module (crates-io sq lx sqlx-db-ssk-tester) #:use-module (crates-io))

(define-public crate-sqlx-db-ssk-tester-0.1.0 (c (n "sqlx-db-ssk-tester") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0fmk7l5v3lblfmdiykswdpzph5b3am7486yszydk6d4hhf61n5lj")))

(define-public crate-sqlx-db-ssk-tester-0.1.1 (c (n "sqlx-db-ssk-tester") (v "0.1.1") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0iqi8vbgi62zc57ja7pz2zrp0h2rxpjn0h7jf4r7zn20i67jh25k")))

