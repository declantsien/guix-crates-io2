(define-module (crates-io sq lx sqlx-meta) #:use-module (crates-io))

(define-public crate-sqlx-meta-0.1.0 (c (n "sqlx-meta") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6") (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls" "sqlite"))) (d #t) (k 2)) (d (n "sqlx-meta-macros") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio-test") (r "^0") (d #t) (k 2)))) (h "1iqvgm2cprp2ncq86rjm5ppzflcgcb5vazdpniqlaccldjn4hbw0")))

