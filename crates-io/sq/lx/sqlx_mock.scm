(define-module (crates-io sq lx sqlx_mock) #:use-module (crates-io))

(define-public crate-sqlx_mock-0.1.0 (c (n "sqlx_mock") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "16nvr5lv5sizsibfwqybbq6f4g2kk68rcx4crk6fqcn3ash2hb51")))

(define-public crate-sqlx_mock-0.1.1 (c (n "sqlx_mock") (v "0.1.1") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1anjq9pk80y3lmi96jgqn16jgrpawzs2ihxrrfqckk4va46fvkvk")))

(define-public crate-sqlx_mock-0.1.2 (c (n "sqlx_mock") (v "0.1.2") (d (list (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0id303jsyjzzjjqxvdc56h6f5gvxb1a836apbdzy1az4gank5fx9")))

