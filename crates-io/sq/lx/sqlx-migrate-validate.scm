(define-module (crates-io sq lx sqlx-migrate-validate) #:use-module (crates-io))

(define-public crate-sqlx-migrate-validate-0.1.0 (c (n "sqlx-migrate-validate") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (k 0)) (d (n "sqlx-core") (r "^0.6") (d #t) (k 0)) (d (n "sqlx-rt") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "03zf9pni7g2fjx6gh683w6v7wp4zp83gh8g8084hx6pj2za0pqai") (f (quote (("sqlite" "sqlx/sqlite") ("postgres" "sqlx/postgres") ("default" "postgres" "sqlite" "sqlx/runtime-tokio-rustls" "sqlx/macros" "sqlx/migrate"))))))

