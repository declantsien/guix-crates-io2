(define-module (crates-io sq lx sqlx_query) #:use-module (crates-io))

(define-public crate-sqlx_query-0.1.0 (c (n "sqlx_query") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "07b9nkx8p4v8kjcimks0ih6yhzzrb6wgh0qb8qhd3w4pj93xv34a") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-sqlx_query-0.2.0 (c (n "sqlx_query") (v "0.2.0") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "0ari6dk56im5ss9w4q3k2anl4i414xwvx3hscl662c8djg8g0jd6") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-sqlx_query-0.2.1 (c (n "sqlx_query") (v "0.2.1") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "1kipy1s8wfja7dzcx10fkwdjs0acj80y7rly0jbn2f9x4f8qmzcx") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

