(define-module (crates-io sq lx sqlx-mock-db-tester) #:use-module (crates-io))

(define-public crate-sqlx-mock-db-tester-0.1.0 (c (n "sqlx-mock-db-tester") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1y6l7crl4qsa80yaxhxyq8czsznqh9mf341aajbawjmky1chd32p")))

(define-public crate-sqlx-mock-db-tester-0.2.0 (c (n "sqlx-mock-db-tester") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("postgres" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0d86nass84yvlnv5s5l2h0wmbscsziazs8mymbgwx9pf125bdphk")))

