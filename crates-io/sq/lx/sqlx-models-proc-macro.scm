(define-module (crates-io sq lx sqlx-models-proc-macro) #:use-module (crates-io))

(define-public crate-sqlx-models-proc-macro-0.0.1 (c (n "sqlx-models-proc-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "sqlx-models-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0xx0ds8zmrr4i73813nrrw6kp3qlriy09x75b35f4072s8xvzzz6")))

(define-public crate-sqlx-models-proc-macro-0.0.2 (c (n "sqlx-models-proc-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "sqlx-models-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "060n6jql3k5g8fj2hgkil473czgfdjklhapbvd3svb8dqlfcdyiy")))

(define-public crate-sqlx-models-proc-macro-0.0.3 (c (n "sqlx-models-proc-macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "sqlx-models-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "1m93blkj0m8vz86x3dw3hxxw5xyc42jrl3pa9c7wd6whdi16a3vb") (f (quote (("helpers"))))))

