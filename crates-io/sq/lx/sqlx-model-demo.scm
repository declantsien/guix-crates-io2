(define-module (crates-io sq lx sqlx-model-demo) #:use-module (crates-io))

(define-public crate-sqlx-model-demo-0.0.1-beta.1 (c (n "sqlx-model-demo") (v "0.0.1-beta.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "sqlx") (r "~0.5") (f (quote ("mysql" "offline" "runtime-async-std-native-tls"))) (d #t) (k 0)) (d (n "sqlx-model") (r "~0.0.1-beta.13") (f (quote ("sqlx-mysql"))) (k 0)))) (h "0fv1qnywmippxmflbkvn0mnw45ja0z2m4gqdfxsab13qzrgq4lgx") (y #t)))

