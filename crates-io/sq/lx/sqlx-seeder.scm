(define-module (crates-io sq lx sqlx-seeder) #:use-module (crates-io))

(define-public crate-sqlx-seeder-0.1.0 (c (n "sqlx-seeder") (v "0.1.0") (h "01xiiaad60lqij6sl2qpdh4c0mn4jqdf7xp1srmlf78znq271ds0")))

(define-public crate-sqlx-seeder-0.1.1 (c (n "sqlx-seeder") (v "0.1.1") (h "12vgwqil08m6y5q3vhxwsy3ahwvl543dh2jpawfgcdp9cmmiwa1k")))

(define-public crate-sqlx-seeder-0.1.2 (c (n "sqlx-seeder") (v "0.1.2") (h "14scxbcqpisf37ljnn9hfhmrwbpib31gnsyr17j7zkmrks1rjj3h")))

