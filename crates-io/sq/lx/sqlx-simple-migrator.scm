(define-module (crates-io sq lx sqlx-simple-migrator) #:use-module (crates-io))

(define-public crate-sqlx-simple-migrator-0.0.1 (c (n "sqlx-simple-migrator") (v "0.0.1") (d (list (d (n "sqlx") (r "^0.3") (f (quote ("runtime-tokio" "macros" "postgres" "uuid" "chrono" "tls"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k2hsswzhxjc7kkw2czw319wxajpr509mvms41r0nhad50bz4bjb")))

(define-public crate-sqlx-simple-migrator-0.0.2 (c (n "sqlx-simple-migrator") (v "0.0.2") (d (list (d (n "sqlx") (r "^0.4") (f (quote ("macros" "postgres" "runtime-tokio-rustls" "uuid" "chrono"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19gjwvy3d4gh0f0yrybxkd6fhp75v9mpywx9yb05wpsrqd3x33x1")))

(define-public crate-sqlx-simple-migrator-0.0.3 (c (n "sqlx-simple-migrator") (v "0.0.3") (d (list (d (n "sqlx") (r "^0.4") (f (quote ("macros" "postgres" "runtime-tokio-rustls" "uuid" "chrono"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ibz9jcxf9rah92bac5fja4qkpwcvs59pp1yzy3z193dixlrmdhg")))

(define-public crate-sqlx-simple-migrator-0.0.4 (c (n "sqlx-simple-migrator") (v "0.0.4") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("macros" "postgres" "runtime-tokio-rustls" "uuid" "chrono"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hrb0mq08vkssa0p8mmmps1yhn5laxmcryy2w3k3x3wyjahyhija")))

(define-public crate-sqlx-simple-migrator-0.0.5 (c (n "sqlx-simple-migrator") (v "0.0.5") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("macros" "postgres" "runtime-tokio-rustls" "uuid" "chrono"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jv8h7b7aqa55sb97540bc9xhvb44m17g7xgjkpck4cky8yyskib")))

