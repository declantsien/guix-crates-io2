(define-module (crates-io sq lx sqlxmq_macros) #:use-module (crates-io))

(define-public crate-sqlxmq_macros-0.1.0 (c (n "sqlxmq_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "012sdmrbrbdp96f0x7q84yp9953m8c5wjib7dv8l3972ymgixqi4")))

(define-public crate-sqlxmq_macros-0.1.1 (c (n "sqlxmq_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "07wr8p22py7njaf5cx2kn50xfdqg25vdrxnn67f8b6ncqyshhvbf")))

(define-public crate-sqlxmq_macros-0.1.2 (c (n "sqlxmq_macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1g2wwc9h9m9j21r8yx3yxnhcgs67mvb23rwhll6bm6z8469nwn8k")))

(define-public crate-sqlxmq_macros-0.2.0 (c (n "sqlxmq_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "0nfhw4l374gds0g2ndqqy17v0n2ypsm7b83xmbbv5sk2nvf35c29")))

(define-public crate-sqlxmq_macros-0.2.1 (c (n "sqlxmq_macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1bk0301bddvfy37wsqlsv6hpgb6xiavxi5zyqa9nxw8d4dp3khhj")))

(define-public crate-sqlxmq_macros-0.2.2 (c (n "sqlxmq_macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1fi3k8819gyps0fvdpli75fbh3arczr9d85nklsdvf0hqqpc47yz")))

(define-public crate-sqlxmq_macros-0.3.0 (c (n "sqlxmq_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "1ynfwvivcapmi9qgr7sd4hk9x3j0qdlqhlayqkx7a7i5ym4h7x0m")))

(define-public crate-sqlxmq_macros-0.3.1 (c (n "sqlxmq_macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full"))) (d #t) (k 0)))) (h "15b58qziqh7dfcbig82l2hv6fn9zpvr779mwp4ij4j8cwjm4dzka")))

(define-public crate-sqlxmq_macros-0.3.3 (c (n "sqlxmq_macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "1ka00310i8axj6viigknsynbvq4w8z0ciyfxaqq8mgn27dzq2cg7")))

(define-public crate-sqlxmq_macros-0.3.4 (c (n "sqlxmq_macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "06xivkd9v1rf3grsni0w6q6c6f140rfmiyqkpgnj7zlyiyv6wjd0")))

(define-public crate-sqlxmq_macros-0.3.5 (c (n "sqlxmq_macros") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0as8mapay416jj280cqhbxrrgn5kaf6fm30zm6n2ywdz5q3dr6ar")))

(define-public crate-sqlxmq_macros-0.4.0 (c (n "sqlxmq_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "13mpx944dqz86k1ms3kjfvhcjnfzafgvw8alcaf789l0kk1w25a8")))

(define-public crate-sqlxmq_macros-0.4.1 (c (n "sqlxmq_macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0y2sp8y30sivkyhch0jjr7f8rclp1bqyn9ark6b4sdvn3rhwcp32")))

(define-public crate-sqlxmq_macros-0.5.0 (c (n "sqlxmq_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0kwzig82jq85lgypmvbia5hc739hvg6p2jib9vhih3qjvdz6c05x")))

