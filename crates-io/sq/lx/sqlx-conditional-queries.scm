(define-module (crates-io sq lx sqlx-conditional-queries) #:use-module (crates-io))

(define-public crate-sqlx-conditional-queries-0.1.1 (c (n "sqlx-conditional-queries") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "sqlx-conditional-queries-macros") (r "^0.1.0") (d #t) (k 0)))) (h "03jzm8c7y72zk5qldmhyvrmidymrks80ak8bww6h017nxcssmns5") (f (quote (("sqlite" "sqlx-conditional-queries-macros/sqlite") ("postgres" "sqlx-conditional-queries-macros/postgres") ("mysql" "sqlx-conditional-queries-macros/mysql"))))))

(define-public crate-sqlx-conditional-queries-0.1.2 (c (n "sqlx-conditional-queries") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "sqlx-conditional-queries-macros") (r "^0.1.2") (d #t) (k 0)))) (h "000rjjir21jv73rja5b5i3p4anz6zfpqxsy7h58wr1xq9gg2jb66") (f (quote (("sqlite" "sqlx-conditional-queries-macros/sqlite") ("postgres" "sqlx-conditional-queries-macros/postgres") ("mysql" "sqlx-conditional-queries-macros/mysql"))))))

(define-public crate-sqlx-conditional-queries-0.1.3 (c (n "sqlx-conditional-queries") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "sqlx-conditional-queries-macros") (r "^0.1") (d #t) (k 0)))) (h "02fqr2a2vy51vbm7x49bn0pnnrw2q0ncf9hkkyg7rqfcqwhdrlwh") (f (quote (("sqlite" "sqlx-conditional-queries-macros/sqlite") ("postgres" "sqlx-conditional-queries-macros/postgres") ("mysql" "sqlx-conditional-queries-macros/mysql"))))))

(define-public crate-sqlx-conditional-queries-0.1.4 (c (n "sqlx-conditional-queries") (v "0.1.4") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "sqlx-conditional-queries-macros") (r "^0.1") (d #t) (k 0)))) (h "0j723q3j0i1iqjhlw8ayxcb4b6g6028g60qb1rnnk6h9dlxavzq0") (f (quote (("sqlite" "sqlx-conditional-queries-macros/sqlite") ("postgres" "sqlx-conditional-queries-macros/postgres") ("mysql" "sqlx-conditional-queries-macros/mysql"))))))

