(define-module (crates-io sq lx sqlx-plus-macros) #:use-module (crates-io))

(define-public crate-sqlx-plus-macros-0.1.0 (c (n "sqlx-plus-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gs6hqixgzac83gqv8r6haqxfr1rilcg23nilj0g2dp0x8xn2pvz")))

(define-public crate-sqlx-plus-macros-0.1.1 (c (n "sqlx-plus-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rsb7i3215m07i06b9gp12p9rlc3yhs7pnxqr6rkhi1frv8fjbyx")))

