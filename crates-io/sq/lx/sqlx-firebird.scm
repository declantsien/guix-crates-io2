(define-module (crates-io sq lx sqlx-firebird) #:use-module (crates-io))

(define-public crate-sqlx-firebird-0.1.0-beta.1 (c (n "sqlx-firebird") (v "0.1.0-beta.1") (d (list (d (n "flume") (r "^0.10.14") (f (quote ("async"))) (k 0)) (d (n "futures-channel") (r "^0.3.28") (k 0)) (d (n "futures-core") (r "^0.3.28") (k 0)) (d (n "futures-intrusive") (r "^0.5.0") (k 0)) (d (n "log") (r "^0.4.19") (k 0)) (d (n "rsfbclient") (r "^0.23.0") (f (quote ("linking"))) (k 0)) (d (n "rsfbclient-native") (r "^0.23.0") (k 0)) (d (n "sqlx-core") (r "^0.7.0-alpha.3") (f (quote ("_tls-none"))) (k 0)))) (h "0a6zlk9vx4r9p9jfaspnn58w9g2h61a63yc3n9h4pdsyj69nxvl1")))

