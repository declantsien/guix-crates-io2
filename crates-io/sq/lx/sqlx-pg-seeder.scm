(define-module (crates-io sq lx sqlx-pg-seeder) #:use-module (crates-io))

(define-public crate-sqlx-pg-seeder-0.1.0 (c (n "sqlx-pg-seeder") (v "0.1.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio-rustls" "json" "postgres" "macros" "uuid" "chrono"))) (d #t) (k 0)))) (h "0lp0532pmvki7a27v34xgcwsdkyc1pj36avl8bg7c7wk50wz9dww") (r "1.74")))

(define-public crate-sqlx-pg-seeder-0.1.1 (c (n "sqlx-pg-seeder") (v "0.1.1") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio-rustls" "json" "postgres" "macros" "uuid" "chrono"))) (d #t) (k 0)))) (h "06jq7qnxx2by7il7jmv0jx79bw1s486dm40w7xyfhkz3nbjzm9zb") (r "1.74")))

(define-public crate-sqlx-pg-seeder-0.1.3 (c (n "sqlx-pg-seeder") (v "0.1.3") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio-rustls" "json" "postgres" "macros" "uuid" "chrono"))) (d #t) (k 0)))) (h "1cbs3w8jd11z8v53bxcvyd8ayzv1c83i3p3sgwqmqjkk950yzhqx") (r "1.74")))

(define-public crate-sqlx-pg-seeder-0.1.4 (c (n "sqlx-pg-seeder") (v "0.1.4") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("runtime-tokio-rustls" "json" "postgres" "macros" "uuid" "chrono"))) (d #t) (k 0)))) (h "17dan3kqyc4i343jq3rw48npk1gbwhl3mlzxa3ffyzdl63vjl7j3") (r "1.74")))

