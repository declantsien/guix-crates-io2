(define-module (crates-io sq lx sqlx-meta-macros) #:use-module (crates-io))

(define-public crate-sqlx-meta-macros-0.3.1 (c (n "sqlx-meta-macros") (v "0.3.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09hb2gbcgikgn3wna1sw69ndmf98ixcsc34hcdi6v6mvvgmjs9b3")))

