(define-module (crates-io sq l_ sql_reveser_error) #:use-module (crates-io))

(define-public crate-sql_reveser_error-0.1.0 (c (n "sql_reveser_error") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "mysql") (r "^22.1.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)))) (h "1jxhaaxs4k1kix4cjb37b8wx5c8gazs5h0b83aw16509hc65y8wb") (y #t)))

(define-public crate-sql_reveser_error-0.1.1 (c (n "sql_reveser_error") (v "0.1.1") (d (list (d (n "mysql") (r "^22.1.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "0cfn176y6dyx9r9xk9494c76yj7gbhjmq26630x8cxnbvbrpb5qb") (y #t)))

(define-public crate-sql_reveser_error-0.1.2 (c (n "sql_reveser_error") (v "0.1.2") (d (list (d (n "mysql") (r "^22.1.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "1izqkiap8pbangr21s0sr027s1p8g6sp3mz9rnij1q8394kxn5h1") (y #t)))

