(define-module (crates-io sq l_ sql_minifier) #:use-module (crates-io))

(define-public crate-sql_minifier-0.1.0 (c (n "sql_minifier") (v "0.1.0") (h "1gbcy0is604gig88zmgwhg0mdvl9a6y3plhi7ijajc9lrjyj6z6r")))

(define-public crate-sql_minifier-0.1.1 (c (n "sql_minifier") (v "0.1.1") (h "0fl4xrliym0lsafypp0sn8w57lwkdrj8y46jiccmwjwk0snzqhnb")))

(define-public crate-sql_minifier-0.1.2 (c (n "sql_minifier") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1wgllgnncbrmfqn4wy1zmic1ivwkp1h7i6g5wq56zagphfvwphif")))

(define-public crate-sql_minifier-0.1.3 (c (n "sql_minifier") (v "0.1.3") (d (list (d (n "load_sql_proc") (r "^0.1.3") (d #t) (k 0)) (d (n "minify_sql") (r "^0.1.3") (d #t) (k 0)) (d (n "minify_sql_proc") (r "^0.1.3") (d #t) (k 0)))) (h "1l32ngia28g044jk68cpl70zh55g5p452spl660kbx26sgqn4c68")))

(define-public crate-sql_minifier-0.1.4 (c (n "sql_minifier") (v "0.1.4") (d (list (d (n "load_sql_proc") (r "^0.1.4") (d #t) (k 0)) (d (n "minify_sql") (r "^0.1.4") (d #t) (k 0)) (d (n "minify_sql_proc") (r "^0.1.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0acqy5xjwfg3vj7j0sxsvpjb3ygq9mcdzxhg7cxviw0cdd0rkvm1")))

