(define-module (crates-io sq l_ sql_reverse_error) #:use-module (crates-io))

(define-public crate-sql_reverse_error-0.1.0 (c (n "sql_reverse_error") (v "0.1.0") (d (list (d (n "mysql") (r "^22.1.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "1wdgk0wd8vxan5vn1jk87chkd028f59lz564rfkay5vykylb7q1s")))

(define-public crate-sql_reverse_error-0.1.1 (c (n "sql_reverse_error") (v "0.1.1") (d (list (d (n "mysql") (r "^22.1.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "1i7gqyyc3p95rq764yycarpmzjrxpvarinhvfzmja5cxs0qkgvm9")))

(define-public crate-sql_reverse_error-0.1.2 (c (n "sql_reverse_error") (v "0.1.2") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "1dl4dwqgf5sawzyh9fxjyyi4xmlnm8kpppm72a28bpm6d7ad5hd4")))

