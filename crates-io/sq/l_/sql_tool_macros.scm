(define-module (crates-io sq l_ sql_tool_macros) #:use-module (crates-io))

(define-public crate-sql_tool_macros-0.1.0 (c (n "sql_tool_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0mwdi3gagd0f4p9i7k3kkm6x32fq2qgsqny9i0k1nc66xrkwkn3f")))

(define-public crate-sql_tool_macros-0.1.1 (c (n "sql_tool_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0ih9jzq3r2m6slswz5nwmd9jyb4x2m4gapw8fl7pgvznslm0iyn0")))

(define-public crate-sql_tool_macros-0.1.2 (c (n "sql_tool_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "09dqx1qn4l75s154334lm1ivx10m9yfyhcmxhqzwm5r892rsr2ks")))

(define-public crate-sql_tool_macros-0.1.3 (c (n "sql_tool_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0zy6i9fjizyqblpip2vnk685c5wr4xc1iziq48jz7shkriyb12y1")))

