(define-module (crates-io sq l_ sql_db_creator) #:use-module (crates-io))

(define-public crate-sql_db_creator-0.1.0 (c (n "sql_db_creator") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "13zw2cnim1r7xv23h1vq0aw51ifh9mwy6y54y74fr5jszjp93y3i")))

(define-public crate-sql_db_creator-0.1.1 (c (n "sql_db_creator") (v "0.1.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "1d6h9jql3lcaicyb00hwjzhz8d29yp4241h27a08sf7nc9ya7786")))

(define-public crate-sql_db_creator-0.1.2 (c (n "sql_db_creator") (v "0.1.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "179wvi5hl8qbxgnfwhamxm05rwrws8i91kcdjymbc3byn6683khg")))

(define-public crate-sql_db_creator-0.1.3 (c (n "sql_db_creator") (v "0.1.3") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "0dqavxsl3a7d77fihygsi11vavmvywlfiz1dil52scz5jpz7cc8q")))

(define-public crate-sql_db_creator-0.1.4 (c (n "sql_db_creator") (v "0.1.4") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "1q1901087r0rxdp4dlsppdbqzc0zamv8y88niq61qf1jhcf7qwk1")))

(define-public crate-sql_db_creator-0.1.5 (c (n "sql_db_creator") (v "0.1.5") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "0pwbn3qrkj2waxjr6nrs9l7pq5kw01d45f6fn7h6162w21larwgv")))

(define-public crate-sql_db_creator-0.1.6 (c (n "sql_db_creator") (v "0.1.6") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "0bvzkxadrh716cjm1khs1jd2w18w18ydghkxqqc4y9yh70w03bvl")))

(define-public crate-sql_db_creator-0.1.7 (c (n "sql_db_creator") (v "0.1.7") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "082n25swq552pnhkawm2c1nsqz51lhgys17135if1b0vbydn665z")))

(define-public crate-sql_db_creator-0.1.8 (c (n "sql_db_creator") (v "0.1.8") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "18llpbshv8i8zz8m5gqh9zkrwx204hb2s39w170kjkwf4a55r44j")))

(define-public crate-sql_db_creator-0.1.9 (c (n "sql_db_creator") (v "0.1.9") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "1c717gcjm58psmwizakp23zv1nbva65v13j2hbzxgbcrn9ripizk")))

(define-public crate-sql_db_creator-0.2.0 (c (n "sql_db_creator") (v "0.2.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "15fd5bp443v8v6jf1y5fi2j2aq3ahin5kvr4qd2cvarb0ga61fb2")))

(define-public crate-sql_db_creator-0.2.1 (c (n "sql_db_creator") (v "0.2.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.5.9") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "01z7rhcjf2imvyglgy09yfhiaz1h33a6pk9kbgjqkv5g9jjmlxgj")))

(define-public crate-sql_db_creator-0.2.2 (c (n "sql_db_creator") (v "0.2.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "1mvrd4j6fzi12qvpsmhbhkd66kikzk0ajmf5ylyln6smjdqrhcpz")))

(define-public crate-sql_db_creator-0.2.3 (c (n "sql_db_creator") (v "0.2.3") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "19zacfppag5x87836sabcgaxljmzwr4w76gz2x2krd9cwdqp13c4")))

(define-public crate-sql_db_creator-0.2.4 (c (n "sql_db_creator") (v "0.2.4") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "1nrkvmnbr8f3bf6fjs6jy6zi5f7pfi2dndhli27dpq8vgka5fb0c")))

(define-public crate-sql_db_creator-0.2.5 (c (n "sql_db_creator") (v "0.2.5") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("postgres" "mysql" "migrate" "macros" "chrono" "runtime-async-std-rustls"))) (d #t) (k 0)))) (h "1qcv15fbxj2i4bmxrr1nrnmkbfwxryqmi6gvsisglsdlp3hn74lq")))

