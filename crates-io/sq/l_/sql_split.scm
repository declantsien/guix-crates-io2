(define-module (crates-io sq l_ sql_split) #:use-module (crates-io))

(define-public crate-sql_split-0.1.0 (c (n "sql_split") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.27.0") (d #t) (k 2)))) (h "1hx0xdjc12xd0lcbs777li75bp0layrdcng2xbilsav9k8d3gxv6")))

(define-public crate-sql_split-0.1.1 (c (n "sql_split") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.27.0") (d #t) (k 2)))) (h "15bmr1bn4qhqh3hv4dfmx8gyzall90dxq4kpz91fm21a7fzxwma4")))

