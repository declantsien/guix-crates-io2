(define-module (crates-io sq l_ sql_csv) #:use-module (crates-io))

(define-public crate-sql_csv-0.1.0 (c (n "sql_csv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "datafusion") (r "^6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0snf98mn7l877dfvbv6zghfz350ilbha1g0wzkhkdv9hx4aavglq")))

(define-public crate-sql_csv-0.2.0 (c (n "sql_csv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "datafusion") (r "^6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gka9mjzkix0f0qqa1ffmkyd0qapd5557glkxygx8l5r8y8n2s6k")))

