(define-module (crates-io sq l_ sql_db_mapper) #:use-module (crates-io))

(define-public crate-sql_db_mapper-0.0.1 (c (n "sql_db_mapper") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-chrono"))) (d #t) (k 0)) (d (n "rust_decimal") (r "~1.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1m0ayvfc9xi4kimwcdcps7gm397qvfnyd0nnrdkipkx6x9vwd7zf")))

(define-public crate-sql_db_mapper-0.0.2 (c (n "sql_db_mapper") (v "0.0.2") (d (list (d (n "postgres") (r "^0.17") (f (quote ("with-chrono-0_4"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jb244q7zd321zf3lvwb1c2h8rqhqdj877scb441frs7j1bk2n9v")))

(define-public crate-sql_db_mapper-0.0.3 (c (n "sql_db_mapper") (v "0.0.3") (d (list (d (n "postgres") (r "^0.17") (f (quote ("with-chrono-0_4"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15kvchiflaml61d1jgdqnl5f6yr1xb49gzwnl1xm3pj1ip73zr15")))

(define-public crate-sql_db_mapper-0.1.0 (c (n "sql_db_mapper") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "postgres") (r "^0.19.0") (f (quote ("with-chrono-0_4"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sql_db_mapper_core") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01mfxlr1m8qznjggij8j2zbflgz1jpj83h69fx646361g4x116gy")))

