(define-module (crates-io sq l_ sql_paginatorr) #:use-module (crates-io))

(define-public crate-sql_paginatorr-0.1.0 (c (n "sql_paginatorr") (v "0.1.0") (h "0amhn9dgv8dk9ihkms8kq4x6w4h21jibgy171kwdsyia121pzv9h")))

(define-public crate-sql_paginatorr-0.1.1 (c (n "sql_paginatorr") (v "0.1.1") (h "15n46sfmx0n7x92y583sd0gyhyxsnn6gfld38sg2c7ip59flcybf")))

(define-public crate-sql_paginatorr-0.1.2 (c (n "sql_paginatorr") (v "0.1.2") (h "10i0dfy90qavs5qphz8hd9qv6cgzx49mchkr2nmpx7n2n2cmv9fm")))

