(define-module (crates-io sq l_ sql_from_models-proc-macro) #:use-module (crates-io))

(define-public crate-sql_from_models-proc-macro-0.1.1 (c (n "sql_from_models-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "sql_from_models-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "1hd5zvx6h0arx29sgjnfjf0kp3ydsc9rbs1zbfc4625ar55wj9jg") (f (quote (("helpers"))))))

(define-public crate-sql_from_models-proc-macro-0.1.2 (c (n "sql_from_models-proc-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "sql_from_models-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0gsdd11l30p0yjxpz2iw25w7y11hmgzcpscnlbihm6sfjxx4knyv") (f (quote (("helpers"))))))

