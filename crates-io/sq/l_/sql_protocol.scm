(define-module (crates-io sq l_ sql_protocol) #:use-module (crates-io))

(define-public crate-sql_protocol-0.1.0 (c (n "sql_protocol") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "dakv_logger") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "0lvrvn94h3bp1xidhdi4n8y4j0ap77ifqsq8ixxx95qjvss2nnvd") (y #t)))

