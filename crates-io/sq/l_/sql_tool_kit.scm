(define-module (crates-io sq l_ sql_tool_kit) #:use-module (crates-io))

(define-public crate-sql_tool_kit-0.1.0 (c (n "sql_tool_kit") (v "0.1.0") (d (list (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "sql_tool_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1hgh3czrdfgj7lqp68rjdmznmphnwshscyhrsg6b4mlzj6sd7zw2")))

(define-public crate-sql_tool_kit-0.1.1 (c (n "sql_tool_kit") (v "0.1.1") (d (list (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "sql_tool_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1b2fcr93ds13wafhpc553nhdkc04aa2n2kjgys0psv6f9j877jym")))

(define-public crate-sql_tool_kit-0.1.2 (c (n "sql_tool_kit") (v "0.1.2") (d (list (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "sql_tool_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0lfjhayaxr4vsh322cqhdhlm1b6gwys9p28d1662x8s766vnqnv5")))

(define-public crate-sql_tool_kit-0.1.3 (c (n "sql_tool_kit") (v "0.1.3") (d (list (d (n "sql_tool_core") (r "^0.1.0") (d #t) (k 0)) (d (n "sql_tool_macros") (r "^0.1.3") (d #t) (k 0)))) (h "05nlxfd02iibmmkfig5hz8iclis4zwd9y2bd269jg49p0vywypj5")))

