(define-module (crates-io sq l_ sql_query_builder) #:use-module (crates-io))

(define-public crate-sql_query_builder-0.1.0 (c (n "sql_query_builder") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "153f0rn4anf9z3qgfg8aq9hvp6h9v3r6i5m3ancgi7q5nxbjldcp")))

(define-public crate-sql_query_builder-0.1.1 (c (n "sql_query_builder") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0glghcrawl6fflk93qc1myca00g6852izwz2lgaz0ri9ayx7y696")))

(define-public crate-sql_query_builder-0.1.2 (c (n "sql_query_builder") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0dnym17ffbbjnhcs7zba12mdlmlvs1xfffw0y9gfyycp1b1gcbks")))

(define-public crate-sql_query_builder-0.1.3 (c (n "sql_query_builder") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "16wzhlqf189wfg7vycsd56r4irxkprg5ll7r663dicpsx70arz6f")))

(define-public crate-sql_query_builder-0.2.4 (c (n "sql_query_builder") (v "0.2.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1hcvwf0lif5cfr9d65vxai880nk7l408gqwzhlvl1czpc8534hnx")))

(define-public crate-sql_query_builder-0.3.4 (c (n "sql_query_builder") (v "0.3.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0q7bw7x7f81yfyhrm8ghlcw27475i7p30nvf7j1z6nf68i924df0")))

(define-public crate-sql_query_builder-0.4.4 (c (n "sql_query_builder") (v "0.4.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0n5d2rv2k579x5vnv734hay41978w35454w37is46fvhraywv1p5")))

(define-public crate-sql_query_builder-0.5.4 (c (n "sql_query_builder") (v "0.5.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1dqgvmx2rjyq4lvcs3lskz2k0k1f7bdmhg8b0y2k1cazqxmhn3d4")))

(define-public crate-sql_query_builder-0.6.4 (c (n "sql_query_builder") (v "0.6.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1pazmv8016j3zsgs0nf40rfxpzf3ax6ilz3bxkpq4jsvf0z7r5k9")))

(define-public crate-sql_query_builder-0.7.4 (c (n "sql_query_builder") (v "0.7.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1v43xb67c53rxhzxi3g63mmgapp08x62iwnjxpdpx6mg354vm976") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-0.8.4-rc.1 (c (n "sql_query_builder") (v "0.8.4-rc.1") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0hq5j3acawcgm3i55pbw08h9a94sqhy6pgsf55c08ljawfj4arja") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-0.8.4 (c (n "sql_query_builder") (v "0.8.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "10q99bxqbfjbjacqi25yz4095x3j9jik246b438z3prnqbkh8j1p") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-0.9.4 (c (n "sql_query_builder") (v "0.9.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "08ryrk79n8ddpv4v305cvi9nx7f22i7qjl262kgp54rkk8glzfar") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-0.9.5 (c (n "sql_query_builder") (v "0.9.5") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0dr25z51jx39zkbm2wcsh7azbb1c2z742lgw0i3y29mwcai54qa5") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-0.10.5 (c (n "sql_query_builder") (v "0.10.5") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0ynqd70pjggi0wxh2zd2dk60s5pvkif39rgc921i6v4hg6f1xb6b") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-0.11.5 (c (n "sql_query_builder") (v "0.11.5") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "04l3jivb9dblh62l1f8r98xyashgh85fmw6rimxdn7g01922ll1n") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-0.11.6 (c (n "sql_query_builder") (v "0.11.6") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "05hhbcyidw7fzmf13lf1yqkcc3ymdmm0malhx6z2slr4v23j8bcf") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-1.0.0 (c (n "sql_query_builder") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0xna90cykxrnjjl4gvs17awsjgcsi0arr4y5flyb7c8x1xnvpsw7") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-1.0.1 (c (n "sql_query_builder") (v "1.0.1") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1f8vjiqsjbiwbxrs8jflbvwcixkcdyr24s59m0c75h3y9wq2kxhm") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-1.0.2 (c (n "sql_query_builder") (v "1.0.2") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0yc9ak0ba44mka118ylrs8xhi8mpb9dhk0xv93ib04apca9m9h7w") (f (quote (("postgresql"))))))

(define-public crate-sql_query_builder-1.0.3 (c (n "sql_query_builder") (v "1.0.3") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1qk4qwpzflszi6jwb7mvx0wz5zg779c7zd5jzqnifgkbdgrn9f94") (f (quote (("postgresql")))) (r "1.58")))

(define-public crate-sql_query_builder-1.0.4 (c (n "sql_query_builder") (v "1.0.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1nlsgv7z4h9jcrasmk7shccfpyfs8xsharwbcb559g2ny5kvggz0") (f (quote (("postgresql")))) (r "1.58")))

(define-public crate-sql_query_builder-1.1.4 (c (n "sql_query_builder") (v "1.1.4") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0swl78jpp9323z33qljvl2ak1cp7g0y87cclgp89l1k98k4pwpw7") (f (quote (("postgresql")))) (r "1.58")))

(define-public crate-sql_query_builder-2.0.0-rc.1 (c (n "sql_query_builder") (v "2.0.0-rc.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0pnhr6ak5rmmqjv5mfqr8x8a58m8cpc8ji5kypx0czp5bjbpb4p0") (f (quote (("sqlite") ("postgresql")))) (r "1.62")))

(define-public crate-sql_query_builder-2.0.0 (c (n "sql_query_builder") (v "2.0.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "06ps2nvngk6d0jkz7wlpzbhr4fqla09vjwaag12ls6vln3kkz1jp") (f (quote (("sqlite") ("postgresql")))) (r "1.62")))

(define-public crate-sql_query_builder-2.1.0 (c (n "sql_query_builder") (v "2.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "04cn710vfzzkydf28hm9cvglkfdf1qb33vwn710swpr7vjbf8wdk") (f (quote (("sqlite") ("postgresql")))) (r "1.62")))

(define-public crate-sql_query_builder-2.2.0 (c (n "sql_query_builder") (v "2.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1x8dkazw5n2a0pzjnzh9zkb7p81n09an0655jn5bx2nhr3rvlpd8") (f (quote (("sqlite") ("postgresql")))) (r "1.62")))

