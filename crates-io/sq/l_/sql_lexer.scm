(define-module (crates-io sq l_ sql_lexer) #:use-module (crates-io))

(define-public crate-sql_lexer-0.1.0 (c (n "sql_lexer") (v "0.1.0") (h "04zb7j4p50gskgmap7rxq46fgabzgk3l7s0s8q7j42gvs3p08jhk")))

(define-public crate-sql_lexer-0.2.0 (c (n "sql_lexer") (v "0.2.0") (h "1l5l6qs982al7q8qh5v3ii7cbra05fj0bg04xax1y5iq1p41ljk1")))

(define-public crate-sql_lexer-0.3.0 (c (n "sql_lexer") (v "0.3.0") (h "1yhpxabf12z2p8wqqasl66pl43j32lzpgyk1w8rnc4hf3wkvb1xl")))

(define-public crate-sql_lexer-0.3.1 (c (n "sql_lexer") (v "0.3.1") (h "1i0ynrly8w71l6r2vmiw2f5xhpml8n963n8d87m3p927b0nldhck")))

(define-public crate-sql_lexer-0.3.2 (c (n "sql_lexer") (v "0.3.2") (h "0d9gypzrkxdxzk9rfnafzc60j7vkas4cdx8wxfsnf1zycs2skaz8")))

(define-public crate-sql_lexer-0.4.0 (c (n "sql_lexer") (v "0.4.0") (h "0zhfq6a1hwg6iny1iimp3nvb92vk7zb5g0idr2jfa64glw5jmngm")))

(define-public crate-sql_lexer-0.4.1 (c (n "sql_lexer") (v "0.4.1") (h "0rh16h5qckx467rv8gpqydh71qhkzpz3djhvwjvjv92hcnrai9nx")))

(define-public crate-sql_lexer-0.4.2 (c (n "sql_lexer") (v "0.4.2") (h "05g49grbyj5nl7xdffnppns1c43w4pvz85ipm7lzrg0q0mb5lafa")))

(define-public crate-sql_lexer-0.5.0 (c (n "sql_lexer") (v "0.5.0") (h "0ph81ix45x1yy36y5bqnz39fn2gwyqb6zv6kfamqvn3gbi62dvx8")))

(define-public crate-sql_lexer-0.6.0 (c (n "sql_lexer") (v "0.6.0") (h "10lmk8i2d6r7sp2wj7zixix4v952ragwq3ihms2sjyf7sk9704g1")))

(define-public crate-sql_lexer-0.7.0 (c (n "sql_lexer") (v "0.7.0") (h "0k940930carwl9wg9yz6h4k7wpcb9xydcf4yig9i110ajvcl6mmk")))

(define-public crate-sql_lexer-0.8.0 (c (n "sql_lexer") (v "0.8.0") (h "0ac6bsz2b7pciidzzanprl8c9rickyfzkb3zspzfgx2dxvssmv3s")))

(define-public crate-sql_lexer-0.8.1 (c (n "sql_lexer") (v "0.8.1") (h "066xhxz5hr8w8mvgmffkwaq72sbc2r9mf1wigqv1yrlx7bqz17h5")))

(define-public crate-sql_lexer-0.9.0 (c (n "sql_lexer") (v "0.9.0") (h "0jpr5qy7p28snrnmqbi4ilnfr0cqz7dpxalf01w74pj1j2pccz2i")))

(define-public crate-sql_lexer-0.9.1 (c (n "sql_lexer") (v "0.9.1") (h "0mr01s8w26karpdhfaiir35adzrjl6d4a4zw90nsd05qmf4xndph")))

(define-public crate-sql_lexer-0.9.2 (c (n "sql_lexer") (v "0.9.2") (h "0714fgkijxdzc3ki8bwnv400nk0snpaydmhjlj8r0s14y0qy9can")))

(define-public crate-sql_lexer-0.9.3 (c (n "sql_lexer") (v "0.9.3") (h "1l4p6fhbm03na1npik5zdxbvf0cl5lmjgw884p4mrif5pbdq3z3c")))

(define-public crate-sql_lexer-0.9.4 (c (n "sql_lexer") (v "0.9.4") (h "17722viaznsq6lq6clvqw0rf3pr85kca9p3rzb3j3mvy9hdkl1fj")))

(define-public crate-sql_lexer-0.9.5 (c (n "sql_lexer") (v "0.9.5") (h "042krn5wmvzkn14kppwmhc9gsgxxnh4r4r547q08wgkk9wxi69hf")))

(define-public crate-sql_lexer-0.9.6 (c (n "sql_lexer") (v "0.9.6") (h "03wa43wiczxnyp73fjxrly2sq64xnv37cvw3a9g32dvj6c36i16y")))

