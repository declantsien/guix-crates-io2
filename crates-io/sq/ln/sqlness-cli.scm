(define-module (crates-io sq ln sqlness-cli) #:use-module (crates-io))

(define-public crate-sqlness-cli-0.4.1 (c (n "sqlness-cli") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "sqlness") (r "^0.4.1") (f (quote ("mysql"))) (d #t) (k 0)))) (h "0lj0q152a49n0p72z5hyri7276l52sqrqnm7bif6ffrb6adr1qbj")))

(define-public crate-sqlness-cli-0.4.2 (c (n "sqlness-cli") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "sqlness") (r "^0.4.1") (f (quote ("mysql"))) (d #t) (k 0)))) (h "1d53phah4ba2fcpzhn68mgbw59n4l99g9z6dlj4yc99k68d9y2lq")))

(define-public crate-sqlness-cli-0.4.3 (c (n "sqlness-cli") (v "0.4.3") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "sqlness") (r "^0.4.1") (f (quote ("mysql"))) (d #t) (k 0)))) (h "0ckk9h02azpjysyw23vnh24l4pl7pcadn72lg1m5vz1mdndgvhjz")))

(define-public crate-sqlness-cli-0.5.0 (c (n "sqlness-cli") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "sqlness") (r "^0.5") (f (quote ("mysql"))) (d #t) (k 0)))) (h "07zp06csq3ryjmzbssxfg0bf8ydjhvx2abdb4smya0lf6vvfw9bv")))

(define-public crate-sqlness-cli-0.6.0 (c (n "sqlness-cli") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "sqlness") (r "^0.6") (f (quote ("mysql" "postgres"))) (d #t) (k 0)))) (h "0ziydnbkyg96yzv037chf49qlg2s1gb6c4hnda21aiqqzi25sk6z")))

