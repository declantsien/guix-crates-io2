(define-module (crates-io sq s- sqs-replay) #:use-module (crates-io))

(define-public crate-sqs-replay-0.1.0 (c (n "sqs-replay") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.40.0") (d #t) (k 0)) (d (n "rusoto_sqs") (r "^0.40.0") (d #t) (k 0)))) (h "1xxl7b6vwkgv0xmga3fhq6nz0xxkxxylw8mxxg7igksph2z0ns96")))

(define-public crate-sqs-replay-0.2.0 (c (n "sqs-replay") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (d #t) (k 0)) (d (n "rusoto_sqs") (r "^0.42.0") (d #t) (k 0)))) (h "0bk3jlpb1brqxbrvcrgfdykv719lr5lq37gyzvad19n2sv1drxkx")))

(define-public crate-sqs-replay-0.2.1 (c (n "sqs-replay") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (d #t) (k 0)) (d (n "rusoto_sqs") (r "^0.42.0") (d #t) (k 0)))) (h "1djjly1qlpph70cbbmlbdph8k5m9ncwg9d15jfgpxy3cb6d6z7xv")))

