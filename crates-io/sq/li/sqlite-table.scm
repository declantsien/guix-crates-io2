(define-module (crates-io sq li sqlite-table) #:use-module (crates-io))

(define-public crate-sqlite-table-0.1.0 (c (n "sqlite-table") (v "0.1.0") (d (list (d (n "sqlite-decoder") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlite-types") (r "^0.1.1") (d #t) (k 0)))) (h "00lclpw5zgy88jhnyngkx4swd2ym4ss2amyzihxjddwp12v5zy2b")))

