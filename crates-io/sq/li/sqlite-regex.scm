(define-module (crates-io sq li sqlite-regex) #:use-module (crates-io))

(define-public crate-sqlite-regex-0.2.3-alpha.9 (c (n "sqlite-regex") (v "0.2.3-alpha.9") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "1hn5sy7vj8cxs4ndbqdzx8byi5781vcdnd52jn1vmh4ib05jgvlk")))

(define-public crate-sqlite-regex-0.2.3-alpha.10 (c (n "sqlite-regex") (v "0.2.3-alpha.10") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "0bj8k8n2i8z7qh8ph28wjl7vjdvdl0xj5c8yq6rzm38xwjjxza6y")))

(define-public crate-sqlite-regex-0.2.3-alpha.11 (c (n "sqlite-regex") (v "0.2.3-alpha.11") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "0xxljfjn96ik3a3ngni20sjybv7ss748ilbn8x2kjlvjs7yc3vly")))

(define-public crate-sqlite-regex-0.2.3-alpha.12 (c (n "sqlite-regex") (v "0.2.3-alpha.12") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "08fj8fvws9sk5jqa7xm8p2b73n7z884m5bndgxv13gvsx3gmydxx")))

(define-public crate-sqlite-regex-0.2.3 (c (n "sqlite-regex") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "1ym4a86jmz2x1qm5jay716p7vdpmblv0cykv2zyj8mbzwa3mnrln")))

(define-public crate-sqlite-regex-0.2.4-alpha.1 (c (n "sqlite-regex") (v "0.2.4-alpha.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)))) (h "1s7k0v9pd5c2bzxh11x7svbdayfkqgzb9w8wnyyjckz3dfy1xl5d")))

