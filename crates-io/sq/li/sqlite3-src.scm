(define-module (crates-io sq li sqlite3-src) #:use-module (crates-io))

(define-public crate-sqlite3-src-0.1.0 (c (n "sqlite3-src") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19ffrr842xmfc9aqxp74p1v2nsz4rqr7a2kb45wnr041jlsnxcx1")))

(define-public crate-sqlite3-src-0.2.0 (c (n "sqlite3-src") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vyrfqzsq1141sw5k02l82q3m648icgxzz706ydx0wa1jwpj0lh4") (f (quote (("bundled")))) (y #t)))

(define-public crate-sqlite3-src-0.2.1 (c (n "sqlite3-src") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hzsxjqhyi13wv5nyjx5d05qm4l1m17z3ca3z3rszgwmkc3dr7gf") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.2 (c (n "sqlite3-src") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0frifms5v04k5as5pf8vbckxiymshpxb116y1v30f6vlmrbd9ngn") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.3 (c (n "sqlite3-src") (v "0.2.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w7zfav4s4jxk8w704i6d1450pvry872sv3xrax75hnnq66pih8l") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.4 (c (n "sqlite3-src") (v "0.2.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19wni97zzyskxlykhcz0n3qlgl4385fvs18qlbsz92j9yxaddkag") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.6 (c (n "sqlite3-src") (v "0.2.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1vfv36kpjaghigigyw3db7nxq74zbl3mhdz43rc4cd2clv3jmw2j") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.7 (c (n "sqlite3-src") (v "0.2.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1in4jgds8v7hcanvybwja09ya5fszx566p40v1v8xbdzkg5q8xln") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.8 (c (n "sqlite3-src") (v "0.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "171fx4fsii78v50gwi33cdd7sxdlp96pjy6g1kaqs5653lss0i1c") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.9 (c (n "sqlite3-src") (v "0.2.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "122sixycaivmi1qna73xyli8aj5zardqkvv5qvk1wfsnbc8vrq26") (f (quote (("bundled"))))))

(define-public crate-sqlite3-src-0.2.10 (c (n "sqlite3-src") (v "0.2.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1339by6wdnz5jw0lzigkx2p7zbk5bxi9y15q36ha557bq5kmvkb2") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.2.11 (c (n "sqlite3-src") (v "0.2.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ppnq8chb5fdwpvb562icpdmv1f59bv7r83nbik7ybv12pxk2bya") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.2.12 (c (n "sqlite3-src") (v "0.2.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "055klrb7q9ycjwydg6ndincwapniwchsvq4pi8i8hr02dpk2bfxq") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.3.0 (c (n "sqlite3-src") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18ygmfcpkccs8s9m5s9q31rrx1mrdps387w9yp3481jswxyb0q52") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.4.0 (c (n "sqlite3-src") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "14ancc9jafw5ql9carg27icjxcfrdz5izxk4bj7fp5n909x5m0fi") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.5.0 (c (n "sqlite3-src") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05j962gfn43jklbqq64lm2ask7mgzs3dkma9bvypi7wqg3ccz8mx") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.5.1 (c (n "sqlite3-src") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0m74wrkpify3z0xvrw4i2yssn9m9sjwqa5ipk6aq6f7fl58mmjdz") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.6.0 (c (n "sqlite3-src") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "00q50hmsapa8mdh4m3qdzd8kbha7jyp8if5h8qdcsnj9k5cd8j4l") (f (quote (("bundled")))) (l "sqlite3")))

(define-public crate-sqlite3-src-0.6.1 (c (n "sqlite3-src") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jkvjhgrfsq5m2ps3hh792mamwv8v6kf2gdj3wldn9vwyxnllk8p") (f (quote (("bundled")))) (l "sqlite3")))

