(define-module (crates-io sq li sqlite3-provider) #:use-module (crates-io))

(define-public crate-sqlite3-provider-0.0.1 (c (n "sqlite3-provider") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06d5jvv48wl2l68n5jqbd0crg83ccp94vgnb2ydk553fg66pv1pl")))

(define-public crate-sqlite3-provider-0.1.0 (c (n "sqlite3-provider") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0k0xfvamfxhhp7zj97j77d8palrxzkbccbb3xckx5fa2jb1jff99")))

