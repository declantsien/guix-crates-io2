(define-module (crates-io sq li sqlite-encoder) #:use-module (crates-io))

(define-public crate-sqlite-encoder-0.1.0 (c (n "sqlite-encoder") (v "0.1.0") (d (list (d (n "sqlite-types") (r "^0.1.0") (d #t) (k 0)))) (h "0z0639ycm15s98jqjbb3pngs4rfxhasq6cywhlnnz1g8c1mig0zd")))

(define-public crate-sqlite-encoder-0.1.1 (c (n "sqlite-encoder") (v "0.1.1") (d (list (d (n "sqlite-types") (r "^0.1.1") (d #t) (k 0)))) (h "1xk4b1d8zrxrhy25k0hqcn1v731d72gamax6a3vk571raaj90l4l")))

