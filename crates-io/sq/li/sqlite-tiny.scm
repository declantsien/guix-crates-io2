(define-module (crates-io sq li sqlite-tiny) #:use-module (crates-io))

(define-public crate-sqlite-tiny-0.1.0 (c (n "sqlite-tiny") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (k 1)))) (h "0d001lviv3l583ygllcnisv62b41wn6wnzxm3ipj9n6qf6yn80nr") (f (quote (("default" "api") ("api"))))))

(define-public crate-sqlite-tiny-0.1.1 (c (n "sqlite-tiny") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (k 1)))) (h "0zgmd4qgfg598p9nyx8iyjhsihjj1iyb57jlcjr6x6cn1s329kxr") (f (quote (("default" "api") ("api"))))))

