(define-module (crates-io sq li sqlite-opcode) #:use-module (crates-io))

(define-public crate-sqlite-opcode-0.1.0 (c (n "sqlite-opcode") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlite-table") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "148avapm6s03sv4ffhah7nysi2r4qpbkg73q23h6iv14c6rxifps")))

