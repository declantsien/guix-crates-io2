(define-module (crates-io sq li sqlite-robotstxt) #:use-module (crates-io))

(define-public crate-sqlite-robotstxt-0.0.1-alpha.2 (c (n "sqlite-robotstxt") (v "0.0.1-alpha.2") (d (list (d (n "robotstxt") (r "^0.3.0") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.6-alpha.2") (d #t) (k 0)))) (h "1hsa7jij4fwjaahvapvgcgamhd567wv9gdw78i7pa4hhb1iwb85l")))

(define-public crate-sqlite-robotstxt-0.0.1-alpha.3 (c (n "sqlite-robotstxt") (v "0.0.1-alpha.3") (d (list (d (n "robotstxt") (r "^0.3.0") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.6-alpha.2") (d #t) (k 0)))) (h "0xn9pzn3d1pc6nbf9yckhmxy8b789xq5jky6asyilwqw66a02jvj")))

