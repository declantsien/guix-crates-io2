(define-module (crates-io sq li sqlite3-sys) #:use-module (crates-io))

(define-public crate-sqlite3-sys-0.0.1 (c (n "sqlite3-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0237ap0v0397pfg1i2aa5aqqdkrgr9yp6n9khhp4356a77rn7n68")))

(define-public crate-sqlite3-sys-0.0.2 (c (n "sqlite3-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "15cqbvpldh58a5l1jqzwfbd312qib3lk7ykc4rqr45arvh74sbf7")))

(define-public crate-sqlite3-sys-0.1.0 (c (n "sqlite3-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1rvqn5gldkmv7vis6648qrlhvraz6x4bjg2yd78wdaci43sbmc1r")))

(define-public crate-sqlite3-sys-0.2.0 (c (n "sqlite3-sys") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0089p78jpq98i6qcdagvcxpldbn52aghfsqv6cvhrb861fx8r9lg")))

(define-public crate-sqlite3-sys-0.2.1 (c (n "sqlite3-sys") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1gxbdnv5s51j18i949bs1c0c8gjymrhyw3ggvjrshav3gj49w62h")))

(define-public crate-sqlite3-sys-0.2.2 (c (n "sqlite3-sys") (v "0.2.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1kn0p8gy8ddwhars58rppcm1q5dcdb9pqls8js3n00ahynd2fbys")))

(define-public crate-sqlite3-sys-0.2.3 (c (n "sqlite3-sys") (v "0.2.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1i45r2wmxqkyc10jind4my8426fkwma5xx9mdyb4p302yl2r64vy")))

(define-public crate-sqlite3-sys-0.3.0 (c (n "sqlite3-sys") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0zdhy3p8dwxds62dk0npk5nll33q98q03cpc2caxwz0kb9lwfgkw")))

(define-public crate-sqlite3-sys-0.4.0 (c (n "sqlite3-sys") (v "0.4.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1ddfmqxdxl7iid0xaawhva26i9frcfl7zkkpnsrza9zj80kqppyr")))

(define-public crate-sqlite3-sys-0.5.0 (c (n "sqlite3-sys") (v "0.5.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0kfmg15r4kxx3998vr87vsmc2y93hzxidf6bkg4yk354k8n7ls34") (f (quote (("edge"))))))

(define-public crate-sqlite3-sys-0.5.1 (c (n "sqlite3-sys") (v "0.5.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1nignb9glnjvmjjl7zvq9fz4a11ia6a1f4dhrdrmwy8lbrinyrks") (y #t)))

(define-public crate-sqlite3-sys-0.6.0 (c (n "sqlite3-sys") (v "0.6.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1sz9vpc7mkhj9xiqhq23w633swk0z0inxf001sh27afha5fspfvl")))

(define-public crate-sqlite3-sys-0.6.1 (c (n "sqlite3-sys") (v "0.6.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1nlpi8x1gbwh5bd9axvxad95m7szj5ydacjim7d3g3lqm673b1nc")))

(define-public crate-sqlite3-sys-0.6.2 (c (n "sqlite3-sys") (v "0.6.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "056f8sy61p86ngc3la96yj8wfmi72fdsq4lnqya4sgg79lbl255w")))

(define-public crate-sqlite3-sys-0.6.3 (c (n "sqlite3-sys") (v "0.6.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "05h2asjnw73b2hqnixji1rj7jjj7x0jwv7m6qvcnlawpic8sincb")))

(define-public crate-sqlite3-sys-0.6.4 (c (n "sqlite3-sys") (v "0.6.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "09qs0s91ww0hzhgwahaf39xplk01lc3v8wf8daf1rcsbq5drqsa5")))

(define-public crate-sqlite3-sys-0.6.5 (c (n "sqlite3-sys") (v "0.6.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "08a7mx925q873khvgvzrzrzznqs62gn6yh9sykrx6kg8b341v74f")))

(define-public crate-sqlite3-sys-0.6.6 (c (n "sqlite3-sys") (v "0.6.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0zrn7vnxj6f1y88g490kr8wl7myc3nhjdx6jl7ajcx51d99dgk6g")))

(define-public crate-sqlite3-sys-0.6.7 (c (n "sqlite3-sys") (v "0.6.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "061kd3f0jqi3s4qrwfbpdahb4718fq94aq53x64jpmkqvk6wlna1")))

(define-public crate-sqlite3-sys-0.7.0 (c (n "sqlite3-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlcipher-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "sqlite3-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "06p5wp086qv384y1sydy7yprzwl0ns233cbdw15y4g70hm093b0i") (f (quote (("standard" "sqlite3-provider") ("sqlcipher" "sqlcipher-provider") ("default" "standard"))))))

(define-public crate-sqlite3-sys-0.8.0 (c (n "sqlite3-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlcipher-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "sqlite3-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "16wzyp5f60knxz6vin1klz65mbqmf410afy4mfbrpi4x6w6psd3r") (f (quote (("standard" "sqlite3-provider") ("sqlcipher" "sqlcipher-provider") ("default" "standard"))))))

(define-public crate-sqlite3-sys-0.9.0 (c (n "sqlite3-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlcipher-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "sqlite3-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "1q3m5lb758cbybmdy0y3ps2q5bi3df61b72d32l00wbam9flr0h2") (f (quote (("standard" "sqlite3-provider") ("sqlcipher" "sqlcipher-provider") ("default" "standard"))))))

(define-public crate-sqlite3-sys-0.9.1 (c (n "sqlite3-sys") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlcipher-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "sqlite3-provider") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "0abl6c6idldxampzc707hlvd1bj6pvxh8bhx45p5hr1yrddisg7b") (f (quote (("standard" "sqlite3-provider") ("sqlcipher" "sqlcipher-provider") ("default" "standard"))))))

(define-public crate-sqlite3-sys-0.9.2 (c (n "sqlite3-sys") (v "0.9.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlcipher-provider") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "sqlite3-provider") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "1gmb6mqd9d94wa5ly85imv32kzrzf3xxmbya910d8sv271v04b9m") (f (quote (("standard" "sqlite3-provider") ("sqlcipher" "sqlcipher-provider") ("default" "standard"))))))

(define-public crate-sqlite3-sys-0.9.3 (c (n "sqlite3-sys") (v "0.9.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlcipher-src") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1pmp4j8hzl4l1g9advl8jlibc17j1axbkxmijdyy10v54n9ab5s7") (f (quote (("standard" "sqlite3-src") ("sqlcipher" "sqlcipher-src") ("default" "standard"))))))

(define-public crate-sqlite3-sys-0.10.0 (c (n "sqlite3-sys") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlcipher-src") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1bbrppam5fiyphlkpsybj5hagr6ry6mvbfb0ashznyb3l3nipqz7") (f (quote (("standard" "sqlite3-src") ("sqlcipher" "sqlcipher-src") ("default" "standard"))))))

(define-public crate-sqlite3-sys-0.11.0 (c (n "sqlite3-sys") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "08qlxfj958d6ghsf4w36ikvxbvzb1czxs0ypnzcggjiqmqir3a84") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.11.1 (c (n "sqlite3-sys") (v "0.11.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0gp68y35xcx4vv5nbh3494hjyjq3zfzzpgbhdr2jamrjgkwxqr92") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.11.2 (c (n "sqlite3-sys") (v "0.11.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1y9cjx6qsvzcbva648ks98dr8pbnnid7k5xvg3dand96a4qh9x4p") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.11.3 (c (n "sqlite3-sys") (v "0.11.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "15na9x08r8pjyjd04685cvn8zzhw8sqjqgz72vc3vipyj3yghgc6") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.12.0 (c (n "sqlite3-sys") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "027vm1ifq9i78n5f2mfzd3f9piv7blbrdhxfx8zd2jskl43wizki") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.13.0 (c (n "sqlite3-sys") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0m1f5r4xg5i3r6795q8vwqfdcq3gh1qlfjwkywnka57bz8lg1lh4") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.14.0 (c (n "sqlite3-sys") (v "0.14.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1vmrzgchmbqk9jk1dq1jp1lq6id0p3h8vwna02x60ly59y19jz6l") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.15.0 (c (n "sqlite3-sys") (v "0.15.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "135mm35755h9in2dy7mxsh54lcfh337qzqz6qm9wh08hrwcvnxcm") (f (quote (("linkage" "sqlite3-src") ("default" "linkage"))))))

(define-public crate-sqlite3-sys-0.15.1 (c (n "sqlite3-sys") (v "0.15.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "11w9mxpp5mh7b1c9b41b3cwn0s90svw0b3j4z1bghqjv16nmswn3") (f (quote (("linkage" "sqlite3-src") ("default" "linkage") ("bundled" "sqlite3-src/bundled"))))))

(define-public crate-sqlite3-sys-0.15.2 (c (n "sqlite3-sys") (v "0.15.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0fq6m21dnd5yqrzknsmnl2565nahdwa29s7x12xhxr1kjik2qxgj") (f (quote (("linkage" "sqlite3-src") ("default" "linkage") ("bundled" "sqlite3-src/bundled"))))))

(define-public crate-sqlite3-sys-0.16.0 (c (n "sqlite3-sys") (v "0.16.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "08bk14lxjqrf360v8c4p65rk2j9djzqmsb8djc1i2biq50cbdl5f") (f (quote (("linkage" "sqlite3-src") ("default" "linkage") ("bundled" "sqlite3-src/bundled"))))))

(define-public crate-sqlite3-sys-0.17.0 (c (n "sqlite3-sys") (v "0.17.0") (d (list (d (n "sqlite3-src") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1rdds3kzxbxwy3lpsvgy7g8nh609nzqpxv4jvj23ag0c16kss09r") (f (quote (("linkage" "sqlite3-src") ("encryption") ("default" "linkage") ("bundled" "sqlite3-src/bundled"))))))

