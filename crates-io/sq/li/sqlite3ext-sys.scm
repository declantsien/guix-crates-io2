(define-module (crates-io sq li sqlite3ext-sys) #:use-module (crates-io))

(define-public crate-sqlite3ext-sys-0.0.1 (c (n "sqlite3ext-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.46") (d #t) (k 1)))) (h "05ng0z2p4hcv7xysdk5i79h6i8kmk78hgapqxip6vw88vjrw5z9s")))

