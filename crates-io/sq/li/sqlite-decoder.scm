(define-module (crates-io sq li sqlite-decoder) #:use-module (crates-io))

(define-public crate-sqlite-decoder-0.1.0 (c (n "sqlite-decoder") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "sqlite-types") (r "^0.1.0") (d #t) (k 0)))) (h "1lxds6v2kq4c2jc0jigbn4jb4i0k0a2h5sjh6iqann2a7g5ixrwx")))

(define-public crate-sqlite-decoder-0.1.1 (c (n "sqlite-decoder") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "sqlite-types") (r "^0.1.1") (d #t) (k 0)))) (h "19ymw7p1vqrj6wawxrr0iznngx3wp60x09dz9lc12219ihj59205")))

