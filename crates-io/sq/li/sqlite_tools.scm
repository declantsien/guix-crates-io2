(define-module (crates-io sq li sqlite_tools) #:use-module (crates-io))

(define-public crate-sqlite_tools-0.1.0 (c (n "sqlite_tools") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "049mm3f83dkln3rbi2cgdfgcqx0h0w3sph3xblc6vx557dy1r3bp")))

