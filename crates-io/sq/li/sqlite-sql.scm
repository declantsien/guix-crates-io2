(define-module (crates-io sq li sqlite-sql) #:use-module (crates-io))

(define-public crate-sqlite-sql-0.1.0 (c (n "sqlite-sql") (v "0.1.0") (h "1xyx3jp0dy9cy5r3ssq5m3kgaf6mq9lb29pf2yyl8mx9i5vx3dyz")))

(define-public crate-sqlite-sql-0.1.1 (c (n "sqlite-sql") (v "0.1.1") (h "1zjz6i5bbq4k9pb2bhpwa8qd9hdcg78gas23fk44a1g0ff9hap3z")))

(define-public crate-sqlite-sql-0.1.2 (c (n "sqlite-sql") (v "0.1.2") (h "1cgwqaayzv9lpc5lg2ncbw5yx99f8nm3cx9qzr74zbaylsimqhmk")))

