(define-module (crates-io sq li sqlite3-builder) #:use-module (crates-io))

(define-public crate-sqlite3-builder-3.38.1 (c (n "sqlite3-builder") (v "3.38.1") (h "0knaxqm1gnhhw45f00r5lf2afwi7aagpka4bnn9dxcmx786pnzpr") (y #t)))

(define-public crate-sqlite3-builder-3.38.2 (c (n "sqlite3-builder") (v "3.38.2") (h "07hv53qdfi5c675xf6x33jrkc5kwcv2r5kpbm5xn1svj74kv1fr2") (y #t)))

(define-public crate-sqlite3-builder-3.38.3 (c (n "sqlite3-builder") (v "3.38.3") (h "0ki7fdbm17ailbq90sxlnbld0xwyilfg2s47j5vmqpy8yfw2h8y5") (y #t)))

(define-public crate-sqlite3-builder-3.38.4 (c (n "sqlite3-builder") (v "3.38.4") (h "04fyar1a5503rdn5xw69yqcr1jrss1c3nml723yzda6bd3s6f0kr") (y #t)))

(define-public crate-sqlite3-builder-3.38.5 (c (n "sqlite3-builder") (v "3.38.5") (h "1gjn1mcxdd2gg2xgcx5wmikd2g2gwf25ciz3fd58bim9fh0ybbhw")))

(define-public crate-sqlite3-builder-3.38.6 (c (n "sqlite3-builder") (v "3.38.6") (h "0rs7s1wrjnqify3p6qrprv19slr24k9lslvrq7a3dzib06yb9y08")))

(define-public crate-sqlite3-builder-3.38.7 (c (n "sqlite3-builder") (v "3.38.7") (h "0l0hgs5w926bg6mgrxf0sdf7b1qki68123d0sz5rnrbykydl17hm")))

(define-public crate-sqlite3-builder-3.39.4 (c (n "sqlite3-builder") (v "3.39.4") (h "0ggd0639b8ifkhghqafpi3s2qhm5sr6rhv6wark6ja4bqfg0b5dc")))

