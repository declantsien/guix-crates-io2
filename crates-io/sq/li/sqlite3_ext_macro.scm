(define-module (crates-io sq li sqlite3_ext_macro) #:use-module (crates-io))

(define-public crate-sqlite3_ext_macro-0.1.0 (c (n "sqlite3_ext_macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "17a63iwbclk0gvkkm7myh6xsw3mf7bi0hvwg9spn963h387b3zxn")))

