(define-module (crates-io sq li sqlite_parser) #:use-module (crates-io))

(define-public crate-sqlite_parser-0.1.0 (c (n "sqlite_parser") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1vg54k7l7fws5mmagvsgx3mmsim4y4d05gl2nkhgll7f3v1aac2d")))

(define-public crate-sqlite_parser-0.1.1 (c (n "sqlite_parser") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1aq4v3d8hlylm9449dp344dbd63pakdgnmdz3pi5b3k4lj78lp1b")))

(define-public crate-sqlite_parser-0.1.2 (c (n "sqlite_parser") (v "0.1.2") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0bkq2mhm82x6p5z8j9zwmwgbn7pm7hh7zdacdg6rkwz4qx67a7py")))

(define-public crate-sqlite_parser-0.1.3 (c (n "sqlite_parser") (v "0.1.3") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1pvhzgyg8z7n7d1bhpg99h6bmp6wgizhamdl6h3k1yix82gizjpj")))

(define-public crate-sqlite_parser-0.1.4 (c (n "sqlite_parser") (v "0.1.4") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1aykpi5d09qyrvni7lkna0x0janrhmgfsln320b1i14xkbb7lzfr")))

(define-public crate-sqlite_parser-0.1.5 (c (n "sqlite_parser") (v "0.1.5") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0f1289qm4h9dx54rk4ql9xaws6mq23zxcvbpy2pf9mf90b6yxh7k")))

(define-public crate-sqlite_parser-0.1.6 (c (n "sqlite_parser") (v "0.1.6") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0nn3vq48x3rb07lzn8bfkyb4l2hhz7dwm92im1prw1mxqn8ci8yv")))

(define-public crate-sqlite_parser-0.1.7 (c (n "sqlite_parser") (v "0.1.7") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0zgswdns0aiqgvhv68lf1rv0d6jkl6y1slsva1y65hynk1ifl54w")))

(define-public crate-sqlite_parser-0.1.8 (c (n "sqlite_parser") (v "0.1.8") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1lsvkjvxzw1i40gbh4ja0j19la204px1h24xl44qrbs29p1rgx7w")))

(define-public crate-sqlite_parser-0.1.9 (c (n "sqlite_parser") (v "0.1.9") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0hpa7w2lcygi982l5zxkz9zi7z7s3jqbkpcy9xzb5fx521vgsraq")))

(define-public crate-sqlite_parser-0.1.10 (c (n "sqlite_parser") (v "0.1.10") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "12ccdr91rkdy0jsg6w8jhf393h66fwyhxpvjifs6w35g7l91ils6")))

(define-public crate-sqlite_parser-0.1.11 (c (n "sqlite_parser") (v "0.1.11") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "06mdg6sgihv9jkjl8dxgak6rbaycw5dwxlfzj8kgz6zw821ap4n3")))

(define-public crate-sqlite_parser-0.1.12 (c (n "sqlite_parser") (v "0.1.12") (d (list (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0sd110qhc4jw6fx3vdp31hxh7sfzk1nbyxgriv3mal04mzmbgjvk")))

(define-public crate-sqlite_parser-0.1.13 (c (n "sqlite_parser") (v "0.1.13") (d (list (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1sczaxcnpv4wn875ca99cny8bhmng9ndplc8a78z1rlcgjr8zzjy")))

(define-public crate-sqlite_parser-0.1.14 (c (n "sqlite_parser") (v "0.1.14") (d (list (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1whfb97bcbx2whhqgf1v985sj89rqr6pn9z7p7hv9n3rc9izpwzf")))

(define-public crate-sqlite_parser-0.1.15 (c (n "sqlite_parser") (v "0.1.15") (d (list (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0dvxh9bk60if6hns1995xs743ga305gr7vyqwfm0ljw88dkbjsjg")))

(define-public crate-sqlite_parser-0.1.16 (c (n "sqlite_parser") (v "0.1.16") (d (list (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "164yw1igxvjyhyy42m5s9k34x35ilzpkkql585frzvixrbh4fikj")))

(define-public crate-sqlite_parser-0.1.17 (c (n "sqlite_parser") (v "0.1.17") (d (list (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1lpwrj6rn926fxf2px3zkd3bwf7qhlzlk75ickv9yd8l5baff0hs")))

(define-public crate-sqlite_parser-0.1.18 (c (n "sqlite_parser") (v "0.1.18") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1cw7f7c30ysf12igfkslzf8p26wd9hsnmqzmn5kv2w8x9qnxf32w")))

(define-public crate-sqlite_parser-0.1.19 (c (n "sqlite_parser") (v "0.1.19") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1fbbw8gqfsv4ay65vg83pkcc23bpc0wkl92kk1wxqc0d3zqylv2m")))

(define-public crate-sqlite_parser-0.1.20 (c (n "sqlite_parser") (v "0.1.20") (d (list (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1bv19qcmim2l25v6j5vafpj21714rmxwh6b5nf82963n1n3lidw1")))

