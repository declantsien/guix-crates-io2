(define-module (crates-io sq li sqlite3-header) #:use-module (crates-io))

(define-public crate-sqlite3-header-0.1.0 (c (n "sqlite3-header") (v "0.1.0") (h "11a0mwf128plzkrhm65m2w53v2d54w6q0zrw0jm5h0cjak3r8ghs")))

(define-public crate-sqlite3-header-0.1.1 (c (n "sqlite3-header") (v "0.1.1") (h "063hdvy11vz367a9jg8azg3535raw0l1rahc605hpbswkkwrvlls")))

(define-public crate-sqlite3-header-0.2.0 (c (n "sqlite3-header") (v "0.2.0") (h "1adbxcqr5k79xpyblsvwb38cdchvrpbx3a5fqp45lmq4kcn0q0y1")))

(define-public crate-sqlite3-header-0.3.0 (c (n "sqlite3-header") (v "0.3.0") (h "1x0bkkvcv71pff0216m7bv9bs77y9n98nxwz9n7b0db81vgw11zm")))

(define-public crate-sqlite3-header-0.4.0 (c (n "sqlite3-header") (v "0.4.0") (h "1lmyprpv9f6pa4h32ny9936gbv0zjz9jzl0rmfx3iaj55lva3d0f")))

(define-public crate-sqlite3-header-0.4.1 (c (n "sqlite3-header") (v "0.4.1") (h "1zwnwi8pq770jrzmzqsihzaq33p3wqrqr82p2ccb42hq77i02mvr")))

