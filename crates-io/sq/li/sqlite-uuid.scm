(define-module (crates-io sq li sqlite-uuid) #:use-module (crates-io))

(define-public crate-sqlite-uuid-0.2.0 (c (n "sqlite-uuid") (v "0.2.0") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "0gr6h1sndmj3hm46f1ylrk9mr4w1901f1vrd9dfkwk4zchlmqbyx")))

(define-public crate-sqlite-uuid-0.2.1 (c (n "sqlite-uuid") (v "0.2.1") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "0h56740603gf83bzdf1a0pcbb2m93av70x0qmcijshggl1nf6qrm")))

(define-public crate-sqlite-uuid-0.2.2 (c (n "sqlite-uuid") (v "0.2.2") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "1z0pvgmpb14q5hi31lcqbb5fay4xd351zfz43mprsv66nnl3cxng")))

(define-public crate-sqlite-uuid-0.2.3 (c (n "sqlite-uuid") (v "0.2.3") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "0l6zljw47kkx22ijy13mfndi40k95y3l5294m9h7im49n7nr7pl9")))

(define-public crate-sqlite-uuid-0.2.4 (c (n "sqlite-uuid") (v "0.2.4") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "0450wk8qvz8ax2k0z40svlkdjwpl6hplkxq5likvp2wbdbjprdwh")))

(define-public crate-sqlite-uuid-0.2.5 (c (n "sqlite-uuid") (v "0.2.5") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "0sab2bw4wb11f79lf93ylv1mvp7h9wm2s8iq6wpag9npzib96ybc")))

(define-public crate-sqlite-uuid-0.2.6 (c (n "sqlite-uuid") (v "0.2.6") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "08rp7hihh7rjpja21w1ywvgg7zn0jqs33wny5zsinja5g146mhsj")))

(define-public crate-sqlite-uuid-0.2.7 (c (n "sqlite-uuid") (v "0.2.7") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "0gh5bsgfyx9l1k01334v9wj0gql1nnszykg6y3sqana6j16r252z")))

(define-public crate-sqlite-uuid-0.2.8 (c (n "sqlite-uuid") (v "0.2.8") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "0qz58ynjyjbhsy04whwczg8wg32dzxp336zh4q7ck8004aafn3gj")))

(define-public crate-sqlite-uuid-0.2.9 (c (n "sqlite-uuid") (v "0.2.9") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "146hx42q2r6bbhra959qlrxqnbkcf6bcapwi3ydgvhc8f5ayrjli")))

(define-public crate-sqlite-uuid-0.3.0 (c (n "sqlite-uuid") (v "0.3.0") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "1kybx7y184rby5pb1aaa35lw62h6c3c54qn2yblp0dskibr654q1")))

(define-public crate-sqlite-uuid-0.4.0 (c (n "sqlite-uuid") (v "0.4.0") (d (list (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("std" "v7" "v4"))) (d #t) (k 0)))) (h "1bcpmqg5lrrris3br2l1w0kn5p53b2nd00xi3m76k8l3dy11a768")))

