(define-module (crates-io sq li sqlite-vfs) #:use-module (crates-io))

(define-public crate-sqlite-vfs-0.1.0 (c (n "sqlite-vfs") (v "0.1.0") (d (list (d (n "libsqlite3-sys") (r "^0.23") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0ajyqknk099llfvv9q6wbxhfxv96xkx4va0sb837jrk7sxl7mrnk")))

(define-public crate-sqlite-vfs-0.2.0 (c (n "sqlite-vfs") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0f6zr5pmbj0w8v4p316l8il9lwbw4d6mqa5yz118z8gsrdr8j9d0") (f (quote (("syscall") ("sqlite_test") ("loadext") ("default"))))))

