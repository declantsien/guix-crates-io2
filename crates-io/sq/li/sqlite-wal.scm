(define-module (crates-io sq li sqlite-wal) #:use-module (crates-io))

(define-public crate-sqlite-wal-0.1.0 (c (n "sqlite-wal") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "sqlite-decoder") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlite-encoder") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlite-types") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0izz606ny04xjny3ipcfm58hh4hr7i7c6iq91ymzvgmqzlrlyw4s")))

