(define-module (crates-io sq li sqlib) #:use-module (crates-io))

(define-public crate-sqlib-0.1.0 (c (n "sqlib") (v "0.1.0") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "09wc3c202023sn80dg6rl55njwgg8151vx8pcgsc3h3azg3jwwmx")))

(define-public crate-sqlib-0.1.1 (c (n "sqlib") (v "0.1.1") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "0pzn5bgfhpdjvxggv5kixq1iirzrgivl83kfhnbhv53pyg4av4kq")))

(define-public crate-sqlib-0.1.2 (c (n "sqlib") (v "0.1.2") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "0sfhidhpnpr1cqv7979xcwlpqryzk8xblla459d75r5y18pap67m")))

(define-public crate-sqlib-0.1.3 (c (n "sqlib") (v "0.1.3") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "0pk3g0avs4bsb6gj0id7cb8i7fgb5rw2881ami4rk8ja0h0ibz0a")))

(define-public crate-sqlib-0.1.4 (c (n "sqlib") (v "0.1.4") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "1xlkxjw4lrskcda4fvvdyx4flad446p71xli736f15ab4i6ks690")))

(define-public crate-sqlib-0.1.5 (c (n "sqlib") (v "0.1.5") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "1kcxjzyy7x061mwjxyffb4rm2qcp7gmy2q0kmpiy6xfbia0kv3q8")))

(define-public crate-sqlib-0.2.6 (c (n "sqlib") (v "0.2.6") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 0)))) (h "1dlh71132hyw8dvxiigqy1c6hz4nf8xwf1cizihgyi4ywppaxr0a")))

