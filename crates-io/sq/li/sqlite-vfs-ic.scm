(define-module (crates-io sq li sqlite-vfs-ic) #:use-module (crates-io))

(define-public crate-sqlite-vfs-ic-0.2.0 (c (n "sqlite-vfs-ic") (v "0.2.0") (d (list (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sqgzl19qa6ys9qvdpkpgqc055j8js6ikk43yv4w3fyaffamsqqm") (f (quote (("syscall") ("sqlite_test") ("loadext") ("default"))))))

