(define-module (crates-io sq li sqlite-types) #:use-module (crates-io))

(define-public crate-sqlite-types-0.1.0 (c (n "sqlite-types") (v "0.1.0") (h "01kd438byv3ar2kbz2cq3l208dn6d8j2lqni5ijn0y9apbi7yigf")))

(define-public crate-sqlite-types-0.1.1 (c (n "sqlite-types") (v "0.1.1") (h "11f88v40a7yvpcyjcav8byldhirb7fkwp1870vdd58pdfgq4wprj")))

