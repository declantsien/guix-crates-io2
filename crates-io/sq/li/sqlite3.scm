(define-module (crates-io sq li sqlite3) #:use-module (crates-io))

(define-public crate-sqlite3-0.24.0 (c (n "sqlite3") (v "0.24.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0g2qbvxljby4ldpyha70yax7mcprb1cjz6aqyzrb6h0rbq7v21hr") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

