(define-module (crates-io sq li sqlite-fastrand) #:use-module (crates-io))

(define-public crate-sqlite-fastrand-0.2.1-alpha.9 (c (n "sqlite-fastrand") (v "0.2.1-alpha.9") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "0d168jw58yx347pc9n8f9hq6w29ys0vldjb4y9sz8fzll6av4j68")))

(define-public crate-sqlite-fastrand-0.2.1-alpha.10 (c (n "sqlite-fastrand") (v "0.2.1-alpha.10") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "04y100jc2zd6s08niawf9fq02s5wdp8na9vfxffgdqaksr7h7nxj")))

(define-public crate-sqlite-fastrand-0.2.1 (c (n "sqlite-fastrand") (v "0.2.1") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "0jy8pahnv0cly4cb82fns1wiikz2a1cyqf8gm6favykl54dra37y")))

