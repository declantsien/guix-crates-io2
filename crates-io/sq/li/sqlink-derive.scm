(define-module (crates-io sq li sqlink-derive) #:use-module (crates-io))

(define-public crate-sqlink-derive-0.1.0 (c (n "sqlink-derive") (v "0.1.0") (d (list (d (n "sqlink") (r "^0.1.0") (d #t) (k 0)))) (h "1ivf7ii8qysz809nk3ybfcym5bp7m0iglnb28myqlyhzrjr9470z")))

(define-public crate-sqlink-derive-0.2.0 (c (n "sqlink-derive") (v "0.2.0") (d (list (d (n "sqlink") (r "^0.2.0") (d #t) (k 0)))) (h "0nqhqcbhpcjyd764vhi68489zihq2a7b6wvhsrw2mafm7v547hsv")))

(define-public crate-sqlink-derive-0.2.1 (c (n "sqlink-derive") (v "0.2.1") (d (list (d (n "sqlink") (r "^0.2.0") (d #t) (k 0)))) (h "0qks3l9znzaj7qyv8lz0fgqjfp4wl9r0jjjkz314jn6a21y2w383")))

(define-public crate-sqlink-derive-0.3.0 (c (n "sqlink-derive") (v "0.3.0") (d (list (d (n "sqlink") (r "^0.3.0") (d #t) (k 0)))) (h "0sw53ss0awjlg0vm2mv9579aglnn87vj02lzqikn4hc4ayfivzjv")))

(define-public crate-sqlink-derive-0.4.0 (c (n "sqlink-derive") (v "0.4.0") (d (list (d (n "sqlink") (r "^0.4.0") (d #t) (k 0)))) (h "1ckns7wpvab0bgg8r46b9c6jgs49dn00vz9ybr3cqy811699cy1j")))

