(define-module (crates-io sq li sqlite-parser-nom) #:use-module (crates-io))

(define-public crate-sqlite-parser-nom-0.1.0 (c (n "sqlite-parser-nom") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0qjd9fsvzbzg3cj40cclir83hp19sdq6pwahrr098yfhbq3rkcr5")))

(define-public crate-sqlite-parser-nom-0.1.1 (c (n "sqlite-parser-nom") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1crdcqm8g0937ldd4bs8rvhnij4kf30cw6gj05cig1sjci6lgxx1")))

(define-public crate-sqlite-parser-nom-1.0.0 (c (n "sqlite-parser-nom") (v "1.0.0") (d (list (d (n "memmap2") (r "^0.5.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1n1xwxy69025bzbs6csc0lh08ps9wx1iyn9lsbb62dpan0r4nrgc")))

