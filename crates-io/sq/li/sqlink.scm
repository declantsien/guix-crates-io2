(define-module (crates-io sq li sqlink) #:use-module (crates-io))

(define-public crate-sqlink-0.1.0 (c (n "sqlink") (v "0.1.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "1ml5cyv3vlpbd6qal6ibd8lygj4147zpnjnr73448yxaj9c758gp") (f (quote (("default"))))))

(define-public crate-sqlink-0.2.0 (c (n "sqlink") (v "0.2.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "0dk15xvq2h7blnmb2blglch6bxgni2cnhp22r9nldfpfwjpjkq46") (f (quote (("default"))))))

(define-public crate-sqlink-0.2.1 (c (n "sqlink") (v "0.2.1") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "0ns9dan7kkklqldxsla4nb6j223992jsdirfhf2h1wam6di4ffjs") (f (quote (("default"))))))

(define-public crate-sqlink-0.3.0 (c (n "sqlink") (v "0.3.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "0jn822fb6zvwwrynilr0fy4zwablylfkyqw5yk7x7afzi0qar545") (f (quote (("default"))))))

(define-public crate-sqlink-0.4.0 (c (n "sqlink") (v "0.4.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)))) (h "02kpir5kljqa02v5h4y6yfmqfxh8idz276b360n8mnxbbf292315") (f (quote (("default"))))))

(define-public crate-sqlink-0.5.0-alpha.2 (c (n "sqlink") (v "0.5.0-alpha.2") (d (list (d (n "postgres-types") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0d2iqh7k9f42bz6xn2av11iv76zg8m6qzag028qadsip0dl1307b") (f (quote (("default"))))))

(define-public crate-sqlink-0.6.0-alpha.2 (c (n "sqlink") (v "0.6.0-alpha.2") (d (list (d (n "postgres-types") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "1xf4hn9g8bniygcq4gxn0svlb9pcjhdlg36sxcn1sly15a93n8dd") (f (quote (("default"))))))

(define-public crate-sqlink-0.6.0 (c (n "sqlink") (v "0.6.0") (d (list (d (n "postgres-types") (r "^0.1") (d #t) (k 0)))) (h "1c8ar432ppxajqsb54q36nd4bhv267my1xzpzf2ihjkj8jgjwa5j") (f (quote (("default"))))))

