(define-module (crates-io sq li sqlite-jsonschema) #:use-module (crates-io))

(define-public crate-sqlite-jsonschema-0.2.3-alpha.6 (c (n "sqlite-jsonschema") (v "0.2.3-alpha.6") (d (list (d (n "jsonschema") (r "^0.16.1") (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "1mz1fm7yvx4hi70fah5w8wipmm6kl47v715zxijbn4w3917whxz9")))

(define-public crate-sqlite-jsonschema-0.2.3-alpha.7 (c (n "sqlite-jsonschema") (v "0.2.3-alpha.7") (d (list (d (n "jsonschema") (r "^0.16.1") (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "0988yc4v6i8f0shw4j163c401gcms7iqdz33m8gppamfys9nx27a")))

(define-public crate-sqlite-jsonschema-0.2.3 (c (n "sqlite-jsonschema") (v "0.2.3") (d (list (d (n "jsonschema") (r "^0.16.1") (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)))) (h "1x9jy8cplzk15hc7nvzg97b2zpjb243ivaxsaqvbk425s1fndfnl")))

