(define-module (crates-io sq li sqlite-loadable-macros) #:use-module (crates-io))

(define-public crate-sqlite-loadable-macros-0.0.1 (c (n "sqlite-loadable-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "00liadq10nvm6sx9qv3zf754w7j42yk9nc2ml4vbsyi99qxr4x20")))

(define-public crate-sqlite-loadable-macros-0.0.2 (c (n "sqlite-loadable-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "0j0yjy0s7vr6r7p52c4x4wbzm047fll4z8zy5im4zlkgimdcg2zr")))

(define-public crate-sqlite-loadable-macros-0.0.3 (c (n "sqlite-loadable-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "13syg98rlr046jfa5j2nzz4492w7mpx01xw3nxfng8hmc4wpl0wn")))

