(define-module (crates-io sq li sqlite3builder) #:use-module (crates-io))

(define-public crate-sqlite3builder-0.1.0 (c (n "sqlite3builder") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "0dzfl7rqc59j11xvfwqwp318bralzri5b7dd9h12r4ccl2rvvm1b")))

(define-public crate-sqlite3builder-0.1.1 (c (n "sqlite3builder") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "12sns7cb8lxwyj8m336k9578l07yh7rkw5rrwxjp3nvnvqnr5pbv")))

(define-public crate-sqlite3builder-0.1.2 (c (n "sqlite3builder") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "08dxf8wavywck72hwr3202xy8lfk2f53rbh25i6lcqqc8w9yrbfm")))

(define-public crate-sqlite3builder-0.1.3 (c (n "sqlite3builder") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sql-builder") (r "^0.1") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "002d1sf6fk3a4f154nd2r85n0clrjfsm0njl7sza7fvpilzybjxk")))

(define-public crate-sqlite3builder-0.1.4 (c (n "sqlite3builder") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sql-builder") (r "^0.1") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "0bx9rwpkrvnxh7n7prk49g5nrsfl93zcy5j7v25wpn8jlg17z7vv")))

(define-public crate-sqlite3builder-0.1.5 (c (n "sqlite3builder") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sql-builder") (r "^0.1") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "0qsx3n9f6nls0vvbnhd3x29zxnjzrdvr5wgaqkpn2y954y8az5gh")))

(define-public crate-sqlite3builder-0.2.0 (c (n "sqlite3builder") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sql-builder") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "1d7pppzm29a8dm73j3cq06cgx63mmza6327c4w2fxa9mnrzggvfd")))

(define-public crate-sqlite3builder-0.3.0 (c (n "sqlite3builder") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-sqlite3") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sql-builder") (r "^0.8") (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 0)))) (h "01bwpp2pl2gfnrhwssxsp16kw2abyys4zvsbn8glg94y38whdxh0")))

