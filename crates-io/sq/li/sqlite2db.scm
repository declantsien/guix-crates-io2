(define-module (crates-io sq li sqlite2db) #:use-module (crates-io))

(define-public crate-sqlite2db-0.0.0 (c (n "sqlite2db") (v "0.0.0") (h "1m9qdnxpaabh2q4h5w2sgsqmv0zb60d2rnbzisrzpc0ikbl7qy89")))

(define-public crate-sqlite2db-0.0.1 (c (n "sqlite2db") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1019gs6s4v28mf8r02dc10db5ypcgyhsa47nmq0a4yg3y0d6m5a7")))

