(define-module (crates-io sq li sqlite_playground) #:use-module (crates-io))

(define-public crate-sqlite_playground-0.1.0 (c (n "sqlite_playground") (v "0.1.0") (d (list (d (n "sqlite") (r "^0.31.1") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)))) (h "1zv0h83pwv3cny8f6z6h2md8q3ppqm6qk7yraivp3h1f6h2rlpni")))

