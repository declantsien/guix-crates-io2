(define-module (crates-io sq li sqlite-bindings-lunatic) #:use-module (crates-io))

(define-public crate-sqlite-bindings-lunatic-0.30.3 (c (n "sqlite-bindings-lunatic") (v "0.30.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0qy5cslx4lgfwj8nl3hkzf9bfp5wld7kxm9jpiv6hb20s3f33ppx") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-bindings-lunatic-0.30.4 (c (n "sqlite-bindings-lunatic") (v "0.30.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1h2c25nqry7p4j9dizj8aj6ddlkg1sz66w93al2r7119hg6bd022") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

