(define-module (crates-io sq li sqlite-glance) #:use-module (crates-io))

(define-public crate-sqlite-glance-0.1.0 (c (n "sqlite-glance") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (f (quote ("detect-tty" "detect-env"))) (d #t) (k 0)))) (h "19n98v4jzrhyd11x2hy8ycw52d2ak6d320fcr48ws82nfdr1d1nc")))

(define-public crate-sqlite-glance-0.2.0 (c (n "sqlite-glance") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "comfy-table") (r "^7.0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (f (quote ("detect-tty" "detect-env"))) (d #t) (k 0)))) (h "1d845cv0np5sq7mdjg08mbyqn510w5x6kqlf4kiymzf4asd2rsz9")))

(define-public crate-sqlite-glance-0.3.0 (c (n "sqlite-glance") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "comfy-table") (r "^7.0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "sqlparser") (r "^0.39.0") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (f (quote ("detect-tty" "detect-env"))) (d #t) (k 0)))) (h "16rza6grnznrp47w64zyi5hhn481bb4xcfl2bwwhhgwsyab2s5p4")))

(define-public crate-sqlite-glance-0.4.0 (c (n "sqlite-glance") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "comfy-table") (r "^7.0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "sqlparser") (r "^0.43.1") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (f (quote ("detect-tty" "detect-env"))) (d #t) (k 0)))) (h "0ddlrd7vamb32jxxwvp919npsv3dxxqkymxgq5csrk959f6zbdya")))

