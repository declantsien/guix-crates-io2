(define-module (crates-io sq li sqlite-ulid) #:use-module (crates-io))

(define-public crate-sqlite-ulid-0.2.1-alpha.14 (c (n "sqlite-ulid") (v "0.2.1-alpha.14") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "0dzak6kgc418bqrszcrf73r9acf2dn22l9mkrw7r8hj7y2q855v9")))

(define-public crate-sqlite-ulid-0.2.1-alpha.15 (c (n "sqlite-ulid") (v "0.2.1-alpha.15") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "0wiba14b2rxj2k2ps263k9l60ak5153kh2j1fy3zwnr5kj5hpvwq")))

(define-public crate-sqlite-ulid-0.2.1-alpha.17 (c (n "sqlite-ulid") (v "0.2.1-alpha.17") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "16ydi5y8lgjifycphbpbadjsm6rn53h48djsl4i2608l62s2184x")))

(define-public crate-sqlite-ulid-0.2.1-alpha.18 (c (n "sqlite-ulid") (v "0.2.1-alpha.18") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "0zaww1nwlgh1s827cf4sg6yrdh48g7isb635a5vgwr4hvrgqfx76")))

(define-public crate-sqlite-ulid-0.2.1-alpha.19 (c (n "sqlite-ulid") (v "0.2.1-alpha.19") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "17i0332xa9m9zhw7s5dmdlshv7zkn6ai6qzw1ahqfld52hg4lcn0")))

(define-public crate-sqlite-ulid-0.2.1 (c (n "sqlite-ulid") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "1ripfxn393swi8a68f5bamjnav7r80kk3y26j7aqdrhgxbhy6mn6")))

(define-public crate-sqlite-ulid-0.2.2-alpha.1 (c (n "sqlite-ulid") (v "0.2.2-alpha.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "sqlite-loadable") (r "^0.0.6-alpha.6") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "1dyfcix4nylmd5vmxg1426jdfhm6c9icq0z9cdw67ngjz98vj9xy")))

