(define-module (crates-io sq li sqlite) #:use-module (crates-io))

(define-public crate-sqlite-0.0.1 (c (n "sqlite") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)))) (h "1bmhqlq58x0gqkpcrs3kbbzmn90wyjigynmz7dyrjppv306c1dnv")))

(define-public crate-sqlite-0.1.0 (c (n "sqlite") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0k8fnyvy3j76gcg6asc9af1d858rd4h53vj3qrzaayqy9azkgglz")))

(define-public crate-sqlite-0.2.0 (c (n "sqlite") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0hf8chixyhv2c8skqwyqs636xz0zw8vs9vyfa7jvn4xxqb5kby30")))

(define-public crate-sqlite-0.2.1 (c (n "sqlite") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "10z6hl6yk05iw6dr2pf947d90cmr62h30qkcgxc3q2zkrkbl6bd3")))

(define-public crate-sqlite-0.2.2 (c (n "sqlite") (v "0.2.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1cs0lwpfb88izwjcppjf5wpajm367wd6sr663py8qls3vwiadx0v")))

(define-public crate-sqlite-0.3.0 (c (n "sqlite") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "118hr0n9m661pmh8pk23zxh5h3ak3cz1cb0c4817m870r555v4m3")))

(define-public crate-sqlite-0.4.0 (c (n "sqlite") (v "0.4.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1q5y9pxcpr5yx7rxkyklyyyczmc7kdnq64vma6npngxsw9sk1rmk")))

(define-public crate-sqlite-0.5.0 (c (n "sqlite") (v "0.5.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1gv9yfngn3ih5jd9d3xvzlvs1l8hbk5k23bc76x0hjz9j6qg5cdp")))

(define-public crate-sqlite-0.5.1 (c (n "sqlite") (v "0.5.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "14avnywj06di370xphgvpiqqq7r12m35krcfjvqs9vs7v7agaqqf")))

(define-public crate-sqlite-0.6.0 (c (n "sqlite") (v "0.6.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1rd5xhd75k2qijgsfp4kpcpixzj6a3z7qf2gj982vn050hrrcr3a")))

(define-public crate-sqlite-0.7.0 (c (n "sqlite") (v "0.7.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "126fip69p2jdkdi9v4s88h0nm1r0czkbjb859swl5f0ha5pzdidp")))

(define-public crate-sqlite-0.8.0 (c (n "sqlite") (v "0.8.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "18d2pfkp4i1gl0x7hyfwidifgi9fwp6sx520mc3qslba58xkxvjx")))

(define-public crate-sqlite-0.9.0 (c (n "sqlite") (v "0.9.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "16ila9y8myshgfxiqq3nd2ai12wwjxydrxg3ngkynxcxs8bl4zw3")))

(define-public crate-sqlite-0.9.1 (c (n "sqlite") (v "0.9.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0lki1g39mbhpr7q8h3ialr90m6yiqj3glwcvayzyypi5y9v0my18")))

(define-public crate-sqlite-0.10.0 (c (n "sqlite") (v "0.10.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "13q6ckq087hmbl9h7vfxplv6b1cphsf8fs3kdgn17l677kmlmvgj")))

(define-public crate-sqlite-0.11.0 (c (n "sqlite") (v "0.11.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0rx1ysvl2lg8yiv943qh41gdr8cq2dvg1iw2dkl77sy2y6yv4b7r") (f (quote (("edge" "sqlite3-sys/edge"))))))

(define-public crate-sqlite-0.12.0 (c (n "sqlite") (v "0.12.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1q5asxd2x3m9jkzbjvhvcial1630sidy609fn5mbwlbrh9lg5qw4") (f (quote (("edge" "sqlite3-sys/edge"))))))

(define-public crate-sqlite-0.13.0 (c (n "sqlite") (v "0.13.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1xippaabk34jvnbcf4fla07fxcm84pf3vd43m5ryq5iipf3hx5cd") (f (quote (("edge" "sqlite3-sys/edge"))))))

(define-public crate-sqlite-0.14.0 (c (n "sqlite") (v "0.14.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1q4c0db9kb1rm1r723gspr87zgikgjdnz0px2q88d7nd3lqfssc6") (f (quote (("edge" "sqlite3-sys/edge"))))))

(define-public crate-sqlite-0.14.1 (c (n "sqlite") (v "0.14.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "05d7xniwiq9qfy1zyxpnhp42yfi6v6lvba3q0sqkpj9vjys33ssk") (f (quote (("edge" "sqlite3-sys/edge"))))))

(define-public crate-sqlite-0.14.2 (c (n "sqlite") (v "0.14.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "037r31a2cs4xk0ybb89ijggs19m40miilj4jkd3wd7dqq1kkd2jd") (f (quote (("edge" "sqlite3-sys/edge"))))))

(define-public crate-sqlite-0.15.0 (c (n "sqlite") (v "0.15.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "17v3k4rpg9fzjhxdxzlsf18w2bsp2x8g8n2zvjar15q9ipl20053")))

(define-public crate-sqlite-0.16.0 (c (n "sqlite") (v "0.16.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0clxlv6q909gd8qz719p881cmf94l7fwcgjp70lx9vcc3na2jh7j")))

(define-public crate-sqlite-0.17.0 (c (n "sqlite") (v "0.17.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1b203806j0d1pn4sx9s9aqr0zclz4vr5dacks84cv0v3504b94hi")))

(define-public crate-sqlite-0.17.1 (c (n "sqlite") (v "0.17.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "06kg1zlmrhnp799f5k2ddqc0ikj89plibj2v8vrrhvrjwvn1cxph")))

(define-public crate-sqlite-0.17.2 (c (n "sqlite") (v "0.17.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1dl8lx1sgq28swi1cqmaymwbng69l4q9hhn0cs4di6sbqv3fj1pi")))

(define-public crate-sqlite-0.18.0 (c (n "sqlite") (v "0.18.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "02x1k2ma9cdqmmhsp6fpf600an19d2c05wm69fzdlgwsdcwbpxci")))

(define-public crate-sqlite-0.19.0 (c (n "sqlite") (v "0.19.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0skxr68njslnrwz9rkpc3k1lhb0fk434s5gi38mdrskjaysvbli7")))

(define-public crate-sqlite-0.19.1 (c (n "sqlite") (v "0.19.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1l34y35mkh149fzwsbsgb50wvxf1s82flzra01hbq340476cy6sz")))

(define-public crate-sqlite-0.19.2 (c (n "sqlite") (v "0.19.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0pvf6j13zfnjajphryb8kz9a51604qi0kf715vbp6xz9fw1p0j7a")))

(define-public crate-sqlite-0.19.3 (c (n "sqlite") (v "0.19.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0kabn7h0bjsm939gi7ax2l998pz6gn7wb0xxqxqaj1i4azx8z2rk")))

(define-public crate-sqlite-0.19.4 (c (n "sqlite") (v "0.19.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "06fwnwm2k8fcmw821ap5zgbcyi4i04fq61spy0wbdyq7hfkfj1ac")))

(define-public crate-sqlite-0.19.5 (c (n "sqlite") (v "0.19.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1s9mjhjf85hfdl60kfghz42g5q08bfmplxmmwzszb8j2wk3wmpia")))

(define-public crate-sqlite-0.19.6 (c (n "sqlite") (v "0.19.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "17y6dapdsdxyl8swhcxd2w24bdqd2rr5v0pzkdnyqwrzzcklq3v5")))

(define-public crate-sqlite-0.19.7 (c (n "sqlite") (v "0.19.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "1xyjllkad3nha0ym432w57shv7i3ixjgyqha3yivccjzqd196451")))

(define-public crate-sqlite-0.19.8 (c (n "sqlite") (v "0.19.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "02ffvj538l03d2gyv4jqcxaiqmvg98k1ppfjpr39h652fcw8dkj3")))

(define-public crate-sqlite-0.20.0 (c (n "sqlite") (v "0.20.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.6") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "0spfvfc8j15160g69ai8jfiabj3ks059kldzcai8pnxsr3z6rnwp")))

(define-public crate-sqlite-0.21.0 (c (n "sqlite") (v "0.21.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.7") (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "0k2b9z98rw2i71rnx9mndds6ax151rw2sq82gs68d03l8wd1c9c7") (f (quote (("standard" "sqlite3-sys/standard") ("sqlcipher" "sqlite3-sys/sqlcipher") ("default" "standard"))))))

(define-public crate-sqlite-0.21.1 (c (n "sqlite") (v "0.21.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.9") (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "1a7xv5x5jalc0acp03ardq7dip9iqdj9imxwlf7b8pj5zpkcm5md") (f (quote (("standard" "sqlite3-sys/standard") ("sqlcipher" "sqlite3-sys/sqlcipher") ("default" "standard"))))))

(define-public crate-sqlite-0.21.2 (c (n "sqlite") (v "0.21.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.9") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1rp60rsscx18r52r9ap2rsnhs9w65vbggwnai130qqjasa85crzi") (f (quote (("standard" "sqlite3-sys/standard") ("sqlcipher" "sqlite3-sys/sqlcipher") ("default" "standard"))))))

(define-public crate-sqlite-0.22.0 (c (n "sqlite") (v "0.22.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.10") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1swsa4ibh86q5bmgd91jymxnw5qrl0i4qlvc5bx28bzchcvkkhr3") (f (quote (("standard" "sqlite3-sys/standard") ("sqlcipher" "sqlite3-sys/sqlcipher") ("default" "standard"))))))

(define-public crate-sqlite-0.23.0 (c (n "sqlite") (v "0.23.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.11") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0c98llkw6y092ssl3nvyizzm71dikxgp6xca68sa2ylvd7lbmc5r") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.1 (c (n "sqlite") (v "0.23.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.11") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "15mc4a0i5nbw4z7pd8q4c8cir6s6baynw4a0mw4ykpi4i62vsdn8") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.2 (c (n "sqlite") (v "0.23.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.11") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0laiyaq58hpb5mxfnjad8dmvs5nvg1yb3lspa4iynrll7xbj74dc") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.3 (c (n "sqlite") (v "0.23.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.11") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "05rpz0kdcyrc7rg8h16kbpa93465cj27x2whf5y374y1m9x167dv") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.4 (c (n "sqlite") (v "0.23.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0r52sx0pz3vyy18vww58h7qnh8qg33cp6gs02d9v2acj1pw2gkd3") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.5 (c (n "sqlite") (v "0.23.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0439my4w5qj3jr80sn3cbrxxpdky9ih53i601rvhqagkha1im51n") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.6 (c (n "sqlite") (v "0.23.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0brib5z7f82wzglj52izl476izi5q0my51w9vfdzvsaip7blgpsh") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage")))) (y #t)))

(define-public crate-sqlite-0.23.7 (c (n "sqlite") (v "0.23.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1vh7y4l7bdphlg2a3qdjgfvminnlb7fzzh016l5s2q09xsfza89h") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.8 (c (n "sqlite") (v "0.23.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "19jw77smqq9z458g869c5xpk6r93qhr8knbhi310l4yh4bn5h7rk") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.23.9 (c (n "sqlite") (v "0.23.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "09mndphn5hrnn50sidiqcxhikx4z1yd3jnf2kpyyfdl34w87p3fi") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.24.0 (c (n "sqlite") (v "0.24.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0fvznx22da7ycsv5qinvs0pzqma59pipl8gh3jrdv0irw25cvgf3") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.24.1 (c (n "sqlite") (v "0.24.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0np92r6karjbffqhi82lys7qvg51k9m3y2x96bppdq8qga7048al") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.25.0 (c (n "sqlite") (v "0.25.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "16z8iw8jqlwhk0v3zwlgmbw4sdzavinbq0s573k3lj2djiflsp0z") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.25.1 (c (n "sqlite") (v "0.25.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1z42nf21xrxq9n5xa3r4hxjrh9kg4ibbby4g8rk0wjmidq6c5sbr") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.25.2 (c (n "sqlite") (v "0.25.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1nb1xbn1pyg68hk9s8s4lka5v7s6c5xc9nx1jrr0j5hks99wz184") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.25.3 (c (n "sqlite") (v "0.25.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0ccyrg4wsvyd7xj26yh7qmh66h9nj6m8gni71bfiwgip5vf5kxrm") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.26.0 (c (n "sqlite") (v "0.26.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.13") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0snvg09bs0n8skcxkx52lcymdn0l130a2m8fpvxpdhkyq0sabc9z") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.27.0 (c (n "sqlite") (v "0.27.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "131y2xanw4qcspvqdk46619g70j10bljpbxa1malhl38apfqxpz2") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.27.1 (c (n "sqlite") (v "0.27.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1yaglz511n8ni5njnk3s6nvd590wbxpf12sa4fgmddqv1z3fvy7p") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.27.2 (c (n "sqlite") (v "0.27.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0vykm2xjw73w8a9s4zz8554163d2498i11050ic17aqh3n8r6n9w") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.27.3 (c (n "sqlite") (v "0.27.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "11f1fw5gffni7mqr6mrliacr86v0yg9zmgvj3lhfdv1iz54vjv76") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.28.0 (c (n "sqlite") (v "0.28.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "10zhxy07siz6vh19mjhv90l40231fwzn7hxnjp42zvi8aiplqnqj") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.28.1 (c (n "sqlite") (v "0.28.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "05xp4wzynxkdg28qvmwdzvvkr71y5f22fwnlj8b18irjbk6cyz9d") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.29.0 (c (n "sqlite") (v "0.29.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "01x67kks7caz3gw2hn9m8rchqppqssj20j953jrayfzqzw6siz2q") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.30.0 (c (n "sqlite") (v "0.30.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "11xq5qr2r232dpj1680ipdd5sg208m0pqvgpzlm0l4xqm65ck3cy") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.30.1 (c (n "sqlite") (v "0.30.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1xzmg1ynxzg4pqk58gzg9rpx3c8rsp8yamx23maalbchf1j7ibpa") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.30.2 (c (n "sqlite") (v "0.30.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "05x7kazpbvp5gjzgqqi691kfap3f87v0bnkqhhr7wxcyd3jqqmfx") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.30.3 (c (n "sqlite") (v "0.30.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0j1d6z2in5v8k2fq5bmkkwzm1swg6iv4cn69x7jkz6xqbz5p5q0j") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.30.4 (c (n "sqlite") (v "0.30.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.14") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "07anx4wxb0rczbvl9gzmi8ryr5m1a96k8cdmwlw1mhii85k0h6cb") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.30.5 (c (n "sqlite") (v "0.30.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.15") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1lbfa0gjkqlhcmj4jy72kzfgd6a57z8gs1y7g34cbp4msvm4rk3f") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-sqlite-0.31.0 (c (n "sqlite") (v "0.31.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.15") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1bc64898nvpr0c8bi53c48n9jkbq4xa8160jnfimg8k9qijdmpgk") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage") ("bundled" "sqlite3-sys/bundled"))))))

(define-public crate-sqlite-0.31.1 (c (n "sqlite") (v "0.31.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.15") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1214z4d6mv0gj1gf28ghil8dxbb8yf8wdd6iyqw0pkm0myvrshq5") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage") ("bundled" "sqlite3-sys/bundled"))))))

(define-public crate-sqlite-0.32.0 (c (n "sqlite") (v "0.32.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.15") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1rpqpkpxn2qdvghsnak2b73cn5ca37p6ri0ylyjdcmrq3481r003") (f (quote (("linkage" "sqlite3-sys/linkage") ("extension") ("default" "linkage") ("bundled" "sqlite3-sys/bundled"))))))

(define-public crate-sqlite-0.33.0 (c (n "sqlite") (v "0.33.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.16") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1qq5wqqvxl6790qar2lgxzr7mix4vqzn3dn3vp7vfm5hwmp7zabq") (f (quote (("linkage" "sqlite3-sys/linkage") ("extension") ("default" "linkage") ("bundled" "sqlite3-sys/bundled"))))))

(define-public crate-sqlite-0.34.0 (c (n "sqlite") (v "0.34.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.16") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "16gbb4y5nxxqdigfc3qpwfhdi1y4x6k25akq4wzg66msvvd2mx0v") (f (quote (("linkage" "sqlite3-sys/linkage") ("extension") ("default" "linkage") ("bundled" "sqlite3-sys/bundled"))))))

(define-public crate-sqlite-0.35.0 (c (n "sqlite") (v "0.35.0") (d (list (d (n "sqlite3-sys") (r "^0.17") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "179knfc4mppz56hj0mhzhqyp68y7wsqiw2x5s0l9djjphn9rj0k4") (f (quote (("linkage" "sqlite3-sys/linkage") ("extension") ("default" "linkage") ("bundled" "sqlite3-sys/bundled"))))))

(define-public crate-sqlite-0.36.0 (c (n "sqlite") (v "0.36.0") (d (list (d (n "sqlite3-sys") (r "^0.17") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "1cjcd3wr00larc8irjqil71f7lxw9ia5mrrzznbrgg3816d5lki7") (f (quote (("linkage" "sqlite3-sys/linkage") ("extension") ("encryption" "sqlite3-sys/encryption") ("default" "linkage") ("bundled" "sqlite3-sys/bundled"))))))

