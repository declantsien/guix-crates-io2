(define-module (crates-io sq li sqlite2dir) #:use-module (crates-io))

(define-public crate-sqlite2dir-0.1.0 (c (n "sqlite2dir") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "git2") (r "^0.10.0") (k 0)) (d (n "rusqlite") (r "^0.20.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (k 0)))) (h "0lfpb209xs7xppyf3i46g0nwazjsw5w4ni9hmx6js9qf7420xjyv") (f (quote (("vendored-sqlite" "rusqlite/bundled"))))))

(define-public crate-sqlite2dir-0.2.0 (c (n "sqlite2dir") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "git2") (r "^0.10.0") (k 0)) (d (n "once_cell") (r "^1.1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.20.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (k 0)))) (h "1n8lwymmj7hv3irhw0dc58h4nkmh7wmhvg5rkqymn5ckrvci9bhs") (f (quote (("vendored-sqlite" "rusqlite/bundled"))))))

