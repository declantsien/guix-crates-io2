(define-module (crates-io sq li sqlite_varint) #:use-module (crates-io))

(define-public crate-sqlite_varint-0.1.0 (c (n "sqlite_varint") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1hwwsg2d7f2gg7bl81r7zx6vravhlns9nasz1dmk807sh4396zyv") (y #t)))

(define-public crate-sqlite_varint-0.1.1 (c (n "sqlite_varint") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1hcpp64i7xqsyfv7n69wx4vx05qk4if8331vpb0ldzl0k4l7i28p") (y #t)))

(define-public crate-sqlite_varint-0.1.2 (c (n "sqlite_varint") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1z6gm0a28y40pqpsmv1hlqv31773wkb226n0w818xnms1vkq98nn")))

