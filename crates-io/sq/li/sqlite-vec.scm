(define-module (crates-io sq li sqlite-vec) #:use-module (crates-io))

(define-public crate-sqlite-vec-0.0.1-alpha.4 (c (n "sqlite-vec") (v "0.0.1-alpha.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 2)))) (h "170ixqk983xn4paasqy97mhw7j0jsz6k0xsdgzd5pn3v8b84m75s")))

(define-public crate-sqlite-vec-0.0.1-alpha.5 (c (n "sqlite-vec") (v "0.0.1-alpha.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 2)))) (h "089bp570s63z2zi7xkqi76bww9npnkhffahgh7bxa4qbk8pf1z5r")))

(define-public crate-sqlite-vec-0.0.1-alpha.6 (c (n "sqlite-vec") (v "0.0.1-alpha.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 2)))) (h "06rzjrdynnnb9hakdgz8hw9xf57hs6bh2ahwrlq6w4x5g7dqhyil")))

(define-public crate-sqlite-vec-0.0.1-alpha.7 (c (n "sqlite-vec") (v "0.0.1-alpha.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 2)))) (h "0cx4qry3vhnd58p3b7xqzivpsf14n1pvv18qlmwrdfjqdskvb8wb")))

(define-public crate-sqlite-vec-0.0.1-alpha.8 (c (n "sqlite-vec") (v "0.0.1-alpha.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 2)))) (h "04nybw13rg67p7xxjg70w35wl685c4i238mkh1sg756d9krn2l9r")))

(define-public crate-sqlite-vec-0.0.1-alpha.9 (c (n "sqlite-vec") (v "0.0.1-alpha.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 2)))) (h "1y7217a7yj85v295sfvygvzz48ihdla9v9llsc86xiz0rw6xypip")))

(define-public crate-sqlite-vec-0.0.1-alpha.10 (c (n "sqlite-vec") (v "0.0.1-alpha.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rusqlite") (r "^0.31.0") (d #t) (k 2)))) (h "0avhwvb4kj548dccyvi6m85z3a0gm36qp7m3ysr2jqjnpmf7fr6d")))

