(define-module (crates-io sq li sqlite-rs) #:use-module (crates-io))

(define-public crate-sqlite-rs-0.1.0 (c (n "sqlite-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hexyl") (r "^0.13") (d #t) (k 2)))) (h "0axlrq3lb22rsvyvv9lx7dzbgzqqk9h0iqjrjj1jwfzkl6pfkhsf") (y #t) (r "1.71")))

(define-public crate-sqlite-rs-0.1.1 (c (n "sqlite-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hexyl") (r "^0.13") (d #t) (k 2)))) (h "1a7gckarlkf7s7w0vxxxn8j6xxfaz2n9vb2fn6xhgfy8mpzhcqqj") (y #t) (r "1.71")))

(define-public crate-sqlite-rs-0.1.2 (c (n "sqlite-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "hexyl") (r "^0.13.1") (d #t) (k 2)))) (h "0p7k0wl4rapizkb0l0h0xw9hkpzyf0939rhjglvdjgydnsn0rv6j") (r "1.71")))

(define-public crate-sqlite-rs-0.1.3 (c (n "sqlite-rs") (v "0.1.3") (d (list (d (n "hexyl") (r "^0.13.1") (d #t) (k 2)))) (h "1jyq3i1nq648f96jw5xg8bsw479gmw3k2csi2ch196d00x576k34") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t) (r "1.72")))

(define-public crate-sqlite-rs-0.1.4 (c (n "sqlite-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "hexyl") (r "^0.13.1") (d #t) (k 2)))) (h "0w7iqsbm935zikdb5vi5apx9y18fb5wil0532g9j90qlpgm9bllm") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.72")))

(define-public crate-sqlite-rs-0.1.5 (c (n "sqlite-rs") (v "0.1.5") (h "1z4vrcb9yc5cn7ial8z2pqphwgisqwrwdkg1qk2r6lwwlqqlca2x") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.72")))

(define-public crate-sqlite-rs-0.1.6 (c (n "sqlite-rs") (v "0.1.6") (h "03ybl53szhh6zcv4nnjjzdvdzk3x5s450gz65wphrxy095s96c7v") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.72")))

(define-public crate-sqlite-rs-0.1.7 (c (n "sqlite-rs") (v "0.1.7") (h "048c7inw95ysf2z7sz3c1b2vbx8m2kqkp79lp8r57qj9fz0wv2bb") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.72")))

(define-public crate-sqlite-rs-0.1.8 (c (n "sqlite-rs") (v "0.1.8") (h "1qnh7sphdpg6n1z8vskdq7ng1jfhhf7r3xjlwsnw004wrv06fzqm") (f (quote (("std") ("default" "std")))) (r "1.72")))

(define-public crate-sqlite-rs-0.2.0 (c (n "sqlite-rs") (v "0.2.0") (h "0ryxzlzv4mfqhmyzdpc7bp0qw8v42aqcb5jsmxq46cyxw4jvbqh2") (r "1.74.1")))

(define-public crate-sqlite-rs-0.2.1 (c (n "sqlite-rs") (v "0.2.1") (h "1z5hjp6y4pzzn48nklzvm7yxczn9nq50i2q5alh9rxrlv12n8ifp") (f (quote (("log") ("default" "log")))) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.0 (c (n "sqlite-rs") (v "0.3.0") (h "06x3av7id5wfgnm7i1dbz0ydrzrhvi5gpasc0dmhji88q0zfxv4n") (f (quote (("log") ("default" "log")))) (y #t) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.1 (c (n "sqlite-rs") (v "0.3.1") (h "1gbzkjk714pfl15islakp5si202857kvgq7nlaq6jk5cvy50wnmn") (f (quote (("log") ("default" "log")))) (y #t) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.2 (c (n "sqlite-rs") (v "0.3.2") (h "1m465xbkgp4sfvdnp5kkx18w24xja65aid33svp3nwy9sxgr12cd") (f (quote (("log") ("default" "log")))) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.3 (c (n "sqlite-rs") (v "0.3.3") (h "08kh3q7rrzr0mmby1r33z700v404xs29ggvz5bvh47b9iznjb8ab") (f (quote (("log") ("default" "log")))) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.4 (c (n "sqlite-rs") (v "0.3.4") (h "19y1c29bnm6n9zc14l3c2v68sshj1xgrcnq4ryzhiwg2nxzvrh1y") (f (quote (("log") ("default" "log")))) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.5 (c (n "sqlite-rs") (v "0.3.5") (h "1g43ifdgzs38wwlsw8v4w6mrv12l75ip0f4avasj4vgdyqx3qfkr") (f (quote (("log") ("default" "log")))) (y #t) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.6 (c (n "sqlite-rs") (v "0.3.6") (h "1vpdqa2pshvil5xcqrfl2jgd6dn60sprsfw4inznmg80klvwdnhg") (f (quote (("log") ("default" "log")))) (r "1.74.1")))

(define-public crate-sqlite-rs-0.3.7 (c (n "sqlite-rs") (v "0.3.7") (h "1kba1yalgcac6b1nwlcjagmxf3i66jw7f2ch7j8vvc5hm9bhgf47") (f (quote (("log") ("default" "log")))) (r "1.74.1")))

