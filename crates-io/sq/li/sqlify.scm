(define-module (crates-io sq li sqlify) #:use-module (crates-io))

(define-public crate-sqlify-0.1.0 (c (n "sqlify") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)))) (h "0zf4bwy4389hgrp7vm6fmnb1zkpj9klnif9qfxr0gaki7h1s8n47")))

(define-public crate-sqlify-0.1.1 (c (n "sqlify") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlformat") (r "^0.2.1") (d #t) (k 0)))) (h "11mr69axrwn2n7z06xgfq7zhbndlp944sh4yxdi7dzs5n4v5nhv3")))

