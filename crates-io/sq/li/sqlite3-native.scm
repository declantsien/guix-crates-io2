(define-module (crates-io sq li sqlite3-native) #:use-module (crates-io))

(define-public crate-sqlite3-native-0.1.0 (c (n "sqlite3-native") (v "0.1.0") (d (list (d (n "c2rust-bitfields") (r "^0.3.0") (d #t) (k 0)) (d (n "f128") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "093hwyxv8hv3s2lg3hw29x08qiylmmly0yfakmc7h9147k1493w2")))

