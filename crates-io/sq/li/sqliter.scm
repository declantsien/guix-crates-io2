(define-module (crates-io sq li sqliter) #:use-module (crates-io))

(define-public crate-sqliter-0.0.1 (c (n "sqliter") (v "0.0.1") (d (list (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1s53qjvv7792hsp4vgmai374a01x6h12m1abild9srfn4fp855b5")))

(define-public crate-sqliter-0.1.0 (c (n "sqliter") (v "0.1.0") (d (list (d (n "async-rusqlite") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "06q2wcri7ib5rychrgqd0yz18g03623jah5nydafaiazyrc0vh1l")))

(define-public crate-sqliter-0.1.1 (c (n "sqliter") (v "0.1.1") (d (list (d (n "async-rusqlite") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "19kcgcjhwm8nwblmqv5hbcg2dhx7k3jmppyw9qhddsj77s7sjaic")))

(define-public crate-sqliter-0.2.0 (c (n "sqliter") (v "0.2.0") (d (list (d (n "async-rusqlite") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0q7jj1dckbjrqagpawbwblyv5yisib51v31sjx6xkkb2b6pa2pcf")))

(define-public crate-sqliter-0.3.0 (c (n "sqliter") (v "0.3.0") (d (list (d (n "async-rusqlite") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1lhw9nzym5ck2hy401nbi4lr1khajsggwwz1jqy9cx2b89r43k7i")))

(define-public crate-sqliter-0.4.0 (c (n "sqliter") (v "0.4.0") (d (list (d (n "async-rusqlite") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1vvm9yy51960dp95qbzqyrnvcnp865dz9gkj3p7bq1p5kf7n1006")))

(define-public crate-sqliter-0.5.0 (c (n "sqliter") (v "0.5.0") (d (list (d (n "async-rusqlite") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "07gghzrr345qms2bb4w2gyq5lkdfs48pkn4h13f5zbnpafq5hnwb")))

(define-public crate-sqliter-0.5.1 (c (n "sqliter") (v "0.5.1") (d (list (d (n "async-rusqlite") (r "^0.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0d0wiaihz6zqr10h5076q1sxrq3lkld4445r4j8w6ilg9ganagc5")))

