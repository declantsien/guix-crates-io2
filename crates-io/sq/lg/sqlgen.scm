(define-module (crates-io sq lg sqlgen) #:use-module (crates-io))

(define-public crate-sqlgen-0.1.0 (c (n "sqlgen") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0rsvgrbbj252jqymbvxgwakpy7wdw8fhbfw1zkm9919rfrk759l5")))

(define-public crate-sqlgen-0.1.1 (c (n "sqlgen") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "01wfh77w26ga01lqrdsgd08fr7j04jkwc8gv7c4s98wwc3m8jqpb")))

(define-public crate-sqlgen-0.1.2 (c (n "sqlgen") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0b7ia674zs1ll4pcs6ryrxnfqwx1ndhfcxmq4znnxw1z08kzy641")))

(define-public crate-sqlgen-0.1.3 (c (n "sqlgen") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1yn0p9xvn8yqr6c8z5rqgyk04a3sxx3zjdcl3jr38613ag9ay8sq")))

(define-public crate-sqlgen-0.1.4 (c (n "sqlgen") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1lvbj00f6ak84xbrca5l0rz8srph6bvv4pyhrx3pqhg13qjsckad")))

(define-public crate-sqlgen-0.1.5 (c (n "sqlgen") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1pbc1da14wir2jqknjaycdd1f0swpqrmdn08525bxbfnbs22hm0z") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-sqlgen-0.1.6 (c (n "sqlgen") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1yan7bygl5r1frmjmva8l6d7ksarljhpzl0lrycbfia4yi35n5fp") (s 2) (e (quote (("serde" "dep:serde"))))))

