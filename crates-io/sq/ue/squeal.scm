(define-module (crates-io sq ue squeal) #:use-module (crates-io))

(define-public crate-squeal-0.0.1 (c (n "squeal") (v "0.0.1") (h "0g9yq61rvwk3v2a14arbws4vcpfjl8va1zg8ywd8r81hxih0lim7")))

(define-public crate-squeal-0.0.2 (c (n "squeal") (v "0.0.2") (h "1db2dfysda5wymrwa2hzklz2d6h4aikcm3mywkvgw77k2i6i128j")))

(define-public crate-squeal-0.0.3 (c (n "squeal") (v "0.0.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0apsiqblfg3zwxc45vpfm78aj3xi1ls2ns8qpa8l2mhjwznxc659")))

(define-public crate-squeal-0.0.4 (c (n "squeal") (v "0.0.4") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1zw1lcv70vdw5parjmlq3hp2d17gp2xgdn40aqkdhpk3hggskmf5")))

(define-public crate-squeal-0.0.5 (c (n "squeal") (v "0.0.5") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "testcontainers") (r "^0.15") (d #t) (k 2)) (d (n "testcontainers-modules") (r "^0.2.1") (f (quote ("postgres"))) (d #t) (k 2)))) (h "1x03fxlbp5wq0vpr3l32v0xd2gnzqy5s03hdn6z7b1dgfq8idd3b") (f (quote (("postgres-docker")))) (r "1.75.0")))

(define-public crate-squeal-0.0.6 (c (n "squeal") (v "0.0.6") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "postgres") (r "^0.19.7") (d #t) (k 0)) (d (n "testcontainers") (r "^0.15") (d #t) (k 2)) (d (n "testcontainers-modules") (r "^0.2.1") (f (quote ("postgres"))) (d #t) (k 2)))) (h "0ar04hjbmk2ybfdx50qcbcb1chzrrismz5hx7rzcwga52blgp389") (f (quote (("postgres-docker")))) (r "1.75.0")))

