(define-module (crates-io sq ue squeue) #:use-module (crates-io))

(define-public crate-squeue-0.10.0 (c (n "squeue") (v "0.10.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bfpla9nz5sk4ibcbqh3gdjvsc0zb0snaj1rdl476pcas0vwrih4") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

(define-public crate-squeue-0.10.1 (c (n "squeue") (v "0.10.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10d9myanjgpn21xrbcxcsfhfxrjcjmmaw51kislyj5bhmvhi2kk7") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

(define-public crate-squeue-0.10.2 (c (n "squeue") (v "0.10.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10ycmngn2v70pzd91pvwipkrl2566kcdnnz2mxz3mq0kb133ws0z") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

(define-public crate-squeue-0.10.3 (c (n "squeue") (v "0.10.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pglgrl3wlii3b529dsrxc0kh9p42nkw2nnniw1xjwi6qp6ka639") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

