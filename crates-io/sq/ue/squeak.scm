(define-module (crates-io sq ue squeak) #:use-module (crates-io))

(define-public crate-squeak-0.1.0 (c (n "squeak") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)))) (h "0ghpk9xnx10mqhpafibpx5n7r3s3aic92rd3ggxdxm31452wsxw8") (r "1.60.0")))

(define-public crate-squeak-0.2.0 (c (n "squeak") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)))) (h "1gqzqm78gp80hb2p9bf4chf5q674sp0plmp94bw2bqf1hqhgwjjl") (r "1.60.0")))

