(define-module (crates-io sq la sqlant) #:use-module (crates-io))

(define-public crate-sqlant-0.1.0 (c (n "sqlant") (v "0.1.0") (d (list (d (n "postgres") (r "^0.19.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "05gv922918745mr9l2m63nh4w15qxn2v7l4lz3fwy5dxiy5d3wa4")))

(define-public crate-sqlant-0.1.1 (c (n "sqlant") (v "0.1.1") (d (list (d (n "postgres") (r "^0.19.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "0n3i5p1y1a8km2by6yxgmw0xhzbkzvwv3y8q4di133884fp2i9sh")))

(define-public crate-sqlant-0.1.2 (c (n "sqlant") (v "0.1.2") (d (list (d (n "postgres") (r "^0.19.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "125pl3sh2kw27vhyk7w9z2fd231j21559nlrs4yrs3mpn8mz32g8")))

(define-public crate-sqlant-0.1.3 (c (n "sqlant") (v "0.1.3") (d (list (d (n "postgres") (r "^0.19.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "16jjf5bq56rdjzfjqivingarkdflnlyd28a7ciapfz5jnjlp8bqd")))

(define-public crate-sqlant-0.1.4 (c (n "sqlant") (v "0.1.4") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "0pndbnwn8w87yj05sx024iyi9aw9pvaa1lgclrzsvclcigj42syg")))

(define-public crate-sqlant-0.1.5 (c (n "sqlant") (v "0.1.5") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "0z21qv551vqnp62fpzad3wna5lk6wklgih53nbb1gh0ynzjnmnjh")))

(define-public crate-sqlant-0.2.0 (c (n "sqlant") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "00zdvd2n51l1rbws0pzxcxg1haw475qd5iyyqh11q53d4cdgn49l")))

(define-public crate-sqlant-0.3.0 (c (n "sqlant") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)))) (h "1anf1xnw0z32k2vzl45plhnz7qjy4i5y5iqka1a0y43wql4bajqr")))

(define-public crate-sqlant-0.3.1 (c (n "sqlant") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "157mhb97aijzmskxf6vascz166miwlny5m4h1h4fjwsw8g3wfwwk")))

