(define-module (crates-io sq la sqlarfs-cli) #:use-module (crates-io))

(define-public crate-sqlarfs-cli-0.1.0 (c (n "sqlarfs-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "sqlarfs") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "xpct") (r "^0.5.1") (d #t) (k 2)))) (h "1w4zs9nbxhgfnzgfkgcvkb129rlvfm4mgdzl3yic2z1l0xlyznpa") (r "1.75.0")))

