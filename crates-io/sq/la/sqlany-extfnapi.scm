(define-module (crates-io sq la sqlany-extfnapi) #:use-module (crates-io))

(define-public crate-sqlany-extfnapi-0.1.0 (c (n "sqlany-extfnapi") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1wqyvc3a5yrqik7qf86qlh74942dba65sf2vagdrj0r8f6j7hkak")))

(define-public crate-sqlany-extfnapi-0.2.0 (c (n "sqlany-extfnapi") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wwfbr33gc0nklq2j8hi0hrgjlhf0q5p4wq4kqcasb1bqm9nky2q")))

(define-public crate-sqlany-extfnapi-0.2.1 (c (n "sqlany-extfnapi") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jr42y0gswxx2jxnr42qprirvazqs4idl1xs1v47czyh70n8hljl")))

