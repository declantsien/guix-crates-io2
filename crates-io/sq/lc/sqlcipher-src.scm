(define-module (crates-io sq lc sqlcipher-src) #:use-module (crates-io))

(define-public crate-sqlcipher-src-0.1.0 (c (n "sqlcipher-src") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11x5frqw548y95pjbl0wd8q8jsxwj6qbdysaifi9421s4k4kaiiv")))

(define-public crate-sqlcipher-src-0.1.1 (c (n "sqlcipher-src") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ydv8yddipmd8wcqkbhy16gmf84jjznvplpwz6rrzks9a1j04rib")))

