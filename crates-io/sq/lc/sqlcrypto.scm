(define-module (crates-io sq lc sqlcrypto) #:use-module (crates-io))

(define-public crate-sqlcrypto-0.0.1 (c (n "sqlcrypto") (v "0.0.1") (d (list (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)))) (h "0azzfr0vm804vxfmh386glpjkqlllps27flj9b29f6iqmx45j9jn") (y #t)))

(define-public crate-sqlcrypto-0.0.2 (c (n "sqlcrypto") (v "0.0.2") (d (list (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)))) (h "0sp4njand2llrp50y46z5xg8x9a3fkxavygg586yjkzxic3qr8qr") (y #t)))

(define-public crate-sqlcrypto-0.0.4 (c (n "sqlcrypto") (v "0.0.4") (d (list (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.12") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)))) (h "0qr1nan9rfg4lj8b4rb8glcaf1hmapkprzbv03lgh6qi77g1byd5") (y #t)))

(define-public crate-sqlcrypto-0.0.5 (c (n "sqlcrypto") (v "0.0.5") (d (list (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)))) (h "0v09rzsb4lz8cd8ip019cxc51lags6qvzpzjb82vbskhwg6vmxbj") (y #t)))

(define-public crate-sqlcrypto-0.1.0 (c (n "sqlcrypto") (v "0.1.0") (d (list (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)))) (h "06dwdaxmzkpiazs929vn0whg8yz28wpxz4axq9hf7f2anz8vs4nq") (y #t)))

(define-public crate-sqlcrypto-0.1.1 (c (n "sqlcrypto") (v "0.1.1") (d (list (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)))) (h "127b1f64fzmnhw8hcx424gk1d4s0zbbyw5i8xbfxnhrv6j2bqryk") (y #t)))

(define-public crate-sqlcrypto-0.1.2 (c (n "sqlcrypto") (v "0.1.2") (d (list (d (n "aes_frast") (r "^0.1.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.15") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)))) (h "0zjyhgkycrv58jalyi8qwc92lv3qbwqlm7dilrz8rnpmw2923xg2") (y #t)))

(define-public crate-sqlcrypto-0.2.0 (c (n "sqlcrypto") (v "0.2.0") (d (list (d (n "aesni") (r "^0.9.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.6.1") (d #t) (k 0)) (d (n "hmac") (r "^0.9.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.5.0") (f (quote ("parallel"))) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)))) (h "1w8hlsmcnqlady38ag9w1x7hkzwn0b0p6sv5n5czdbrkf0v5yhab") (y #t)))

(define-public crate-sqlcrypto-0.2.1 (c (n "sqlcrypto") (v "0.2.1") (d (list (d (n "aesni") (r "^0.9.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.6.1") (d #t) (k 0)) (d (n "hmac") (r "^0.9.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.5.0") (f (quote ("parallel"))) (k 0)) (d (n "rayon") (r "^1.4.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)))) (h "0q8w2yf498v7h43cjqi40pax4hkzwl218736sl18y3c9xvnd0m70") (y #t)))

(define-public crate-sqlcrypto-1.0.0 (c (n "sqlcrypto") (v "1.0.0") (d (list (d (n "aes") (r "^0.6.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.7.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.6.0") (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)))) (h "1qq645dyq7wzwk70l3ypxb1v26yry625nwzpjcj8jb7jxgnjfbhk") (f (quote (("parallel" "rayon" "pbkdf2/parallel") ("js" "getrandom/js")))) (y #t)))

(define-public crate-sqlcrypto-1.0.1 (c (n "sqlcrypto") (v "1.0.1") (d (list (d (n "aes") (r "^0.6.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.7.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.0") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.6.0") (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "02ll5ibxry0ijdbiibd2z6a15vhcvf3gmng1f3zbxf4jkg9znjq3") (f (quote (("parallel" "rayon" "pbkdf2/parallel")))) (y #t)))

