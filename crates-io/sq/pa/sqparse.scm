(define-module (crates-io sq pa sqparse) #:use-module (crates-io))

(define-public crate-sqparse-0.1.0 (c (n "sqparse") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lexical") (r "^6.1") (f (quote ("power-of-two"))) (d #t) (k 0)))) (h "0xapv9clsbskiasfh1bm8zg5qbq3g9z3hgw8pvxhv8lvfwblb42c")))

(define-public crate-sqparse-0.2.0 (c (n "sqparse") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lexical") (r "^6.1") (f (quote ("power-of-two"))) (d #t) (k 0)))) (h "0xm3l3ihl75mcchicg9g4gm2gv9wnic7a0rymdmxqs8pv1szrfsy")))

(define-public crate-sqparse-0.3.0 (c (n "sqparse") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lexical") (r "^6.1") (f (quote ("power-of-two"))) (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0wyr4rnv7jlb8rjz749gzw6k858w1mj2ch90fmn2n80s1nfhbbf3")))

