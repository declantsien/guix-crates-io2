(define-module (crates-io sq lf sqlformat) #:use-module (crates-io))

(define-public crate-sqlformat-0.1.0 (c (n "sqlformat") (v "0.1.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0cf609g4hsfk1fmcnnr6zk8gwv0gikdwrwqva4p0lwp1fr2lmrjw")))

(define-public crate-sqlformat-0.1.1 (c (n "sqlformat") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s99nl7pbj3lrbv0dibkmics87iq2qaclb4375ngv3srxhg316gn")))

(define-public crate-sqlformat-0.1.2 (c (n "sqlformat") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0alq0j1fp5ldf9b4ri3nd3g5fp4626hsj445g89xwwqbvbqgia3i")))

(define-public crate-sqlformat-0.1.3 (c (n "sqlformat") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0sn12wafpyw3lkqvdli1n1i7hfq456g0kqq0cx595spc5r4z7v04")))

(define-public crate-sqlformat-0.1.4 (c (n "sqlformat") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "16lhs37bl21kh4i3hdy3na5vkx1gbk8c3k24qraicdix1n7fansh")))

(define-public crate-sqlformat-0.1.5 (c (n "sqlformat") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0bcw71x2abjnp0lf6ldqm22lismm50daylkc22xmr4mr6l10zivl")))

(define-public crate-sqlformat-0.1.6 (c (n "sqlformat") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0777bmv9b0pycih1qj84z06l93jbxykh393b6hlai0pqgz3y71kd")))

(define-public crate-sqlformat-0.1.7 (c (n "sqlformat") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "15kx9gsp9ivyllvpxp9zdf3sf8pga78pxdv3d6kakhayk3kh2h38")))

(define-public crate-sqlformat-0.1.8 (c (n "sqlformat") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "1m7z9g1yi3jszbl1c6vay3s49mmx70zm49g11f871vhpw0mr5dxl")))

(define-public crate-sqlformat-0.2.0 (c (n "sqlformat") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "12n3vf2jh4lxzbv5ngwxb7ncmjz2ci1pghs33abm9wci88mjjzpq")))

(define-public crate-sqlformat-0.2.1 (c (n "sqlformat") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0gpf3a5yr53vhk8n54h1rz6igx87gis52w4bcws85nyik68vq4hc") (r "1.56")))

(define-public crate-sqlformat-0.2.2 (c (n "sqlformat") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "11cymbg6xr7xyy5hvry8pfkgpwzfl2br9wy0546lvgp7i23jfyvb") (r "1.56")))

(define-public crate-sqlformat-0.2.3 (c (n "sqlformat") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "0v0p70wjdshj18zgjjac9xlx8hmpx33xhq7g8x9rg4s4gjyvg0ff") (r "1.56")))

