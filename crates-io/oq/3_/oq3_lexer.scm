(define-module (crates-io oq #{3_}# oq3_lexer) #:use-module (crates-io))

(define-public crate-oq3_lexer-0.0.1 (c (n "oq3_lexer") (v "0.0.1") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0f5rcgf0ws5s7i492dy489y90wi643rs0qg3pxa2hig96v3x86l7") (r "1.70")))

(define-public crate-oq3_lexer-0.0.2 (c (n "oq3_lexer") (v "0.0.2") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1q6i53lcmkc5r00wdlnqb8awplnnz7ajjdiq2ig5285m93jmx6n7") (r "1.70")))

(define-public crate-oq3_lexer-0.0.3 (c (n "oq3_lexer") (v "0.0.3") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "11b2p2p3slnrbnpmf1paa7ycflvxjn4lahi67ijimwr57hl6513j") (r "1.70")))

(define-public crate-oq3_lexer-0.0.4 (c (n "oq3_lexer") (v "0.0.4") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "089dyjxg0qlfrkxdl1ndz19xrkpz15bslknbcpzfl34f5xanqv7a") (r "1.70")))

(define-public crate-oq3_lexer-0.0.5 (c (n "oq3_lexer") (v "0.0.5") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0aa8b4rark7nfqlj3r7qayvlpgiv4pgi0hzlsaczjq2k06vf3hd4") (r "1.70")))

(define-public crate-oq3_lexer-0.0.6 (c (n "oq3_lexer") (v "0.0.6") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1pk3q71j9g33j3xmc54dsjiv7v8dmlminbcrc43a28vmwcxk1xp8") (r "1.70")))

(define-public crate-oq3_lexer-0.0.7 (c (n "oq3_lexer") (v "0.0.7") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0csnn71qpcy631qxzvfp4j25jz2nlmk6bqhmhw3bh03ig796gs64") (r "1.70")))

(define-public crate-oq3_lexer-0.1.0 (c (n "oq3_lexer") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "15afy1j79wd81wg0q0rd6vr64l7dm1y3ny9q1j8ziji40h1n7xcm") (r "1.70")))

(define-public crate-oq3_lexer-0.2.0 (c (n "oq3_lexer") (v "0.2.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1hyh3lc2ml4s5s52y400pch7wkvp6l8l49f5d9g7axwly7p6qqr2") (r "1.70")))

(define-public crate-oq3_lexer-0.3.0 (c (n "oq3_lexer") (v "0.3.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1jzy9j7h1n9qv84cd8znnqz41pwm7s8cmrlc8ywx4w1gv3qv78fa") (r "1.70")))

(define-public crate-oq3_lexer-0.4.0 (c (n "oq3_lexer") (v "0.4.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "12f4mf9d9xj85xjhv6bd88lsjrgpwziyidggdi45iyvsyf8v0vwv") (r "1.70")))

(define-public crate-oq3_lexer-0.5.0 (c (n "oq3_lexer") (v "0.5.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "05i62z4lykl0gb8jqcwmazsdwjl3kfhm48f3q32ma1ck8qkrmv5q") (r "1.70")))

(define-public crate-oq3_lexer-0.6.0 (c (n "oq3_lexer") (v "0.6.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "18s16llya0iyysnzdhwg21z646476w40hmdjh8pw2hl0skwz1qhd") (r "1.70")))

