(define-module (crates-io oq #{3_}# oq3_source_file) #:use-module (crates-io))

(define-public crate-oq3_source_file-0.0.1 (c (n "oq3_source_file") (v "0.0.1") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.0.1") (d #t) (k 0)))) (h "19f5k1slxr8rkm1f9971cmlpzqwicaw9r6q4a0i7fjxi6x7jbfrs") (r "1.70")))

(define-public crate-oq3_source_file-0.0.2 (c (n "oq3_source_file") (v "0.0.2") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.0.1") (d #t) (k 0)))) (h "15l5jgigkswrk0hkv1n4v42b9za2jm052zpdi000b3nna3lbbkfa") (r "1.70")))

(define-public crate-oq3_source_file-0.0.3 (c (n "oq3_source_file") (v "0.0.3") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.0.3") (d #t) (k 0)))) (h "05i70izf3jzczjyvs5agcxxn33c1bkm1q0kw0kmagbsvqz19hw5a") (r "1.70")))

(define-public crate-oq3_source_file-0.0.4 (c (n "oq3_source_file") (v "0.0.4") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.0.4") (d #t) (k 0)))) (h "1rvr72alkyn8a3zkfw2c8h4py277c0rqz9pr3m76ib2xg3gbs5yx") (r "1.70")))

(define-public crate-oq3_source_file-0.0.5 (c (n "oq3_source_file") (v "0.0.5") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.0.5") (d #t) (k 0)))) (h "0aq2g7mwrf5c8g56j1zxr7v89k4p38a9a94g2zi9k1p8j3ns5z9g") (r "1.70")))

(define-public crate-oq3_source_file-0.0.6 (c (n "oq3_source_file") (v "0.0.6") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.0.6") (d #t) (k 0)))) (h "0qwv1g5zhmngnz3bv889qk4i0v16ly4wwlsiglz012xqa38k4g6z") (r "1.70")))

(define-public crate-oq3_source_file-0.0.7 (c (n "oq3_source_file") (v "0.0.7") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.0.7") (d #t) (k 0)))) (h "1741dxv70mwcrd2d1wxiyp1r7mlify4iqw93ga6ss061q781za43") (r "1.70")))

(define-public crate-oq3_source_file-0.1.0 (c (n "oq3_source_file") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.1.0") (d #t) (k 0)))) (h "04h0z6p90a2gznpj148ibvi83wz01yajf6p1sr1556nswvwqxkia") (r "1.70")))

(define-public crate-oq3_source_file-0.2.0 (c (n "oq3_source_file") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.2.0") (d #t) (k 0)))) (h "0hf1ndxzn8znih5gq5y1yscx1gk9sygmwrpnfa91vc1g7ppb0qb0") (r "1.70")))

(define-public crate-oq3_source_file-0.3.0 (c (n "oq3_source_file") (v "0.3.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.3.0") (d #t) (k 0)))) (h "1c67lrin809r07jchdiyr1n7x64qbj34a3zrixg2z1z37kclr9fz") (r "1.70")))

(define-public crate-oq3_source_file-0.4.0 (c (n "oq3_source_file") (v "0.4.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.4.0") (d #t) (k 0)))) (h "0wmb1ybnxybkn2yizdrv1rvf9z3bp8rilrjxilqki0cgan0qi20y") (r "1.70")))

(define-public crate-oq3_source_file-0.5.0 (c (n "oq3_source_file") (v "0.5.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.5.0") (d #t) (k 0)))) (h "0aj1g6nr673lz0l89iqr1kql7zknr7z7qli54lsggncmmiy4q4hm") (r "1.70")))

(define-public crate-oq3_source_file-0.6.0 (c (n "oq3_source_file") (v "0.6.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "oq3_syntax") (r "^0.6.0") (d #t) (k 0)))) (h "1rrhcp6530iqrznb6d605fm56k2l2xndn8aaah660z40qhy28rag") (r "1.70")))

