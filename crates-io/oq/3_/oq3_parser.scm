(define-module (crates-io oq #{3_}# oq3_parser) #:use-module (crates-io))

(define-public crate-oq3_parser-0.0.1 (c (n "oq3_parser") (v "0.0.1") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.0.1") (d #t) (k 0)))) (h "1h94knla4vrr7kqlm095vnhsjhjllg1a1camigldphg77rrnrw5b") (r "1.70")))

(define-public crate-oq3_parser-0.0.2 (c (n "oq3_parser") (v "0.0.2") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.0.1") (d #t) (k 0)))) (h "1wg82gwyj6xf827v7h0vka8aignn4j284q32xiwmsdr60v8lmnmm") (r "1.70")))

(define-public crate-oq3_parser-0.0.3 (c (n "oq3_parser") (v "0.0.3") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.0.3") (d #t) (k 0)))) (h "05lnwnl6sby557pljfj80snypywc00w8xwz4d7pbplq70s0pkpv8") (r "1.70")))

(define-public crate-oq3_parser-0.0.4 (c (n "oq3_parser") (v "0.0.4") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.0.4") (d #t) (k 0)))) (h "1avknylfc9d518jyyly3m51sm9wwxi9f9wgaazlidwvmcrsdbmc9") (r "1.70")))

(define-public crate-oq3_parser-0.0.5 (c (n "oq3_parser") (v "0.0.5") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.0.5") (d #t) (k 0)))) (h "0q9dwr8fqcn9qhcgylbf3kr7ncrac38amhr7g9y6zd1blli3y2q0") (r "1.70")))

(define-public crate-oq3_parser-0.0.6 (c (n "oq3_parser") (v "0.0.6") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.0.6") (d #t) (k 0)))) (h "14l4984c2g7np54rmqwnaihb1icc7kcbm2xqrk8p9a6yz3in17ik") (r "1.70")))

(define-public crate-oq3_parser-0.0.7 (c (n "oq3_parser") (v "0.0.7") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.0.7") (d #t) (k 0)))) (h "1y8kavr2sbh0dr0szjaf5g9a9c7rq546fiqx15fl0sxmf7m0s9ng") (r "1.70")))

(define-public crate-oq3_parser-0.1.0 (c (n "oq3_parser") (v "0.1.0") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.1.0") (d #t) (k 0)))) (h "18jdh2fwy705bc5pb4linlgcb0xrp4dpd22m71q7rfq0jfn1a5sm") (r "1.70")))

(define-public crate-oq3_parser-0.2.0 (c (n "oq3_parser") (v "0.2.0") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.2.0") (d #t) (k 0)))) (h "11i3r1rjkqh63vv3k69b861dvvl9lpimfwkd8jp5sjm8qknpyxjn") (r "1.70")))

(define-public crate-oq3_parser-0.3.0 (c (n "oq3_parser") (v "0.3.0") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.3.0") (d #t) (k 0)))) (h "0qx4ghnn1142prxfg1df9agi2vqc12xy5gvnfkl39v7s4blw6fa0") (r "1.70")))

(define-public crate-oq3_parser-0.4.0 (c (n "oq3_parser") (v "0.4.0") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.4.0") (d #t) (k 0)))) (h "1xzayvnxnb0vlmc6pgr47zapvmxl45niilnf25mx8ngp1jjssazc") (r "1.70")))

(define-public crate-oq3_parser-0.5.0 (c (n "oq3_parser") (v "0.5.0") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.5.0") (d #t) (k 0)))) (h "14cdxz2qkwfa15z0flbhvkyd986nif52p213cri9n6nd2n9jqhh7") (r "1.70")))

(define-public crate-oq3_parser-0.6.0 (c (n "oq3_parser") (v "0.6.0") (d (list (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "limit") (r "^0.0.188") (d #t) (k 0) (p "ra_ap_limit")) (d (n "oq3_lexer") (r "^0.6.0") (d #t) (k 0)))) (h "0jvhnchh7ijshz50l9pf76ms0fv3di1s9k32zlis18m44ra236z6") (r "1.70")))

