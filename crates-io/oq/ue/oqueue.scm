(define-module (crates-io oq ue oqueue) #:use-module (crates-io))

(define-public crate-oqueue-0.1.0 (c (n "oqueue") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "readonly") (r "^0.1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "03iqy29yhib3z0xz1vd6h6akxamfddhf3q26wl9aqffmw3zkd8bj")))

(define-public crate-oqueue-0.1.1 (c (n "oqueue") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "readonly") (r "^0.1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0qz30lr97hgj6k1xxkfqiimbqsmilrg6i0l362zlbcl4mskh20m8")))

(define-public crate-oqueue-0.1.2 (c (n "oqueue") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "019y4aqvg9czlr9njgilx2q1x0v4mlynnx296jd64p5hxjkqqszc")))

(define-public crate-oqueue-0.1.3 (c (n "oqueue") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0yvwdida84mfl5h936q80clwszjxyffzjvl67l4bizf61lb38g76") (r "1.33")))

(define-public crate-oqueue-0.1.4 (c (n "oqueue") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0si6b8wvl5sysi2r65q0278n2jxdn14cgcj5hhf2mq48qkchfzs5") (r "1.33")))

(define-public crate-oqueue-0.1.5 (c (n "oqueue") (v "0.1.5") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1ja6zqcsr8xj1adjmrziq3pbcp9s18nb7wg0sqxs1w8dklblw2hi") (r "1.33")))

(define-public crate-oqueue-0.1.6 (c (n "oqueue") (v "0.1.6") (d (list (d (n "num_cpus") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1a5v86v52vsjpbg5zkh9qbklvmdbi4hy5lzq8fl89fkhx4kx4chy") (r "1.33")))

(define-public crate-oqueue-0.1.7 (c (n "oqueue") (v "0.1.7") (d (list (d (n "num_cpus") (r "^1.16") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "readonly") (r "^0.2.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "01bdp136vjvlbnm47744wm1kwiigxanhjxmw8fg7xmfr7rxrhscs") (r "1.33")))

(define-public crate-oqueue-0.1.8 (c (n "oqueue") (v "0.1.8") (d (list (d (n "num_cpus") (r "^1.16") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "readonly") (r "^0.2.10") (d #t) (k 0)) (d (n "termcolor") (r "^1.2") (d #t) (k 0)))) (h "0ksvjynhsrwikz43pnzkjxnj7500ma18p2i17fzwz5wj48jx052z") (r "1.33")))

