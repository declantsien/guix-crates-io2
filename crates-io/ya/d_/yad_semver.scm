(define-module (crates-io ya d_ yad_semver) #:use-module (crates-io))

(define-public crate-yad_semver-1.0.0 (c (n "yad_semver") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1bgf6l7rgwxwpi0jj38nrgdc1bp85sxgfcrw5nvfb717py5yqy0n")))

