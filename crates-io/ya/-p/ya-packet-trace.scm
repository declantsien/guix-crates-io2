(define-module (crates-io ya -p ya-packet-trace) #:use-module (crates-io))

(define-public crate-ya-packet-trace-0.1.0 (c (n "ya-packet-trace") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 2)) (d (n "serial_test") (r "^0.9") (d #t) (k 2)))) (h "0k3izcd0hkdy53dh65dlrsqfwgfbh7rgdnia9mc9llax8x2597ia") (f (quote (("enable") ("default"))))))

