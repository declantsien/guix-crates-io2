(define-module (crates-io ya rw yarw_core) #:use-module (crates-io))

(define-public crate-yarw_core-0.0.0 (c (n "yarw_core") (v "0.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "13jrpyv0jh2hb2kqc3azk1kxqd8ia0ynr11ysdbgas28r10f23a8")))

