(define-module (crates-io ya ge yaged) #:use-module (crates-io))

(define-public crate-yaged-0.1.0 (c (n "yaged") (v "0.1.0") (d (list (d (n "lzw") (r "^0.10.0") (d #t) (k 0)))) (h "1xrlyih2m2zsnydn5vhmwww1ga30f8nfkyflz6m1cfrwarq5l1kx")))

(define-public crate-yaged-0.2.0 (c (n "yaged") (v "0.2.0") (d (list (d (n "lzw") (r "^0.10.0") (d #t) (k 0)))) (h "0bb4kqg798b1nkc73yivdhj6a1n40b9wsyd4r4jdz085qj0v2bjk")))

