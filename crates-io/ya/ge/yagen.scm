(define-module (crates-io ya ge yagen) #:use-module (crates-io))

(define-public crate-yagen-0.1.0 (c (n "yagen") (v "0.1.0") (h "1f24zkf33hjylxm6qvx8rbpix9bg6qhsxgdbrs3ksmmcwz9ifhr9")))

(define-public crate-yagen-0.2.0 (c (n "yagen") (v "0.2.0") (h "1zbhgv79g0jxq9mkw6xz7c0pvwbqqv0gz3pfba2hspc5lwd837cc")))

(define-public crate-yagen-0.3.0 (c (n "yagen") (v "0.3.0") (h "0pg32v4bhhlrjxq61w6dkpsipw7lrgzh9pbw264igskxly8g8lj7")))

(define-public crate-yagen-0.3.1 (c (n "yagen") (v "0.3.1") (h "0bx734dbhilys75b8wn4jz14b3hhvqffvpcd5wgqfxx45f9lf1ca")))

