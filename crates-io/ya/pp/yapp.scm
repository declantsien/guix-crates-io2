(define-module (crates-io ya pp yapp) #:use-module (crates-io))

(define-public crate-yapp-0.1.0 (c (n "yapp") (v "0.1.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "0.12.*") (d #t) (k 2)))) (h "1wki0w2sjvb30dl466z3rpjkwg6xy94cyxaxq8pxfnm5zz0wa57i")))

(define-public crate-yapp-0.1.1 (c (n "yapp") (v "0.1.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "0.12.*") (d #t) (k 2)))) (h "05yyg6niv2zfa6vy9kyfan4gvqw0b3xikvc7hja14j8ppqyp8wp7")))

(define-public crate-yapp-0.1.2 (c (n "yapp") (v "0.1.2") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "0.12.*") (d #t) (k 2)))) (h "0qmrkzfw7iyxga84afx5a89flrrdd5l9aj1sf9cbx1clahsq6smd") (r "1.70.0")))

(define-public crate-yapp-0.2.0 (c (n "yapp") (v "0.2.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "0.12.*") (d #t) (k 2)))) (h "0b065va5bgava2phalavr1vqcb579j5n8fan3pxg16srnwnga4bc") (r "1.70.0")))

(define-public crate-yapp-0.3.0 (c (n "yapp") (v "0.3.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "0.12.*") (d #t) (k 2)))) (h "1zg7xf5g76njzvfpiffbyn4sv4wz8mxbda9629p7rggi39jga02x") (r "1.70.0")))

(define-public crate-yapp-0.4.0 (c (n "yapp") (v "0.4.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "0.12.*") (d #t) (k 2)))) (h "09kcbfkg0v4lgpvs2yqbf4ygfv4cx50444chi5157i06c0n7x8lq") (r "1.70.0")))

(define-public crate-yapp-0.4.1 (c (n "yapp") (v "0.4.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "0.12.*") (d #t) (k 2)))) (h "0k9993ibaiwv8lmv7nfzksl1h199xqm0ldjcm4isspkzy35b2q37") (r "1.70.0")))

