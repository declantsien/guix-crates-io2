(define-module (crates-io ya ml yaml2json) #:use-module (crates-io))

(define-public crate-yaml2json-0.1.0 (c (n "yaml2json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "0hn7h88xml7vlg2k5cqf9m531l2g1naf42kqbmlrrqgi6fpvby2j") (y #t)))

(define-public crate-yaml2json-0.1.1 (c (n "yaml2json") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "1kfa5j2pldncxq2c6r39lvjcfhimd21vp7d0caxiljhcfwglm6j2") (y #t)))

(define-public crate-yaml2json-0.1.2 (c (n "yaml2json") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)))) (h "0l54dfkkcfl9w0j6l15ddjlh4v43jf64wz6k9sa9k77jqk6j59xy")))

(define-public crate-yaml2json-0.1.3 (c (n "yaml2json") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "15v9f209q7ax7yh9qffdlpg7gs8f8fmdcw82p0kw6h17w81hgs6s")))

