(define-module (crates-io ya ml yaml_extras) #:use-module (crates-io))

(define-public crate-yaml_extras-0.1.0 (c (n "yaml_extras") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dx6hjm7a7lgymc8nxhywwqc4w6fcarm1bxdb79gh5qw4m911b8k")))

(define-public crate-yaml_extras-0.1.1 (c (n "yaml_extras") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hllgrgmyrvr1q9v8l9jxa4ihswjm467qmb9yd4v8wcqasq00n3n")))

(define-public crate-yaml_extras-0.2.0 (c (n "yaml_extras") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jvqxsvxqy3sdm9r6fvdx5vpi7vszm6l5s1zk527x9xw7f8hdlmj")))

