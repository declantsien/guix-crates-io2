(define-module (crates-io ya ml yaml-validator-cli) #:use-module (crates-io))

(define-public crate-yaml-validator-cli-0.0.1 (c (n "yaml-validator-cli") (v "0.0.1") (d (list (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "yaml-validator") (r "^0.0.1") (d #t) (k 0)))) (h "036ppjx0cg3dg9vhjr80108xf5k3zjcyvr3j1ij6rjf6mam13izf")))

(define-public crate-yaml-validator-cli-0.0.2 (c (n "yaml-validator-cli") (v "0.0.2") (d (list (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "yaml-validator") (r "^0.0.3") (d #t) (k 0)))) (h "0rliq8x0z3cyqs69b1p7kv1bgwfhppx2c0r1gvbr3k6l5jx4cpsl")))

(define-public crate-yaml-validator-cli-0.0.3 (c (n "yaml-validator-cli") (v "0.0.3") (d (list (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "yaml-validator") (r "^0.0.4") (d #t) (k 0)))) (h "16rqcqdvrc2dqzjad7bwvvwz0ryx2lpxlypfd0zd6kl66nc541rr")))

(define-public crate-yaml-validator-cli-0.1.0-beta.0 (c (n "yaml-validator-cli") (v "0.1.0-beta.0") (d (list (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "yaml-validator") (r "^0.1.0-beta.0") (d #t) (k 0)))) (h "0qdnmbbazrb34sxjrmqz5s6grn82xhwk4fl77a5kdjmqi1v6ck9f")))

(define-public crate-yaml-validator-cli-0.1.0 (c (n "yaml-validator-cli") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "yaml-validator") (r "^0.1.0") (d #t) (k 0)))) (h "0sgp57axc777andi6af1wqm3n2ab3g3aa39av8ylkyna088vr8rj")))

(define-public crate-yaml-validator-cli-0.1.1 (c (n "yaml-validator-cli") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "yaml-validator") (r "^0.2.0") (d #t) (k 0)))) (h "16asgb560z8q20z2659mmn1fgw8g5p8lbls2n1zliwzsvgr3vd81")))

