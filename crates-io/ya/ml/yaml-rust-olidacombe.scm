(define-module (crates-io ya ml yaml-rust-olidacombe) #:use-module (crates-io))

(define-public crate-yaml-rust-olidacombe-0.6.0 (c (n "yaml-rust-olidacombe") (v "0.6.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1w6593pkqdhsh2q45y09md60hd61smhniyf2x11g0d4cpkblj8zb")))

