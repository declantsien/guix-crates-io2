(define-module (crates-io ya ml yaml2json-rs) #:use-module (crates-io))

(define-public crate-yaml2json-rs-0.2.0 (c (n "yaml2json-rs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1sxj1b96brkjj6vcswwh9yyvak9w31izlbczx9i40n9d5j47lf37")))

(define-public crate-yaml2json-rs-0.2.1 (c (n "yaml2json-rs") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1d1vyw1219bfkgy2b23lgx2c99alczf76d2mjffk5yl1kqqxx1h7")))

(define-public crate-yaml2json-rs-0.2.2 (c (n "yaml2json-rs") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "007m998k3wirk500fsmrrhkg13hc2aj96302gnyk7429swspgzmv")))

(define-public crate-yaml2json-rs-0.2.3 (c (n "yaml2json-rs") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0l8g2f14xvkvq1pxkhbq6hq6lchylkwmnq6rzq7rqk9vp644lpqk")))

(define-public crate-yaml2json-rs-0.2.5 (c (n "yaml2json-rs") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "12ypvfa4dprwn68gwyr73zwrc6kfig8kar0gjf0w41ykjdarkx9q")))

(define-public crate-yaml2json-rs-0.2.7 (c (n "yaml2json-rs") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0igqz57xk02pp20pqf3kx9pk3rd23h5915xcpxxzyg1idhrx0a2l")))

(define-public crate-yaml2json-rs-0.2.8 (c (n "yaml2json-rs") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0gsbg5jm81jxaah013gs1y6arv66qks0mf9rwmng9zs0lsliiig7")))

(define-public crate-yaml2json-rs-0.3.1 (c (n "yaml2json-rs") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1ghkk5r6jf2pzcrjdmvc1c388n1g94sla639sqgyxagyr37y5wpn")))

(define-public crate-yaml2json-rs-0.3.2 (c (n "yaml2json-rs") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0pb1bdgwr655fa2dp7pyxp2dxn98w0zxk0hs0vyjyhq24dqkqsyn")))

(define-public crate-yaml2json-rs-0.4.0 (c (n "yaml2json-rs") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1kz8v8gqzzyph9yvxpvmf4zda58nqcsqiri7p3jxldfxg02bp828")))

