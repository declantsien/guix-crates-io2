(define-module (crates-io ya ml yaml-path) #:use-module (crates-io))

(define-public crate-yaml-path-0.1.0 (c (n "yaml-path") (v "0.1.0") (d (list (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1cpdq69yvxc79yfd8hz3q2dwxgnkphaaw1can8b361h3m5kyma84")))

