(define-module (crates-io ya ml yamlcheck) #:use-module (crates-io))

(define-public crate-yamlcheck-0.1.0 (c (n "yamlcheck") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "valico") (r "^4.0.0-rc.0") (f (quote ("js"))) (d #t) (k 0)))) (h "0nb1axamxwiq9r0phfksai0vvp8i95b3v49hsdax06iwfvp6n0am")))

