(define-module (crates-io ya ml yaml-include) #:use-module (crates-io))

(define-public crate-yaml-include-0.3.0 (c (n "yaml-include") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "17l93hs7vpm0fg3g4gh95m0a3sfv7p0kxjnyw6ym9f5zq4y8iw6f")))

(define-public crate-yaml-include-0.4.0 (c (n "yaml-include") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1dpbpx7gk0ja4ra09bnglyz3z81lck4k2sn8mfw2kxhr3qh3glwl") (y #t)))

(define-public crate-yaml-include-0.4.1 (c (n "yaml-include") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0wxryr3gwpppjns7nr89ys7cbkyg7y0qwz2b6p4r9nyjgppkxdvx")))

(define-public crate-yaml-include-0.5.0 (c (n "yaml-include") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0c30aqg8xaixc4ygy79h2rgwvg773sjssgy98nzq6cyqp4985lqg")))

(define-public crate-yaml-include-0.6.0 (c (n "yaml-include") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1sgabml5qz7af0r5i91n4xcc63s8cv722ym9rb5a0jsxhrhsk7sb")))

(define-public crate-yaml-include-0.7.0 (c (n "yaml-include") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1wav6bar9h8nc020ils3lmvl8a94b9gqbgfh2095k6ps9l3ap965")))

