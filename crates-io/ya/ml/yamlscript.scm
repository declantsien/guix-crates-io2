(define-module (crates-io ya ml yamlscript) #:use-module (crates-io))

(define-public crate-yamlscript-0.0.16 (c (n "yamlscript") (v "0.0.16") (h "09kn0dnqdzxx92jrd0kbmgaxr3z4r063jjajlddyxnq5l6zxc45k")))

(define-public crate-yamlscript-0.1.0 (c (n "yamlscript") (v "0.1.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1hn29lc4w8cp9yzkx4y0sznqmgc86rcqkxqjjayz9r5i73m1q6q0")))

(define-public crate-yamlscript-0.1.1 (c (n "yamlscript") (v "0.1.1") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1asw8yjkm2w55ss44bqcvaq7f5qj01faack5r6vx5i6i74jj7g3b")))

(define-public crate-yamlscript-0.1.2 (c (n "yamlscript") (v "0.1.2") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1d9sdaj6cai0kfkkapcnnllxmg8qwlix9b74pasjzyjaf9hph11h")))

(define-public crate-yamlscript-0.1.35 (c (n "yamlscript") (v "0.1.35") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "0bm6xv9hr5jlpyrq5h0ibyic4mrmys72s88v9wnn1yljdyyaz4bv")))

(define-public crate-yamlscript-0.1.36 (c (n "yamlscript") (v "0.1.36") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "0y17agsp4zxi1iqj4l2xfkx64g5c2hp79q326fy5r9ph762kiwn5")))

(define-public crate-yamlscript-0.1.37 (c (n "yamlscript") (v "0.1.37") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "14gqclblk0bv179x98y0r731zikhfsdrl8azgcphqprb3c0jq4ya")))

(define-public crate-yamlscript-0.1.38 (c (n "yamlscript") (v "0.1.38") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1cf64gy5l34kz5bz7zylknzbnvvnnq2spby8w49l2mkgrdlndbvn")))

(define-public crate-yamlscript-0.1.39 (c (n "yamlscript") (v "0.1.39") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1vfcgcpavh6zs7b23kddkc14hidjq7518lj8292v6j3x01hbw3hr")))

(define-public crate-yamlscript-0.1.40 (c (n "yamlscript") (v "0.1.40") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1nb13g66mm31a08g2xxka2w7c20s1vlf328n6az8pa57r62srmk3")))

(define-public crate-yamlscript-0.1.41 (c (n "yamlscript") (v "0.1.41") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "17jc6dgzw28biyanj4sq887d1sx77myxhzfrsf763m89vzyyny50")))

(define-public crate-yamlscript-0.1.42 (c (n "yamlscript") (v "0.1.42") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1n16l5sfz3js2fahmvirbmgsvq3y9v6rhdfakhcb5kjgfi94jmxs")))

(define-public crate-yamlscript-0.1.44 (c (n "yamlscript") (v "0.1.44") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1szigvcjsj00aymmb3wqv2f5yx42mx39jfni58swha82f06ybgwl")))

(define-public crate-yamlscript-0.1.45 (c (n "yamlscript") (v "0.1.45") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1apshfn1aw6yvj6yznnzpypfddjq10z77crbh0ciwcajz0ws1lv6")))

(define-public crate-yamlscript-0.1.46 (c (n "yamlscript") (v "0.1.46") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1jvdmmmgakr831g2gganhxsy91v3r73brwxw8z88hm8rkds7r4dh")))

(define-public crate-yamlscript-0.1.47 (c (n "yamlscript") (v "0.1.47") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "04i911598nciqn2zxjypyi49w3rj3kd21rz07irjfajd452jd0yg")))

(define-public crate-yamlscript-0.1.48 (c (n "yamlscript") (v "0.1.48") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "0bd7dkqwdsl0ww8qszavd8c6q2685r66ippbck674py00fcbn5ln")))

(define-public crate-yamlscript-0.1.49 (c (n "yamlscript") (v "0.1.49") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "0vk8ms6n9cxnxcg3bqg1inx2kik2inhg8v0s79c3icnkhlw3hijn")))

(define-public crate-yamlscript-0.1.50 (c (n "yamlscript") (v "0.1.50") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "19zipgmcigfdar7hp876mx61kn0y9scpvjzf7vvb90x690r5i8qk")))

(define-public crate-yamlscript-0.1.51 (c (n "yamlscript") (v "0.1.51") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1sb562w38kw258c5cy9i6j59hfs72x73diz0fkm19k2hbcxsb5ky")))

(define-public crate-yamlscript-0.1.52 (c (n "yamlscript") (v "0.1.52") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "19nkvkwjqzz3bgwgmclj9wgbry78hkmjwcnakv5fz2z2bvjakn5m")))

(define-public crate-yamlscript-0.1.53 (c (n "yamlscript") (v "0.1.53") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "0g3yf1p2kgaf24m8i4b5rm7qzs1844wk8hk9griclibj5ni869cl")))

(define-public crate-yamlscript-0.1.54 (c (n "yamlscript") (v "0.1.54") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "1mm49xf7yvww19jlb0b12ggsmddfpgrzhphfwdhyn422zkbdy9rz")))

(define-public crate-yamlscript-0.1.55 (c (n "yamlscript") (v "0.1.55") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "0gs6181hxkavj8775pl26g4xrp9c8dy810ksw4v91wimy58blxm1")))

(define-public crate-yamlscript-0.1.56 (c (n "yamlscript") (v "0.1.56") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "19571gqx5izww0kykr776r0y9igcr1w6w1v9awc2blabrwmnmvkn")))

(define-public crate-yamlscript-0.1.57 (c (n "yamlscript") (v "0.1.57") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "16ialmrsh67l2i9b6x32n88qpa2qzg3w4dvcvlii2l7xzcdlrdl4")))

(define-public crate-yamlscript-0.1.58 (c (n "yamlscript") (v "0.1.58") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "02qsl3ysvihaxr7n10p3afb6nv5c9fsh8056gqn3438140dii8cy")))

(define-public crate-yamlscript-0.1.59 (c (n "yamlscript") (v "0.1.59") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "140l27mbg3rvm4k8r671ffgryi39b0idjx2kr294d8viwg9qpg4n")))

