(define-module (crates-io ya ml yaml-split) #:use-module (crates-io))

(define-public crate-yaml-split-0.2.0 (c (n "yaml-split") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0sbx9r1kpgjni2n0vk8swi04j09g7dwyj4mnyms7lwpr0hd8vn8v")))

(define-public crate-yaml-split-0.2.1 (c (n "yaml-split") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "107lprpiw560iv4hls97qsnd2yi08h1rdv80579q4yws2s902p5r")))

(define-public crate-yaml-split-0.2.2 (c (n "yaml-split") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "13i699zhdw2f6083ixm692nb6v7fl5b7q3mc6gmwlkm99a5p2wwb")))

(define-public crate-yaml-split-0.2.5 (c (n "yaml-split") (v "0.2.5") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "01a86fi0frvjqh79i9lfp889fwwl78mkh5r3qrz4hgxk9q30jy2n")))

(define-public crate-yaml-split-0.2.6 (c (n "yaml-split") (v "0.2.6") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "00dh27q0rw3rc5v1ycyx7p7ccb82zklmd6v47aff638djvdblkn1")))

(define-public crate-yaml-split-0.2.7 (c (n "yaml-split") (v "0.2.7") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0yl3q603096rxgfc05qdyzbcrii0lc79kvf3rg1mvik3wilz4qmx")))

(define-public crate-yaml-split-0.2.8 (c (n "yaml-split") (v "0.2.8") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0d5xsbln6rzm4mkzjc5hfajr3y313lj38dfh3k1sxwir93wyh5mk")))

(define-public crate-yaml-split-0.3.1 (c (n "yaml-split") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1wkhdrwa8c66g6wxa3jcq14bahcwzn9pbmaga4v47h9h6sdd6gj6")))

(define-public crate-yaml-split-0.3.2 (c (n "yaml-split") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "00lwhwm7ss8ddd4aj01c1faaln7b1vahv6p4mabsxksnpj3bc1w1")))

(define-public crate-yaml-split-0.4.0 (c (n "yaml-split") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0n7wh3fmg8sk0imxcd6k9dy8b8y64sai7rg04j29x84s7gz2pawx")))

