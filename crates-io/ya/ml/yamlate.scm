(define-module (crates-io ya ml yamlate) #:use-module (crates-io))

(define-public crate-yamlate-0.1.0 (c (n "yamlate") (v "0.1.0") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "yaml-rust") (r "*") (d #t) (k 0)))) (h "1qv5w9fxayq796ryr4cyhsyf4bwc2jms6wr0z0vsmsammwy64375") (f (quote (("default"))))))

(define-public crate-yamlate-0.1.1 (c (n "yamlate") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.3") (d #t) (k 0)))) (h "15c5zcgqs4wh26ybwnshvjjyzlz5ryz3p5kwifh4lbs3lg0h58yc") (f (quote (("default"))))))

