(define-module (crates-io ya ml yaml-peg) #:use-module (crates-io))

(define-public crate-yaml-peg-0.1.0 (c (n "yaml-peg") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1wkizb3cqp2mqi9n6jvl0hs7aa3gclhz0ip5pghwihg3zppvbnzp")))

(define-public crate-yaml-peg-0.2.0 (c (n "yaml-peg") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1dmjycxyw6vjh1913w75gk1s57sp4fc98n9vxszqq6wm4a7v56f6")))

(define-public crate-yaml-peg-0.3.0 (c (n "yaml-peg") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1f91336kydmplkqlwjccipxmlpsysnyg7dc5gzap8k9qzps7damb")))

(define-public crate-yaml-peg-0.4.0 (c (n "yaml-peg") (v "0.4.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0ni70pqv3s18ikmil3gjq9gsq1cxcza7ikmvkg25ag2k5qvmn455")))

(define-public crate-yaml-peg-0.5.0 (c (n "yaml-peg") (v "0.5.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1yfnxgw75ghx8ywd0b8m19kl0sh869rdw4nihpnd2s8kiy8dhc3k")))

(define-public crate-yaml-peg-0.5.1 (c (n "yaml-peg") (v "0.5.1") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0iaxna7w8l7nwnw9xi0mkpbak9ajzpl526zig6qd6xng4cxbp1cb")))

(define-public crate-yaml-peg-0.6.0 (c (n "yaml-peg") (v "0.6.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "15vx76ymz7r0wsn0acxkq72w8nvd859lw6jsx20ggjrcxg2y86yr")))

(define-public crate-yaml-peg-0.7.0 (c (n "yaml-peg") (v "0.7.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1yyla4vi0wqq6rgwqh2k010696rbw96q2bk03hcyj2paz280hn6c")))

(define-public crate-yaml-peg-0.7.1 (c (n "yaml-peg") (v "0.7.1") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)))) (h "130sv37yszvha6v176jz8bs2754yl3cszp0xg3pl6shrv6237010")))

(define-public crate-yaml-peg-0.8.0 (c (n "yaml-peg") (v "0.8.0") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)))) (h "09a4lc93xpy76ffppbkbmq07233k1xxf1kl129f6k6iahfvfpagm")))

(define-public crate-yaml-peg-0.9.0 (c (n "yaml-peg") (v "0.9.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)))) (h "1g67frlia2qm4a7l8kdxjljwlwprhis7g4cqq3pjxjshbrwjvd1a")))

(define-public crate-yaml-peg-0.10.0 (c (n "yaml-peg") (v "0.10.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)))) (h "03yq6nr7xx3v1wfcrhgingvzhkmw8m439ysk1ny17028s4j7xv8j")))

(define-public crate-yaml-peg-0.11.0 (c (n "yaml-peg") (v "0.11.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)))) (h "1hkkrgkw5s4di4gxgjavj4agdy1cl6ly147bxigl0c55k3yvwg40")))

(define-public crate-yaml-peg-0.12.0 (c (n "yaml-peg") (v "0.12.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)))) (h "0fzh0nl6yba73a610fwn8mbf0nm3cqj9ix3k54l381n0rg035cwl")))

(define-public crate-yaml-peg-0.13.0 (c (n "yaml-peg") (v "0.13.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "1r4y68lvg0iv7zmw0aikygri31fcj8sf0ngrh4s7lx3j5xzp9r8g") (f (quote (("serialize" "serde"))))))

(define-public crate-yaml-peg-0.14.0 (c (n "yaml-peg") (v "0.14.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "0vdpnvz5374h0673sqwva0x3c72s8iiadgxinbxr2lwysggg4nf3") (f (quote (("serialize" "serde"))))))

(define-public crate-yaml-peg-0.15.0 (c (n "yaml-peg") (v "0.15.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "1kv7wkj5r7pwxrzhzinrlxkmyalklrahl5xvh8gkkig0kwlc4f2i") (f (quote (("serialize" "serde"))))))

(define-public crate-yaml-peg-0.16.0 (c (n "yaml-peg") (v "0.16.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "0qb4hk4nbfsjk3y4c1fj1l8w1mfvp50lw87slys6gkzwvq47f575") (f (quote (("serde-std" "serde" "serde/std"))))))

(define-public crate-yaml-peg-0.17.0 (c (n "yaml-peg") (v "0.17.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "16qjjzf0i35ij2xhq0mnhnrwj4gxak7m46p4js7hd529f6b66a9x") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.18.0 (c (n "yaml-peg") (v "0.18.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "0ac45l8hsm3mb5lankyn4llk8dfzacqsz2lb3ppkswr9b3m2d2ql") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.19.0 (c (n "yaml-peg") (v "0.19.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "0n7ivjrx3hyab5drkvgzb8p0893zj4853c7kzl3n32a8069brvdj") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.20.0 (c (n "yaml-peg") (v "0.20.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "1ycb7xlm93k3y4s9r4kb1g413yyl9j7slq9lnnnybcjanl8vvvvl") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.21.0 (c (n "yaml-peg") (v "0.21.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "1x1p3h2jfjf1ggrb817izd05ys010m5ymmywhkcyfzjlghkrrrlb") (f (quote (("serde-std" "serde/std")))) (y #t)))

(define-public crate-yaml-peg-0.22.0 (c (n "yaml-peg") (v "0.22.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "135z0ljwz9arbk0x52gmq8bddw640z24li8mlvbnc6a8rnqdvglj") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.23.0 (c (n "yaml-peg") (v "0.23.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 2)))) (h "0s3c92qirm4vry04826rz0y5bj33x4jjgarvd3f55dw47480w3n8") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.23.2 (c (n "yaml-peg") (v "0.23.2") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "0hzwd4qak3plznwmy6kl0pahp1sa81bdrh4f5q6gy66akfbmpdx9") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.24.0 (c (n "yaml-peg") (v "0.24.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "092wichsgy5k3hsfcf3p32qdvhq7plrb9gpdyi1097ici63mfmiz") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.24.1 (c (n "yaml-peg") (v "0.24.1") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "1ii4na0aybk3bbj3jrn6rpfnrrm2smnwxa4v2i2w9y9v0ww29mvm") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.25.0 (c (n "yaml-peg") (v "0.25.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "0zkncqf7wi82hlj21w0m8lqbnjgc9a1phzgyx1cygrv1bmkhjfdg") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.26.0 (c (n "yaml-peg") (v "0.26.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "1d60kz19c5qmwnwk9a7rb0gnlzz5hxqzwc4qwsbmihf4228ypx03") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.26.1 (c (n "yaml-peg") (v "0.26.1") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "1j3fvyafsxp2npb8x2rwl4qmz49rm0flmp44pxq95a9hac3l1jf3") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.26.2 (c (n "yaml-peg") (v "0.26.2") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "0wqyzw1r72726h18dixr9sdin9crxal859442lhrjmb7axaixg9l") (f (quote (("serde-std" "serde/std"))))))

(define-public crate-yaml-peg-0.27.0 (c (n "yaml-peg") (v "0.27.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (k 2)))) (h "1b024856k4l9vn1jvcpg5n8b59127y4gn80irp300259flkd84z5") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.0 (c (n "yaml-peg") (v "1.0.0") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0zr1ilhid3qidajr9lwcg7xz2ylzjz9ajqgkajqn1jyxxz9hipj8") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.1 (c (n "yaml-peg") (v "1.0.1") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1z3rqyf128wr0c5lpwv00qb1s67pv7w4q598p4ld010vjzf52wkb") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.2 (c (n "yaml-peg") (v "1.0.2") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1lk6sz9afxv63da4smaaj3l2mapvli6ivg0547whxjhnxll3821j") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.3 (c (n "yaml-peg") (v "1.0.3") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "13q41gjl47h9qhi50323q5kgkxd437jzbd5cwwgnbdq01hjs9409") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.4 (c (n "yaml-peg") (v "1.0.4") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1q2v5a96pn7bkdg70cn97kqb25qz1im0b8r82yq9p091h5jij2nk") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.5 (c (n "yaml-peg") (v "1.0.5") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1fn4irbgwiykbvmnxpjs88cysja1hpkrmxa0x1fw7bsmicxbm4zs") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.6 (c (n "yaml-peg") (v "1.0.6") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1ri84hxb0z8qmmamb1ifprbch3mdlhn3zm9hlhhr0zdd8w2s0ggz") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.7 (c (n "yaml-peg") (v "1.0.7") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0ap31dfnqvrg7qykqwf77bdw03hvxilxznnrzxcjgwhn9x6f9ajs") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-yaml-peg-1.0.8 (c (n "yaml-peg") (v "1.0.8") (d (list (d (n "ritelinked") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "11pikh6in2iiszkkflppyv0w9qycdymn8jvymnc72lq9zq14lfqp") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

