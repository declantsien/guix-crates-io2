(define-module (crates-io ya ml yaml-validator) #:use-module (crates-io))

(define-public crate-yaml-validator-0.0.1 (c (n "yaml-validator") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0j8w4cb6rkch0h7n82x62dcnddsj1fgvj4lilvpx4mpdzmykyjzd")))

(define-public crate-yaml-validator-0.0.2 (c (n "yaml-validator") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0ra00pfy8k2k1mffaz4krrkl6kb6a430llhha10bm5sa5cxw6iny")))

(define-public crate-yaml-validator-0.0.3 (c (n "yaml-validator") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1s1jc5zn5iaqr33nzx1pngq741v9f1d5a5cwvyavjykv7wy03msq")))

(define-public crate-yaml-validator-0.0.4 (c (n "yaml-validator") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1amrvyqkiaavl49pqgv4dnq9dx32mlddviggyxrsz5lxqrabh3s7")))

(define-public crate-yaml-validator-0.1.0-beta.0 (c (n "yaml-validator") (v "0.1.0-beta.0") (d (list (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0snx3rypmbyasbzzrn7ig55kkxhhwfmw8ykqd72nh7bzhv46q374")))

(define-public crate-yaml-validator-0.1.0 (c (n "yaml-validator") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0b4h3d3bjvpxjgis75060l1ivrplv9iz09nyhrdfkfjv3k8rj5bp")))

(define-public crate-yaml-validator-0.2.0 (c (n "yaml-validator") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1xvqd8c61sj0ai0k0hpaa630qh7xnd4ypiy7yh29z1a0vf6h41q5") (f (quote (("default" "regex" "smallvec")))) (s 2) (e (quote (("smallvec" "dep:smallvec") ("regex" "dep:regex"))))))

