(define-module (crates-io ya ml yaml-front-matter) #:use-module (crates-io))

(define-public crate-yaml-front-matter-0.0.1 (c (n "yaml-front-matter") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "02rxll3v2pg5r5la0hj7rkkqf815532a5wwxrgibqfln2f44a33w")))

(define-public crate-yaml-front-matter-0.1.0-beta (c (n "yaml-front-matter") (v "0.1.0-beta") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "14v5xynxcym526nga0zh429w4v4kn7pidfp81ndzh9bajz4vsppr")))

(define-public crate-yaml-front-matter-0.1.0 (c (n "yaml-front-matter") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1bhb3q1jfzmbsajnsv6aacwdcd3vp2ggxyq1z7fkz3j35cnv6kx9")))

