(define-module (crates-io ya ml yaml-patcher) #:use-module (crates-io))

(define-public crate-yaml-patcher-0.1.0 (c (n "yaml-patcher") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "09v63lr8lrkjm334ljpik56r673xnq9h2s9grdg34qvn5h5yq4j3")))

(define-public crate-yaml-patcher-0.1.1 (c (n "yaml-patcher") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.49") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1iha99nl16lqdbp50lnclr9lnk28x8vmq3x2fsvglnv3hwbmzqsf")))

