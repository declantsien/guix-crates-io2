(define-module (crates-io ya ml yaml) #:use-module (crates-io))

(define-public crate-yaml-0.0.3 (c (n "yaml") (v "0.0.3") (h "0l3kxaxd1q4s5ji5f4v4vncnmkc4311r4hkc8ibv3mn8yg15gpzv")))

(define-public crate-yaml-0.0.4 (c (n "yaml") (v "0.0.4") (h "0k7br3nnz5mkzjmgg7y9x07yjii79h1n1f8hrr78g3am48a2j5v3")))

(define-public crate-yaml-0.0.5 (c (n "yaml") (v "0.0.5") (h "0bh9kcg8vzlp3wdszkbpdn1j0ban3kk9d7sx8a2dq0z07arpbqc2")))

(define-public crate-yaml-0.0.6 (c (n "yaml") (v "0.0.6") (h "0w1b965rsgb1s4l330npvv65fizlmy6x6vc0psbfwn5nqdn7n13d")))

(define-public crate-yaml-0.0.8 (c (n "yaml") (v "0.0.8") (d (list (d (n "regex") (r "~0.1.9") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.4") (d #t) (k 0)))) (h "0rj0ri7nsv0856rxsxqm79qnfd5yd2bh3kl1xm7d011n503gmbr7")))

(define-public crate-yaml-0.0.9 (c (n "yaml") (v "0.0.9") (d (list (d (n "regex") (r "~0.1.9") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.4") (d #t) (k 0)))) (h "169zw8yq9xg6sjff64xp86bc9g306j5r8x4jk2fx04w12scqmc7z")))

(define-public crate-yaml-0.0.10 (c (n "yaml") (v "0.0.10") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "02gwh7ads0iiy6z4ldf3lz8cmsn7rfbgci32z31nbqmfx4vgzqzr")))

(define-public crate-yaml-0.0.11 (c (n "yaml") (v "0.0.11") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "0xvz44h4m6mfxv69mm7mgvbblp2md5yaaanhqcs2fm5by0k9d25y")))

(define-public crate-yaml-0.0.12 (c (n "yaml") (v "0.0.12") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "0537ynkqlva6kcl1xqngrd4pd2myvs2ngq46z4sw5r5p7zbrnri4")))

(define-public crate-yaml-0.1.0 (c (n "yaml") (v "0.1.0") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "0iliyxc9r40xj943jvma2rrd483fvhss8g4p4d0iypqc8xnzycfh")))

(define-public crate-yaml-0.1.1 (c (n "yaml") (v "0.1.1") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "1992hyc4196kn97fb2f7v6a62hvnp00i5ajrpp56kvsr87h0vqzs")))

(define-public crate-yaml-0.1.2 (c (n "yaml") (v "0.1.2") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "1ghjhg6fd89bjdwgi90c5x1pmmpx5wpsx7rf1rlmkf7akyjzxrc8")))

(define-public crate-yaml-0.1.3 (c (n "yaml") (v "0.1.3") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "0c4xii3qz6yfwscd0g7x9wlkpidccnm9v92yaxp82cqbmhq54py9")))

(define-public crate-yaml-0.1.4 (c (n "yaml") (v "0.1.4") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "1a59s63qd7r3k9ii51ybqnkp3ia2yxg87sjf3d2bzmih3hwcw3gn")))

(define-public crate-yaml-0.2.0 (c (n "yaml") (v "0.2.0") (d (list (d (n "regex") (r "~0.1.12") (d #t) (k 0)) (d (n "regex_macros") (r "~0.1.6") (d #t) (k 0)))) (h "1szfi00n5rdra96z0ihi9jlmj4a728lm8avpw7lffjl3sbmc5kbj")))

(define-public crate-yaml-0.2.1 (c (n "yaml") (v "0.2.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1sgyd8zsy0xn5p3v6kyk644b540rpx5zrj204yy94086ykvnn8x1")))

(define-public crate-yaml-0.3.0 (c (n "yaml") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0378dnsf5fy33zwlrcydn80safjadvkdi299x2psg6knfmjq6glg")))

