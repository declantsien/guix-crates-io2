(define-module (crates-io ya ml yaml-hash) #:use-module (crates-io))

(define-public crate-yaml-hash-0.1.0 (c (n "yaml-hash") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.5.0") (d #t) (k 0)))) (h "1ksvjqnrznbi86jlmzckfgd290crvq244y2zcmcd9k5siq2m58s8")))

(define-public crate-yaml-hash-0.1.1 (c (n "yaml-hash") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.5.0") (d #t) (k 0)))) (h "1y95k1lmx3bmc4b2b7a18yanm2l7jk6s34abahc63q5smc1w002j")))

(define-public crate-yaml-hash-0.1.2 (c (n "yaml-hash") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.5.0") (d #t) (k 0)))) (h "1hm43bagqm372qs95mp9ndqvz746kfgsx24rnk6mjdzrk5snkx4z")))

(define-public crate-yaml-hash-0.2.0 (c (n "yaml-hash") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.5.0") (d #t) (k 0)))) (h "07yfqyxz6chq02n3306b1gb4wz7r81rlpj5mfam2q7j5j687mlmf")))

(define-public crate-yaml-hash-0.3.0 (c (n "yaml-hash") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "yaml-rust2") (r "^0.5.0") (d #t) (k 0)))) (h "1cjzn9r440d4xqcpj0mvm03kgpki1ah88xdj8q7sh4bgyf2n41kh")))

