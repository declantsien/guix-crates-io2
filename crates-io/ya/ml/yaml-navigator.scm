(define-module (crates-io ya ml yaml-navigator) #:use-module (crates-io))

(define-public crate-yaml-navigator-0.1.0 (c (n "yaml-navigator") (v "0.1.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "gat-lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "1dn2hhf4b0dbkayibbfmmcqn49d0bffqjdlv6lwac99q97hys76x")))

(define-public crate-yaml-navigator-0.2.0 (c (n "yaml-navigator") (v "0.2.0") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "gat-lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "16j9g423rpbbk8gicq1rhav2pf1prqhlm357idz3vjfk7lb2mq43")))

(define-public crate-yaml-navigator-0.2.1 (c (n "yaml-navigator") (v "0.2.1") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "gat-lending-iterator") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)))) (h "0ijz8p9kqgzv4plqirn11wpv7jlgm44c8lzxy84mxbvm778jx6wp")))

