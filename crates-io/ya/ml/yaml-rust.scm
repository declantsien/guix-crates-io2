(define-module (crates-io ya ml yaml-rust) #:use-module (crates-io))

(define-public crate-yaml-rust-0.2.0 (c (n "yaml-rust") (v "0.2.0") (h "10jg0pvlqf5wszj5152vddq8l0pvphkqk5pb941gnc44k57capzw")))

(define-public crate-yaml-rust-0.2.1 (c (n "yaml-rust") (v "0.2.1") (h "0ijchqc9d3djp44nif1ynvw7xknyq845yybcxl8zm499ng5pj4h3")))

(define-public crate-yaml-rust-0.2.2 (c (n "yaml-rust") (v "0.2.2") (h "1r7a1097q4llmddqa4xgflamcyp64xv5jsvbx1v4774f9b7z4l47")))

(define-public crate-yaml-rust-0.3.0 (c (n "yaml-rust") (v "0.3.0") (h "1vmwrdqdfq486izb0priq89599kkplfbya9wwya7k9r9a4q42rzr")))

(define-public crate-yaml-rust-0.3.2 (c (n "yaml-rust") (v "0.3.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.0.9") (o #t) (d #t) (k 0)))) (h "0bhf6gip30bmy57cy25awgp8kbn481gjjwmkfy154c5j80kqjw17") (f (quote (("preserve_order" "linked-hash-map"))))))

(define-public crate-yaml-rust-0.3.3 (c (n "yaml-rust") (v "0.3.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.0.9") (o #t) (d #t) (k 0)))) (h "132rhh8d6zpx4f6lz69mcm4j48383vah9w7bg1hvwnddfps15zpb") (f (quote (("preserve_order" "linked-hash-map"))))))

(define-public crate-yaml-rust-0.3.4 (c (n "yaml-rust") (v "0.3.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r ">= 0.0.9, < 0.4") (o #t) (d #t) (k 0)))) (h "1zd8w95v4v4zjffjzw9dvzmm3xj4gsrjz6f0hgf133d56cxfl71p") (f (quote (("preserve_order" "linked-hash-map"))))))

(define-public crate-yaml-rust-0.3.5 (c (n "yaml-rust") (v "0.3.5") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r ">= 0.0.9, < 0.4") (o #t) (d #t) (k 0)))) (h "14m9dzwb8fb05f4jjb4nqp49rxd9c5vcmwpv3a04d2y5iphncqz6") (f (quote (("preserve_order" "linked-hash-map"))))))

(define-public crate-yaml-rust-0.4.0 (c (n "yaml-rust") (v "0.4.0") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0a6nwk5gz7l0l91zd9fgq7kddn1832pzjv296g86w9ja3bp3iasp")))

(define-public crate-yaml-rust-0.4.1 (c (n "yaml-rust") (v "0.4.1") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1qgrm4533pcj52djznbr1bpwlp017ypkhsw4i2azmjmymm4as3zy")))

(define-public crate-yaml-rust-0.4.2 (c (n "yaml-rust") (v "0.4.2") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0ki506c74hd4djv06wzbrbi2bn62djxf1h2yjslpvl0mapdz1b4m")))

(define-public crate-yaml-rust-0.4.3 (c (n "yaml-rust") (v "0.4.3") (d (list (d (n "linked-hash-map") (r ">= 0.0.9, < 0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0ka3qhqc5lvk3hz14wmsj32jhmh44blcbfrx5hfxli2gg38kv4k5")))

(define-public crate-yaml-rust-0.4.4 (c (n "yaml-rust") (v "0.4.4") (d (list (d (n "linked-hash-map") (r ">=0.0.9, <0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "038byay0dxsz6isckviz4qshfpyjqmyvda7pq96i6d53y4ickw1r")))

(define-public crate-yaml-rust-0.4.5 (c (n "yaml-rust") (v "0.4.5") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "118wbqrr4n6wgk5rjjnlrdlahawlxc1bdsx146mwk8f79in97han")))

