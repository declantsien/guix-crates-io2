(define-module (crates-io ya ml yaml-patch) #:use-module (crates-io))

(define-public crate-yaml-patch-0.1.0 (c (n "yaml-patch") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rkf2nx76rp4r6cdppgzij53f6l1mhbi2xcs5d5b0aga6c0kns5z")))

(define-public crate-yaml-patch-0.1.1 (c (n "yaml-patch") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n7awqyzwbacnybkx5jg4bn8r2xqamzd138kwl06f43dvwdx4inw")))

