(define-module (crates-io ya ml yamlette) #:use-module (crates-io))

(define-public crate-yamlette-0.0.1 (c (n "yamlette") (v "0.0.1") (d (list (d (n "fraction") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (f (quote ("bigint"))) (k 0)) (d (n "skimmer") (r "^0.0.1") (d #t) (k 0)) (d (n "twine") (r "^0.0.1") (d #t) (k 0)))) (h "1jq3pcvcc2xnfy8kghb61zakiwiwrgazhzq2dzcn91fvnz13s1nr") (f (quote (("dev"))))))

(define-public crate-yamlette-0.0.2 (c (n "yamlette") (v "0.0.2") (d (list (d (n "fraction") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (f (quote ("bigint"))) (k 0)) (d (n "skimmer") (r "^0.0.1") (d #t) (k 0)) (d (n "twine") (r "^0.0.1") (d #t) (k 0)))) (h "1zkq07gx243i2230l0xyvvi6qrj9xigv350hgspdf7jnl3p5zawv") (f (quote (("dev"))))))

(define-public crate-yamlette-0.0.3 (c (n "yamlette") (v "0.0.3") (d (list (d (n "fraction") (r "^0.3.5") (d #t) (k 0)) (d (n "num") (r "^0.1.37") (f (quote ("bigint"))) (k 0)) (d (n "skimmer") (r "^0.0.2") (d #t) (k 0)))) (h "12zfcwvg84xzb0d2p7s61l4grx46xif7xjk2fns21vqvf7vj6yw3") (f (quote (("test_savant" "onetest") ("test_sage" "onetest") ("test_reader" "onetest") ("test_orchestra" "onetest") ("test_face" "onetest") ("test_book" "onetest") ("onetest") ("dev"))))))

(define-public crate-yamlette-0.0.4 (c (n "yamlette") (v "0.0.4") (d (list (d (n "fraction") (r "^0.3.7") (d #t) (k 0)) (d (n "num") (r "^0.1.37") (f (quote ("bigint"))) (k 0)) (d (n "skimmer") (r "^0.0.2") (d #t) (k 0)))) (h "0zny5a3g3d4cz443lh9skd9vsh8b7vzzwa3slhj2xxjsbd6x81p5") (f (quote (("test_savant" "onetest") ("test_sage" "onetest") ("test_reader" "onetest") ("test_orchestra" "onetest") ("test_face" "onetest") ("test_book" "onetest") ("onetest") ("dev"))))))

(define-public crate-yamlette-0.0.5 (c (n "yamlette") (v "0.0.5") (d (list (d (n "fraction") (r "^0.3.7") (d #t) (k 0)) (d (n "num") (r "^0.1.37") (f (quote ("bigint"))) (k 0)) (d (n "skimmer") (r "^0.0.2") (d #t) (k 0)))) (h "1nfxc194vkys29bh9wq8qqnk1qra2959962ajdvzdcpy91kjfgq4") (f (quote (("test_savant" "onetest") ("test_sage" "onetest") ("test_reader" "onetest") ("test_orchestra" "onetest") ("test_face" "onetest") ("test_book" "onetest") ("onetest") ("dev"))))))

(define-public crate-yamlette-0.0.8 (c (n "yamlette") (v "0.0.8") (d (list (d (n "base91") (r "^0.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fraction") (r "^0.6.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (f (quote ("num-bigint" "std"))) (k 0)) (d (n "skimmer") (r "^0.0.2") (d #t) (k 0)))) (h "14hyp02rrdiyxqirm400rsssjhrcjs55n3i2md52vf7dxfzi70vf") (f (quote (("test_savant") ("test_sage") ("test_reader") ("test_orchestra") ("test_face") ("test_book") ("onetest" "test_reader" "test_savant" "test_sage" "test_book" "test_orchestra" "test_face") ("dev"))))))

(define-public crate-yamlette-0.1.0 (c (n "yamlette") (v "0.1.0") (d (list (d (n "fraction") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4") (f (quote ("num-bigint" "std"))) (k 0)) (d (n "skimmer") (r "^0.0.3") (d #t) (k 0)))) (h "1inricimav8bpxg38m8a5h806y80hnngajikhq5zr35bqch731g8")))

(define-public crate-yamlette-0.1.1 (c (n "yamlette") (v "0.1.1") (d (list (d (n "fraction") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4") (f (quote ("num-bigint" "std"))) (k 0)) (d (n "skimmer") (r "^0.0.3") (d #t) (k 0)))) (h "1280zdrlzmzpkrd6n7zzlfcxpybs2knd84w36vrv9dxj8am9vz1j")))

