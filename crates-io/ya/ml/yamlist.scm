(define-module (crates-io ya ml yamlist) #:use-module (crates-io))

(define-public crate-yamlist-0.1.0 (c (n "yamlist") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "motion_list_rs") (r "^1.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "07b2nhzakmcddxwp3hb2bj7ljdc21xy1q7g7m010p7rhsdbvwq4f")))

(define-public crate-yamlist-0.1.1 (c (n "yamlist") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "motion_list_rs") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "03br4p1r1q83x0jb4r4b5ybqgmb4fnghm5w2i1sqzvy1103wihbq")))

(define-public crate-yamlist-0.1.2 (c (n "yamlist") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "motion_list_rs") (r "^1.4.2") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1ngdfmjy85rm2aqcd1qnz2km4s9y03pgvjwvav68chhpf52jlghm")))

