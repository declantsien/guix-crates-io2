(define-module (crates-io ya ml yaml_file_handler) #:use-module (crates-io))

(define-public crate-yaml_file_handler-0.1.0 (c (n "yaml_file_handler") (v "0.1.0") (d (list (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "1vcgydxgsgdlljkwyfbxkgbzk7lyv8hhizj4vpgcbmjifby2xv3g")))

(define-public crate-yaml_file_handler-0.1.1 (c (n "yaml_file_handler") (v "0.1.1") (d (list (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "13kq381nykfg6spmkr7h2xz34yjgrbnq2bd10gzvqmv23l2rcjbz")))

(define-public crate-yaml_file_handler-0.1.3 (c (n "yaml_file_handler") (v "0.1.3") (d (list (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "0n1cggmsy7gr5r7204z6plcjgswac48lichh3s7h6y3cyqxfq8yd")))

(define-public crate-yaml_file_handler-0.1.4 (c (n "yaml_file_handler") (v "0.1.4") (d (list (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "0iw7634728lvaswnx6k4xvsidqlx78j1iw24xkrpg91ljm28lcsl")))

