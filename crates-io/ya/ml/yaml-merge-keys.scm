(define-module (crates-io ya ml yaml-merge-keys) #:use-module (crates-io))

(define-public crate-yaml-merge-keys-0.1.0 (c (n "yaml-merge-keys") (v "0.1.0") (d (list (d (n "error-chain") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "itertools") (r "~0.5") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "~0.3") (d #t) (k 0)))) (h "13kr689ncz39zng46g3mgqqdvvqwzv7z5kss3kin0azz4k0dfnjh")))

(define-public crate-yaml-merge-keys-0.2.0 (c (n "yaml-merge-keys") (v "0.2.0") (d (list (d (n "error-chain") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "itertools") (r "~0.6") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "~0.3") (d #t) (k 0)))) (h "18kczkpf4ardbai10i0myllahwld68q536fashm4779swh1wq87b")))

(define-public crate-yaml-merge-keys-0.2.1 (c (n "yaml-merge-keys") (v "0.2.1") (d (list (d (n "error-chain") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "itertools") (r "~0.7") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "~0.3") (d #t) (k 0)))) (h "15bz2wfmqdibphnpa9634mp5l6rds8j5xahfmpvx9mpqqy2ac5m7")))

(define-public crate-yaml-merge-keys-0.3.0 (c (n "yaml-merge-keys") (v "0.3.0") (d (list (d (n "error-chain") (r ">= 0.9, < 0.11") (d #t) (k 0)) (d (n "itertools") (r "~0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "~0.4") (d #t) (k 0)))) (h "0wqfmhkqaqbgr1255ryhi2anv5ad1i51x67gp0zn6z507q6j7p06")))

(define-public crate-yaml-merge-keys-0.4.0 (c (n "yaml-merge-keys") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.4") (d #t) (k 0)))) (h "1ylccnxg4nqlq2gwx2w3bwng9shh98hn2xwc942bglisp8c372ar")))

(define-public crate-yaml-merge-keys-0.4.1 (c (n "yaml-merge-keys") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.4") (d #t) (k 0)))) (h "010050idflm63g6ibajd518fw7qq957pba74kws8yndvr5ynl8zx")))

(define-public crate-yaml-merge-keys-0.5.0 (c (n "yaml-merge-keys") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.4.5") (d #t) (k 0)))) (h "0hiijr15hpmij5jn5gpa1a0y9m13h1ifqdiqlw78h3hly43ax132")))

(define-public crate-yaml-merge-keys-0.5.1 (c (n "yaml-merge-keys") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.8.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.4.5") (d #t) (k 0)))) (h "0ni8dkw019yxminn59pq6d499flfv3hh8pnfxdqfzahw0w2x4ixg")))

(define-public crate-yaml-merge-keys-0.6.0 (c (n "yaml-merge-keys") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.9.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.4.5") (d #t) (k 0)))) (h "1483qd0p8hbvcxba5kq7z7l3h5pvif4z497cjnn487m7llcmgfjb")))

(define-public crate-yaml-merge-keys-0.7.0 (c (n "yaml-merge-keys") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "serde_yaml") (r "~0.9.28") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "~0.8") (d #t) (k 0) (p "yaml-rust2")))) (h "1y4fmhsd1qmi1pj3lp714vh0p39kcv4kq09f5ri7c0hdvx871njk")))

