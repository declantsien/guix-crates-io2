(define-module (crates-io ya ml yaml-config) #:use-module (crates-io))

(define-public crate-yaml-config-1.0.0 (c (n "yaml-config") (v "1.0.0") (d (list (d (n "enum-as-inner") (r "^0.5.1") (d #t) (k 0)) (d (n "envtestkit") (r "^1.1.2") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0r9ka91k7hrzxlsx2xqsynvj77am9hky75az9l39bvjlj49rvlvh")))

