(define-module (crates-io ya ml yaml-rust-davvid) #:use-module (crates-io))

(define-public crate-yaml-rust-davvid-0.4.5 (c (n "yaml-rust-davvid") (v "0.4.5") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1lgbxgb408l84mfmijy37rxff4d28jcwf5d5cmwhz7ah0wg009s2")))

(define-public crate-yaml-rust-davvid-0.5.0 (c (n "yaml-rust-davvid") (v "0.5.0") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1c7filfasghwqgyw788ml17jx538fvvny61la5dsq7z9hnfpyy45")))

(define-public crate-yaml-rust-davvid-0.5.1 (c (n "yaml-rust-davvid") (v "0.5.1") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0ayf3538lcf94qqwv0n6j2wbmwpckfp0zrprmjk12af0wp24h1pz")))

(define-public crate-yaml-rust-davvid-0.5.2 (c (n "yaml-rust-davvid") (v "0.5.2") (d (list (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1h9kb03k77vl08bb120whgn3nzx2nla9ma08jbaywvcr3plkbygd")))

(define-public crate-yaml-rust-davvid-0.6.0 (c (n "yaml-rust-davvid") (v "0.6.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1hr26vif1m3jz0ambdipz61y717rgy9k4bq5q49frc10j5ya3y62")))

(define-public crate-yaml-rust-davvid-0.6.1 (c (n "yaml-rust-davvid") (v "0.6.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "07n51yhsd047lrn5bnxac0mmpvc19xznnh7qkzzgfyryi7m0bz7a")))

(define-public crate-yaml-rust-davvid-0.6.2 (c (n "yaml-rust-davvid") (v "0.6.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1bfc993l48mlf48l1vij4a2gf3v0r52qkrrrldpdc9bbpc1crca8")))

(define-public crate-yaml-rust-davvid-0.6.3 (c (n "yaml-rust-davvid") (v "0.6.3") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1aqqjhgpvjjnqgaq5g0z5fyffdvpv65vkmnindc48h6w8i4ris9p")))

(define-public crate-yaml-rust-davvid-0.6.4 (c (n "yaml-rust-davvid") (v "0.6.4") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "1p1ck4fc46q7lcca60szfrzfhqy8f67421y2kkdg6zamq3hs5ap4")))

