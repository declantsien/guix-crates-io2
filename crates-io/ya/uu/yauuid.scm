(define-module (crates-io ya uu yauuid) #:use-module (crates-io))

(define-public crate-yauuid-0.1.0 (c (n "yauuid") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "md-5") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (f (quote ("v1" "v3" "v4" "v5"))) (d #t) (k 2)))) (h "07l05g5w8mayyh56nq7a6qxya265ipsqi2zavj1n61c7wcf9bi44")))

(define-public crate-yauuid-0.2.0 (c (n "yauuid") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "md-5") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (f (quote ("v1" "v3" "v4" "v5"))) (d #t) (k 2)))) (h "026rmlax93wmqfvkq8hpjjw8sbpqf0hx0dvbipvvvvgmp8f8q5g5")))

(define-public crate-yauuid-0.2.1 (c (n "yauuid") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "md-5") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (f (quote ("v1" "v3" "v4" "v5"))) (d #t) (k 2)))) (h "032cci32xg3ikci35c7g0j3r1bh2wck88sz4zmpm537bb6d11w72")))

