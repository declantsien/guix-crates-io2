(define-module (crates-io ya tl yatlv) #:use-module (crates-io))

(define-public crate-yatlv-1.0.0 (c (n "yatlv") (v "1.0.0") (h "14spy2gj4nnj9kbyb8clxmg4rfzwi948vibjw6fb6f46q3h48wsr")))

(define-public crate-yatlv-1.1.0 (c (n "yatlv") (v "1.1.0") (h "1pi8f9g57y8g5awc561p8ww2q9wnp2wmz97fmfdr7gzbmkn2r7jn")))

(define-public crate-yatlv-1.2.0 (c (n "yatlv") (v "1.2.0") (d (list (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "00wvzsr5w0plbdyq1rdqbl79sw4dysddbxb4l6kpq7qrrgz8cm6m") (f (quote (("default" "uuid"))))))

(define-public crate-yatlv-1.3.0 (c (n "yatlv") (v "1.3.0") (d (list (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0ny4z9k1b8pzd8k4p0q2pygb8k418s0zrih37q1kqs26vs8xjm7k") (f (quote (("default" "uuid"))))))

