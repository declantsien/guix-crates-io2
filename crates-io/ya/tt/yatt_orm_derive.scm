(define-module (crates-io ya tt yatt_orm_derive) #:use-module (crates-io))

(define-public crate-yatt_orm_derive-0.1.0 (c (n "yatt_orm_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qlk2854xfg9n54k8y4yfsdh71d4xhwljm8wfpqzsp1zjfxmwqli") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-yatt_orm_derive-0.2.0 (c (n "yatt_orm_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16s341hqwjizvwvhx533aysnss04missp7hnywsc79ffbzd3pxmz") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-yatt_orm_derive-0.2.1 (c (n "yatt_orm_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wxqhqb0abar14q2l1s67y0981z4808dfv68krvs2flj162visjp") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-yatt_orm_derive-0.3.0 (c (n "yatt_orm_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wbzk7vjpqhcsrqj7bq0yf9z79in0xmxpw278bxy358mn87sbhv4") (f (quote (("proc-macro" "proc-macro2/proc-macro") ("default" "proc-macro"))))))

