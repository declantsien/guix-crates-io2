(define-module (crates-io ya tt yatt_orm) #:use-module (crates-io))

(define-public crate-yatt_orm-0.1.0 (c (n "yatt_orm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "yatt_orm_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0agijia1v2lh2v5abl0lq6mayjrcav8hlad3swda0vmpv4g3rh9d")))

(define-public crate-yatt_orm-0.2.0 (c (n "yatt_orm") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yatt_orm_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0ra66lymkbqsbd19pszxnm0wa12381i18ws99pkbzi6nq45f441r")))

(define-public crate-yatt_orm-0.2.1 (c (n "yatt_orm") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yatt_orm_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1bgzw3rb7xgrf0ya7vr4xzyksh98cbnqp9i8hljkygygc7v43zzy")))

(define-public crate-yatt_orm-0.3.0 (c (n "yatt_orm") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yatt_orm_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1k3zwvjhd06k2r55j3zskvy853gaya0sj7sgz2bbqp1c3jffcfcf")))

(define-public crate-yatt_orm-0.4.0 (c (n "yatt_orm") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23") (f (quote ("chrono" "bundled"))) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yatt_orm_derive") (r "^0.3") (d #t) (k 0)))) (h "135pysm310hw5rgfj3lk0xib2n1yaj210j6w6cr2gr5k7204dbiv")))

(define-public crate-yatt_orm-0.5.0 (c (n "yatt_orm") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23") (f (quote ("chrono" "bundled"))) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yatt_orm_derive") (r "^0.3") (d #t) (k 0)))) (h "1nlily78k7pddhkdv1486s39k3bbr7hwpvhq0piwggj8qabd8h3z")))

(define-public crate-yatt_orm-0.5.1 (c (n "yatt_orm") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("chrono" "bundled"))) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "yatt_orm_derive") (r "^0.3") (d #t) (k 0)))) (h "1fa7nydy1nw70r18n8ns40h2bsgknc0v30ck308nv1yb83r3yczw")))

