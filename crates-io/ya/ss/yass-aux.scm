(define-module (crates-io ya ss yass-aux) #:use-module (crates-io))

(define-public crate-yass-aux-0.1.0 (c (n "yass-aux") (v "0.1.0") (h "13p655yrairwpl021v55xsl0pmzis2ljrnj388mri88mw3dpn83y") (y #t)))

(define-public crate-yass-aux-0.1.1 (c (n "yass-aux") (v "0.1.1") (h "1hpcv0mrvf8yka219bf73apwcfz15djkvna8kqdszp82dqi6mn1r") (y #t)))

