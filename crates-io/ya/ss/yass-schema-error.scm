(define-module (crates-io ya ss yass-schema-error) #:use-module (crates-io))

(define-public crate-yass-schema-error-0.1.0 (c (n "yass-schema-error") (v "0.1.0") (d (list (d (n "yass") (r "= 0.1.0") (d #t) (k 0)))) (h "1jy6q5y67qnil6y3331lnlx6k013ydwffcnac66p191akypj09bp") (y #t)))

(define-public crate-yass-schema-error-0.1.1 (c (n "yass-schema-error") (v "0.1.1") (d (list (d (n "yass") (r "= 0.1.1") (d #t) (k 0)))) (h "0gds3f88pji929yjc8nbk6wy6cfwy7xx084kcf75073w6g9zvw0d") (y #t)))

