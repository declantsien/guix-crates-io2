(define-module (crates-io ya ss yassg) #:use-module (crates-io))

(define-public crate-yassg-0.1.0 (c (n "yassg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1sjpbdww8c0znhxffyhd075an4lwprhq2sw5w2wai8l5gwlr3f0g")))

(define-public crate-yassg-0.1.1 (c (n "yassg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0x0qp6wndb57gi535xhgnpsllj1x43g9h3l0q64yn4hl8dvn62ma")))

(define-public crate-yassg-0.2.0 (c (n "yassg") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "06qg5404avhcd88c5fqs22vp73pnkwcqbasxszjlawiymb9095sx")))

