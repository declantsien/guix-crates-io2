(define-module (crates-io ya ss yass-codegen) #:use-module (crates-io))

(define-public crate-yass-codegen-0.1.0 (c (n "yass-codegen") (v "0.1.0") (h "0y7rk2v54mlnqa6rgs6c3y5v2awsgfwkkf9v3mswl5lfbvh3akwf") (y #t)))

(define-public crate-yass-codegen-0.1.1 (c (n "yass-codegen") (v "0.1.1") (h "16kq2x37778h3iyabjsffg8x9i80q4dqvxjigyzgpkcsr5hax8av") (y #t)))

