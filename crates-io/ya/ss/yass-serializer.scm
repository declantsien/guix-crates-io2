(define-module (crates-io ya ss yass-serializer) #:use-module (crates-io))

(define-public crate-yass-serializer-0.1.0 (c (n "yass-serializer") (v "0.1.0") (d (list (d (n "yass") (r "= 0.1.0") (d #t) (k 0)) (d (n "yass-parser") (r "= 0.1.0") (d #t) (k 2)))) (h "16zh3kicwf31vl7bv1bqzqqqz1mbydixvmlrm7zd7hif6xla92py") (y #t)))

(define-public crate-yass-serializer-0.1.1 (c (n "yass-serializer") (v "0.1.1") (d (list (d (n "yass") (r "= 0.1.1") (d #t) (k 0)) (d (n "yass-parser") (r "= 0.1.1") (d #t) (k 2)))) (h "12d2r7ys09jn1wmh59z94l8jz6sf6g7ik2kyniqpwzd5kypjnhab") (y #t)))

