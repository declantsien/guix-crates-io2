(define-module (crates-io ya ss yass-parser) #:use-module (crates-io))

(define-public crate-yass-parser-0.1.0 (c (n "yass-parser") (v "0.1.0") (d (list (d (n "yass") (r "= 0.1.0") (d #t) (k 0)))) (h "10qxnasff9g8pi2ahyb2lyir64kbd28i15gzykcp3gqbk74m9r6w") (y #t)))

(define-public crate-yass-parser-0.1.1 (c (n "yass-parser") (v "0.1.1") (d (list (d (n "yass") (r "= 0.1.1") (d #t) (k 0)))) (h "0rm7ybx64fimkfrkhbgnsnbq6daa5qivmad0j2i8ijnxqli60w9s") (y #t)))

