(define-module (crates-io ya ss yass) #:use-module (crates-io))

(define-public crate-yass-0.1.0 (c (n "yass") (v "0.1.0") (h "0wfwsfqc7wx970xl25qg8vyspx0qaxvxq95i4jb5zb7jmlgchmk9") (y #t)))

(define-public crate-yass-0.1.1 (c (n "yass") (v "0.1.1") (h "0r8fa978zinnml72g56wsjs7ck6l56faa7h35zc6yl7hpxz5567l") (y #t)))

