(define-module (crates-io ya ws yaws) #:use-module (crates-io))

(define-public crate-yaws-0.0.0 (c (n "yaws") (v "0.0.0") (d (list (d (n "lunatic") (r "^0.9") (d #t) (k 0)))) (h "1kccrkynmjczmdw95c6wdbma8v7x4m8k17iwdyg2aw6xvjmyzi0y")))

(define-public crate-yaws-0.0.0-202301.Jan.15 (c (n "yaws") (v "0.0.0-202301.Jan.15") (h "126hykwlcaawnyagyjyq7ql40vhmnynaqca2jk9a6qd3bw12rf45")))

