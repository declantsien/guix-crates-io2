(define-module (crates-io ya ws yaws-spec-h1) #:use-module (crates-io))

(define-public crate-yaws-spec-h1-0.0.0-202301.Jan.15 (c (n "yaws-spec-h1") (v "0.0.0-202301.Jan.15") (d (list (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4") (d #t) (k 0)))) (h "0rs5g0piii57jnd20jclmgmqh3bwa4dva1xxzsicxhk6qlan205c")))

