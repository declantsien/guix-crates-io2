(define-module (crates-io ya da yadacha) #:use-module (crates-io))

(define-public crate-yadacha-0.0.1 (c (n "yadacha") (v "0.0.1") (d (list (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 2)))) (h "1fsagcsdws40a4wqsawwd88k07s7xryg3xizcbq67br4nph7apqi") (f (quote (("cli" "getrandom" "memmap")))) (y #t)))

(define-public crate-yadacha-0.0.2 (c (n "yadacha") (v "0.0.2") (d (list (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 2)))) (h "0f84xpk273fk987g48rj1xmg2bfi5ar9s1xl5h4rr67qywq6673k") (f (quote (("cli" "getrandom" "memmap")))) (y #t)))

(define-public crate-yadacha-0.0.3 (c (n "yadacha") (v "0.0.3") (d (list (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 2)))) (h "1yx46v3c6z0n1d610wqzgqh0dp8yd32lnxjmzy8fr3i8z10sa5xa") (f (quote (("cli" "getrandom" "memmap")))) (y #t)))

(define-public crate-yadacha-0.0.4 (c (n "yadacha") (v "0.0.4") (d (list (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 2)))) (h "1smfjfz4ipc8nc2ydya4zm35mwrspsb4k21av9ip4chvvahrijjb") (f (quote (("cli" "getrandom" "memmap"))))))

(define-public crate-yadacha-0.0.5 (c (n "yadacha") (v "0.0.5") (d (list (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.22.0") (d #t) (k 2)))) (h "09iqvwq57klnlgij5dynzchd2zfpqwdi1xvw638ac06ly449zy5x") (f (quote (("cli" "getrandom" "memmap"))))))

