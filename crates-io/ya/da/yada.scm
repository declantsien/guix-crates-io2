(define-module (crates-io ya da yada) #:use-module (crates-io))

(define-public crate-yada-0.1.0 (c (n "yada") (v "0.1.0") (h "1ak7by18n26snzq14xd6c4ci9xrpn0j2kc3iqbp705yni5bzhqjn")))

(define-public crate-yada-0.2.0 (c (n "yada") (v "0.2.0") (h "0xlnqhpsv5c6wqagdxxf32njzgfbiwhfwqb3ljl7s59xzgg8sqkm")))

(define-public crate-yada-0.3.0 (c (n "yada") (v "0.3.0") (h "1xh92bmpjy3r8fb4yfnjb60v3s569v8ym8irrczfyp5ydsi5mgw1")))

(define-public crate-yada-0.3.1 (c (n "yada") (v "0.3.1") (h "08lnd1dv7rjxi05xrzw703kwix6j2pvdhikb79d28154x94585v6")))

(define-public crate-yada-0.3.2 (c (n "yada") (v "0.3.2") (h "05hagr5p19ladllzc8xypa03bg4bjhs24z7lmb1rjcb09dnyd068")))

(define-public crate-yada-0.4.0 (c (n "yada") (v "0.4.0") (h "1yrc1zr3i8wb41iypk9ri7fb364b7i55765x7vnca0s0cgqi3m1b")))

(define-public crate-yada-0.4.1 (c (n "yada") (v "0.4.1") (h "19bi13skglzsgvx5sdnbnf02xg3md86l538z9yz829w97mwvcyy8")))

(define-public crate-yada-0.5.0 (c (n "yada") (v "0.5.0") (h "1hc5wl40406fi6d2ffchgxa4i034wfx4b5gdf2v2mgvvlnvjrldn")))

(define-public crate-yada-0.5.1 (c (n "yada") (v "0.5.1") (h "1pgzmm965f5396q1mj5rfbxmmd7hmnynr435hx8h5a28ksyi3ldf")))

