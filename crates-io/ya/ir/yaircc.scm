(define-module (crates-io ya ir yaircc) #:use-module (crates-io))

(define-public crate-yaircc-0.0.0 (c (n "yaircc") (v "0.0.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("io-compat"))) (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 2)) (d (n "tokio") (r "^0.1.21") (d #t) (k 2)))) (h "1i65naiijmzmkpcycwq25kd1irdxydiqy72dvxiifsvn549kzga6")))

(define-public crate-yaircc-0.0.1 (c (n "yaircc") (v "0.0.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("io-compat"))) (d #t) (k 2)) (d (n "memchr") (r "^2.2.1") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.8") (d #t) (k 2)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)))) (h "0dbq88lslx53phr9qbfv3k0l5bip716hwdl7v45bchqszi7rpksv")))

(define-public crate-yaircc-0.1.0 (c (n "yaircc") (v "0.1.0") (d (list (d (n "async-std") (r "^0.99.12") (d #t) (k 2)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "memchr") (r "^2.2.1") (d #t) (k 0)))) (h "0wfxg69icdigxswcwhaxzwbw358zy5bx41cdfmcsklx4lc0ylz0l")))

