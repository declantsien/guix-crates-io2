(define-module (crates-io ya ar yaar) #:use-module (crates-io))

(define-public crate-yaar-0.1.0 (c (n "yaar") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (o #t) (t "cfg(unix)") (k 0)) (d (n "lock_api") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (o #t) (t "cfg(windows)") (k 0)))) (h "0qs0gg991ypdldnxz2ir1c355n0kvbx789yicmb5nxc9sgkjnfmg") (f (quote (("serial") ("parallel" "lock_api") ("default" "serial" "parallel"))))))

(define-public crate-yaar-0.1.1 (c (n "yaar") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (o #t) (t "cfg(unix)") (k 0)) (d (n "lock_api") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (o #t) (t "cfg(windows)") (k 0)))) (h "0hr3605j7nvafi6404pf08z6sfdagak0s5dscyk53zrqfkv0yd1c") (f (quote (("serial") ("parallel" "lock_api") ("default" "serial" "parallel"))))))

(define-public crate-yaar-0.1.1-alpha.1 (c (n "yaar") (v "0.1.1-alpha.1") (d (list (d (n "libc") (r "^0.2") (o #t) (t "cfg(unix)") (k 0)) (d (n "lock_api") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (o #t) (t "cfg(windows)") (k 0)))) (h "1a0hr7nadpfb93dfsis18k3pwja84p7w7h5carb23lc2is4l2wn4") (f (quote (("serial") ("parallel" "lock_api") ("default" "serial" "parallel"))))))

