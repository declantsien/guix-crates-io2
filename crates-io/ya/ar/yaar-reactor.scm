(define-module (crates-io ya ar yaar-reactor) #:use-module (crates-io))

(define-public crate-yaar-reactor-0.1.1 (c (n "yaar-reactor") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (o #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (o #t) (t "cfg(windows)") (k 0)))) (h "11sgwabgahsnli99sp7hvmgivriqcj0l9dfz2pdkhjpi21ccs0cw") (f (quote (("os" "libc" "winapi") ("default" "os"))))))

(define-public crate-yaar-reactor-0.1.2 (c (n "yaar-reactor") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (o #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (o #t) (t "cfg(windows)") (k 0)))) (h "0d0w136zymi1sfarglizcphv1qsisaw5ppi4v672mabbg89smc92") (f (quote (("os" "libc" "winapi") ("default" "os"))))))

(define-public crate-yaar-reactor-0.1.2-alpha.1 (c (n "yaar-reactor") (v "0.1.2-alpha.1") (d (list (d (n "libc") (r "^0.2") (o #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (o #t) (t "cfg(windows)") (k 0)))) (h "0c4w7zbwkqyidavfrf4wkzr0cgbqpi9cyrw30rf3z3pn4jw4p5gj") (f (quote (("os" "libc" "winapi") ("default" "os"))))))

