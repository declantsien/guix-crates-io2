(define-module (crates-io ya st yastl) #:use-module (crates-io))

(define-public crate-yastl-0.1.0 (c (n "yastl") (v "0.1.0") (d (list (d (n "flume") (r "^0.10") (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)))) (h "0k4qxppg52s57qj2h8461164n1ch22cnff8ab54z5pg9ry4h7swq")))

(define-public crate-yastl-0.1.1 (c (n "yastl") (v "0.1.1") (d (list (d (n "flume") (r "^0.10") (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)))) (h "12h7q28qvw7kgcvbymvcvf6c23wdzs0121w47gyz6kgw63wh3rd2")))

(define-public crate-yastl-0.1.2 (c (n "yastl") (v "0.1.2") (d (list (d (n "coz") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "flume") (r "^0.10") (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)))) (h "06iwvg3psa9zx9singh3wpkksd177ivz8741c6i9w6kcssjcb9lc")))

