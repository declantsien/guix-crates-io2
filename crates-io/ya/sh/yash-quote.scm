(define-module (crates-io ya sh yash-quote) #:use-module (crates-io))

(define-public crate-yash-quote-0.1.0 (c (n "yash-quote") (v "0.1.0") (h "1hx6hdxv79pyhmhkw930pyi52q4aifxplmw83iqjyw5xg1h799cz") (r "1.56.0")))

(define-public crate-yash-quote-1.0.0 (c (n "yash-quote") (v "1.0.0") (h "1z9ij4n4hsr31afhbhy8a6b14b33b7hvscv6c0iskmxk83w7fsd9") (r "1.56.0")))

(define-public crate-yash-quote-1.0.1 (c (n "yash-quote") (v "1.0.1") (h "042igp061fml1hr2vq8cc4m198qdxcz6k62szik2d3y6jr524l2v") (r "1.56.0")))

(define-public crate-yash-quote-1.1.0 (c (n "yash-quote") (v "1.1.0") (h "1lgna9r1sadg7vfc8av8ah4jcc1ak6y5fzifvabpj5d4hrzsd6iy") (r "1.56.0")))

(define-public crate-yash-quote-1.1.1 (c (n "yash-quote") (v "1.1.1") (h "1wqvp1y4c4157i0q986353bnpbcdjs50ay39fgwmzl1r6sgchc38") (r "1.56.0")))

