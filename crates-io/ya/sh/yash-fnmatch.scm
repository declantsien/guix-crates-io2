(define-module (crates-io ya sh yash-fnmatch) #:use-module (crates-io))

(define-public crate-yash-fnmatch-1.0.0 (c (n "yash-fnmatch") (v "1.0.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1mqckm8s659ivvb75408zbxly54pzkki12agahgfpnm480l1kz5n") (r "1.58.0")))

(define-public crate-yash-fnmatch-1.0.1 (c (n "yash-fnmatch") (v "1.0.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "12wcb0h5qz7p74vrw12iwnbwnhrzcpnmjbrjpj0nqsxallwynlaq") (r "1.58.0")))

(define-public crate-yash-fnmatch-1.1.0 (c (n "yash-fnmatch") (v "1.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pz5wdqcfk5zl8iricagfzck5fmsrihbc37c92qy7jkr50adycxg") (r "1.58.0")))

(define-public crate-yash-fnmatch-1.1.1 (c (n "yash-fnmatch") (v "1.1.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0kz5a3k3vq8jczj3ws3c1madijwh1v9gsfq7kag43rnjg6s20z39") (r "1.65.0")))

