(define-module (crates-io ya sh yash-arith) #:use-module (crates-io))

(define-public crate-yash-arith-0.1.0 (c (n "yash-arith") (v "0.1.0") (h "02ixpihb3da31p9l2v85xx2s8yiv4mh98lrmx0qf46zjfadak2qg") (r "1.58.0")))

(define-public crate-yash-arith-0.2.0 (c (n "yash-arith") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1xpl0w37q8kpvxrs1igqwh0p5dv1f0zggxp8xchfrx6blkir389a") (r "1.58.0")))

(define-public crate-yash-arith-0.2.1 (c (n "yash-arith") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1jcf0y80fa6dy8lf759c1aydcdadq1d1y5103f165qa8hc5m78sc") (r "1.58.0")))

