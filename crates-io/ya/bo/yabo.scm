(define-module (crates-io ya bo yabo) #:use-module (crates-io))

(define-public crate-yabo-0.1.0 (c (n "yabo") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "bumpalo") (r "^3.9.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "salsa") (r "^0.16.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)) (d (n "tree-sitter-yabo") (r "^0.0.1") (d #t) (k 0)))) (h "1zj4awc2vvl5yhp30ah4x5fw0m4rc65jh00w6s20gzhqpn3jg9pk")))

