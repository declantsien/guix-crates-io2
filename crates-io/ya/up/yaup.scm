(define-module (crates-io ya up yaup) #:use-module (crates-io))

(define-public crate-yaup-0.1.0 (c (n "yaup") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1ikkq6ikj39mz341fdlkp5kslqsvpz2z7xfjka5jqx6v30yiv3kz")))

(define-public crate-yaup-0.2.0 (c (n "yaup") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0i9b2mmr26icamqkck7cry5jfkqq8jy1xwxb182mgs7pcdlyzj9v")))

(define-public crate-yaup-0.2.1 (c (n "yaup") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1n99wkirrb2r6ch04blh584shdflm4p1kxaxq8vpqgylpqkpv7m5")))

