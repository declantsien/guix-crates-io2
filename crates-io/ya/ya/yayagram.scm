(define-module (crates-io ya ya yayagram) #:use-module (crates-io))

(define-public crate-yayagram-0.2.0 (c (n "yayagram") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.2.0") (d #t) (k 0) (p "tanmatsu")))) (h "1njfw2apxk492scgabny103vfgfs6vh9ps4y4nivy330akqnyz5r")))

(define-public crate-yayagram-0.2.5 (c (n "yayagram") (v "0.2.5") (d (list (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.2.0") (d #t) (k 0) (p "tanmatsu")))) (h "1i8b0wms32dxgwmjarh0k0yr0p4axififkfl7scdxyr4mrk0f1lm")))

(define-public crate-yayagram-0.3.0 (c (n "yayagram") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.2.5") (d #t) (k 0) (p "tanmatsu")))) (h "0jazgrd1gbh5bxi5ddzrm5js18r34g686hqaf8c12axq26vd4l39")))

(define-public crate-yayagram-0.3.1 (c (n "yayagram") (v "0.3.1") (d (list (d (n "ctrlc") (r "^3.1.9") (f (quote ("termination"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.2.5") (d #t) (k 0) (p "tanmatsu")))) (h "1knp7v21bdgckn042f9wj064z5pj6qj5m4ic7kz40qxn223cf6rk")))

(define-public crate-yayagram-0.4.0 (c (n "yayagram") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.2.5") (d #t) (k 0) (p "tanmatsu")))) (h "00ga9djnyls86w160y9r0515ca8kfjdk78jaq5fibjakl1a1a15b")))

(define-public crate-yayagram-0.5.0 (c (n "yayagram") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.3.0") (d #t) (k 0) (p "tanmatsu")))) (h "1mzh6fy25wrrx3a9qld9j0h9s6h5narznz71i4mzblg5hbb53gv4")))

(define-public crate-yayagram-0.6.0 (c (n "yayagram") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.3.0") (d #t) (k 0) (p "tanmatsu")))) (h "06q8ad3p5i4v226yzv0sjrvv4r2kqpr9vvy7wli5b8y1crw56bp4")))

(define-public crate-yayagram-0.7.0 (c (n "yayagram") (v "0.7.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.4.0") (d #t) (k 0) (p "tanmatsu")))) (h "040g0lskks8v34nzrynr8f15f0va8gljakn1dpd2xhsszb0amfvj")))

(define-public crate-yayagram-0.7.1 (c (n "yayagram") (v "0.7.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.5.0") (d #t) (k 0) (p "tanmatsu")))) (h "0y7jqvqzvk1n86fzja0k84yrkdw3cwwpcffdkqa00z71mpcijhiy")))

(define-public crate-yayagram-0.8.0 (c (n "yayagram") (v "0.8.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "terminal") (r "^0.6.0") (d #t) (k 0) (p "tanmatsu")))) (h "1ga8vjl54v5mp05j8grl9pj7r8l90pighfvgxhbyhnadaayb1p6h")))

(define-public crate-yayagram-0.8.5 (c (n "yayagram") (v "0.8.5") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "line_drawing") (r "^0.8.1") (d #t) (k 0)) (d (n "terminal") (r "^0.6.4") (d #t) (k 0) (p "tanmatsu")))) (h "1fbd23hnswwqbvrlvsrnn67xhh7r6l4lkys1hx0k5h6g8rq3nwnk")))

