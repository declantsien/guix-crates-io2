(define-module (crates-io ya ca yaca) #:use-module (crates-io))

(define-public crate-yaca-0.0.1 (c (n "yaca") (v "0.0.1") (h "1aywwid5jz76zd33h5asd41w9r5rmlmvqs3amysf6jmrvfpg0b6m")))

(define-public crate-yaca-0.1.0 (c (n "yaca") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1jz62jrws0blcn7z0lv6aw2g5z9qcv9w8g1a0jrj5wyyy0cq59b8")))

(define-public crate-yaca-0.1.1 (c (n "yaca") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0p728ywrpsyg6y3vi2pcbzsf0wpl8f2pax76739g7izmzc2nv824")))

(define-public crate-yaca-0.1.2 (c (n "yaca") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1rshg83bnai2vwq7j1l3xfl79bhxm5p9vak9zbdalgh7jp3br27m")))

(define-public crate-yaca-0.1.3 (c (n "yaca") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1nbx7n4zbzzy5fiyynjxifkqjsq60qvw4mdfja46kypd6mqcs7g3")))

(define-public crate-yaca-0.1.4 (c (n "yaca") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0j1b9jlqfb3ccyim455f11brvqhs0zdfssrr8wfvyfbli299vsq6") (f (quote (("static"))))))

(define-public crate-yaca-0.1.5 (c (n "yaca") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "1vlrg2g0i63dj81ncgara4cz0h2dqps3kdic1a2l0hds21snxzzy") (f (quote (("static"))))))

(define-public crate-yaca-0.1.6 (c (n "yaca") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "1lamg6h33npzm7z01rkwc9hd14m3glkchpmzpxnslh14xavnxmnn") (f (quote (("static"))))))

