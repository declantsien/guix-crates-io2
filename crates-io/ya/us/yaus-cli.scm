(define-module (crates-io ya us yaus-cli) #:use-module (crates-io))

(define-public crate-yaus-cli-0.1.0 (c (n "yaus-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "10hxbl99aai9hnwvqrfdllrhh1dn6mknmxinyn7p47d9z2hkbz6l")))

(define-public crate-yaus-cli-0.1.1 (c (n "yaus-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1yh6i6bvnx4bppxainvwk9v60m2jxsgxplbkgsrrs7nxcd3vyclf")))

