(define-module (crates-io ya me yamemcache) #:use-module (crates-io))

(define-public crate-yamemcache-0.0.1 (c (n "yamemcache") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "12iwg75wyghbzyx6rkhxcwl243q3p606piwcn16fm12rzqfm30vq") (y #t)))

(define-public crate-yamemcache-0.0.2 (c (n "yamemcache") (v "0.0.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1yink564c1a2n7qzxyr0fn010pm5qi038nk9win3x2rc2v3wydw1") (y #t)))

(define-public crate-yamemcache-0.0.3 (c (n "yamemcache") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1bcxk55h14yycwmw7cfbrj93haar33fdbfspd3k6qlr9zy8y7ak3")))

(define-public crate-yamemcache-0.0.4 (c (n "yamemcache") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1p0g5m447w6c6xnadw8lwc89hfqn2qadndwrhn7f1d0r8mqy5r7a")))

(define-public crate-yamemcache-0.0.5 (c (n "yamemcache") (v "0.0.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "17xpcyw3xflx1fkcnvk9yiszdwdk95czn9gyni7z31kj8i3jvyrf")))

