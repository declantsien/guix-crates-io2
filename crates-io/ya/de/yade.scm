(define-module (crates-io ya de yade) #:use-module (crates-io))

(define-public crate-yade-0.1.0 (c (n "yade") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.1") (d #t) (k 0)))) (h "01qsb8gkddf5s0l29h65bmm08wbc0mmcjqpafpjlrf6hgm8vgz3s")))

(define-public crate-yade-0.1.1 (c (n "yade") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.1") (d #t) (k 0)))) (h "19518r5s0n4cgcnsbzgvm8bmzjs81r25lhpi3qhn82l6yxp9887l")))

(define-public crate-yade-0.1.2 (c (n "yade") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.1") (d #t) (k 0)))) (h "001dv9mnpvj9063nh65fjkbnzzgriawhp7q2f29h2jv60ad3yz7h")))

