(define-module (crates-io ya si yasi) #:use-module (crates-io))

(define-public crate-yasi-0.1.0 (c (n "yasi") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (f (quote ("raw"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0d2zgf2ja6gl4fn2s781g9wcavr19phplcnar48a85ddvgf8i9dc")))

(define-public crate-yasi-0.1.1 (c (n "yasi") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (f (quote ("raw"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0y8l9560yi700vvdbb0bzck17ln8d60h1himdjjpb9g6n09ca4b9")))

(define-public crate-yasi-0.1.2 (c (n "yasi") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("raw"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "096xlpk5ahvzwh1wsqdjraw12ba2q7n0j4iqgz6m3cwkbcwrdj4q") (f (quote (("default" "serde"))))))

(define-public crate-yasi-0.1.3 (c (n "yasi") (v "0.1.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("raw"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "158lrcx96973z9583p38b63aj5j5p3vgj7n79hv9j090nrzaxdj3") (f (quote (("default" "serde"))))))

(define-public crate-yasi-0.1.4 (c (n "yasi") (v "0.1.4") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("raw"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0prj2mnpb7dmp43wxz0jcv8k8f5m82cy0jmg1lrw23gf5z41krgz") (f (quote (("default" "serde"))))))

(define-public crate-yasi-0.1.5 (c (n "yasi") (v "0.1.5") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("raw"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "122mjhddfw4cs43qr7gvvfvw88k761m0kazlq5cbfc5y5sv5ldbz") (f (quote (("default" "serde"))))))

