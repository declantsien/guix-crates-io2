(define-module (crates-io ya ps yaps-hyper-router) #:use-module (crates-io))

(define-public crate-yaps-hyper-router-0.1.0 (c (n "yaps-hyper-router") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hcontext") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "15ijaqxsmj58hg7d2fzjfwjiv64g0nl1xn96ls1wswrg5xm2wzs3")))

(define-public crate-yaps-hyper-router-0.1.1 (c (n "yaps-hyper-router") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hcontext") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1jaxi1ag2mk6il5x1y3iin3yxw8mcizh44y3yjzjf1q091wj1if1")))

