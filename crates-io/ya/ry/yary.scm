(define-module (crates-io ya ry yary) #:use-module (crates-io))

(define-public crate-yary-0.1.1 (c (n "yary") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "atoi") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "0b7zycmvbgsw3csp1gb1iw2bpdz8mbv57xxs3b6p7y3mz9s51786") (f (quote (("test_lazy") ("test_buffer_small" "test_buffer") ("test_buffer_medium" "test_buffer") ("test_buffer_large" "test_buffer") ("test_buffer"))))))

