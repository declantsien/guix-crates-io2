(define-module (crates-io ya ti yatima-rustyline-derive) #:use-module (crates-io))

(define-public crate-yatima-rustyline-derive-0.1.0 (c (n "yatima-rustyline-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0f1ns6ll4bdp03ma4p4cgbfrm8vgybm57pr30xxq3fjkd3qbvbj4")))

