(define-module (crates-io ya ra yara-x-proto) #:use-module (crates-io))

(define-public crate-yara-x-proto-0.1.0 (c (n "yara-x-proto") (v "0.1.0") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-codegen") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-parse") (r "^3.4.0") (d #t) (k 1)))) (h "0989sl1y34gcyr4hfakv2rm5kdh573zgm171v068w48xhmc70b6h") (r "1.74.0")))

(define-public crate-yara-x-proto-0.2.0 (c (n "yara-x-proto") (v "0.2.0") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-codegen") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-parse") (r "^3.4.0") (d #t) (k 1)))) (h "0gzxskjc4ydnkb6wkg50idrlab4r21s8p2rbz0ayqr3fa7q5k763") (r "1.74.0")))

(define-public crate-yara-x-proto-0.3.0 (c (n "yara-x-proto") (v "0.3.0") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-codegen") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-parse") (r "^3.4.0") (d #t) (k 1)))) (h "1yg2170kzww190634qpk2n4p3rpz0p20n9dbm07jr4725glpcxd2") (r "1.74.0")))

(define-public crate-yara-x-proto-0.4.0 (c (n "yara-x-proto") (v "0.4.0") (d (list (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-codegen") (r "^3.4.0") (d #t) (k 1)) (d (n "protobuf-parse") (r "^3.4.0") (d #t) (k 1)))) (h "094mv897yv7wf5c7b0595h7nw0i41d4c3c7s5v5if5d5q4qslmj5") (r "1.74.0")))

