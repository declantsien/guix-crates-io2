(define-module (crates-io ya ra yara-x-macros) #:use-module (crates-io))

(define-public crate-yara-x-macros-0.1.0 (c (n "yara-x-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "parsing" "visit"))) (d #t) (k 0)))) (h "0mqaykrv2sa49zx6s228ckyzsrbw9r636hblpyw9nkc9j8wia0m1") (r "1.74.0")))

(define-public crate-yara-x-macros-0.2.0 (c (n "yara-x-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "parsing" "visit"))) (d #t) (k 0)))) (h "06mw4xs0p6fbcr7z5hyqwvc2av923zbzc51yr0sihm90xxvc73pz") (r "1.74.0")))

(define-public crate-yara-x-macros-0.3.0 (c (n "yara-x-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "parsing" "visit"))) (d #t) (k 0)))) (h "0bhxhanqbb0i7n7gyhwmb87fr45y6h88dbjikdvjzi7fv456p5q2") (r "1.74.0")))

(define-public crate-yara-x-macros-0.4.0 (c (n "yara-x-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "parsing" "visit"))) (d #t) (k 0)))) (h "0ifmb3vmjm3304zgpq1lg7llcnlwd1ks6pz6h4k8lc1y4bkj1bfh") (r "1.74.0")))

