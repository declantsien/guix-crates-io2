(define-module (crates-io ya ra yara-src) #:use-module (crates-io))

(define-public crate-yara-src-0.1.0 (c (n "yara-src") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0sz4d919vrwkjky5wjsiwyancvbgx3m7wdaai8xf78g48ckc4zgs")))

(define-public crate-yara-src-0.1.1 (c (n "yara-src") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0dgk0w0njrwxzmwmdkizn9rawkzndqj62qbrg5an2ys9dqmcs0j7")))

(define-public crate-yara-src-0.1.2+3.11.0 (c (n "yara-src") (v "0.1.2+3.11.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "05j4hlf2xv8fggl5qchnkl3m465y8k8133vsl7rfwdh3pdzhikfi")))

