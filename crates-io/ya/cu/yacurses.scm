(define-module (crates-io ya cu yacurses) #:use-module (crates-io))

(define-public crate-yacurses-0.0.0 (c (n "yacurses") (v "0.0.0") (d (list (d (n "chlorine") (r "^1") (d #t) (k 0)))) (h "0wmzkyarb1ixpbpwxqlgqp4rq841yc1vi5zsc35igvqng0k66hxp") (y #t)))

(define-public crate-yacurses-0.0.1 (c (n "yacurses") (v "0.0.1") (d (list (d (n "chlorine") (r "^1") (d #t) (k 0)))) (h "11ga7xrpjzwiivsnswqbwvk30n03py5r49hbclcyb08xiy17dfsn")))

(define-public crate-yacurses-0.1.0 (c (n "yacurses") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "chlorine") (r "^1") (d #t) (k 0)))) (h "0fwjyvwsy8xkkn3y1wxz6nri7qd6zpm72c8fkjz227qagsv08yym")))

(define-public crate-yacurses-0.1.1 (c (n "yacurses") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "chlorine") (r "^1") (d #t) (k 0)))) (h "092a4h8pgmax1235a1zbw2yisnn9wn7x5d7y2iqmqanch44n32ks")))

(define-public crate-yacurses-0.2.0 (c (n "yacurses") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)) (d (n "chlorine") (r "^1") (d #t) (k 0)))) (h "0mkcabfvbvgfcwg0i2amfhg48sms5kb205npjhq0vwx1a6jv0zsl")))

(define-public crate-yacurses-0.2.1 (c (n "yacurses") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)) (d (n "chlorine") (r "^1") (d #t) (k 0)))) (h "102dyh91frrya2r7z0zx2h6iql99qldmykh4bk2banrsydqy3m2q")))

(define-public crate-yacurses-0.2.2 (c (n "yacurses") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)))) (h "1b1v0mcq4a6nsiwjq0sc47vax0lfs1cvpzlqyimflkjbqiz8q93m")))

(define-public crate-yacurses-0.2.3 (c (n "yacurses") (v "0.2.3") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)))) (h "1mr3bjxnhlc36jvl72f9kzkk8z41lcbn4hksaa9hs4zdy3q0a449")))

(define-public crate-yacurses-0.2.4 (c (n "yacurses") (v "0.2.4") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)))) (h "1kw9yxw2f9xc8djn767aqx17fs57jkb596yyl9sfh2zq112jl8hx")))

