(define-module (crates-io ya -u ya-utils-process) #:use-module (crates-io))

(define-public crate-ya-utils-process-0.2.0 (c (n "ya-utils-process") (v "0.2.0") (d (list (d (n "actix") (r "^0.13") (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (t "cfg(target_family = \"unix\")") (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("process" "signal"))) (d #t) (k 0)))) (h "1vw1kcssyh1bs692yb3pyk3gsimwqjvxv5hz4hjcqqj3piyk9bjr") (f (quote (("lock" "fs2") ("default"))))))

