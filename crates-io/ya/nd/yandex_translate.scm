(define-module (crates-io ya nd yandex_translate) #:use-module (crates-io))

(define-public crate-yandex_translate-2.0.0 (c (n "yandex_translate") (v "2.0.0") (d (list (d (n "clap") (r "^2.22.1") (d #t) (k 0)) (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1kym7z1qj6hp5ask37cr9vq7691bbpvhcf6y9ms45kblbbfk390y")))

(define-public crate-yandex_translate-2.1.0 (c (n "yandex_translate") (v "2.1.0") (d (list (d (n "clap") (r "^2.22.1") (d #t) (k 0)) (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0jsdh0hhj3dvkj191191kc6swip2m9h9f27nf0qbxzil4yfbbvz3")))

