(define-module (crates-io ya nd yandex-cloud) #:use-module (crates-io))

(define-public crate-yandex-cloud-2023.4.24 (c (n "yandex-cloud") (v "2023.4.24") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("tls" "tls-roots" "gzip"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "17c2mqvri6nwr4pl1crf83hqyzir3k7lxch3g6j82r7gr8xnnlpb")))

(define-public crate-yandex-cloud-2023.4.24-2 (c (n "yandex-cloud") (v "2023.4.24-2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("tls" "tls-roots" "gzip"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "16sqiw1xbpm2jsks11rbxfhigxajdz1x05npl0ln4gznjwwmkpy6")))

(define-public crate-yandex-cloud-2023.4.25 (c (n "yandex-cloud") (v "2023.4.25") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("tls" "tls-roots" "gzip"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "1ryqrphg2qad33gr3g834lg9azhmrz7h5bql6ihxabcfabjjfvk7")))

(define-public crate-yandex-cloud-2023.5.19 (c (n "yandex-cloud") (v "2023.5.19") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("tls" "tls-roots" "gzip"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "0d69avcw9fs8jacsfdxf79bdkcwl53xlsciv0wdwa20ipr65jkhc")))

(define-public crate-yandex-cloud-2023.5.23 (c (n "yandex-cloud") (v "2023.5.23") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("tls" "tls-roots" "gzip"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "1nld908zqxn4a4096m736yy2zkjnn8cdi9r01h63m29blflzy027")))

(define-public crate-yandex-cloud-2023.6.13 (c (n "yandex-cloud") (v "2023.6.13") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (d #t) (k 2)) (d (n "tonic") (r "^0.9") (f (quote ("tls" "tls-roots" "gzip"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "012w8avab39bbf2b0c8hpavmd5z4hl4gjqhm9jcr3n375xz94cgi")))

(define-public crate-yandex-cloud-2023.9.4 (c (n "yandex-cloud") (v "2023.9.4") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (d #t) (k 2)) (d (n "tonic") (r "^0.9") (f (quote ("tls" "tls-roots" "gzip"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "1bng046g3q6p5jnv5a2pq1lmaafzd84ja2dp4q70k3jf4r073xlq")))

