(define-module (crates-io ya nd yandex_checkout) #:use-module (crates-io))

(define-public crate-yandex_checkout-0.1.0 (c (n "yandex_checkout") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "iso4217") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.21") (d #t) (k 2)) (d (n "uuid") (r "^0.7.4") (d #t) (k 0)))) (h "1g27kcwknyls7p8sffdkfnhm2qpbwvh5gnxqdj0yibdmjznnc8nk")))

