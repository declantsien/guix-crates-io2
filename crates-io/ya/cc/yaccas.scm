(define-module (crates-io ya cc yaccas) #:use-module (crates-io))

(define-public crate-yaccas-0.1.0 (c (n "yaccas") (v "0.1.0") (h "1yi1nxv027g6dsrbj1xyvx42zcfcfyckjlc4gb6wsrnlafmsm6id")))

(define-public crate-yaccas-0.2.1 (c (n "yaccas") (v "0.2.1") (h "0wabrdgv25666hablf1bvmyqsw6y6w0axwpkdmadvcgjvv09048l")))

