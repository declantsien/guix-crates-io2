(define-module (crates-io ya gg yagg) #:use-module (crates-io))

(define-public crate-yagg-0.1.0 (c (n "yagg") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "147qk7dj429na93ybwvjlzv5m4mg31xr4p87k5fzl5hhq6k3v173") (y #t)))

(define-public crate-yagg-0.1.1 (c (n "yagg") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "02nxfm4hnbh9zzj7d0wiab0v544f94vkxrxqnfc5iy4b3s09jy46")))

(define-public crate-yagg-0.1.2 (c (n "yagg") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1z3iaam5v2wpxlx1a8krkk9sgqjhda97z9yg2hk9sbglxi8xxf2x")))

