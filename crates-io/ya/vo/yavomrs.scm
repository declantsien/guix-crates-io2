(define-module (crates-io ya vo yavomrs) #:use-module (crates-io))

(define-public crate-yavomrs-0.1.0 (c (n "yavomrs") (v "0.1.0") (h "1spayf657xd0gz7x2jc1hdxgq4kqb2a65jca2rx8cjkli7s33v66")))

(define-public crate-yavomrs-0.1.1 (c (n "yavomrs") (v "0.1.1") (h "06l6snqia9xz09ryg7mhngw6kkwca75h5hk7qlfs4g61j9l3xfc4")))

