(define-module (crates-io ya jl yajlish) #:use-module (crates-io))

(define-public crate-yajlish-0.1.0 (c (n "yajlish") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vnfml3v9w4mvy2z5dm04rvkvbz2xpra6ban62a7fwmdcr51j5dd") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.1.1 (c (n "yajlish") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gligifs2f4r957h2qxhdakzl9l5ppra4f14gljjs582nrw4ymrs") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.1.2 (c (n "yajlish") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "=6.0.0-alpha1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00v4x0g42nbnwq92wwkjpgak5q0va9wz6zd3n76ky0dfklbm206g") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.1.3 (c (n "yajlish") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "=6.0.0-alpha1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v46fymr4r6spzjj2qykqqh1g7l7qflralxl4yjgsji6dpycws9j") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.1.4 (c (n "yajlish") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "=6.0.0-alpha1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kvk7cddmd8v9dgx6r73zridp261nxgk43j83l62h15k21wzqibh") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.1.5 (c (n "yajlish") (v "0.1.5") (d (list (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "nom") (r "=6.0.0-alpha1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pretty_assertions") (r ">=0.6.0, <0.7.0") (d #t) (k 2)) (d (n "proptest") (r ">=0.10.0, <0.11.0") (d #t) (k 2)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1mxfbjh83qsfvnhrvrjq74g53s8r82dpwk85yjqgkhn9nv3jvxyb") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.2.0 (c (n "yajlish") (v "0.2.0") (d (list (d (n "json-tools") (r "^1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1dx7dqdxvci3d6jvi73yji8bzl2am6jmchghr3raifmir146v4kd") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.3.0 (c (n "yajlish") (v "0.3.0") (d (list (d (n "json-tools") (r "^1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1ikrbzfyq8kslpqi87aip19b35041ii4hdm98iva9k5kl7zq25i3") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.3.1 (c (n "yajlish") (v "0.3.1") (d (list (d (n "json-tools") (r "^1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0rw0xrz7n713s307xm6x026hpb41jjd56yikhx8s1j0pmyqf49hk") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.3.2 (c (n "yajlish") (v "0.3.2") (d (list (d (n "json-tools") (r "^1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0sd4pjxkj4g8vk9czznqz1dnk3qf3am9b6zvhdp18wdvmk1mzmm2") (f (quote (("ndjson"))))))

(define-public crate-yajlish-0.4.0 (c (n "yajlish") (v "0.4.0") (d (list (d (n "json-tools") (r "^1.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19") (o #t) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1qs6qj1b01axizb9ssyahpa6rw379y3qnrk80jq1nkji84bgfjn5") (f (quote (("ndjson" "lalrpop" "lalrpop-util"))))))

