(define-module (crates-io ya ma yamakan) #:use-module (crates-io))

(define-public crate-yamakan-0.0.1 (c (n "yamakan") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)))) (h "0pp8zqxf82dk25v1l11a1wdirxcq3phvf1kpj32byjfs9548109w")))

(define-public crate-yamakan-0.0.2 (c (n "yamakan") (v "0.0.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)))) (h "0w978fpz62chidq6jfi05w0xnn02g3bz1fw6v4a4qxcc24lg79iv")))

(define-public crate-yamakan-0.0.3 (c (n "yamakan") (v "0.0.3") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1k2549nk6mxc1hlgajv2zpsr6n70fdy7my4dvziccf9zc77dd24m")))

(define-public crate-yamakan-0.0.4 (c (n "yamakan") (v "0.0.4") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0mpygbg11q2ywqj2jh1y6304f2hyfx6vqsvabpjxfhk49jm0xkff")))

(define-public crate-yamakan-0.0.5 (c (n "yamakan") (v "0.0.5") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0kpw08w4z7frvjnw96sw1hbxa85lmj585q0jdm9ykxdgbb1gk6wb")))

(define-public crate-yamakan-0.0.6 (c (n "yamakan") (v "0.0.6") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1ka43ry2kvqi90zv0k4pxnsjq38nrrdyv1j414zd587lhq7c4m0w")))

(define-public crate-yamakan-0.0.7 (c (n "yamakan") (v "0.0.7") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0f6kr1s6x5cjlkvqz5g5bw5864dp5kgm0hkja9jqlrphamb84wpc")))

(define-public crate-yamakan-0.0.8 (c (n "yamakan") (v "0.0.8") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0cc2spplawrxq51rplhjag8dpjm64w5hdp8lmgkazl62k3cwnbnh")))

(define-public crate-yamakan-0.0.9 (c (n "yamakan") (v "0.0.9") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "17j1k0fwdrwx11yiqmvvba4vyspphl1qvj9wr48b0jfkcxby18hw")))

(define-public crate-yamakan-0.0.10 (c (n "yamakan") (v "0.0.10") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1ig0s40687zns5hkgly4yfcwkqlh3dp7p0yzxci4s9a5k3lrfqil")))

(define-public crate-yamakan-0.0.11 (c (n "yamakan") (v "0.0.11") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "statrs") (r "^0.10") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0dsdjhz552mhn1jh4qp2pa78dzl131p5ifxlfmmabwvhcwy541rg")))

(define-public crate-yamakan-0.0.12 (c (n "yamakan") (v "0.0.12") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustats") (r "^0.0") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "139yxmnz8c57lh0h8zn5hf9p2qacny4l3my0bmfyf2lbrphizdzv")))

(define-public crate-yamakan-0.0.13 (c (n "yamakan") (v "0.0.13") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustats") (r "^0.0") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "110597912zfhy56gkykl7ggaixxhphzyzmz14vr6djvhmww2p8q7")))

(define-public crate-yamakan-0.0.14 (c (n "yamakan") (v "0.0.14") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustats") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0fwmym769fl6f530m3w2y13ziyn0yz9q3qvd7rm3k66jvinkcwg5")))

(define-public crate-yamakan-0.0.15 (c (n "yamakan") (v "0.0.15") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustats") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1wndhjg6g698rcmx7m4hsy76imv0igcx2m90kca1cbhpw8443xdl")))

(define-public crate-yamakan-0.0.16 (c (n "yamakan") (v "0.0.16") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustats") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0rbii8mq2v7a3ifija7gbhd8gfnrng8z7r62wpbm5n4kabqfvhf7")))

(define-public crate-yamakan-0.0.17 (c (n "yamakan") (v "0.0.17") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rustats") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1qznhi4c5xx5m01vmfpgqzhh8wh3yqk8b3k3c5ffxmwykns654d6")))

(define-public crate-yamakan-0.0.18 (c (n "yamakan") (v "0.0.18") (d (list (d (n "factory") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustats") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1hp067bjvdjzh8nkqxsy91lfnb42mi2gm03sdy05736ycf9xz3v0")))

(define-public crate-yamakan-0.1.0 (c (n "yamakan") (v "0.1.0") (d (list (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1j9vc1ph5ig2qy8mvqvgkbkx0wz8s0gq99ra8y9zx5d37zl8a34w")))

(define-public crate-yamakan-0.1.1 (c (n "yamakan") (v "0.1.1") (d (list (d (n "ordered-float") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0q8yhw69rvvr8zxlp7d9jxv69v4nxhmj7gk5vw1qjid8h5y3sgx7")))

(define-public crate-yamakan-0.2.0 (c (n "yamakan") (v "0.2.0") (d (list (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1z35rfs9kgclnmb50g5g11fb8j0mnbv31dsjz42cxhfbfvnzsn58")))

