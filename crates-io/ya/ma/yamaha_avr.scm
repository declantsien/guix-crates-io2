(define-module (crates-io ya ma yamaha_avr) #:use-module (crates-io))

(define-public crate-yamaha_avr-0.0.1 (c (n "yamaha_avr") (v "0.0.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "09a8p468npas4nm04fhx1353dggp2wblcfx32nmgsllx7k0mx24b")))

(define-public crate-yamaha_avr-0.1.0 (c (n "yamaha_avr") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "1ydv7kikjn6c4nvmldl3iak61n9aca0kvkxqai3dp52nzf14mwkg")))

(define-public crate-yamaha_avr-0.2.0 (c (n "yamaha_avr") (v "0.2.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "1pw6ivanh61g5dhh0nwsg7np2f4cn8i51a387hl418brka1yxpn8")))

