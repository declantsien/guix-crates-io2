(define-module (crates-io ya sl yaslapi) #:use-module (crates-io))

(define-public crate-yaslapi-0.1.0 (c (n "yaslapi") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 2)) (d (n "yaslapi-sys") (r "^0.2.3") (d #t) (k 0)))) (h "0s1aipff66y51jzys6c2m5ff8wyrrr0j6li7w386fhd3a45a3b24")))

