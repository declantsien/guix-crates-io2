(define-module (crates-io ya sl yaslapi-sys) #:use-module (crates-io))

(define-public crate-yaslapi-sys-0.1.0 (c (n "yaslapi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0zg6jnlnxgzd7q52yqz887v58s1fzsppq7m9l9yjvw6bx8sj91ig")))

(define-public crate-yaslapi-sys-0.1.1 (c (n "yaslapi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0wx94f14izp2x8l24mcl1a7wnbq8s6x21zqz8mlgfp42ykdl0ilk")))

(define-public crate-yaslapi-sys-0.2.0 (c (n "yaslapi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "02j1826p57wl5qzgqghv9cg0q8vccf2rmxznask2v70vf52mj1jx")))

(define-public crate-yaslapi-sys-0.2.1 (c (n "yaslapi-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1qjkc04wngbharwjyyj08zr43i0cspgpy265hin805lc08s3q98v")))

(define-public crate-yaslapi-sys-0.2.2 (c (n "yaslapi-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1mrq1v79vbqg1b7js7fkadswq4r44p67w2xh0d78p8p2z2zsnbjj")))

(define-public crate-yaslapi-sys-0.2.3 (c (n "yaslapi-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "06yin6wz20rmyn58ml1i7y1rz4xhmvkccx9sm9hy73wlk5dgb2vl")))

(define-public crate-yaslapi-sys-0.2.4 (c (n "yaslapi-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1kn18bbdq5yzrhzdz06k34hiz0f7r232x0xdzz9xq5jc7aaf16iv")))

(define-public crate-yaslapi-sys-0.2.5 (c (n "yaslapi-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0sb1h2zl8qlhm03gjwlwxzax7hd0b2fj8ly9vdbz3k0irin0i5fh")))

