(define-module (crates-io ya ku yakui-miniquad) #:use-module (crates-io))

(define-public crate-yakui-miniquad-0.1.0 (c (n "yakui-miniquad") (v "0.1.0") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 0)))) (h "0lkpnmspacyxy4lnsmsmnx766fzk1r87z7gkz40vjm8wifmx4qjh")))

(define-public crate-yakui-miniquad-0.1.1 (c (n "yakui-miniquad") (v "0.1.1") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 0)))) (h "1znlg1v1j9j6qb06582r3apa270l5aw8z6gqgzr185mv6ham55fx")))

(define-public crate-yakui-miniquad-0.1.2 (c (n "yakui-miniquad") (v "0.1.2") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 0)))) (h "04yd7abmxb4hahz5j8iklvvzbfzm7pmnxwqvvvhkgangclww73gi")))

(define-public crate-yakui-miniquad-0.1.3 (c (n "yakui-miniquad") (v "0.1.3") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 0)))) (h "1lb5fv1ah7cdssbkb88m9gl5nnjsxxv2w5bzg30sq9p9sqkxrrcr")))

(define-public crate-yakui-miniquad-0.1.4 (c (n "yakui-miniquad") (v "0.1.4") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 0)))) (h "1ah0nbx81acpm6blff5np2scnhra1w3lfh199z4dccp3rj703h09")))

(define-public crate-yakui-miniquad-0.2.0 (c (n "yakui-miniquad") (v "0.2.0") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "1q8x9m707raqafq6wymk67yc8c9w23zkqwqdbdzjjvc9sdmpfml5")))

(define-public crate-yakui-miniquad-0.2.1 (c (n "yakui-miniquad") (v "0.2.1") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "0x79wi2z8b3r13kaj845agrsjzr1v1l3i3k29a2sv97k2bp2nbm0")))

(define-public crate-yakui-miniquad-0.2.2 (c (n "yakui-miniquad") (v "0.2.2") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "1hanigbmkdsjy6f5xmv99cdc4dj14n1s5ibf5id3bpcr2pk0wls7")))

(define-public crate-yakui-miniquad-0.2.3 (c (n "yakui-miniquad") (v "0.2.3") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "01a7y2h3840wairvg5wq1rkgbjqi5hkam42ik8jlbl8pv9y8gwbp")))

(define-public crate-yakui-miniquad-0.2.4 (c (n "yakui-miniquad") (v "0.2.4") (d (list (d (n "miniquad") (r "^0.3.16") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "1s9kmg6zi5jjx060cdm3rafjgg175h140mcrf8wpql3fb1kwkw7r")))

(define-public crate-yakui-miniquad-0.3.0 (c (n "yakui-miniquad") (v "0.3.0") (d (list (d (n "miniquad") (r "^0.4.0") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "07gjpyw6cn8yaazbvxshbzl1qb1l22br453xdl0p58wbhqypa8dm")))

(define-public crate-yakui-miniquad-0.3.1 (c (n "yakui-miniquad") (v "0.3.1") (d (list (d (n "miniquad") (r "^0.4.0") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "1249dqsyxqijnbi8gqg726zfbb06z9mjwgqwxg9ji35vhrv2ncci")))

