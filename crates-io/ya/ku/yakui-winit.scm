(define-module (crates-io ya ku yakui-winit) #:use-module (crates-io))

(define-public crate-yakui-winit-0.1.0 (c (n "yakui-winit") (v "0.1.0") (d (list (d (n "winit") (r "^0.26.1") (d #t) (k 0)) (d (n "yakui-core") (r "^0.1.0") (d #t) (k 0)))) (h "132kpky12kd30pf9cvpcfd9y4mnc3fzgvfgvdvafqkcxjnvg2w5m")))

(define-public crate-yakui-winit-0.2.0 (c (n "yakui-winit") (v "0.2.0") (d (list (d (n "winit") (r "^0.27.3") (d #t) (k 0)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "1y8s5b4gmqwg76w63fklbsqjyh29l0k7qznajsrsvvw31ka25z4k")))

