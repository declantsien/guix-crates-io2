(define-module (crates-io ya ku yakui-widgets) #:use-module (crates-io))

(define-public crate-yakui-widgets-0.1.0 (c (n "yakui-widgets") (v "0.1.0") (d (list (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "yakui-core") (r "^0.1.0") (d #t) (k 0)))) (h "048fvb1541hrjqwfvzkdsrflczapd1qvb1chb8qh1gn3mi2s6klw")))

(define-public crate-yakui-widgets-0.2.0 (c (n "yakui-widgets") (v "0.2.0") (d (list (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "insta") (r "^1.15.0") (d #t) (k 2)) (d (n "smol_str") (r "^0.1.23") (d #t) (k 0)) (d (n "thunderdome") (r "^0.5.1") (d #t) (k 0)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "1171mm5yg9p9kcnmhhjpc1lgih6lk8r5dlp0daivjs13q5n43bcn") (f (quote (("default-fonts") ("default" "default-fonts"))))))

