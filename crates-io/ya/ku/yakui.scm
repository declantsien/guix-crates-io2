(define-module (crates-io ya ku yakui) #:use-module (crates-io))

(define-public crate-yakui-0.1.0 (c (n "yakui") (v "0.1.0") (d (list (d (n "yakui-core") (r "^0.1.0") (d #t) (k 0)) (d (n "yakui-widgets") (r "^0.1.0") (d #t) (k 0)))) (h "1chamdwis7wsh96xhxy8jqmhg34vgys5dh07i85nsv0jf37k2iy2")))

(define-public crate-yakui-0.2.0 (c (n "yakui") (v "0.2.0") (d (list (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)) (d (n "yakui-widgets") (r "^0.2.0") (k 0)))) (h "065q55vv2cqqykc0ji141fi92hykgwf32018aqlcnl7d32dmcwnk") (f (quote (("default-fonts" "yakui-widgets/default-fonts") ("default" "default-fonts"))))))

