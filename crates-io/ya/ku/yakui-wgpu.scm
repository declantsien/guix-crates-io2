(define-module (crates-io ya ku yakui-wgpu) #:use-module (crates-io))

(define-public crate-yakui-wgpu-0.1.0 (c (n "yakui-wgpu") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.10.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.21.2") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "thunderdome") (r "^0.5.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)) (d (n "yakui-core") (r "^0.1.0") (d #t) (k 0)))) (h "17ldjqznbz6cizfwfdi2mc0ylmi48nl437vkmqs8a0rwpmnqj6gw")))

(define-public crate-yakui-wgpu-0.2.0 (c (n "yakui-wgpu") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.10.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.21.2") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "profiling") (r "^1.0.6") (d #t) (k 0)) (d (n "thunderdome") (r "^0.5.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)) (d (n "yakui-core") (r "^0.2.0") (d #t) (k 0)))) (h "0wdhhmqfq9aqdrhf8lxvsr9qmyc4pvn8vr4zq29kv0cb2a8rz34x")))

