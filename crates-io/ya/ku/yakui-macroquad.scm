(define-module (crates-io ya ku yakui-macroquad) #:use-module (crates-io))

(define-public crate-yakui-macroquad-0.1.0 (c (n "yakui-macroquad") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.24") (k 0)) (d (n "yakui-miniquad") (r "^0.1.1") (d #t) (k 0)))) (h "18ng10mqsl28s4z3xb11gzffnnfxj3c5f7b30blhrkk5s5albr5v")))

(define-public crate-yakui-macroquad-0.1.1 (c (n "yakui-macroquad") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.24") (k 0)) (d (n "yakui-miniquad") (r "^0.1.1") (d #t) (k 0)))) (h "16hn4f8944klnxw3h5ggr95gwzrs6rsaxw7vdf3f38k4qaa1xdqz")))

(define-public crate-yakui-macroquad-0.2.0 (c (n "yakui-macroquad") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.3.24") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-miniquad") (r "^0.2.0") (d #t) (k 0)))) (h "02cwa4gwfcqg9appc61j4yif2hghc8vwaypvf20z7qjmkdy8jwvj")))

(define-public crate-yakui-macroquad-0.2.1 (c (n "yakui-macroquad") (v "0.2.1") (d (list (d (n "macroquad") (r "^0.3.24") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-miniquad") (r "^0.2.3") (d #t) (k 0)))) (h "1dl9mg710vc4xj2rsc4lhr8337cxkynlhn16ch8gp5b77y0kwh67")))

(define-public crate-yakui-macroquad-0.2.2 (c (n "yakui-macroquad") (v "0.2.2") (d (list (d (n "macroquad") (r "^0.3.24") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-miniquad") (r "^0.2.4") (d #t) (k 0)))) (h "01r7yyp9qx6z1kin82gvk5dzi5lrgn0r99775idamd0sgp477qnc")))

(define-public crate-yakui-macroquad-0.3.0 (c (n "yakui-macroquad") (v "0.3.0") (d (list (d (n "macroquad") (r "^0.4.5") (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-miniquad") (r "^0.3.0") (d #t) (k 0)))) (h "0hp8jyi1pxqd8g406vzydfgn7w6r01splx7g74qlpc24s04q7wyr")))

(define-public crate-yakui-macroquad-0.3.1 (c (n "yakui-macroquad") (v "0.3.1") (d (list (d (n "macroquad") (r "^0.4.5") (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)) (d (n "yakui") (r "^0.2.0") (d #t) (k 2)) (d (n "yakui-miniquad") (r "^0.3.0") (d #t) (k 0)))) (h "1f02i208kywamra56sdmsyds3ld149zk33sl7qkjlgggi7nzm3w7")))

