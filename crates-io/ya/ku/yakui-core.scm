(define-module (crates-io ya ku yakui-core) #:use-module (crates-io))

(define-public crate-yakui-core-0.1.0 (c (n "yakui-core") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "glam") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "profiling") (r "^1.0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thunderdome") (r "^0.5.1") (d #t) (k 0)))) (h "1ds1f3x8if39vw7ffc0ksglsgddhfnh9gvav7vlyzlk1bancb4fn")))

(define-public crate-yakui-core-0.2.0 (c (n "yakui-core") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "glam") (r "^0.21.2") (d #t) (k 0)) (d (n "keyboard-types") (r "^0.6.2") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "profiling") (r "^1.0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "thunderdome") (r "^0.5.1") (d #t) (k 0)))) (h "1q4mg07fsvm7ws5y926hbx4w328ablsaacd0bkqcvzzs8jsngn01")))

