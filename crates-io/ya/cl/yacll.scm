(define-module (crates-io ya cl yacll) #:use-module (crates-io))

(define-public crate-yacll-0.0.0 (c (n "yacll") (v "0.0.0") (h "0qplbm0wckiqlmdbvawd88sfirzsnpqsmky9sj46pwhbsbq72k4f")))

(define-public crate-yacll-0.0.1 (c (n "yacll") (v "0.0.1") (h "0jdl92b7invxr71cva0pwwvx2wq5yla9b0yj5gbrrcvhdhc1jgb9")))

(define-public crate-yacll-0.1.0 (c (n "yacll") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "005y1s34q9bv3l75nwhp1z5v6wimsc6qicb6kga4mccw2hpzg74i")))

(define-public crate-yacll-0.2.0 (c (n "yacll") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1c1krkxah2zsjmmw5fc14mx69jxapbank6qij7qjwdjh6p2r7mf1")))

(define-public crate-yacll-0.3.0 (c (n "yacll") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "01rzqs6irbyi972p6n6s05q4iraqdks0mmnynhzfwwd0ymjr3yfx")))

(define-public crate-yacll-0.4.0 (c (n "yacll") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1ax40yqqmnv8mlzjld4k8xd0vb11pqsrc7i8zqyg3frj5nwrdsr8")))

(define-public crate-yacll-0.5.0 (c (n "yacll") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1i0jpdianwzr7jm3cbmyaamfn19vam2gyhmwg6b9dhpwpc1fl67s")))

(define-public crate-yacll-0.6.0 (c (n "yacll") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0fvnqclg2a2gqv69xxppyqxcdp5qfyx2xv1r8nhhsx64ppwyv2g5") (s 2) (e (quote (("serde" "dep:serde" "crossterm/serde"))))))

(define-public crate-yacll-0.6.1 (c (n "yacll") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1zygih8d0wcifxk00lzyb0knpig849zr3xmga3zkb9sqbrm56pgk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-yacll-0.7.0 (c (n "yacll") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1csbdlfsmv2kg0yyhigf8bffcbdinhy3c0h1sy3zik97zba9hqky") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-yacll-0.7.1 (c (n "yacll") (v "0.7.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1dhhvjy74j805jqndr3584z2ikrrkisxlrbkh523rsqas160wcdd") (s 2) (e (quote (("serde" "dep:serde"))))))

