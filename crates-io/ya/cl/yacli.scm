(define-module (crates-io ya cl yacli) #:use-module (crates-io))

(define-public crate-yacli-0.1.0 (c (n "yacli") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "shell") (r "^0.3.0") (d #t) (k 0)))) (h "1zz52mx38bm5pp1x1nz00rrl0y5v8zis7yks02gi84afk8l76gly")))

(define-public crate-yacli-0.2.0 (c (n "yacli") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "shell") (r "^0.3.0") (d #t) (k 0)))) (h "0yn39fnl215260g43m2cmd67virqmc2fa875izkf6lxk2byfbpr6")))

(define-public crate-yacli-0.3.0 (c (n "yacli") (v "0.3.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "shell") (r "^0.3.0") (d #t) (k 0)))) (h "12p0i75f515agrkvgqyg36b46wms14dcjy8klyy6qdybf1digh5l") (y #t)))

(define-public crate-yacli-0.2.1 (c (n "yacli") (v "0.2.1") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "shell") (r "^0.3.0") (d #t) (k 0)))) (h "19i8y939y25mzn4q0ki17x1xxdq1qgs723yrmccjkdb0yhcrl500")))

(define-public crate-yacli-0.4.0 (c (n "yacli") (v "0.4.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "shell") (r "^0.3.0") (d #t) (k 0)))) (h "0gabw74azj1lvf0iahxyjccarq7fdhqd7wfg692ivfd39j7xr63x")))

(define-public crate-yacli-0.4.1 (c (n "yacli") (v "0.4.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "shell") (r "^0.3") (d #t) (k 0)))) (h "1vc6gxbgfn6ncfiyjsh4f3qv1a0cmbw240hndamqwb8j1r1xdxjz")))

(define-public crate-yacli-0.5.0 (c (n "yacli") (v "0.5.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "shell") (r "^0.3") (d #t) (k 0)))) (h "097vzh4dhcfpjy3i6ngdhyr1rc5676glag3iv5b37rxccylhpijr")))

(define-public crate-yacli-0.5.1 (c (n "yacli") (v "0.5.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "shell") (r "^0.3") (d #t) (k 0)))) (h "0dylzkvq27gizjmw872l9g7axddnlyf7yinhbrmwn9yyf83vqf8y")))

(define-public crate-yacli-0.6.0 (c (n "yacli") (v "0.6.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "shell") (r "^0.3") (d #t) (k 0)))) (h "0a7x6ychg4fq9279cl5j03m4pfg9s6zwr25cpb7g4jcpcqh4zv8p")))

(define-public crate-yacli-0.6.1 (c (n "yacli") (v "0.6.1") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "shell") (r "^0.3") (d #t) (k 0)))) (h "08vpf430ij0mbsj837inimd47zp6ds7avmfwd610dcx85ixyfkkz")))

