(define-module (crates-io ya mo yamos6502) #:use-module (crates-io))

(define-public crate-yamos6502-0.1.0 (c (n "yamos6502") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap-num") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "159xwkzgwq2z97vxv3gq3cai7cam5mg8djqifdkja2rk3m7pb46m") (f (quote (("std") ("default" "std"))))))

(define-public crate-yamos6502-0.1.1 (c (n "yamos6502") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap-num") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "02wj7spbq1f0bsy1izy86y32dhrg9fdna632krq64cz5hj3sycyc") (f (quote (("std") ("default" "std"))))))

(define-public crate-yamos6502-0.1.2 (c (n "yamos6502") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap-num") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "042gyczcccijzkm8fm0abnc0dwkbx49fj5j4h4prqr2xkc8f3w0y") (f (quote (("std") ("default" "std"))))))

(define-public crate-yamos6502-0.1.3 (c (n "yamos6502") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap-num") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "07fkybwzznrk4fs3m77g12ilmr9jjrbs16hc58iqhg37s2ylbxxj") (f (quote (("std") ("default" "std"))))))

(define-public crate-yamos6502-0.1.4 (c (n "yamos6502") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "clap-num") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1pv4ydcc2pflar9813yah1a2gnq9xh643zfqqm0dqqvddyxdkmi7") (f (quote (("std") ("default" "std"))))))

