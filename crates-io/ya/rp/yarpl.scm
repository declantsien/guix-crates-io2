(define-module (crates-io ya rp yarpl) #:use-module (crates-io))

(define-public crate-yarpl-0.0.1 (c (n "yarpl") (v "0.0.1") (h "15vw77z4s4d24fx4axyvlggaxw3429k6br66s38j0nbg96h1nq6q") (y #t)))

(define-public crate-yarpl-0.0.2 (c (n "yarpl") (v "0.0.2") (h "19imm5w51p6qw6aiglss49hpmfb07whms0pihawz5ilp3fq86a3m") (y #t)))

(define-public crate-yarpl-0.0.3 (c (n "yarpl") (v "0.0.3") (h "0k042m3z2wlvpmhq8vq6iw2mpgwxkp9qyzk3sxg9dzw4ridn6rwr") (y #t)))

(define-public crate-yarpl-0.0.4 (c (n "yarpl") (v "0.0.4") (h "0arybb564m4spcy4i0s7zijdm0gvx6wclai9cycaa05lfnkapbwa") (y #t)))

(define-public crate-yarpl-0.0.6-pre-release (c (n "yarpl") (v "0.0.6-pre-release") (h "0rw5i2qhprhn0w060y69xz0vnqkywh1qk1vgprqivm49v1m3pwm8") (y #t)))

(define-public crate-yarpl-0.0.7 (c (n "yarpl") (v "0.0.7") (h "1bgn6rvfa5c1y2z18jrvjwm3a34l2x442fn89lishmdx6v4qb4cy") (y #t)))

(define-public crate-yarpl-0.0.9 (c (n "yarpl") (v "0.0.9") (h "0b37dq327p7y88bz8lq1i01cvjbsn51grmifr7vmh48v617jgbiv") (y #t)))

(define-public crate-yarpl-0.0.10 (c (n "yarpl") (v "0.0.10") (h "141p651pfgvdb54hi52g7xicaswhr245wjvmlz7xwz5hh2z5wkh0") (y #t)))

(define-public crate-yarpl-0.0.11 (c (n "yarpl") (v "0.0.11") (h "14nc287npx9f92l2qv58l4xqqm8iy76m31x32gc4wxc59d87ymnf") (y #t)))

(define-public crate-yarpl-0.0.12 (c (n "yarpl") (v "0.0.12") (h "173s3pnqqwj95127k2vf8dl7fri18c710ywbx823gqwqy5hrhnp1") (y #t)))

(define-public crate-yarpl-0.0.13 (c (n "yarpl") (v "0.0.13") (h "0qkcndhdfl9aqm4kcsv61p7ris98pynhp4djs58r5dmvn0gkb757") (y #t)))

(define-public crate-yarpl-0.0.14 (c (n "yarpl") (v "0.0.14") (h "098i5nkh0a5kgxji7axrc0ig9yw2cn86aa5grank8bbqkmb3g2gj") (y #t)))

(define-public crate-yarpl-0.0.15 (c (n "yarpl") (v "0.0.15") (h "0ngnjjhli50kdx92g2pihgr4f4q78wqh38s0346xrh57s9c93yvk") (y #t)))

(define-public crate-yarpl-0.0.16 (c (n "yarpl") (v "0.0.16") (h "0a5bgp1i7jsrmhzwa0f0wcxcp7ivfilcl2pfq65j759ly58x5369") (y #t)))

(define-public crate-yarpl-0.0.17 (c (n "yarpl") (v "0.0.17") (h "0knqi0liwn04ymjj4kcm74i3v6qpxqzw1q2ly9gn5vfg79lcrbza") (y #t)))

(define-public crate-yarpl-0.0.18 (c (n "yarpl") (v "0.0.18") (h "1nz18z2k16lwn5fjlqz3kmgskhjh5pmhmnxib3f40x0v2dnqlhbb")))

(define-public crate-yarpl-0.0.19 (c (n "yarpl") (v "0.0.19") (h "1sdavq2x2747zviq34ls27z0pccg4y6m1icr8cfi0zqq4v1c88jr")))

(define-public crate-yarpl-0.0.20 (c (n "yarpl") (v "0.0.20") (h "12p7xj5fd66qsz1blp1jgjx59gcq7w3kinkbpq3gvbnhd7m49zlx")))

(define-public crate-yarpl-0.0.21 (c (n "yarpl") (v "0.0.21") (h "1syk5is454xazrac4f6lkk5lm4vajp7nid3p4iinlfyfwmnnd2cr")))

(define-public crate-yarpl-0.0.23 (c (n "yarpl") (v "0.0.23") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0dlm7ir5a9m9i3a19fli2349n41kfxn0zbbhwfj9xyqchp3vvs81")))

