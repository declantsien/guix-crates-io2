(define-module (crates-io ya rp yarpm) #:use-module (crates-io))

(define-public crate-yarpm-0.1.0 (c (n "yarpm") (v "0.1.0") (d (list (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ng27g073n353vv71m18vh1n3743688fk5khbg9bfczap486avhc")))

(define-public crate-yarpm-0.1.1 (c (n "yarpm") (v "0.1.1") (d (list (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10xdrjncn60wmr63p30vmnwd3x4z6vs9lsa8cvnqlic6ragxrn8c")))

(define-public crate-yarpm-0.1.2 (c (n "yarpm") (v "0.1.2") (d (list (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ic31cbwkjlkwisdx4kfjbqy87gm9kdlkfbc4dajbplayqcvfxp1")))

(define-public crate-yarpm-0.1.3 (c (n "yarpm") (v "0.1.3") (d (list (d (n "clap") (r "^2.3.33") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l8yhx4z0gwzlwa1anxsiicbw2qzqk6sxxaqcvcybh3knj2mfgg6")))

