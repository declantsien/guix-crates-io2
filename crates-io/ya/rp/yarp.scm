(define-module (crates-io ya rp yarp) #:use-module (crates-io))

(define-public crate-yarp-0.1.0 (c (n "yarp") (v "0.1.0") (h "01875wvj0rrrkwxjh2awh666660ka1s4z4x0dp8pv980xgjca3fs") (y #t)))

(define-public crate-yarp-0.1.1 (c (n "yarp") (v "0.1.1") (h "0jgpjr3hhjm2gvhkm3v8v6dzbnjsyi674p5ajmb94bs0hg015h7h") (y #t)))

