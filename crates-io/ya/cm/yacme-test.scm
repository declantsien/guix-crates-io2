(define-module (crates-io ya cm yacme-test) #:use-module (crates-io))

(define-public crate-yacme-test-1.0.1 (c (n "yacme-test") (v "1.0.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("async-await"))) (d #t) (k 0)))) (h "0hk8q51f9dl3lyad0xciw2p22hvlcdw8a6xvi2ij8ibfj3na3bz3")))

(define-public crate-yacme-test-1.0.2 (c (n "yacme-test") (v "1.0.2") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("async-await"))) (d #t) (k 0)))) (h "1a45fvqki8ik9cm58cxczpv1xpcd0qvnbdzl9y9hndg7qnpfb746")))

(define-public crate-yacme-test-1.1.0 (c (n "yacme-test") (v "1.1.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("async-await"))) (d #t) (k 0)))) (h "1m7b5xmqwcbwwj7jvyq92ysm28lfbf6v5lv8m6dd7w6ngbixq449")))

