(define-module (crates-io ya cm yacm_derive) #:use-module (crates-io))

(define-public crate-yacm_derive-0.1.0 (c (n "yacm_derive") (v "0.1.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "05x2j57jvdj4gj50319g7q8qgnadzpyb7zx7jzd7g4c3fqz041d4")))

(define-public crate-yacm_derive-0.1.1 (c (n "yacm_derive") (v "0.1.1") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1sacd5a948cjxn9na53ckw056f0hi529c8j99448k7p1aghw3x")))

(define-public crate-yacm_derive-0.1.2 (c (n "yacm_derive") (v "0.1.2") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0jp9n04xmw48ccfq85ysqbdab8avg4i0n8na7hl6w2rwkv8jkhbf")))

(define-public crate-yacm_derive-0.1.3 (c (n "yacm_derive") (v "0.1.3") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1llaidx7q7ilg5rys46pmbv2zwbrwjn4lasqwav0h3cw0lgmaair")))

