(define-module (crates-io ya cm yacm) #:use-module (crates-io))

(define-public crate-yacm-0.1.0 (c (n "yacm") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.0") (d #t) (k 2)))) (h "1lppahkxymn9aqgyffnzyixxg40nf06p6z2hbdibgh1pc5lxvcc8") (f (quote (("derive" "yacm_derive"))))))

(define-public crate-yacm-0.1.1 (c (n "yacm") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.0") (d #t) (k 2)))) (h "16rifdrs5hazkhrb18f9vjim4p9vkr9l4dm9v6ihnh7maabj6kqc") (f (quote (("derive" "yacm_derive"))))))

(define-public crate-yacm-0.1.2 (c (n "yacm") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.0") (d #t) (k 2)))) (h "1rcdhavz2yjgd7y3nxsx4948h465i4drsmnikidjibj6b0z49rnj") (f (quote (("derive" "yacm_derive"))))))

(define-public crate-yacm-0.1.3 (c (n "yacm") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "yacm_derive") (r "^0.1.3") (d #t) (k 2)))) (h "1ccc4xia8y7nj5vm2gsy3hziwcl36zqszmjcwdb6pgxqb2c3yv66") (f (quote (("derive" "yacm_derive"))))))

