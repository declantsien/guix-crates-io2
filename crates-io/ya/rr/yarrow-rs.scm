(define-module (crates-io ya rr yarrow-rs) #:use-module (crates-io))

(define-public crate-yarrow-rs-0.1.0 (c (n "yarrow-rs") (v "0.1.0") (d (list (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (k 0)))) (h "1rbxlw3ibs5qmc0vxhijakxbq0lhi1nzpwbibbzy41xqymf0mc1g")))

