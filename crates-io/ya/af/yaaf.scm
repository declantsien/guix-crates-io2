(define-module (crates-io ya af yaaf) #:use-module (crates-io))

(define-public crate-yaaf-0.1.0 (c (n "yaaf") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yaaf-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0naip8h3k4v8hm2rd940s37kx1c80wsx5znxz6iq54qh188avr5k")))

(define-public crate-yaaf-0.2.0 (c (n "yaaf") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)) (d (n "yaaf-macros") (r "^0.2.0") (d #t) (k 0)))) (h "095j949z7j2xcyx948mfc9w9z2kasqpvpa007m4yf7i9dcg07si7")))

