(define-module (crates-io ya af yaaf-macros) #:use-module (crates-io))

(define-public crate-yaaf-macros-0.1.0 (c (n "yaaf-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1x3md2lmgzwcppasv2yn5pwr8r8kmbldy26z2c9x0jfxsm8rxyxg")))

(define-public crate-yaaf-macros-0.2.0 (c (n "yaaf-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "10mf9q3n0jxclyrn2v3fagq9rpk8yflgm4827xkrc1188a8sfz6z")))

