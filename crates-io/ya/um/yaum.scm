(define-module (crates-io ya um yaum) #:use-module (crates-io))

(define-public crate-yaum-0.1.0 (c (n "yaum") (v "0.1.0") (h "18qd492zpw8c834nh68fkfpv62sxjymkvrz0ckbgrqzphr27h4kq") (f (quote (("double_precision"))))))

(define-public crate-yaum-0.1.1 (c (n "yaum") (v "0.1.1") (h "0cgwd6w2znx43bj4kvc627z0dxfm9yx0civxvqlcz1ivd9zmzdjs") (f (quote (("double_precision"))))))

(define-public crate-yaum-0.1.2 (c (n "yaum") (v "0.1.2") (h "05llx6bg56j8dr4lxcvnqhdb2872yakyrmgc61cjmmlxyz65ajzj") (f (quote (("double_precision"))))))

