(define-module (crates-io ya ce yacexits) #:use-module (crates-io))

(define-public crate-yacexits-0.1.2 (c (n "yacexits") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (k 0)))) (h "07887y5fyp6aw03r9zd9qdb9f48304ylwn8k4hla9xrjm89hdf00") (y #t)))

(define-public crate-yacexits-0.1.3 (c (n "yacexits") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (k 0)))) (h "1glrs039ifjbh49n1p5hna4plicirqnpwp55nfagx5gz1lkvxbxk")))

(define-public crate-yacexits-0.1.4 (c (n "yacexits") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (k 0)))) (h "0vqlqryr73xv5mq878k08vj6a0alf4xp1vgv08iib5zxg31frbp6")))

(define-public crate-yacexits-0.1.5 (c (n "yacexits") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (k 0)))) (h "066w1mrz1drmmsffk2jiw2ah38xyq7k45s1ijj8vq6sws06p9zjk")))

(define-public crate-yacexits-0.1.6 (c (n "yacexits") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.14") (k 0)))) (h "0nyykxs7iqav7q7lm5xsbb47jnrnaqri81gkidkf48vhki38a3gq")))

