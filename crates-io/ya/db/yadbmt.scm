(define-module (crates-io ya db yadbmt) #:use-module (crates-io))

(define-public crate-yadbmt-0.1.0 (c (n "yadbmt") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-async-std-native-tls" "postgres"))) (d #t) (k 0)))) (h "143qdrzhdv0i40k5wg9wdblavr9474iy8fw0dzsxh8fxyxh97s2x")))

(define-public crate-yadbmt-0.1.1 (c (n "yadbmt") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-async-std-native-tls" "postgres"))) (d #t) (k 0)))) (h "170hnzazbs4y5fm1jy4lnkhyv10sz6amlkp23mncn5aldlbdhpa9")))

