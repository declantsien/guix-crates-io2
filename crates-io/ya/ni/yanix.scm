(define-module (crates-io ya ni yanix) #:use-module (crates-io))

(define-public crate-yanix-0.9.0 (c (n "yanix") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11ad4rqsvxgscf57ymf0rzbsrkjhm3f0pqvg8za3a42fjcn0n4a6")))

(define-public crate-yanix-0.10.0 (c (n "yanix") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qklz0zslrks71qszkp3999r02lry8q6bxgjdnd5pba0girnfy42")))

(define-public crate-yanix-0.11.0 (c (n "yanix") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pchcg4dgdr3bf6biwm72ndxbwlcxiq98f9lcfr2nd9240yimlr5")))

(define-public crate-yanix-0.12.0 (c (n "yanix") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y1ybs7bh9jb4dssmvzqfpbzcpsxbjcznl0b4g7njqhv57akdafr")))

(define-public crate-yanix-0.13.0 (c (n "yanix") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0821kqxfz1jrnwb5hvfhdpfwim8mbxiw88xcr4ahkczsxrjjwrbs")))

(define-public crate-yanix-0.14.0 (c (n "yanix") (v "0.14.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wrymz8s3rzgzqs7gjdyi1r86mzh2x2pijyg5k5sh107mlggg7d6")))

(define-public crate-yanix-0.15.0 (c (n "yanix") (v "0.15.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1v4538nn8nhnpddm3gdxp38xv72ymmp85i6hq314650fy2xb7sag")))

(define-public crate-yanix-0.16.0 (c (n "yanix") (v "0.16.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vq59lis992k7fmvfb55ys1dyk75q4flcadimn7z9p7rhmy1m6vw")))

(define-public crate-yanix-0.17.0 (c (n "yanix") (v "0.17.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19njjcryiz8klji2dshac38sz6a02qvfnm0z4f2ai28sa0y6gl8g")))

(define-public crate-yanix-0.18.0 (c (n "yanix") (v "0.18.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0y6kcr5bsa8w6vwclayairmsqcimwd2a1m4l1kyw7h1f0dj15yk9")))

(define-public crate-yanix-0.19.0 (c (n "yanix") (v "0.19.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rr5s60mmhkvi6m61fzxcck2bwfr02cqks60yvnhpvs32xnl10px")))

(define-public crate-yanix-0.20.0 (c (n "yanix") (v "0.20.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.15") (d #t) (k 0)))) (h "0g4bqjjxka831yqlyccvnhyljxjn483cgl0vb630cix3m2ic01y5")))

(define-public crate-yanix-0.21.0 (c (n "yanix") (v "0.21.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.15") (d #t) (k 0)))) (h "0w4a745kiqwrmmivhk01x4shp8x4wlwimpy5xsbv45l62mdp4ifq")))

(define-public crate-yanix-0.22.0 (c (n "yanix") (v "0.22.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.15") (d #t) (k 0)))) (h "1nq1i710xzgi5kidsdxg9vhvji23rcdaa6flaw87zrxrhxmdf105")))

(define-public crate-yanix-0.23.0 (c (n "yanix") (v "0.23.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.15") (d #t) (k 0)))) (h "19q303cf99nars17psib6hvrw5j0r08h4ir8aw67gc60hmfz6qg8")))

