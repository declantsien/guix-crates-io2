(define-module (crates-io ya k- yak-sitter) #:use-module (crates-io))

(define-public crate-yak-sitter-0.3.0 (c (n "yak-sitter") (v "0.3.0") (d (list (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-json") (r "^0.19.0") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)))) (h "0smk35fawmj8l3yrvam26k4y19nzkqawnwfphh55zs9qlbsqgi1i")))

(define-public crate-yak-sitter-0.3.1 (c (n "yak-sitter") (v "0.3.1") (d (list (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)) (d (n "tree-sitter-json") (r "^0.20.2") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.4") (d #t) (k 2)))) (h "1m38qjn2vljd73v6bvax6pqmbwxjvbq8n62wgd7gga2ayk5z72zr") (y #t)))

(define-public crate-yak-sitter-0.4.0 (c (n "yak-sitter") (v "0.4.0") (d (list (d (n "streaming-iterator") (r "^0.1.9") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.2") (d #t) (k 0)) (d (n "tree-sitter-json") (r "^0.20.2") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.4") (d #t) (k 2)))) (h "1ns0k7mz3j0jykss8zsb5c2fg51fw6n6jppb1dk8k4qjndiisgf8")))

