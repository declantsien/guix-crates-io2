(define-module (crates-io ya gl yagl) #:use-module (crates-io))

(define-public crate-yagl-0.0.1 (c (n "yagl") (v "0.0.1") (d (list (d (n "a2d") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)))) (h "1fy5j634br5lngmhigjw63csknqf7549cczk8njqknjgqvrcpngk")))

(define-public crate-yagl-0.0.2 (c (n "yagl") (v "0.0.2") (d (list (d (n "a2d") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)))) (h "0lifg8hpgr2ry756vz37hjc0ckpmpnbdms4z5a8609bzqq82bfgv")))

(define-public crate-yagl-0.0.3 (c (n "yagl") (v "0.0.3") (d (list (d (n "a2d") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)))) (h "1r28xqcrbkvwvbyqnyk0rb0v03lc914kplxp990b0q12lqyidj3m")))

(define-public crate-yagl-0.0.4 (c (n "yagl") (v "0.0.4") (d (list (d (n "a2d") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)))) (h "16qfl9ikyx0ji0vsq54cq9dyafl1acvl5yfxs9q3dbsyz97x4a1s")))

(define-public crate-yagl-0.0.5 (c (n "yagl") (v "0.0.5") (d (list (d (n "a2d") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)))) (h "1xy4zx69bdzzf7vgcz960m31jl9axnfaq1n6qj2wg97dp9vjr9qa")))

(define-public crate-yagl-0.0.6 (c (n "yagl") (v "0.0.6") (d (list (d (n "a2d") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)))) (h "07fg1bxas7p0vfga7h1apaw5ddjr5hfmilaha622adv529wxhy5f")))

(define-public crate-yagl-0.0.7 (c (n "yagl") (v "0.0.7") (d (list (d (n "a2d") (r "^0.1.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)))) (h "0fpc70avz7kp0jx0hdvp1vj4snanya5624ncg32cm6yn2kz330gv")))

(define-public crate-yagl-0.0.8 (c (n "yagl") (v "0.0.8") (d (list (d (n "a2d") (r "^0.1.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)))) (h "0j4fd0rwg76y9mqk5j32f0q52j71yn5sfc8lgvvsvr412738ib3p")))

(define-public crate-yagl-0.0.9 (c (n "yagl") (v "0.0.9") (d (list (d (n "a2d") (r "^0.1.7") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)))) (h "064j14zfnf8n8s9h3376caama46bnxmm1pxsmb4qq7162cwmv6d0")))

(define-public crate-yagl-0.0.10 (c (n "yagl") (v "0.0.10") (d (list (d (n "a2d") (r "^0.1.8") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)))) (h "06wg69x94byiwg5fa9qbqxqifyj6lccrqkwvk1x9fzfhc3vnjfb2")))

