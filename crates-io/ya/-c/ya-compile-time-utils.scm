(define-module (crates-io ya -c ya-compile-time-utils) #:use-module (crates-io))

(define-public crate-ya-compile-time-utils-0.1.0 (c (n "ya-compile-time-utils") (v "0.1.0") (d (list (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "metrics") (r "^0.12") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0mji47hr65kis4xrybyxg71z9pn7nbcy6wlfx99zd4mqiz32k2hk") (y #t)))

(define-public crate-ya-compile-time-utils-0.1.1 (c (n "ya-compile-time-utils") (v "0.1.1") (d (list (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "metrics") (r "^0.12") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1ixxy9nad2nmh9psrmryvs9v4xmyhl8d39k2skq9sqvk0i7m3zm1")))

(define-public crate-ya-compile-time-utils-0.1.2 (c (n "ya-compile-time-utils") (v "0.1.2") (d (list (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "metrics") (r "^0.12") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "0wda2hlmp1s4mkpw4kcy30wwnrfwx8kzxv0rc1ngw5ghpa7habr5")))

(define-public crate-ya-compile-time-utils-0.1.3 (c (n "ya-compile-time-utils") (v "0.1.3") (d (list (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "metrics") (r "^0.12") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "16j0k509hc4zbari2kb5r83rabxxfd0m3s3gq6cfn372qkw2ji6g")))

(define-public crate-ya-compile-time-utils-0.2.0 (c (n "ya-compile-time-utils") (v "0.2.0") (d (list (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "metrics") (r "^0.12") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1rf3sj4r9qpvmnzwcz5hg4hmihyn6jq49bbvwqgk3y8814l5lfgb")))

(define-public crate-ya-compile-time-utils-0.2.1 (c (n "ya-compile-time-utils") (v "0.2.1") (d (list (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "metrics") (r "^0.12") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "1ncvb07xqiizkdgv8qcbjs191pszq4ic51awiar8zawrr74abhf2")))

