(define-module (crates-io ya ng yangfmt) #:use-module (crates-io))

(define-public crate-yangfmt-0.1.0 (c (n "yangfmt") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "17y5hq6vq59npm89i7yl73h1mlsqwni6n3rfiyn1iqz84pyyqsvx")))

(define-public crate-yangfmt-0.1.1 (c (n "yangfmt") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "1hjmjy2syi042266f3n7rhlvi11s5pvdjg7lhrccamvx1rmkwbsj")))

(define-public crate-yangfmt-0.1.2 (c (n "yangfmt") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "0ldcb60vw912gh2mlmi08crq59pzf9hy36bz0238fcbhyl0prjqa")))

