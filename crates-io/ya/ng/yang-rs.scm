(define-module (crates-io ya ng yang-rs) #:use-module (crates-io))

(define-public crate-yang-rs-0.1.0 (c (n "yang-rs") (v "0.1.0") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "08hqlav4zy9pjyjhjdvvl64cm73wnxv8qpy8239f4cb7k6i9inda") (y #t)))

(define-public crate-yang-rs-0.1.1 (c (n "yang-rs") (v "0.1.1") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1rbi8i3vs5qj55avmgdjwfxs6nk7iaarivj9x24dhbl2bgpny5gf")))

