(define-module (crates-io ya di yadict) #:use-module (crates-io))

(define-public crate-yadict-0.1.0 (c (n "yadict") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1jcnfnxxi4dlak6hn7lz6xkqwljynnb6r0m0ah733r653im0m9jl")))

(define-public crate-yadict-0.1.1 (c (n "yadict") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "07ldzaqhvji1fnk666b1mspxijn8vfds4anavnkfqnif18qmcnd3")))

