(define-module (crates-io ya di yadio) #:use-module (crates-io))

(define-public crate-yadio-0.1.0 (c (n "yadio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3.3") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("aac" "mpa"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "youtube_chat") (r "^0.2.0") (d #t) (k 0)) (d (n "yt_tsu") (r "^0.1.0") (d #t) (k 0)))) (h "146qs51y98z2qdk9dh4gj19bx2x76b40hsxrhpyw8ya2v6svxbha")))

