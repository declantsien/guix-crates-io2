(define-module (crates-io ya -w ya-world-time-sync) #:use-module (crates-io))

(define-public crate-ya-world-time-sync-0.1.3 (c (n "ya-world-time-sync") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sntpc") (r "^0.3.2") (f (quote ("utils"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "macros" "net"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)) (d (n "ya-world-time") (r "^0.1.3") (d #t) (k 0)))) (h "114zgx2qm2acpijjiksg0xlcb7pxf4xgpvap3jjmmk66k13a5vkk")))

