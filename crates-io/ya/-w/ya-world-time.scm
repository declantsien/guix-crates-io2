(define-module (crates-io ya -w ya-world-time) #:use-module (crates-io))

(define-public crate-ya-world-time-0.1.1 (c (n "ya-world-time") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sntpc") (r "^0.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net"))) (d #t) (k 0)))) (h "16pdvsxx7cj6hbnpylbmyqymyl0sgg40rkjzznc3bpchzvj8w1nx")))

(define-public crate-ya-world-time-0.1.3 (c (n "ya-world-time") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "sntpc") (r "^0.3.2") (d #t) (k 0)))) (h "1g5p3pzc8g5qakxs2s4bgl0vxgkfgx3j5zrxgpyaxlz62ff7w9p9")))

