(define-module (crates-io ya ac yaac) #:use-module (crates-io))

(define-public crate-yaac-0.1.0-alpha.1 (c (n "yaac") (v "0.1.0-alpha.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "180ninqvhm7wx3iz45017plzp9km5g6ah780x31sf3j237hgkgw9")))

(define-public crate-yaac-0.1.0-alpha.2 (c (n "yaac") (v "0.1.0-alpha.2") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0j2pwvljba2f4af79nmiymi28gg1ack3310ln8yy3lqn769x8kyi")))

(define-public crate-yaac-0.1.0 (c (n "yaac") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0kswx2j2hlsxdbl4z7pbkzd4rzsb34fss4mg6dk34sfi7cz2jh65")))

(define-public crate-yaac-0.1.1 (c (n "yaac") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "13a0dsb1112x5an5rwnicf982n6817649fmffqv5l2r4s11sykqg")))

(define-public crate-yaac-0.1.2 (c (n "yaac") (v "0.1.2") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "07qqwwy05flavrn9dh7qhxsynis2pyil7w3w47cq3x7k8c4sb8ap")))

