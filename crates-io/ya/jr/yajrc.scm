(define-module (crates-io ya jr yajrc) #:use-module (crates-io))

(define-public crate-yajrc-0.1.0 (c (n "yajrc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16n74jv8rfk67lnrn2cqgiz8jdhghk45a03k6l77djv5q6s8f1ml") (f (quote (("strict"))))))

(define-public crate-yajrc-0.1.1 (c (n "yajrc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lw7xyxl2pcr6kv0kbl64d3437dixvnilsl8xyhp85ksa1iba26c") (f (quote (("strict"))))))

(define-public crate-yajrc-0.1.2 (c (n "yajrc") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z7kdslb8m0kvc3jcziv21c3gz4qzl6x4kq7vnh26sgv475k7js7") (f (quote (("strict"))))))

(define-public crate-yajrc-0.1.3 (c (n "yajrc") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gs89829jpkvmvhzkxjbxxzfnvg6b7c8gvrkfcszihl3v5xg8ynf") (f (quote (("strict"))))))

