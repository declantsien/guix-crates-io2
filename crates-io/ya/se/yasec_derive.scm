(define-module (crates-io ya se yasec_derive) #:use-module (crates-io))

(define-public crate-yasec_derive-1.0.0 (c (n "yasec_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "1dw4nlp1zdmj4iqhja6lwrfx2650ljk4095pf6fvpjpj5yxh8mwk")))

