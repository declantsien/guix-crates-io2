(define-module (crates-io ya se yaserde) #:use-module (crates-io))

(define-public crate-yaserde-0.1.0 (c (n "yaserde") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.1") (d #t) (k 2)))) (h "14fmavhm9fga6vj4lrqgrsl54k1cz8dfydiqqp7dx0zagrhc6z7p")))

(define-public crate-yaserde-0.2.0 (c (n "yaserde") (v "0.2.0") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.2") (d #t) (k 2)))) (h "1czs4sx0zy2fwijg223ys5cpggxl7nhm90bs8yavy9iq94rfq0x9")))

(define-public crate-yaserde-0.3.0 (c (n "yaserde") (v "0.3.0") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3") (d #t) (k 2)))) (h "16x0yc5hqx5js3k8s5ly2418f11bmgacg56m76i3cx6j2qwn1jnd")))

(define-public crate-yaserde-0.3.1 (c (n "yaserde") (v "0.3.1") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (d #t) (k 2)))) (h "05x7i27aqzcsy6ahpmv3cim2dwmilms6jy0509d9ch11rygs5wbi")))

(define-public crate-yaserde-0.3.2 (c (n "yaserde") (v "0.3.2") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (d #t) (k 2)))) (h "1cv46mb226imw0ib0vyvqg199d9r6z5sha32dsxkqvpzrix8nm00")))

(define-public crate-yaserde-0.3.3 (c (n "yaserde") (v "0.3.3") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (d #t) (k 2)))) (h "1mr84x6qzp2q00rjx1hmcjwrvcpb031yy4hd0hf7gihzrirrl4q5")))

(define-public crate-yaserde-0.3.4 (c (n "yaserde") (v "0.3.4") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (d #t) (k 2)))) (h "12mg3rzc4kn5xykcssfrs4xbddndj2hq8ghpn2bw3cdifwxg1p2x")))

(define-public crate-yaserde-0.3.5 (c (n "yaserde") (v "0.3.5") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.1") (d #t) (k 2)))) (h "1a0cwg4d5ac8sqilby54zxndslcr6kkmpi2v8p1ag959m1cr10z5")))

(define-public crate-yaserde-0.3.6 (c (n "yaserde") (v "0.3.6") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.6") (d #t) (k 2)))) (h "1bzp0vlly70syswcs98mfzh83jb99dqayjlcsv7jjfycsl76lj8v")))

(define-public crate-yaserde-0.3.7 (c (n "yaserde") (v "0.3.7") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (d #t) (k 2)))) (h "04cyygp4dd0xgxwf0v6cflmy3pikvrw5qkqkymikkp72a4f4ribi")))

(define-public crate-yaserde-0.3.8 (c (n "yaserde") (v "0.3.8") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (d #t) (k 2)))) (h "09j7mb4fb1z54248cc9yg92clii681avvgblxd4l52z6xvvkjpx5")))

(define-public crate-yaserde-0.3.9 (c (n "yaserde") (v "0.3.9") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (d #t) (k 2)))) (h "141ihq0hhnd9lcfqw1krm1av6mqwprc6izx5nvpr982mffazb7y6")))

(define-public crate-yaserde-0.3.10 (c (n "yaserde") (v "0.3.10") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.7") (d #t) (k 2)))) (h "1mgwwyji792yssj87bvh09p20q16pkpc7i131pgbg0dr64s8a770")))

(define-public crate-yaserde-0.3.11 (c (n "yaserde") (v "0.3.11") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (d #t) (k 2)))) (h "14q8fjbiigz7mqwmdrh307cq2svq59jcskrjxslvznbgihy7y43s")))

(define-public crate-yaserde-0.3.12 (c (n "yaserde") (v "0.3.12") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (d #t) (k 2)))) (h "06b96w0y0cdzmi285wf6gych096j6p7sakfjz246dr6gk1kzq70l")))

(define-public crate-yaserde-0.3.13 (c (n "yaserde") (v "0.3.13") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (d #t) (k 2)))) (h "0fsynk3dm8qqy2hs7spvdp7zzpr84prr3nlp91xgkkqvdjxld6rk")))

(define-public crate-yaserde-0.3.14 (c (n "yaserde") (v "0.3.14") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (d #t) (k 2)))) (h "1il2wmqylcc5r10n8qdr60a0s2m34xhw37h4xhf0xnn0dhi491lf")))

(define-public crate-yaserde-0.3.15 (c (n "yaserde") (v "0.3.15") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (d #t) (k 2)))) (h "0sjqmncn76m68fri2ahhwqjpfbbrx114hkk497qf8gzml19cfaq5")))

(define-public crate-yaserde-0.3.16 (c (n "yaserde") (v "0.3.16") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.11") (d #t) (k 2)))) (h "12bwxszz8alzrq4f0afsb531izrfqydilz0ahmqdl9nr6rdhb0gy")))

(define-public crate-yaserde-0.4.0 (c (n "yaserde") (v "0.4.0") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "0d3n9abiwf8valdh9r6wpg6h3zjm7q0n6ivsw3vcp354qzjjvxv7")))

(define-public crate-yaserde-0.4.1 (c (n "yaserde") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "0ncw90bg51i4d60w031z0dqrg8ai39cljf4pbx2y207d7cl65dhm")))

(define-public crate-yaserde-0.4.2 (c (n "yaserde") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "1vy9hxrjx9qrb78859fm4h06zrskn71kcgxphas7nsxsbda2wg6a")))

(define-public crate-yaserde-0.4.3 (c (n "yaserde") (v "0.4.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.4") (d #t) (k 2)))) (h "1f1rj4ksap235784g268pnqq372im87vlzg8lpa3zxrc485rhpj4")))

(define-public crate-yaserde-0.5.0 (c (n "yaserde") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.5") (d #t) (k 2)))) (h "1390j0zw5frcrd9qk72adcanilx65mi3xvy6xpfn0lq8cgdzvwpg")))

(define-public crate-yaserde-0.5.1 (c (n "yaserde") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.5") (d #t) (k 2)))) (h "0m7c9wgfrr5jj7iddmccwzghnrcbqvh6x55zin8l26n4rr7rzv5b")))

(define-public crate-yaserde-0.6.0 (c (n "yaserde") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.6") (d #t) (k 2)))) (h "0phb99g35c36vpabc1fbf12w3k2isr7vn58ac3hgxj4yxvxnw2dw")))

(define-public crate-yaserde-0.7.0 (c (n "yaserde") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.0") (d #t) (k 2)))) (h "0s00cszrlz2y2dr3xjzqwj6ka4xbcbq2dlanskmk8vywpxlbl33s")))

(define-public crate-yaserde-0.7.1 (c (n "yaserde") (v "0.7.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.1") (d #t) (k 2)))) (h "1z47kw0s7pcaci6iwxz9am7vjy60cvmf31wf4s4nvrr0pg2nwxx2")))

(define-public crate-yaserde-0.8.0 (c (n "yaserde") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.8.0") (d #t) (k 2)))) (h "10h5z8ys79bv1gasyjgy7pdri8pwssmpwgfnm9m8c2x5aksjmxab")))

(define-public crate-yaserde-0.9.0 (c (n "yaserde") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.9.0") (d #t) (k 2)))) (h "1sfn6n8pn2kdyf3yfpx2n605skr4r2nfszjz0x8zk3ybb62akbf3")))

(define-public crate-yaserde-0.9.1 (c (n "yaserde") (v "0.9.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.9.1") (d #t) (k 2)))) (h "0vvxa50j7rrdyz2cgkzxascmailja29avq54kmbpmzq0abpg3b4f")))

(define-public crate-yaserde-0.9.2 (c (n "yaserde") (v "0.9.2") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.9.2") (d #t) (k 2)))) (h "0a340mwq2gvivnvp2dmcwbhfgfzdn8p1jmbwzsz8x92j9k0kdvya")))

(define-public crate-yaserde-0.10.0 (c (n "yaserde") (v "0.10.0") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.10.0") (d #t) (k 2)))) (h "0p53lqyhm8jk29kkl5ymnbvni9k1k2npf19wpmh1l3nfp6g2xxg8")))

