(define-module (crates-io ya se yaserde-examples) #:use-module (crates-io))

(define-public crate-yaserde-examples-0.9.0 (c (n "yaserde-examples") (v "0.9.0") (d (list (d (n "yaserde") (r "^0.9.0") (f (quote ("yaserde_derive"))) (d #t) (k 0)))) (h "09wrvvd3pq09g4l789jwnwcka5x3hbpw9lc8bnlxcms4j2m68jy0")))

(define-public crate-yaserde-examples-0.9.1 (c (n "yaserde-examples") (v "0.9.1") (d (list (d (n "yaserde") (r "^0.9.1") (f (quote ("yaserde_derive"))) (d #t) (k 0)))) (h "07l2pslm7gmxqak3x6mqc9wqp3gl46sszsdia4kbsn10c3d2j9f0")))

(define-public crate-yaserde-examples-0.9.2 (c (n "yaserde-examples") (v "0.9.2") (d (list (d (n "yaserde") (r "^0.9.2") (f (quote ("yaserde_derive"))) (d #t) (k 0)))) (h "1jbfq7rfskfg8xwi539fxc9lpp5y2jfwv1n269q1x4phzwwdabmc")))

(define-public crate-yaserde-examples-0.10.0 (c (n "yaserde-examples") (v "0.10.0") (d (list (d (n "yaserde") (r "^0.10.0") (f (quote ("yaserde_derive"))) (d #t) (k 0)))) (h "1ngm96xjc3h81hh3jkhagi5c9ayifbinl69mgic2z69f2axqkl06")))

