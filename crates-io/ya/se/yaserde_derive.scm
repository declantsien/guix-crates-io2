(define-module (crates-io ya se yaserde_derive) #:use-module (crates-io))

(define-public crate-yaserde_derive-0.1.0 (c (n "yaserde_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "04c0x2mgzg7hwmvm618rn5ckd7j80z825542pni894zmj2ywpww4")))

(define-public crate-yaserde_derive-0.2.0 (c (n "yaserde_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1xzn6mkkly6h4rh3jr0s8khay322ljfyfhjr55pxwbngsackj96p")))

(define-public crate-yaserde_derive-0.3.0 (c (n "yaserde_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1b082i5x0ph3p0d456wqa5mmq8vgmswiiz6mk3inmlwkyq8sy67p")))

(define-public crate-yaserde_derive-0.3.1 (c (n "yaserde_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0mcwy4x707dylk1501bfrpdpv1r6zxmz6n5rlxlqhzs6sm6w1x45")))

(define-public crate-yaserde_derive-0.3.2 (c (n "yaserde_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1waqis0hlavmjmxrfw65gb58rrzgifwiw0qpq7p4c4qb6hsi0dqm")))

(define-public crate-yaserde_derive-0.3.3 (c (n "yaserde_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "08f506q6m652q0r7pa3k6nayc0a74dlals3cg190iaz5szcfqwyz")))

(define-public crate-yaserde_derive-0.3.4 (c (n "yaserde_derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "19h7nh98rb6wxxfnyiixlnbi6abj4q7pscifgkkfnbaal7nmwa1w")))

(define-public crate-yaserde_derive-0.3.5 (c (n "yaserde_derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1inqfxyadhxh2px3cw590jxazvx1m6v7qfr26q18scvvism8c209")))

(define-public crate-yaserde_derive-0.3.6 (c (n "yaserde_derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "10jkcsm0b2jv8dqh7vqwdbxd1whf705kgjb7gx3gyglaq37vmfx3")))

(define-public crate-yaserde_derive-0.3.7 (c (n "yaserde_derive") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0nhhlwvlp6icy4c8wm76q35rqlhv1mkhzlnww6khgmaa4zz9131n")))

(define-public crate-yaserde_derive-0.3.8 (c (n "yaserde_derive") (v "0.3.8") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0nwkhhmfkq6qizmrci4dndacnzz7lg9lr063x9b98fvvqxsj5qv0")))

(define-public crate-yaserde_derive-0.3.9 (c (n "yaserde_derive") (v "0.3.9") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1271sg8ihkqhw44361fqx1b8vqvldy8b7qvikhzs7ajhkrrky187")))

(define-public crate-yaserde_derive-0.3.10 (c (n "yaserde_derive") (v "0.3.10") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0rxnwjxymj4pnz8c884kqs11rhjwwca1lbdn4lf5q5g1cqyisylf")))

(define-public crate-yaserde_derive-0.3.11 (c (n "yaserde_derive") (v "0.3.11") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1s26sh0pvzxkm0x62awhlbxndqd7p9p6421qqbp4hzmdlcxan491")))

(define-public crate-yaserde_derive-0.3.12 (c (n "yaserde_derive") (v "0.3.12") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "02nr65vlcbhqwyyhy980x4jzn5jf1phra2m8wikzlyfdcdcrf7sa")))

(define-public crate-yaserde_derive-0.3.13 (c (n "yaserde_derive") (v "0.3.13") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0a2v363qk1bj72q3jzm88ipcrip6zh73q8rzf8885v9d9l05zyz8")))

(define-public crate-yaserde_derive-0.3.14 (c (n "yaserde_derive") (v "0.3.14") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0d8mij7vfkrl9mykprw6l0522fjcaqypy4s69s2w2yjlbwy0aq5d")))

(define-public crate-yaserde_derive-0.3.15 (c (n "yaserde_derive") (v "0.3.15") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "047gwpk440h6hrnch1h7h8sds351p80z7wacr3ismh1j55p8d1zp")))

(define-public crate-yaserde_derive-0.3.16 (c (n "yaserde_derive") (v "0.3.16") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "014138q4vb2s1l0h8p84bacn5nlck63fxz1ibm0prxy0xaca0m90")))

(define-public crate-yaserde_derive-0.3.17 (c (n "yaserde_derive") (v "0.3.17") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "05552smmx7inj189hrqjsyl5rj654fqi9vvsry4id5w7pf4g1kaz")))

(define-public crate-yaserde_derive-0.4.0 (c (n "yaserde_derive") (v "0.4.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1apysj7i1mdxx9z4y70iap5l8kppc2nlifi0yzaffk1fxabn2kjr")))

(define-public crate-yaserde_derive-0.4.1 (c (n "yaserde_derive") (v "0.4.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0qq5hlym87628dczrp4q1sm06i5lk549kxjw9pql1x6dbm7sdm2n")))

(define-public crate-yaserde_derive-0.4.2 (c (n "yaserde_derive") (v "0.4.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0kg3qkrjifhcva5wgqh8pildfpmwgpd0m8jwj4j7z6h254n81p06")))

(define-public crate-yaserde_derive-0.4.3 (c (n "yaserde_derive") (v "0.4.3") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1a94j13hdm4hpmjzkmg7ls9c7n5a0an4xapggi6r21cf7ki38mzb")))

(define-public crate-yaserde_derive-0.5.0 (c (n "yaserde_derive") (v "0.5.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "03wmb3sg6hx5pcb6i2lpw675z25l56hfg6c8kfimsmdmgxgydscy")))

(define-public crate-yaserde_derive-0.5.1 (c (n "yaserde_derive") (v "0.5.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0q7874306ldf9xs3ia5wdpl25qfrcfn7flkgrd6wm0bpjy8c3skk")))

(define-public crate-yaserde_derive-0.6.0 (c (n "yaserde_derive") (v "0.6.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "1fvpm54ygfj6njmr8nbh2a5kyn1r3pbagx2whbfid5q390fizy4s")))

(define-public crate-yaserde_derive-0.7.0 (c (n "yaserde_derive") (v "0.7.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "11bhybkv5d90j0gzwv8f9wd4cals8cq5cxlhwvgdy3c4qzqxwc38")))

(define-public crate-yaserde_derive-0.7.1 (c (n "yaserde_derive") (v "0.7.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "03ak70xgzzkgzkfkkp3vc5xs0njpp1mim5z4rjpfn0zj053hl2sw")))

(define-public crate-yaserde_derive-0.8.0 (c (n "yaserde_derive") (v "0.8.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "07r2zidivmg02a21f1nx01dqipfvpl5d6cv8na087fzffrfbvf3s")))

(define-public crate-yaserde_derive-0.9.0 (c (n "yaserde_derive") (v "0.9.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "131gni9xanvhmdncwiv8d520jfsmk7pmcs8izyfcm5a27ilf2r36")))

(define-public crate-yaserde_derive-0.9.1 (c (n "yaserde_derive") (v "0.9.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1l4pp8qa7l4j4mdn35gwcqqw0h07i1ch8fjn0qip966xl3gjyz4j")))

(define-public crate-yaserde_derive-0.9.2 (c (n "yaserde_derive") (v "0.9.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0zqqbbsvpbi9m3zm0as9lndw2xr2qyrigv1xypw8kjxiz252cwmd")))

(define-public crate-yaserde_derive-0.10.0 (c (n "yaserde_derive") (v "0.10.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1v5d6sp3gfc0rf7w29xjcyd4wn7klaady83s1ri5dapvhvfnky0b")))

