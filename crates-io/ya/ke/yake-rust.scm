(define-module (crates-io ya ke yake-rust) #:use-module (crates-io))

(define-public crate-yake-rust-0.1.0 (c (n "yake-rust") (v "0.1.0") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "08b96kvbql1v494bczisdb00sidb0a4aq6jqnh32sdsifgk4xs8q")))

(define-public crate-yake-rust-0.1.1 (c (n "yake-rust") (v "0.1.1") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1lidp4pdjdz5km3amd83lybph4fw67zy617k9vfsix2zv2x4irgz")))

(define-public crate-yake-rust-0.1.2 (c (n "yake-rust") (v "0.1.2") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1z12m3s49cznsswc6vg5adphgkgcw0lxfkw1kxdcqwxwc1rkrn23")))

(define-public crate-yake-rust-0.1.3 (c (n "yake-rust") (v "0.1.3") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "004lr0azb26y2nxmwmdwnvrhgib06wqy3ypc89gd1xqw55zl3zwa")))

(define-public crate-yake-rust-0.1.4 (c (n "yake-rust") (v "0.1.4") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0n470dmslfwpnjn3sldbbhafrj405xi8qmzi7w73ajdyrwmxb74f")))

(define-public crate-yake-rust-0.1.5 (c (n "yake-rust") (v "0.1.5") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1a8973mj95c0prj0cf0877lxkfz3dwar9mwijv5g1lgv63m6cfym")))

(define-public crate-yake-rust-0.1.6 (c (n "yake-rust") (v "0.1.6") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0iln5iy3mh1l2s7r3nsh3mn35i5s88nyla3m3fca36pk7f6wxhzj")))

(define-public crate-yake-rust-0.1.7 (c (n "yake-rust") (v "0.1.7") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0ji53d4sqrdpkxj4zhp3dbcnp67ri0a0x1lpircf6wmw04y214rm")))

(define-public crate-yake-rust-0.1.8 (c (n "yake-rust") (v "0.1.8") (d (list (d (n "contractions") (r "^0.5.4") (d #t) (k 0)) (d (n "natural") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1hjpjv8l1c6mzzv3vdsnjlw01906gw81py8x84pfzphivarcjsdf")))

