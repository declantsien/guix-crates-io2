(define-module (crates-io ya r- yar-hash) #:use-module (crates-io))

(define-public crate-yar-hash-0.1.0 (c (n "yar-hash") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "scroll") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "yzb64") (r "^0.1") (d #t) (k 0)))) (h "1bq51d2avci0vx923b8dfcnbrbbx0h73adfa7c9phl0b9fmwv3g4") (f (quote (("rayon" "blake3/rayon") ("default" "scroll" "serde"))))))

