(define-module (crates-io ya ec yaecs) #:use-module (crates-io))

(define-public crate-yaecs-0.1.0 (c (n "yaecs") (v "0.1.0") (d (list (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "0is78c3fvh9h5f1v9949znrnw2idsni5npdzphg6dsw14c1hmxzq")))

(define-public crate-yaecs-0.3.0 (c (n "yaecs") (v "0.3.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "0zi34sh8nxgxpyv19y39cdnzq1qwwhmdij26xlfny2p9px7zr225")))

(define-public crate-yaecs-0.4.0 (c (n "yaecs") (v "0.4.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1g33d2yrbl2c5a9cgjnnx01cvda6p44s5lr6hxgf1g4fslw8qz62")))

(define-public crate-yaecs-0.5.0 (c (n "yaecs") (v "0.5.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1146gljwms7bn33z01m5yy2hh470p28i9ahwvxcmd9mcy85nl8n8")))

(define-public crate-yaecs-0.5.1 (c (n "yaecs") (v "0.5.1") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1a9cvml3mdrj8jb2sjiy8jp5gzwclhffnz409wv283w10jarwliy")))

(define-public crate-yaecs-0.5.2 (c (n "yaecs") (v "0.5.2") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1wpmiaxdnr9qlfccclka46s4bn0wrhqn4cwxvwn2p3lpk59xnj6m")))

(define-public crate-yaecs-0.5.3 (c (n "yaecs") (v "0.5.3") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1djsy34hplzmapc5np9qc9w3a0n6bqckr288np8akxq81cdmal2h")))

(define-public crate-yaecs-0.6.0 (c (n "yaecs") (v "0.6.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1dy4wiib8lmzpry58h7s0aiax04pnjscraibaazzy2x9r3axrmfj")))

(define-public crate-yaecs-0.7.0 (c (n "yaecs") (v "0.7.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "0prmwnjw5n5gbgrwvdc6l3fm2gsis852ic0lvqh23xcqwf8pzhbv")))

(define-public crate-yaecs-0.7.1 (c (n "yaecs") (v "0.7.1") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "0mbw7hjpjgj39xsyr71542kal7ypbdrmajw9sxry0zvs5r1309m0")))

(define-public crate-yaecs-0.7.2 (c (n "yaecs") (v "0.7.2") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1a4fkm2cz9hsa0zfjk6fh47rjcgnhwxjswki9wz6n0db4mpm9zqa")))

(define-public crate-yaecs-0.8.0 (c (n "yaecs") (v "0.8.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "0vcffgb747wyi8hyhlfvqaiqi13zdxwlv3syn2hb7cz1jdzw1r0c")))

(define-public crate-yaecs-0.8.1 (c (n "yaecs") (v "0.8.1") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "0im2j7nrb39nkl3972mj9wfsfhsarlgryzsbmrsbfqaiivrlik2g")))

(define-public crate-yaecs-0.9.0 (c (n "yaecs") (v "0.9.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "0kcf99alpa6d7902yn3x7yx239waywhx9hsk4hnxwlapgxip57d6")))

(define-public crate-yaecs-0.9.1 (c (n "yaecs") (v "0.9.1") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1137ikqiqnf29d8y7gx5irk8rpxwz5ff1lgh1xcr9kp0sz704rq6")))

(define-public crate-yaecs-0.9.2 (c (n "yaecs") (v "0.9.2") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1k4yrs8vg01b8ms6ryr6m0rs1dqwk23v29i0fxmmzablgn3a27vb")))

(define-public crate-yaecs-0.9.3 (c (n "yaecs") (v "0.9.3") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "1fmsw3p11m0nx6alxnnlkil61k621ggsjhg6ylsz7kfkx6fk7643")))

(define-public crate-yaecs-0.10.0 (c (n "yaecs") (v "0.10.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "173bd5kapq9zrw47lc7ic15hrz318ipxvpn3mdnlpa019kk8qaqn")))

(define-public crate-yaecs-0.11.0 (c (n "yaecs") (v "0.11.0") (d (list (d (n "anymap") (r "^0.12.0") (d #t) (k 0)) (d (n "avow") (r "^0.1.0") (d #t) (k 2)))) (h "03zph8zhnrvci314q0x0cmarzfikqy3qpg3wgjx5fkkq24446fzl")))

