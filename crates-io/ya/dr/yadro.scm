(define-module (crates-io ya dr yadro) #:use-module (crates-io))

(define-public crate-yadro-0.0.1 (c (n "yadro") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "rustls") (r "^0.21.6") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (d #t) (k 0)) (d (n "yadro-codegen") (r "^0.0.1") (d #t) (k 0)))) (h "0xmxjjq2jvgna5aj690ib1l624j3dhqp1cq1filrdkx4qh3a1kxr")))

