(define-module (crates-io ya gr yagraphc) #:use-module (crates-io))

(define-public crate-yagraphc-0.1.0 (c (n "yagraphc") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0b0hcm9dm3y1blvsmlz4hklwcjz0fqrwczin1a427s9k327i1zpg")))

(define-public crate-yagraphc-0.1.1 (c (n "yagraphc") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "13206mgqmybgkzd7bp718hbkr95z2may95hdjndrqi9xy7m1hcgr")))

(define-public crate-yagraphc-0.1.2 (c (n "yagraphc") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1amjsn6v1qzqdapjvpm9lnalxi62g02r9isg05h1fnps2a3z07x1")))

