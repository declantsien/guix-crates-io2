(define-module (crates-io ya ta yata) #:use-module (crates-io))

(define-public crate-yata-0.1.2 (c (n "yata") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02gmvpl9s2m9zw7gndbsx63mrimy9kh7iv0pl8dgdwi3azngfjaq") (f (quote (("value_type_f32") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.1.3 (c (n "yata") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q4m9z11dcamdfc8r6krajlikh9iihwch9avkl5hf23hdlpbpfkm") (f (quote (("value_type_f32") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.1.4 (c (n "yata") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07kxklcrf9d97s8wiwp77r35y95m5r25i3280085s6jwv1ffd7gi") (f (quote (("value_type_f32") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.1.5 (c (n "yata") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rcxjnh4qgbmf91mkdifylcml8h05idx5ykihfmpy52ky2kh1xy3") (f (quote (("value_type_f32") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.1.6 (c (n "yata") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18j7zlqbp1vpijgy330zcy2629qnsyn657kl9zmqmln5xly4c2z3") (f (quote (("value_type_f32") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.1.7 (c (n "yata") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b4npnnp1pcjmdihhsp2371s9dgqz59s668pzgz2h2f6af90dwz4") (f (quote (("value_type_f32") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.2.0 (c (n "yata") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "114j84np3r44wjj7lg31sfkcmpbs81p4fpyk4qr83xmj135idanj") (f (quote (("value_type_f32") ("unsafe_perfomance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.2.1 (c (n "yata") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d9h1dxj5w0grcazf1kkjfw39inzz87sy0kwymkmps7ryxk4z25k") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.0 (c (n "yata") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qrh4jbxxs4q9xj60p1axkyyqqyk7a04nwv1k7baa47fswrqd424") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.1 (c (n "yata") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cl76h2wmb7bp3fmqgjs6pnls2da46l6ip4kslgl827c8g4sjla6") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.2 (c (n "yata") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hpx2f4lbzm2dkqry1mlyjj8scdb2j2154wg0rk6jvr10ar68dbv") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.3 (c (n "yata") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r8rwhkqkhqfd695waci5n19zj9yx8nyq2qczj5qr8glpanynf2j") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.4 (c (n "yata") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kvqry1d8lib8fr8rqcjfilbr50y2q555y26sdlq2bk017yjighn") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.5 (c (n "yata") (v "0.4.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cm69yh6dvz9a5l0rb695wwmz8jp76pf9ycqq364cm4cccy30crq") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.6 (c (n "yata") (v "0.4.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ki6wy4r21vkiqkckq2nrvivvaba9imcv8bych9wshxhr6rxdjll") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.4.7 (c (n "yata") (v "0.4.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "117vyr3dslqhsgivd586j66kixv92mqlcl5p74rry7151rnld8fl") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.5.0 (c (n "yata") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yjvmhwv1lyycc8775bp2l34zq88jbdh92brbwiynwf0cl7n378b") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.5.1 (c (n "yata") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ag1ykdvzivxfs8d9kc0b5h5j1lnm9qampr4i5ngcwdmpbnb05qi") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.6.0 (c (n "yata") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b3pf56hw3ll9kkhnnarwyawnrbk5zlf9swm4jp1srl6f7pjl2v9") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.6.1 (c (n "yata") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10zjg9k9bs30ssaa003dd091c4ml15r1vbl4zjcw7khr0kzsl07v") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.6.2 (c (n "yata") (v "0.6.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "002jd1vhf5r69hls33jch44zx4dh2v1vmhdgrzzg8g0dvfhzshvs") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.6.3 (c (n "yata") (v "0.6.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0myx7d4xs6hnb39714jv94vbi8lp794a8vxzkw5x556smvgbl66l") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

(define-public crate-yata-0.7.0 (c (n "yata") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17r25181sn7p2lc4w5q4sh1z9fx28c5fdh32892r7k9wzbfzhkkb") (f (quote (("value_type_f32") ("unsafe_performance") ("period_type_u64") ("period_type_u32") ("period_type_u16") ("default" "serde"))))))

