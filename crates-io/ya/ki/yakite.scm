(define-module (crates-io ya ki yakite) #:use-module (crates-io))

(define-public crate-yakite-0.1.7 (c (n "yakite") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1ydbhshcqdvs1953c8f8avdp29yfsm67pcjb6xhkabfkbnjnwsb6")))

(define-public crate-yakite-0.1.8 (c (n "yakite") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1263d1db3qylyl2wwsqqvlmshpknx9vfqr1inh53d29d2r7ny78m")))

(define-public crate-yakite-0.1.9 (c (n "yakite") (v "0.1.9") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1bbnl2d999qkhay0ga7xvjvkhvw84zr36q86kvwvb15snfqbkjx8")))

(define-public crate-yakite-0.1.10 (c (n "yakite") (v "0.1.10") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0q86yvamq4ck2ghka0gidb4y2bvbfi45098zy5cqxxn8mwm42p6g")))

