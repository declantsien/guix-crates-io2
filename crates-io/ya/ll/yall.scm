(define-module (crates-io ya ll yall) #:use-module (crates-io))

(define-public crate-yall-0.1.0 (c (n "yall") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "08lajkymgdx7ig5s9l0ix3hqnprywbwaad5vmj2bvhpr14154j2v")))

(define-public crate-yall-0.1.1 (c (n "yall") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1d8asa4r99jy7wn34brqf1hzqkl0wdmj5y359sscj2qihww26x3k")))

(define-public crate-yall-0.2.0 (c (n "yall") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0z1g6yvmahvih7kayl0m80q96ysssh9l0zg52v5zmrnyifs8zbyl")))

(define-public crate-yall-0.3.0 (c (n "yall") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0vdz0klj63fgimif5f3421sf1w8dyq6xyd7ylzb2mgf9grq90n7y")))

(define-public crate-yall-0.4.0 (c (n "yall") (v "0.4.0") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "clap") (r ">=2.0.0, <3.0.0") (k 2)) (d (n "log") (r ">=0.4.0, <0.5.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "0anr37h9x5y9rigdir5yliv7hmm9bxx03snai2baagaja2jn7qrd")))

(define-public crate-yall-0.5.0 (c (n "yall") (v "0.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1scx4bv3jppdgp6rm9k9v8lgbfzrvxj61p5sr0zh7kii7dn0bi8m")))

(define-public crate-yall-0.6.0 (c (n "yall") (v "0.6.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0sf6zqkv4hpcbxw430585rfqpp697wxqkdgkgm4ixyb04qpblxab")))

(define-public crate-yall-0.6.1 (c (n "yall") (v "0.6.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "0g02x521mi4y93dk177vg8h4cicgmvm8wci424jy3qpwg8xxyamw")))

