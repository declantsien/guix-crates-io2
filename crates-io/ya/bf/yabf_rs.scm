(define-module (crates-io ya bf yabf_rs) #:use-module (crates-io))

(define-public crate-yabf_rs-0.1.0 (c (n "yabf_rs") (v "0.1.0") (h "03sv630ircpwx6wp5mj8aw8vigkix2nqjhqa2bp4yi9lqhasgwky")))

(define-public crate-yabf_rs-0.1.1 (c (n "yabf_rs") (v "0.1.1") (h "1nyxmqq5b2999rg0vdqr69h9mdfsayv8a391mynfsyai1dkmyfpj")))

(define-public crate-yabf_rs-0.1.2 (c (n "yabf_rs") (v "0.1.2") (h "1bi81r0bg32xgm2k3hzbvxrkahcxxbq3chf79drjbq2ksk7bcpd1")))

