(define-module (crates-io ya bf yabfi) #:use-module (crates-io))

(define-public crate-yabfi-1.0.0 (c (n "yabfi") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "003dfhfyv41qmm49gb03r4g0wjxw0nwclh7qp9hsy9wg2jslqi2a")))

(define-public crate-yabfi-1.0.1 (c (n "yabfi") (v "1.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0lqd4s617jnwj21q0c3a82j0qhzy7402yjzigivw1wwrb1g0h3mz")))

