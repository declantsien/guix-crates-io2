(define-module (crates-io ya bf yabfr) #:use-module (crates-io))

(define-public crate-yabfr-1.0.0 (c (n "yabfr") (v "1.0.0") (h "0chza7ajc9cxqp8sifxspha6285ihql0q7n4rrzlbm2dn4pvdzdn")))

(define-public crate-yabfr-1.1.0 (c (n "yabfr") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sp7j52jz54bkws0sldc9mpxfvvdw2a3r4jwg1bgng69yfvl6m83")))

(define-public crate-yabfr-2.0.0 (c (n "yabfr") (v "2.0.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 2)))) (h "0gilkijnr8q38vj15bn2z3ndjada9wcnv5vibhw25ndmfy6hxw6b") (f (quote (("default" "console_error_panic_hook"))))))

