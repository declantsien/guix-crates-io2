(define-module (crates-io ya fn yafnv) #:use-module (crates-io))

(define-public crate-yafnv-0.1.0 (c (n "yafnv") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.18") (k 0)))) (h "0w1b0mx8940ig1c3k568fzpvchqbv6y0gdiwbd92mc10vx26jkw0") (f (quote (("std"))))))

(define-public crate-yafnv-0.1.1 (c (n "yafnv") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.18") (k 0)))) (h "0x6f96lw6sdprqcr5jxwzfbk6pnvv5r87gqifwwy9blrg207alkg") (f (quote (("std"))))))

(define-public crate-yafnv-1.0.0 (c (n "yafnv") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.18") (k 0)))) (h "06xajg6irx6zqvcpjpdh96yb0hlqnjqa14lnh09cgbj95h1n78a2") (f (quote (("std"))))))

