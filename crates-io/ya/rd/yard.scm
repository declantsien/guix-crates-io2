(define-module (crates-io ya rd yard) #:use-module (crates-io))

(define-public crate-yard-0.1.0 (c (n "yard") (v "0.1.0") (h "1wraci53139my96b0laz81qi9fxyggv977ncg85ry9w202s3vv7j")))

(define-public crate-yard-0.2.0 (c (n "yard") (v "0.2.0") (h "1b7g6g7qxqag91kpk4jmxcjlfy2q5anx43gszg3j7n5lc0i2mq6w") (y #t)))

(define-public crate-yard-0.1.1 (c (n "yard") (v "0.1.1") (h "0245zdnc59qi3wk1bap68l2yyfmmzn6g5fns9fcvjm4cjz36vizd")))

(define-public crate-yard-0.1.2 (c (n "yard") (v "0.1.2") (h "1d55cgalzkrgbqg3m1bpvi1dxplzfqa1m4jq346klnd4ga54qvc1")))

(define-public crate-yard-0.1.3 (c (n "yard") (v "0.1.3") (h "077q48s5vxnmwsjy2k7r8brcsc3cm3g1pk94gdbhpz6wgjykizcw")))

(define-public crate-yard-0.1.4 (c (n "yard") (v "0.1.4") (h "0x6c4c6y3fxrxw7zfkqvwnzcfixa979j3cw13l9qvzpa1b07kwn6")))

(define-public crate-yard-0.2.1 (c (n "yard") (v "0.2.1") (h "0q51h8x7c7jlir8mrrrh1j5ax4szdiw95rmkjfisz2yn899p4pr2")))

(define-public crate-yard-0.2.2 (c (n "yard") (v "0.2.2") (h "1ifavdpyxxg4vb0qz1ixm9mjdphsivwnq75jsaaix2yqljfxix96")))

(define-public crate-yard-0.2.3 (c (n "yard") (v "0.2.3") (h "0wx9mqcw0n4vlrn5qzwn32gyb42xzrxkbxisf58297az2bbadbha")))

(define-public crate-yard-0.2.4 (c (n "yard") (v "0.2.4") (h "0z1dppdnrw318gmh0nqlnw6yl20nnbyw8i3223xzfr9j23w96vm2")))

(define-public crate-yard-0.2.5 (c (n "yard") (v "0.2.5") (h "0s8l8bih337c75diwxbfhl4jn04x4pbqc4sgrnqzfrv7fbf2ygxq")))

(define-public crate-yard-1.0.0 (c (n "yard") (v "1.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1nw615y46dpgypvjc1cc9gj8adyp87mwmsnlhv58vx0v9n7zvi0w")))

