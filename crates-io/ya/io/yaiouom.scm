(define-module (crates-io ya io yaiouom) #:use-module (crates-io))

(define-public crate-yaiouom-0.1.0 (c (n "yaiouom") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "00rjy8w82cy7s48vmci972b7srnl9mvfj8djn8k49b3qapb8qqnh")))

(define-public crate-yaiouom-0.1.1 (c (n "yaiouom") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1h2wgz6y59p3a2jq60jvj87dc3jzv4qz7a4a6k05kwl1pmjx28g4")))

(define-public crate-yaiouom-0.1.2 (c (n "yaiouom") (v "0.1.2") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "0gxjk3b44i50b2dys3j7mmrx1ads7cjhm7qylzpya9rc8zqk85c0")))

(define-public crate-yaiouom-0.1.3 (c (n "yaiouom") (v "0.1.3") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0h3ilaqvsgmssp94rh10azq777vqd9xpa78l3ja9z9ds9zq9ih2l")))

