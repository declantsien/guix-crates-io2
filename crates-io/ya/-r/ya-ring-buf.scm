(define-module (crates-io ya -r ya-ring-buf) #:use-module (crates-io))

(define-public crate-ya-ring-buf-0.1.0 (c (n "ya-ring-buf") (v "0.1.0") (d (list (d (n "atomic-waker") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.5.4") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "test-util"))) (k 2)))) (h "13sg7p9dk05l3sifsyz3lr4093yvbvrpdllz4qr4p8pr2y1g9apq") (f (quote (("std") ("default" "std") ("async" "atomic-waker"))))))

