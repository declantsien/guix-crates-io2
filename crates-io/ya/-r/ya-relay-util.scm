(define-module (crates-io ya -r ya-relay-util) #:use-module (crates-io))

(define-public crate-ya-relay-util-0.1.0 (c (n "ya-relay-util") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (o #t) (d #t) (k 0)))) (h "1zq3fr02fkf3xnpdkf4bnnrg1x41xz1c0nx2kmy9i1i2rvv76sih") (f (quote (("payload" "bytes" "derive_more") ("default" "payload"))))))

