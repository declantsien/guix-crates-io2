(define-module (crates-io ya -r ya-runtime-sdk-derive) #:use-module (crates-io))

(define-public crate-ya-runtime-sdk-derive-0.1.0 (c (n "ya-runtime-sdk-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "081nnarwz4nkxdhsn4zsynwq5qd3dniij4g773h0cwdaip1cx0hs")))

