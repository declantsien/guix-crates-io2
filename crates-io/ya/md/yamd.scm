(define-module (crates-io ya md yamd) #:use-module (crates-io))

(define-public crate-yamd-0.1.0 (c (n "yamd") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0p06sakbvgqshq1msf9q0miajdbz66bvrgv5yk8jlkbk65gapfkq")))

(define-public crate-yamd-0.2.0 (c (n "yamd") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0h0rk0qb4s2lfaa4bvxf6czvxd1ybzwn421q2jfvnbxchnkd3fvg")))

(define-public crate-yamd-0.3.0 (c (n "yamd") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0ri9wihkx0vprrd2lbqvdrcryfzf79ghk8an6cpkh4zvdypn8z2n")))

(define-public crate-yamd-0.4.0 (c (n "yamd") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "14siv9p1kzpx1jwi9s6kq5jz7hkip04ckqnixmjbngzzgkwfkqq7")))

(define-public crate-yamd-0.5.0 (c (n "yamd") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "13izsffqm8jaj9nx1v5pvd72sygqzxbbr36yc9z3fs85szfd87l6")))

(define-public crate-yamd-0.5.1 (c (n "yamd") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1a3l9dmai9ihc250vh4h3c6vkzba5ffpvyii6la4i6jsg99yxbqk")))

(define-public crate-yamd-0.6.0 (c (n "yamd") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "17yyihid1qadp2l79l8qbi96qajs4mphxk58zff3f59wajyc9sw1")))

(define-public crate-yamd-0.6.1 (c (n "yamd") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "117agcdclm5gc4b3hqw6j3d4axwj2sr96g9kw6hzlbp9wgsp52dn")))

(define-public crate-yamd-0.7.0 (c (n "yamd") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0z3hy5pl5qmcrykgpjl17dc1ddj455a61ycbyv526ynlq9pvl76w")))

(define-public crate-yamd-0.8.0 (c (n "yamd") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1gqhhb1siw9n8fk9g54aggacp054yb5x9va7y564jbx20gnhh65f")))

(define-public crate-yamd-0.9.0 (c (n "yamd") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n4shqmgj9k6yfrzr2s530vl0lml9nvkvgm6p6in6ac9k4vbzwc6")))

(define-public crate-yamd-0.10.0 (c (n "yamd") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)))) (h "1axm2x6bclj4aa6ic6frzl27pkgzz4g0ayxn3h1vwfzr9hg65v21")))

(define-public crate-yamd-0.11.0 (c (n "yamd") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)))) (h "0y3khlqbfwmfa4zkj3bpqbmj0c43kl89maa3v09534r8vsc43yzq")))

(define-public crate-yamd-0.12.0 (c (n "yamd") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)))) (h "00ajsncxq1awq3k3sv9qbici6klyp5dawl1d2di608cjal71scmp")))

(define-public crate-yamd-0.12.1 (c (n "yamd") (v "0.12.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)))) (h "0hjfy2l1kqnb3gh5lixbhsdl3hlszrh95bprgnha0ipm77hllylw")))

(define-public crate-yamd-0.13.1 (c (n "yamd") (v "0.13.1") (d (list (d (n "chrono") (r "^0.4.37") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)))) (h "1rgafqrbgbrzwda6zh4ahx489kgbl7s7lz5mm1b9gz574kh62l48")))

(define-public crate-yamd-0.13.2 (c (n "yamd") (v "0.13.2") (d (list (d (n "chrono") (r "^0.4.37") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)))) (h "1xz9qvda74x34nw9yjnfz48q5nv6linsr5ckmg2nwjs290pxfsrr")))

(define-public crate-yamd-0.13.3 (c (n "yamd") (v "0.13.3") (d (list (d (n "chrono") (r "^0.4.37") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)))) (h "19n6p6ms8bzpqpx9mzkdlhhyb39jjn66d07fa8dwa7yn6q71ijns")))

