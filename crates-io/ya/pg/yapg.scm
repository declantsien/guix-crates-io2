(define-module (crates-io ya pg yapg) #:use-module (crates-io))

(define-public crate-yapg-0.1.0 (c (n "yapg") (v "0.1.0") (d (list (d (n "cargo-make") (r "^0.32.14") (d #t) (k 2)) (d (n "cargo-semver") (r "^1.0.0-alpha.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1r6v1pqfsxhwgz86c7w4d3hm9g6yxamxbib2ghqyhdj5zv8ghmrv")))

