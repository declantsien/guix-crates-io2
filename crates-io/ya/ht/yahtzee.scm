(define-module (crates-io ya ht yahtzee) #:use-module (crates-io))

(define-public crate-yahtzee-0.1.0 (c (n "yahtzee") (v "0.1.0") (d (list (d (n "ncurses") (r "^5.101.0") (f (quote ("wide"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fgahqcycv4qmjxvjnln0n2jmlr9r7by9ywdzwzijs982pg553c9")))

(define-public crate-yahtzee-0.2.0 (c (n "yahtzee") (v "0.2.0") (d (list (d (n "ncurses") (r "^5.101.0") (f (quote ("wide"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mzzkqadkyl7bqck784csn6j19fwcrjrqip1b3r3wvpfwjwdmh74")))

(define-public crate-yahtzee-0.3.0 (c (n "yahtzee") (v "0.3.0") (d (list (d (n "ncurses") (r "^5.101.0") (f (quote ("wide"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q3k1yrrlv14r2fgdaaw5v64fnl3h3b5mg8c825zkws3ldwicfka")))

