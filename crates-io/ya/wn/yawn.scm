(define-module (crates-io ya wn yawn) #:use-module (crates-io))

(define-public crate-yawn-0.1.0 (c (n "yawn") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0m4mpknf38rs7r0vhgnp1sxj4gbx7v4nj08g53kv1466v8xg7rp2")))

(define-public crate-yawn-0.1.1 (c (n "yawn") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "1bah431i6wry1pqxf1rwkb213wbljlsac1dlhwfqyzag28a1blmh")))

(define-public crate-yawn-0.1.2 (c (n "yawn") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0x1hc00b1c6iwqqxy70l3xhr7m0ykq976c69rrvhpsyvwaz7cyi3")))

(define-public crate-yawn-0.1.3 (c (n "yawn") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13rkc4y8h96pzzjwsdx5k0d7czgmi7iml4mllf0dmhiyz5acmcms")))

