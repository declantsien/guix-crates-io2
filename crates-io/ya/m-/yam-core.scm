(define-module (crates-io ya m- yam-core) #:use-module (crates-io))

(define-public crate-yam-core-0.1.0 (c (n "yam-core") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "135ss4djscirj2qwmfm2igsg091fr37w20akpi0xib3hia5kabfy") (r "1.65")))

