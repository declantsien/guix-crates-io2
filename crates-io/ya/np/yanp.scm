(define-module (crates-io ya np yanp) #:use-module (crates-io))

(define-public crate-yanp-0.1.0 (c (n "yanp") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.0") (k 0)))) (h "1pqn44253l4j3vycxxd8c1iz04zkanhpkc2rq4jmm0fab32z5rd5") (f (quote (("default") ("alloc")))) (y #t)))

(define-public crate-yanp-0.1.1 (c (n "yanp") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.0") (k 0)))) (h "0hxsxppr2zvvlgmmm91j0flpqvx6ax9abc9sh1fwkbs13j4xz7jj") (f (quote (("default") ("alloc"))))))

