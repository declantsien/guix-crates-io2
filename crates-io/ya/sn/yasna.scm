(define-module (crates-io ya sn yasna) #:use-module (crates-io))

(define-public crate-yasna-0.1.0 (c (n "yasna") (v "0.1.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "00l8qf5kc1r0fkgdsqnkb9g48llghiybi1z9y8q64snzx9wzdjrw")))

(define-public crate-yasna-0.1.1 (c (n "yasna") (v "0.1.1") (d (list (d (n "num") (r "^0.1.32") (f (quote ("bigint"))) (o #t) (d #t) (k 0)))) (h "0qp1lysqr5wazf9qd77akxv196181lmgws9s2ah9c5plk2ybspxy") (f (quote (("default" "bigint") ("bigint" "num"))))))

(define-public crate-yasna-0.1.2 (c (n "yasna") (v "0.1.2") (d (list (d (n "num") (r "^0.1.32") (f (quote ("bigint"))) (o #t) (d #t) (k 0)))) (h "1nzgbqscjabap8izha8v5v7rba7xsax5s16s480664dgdh7xiksy") (f (quote (("default" "bigint") ("bigint" "num"))))))

(define-public crate-yasna-0.1.3 (c (n "yasna") (v "0.1.3") (d (list (d (n "bit-vec") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.32") (f (quote ("bigint"))) (o #t) (d #t) (k 0)))) (h "1frcd79rzi6dlly7lldjn2avnhfmj6yxrjsgvb2p1k2zbxdzyc9s") (f (quote (("default" "bigint" "bitvec") ("bitvec" "bit-vec") ("bigint" "num"))))))

(define-public crate-yasna-0.2.0 (c (n "yasna") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "0w2y2ci5s0jfcdd6bs72sh8bn0gs7dh5lc2z0jlbgaq0p6vh5idb") (f (quote (("default"))))))

(define-public crate-yasna-0.2.1 (c (n "yasna") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "0ss9x8qg1jdp5aki17x0w7qqpkb3p65fz7a9ckyg9sby0lfgv1w6") (f (quote (("default"))))))

(define-public crate-yasna-0.2.2 (c (n "yasna") (v "0.2.2") (d (list (d (n "bit-vec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "151n5xpqwk56cy5l70gc03pav341p3pgh222snglqj5hws4k3bvr") (f (quote (("default"))))))

(define-public crate-yasna-0.3.0 (c (n "yasna") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "0d4p1v54zaqn26i451iw1yp8x37v17y2dc3yqyif0098bwsxzj4v") (f (quote (("default"))))))

(define-public crate-yasna-0.3.1 (c (n "yasna") (v "0.3.1") (d (list (d (n "bit-vec") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "1na5cqvvgcd5fwzhsp2dxsy71bcmyi04zm2pwfcdgql7ml7d2qx5") (f (quote (("default"))))))

(define-public crate-yasna-0.3.2 (c (n "yasna") (v "0.3.2") (d (list (d (n "bit-vec") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (o #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "1nsdd1di06yvh6n2mv54wgvkl5fz155lbn7nhmna1wmlfbwvzrqd") (f (quote (("default"))))))

(define-public crate-yasna-0.4.0 (c (n "yasna") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (o #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "0xgsvxqnycdakh6j3hg4dk3mylrpnba50w0d36vg5k311sfs4qp2") (f (quote (("std") ("default"))))))

(define-public crate-yasna-0.5.0 (c (n "yasna") (v "0.5.0") (d (list (d (n "bit-vec") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)) (d (n "time") (r "^0.3") (f (quote ("std"))) (o #t) (k 0)))) (h "0k1gk11hq4rwlppv9f50bz8bnmgr73r66idpp7rybly96si38v9l") (f (quote (("std") ("default"))))))

(define-public crate-yasna-0.5.1 (c (n "yasna") (v "0.5.1") (d (list (d (n "bit-vec") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)) (d (n "time") (r "^0.3.1") (f (quote ("std"))) (o #t) (k 0)))) (h "1d0h4xqjkvdjj58ny8nlkz2893i5cnh0mhh51i6sni1p5sjyglmf") (f (quote (("std") ("default"))))))

(define-public crate-yasna-0.5.2 (c (n "yasna") (v "0.5.2") (d (list (d (n "bit-vec") (r "^0.6.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.1") (f (quote ("std"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 2)))) (h "1ka4ixrplnrfqyl1kymdj8cwpdp2k0kdr73b57hilcn1kiab6yz1") (f (quote (("std") ("default"))))))

