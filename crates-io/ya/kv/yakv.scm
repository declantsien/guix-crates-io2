(define-module (crates-io ya kv yakv) #:use-module (crates-io))

(define-public crate-yakv-0.1.0 (c (n "yakv") (v "0.1.0") (d (list (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1759vpwgh45b0i3ckfkzdxjdyrylq8adg0k8zsxay5221spbfzni")))

(define-public crate-yakv-0.1.1 (c (n "yakv") (v "0.1.1") (d (list (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0b04lq5gxcgh2z8jjs7g74dkfs622jfbnzy4xknz2f4j4r8327r5")))

(define-public crate-yakv-0.1.2 (c (n "yakv") (v "0.1.2") (d (list (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1pqwwzldssaddsv6bsy3dp7kab0jmmm9xkzbsyw7sps3aywcmmmx")))

(define-public crate-yakv-0.1.3 (c (n "yakv") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "02q0lcphbxgk6m8x482yi6rprhbd2bxs0hj65issrjbvvmibiljl")))

(define-public crate-yakv-0.1.4 (c (n "yakv") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0rqadi667ga798g4l6k3g5mmxj2gaaaxkiww8vm1xpsy9wkffsma")))

(define-public crate-yakv-0.1.5 (c (n "yakv") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0ggi1jl49i9zx3fh82z5r71kqjfz8np905w0jpdhpzd26yy3jw48")))

(define-public crate-yakv-0.1.6 (c (n "yakv") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1hd0c2fg1n3diarjqx2y3cr5nzl2csbv1ycac8iy7fxaw1wdlzn1")))

(define-public crate-yakv-0.1.7 (c (n "yakv") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0cx21l4cd3ixrfpmxxm7gmxphl7j8m2lq4jvvvbfd8li12qvbg8k")))

(define-public crate-yakv-0.1.8 (c (n "yakv") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "12z3s5z6q7f38n6hysmxdwc921lv1w0x6751m8d00kf9wf7z9x76")))

(define-public crate-yakv-0.1.9 (c (n "yakv") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0m8n37cpw0yd3177h7rrk455kpcri71f7q0fmsnbglq4m2md9q6r")))

(define-public crate-yakv-0.2.0 (c (n "yakv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "crc32c") (r "^0.6.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1c1dqzfyb3817jd8sybg1mlgxdnhj5bysvsvrpzlw43h643m1q9g")))

(define-public crate-yakv-0.2.1 (c (n "yakv") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1n91m9ciadk59abkrp2vinak8z09gz2r4d0i1fhj0gqrcxyfrqca")))

(define-public crate-yakv-0.2.2 (c (n "yakv") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1bh1gmbx03yqshmq25b87d88403v8v2dny66inz8dd9ailyw4ywj")))

(define-public crate-yakv-0.2.3 (c (n "yakv") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "19jc421lwvjfsjk07dwh1wdnr8i2nv9bxgxlwk330zyinr1gh88f")))

(define-public crate-yakv-0.2.4 (c (n "yakv") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "08cavl6m96sj881j3fh5d071jzi0h02cr21zi9cppa6vxybh04i7")))

(define-public crate-yakv-0.2.5 (c (n "yakv") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0lqyl683l1iyy7gjma07ywcyyi15p8689qr75gcgr6809h1fj275")))

(define-public crate-yakv-0.2.6 (c (n "yakv") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "13qx2fbqf9vkrk487pd7yqj8sr7i8abslr42wih9yidrd2345fis")))

(define-public crate-yakv-0.2.7 (c (n "yakv") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0d3swj6s09lqqy9hk8vm142idixa8wm6ka81v567gnhxnfms3sqp")))

