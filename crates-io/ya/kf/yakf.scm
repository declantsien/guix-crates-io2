(define-module (crates-io ya kf yakf) #:use-module (crates-io))

(define-public crate-yakf-0.1.3 (c (n "yakf") (v "0.1.3") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0db4wmqh87zdbhp0kyapdqlnwmq5gdd3ip8dn83b6nvf9fwslidk")))

(define-public crate-yakf-0.1.4 (c (n "yakf") (v "0.1.4") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0z4v77wsdljwy3xq9c45bmlvcbrbg1ynq15f54h535raba2fw1wy")))

(define-public crate-yakf-0.1.5 (c (n "yakf") (v "0.1.5") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1pldjvcd235s8abv48j89v83myy4jfy8syd2j9bs2rpwfjjnrg4s")))

(define-public crate-yakf-0.1.6 (c (n "yakf") (v "0.1.6") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "14bvwkq4iyfj633brp46jfzn4kn43a4l8nw9mzk40w91vcdvq530")))

(define-public crate-yakf-0.1.7 (c (n "yakf") (v "0.1.7") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("default"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1pl6vj2iqbnh5ifc5a3qpgc7nljvnv7j96sd7mbdjfw9y62x7zzx")))

(define-public crate-yakf-0.1.8 (c (n "yakf") (v "0.1.8") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (f (quote ("default"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "164mdzkvhd55zdsvac6xaf4yxxd0gp0ip9k24vgmzk3cryx1zv3f")))

(define-public crate-yakf-0.1.9 (c (n "yakf") (v "0.1.9") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0i31mg6a7z0gwq0afikyfzmdmzvcvbdwjxdc9w5dnwc71m4pwczh")))

(define-public crate-yakf-0.1.10 (c (n "yakf") (v "0.1.10") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vagavlf1idh5da6af4kyfkmr6bqsx3i57i1ny7pi6sa677s5jyp")))

(define-public crate-yakf-0.1.11 (c (n "yakf") (v "0.1.11") (d (list (d (n "hifitime") (r "^3.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11rp070kxsvkr8cgpr7cajs1vq818827lsxsgv6yvlj86v6a04pq")))

(define-public crate-yakf-0.1.12 (c (n "yakf") (v "0.1.12") (d (list (d (n "hifitime") (r "^3.2.0") (k 0)) (d (n "itertools") (r "^0.10.3") (k 0)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (f (quote ("libm-force" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1naxcw4rc4xnmmjs5nx1zwcjcqnyjg5bz68jy6xc8vmyijpn3xzq")))

