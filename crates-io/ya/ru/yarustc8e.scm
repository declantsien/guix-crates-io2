(define-module (crates-io ya ru yarustc8e) #:use-module (crates-io))

(define-public crate-yarustc8e-0.1.0 (c (n "yarustc8e") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1l0ygkg37x4l29hdqylglr2444bz2h6qfqc9njz96xc5s9kqmwai") (y #t)))

(define-public crate-yarustc8e-0.1.1 (c (n "yarustc8e") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "12n925sa8dgf0ahxldq6jv0drli8p8pdav2kjx3y7z6gz2swn97n") (y #t)))

(define-public crate-yarustc8e-0.1.2 (c (n "yarustc8e") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0yrjfrf687gbsyv7n6sn5s4fp0bwri5737lascl66fmf7hy7wq4i") (y #t)))

(define-public crate-yarustc8e-0.1.3 (c (n "yarustc8e") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0hrclg9spmp4bpw0bwmpq8yqfmq86pcrrwhdmaz1x75pri1v2dhm") (y #t)))

(define-public crate-yarustc8e-0.1.4 (c (n "yarustc8e") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "19z4mcbn6n1p3miqb06a1swxvn2fb3dwn8qj2sv4mlimdjavnx90") (y #t)))

