(define-module (crates-io ya ru yarustc8e-cli) #:use-module (crates-io))

(define-public crate-yarustc8e-cli-0.1.1 (c (n "yarustc8e-cli") (v "0.1.1") (d (list (d (n "booldisplay") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "yarustc8e") (r "^0.1.2") (d #t) (k 0)))) (h "08l1il8fjq39ld3pchpp4fcb2jkjvwc9g8p45vsg4az4zmdhmi9n") (y #t)))

(define-public crate-yarustc8e-cli-0.1.3 (c (n "yarustc8e-cli") (v "0.1.3") (d (list (d (n "booldisplay") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "yarustc8e") (r "^0.1.3") (d #t) (k 0)))) (h "0vsa6g82righj1cckxzl0d0l0slp78w1cpvbx7f8l9bjv2wzqw3j") (y #t)))

(define-public crate-yarustc8e-cli-0.1.4 (c (n "yarustc8e-cli") (v "0.1.4") (d (list (d (n "booldisplay") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "yarustc8e") (r "^0.1.3") (d #t) (k 0)))) (h "17ryjaci8l17dnla8ds3ikv7r6yisiqncijvmrs8rvg1076i03zk") (y #t)))

