(define-module (crates-io ya re yare-macro) #:use-module (crates-io))

(define-public crate-yare-macro-1.0.0 (c (n "yare-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0asn86yhg37qdzshazwj3l8szyx8xzh9b8rhqjqa0ki47ir1j2ii")))

(define-public crate-yare-macro-1.0.1 (c (n "yare-macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0csnc3j4g9kby96kn94skpf69hqawi3xxp8am0znd2v6lq5aqrwl")))

(define-public crate-yare-macro-2.0.0 (c (n "yare-macro") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "178qshdmaxc47dyvpbvdvgqm5in903hgzms8j5rr9wdzfwvmgrvl") (r "1.56")))

(define-public crate-yare-macro-3.0.0 (c (n "yare-macro") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zlf01xmj45zgzg2m0y6xljh16w13gmn50nny2flk0dxy0k4d3dc") (r "1.56")))

