(define-module (crates-io ya re yare) #:use-module (crates-io))

(define-public crate-yare-1.0.0 (c (n "yare") (v "1.0.0") (d (list (d (n "yare-macro") (r "^1.0.0") (d #t) (k 0)))) (h "1hx7h6siwvc6akc0hi0fvz06gpx7src3anbdxrxdv88b6jqm41rc")))

(define-public crate-yare-1.0.1 (c (n "yare") (v "1.0.1") (d (list (d (n "yare-macro") (r "^1.0.0") (d #t) (k 0)))) (h "1dwayas3nl37dgrn31yfp16a5pfxylyswzqkiqh5vgqws53ngbf4")))

(define-public crate-yare-1.0.2 (c (n "yare") (v "1.0.2") (d (list (d (n "yare-macro") (r "^1.0.0") (d #t) (k 0)))) (h "0iwccyjg8b968rvx1b41zx003c0mlqay1glfx2rhl5fj5i0ry4fz")))

(define-public crate-yare-2.0.0 (c (n "yare") (v "2.0.0") (d (list (d (n "yare-macro") (r "^2.0.0") (d #t) (k 0)))) (h "1as56bp4fyhn2i4mqn2b69dkl6bfyi23an23v1ny5kd6wrvyhjyq") (r "1.56")))

(define-public crate-yare-3.0.0 (c (n "yare") (v "3.0.0") (d (list (d (n "yare-macro") (r "^3.0.0") (d #t) (k 0)))) (h "0z2wc7pkbpbgxbwhqpcal2cmwm9d8p7ysp8pfmyycc8llhgbcf6d") (r "1.56")))

