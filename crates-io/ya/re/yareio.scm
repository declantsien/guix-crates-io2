(define-module (crates-io ya re yareio) #:use-module (crates-io))

(define-public crate-yareio-0.1.0 (c (n "yareio") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0p13s3qi80hvxs65pqkxxwmkn3v5gk84yk0cfr9h0sxrlc0vz4i5") (f (quote (("headless" "rand"))))))

(define-public crate-yareio-0.1.1 (c (n "yareio") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0ryk6sgby6mmjmh0713b8bnlkgyrddb3k8inmc14669wx0ld0hdy") (f (quote (("headless" "rand"))))))

(define-public crate-yareio-0.1.2 (c (n "yareio") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rfp4dxsdc5y9kp05mj2ld1ijx3ibzrds4f8f8y4j2y6iz6djw64") (f (quote (("headless" "rand" "serde" "serde_json"))))))

(define-public crate-yareio-0.1.3 (c (n "yareio") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lyjd9fw9jx1gxa2bnfswj5bx26hyyifqipxphdl8l42lx8ym5xk") (f (quote (("headless" "rand" "serde" "serde_json"))))))

(define-public crate-yareio-0.1.4 (c (n "yareio") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0310im0lymf4mfgnki6yzb8jndphs989vqqrhrhncg0bza67gk7b") (f (quote (("headless" "rand" "serde" "serde_json"))))))

