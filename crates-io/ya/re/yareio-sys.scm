(define-module (crates-io ya re yareio-sys) #:use-module (crates-io))

(define-public crate-yareio-sys-0.9.0 (c (n "yareio-sys") (v "0.9.0") (d (list (d (n "js-sys") (r "^0.3.52") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.75") (d #t) (k 0)))) (h "1iym2ywwmbz0pnqssicbbs7mq0wmgds90snlicynqm24afc5zfnx") (f (quote (("triangles") ("squares") ("circles") ("RenderService")))) (y #t)))

(define-public crate-yareio-sys-0.9.1 (c (n "yareio-sys") (v "0.9.1") (d (list (d (n "js-sys") (r "^0.3.52") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.75") (d #t) (k 0)))) (h "1bw35yy5hc5hbchszyimpkrh27d3icm81qr9mhlgd0vk1g23ywsh") (f (quote (("triangles") ("squares") ("circles") ("RenderService")))) (y #t)))

(define-public crate-yareio-sys-0.9.2 (c (n "yareio-sys") (v "0.9.2") (d (list (d (n "js-sys") (r "^0.3.52") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.75") (d #t) (k 0)))) (h "1y2cnf444q2mk0hjr72bklj0afh9y9ix9nazfr456vlq7pi4rm3x") (f (quote (("triangles") ("squares") ("circles") ("RenderService")))) (y #t)))

(define-public crate-yareio-sys-0.9.3 (c (n "yareio-sys") (v "0.9.3") (d (list (d (n "js-sys") (r "^0.3.52") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.75") (d #t) (k 0)))) (h "1qcsy9hwpx2wlvn6qz09sh8h1nvpm01jg2k82cz8p00spvzhqc7w") (f (quote (("triangles") ("squares") ("circles") ("RenderService"))))))

(define-public crate-yareio-sys-0.9.4 (c (n "yareio-sys") (v "0.9.4") (d (list (d (n "js-sys") (r "^0.3.52") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.75") (d #t) (k 0)))) (h "0sfy7s7fwh9i5m0n8xwpnhbj8fygqn0x61paikby30684nxxrxnc") (f (quote (("triangles") ("squares") ("circles") ("RenderService"))))))

(define-public crate-yareio-sys-0.9.5 (c (n "yareio-sys") (v "0.9.5") (d (list (d (n "js-sys") (r "^0.3.52") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.75") (d #t) (k 0)))) (h "10d11i8ppiq83whx2dqjfvshqmxlkvxrb36lxg6nzqc5jkfhcj62") (f (quote (("triangles") ("squares") ("circles") ("RenderService"))))))

