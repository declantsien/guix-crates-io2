(define-module (crates-io ya ap yaap) #:use-module (crates-io))

(define-public crate-yaap-0.0.1 (c (n "yaap") (v "0.0.1") (h "0fdd4xwa8nhaxgi3s6yr6wsj20m3y7ldca5c88pza6l8ah2hgvl7")))

(define-public crate-yaap-0.0.2-dev01 (c (n "yaap") (v "0.0.2-dev01") (h "0mkjx6g6lldpp9bnramhahabmgjy10jq8vaiqldanlw7as8n1qdn")))

(define-public crate-yaap-0.0.2 (c (n "yaap") (v "0.0.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 2)))) (h "12v739ksf72iadwbjqgqvhr54km32bfpg0crd4a64w8yyxgga85x") (f (quote (("default" "alloc") ("alloc"))))))

