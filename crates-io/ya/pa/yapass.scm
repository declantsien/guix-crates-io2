(define-module (crates-io ya pa yapass) #:use-module (crates-io))

(define-public crate-yapass-0.1.0 (c (n "yapass") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14mm7ralpb2kqby74w23sssxmbxhamb0r2imvgxhvr1r55c9q52m")))

