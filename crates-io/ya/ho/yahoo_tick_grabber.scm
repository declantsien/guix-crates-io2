(define-module (crates-io ya ho yahoo_tick_grabber) #:use-module (crates-io))

(define-public crate-yahoo_tick_grabber-0.1.0 (c (n "yahoo_tick_grabber") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a8kmh1097i7kvzdnm9fy5k5ady9ykr01dk5amkz85yq5x0jjmy8")))

(define-public crate-yahoo_tick_grabber-0.1.1 (c (n "yahoo_tick_grabber") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y1ka6vdqnkgmzycl0yxp6szvd45lx4acl4asa499h2vx62744vn") (y #t)))

(define-public crate-yahoo_tick_grabber-0.1.2 (c (n "yahoo_tick_grabber") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kw93clp1rp1kzsklici9ml7z4b73y60radlcgaf9wzf9vf5630h")))

(define-public crate-yahoo_tick_grabber-0.1.3 (c (n "yahoo_tick_grabber") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "039i902v97x0prrx1zd9z8g0mc7fmjc52pgb1ybypy9rah4vgmil")))

(define-public crate-yahoo_tick_grabber-0.1.4 (c (n "yahoo_tick_grabber") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02dm7pisgv65q3wqxqagdki9qjp82pjfzbmyvbc32ijav3q7yxqr")))

