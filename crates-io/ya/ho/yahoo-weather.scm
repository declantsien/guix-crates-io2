(define-module (crates-io ya ho yahoo-weather) #:use-module (crates-io))

(define-public crate-yahoo-weather-0.2.0 (c (n "yahoo-weather") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.9") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xs81ncadf544532w2mj62dzl0zzdynchjkyl3vpvzpwc5w4mahj")))

