(define-module (crates-io ya te yate) #:use-module (crates-io))

(define-public crate-yate-0.0.1 (c (n "yate") (v "0.0.1") (d (list (d (n "html-to-string-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1iqp37vchbd1bjb6n034xbj3sr645v8phyff3y6ps3palsbdj91x") (y #t)))

(define-public crate-yate-0.1.1 (c (n "yate") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "html-to-string-macro") (r "^0.1") (d #t) (k 0)))) (h "08ci05aky63nifpka27clagfxbnarabh9qm40qw3z7k62n9z2939") (y #t)))

(define-public crate-yate-0.1.2 (c (n "yate") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "html-to-string-macro") (r "^0.1") (d #t) (k 0)))) (h "10da8nrawwm99vjl8mp9c2hbgaps9djbqcgrdfg17bjd9iw9567x") (y #t)))

(define-public crate-yate-0.1.3 (c (n "yate") (v "0.1.3") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "html-to-string-macro") (r "^0.1") (d #t) (k 0)))) (h "1hg7mlc9s5pvrk60l564jci75klf4bi1d5i2djihkvi8hq3fgmb6") (y #t)))

