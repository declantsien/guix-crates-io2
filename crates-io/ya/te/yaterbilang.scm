(define-module (crates-io ya te yaterbilang) #:use-module (crates-io))

(define-public crate-yaterbilang-0.1.0 (c (n "yaterbilang") (v "0.1.0") (h "0zp6yx1ppqsriqza23082yx3nlinnd3ms6x74ayrg35i96h661gl")))

(define-public crate-yaterbilang-0.1.1 (c (n "yaterbilang") (v "0.1.1") (h "0vvi7cf605hm08sywrlsjljd1x7xaxwdk7082rldgz08xkdidpay")))

(define-public crate-yaterbilang-0.1.2 (c (n "yaterbilang") (v "0.1.2") (h "0c1sk474zm4k6hj1gg0h74x95ny2vh0icpfy17sbr0zj20gjgqm8")))

(define-public crate-yaterbilang-0.1.3 (c (n "yaterbilang") (v "0.1.3") (h "14m0ndaql3c9i0dk2mxyxj5d8g778l8k82ciby96wa23ygmb331g")))

(define-public crate-yaterbilang-0.1.4 (c (n "yaterbilang") (v "0.1.4") (h "0jn6m427pd1bx686cg46f4zdyg47shjgsxc66whdx8vhm9sc3b7p")))

(define-public crate-yaterbilang-0.1.5 (c (n "yaterbilang") (v "0.1.5") (h "0bcdxc3vkzrsgrlkavyg72xd4zf1zz337g6pgvb96kb22kciamky")))

(define-public crate-yaterbilang-0.1.6 (c (n "yaterbilang") (v "0.1.6") (h "0kxd020kqzpzxs1a26jjjy7s7kvnxb6rv6q2p69x7d7f1c5qihrs")))

