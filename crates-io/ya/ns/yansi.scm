(define-module (crates-io ya ns yansi) #:use-module (crates-io))

(define-public crate-yansi-0.1.0 (c (n "yansi") (v "0.1.0") (h "1qsa5pcvlk4snv7a1kivkww2pa7xlm5rq0gmbmg02kplkphkdj9z") (f (quote (("nightly"))))))

(define-public crate-yansi-0.1.1 (c (n "yansi") (v "0.1.1") (h "112vhabxy24f3gr6k3gqx4zz5r7aq7n6i1ks38v1j2kxnkx363xr") (f (quote (("nightly"))))))

(define-public crate-yansi-0.2.0 (c (n "yansi") (v "0.2.0") (h "1n0ii1vds80lply7670k25kr3zllvy3qrmkq4hj4011zf2hcawha") (f (quote (("nightly"))))))

(define-public crate-yansi-0.3.0 (c (n "yansi") (v "0.3.0") (h "00n9s67hvbgcqgbzk30pq3dp57395glqrn1n527rid0kgidz2cdp") (f (quote (("nightly"))))))

(define-public crate-yansi-0.3.1 (c (n "yansi") (v "0.3.1") (h "1pnmgsj3lkyc86rsqka5yi38dz54wiwnqqdbmkxysa2d6xbglbfx") (f (quote (("nightly"))))))

(define-public crate-yansi-0.3.2 (c (n "yansi") (v "0.3.2") (h "02sw7ydxwa33swyqr0lrnhqlpyb8npixhb5qjhwfd802q0gj2wb8") (f (quote (("nightly"))))))

(define-public crate-yansi-0.3.3 (c (n "yansi") (v "0.3.3") (h "0fjpc038g2j7ffbk0aj95pcqpbl4l8b0cl6pr172vflcm0gm4042") (f (quote (("nightly"))))))

(define-public crate-yansi-0.3.4 (c (n "yansi") (v "0.3.4") (h "0d7r70nzh342wb8fpikia7gbkcw8pbfixvf8jfk4bw99lvpf80x5") (f (quote (("nightly"))))))

(define-public crate-yansi-0.4.0 (c (n "yansi") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.5") (f (quote ("nightly"))) (d #t) (k 2)))) (h "0j6yjgzglk8fjgriljk43kxf21bl11dlpf5k0vxl5v6dr543n36n")))

(define-public crate-yansi-0.5.0 (c (n "yansi") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.5") (f (quote ("nightly"))) (d #t) (k 2)))) (h "0wdx8syhc61lphmgw5cw1vq73isi4szjqriz1k07z19r3r59ziwz")))

(define-public crate-yansi-0.5.1 (c (n "yansi") (v "0.5.1") (d (list (d (n "serial_test") (r "^0.6") (d #t) (k 2)))) (h "1v4qljgzh73knr7291cgwrf56zrvhmpn837n5n5pypzq1kciq109")))

(define-public crate-yansi-1.0.0-beta (c (n "yansi") (v "1.0.0-beta") (d (list (d (n "is-terminal") (r "^0.4.7") (o #t) (d #t) (k 0)))) (h "10pgcwn1azz059n88axjrxaqhds6c35wh7ngcpgpr59445yzz67m") (f (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (r "1.61")))

(define-public crate-yansi-1.0.0-gamma (c (n "yansi") (v "1.0.0-gamma") (d (list (d (n "is-terminal") (r "^0.4.7") (o #t) (d #t) (k 0)))) (h "16k5w0vlbfs0jiqjmxcxcqc6i53k69acqapdpgakyzsm5z25zb3g") (f (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (r "1.61")))

(define-public crate-yansi-1.0.0-rc (c (n "yansi") (v "1.0.0-rc") (d (list (d (n "is-terminal") (r "^0.4.7") (o #t) (d #t) (k 0)))) (h "0gp4lxs1h49drdxraj4yja6jg68bn2rql0ja1v23ppai72nldrwy") (f (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (r "1.63")))

(define-public crate-yansi-1.0.0-rc.1 (c (n "yansi") (v "1.0.0-rc.1") (d (list (d (n "is-terminal") (r "^0.4.7") (o #t) (d #t) (k 0)))) (h "0xr3n41j5v00scfkac2d6vhkxiq9nz3l5j6vw8f3g3bqixdjjrqk") (f (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (r "1.63")))

(define-public crate-yansi-1.0.0 (c (n "yansi") (v "1.0.0") (d (list (d (n "is-terminal") (r "^0.4.11") (o #t) (d #t) (k 0)))) (h "1lx0vs970w46d3x02g6k5bbjzcgp2zjb3f88az4qzv2qdzbn2a3c") (f (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (y #t) (r "1.63")))

(define-public crate-yansi-1.0.1 (c (n "yansi") (v "1.0.1") (d (list (d (n "is-terminal") (r "^0.4.11") (o #t) (d #t) (k 0)))) (h "0jdh55jyv0dpd38ij4qh60zglbw9aa8wafqai6m0wa7xaxk3mrfg") (f (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (r "1.63")))

