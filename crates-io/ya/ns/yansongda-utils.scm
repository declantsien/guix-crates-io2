(define-module (crates-io ya ns yansongda-utils) #:use-module (crates-io))

(define-public crate-yansongda-utils-0.9.0 (c (n "yansongda-utils") (v "0.9.0") (d (list (d (n "once_cell") (r "~1.17.0") (d #t) (k 0)) (d (n "regex") (r "~1.7.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ngy0ai37n9dql6zq4cgmx7j64kqqrx3v8gpb078p45xiplq617c")))

(define-public crate-yansongda-utils-1.0.0 (c (n "yansongda-utils") (v "1.0.0") (d (list (d (n "once_cell") (r "~1.17.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "~1.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vikp9gjndnprx83x7vy0ig11cywxdahv79alrwr2wvzcgql0d4q") (f (quote (("phone" "macros" "serde") ("macros" "regex" "once_cell"))))))

(define-public crate-yansongda-utils-1.0.1 (c (n "yansongda-utils") (v "1.0.1") (d (list (d (n "once_cell") (r "~1.17.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "~1.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0.92") (d #t) (k 2)))) (h "0afjmyy886mp75ma4h9mdzy00mp2xbzd9glsaq1vh03nvi2d4y0d") (f (quote (("phone" "macros" "serde") ("macros" "regex" "once_cell"))))))

(define-public crate-yansongda-utils-1.0.2 (c (n "yansongda-utils") (v "1.0.2") (d (list (d (n "regex") (r "~1.9.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "~1.9.0") (d #t) (k 2)) (d (n "serde") (r "~1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0.92") (d #t) (k 2)))) (h "11wjvzi85hfwr31lf7m4gc4cxdby96ap61ca8x6hp8jsj3f4wyy0") (f (quote (("phone" "macros" "serde") ("macros" "regex"))))))

