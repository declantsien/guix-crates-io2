(define-module (crates-io ya rn yarner-lib) #:use-module (crates-io))

(define-public crate-yarner-lib-0.5.1 (c (n "yarner-lib") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0qlc90aya2rfvgqy6xj1m3hqzjc06jsvnxzc7xy3ndfa3dh9h2ic")))

(define-public crate-yarner-lib-0.5.2 (c (n "yarner-lib") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0ljxjrw57x9flw9lj75vfwmfjbpd0zmnxak6q4q7p0l993hqmn2b")))

(define-public crate-yarner-lib-0.6.0 (c (n "yarner-lib") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0d5dga9l38zicbm1nyc1jzd44s4m416ck5s0rrl9la8v8167mvdr")))

(define-public crate-yarner-lib-0.6.1 (c (n "yarner-lib") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1d5jmhmjdz521m74a3s4rbk66b2n12a6ailgf24bbra8ghnsb5rh")))

