(define-module (crates-io ya rn yarns) #:use-module (crates-io))

(define-public crate-yarns-0.1.0 (c (n "yarns") (v "0.1.0") (h "089r5a3xjjjmazkcfck2549yv3asjsd10fi0yvrpwpfl2l10ibb0")))

(define-public crate-yarns-0.2.0 (c (n "yarns") (v "0.2.0") (h "07p2c6d9sck8rhbzdysdlld5z1x1jj09jpvhpyjg9qazz1j9i6qk")))

(define-public crate-yarns-0.3.0 (c (n "yarns") (v "0.3.0") (h "1vnyfsnsya97z8nf716axyqpdijvf3d7jp2pc2idjn676pk2f4zl")))

(define-public crate-yarns-0.3.1 (c (n "yarns") (v "0.3.1") (h "0x4q67zpzvpfqsn0ihwgf8xs34yjwkpw2wvmn9nwgrgrpl8raa9z")))

(define-public crate-yarns-0.4.0 (c (n "yarns") (v "0.4.0") (h "0734c09hs0qzw4sq5f1i9d0z6z0lavh6khm9ya0lrxrn8pv8i6cj")))

(define-public crate-yarns-0.4.1 (c (n "yarns") (v "0.4.1") (h "0fv2jphryha2yzgw4bk4gm9x2n6s7ni1lfdx1hzs7k08syxd6xs0")))

