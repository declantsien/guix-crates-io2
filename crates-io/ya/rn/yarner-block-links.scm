(define-module (crates-io ya rn yarner-block-links) #:use-module (crates-io))

(define-public crate-yarner-block-links-0.1.1 (c (n "yarner-block-links") (v "0.1.1") (d (list (d (n "handlebars") (r "^3.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yarner-lib") (r "^0.5.2") (d #t) (k 0)))) (h "01c43awbwhr598vrsk0w3pdgfysyv181bn29c78s88lmk8w18r9v")))

(define-public crate-yarner-block-links-0.1.2 (c (n "yarner-block-links") (v "0.1.2") (d (list (d (n "handlebars") (r "^3.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yarner-lib") (r "^0.5.2") (d #t) (k 0)))) (h "0pw26sv7zzwdghc3bzk51vnamd9kc8ghh5v5whhg7adn9vbil9ah")))

