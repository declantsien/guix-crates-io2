(define-module (crates-io ya rn yarn_remapper) #:use-module (crates-io))

(define-public crate-yarn_remapper-0.1.0 (c (n "yarn_remapper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive-getters") (r "^0.3.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)))) (h "0s6dsgzb66wqf7mbxl45fa52cnfi5szv4nkfkhcivl18wbx251sb")))

