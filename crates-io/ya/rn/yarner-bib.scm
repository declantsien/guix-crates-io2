(define-module (crates-io ya rn yarner-bib) #:use-module (crates-io))

(define-public crate-yarner-bib-0.1.1 (c (n "yarner-bib") (v "0.1.1") (d (list (d (n "biblatex") (r "^0.4.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "path-clean") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "yarner-lib") (r "^0.5.1") (d #t) (k 0)))) (h "0m4cc7zy11k5546g801cwcggh20zba7wf1jqfm9f87lmb2fy8a9w")))

(define-public crate-yarner-bib-0.1.2 (c (n "yarner-bib") (v "0.1.2") (d (list (d (n "biblatex") (r "^0.4.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "path-clean") (r "^0.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "yarner-lib") (r "^0.5.2") (d #t) (k 0)))) (h "1yc4qjmn2aa5gy3qyf9x22lpwhw2fsys0gk2diban5rb4h7bq6vz")))

