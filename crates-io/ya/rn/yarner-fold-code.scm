(define-module (crates-io ya rn yarner-fold-code) #:use-module (crates-io))

(define-public crate-yarner-fold-code-0.2.1 (c (n "yarner-fold-code") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "yarner-lib") (r "^0.5.2") (d #t) (k 0)))) (h "0p5ayxx5zs3s4zhzzg4ipgc0dc25p4xs5vkfxpxh4l9bvn3hmwhf")))

(define-public crate-yarner-fold-code-0.2.2 (c (n "yarner-fold-code") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "yarner-lib") (r "^0.5.2") (d #t) (k 0)))) (h "1c98hsalk6g90lk95syjdfxdb6cfli21h2695b3sgvrqs9vpsgml")))

