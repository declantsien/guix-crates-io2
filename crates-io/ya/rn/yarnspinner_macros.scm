(define-module (crates-io ya rn yarnspinner_macros) #:use-module (crates-io))

(define-public crate-yarnspinner_macros-0.1.0 (c (n "yarnspinner_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0c3bi76nfhcqcxa0g4hb7bci29j970a3lnackbz59rcppnc8rbn8")))

