(define-module (crates-io ya rn yarnspinner_codegen) #:use-module (crates-io))

(define-public crate-yarnspinner_codegen-0.1.0 (c (n "yarnspinner_codegen") (v "0.1.0") (d (list (d (n "prost") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (o #t) (d #t) (k 0)))) (h "19x5qxyhi6swlyk3pm5k61cl407y5008rbvhygqyin2r27l4jmja") (f (quote (("default")))) (s 2) (e (quote (("proto" "dep:prost" "dep:prost-build"))))))

