(define-module (crates-io ya rn yarn-lock-parser) #:use-module (crates-io))

(define-public crate-yarn-lock-parser-0.1.0 (c (n "yarn-lock-parser") (v "0.1.0") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1s8k3gpxgw5c6hvsn3jzwqd54rssga2avkycc7wd5sv7h309wn2h")))

(define-public crate-yarn-lock-parser-0.1.1 (c (n "yarn-lock-parser") (v "0.1.1") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1m088z6x6ljhvnik8y5d5nv1wz5hilzc9906hk31y044pgmgnh80")))

(define-public crate-yarn-lock-parser-0.2.0 (c (n "yarn-lock-parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "114psbagi3fsviccpa3khydkaxiwnrfcl543kb6qa9961mm08zm0")))

(define-public crate-yarn-lock-parser-0.3.0 (c (n "yarn-lock-parser") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0g1nckvxk9q2qkvqv3l3njjfhh36qxzfpf1swnibyyprczzckzxb")))

(define-public crate-yarn-lock-parser-0.3.1 (c (n "yarn-lock-parser") (v "0.3.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "060rd8x8r22bs8y0zhyqy6f9xs599bwkf3gqgv80nh9v4pyjf68j")))

(define-public crate-yarn-lock-parser-0.3.2 (c (n "yarn-lock-parser") (v "0.3.2") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1551axx42pma32ybdzqg7bkwcdxnf0gxw560nwg1mca8xi2pc250")))

(define-public crate-yarn-lock-parser-0.4.0 (c (n "yarn-lock-parser") (v "0.4.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "08zdn828qf26i5b5lmlmj9gj161nspnwf1gzk6s1a2j9g6c868yp")))

(define-public crate-yarn-lock-parser-0.4.1 (c (n "yarn-lock-parser") (v "0.4.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "00ad07y5cncd0afdxlhcrga59lj63npqhhs9q5fvhqmb2vn5wlqp")))

(define-public crate-yarn-lock-parser-0.5.0 (c (n "yarn-lock-parser") (v "0.5.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hxjfxqn41733m2g8z4zvwpm2ccl0zz396yv3vmr75ckarjnqdcw")))

(define-public crate-yarn-lock-parser-0.6.0 (c (n "yarn-lock-parser") (v "0.6.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0sssbn8xacls346c1hw92yx945d086mmf92d4z0bmvvmyakcgd7m")))

(define-public crate-yarn-lock-parser-0.7.0 (c (n "yarn-lock-parser") (v "0.7.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "07z5pf0k64l4sb9sphqk4lwkbr5hfnfgij9ql2ia30l49y8061kx")))

