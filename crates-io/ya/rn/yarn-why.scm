(define-module (crates-io ya rn yarn-why) #:use-module (crates-io))

(define-public crate-yarn-why-1.0.0-rc2 (c (n "yarn-why") (v "1.0.0-rc2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.3.0") (d #t) (k 0)))) (h "1cgxfsmfann4yp0qsfi6l4kc41c4sz1vba1ff3fbl47wh9mwhv64")))

(define-public crate-yarn-why-1.0.0-rc3 (c (n "yarn-why") (v "1.0.0-rc3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.6") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "yarn-lock-parser") (r "^0.4.0") (d #t) (k 0)))) (h "0s8cxabpmqavggc0vw3qqv0q8hl9b93wrlqv26skrb9qmh8l47bj")))

