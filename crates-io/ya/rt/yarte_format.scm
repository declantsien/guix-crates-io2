(define-module (crates-io ya rt yarte_format) #:use-module (crates-io))

(define-public crate-yarte_format-0.1.0 (c (n "yarte_format") (v "0.1.0") (d (list (d (n "yarte_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.7.0") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.7.0") (d #t) (k 1)))) (h "0c5idrw8hr1aqrnh03gd1lsvsw65ny693chndjrc24g9f9fp311l")))

(define-public crate-yarte_format-0.8.0 (c (n "yarte_format") (v "0.8.0") (d (list (d (n "yarte_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.0") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.0") (d #t) (k 1)))) (h "105wh7dmalb3h3x3g2l6in13kml4rqp76y4qsbl1r8b6flq24vlm")))

(define-public crate-yarte_format-0.8.1 (c (n "yarte_format") (v "0.8.1") (d (list (d (n "yarte_derive") (r "^0.8.1") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.1") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.1") (k 1)))) (h "0rfpm6xr22fjg3dlq75ybva67q3vh7mfd9s12djfh0iv0yq9a539")))

(define-public crate-yarte_format-0.8.2 (c (n "yarte_format") (v "0.8.2") (d (list (d (n "yarte_derive") (r "^0.8.2") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.2") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.2") (k 1)))) (h "0hfc6kfb8sl25hmkjpk0cv8hcz51b7k4k30x2a102pydqga66vpw")))

(define-public crate-yarte_format-0.8.3 (c (n "yarte_format") (v "0.8.3") (d (list (d (n "yarte_derive") (r "^0.8.3") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.3") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.8.3") (k 1)))) (h "0ryymjfyw4vln9m61q4wmccbyjdrvadyj18b8sjdy8aivqc3nr4j")))

(define-public crate-yarte_format-0.9.0 (c (n "yarte_format") (v "0.9.0") (d (list (d (n "yarte_derive") (r "^0.9.0") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.0") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.0") (k 1)))) (h "05gqjvz1hz813a8jhsf370nqslrhz3ygjdqs42wjdjlmy0d2mwpv")))

(define-public crate-yarte_format-0.9.1 (c (n "yarte_format") (v "0.9.1") (d (list (d (n "yarte_derive") (r "^0.9.1") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.1") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.1") (k 1)))) (h "0wki970yp85f7wwbxrzd1nkqwi9dfi6c8wp2zhk6py056zgg4vhj")))

(define-public crate-yarte_format-0.9.2 (c (n "yarte_format") (v "0.9.2") (d (list (d (n "yarte_derive") (r "^0.9.2") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.2") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.2") (k 1)))) (h "1wn9jplysallbzynl78kkiidciw6dwv9dphml63ivsvlbfvqhhha")))

(define-public crate-yarte_format-0.9.3 (c (n "yarte_format") (v "0.9.3") (d (list (d (n "yarte_derive") (r "^0.9.3") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.3") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.3") (k 1)))) (h "0sdf85qd91vmxc7k76isnk5a0q7cnd4wr7nf42madlibmx4r60mn")))

(define-public crate-yarte_format-0.9.4 (c (n "yarte_format") (v "0.9.4") (d (list (d (n "yarte_derive") (r "^0.9.4") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.4") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.4") (k 1)))) (h "13mxqwxka4zyfdbzljblrxh649g362nnjxr45gbragslirq71rm2")))

(define-public crate-yarte_format-0.9.5 (c (n "yarte_format") (v "0.9.5") (d (list (d (n "yarte_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.5") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.5") (k 1)))) (h "01cmb9famk085xm3ki68lvcl8xaidws5rylaq5cn0kf0yasyijxf")))

(define-public crate-yarte_format-0.9.6 (c (n "yarte_format") (v "0.9.6") (d (list (d (n "yarte_derive") (r "^0.9.6") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.6") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.6") (k 1)))) (h "15cnbk3baxspf4jkbh1jhs4wdgkmh6hp3n81y880j0714xa1fr9n")))

(define-public crate-yarte_format-0.9.7 (c (n "yarte_format") (v "0.9.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.9.7") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.7") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.7") (k 1)))) (h "0h14pmyxyp7vgk5d7p53a99hah2kzdb2p976zbxnllw4gxd81k2p") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.9.8 (c (n "yarte_format") (v "0.9.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.9.8") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.8") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.8") (k 1)))) (h "0rhcd5k2djww7qvksqsn51y47c52zgrwrsbq4skhgijxlbh919bc") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.9.9 (c (n "yarte_format") (v "0.9.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.9.9") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.9") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.9") (k 1)))) (h "1pc6v8fhm95047h4wqh4dywk0izyf5shc946qw8gkmhq8mz5xy97") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.9.10 (c (n "yarte_format") (v "0.9.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.9.10") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.10") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.9.10") (k 1)))) (h "0yh9yccan6x335ncqrg23zknn6amwksawwb0wmw8109qkwyi9jsy") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.10.0 (c (n "yarte_format") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.10.0") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.10.0") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.10.0") (k 1)))) (h "051k0m3g3hd0mx4nkfxmsgyg8vkdf00cprczwkbr6cyqrhj1hwck") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.10.1 (c (n "yarte_format") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.10.1") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.10.1") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.10.1") (k 1)))) (h "0vlymwxqbnpr4b9710czj5ygdpani3xnnf9xpb46lr3c32snjvfn") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.11.0 (c (n "yarte_format") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.11.0") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.0") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.0") (k 1)))) (h "1x8ly61mhp5cv0da0i9fkg4diy0fzmvbra10l9rz51f0zzwp7g6x") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.11.1 (c (n "yarte_format") (v "0.11.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.11.1") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.1") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.1") (k 1)))) (h "0jk2038h2bxbl0mmaimpxr5kxdbyw3n2fqcxv1wx77q144asy56j") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.11.2 (c (n "yarte_format") (v "0.11.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.11.2") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.2") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.2") (k 1)))) (h "00lhvggac1446mci33i66b445ydcxfinyilq6nbh9ydn63261mr9") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.11.3 (c (n "yarte_format") (v "0.11.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.11.3") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.3") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.11.3") (k 1)))) (h "0babi5an0ki3wm21nsyr8lsdvlydg8qiyaj9fl7zjg4323v9awzw") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.12.0 (c (n "yarte_format") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.12.0") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.12.0") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.12.0") (k 1)))) (h "08dnmsjifpinz57i612f806lyjpnadg64ssnq1kshil9jp58alzq") (f (quote (("json" "yarte_helpers/json"))))))

(define-public crate-yarte_format-0.12.1 (c (n "yarte_format") (v "0.12.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yarte_derive") (r "^0.12.1") (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.12.1") (f (quote ("display-fn"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.12.1") (k 1)))) (h "1wgx4vbnl516aas4rgkdkl3j0a2dxd9hbhsng5fdj4pm9jwsl2ha") (f (quote (("json" "yarte_helpers/json"))))))

