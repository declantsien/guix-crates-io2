(define-module (crates-io ya rt yarte_wasm_app) #:use-module (crates-io))

(define-public crate-yarte_wasm_app-0.0.1 (c (n "yarte_wasm_app") (v "0.0.1") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1zm3zzbfswri0iqdh2bjavhwxpq0c5hrhlmsyrlfsl82hns2zm3x") (f (quote (("default"))))))

(define-public crate-yarte_wasm_app-0.0.2 (c (n "yarte_wasm_app") (v "0.0.2") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1vlbfhl3vw8yzbn0xql8vlh2b2lhr8h35gfmynvwqg3mrnq46y66")))

(define-public crate-yarte_wasm_app-0.0.3 (c (n "yarte_wasm_app") (v "0.0.3") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "09h2bfq9y82ck7vqw8932qmnwdxvhfwkma7kyi2mk0csqjapkgdx")))

(define-public crate-yarte_wasm_app-0.0.4 (c (n "yarte_wasm_app") (v "0.0.4") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0jq0s57s5rwhx7zfiab02hvqpvxd89lrg8pf3i1pkf5jq5phd33d")))

(define-public crate-yarte_wasm_app-0.0.5 (c (n "yarte_wasm_app") (v "0.0.5") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0wc04akqxps2s6clivgn55w6sv9l23hvwkwhhg5r0c2i25i5805d")))

(define-public crate-yarte_wasm_app-0.0.6 (c (n "yarte_wasm_app") (v "0.0.6") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0bsxw0mnmbx71wnai11xydda948bi8mbjsk8vpz8sca0621ik9hh")))

(define-public crate-yarte_wasm_app-0.0.7 (c (n "yarte_wasm_app") (v "0.0.7") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1lpp77b24sp1rh3czd9mxvjp0pbi7nnpb9vhlxn1k8hdh9w66jzs")))

(define-public crate-yarte_wasm_app-0.0.8 (c (n "yarte_wasm_app") (v "0.0.8") (d (list (d (n "async-timer") (r "^0.7") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "Event" "EventTarget" "HtmlCollection" "HtmlElement" "Node" "Text" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "yarte_derive") (r "^0.7.0") (f (quote ("wasm-app"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "yarte_helpers") (r "^0.7.0") (f (quote ("big_num_32"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "07igvsap4daz3svwga8p3anfbhwz0f7qnj24iy7l2dzzmg4ph9fq")))

(define-public crate-yarte_wasm_app-0.0.9 (c (n "yarte_wasm_app") (v "0.0.9") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "Event" "EventTarget" "HtmlCollection" "HtmlElement" "Node" "Text" "Window"))) (d #t) (k 0)) (d (n "yarte_derive") (r "^0.12.2") (f (quote ("wasm-app"))) (d #t) (k 0)) (d (n "yarte_helpers") (r "^0.12.2") (f (quote ("big-num-32"))) (d #t) (k 0)))) (h "09p6vhjzmp0saqafzmh4dkv3azgwymgqarh7h4zxvk4dvkvia5a2")))

(define-public crate-yarte_wasm_app-0.1.0 (c (n "yarte_wasm_app") (v "0.1.0") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "12jagry91bhqa9vpm0ca6r0z0asn5w0xvg9ffks6dm4p0ncczpbx")))

(define-public crate-yarte_wasm_app-0.1.1 (c (n "yarte_wasm_app") (v "0.1.1") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1l3i783bdn24znzg7mwfrmr9jwzr2ij42dl1igdxx3rvw8ppr1s6")))

(define-public crate-yarte_wasm_app-0.1.2 (c (n "yarte_wasm_app") (v "0.1.2") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yarte_helpers") (r "^0.12.2") (k 1)))) (h "0lawwpbv65s717qmxp4xda86rakcl0xknvzr5zv1yyhbinq8smyx")))

(define-public crate-yarte_wasm_app-0.2.0 (c (n "yarte_wasm_app") (v "0.2.0") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yarte_helpers") (r "^0.12.2") (k 1)))) (h "1lap7mjpidkqlb1glp54jfzmjgi1413vfp5ydgs7ajrihr9xlgwy")))

(define-public crate-yarte_wasm_app-0.2.1 (c (n "yarte_wasm_app") (v "0.2.1") (d (list (d (n "async-timer") (r "^0.7") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "yarte_helpers") (r "^0.12.2") (k 1)))) (h "0vj8n6bzi6iz4hbnrsqbf4c3if8mrwn5mrql2hknl9lnvpm01l3c")))

