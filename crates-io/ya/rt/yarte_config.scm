(define-module (crates-io ya rt yarte_config) #:use-module (crates-io))

(define-public crate-yarte_config-0.0.1 (c (n "yarte_config") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0mfcz0wcm5rlsqh7hvli4ryzmyn7p35b2pw2f819bxsdzwysphyd") (y #t)))

(define-public crate-yarte_config-0.0.2 (c (n "yarte_config") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "09f5vry5p410s9mbc1m0crfs028i50nzxps594b3jmiwc66pg5d8") (y #t)))

(define-public crate-yarte_config-0.0.3 (c (n "yarte_config") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1cpyc7c45dkjxdq3wqh3lhw36yjni0sd513gi7i1wnv3h5a0vd5n") (y #t)))

(define-public crate-yarte_config-0.0.4 (c (n "yarte_config") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0rbc868qzlwmdy3wfh9s64wkji8g2y89vrvh29kj7rr48in5iy7k") (y #t)))

(define-public crate-yarte_config-0.0.5 (c (n "yarte_config") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0p0qzq74dkngz2mql3jj39a7kpyp4yr8m01bxis9651lpj4wz407") (y #t)))

(define-public crate-yarte_config-0.0.6 (c (n "yarte_config") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "197hr11a2r2df8rmq9pf583p9k8xqn3iyknzzpd851gf1m6pl4if") (y #t)))

(define-public crate-yarte_config-0.0.7 (c (n "yarte_config") (v "0.0.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1ql5xyq95zqd1lla69qaf8rlkhs8i2ricq555a3ama8wfq4l8vmv") (y #t)))

(define-public crate-yarte_config-0.0.8 (c (n "yarte_config") (v "0.0.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1y5rih2r0b0h4icb35j85x735va6if3a88fd02g5bsvqmlzk63c0") (y #t)))

(define-public crate-yarte_config-0.0.9 (c (n "yarte_config") (v "0.0.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1ajninyj9dqcd3xm3dz9adg1hc1mgrmqpfkcfly01bx8xp04hi6s") (y #t)))

(define-public crate-yarte_config-0.0.10 (c (n "yarte_config") (v "0.0.10") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1by0w77ygar9gzpjrwvvpng6ib9xcwsrnk41xl83204wi297q32b") (y #t)))

(define-public crate-yarte_config-0.1.0 (c (n "yarte_config") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1557i5nhncbsd5j96p28wrcc33zkxl8p9ps88wp104pc1zw5c4y8") (y #t)))

(define-public crate-yarte_config-0.1.1 (c (n "yarte_config") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1fikpmb27nl5gax7irmbbnkmqj7dz487xgb2qqcp9vmwxsw7gqg0") (y #t)))

(define-public crate-yarte_config-0.2.0 (c (n "yarte_config") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0431nrg935bv8h39w8xms4kn2ws175xlnjyqz9ymmfy5rq5a9cr8") (y #t)))

(define-public crate-yarte_config-0.3.0 (c (n "yarte_config") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1m5qyhxixiyddck7776pd2zczjc89mxgr2l8spjzllxczfi3n6ax")))

(define-public crate-yarte_config-0.3.1 (c (n "yarte_config") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1iim47snsfp0590gk8kxrngd5nhncwzk4n91nqa1l1mlj1jpgyjs")))

(define-public crate-yarte_config-0.3.2 (c (n "yarte_config") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1gc08mhmgkn5smpv6da6xla911i745y5dqqzhx44r6xd1ki5nk5r")))

(define-public crate-yarte_config-0.3.3 (c (n "yarte_config") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0y1qaff4y36l5ss5mm6ch5jmdb823pzk0jz6akwbpy39a6wzhmwf")))

(define-public crate-yarte_config-0.3.4 (c (n "yarte_config") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lv928df85i61vyy7m3bqsq90p7lw62bdv0s600k3w4kzz0n3zgn")))

(define-public crate-yarte_config-0.3.5 (c (n "yarte_config") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1vlkdn1qhmv59vxr1dnf9896806x3zr5habr3ar88bvfn0jwp0iv")))

(define-public crate-yarte_config-0.4.0 (c (n "yarte_config") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02d700igp95ny76l7v0ikhvn0yrlb1laszdsjnyw5dhzv19xhi5b")))

(define-public crate-yarte_config-0.5.0 (c (n "yarte_config") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1wz7gac4r4pdi9dcrqrdhqih7c96iwsxj84g8bf7dbll704jx1rr")))

(define-public crate-yarte_config-0.5.1 (c (n "yarte_config") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ng9a7v4354z267919qk9kd2mrwvsglg4dzqd4ndp81pxcji1x1k")))

(define-public crate-yarte_config-0.5.2 (c (n "yarte_config") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08gfrzs53ja9z5ai3lbwxpxzl6wlwpjdpgsr3wpfqjn7d5q4pir8")))

(define-public crate-yarte_config-0.5.3 (c (n "yarte_config") (v "0.5.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0a8nj0wn2nyh3b2wbdlb7mlwnixsc317kj4agn6gfr5ffy5g9rv7")))

(define-public crate-yarte_config-0.5.4 (c (n "yarte_config") (v "0.5.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0iygmdkfrp9crmm40mi4l96l5rwwl02m2mhzwknk6xvc44qljhdb")))

(define-public crate-yarte_config-0.6.0 (c (n "yarte_config") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0hq24vf2hnz98fmf8b473dkfjhnb1czzc6b1fnacf5gwa341vm8y")))

(define-public crate-yarte_config-0.6.1 (c (n "yarte_config") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0n017arvzkclhw9sjwqcjbycbd97r720za2j25550krmfxvwwzdr")))

