(define-module (crates-io ya rt yarte_lexer) #:use-module (crates-io))

(define-public crate-yarte_lexer-0.0.1 (c (n "yarte_lexer") (v "0.0.1") (d (list (d (n "annotate-snippets") (r "^0.9") (f (quote ("color"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "gencode") (r "^0.0") (d #t) (k 0) (p "yarte_lexer_gencode")) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "059ncvsqayk3bh9pl0pkz9v3x6ac7zbfxg1vy3r64i7cnfwddic2")))

