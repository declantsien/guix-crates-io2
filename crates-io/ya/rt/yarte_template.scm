(define-module (crates-io ya rt yarte_template) #:use-module (crates-io))

(define-public crate-yarte_template-0.5.4 (c (n "yarte_template") (v "0.5.4") (d (list (d (n "yarte_helpers") (r "^0.5") (d #t) (k 0)))) (h "064r6sn06jpzifpzmm1z8b4q4m386wpn7zcrpdk1qay9vw1wwgyi") (f (quote (("mime"))))))

(define-public crate-yarte_template-0.6.0 (c (n "yarte_template") (v "0.6.0") (d (list (d (n "yarte_helpers") (r "^0.6") (d #t) (k 0)))) (h "0d5si0xsyvwjig0q6ki3svnf76gffbqpj37f9n8bm5akwahzkagx") (f (quote (("mime"))))))

(define-public crate-yarte_template-0.6.1 (c (n "yarte_template") (v "0.6.1") (d (list (d (n "yarte_helpers") (r "^0.6") (d #t) (k 0)))) (h "1scsalrqva19amlhs1sfymzfbp7kzf0sqcfdcvw7pv0b3mr5j731") (f (quote (("mime"))))))

