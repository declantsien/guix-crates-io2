(define-module (crates-io ya rt yarte_lexer_gencode) #:use-module (crates-io))

(define-public crate-yarte_lexer_gencode-0.0.1 (c (n "yarte_lexer_gencode") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lhk9n6bzfh59likk9ry962lq4fikpkq3lhqz32g1ss3alwq4zww")))

