(define-module (crates-io ya dn yadns) #:use-module (crates-io))

(define-public crate-yadns-0.1.0 (c (n "yadns") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_json") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0fgggxwqizizhfxrzq2mxv1yla9pyl89mkiwvmzra2jh0s0378jg")))

(define-public crate-yadns-0.2.1 (c (n "yadns") (v "0.2.1") (d (list (d (n "hyper") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.1") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.31.0") (o #t) (d #t) (k 1)) (d (n "url") (r "^0.5.7") (d #t) (k 0)))) (h "0fk23hg2ik67j23hjh61vmbaq82w3q4xl9akdg32alc3a41z2daa") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen" "syntex"))))))

