(define-module (crates-io ya mb yambler) #:use-module (crates-io))

(define-public crate-yambler-0.1.0 (c (n "yambler") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0yy8sy3nm2blarz28n28xlfv5aiy743mcc7aa9cmb5iq0wzxszmw")))

(define-public crate-yambler-0.2.1 (c (n "yambler") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1y0df70s7n62bwrqh2q6p80lci9rw1d2plmaznwffq204gvdmlv2")))

(define-public crate-yambler-0.2.0 (c (n "yambler") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0kcch80sqdfclmfvln0p2iv4sn7lj5n0xz69r78j8vh19cklxab7")))

