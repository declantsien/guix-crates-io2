(define-module (crates-io ya mb yambar-hyprland-wses) #:use-module (crates-io))

(define-public crate-yambar-hyprland-wses-0.1.0 (c (n "yambar-hyprland-wses") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "hyprland") (r "^0.3.8") (d #t) (k 0)))) (h "15chaij8jzx0832yjyrybar3i430l4dslx111bxy9qjaxb561n2k")))

(define-public crate-yambar-hyprland-wses-0.2.0-alpha.1 (c (n "yambar-hyprland-wses") (v "0.2.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "hyprland") (r "^0.4.0-alpha.2") (d #t) (k 0)))) (h "098myz2k7czv1fij089852nnkk041ksqn03k67rh15g4yac4rfr2")))

