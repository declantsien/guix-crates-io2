(define-module (crates-io ya sg yasg) #:use-module (crates-io))

(define-public crate-yasg-0.0.0 (c (n "yasg") (v "0.0.0") (h "1nwca2nmlk8dwgx3avm4w3h1m53b98bdd4lxqfx36pz7qr2pp001")))

(define-public crate-yasg-0.1.0 (c (n "yasg") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "mustache") (r "^0.9.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "055xyd1imw500010r5ca5sgwlc6bba0zmp3xrcisxqdwj1izv7a6")))

