(define-module (crates-io ya sg yasgs) #:use-module (crates-io))

(define-public crate-yasgs-0.1.0 (c (n "yasgs") (v "0.1.0") (h "1rd8dx2a10zmx61kfi16xqpyvahhr8x8g4vzgvvrm0l228glrwcw")))

(define-public crate-yasgs-0.1.1 (c (n "yasgs") (v "0.1.1") (h "1r5san0idi2hkh6yxfrkyl03x03dslzr1y834c4rx49qnmw9w0i4")))

