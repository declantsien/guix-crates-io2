(define-module (crates-io ya ka yakatan_guessing_game) #:use-module (crates-io))

(define-public crate-yakataN_guessing_game-0.1.0 (c (n "yakataN_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bcpzkw1dfn9vh9wslpqbprfc5fi49kf07kgyf0smxsspg0n6iq2")))

(define-public crate-yakataN_guessing_game-0.1.1 (c (n "yakataN_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "102xz2fzkpd4n0nj7h202a77ih25n04b23mxbcncg7gi6m77s0x8")))

