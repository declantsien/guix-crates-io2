(define-module (crates-io ya go yago) #:use-module (crates-io))

(define-public crate-yago-4.0.0 (c (n "yago") (v "4.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("cloudflare_zlib"))) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rio_api") (r "^0.5") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.5") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0i04farjshf3prg0jc0h5ix5dwvxi662ld6ka6z07kak4zzj45n7")))

