(define-module (crates-io ya go yagoll) #:use-module (crates-io))

(define-public crate-yagoll-0.1.0 (c (n "yagoll") (v "0.1.0") (h "07szm5p4irbm8mii182ac8y2cw653sck6wgnaw7d47nh13p99mzk")))

(define-public crate-yagoll-0.1.1 (c (n "yagoll") (v "0.1.1") (h "0150sk84k5cql4m55zwg1cz8ccp24wd3c37a4sa4h75wbln4h1i0")))

(define-public crate-yagoll-0.1.2 (c (n "yagoll") (v "0.1.2") (h "064h0gb0x5cg90qzpc5hwa5vd5lxrrv6x2virhi42cia716b8iib")))

