(define-module (crates-io ya do yadon) #:use-module (crates-io))

(define-public crate-yadon-0.1.0 (c (n "yadon") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1wkjsz529nkcyypk2kdn4sfn5xcra266mdvh6rjf4zha0d7mvpr7") (y #t)))

(define-public crate-yadon-0.1.1 (c (n "yadon") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0hskqz2kxzchihccciqjj17lrwfjy45126dlhdyn1ywfadilfxmc")))

