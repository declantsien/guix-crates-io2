(define-module (crates-io ya xp yaxpeax-avr) #:use-module (crates-io))

(define-public crate-yaxpeax-avr-0.0.1 (c (n "yaxpeax-avr") (v "0.0.1") (d (list (d (n "yaxpeax-arch") (r "^0.0.3") (k 0)))) (h "1k4xi5f42fcwz4pvwjryn5nr58yhs68fi22falznacnny6dr1acr")))

(define-public crate-yaxpeax-avr-0.0.2 (c (n "yaxpeax-avr") (v "0.0.2") (d (list (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "0hc6jkm3y5396mavpqq2v2165jnhnqjblda1f432193h68l6g9qn")))

(define-public crate-yaxpeax-avr-0.0.3 (c (n "yaxpeax-avr") (v "0.0.3") (d (list (d (n "yaxpeax-arch") (r "^0.0.5") (k 0)))) (h "0cn6n4vkbkc5s06pqsrqbjwi9582lk5j6lagiljl62c8gnikwdxq")))

(define-public crate-yaxpeax-avr-0.1.0 (c (n "yaxpeax-avr") (v "0.1.0") (d (list (d (n "yaxpeax-arch") (r "^0.2.3") (k 0)))) (h "0779njr9x5knqxkk8vflqfljjakzchardkk1lyzb1m1x6r73w5cl")))

