(define-module (crates-io ya xp yaxpeax-pic18) #:use-module (crates-io))

(define-public crate-yaxpeax-pic18-0.0.2 (c (n "yaxpeax-pic18") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.3") (k 0)))) (h "1dlqqssvgb2gwqsxfmmz450j8ysgrdkysfb4l2l99svlxm5mssd5") (f (quote (("use-serde") ("default"))))))

(define-public crate-yaxpeax-pic18-0.0.3 (c (n "yaxpeax-pic18") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "069frxghpxcqsqrwrn327s136cpy46z4xxa6zqf1qrz43lp74niz") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

(define-public crate-yaxpeax-pic18-0.0.4 (c (n "yaxpeax-pic18") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.5") (k 0)))) (h "0ql5dm8b0mrxfkr70mpi5h7zigpkxj219g7imm5fcpcwa7h7ka8n") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

(define-public crate-yaxpeax-pic18-0.1.0 (c (n "yaxpeax-pic18") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.3") (k 0)))) (h "0yh922by98f14kiqc3br1iv210wdwyy507p3jhrx5xzhhbab07yg") (f (quote (("use-serde" "serde" "serde_derive") ("default")))) (y #t)))

(define-public crate-yaxpeax-pic18-0.1.1 (c (n "yaxpeax-pic18") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.3") (k 0)))) (h "113zr7f2s60z3nqds32il85wyqmf2viyp7wyj7jj2my6dz9v0jzn") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

