(define-module (crates-io ya xp yaxpeax-mips) #:use-module (crates-io))

(define-public crate-yaxpeax-mips-0.0.2 (c (n "yaxpeax-mips") (v "0.0.2") (d (list (d (n "num_enum") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.3") (k 0)))) (h "0gsj0f5dqdnw3ayn3139qcjp1sskg57il1r9knwhl4faz650yajj") (f (quote (("use-serde") ("default"))))))

(define-public crate-yaxpeax-mips-0.0.3 (c (n "yaxpeax-mips") (v "0.0.3") (d (list (d (n "num_enum") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "00wspnflrdzj35aw7s8x9pvjdimgy64r9vpq7h6s2b1swcsr5rnf") (f (quote (("use-serde") ("default"))))))

(define-public crate-yaxpeax-mips-0.0.4 (c (n "yaxpeax-mips") (v "0.0.4") (d (list (d (n "num_enum") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.5") (k 0)))) (h "13gzw812ynwwyv2w2l6w4w4nxx2gkj0z760789mva0wh0h0l6pc2") (f (quote (("use-serde") ("default"))))))

(define-public crate-yaxpeax-mips-0.1.0 (c (n "yaxpeax-mips") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.2") (k 0)))) (h "0fl7898aflz6clykcxk1g0jvdwqbryb1axpgg0mk3yazlx1x50wd") (f (quote (("use-serde") ("default"))))))

