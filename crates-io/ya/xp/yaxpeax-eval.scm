(define-module (crates-io ya xp yaxpeax-eval) #:use-module (crates-io))

(define-public crate-yaxpeax-eval-0.0.1 (c (n "yaxpeax-eval") (v "0.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (f (quote ("mman" "process" "ptrace"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.4") (f (quote ("std"))) (k 0)) (d (n "yaxpeax-x86") (r "^1.1.5") (f (quote ("fmt" "std"))) (k 0)))) (h "1cmk3jzqk9yf9f5ddps4pdbmycmzxgx2gqlq87viy69ammfpc8x7")))

