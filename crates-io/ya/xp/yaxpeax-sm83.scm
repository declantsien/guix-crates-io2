(define-module (crates-io ya xp yaxpeax-sm83) #:use-module (crates-io))

(define-public crate-yaxpeax-sm83-0.1.0 (c (n "yaxpeax-sm83") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "05b9fqnw5g3jcfhlrzmy0wdigzn318ggls127pywqi77adhxfksn") (f (quote (("use-serde") ("default"))))))

(define-public crate-yaxpeax-sm83-0.2.0 (c (n "yaxpeax-sm83") (v "0.2.0") (d (list (d (n "num_enum") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.3") (k 0)))) (h "1w6jx11l2gdj15xsiprr8j7qk6zq12b5f32hff3jn6hg43pdjxai") (f (quote (("use-serde") ("default"))))))

