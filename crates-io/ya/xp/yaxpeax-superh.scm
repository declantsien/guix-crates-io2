(define-module (crates-io ya xp yaxpeax-superh) #:use-module (crates-io))

(define-public crate-yaxpeax-superh-0.1.0 (c (n "yaxpeax-superh") (v "0.1.0") (d (list (d (n "yaxpeax-arch") (r "^0.0.4") (o #t) (k 0)))) (h "1xyinhmmdfxxl9jv84xkf2xxpd6wm8gplh57c906fx6qkmvryy02") (f (quote (("yaxpeax" "yaxpeax-arch") ("default" "yaxpeax"))))))

(define-public crate-yaxpeax-superh-0.1.1 (c (n "yaxpeax-superh") (v "0.1.1") (d (list (d (n "yaxpeax-arch") (r "^0.0.5") (o #t) (k 0)))) (h "0vlkl0xq5fwb4fj1arla7fxvm1jy5494qqmqwan62y8fnv91ckf0") (f (quote (("yaxpeax" "yaxpeax-arch") ("default" "yaxpeax"))))))

(define-public crate-yaxpeax-superh-0.2.0 (c (n "yaxpeax-superh") (v "0.2.0") (d (list (d (n "yaxpeax-arch") (r "^0.2.4") (o #t) (k 0)))) (h "0lsj3v5gr492i9yk6lcbm62i44dl5ik480nnffw3qlyichs9b08p") (f (quote (("yaxpeax" "yaxpeax-arch") ("default" "yaxpeax"))))))

(define-public crate-yaxpeax-superh-0.3.0 (c (n "yaxpeax-superh") (v "0.3.0") (d (list (d (n "yaxpeax-arch") (r "^0.2.4") (o #t) (k 0)))) (h "0krz809109jba2xy5cqplpjrqkbfqfci7nwzgs6plmdsh71w334c") (f (quote (("yaxpeax" "yaxpeax-arch") ("std" "yaxpeax-arch/std") ("default" "yaxpeax" "std"))))))

(define-public crate-yaxpeax-superh-1.0.0 (c (n "yaxpeax-superh") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.4") (o #t) (k 0)))) (h "0b2fqlc1fnqf9mxzqwk60za8jj3jx01ifa8kcghpdmjvb28i0a65") (f (quote (("yaxpeax" "yaxpeax-arch") ("std" "yaxpeax-arch/std") ("default" "yaxpeax" "std"))))))

