(define-module (crates-io ya xp yaxpeax-pic17) #:use-module (crates-io))

(define-public crate-yaxpeax-pic17-0.0.2 (c (n "yaxpeax-pic17") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.3") (k 0)))) (h "0pd7qm7i6cwg4vxmcmscnswdp9sfr5mkbdp0hkal1nlwhslw3xsj") (f (quote (("use-serde") ("default"))))))

(define-public crate-yaxpeax-pic17-0.0.3 (c (n "yaxpeax-pic17") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "167v8fr0z7sz4z37s5l1vp4wlayiachiygdarc885yvsbm69p56b") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

(define-public crate-yaxpeax-pic17-0.0.4 (c (n "yaxpeax-pic17") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.5") (k 0)))) (h "0p5psi5jlq0j13zj3nmiyd94n2bww2hr1s48f5c71qx7i9dm8jkl") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

(define-public crate-yaxpeax-pic17-0.1.0 (c (n "yaxpeax-pic17") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.3") (k 0)))) (h "195d84f0z3v033k64dm7jjhkjvlaa2sjllcwavhkvs3cx4ipqwfb") (f (quote (("use-serde" "serde" "serde_derive") ("default")))) (y #t)))

(define-public crate-yaxpeax-pic17-0.1.1 (c (n "yaxpeax-pic17") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.3") (k 0)))) (h "07jwq4b0cispn7kmx63nb73s67zj7mmayip1cgjl68bi4frn7jhm") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

