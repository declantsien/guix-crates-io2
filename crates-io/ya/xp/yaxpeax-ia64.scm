(define-module (crates-io ya xp yaxpeax-ia64) #:use-module (crates-io))

(define-public crate-yaxpeax-ia64-0.1.0 (c (n "yaxpeax-ia64") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "1wqkxwj7hh6ib3w8k54z6f2p8rcw61rfn0gjy1kl99m3yg14h83c")))

(define-public crate-yaxpeax-ia64-0.1.1 (c (n "yaxpeax-ia64") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "14969gcmxj980wrk7g8cpkm2rrmkf6bx79kblfk31mlx2vg7n8dp")))

(define-public crate-yaxpeax-ia64-0.1.2 (c (n "yaxpeax-ia64") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "0v3yaa4icd13zxib5vxzx4lpr96f8gph0153cypv7x9gg9ikf1iz")))

(define-public crate-yaxpeax-ia64-0.1.3 (c (n "yaxpeax-ia64") (v "0.1.3") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "12l2rg9mm6yzi6knvqvvazna3ssb9brb60qgdhxd15dx64ca62n3")))

(define-public crate-yaxpeax-ia64-0.1.4 (c (n "yaxpeax-ia64") (v "0.1.4") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.5") (k 0)))) (h "0icshqzlab4kww9d28rjjc5hij42lwk1pq08gfj0cwi6d8k3jbr2")))

(define-public crate-yaxpeax-ia64-0.2.0 (c (n "yaxpeax-ia64") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.2") (k 0)))) (h "1j7a81jvg5xbzd91m5n27hwq7f2rxn4221a2yciqz9zrn8p8dmlz")))

(define-public crate-yaxpeax-ia64-0.2.1 (c (n "yaxpeax-ia64") (v "0.2.1") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.2") (k 0)))) (h "0kliz6awljhayy6m93bp8cw9kpgpbxswschmhsqpxyr8awmhzsnk") (f (quote (("std") ("default" "std"))))))

