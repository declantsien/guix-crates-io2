(define-module (crates-io ya xp yaxpeax-msp430) #:use-module (crates-io))

(define-public crate-yaxpeax-msp430-0.0.3 (c (n "yaxpeax-msp430") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.3") (k 0)))) (h "0na2lgpz6h9gb1x0j0za2n11kqk32bsjm87hkxwsjrdi2vaqa64c") (f (quote (("use-serde") ("default"))))))

(define-public crate-yaxpeax-msp430-0.0.5 (c (n "yaxpeax-msp430") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.4.0") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (k 0)))) (h "090ziv5m1hc84grsv005jf7raxj96iphwmbm8ghw3bda0m3zwmnm") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

(define-public crate-yaxpeax-msp430-0.0.6 (c (n "yaxpeax-msp430") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.5") (k 0)))) (h "01nrpfpkrq66s7g0457mg8vxp61vxi4cc4c9v0a6cxm1a4aa0phz") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

(define-public crate-yaxpeax-msp430-0.1.0 (c (n "yaxpeax-msp430") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.3") (k 0)))) (h "1ialm3j568cngrazik87c6fiw048agwqa5rrlfillri4hvb2p5k3") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

(define-public crate-yaxpeax-msp430-0.1.1 (c (n "yaxpeax-msp430") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.2.7") (k 0)))) (h "1gp8yr442kpl9l1ygqkv3qdy9s4gkrw66grs595nnwisq0g23qjx") (f (quote (("use-serde" "serde" "serde_derive") ("default"))))))

