(define-module (crates-io ya xp yaxpeax-6502) #:use-module (crates-io))

(define-public crate-yaxpeax-6502-0.0.1 (c (n "yaxpeax-6502") (v "0.0.1") (d (list (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.5") (k 0)))) (h "18sybkq34dank8w9hpfmfcvahbl593yqw477xzp895vrrm4zwfnm")))

(define-public crate-yaxpeax-6502-0.0.2 (c (n "yaxpeax-6502") (v "0.0.2") (d (list (d (n "yaxpeax-arch") (r "^0.2.4") (k 0)))) (h "1hqpgjg063piwnz6aij0y7l30xrv5imw2n0amna71xcrlwhd52by") (f (quote (("std" "yaxpeax-arch/std") ("default"))))))

