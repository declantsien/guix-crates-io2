(define-module (crates-io ya xp yaxpeax-lc87) #:use-module (crates-io))

(define-public crate-yaxpeax-lc87-1.0.0 (c (n "yaxpeax-lc87") (v "1.0.0") (d (list (d (n "yaxpeax-arch") (r "^0.2.4") (k 0)))) (h "1xmw0jidd3a5cpck6d8nmg9kn66rvj2lsqnz6p8fx6k75ciww52d")))

(define-public crate-yaxpeax-lc87-1.0.1 (c (n "yaxpeax-lc87") (v "1.0.1") (d (list (d (n "yaxpeax-arch") (r "^0.2.4") (k 0)))) (h "1zqcmk6xgw7lmfaqq5npvy2j1xx2v42n38z9wh223yn2cprmdx9c")))

