(define-module (crates-io ya rf yarf) #:use-module (crates-io))

(define-public crate-yarf-0.0.1 (c (n "yarf") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "yarf-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1g5k7b0cqk6n8v6vjxynfcaqyagza4vpi1a5k909vaaaz1s6di38")))

(define-public crate-yarf-0.0.2 (c (n "yarf") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "yarf-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1s6pk0jxiv8pf2msffxjp090mi4bazyca84wflrf65pbi35ib41v")))

