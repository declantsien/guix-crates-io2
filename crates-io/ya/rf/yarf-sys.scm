(define-module (crates-io ya rf yarf-sys) #:use-module (crates-io))

(define-public crate-yarf-sys-0.0.1 (c (n "yarf-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "013nfrrxccx751c4biiy2r56b1v2hc046a9dbm742inp8xkwnhg6")))

(define-public crate-yarf-sys-0.0.2 (c (n "yarf-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1khndcidsaj4dq5mi0r9fb33r2vfk02rgv9mp9c5wihmni1fgqif")))

