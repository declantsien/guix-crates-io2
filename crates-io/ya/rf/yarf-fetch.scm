(define-module (crates-io ya rf yarf-fetch) #:use-module (crates-io))

(define-public crate-yarf-fetch-0.1.0 (c (n "yarf-fetch") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.8.0") (d #t) (k 0)))) (h "185vhfmv5ks9g9szp5gg367asyxa13bjhhigcy4z9crz7m1ig71n")))

