(define-module (crates-io ya so yason) #:use-module (crates-io))

(define-public crate-yason-0.0.0 (c (n "yason") (v "0.0.0") (d (list (d (n "decimal-rs") (r "^0.1.30") (d #t) (k 0)) (d (n "sqldatetime") (r "^0.1.23") (f (quote ("oracle"))) (d #t) (k 0)))) (h "0c3qv9wn4bkjdbvp5ga02fzf83n4wp96d8nvswjh79bm23919b6k")))

(define-public crate-yason-0.0.1 (c (n "yason") (v "0.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "decimal-rs") (r "^0.1.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (f (quote ("arbitrary_precision"))) (d #t) (k 0)))) (h "0pi248ych11wsajz60x7ll6d7gmam1lv7i7vliapi1gjzd18vih5")))

(define-public crate-yason-0.0.2 (c (n "yason") (v "0.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "decimal-rs") (r "^0.1.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (f (quote ("arbitrary_precision"))) (d #t) (k 0)))) (h "0zm5c8nkq5k9sqa5j4af49ikd89yi61n5z74x2mj27y72wbmh727") (r "1.57")))

