(define-module (crates-io ya zi yazi-cli) #:use-module (crates-io))

(define-public crate-yazi-cli-0.2.5 (c (n "yazi-cli") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.82") (d #t) (k 1)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.5.2") (d #t) (k 1)) (d (n "clap_complete_fig") (r "^4.5.0") (d #t) (k 1)) (d (n "clap_complete_nushell") (r "^4.5.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 1)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "yazi-dds") (r "^0.2.5") (d #t) (k 0)))) (h "1v8p32nqgaxw303h4cyfpw49kw5k4vzm2jh8d9dy2b2kr8v7wbnv")))

