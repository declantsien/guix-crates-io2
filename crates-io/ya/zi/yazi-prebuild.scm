(define-module (crates-io ya zi yazi-prebuild) #:use-module (crates-io))

(define-public crate-yazi-prebuild-0.1.0 (c (n "yazi-prebuild") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "13icp5xiy1la6ppnb4phf6dm31pg89ndlbv59kalpr96z9rzzpc1")))

(define-public crate-yazi-prebuild-0.1.1 (c (n "yazi-prebuild") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "syntect") (r "^5") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2") (o #t) (d #t) (k 0)))) (h "1q5759hcvbi8wz6yr67jr3lv84v26dwgh3iwrc7mgbvpkmq5fi1r") (f (quote (("build_deps" "anyhow" "syntect" "walkdir"))))))

(define-public crate-yazi-prebuild-0.1.2 (c (n "yazi-prebuild") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "syntect") (r "^5") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2") (o #t) (d #t) (k 0)))) (h "0cpnfhyb82f7xqlxgawi0qiqvnp0i1dnxwwnz9whzb1r5vhwidpl") (f (quote (("build_deps" "anyhow" "syntect" "walkdir"))))))

