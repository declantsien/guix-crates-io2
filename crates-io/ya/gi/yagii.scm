(define-module (crates-io ya gi yagii) #:use-module (crates-io))

(define-public crate-yagii-0.1.0 (c (n "yagii") (v "0.1.0") (d (list (d (n "digest") (r "^0.7.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)))) (h "12b42kbzv7faz5fwgcpcd1hiyb2s104yzl7c7s12q5amz046xgri") (y #t)))

(define-public crate-yagii-0.1.1 (c (n "yagii") (v "0.1.1") (d (list (d (n "digest") (r "^0.7.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)))) (h "0g2s2gsb9j6yc3afsmpzx3x16kkn45mpivkhmci98cq2sbx49qzx") (y #t)))

(define-public crate-yagii-0.1.2 (c (n "yagii") (v "0.1.2") (d (list (d (n "digest") (r "^0.7.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)))) (h "1mxf424b7p9nxlzcy51wsh3mb2k5zb27b6rlsf3dvmg54skh4ik5") (y #t)))

(define-public crate-yagii-0.1.3 (c (n "yagii") (v "0.1.3") (d (list (d (n "digest") (r "^0.7.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "palette") (r "^0.4.1") (d #t) (k 0)) (d (n "sha3") (r "^0.7.3") (d #t) (k 0)))) (h "1yksxgyd40v410q2jxbw4w54ala4plpzac8w1xxvn8bsrdb6n0xj")))

