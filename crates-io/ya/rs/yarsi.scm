(define-module (crates-io ya rs yarsi) #:use-module (crates-io))

(define-public crate-yarsi-0.1.0 (c (n "yarsi") (v "0.1.0") (d (list (d (n "columns") (r "^0.1.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.2") (d #t) (k 0)) (d (n "whoami") (r "^1.3.0") (d #t) (k 0)))) (h "0qmksqhxbz8qka8syb8is4wdzf0w3rqgcgl8qzsg3lph1r5fpbfg")))

