(define-module (crates-io ya ga yaga-derive) #:use-module (crates-io))

(define-public crate-yaga-derive-0.1.0 (c (n "yaga-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0pizdw1ngcp7vnf2swg1klf6jli5bbg39084wbh7q4gvyz83qy4f")))

