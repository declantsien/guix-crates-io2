(define-module (crates-io ya ga yaga) #:use-module (crates-io))

(define-public crate-yaga-0.1.0 (c (n "yaga") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "yaga-derive") (r "^0.1.0") (d #t) (k 0)))) (h "05cxzmwq94f0ypfy6m6rwmjvady23c4fgwyc7wwn2wkrwxa6gpvh")))

