(define-module (crates-io ur cu urcu) #:use-module (crates-io))

(define-public crate-urcu-0.0.1 (c (n "urcu") (v "0.0.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1rjz26572c4zqxvm3nsy3zganxfac3hlg1xcn43ryc4727m1v814") (l "urcu-memb")))

(define-public crate-urcu-0.0.2 (c (n "urcu") (v "0.0.2") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0grfcyrf9f4n2vngdysnwlb7l5qgc6q4zhqf6a8aw13x7d1i6gif") (l "urcu-memb")))

(define-public crate-urcu-0.0.3 (c (n "urcu") (v "0.0.3") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0rwrvqvm69qym1i3xm4y1h603zngmajzwhhcpafjl83bwp8pmalx") (l "urcu-memb")))

