(define-module (crates-io ur cu urcu-sys) #:use-module (crates-io))

(define-public crate-urcu-sys-0.0.1 (c (n "urcu-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0vm4kpmdggx1cmwfmfawpspkfgl60485s9pc98jgns68fccs0jqc") (l "urcu")))

(define-public crate-urcu-sys-0.0.2 (c (n "urcu-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "04g0153qni73797gs3bwi9hjr2iyrhazyw9bry5r2d3ls4ihssjc") (l "urcu")))

(define-public crate-urcu-sys-0.0.3 (c (n "urcu-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "144441i05zpia1vm4vimab4k74az20m2y8yxmz4jfk1f9964gnbx") (l "urcu")))

(define-public crate-urcu-sys-0.0.4 (c (n "urcu-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0pqswc9k8niq54ggrd2q6n5y8xqcqspggb5998gs1y7a2nvaix8k") (l "urcu")))

(define-public crate-urcu-sys-0.0.5 (c (n "urcu-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "1dbig1a81935ari3gf8v2gyzjxsr7zg0qba04g9vmy9998g73wgz") (l "urcu")))

