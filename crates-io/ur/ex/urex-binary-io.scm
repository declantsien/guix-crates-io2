(define-module (crates-io ur ex urex-binary-io) #:use-module (crates-io))

(define-public crate-urex-binary-io-0.1.0 (c (n "urex-binary-io") (v "0.1.0") (d (list (d (n "half") (r "^2.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0adjyrjj5clm11sz0x337vzr011ylyk2kwz90ss6fx5iwwxjwxms")))

(define-public crate-urex-binary-io-0.1.1 (c (n "urex-binary-io") (v "0.1.1") (d (list (d (n "half") (r "^2.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1s308v80x281nb1w0qg08j0dwb2qyxgvm4s3wpz5wyq15byrhw1i") (r "1.61")))

(define-public crate-urex-binary-io-0.1.2 (c (n "urex-binary-io") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "half") (r "^2.2.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "06z1vqj0hk80w0mn0339w33is7i26abdb43mcsj3zsnq649sfld0") (r "1.61")))

