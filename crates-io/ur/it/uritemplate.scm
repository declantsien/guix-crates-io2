(define-module (crates-io ur it uritemplate) #:use-module (crates-io))

(define-public crate-uritemplate-0.1.0 (c (n "uritemplate") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.32") (d #t) (k 0)))) (h "01qs5r2pmzx1rc88a24kzvx8nlqnlwnljlfxyjxbaazdxc7h52lw")))

(define-public crate-uritemplate-0.1.1 (c (n "uritemplate") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.32") (d #t) (k 0)))) (h "09h3z5a37xmp8yvg5iwcm6qrc4acmqpkh58ca0hxv6s7y76h1ail")))

(define-public crate-uritemplate-0.1.2 (c (n "uritemplate") (v "0.1.2") (d (list (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "0j97yaxhygv8q55wnjya683c9dllwfahmyq0shphrm40fcna7sh1")))

