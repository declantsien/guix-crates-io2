(define-module (crates-io ur in uring-fs) #:use-module (crates-io))

(define-public crate-uring-fs-0.1.0 (c (n "uring-fs") (v "0.1.0") (d (list (d (n "extreme") (r "6.*") (d #t) (k 2)) (d (n "io-uring") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (k 0)))) (h "0dk61g5i0w6hiwrwaqz4dggk1qsarsgpzl6r2x5adcf3nv1fcx9c")))

(define-public crate-uring-fs-0.2.0 (c (n "uring-fs") (v "0.2.0") (d (list (d (n "extreme") (r "6.*") (d #t) (k 2)) (d (n "io-uring") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (k 0)))) (h "1b5cwr8lb7jk1nlm825pw4hxqdpz90aymyjack6jc8kpyq6zv5nq")))

(define-public crate-uring-fs-1.2.0 (c (n "uring-fs") (v "1.2.0") (d (list (d (n "extreme") (r "6.*") (d #t) (k 2)) (d (n "io-uring") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (k 0)))) (h "0lx6fh98n0y5ilxbn9vca9dvzw30mmp7f2b23j9wb8ia588mywb7") (y #t)))

(define-public crate-uring-fs-1.3.0 (c (n "uring-fs") (v "1.3.0") (d (list (d (n "extreme") (r "6.*") (d #t) (k 2)) (d (n "io-uring") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (k 0)))) (h "07yh8jijnjv6f2vzh7bsl0hlv2ixm5905cpi0h1vbbjbj884qd4f") (y #t)))

(define-public crate-uring-fs-1.4.0 (c (n "uring-fs") (v "1.4.0") (d (list (d (n "extreme") (r "6.*") (d #t) (k 2)) (d (n "io-uring") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (k 0)))) (h "0sck03q1y1gr8acz8cb63clms1px85n2h5rkmanaq1xbf7zxhwmd")))

