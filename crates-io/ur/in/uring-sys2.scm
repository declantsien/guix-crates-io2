(define-module (crates-io ur in uring-sys2) #:use-module (crates-io))

(define-public crate-uring-sys2-0.8.0 (c (n "uring-sys2") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "04rgdybirb98rdxwl9404p1fm3anipq0bnff848hmdd5x41g0b89") (l "uring")))

(define-public crate-uring-sys2-0.9.0 (c (n "uring-sys2") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "0y24zlpyk9g95haax7w3awpcmdbhbw3vrsq5whqg6asjk9f09105") (y #t) (l "uring")))

(define-public crate-uring-sys2-0.9.1 (c (n "uring-sys2") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.69.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "0g0a9d5436djn1yx7yjx3am5hhdxm81wlajwzxwvnxhnzjx2qfg8") (l "uring")))

