(define-module (crates-io ur in uring-sys) #:use-module (crates-io))

(define-public crate-uring-sys-1.0.0-alpha (c (n "uring-sys") (v "1.0.0-alpha") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "18ljlnjxvrwja7xdn902fz9phcvjkzb94ji10q1iwmj3jqw4d8j5") (l "uring")))

(define-public crate-uring-sys-1.0.0-beta (c (n "uring-sys") (v "1.0.0-beta") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0mpqvbba89iby8mzl9yxx6bi1br3g7icqvl2rbxi1433s7n2h4aq") (l "uring")))

(define-public crate-uring-sys-0.6.0 (c (n "uring-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1v3id7agkxfy720i183148247dncj0jpcc05bmrg18f3py4wvz8p") (l "uring")))

(define-public crate-uring-sys-0.6.1 (c (n "uring-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1rwf2y7yxlqc14ibapqvkyy0132x6cn23rzzyazsx6fhgxxnv72a") (l "uring")))

(define-public crate-uring-sys-0.7.0 (c (n "uring-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "05hw19svhrhk59sbvakza66mdnj5bc3r3gfc6kkkx19ah8jcgdcn") (y #t) (l "uring")))

(define-public crate-uring-sys-0.7.1 (c (n "uring-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "13w8plvbgzwiza1fq1gx9avn6p94v3pm49kfc1gaw05dh762kjg2") (y #t) (l "uring")))

(define-public crate-uring-sys-0.7.2 (c (n "uring-sys") (v "0.7.2") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0bsnhjqw4xixrci1sm9lipjh6qzzlcakivbn5ck9phjmhxdaa5gc") (l "uring")))

(define-public crate-uring-sys-0.7.3 (c (n "uring-sys") (v "0.7.3") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "128slw4dqlhx1ad13brxw7nrmgjx24c0xx1spnnzd51pr98zqqcl") (l "uring")))

(define-public crate-uring-sys-0.7.4 (c (n "uring-sys") (v "0.7.4") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0k1jldflhr522bwm31i4f6cb22d72fj47ml4knasiax50404c34a") (l "uring")))

