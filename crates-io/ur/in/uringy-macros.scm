(define-module (crates-io ur in uringy-macros) #:use-module (crates-io))

(define-public crate-uringy-macros-0.1.0 (c (n "uringy-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "06nywh2r20sfvphkkj7x8hqwbd87pk5ghbz536b2s4ab8r917mra")))

(define-public crate-uringy-macros-0.2.0 (c (n "uringy-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0z5p32c9anxnsfksqinzlvk782c7mwi0lw12cvj1izhxhip75ffy")))

(define-public crate-uringy-macros-0.2.1 (c (n "uringy-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0v2lxgsw4d8w3yxbxmilj588ikfqgsbdnhh3gpx50mr614yq9vmp")))

