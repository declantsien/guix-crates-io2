(define-module (crates-io ur lo urlocator) #:use-module (crates-io))

(define-public crate-urlocator-0.1.0 (c (n "urlocator") (v "0.1.0") (h "1dwl9zvlv1m62vxb2xyldw73y3p02drglc69n57nlxbdlhb9wwlh") (f (quote (("bench"))))))

(define-public crate-urlocator-0.1.1 (c (n "urlocator") (v "0.1.1") (h "000ppssk6gb73d4cdn17i5bvwp8rqd35czgpykx96krbcmba3al0") (f (quote (("bench"))))))

(define-public crate-urlocator-0.1.2 (c (n "urlocator") (v "0.1.2") (h "1xzhwmqrqyk8p3s5npqpidrn0gjapqx5fshrx633fk56j7cm8qm1") (f (quote (("nightly"))))))

(define-public crate-urlocator-0.1.3 (c (n "urlocator") (v "0.1.3") (h "0r5ig00np3svjpvb1gha3ni798cwj2w7rnlwrc8jrrw7bvlb2yri") (f (quote (("nightly"))))))

(define-public crate-urlocator-0.1.4 (c (n "urlocator") (v "0.1.4") (h "181mqlydc1i7q57jp56pbysn4km4cx34jldr92kv1bvd217rmqql") (f (quote (("nightly"))))))

