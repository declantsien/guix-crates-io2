(define-module (crates-io ur an urandom) #:use-module (crates-io))

(define-public crate-urandom-0.1.0 (c (n "urandom") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dataview") (r "^0.1") (k 0)) (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)))) (h "146xk93jp6lw7gbxzcz41f0biz06jfgqjbapmh4y4nrcaq8zbslg") (f (quote (("default" "getrandom"))))))

(define-public crate-urandom-0.1.1 (c (n "urandom") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dataview") (r "~1.0") (k 0)) (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)))) (h "116k3h6hkl3rnrv89w6kk7ba16d6cgln4lnblfk3alfwxgh1mn0w") (f (quote (("std") ("default" "getrandom" "std"))))))

