(define-module (crates-io ur lc urlcode) #:use-module (crates-io))

(define-public crate-urlcode-0.0.0 (c (n "urlcode") (v "0.0.0") (h "1sigiwf84iwhf82w4f2gg4wqzxcwj536rcbiss2cq0qkwg45hwll") (y #t)))

(define-public crate-urlcode-0.1.0 (c (n "urlcode") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qyca3l86z2vkila34kyhp14dcpdr0zjgbxyj4dimzrd1ivlsimx")))

