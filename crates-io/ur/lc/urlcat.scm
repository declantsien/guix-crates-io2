(define-module (crates-io ur lc urlcat) #:use-module (crates-io))

(define-public crate-urlcat-0.1.0 (c (n "urlcat") (v "0.1.0") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0g5nnwn7f78yiflvi4hz042hymh5zgd3s4xqj8kjaq1p8jp2fid4") (y #t)))

(define-public crate-urlcat-0.1.1 (c (n "urlcat") (v "0.1.1") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0h3jxcn8kwrx0yw4rjkpbsa3r351q6xwc9xw8y3zj31ks1xz6jb0") (y #t)))

(define-public crate-urlcat-0.1.2 (c (n "urlcat") (v "0.1.2") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1gpiyjzipkj5h7azccc7kq7gjklmpd39awcz5fb1jvx36yl699ll") (y #t)))

(define-public crate-urlcat-0.1.3 (c (n "urlcat") (v "0.1.3") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0m0fk182hf4i11vihfka5wg64ydch2c5zzpl7d30vrz6f2d0zh4z") (y #t)))

(define-public crate-urlcat-0.1.4 (c (n "urlcat") (v "0.1.4") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "02k7wfd8347kmwqf43zx3gllsx40y4f2zs2krw1bsicqf5mwqrd8") (y #t)))

(define-public crate-urlcat-0.1.5 (c (n "urlcat") (v "0.1.5") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1ci92q55gif1zm6fncfk27vb1d5syqkafm8kiswalfq7pscvypdr")))

(define-public crate-urlcat-0.2.0 (c (n "urlcat") (v "0.2.0") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "082mfklmk6g49s1d5lmysgj4bmdgkryvl9h5dq88cj0lmrbwjhcz")))

(define-public crate-urlcat-0.2.1 (c (n "urlcat") (v "0.2.1") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "038zn0j0cwlls9aad9nfxn6qng40dwdim9hlxsxgnjq55c8mdvq6")))

(define-public crate-urlcat-0.2.2 (c (n "urlcat") (v "0.2.2") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1gbkg5lkhi858dlq07qvii0vnvq888cmqxndq3bpqb7ak8pis06m")))

