(define-module (crates-io ur lc urlchecker) #:use-module (crates-io))

(define-public crate-urlchecker-0.1.0 (c (n "urlchecker") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "04997y52452cchhr9r815v05h1hkdbbrkcm2gnkwa85ksw96ffjy")))

(define-public crate-urlchecker-0.2.0 (c (n "urlchecker") (v "0.2.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1cafmq28lccxk0a5b8wa49ihli882k48qdv1gy612rj53i3kgnsh")))

(define-public crate-urlchecker-0.3.0 (c (n "urlchecker") (v "0.3.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1haddbg721gdf5dqgwsc960fy208fb5ngczdz9yhj4ib44q2slys")))

