(define-module (crates-io ur pc urpc) #:use-module (crates-io))

(define-public crate-urpc-0.1.0-alpha (c (n "urpc") (v "0.1.0-alpha") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "postcard") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (k 0)))) (h "1ihmxvcn9a9qppsb0y79zq14gyz0smir07hfyvffxic9vnc79q2s") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-urpc-0.1.0-alpha.1 (c (n "urpc") (v "0.1.0-alpha.1") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "postcard") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (k 0)))) (h "08dmi2bj1lccr6ym1sfi4vk6rd9ybzdrn9c5kwj5hshdf87jq46r") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-urpc-0.1.0-alpha.2 (c (n "urpc") (v "0.1.0-alpha.2") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "postcard") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (k 0)))) (h "0rhh2wshw66jmhj05kv5db1n2j20ld9lqyj524cwa0r9cs667wkz") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-urpc-0.2.0 (c (n "urpc") (v "0.2.0") (d (list (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)) (d (n "postcard") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (k 0)))) (h "13najaf40wqks00zqwjva6lyskmaz02wwxfjqy9vgmy808nsbvxj") (f (quote (("std" "serde/std") ("default" "std"))))))

