(define-module (crates-io ur i_ uri_parser) #:use-module (crates-io))

(define-public crate-uri_parser-0.1.0 (c (n "uri_parser") (v "0.1.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0zl93kvq1c2x5ki3fv409059bix03qbi73qwq6lp7zkhxnb2h8ni")))

(define-public crate-uri_parser-0.2.0 (c (n "uri_parser") (v "0.2.0") (d (list (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1y9kkbr4vsp636da78i42q77m1rinfs258nd8vjnjjy6szn9005s")))

