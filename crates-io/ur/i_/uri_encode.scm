(define-module (crates-io ur i_ uri_encode) #:use-module (crates-io))

(define-public crate-uri_encode-1.0.0 (c (n "uri_encode") (v "1.0.0") (h "098rgs5l26ys82kxkxqfqpghpmrfxgiaddxj92k20frmknabfirp")))

(define-public crate-uri_encode-1.0.1 (c (n "uri_encode") (v "1.0.1") (h "1svwz3w4lpldcsvnxz99ng0vy4vjqly2qsd2pjyq91vgfcfbbb1l")))

(define-public crate-uri_encode-1.0.2 (c (n "uri_encode") (v "1.0.2") (h "0ly4sdkalxkvkng80vc2si85iydxmk6by3gg6cnqqdrfr082w16i")))

