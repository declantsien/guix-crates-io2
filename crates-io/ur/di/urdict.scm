(define-module (crates-io ur di urdict) #:use-module (crates-io))

(define-public crate-urdict-0.2.0 (c (n "urdict") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^1.4.5") (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 0)) (d (n "select") (r "^0.1.2") (d #t) (k 0)))) (h "0j0f46qp7s6yrfwa26i2vm9ndlizhrb2rxrkkv3s9fcyyp7ih9d1")))

(define-public crate-urdict-0.3.0 (c (n "urdict") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^1.4.5") (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 0)) (d (n "select") (r "^0.1.2") (d #t) (k 0)))) (h "152qv2fzap4rx5rrarxdlhy11fmrnfx416kwnmv1ljj26b45gk7h")))

(define-public crate-urdict-0.3.1 (c (n "urdict") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clap") (r "^1.4.5") (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 0)) (d (n "select") (r "^0.1.2") (d #t) (k 0)))) (h "1cy4ijbp2f8b4srrg7n05cjdq6gn0sgnvh8vc59h1pdpgcplxbj9")))

(define-public crate-urdict-0.3.2 (c (n "urdict") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.7.1") (d #t) (k 0)) (d (n "clap") (r "^2.0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "select") (r "^0.2.2") (d #t) (k 0)))) (h "0gxndyrhmxx12y0gd8bj1zr0v6ba8wn7k0d7v8i8yynfp6x9vxla")))

(define-public crate-urdict-0.3.3 (c (n "urdict") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "1jhqsngb5qyp1w2931kpinsspgkadbx7xjwp43fmkpw514vkas9c")))

(define-public crate-urdict-0.3.4 (c (n "urdict") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "select") (r "^0.4.2") (d #t) (k 0)))) (h "0rdvr5q4hd58g4fzz2lz0rzssx614x52qg6990s9rbraisb7achj")))

