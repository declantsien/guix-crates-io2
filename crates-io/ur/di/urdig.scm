(define-module (crates-io ur di urdig) #:use-module (crates-io))

(define-public crate-urdig-0.9.5 (c (n "urdig") (v "0.9.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "phollaits") (r "^0.1") (d #t) (k 0)) (d (n "udev") (r "^0.5") (d #t) (k 0)))) (h "1pq7ygr11wcmx490qa0wz51v7qvh4dlq710a1am5djvy2mkp66n3")))

(define-public crate-urdig-0.9.6 (c (n "urdig") (v "0.9.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "phollaits") (r "^0.1") (d #t) (k 0)) (d (n "udev") (r "^0.5") (d #t) (k 0)))) (h "0gcfizqxs796bmz5agyw1jc08ps9vf0h9k8vcz6k62629r8n3n7l")))

(define-public crate-urdig-1.0.0 (c (n "urdig") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "phollaits") (r "^0.2") (d #t) (k 0)) (d (n "udev") (r "^0.5") (d #t) (k 0)))) (h "0kip5cgshkkvkjlfhyxypqqhjms3v7p3nll4x9c6f70hcy618d71")))

