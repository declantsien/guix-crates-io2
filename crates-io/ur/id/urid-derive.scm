(define-module (crates-io ur id urid-derive) #:use-module (crates-io))

(define-public crate-urid-derive-0.1.0 (c (n "urid-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0vyzywa7br0j0wr8137wign7b10zc9h2pc3m88vb9qwmm28lfcpm")))

(define-public crate-urid-derive-0.1.1 (c (n "urid-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0i1nf0sgq4ai051h17s9msaavl3jfzdmdlsy8455pr88y0pfx7l1")))

