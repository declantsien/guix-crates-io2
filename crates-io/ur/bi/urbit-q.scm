(define-module (crates-io ur bi urbit-q) #:use-module (crates-io))

(define-public crate-urbit-q-0.1.0 (c (n "urbit-q") (v "0.1.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "12nmbjln9hd5d8rslbpg9kn4bqw66rsb4m3alk05afk8fnyc6af8")))

(define-public crate-urbit-q-0.1.1 (c (n "urbit-q") (v "0.1.1") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "0680fwagcxybakw03npw8v9cackzy68cfq58s30y20l8kc8y3m1i")))

(define-public crate-urbit-q-0.1.2 (c (n "urbit-q") (v "0.1.2") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "1m48qki1mbmih80zrfrlyi686nn61lfb8iipx514bsinn1iamfm5")))

(define-public crate-urbit-q-0.2.0 (c (n "urbit-q") (v "0.2.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "0xs6h0fac0nxdh2kkn81ig94bl6ypqy9p0y6wf3mx3q279dv7cdp")))

(define-public crate-urbit-q-0.2.1 (c (n "urbit-q") (v "0.2.1") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "1b3ncf21fi2jy5js0visj42q47ss4gq9ayz394hb910qlxnkwc9y")))

(define-public crate-urbit-q-0.2.2 (c (n "urbit-q") (v "0.2.2") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)))) (h "0gz608s0z5y4kvnhdiykf4zgm054g5plsn3hn2pxmvxv9cs3whpw")))

(define-public crate-urbit-q-0.2.3 (c (n "urbit-q") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1m93in9jpng5yxia93j843rz3ri82lxr85kv2j5jd1ni4r35yhfh")))

(define-public crate-urbit-q-0.2.5 (c (n "urbit-q") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1hsmkyfxnb3ghx8pjmdz32ir92clfh1h26fjqr4b8anmjxj6giid")))

(define-public crate-urbit-q-0.2.6 (c (n "urbit-q") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0qkar73jk5mf5fvfv3bc6y987y6w96hzzfgkr4mg95yyli20gw8w")))

(define-public crate-urbit-q-0.3.0 (c (n "urbit-q") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "naughty-strings") (r "^0.2.3") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0s4dr87c8gq9g4x3q0nihskj3zjfnr6xvv24zz6lzll2l7vxj283")))

(define-public crate-urbit-q-0.3.1 (c (n "urbit-q") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "naughty-strings") (r "^0.2.3") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1vjibsm1l4l6gj6zcfwggbjn1rjwblgamyg44cmiqz4i7j42zks1")))

(define-public crate-urbit-q-0.3.2 (c (n "urbit-q") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "naughty-strings") (r "^0.2.3") (d #t) (k 2)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0bg96pcvmlj4hgjzkz4w1wcs2jkq91j18vxa3iwp6xdv7danrhl3")))

