(define-module (crates-io ur bi urbit-ob) #:use-module (crates-io))

(define-public crate-urbit-ob-0.1.0 (c (n "urbit-ob") (v "0.1.0") (d (list (d (n "ibig") (r "^0.3.6") (f (quote ("std" "num-traits"))) (k 0)) (d (n "murmur3") (r "^0.5.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ds5bz0dibzn1hbwdjf8aji71sw0vyzpi4nzwlzp42jh8yad0aia")))

(define-public crate-urbit-ob-0.2.0 (c (n "urbit-ob") (v "0.2.0") (d (list (d (n "ibig") (r "^0.3.6") (f (quote ("std" "num-traits"))) (k 0)) (d (n "murmur3") (r "^0.5.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0h9f0ggjws58l6x4p9p8rr30myjqn7yh5akpbn5jm7m0fhw4i1qa")))

