(define-module (crates-io ur bi urbit) #:use-module (crates-io))

(define-public crate-urbit-0.0.0 (c (n "urbit") (v "0.0.0") (h "1z1zc6v9idn3irqan4axny5yspzzcdpvzvw920pyd7ypg8vaqx7z")))

(define-public crate-urbit-0.1.0 (c (n "urbit") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nock") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1a59ia79ryjm484wprh1h1w08jrppx60s1f3hw152s7abp14m8qi")))

