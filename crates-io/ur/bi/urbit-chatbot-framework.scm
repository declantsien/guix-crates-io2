(define-module (crates-io ur bi urbit-chatbot-framework) #:use-module (crates-io))

(define-public crate-urbit-chatbot-framework-0.1.0 (c (n "urbit-chatbot-framework") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "urbit-http-api") (r "^0.2.4") (d #t) (k 0)))) (h "1yr2nd6zzzqg7zxl3c3s9sd0132pxb48gjll9mi51xiqirscarm0")))

(define-public crate-urbit-chatbot-framework-0.1.1 (c (n "urbit-chatbot-framework") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "urbit-http-api") (r "^0.2.4") (d #t) (k 0)))) (h "0kz8chpam2vz5n2j5zqnw5h1q4jdwa9s79zrl0j2pgpgqs8ngx83")))

(define-public crate-urbit-chatbot-framework-0.1.2 (c (n "urbit-chatbot-framework") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "urbit-http-api") (r "^0.7.0") (d #t) (k 0)))) (h "0qwsrj4i929xv5yisa243vvry3rkgi7n35k7rqh9098viwx6g5xv")))

