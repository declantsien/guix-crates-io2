(define-module (crates-io ur is uris) #:use-module (crates-io))

(define-public crate-uris-0.1.0 (c (n "uris") (v "0.1.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00xjiix7wns0iahrbvkydskhai74a187rw7mgfzvylfiwd242ilp") (y #t)))

(define-public crate-uris-0.1.1 (c (n "uris") (v "0.1.1") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "188bjbsf5wz7ypcqa7dhlyf65lizl8s9walsaizl6hzzr7hniy32")))

