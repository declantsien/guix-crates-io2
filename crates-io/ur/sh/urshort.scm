(define-module (crates-io ur sh urshort) #:use-module (crates-io))

(define-public crate-urshort-0.1.1 (c (n "urshort") (v "0.1.1") (d (list (d (n "axum") (r "^0.5.16") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s3mpn4k0h3cq3xzsakm4zfsp7p5sg0yjwsqzy2vzkrcm8q2hzkl")))

