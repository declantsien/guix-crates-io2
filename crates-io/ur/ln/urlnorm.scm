(define-module (crates-io ur ln urlnorm) #:use-module (crates-io))

(define-public crate-urlnorm-0.1.0 (c (n "urlnorm") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0v64c4v92nw0kdq0nl7hqh815siw7mqmx7h7xdnaw4nka4z19lja") (f (quote (("nightly"))))))

(define-public crate-urlnorm-0.1.1 (c (n "urlnorm") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0dlgc53x3si54y4yiy0ssj2dx0z2zwsvya0swrfq1fkgzjaz5gc2") (f (quote (("nightly"))))))

(define-public crate-urlnorm-0.1.2 (c (n "urlnorm") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1rf4dsvgckfiyrs8qcapd3ap4bz0jx49sj27frszzv5khwirs2m1")))

(define-public crate-urlnorm-0.1.3 (c (n "urlnorm") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1lyf6nljl1zkjzw5afyp8843625qhzddazsrkdjhbm4jjllpwzqw")))

(define-public crate-urlnorm-0.1.4 (c (n "urlnorm") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1q3pv19jav5ia2apkpj5k011d0gk9cyka4r64h831zx3dnx9640d")))

(define-public crate-urlnorm-0.1.5 (c (n "urlnorm") (v "0.1.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "16a9b9vlai5gwn384fq96ywj06l3bc3z3kkfhxqfyp0zjnkzzybw")))

(define-public crate-urlnorm-0.1.6 (c (n "urlnorm") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "07blgy4vjxiwga7qm46mz36aqjhh1jiwx63s5nc8z3yriy2dhkkg")))

