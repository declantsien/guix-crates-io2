(define-module (crates-io ur -s ur-script) #:use-module (crates-io))

(define-public crate-ur-script-0.1.0 (c (n "ur-script") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.4") (k 0)) (d (n "num") (r "^0.4.0") (k 0)))) (h "10af36gkymz39wlx0jiqw379b7fxplwzwxrdkzhh5l5cpd5my5r7") (f (quote (("std" "num/std" "nalgebra/std") ("libm" "num/libm" "nalgebra/libm") ("default" "libm")))) (y #t)))

(define-public crate-ur-script-0.1.1 (c (n "ur-script") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.4") (k 0)) (d (n "num") (r "^0.4.0") (k 0)))) (h "14m8qgcj0w6idpq7vxjlp7lagp8wxy0rcgzbhmbwhbfkf5378q73") (f (quote (("std" "num/std" "nalgebra/std") ("libm" "num/libm" "nalgebra/libm") ("default" "libm"))))))

(define-public crate-ur-script-0.2.0 (c (n "ur-script") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.31.4") (k 0)) (d (n "num") (r "^0.4.0") (k 0)))) (h "00xzb7wbwn87dh9lpzyyxnpd5lk41dirivzpzwcx7ia6ay014nln") (f (quote (("std" "num/std" "nalgebra/std") ("libm" "num/libm" "nalgebra/libm") ("default" "libm"))))))

