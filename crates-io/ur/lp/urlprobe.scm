(define-module (crates-io ur lp urlprobe) #:use-module (crates-io))

(define-public crate-urlprobe-0.0.1 (c (n "urlprobe") (v "0.0.1") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.8") (d #t) (k 0)) (d (n "http") (r ">=0.2.1") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.9") (d #t) (k 0)) (d (n "tokio") (r "=0.2.23") (f (quote ("macros"))) (d #t) (k 0)))) (h "1hniy4lhywriricyx2wi426xp45n48ffxc1qr31x5q7p2hp4jhif")))

(define-public crate-urlprobe-0.2.1 (c (n "urlprobe") (v "0.2.1") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.8") (d #t) (k 0)) (d (n "http") (r ">=0.2.1") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.9") (d #t) (k 0)) (d (n "tokio") (r "=0.2.23") (f (quote ("macros"))) (d #t) (k 0)))) (h "0jrhh9apds07i0xnvdyg9cns75sxwvqjffgxlqs4v6ka8dzmrsyc")))

(define-public crate-urlprobe-0.2.2 (c (n "urlprobe") (v "0.2.2") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.8") (d #t) (k 0)) (d (n "http") (r ">=0.2.1") (d #t) (k 0)) (d (n "reqwest") (r ">=0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0s87fkpspwqn7iz59cgrcdyhn5w7r5x07lmjmdcv6800ap4f8mwa")))

(define-public crate-urlprobe-0.2.3 (c (n "urlprobe") (v "0.2.3") (d (list (d (n "clap") (r ">=2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures") (r ">=0.3.8") (d #t) (k 0)) (d (n "http") (r ">=0.2.1") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r ">=0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0z2hi8vksa2zwr7g3rdwn76a004izi219s7al8v8iax85d7zrs9v")))

(define-public crate-urlprobe-0.3.0 (c (n "urlprobe") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r ">=0.3.8") (d #t) (k 0)) (d (n "http") (r ">=0.2.1") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r ">=0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0bcl2y29aywxb83gm7pwja4dvbwigcgxvrs4apys8y41x9mrg61z")))

(define-public crate-urlprobe-0.4.0 (c (n "urlprobe") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r ">=0.3.8") (d #t) (k 0)) (d (n "http") (r ">=0.2.1") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "reqwest") (r ">=0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0hcq40ix6iprr57bbkf8f054wgf8cn42ac5s891vksasr0c6kki6")))

