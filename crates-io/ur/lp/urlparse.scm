(define-module (crates-io ur lp urlparse) #:use-module (crates-io))

(define-public crate-urlparse-0.6.0 (c (n "urlparse") (v "0.6.0") (h "0k37jdvczgpg3b6kwpalq9pi70g139fjk42wjbddxv9rarjp27wb")))

(define-public crate-urlparse-0.7.0 (c (n "urlparse") (v "0.7.0") (h "0gy69f6h3xsignndwb5yyj6arxb8z8yfhjaal41nagd1pgq05sk2")))

(define-public crate-urlparse-0.7.1 (c (n "urlparse") (v "0.7.1") (h "0y70nhrrbn4iy4z9rshjkgpis7l8dl0dhzflna5jbwpnbipvkkf0")))

(define-public crate-urlparse-0.7.2 (c (n "urlparse") (v "0.7.2") (h "04vjragcsfzfgf5iqs3b7rmxafp6yskvi8pswgr69kbfc7hhp2cy")))

(define-public crate-urlparse-0.7.3 (c (n "urlparse") (v "0.7.3") (h "05w58v7siyiymxsv7q0kxp6lvqh4hs6piiq3j21nfv07x7a540qi")))

