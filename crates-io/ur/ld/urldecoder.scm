(define-module (crates-io ur ld urldecoder) #:use-module (crates-io))

(define-public crate-urldecoder-1.3.2 (c (n "urldecoder") (v "1.3.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "die-exit") (r "^0.5.0") (f (quote ("red"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0p5z9jriq6x085zawy05q99g34zpmx2frv9927jabprgk9ddq8bd")))

