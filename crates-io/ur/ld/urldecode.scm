(define-module (crates-io ur ld urldecode) #:use-module (crates-io))

(define-public crate-urldecode-0.1.0 (c (n "urldecode") (v "0.1.0") (h "0ryyxv44c498ffpk5xpqp0jazis1xchkrxnyqhnqf7gqg7wc8f16")))

(define-public crate-urldecode-0.1.1 (c (n "urldecode") (v "0.1.1") (h "1p4f5kdqk2azgmr10nsx3i2fdnc67fl98pw8ff588s52kfvjw307")))

