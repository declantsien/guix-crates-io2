(define-module (crates-io ur ip uriparser) #:use-module (crates-io))

(define-public crate-uriparser-0.1.0 (c (n "uriparser") (v "0.1.0") (d (list (d (n "uriparser-sys") (r "^0.1") (d #t) (k 0)))) (h "13z4033dxyxcbpnjzxhg4ziqmvxdrcgw1nkbyd2nk37n7c7ywazc")))

(define-public crate-uriparser-0.2.0 (c (n "uriparser") (v "0.2.0") (d (list (d (n "uriparser-sys") (r "^0.1") (d #t) (k 0)))) (h "04r1si5jrcv6bbz4fn6dnnnpsj8daq5jm9f8k79qx98bhf2jh9zf")))

