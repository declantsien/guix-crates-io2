(define-module (crates-io ur ip uriparse) #:use-module (crates-io))

(define-public crate-uriparse-0.1.0 (c (n "uriparse") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "0cz5sl4jhimw7aw2qps5k2kvbg11n6afg38cwf4yjvrzl8m9q6dd")))

(define-public crate-uriparse-0.2.0 (c (n "uriparse") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "1xy4snz28ks8a7yandw1fhw79wda2xga9z35w25kfy67dprmam8p")))

(define-public crate-uriparse-0.2.1 (c (n "uriparse") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "171bbg4gwf3wxwd4f20ssjvp61cg21z3rsm01fx7ak06fr6csrwp")))

(define-public crate-uriparse-0.3.0 (c (n "uriparse") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "1kw2vfybc7d0y5jjkp4as7bczf9sjgpimxsf66cn59iz4yj2nwmn")))

(define-public crate-uriparse-0.3.1 (c (n "uriparse") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "1ziprkhzikwrfqrdl1bbii940yndcp7mf1phrrf9z75kv42znzbl")))

(define-public crate-uriparse-0.3.2 (c (n "uriparse") (v "0.3.2") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "01frxrqqrbhc7lnvrs7pncypwv35qz9qwfnfypyagkbayd39w302")))

(define-public crate-uriparse-0.3.3 (c (n "uriparse") (v "0.3.3") (d (list (d (n "criterion") (r "^0.2.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)))) (h "1c4rnnqqkwcwqz8m6kaxnqa12vsjz4v4mhcq0byzcc0im3zlba1b")))

(define-public crate-uriparse-0.4.0 (c (n "uriparse") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "01a84c7zaj5ywbvbi92j4965nkss5a1jqbr9np7jk6h41iw5vbsi")))

(define-public crate-uriparse-0.5.0 (c (n "uriparse") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0y3yg9xy42f3kpay8hh4gnr9m65bsh0dw1qsxzdb675y9sy765z1")))

(define-public crate-uriparse-0.6.0 (c (n "uriparse") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1v2gzdgis1ihpn14xsq13i7w8s17wv0zb3r109kbw7gxlshw168n")))

(define-public crate-uriparse-0.6.1 (c (n "uriparse") (v "0.6.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0536603drya7gba5zwnvqi9hk30kld6hns356k1i81z4d7g4f0ai")))

(define-public crate-uriparse-0.6.2 (c (n "uriparse") (v "0.6.2") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0sidvmx6cmch8cvzbjc34yf7k6s9ymf226r1ygshbvyc56vqnp9g")))

(define-public crate-uriparse-0.6.3 (c (n "uriparse") (v "0.6.3") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "08bj3dyb3az288nmba014abwy11gqjiznnn5b8a8w5h4ljnv25g5") (f (quote (("default"))))))

(define-public crate-uriparse-0.6.4 (c (n "uriparse") (v "0.6.4") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "1zsjwc715x4720y4b3dsdras50imvaakqgyl59n3j2fq0kyd0002") (f (quote (("default"))))))

