(define-module (crates-io ur ip uriparser-sys) #:use-module (crates-io))

(define-public crate-uriparser-sys-0.1.0 (c (n "uriparser-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0q8ay2q4cjv6nh3x33b94fmrhkanw54swj7wy7by7kbx79jip3ch")))

