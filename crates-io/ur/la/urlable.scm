(define-module (crates-io ur la urlable) #:use-module (crates-io))

(define-public crate-urlable-0.1.0 (c (n "urlable") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1qab5kk91jlyw4v2z48qfcmba5669pgk93kpi77i07nkdfzacw3f")))

(define-public crate-urlable-0.1.1 (c (n "urlable") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1yd0yxz8knhv4mvqqa5klzr4c16sc8k8dh7bm8xifmjbmss9y4ah")))

