(define-module (crates-io ur ba urban-rs) #:use-module (crates-io))

(define-public crate-urban-rs-0.1.0 (c (n "urban-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1hkqr59vydwvl4zqwh2nmpa6zal8g79gsxv50bzyslny9l76s6wd")))

(define-public crate-urban-rs-0.1.1 (c (n "urban-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0164kyl7y3zdpqw29xa2wys8256145fvzlrr28aiqh0s9xdmqvz7")))

