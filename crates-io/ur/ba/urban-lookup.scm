(define-module (crates-io ur ba urban-lookup) #:use-module (crates-io))

(define-public crate-urban-lookup-1.1.0 (c (n "urban-lookup") (v "1.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0z2zvag1v3vp1dvydwlkzrn8pgk7zxzwgigmab7hjy1670kzll35")))

