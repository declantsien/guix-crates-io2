(define-module (crates-io ur ba urbandict) #:use-module (crates-io))

(define-public crate-urbandict-0.1.0 (c (n "urbandict") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1wzxh2mgnvd0a9kzr9mmaf47vcpwnhgiqxkr5g2p2gj9aacgnlvb")))

(define-public crate-urbandict-0.2.0 (c (n "urbandict") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0zz48483m01p6gp2jcnkqnz0jzihcass3whl55bw0v0pbls3p6lv")))

