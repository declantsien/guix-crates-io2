(define-module (crates-io ur ch urchin) #:use-module (crates-io))

(define-public crate-urchin-0.1.0 (c (n "urchin") (v "0.1.0") (h "0487rcpgjxmhp83bikpfg8qz06w9vqgkc8hx544f3hhpqpv42nrw")))

(define-public crate-urchin-0.1.1 (c (n "urchin") (v "0.1.1") (h "0k7r8y46w8cx3cs0lmb86n9mzpw9h7sa0yf52lxgdza67ialh4bn")))

