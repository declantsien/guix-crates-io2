(define-module (crates-io ur #{20}# ur20-modbus) #:use-module (crates-io))

(define-public crate-ur20-modbus-0.1.0 (c (n "ur20-modbus") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.2") (f (quote ("tcp"))) (k 0)) (d (n "ur20") (r "^0.3") (d #t) (k 0)))) (h "0k43xj73pcvinyf17x8813804a98wnwikyqz8m1bnm9q5ipknrca")))

(define-public crate-ur20-modbus-0.1.1 (c (n "ur20-modbus") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.2") (f (quote ("tcp"))) (k 0)) (d (n "ur20") (r "^0.4") (d #t) (k 0)))) (h "0689rgpclr9x9mvaqmfzaa6qxfqpdjqwpp93vra1ivqgd8pkc31d")))

(define-public crate-ur20-modbus-0.1.2 (c (n "ur20-modbus") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.2") (f (quote ("tcp"))) (k 0)) (d (n "ur20") (r "^0.4") (d #t) (k 0)))) (h "0bdcb0jhgc4k63qrq75wcg244c23wqshvg7xwkh2rkhpsbfsjsg9")))

(define-public crate-ur20-modbus-0.2.0 (c (n "ur20-modbus") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.2") (f (quote ("tcp"))) (k 0)) (d (n "ur20") (r "^0.5") (d #t) (k 0)))) (h "0r9jlw3c17s3vwd3m2w58fs1rpd8s3rq9bxdy1iihn5ixnsy81k3")))

(define-public crate-ur20-modbus-0.3.0 (c (n "ur20-modbus") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.3") (f (quote ("tcp"))) (k 0)) (d (n "ur20") (r "^0.5") (d #t) (k 0)))) (h "0pb60xk7zdb3rvvmmam2ih6s9qdc71a7c856vfsnfh4h5jfp1d02")))

(define-public crate-ur20-modbus-0.4.0 (c (n "ur20-modbus") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-modbus") (r "^0.4") (f (quote ("tcp"))) (k 0)) (d (n "ur20") (r "^0.5") (d #t) (k 0)))) (h "1xqrapzkai7dn7h1gf97bqsyhv6vpiq94f7w0n0wiqm93gad336k")))

