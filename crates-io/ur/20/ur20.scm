(define-module (crates-io ur #{20}# ur20) #:use-module (crates-io))

(define-public crate-ur20-0.1.0 (c (n "ur20") (v "0.1.0") (h "07z72dzz9qp267lpchn7x8gyclbgrb5w1dwjqh525qn6hp4j1xz8")))

(define-public crate-ur20-0.1.1 (c (n "ur20") (v "0.1.1") (h "05v6xskd7blnhvmnf8vp3vbnj91lan6wcphkzwhywi42137cqwna")))

(define-public crate-ur20-0.1.2 (c (n "ur20") (v "0.1.2") (h "1z5qvzkgknih7ym8wgxy8hyd2wsfjmcql7r4yx3i9a86cpmydgr4")))

(define-public crate-ur20-0.2.0 (c (n "ur20") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1lajyvj0ws1kxkx1vj3qhxza95hrqqfy0c0jc305zkf14ym8mpva")))

(define-public crate-ur20-0.3.0 (c (n "ur20") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qcnbvcggl4z7j96wsa74zpfvvaqsi5y4l5cqmxzp946xbn9awvq")))

(define-public crate-ur20-0.4.0 (c (n "ur20") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1jlcf1dl3v0hlpxl22j9q3mxdj6iy6gip0in0bfjnazamm60bsjp")))

(define-public crate-ur20-0.4.1 (c (n "ur20") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09qf5gydc6rmi5cpjhnh0v4fqqv21bd4296mwvlp6d1wy7yanms0")))

(define-public crate-ur20-0.4.2 (c (n "ur20") (v "0.4.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0j52k769061ysay9dsrq3snq4ckk2r4mix1ri0w76s5qkfdg5549")))

(define-public crate-ur20-0.5.0 (c (n "ur20") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ngbi2r1hz7hhx1apip24j10qpzfxlc9qk8hhbbyp7hpr3s87avk")))

(define-public crate-ur20-0.5.1 (c (n "ur20") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1dg5l43m639kyx3wp0d6qb6k9xsx9372ra2b48p2cq5rbki3glr0")))

