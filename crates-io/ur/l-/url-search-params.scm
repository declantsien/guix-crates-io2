(define-module (crates-io ur l- url-search-params) #:use-module (crates-io))

(define-public crate-url-search-params-1.0.0 (c (n "url-search-params") (v "1.0.0") (h "1xcik145i8jkr81wi3bj6c2sldacmykxkanpragn3wnr8928ypnl") (y #t) (r "1.56")))

(define-public crate-url-search-params-2.0.0 (c (n "url-search-params") (v "2.0.0") (h "0kp37wni26crfk2bn7zldiydb4r0qpmnd7k7kf0zif5b154pxqbc") (r "1.64")))

(define-public crate-url-search-params-8.0.0 (c (n "url-search-params") (v "8.0.0") (h "0xa37d38blwgfbkpx71mfyq0n2ld4wpxrd8k82f82rplbz7jw7jk") (r "1.65")))

(define-public crate-url-search-params-9.0.0 (c (n "url-search-params") (v "9.0.0") (h "0cpmmcki8p4lg1s055b4mwlqy4ydpzcbgfmz27k0ms951ia90j78") (r "1.66")))

(define-public crate-url-search-params-10.0.0 (c (n "url-search-params") (v "10.0.0") (h "0vc28mpgvi92jvz7jmn8ybxbvz0nzy09s7rl22q7v19yrxky70nk") (r "1.68")))

(define-public crate-url-search-params-11.0.0 (c (n "url-search-params") (v "11.0.0") (h "0chxksmhskyxcnnhrd3lznn3j3fsr07c4365pxvvjb7wxvzcwqx4")))

(define-public crate-url-search-params-12.0.0 (c (n "url-search-params") (v "12.0.0") (h "19mpvcb5bs7pg2szzpijsyy36nk9kagykj85026m9rra08h52jra")))

