(define-module (crates-io ur l- url-parse) #:use-module (crates-io))

(define-public crate-url-parse-0.1.0 (c (n "url-parse") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1ya5vahllyhzqbrnsfxcpkch06dn3dy02jrc96d8wffqamxmsbs6") (y #t)))

(define-public crate-url-parse-0.1.1 (c (n "url-parse") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "078m862dzif5mli30r8xgn5h6gkgjijf0xcgbr04js5asbympxjm") (y #t)))

(define-public crate-url-parse-0.1.2 (c (n "url-parse") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "099bw2h0c93m3875d065k1k3jllr56ry93fsd1mr9q5pcbsn7j5i")))

(define-public crate-url-parse-0.2.0 (c (n "url-parse") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0br56sm545az0njpm5i3k3p1igq3aa4pj1gilvjqn80g1jmhglmd")))

(define-public crate-url-parse-0.2.1 (c (n "url-parse") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0p643nvfqmjb4m9xm7wy6jkx6h0lnglixi198p6wdb39ymn45b0w")))

(define-public crate-url-parse-0.2.2 (c (n "url-parse") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0nnr5a4l0qknw6qcinlwrdfpaqg404snkj4v288d1qkh3hk3i868")))

(define-public crate-url-parse-0.2.3 (c (n "url-parse") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0s6gr7zm93p1md9dl131vbcgfka0yrqg6gj6cg9nlg1y88ijscwi")))

(define-public crate-url-parse-0.3.0 (c (n "url-parse") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1d5hl0hy86pfpj6fw2fi11y8lhmwdg7c7sixn150d1sgfqnpk2vg")))

(define-public crate-url-parse-0.4.0 (c (n "url-parse") (v "0.4.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0h4yp8rr3gczy713sm8zw10xjavzvhvg4ngqhhkkfjx2xzpnvjkk")))

(define-public crate-url-parse-0.4.1 (c (n "url-parse") (v "0.4.1") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "04wm8602ygapa5c0iyyxj8sinmn9s3ly41myk300kjc97mx7vh7d")))

(define-public crate-url-parse-0.5.0 (c (n "url-parse") (v "0.5.0") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1403f6ryic46rw1wsl80z0sm34s6i6r5lr6l5s1qym4m836y38zy")))

(define-public crate-url-parse-0.5.1 (c (n "url-parse") (v "0.5.1") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "11yaiaqdssa7kf8ccbgmhrz7p4gd10m6yrk9jcdj8hqcx31d745q")))

(define-public crate-url-parse-0.5.2 (c (n "url-parse") (v "0.5.2") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "06wyynhwfahfcxq4x3v5fdij29mk460c91xyfiyr0wk0ksdcsali")))

(define-public crate-url-parse-0.5.3 (c (n "url-parse") (v "0.5.3") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1znmn5abbp08ky6cnvg71gmcznww2hmv2camnjcxpknj83adjsya")))

(define-public crate-url-parse-0.5.4 (c (n "url-parse") (v "0.5.4") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0cadc89rzkia3gvvxxjx9jw8xnx1c80sym25pfbzybgb5xi1cw0h")))

(define-public crate-url-parse-0.5.5 (c (n "url-parse") (v "0.5.5") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "06p1hp2mcpnrkf3ymyfg5sy63yqxyks2knimf5xmffwjdh4qnng1")))

(define-public crate-url-parse-0.5.6 (c (n "url-parse") (v "0.5.6") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "0dkxhg313fbr0b0f5sa3bzilwnlrhzw3n7zv2ij6sqlim5hird7s")))

(define-public crate-url-parse-0.5.7 (c (n "url-parse") (v "0.5.7") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0q8qgq01cbqcgwdw0sw9pyqipl982ks0kfa07dzaqwk7lndf5mkd")))

(define-public crate-url-parse-0.5.8 (c (n "url-parse") (v "0.5.8") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0irqsacs7w0m6n43r2jz05r17yyx1i2dli8baaa05dljrc9fyiq8")))

(define-public crate-url-parse-0.5.9 (c (n "url-parse") (v "0.5.9") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1my15bl5a26scp038nh2rdgcy00dg1j35ivppyghjig05kchkq3r")))

(define-public crate-url-parse-0.5.10 (c (n "url-parse") (v "0.5.10") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1yzxpfyfbh328jabyr6rk6m6cy4k0l7wy4f0ndpd2swdcp6bbhfd")))

(define-public crate-url-parse-0.5.11 (c (n "url-parse") (v "0.5.11") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0w099hi8933w5mph0zhqbp3bbgj652q07j6chz400l9g0fik4xx4")))

(define-public crate-url-parse-0.5.12 (c (n "url-parse") (v "0.5.12") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "10ydgxg6gjxqb5vba2sns3hbn378gn4zm0zxasg6pvcywr8fmc07")))

(define-public crate-url-parse-0.5.13 (c (n "url-parse") (v "0.5.13") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "05y3qqkzkcp6asmbarrh2i31sf43skwpgyivbh1g9pcn855cdr5v")))

(define-public crate-url-parse-0.5.14 (c (n "url-parse") (v "0.5.14") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0cyhh253p4qb6xrn955z11zgr7nrq72rn93z02wmzv69pg1rsbqd")))

(define-public crate-url-parse-0.5.15 (c (n "url-parse") (v "0.5.15") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0y2kzgd15nhzg5b24xxplvpzq4jv3acijxh6v8jb5lr9w97gwbp4")))

(define-public crate-url-parse-1.0.0 (c (n "url-parse") (v "1.0.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "149ba7sx6chqhsycizrrh87q8apwys5ldp3yx7h0s2n18yzwbrik")))

(define-public crate-url-parse-1.0.1 (c (n "url-parse") (v "1.0.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "11hwnd740ag1dpcqzwh52dd3m8igij5i7lrp0131i4c2y1lan80l")))

(define-public crate-url-parse-1.0.2 (c (n "url-parse") (v "1.0.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0mvlal18g5v71ly114qs56g7laaamsrs9mka0h0iyfvxlazb31ay") (y #t)))

(define-public crate-url-parse-1.0.3 (c (n "url-parse") (v "1.0.3") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1sars9jrr7ys9c50p65lf3w9rx2746if2vm4xp6xhzp139xzqqxf")))

(define-public crate-url-parse-1.0.4 (c (n "url-parse") (v "1.0.4") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0fqqwhgn7r7qz5jzag7g5121i9lkr7i9w7bv9vqnhf9kr4lld73i")))

(define-public crate-url-parse-1.0.5 (c (n "url-parse") (v "1.0.5") (d (list (d (n "regex") (r "^1.7.1") (f (quote ("std"))) (k 0)))) (h "1hcpzzk1iwf0l56axxrj4q5f3s2idkn8w4f8laarq31iq42ra4ls")))

(define-public crate-url-parse-1.0.6 (c (n "url-parse") (v "1.0.6") (d (list (d (n "regex") (r "^1.8.1") (f (quote ("std"))) (k 0)))) (h "1ch89vd4vdvr977m9amky34xv3wsp1whffsdz0k3vms0qayidcyk")))

(define-public crate-url-parse-1.0.7 (c (n "url-parse") (v "1.0.7") (d (list (d (n "regex") (r "^1.9.1") (f (quote ("std"))) (k 0)))) (h "0qw0b4mdwhyws9xahsbmads1164sdgxnhr7kjwv9pfklc6k5sdqd")))

(define-public crate-url-parse-1.0.8 (c (n "url-parse") (v "1.0.8") (d (list (d (n "regex") (r "^1.10.2") (f (quote ("std"))) (k 0)))) (h "1lmxa1kx0gmbyc3xlf0r32632p52v98yask3h3qk1bjwq5hwwpl6")))

