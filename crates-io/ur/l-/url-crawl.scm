(define-module (crates-io ur l- url-crawl) #:use-module (crates-io))

(define-public crate-url-crawl-0.0.0 (c (n "url-crawl") (v "0.0.0") (h "06g3c9lvrsdpd1wdxazzx2m94ikn6m1f8mjxvkvbpksg92qv6vs6")))

(define-public crate-url-crawl-0.1.0 (c (n "url-crawl") (v "0.1.0") (d (list (d (n "select") (r "^0.5") (d #t) (k 0)))) (h "03izx0rrzibm26jrdybdsrdvl1vn6bfriynkv6alv3pwlvkhj5ak") (r "1.56")))

(define-public crate-url-crawl-0.2.0 (c (n "url-crawl") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "08x0ahswd9w9d9bgni6cgjnz6bv2ga6ar4mdg1a33n3xc84y41d3") (r "1.56")))

