(define-module (crates-io ur l- url-prefix) #:use-module (crates-io))

(define-public crate-url-prefix-1.0.0 (c (n "url-prefix") (v "1.0.0") (d (list (d (n "validators") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0jb6wli7gqamyw92rnyf4zd9rmn6g520pf8ihjrjcjahj1k6qsvc") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.0.1 (c (n "url-prefix") (v "1.0.1") (d (list (d (n "validators") (r "^0.9") (o #t) (d #t) (k 0)))) (h "076ssjh4x2ihjlw1mix3jxd297vqkl6rkv8i6h5mqd08g1xrvn3w") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.0.2 (c (n "url-prefix") (v "1.0.2") (d (list (d (n "validators") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1mwf4bbxgn8ymzn0m2h627v89n8xxg6a3ikkcv1jjy66cpmkbah2") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.0.3 (c (n "url-prefix") (v "1.0.3") (d (list (d (n "validators") (r "^0.9") (o #t) (d #t) (k 0)))) (h "05ik3l31va6ri630ffzd07ck2gvhr37vhya85afacrgc9dsqb69c") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.0.4 (c (n "url-prefix") (v "1.0.4") (d (list (d (n "validators") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1l1vjrggjf5ka9r2lb1r5j382rkh9fb9zmp4d0w7lricdnappkd1") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.0.5 (c (n "url-prefix") (v "1.0.5") (d (list (d (n "validators") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0ag811b88103b2bmaysp2aby2cvfw8jggiqcj3dvnnl9c81pi5dj") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.0.6 (c (n "url-prefix") (v "1.0.6") (d (list (d (n "validators") (r "^0.13") (o #t) (d #t) (k 0)))) (h "1f3kaddlspcsyic5kw32iz3x3d2yqxkhp2xc7gnnw9xv6k2ihr2v") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.1.0 (c (n "url-prefix") (v "1.1.0") (d (list (d (n "validators") (r "^0.13") (o #t) (d #t) (k 0)))) (h "02bq1wq9vg85771c0gnzm8l1x6yh3q2581n0r0xvg8hkihpa5mn0") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.1.1 (c (n "url-prefix") (v "1.1.1") (d (list (d (n "validators") (r "^0.15.1") (o #t) (d #t) (k 0)))) (h "16agbm3xj6rmga2k05xn0z53zmz2sdny20m4k9x9hyvnm30k2bv0") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.2.0 (c (n "url-prefix") (v "1.2.0") (d (list (d (n "validators") (r "^0.16") (o #t) (d #t) (k 0)))) (h "17j2adxacrvs7zxb265m3jxjk4j3zg37mwil551g0sidlsv5fmns") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.2.1 (c (n "url-prefix") (v "1.2.1") (d (list (d (n "validators") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1q0xr7vzsjvw587415niq0i6x09z2dbygyb2i5m5gnaw4nm98gng") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.2.2 (c (n "url-prefix") (v "1.2.2") (d (list (d (n "validators") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0czcxs740rnlyiqxb4cmfx1yxp59677lpz9lzcyx6x33s1l8fm32") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.2.3 (c (n "url-prefix") (v "1.2.3") (d (list (d (n "validators") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1rw32svh6am7n26kfavmk40m7i2q7qsybkp4cm8y3wrh4f5i3nzg") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.3.0 (c (n "url-prefix") (v "1.3.0") (d (list (d (n "validators") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1lg0imm6j3yw6yr935achzd79zq405z6j8kp38mswhi3d6lzc6kj") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.3.1 (c (n "url-prefix") (v "1.3.1") (d (list (d (n "validators") (r ">= 0.18, < 0.20") (o #t) (d #t) (k 0)))) (h "1fahcrcgls3z2w6pw0lpgdlghvyk5c3b5lni6ja6pnhm81c9p321") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.3.2 (c (n "url-prefix") (v "1.3.2") (d (list (d (n "validators") (r ">= 0.18, < 0.21") (o #t) (d #t) (k 0)))) (h "0gxd7yrdkln54r0i15fpq6ah240q4zmcaagwbg4zn28979wzkd7n") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.3.3 (c (n "url-prefix") (v "1.3.3") (d (list (d (n "validators") (r ">= 0.18, < 0.21") (o #t) (d #t) (k 0)))) (h "1dmwj2p0nyjfkgigy2c4f4d97gpaqz4bnyafirq7rw3dvwp704y2") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.3.4 (c (n "url-prefix") (v "1.3.4") (d (list (d (n "validators") (r ">= 0.18, < 0.21") (o #t) (d #t) (k 0)))) (h "0493pm6r3l0rmhj90anlrw82himjj5mjnd39xdir6734r2ra9rg6") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-1.3.5 (c (n "url-prefix") (v "1.3.5") (d (list (d (n "validators") (r ">=0.18, <0.21") (o #t) (d #t) (k 0)))) (h "0r8zcycznmavkhxg6w6g89waa6ir1d2qxyvb2k4c7lzs4y1ls9bb") (f (quote (("validator" "validators"))))))

(define-public crate-url-prefix-2.0.0 (c (n "url-prefix") (v "2.0.0") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 0)))) (h "0k9g72wp3wjapvwaf7j6032czyw949973ykfxhx8ss5y8w6hmn1l")))

(define-public crate-url-prefix-2.0.1 (c (n "url-prefix") (v "2.0.1") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 0)))) (h "0r4amdw1lir77nr6ysbv3p0bgaijlsxyj9gnpbzpnv9dnq5w52v4")))

(define-public crate-url-prefix-2.0.2 (c (n "url-prefix") (v "2.0.2") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 0)))) (h "0g449p31gwxg9grzn0zxz8vwrqx9ad5321xwxkarqba5bvwr3inz")))

(define-public crate-url-prefix-2.0.3 (c (n "url-prefix") (v "2.0.3") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 0)))) (h "0qbf5sp3i090h35zwnhg7kp5cd93j8avr6227qcpd8fqp9cgaa34")))

(define-public crate-url-prefix-2.0.4 (c (n "url-prefix") (v "2.0.4") (d (list (d (n "cow-utils") (r "^0.1") (d #t) (k 0)) (d (n "slash-formatter") (r "^3") (d #t) (k 0)))) (h "017iylnpkhgvz25rfd3ipb2r8780izm1apk831qk6wgwz53chbsk")))

