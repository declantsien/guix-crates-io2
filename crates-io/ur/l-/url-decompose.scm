(define-module (crates-io ur l- url-decompose) #:use-module (crates-io))

(define-public crate-url-decompose-0.1.0 (c (n "url-decompose") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.3") (d #t) (k 0)))) (h "0n7qn6rvfzhmi388pb77b3d2xzldvgii99q4wvyvldz5qbpv4k4d")))

(define-public crate-url-decompose-0.1.1 (c (n "url-decompose") (v "0.1.1") (d (list (d (n "const_format") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.3") (d #t) (k 0)))) (h "0qqm5qw755npl9lr3ynp0mp7xl9v1vhhyhwwc0xzzcq9x439msvd")))

