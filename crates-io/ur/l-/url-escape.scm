(define-module (crates-io ur l- url-escape) #:use-module (crates-io))

(define-public crate-url-escape-0.1.0 (c (n "url-escape") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "073p6miz0xf4lchllhv1hrvqdd877kw8l62kvf6i3grf2p3bvism")))

(define-public crate-url-escape-0.1.1 (c (n "url-escape") (v "0.1.1") (d (list (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "065j1namwrh56av6c8wfs1a60yp86cfv9i5ybb57bl26296wxq24")))

