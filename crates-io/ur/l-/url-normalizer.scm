(define-module (crates-io ur l- url-normalizer) #:use-module (crates-io))

(define-public crate-url-normalizer-0.1.0 (c (n "url-normalizer") (v "0.1.0") (d (list (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1658fr2jf5wp5g8zw398dalbm9kl9apfxg5zpdpgzpzq27dm2qjm")))

(define-public crate-url-normalizer-0.1.1 (c (n "url-normalizer") (v "0.1.1") (d (list (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0pwbnfhk2b4m5vaxpnnmkp14b6y72pcwd9n5kwqc5gxk3ngsflm5")))

(define-public crate-url-normalizer-0.2.0 (c (n "url-normalizer") (v "0.2.0") (d (list (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "007hgvd30rb0zxb8vfgbpnq2zp5sqvsywa1apj5534acfw172328")))

