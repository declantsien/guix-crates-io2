(define-module (crates-io ur l- url-utils) #:use-module (crates-io))

(define-public crate-url-utils-0.1.0 (c (n "url-utils") (v "0.1.0") (h "0rifs223h6p5x9iqr2gpplr2wssr05c7xbd3qifcb7r3p4snj55b")))

(define-public crate-url-utils-0.1.1 (c (n "url-utils") (v "0.1.1") (h "0j2kl74mdnxxyy162zn6cab34nbwsmfgvz1c0gbxbv4qgdf7gx5b")))

(define-public crate-url-utils-0.1.2 (c (n "url-utils") (v "0.1.2") (h "0cj62az0d4yyw38p4f8siyhyfbq3f4bvmk3jkliirifrv8zf5k13")))

