(define-module (crates-io ur l- url-build-parse) #:use-module (crates-io))

(define-public crate-url-build-parse-1.0.0 (c (n "url-build-parse") (v "1.0.0") (d (list (d (n "url-search-params") (r "^1.0.0") (d #t) (k 0)))) (h "1i7q084myd9jdvdng10fx5n118pz5na7az5ixdzp2i7qd18fh5nb") (y #t) (r "1.56")))

(define-public crate-url-build-parse-2.0.0 (c (n "url-build-parse") (v "2.0.0") (d (list (d (n "url-search-params") (r "^2.0.0") (d #t) (k 0)))) (h "0dyaxgb1k0vilqs227jq21brnk2vpqadv4z3h1g3q2d1dk2vynw6") (r "1.64")))

(define-public crate-url-build-parse-8.0.0 (c (n "url-build-parse") (v "8.0.0") (d (list (d (n "url-search-params") (r "^8.0.0") (d #t) (k 0)))) (h "07kwqaqvf2zks4igvl19xp6p66s7p2dja3x4b2i38bjgrnxn3x7k") (r "1.65")))

(define-public crate-url-build-parse-9.0.0 (c (n "url-build-parse") (v "9.0.0") (d (list (d (n "url-search-params") (r "^9.0.0") (d #t) (k 0)))) (h "0xi7z9zqpkd90m6y26m8xxyj7lf695wzwblivwjp4m96pfyfnka3") (r "1.66")))

(define-public crate-url-build-parse-10.0.0 (c (n "url-build-parse") (v "10.0.0") (d (list (d (n "url-search-params") (r "^10.0.0") (d #t) (k 0)))) (h "095if9wbr6mavzfrqyygarjb2320z88d6ih1vppvzscldj8piyjj") (r "1.68")))

(define-public crate-url-build-parse-11.0.0 (c (n "url-build-parse") (v "11.0.0") (d (list (d (n "url-search-params") (r "^12.0.0") (d #t) (k 0)))) (h "0kaanqs748kmls4lprvjpxah7675p461bh3n0k4ryq0ycmv93d30")))

(define-public crate-url-build-parse-12.0.0 (c (n "url-build-parse") (v "12.0.0") (d (list (d (n "url-search-params") (r "^12.0.0") (d #t) (k 0)))) (h "0p55n5k99ckhf565p024c9i3940mpbh4nm124ijnfrma9ph8344a")))

(define-public crate-url-build-parse-12.1.0 (c (n "url-build-parse") (v "12.1.0") (d (list (d (n "url-search-params") (r "^12.0.0") (d #t) (k 0)))) (h "05pvns4dm1nrj45q2s0vmn4rn1cwy16ygpsp1haig25aqrjgngnh")))

