(define-module (crates-io ur l- url-status-checker) #:use-module (crates-io))

(define-public crate-url-status-checker-0.1.0 (c (n "url-status-checker") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18kfv06b4sld1icj0r9ra0yypmls2qwdizzibbjahgnbpdpyvli5")))

