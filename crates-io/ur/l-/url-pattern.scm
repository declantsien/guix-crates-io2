(define-module (crates-io ur l- url-pattern) #:use-module (crates-io))

(define-public crate-url-pattern-0.1.0 (c (n "url-pattern") (v "0.1.0") (h "0vwdypgkdzrkk83zbdanqqcdg0af71acc1h8bd2kgqv3sdv1s0nz")))

(define-public crate-url-pattern-0.1.1 (c (n "url-pattern") (v "0.1.1") (h "0prmgc5phv0gpm8n37g5hd9q6saahglc0bil3s67cdbvfjys4p9m")))

