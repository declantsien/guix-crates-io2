(define-module (crates-io ur l- url-matcher) #:use-module (crates-io))

(define-public crate-url-matcher-0.1.0 (c (n "url-matcher") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "recap") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nb4xrfsm8c60f4g0ps0yi52qykzcaq86a0xs73s1ngpwfs1a5yg")))

