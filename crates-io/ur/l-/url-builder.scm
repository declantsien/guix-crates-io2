(define-module (crates-io ur l- url-builder) #:use-module (crates-io))

(define-public crate-url-builder-0.1.0 (c (n "url-builder") (v "0.1.0") (h "1ii607ia5s4lvp8di5xd80mvrmx2rikxkign9sw42ml9ygazwsqh")))

(define-public crate-url-builder-0.1.1 (c (n "url-builder") (v "0.1.1") (h "15714cql4yfmi85qjrqwd6bagi8ijcmbh5f5h0vcadrr2qchsmkc")))

