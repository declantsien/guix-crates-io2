(define-module (crates-io ur l- url-match) #:use-module (crates-io))

(define-public crate-url-match-0.1.0 (c (n "url-match") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0h739if4hdzhnxdb4krrbhgxlr9k238616gj9vpr3f0zdf4xsh1d")))

(define-public crate-url-match-0.1.1 (c (n "url-match") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0hpkrb6kfqnw07hmma1fiq3j1wgz8q7lil9h47c5936xkwsp6sn2")))

(define-public crate-url-match-0.1.2 (c (n "url-match") (v "0.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1n89v96cdxc26ydn9xzcvp9wnhd1xdhs5rfrb2q0q36k77qp9jfv")))

(define-public crate-url-match-0.1.3 (c (n "url-match") (v "0.1.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0s89rk64i4kga8jyqzcqdadnll8adfm6899dafyfc4rclxyr1fps")))

(define-public crate-url-match-0.1.4 (c (n "url-match") (v "0.1.4") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1zgdqj0l7yz1f7bsfvcw1slm95xif1n9z3gm61a5k4zs9icv076x")))

(define-public crate-url-match-0.1.5 (c (n "url-match") (v "0.1.5") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ji3ccc7bkxjf6hjnw1inxnnv0k2fzxl30hcfhp32m10l2bzy24x")))

(define-public crate-url-match-0.1.6 (c (n "url-match") (v "0.1.6") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "09m6pi7ijnw3hkgl9xy137hnf194in8w27400bniiaxaci5hkllg")))

(define-public crate-url-match-0.1.7 (c (n "url-match") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ch7jzq9vf1mkd1hy9g3q19g3cxim6d1adwpvn2r7prb320bqsqw")))

