(define-module (crates-io ur l- url-hash) #:use-module (crates-io))

(define-public crate-url-hash-0.1.0 (c (n "url-hash") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ring") (r "^0.17.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "007f5i4cf3hlmqakz3gx55ingd5vds1rvjq82x6cm1n64dbpk3wj")))

