(define-module (crates-io ur l- url-scraper) #:use-module (crates-io))

(define-public crate-url-scraper-0.1.0 (c (n "url-scraper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "scraper") (r "^0.7.0") (d #t) (k 0)))) (h "1pg8qz7l6mc4mr4j249j4hd8dmg93n946nvgs72k1f1adx58p13g")))

(define-public crate-url-scraper-0.1.1 (c (n "url-scraper") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "190vbdlr53rfamhjbbwbsrma4kpicq5al0v3368yf1wbx4hrh5s8")))

(define-public crate-url-scraper-0.1.2 (c (n "url-scraper") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)))) (h "0lr1vpcczsh91045qf3r0fih630bf643dvdf9gzkdy0rxr5v59fb")))

