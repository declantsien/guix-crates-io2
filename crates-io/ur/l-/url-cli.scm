(define-module (crates-io ur l- url-cli) #:use-module (crates-io))

(define-public crate-url-cli-0.2.0 (c (n "url-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "rustls-tls"))) (k 0)))) (h "1pp5x4zrc3xn13qc5k03qbyv9219psky00i63i01fx8gk3hp04yq")))

