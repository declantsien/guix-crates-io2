(define-module (crates-io ur l- url-crawler) #:use-module (crates-io))

(define-public crate-url-crawler-0.1.0 (c (n "url-crawler") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "url-scraper") (r "^0.1.0") (d #t) (k 0)))) (h "0sp1q78w52dkgzyr7iqsdamcm1pz8xqmn4d97y9sih881afmlwag") (y #t)))

(define-public crate-url-crawler-0.1.1 (c (n "url-crawler") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "url-scraper") (r "^0.1.0") (d #t) (k 0)))) (h "19bqh6dsam27xaxlril02szkyggxprz1c4nli3azaj52l2dk6car") (y #t)))

(define-public crate-url-crawler-0.2.0 (c (n "url-crawler") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "url-scraper") (r "^0.1.0") (d #t) (k 0)))) (h "0arqs8l1c9qjycibvcbwbv611wl2m1ahlcznnl04jz91jiz57664") (y #t)))

(define-public crate-url-crawler-0.2.1 (c (n "url-crawler") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "url-scraper") (r "^0.1.0") (d #t) (k 0)))) (h "0icvphsqdrmqn39dck11n6vxiz71vki9144wc1kjh1xd77lvp8s7") (y #t)))

(define-public crate-url-crawler-0.3.0 (c (n "url-crawler") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "url-scraper") (r "^0.1.0") (d #t) (k 0)))) (h "1p0wahrz3x4l3wlqj4z24vxwfvgzr5nm0lfc0fiqcl0d6hkr0ag9") (y #t)))

(define-public crate-url-crawler-0.3.1 (c (n "url-crawler") (v "0.3.1") (h "1j9g4rjs3r293csigra6rr2pvadv3nkwp0hchj15j9imz3dwcmdh")))

