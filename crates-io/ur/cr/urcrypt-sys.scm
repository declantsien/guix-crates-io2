(define-module (crates-io ur cr urcrypt-sys) #:use-module (crates-io))

(define-public crate-urcrypt-sys-0.1.0 (c (n "urcrypt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "099x4wqhpp2ifqd5wn2np8mn5a7gx1z9x6w7ia4lkh7rqlc64q3d")))

(define-public crate-urcrypt-sys-0.1.1 (c (n "urcrypt-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "069h8w11a439axgmd9ysbrc42g89wx55xiv7xdc38yjjbbwm3myf")))

