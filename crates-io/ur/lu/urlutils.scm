(define-module (crates-io ur lu urlutils) #:use-module (crates-io))

(define-public crate-urlutils-3.3.3 (c (n "urlutils") (v "3.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "osutils") (r ">=0.1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "130svj6gwf0dmilb9c8b2rzkl8sn66djb5g6n1xhap1s9miwr2s7")))

