(define-module (crates-io ur l_ url_serde) #:use-module (crates-io))

(define-public crate-url_serde-0.1.0 (c (n "url_serde") (v "0.1.0") (d (list (d (n "serde") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0") (d #t) (k 2)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "0y20qdkw9zs7l1ymk7i7c1kgrzx4z6pvyixfv4bx924h3y1hlw6m")))

(define-public crate-url_serde-0.1.1 (c (n "url_serde") (v "0.1.1") (d (list (d (n "serde") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.0") (d #t) (k 2)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "0c72jl9g848aa050islq1lz2cpv0hzpkyfsypydsnvxhxqgh92cc")))

(define-public crate-url_serde-0.1.2 (c (n "url_serde") (v "0.1.2") (d (list (d (n "serde") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0") (d #t) (k 2)) (d (n "serde_json") (r "^0.9.0") (d #t) (k 2)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "1fch1wnhp7bvwixlggc43i1pb2zdsaq5radsv6ab74hnqndgr8k8")))

(define-public crate-url_serde-0.1.3 (c (n "url_serde") (v "0.1.3") (d (list (d (n "serde") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.0") (d #t) (k 2)) (d (n "serde_json") (r "^0.9.0") (d #t) (k 2)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "01pimxrwwfwya45qw785nz2r5sqakq938s8ng60pfc5fcw5brpb4")))

(define-public crate-url_serde-0.2.0 (c (n "url_serde") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^1.0.0") (d #t) (k 0)))) (h "1snxgdzlcj5mpnbkpnzm533l6830qf9hrmmxshizhlpfy6cx1rvl")))

