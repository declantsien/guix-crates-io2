(define-module (crates-io ur l_ url_parser_on_rust) #:use-module (crates-io))

(define-public crate-url_parser_on_rust-1.0.0 (c (n "url_parser_on_rust") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)))) (h "00r6r9ig614x6l57kvg0n55kc6h0a5rjy5jq7idd6qs3ijd03yw4")))

