(define-module (crates-io ur l_ url_encoded_data) #:use-module (crates-io))

(define-public crate-url_encoded_data-0.5.0 (c (n "url_encoded_data") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "19qdnsqcris01v8wg54x87fx82pj4y187269nwfnvvz5fap45v32")))

(define-public crate-url_encoded_data-0.6.0 (c (n "url_encoded_data") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1sqp266xv9hkjbb01s1vvz6hc5y3wc1i83f0rif4akl5wgyvzzgs")))

(define-public crate-url_encoded_data-0.6.1 (c (n "url_encoded_data") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "0wjrl0p0x0sa64m91sfwn3arvanmcsmvbkw5nk1y48kvclsxy7sy")))

