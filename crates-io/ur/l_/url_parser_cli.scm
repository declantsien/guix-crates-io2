(define-module (crates-io ur l_ url_parser_cli) #:use-module (crates-io))

(define-public crate-url_parser_cli-4.0.0 (c (n "url_parser_cli") (v "4.0.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0rx51qy0sxc85ikjzc8xprcn4kicyfdk4z2inygk7g60bddpsh9g")))

