(define-module (crates-io ur l_ url_validator) #:use-module (crates-io))

(define-public crate-url_validator-0.1.0 (c (n "url_validator") (v "0.1.0") (h "1xrsngc8234f7m68jrglwqyiqfdsbdjs84l4fys7pgcdqsj2qi5y") (y #t)))

(define-public crate-url_validator-0.1.1 (c (n "url_validator") (v "0.1.1") (h "11qj5xirqdalmf5w9rk0mr65zrx82pj88zaccixhykx03jm95q94") (y #t)))

(define-public crate-url_validator-0.1.2 (c (n "url_validator") (v "0.1.2") (h "0isiryp804xyg0sakznkzwiqafrniff9rb1fr7d447g6wd7k5yw0") (y #t)))

(define-public crate-url_validator-0.1.3 (c (n "url_validator") (v "0.1.3") (h "0r3vqrxwifvbrs5a6zfymb0d2yh96ihy4zfihbzmjnm781ln9ixp") (y #t)))

(define-public crate-url_validator-0.1.3-2 (c (n "url_validator") (v "0.1.3-2") (h "1bs0mjz6yr3z02rsqxpcvlkjdlycsmgsppsl483s3d56aiy550n2") (y #t)))

(define-public crate-url_validator-0.1.4 (c (n "url_validator") (v "0.1.4") (h "0vrzs1k64z7z0lnmq62a98i2kmfdq94b5bamibdwcdhrk9sylphh") (y #t)))

