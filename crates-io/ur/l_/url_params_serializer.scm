(define-module (crates-io ur l_ url_params_serializer) #:use-module (crates-io))

(define-public crate-url_params_serializer-0.1.0 (c (n "url_params_serializer") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "url") (r "^1.7.0") (d #t) (k 2)))) (h "0g97j0z5rzcb675gf8w03xqqj601jpn69s90zgvzh1fck0k7narr")))

(define-public crate-url_params_serializer-0.1.1 (c (n "url_params_serializer") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 2)) (d (n "url") (r "^1.7.0") (d #t) (k 2)))) (h "0xdc5iyap04nm0015c8hrglf65jswzs7swx6i8kkg4znhmnpd8v4")))

