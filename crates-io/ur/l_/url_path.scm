(define-module (crates-io ur l_ url_path) #:use-module (crates-io))

(define-public crate-url_path-0.1.0 (c (n "url_path") (v "0.1.0") (h "14xwbn04f5zh1vm8m783kp55j964zrm4s1y5g38469iskmlz5vmp")))

(define-public crate-url_path-0.1.1 (c (n "url_path") (v "0.1.1") (h "1kf8aqz65hw1y5j0wszrxac1jfxp6cf6k4w434dwxrfk0ymzp14z")))

(define-public crate-url_path-0.1.2 (c (n "url_path") (v "0.1.2") (h "10ni2a6nsf8nnfnxxn41rli393f0d0fg38zs6i2ha6hsghqwzjar")))

(define-public crate-url_path-0.1.3 (c (n "url_path") (v "0.1.3") (h "10n8s6gj7h4wxhzl3m8k3dbdx4b1zpf6myjlzfs3pz5pg647liqh")))

