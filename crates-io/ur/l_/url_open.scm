(define-module (crates-io ur l_ url_open) #:use-module (crates-io))

(define-public crate-url_open-0.0.1 (c (n "url_open") (v "0.0.1") (d (list (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y6wwcbgixiqfyq81lrnfvp8viachp6sa5ba9v395rinbrvql2xf")))

(define-public crate-url_open-0.0.2 (c (n "url_open") (v "0.0.2") (d (list (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("shellapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0v5dyaplzhawr6snsrxw4j32sv7whi09hqxai64is0hw5x3v1idq")))

