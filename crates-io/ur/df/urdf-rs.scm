(define-module (crates-io ur df urdf-rs) #:use-module (crates-io))

(define-public crate-urdf-rs-0.1.0 (c (n "urdf-rs") (v "0.1.0") (d (list (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "1wrnjdilihf8ir4aj02sm01sx0vidp64f8qq9aphara102caqfnp")))

(define-public crate-urdf-rs-0.1.1 (c (n "urdf-rs") (v "0.1.1") (d (list (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "19f7v1xdd95yq1fwp7h00kds35d6x0si5smp5gq2g9yxf1q5rhb6")))

(define-public crate-urdf-rs-0.1.2 (c (n "urdf-rs") (v "0.1.2") (d (list (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "10kps2g50hbbcjqlyw1i24wrpcy19fiq9q457vp5wnhxlyw10gv0")))

(define-public crate-urdf-rs-0.1.3 (c (n "urdf-rs") (v "0.1.3") (d (list (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "0x9swlzxmca7kmnxwbvddvkhfrrh8qw1rslkj187ipnwz0xvpqix")))

(define-public crate-urdf-rs-0.1.4 (c (n "urdf-rs") (v "0.1.4") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "1drg4cwb6rrwz25lxsxwr9g75cyk2nkr4c416vs7gzfjg838balv")))

(define-public crate-urdf-rs-0.1.5 (c (n "urdf-rs") (v "0.1.5") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "1qnzl9w6zzk1db5rc8gjl4qn1ik33r6zwzxw6r6a87f1d8nyr44f")))

(define-public crate-urdf-rs-0.1.6 (c (n "urdf-rs") (v "0.1.6") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "12i6pqgxci0rawcq2d0r4pigx9iapyfsgvlbha90iijxf4g0435j")))

(define-public crate-urdf-rs-0.2.0 (c (n "urdf-rs") (v "0.2.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "1h2hvimj2jdcf4acd6l288i7jq8rwqc9kkipwhywciirl69jbp2k")))

(define-public crate-urdf-rs-0.2.1 (c (n "urdf-rs") (v "0.2.1") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "1c1kkb7a2vx4iinqz3m3258bgzhm3isi0x7y1q2bbqpinfbphq52")))

(define-public crate-urdf-rs-0.3.0 (c (n "urdf-rs") (v "0.3.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "16j8r9rw848zfy5vgx511xf7vb8x3zpnc7h569v2fc0icy4hjml7")))

(define-public crate-urdf-rs-0.3.1 (c (n "urdf-rs") (v "0.3.1") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "0bsyfc2dwpm8m5q8s1mhp9f3nf9rldfcwzrwm088apfqny833hfw")))

(define-public crate-urdf-rs-0.3.2 (c (n "urdf-rs") (v "0.3.2") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "193n1wavxrlkm63f65s1wkw3r7nc4ijk5mr5y65mzvqrwlxyj69s")))

(define-public crate-urdf-rs-0.4.0 (c (n "urdf-rs") (v "0.4.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "16rgnkrxysijdqamn7cbm85db6k3zmhd9zl7wir46k8r8nz14hv8")))

(define-public crate-urdf-rs-0.4.1 (c (n "urdf-rs") (v "0.4.1") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "1vkv9p92vshikbnxizwc57k9xy4dji9shb8r0fsschy2misbnfdn")))

(define-public crate-urdf-rs-0.4.2 (c (n "urdf-rs") (v "0.4.2") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)))) (h "173g90c1r25d7mixhyzdnl8r46kyb7c963b8v22c3baqmpy40mjn")))

(define-public crate-urdf-rs-0.5.0 (c (n "urdf-rs") (v "0.5.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)))) (h "1qcbn1ypsd8dlcp1h5znv9pkmm6545ia8yb0zbwjd4d1z2dymjrr")))

(define-public crate-urdf-rs-0.6.0 (c (n "urdf-rs") (v "0.6.0") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "1q00lfhr8izzz0igpn2s8ahnfap2w4q24rpmjpdwx9yglj150m94")))

(define-public crate-urdf-rs-0.6.1 (c (n "urdf-rs") (v "0.6.1") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "09zjlx26h58bvnmw31lfpsi64ibags785pdz4kg1y8knjz0139w1")))

(define-public crate-urdf-rs-0.6.2 (c (n "urdf-rs") (v "0.6.2") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "0i4y2iiciab03flarv9mv1lwvsxjmkj7sibi1pkzak7vng9358c0")))

(define-public crate-urdf-rs-0.6.3 (c (n "urdf-rs") (v "0.6.3") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "1mra4qk0shgxkvvars5piknbyplg02bivppjjxg423yc8l4phy5a")))

(define-public crate-urdf-rs-0.6.4 (c (n "urdf-rs") (v "0.6.4") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "02hkkxy6azs5hhk8phnxzcv9nx07hv31iay6vxyzjswq03vx0zpi")))

(define-public crate-urdf-rs-0.6.5 (c (n "urdf-rs") (v "0.6.5") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "1m0xax8nsl3rja7va4y164dq8psg7lb4xp707lmikvxj2hb06b7l")))

(define-public crate-urdf-rs-0.6.6 (c (n "urdf-rs") (v "0.6.6") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "1091nnpz9xl62vpq7wsjxw6y8n0zpgd0j01mpgxf0rb5s81qpkxz")))

(define-public crate-urdf-rs-0.6.7 (c (n "urdf-rs") (v "0.6.7") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "0avnkr8h39jw855am6nxxdrfz3yxd0088qm1ya42xs3nks4kmvlc")))

(define-public crate-urdf-rs-0.6.8 (c (n "urdf-rs") (v "0.6.8") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "0a4nsi7avj5h197wdavnxwiqvy4j48z0w8aqb5n60ykjmisq99nw")))

(define-public crate-urdf-rs-0.6.9 (c (n "urdf-rs") (v "0.6.9") (d (list (d (n "RustyXML") (r "^0.3.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)))) (h "0bicmiakf2m777xj1xia6j409326zap4zy6imszsmy10i1x7y065")))

(define-public crate-urdf-rs-0.7.0 (c (n "urdf-rs") (v "0.7.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)) (d (n "yaserde") (r "^0.7.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.0") (d #t) (k 0)))) (h "0vdz9kvwcwijlzsywddba8p1bj4ax7vll9qlwycsh03lrx63lkvv")))

(define-public crate-urdf-rs-0.7.1 (c (n "urdf-rs") (v "0.7.1") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)) (d (n "yaserde") (r "^0.7.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.0") (d #t) (k 0)))) (h "0ay0l19hyzhw6yw0p8pxsgkr7kapnbqara5n6039s2mfc86clpgs")))

(define-public crate-urdf-rs-0.7.2 (c (n "urdf-rs") (v "0.7.2") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)) (d (n "yaserde") (r "^0.7.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.0") (d #t) (k 0)))) (h "0r0d46xxb3pgd6625bdkfqypmffpxs2b41da6sbfsw2kvs584ag0")))

(define-public crate-urdf-rs-0.7.3 (c (n "urdf-rs") (v "0.7.3") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)) (d (n "yaserde") (r "^0.7.0") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.7.0") (d #t) (k 0)))) (h "1zf70v956fnlm8jf3cmjnjglz25i302kqjq43ckdj1hzg881drwq")))

(define-public crate-urdf-rs-0.8.0 (c (n "urdf-rs") (v "0.8.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)) (d (n "yaserde") (r "^0.8") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.8") (d #t) (k 0)))) (h "1mqrsc7lgnsdjam735si9hj1w3jgdwlby94rpcn4scyii9y80ssv")))

