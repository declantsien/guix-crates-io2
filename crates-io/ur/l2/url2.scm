(define-module (crates-io ur l2 url2) #:use-module (crates-io))

(define-public crate-url2-0.0.1 (c (n "url2") (v "0.0.1") (d (list (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0c37x9i3i4vg0pxpmljmsb2hfpwf4jck9f45y7w36ybzk9kmlh4b")))

(define-public crate-url2-0.0.2 (c (n "url2") (v "0.0.2") (d (list (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1610mmwfrg9y2fwsgipfq3jgggnb35hlwq9pmvhzr036gljmfglh")))

(define-public crate-url2-0.0.3 (c (n "url2") (v "0.0.3") (d (list (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0nh26fqxnwsgcraq41ahqf7qxbmigxn3sflidsyixwi5yzcznkbr")))

(define-public crate-url2-0.0.4 (c (n "url2") (v "0.0.4") (d (list (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "17dh824k09q9yxmlmjk0lxwjac5x6z7z11cszw9znqqbc0z2pqdi")))

(define-public crate-url2-0.0.5 (c (n "url2") (v "0.0.5") (d (list (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1my664a6byyxy3sjg0kz1wv7g0c7f6sdfw91nhx785k1bgmkfm2l")))

(define-public crate-url2-0.0.6 (c (n "url2") (v "0.0.6") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "07hv0603wz8bm2bb4bw198m4pdnjvkdgzx886cv2v1p93lzx3768")))

