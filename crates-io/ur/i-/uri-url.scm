(define-module (crates-io ur i- uri-url) #:use-module (crates-io))

(define-public crate-uri-url-0.1.0 (c (n "uri-url") (v "0.1.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "151mk2k69q9x6wp2xaim83xmwzrxzfk9lkvbk4q3c4180snfywzm")))

(define-public crate-uri-url-0.2.0 (c (n "uri-url") (v "0.2.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "08a774g8p32wvfgb4w6hg7mz0ym4vfiyv7ilnv44hrjjdvw292r0")))

(define-public crate-uri-url-0.3.0 (c (n "uri-url") (v "0.3.0") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0wrgaqfs16xnv11fqa5kh10w2cgfiv1zl23854i9balsg5x7ma66")))

