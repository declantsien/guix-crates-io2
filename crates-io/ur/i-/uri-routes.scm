(define-module (crates-io ur i- uri-routes) #:use-module (crates-io))

(define-public crate-uri-routes-0.1.0 (c (n "uri-routes") (v "0.1.0") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "1slqpz7yn7wvj2k70pdbhg2vgx4bfv41c5cab2yl1gjmd6c4xrxr")))

(define-public crate-uri-routes-0.1.1 (c (n "uri-routes") (v "0.1.1") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "05xfx72k4hsj335dkzv038srclz5z8fdpk6lzfiq574zywv3j2z4")))

(define-public crate-uri-routes-0.1.2 (c (n "uri-routes") (v "0.1.2") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "0f87cyrkbg2zmxzvzcy5jg8cmzxnmp3x99skmpl15xbqlmcm3zb9")))

(define-public crate-uri-routes-0.1.3 (c (n "uri-routes") (v "0.1.3") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "0i8sch53yf76a0588diff2xc1wg6css32dqlv4qfhaqm7qb7anqj")))

