(define-module (crates-io ur i- uri-parsing-rs) #:use-module (crates-io))

(define-public crate-uri-parsing-rs-0.0.1 (c (n "uri-parsing-rs") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "prop-check-rs") (r "^0.0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0pjl894dmpgrfqnwb4ravrw4nd2gk940kqdhpsg7bji5x1hianvg") (f (quote (("default" "serde"))))))

