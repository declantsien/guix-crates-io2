(define-module (crates-io ur i- uri-template-system-core) #:use-module (crates-io))

(define-public crate-uri-template-system-core-0.1.0 (c (n "uri-template-system-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02gi6c170mw1pf5wvbx3lydfd6zbgblr5h6az4xmkzf2khrndql7")))

(define-public crate-uri-template-system-core-0.1.1 (c (n "uri-template-system-core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1iczacrfp6jzw9qv78bnszv4rdvahc40v8dadhgrqmz03qs7q3x0")))

(define-public crate-uri-template-system-core-0.1.2 (c (n "uri-template-system-core") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rl3sq7vw183cih9s0m0257xrrz0d8i9i56gyi9q5avi99c2p1vq")))

(define-public crate-uri-template-system-core-0.1.3 (c (n "uri-template-system-core") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1niz2c13hckac2cgsikfn9gizfb54ka4dsqiksfq4psx708bihyj")))

(define-public crate-uri-template-system-core-0.1.4 (c (n "uri-template-system-core") (v "0.1.4") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w1g7silihnd74fw9lf0r8yh3cpw0zsjs51ba5kwl22cslwcyk4g") (r "1.64.0")))

(define-public crate-uri-template-system-core-0.1.5 (c (n "uri-template-system-core") (v "0.1.5") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yriz3bk9lfccsx723galk46as679mhhmfx0v63gqbprb8wm9wi6") (r "1.64.0")))

