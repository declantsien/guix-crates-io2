(define-module (crates-io ur i- uri-pattern-matcher) #:use-module (crates-io))

(define-public crate-uri-pattern-matcher-0.1.0 (c (n "uri-pattern-matcher") (v "0.1.0") (h "1qri25qqhfsfq6shzrkrriq12w7n4zj8s4svav3pxp5254n17pb4")))

(define-public crate-uri-pattern-matcher-0.1.1 (c (n "uri-pattern-matcher") (v "0.1.1") (h "02f9xmyfwvl7zap27c5yz0rkm36cwdfr4qvw9iq35fb2phrr0a4x")))

(define-public crate-uri-pattern-matcher-0.1.2 (c (n "uri-pattern-matcher") (v "0.1.2") (h "1l9c6s9j14vlfaqgbln2vs38bmkm6ciwsb62s68aq73bvq3d1iz2")))

(define-public crate-uri-pattern-matcher-0.1.3 (c (n "uri-pattern-matcher") (v "0.1.3") (h "11igm4p9n1fgrhy12sz0vzc3i0jd4aj94snzhi7g248bg5jl04dn")))

(define-public crate-uri-pattern-matcher-0.1.4 (c (n "uri-pattern-matcher") (v "0.1.4") (h "02aq1l02izsj022rjpx3a086aydg0a2w5yddyhrlarjxdnnwnc5h")))

