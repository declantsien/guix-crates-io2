(define-module (crates-io ur i- uri-template-system) #:use-module (crates-io))

(define-public crate-uri-template-system-0.1.0 (c (n "uri-template-system") (v "0.1.0") (d (list (d (n "uri-template-system-core") (r "^0.1.0") (d #t) (k 0)))) (h "1a8w4q4mcw0na18gzmvjf1pghm1qndw0bqs02pfh1cy9g6slgkjh")))

(define-public crate-uri-template-system-0.1.1 (c (n "uri-template-system") (v "0.1.1") (d (list (d (n "uri-template-system-core") (r "^0.1") (d #t) (k 0)))) (h "1a35j8bnjwax5pj3ah45svmbrgxpc6v8yyrg1qhywyga2jlzc4gz")))

(define-public crate-uri-template-system-0.1.2 (c (n "uri-template-system") (v "0.1.2") (d (list (d (n "uri-template-system-core") (r "^0.1") (d #t) (k 0)))) (h "1srrl618dvpbckax0l1c6ky6ravx2jy5l81baskwyhhxn5vq7v5i")))

(define-public crate-uri-template-system-0.1.3 (c (n "uri-template-system") (v "0.1.3") (d (list (d (n "uri-template-system-core") (r "^0.1.3") (d #t) (k 0)))) (h "04km5vw1gychmq3gmf55rb3y60va818mgdgg96w1489x2plly2cv")))

(define-public crate-uri-template-system-0.1.4 (c (n "uri-template-system") (v "0.1.4") (d (list (d (n "uri-template-system-core") (r "^0.1.4") (d #t) (k 0)))) (h "1r9qdviw00bnqr33w8ghks51hmalddqmrn0lm3g35h2ds5nblpb2") (r "1.64.0")))

(define-public crate-uri-template-system-0.1.5 (c (n "uri-template-system") (v "0.1.5") (d (list (d (n "uri-template-system-core") (r "^0.1.5") (d #t) (k 0)))) (h "0xy1rq4d6xi3jjdbzmdd9a7jjwxf485hx7mxfc515zx4lk496ipd") (r "1.64.0")))

