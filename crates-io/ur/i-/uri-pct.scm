(define-module (crates-io ur i- uri-pct) #:use-module (crates-io))

(define-public crate-uri-pct-0.1.0 (c (n "uri-pct") (v "0.1.0") (h "1ifm8dgavyfqnyj5yxa4pn59k0k2bmr26v6imdkzjvqgaaa1zkgj")))

(define-public crate-uri-pct-0.1.1 (c (n "uri-pct") (v "0.1.1") (h "0957bcp83ms21fgqxx2dcp25daa164hi3q6lnrb4w3rsjnqlccx2")))

