(define-module (crates-io ur i- uri-formatter) #:use-module (crates-io))

(define-public crate-uri-formatter-0.1.0 (c (n "uri-formatter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "071qf3qk9hx74xk12pra1h0xp29firf1pfqhm7c0gncg1h7hx301")))

(define-public crate-uri-formatter-0.1.1 (c (n "uri-formatter") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0ig4pv4am75mdq7crfl1gy5rra7dw6d60kld31p5jwp93vpl6g7a")))

(define-public crate-uri-formatter-0.1.2 (c (n "uri-formatter") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0rsd9999skyak3yx4qbzm82dyjxkpaw2aq9fircmhycz5l6mw5a1")))

