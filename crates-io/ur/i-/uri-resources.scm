(define-module (crates-io ur i- uri-resources) #:use-module (crates-io))

(define-public crate-uri-resources-0.1.0 (c (n "uri-resources") (v "0.1.0") (h "0nhwpkryifxcvq760qhyag1s744bdl4bx1w8362h5xcrnrw5mwgp")))

(define-public crate-uri-resources-0.1.1 (c (n "uri-resources") (v "0.1.1") (h "035289qdminvyy8i9yg37l4akihlxb6x2farfi5596wwazrb4qy4")))

(define-public crate-uri-resources-0.1.2 (c (n "uri-resources") (v "0.1.2") (h "1j6knn8wji69z0933g08hkwliv82i78nbx8d806prv7kqrc0s7af")))

(define-public crate-uri-resources-0.1.3 (c (n "uri-resources") (v "0.1.3") (h "071dmnhp7v2b3fkcy14hifgly4p0pwvnjx05slv77qgpj8lkqn82")))

(define-public crate-uri-resources-0.1.4 (c (n "uri-resources") (v "0.1.4") (h "0lrlcx04lrdva11sxr0r57irjc1piqg4z31vbbblbrk2q7m4n5d6")))

(define-public crate-uri-resources-0.2.1 (c (n "uri-resources") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "03clslf8w52m5n6n5lbx4s17xd1avvl0n4z0jv352ih7855z8iar")))

(define-public crate-uri-resources-0.2.2 (c (n "uri-resources") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "07idgvs7f5ka9mb20jid6vd8iv73h8wwqr7n2xv5l1kxn06vdz6p")))

(define-public crate-uri-resources-0.2.4 (c (n "uri-resources") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0l9gw07nid6slld6c4srw01vn4qbalpqhsk6aqngdd2mzf4dx4vv")))

(define-public crate-uri-resources-0.2.5 (c (n "uri-resources") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "15g4fsqpzgmbpmiwmj69a7jps4syrwlzscrfw10yyb1zivr5z3j4")))

