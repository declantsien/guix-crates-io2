(define-module (crates-io ur lt urltemplate) #:use-module (crates-io))

(define-public crate-urltemplate-0.1.0 (c (n "urltemplate") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1zyhjr3sl0wz09ilc2dk7y5fycgry2yi3shy2kxb4caq9vrha48y")))

(define-public crate-urltemplate-0.1.1 (c (n "urltemplate") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "150m18gy3jzvwrrr03k8zi3c9890rd0zkq58yn7fy0jybhfa66hz")))

(define-public crate-urltemplate-0.1.2 (c (n "urltemplate") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0ki5bimmc8dp8lbyyc3z64yfrh0jggdhzbxq2a4mwz5qlp687qaz")))

(define-public crate-urltemplate-0.1.3 (c (n "urltemplate") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0vlxfqjdjf508nyv6pn25xibwcyia85zj9df4wnbabqyx9b483hc")))

(define-public crate-urltemplate-0.1.4 (c (n "urltemplate") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "serde") (r "^1.0.86") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1wdp4j3rd8vaf8irv4cz1x893cdzk1k3r130p637jz7r7bd7ncq9")))

(define-public crate-urltemplate-0.1.5 (c (n "urltemplate") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "serde") (r "^1.0.86") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1aavrw56jbf47yqdsyff3d214yiqc1c8dm59j48hngxx9l60aqbc")))

