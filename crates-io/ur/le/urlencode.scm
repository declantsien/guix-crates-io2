(define-module (crates-io ur le urlencode) #:use-module (crates-io))

(define-public crate-urlencode-0.1.0 (c (n "urlencode") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1ai519ghf5mhg22yap3rvc3w49rivz89zkxhfmy1yridq16r9b7q")))

(define-public crate-urlencode-0.1.1 (c (n "urlencode") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "1k15h7bycfip0xk8zwgcva59lwm4qn6vgla9j529p53qf59ar647")))

(define-public crate-urlencode-0.1.2 (c (n "urlencode") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1") (d #t) (k 0)))) (h "01c6aqswrsl6f5gbyl9whl0dnkbqw53caw43gikimcgrvvkipfzq")))

(define-public crate-urlencode-1.0.0 (c (n "urlencode") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0h08mpzrr9y5cgwz3nlg207ifv6936pbf7dsk183jmzdbi7r4n62")))

(define-public crate-urlencode-1.0.1 (c (n "urlencode") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1bsmqxc839ygrdk10r7ykkphsx2fazi2mqzs0pkdas5jk8hsi4n9")))

