(define-module (crates-io ur le urlendec) #:use-module (crates-io))

(define-public crate-urlendec-0.1.0 (c (n "urlendec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 0)) (d (n "urlencoding") (r "^2.0") (d #t) (k 0)))) (h "0n7zbfy3iqnk2ws8rz9cpmkd9xqwvrv3k1w3lyf6qfjlhg9k0vip")))

