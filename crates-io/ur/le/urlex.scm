(define-module (crates-io ur le urlex) #:use-module (crates-io))

(define-public crate-urlex-0.1.0 (c (n "urlex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0cf3y30g8xj82ba1jyjavq7qi01n4yjhqg27k5skq7ld9nll9pbh")))

(define-public crate-urlex-0.1.1 (c (n "urlex") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "19r73x8l9hw433mcbl5fy6gvqw0afirxq51ll06qxlmn9mzws2xv")))

