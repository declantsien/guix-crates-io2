(define-module (crates-io ur le urlencoded) #:use-module (crates-io))

(define-public crate-urlencoded-0.1.1 (c (n "urlencoded") (v "0.1.1") (d (list (d (n "bodyparser") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1gvfsbwymip4087sakrwlspnkm4b7gai4cqfkdzbfxyklyiicyzl")))

(define-public crate-urlencoded-0.1.2 (c (n "urlencoded") (v "0.1.2") (d (list (d (n "bodyparser") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0n1a72ng6i8rp5rydgnh589nm028my9zn3d1xk8w4agxpxbvs4pk")))

(define-public crate-urlencoded-0.2.0 (c (n "urlencoded") (v "0.2.0") (d (list (d (n "bodyparser") (r "*") (d #t) (k 0)) (d (n "iron") (r "*") (d #t) (k 0)) (d (n "plugin") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "12zq34g64f86inc6nw5yw5z2fr99vgjvzq70pddjyr6s6c8diq0q")))

(define-public crate-urlencoded-0.2.1 (c (n "urlencoded") (v "0.2.1") (d (list (d (n "bodyparser") (r "^0") (d #t) (k 0)) (d (n "iron") (r "^0.2") (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "1cgksl9nv0fjwdgfhklvdgaivc15547srki9wdgh44pg8fm9hd4d")))

(define-public crate-urlencoded-0.3.0 (c (n "urlencoded") (v "0.3.0") (d (list (d (n "bodyparser") (r "^0.3") (d #t) (k 0)) (d (n "iron") (r "^0.3") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "1mqsdlx9fggwy3w9g94rf6hi8lnni0qi3v441h5fv7yyizlqgf6z")))

(define-public crate-urlencoded-0.4.0 (c (n "urlencoded") (v "0.4.0") (d (list (d (n "bodyparser") (r "^0.4") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "0x4qdv6x8x2ckv9yszxylrbffivphklr3lqf7098qrlpnw646vbm")))

(define-public crate-urlencoded-0.4.1 (c (n "urlencoded") (v "0.4.1") (d (list (d (n "bodyparser") (r "^0.4") (d #t) (k 0)) (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0bhfqx11s9bck4fxk8bpg2899abnmchwbaqcyp6vbvdyl39z5p2x")))

(define-public crate-urlencoded-0.5.0 (c (n "urlencoded") (v "0.5.0") (d (list (d (n "bodyparser") (r "^0.5") (d #t) (k 0)) (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "plugin") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "06fkf73fpcs19p7q7jx714r55azi82pvdkdiaf52kxyn6s370a4c")))

(define-public crate-urlencoded-0.6.0 (c (n "urlencoded") (v "0.6.0") (d (list (d (n "bodyparser") (r "^0.8") (d #t) (k 0)) (d (n "iron") (r ">= 0.5, < 0.7") (d #t) (k 0)) (d (n "plugin") (r "^0.2.6") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "1qrkblcj3gpz256d5fci9g9ig20mxlavy25gj6p612qi740zalha")))

