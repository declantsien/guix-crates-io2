(define-module (crates-io ur le urlencoding) #:use-module (crates-io))

(define-public crate-urlencoding-0.1.0 (c (n "urlencoding") (v "0.1.0") (h "0in43cfgqm42r56y29y90slw7n2h8zqajbafa4lhgr7vm1whc30b") (y #t)))

(define-public crate-urlencoding-0.1.1 (c (n "urlencoding") (v "0.1.1") (h "1lk6lvbjwpkv2kvvj9zypz8mgaaadn3ashp1s3fr5kri0dvv90wn") (y #t)))

(define-public crate-urlencoding-0.2.0 (c (n "urlencoding") (v "0.2.0") (h "0xnjw1mjjixnp7830fyx7n4xvkdmhnwdc3agij5xkakrfz9xvzif") (y #t)))

(define-public crate-urlencoding-1.0.0 (c (n "urlencoding") (v "1.0.0") (h "1v9piz5dwrkiyk0w9cfr55y9rqnr90rw9r52wmblrfx854b5dwrx") (y #t)))

(define-public crate-urlencoding-1.1.0 (c (n "urlencoding") (v "1.1.0") (h "10pia3ibfhjwdsiipvvwrfn82lg17k70n1jj75jlf7xmmfi1mj0v") (y #t)))

(define-public crate-urlencoding-1.1.1 (c (n "urlencoding") (v "1.1.1") (h "14sm5c8idb5jzib8dwf85p5yhd65vxjh946p80p49d2j6fsjw8y9")))

(define-public crate-urlencoding-1.2.0 (c (n "urlencoding") (v "1.2.0") (h "1c016jzdvkswmnpza0iilpms66hgp07qwvamvl0f9hbndcz1jn7q")))

(define-public crate-urlencoding-1.3.0 (c (n "urlencoding") (v "1.3.0") (h "15gpq5968jmjra5srlr6c7djx5sg4x9yxdrbav6drsfa4yv33cfa") (y #t)))

(define-public crate-urlencoding-1.3.1 (c (n "urlencoding") (v "1.3.1") (h "1830ba256w8hwzl4azjpfcm2md5411b9lr00ds12chdp12lxf2ly") (y #t)))

(define-public crate-urlencoding-1.3.2 (c (n "urlencoding") (v "1.3.2") (h "1z6igpqmb3kwjppbbwymy4l8x3nz2lrzwzzlhkw1jim2bb3r3qr1") (y #t)))

(define-public crate-urlencoding-2.0.0-alpha.1 (c (n "urlencoding") (v "2.0.0-alpha.1") (h "06pp8sa00xdrqvjizc6a37dyhs68fa1ccy9kl71qdvqygr3w0379") (y #t)))

(define-public crate-urlencoding-1.3.3 (c (n "urlencoding") (v "1.3.3") (h "1yqgq2qigm1s8zyv23j0422j4vn20ppnnizx9b7p629sw1sh27ss")))

(define-public crate-urlencoding-2.0.0 (c (n "urlencoding") (v "2.0.0") (h "0a6m0vahk16s382rcfgc3x4amw5zbcrangg4qqfddfzwxncvqnyr")))

(define-public crate-urlencoding-2.1.0 (c (n "urlencoding") (v "2.1.0") (h "08cq5w84imxrpyifhmx719026dzjih29gdq0ncsb1fcs08qhkfb8")))

(define-public crate-urlencoding-2.1.2 (c (n "urlencoding") (v "2.1.2") (h "1agfwfzw66krnpqjiv4mhjqq1fcqgwdzikd7x9v835inz4kp9nz8")))

(define-public crate-urlencoding-2.1.3 (c (n "urlencoding") (v "2.1.3") (h "1nj99jp37k47n0hvaz5fvz7z6jd0sb4ppvfy3nphr1zbnyixpy6s")))

