(define-module (crates-io ur le urlexpand) #:use-module (crates-io))

(define-public crate-urlexpand-0.0.1 (c (n "urlexpand") (v "0.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1fm898wdz1v0s3kynhdlvyhacp3pjl8v08g6nin792qpb0m3jwl2")))

(define-public crate-urlexpand-0.0.2 (c (n "urlexpand") (v "0.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0pzzziym9x07av4147rdq0cfw0qaxwjbmfwvzcz59vaycp41yqsv")))

(define-public crate-urlexpand-0.0.3 (c (n "urlexpand") (v "0.0.3") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0y08wwfbbxdc2mjlc6vhj5y7wxa1iyrpgh64335w7z4g9625wawr")))

(define-public crate-urlexpand-0.0.4 (c (n "urlexpand") (v "0.0.4") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0fgp6bp1xqrrl4lzcwhscqyx6kdshhsgjzv7zp63chxgpsi56zqw")))

(define-public crate-urlexpand-0.0.5 (c (n "urlexpand") (v "0.0.5") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1f7163j4nkwpc444lsmay08ib2gb7vyxr3sax04a23f094xhdhw8")))

(define-public crate-urlexpand-0.0.6 (c (n "urlexpand") (v "0.0.6") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1pp3b5fwcy0vhh241vhbg29kkfmw7yr1y67xfmav1l031m43lh64")))

(define-public crate-urlexpand-0.0.7 (c (n "urlexpand") (v "0.0.7") (d (list (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1k6aq157v636j4xxkf1gyv36c726vc9580jidgfnkx0xa0af7ni5")))

(define-public crate-urlexpand-0.0.8 (c (n "urlexpand") (v "0.0.8") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1dc3zqnj7nkscgrmij6h9yzvzn6nknn57d6nqvqdzq6bgsg76107")))

(define-public crate-urlexpand-0.0.9 (c (n "urlexpand") (v "0.0.9") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1jmf3in89z2bp4lf5bisp5bf0dknahva96h99ws3d1hfz4gzc3ll")))

(define-public crate-urlexpand-0.0.10 (c (n "urlexpand") (v "0.0.10") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1ivigffpi7mvl6wyf86azia8np680xn9cwgz1k6hnc931g3m2bfp")))

(define-public crate-urlexpand-0.1.0 (c (n "urlexpand") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0skgvc60d037xx8qkqkwp9j7pfi3yn681ggq4pmgj12ai6nkj31v")))

(define-public crate-urlexpand-0.1.1 (c (n "urlexpand") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "01nk2maxcsp6gjf39ir92qjq2wqwlrqik9g4930iz9ns6sk220pq")))

(define-public crate-urlexpand-0.1.2 (c (n "urlexpand") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "12lk6c7ankmm7rb55nhpr5hwnqhm640g96w3jq9gz9aryhmb9c6h")))

(define-public crate-urlexpand-0.1.3 (c (n "urlexpand") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1igrfb2njn1xrgrxg1y95crqq6qa41aypngi2j0jilr6j7zfnmis")))

(define-public crate-urlexpand-0.2.0 (c (n "urlexpand") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1v5ggjh4rlsss7fkw09riz18m6pfhq17fqlclmk9fv211022b0kx") (f (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2.1 (c (n "urlexpand") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1m03rlij667x3jyp4vhivyf8fqg1q570g7yfcaldcn71g4b5a7nl") (f (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2.2 (c (n "urlexpand") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0xnrzcyrdhkys5qf4h84nzb9hlyl50975llvkhryakhg98xvlw9g") (f (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2.3 (c (n "urlexpand") (v "0.2.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1i396ph9wwqqx1n0gkp3116ad0j9cknxkr8icrrcaywbvr0r40nl") (f (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2.4 (c (n "urlexpand") (v "0.2.4") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("native-tls-vendored"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1mw0fsz6afj42pviiqpw88c9w7rz8krif81b30f38b2ajq63a074") (f (quote (("blocking" "tokio"))))))

