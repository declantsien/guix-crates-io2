(define-module (crates-io ur lm urlmatic) #:use-module (crates-io))

(define-public crate-urlmatic-0.1.0 (c (n "urlmatic") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1nc8mngh5p2zsqq2nlnkfnja9jcq3z8akm1kzlx3f4adi3yxcxb7")))

(define-public crate-urlmatic-0.2.0 (c (n "urlmatic") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0i0s0jlq75w4a2djcdm6xwa2yfbjjqfr1sj5lrv173p4yy0nsy3r")))

(define-public crate-urlmatic-0.2.1 (c (n "urlmatic") (v "0.2.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "182p910zacsl16i6vfjfk04kqzw8k3i1y26jbxvmnnn60j80ds3g")))

(define-public crate-urlmatic-0.3.0 (c (n "urlmatic") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0khw451xb4w01m22g6mlmf86hrcwbs6mvwrm088v26s31izh6a8q")))

(define-public crate-urlmatic-0.4.0 (c (n "urlmatic") (v "0.4.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1znhsgjhvk087f4gwh0yszsp7s4y3v6bqgx65rjlm2jzqymxmkqi")))

(define-public crate-urlmatic-0.5.0 (c (n "urlmatic") (v "0.5.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1rlh6qx4a5ppqvz3az2s4l4jmzy1b5sijhg35cinaqa5jqi89ix8")))

