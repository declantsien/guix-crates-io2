(define-module (crates-io ur ls urlshortener-dbus-daemon) #:use-module (crates-io))

(define-public crate-urlshortener-dbus-daemon-0.1.1 (c (n "urlshortener-dbus-daemon") (v "0.1.1") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 2)) (d (n "urlshortener") (r "^0.9") (d #t) (k 0)))) (h "0al4qbwwlpjhwrdd8n6k1xav3m8cgshzpixssilkq45v4w9612ba")))

