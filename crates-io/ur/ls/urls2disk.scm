(define-module (crates-io ur ls urls2disk) #:use-module (crates-io))

(define-public crate-urls2disk-0.1.0 (c (n "urls2disk") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1c1ij98rii428407h19y27j9fwl3wlgvgxdccfvmpbsrc0k1wdpq")))

(define-public crate-urls2disk-0.1.1 (c (n "urls2disk") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0306bq1fc107qa68ppjydhqh02llnmdhlwwsvr4yjbq3s7j5ghnj")))

