(define-module (crates-io ur ls urlshortener-cli) #:use-module (crates-io))

(define-public crate-urlshortener-cli-0.1.0 (c (n "urlshortener-cli") (v "0.1.0") (d (list (d (n "urlshortener") (r "^0.5.0") (d #t) (k 0)))) (h "0v22mhs0ak975k072w6xdbyf7b7l1mq6h7dbpk1nmwszn67l0bfl")))

(define-public crate-urlshortener-cli-0.1.1 (c (n "urlshortener-cli") (v "0.1.1") (d (list (d (n "urlshortener") (r "^0.5.0") (d #t) (k 0)))) (h "0zdmazirfg4vrhglzq824wqj46nyplyjrkcir7k7h3l5z34gnn6v")))

(define-public crate-urlshortener-cli-0.1.2 (c (n "urlshortener-cli") (v "0.1.2") (d (list (d (n "urlshortener") (r "^0.8") (d #t) (k 0)))) (h "1h6njpahwcvc2zjps4j2hpq5r6d846c7vqbja03habd14fy93wmx")))

(define-public crate-urlshortener-cli-1.0.0 (c (n "urlshortener-cli") (v "1.0.0") (d (list (d (n "urlshortener") (r "^1") (d #t) (k 0)))) (h "1g1nqrqlc126735lqz37905ijcpxyhfk55ny0faihj3dv63bps52")))

(define-public crate-urlshortener-cli-1.0.1 (c (n "urlshortener-cli") (v "1.0.1") (d (list (d (n "urlshortener") (r "^3") (d #t) (k 0)))) (h "1cqnzavha786rvylz7rxy9127vy9hg08w6a4zhn6w9drbigvjqj7")))

