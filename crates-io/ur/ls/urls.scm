(define-module (crates-io ur ls urls) #:use-module (crates-io))

(define-public crate-urls-2.5.0 (c (n "urls") (v "2.5.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "form_urlencoded") (r "^1.2.1") (d #t) (k 0)) (d (n "idna") (r "^0.5.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "scionnet") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fvhma04qin4nla0nk324s17b6jd80nnp2x2jw2sw8kiw2gv2avg") (f (quote (("expose_internals") ("default") ("debugger_visualizer")))) (r "1.56")))

