(define-module (crates-io ur ls urlshortener) #:use-module (crates-io))

(define-public crate-urlshortener-0.1.0 (c (n "urlshortener") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)))) (h "17ai6571424zxvl7c66p7c1yzhfyq95ak9r90pqxgmgj0iaqb96m")))

(define-public crate-urlshortener-0.2.0 (c (n "urlshortener") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1fhq4cyna1v9h44i37y94fs0abyjg54mg250wi8b60jqaqgjs50c")))

(define-public crate-urlshortener-0.3.0 (c (n "urlshortener") (v "0.3.0") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1mky0z5nh8y5amsngz4mg42qqks3dh7nn0ri9ff1lza71sbsp9sc")))

(define-public crate-urlshortener-0.4.0 (c (n "urlshortener") (v "0.4.0") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1j9lf40y7rh3fyrwkl7xxxknm5a6k69zzpaivhrljm19ljq9w4zb")))

(define-public crate-urlshortener-0.5.0 (c (n "urlshortener") (v "0.5.0") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1jnrn4pynw0i9w364a8qap1kq6h5s14pgjzj3m1d55g9971n0vsj")))

(define-public crate-urlshortener-0.6.0 (c (n "urlshortener") (v "0.6.0") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "0inizagqn679wmfafa1bk3za9xzd31hryda0rxphmcww3n9yr1i0")))

(define-public crate-urlshortener-0.6.1 (c (n "urlshortener") (v "0.6.1") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1yx7j8jk9vsvrwpaac3wdp0vywcjn1z4iicwbzpcnr5mg03lrnnp")))

(define-public crate-urlshortener-0.6.2 (c (n "urlshortener") (v "0.6.2") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "028vp8hy63czxkzfa9lhsf6y899d62ibv85h0w4irkjrmiph03ml")))

(define-public crate-urlshortener-0.7.0 (c (n "urlshortener") (v "0.7.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "017sq7fca5sddr7ck7kl8zwv45hdh2z4iqym4alj345w4xnlp1il")))

(define-public crate-urlshortener-0.7.1 (c (n "urlshortener") (v "0.7.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "07li8lhxvnyy7nvw27grslqc7k3xh0y81gb0d05499ci0q3dzryh")))

(define-public crate-urlshortener-0.8.0 (c (n "urlshortener") (v "0.8.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0crir29nrdzfqs0s16hybzlvdy1v6vzw2ki9llpnbj26cmbr8yd7")))

(define-public crate-urlshortener-0.8.1 (c (n "urlshortener") (v "0.8.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "16vqkwfs6945b62dfhrcgs62l8i2bx8jqg5brf0mflik0gmgsxsp")))

(define-public crate-urlshortener-0.9.0 (c (n "urlshortener") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "025332bb4phd3makwg542gd0z1llwsicijzzi8p9rvdmjw72f5hg") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-0.10.0 (c (n "urlshortener") (v "0.10.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1a5hvhhy2b7g7l7z7ichlllk138swxzjhpll98zj0q7ja0j2d1jq") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-1.0.0 (c (n "urlshortener") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "17ih3wb533qrdcib6lgq1nb78c93lvw7ds0aximqcy750bh99kkf") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-1.1.0 (c (n "urlshortener") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0py7hjswfb7cgv331f2m84pqfl1jl3mz41bgibni4rpgndk1ix58") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-2.0.0 (c (n "urlshortener") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "11lkvlam7iv03a2knfwpr83vgfsp4ppqs5mzxsbcg3s5n7xrrvdv") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-2.1.0 (c (n "urlshortener") (v "2.1.0") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1dckkd84p74h08kdbq7724yzl6bf928pzfipw29iss8spmplaqbn") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-2.1.1 (c (n "urlshortener") (v "2.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "13siyhrglia02nabd0qrx9a19nm7i11hd97xkwpglyllgjd6zzmv") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-3.0.0 (c (n "urlshortener") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "10cqk3m5bnldb2ppnsjfyswy2l9gsfqh4y9j02vasgjdzgjqs2gd") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-3.0.1 (c (n "urlshortener") (v "3.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1f69pycr6dnk9hz4j0bksr1iw1cy2p8dk7rk0mfinmhg50hqnd2f") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-3.0.2 (c (n "urlshortener") (v "3.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0wp5zfhn3np94968bs2h6j74jm8kzp14dmssvwy84qn4x8l2zxa5") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-3.1.0 (c (n "urlshortener") (v "3.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "02ff87gfkgfkdb3mk32gdp1g7lwixshfwdn26z8hhanx4np3lnbw") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-3.2.0 (c (n "urlshortener") (v "3.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0fkinhm6i97wy32ja9cjwzc1ycqswrql8wv50milh1l25l5c1v2r") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-urlshortener-3.2.1 (c (n "urlshortener") (v "3.2.1") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1dngzrnfxkq84y0l677zcfc3syb8a5rfdc0dpcw6dg08fzb00y2f") (f (quote (("default" "client") ("client" "reqwest"))))))

