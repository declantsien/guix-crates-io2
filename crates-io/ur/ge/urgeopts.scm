(define-module (crates-io ur ge urgeopts) #:use-module (crates-io))

(define-public crate-urgeopts-0.1.0 (c (n "urgeopts") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0cdrxc25c4v4diqqfcprhdad1fmwbkd5mwh9mg0bmikcfq2rxjil")))

(define-public crate-urgeopts-0.1.1 (c (n "urgeopts") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "1ahalhknd9fjrwf5nsnisw61w17frb1q3ms7x241vsf18b6vk94r")))

(define-public crate-urgeopts-0.1.2 (c (n "urgeopts") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0dyn3vq1ccgwrc3pnb6w7lfwffknbwwck2y86l1sx12kl3b67jdv")))

