(define-module (crates-io og mo ogmo3) #:use-module (crates-io))

(define-public crate-ogmo3-0.0.1 (c (n "ogmo3") (v "0.0.1") (d (list (d (n "hashbrown") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "17b1xj9y8z9g6prdd0pv0yyxqkpfxpaymcdd4w5sfbsh5ih32zwv")))

(define-public crate-ogmo3-0.0.2 (c (n "ogmo3") (v "0.0.2") (d (list (d (n "hashbrown") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1r0af8cq8h6v0kai8gxgjxqksmprl2raan697bn72i1igzv4lhh8")))

(define-public crate-ogmo3-0.0.3 (c (n "ogmo3") (v "0.0.3") (d (list (d (n "hashbrown") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tetra") (r "^0.5") (d #t) (k 2)))) (h "16djrf7h91x0avb3z5dpv0fildp3a8aa45f76xdz5yxch5brgx2g")))

(define-public crate-ogmo3-0.0.4 (c (n "ogmo3") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tetra") (r "^0.5") (d #t) (k 2)))) (h "1ml00pba0k5jpg63dg41pblbji3rasjc2x4p9a17y72xvi9hgsns")))

(define-public crate-ogmo3-0.1.0 (c (n "ogmo3") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tetra") (r "^0.5") (d #t) (k 2)))) (h "031gxaq7l9racyqkyrswjcfn49ra7nrm5phvzqcd20dyrxa49zfk")))

(define-public crate-ogmo3-0.1.1 (c (n "ogmo3") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 2)))) (h "1dk4lv4nhvwy6a377zrlv8p45hkh54fg1w5bp63vnc40mdgjq9vi")))

