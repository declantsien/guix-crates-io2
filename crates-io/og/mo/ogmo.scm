(define-module (crates-io og mo ogmo) #:use-module (crates-io))

(define-public crate-ogmo-0.1.0 (c (n "ogmo") (v "0.1.0") (d (list (d (n "serde") (r "> 0.7") (d #t) (k 0)) (d (n "serde_derive") (r "> 0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "16y4m7v216k9gyw8mpgjjd31an6jhnjk0v9dm95yxph3hbcgn1f3")))

(define-public crate-ogmo-0.2.0 (c (n "ogmo") (v "0.2.0") (d (list (d (n "serde") (r "> 0.7") (d #t) (k 0)) (d (n "serde_derive") (r "> 0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "12wrw9pzxcwd59jxr840c7qhbybf4y2lbjfigvgs05hn00jnzm3d")))

