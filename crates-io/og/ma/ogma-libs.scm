(define-module (crates-io og ma ogma-libs) #:use-module (crates-io))

(define-public crate-ogma-libs-0.1.0 (c (n "ogma-libs") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)) (d (n "nl-parser") (r "^0.1") (d #t) (k 0)) (d (n "nloq") (r "^0.1") (d #t) (k 0)) (d (n "nlsd") (r "^0.1") (d #t) (k 0)) (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0z8v8jvm5x44bw7jwvwlhl58k7780grdwywcj9wplcrdcxzdyy97") (f (quote (("default"))))))

(define-public crate-ogma-libs-0.1.1 (c (n "ogma-libs") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)) (d (n "nl-parser") (r "^0.1") (d #t) (k 0)) (d (n "nloq") (r "^0.1") (d #t) (k 0)) (d (n "nlsd") (r "^0.1") (d #t) (k 0)) (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0bv3z3gr60cfkb3nq759qf8kipiy7xvzpa9pjcc8i2b2163vjbxv") (f (quote (("default"))))))

(define-public crate-ogma-libs-0.1.2 (c (n "ogma-libs") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)) (d (n "nl-parser") (r "^0.1") (d #t) (k 0)) (d (n "nloq") (r "^0.1") (d #t) (k 0)) (d (n "nlsd") (r "^0.1") (d #t) (k 0)) (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1mhq1p0qdjzsw6cqqb8sp405qaxvgx0s7sy07irmgglq39zkysww") (f (quote (("default"))))))

(define-public crate-ogma-libs-0.1.4 (c (n "ogma-libs") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.9.0") (d #t) (k 0)) (d (n "nl-parser") (r "^0.1") (d #t) (k 0)) (d (n "nloq") (r "^0.1") (d #t) (k 0)) (d (n "nlsd") (r "^0.1") (d #t) (k 0)) (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "12d4ivj3yivy1bdqhdjs9kwsbfvy31389jav411s92cbbk3f3fgm") (f (quote (("default"))))))

(define-public crate-ogma-libs-0.1.5 (c (n "ogma-libs") (v "0.1.5") (d (list (d (n "nl-parser") (r "^0.1") (k 0)) (d (n "nloq") (r "^0.1") (k 0)) (d (n "nlsd") (r "^0.1") (k 0)) (d (n "object-query") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0s4m17n8iqsnp3w7xmy2kxdh4hb7jjr4sy36idfsb071h3fjvs8s") (f (quote (("std" "serde/std" "nl-parser/std" "nlsd/std" "nloq/std" "object-query/std") ("default" "std"))))))

(define-public crate-ogma-libs-0.1.6 (c (n "ogma-libs") (v "0.1.6") (d (list (d (n "nl-parser") (r "^0.1") (k 0)) (d (n "nloq") (r "^0.1") (k 0)) (d (n "nlsd") (r "^0.1") (k 0)) (d (n "object-query") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0f95mgs087y2qqfb0ac5khf636pspwvlxdyfcyw24q0vwmcgwmnl") (f (quote (("std" "serde/std" "nl-parser/std" "nlsd/std" "nloq/std" "object-query/std") ("default" "std"))))))

