(define-module (crates-io og ma ogma) #:use-module (crates-io))

(define-public crate-ogma-0.1.0 (c (n "ogma") (v "0.1.0") (d (list (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "ogma-libs") (r "^0.1") (d #t) (k 0)) (d (n "ogma-macros") (r "^0.1") (d #t) (k 0)))) (h "0w2x59g1vjp6jsgg8d4293wg5rbxcv7p5i4gw14g75ayx0l1pm1l") (f (quote (("default"))))))

(define-public crate-ogma-0.1.1 (c (n "ogma") (v "0.1.1") (d (list (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "ogma-libs") (r "^0.1") (d #t) (k 0)) (d (n "ogma-macros") (r "^0.1") (d #t) (k 0)))) (h "1ryb6fbjz39zygy5cyp8dsvy4kikh3yl1m6851069nnlz941crzg") (f (quote (("default"))))))

(define-public crate-ogma-0.1.2 (c (n "ogma") (v "0.1.2") (d (list (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "ogma-libs") (r "^0.1.2") (d #t) (k 0)) (d (n "ogma-macros") (r "^0.1.2") (d #t) (k 0)))) (h "1d0qsw3cxl9jd2jk7xi2gwhlzy760k72gsf567ycrkig4xdadg87") (f (quote (("default"))))))

(define-public crate-ogma-0.1.4 (c (n "ogma") (v "0.1.4") (d (list (d (n "object-query") (r "^0.1") (d #t) (k 0)) (d (n "ogma-libs") (r "^0.1.4") (d #t) (k 0)) (d (n "ogma-macros") (r "^0.1.4") (d #t) (k 0)))) (h "1i9i97s5n2w3zqw2z2wr8n6gip7fq8n0qzyk4yg0460wpvmmq7l8") (f (quote (("default"))))))

(define-public crate-ogma-0.1.5 (c (n "ogma") (v "0.1.5") (d (list (d (n "object-query") (r "^0.1") (k 0)) (d (n "ogma-libs") (r "^0.1.5") (k 0)) (d (n "ogma-macros") (r "^0.1.5") (k 0)))) (h "1f4xarixykhqgkzwmcfi2b8w913lxlm42xqpfqjmqgb1y3kp7irb") (f (quote (("std" "ogma-libs/std" "ogma-macros/std" "object-query/std") ("default" "std"))))))

(define-public crate-ogma-0.1.6 (c (n "ogma") (v "0.1.6") (d (list (d (n "object-query") (r "^0.1") (k 0)) (d (n "ogma-libs") (r "^0.1.6") (k 0)) (d (n "ogma-macros") (r "^0.1.6") (k 0)))) (h "1p7875dyzx4fy6za7s28rk6gx8jrm1qdsb0m6nw3jwy7sppvd55z") (f (quote (("std" "ogma-libs/std" "ogma-macros/std" "object-query/std") ("default" "std"))))))

