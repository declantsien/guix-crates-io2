(define-module (crates-io og ma ogmarkup) #:use-module (crates-io))

(define-public crate-ogmarkup-0.99.0 (c (n "ogmarkup") (v "0.99.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "maud") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1rhck4x11f9blmsd1k95nxvpvky62lzrmc41czayya19jcjcadch") (f (quote (("html" "maud") ("default"))))))

