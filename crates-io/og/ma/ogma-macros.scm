(define-module (crates-io og ma ogma-macros) #:use-module (crates-io))

(define-public crate-ogma-macros-0.1.0 (c (n "ogma-macros") (v "0.1.0") (d (list (d (n "ogma-libs") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r9ffraxkh8s8dm6vvy26ak61nqqrigq01hmfl000mnnw8vfnvnk") (f (quote (("default"))))))

(define-public crate-ogma-macros-0.1.2 (c (n "ogma-macros") (v "0.1.2") (d (list (d (n "ogma-libs") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lp3bmf1mw99ka13yl9agfh20xavy819g90din21sf6ws7qrs94d") (f (quote (("default"))))))

(define-public crate-ogma-macros-0.1.4 (c (n "ogma-macros") (v "0.1.4") (d (list (d (n "ogma-libs") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cb6dypzn59jd27pb9g0dnaiixzq49g7ghd67704hs34lbi14ll5") (f (quote (("default"))))))

(define-public crate-ogma-macros-0.1.5 (c (n "ogma-macros") (v "0.1.5") (d (list (d (n "ogma-libs") (r "^0.1.5") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lhf3garn3mqgyz76lpbyp65i0cpqba6wi3rszx4r7lz6w6n2bbk") (f (quote (("std" "ogma-libs/std") ("default" "std"))))))

(define-public crate-ogma-macros-0.1.6 (c (n "ogma-macros") (v "0.1.6") (d (list (d (n "ogma-libs") (r "^0.1.6") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12pd1bwl1wx3xn2srz7l9r6bcglycw2hl8kxh066ij0hjlwpgr51") (f (quote (("std" "ogma-libs/std") ("default" "std"))))))

