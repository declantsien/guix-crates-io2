(define-module (crates-io og -l og-libwebp-sys) #:use-module (crates-io))

(define-public crate-og-libwebp-sys-0.1.1 (c (n "og-libwebp-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wjxpil86wq9jakxyrg8wrbnyvvfyilpv7g05gbkzigrd8m86ry4")))

(define-public crate-og-libwebp-sys-0.1.2 (c (n "og-libwebp-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11r22x50qbniz4vncnsagbkwp7xhaxcir2v1znps6fir7cwhjmvg")))

