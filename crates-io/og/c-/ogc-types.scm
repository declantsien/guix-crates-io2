(define-module (crates-io og c- ogc-types) #:use-module (crates-io))

(define-public crate-ogc-types-0.1.0 (c (n "ogc-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)))) (h "1kmgdpf4ifr1gpbk5gdp72k1a346hiirbhwz2fqy0xk6vl1xal97")))

