(define-module (crates-io og c- ogc-sys) #:use-module (crates-io))

(define-public crate-ogc-sys-0.1.0 (c (n "ogc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "00hhy2nap1jxa9wdp92dj53ryxxaqcm26ch7yw0vdhdn5m0vv0v4")))

(define-public crate-ogc-sys-0.1.1 (c (n "ogc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 1)))) (h "0mbpdrn2qrz2i0bqwn3ncsnxd5ph58iisyvy4h12ih6a0d5gqdb5")))

