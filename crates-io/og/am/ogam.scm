(define-module (crates-io og am ogam) #:use-module (crates-io))

(define-public crate-ogam-1.0.0 (c (n "ogam") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "maud") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1qcpydf6llsax9ag3kqjvk8i9997pd1yjrd5zyrz7j2i1lx7l0dx") (f (quote (("html" "maud") ("default"))))))

(define-public crate-ogam-1.1.0 (c (n "ogam") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "maud") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "1v0qnm8cxb5ns53ma3i558pxbhkky74nh6q11x5nslwyqycgs73l") (f (quote (("html" "maud") ("default"))))))

