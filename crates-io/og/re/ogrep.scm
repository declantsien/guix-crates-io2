(define-module (crates-io og re ogrep) #:use-module (crates-io))

(define-public crate-ogrep-0.1.0 (c (n "ogrep") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.7") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0mkkg3821krr68fnkjg6ydcl89kzij76c0sw5s1ipys9fmybzknn")))

(define-public crate-ogrep-0.2.0 (c (n "ogrep") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.7") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "0vpxs5q7s3c6wc65ygar6xq55kcxsqjgcc2j3cmx5zlw63shhniw")))

(define-public crate-ogrep-0.2.1 (c (n "ogrep") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.7") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)))) (h "1g69m399085v81m916nf4bpzdlxw469yxpi787drl794wpfcaxsg")))

(define-public crate-ogrep-0.4.0 (c (n "ogrep") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "itertools") (r "^0.7.7") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0firy8c2xy04spipimpjrsz5lrfzbr1nm71715x1zk4f79jlbabs")))

