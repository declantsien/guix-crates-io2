(define-module (crates-io og ni ognibuild) #:use-module (crates-io))

(define-public crate-ognibuild-0.0.18 (c (n "ognibuild") (v "0.0.18") (d (list (d (n "pyo3") (r "^0.18") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1n3pjp3s40pnvfb7i08pkjwhcvbkadlsxb18q0j45664v82v6pbh")))

(define-public crate-ognibuild-0.0.19 (c (n "ognibuild") (v "0.0.19") (d (list (d (n "pyo3") (r "^0.18") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "0sacqkan3fjn6sziihyhk6cfdlzxzk66s54vcjf5139vj9n1d7g9")))

