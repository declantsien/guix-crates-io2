(define-module (crates-io og ca ogcapi-types) #:use-module (crates-io))

(define-public crate-ogcapi-types-0.1.0 (c (n "ogcapi-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "geojson") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.8") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0z3q8nzfr7ymyj5b0x1a51r5371grd1644kp6m2j1xbzfipcrh06") (f (quote (("stac") ("edr") ("default"))))))

(define-public crate-ogcapi-types-0.2.0 (c (n "ogcapi-types") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "geojson") (r "^0.24.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.19") (d #t) (k 0)) (d (n "serde_with") (r "^3.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1m0iqmazqm26s4hhf2b33cpkx0qg6pl3abwn449xc2v217xi49gb") (f (quote (("stac") ("edr") ("default"))))))

