(define-module (crates-io og aw ogawa-rs) #:use-module (crates-io))

(define-public crate-ogawa-rs-0.1.0 (c (n "ogawa-rs") (v "0.1.0") (h "1wqk9wz1rxf5f2ppzsdxgm4y2d9mbz14vm35gsbmbmlvfblbll7s")))

(define-public crate-ogawa-rs-0.2.0 (c (n "ogawa-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vmap") (r "^0.4.2") (d #t) (k 0)))) (h "0gbs2m8jhh0ykq5qnnlrh96j8whljw40jp90qfc6ga5hnx09jbxx")))

(define-public crate-ogawa-rs-0.2.1 (c (n "ogawa-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vmap") (r "^0.5.1") (d #t) (k 0)))) (h "10mlgjqwjvwwlbwni2kaadfh3drmc95i9rig0nsm29zpfwhsggx6")))

(define-public crate-ogawa-rs-0.3.0 (c (n "ogawa-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kjgqhpjqwzqcdghfccfb283lxgln4g5bwjl8y01p103hhj52vry")))

(define-public crate-ogawa-rs-0.4.0 (c (n "ogawa-rs") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "half") (r "^2.1.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cqi6m9716vb38pzf8fw9v3v3wgx67wkkgdg85lqfnawsq4j9pwg")))

