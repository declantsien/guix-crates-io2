(define-module (crates-io og g- ogg-opus) #:use-module (crates-io))

(define-public crate-ogg-opus-0.1.0 (c (n "ogg-opus") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "magnum-opus") (r "^0.3") (d #t) (k 0)) (d (n "ogg") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "0znk429mlih0zg9sx25hi375ymrjym3dq7l02sanjy8bzp3xmkri")))

(define-public crate-ogg-opus-0.1.1 (c (n "ogg-opus") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "magnum-opus") (r "^0.3") (d #t) (k 0)) (d (n "ogg") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "1j2nbsy2cvdmck4b9rg75w5r7a8a0pzzjbzj7i5rqp1vs9gpdj69")))

(define-public crate-ogg-opus-0.1.2 (c (n "ogg-opus") (v "0.1.2") (d (list (d (n "audiopus") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "ogg") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "11b02cy5n0jxr3jjmq7prsdi05hpiv2p3fq8f742gn1gwd1qr1nj")))

