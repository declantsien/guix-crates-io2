(define-module (crates-io og g- ogg-sys) #:use-module (crates-io))

(define-public crate-ogg-sys-0.0.1 (c (n "ogg-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 0)))) (h "1xy91xh47s3zy199kgmbymi0g6456jfvji5iicz5pcncb97962y1")))

(define-public crate-ogg-sys-0.0.2 (c (n "ogg-sys") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1xvfrw0xaiwybhdmqzd8hz4ghckds96pwylrgd5rfpijzmn7a1zv")))

(define-public crate-ogg-sys-0.0.3 (c (n "ogg-sys") (v "0.0.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "08ls8lphs5zaxra95rr6np1sp5z1qxq3w95rn49r9ywr2lihbq6v")))

(define-public crate-ogg-sys-0.0.4 (c (n "ogg-sys") (v "0.0.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1wr573jawk6vnahgh654ihp9nrm9l57i438rpmx2904i0ah0a08f")))

(define-public crate-ogg-sys-0.0.5 (c (n "ogg-sys") (v "0.0.5") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1z5nxzc5y7jbvygjsbzh5qav70wrqzafgkiffvcq2rv7x57abm8c")))

(define-public crate-ogg-sys-0.0.6 (c (n "ogg-sys") (v "0.0.6") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0f1abcb8y40b1xx5w6mcmnr444jq16jg00zsp9n71f62f77wirv7")))

(define-public crate-ogg-sys-0.0.7 (c (n "ogg-sys") (v "0.0.7") (d (list (d (n "gcc") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.2") (d #t) (k 1)))) (h "0hl93cwgppa7j4sy3y1r29lr7s6l2z4d74m21navhcgbck22gpab")))

(define-public crate-ogg-sys-0.0.8 (c (n "ogg-sys") (v "0.0.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jy1j7zmhpzp733m616wjf4vl2mj6i9ik9w6cns4zvb80ab1yv2c")))

(define-public crate-ogg-sys-0.0.9 (c (n "ogg-sys") (v "0.0.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1cpx6n5ndh2d59g43l6rj3myzi5jsc0n6rldpx0impqp5qbqqnx9")))

