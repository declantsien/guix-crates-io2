(define-module (crates-io og g- ogg-opus-wasm) #:use-module (crates-io))

(define-public crate-ogg-opus-wasm-0.1.2 (c (n "ogg-opus-wasm") (v "0.1.2") (d (list (d (n "audiopus") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "ogg") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "1v3qn9z27jsyl83x3ipbnkh87g7asdi3g6gi4iwbryr4hlr86zhv") (y #t)))

(define-public crate-ogg-opus-wasm-0.0.1 (c (n "ogg-opus-wasm") (v "0.0.1") (d (list (d (n "audiopus") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "ogg") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "0jwn81q2nws7wjqfy6dgy53fk81qbg25ygmj2kmfcgk33b2lcm62") (y #t)))

(define-public crate-ogg-opus-wasm-0.1.3 (c (n "ogg-opus-wasm") (v "0.1.3") (d (list (d (n "audiopus") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "ogg") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "1vzl1xbbyghmsjq23cabn5hcqjp1w5pkqyjzj9kzdls2rxfg355z") (y #t)))

