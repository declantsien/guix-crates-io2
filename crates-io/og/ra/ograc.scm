(define-module (crates-io og ra ograc) #:use-module (crates-io))

(define-public crate-ograc-0.1.0 (c (n "ograc") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0mmmq71d377hijbv1xjn5fxv8ig9p6rj5i8pvd1jlggak18nqsag")))

(define-public crate-ograc-0.1.1 (c (n "ograc") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "06gpv12lmrl46dczmfq5wfyxh1ij067lqarkmhc2xjyrdsyxidbn")))

(define-public crate-ograc-0.1.2 (c (n "ograc") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1w7058w4dwq3jmm3m8rfwgj7ynfil4gj00svr2rgw3nsfhcqnlrf")))

(define-public crate-ograc-0.1.3 (c (n "ograc") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0mkzv971pcnkbjvhn40gpf31ny090597m4gkzjsk1jf4yp5431sr")))

(define-public crate-ograc-0.1.4 (c (n "ograc") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1fhlc647ivflb603kzfpzp6g9zwhkl84wkc4qj60s0fhfj7fmq14")))

(define-public crate-ograc-0.1.5 (c (n "ograc") (v "0.1.5") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1nkwgilrx7x4r733vy54ksbydbpwv723jq1nbx73dbqfm3gw4idj")))

(define-public crate-ograc-0.1.6 (c (n "ograc") (v "0.1.6") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1gr9kd2gcshv3s9pz83445ii7dr0w42582qwqrnbbxyh3jdvx7vr")))

