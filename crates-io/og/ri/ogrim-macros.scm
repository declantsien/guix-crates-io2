(define-module (crates-io og ri ogrim-macros) #:use-module (crates-io))

(define-public crate-ogrim-macros-0.0.1 (c (n "ogrim-macros") (v "0.0.1") (d (list (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0jk5qvm30fnilcd7zpi14r5kylaxf25gk44csc5x8mpwlgiwaw7i")))

(define-public crate-ogrim-macros-0.0.2 (c (n "ogrim-macros") (v "0.0.2") (d (list (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "12wizqli2pvb28000rvg3jk42bksbqljx6aki2y4w4ip8rxddn8c")))

(define-public crate-ogrim-macros-0.0.3 (c (n "ogrim-macros") (v "0.0.3") (d (list (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "00rrbw8z1jp6mza4is11bs2vvqlpqxcvwpliiyzchl7rjk57ihfr")))

