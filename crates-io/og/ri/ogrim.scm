(define-module (crates-io og ri ogrim) #:use-module (crates-io))

(define-public crate-ogrim-0.1.0 (c (n "ogrim") (v "0.1.0") (d (list (d (n "ogrim-macros") (r "^0.0.1") (d #t) (k 0)))) (h "01mq96znydjz5112cizaqs5n8fwzsj5vj63sdvz9z9cccpssisbd") (r "1.72")))

(define-public crate-ogrim-0.1.1 (c (n "ogrim") (v "0.1.1") (d (list (d (n "ogrim-macros") (r "=0.0.3") (d #t) (k 0)))) (h "1d3mz7jsqdild30hdwgpd6h7ybwakk5pjn74mmbwfhgh40zz3ag9") (r "1.72")))

