(define-module (crates-io og _f og_fmt) #:use-module (crates-io))

(define-public crate-og_fmt-1.0.0 (c (n "og_fmt") (v "1.0.0") (h "0ya2zx8xpns7hqmm48kz6flla5ylmnjccych7aqfyd8iy3qmn70b")))

(define-public crate-og_fmt-1.0.1 (c (n "og_fmt") (v "1.0.1") (h "13g5i5ak1rd7fsv4vcgdjnhvx856zqfizjlx29s2wcd6jg2mwp0g")))

(define-public crate-og_fmt-1.0.2 (c (n "og_fmt") (v "1.0.2") (h "0lkx16zhg7qnzi0yfgnx8a7qidfr0zkw5ry0wd3l3vkmyp5nb8zh")))

(define-public crate-og_fmt-1.0.3 (c (n "og_fmt") (v "1.0.3") (h "1qks0v2d594vl9qyqpv9g9wibvaag4dl4i3cxdrnrykkb4ybckaz")))

(define-public crate-og_fmt-1.0.4 (c (n "og_fmt") (v "1.0.4") (h "0p64wflksy3w4rrxmlns3bih11rf4a1aak4xbsr38wr788v2a78l")))

