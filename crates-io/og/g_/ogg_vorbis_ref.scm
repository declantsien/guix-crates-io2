(define-module (crates-io og g_ ogg_vorbis_ref) #:use-module (crates-io))

(define-public crate-ogg_vorbis_ref-0.0.1 (c (n "ogg_vorbis_ref") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "vorbis-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0434pz33qzfy6bgjabh7y02anbsqnvp8ji0kahqci0mqryjq97xg")))

(define-public crate-ogg_vorbis_ref-0.0.2 (c (n "ogg_vorbis_ref") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "vorbis-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0npkav0gmf1ncb6w0s38g2zskfrzj84zm62bi16gisihbxdhn0dm")))

(define-public crate-ogg_vorbis_ref-0.0.3 (c (n "ogg_vorbis_ref") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.15") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "vorbis-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0vjyw1mnlnn36pn6vwx1cgjdrnpwh8i9p8msky8h90866n78g607")))

