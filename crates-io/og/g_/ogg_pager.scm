(define-module (crates-io og g_ ogg_pager) #:use-module (crates-io))

(define-public crate-ogg_pager-0.1.3 (c (n "ogg_pager") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0v3p57v9hwhigihc69ikqvk6nq67agx0grz2cm8l57kn46nmdgk7")))

(define-public crate-ogg_pager-0.1.4 (c (n "ogg_pager") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1ijrv7ipcz5hcbn9iyqfwrnm48awmpg844n8sy15ik1wmacbg7qx")))

(define-public crate-ogg_pager-0.1.5 (c (n "ogg_pager") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0qsnnba58jfc6cikr3m7x84gya495w4r8yw1fqzyxaxqs54bvxfc")))

(define-public crate-ogg_pager-0.1.6 (c (n "ogg_pager") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1wph92mpv4p9k0hsirxjhkngxrc73w9yf6p8w6yhbmrm15biz6r1")))

(define-public crate-ogg_pager-0.1.7 (c (n "ogg_pager") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0wg293x46wkanypyxi1h9g3gy640ahas87m31d28p8ihpxbqz1km")))

(define-public crate-ogg_pager-0.1.8 (c (n "ogg_pager") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "09yc7m9pk4a7j3iwc4gzig5796cgniwjqf0mgxblhjsidkcifljg")))

(define-public crate-ogg_pager-0.1.9 (c (n "ogg_pager") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "00xxzzhrxq4pbnwl0qwlvir7dnqxpshi3ys867lixk6jhqmw6i5v") (y #t)))

(define-public crate-ogg_pager-0.2.0 (c (n "ogg_pager") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0v804yvz7s5vrdm5v3mpny54wsaczdvba9cj89b2sigx25n2ap0z")))

(define-public crate-ogg_pager-0.3.0 (c (n "ogg_pager") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "06r49cf8y5k7bmg7rg4nrzyj78jm612k6zyswgab9qdqcqjif1s5")))

(define-public crate-ogg_pager-0.3.1 (c (n "ogg_pager") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0nv3ifg187jbqlw065b80dlqnkd5d4xi6b1j7k9qng6a4jky8w1a")))

(define-public crate-ogg_pager-0.3.2 (c (n "ogg_pager") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1x29l3gbc1id0qhb1wiaaylyfib0vl885x76d12mvfbq0m66r11a")))

(define-public crate-ogg_pager-0.4.0 (c (n "ogg_pager") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1g51y78vws7mwdgvv1821nm12l5qd5a4rblcyap37fg0nrdhc1ca") (y #t)))

(define-public crate-ogg_pager-0.5.0 (c (n "ogg_pager") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0h6bjzxg8jqaqifbllg8xwv78vwid7an409d94f8xs2xdr08l88d")))

(define-public crate-ogg_pager-0.6.0 (c (n "ogg_pager") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "1jjk8qlv2n6208gnimwx50450h2bvmi1g7g3yqrc69bv70xxcjf9")))

(define-public crate-ogg_pager-0.6.1 (c (n "ogg_pager") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)))) (h "1yp6yx17mxzvzk6irvx4kayvpz9f44w9a9vpmf85hg2k13wbxc47")))

