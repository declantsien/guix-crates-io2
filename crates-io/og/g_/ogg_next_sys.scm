(define-module (crates-io og g_ ogg_next_sys) #:use-module (crates-io))

(define-public crate-ogg_next_sys-0.1.0 (c (n "ogg_next_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "git2") (r "^0.15.0") (k 1)))) (h "1zdr843ib7dx20b8x22vkhkp4ndq5d16kbd0zri2c9ymbdn3f3af") (f (quote (("build-time-bindgen" "bindgen")))) (y #t) (l "ogg") (r "1.60.0")))

(define-public crate-ogg_next_sys-0.1.1 (c (n "ogg_next_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.61.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0mginibr0lpp73r3w6m9p5lfjdgm42rx41fam0ffmh20kmk58qmv") (f (quote (("build-time-bindgen" "bindgen")))) (l "ogg") (r "1.60.0")))

(define-public crate-ogg_next_sys-0.1.2 (c (n "ogg_next_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "172wy58namfqjmf0nmkln4vc911mz4s45p190rrhmf4is23igzg9") (f (quote (("build-time-bindgen" "bindgen")))) (l "ogg") (r "1.60.0")))

(define-public crate-ogg_next_sys-0.1.3 (c (n "ogg_next_sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1bjnz7lrxl93ynb9ai0bhq7h2jya3rm4ka2n96r6lvvqd1cg4jmc") (f (quote (("build-time-bindgen" "bindgen")))) (l "ogg") (r "1.64.0")))

