(define-module (crates-io og g_ ogg_metadata) #:use-module (crates-io))

(define-public crate-ogg_metadata-0.1.0 (c (n "ogg_metadata") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "ogg") (r "^0.2") (d #t) (k 0)))) (h "1qf2v5gd1mkk5n64ama87922xfg3kishd185sy9lia0z4xwgp4y5")))

(define-public crate-ogg_metadata-0.2.0 (c (n "ogg_metadata") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "ogg") (r "^0.3") (d #t) (k 0)))) (h "1macy9cym7p8lf6r93qj9ax67qds76h8liy0vh94ijg7d0xlf80y")))

(define-public crate-ogg_metadata-0.3.0 (c (n "ogg_metadata") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "ogg") (r "^0.3") (d #t) (k 0)))) (h "00dd82dmxpfchr9j35pshnm6q790v4rq8113rc4sgym6bxrwqmg7")))

(define-public crate-ogg_metadata-0.4.0 (c (n "ogg_metadata") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "ogg") (r "^0.4") (d #t) (k 0)))) (h "1xy2s1c21v279dpyi2qn77gqc2cx5rnvz27p03cszcsn97k565xv")))

(define-public crate-ogg_metadata-0.4.1 (c (n "ogg_metadata") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "ogg") (r "^0.5") (d #t) (k 0)))) (h "1454s3g2rfwqgaizi4g2dqkfk5l92hkd4nn5ayjwp6a38lbmfrpw")))

