(define-module (crates-io og l3 ogl33) #:use-module (crates-io))

(define-public crate-ogl33-0.1.0 (c (n "ogl33") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0d0zv0zjyh43m8l1b9fsmsfsaki8k83ai9gizihsq6m0380vns8w") (f (quote (("debug_trace_messages") ("debug_error_checks")))) (y #t)))

(define-public crate-ogl33-0.1.1 (c (n "ogl33") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0h0phs138lkf593iyh6wmmfk65m3akvh0xxs9ddwwbfq1fdvv6gw") (f (quote (("debug_trace_messages") ("debug_error_checks")))) (y #t)))

(define-public crate-ogl33-0.1.2 (c (n "ogl33") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1284y0z2v2vnx4issblknlds874zbax0f28mdv091jfggh61cgzj") (f (quote (("debug_trace_messages") ("debug_error_checks")))) (y #t)))

(define-public crate-ogl33-0.1.3 (c (n "ogl33") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "09mx6xgb49x8wq8611xzygcvpfr952gaqasr6wixijl10070v7kr") (f (quote (("debug_trace_messages") ("debug_error_checks")))) (y #t)))

(define-public crate-ogl33-0.1.4 (c (n "ogl33") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "12snc0y112mv5g9ak9assnlwqlg4amzqcswk3hs85bzn8bjk81c8") (f (quote (("debug_trace_messages") ("debug_error_checks")))) (y #t)))

(define-public crate-ogl33-0.1.5 (c (n "ogl33") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1m74cbfng77i3gvfn2nvyjp635vlwwa5zjs5gz9qw0qjmi7rrkwy") (f (quote (("debug_trace_messages") ("debug_error_checks")))) (y #t)))

(define-public crate-ogl33-0.1.6 (c (n "ogl33") (v "0.1.6") (d (list (d (n "beryllium") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "04130xvrzw2q38ryqajvv8nmmnn8724kdf2r5vqzr1zxcxkn82d0") (f (quote (("debug_trace_messages") ("debug_error_checks"))))))

(define-public crate-ogl33-0.2.0-alpha.1 (c (n "ogl33") (v "0.2.0-alpha.1") (d (list (d (n "beryllium") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1b62r0q4mhda7hns0ij7db3gwfa46is36s5rlvdi3b297xkqmfbn") (f (quote (("debug_trace_messages") ("debug_error_checks"))))))

(define-public crate-ogl33-0.2.0-alpha.2 (c (n "ogl33") (v "0.2.0-alpha.2") (d (list (d (n "beryllium") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1f4syjn3xifrwj5lsczdhikh6jkwfji9djldbxh1xawqvgpzqg94") (f (quote (("debug_trace_messages") ("debug_error_checks") ("compatibility_profile"))))))

(define-public crate-ogl33-0.2.0 (c (n "ogl33") (v "0.2.0") (d (list (d (n "beryllium") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0ciyrl7rffiag1ma6z3avbwa0cy8a75lvp4w1vwpfs713alg2k42") (f (quote (("debug_trace_messages") ("debug_error_checks") ("compatibility_profile"))))))

(define-public crate-ogl33-0.3.0 (c (n "ogl33") (v "0.3.0") (h "0rk08kayadj49jv1pchrbc3w73jl976v8fdm1q1mg4mb3idgnf1d")))

