(define-module (crates-io og gv oggvorbismeta) #:use-module (crates-io))

(define-public crate-oggvorbismeta-0.1.0 (c (n "oggvorbismeta") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "lewton") (r "^0.9.4") (d #t) (k 0)) (d (n "ogg") (r "^0.7") (d #t) (k 0)))) (h "0rjdfkyshqghaxrd9g9s8chmpja5d3is9srdqxgh30mb8g8g8v59")))

