(define-module (crates-io gb sd gbsdiff) #:use-module (crates-io))

(define-public crate-gbsdiff-1.0.0 (c (n "gbsdiff") (v "1.0.0") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "gb-cpu-sim") (r "^1.1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (f (quote ("supports-colors"))) (d #t) (k 0)) (d (n "parse-display") (r "^0.6.0") (k 0)) (d (n "slicedisplay") (r "^0.2.2") (d #t) (k 0)))) (h "04c7z646kagxmpqzkgagvfhv9k3szhc5z7izb15ajzyp56p1y9n3")))

