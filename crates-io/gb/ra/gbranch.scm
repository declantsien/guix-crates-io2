(define-module (crates-io gb ra gbranch) #:use-module (crates-io))

(define-public crate-gbranch-0.1.0 (c (n "gbranch") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dialoguer") (r "^0.3.0") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)))) (h "05sdx3hn6m23gy1kafwlgpjijgbh74x98dd6wy6np22mi8zl6m8z")))

