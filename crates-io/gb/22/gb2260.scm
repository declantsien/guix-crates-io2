(define-module (crates-io gb #{22}# gb2260) #:use-module (crates-io))

(define-public crate-gb2260-0.0.1 (c (n "gb2260") (v "0.0.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)))) (h "1wl994yrsffrp82r08hcnfy45ym25kdc2ll43icfn63bhfihv021")))

(define-public crate-gb2260-0.0.2 (c (n "gb2260") (v "0.0.2") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)))) (h "0758by662zpxx6x4vs09f5jsl27sv85vgg22vq0j502rg4317fvj")))

(define-public crate-gb2260-0.1.0 (c (n "gb2260") (v "0.1.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)))) (h "0qf0k5zlvmp1v1dzv4cmk93rjrhgxx7mcprja87y21jgwlndf4h4")))

(define-public crate-gb2260-0.1.1 (c (n "gb2260") (v "0.1.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)))) (h "0dvap25mc132izr9cah7ka4jzw58bpqig4vah29qp65pi6pbw2dn")))

(define-public crate-gb2260-0.1.2 (c (n "gb2260") (v "0.1.2") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)))) (h "1r6s3iqffkpccvpwka92xaaqm2znx17wa4s6dr1b9pyjrhq4lbp0")))

