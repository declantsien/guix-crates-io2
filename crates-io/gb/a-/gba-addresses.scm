(define-module (crates-io gb a- gba-addresses) #:use-module (crates-io))

(define-public crate-gba-addresses-0.0.1 (c (n "gba-addresses") (v "0.0.1") (h "0mms2sgbcz70gqqbwr77vs3k7clcw5vwyvx6m8i9zlmfkrz0d3yf")))

(define-public crate-gba-addresses-0.1.0 (c (n "gba-addresses") (v "0.1.0") (h "0chi64h176fw9py79da2rixd9gc4fq031vyzy8fvrhcvbx2zb6d2")))

