(define-module (crates-io gb a- gba-hal) #:use-module (crates-io))

(define-public crate-gba-hal-0.0.1 (c (n "gba-hal") (v "0.0.1") (d (list (d (n "gba-proc-macro") (r "^0.6") (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)) (d (n "voladdress") (r "^0.2") (d #t) (k 0)))) (h "12nmcz8sy1vr4lyp2igy256n6frdiydsbx132sqjcd581a2y7p84") (f (quote (("unsafe_docs_rs_mmio_listing_override") ("default"))))))

