(define-module (crates-io gb a- gba-make-cartridge) #:use-module (crates-io))

(define-public crate-gba-make-cartridge-1.0.0 (c (n "gba-make-cartridge") (v "1.0.0") (d (list (d (n "clap") (r "~2.23.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 0)))) (h "0vjgawwyd1amw1i95x3mkfhmvfpn9nr6m7mnh8b0l304lafhpgxq")))

(define-public crate-gba-make-cartridge-1.0.1 (c (n "gba-make-cartridge") (v "1.0.1") (d (list (d (n "clap") (r "~2.23.2") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 0)))) (h "1xl7wagzpihhlg0srxc1qyibfkmyvgazzs7vyvfrwjc69vs5hlj0")))

