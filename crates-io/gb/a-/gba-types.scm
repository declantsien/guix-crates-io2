(define-module (crates-io gb a- gba-types) #:use-module (crates-io))

(define-public crate-gba-types-0.0.1 (c (n "gba-types") (v "0.0.1") (h "1f3rqic33x6ka684zrvypfw8pf99w16942v2k6fnjh6k50bkfpvv")))

(define-public crate-gba-types-0.0.2 (c (n "gba-types") (v "0.0.2") (h "1n7x80f215nwma4fgr8qxfrm30sah88p1yvfjd777nvhfa3zj0np")))

(define-public crate-gba-types-0.1.0 (c (n "gba-types") (v "0.1.0") (h "1rwn67vxp2jyym7p7iii91braqxjbl4f33ypczc2kmkh8pnw2sby")))

