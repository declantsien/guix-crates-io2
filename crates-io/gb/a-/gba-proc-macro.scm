(define-module (crates-io gb a- gba-proc-macro) #:use-module (crates-io))

(define-public crate-gba-proc-macro-0.1.0 (c (n "gba-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0n2hgcji6k4f1rvc7y1vwqbyr12p78iybwf4lk1a40vi2nhlb394")))

(define-public crate-gba-proc-macro-0.1.1 (c (n "gba-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1d35csgzz49dy4sdzdlwi2vvyrmgjai1ybjdqp6w0m8hdmdsdz6w")))

(define-public crate-gba-proc-macro-0.2.0 (c (n "gba-proc-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixxv4r71lhria3dvwzji0axq5g1pnrpp9h5bxrwl04mf6wn0w31")))

(define-public crate-gba-proc-macro-0.2.1 (c (n "gba-proc-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1jnndmpwk9z06ax2131212i3rdw3v4xx92nx4bjy3qj82az77ai0")))

(define-public crate-gba-proc-macro-0.3.0 (c (n "gba-proc-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0hymf29wgz40isnp9q1zhzpsas3hsjxvd0vcjpclggfq8a9ccxwz")))

(define-public crate-gba-proc-macro-0.4.0 (c (n "gba-proc-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0qja56si81flfdppghgv8sdhx7qx61ab65aynq6x7pdj9x130phc")))

(define-public crate-gba-proc-macro-0.4.1 (c (n "gba-proc-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1myh6b7q643fkdv9a7yz21lvbkysg1xxlrhqmcibi01irn4n8zw4")))

(define-public crate-gba-proc-macro-0.5.0 (c (n "gba-proc-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b153n0wp2pq1s2lbrimx34hcisvrg895k14blbc3r8fk62p5y1c")))

(define-public crate-gba-proc-macro-0.6.0 (c (n "gba-proc-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sjcl3n5yq58vcyls5wmrljws0fgz71gr9xj34yym3rl0vpl6igj")))

