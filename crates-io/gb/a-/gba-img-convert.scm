(define-module (crates-io gb a- gba-img-convert) #:use-module (crates-io))

(define-public crate-gba-img-convert-0.1.0 (c (n "gba-img-convert") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "imagine") (r "^0.3.0") (f (quote ("miniz_oxide"))) (d #t) (k 0)))) (h "0asd0gzla12pxkjl9nf7d1izasgdi8qj9xq9kw4b2z8gdalz2b8w")))

