(define-module (crates-io gb ps gbps) #:use-module (crates-io))

(define-public crate-gbps-0.1.0 (c (n "gbps") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "053nhaj70p5fnmw62a6q3fl04l9i1s3z2w4zp1jn6r3ibd8dnpqq")))

(define-public crate-gbps-0.2.0 (c (n "gbps") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1zpzglvkcmyikqpil9qra4gdw8g40x6py31rjxn2ghgnyr5vah7a")))

