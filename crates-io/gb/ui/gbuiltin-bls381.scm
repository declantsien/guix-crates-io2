(define-module (crates-io gb ui gbuiltin-bls381) #:use-module (crates-io))

(define-public crate-gbuiltin-bls381-1.3.0 (c (n "gbuiltin-bls381") (v "1.3.0") (d (list (d (n "ark-bls12-381") (r "^0.4.0") (f (quote ("curve"))) (k 0)) (d (n "ark-ec") (r "^0.4.2") (k 0)) (d (n "ark-ff") (r "^0.4.2") (k 0)) (d (n "ark-scale") (r "^0.0.12") (f (quote ("hazmat"))) (k 0)) (d (n "ark-serialize") (r "^0.4") (f (quote ("derive"))) (k 0)) (d (n "codec") (r "^3.6.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "1z0xcayiwyjxrzgavjj8fz3nd4yvm46nvdw0as2mx8q020sfikj4")))

