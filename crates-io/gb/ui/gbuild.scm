(define-module (crates-io gb ui gbuild) #:use-module (crates-io))

(define-public crate-gbuild-0.1.0 (c (n "gbuild") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "gio") (r "^0.9") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "119kczy4ydy58qlf7yhf33djy617rjixgqd3d5qy4l933c1p2g32")))

