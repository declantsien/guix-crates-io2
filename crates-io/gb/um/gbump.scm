(define-module (crates-io gb um gbump) #:use-module (crates-io))

(define-public crate-gbump-0.1.0 (c (n "gbump") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1d55l09q4qsrkrp2jkgzvdxzz2if047ji5lrsbw2vag5v9l4ffxq")))

(define-public crate-gbump-0.2.0 (c (n "gbump") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "19piv1hb81z1qw8fvnddjxdg5c7irdb8qwkbimksi231nwk37g3n")))

(define-public crate-gbump-0.3.0 (c (n "gbump") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1c3zrb1sk76l31dph4xaj0yxahqg1d6hgrcnmid13lmnyf7qhdjs")))

(define-public crate-gbump-0.3.1 (c (n "gbump") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "03avmy3www16f9rm8w91bf4w6b3pnaa8fzcf79bcr3d1c1a0fv3l")))

(define-public crate-gbump-0.4.0 (c (n "gbump") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1i00a56mv3l0kcyv4gyid2ilfvx37anr80jvy824piwidl6pl3ki")))

(define-public crate-gbump-1.0.0 (c (n "gbump") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1n7b0z5py163kgfnrfixf2fxln091wj49xw2847065hbsxfhcqxk")))

(define-public crate-gbump-1.0.1 (c (n "gbump") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "07mwgxfxgrj6r33kyk4dkbnzj5czibgp5l3mvk9d6j7k50cdgvgi")))

(define-public crate-gbump-1.0.2 (c (n "gbump") (v "1.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1jhh3gas3qd0lq8wdgd24s0jzgsabq2g0gmga0dn2c8x20kywjjv")))

(define-public crate-gbump-1.0.3 (c (n "gbump") (v "1.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0vhwq5a2nw8br1gcmrdp5q0p5dzr4dvg2q0fj4hm34pwdggj69rx")))

(define-public crate-gbump-1.0.4 (c (n "gbump") (v "1.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0fjlb52swfspl6jq2fjfzn6bza2h666grmlbf57j54awwxc3rbmw")))

(define-public crate-gbump-1.0.5 (c (n "gbump") (v "1.0.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0z84cg0pgx6pgv9hgvk5djjnzxsjlmwr9x9p9v2q7mj4ia1klckj")))

(define-public crate-gbump-1.0.6 (c (n "gbump") (v "1.0.6") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0zfvp0dyq0k45nwb1c1idbkc9qwl49xyr7cahnsp6pmpacgcx161")))

(define-public crate-gbump-1.0.7 (c (n "gbump") (v "1.0.7") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)))) (h "0dl82wlcabgxizvkangmi7g09ycldmxxbbx4z2wsm844x54f7c6x")))

(define-public crate-gbump-1.0.8 (c (n "gbump") (v "1.0.8") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0w16afxhnwalqigx1fs214y1mfzq9v7qdfx89z0i75fa9z7jffdr")))

(define-public crate-gbump-1.0.9 (c (n "gbump") (v "1.0.9") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "00zwkk53gzn97gq8rvy2g3i90x3422fyq51i05s0yk7rk113ffh9")))

(define-public crate-gbump-1.0.10 (c (n "gbump") (v "1.0.10") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "066iswr5zk381vxvz322wi7im8gzg2krhd238xfcp4as7s37ac4b")))

(define-public crate-gbump-1.1.0 (c (n "gbump") (v "1.1.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0q9y26n2xs321jvlli2a26iid7xsfnagdzacw9ffja8r1rx86clg")))

(define-public crate-gbump-1.1.1 (c (n "gbump") (v "1.1.1") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0ffbicjwbj8c2px6bixhlz12gjy67q0wwvxh4pgw8ac9pd0fshg7")))

(define-public crate-gbump-1.1.2 (c (n "gbump") (v "1.1.2") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "08k3d9pn1zm85dc0bhz86frhs4r0wqbbg96bv4p63rf4hm4v1x3v")))

