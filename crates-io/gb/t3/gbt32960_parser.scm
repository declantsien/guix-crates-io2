(define-module (crates-io gb t3 gbt32960_parser) #:use-module (crates-io))

(define-public crate-gbt32960_parser-0.1.0 (c (n "gbt32960_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "108q01pnvgpwaazd037m9rx4nkwx4rakg8ib8c7rrdp8k54ridxh")))

