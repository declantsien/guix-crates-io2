(define-module (crates-io gb af gbafix) #:use-module (crates-io))

(define-public crate-gbafix-0.0.0 (c (n "gbafix") (v "0.0.0") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)))) (h "072nhh7agnd5x7dpzkmy7b1kg6pi3gm83c9y3fm0n26vl6xcdq1d")))

(define-public crate-gbafix-1.0.0 (c (n "gbafix") (v "1.0.0") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)))) (h "1pi4gfh5jxhm310xvkcay4i6mfb676adh9p80d3w6mngyzb4xrrm")))

(define-public crate-gbafix-1.0.1 (c (n "gbafix") (v "1.0.1") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)))) (h "11n1rjs7giqikkizjfcyxzs0j6svaa32k4jywc5ikxjfyw4x5v87")))

(define-public crate-gbafix-1.0.2 (c (n "gbafix") (v "1.0.2") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)))) (h "1ahi51vphkwyns6a152w25l5qwi25p7mgv9gp9q7fcp71ikki070")))

(define-public crate-gbafix-1.0.3 (c (n "gbafix") (v "1.0.3") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "1h33lp5mk9znyzhycw57vxvd118yqannhb24f4laqfgqn1wx8lhl")))

(define-public crate-gbafix-1.0.4 (c (n "gbafix") (v "1.0.4") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "1dxldb11fx3r6z0zxicpbcp1yli8dipr3nakxw6qnyrpspwpmr7p")))

