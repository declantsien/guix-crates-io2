(define-module (crates-io gb dt gbdt) #:use-module (crates-io))

(define-public crate-gbdt-0.1.0 (c (n "gbdt") (v "0.1.0") (h "1m5z8l8b5pddsi0mi0k4achldfl6k30jszddykjdhsm5xhfffg7f")))

(define-public crate-gbdt-0.1.1 (c (n "gbdt") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0d6dkk4p10xhpin8qygbfizribmx84zybyhkxqy917rlxa38693l") (f (quote (("profiling" "time") ("input" "regex") ("enable_training") ("default" "enable_training" "input"))))))

(define-public crate-gbdt-0.1.2 (c (n "gbdt") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "000pazwzj0cc9j5kk1zsja7q2fbzslnsis8mb3wlfgz5czhix6vn") (f (quote (("profiling") ("input" "regex") ("enable_training") ("default" "enable_training" "input"))))))

(define-public crate-gbdt-0.1.3 (c (n "gbdt") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19rjfp92k2gc52i9bpswfrap3vr0sqshrg7yqql67l30ws3gkxnv") (f (quote (("profiling") ("input" "regex") ("enable_training") ("default" "enable_training" "input"))))))

