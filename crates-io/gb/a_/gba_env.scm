(define-module (crates-io gb a_ gba_env) #:use-module (crates-io))

(define-public crate-gba_env-1.0.0 (c (n "gba_env") (v "1.0.0") (h "16hlilv3dmvdws1f3i4mw6fp8rh2wskxgqzb13dhwnj97iy4fha3")))

(define-public crate-gba_env-1.1.0 (c (n "gba_env") (v "1.1.0") (h "1vi5sbmzvrhwz946h0vjlywrnfvrpp3gd7xyady443i3iii9mry4")))

