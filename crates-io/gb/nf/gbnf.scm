(define-module (crates-io gb nf gbnf) #:use-module (crates-io))

(define-public crate-gbnf-0.1.0 (c (n "gbnf") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "10d54bxfba5i7fpgcncf8fswh5nflp6r6fhhb7h0hzn5nnlh9fa2")))

(define-public crate-gbnf-0.1.1 (c (n "gbnf") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "01am2sp59k906c3sd323pz8gfsl9aqh0jrc4yvh9yjaqwnpwxx2v")))

(define-public crate-gbnf-0.1.2 (c (n "gbnf") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "00s8iqn9wmjp6rf00wk6z61gv74sp7b5cai0dgglhvwkzc65y4nv")))

(define-public crate-gbnf-0.1.3 (c (n "gbnf") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0v6abcy4y5pm4g9x6ghw81ikj40yr3xiy83ixl4wgfp28svwfzdx")))

(define-public crate-gbnf-0.1.4 (c (n "gbnf") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "19qa4705qrrgpjsf3857xgj7adfn65ygcl39bchnc68qsjh9zwdd")))

(define-public crate-gbnf-0.1.5 (c (n "gbnf") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0r74raq30prkihinrxx8g5akwkwq5x3s5yd6fkcch4hbhivrdclh")))

(define-public crate-gbnf-0.1.6 (c (n "gbnf") (v "0.1.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1kzm7zkycks76p8w0ni0ifkfz8xamwzvjngiy726qpg3y5fncchz")))

(define-public crate-gbnf-0.1.7 (c (n "gbnf") (v "0.1.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "192xph0fmry72fpviq3zr6s2n7dbcjv8ny3mvg5lq0glca1j5xnd")))

