(define-module (crates-io gb iz gbiz-info-api) #:use-module (crates-io))

(define-public crate-gbiz-info-api-0.1.0 (c (n "gbiz-info-api") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "~0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("full"))) (d #t) (k 2)))) (h "0h4crgjascqdhab972fyylgsh8hf12hcqa1lbr28b8ib5vh2vs8h")))

