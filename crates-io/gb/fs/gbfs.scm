(define-module (crates-io gb fs gbfs) #:use-module (crates-io))

(define-public crate-gbfs-0.2.2 (c (n "gbfs") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7.1") (k 0)) (d (n "byte-slice-cast") (r "^1") (k 0)))) (h "060xqnm1kha72iclb1p7j1sa4yaf5lc9v9wsnkws75vmyknj06rn")))

(define-public crate-gbfs-0.3.0 (c (n "gbfs") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "byte-slice-cast") (r "^1") (k 0)))) (h "1j370smlrhaj5wznsqignmzamy1qhn5cpfzycfvgajx42b2mrmg2")))

(define-public crate-gbfs-0.3.1 (c (n "gbfs") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "byte-slice-cast") (r "^1") (k 0)))) (h "0dfis70vzdqd0gkqjwdd2kln3v73wvap4ni1ssvazldm2c00pbkp")))

