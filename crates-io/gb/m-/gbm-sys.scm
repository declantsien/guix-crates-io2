(define-module (crates-io gb m- gbm-sys) #:use-module (crates-io))

(define-public crate-gbm-sys-0.1.0 (c (n "gbm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.22") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xl3h4dyrj44w0d1qjylnjhsjsssgnimpjbbdb1dc01alnd1gdp0") (f (quote (("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.2.0 (c (n "gbm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ph2658l0xy5y0wxmx08034029x0akqhm0bf4jgpvfgcmriv7qy1") (f (quote (("update_bindings" "gen") ("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.2.1 (c (n "gbm-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05by5hihchbshpkpfkl4756q3pyx4vq68zm1y5ssyz3f5s5mfrn3") (f (quote (("update_bindings" "gen") ("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.2.2 (c (n "gbm-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rxgzvmk861q7x06js5c97qgkj81jdshisrd90a1a8vskfdvlgmn") (f (quote (("update_bindings" "gen") ("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.3.0 (c (n "gbm-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.10") (o #t) (d #t) (k 1)))) (h "0n6w1lmsk3gjizsjpxilhjda8lf8qjjzj1ffxs5v6hq1gjzxdlkg") (f (quote (("update_bindings" "use_bindgen")))) (s 2) (e (quote (("use_bindgen" "bindgen" "dep:proc-macro2" "dep:regex"))))))

