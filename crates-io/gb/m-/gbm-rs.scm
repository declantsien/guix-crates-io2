(define-module (crates-io gb m- gbm-rs) #:use-module (crates-io))

(define-public crate-gbm-rs-0.1.0 (c (n "gbm-rs") (v "0.1.0") (h "1r39jn2cc04pi08i9rfip8djzmxhpw6p8x1wa31miah56dwh3b1y")))

(define-public crate-gbm-rs-0.2.0 (c (n "gbm-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0vd1qz7ywzyk5222sa75pf5p7nbw16shn8q9ki3bbr8wvcxq64s7")))

