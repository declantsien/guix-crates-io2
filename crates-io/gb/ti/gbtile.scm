(define-module (crates-io gb ti gbtile) #:use-module (crates-io))

(define-public crate-gbtile-0.1.0 (c (n "gbtile") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (d #t) (k 0)))) (h "1s4900cjvlcy3b1a9n0zaxpnj3hhjvhv3lhhzy6fz7vz4yca5ysl")))

(define-public crate-gbtile-0.2.0 (c (n "gbtile") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (d #t) (k 0)))) (h "12lfzpvj8fldbxlf34mkvjlah1nqyj3q1dyzrn1qg4mjxn81xc6d")))

