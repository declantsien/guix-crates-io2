(define-module (crates-io wr it writedisk) #:use-module (crates-io))

(define-public crate-writedisk-0.7.0 (c (n "writedisk") (v "0.7.0") (d (list (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1fczxq8bffy4lzg7lw5h6w2k27i6xkqlf053in3a4l455r4zpplm")))

(define-public crate-writedisk-0.8.0 (c (n "writedisk") (v "0.8.0") (d (list (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "13sc73c5919zg3lmr5jf026v6iq3gxklfvds6rp65lxiiqvgc33h")))

(define-public crate-writedisk-0.8.1 (c (n "writedisk") (v "0.8.1") (d (list (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1q6rmfsblc2w7qk4lnvcshycgqix3gv966747lrkk7gcnprk3vwk")))

(define-public crate-writedisk-0.9.0 (c (n "writedisk") (v "0.9.0") (d (list (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "0yfi0f53b8hmfxrxqc3bkyi8zx3w44vvw2vz3xw37977g9qkzzrv")))

(define-public crate-writedisk-1.0.0 (c (n "writedisk") (v "1.0.0") (d (list (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "091sgvw3ia3hmw0lrfxlflv1gdpacv5ill4sx8v953ws73rnhgaq")))

(define-public crate-writedisk-1.0.1 (c (n "writedisk") (v "1.0.1") (d (list (d (n "progress") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "0l02dv1cqz0h4gllp7mpm0bnzz2r6vm8xk5mjc3wnyaiif2n1gg0")))

(define-public crate-writedisk-1.0.2 (c (n "writedisk") (v "1.0.2") (d (list (d (n "progress") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0yj7272kxn66246aq55vi7gdlp8q1k3rqw1wzc3irn9pbv459xfi")))

(define-public crate-writedisk-1.0.3 (c (n "writedisk") (v "1.0.3") (d (list (d (n "progress") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1p7yylnhvwhpr62lwsmfi7lq7qqa19qn5b0ldy5hkm0dsyj2cd23")))

(define-public crate-writedisk-1.1.0 (c (n "writedisk") (v "1.1.0") (d (list (d (n "procfs") (r "^0.8") (d #t) (k 0)) (d (n "progress") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1sijmnsqm3i1i3pddy554wwkvixknl68j7gbaa6yh334f8vfj2bm")))

(define-public crate-writedisk-1.2.0 (c (n "writedisk") (v "1.2.0") (d (list (d (n "procfs") (r "^0.9") (k 0)) (d (n "progress") (r "^0.2") (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1iqwpzblbkfg7zbxcjpqi0wss976f0ayslw9psnzw31phj3ahfmh")))

(define-public crate-writedisk-1.3.0 (c (n "writedisk") (v "1.3.0") (d (list (d (n "procfs") (r "^0.12.0") (k 0)) (d (n "progress") (r "^0.2.0") (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)))) (h "06j152lj22qrss42wrdmi322q9j2ws7nxwphnxc1qvjfwmh6cii7")))

(define-public crate-writedisk-1.4.0 (c (n "writedisk") (v "1.4.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (k 0)) (d (n "progress") (r "^0.2.0") (k 0)))) (h "00di30aspdxfwgc65n94pns4aqwqz40kbr7300fwgqsyaflsik6z")))

