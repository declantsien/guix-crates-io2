(define-module (crates-io wr it writebuf-core) #:use-module (crates-io))

(define-public crate-writebuf-core-0.1.0 (c (n "writebuf-core") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "ufmt") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1zlklpmr2r9v2ycazdjkk92m1algfw40wfkij2scyag4szz2jw0c") (f (quote (("default" "ufmt")))) (s 2) (e (quote (("ufmt" "heapless/ufmt-impl" "heapless/ufmt-write" "dep:ufmt"))))))

