(define-module (crates-io wr it writhing_mass_of_flesh) #:use-module (crates-io))

(define-public crate-writhing_mass_of_flesh-0.1.0 (c (n "writhing_mass_of_flesh") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gif") (r "^0.11.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "0l2zlnxfxbx4whsx0m6r7kd8mfrijvz8as79cizdcmczpjf6b6ja") (y #t)))

(define-public crate-writhing_mass_of_flesh-0.1.1 (c (n "writhing_mass_of_flesh") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gif") (r "^0.11.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "1qhhcxk39j52ysiprz05dy72xgzzj0snqslbsp6zsrligkicbf75")))

(define-public crate-writhing_mass_of_flesh-0.1.2 (c (n "writhing_mass_of_flesh") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gif") (r "^0.11.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "1pxc5bcfqdqh7knl55gi5qwwyng3qm69ama955cmr2nhfnrag1hv")))

