(define-module (crates-io wr it write_atomic) #:use-module (crates-io))

(define-public crate-write_atomic-0.1.0 (c (n "write_atomic") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "14w3yivxcc1yw6vmvwglnwar1fn0l5bg6qvhvvfhi7gz4x2q1xzk")))

(define-public crate-write_atomic-0.2.0 (c (n "write_atomic") (v "0.2.0") (d (list (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "0aw8bhlpr5awvjbcs8w386xza1bn8awvx44gqyhv1b43g3pm6pz5") (r "1.56")))

(define-public crate-write_atomic-0.2.1 (c (n "write_atomic") (v "0.2.1") (d (list (d (n "fastrand") (r "^1.6") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "04i4vxa0ivw348rm1si6plm0as30xbyp9g1jw68b5lalpz3xvhr2") (r "1.56")))

(define-public crate-write_atomic-0.2.2 (c (n "write_atomic") (v "0.2.2") (d (list (d (n "fastrand") (r "1.7.*") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "3.3.*") (d #t) (k 0)))) (h "15np57clkq2pnlxm1wmsp6k1sbk7sqwh4dgmk5h8kb6cgz3ql2qd") (r "1.56")))

(define-public crate-write_atomic-0.2.3 (c (n "write_atomic") (v "0.2.3") (d (list (d (n "fastrand") (r "=1.7.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.3.0") (d #t) (k 0)))) (h "0icldw45s5a50jm9b4a0xbjrpfihxbrrwrb0y2n7yl32l4pjhn8j") (r "1.56")))

(define-public crate-write_atomic-0.2.4 (c (n "write_atomic") (v "0.2.4") (d (list (d (n "fastrand") (r ">=1.7.0, <=1.8.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.3.0") (d #t) (k 0)))) (h "079xrlrsj245dfgyzaryy1ycyjbjzy03mwpqj5r20yrc0fmjjdfv") (r "1.61")))

(define-public crate-write_atomic-0.2.5 (c (n "write_atomic") (v "0.2.5") (d (list (d (n "fastrand") (r ">=1.7.0, <=1.8.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.3.0") (d #t) (k 0)))) (h "02gpckc8v7pyi4fsahh91nhiycx51wp25qhax779pvwhfadhil96") (r "1.61")))

(define-public crate-write_atomic-0.2.6 (c (n "write_atomic") (v "0.2.6") (d (list (d (n "fastrand") (r ">=1.7.0, <=1.8.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.3.0") (d #t) (k 0)))) (h "1s792cr971w4jgd6qkp94j4iqam8nz372knhnc4rxf6pczakvf2g") (r "1.56")))

(define-public crate-write_atomic-0.2.7 (c (n "write_atomic") (v "0.2.7") (d (list (d (n "fastrand") (r ">=1.7.0, <=1.8.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.3.0") (d #t) (k 0)))) (h "0mq9xkvkhrv7rx489438fkl3g4b6jvkbbxvhp3x7nsp3lbpx2gi6") (r "1.56")))

(define-public crate-write_atomic-0.2.8 (c (n "write_atomic") (v "0.2.8") (d (list (d (n "fastrand") (r ">=1.7.0, <=1.8.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.3.0") (d #t) (k 0)))) (h "1159mwm5s0j6arf2wpvk7mv3pk13s3njk5lzdq7cz1fw5dbzd2g1") (r "1.56")))

(define-public crate-write_atomic-0.2.9 (c (n "write_atomic") (v "0.2.9") (d (list (d (n "fastrand") (r ">=1.7.0, <=1.9.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.3.0") (d #t) (k 0)))) (h "146l6a5g9wlac1k0y2p28xmph85bbwxpvphl5w0bjrcabbzj5pfb") (r "1.56")))

(define-public crate-write_atomic-0.2.10 (c (n "write_atomic") (v "0.2.10") (d (list (d (n "fastrand") (r ">=1.7.0, <=1.9.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.4.0") (d #t) (k 0)))) (h "1p3x6pj76s24svnqg90dwbgzyaw2idrr52x6p3hsyld8433z0lpv") (r "1.56")))

(define-public crate-write_atomic-0.3.0 (c (n "write_atomic") (v "0.3.0") (d (list (d (n "rustix") (r "^0.36.0") (f (quote ("fs" "process"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "=3.4.0") (d #t) (k 0)))) (h "00s9xfgvbn2b5jmm1353j1vfp8wxfaaraw34p97g7nar4k9bgdx1") (r "1.56")))

(define-public crate-write_atomic-0.3.1 (c (n "write_atomic") (v "0.3.1") (d (list (d (n "rustix") (r "^0.37.1") (f (quote ("fs" "process"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0iw1iv6zzjd5q64rz8mrrxgz7lmgqk9dlql1y19cndavzb31lxhn") (r "1.56")))

(define-public crate-write_atomic-0.3.2 (c (n "write_atomic") (v "0.3.2") (d (list (d (n "rustix") (r "^0.37.1") (f (quote ("fs" "process"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "00y15hbj4ijc9j9az2mgyqyryk70x69xsa1xcixzbbd6777xvj0j") (r "1.56")))

(define-public crate-write_atomic-0.4.0 (c (n "write_atomic") (v "0.4.0") (d (list (d (n "rustix") (r "^0.38") (f (quote ("fs" "process"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 0)))) (h "17ylgmcpvaswr2vi6j8d26v8cbq3npgly4pn7s2dyc2njz8gzk70") (r "1.63")))

(define-public crate-write_atomic-0.4.1 (c (n "write_atomic") (v "0.4.1") (d (list (d (n "rustix") (r "^0.38") (f (quote ("fs" "process"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 0)))) (h "1z1c6rkhr266ah1pqiwr11p03rmrqf5dla1ky72fz7lhvz9mpk71") (r "1.63")))

(define-public crate-write_atomic-0.5.0 (c (n "write_atomic") (v "0.5.0") (d (list (d (n "tempfile") (r "^3.7.0") (d #t) (k 0)))) (h "0p9wrm4ik32iyh5sc5binnh8krmxsy1xxml43qz4iygnsgvp5in5") (r "1.73")))

