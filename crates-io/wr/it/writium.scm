(define-module (crates-io wr it writium) #:use-module (crates-io))

(define-public crate-writium-0.1.0 (c (n "writium") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.3") (d #t) (k 0)))) (h "0j0n1q3sq8wxblzp5xmfl73mx2q87qylmzsj3zgxnalmxgs2z6cz")))

(define-public crate-writium-0.1.1 (c (n "writium") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_qs") (r "^0.3") (d #t) (k 0)))) (h "0c2rfsn6mfplfh1zc46kfx9r5lhvrj3mj3rs2fli1asirf2pb212")))

