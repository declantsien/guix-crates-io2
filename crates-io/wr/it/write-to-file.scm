(define-module (crates-io wr it write-to-file) #:use-module (crates-io))

(define-public crate-write-to-file-1.0.0 (c (n "write-to-file") (v "1.0.0") (h "17y5g9yz02hn4clny617hh2lpvk68jmlylmmiinbikjgs853l493")))

(define-public crate-write-to-file-1.0.1 (c (n "write-to-file") (v "1.0.1") (h "0pigf4h5x3hmnyck1hnl1nli5nmfjqx6zl27ni8q0xaxbazjdgwz")))

(define-public crate-write-to-file-1.0.2 (c (n "write-to-file") (v "1.0.2") (h "0fws389iqjmrfpc3m21asn0gsr31khkki63w96r94s1asx0mn7ns")))

