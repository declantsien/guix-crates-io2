(define-module (crates-io wr it write-into) #:use-module (crates-io))

(define-public crate-write-into-0.1.0 (c (n "write-into") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 2)))) (h "042kyzp40p47gc17rlxyxz7bzlq1gq3g588dvqq3r421fknbx1bs")))

(define-public crate-write-into-0.1.1 (c (n "write-into") (v "0.1.1") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 2)))) (h "151wzgw5qi34wzp6mwyzgn0gddf4kgw890p5c7vlvv52fdl1zhvz")))

(define-public crate-write-into-0.1.2 (c (n "write-into") (v "0.1.2") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 2)))) (h "1iqisgqm6jgxg5xh3lnw9ak8gm0hn6717ygjwznrkdp830f3jp34")))

(define-public crate-write-into-0.1.3 (c (n "write-into") (v "0.1.3") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 2)))) (h "0ln76k3g177qwpk3448dhf9hdflljb3v93800fcqkxvr66dd86cc")))

(define-public crate-write-into-0.2.0 (c (n "write-into") (v "0.2.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 2)))) (h "1n1d3m8hy8q1zwzwajabrmn39j7nxh2cz95f4hjazfw102mx34f9")))

(define-public crate-write-into-0.3.0 (c (n "write-into") (v "0.3.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 2)))) (h "1ydkdcnq90cpj9314imjzazgv1rwqdjq2fmg8kjqh029330wbzlr")))

(define-public crate-write-into-0.3.1 (c (n "write-into") (v "0.3.1") (h "0pfy7zkw3g2sj23f4y0vm74bvqwldvfnc9nfhl16v37y9xy4hwwq")))

(define-public crate-write-into-0.3.2 (c (n "write-into") (v "0.3.2") (h "014vkwbx9p6qvlmwqjmqczqmjbl3rg1jml27njar3k1m0j9i3kpc")))

(define-public crate-write-into-0.3.3 (c (n "write-into") (v "0.3.3") (h "02rnxv8g7qzy51vxdjr130dlcww823q1j2yiabgvnaksbwnvwkxi")))

(define-public crate-write-into-0.3.4 (c (n "write-into") (v "0.3.4") (h "1wwz8dhpvgjxsr6jqqzd23i9k19lhv71zc2m79iqncdfxj9lnkvw")))

(define-public crate-write-into-0.3.5 (c (n "write-into") (v "0.3.5") (h "0z7ca7p6a9qa5vwrj70ymb0b883yaji0lyajf2kdnx7cm2fwn0fj")))

(define-public crate-write-into-0.3.6 (c (n "write-into") (v "0.3.6") (h "0qzca564b5cpfll18x0bpip7dy74w5nl4n7ydkzfyagyqi3p1823")))

(define-public crate-write-into-0.3.7 (c (n "write-into") (v "0.3.7") (d (list (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "068bsr9ki5z94kadwjr2c4m9daa2fqh5ym3zw78hwlmyrbm92k4j")))

(define-public crate-write-into-0.3.8 (c (n "write-into") (v "0.3.8") (d (list (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "02cf8ds1clq5ml3k364j8xzs7986l327014hgnamkr95ngrrf86h")))

(define-public crate-write-into-0.3.9 (c (n "write-into") (v "0.3.9") (d (list (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "07xiz1qz5nznnwx0ca46rb89sbn3jrif7wx4x942srp118p0k2ii")))

(define-public crate-write-into-0.3.10 (c (n "write-into") (v "0.3.10") (d (list (d (n "test-case") (r "^2.1.0") (d #t) (k 2)))) (h "1r55ij6g76wfvpaarw6m4c382q3kngy2bq9wqrs7pv778zj0dcd3")))

