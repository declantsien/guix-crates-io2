(define-module (crates-io wr it write-html-macro) #:use-module (crates-io))

(define-public crate-write-html-macro-0.1.0 (c (n "write-html-macro") (v "0.1.0") (h "0qlrwa5f870caa1rkb1hxnpv12irfrv0n36xm2f70j0dj24m7awd")))

(define-public crate-write-html-macro-0.1.2 (c (n "write-html-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1lmiwakzcq9rickvmx7wqn7cn12mlvinp9jj4qp2r5jp6sggvasc")))

