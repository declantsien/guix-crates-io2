(define-module (crates-io wr it write-json) #:use-module (crates-io))

(define-public crate-write-json-0.1.0 (c (n "write-json") (v "0.1.0") (h "1l2rgwqyjj25pi9i2hnmh3c3pc3wc43mk0ifipirk6l09iw9lgwb")))

(define-public crate-write-json-0.1.1 (c (n "write-json") (v "0.1.1") (h "1l295wzskhg9akaqzj8nha442axchxj2mlpjn8ac9krslkbxzn4b")))

(define-public crate-write-json-0.1.2 (c (n "write-json") (v "0.1.2") (h "1qxr1xwwyf7rd270rvjlcssm515pikf0sg70wpiymz4miy29l1h6")))

(define-public crate-write-json-0.1.4 (c (n "write-json") (v "0.1.4") (h "0611ic4a3mm3qksqg06cjc6ahzry6kn6f4syz5s4mk364m5igxi3")))

