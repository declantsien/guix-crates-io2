(define-module (crates-io wr it write-monitor) #:use-module (crates-io))

(define-public crate-write-monitor-0.0.1-alpha.1 (c (n "write-monitor") (v "0.0.1-alpha.1") (d (list (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (o #t) (d #t) (k 0)))) (h "0xcn8b49psnddcdfkkj6qjni4nkq606rlwjxh7d3nskxasbgix4c") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:pin-project" "std") ("futures" "dep:futures" "dep:pin-project"))))))

(define-public crate-write-monitor-0.0.1-alpha.2 (c (n "write-monitor") (v "0.0.1-alpha.2") (d (list (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "13ln0phhih55xyn40qz6smkmdgjx3xa2pakcai5sqm7liabvbiq8") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:pin-project" "std") ("futures" "dep:futures" "dep:pin-project"))))))

(define-public crate-write-monitor-0.0.1-alpha.3 (c (n "write-monitor") (v "0.0.1-alpha.3") (d (list (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "1sd9d4jw2rknhmkq9p2nkfk0zf1fb11hp8ahcv1q4gwdg9a5igrm") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:pin-project" "std") ("futures" "dep:futures" "dep:pin-project"))))))

