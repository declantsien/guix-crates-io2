(define-module (crates-io wr it write_x86_64) #:use-module (crates-io))

(define-public crate-write_x86_64-0.1.0 (c (n "write_x86_64") (v "0.1.0") (h "02n0vdbwl9d64jjbnviqlkk5y5v377q3l7skigf90kfspzhb02pc") (r "1.63.0")))

(define-public crate-write_x86_64-0.1.1 (c (n "write_x86_64") (v "0.1.1") (h "01gcdjkmgcs3wxmcl24jcfh1np0pfmm739b38558x1qk6s0alsr8") (r "1.56.1")))

(define-public crate-write_x86_64-0.1.2 (c (n "write_x86_64") (v "0.1.2") (h "1s134f5d7s9qb0zphz5n4h6iyjc9ix5wx8l9vnj11qqkwkc84zlj") (r "1.56.1")))

(define-public crate-write_x86_64-0.2.0 (c (n "write_x86_64") (v "0.2.0") (h "0vsxkc2qkvv0bz5375ynaj78355a37yf7mn0gnjai7iqhx9ljrgw") (r "1.56.1")))

