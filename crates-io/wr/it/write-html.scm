(define-module (crates-io wr it write-html) #:use-module (crates-io))

(define-public crate-write-html-0.1.0 (c (n "write-html") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "html-builder") (r "^0.5.1") (d #t) (k 2)) (d (n "write-html-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0br9rfmly5mjn1baakx4yp4haxs7pxx9m4n0y6a7cq2k9wfrhsvi")))

(define-public crate-write-html-0.1.1 (c (n "write-html") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "html-builder") (r "^0.5.1") (d #t) (k 2)) (d (n "write-html-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0ymzj3p8bgyilzsrgbiqmhnvrkcf1dpnb3qm72hmc2a2nqqlhnf1")))

(define-public crate-write-html-0.1.2 (c (n "write-html") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "html-builder") (r "^0.5.1") (d #t) (k 2)) (d (n "write-html-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1gpwdnxam6n921b32kw57z5sxbnj98n1xdsg4fah6192js5fqvvs")))

(define-public crate-write-html-0.1.3 (c (n "write-html") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "html-builder") (r "^0.5.1") (d #t) (k 2)) (d (n "write-html-macro") (r "^0.1.2") (d #t) (k 0)))) (h "02glgl2wgbh2dqc0wfh4kyyv49py660xm95n69fq5cyp43f4n3pf")))

