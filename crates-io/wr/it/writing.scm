(define-module (crates-io wr it writing) #:use-module (crates-io))

(define-public crate-writing-0.1.0 (c (n "writing") (v "0.1.0") (h "1pgrq69k94f2bg7khxv3qyv5ykgygm79bbzmh2h4ilhq84f7l5da")))

(define-public crate-writing-0.1.1 (c (n "writing") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0257lavl59v8nrp842fhlxfvmap15f5jv6df82hgbkkpxhsk1q0n")))

(define-public crate-writing-0.2.0 (c (n "writing") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "guarding_core") (r "^0.2.5") (d #t) (k 0)) (d (n "guarding_ident") (r "^0.2.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s82hqghwvrh4bvayir2lplssama7a67ilsm51v1sl13650gszcx")))

(define-public crate-writing-0.2.1 (c (n "writing") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "guarding_core") (r "^0.2.6") (d #t) (k 0)) (d (n "guarding_ident") (r "^0.2.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "=0.19.3") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "=0.19.0") (d #t) (k 0)))) (h "14wkq9rzhfzf9r80rv3s4b7mjf4afnzlipn9gnifwczvxfr0zb8y")))

