(define-module (crates-io wr it write16) #:use-module (crates-io))

(define-public crate-write16-1.0.0 (c (n "write16") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (o #t) (d #t) (k 0)))) (h "0dnryvrrbrnl7vvf5vb1zkmwldhjkf2n5znliviam7bm4900z2fi") (f (quote (("alloc"))))))

