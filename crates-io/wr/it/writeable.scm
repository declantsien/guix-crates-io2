(define-module (crates-io wr it writeable) #:use-module (crates-io))

(define-public crate-writeable-0.1.0 (c (n "writeable") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19xrl5lnyzs5d87f1kiwr5jpqa03iydm7fz2hf7ph98gg2s2lddm") (f (quote (("default") ("bench"))))))

(define-public crate-writeable-0.2.0 (c (n "writeable") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "icu_benchmark_macros") (r "^0.2") (d #t) (k 2)))) (h "1fwki0x94zdqgv9w7c3px1fq87jqp83f56kjlxh6zbmwalzl251q") (f (quote (("default") ("bench"))))))

(define-public crate-writeable-0.2.1 (c (n "writeable") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "icu_benchmark_macros") (r "^0.3") (d #t) (k 2)))) (h "1rnc001wizp8zrndpyrvhyd0agjarx21kam5ajykpxblycdxf8k5") (f (quote (("default") ("bench"))))))

(define-public crate-writeable-0.3.0 (c (n "writeable") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "icu_benchmark_macros") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1r9xihid1p08zh4wc2zn5vyyphdsyfyg8z75p6wxz5scphdvlg86") (f (quote (("default") ("bench"))))))

(define-public crate-writeable-0.4.0 (c (n "writeable") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "icu_benchmark_macros") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0klf3g2fxpma198d4wpfc4k06m6anibf43za5dfa8ij4nar5y1mf") (f (quote (("default") ("bench"))))))

(define-public crate-writeable-0.4.1 (c (n "writeable") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "icu_benchmark_macros") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "10qxqiszc18f7c89zfpn2br5nwn2rd1878qp3xg7p3zny2761ayq") (f (quote (("default") ("bench"))))))

(define-public crate-writeable-0.5.0 (c (n "writeable") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "icu_benchmark_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0szh4skrw530xf20w1agbfqjgjxs074gl32r5jplvcm1bm7sprpq") (f (quote (("default") ("bench"))))))

(define-public crate-writeable-0.5.1 (c (n "writeable") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1g5mzlf5yziaaxlclrjsdz1vjkj69lmwia5hkl97m6ivgrl4mmwj") (f (quote (("bench"))))))

(define-public crate-writeable-0.5.2 (c (n "writeable") (v "0.5.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "03ysq0ghn3zbhqibivx3vwpd6507iwpi0y6dyivdrmdipm19xr30") (f (quote (("bench"))))))

(define-public crate-writeable-0.5.3 (c (n "writeable") (v "0.5.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1hmig83h3qhglia9yyrgcv5mqbba16kny9dml3fziszs2cyhrby0") (f (quote (("bench")))) (r "1.66")))

(define-public crate-writeable-0.5.4 (c (n "writeable") (v "0.5.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0kp1p9fbavvrvn3nl9xgps1gvs8f5d2qp8vdgfi0m77gp1jbpmys") (f (quote (("bench")))) (r "1.67")))

(define-public crate-writeable-0.5.5 (c (n "writeable") (v "0.5.5") (d (list (d (n "criterion") (r "^0.5.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "either") (r "^1.9.0") (o #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0lawr6y0bwqfyayf3z8zmqlhpnzhdx0ahs54isacbhyjwa7g778y") (f (quote (("bench")))) (s 2) (e (quote (("either" "dep:either")))) (r "1.67")))

