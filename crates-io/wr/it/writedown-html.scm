(define-module (crates-io wr it writedown-html) #:use-module (crates-io))

(define-public crate-writedown-html-0.1.0 (c (n "writedown-html") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "writedown") (r "^0.1.0") (d #t) (k 0)))) (h "0hkb9bacx72033n7l2bc2py18bw35aw1yayaypj707ck381314k3")))

