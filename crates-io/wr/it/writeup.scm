(define-module (crates-io wr it writeup) #:use-module (crates-io))

(define-public crate-writeup-1.0.0 (c (n "writeup") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0sm1y716zdgwraljm68gr8sv5p19s46f9ijfcd16ybhh0k7ph560")))

(define-public crate-writeup-1.0.1 (c (n "writeup") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1bpfbx771zipzdsvcx3b9a9xs7j0axw950iaihf0iy1s51bh8kw2")))

(define-public crate-writeup-1.0.2 (c (n "writeup") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0q239xmr5ni4rs65sjz2wy2qv0xqabr6pydp79pg6r8cbqqx4l8h")))

