(define-module (crates-io wr in wring-twistree) #:use-module (crates-io))

(define-public crate-wring-twistree-0.1.0 (c (n "wring-twistree") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mod_exp") (r "^1.0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-prime") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "08dvsc99ww7h3jwl5s9l5r8ck3phc9q820if137adibj4r2v456m")))

