(define-module (crates-io wr ea wread-mongodb) #:use-module (crates-io))

(define-public crate-wread-mongodb-0.14.0 (c (n "wread-mongodb") (v "0.14.0") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0licvrsfp75dpz6igh8w1fqq723m36r6nhy75h72p6wpj18ndqrn")))

(define-public crate-wread-mongodb-0.15.0 (c (n "wread-mongodb") (v "0.15.0") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bakbcarw1xxxwrx55wxb0rlflg8087xjpxx6aqs5b405nxnm7si")))

(define-public crate-wread-mongodb-0.15.1 (c (n "wread-mongodb") (v "0.15.1") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1acikv681m2kyq730z5rfs6l0srjwn6v6rdl2n0pa3z2246mxlk5")))

(define-public crate-wread-mongodb-0.16.0 (c (n "wread-mongodb") (v "0.16.0") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fv2kqfvbxbzx23dz97qixigpx1qij3s6g19c10rvsd0xw9g93h8")))

(define-public crate-wread-mongodb-0.16.1 (c (n "wread-mongodb") (v "0.16.1") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m8s1khziazdla61cl2agmy3shi37fz62bgbg9sz0fkgvzqij7c9")))

(define-public crate-wread-mongodb-0.16.3 (c (n "wread-mongodb") (v "0.16.3") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-beta") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kkvjx6klw5m90n66d1cq3iiwdp59sdf67w350lqqx0j5z1dz9q6")))

(define-public crate-wread-mongodb-0.16.4 (c (n "wread-mongodb") (v "0.16.4") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k7a18vf6s0ss3gzfi0n4izcrrhpmgmm4n5p1lh5ys1s8vg0jc5v")))

(define-public crate-wread-mongodb-0.16.5 (c (n "wread-mongodb") (v "0.16.5") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "199936bf5ifymqmxisfhpnh3splmfj6cmiik5zrljqbnlrxz9kn5")))

(define-public crate-wread-mongodb-0.16.6 (c (n "wread-mongodb") (v "0.16.6") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bfknhh5i95xh96wi232sbs53rfm17mjqyn20rfl48h1c9768snn")))

(define-public crate-wread-mongodb-0.17.0 (c (n "wread-mongodb") (v "0.17.0") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0-beta.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x4kp6pgdbvxgwzfk2n4w9b2pg6vygz3a0y0apgpb7rjrp4hk51m")))

(define-public crate-wread-mongodb-0.17.1 (c (n "wread-mongodb") (v "0.17.1") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mongodb") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1psc44nn3byb0wmw0xqa266ma89c1hl101ya37q1120w4wj666r2")))

(define-public crate-wread-mongodb-0.18.1 (c (n "wread-mongodb") (v "0.18.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mongodb") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iay937xiw5lfhjkj8gdhjh47gqng9xws3kbzfq08vr0lwj0dq74")))

(define-public crate-wread-mongodb-0.19.0 (c (n "wread-mongodb") (v "0.19.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mongodb") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "19y4hk9xv7qzyncnyvshzf07xnjgav5r4mxblngriaqrkmnfz5qw")))

(define-public crate-wread-mongodb-0.19.1 (c (n "wread-mongodb") (v "0.19.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mongodb") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "161z1i6g31d8gffj0mpw2mb54h0vjin8xaxn6f4rr4vh0b4f3n18")))

