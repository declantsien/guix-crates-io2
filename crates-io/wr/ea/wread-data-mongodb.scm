(define-module (crates-io wr ea wread-data-mongodb) #:use-module (crates-io))

(define-public crate-wread-data-mongodb-0.1.0 (c (n "wread-data-mongodb") (v "0.1.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "19wzbs0vhndnkxximda8a51m3y63f9ssf0x6zg33h8dhbmgxhbw0")))

(define-public crate-wread-data-mongodb-0.2.0 (c (n "wread-data-mongodb") (v "0.2.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "00nd9mqs57n51ivxs3xa4vlhywg5fl1hl5153s9zb5pizmjihfx1")))

(define-public crate-wread-data-mongodb-0.3.0 (c (n "wread-data-mongodb") (v "0.3.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ls1dysv0gw9ck3h2nf8419j81mbxqmzqcpskwnvgpfmjisdh967")))

(define-public crate-wread-data-mongodb-0.4.0 (c (n "wread-data-mongodb") (v "0.4.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "01b2r60x9bz6s37af379bg0pgmhm7c3d6mydxf7v0d2i4qwa2gq7") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.5.0 (c (n "wread-data-mongodb") (v "0.5.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j5njn7ghm0pqd633ibxxh5flgbh2g92mvp18k6rhv0i8020xlr6") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.6.0 (c (n "wread-data-mongodb") (v "0.6.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "164y7apfqvvsb1g2hxc6zjz1v8m118h0jbwwdpfw3lshsp8j7lp6") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.6.1 (c (n "wread-data-mongodb") (v "0.6.1") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r7i61ql9bz25pdcq4qdb6nfa34pkq2j9wvwbavzbv7f1lfga5ag") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.7.0 (c (n "wread-data-mongodb") (v "0.7.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gg87pl9ghlk3pml5vf8pcm6dady09j26yyfrxvi270nflz3hr64") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.8.0 (c (n "wread-data-mongodb") (v "0.8.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.10.0") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "01pxl288zwif61di9f1gymi3dwa5n6jhixpwyfl1pypqicpsnc1s") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.8.1 (c (n "wread-data-mongodb") (v "0.8.1") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yrrpbnps425fzg7srinhm8zv4ayi9kyhzx40iajxb9rm3msbqbz") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.9.0 (c (n "wread-data-mongodb") (v "0.9.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x8b1flgh7ckqrlxfdxs7s43hnbbnhpn1z3zzbgxg1wc3y31hnxd") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.10.0 (c (n "wread-data-mongodb") (v "0.10.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nnyq9g4w8sigl6g5rjb2w2cknbvxfgxixlw3q99vgvmxq5ga8xr") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.11.0 (c (n "wread-data-mongodb") (v "0.11.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mongodb") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b083m7aqm4vr6q26dy7kfn9yfb2wsk9s683qjm7qgi4yf9kjqkj") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.12.0 (c (n "wread-data-mongodb") (v "0.12.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dsdwp9cq2c73wnzgbw4ih35p0jvkidpllncrjdaw6kbkfmi77vf") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.12.1 (c (n "wread-data-mongodb") (v "0.12.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vf824pk35ysdd8bn5imqcnp9i9yifndff7fkjvnmw2328kv2izi") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.12.2 (c (n "wread-data-mongodb") (v "0.12.2") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ggxg9538sc7az4a3p8x20wl1l9g5c0hpcghkhf8jgmmngh57g3i") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.12.3 (c (n "wread-data-mongodb") (v "0.12.3") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q15i1p9hxf5zvlclw8j76icl9n83a7gjf4y16356lcvjdybbj60") (f (quote (("write") ("read") ("default" "read"))))))

(define-public crate-wread-data-mongodb-0.13.0 (c (n "wread-data-mongodb") (v "0.13.0") (d (list (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1km18jpic4walw0mz3g748rmn83rx94bnhf8ww2mhwc7bavqay9d")))

