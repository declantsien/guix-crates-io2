(define-module (crates-io wr al wral) #:use-module (crates-io))

(define-public crate-wral-0.1.0 (c (n "wral") (v "0.1.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mkit") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "structopt") (r "^0.3.20") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0h6rdr5fnbsvc511lq9k2jyabyrldq66kyn9w5jld5xl1ba0l7ai") (f (quote (("perf" "structopt" "rand"))))))

(define-public crate-wral-0.2.0 (c (n "wral") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mkit") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "structopt") (r "^0.3.20") (o #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0mqmj5pgsa14r1arpmn2yapjn4dakgw83hx6d95kqd8lp41fykc0") (f (quote (("perf" "structopt" "rand"))))))

