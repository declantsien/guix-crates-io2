(define-module (crates-io wr ig wright_omega) #:use-module (crates-io))

(define-public crate-wright_omega-0.1.0 (c (n "wright_omega") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastapprox") (r "^0.3.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cgvr6frbsl2gnjgna7gcdz49d2l1jqvkd3f99004c9wnz2qjrfh") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-wright_omega-0.1.1 (c (n "wright_omega") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastapprox") (r "^0.3.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17nd3j8g87m2rdzgghvff97qnscr3zcs6cxygw7p2kj9s14p46c9") (f (quote (("f64") ("f32") ("default" "f32"))))))

