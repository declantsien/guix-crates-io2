(define-module (crates-io wr ig wright) #:use-module (crates-io))

(define-public crate-wright-0.0.1 (c (n "wright") (v "0.0.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0sdfmjlgqaslpscii1grsqwzwd978zc96njkq87insqhpzhc2z97")))

(define-public crate-wright-0.0.2 (c (n "wright") (v "0.0.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1qyg4xi5s3cnb3nf9hr9gka1a2jwxi4nvrldkg6jp0qm7ifmmyl1")))

(define-public crate-wright-0.0.3 (c (n "wright") (v "0.0.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "064rjbsjav1adbhxjzgnp5am7ja58h8gpvphgry80912hf6wqmll")))

(define-public crate-wright-0.0.4 (c (n "wright") (v "0.0.4") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0mpwjsikmvy7nyy5m565yalvk8mf203kdm7w6f39j6iym8dgv7a0")))

(define-public crate-wright-0.0.5 (c (n "wright") (v "0.0.5") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1bbifvlb88zqxmgpl7dpshywgd20sif8q1f1lqpvyqq7a1f77lk4")))

(define-public crate-wright-0.1.0 (c (n "wright") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0l463zqhl62zf98mpjirmy9hv0nsg6b6kpqvbl8hfk59ax0mi056")))

(define-public crate-wright-0.2.0 (c (n "wright") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1vkqrqkqlqhx55kyakhk2ybniw5h5lsxw4dc2za55g7qd3b1m85x")))

(define-public crate-wright-0.2.1 (c (n "wright") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ji0zlgckajb8v63lq0a2y6hfkw5akr02m3wl91cx2gp81nl141c")))

(define-public crate-wright-0.2.2 (c (n "wright") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1lk1qvxhgd2qnx914yz2dqv403wpay08153h7qa1pmivdw3i7y0l")))

(define-public crate-wright-0.2.3 (c (n "wright") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0i1klsl8mf1154r3q9nb96lqgx53pscfzmhswkwipbsp1xriyz84")))

(define-public crate-wright-0.3.0 (c (n "wright") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1fr3bp3q5h65d2f2ngb1gnaw0kwsxgcndz7cxaz0a9xsx532j1vy")))

(define-public crate-wright-0.4.0 (c (n "wright") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0pvqffwrci36fkvx81zznjdq7pdaq1psjr6c5ymn9zqhg1ig1cfp")))

(define-public crate-wright-0.4.1 (c (n "wright") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "18hc4nsfhjb10hkafjd7p7hsp3iwvgf15df4dx7mzj56z2qbjbi1")))

(define-public crate-wright-0.4.2 (c (n "wright") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "14nmn1j9h502y5r6cdjlfhqcpbkmcywal4il8nzn5avsz91rwvn7")))

(define-public crate-wright-0.5.0 (c (n "wright") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0n430h7hdi832hxpcry5fn915v89ybxcak0m4s5ni0qclf67xa02")))

(define-public crate-wright-0.6.0 (c (n "wright") (v "0.6.0") (d (list (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1c2j3xqbi72p0j1y6359wbyma5sa9bsph13yc4447ngpy0w2phcp")))

(define-public crate-wright-0.6.1 (c (n "wright") (v "0.6.1") (d (list (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "02k2l1g1gvh7q9nb21dbiy58j5jczmywcph972zcvdjajbj3mf2v")))

(define-public crate-wright-0.6.2 (c (n "wright") (v "0.6.2") (d (list (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1m94mhknvsqzl042cdc2vql3704wk6g67bf7rj5m702z4mq2v3ii")))

(define-public crate-wright-0.7.0 (c (n "wright") (v "0.7.0") (d (list (d (n "brain-brainfuck") (r "^1.3.0") (d #t) (k 0)) (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27") (d #t) (k 0)) (d (n "regex") (r "~0.2.0") (d #t) (k 0)) (d (n "wasmi") (r "^0.1.1") (d #t) (k 0)))) (h "14js4lzbsi57870kg7mfibyk2hcybs66w8ar2bg1alm25xmczf53")))

(define-public crate-wright-0.7.1 (c (n "wright") (v "0.7.1") (d (list (d (n "brain-brainfuck") (r "^1.3.0") (d #t) (k 0)) (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "~0.2.0") (d #t) (k 0)))) (h "0lrndghw9895zbpbvkx8zf5nc8b7kyv1jslv0x5qxhkfx6prcw6n")))

(define-public crate-wright-0.7.2 (c (n "wright") (v "0.7.2") (d (list (d (n "brain-brainfuck") (r "^1.3.0") (d #t) (k 0)) (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "~0.2.0") (d #t) (k 0)))) (h "0jjilk4vawi1v4q6fh2340kabljnx7091dh857hvm59nhsg1zi6h")))

(define-public crate-wright-0.7.3 (c (n "wright") (v "0.7.3") (d (list (d (n "brain-brainfuck") (r "^1.3.0") (d #t) (k 0)) (d (n "clap") (r "~2.31.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "~0.2.0") (d #t) (k 0)))) (h "10xzxxq8hrkvrdllv1pjpnma7d4zhpgbii0y58df17148zpyvny0")))

(define-public crate-wright-0.8.0 (c (n "wright") (v "0.8.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "codespan") (r "^0.5") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.5") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gfs3bb7nar0h8b47gjc5r9qk5jdkln583z48pf6gxyg0mcq04vq")))

