(define-module (crates-io wr pc wrpc-transmission) #:use-module (crates-io))

(define-public crate-wrpc-transmission-0.0.0 (c (n "wrpc-transmission") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "1hm9k0z6rmq85y9pzghlwjwbx6wg56mvbj26fyhwb0vg25zkyjr2")))

