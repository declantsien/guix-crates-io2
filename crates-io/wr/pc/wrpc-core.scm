(define-module (crates-io wr pc wrpc-core) #:use-module (crates-io))

(define-public crate-wrpc-core-0.0.0 (c (n "wrpc-core") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "0psdccbmnxg9s5x7mjcb00kyr33wqwdqqlgm629j3x1j6cihxzwa")))

