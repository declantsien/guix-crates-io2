(define-module (crates-io wr pc wrpc-nats) #:use-module (crates-io))

(define-public crate-wrpc-nats-0.0.0 (c (n "wrpc-nats") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "18840mwdj34rmqssrcfqxa9riki7qdykgx0ayc49i22pxdcwmm0d")))

