(define-module (crates-io wr pc wrpc-transport-derive) #:use-module (crates-io))

(define-public crate-wrpc-transport-derive-0.1.0 (c (n "wrpc-transport-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "async-trait") (r "^0.1") (k 0)) (d (n "bytes") (r "^1") (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "wrpc-transport") (r "^0.21") (k 0)) (d (n "wrpc-transport-derive-macro") (r "^0.1") (k 0)))) (h "0yvi2wxgh883s7a90khrikv9zbqiz8kyphqvc7alldshlrw6i5ry")))

