(define-module (crates-io wr pc wrpc-transport-derive-macro) #:use-module (crates-io))

(define-public crate-wrpc-transport-derive-macro-0.1.0 (c (n "wrpc-transport-derive-macro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits" "printing" "clone-impls"))) (k 0)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "env-filter"))) (k 0)))) (h "0n2cc29mivadh3djnafyc0hyhhjfnj25zamswmwa77sp52scilvp")))

