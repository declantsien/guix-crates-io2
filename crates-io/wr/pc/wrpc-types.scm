(define-module (crates-io wr pc wrpc-types) #:use-module (crates-io))

(define-public crate-wrpc-types-0.1.0 (c (n "wrpc-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "1p4knzyvvvl1y1w0zw7va7y5k865gnjpi2b7b564qqxq9l4k6ds5")))

(define-public crate-wrpc-types-0.2.0 (c (n "wrpc-types") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "0balf43w4dp9klwz3c71j3viwsa9x3qawi4bqbadccb5wjn23y86")))

(define-public crate-wrpc-types-0.2.1 (c (n "wrpc-types") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "05rcvv8axraf4zac246w8dvn2x9dai108j5w9qs5r2wfds8alfmg")))

(define-public crate-wrpc-types-0.2.2 (c (n "wrpc-types") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "1ga0dk4jai0llhn2bfn0qmzy5scq7ap8xm8zjq7da0sqs3anhv2j")))

(define-public crate-wrpc-types-0.2.3 (c (n "wrpc-types") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "0zdi97ax16x6rfi6az85bg4zaqvs5l9i3hic1vwbfm75yx8442ky")))

(define-public crate-wrpc-types-0.3.0 (c (n "wrpc-types") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "16dkzfxlb64hfxrdy45z6ylqnb7kwidn7p424ji1avqdbq4n9vp7")))

(define-public crate-wrpc-types-0.4.0 (c (n "wrpc-types") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.13") (k 0)))) (h "1mgvjd099caaagk5hfrgqwrg26fd7h02ym4196gp9vzd1pb5h1lv")))

(define-public crate-wrpc-types-0.5.0 (c (n "wrpc-types") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.201") (k 0)))) (h "0zj5wr3zv6v6cv12lj7zxbfx1idd1dk5c5l0jmv40dm9wnvd6b85")))

(define-public crate-wrpc-types-0.6.0 (c (n "wrpc-types") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("attributes"))) (k 0)) (d (n "wit-parser") (r "^0.202") (k 0)))) (h "0wfa9ki62f6dcn62i65q57sl1ngg55g5qh3dnlxn82q0qgs08lqb")))

