(define-module (crates-io wr pc wrpc-cli) #:use-module (crates-io))

(define-public crate-wrpc-cli-0.1.0 (c (n "wrpc-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "async-nats") (r "^0.33") (o #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "fmt" "smallvec" "tracing-log"))) (k 0)))) (h "1rzlwr0gzp2zsnh5q7j2hd6qv09klz79wmw79l9pwicmzx47ck58") (f (quote (("default" "nats")))) (s 2) (e (quote (("nats" "dep:async-nats" "dep:tokio" "tokio/sync"))))))

