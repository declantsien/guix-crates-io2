(define-module (crates-io wr ou wrought) #:use-module (crates-io))

(define-public crate-wrought-0.1.0 (c (n "wrought") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codespan") (r "^0.11.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.0") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "1w2fg05hx1hvx5jkfaqzs6gcyifzddcr57sa23vlf8569c0zn3gp")))

(define-public crate-wrought-0.1.1 (c (n "wrought") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codespan") (r "^0.11.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.0") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "1m7yp63fnbxpci7r3y7cymjpc3lqffir166a4mw199c8b1lrdydc")))

(define-public crate-wrought-0.1.2 (c (n "wrought") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codespan") (r "^0.11.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.0") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "03ksb9l8902h5b228g2daag0dwa1n2i4v2iywrv8ixswca4csjrj")))

