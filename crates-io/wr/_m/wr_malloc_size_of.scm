(define-module (crates-io wr _m wr_malloc_size_of) #:use-module (crates-io))

(define-public crate-wr_malloc_size_of-0.0.1 (c (n "wr_malloc_size_of") (v "0.0.1") (d (list (d (n "app_units") (r "^0.7") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)))) (h "04bz7bgkrhjnlql9bay3kx3skpcl6sk6n14d3dk1dy0kiagksxx8")))

(define-public crate-wr_malloc_size_of-0.1.0 (c (n "wr_malloc_size_of") (v "0.1.0") (d (list (d (n "app_units") (r "^0.7") (d #t) (k 0)) (d (n "euclid") (r "^0.20") (d #t) (k 0)))) (h "129wb6g3dz8wdkbcz2jxbcahk07mh12vlgh53cg5khca3ppcx0yv")))

