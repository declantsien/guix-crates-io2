(define-module (crates-io wr rm wrrm) #:use-module (crates-io))

(define-public crate-wrrm-0.1.0 (c (n "wrrm") (v "0.1.0") (d (list (d (n "atomicbox_nostd") (r "^0.3.1") (d #t) (k 0)))) (h "0741lkph2hprmmlys7b169vnpdf3dy034ijl3jc52nw90sg8knzb")))

(define-public crate-wrrm-0.1.1 (c (n "wrrm") (v "0.1.1") (d (list (d (n "atomicbox_nostd") (r "^0.3.1") (d #t) (k 0)))) (h "15lwm023rmf3g508bqz7mwkjqr55h3rjy5jyq74lbg1bcwixr3k1")))

(define-public crate-wrrm-1.0.0 (c (n "wrrm") (v "1.0.0") (d (list (d (n "atomicbox_nostd") (r "^0.3.1") (d #t) (k 0)))) (h "0sf4r1zk0hsghr1dgvxy15jg6gjmxs4birdhdr4k18cl47mx1s1y")))

