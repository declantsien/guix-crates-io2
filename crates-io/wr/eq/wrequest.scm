(define-module (crates-io wr eq wrequest) #:use-module (crates-io))

(define-public crate-wrequest-0.1.0 (c (n "wrequest") (v "0.1.0") (d (list (d (n "case_insensitive_hashmap") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)) (d (n "wcookie") (r "^0.1.2") (d #t) (k 0)))) (h "1abvphpx0gpqmgf6qyky11f0clkyyik9r9zc37wpj1d1cjwawf6c")))

(define-public crate-wrequest-0.3.0 (c (n "wrequest") (v "0.3.0") (d (list (d (n "case_insensitive_hashmap") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "wcookie") (r "^0.1.2") (d #t) (k 0)))) (h "03s2pkfwp4n84aj97pvfqffgf83dwzww9xildmgjfmd5qczl02w1")))

(define-public crate-wrequest-0.4.0 (c (n "wrequest") (v "0.4.0") (d (list (d (n "case_insensitive_hashmap") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)) (d (n "wcookie") (r "^0.1.2") (d #t) (k 0)))) (h "0bnsgnpdy5flyw3qk9li2rkbdlnab4kza5nv3a1vi94xc72r2lss")))

(define-public crate-wrequest-0.4.1 (c (n "wrequest") (v "0.4.1") (d (list (d (n "case_insensitive_hashmap") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)) (d (n "wcookie") (r "^0.1.2") (d #t) (k 0)))) (h "11rmzyh5a97nkhihiiiyrnv4syl3kq34wy84cabm00hhj4s95j46")))

(define-public crate-wrequest-0.4.2 (c (n "wrequest") (v "0.4.2") (d (list (d (n "case_insensitive_hashmap") (r "^1.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "wcookie") (r "^0.1.2") (d #t) (k 0)))) (h "0cjvz814xn61dk6884l5z7sfavbzxplgkrpz25n7yyqdbkcdk6mx")))

