(define-module (crates-io wr ap wrapped_slab) #:use-module (crates-io))

(define-public crate-wrapped_slab-0.1.0 (c (n "wrapped_slab") (v "0.1.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "wrapped_slab_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1syypn431dkhrj8mmz639gs9m0yw6397vjgwx5c915mig67icw9p") (r "1.62")))

(define-public crate-wrapped_slab-0.1.1 (c (n "wrapped_slab") (v "0.1.1") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "wrapped_slab_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0ba5h55kklxyb7d0xikk5n6ia12dn1nf6afzc811zvd7hn0vvz19") (r "1.62")))

(define-public crate-wrapped_slab-0.2.0 (c (n "wrapped_slab") (v "0.2.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "wrapped_slab_derive") (r "^0.2.0") (d #t) (k 0)))) (h "117y8cxj48cj9j8ip12cq4jcx3n0y3hf78izc62xjyf7r0fy16xy") (r "1.62")))

(define-public crate-wrapped_slab-0.2.1 (c (n "wrapped_slab") (v "0.2.1") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "wrapped_slab_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1qp4iyn60jmg6pigc9zhz187jn8q28rikljrhfgspcnigp731gj7") (r "1.62")))

(define-public crate-wrapped_slab-0.2.2 (c (n "wrapped_slab") (v "0.2.2") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "wrapped_slab_derive") (r "^0.2.2") (d #t) (k 0)))) (h "085as7bdydcppp410xj5jrzp7gqhj79k6ladc3zgx0aykl7bgqs8") (r "1.62")))

