(define-module (crates-io wr ap wrap-match-impl) #:use-module (crates-io))

(define-public crate-wrap-match-impl-1.0.3 (c (n "wrap-match-impl") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "fold" "clone-impls" "proc-macro" "extra-traits"))) (k 0)))) (h "0k4l7r22p8lnvksfccsifp9f24f7bi4jl81sf2zj92js1nrf030s")))

(define-public crate-wrap-match-impl-1.0.4 (c (n "wrap-match-impl") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "fold" "clone-impls" "proc-macro" "extra-traits"))) (k 0)))) (h "1yg7lqjp62x04sp7fhi60d4apsvly2ywjhsy6rz70k2jc0km2bc4")))

(define-public crate-wrap-match-impl-1.0.5 (c (n "wrap-match-impl") (v "1.0.5") (d (list (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "fold" "clone-impls" "proc-macro"))) (k 0)))) (h "0xcb4d95k5a38xb7263h78mbknrliavz9fbgsar6jsc6rvrzar6r") (f (quote (("tracing") ("default"))))))

(define-public crate-wrap-match-impl-1.0.6 (c (n "wrap-match-impl") (v "1.0.6") (d (list (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "fold" "clone-impls" "proc-macro"))) (k 0)))) (h "02wgjb0jla52sihkpli92ly4kwwwh9r8scxfzx9facdzvkjvmir8") (f (quote (("tracing") ("default"))))))

(define-public crate-wrap-match-impl-1.0.7 (c (n "wrap-match-impl") (v "1.0.7") (d (list (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "fold" "clone-impls" "proc-macro"))) (k 0)))) (h "02dx23r0azxibvx60lwqbcxhrzdrgvklddfig731487dvlrg410x") (f (quote (("tracing") ("default"))))))

