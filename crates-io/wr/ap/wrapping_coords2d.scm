(define-module (crates-io wr ap wrapping_coords2d) #:use-module (crates-io))

(define-public crate-wrapping_coords2d-0.1.0 (c (n "wrapping_coords2d") (v "0.1.0") (h "15x2yi0j2033940370b4vly2p6x3931jgg22p90axrx7846cqxrk")))

(define-public crate-wrapping_coords2d-0.1.1 (c (n "wrapping_coords2d") (v "0.1.1") (h "0h43rqaqwz5rkf0rphwpzym1mnxyjgclvcsgvcwmb4cvyf568bni")))

(define-public crate-wrapping_coords2d-0.1.2 (c (n "wrapping_coords2d") (v "0.1.2") (h "0m5i2zvp35gskwgvgyyklpy3sqxvav92myd0ildbw7k0msk9vyz8")))

(define-public crate-wrapping_coords2d-0.1.3 (c (n "wrapping_coords2d") (v "0.1.3") (h "1q9dr2ncz8l42k26qwa6x7s21kv6yjyv0cvgklzy8yin0chgs5jq")))

(define-public crate-wrapping_coords2d-0.1.4 (c (n "wrapping_coords2d") (v "0.1.4") (h "1cxgk3gnqif3arg26hjpjy3d98kzxpqcz54r9xj451kf0jzym6la")))

(define-public crate-wrapping_coords2d-0.1.5 (c (n "wrapping_coords2d") (v "0.1.5") (h "14afa6q03iirm15nw0061dl70y3wwha5jl9qm8v7qacvc3hnydf9")))

(define-public crate-wrapping_coords2d-0.1.6 (c (n "wrapping_coords2d") (v "0.1.6") (h "1aa0cx26vy0pzbhwla0f6181ahnb4idkqa40f2k8jlk2ih5x3vnw")))

(define-public crate-wrapping_coords2d-0.1.7 (c (n "wrapping_coords2d") (v "0.1.7") (h "1i0ixgi7nskdhwlz26n44wlyfp2zk9yxn7k5aqi6d9f2s0rw515w")))

(define-public crate-wrapping_coords2d-0.1.8 (c (n "wrapping_coords2d") (v "0.1.8") (h "1fckb4crnj23p7n5xqc8frblq1jjbyqy37z39szm5zr0ly168ir6")))

(define-public crate-wrapping_coords2d-0.1.9 (c (n "wrapping_coords2d") (v "0.1.9") (h "0ays8kz68ls5r15am0bc69anvybkaxxvq2p5d8c5xm7fvq2zbn3y")))

(define-public crate-wrapping_coords2d-0.1.10 (c (n "wrapping_coords2d") (v "0.1.10") (h "0m286c9rnrckgq865apacagl5v08w0xx4fqb897dxdg0kr1xm9b2")))

