(define-module (crates-io wr ap wrapgen) #:use-module (crates-io))

(define-public crate-wrapgen-0.1.0 (c (n "wrapgen") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1550cyv2ig55qdjxy7jm9rwd5hpm39d6y00vkz72d10n1sl7kj58")))

(define-public crate-wrapgen-0.1.1 (c (n "wrapgen") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "131n2l5zxjavf91f5nl9b6j16m96cg85zr33b46ha43rpjzy18vm")))

(define-public crate-wrapgen-0.2.0 (c (n "wrapgen") (v "0.2.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1mmm7n7vipjyh8di7f1p7vw1pkzgix6z1jy375jm52azdz3xk81z")))

