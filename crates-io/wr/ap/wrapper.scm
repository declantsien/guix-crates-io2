(define-module (crates-io wr ap wrapper) #:use-module (crates-io))

(define-public crate-wrapper-0.1.0 (c (n "wrapper") (v "0.1.0") (d (list (d (n "maybe-std") (r "^0.1.0") (d #t) (k 0)))) (h "0dax999pldns6sdhkvpx16nhh7fqn5nkckz77wp7a1hp25gnqbfl") (f (quote (("unstable" "maybe-std/unstable") ("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

(define-public crate-wrapper-0.1.1 (c (n "wrapper") (v "0.1.1") (d (list (d (n "maybe-std") (r "^0.1.2") (d #t) (k 0)))) (h "17c83kgkbzbp11n5707c0iy986x94qjlkmpngllpmwfjzgcc5fyj") (f (quote (("unstable" "maybe-std/unstable") ("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

