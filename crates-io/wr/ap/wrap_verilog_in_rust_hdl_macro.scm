(define-module (crates-io wr ap wrap_verilog_in_rust_hdl_macro) #:use-module (crates-io))

(define-public crate-wrap_verilog_in_rust_hdl_macro-0.1.0 (c (n "wrap_verilog_in_rust_hdl_macro") (v "0.1.0") (d (list (d (n "extract_rust_hdl_interface") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.44.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 2)))) (h "0wzibwx5lsc99x4fgjay5vv4qadk2ba4a5235cpky8v0ad2l78xa")))

(define-public crate-wrap_verilog_in_rust_hdl_macro-0.1.1 (c (n "wrap_verilog_in_rust_hdl_macro") (v "0.1.1") (d (list (d (n "extract_rust_hdl_interface") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rust-hdl") (r "^0.45.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.79") (d #t) (k 2)))) (h "1a03avv3kkmyjxbdaigsfhpvkhmw1x0q7rsbgm1v113lm9rvj0bz")))

