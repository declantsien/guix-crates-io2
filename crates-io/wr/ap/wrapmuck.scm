(define-module (crates-io wr ap wrapmuck) #:use-module (crates-io))

(define-public crate-wrapmuck-0.1.0 (c (n "wrapmuck") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1ls0kx6894qbddhd51z69yvfwhkhq53lb085j40rs4h4wpa4drz1")))

(define-public crate-wrapmuck-0.2.0 (c (n "wrapmuck") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0r7798rnapkx78jq8y7j8jwmga1nmj0zpg2ikjy0z3mr72f2w99r")))

