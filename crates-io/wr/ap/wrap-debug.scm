(define-module (crates-io wr ap wrap-debug) #:use-module (crates-io))

(define-public crate-wrap-debug-0.1.0 (c (n "wrap-debug") (v "0.1.0") (h "0zbqahrpmfkl1mxlc91rh8f2xmbi81sys2q4x8d2g57ixb7nyw23")))

(define-public crate-wrap-debug-0.1.1 (c (n "wrap-debug") (v "0.1.1") (h "0pr20fb4f7n9ar56i182jbrqbvmziga5ifxi1wxnnvjydprmcvfy")))

