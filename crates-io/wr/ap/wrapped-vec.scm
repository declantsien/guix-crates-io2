(define-module (crates-io wr ap wrapped-vec) #:use-module (crates-io))

(define-public crate-wrapped-vec-0.1.0 (c (n "wrapped-vec") (v "0.1.0") (h "1hdz0qw2acqbffbxxyd12d5j1jkpx6mw8c9g0ci5wyvsfi394ya3")))

(define-public crate-wrapped-vec-0.1.1 (c (n "wrapped-vec") (v "0.1.1") (h "01mxrlg8js3q8h0nfx1xxa9grkw3y2xj21711na44llmwp6w5fdn")))

(define-public crate-wrapped-vec-0.2.0 (c (n "wrapped-vec") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1zvr4185jrm9z2jcd22wvnmyqcz0wvv24094vr5dhmfd29bspbvw")))

(define-public crate-wrapped-vec-0.2.1 (c (n "wrapped-vec") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0icybwpsfzj71nmamaksr72sdjpgs1qg4q4vyy71qgg9mfs9phh6")))

(define-public crate-wrapped-vec-0.3.0 (c (n "wrapped-vec") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rdl013c0drbgqs70f8gkmlbp92yqz4i60p9w5lrd48y5iq0hpmq")))

