(define-module (crates-io wr ap wrapped-list) #:use-module (crates-io))

(define-public crate-wrapped-list-1.0.0 (c (n "wrapped-list") (v "1.0.0") (d (list (d (n "duplicate") (r "^1.0.0") (d #t) (k 2)))) (h "18ihvbxav7dk0anlk8n2asds28vwpvrvn753kvk8ns43qmnr1pqn")))

(define-public crate-wrapped-list-1.0.1 (c (n "wrapped-list") (v "1.0.1") (d (list (d (n "duplicate") (r "^1.0.0") (d #t) (k 2)))) (h "0dbyg6kpqll6cyyhvqi6lz062lhw7w5nkp0v5ljml3h2m0qi3n8r")))

