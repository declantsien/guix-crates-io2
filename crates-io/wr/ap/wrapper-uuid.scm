(define-module (crates-io wr ap wrapper-uuid) #:use-module (crates-io))

(define-public crate-wrapper-uuid-0.1.0 (c (n "wrapper-uuid") (v "0.1.0") (d (list (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "06w0k3b8l0zxn8a3v96gnd7sw6s03kwlskpmkw91pv275428qqxm")))

