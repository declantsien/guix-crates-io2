(define-module (crates-io wr ap wrapper-jsonwebtoken) #:use-module (crates-io))

(define-public crate-wrapper-jsonwebtoken-0.1.0 (c (n "wrapper-jsonwebtoken") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "darth-rust") (r "^0.4.7") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0wg7glijqj6g2kjp1aajksy4m6nhmnx3dgjsnk4sr0dz1n34rk69")))

(define-public crate-wrapper-jsonwebtoken-0.1.1 (c (n "wrapper-jsonwebtoken") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "darth-rust") (r "^0.4.7") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1037m0kr4ni4zixpanplglx52ni000a94x1mq7854dpnnslib1mq")))

