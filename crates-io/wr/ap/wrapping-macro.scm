(define-module (crates-io wr ap wrapping-macro) #:use-module (crates-io))

(define-public crate-wrapping-macro-0.1.0 (c (n "wrapping-macro") (v "0.1.0") (h "1jqbv9zrjpsy8gkx1dx079qlvd97453phadssivq7x9ssjbkqwwp") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-wrapping-macro-0.2.0 (c (n "wrapping-macro") (v "0.2.0") (h "0w5x3rkpx4jfz09mafv79vp2ffdyg1y1n5b0zkkn3q9ccr5b5fgm") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-wrapping-macro-0.2.1 (c (n "wrapping-macro") (v "0.2.1") (h "16pl0dgb24zgz6ni8sfw6hxg22nghj3p7khq9fzzlszncr66i3ai") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

