(define-module (crates-io wr ap wrap) #:use-module (crates-io))

(define-public crate-wrap-0.0.1 (c (n "wrap") (v "0.0.1") (h "1jpgg7cdk9ms48vpzn8xnvx5i2nv6il901ng9jr7vbvwbgw6siic")))

(define-public crate-wrap-0.1.0 (c (n "wrap") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "1ax1fzbs1a8k5jr1x8yahw3yz41hmkqir5xz19063bz85i8lh57y")))

