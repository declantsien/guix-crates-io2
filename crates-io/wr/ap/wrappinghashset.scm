(define-module (crates-io wr ap wrappinghashset) #:use-module (crates-io))

(define-public crate-wrappinghashset-0.4.1 (c (n "wrappinghashset") (v "0.4.1") (h "0kxr0mqs13sz4nn0b8iyhyjsmam9dsqm7kjj0brvsvb68asl10bm")))

(define-public crate-wrappinghashset-0.5.0 (c (n "wrappinghashset") (v "0.5.0") (h "08ihgjkn85z19cki97v6mx8h89ppvca7gfpc01qzw4ym6qrhv1jh")))

