(define-module (crates-io wr ap wrapping_proc_macro) #:use-module (crates-io))

(define-public crate-wrapping_proc_macro-1.0.0 (c (n "wrapping_proc_macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "02hm5grl1cw495inakvyw6nxwi7hxhb6l5zkqsqb4053s08c328n")))

