(define-module (crates-io wr ap wrapped_enum) #:use-module (crates-io))

(define-public crate-wrapped_enum-0.1.0 (c (n "wrapped_enum") (v "0.1.0") (h "1kq7srz6va44d0xs8v12s6y2xc52qjbc568c5kiznz6w8bn7b0wq") (y #t)))

(define-public crate-wrapped_enum-0.1.1 (c (n "wrapped_enum") (v "0.1.1") (h "04b7p3yjhj5hxalhxvly731hzy9zd6wj6ms5dr6q118pfj1siqbp") (y #t)))

(define-public crate-wrapped_enum-0.1.2 (c (n "wrapped_enum") (v "0.1.2") (h "0afgcc7sk0a9aqlz721wb5ndp284b7k6acvaq23i7ib50qw4ciz0")))

(define-public crate-wrapped_enum-0.1.3 (c (n "wrapped_enum") (v "0.1.3") (h "168nkbhnh0mzl47qn03qssg8v7q036ikxh7q1k7r1g4x1kxnqan0")))

