(define-module (crates-io wr ap wrapped_mono_macros) #:use-module (crates-io))

(define-public crate-wrapped_mono_macros-0.2.0 (c (n "wrapped_mono_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f0k2fm5anprl0sgsxbfvfvm5dnl8nri7p4jzl0anw86hk0ih6iy") (f (quote (("dump_macro_results"))))))

(define-public crate-wrapped_mono_macros-0.3.0 (c (n "wrapped_mono_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0na0p3akg2iffp289gyayjrhd1583lv7fp5n1gi0srs8sah952z9") (f (quote (("dump_macro_results"))))))

