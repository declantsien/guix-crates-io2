(define-module (crates-io wr ap wrapped-azero) #:use-module (crates-io))

(define-public crate-wrapped-azero-1.0.0 (c (n "wrapped-azero") (v "1.0.0") (d (list (d (n "ink") (r "=4.3.0") (k 0)) (d (n "psp22") (r "=0.2.2") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.9") (f (quote ("derive"))) (o #t) (k 0)))) (h "13524iynf7nhi8hm0gpdab6l6vqcc2hhgp141hbfxl3fc10pq2yb") (f (quote (("std" "ink/std" "scale/std" "scale-info/std" "psp22/std") ("ink-as-dependency") ("default" "std"))))))

