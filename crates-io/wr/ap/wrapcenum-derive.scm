(define-module (crates-io wr ap wrapcenum-derive) #:use-module (crates-io))

(define-public crate-wrapcenum-derive-0.1.0 (c (n "wrapcenum-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0blqm57nxxiyp5x71nzsshl29kl88cs3az6ydlaczfv8f8c5sjav")))

(define-public crate-wrapcenum-derive-0.2.0 (c (n "wrapcenum-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1yqjlh81vhyr1pzcgid4w3drj878j7rkclj5k59gpjcyllwwhbdm")))

(define-public crate-wrapcenum-derive-0.3.0 (c (n "wrapcenum-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1gvd8wj5nn76l92xklmrjsh9nlqg7s8jzf834rfq36qsymywqm7v")))

(define-public crate-wrapcenum-derive-0.4.0 (c (n "wrapcenum-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v0aqs935881g1ar5hw0a3i14mw3ckqqn4d45b8knb5dhmf0dk3b")))

(define-public crate-wrapcenum-derive-0.4.1 (c (n "wrapcenum-derive") (v "0.4.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vi67wxkpw40cch5jx010wvwf0rjc5f13yxfzd60acimadcz4vx7")))

