(define-module (crates-io wr ap wrapped_slab_derive) #:use-module (crates-io))

(define-public crate-wrapped_slab_derive-0.1.0 (c (n "wrapped_slab_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "061hzf1r7kniny3qb9k26a0ma7pag8msmkpyfhz9cszy68cxmbhv") (r "1.62")))

(define-public crate-wrapped_slab_derive-0.1.1 (c (n "wrapped_slab_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sp62h9z0lgicl3c2crvagrcjnz4sk4bbhxr3q12bwid4sr18jsj") (r "1.62")))

(define-public crate-wrapped_slab_derive-0.2.0 (c (n "wrapped_slab_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18kipp0wmif4697jm25189cslh7j8ww4vj2n5pxrxvbyw0jqqkyz") (r "1.62")))

(define-public crate-wrapped_slab_derive-0.2.1 (c (n "wrapped_slab_derive") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zzdr9ymv4z59i0j4bnq6g1hqi55kzs6904aib83fsbg512aqb5y") (r "1.62")))

(define-public crate-wrapped_slab_derive-0.2.2 (c (n "wrapped_slab_derive") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10lvhar3gp7zawhvxk5jm9zvsm567rc6plpr9rnvjjkynwms8l29") (r "1.62")))

