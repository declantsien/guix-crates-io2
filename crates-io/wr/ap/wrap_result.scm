(define-module (crates-io wr ap wrap_result) #:use-module (crates-io))

(define-public crate-wrap_result-0.1.0 (c (n "wrap_result") (v "0.1.0") (h "0qb7aag73cdy2yly1z0iipayicvfpm19a58pmz4s872zlrpcmsvy") (y #t)))

(define-public crate-wrap_result-0.1.1 (c (n "wrap_result") (v "0.1.1") (h "1i2p6gi5kg8ixzc085r956xl51akhvh3w75wlv9gvq4i5wk8b7lh")))

