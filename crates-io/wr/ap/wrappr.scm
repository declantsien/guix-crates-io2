(define-module (crates-io wr ap wrappr) #:use-module (crates-io))

(define-public crate-wrappr-1.0.0 (c (n "wrappr") (v "1.0.0") (h "01gf1g2flbbbxwzyf3miq9d86g0y75ql7dfzqxcq53dhgi9hk53h")))

(define-public crate-wrappr-1.1.0 (c (n "wrappr") (v "1.1.0") (h "0mlp4dq368k0c21alga9md82mvfbszqhnz03x68zaixfpr4j4z4x")))

(define-public crate-wrappr-1.1.1 (c (n "wrappr") (v "1.1.1") (h "1vsjdg3ig13kg7xfhmjc9gscgwpmj9dxvslz02gbsxgq79lii03z")))

(define-public crate-wrappr-1.2.0 (c (n "wrappr") (v "1.2.0") (h "0zk7dc8xs3fp8sc73yfl47pjj4wmxwyxhsv19bm3544l0k5hca0d")))

