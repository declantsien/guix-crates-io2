(define-module (crates-io wr ap wrapper-bcrypt) #:use-module (crates-io))

(define-public crate-wrapper-bcrypt-0.1.0 (c (n "wrapper-bcrypt") (v "0.1.0") (d (list (d (n "bcrypt") (r "^0.15.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "darth-rust") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1xygxp6rmr4n5ribndgr2g2iix5wx3cq0dc89xy7lk5yxr6vib3a")))

