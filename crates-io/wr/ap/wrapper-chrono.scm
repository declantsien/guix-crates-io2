(define-module (crates-io wr ap wrapper-chrono) #:use-module (crates-io))

(define-public crate-wrapper-chrono-0.1.0 (c (n "wrapper-chrono") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "darth-rust") (r "^0.4.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0rkqbwysa96rfcvmjclrg8rq88xahxynp367d1n98hjrfp78bfjm")))

