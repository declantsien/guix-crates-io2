(define-module (crates-io wr ap wrapping_arithmetic) #:use-module (crates-io))

(define-public crate-wrapping_arithmetic-0.1.0 (c (n "wrapping_arithmetic") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1z5330k7iphvcypn3pyaf0155qdg3zzqmcink5l76nj62wcgjh1w")))

