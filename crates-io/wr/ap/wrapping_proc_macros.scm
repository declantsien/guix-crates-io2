(define-module (crates-io wr ap wrapping_proc_macros) #:use-module (crates-io))

(define-public crate-wrapping_proc_macros-1.0.0 (c (n "wrapping_proc_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0fhp5rz4rbvlsb0z6l5y17kr83xdgch23cxqpcaxkc14i6jiwh77") (y #t)))

