(define-module (crates-io wr ap wrapping) #:use-module (crates-io))

(define-public crate-wrapping-0.1.0 (c (n "wrapping") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0m7ik14734c568vika8v810v78dyc957qmpmrfd3s3a6mghaqlp4") (f (quote (("default") ("array" "generic-array"))))))

(define-public crate-wrapping-0.1.1 (c (n "wrapping") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "1y44k6sf64qihfj9zw0m08avabl5vjgm1z4kvyqry5wb1ff54jbr") (f (quote (("default") ("array" "generic-array"))))))

(define-public crate-wrapping-0.1.2 (c (n "wrapping") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "1pa4z652yv40qafixz6rdg1s31hxbmqjpq36rq59d2pmpxn82qik") (f (quote (("default") ("array" "generic-array"))))))

(define-public crate-wrapping-0.2.0 (c (n "wrapping") (v "0.2.0") (h "1limkqc3vxwkjhmlf442hxyj20s3xwpn8gaglmsmwl8vsaldvif7")))

