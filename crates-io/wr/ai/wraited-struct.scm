(define-module (crates-io wr ai wraited-struct) #:use-module (crates-io))

(define-public crate-wraited-struct-0.1.0 (c (n "wraited-struct") (v "0.1.0") (h "1hb2hw6iif3j7v7ggml74ij5l6vq0w0l1f8h6j24np3cnfq7x757")))

(define-public crate-wraited-struct-0.2.0 (c (n "wraited-struct") (v "0.2.0") (h "1b9fws71yi3q57hr28afz3a36j1xmk25zmc16ngcfd7hiwsvm2ar")))

