(define-module (crates-io wr ak wrake) #:use-module (crates-io))

(define-public crate-wrake-0.1.0 (c (n "wrake") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std" "color"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "1y1jj790cc5zpgbnx356jv2rfrfsyr06aanrddpzxpbk1810bva9")))

(define-public crate-wrake-0.1.1 (c (n "wrake") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std" "color"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "01709a4m43wcsvimadcf1jfag2vw9x4y784zzicmn5h310v2jy7b")))

(define-public crate-wrake-0.2.0 (c (n "wrake") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std" "color"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "0f18fqkri8z97mw2wbp733mwxssdjj4rzqhq1b3ch21mlpgq96gd")))

(define-public crate-wrake-0.3.0 (c (n "wrake") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std" "color"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "1pnb3kly2171n7q71ldwg5wywsgp12xnqpi37hzvmqm8n39g225z")))

