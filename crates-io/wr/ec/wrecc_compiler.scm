(define-module (crates-io wr ec wrecc_compiler) #:use-module (crates-io))

(define-public crate-wrecc_compiler-0.1.0 (c (n "wrecc_compiler") (v "0.1.0") (h "0sn816j3gblb3h3i6fvj4za9q3sxblkc09a7q4why4kncdx8m609")))

(define-public crate-wrecc_compiler-0.2.0 (c (n "wrecc_compiler") (v "0.2.0") (h "1g3xaq1kfaa2yf6ivwf0h12y5dic9k82v116wqm031388v8fkrj5")))

