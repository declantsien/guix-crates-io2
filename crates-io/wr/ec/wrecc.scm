(define-module (crates-io wr ec wrecc) #:use-module (crates-io))

(define-public crate-wrecc-0.1.0 (c (n "wrecc") (v "0.1.0") (d (list (d (n "wrecc_compiler") (r "^0.1.0") (d #t) (k 0)))) (h "1y74pvd0baixij7wbxpf9kvr5pyjy8826zwln6s4fab2d13gka33")))

(define-public crate-wrecc-0.2.0 (c (n "wrecc") (v "0.2.0") (d (list (d (n "wrecc_compiler") (r "^0.2.0") (d #t) (k 0)))) (h "0ccvr7f5mn2gqfh21n71cvva7z68994adakl7hxgfvrkn7wgk5kf")))

