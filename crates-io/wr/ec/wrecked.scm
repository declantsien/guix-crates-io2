(define-module (crates-io wr ec wrecked) #:use-module (crates-io))

(define-public crate-wrecked-0.1.0 (c (n "wrecked") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "05s10bw71y82pphpjn0wsa8z8q9q8f26bdzjl8177c9m1g24bhkq") (y #t)))

(define-public crate-wrecked-0.1.1 (c (n "wrecked") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1yn36yd9v1knlsh7prgavmx879n09qc2vzgpm3qpkzfd096i21lf") (y #t)))

(define-public crate-wrecked-0.1.2 (c (n "wrecked") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "02bckrfn2xx7ab23fnc6g9p53jw70akzqa63hahky9h7jyydjhn9") (y #t)))

(define-public crate-wrecked-0.1.3 (c (n "wrecked") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "07am2bj3mdmbdnc7267p6yr0c8q6xkany9vz3zbkqmrh0gndhnqh") (y #t)))

(define-public crate-wrecked-0.1.4 (c (n "wrecked") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "02ax9sgi290x043mx015isv9v7fxp4plvcdina56fqaj6msqgs80") (y #t)))

(define-public crate-wrecked-0.1.5 (c (n "wrecked") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1cy79c6wnk9vgxasdpgiz0fvkzrcp35hkkdk80pv1fbwkkbfig58") (y #t)))

(define-public crate-wrecked-0.1.6 (c (n "wrecked") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0is48mv3ln8s79rg7hk2j3dl8dr747rbziaff9kfifyxgzfw0fdw") (y #t)))

(define-public crate-wrecked-0.1.7 (c (n "wrecked") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1g10lplfyhwniy24a9c6biivwf9w5g5lrf6adjsrk2nb654fb4kv") (y #t)))

(define-public crate-wrecked-0.1.8 (c (n "wrecked") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0gk6ds6j5cpldm4c2qnibchyvahpgj4nzakm67w28pdjljcfmnyg") (y #t)))

(define-public crate-wrecked-0.1.9 (c (n "wrecked") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "19fq49vpp16n2r0q9vqmmvswl6qhinck5pdh3vdn86crwjjp8nwq") (y #t)))

(define-public crate-wrecked-0.1.11 (c (n "wrecked") (v "0.1.11") (d (list (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "termios") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "05j89dqpl46q6s7k0yivzkyaic8k04b6bjhax2fyhs7ldf6r2jjz") (y #t)))

(define-public crate-wrecked-0.1.12 (c (n "wrecked") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1180gclr5nhi5pjhihv2ffvgkchfwd58sg4xjlxil8snbpfakf9w") (y #t)))

(define-public crate-wrecked-0.1.13 (c (n "wrecked") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0ya2qy30h27agmpcshzzvz0q720j6bgsyzh9266ib9llrvh6l35h") (y #t)))

(define-public crate-wrecked-0.1.14 (c (n "wrecked") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1avgjcpk192g5y87wg2bsmiingbrb5z6rxl4gxfy22pbhaf51v1z") (y #t)))

(define-public crate-wrecked-0.1.15 (c (n "wrecked") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1qy0d7nwycq151ip6cmzfpxw7n9f8rnv4mqh2ma4sdcsy26w47sr") (y #t)))

(define-public crate-wrecked-0.1.16 (c (n "wrecked") (v "0.1.16") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1as1l8833bwfc8vb3s8bbzr4g3pb5iqpzwfzmzxnjlmvdcf3y8mg") (y #t)))

(define-public crate-wrecked-0.1.17 (c (n "wrecked") (v "0.1.17") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0cf73pjf4masmkz15msmlyr0ak9jyl57l1mca3qy5s3jl7v8giyz") (y #t)))

(define-public crate-wrecked-0.1.18 (c (n "wrecked") (v "0.1.18") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0qrl85gzgiijf5x59827mf38rvaq6lwn9zj0bs7kjqhpqwhdj91l") (y #t)))

(define-public crate-wrecked-0.1.19 (c (n "wrecked") (v "0.1.19") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "106rf4bdkgan74cq54l6hp311d67cb20i5vl7vzzsnr0rfkjrqq4") (y #t)))

(define-public crate-wrecked-0.1.20 (c (n "wrecked") (v "0.1.20") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0h9rpwbv9r53p0ax1iq99nns56dp0cdg0xq1smi84vkm1n4hwhps") (y #t)))

(define-public crate-wrecked-0.1.21 (c (n "wrecked") (v "0.1.21") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1m3bairzq7idg5z4d2k8zxj9hv7ysk4h9wqgi07gcdk788ccc982") (y #t)))

(define-public crate-wrecked-0.1.22 (c (n "wrecked") (v "0.1.22") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1pxsh7fy56q3ijwbr1na80my8d4zk9yj8iqcg7v41n1dy84yhnl4") (y #t)))

(define-public crate-wrecked-0.1.23 (c (n "wrecked") (v "0.1.23") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0bn6ai1bc8hg1gndxjfw1rxh1c19h5bgg5bg73ysbzdxcx5zpgf2") (y #t)))

(define-public crate-wrecked-0.1.24 (c (n "wrecked") (v "0.1.24") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0waam02wcjjihnmyablasmgq3n1mga6g3aynavjm9jyj5yqhvnc2") (y #t)))

(define-public crate-wrecked-1.0.0 (c (n "wrecked") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "09q9c33347gcg8v7rfimfaf88ihbsc5jvxn6vbazh9fg11zsg0vk")))

(define-public crate-wrecked-1.0.1 (c (n "wrecked") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0vg0xysq64yzwrlh5pasrhv58ngd4d5l6xvg8h9v756bh72mfkzj") (y #t)))

(define-public crate-wrecked-1.0.2 (c (n "wrecked") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0hn8c0j4g5ibqql40z21z89wkb91jmljvp0sdxi4kh4hbiqb8vw5") (y #t)))

(define-public crate-wrecked-1.0.3 (c (n "wrecked") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0ly088jzy3dmdiqpv5n92dp2m7rqnyxi4ck59a2cl5zvk4r9zpvg") (y #t)))

(define-public crate-wrecked-1.0.4 (c (n "wrecked") (v "1.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1caww6jlhflyfam1qmaf8q1bjax9g21sr8039vslbxjxsl0k4p89") (y #t)))

(define-public crate-wrecked-1.0.5 (c (n "wrecked") (v "1.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "17lgzbcnddc5b4cwkcsdvc3kq8q7nnba03m31yi0dh14gl9vljqg") (y #t)))

(define-public crate-wrecked-1.0.6 (c (n "wrecked") (v "1.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "068rb6bncww122wk6bnyzzn3n9371mcjiwz8l6igb7fiq22ill56") (y #t)))

(define-public crate-wrecked-1.0.7 (c (n "wrecked") (v "1.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "13ycnb23sg5k31141vlf3kpylwv54y97zsydkbgjmgk5f1c7z1vb") (y #t)))

(define-public crate-wrecked-1.0.8 (c (n "wrecked") (v "1.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0ki2z58z8lwn3030f0yq3s2clzr4pz316kww03f6aigdzkwjk86l") (y #t)))

(define-public crate-wrecked-1.0.9 (c (n "wrecked") (v "1.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "1qd0sp4n0jn0lmxaxc4nfss0viv39cml546ra0wap7naiplp82xw") (y #t)))

(define-public crate-wrecked-1.0.11 (c (n "wrecked") (v "1.0.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "1ckxd6q757cgx38sqkfnb87ifgmrk4p4fglgqcb9jd1g010cm2sx") (y #t)))

(define-public crate-wrecked-1.0.12 (c (n "wrecked") (v "1.0.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "1clw4w0wlq1jlnrxkviq6zjk9bzlbl1dpq2naz4kll4r7l9ly9ni")))

(define-public crate-wrecked-1.1.0 (c (n "wrecked") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "0zc5zrdxayhcvl69f34vx0zfkav3l36v0g41iidaaqcjy87r5pnm")))

(define-public crate-wrecked-1.2.0 (c (n "wrecked") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (t "cfg(unix)") (k 0)) (d (n "terminal_size") (r "^0.1.13") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0x6hzpl5kzvbd3afg6d6s2qfc2nf2fcvkccwlcsakhxhl05szha3")))

