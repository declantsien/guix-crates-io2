(define-module (crates-io wr at wrath) #:use-module (crates-io))

(define-public crate-wrath-0.1.0 (c (n "wrath") (v "0.1.0") (d (list (d (n "insta") (r "^1.34.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)) (d (n "wrath-macros") (r "^0.1.0") (d #t) (k 0)))) (h "030m2146nwa2a5h74y454an620p90naq0n5zqvg9n2xd94h7qxpr")))

