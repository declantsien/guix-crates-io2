(define-module (crates-io wr at wrath-macros) #:use-module (crates-io))

(define-public crate-wrath-macros-0.1.0 (c (n "wrath-macros") (v "0.1.0") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0rij7i34s00p6bsrpj955sqa62zk1hbq8wmxvbr9agz06wkb9fkn")))

