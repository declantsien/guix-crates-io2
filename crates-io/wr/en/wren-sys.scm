(define-module (crates-io wr en wren-sys) #:use-module (crates-io))

(define-public crate-wren-sys-0.1.0 (c (n "wren-sys") (v "0.1.0") (h "0238ijapjh8a8v23cp5h87sjybq5wi99lg0bdqp5kn8pv5vki6n4")))

(define-public crate-wren-sys-0.1.1 (c (n "wren-sys") (v "0.1.1") (h "1dx6laiv0m68axhdlvk81ng9a90nir31g3pb0hiaj8v1mr3g4jrb")))

(define-public crate-wren-sys-0.1.2 (c (n "wren-sys") (v "0.1.2") (h "19x29ip0pvbp3lx1b1r376s7wjyk7yfj4930zi3fva705vsbhs87")))

(define-public crate-wren-sys-0.1.3 (c (n "wren-sys") (v "0.1.3") (h "1gw14929ldggcllfjccrhz49kz163c67qy8wbp97bf61di3b7v8a")))

(define-public crate-wren-sys-0.1.4 (c (n "wren-sys") (v "0.1.4") (h "1wcmx8wd4xp8xj09qydzbsgl7m5fnrvi893ckmdzq5sswa1q7wvn")))

(define-public crate-wren-sys-0.1.5 (c (n "wren-sys") (v "0.1.5") (h "1p3w5fljc0irvg4hzbnrpxwizbww3fhy0ighdxjn549l2v831lw8")))

(define-public crate-wren-sys-0.1.6 (c (n "wren-sys") (v "0.1.6") (h "188yn4551vhhr9y0dc0vg7wdpfxybds9cz855fwafi1p1dhzz34w")))

(define-public crate-wren-sys-0.1.7 (c (n "wren-sys") (v "0.1.7") (h "0mqgm8w5j77h86m1g0rvg6asyfchvhb284rmmfc1ybrp0ngdrxn3")))

(define-public crate-wren-sys-0.1.8 (c (n "wren-sys") (v "0.1.8") (h "12ck5f3zijyaawdwa2ahkldyvqaawr60y5i33crcl63mqa2wzpsn")))

(define-public crate-wren-sys-0.1.9 (c (n "wren-sys") (v "0.1.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1qdmnp7z28xyj37b7gls8mp55dpydwxcq6bdwrdr40k3dqk8r19p")))

(define-public crate-wren-sys-0.2.0 (c (n "wren-sys") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1v5v9w6af806fg5vml3i7837zk1rnmj3b2qdgm3flinjayvgwyg1")))

(define-public crate-wren-sys-0.2.1 (c (n "wren-sys") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "19p4zrlx2ws1j6ba21p3ammvcg3791akdyfhm95l9fpj4mzd5i4k")))

(define-public crate-wren-sys-0.2.2 (c (n "wren-sys") (v "0.2.2") (d (list (d (n "libc") (r "~0.2.11") (d #t) (k 0)))) (h "1dvi66c8sa7mbx7vz9c9j3zymq2d5d2v1yjg27km0zwj8343w8jg")))

(define-public crate-wren-sys-0.2.3 (c (n "wren-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04qharx1b1nnglvvcihps1klanrxdvbf5cngj80nflncxsy476xj")))

(define-public crate-wren-sys-0.2.4 (c (n "wren-sys") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14c9z0b6x25sbpm8qs3viza5pq39ksjl7l5h4r4crvk4ykgb4fl5")))

(define-public crate-wren-sys-0.2.5 (c (n "wren-sys") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17yinm7p53k5i060h23vbr0lzxhgagxip9vxnqqgcw7dz28chq7a")))

