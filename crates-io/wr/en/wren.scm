(define-module (crates-io wr en wren) #:use-module (crates-io))

(define-public crate-wren-0.1.0 (c (n "wren") (v "0.1.0") (d (list (d (n "wren-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0bz8hmbghn6r6rj8zfjip8byiiqslcmlppp1d6pawmlv9wwjcnci")))

(define-public crate-wren-0.1.2 (c (n "wren") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "wren-sys") (r "*") (d #t) (k 0)))) (h "153nhxfdgns430b0n0ph3z5b7wqla5z0rlxvvka0rhpkyjx0fqh8")))

(define-public crate-wren-0.1.3 (c (n "wren") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "wren-sys") (r "*") (d #t) (k 0)))) (h "02bbsl44p9zc1aagyfrqrwm40m09k6nwm3h54mrm1kp7bd3ncc5f")))

(define-public crate-wren-0.1.4 (c (n "wren") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nzb8krzgbl3nqiswq9qam2k5zgx6rzv2nj6d447w3j9pi2ia4m9")))

(define-public crate-wren-0.1.5 (c (n "wren") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "^0.2.3") (d #t) (k 0)))) (h "1p7g3ml6b3f0zkh0423d1sshjmck0rydyqxl5wipczc512m2nmiw")))

(define-public crate-wren-0.1.6 (c (n "wren") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "~0.2.3") (d #t) (k 0)))) (h "0f7vm2v7ib3y9d6iz16p52w61ka4phamdnvlz1ykr06n0bbp431k")))

(define-public crate-wren-0.1.7 (c (n "wren") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "~0.2.3") (d #t) (k 0)))) (h "00yr9d6a368apkafwk0v8w6dqmdhrdqs80fz5gikyrn0q05wy92h")))

(define-public crate-wren-0.1.8 (c (n "wren") (v "0.1.8") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "~0.2.3") (d #t) (k 0)))) (h "0bzba7lrk95s1n9vb54cg9bnhz2vbdp2xrivm957fwrpxwr7pif7")))

(define-public crate-wren-0.1.9 (c (n "wren") (v "0.1.9") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "~0.2.3") (d #t) (k 0)))) (h "0gj4f00i7ygcybnhv9j7rkyrgyx7k0zjag8a90xwcrgrnw7mia3q")))

(define-public crate-wren-0.1.10 (c (n "wren") (v "0.1.10") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "~0.2.3") (d #t) (k 0)))) (h "194lxf3jzizy9l5pcd034cqfipz27hl5g0yc1d4sr8xjipkrn60l")))

(define-public crate-wren-0.1.11 (c (n "wren") (v "0.1.11") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "~0.2.3") (d #t) (k 0)))) (h "1mg1q6svmis2qc6cfgq48sqx9aijj9rfz3ik4pk5l720gfnscdjq")))

(define-public crate-wren-0.1.12 (c (n "wren") (v "0.1.12") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wren-sys") (r "~0.2.3") (d #t) (k 0)))) (h "0q6i7v3zxjlkn1b8s1167avb6hrn9rpyf6klvjb9pz4g4fc3js05")))

