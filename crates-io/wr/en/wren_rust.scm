(define-module (crates-io wr en wren_rust) #:use-module (crates-io))

(define-public crate-wren_rust-0.1.0 (c (n "wren_rust") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1aj72b5k537aiyv2m9n29mvgb3rc08ipdz730a56km3iijr93yiw")))

(define-public crate-wren_rust-0.1.1 (c (n "wren_rust") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07kr01i3rgkbcai1s4jary81bxsv99g050lzgs9s8gy3bxzbaahm")))

(define-public crate-wren_rust-0.1.2 (c (n "wren_rust") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gjyfki378b3ikx36ddfq476l05hb6p1a2hl65lsj3blaqx2clzc")))

(define-public crate-wren_rust-0.1.3 (c (n "wren_rust") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14qsrdv8x7lxhpcfz0i3p9sq2yy3mfmls6azkkkiwza93kqdqabn")))

(define-public crate-wren_rust-0.2.0 (c (n "wren_rust") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1prz4l9pzpk7wq1yx225ymc3mndc0l73gxv5affdv954dr1dsb57") (y #t)))

