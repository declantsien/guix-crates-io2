(define-module (crates-io wr fl wrflib_shader_compiler) #:use-module (crates-io))

(define-public crate-wrflib_shader_compiler-0.0.1 (c (n "wrflib_shader_compiler") (v "0.0.1") (h "1gassh1g9j4zsg98z11a0gsydlr9qi9rn8qhnal4zwqdbc3qgs09")))

(define-public crate-wrflib_shader_compiler-0.0.2 (c (n "wrflib_shader_compiler") (v "0.0.2") (h "09dzlk5jp0akqw9jcividwj6acc6nmdclgiwaljwck9bw4v9zanx")))

(define-public crate-wrflib_shader_compiler-0.0.3 (c (n "wrflib_shader_compiler") (v "0.0.3") (h "08kwjn6gbavm7r7crz2gpnhihh1pgiphbklsarhm7q605zzzf0rw")))

