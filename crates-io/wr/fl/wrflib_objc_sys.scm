(define-module (crates-io wr fl wrflib_objc_sys) #:use-module (crates-io))

(define-public crate-wrflib_objc_sys-0.0.1 (c (n "wrflib_objc_sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1r2p3sk4dyhs8iskpfqxrvhd4yzk2shkrdj14wh2d8nsw1xw3wq7")))

(define-public crate-wrflib_objc_sys-0.0.2 (c (n "wrflib_objc_sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0fqkydvyrvsv8s3p79kl699cpyl5vp2smsrgmf3hlzz0n4ynv9kz")))

(define-public crate-wrflib_objc_sys-0.0.3 (c (n "wrflib_objc_sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "13q9hy5z4wrpz1g51cq1kjz46dn60h4xww3zwk3nd7m8k874fg88")))

