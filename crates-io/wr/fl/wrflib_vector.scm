(define-module (crates-io wr fl wrflib_vector) #:use-module (crates-io))

(define-public crate-wrflib_vector-0.0.1 (c (n "wrflib_vector") (v "0.0.1") (h "11y1ahh814jwygihrp3ya3ff10wm14y57f12j7qkz5mry167ym8c")))

(define-public crate-wrflib_vector-0.0.2 (c (n "wrflib_vector") (v "0.0.2") (h "13ymrbf5f3v4y5crq4mnybwflp6zvfc6bmx96y0qq9lpplmh6azd")))

(define-public crate-wrflib_vector-0.0.3 (c (n "wrflib_vector") (v "0.0.3") (h "1g99fqv6j5p1x2x8ak0vrgp337bni49av1wv24vpppjy5s4zsxjv")))

