(define-module (crates-io wr fl wrflib_widget) #:use-module (crates-io))

(define-public crate-wrflib_widget-0.0.2 (c (n "wrflib_widget") (v "0.0.2") (d (list (d (n "wrflib") (r "^0.0.2") (d #t) (k 0)))) (h "0r9lhvna73khq6gagai915ixvil6rsa5mx8py0nx02ddvkwkyigj")))

(define-public crate-wrflib_widget-0.0.3 (c (n "wrflib_widget") (v "0.0.3") (d (list (d (n "wrflib") (r "^0.0.3") (d #t) (k 0)))) (h "0zdckf2hhrwdnw16v2xpczgmww52baibna2yim16kpns5mjg34rr")))

