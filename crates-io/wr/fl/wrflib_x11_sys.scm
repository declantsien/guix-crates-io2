(define-module (crates-io wr fl wrflib_x11_sys) #:use-module (crates-io))

(define-public crate-wrflib_x11_sys-0.0.1 (c (n "wrflib_x11_sys") (v "0.0.1") (h "1skqifirrijzn77g2chww8dy5z3gwxs0222z3ackay2i87pkx8b5")))

(define-public crate-wrflib_x11_sys-0.0.2 (c (n "wrflib_x11_sys") (v "0.0.2") (h "0c7mcalwk638y3snj8ld2mdjy1hk6y9kcc78s2f9v7k042qgcxv7")))

(define-public crate-wrflib_x11_sys-0.0.3 (c (n "wrflib_x11_sys") (v "0.0.3") (h "0gsqabj5zlvlavijfjarnmddn2lg3cwvq4jgqnwzx94lk9577f0v")))

