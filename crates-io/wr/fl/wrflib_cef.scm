(define-module (crates-io wr fl wrflib_cef) #:use-module (crates-io))

(define-public crate-wrflib_cef-0.0.1 (c (n "wrflib_cef") (v "0.0.1") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "wrflib_cef_sys") (r "^0.0.1") (d #t) (k 0)))) (h "097p2gwlcmn933mbkx26ln85aap6z3liqixc8llmvr4bmpmbclr5") (f (quote (("debug" "wrflib_cef_sys/debug"))))))

(define-public crate-wrflib_cef-0.0.2 (c (n "wrflib_cef") (v "0.0.2") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "wrflib_cef_sys") (r "^0.0.2") (d #t) (k 0)))) (h "0f7lqcdl74hbh7lzkhnacs5yhby8zhlrgv5wqddbqw7ai063hwrx") (f (quote (("debug" "wrflib_cef_sys/debug"))))))

(define-public crate-wrflib_cef-0.0.3 (c (n "wrflib_cef") (v "0.0.3") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "wrflib_cef_sys") (r "^0.0.3") (d #t) (k 0)))) (h "1sifrsff3ffm6glpy3vsq8rg5h9v3gm0b1vdxz8ha137lfiydhhw") (f (quote (("debug" "wrflib_cef_sys/debug"))))))

