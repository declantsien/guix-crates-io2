(define-module (crates-io wr fl wrflib_glx_sys) #:use-module (crates-io))

(define-public crate-wrflib_glx_sys-0.0.1 (c (n "wrflib_glx_sys") (v "0.0.1") (h "0rvk0pm743045804dhzghhv1vksf5pwiax5h42p31cd79w21ppgc")))

(define-public crate-wrflib_glx_sys-0.0.2 (c (n "wrflib_glx_sys") (v "0.0.2") (h "05s8mbih02mmqwpnh3zixmlgwb803gl0mzzv5nrxvvp08ibd9fm9")))

(define-public crate-wrflib_glx_sys-0.0.3 (c (n "wrflib_glx_sys") (v "0.0.3") (h "13v5bzq2s4a2iayap53v00na9r0b2pwmvgg6g6b57xxc8p1fmjh8")))

