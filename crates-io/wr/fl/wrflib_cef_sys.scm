(define-module (crates-io wr fl wrflib_cef_sys) #:use-module (crates-io))

(define-public crate-wrflib_cef_sys-0.0.1 (c (n "wrflib_cef_sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)))) (h "1jbfx7pvdyp2mj5nfw9vx8wfjhwbxf5nwrjirpmqimwxvsmvd5vc") (f (quote (("debug")))) (l "cef")))

(define-public crate-wrflib_cef_sys-0.0.2 (c (n "wrflib_cef_sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)))) (h "12xhz0nilr1yd7j5si493cilbcpp4z4k9vva8xl0lsa3hpm4xwwj") (f (quote (("debug")))) (l "cef")))

(define-public crate-wrflib_cef_sys-0.0.3 (c (n "wrflib_cef_sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)))) (h "1pl1mh866sqmpx49dll97avqc97r9v2vfis7s9qfzdlhzb5saq2r") (f (quote (("debug")))) (l "cef")))

