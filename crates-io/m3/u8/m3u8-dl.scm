(define-module (crates-io m3 u8 m3u8-dl) #:use-module (crates-io))

(define-public crate-m3u8-dl-0.1.0 (c (n "m3u8-dl") (v "0.1.0") (d (list (d (n "aes-stream") (r "^0.2.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ffcli") (r "^0.1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "m3u8-rs") (r "^1.0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros" "rt-threaded" "fs" "sync"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0sc5166b9vjpqr9rw8ikwg59fw3smqjrfp2i0zwbr9gnd552y66p")))

