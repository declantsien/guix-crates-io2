(define-module (crates-io m3 u8 m3u8-rs) #:use-module (crates-io))

(define-public crate-m3u8-rs-1.0.0 (c (n "m3u8-rs") (v "1.0.0") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "07gx79iayh9a3mxn2mjam3rglb43gbp0542lxmfss5vhb3wyzw6r")))

(define-public crate-m3u8-rs-1.0.1 (c (n "m3u8-rs") (v "1.0.1") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "12sd5awhlbwpv77bdmcknyp2kcnzh3wvksjsd1l4l3bbz7n4vv95")))

(define-public crate-m3u8-rs-1.0.2 (c (n "m3u8-rs") (v "1.0.2") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "1s1p44pzaimbnkpnzhr3ki56l2p5m83yj8bl68xy98lsp9vf8f8d")))

(define-public crate-m3u8-rs-1.0.3 (c (n "m3u8-rs") (v "1.0.3") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "07j6jhlhviljk63hs5998vgxr4kyx4pv0b5a5lbxp2vg9s29nzvl")))

(define-public crate-m3u8-rs-1.0.4 (c (n "m3u8-rs") (v "1.0.4") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "11kchp86llrfaxsfla84nl5bp7lq1yd3plp8r1nyfs6dshkcxzkz")))

(define-public crate-m3u8-rs-1.0.5 (c (n "m3u8-rs") (v "1.0.5") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "0nnbscj4n0ix6yisdicvpfi91varm908jxlrg253gny6pnnqg883")))

(define-public crate-m3u8-rs-1.0.6 (c (n "m3u8-rs") (v "1.0.6") (d (list (d (n "nom") (r "^1.2.3") (d #t) (k 0)))) (h "1zp375jf8cl0lbz9sr9chx6z5lxg1mf379wfigrzsd8hlqv7fis0")))

(define-public crate-m3u8-rs-1.0.7 (c (n "m3u8-rs") (v "1.0.7") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "0zf1dz602hymgwnws7xx0fwn2p6a4n5s3mzghchy0gfvjln6c19p")))

(define-public crate-m3u8-rs-1.0.8 (c (n "m3u8-rs") (v "1.0.8") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "1rfhhq94nfw3drih42nz05q46kl1lmj94b7nl0jikka2pa9s6538")))

(define-public crate-m3u8-rs-2.0.0 (c (n "m3u8-rs") (v "2.0.0") (d (list (d (n "nom") (r "^5.1.0") (o #t) (d #t) (k 0)))) (h "0g14ph9hc2fbfsy7cw5f0xsy3vikh2gk9lr2skhih6r0x4j2qlcv") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-2.1.0 (c (n "m3u8-rs") (v "2.1.0") (d (list (d (n "nom") (r "^5.1.0") (o #t) (d #t) (k 0)))) (h "16xki50n4wdcw8lym0cb1i07gra6g9ldyxjfwh0xqgrn1rzcq56n") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-3.0.0 (c (n "m3u8-rs") (v "3.0.0") (d (list (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "0fw8rl3la787bj2zznraclgrqz6pjg1bvpndsrcqnhbz39whbzjh") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-3.0.1 (c (n "m3u8-rs") (v "3.0.1") (d (list (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1kbj4p46i08qlc334z1n0k47q6l3awxbvpq3d35451fv12dpgnsg") (f (quote (("parser" "nom") ("default" "parser")))) (y #t)))

(define-public crate-m3u8-rs-4.0.0 (c (n "m3u8-rs") (v "4.0.0") (d (list (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "0iyrqqlmfiir2lk5q954byj738453wcz15wc7kwi0zcf4y34lzy2") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-5.0.0 (c (n "m3u8-rs") (v "5.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1qvk8mf48mdv8qf74xbw1hp5ls9ybvdhca7hqirqwpawic39411b") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-5.0.1 (c (n "m3u8-rs") (v "5.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "0qax3xvzdkhdl37hzbaaq6n6vrv2g9lxvs53yaf80kwyiivfm3rm") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-5.0.2 (c (n "m3u8-rs") (v "5.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1knifvkrwhx40p9jqqwdiskcs3cdhdx3na05g10j1aflgy493l0m") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-5.0.3 (c (n "m3u8-rs") (v "5.0.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1z6gz9p0jmzcvjrr676hjzz4bggklj4kq39kaz5l8l483sbryfrc") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-5.0.4 (c (n "m3u8-rs") (v "5.0.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "0xcd8dpx8qkihj3g0vk1dbgx61v4kvmszjvd53in3afwbs2gi6nk") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-5.0.5 (c (n "m3u8-rs") (v "5.0.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1rz68577x5p1bmb7xz5dgpf9sqa46blma31iyhbjz9kydyl7n78c") (f (quote (("parser" "nom") ("default" "parser"))))))

(define-public crate-m3u8-rs-6.0.0 (c (n "m3u8-rs") (v "6.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1hw6da9b149pjxs47d4xd9i16q7pf2fdlp6lamvl9wmmbwrx6g7h") (f (quote (("parser" "nom") ("default" "parser"))))))

