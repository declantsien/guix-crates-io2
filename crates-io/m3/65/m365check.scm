(define-module (crates-io m3 #{65}# m365check) #:use-module (crates-io))

(define-public crate-m365check-0.1.0 (c (n "m365check") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z7zb77bzxq5sxm9pjki7qwswb5nfzswlphvsy50x8mqy1ma0apa")))

(define-public crate-m365check-0.2.0 (c (n "m365check") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jv7316h1shks4d25xj80km9zwmgwi3rb50mcsws322rpp93nj46")))

