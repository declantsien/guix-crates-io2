(define-module (crates-io m3 u_ m3u_cli_parser) #:use-module (crates-io))

(define-public crate-m3u_cli_parser-0.1.0 (c (n "m3u_cli_parser") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p86kwv3hs1a83mpmkp8f5zxl53wv364m5z2wfxikja6j9nh0cdf")))

