(define-module (crates-io m3 rs m3rs_snippets) #:use-module (crates-io))

(define-public crate-m3rs_snippets-0.0.1 (c (n "m3rs_snippets") (v "0.0.1") (d (list (d (n "prettier-please") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (d #t) (k 0)))) (h "01nm4xqvkrvy6s8y3jyffh30lfxnz1i5awcfhfsgy73waacc9l87")))

