(define-module (crates-io m3 rs m3rs_yew) #:use-module (crates-io))

(define-public crate-m3rs_yew-0.0.1 (c (n "m3rs_yew") (v "0.0.1") (d (list (d (n "color_utilities") (r "^0.1.1") (d #t) (k 0) (p "material_rs_color_utilities")) (d (n "core") (r "^0.0.1") (d #t) (k 0) (p "m3rs_core")) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1aac01ac5dgp3ffkmysagnxha2xxvn3mdhmk4vi0nvvvhvlfrnjc")))

