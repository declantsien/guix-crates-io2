(define-module (crates-io m3 rs m3rs) #:use-module (crates-io))

(define-public crate-m3rs-0.0.1 (c (n "m3rs") (v "0.0.1") (d (list (d (n "m3rs_core") (r "^0.0.1") (d #t) (k 0)) (d (n "m3rs_yew") (r "^0.0.1") (d #t) (k 0)))) (h "0fxmp30isn0b1rzmj322ym9wp5vg1nqrmspigarsw6a8a16g1pgy") (f (quote (("yew") ("core"))))))

