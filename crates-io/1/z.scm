(define-module (crates-io #{1}# z) #:use-module (crates-io))

(define-public crate-z-0.1.0 (c (n "z") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.1") (d #t) (k 0)) (d (n "signpost") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1yhsjmgf9x0c9d99qj5ds6sg5mlrky29zyc05yrl36z5xh5kw9qy") (f (quote (("nightly"))))))

(define-public crate-z-0.2.0 (c (n "z") (v "0.2.0") (h "0514xsbnkh6n6wh4nszlnyapjjp33r1x3cm6h81pk5c1fcbjfcii") (f (quote (("nightly"))))))

