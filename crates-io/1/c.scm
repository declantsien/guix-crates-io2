(define-module (crates-io #{1}# c) #:use-module (crates-io))

(define-public crate-c-0.0.1 (c (n "c") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ms2k5ga1lqzfa5ifn8virifva204a2xqyygcfzk0mwg996z8qlj") (y #t)))

(define-public crate-c-1.0.0 (c (n "c") (v "1.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1a0xxz5bl6hcqm8h3adr2wsf449pc7lj34ip55cqn5jf6cdlwf93") (y #t)))

