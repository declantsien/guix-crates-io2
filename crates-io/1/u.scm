(define-module (crates-io #{1}# u) #:use-module (crates-io))

(define-public crate-u-0.2.0 (c (n "u") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "06lqdgr7c6kfav0jnp26rjnv87bs6ac7palmh0m5442f6lcdl2ll") (f (quote (("schedule") ("default"))))))

