(define-module (crates-io #{1}# b) #:use-module (crates-io))

(define-public crate-b-0.1.0 (c (n "b") (v "0.1.0") (h "10rpjci51vj11syf3ryf6vdxdnvk8aadifbk966ylf1yf4l23q7s")))

(define-public crate-b-0.2.0 (c (n "b") (v "0.2.0") (h "1nms3kl4z6v7w1xlavg5d5m0wln290rcdmx93l0m20pzjhpq4wdd")))

