(define-module (crates-io #{1}# d) #:use-module (crates-io))

(define-public crate-d-0.0.0 (c (n "d") (v "0.0.0") (h "0ahw88pcha9637cwcwaxvcwyw4pxq3lgd1gi3sa6q4s3acc93bpz")))

(define-public crate-d-0.0.1 (c (n "d") (v "0.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)))) (h "0hfhy5v8xx961d7r6lb2s8750ll2dccxcizqrx2p7594j2iril5m")))

