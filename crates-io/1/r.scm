(define-module (crates-io #{1}# r) #:use-module (crates-io))

(define-public crate-r-0.0.1 (c (n "r") (v "0.0.1") (h "1kakxslbnpvq1hnmx74k4xv82yb5c3yl0nb4rkl956ji1gppay4f") (y #t)))

(define-public crate-r-0.0.2 (c (n "r") (v "0.0.2") (h "0x7srkhb7xvqv86zyy1kmwyszxmm04zmbg6wx58h082sxvgcfxg3") (y #t)))

(define-public crate-r-0.0.3 (c (n "r") (v "0.0.3") (h "1rn6p3mkrkmlphzfgnrgxj48vppbfa9ad8g1lsd2x1py6fmz698i") (y #t)))

(define-public crate-r-0.0.4 (c (n "r") (v "0.0.4") (h "0d09nn3qyd4zp8q8qv6c28zmnwl2x1cqaj2fx1m503pw3jqsa6q6") (y #t)))

(define-public crate-r-0.0.5 (c (n "r") (v "0.0.5") (h "1f2d0rw7x8lsc42f6za9y5f8ssn7snq7nlvfkmwb49fzkdf6br7h")))

(define-public crate-r-0.0.6 (c (n "r") (v "0.0.6") (h "0zxfiwczj67nqanq6i12b7f2hhn39d031dns5c0m9b7qghfm7qxz")))

(define-public crate-r-0.0.7 (c (n "r") (v "0.0.7") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "0c0frxgmwzaivskklzhbkf1i9n8jh34m2aq9jn9lrsdy3gp1cxd3")))

(define-public crate-r-0.0.9 (c (n "r") (v "0.0.9") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "17qrmpfiizha940x38yw4936y377divysxc629j0banb8kbkdsc0")))

