(define-module (crates-io #{1}# e) #:use-module (crates-io))

(define-public crate-e-0.1.0 (c (n "e") (v "0.1.0") (h "0gy30ck5yf01j5wnscb9gmiq49a0wccc0kwlspjmq6qjgmy32hqj") (y #t)))

(define-public crate-e-0.0.0 (c (n "e") (v "0.0.0") (h "1w9mh2532f5abnw3y7420lhhx9m4zrw8fj6i7kv9yq4xg4jnwr5s") (y #t)))

(define-public crate-e-0.1.1 (c (n "e") (v "0.1.1") (d (list (d (n "e_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1ligvd6bz40izxs77a8c9gygrn36bpkc3b2a5hdq6in3ll565z67") (f (quote (("macros") ("default" "macros")))) (y #t)))

(define-public crate-e-0.2.0 (c (n "e") (v "0.2.0") (d (list (d (n "e_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0rrwybqcpgfd6ixxq6bw0jpbl2gc6dzia0fxpvdvv13kjvg806am") (f (quote (("macros") ("default" "macros")))) (y #t)))

(define-public crate-e-0.2.1 (c (n "e") (v "0.2.1") (d (list (d (n "e_macro") (r "^0.2") (d #t) (k 0)))) (h "1vsph5zwprnbj6m9iws4in3qkb64i8gr3vx06l1d6qhngx6p52il") (f (quote (("macros") ("default" "macros")))) (y #t)))

(define-public crate-e-0.0.1 (c (n "e") (v "0.0.1") (h "0n5khp2f5gs9a2lvisd00s7ndr619lazr3s71rqw340bw1vdgyqw")))

