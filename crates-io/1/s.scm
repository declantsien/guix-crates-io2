(define-module (crates-io #{1}# s) #:use-module (crates-io))

(define-public crate-s-0.2.0 (c (n "s") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1s91hwynkhjjf0wnvk5yj2kp8fi5qy41cjjjjk7f8vigwp79l8vw") (f (quote (("schedule") ("default"))))))

