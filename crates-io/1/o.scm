(define-module (crates-io #{1}# o) #:use-module (crates-io))

(define-public crate-o-0.2.0 (c (n "o") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1yr7y4lyk2dqj8kl6yam9nm98msbqm73a83hl97wlk326bxvmnjh") (f (quote (("schedule") ("default"))))))

