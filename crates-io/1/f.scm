(define-module (crates-io #{1}# f) #:use-module (crates-io))

(define-public crate-f-0.2.0 (c (n "f") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1ra8fm62lp0b0kxwb8dq8sl8qxm2q4i1fixgjbgzi69m0fkpfkvl") (f (quote (("schedule") ("default"))))))

