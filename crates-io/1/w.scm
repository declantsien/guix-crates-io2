(define-module (crates-io #{1}# w) #:use-module (crates-io))

(define-public crate-w-0.2.0 (c (n "w") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0ga4zrwy4zk7zvr5xqwsmwvzk1s7fqm2d28pm9d94jhpmk1gzs79") (f (quote (("schedule") ("default"))))))

