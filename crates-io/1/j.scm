(define-module (crates-io #{1}# j) #:use-module (crates-io))

(define-public crate-j-0.1.0 (c (n "j") (v "0.1.0") (h "1d3jzzyyhl74qwmw9s5z6pgbafpakvjw2hk93dj8fwkw9nbwwhji")))

(define-public crate-j-0.1.1 (c (n "j") (v "0.1.1") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)))) (h "1rqfac89m0h94l1paif9gndasmgaka3clwvv04r93db3vy84ls1b")))

(define-public crate-j-0.1.2 (c (n "j") (v "0.1.2") (h "1vw6wdgm4xij4305iai7skkanl4znvi1ngriccqqc5bpavzy7ijg")))

(define-public crate-j-0.1.3 (c (n "j") (v "0.1.3") (h "1lrhhn6vllj0ifs4l5gx8if08xkmk7cq3hjw6g2g8xlpkw5cx8n0")))

(define-public crate-j-0.1.4 (c (n "j") (v "0.1.4") (h "1v7b8mq5flp5la47zywl2y6sq44ykarfqdjw9aiwmjivq2gwlm6a")))

(define-public crate-j-0.1.5 (c (n "j") (v "0.1.5") (h "0kp4nygs644zmx4ag3hdifb5afhddgb8jcjb12nasrj6bcfz4rgv")))

(define-public crate-j-0.2.0 (c (n "j") (v "0.2.0") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0ylrnfk2rxrf66r425lzzx3i177vbz9ki2iq0xp2jyppzygd559f")))

(define-public crate-j-0.2.1 (c (n "j") (v "0.2.1") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "03g7crb0c18022ap3cay50dgv92098ygsz1sgfd86kl5nb4qi0cq")))

(define-public crate-j-0.2.2 (c (n "j") (v "0.2.2") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "027a81hlff9nysf35vzk2ah1ma05jkbls59vrg8jf19xwniqvl1v")))

(define-public crate-j-0.2.3 (c (n "j") (v "0.2.3") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "04rlsc9zvmwqnad0j6a9d9gqidib51fd5s3p2ygmbwxkci3sdgz0")))

(define-public crate-j-0.2.4 (c (n "j") (v "0.2.4") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1n71bbw5bpxzx499npdsjyq6nxwc0fmbn3p7j3gnp1zbg7ryy35p")))

(define-public crate-j-0.2.5 (c (n "j") (v "0.2.5") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1syd2j2yfid638smwmrczawy5v4vf8wbw7cqkglxp575vrp844s4")))

(define-public crate-j-0.2.6 (c (n "j") (v "0.2.6") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "10cakvcxs8ynqsaz020gyys05x6plsq16y5rxcdy8axd3f3mjaxa")))

(define-public crate-j-0.2.7 (c (n "j") (v "0.2.7") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1yfrdd5k0nr8hnzcfqa9fzi05bsaffg6hk6x2v1kjwawb46366za")))

(define-public crate-j-0.2.8 (c (n "j") (v "0.2.8") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "078n64ab528mmgw1k5kc8d5mcwhly8cj0l2lw3f5mr1kk9hvlfd7")))

(define-public crate-j-0.2.9 (c (n "j") (v "0.2.9") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0x2rls1x1a2mgild87vqd3p8dyn5iys52gshdxvzlxly32yz8sw9")))

(define-public crate-j-0.2.10 (c (n "j") (v "0.2.10") (d (list (d (n "brev") (r "^0.1.6") (d #t) (k 0)) (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1r3baq35bqy64yyqba7aflcicfyx0642nsq6h8l4xkal15qz6hp9")))

(define-public crate-j-0.2.11 (c (n "j") (v "0.2.11") (h "0i67z2c0md4inyzw8nc4qblmmz7qqg55qclb59gv39jkr07lqbw9")))

