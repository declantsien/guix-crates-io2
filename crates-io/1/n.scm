(define-module (crates-io #{1}# n) #:use-module (crates-io))

(define-public crate-n-0.2.0 (c (n "n") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "17bmzs623vadfkgcxp9y91ky00gyg93ikcv8y9ii2qcykhmvwhpg") (f (quote (("schedule") ("default"))))))

