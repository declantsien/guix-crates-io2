(define-module (crates-io #{1}# m) #:use-module (crates-io))

(define-public crate-m-0.1.0 (c (n "m") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)))) (h "12ay7ka77gzpq3nk5diy27mjacpvkacbgdjm3fchpl5978z4zqp8")))

(define-public crate-m-0.1.1 (c (n "m") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)))) (h "0xynpz2iv6rkbz8srf9rfwa26inx3a93mh2qvn9k8ipkzf7196ci")))

