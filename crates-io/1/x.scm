(define-module (crates-io #{1}# x) #:use-module (crates-io))

(define-public crate-x-0.0.0 (c (n "x") (v "0.0.0") (h "07blwzb32a7n2xxx8z5bzaa9c64mhnrlhf7jq4qwkagmicy6h77d")))

(define-public crate-x-0.0.1 (c (n "x") (v "0.0.1") (h "1x2480vlmwjy3b5p7h8ys5lccamgmry0257kiv7dfna9v368wfd1")))

