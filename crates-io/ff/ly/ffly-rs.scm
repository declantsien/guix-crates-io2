(define-module (crates-io ff ly ffly-rs) #:use-module (crates-io))

(define-public crate-ffly-rs-0.0.0 (c (n "ffly-rs") (v "0.0.0") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "06jkzn1l2fhw0bsmn3120aakbdvvyc6hc36xp2m4qihykrv80w0j") (y #t)))

(define-public crate-ffly-rs-0.0.0-dev (c (n "ffly-rs") (v "0.0.0-dev") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "10v34ahjj0kcvs5xx47qbzfvapqpj4bq8brsmp055f5bld67q3z9")))

(define-public crate-ffly-rs-0.0.1 (c (n "ffly-rs") (v "0.0.1") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "199w4laa6z7jrr2bzs2489wssjqxip5f8hljf8k6yqz9dm9k4k5a")))

(define-public crate-ffly-rs-0.0.2 (c (n "ffly-rs") (v "0.0.2") (d (list (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qy07crkj6b8nj3lq85irwpfc3fjik55ljvgp5cwbg9am6k7jlda")))

(define-public crate-ffly-rs-0.0.3 (c (n "ffly-rs") (v "0.0.3") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.23") (d #t) (k 2)) (d (n "redis") (r "^0.21.5") (d #t) (k 2)) (d (n "skytable") (r "^0.7.1") (f (quote ("tokio" "aio"))) (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "19826jbg6af59f6w26cxif1s0wxa2kayv1rv4diallsrdhpqix3k")))

(define-public crate-ffly-rs-0.0.4 (c (n "ffly-rs") (v "0.0.4") (d (list (d (n "fastrand") (r "^1.8") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.22") (d #t) (k 2)) (d (n "skytable") (r "^0.7") (f (quote ("tokio" "aio"))) (d #t) (k 2)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0nq52acgpc4ysw74z2y35w25givfynzhc5mm19f5q72khijgrlck")))

(define-public crate-ffly-rs-0.0.5 (c (n "ffly-rs") (v "0.0.5") (d (list (d (n "fastrand") (r "^1.8") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.22") (d #t) (k 2)) (d (n "skytable") (r "^0.7") (f (quote ("tokio" "aio"))) (d #t) (k 2)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0v86yh4hgi28vixa3d9vk8zvxqv8w802v6g81f7959xmk3i02kzk")))

(define-public crate-ffly-rs-0.0.6 (c (n "ffly-rs") (v "0.0.6") (d (list (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.8") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "redis") (r "^0.22") (d #t) (k 2)) (d (n "skytable") (r "^0.7") (f (quote ("tokio" "aio"))) (d #t) (k 2)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1blas0vnzp8vgv47ky0k3bzysfg9wiksq0824h6c8608c33k437b")))

