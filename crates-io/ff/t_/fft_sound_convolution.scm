(define-module (crates-io ff t_ fft_sound_convolution) #:use-module (crates-io))

(define-public crate-fft_sound_convolution-0.1.0 (c (n "fft_sound_convolution") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0h7j3inj88w2wbga28bnzpxr746aa08lrb4c37jnlk56p8rdb1fs")))

(define-public crate-fft_sound_convolution-0.1.1 (c (n "fft_sound_convolution") (v "0.1.1") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0mm3gnnxlc6rm17nq9yxi3gbr2dp3qfvxpd8ygnpxnkjj9b1arx3")))

(define-public crate-fft_sound_convolution-0.1.2 (c (n "fft_sound_convolution") (v "0.1.2") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "slice-ring-buffer") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0hmjq4zgglxin9a28s277mgg0pah0fqnzcdsnn2ibvsjhpq2kp60") (s 2) (e (quote (("slice-ring-buffer" "dep:slice-ring-buffer"))))))

(define-public crate-fft_sound_convolution-0.1.3 (c (n "fft_sound_convolution") (v "0.1.3") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "slice-ring-buffer") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "1vs7fiayljlaqv3v37q9aqahvdpch513w9f9nf2k1ww5zmzv7hg8") (s 2) (e (quote (("slice-ring-buffer" "dep:slice-ring-buffer"))))))

(define-public crate-fft_sound_convolution-0.1.4 (c (n "fft_sound_convolution") (v "0.1.4") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "slice-ring-buffer") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0fys73hv7vshya6c4wraw4l7flxyw6yjgzl40ryg5difzvqpgia0") (s 2) (e (quote (("slice-ring-buffer" "dep:slice-ring-buffer"))))))

(define-public crate-fft_sound_convolution-0.1.5 (c (n "fft_sound_convolution") (v "0.1.5") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "slice-ring-buffer") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "14a5vkdjcisv3vhy01xm94g3fxxn4854833c6v5is03p4rpjm36g") (y #t) (s 2) (e (quote (("slice-ring-buffer" "dep:slice-ring-buffer"))))))

(define-public crate-fft_sound_convolution-0.1.6 (c (n "fft_sound_convolution") (v "0.1.6") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "slice-ring-buffer") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0qay5qw3nnjh06mk67bannsmpzkf283in8cjj2wjc4814vkk22vk") (s 2) (e (quote (("slice-ring-buffer" "dep:slice-ring-buffer"))))))

(define-public crate-fft_sound_convolution-0.1.7 (c (n "fft_sound_convolution") (v "0.1.7") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "slice-ring-buffer") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0037rxacy0829wmv998dlvwapi1ibpsimy16igfwvwqdp3b745xp") (s 2) (e (quote (("slice-ring-buffer" "dep:slice-ring-buffer"))))))

(define-public crate-fft_sound_convolution-0.1.8 (c (n "fft_sound_convolution") (v "0.1.8") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)) (d (n "slice-ring-buffer") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0nlgsljddbi04rra69sfiwjvvd27bmgljdpxfn41iyfizxi1zj06") (s 2) (e (quote (("slice-ring-buffer" "dep:slice-ring-buffer"))))))

