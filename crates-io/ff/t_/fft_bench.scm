(define-module (crates-io ff t_ fft_bench) #:use-module (crates-io))

(define-public crate-fft_bench-0.1.0 (c (n "fft_bench") (v "0.1.0") (d (list (d (n "chfft") (r "^0.3.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-complex") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "017ii4jcygm2nzs6d9zdhw8dpnjh3z8xdnqzr50wl3c8w10zfd9s")))

(define-public crate-fft_bench-0.1.1 (c (n "fft_bench") (v "0.1.1") (d (list (d (n "chfft") (r "^0.3.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-complex") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "0mwldmcmiavn1aws9rldjs494cn6j1kyk37fkmigkqb80wf92nx1")))

