(define-module (crates-io ff i_ ffi_wrapper_nounwind) #:use-module (crates-io))

(define-public crate-ffi_wrapper_nounwind-0.1.0 (c (n "ffi_wrapper_nounwind") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lri1qdwpiq0aj7jddlzyicm4plvwrd1qbxmwlanzz7y0fbiirkj")))

(define-public crate-ffi_wrapper_nounwind-0.1.1 (c (n "ffi_wrapper_nounwind") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nxdma7v9rnyqgri3z6vdhj9k2c5l8369q15riffz8lg1nzknqhs")))

