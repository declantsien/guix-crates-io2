(define-module (crates-io ff i_ ffi_reflect) #:use-module (crates-io))

(define-public crate-ffi_reflect-1.0.0 (c (n "ffi_reflect") (v "1.0.0") (d (list (d (n "ffi_reflect_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0fdxbmd759dzx5sv3bj3x97pi7vmwkf0rpv00qzbgj4knrbj54p1")))

(define-public crate-ffi_reflect-1.0.1 (c (n "ffi_reflect") (v "1.0.1") (d (list (d (n "ffi_reflect_derive") (r "^1.0.1") (d #t) (k 0)))) (h "1a26rdfrwb87x3gvpin6zqjm2w3jh68sj5ddxafnm16pi53iv2fn")))

(define-public crate-ffi_reflect-1.0.2 (c (n "ffi_reflect") (v "1.0.2") (d (list (d (n "ffi_reflect_derive") (r "^1.0.1") (d #t) (k 0)))) (h "1ywsgqamc4v7v031jvd4yxz87946xlbl2icfqb258vf8aa1p2gn2")))

