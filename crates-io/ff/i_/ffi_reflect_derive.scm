(define-module (crates-io ff i_ ffi_reflect_derive) #:use-module (crates-io))

(define-public crate-ffi_reflect_derive-1.0.0 (c (n "ffi_reflect_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0mamypql9msa9j8g103l44rmks9m3zqg8slba13xcn0ga9sjw86g")))

(define-public crate-ffi_reflect_derive-1.0.1 (c (n "ffi_reflect_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1m97saiaqg252nhic99l96hnsp2nbhiigqda0zx2ydjhyvslczd9")))

