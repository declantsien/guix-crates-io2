(define-module (crates-io ff i_ ffi_helpers) #:use-module (crates-io))

(define-public crate-ffi_helpers-0.1.0 (c (n "ffi_helpers") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0hy8xlqqbwnis55kr9a1aivwj4jl1bhvjb5jh6g79y51b2w5sp2l")))

(define-public crate-ffi_helpers-0.2.0 (c (n "ffi_helpers") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)))) (h "0355c1h1a2ck8r87pbfr7n1g4lg9srq93cwqjscw7yqg0ihi58in")))

(define-public crate-ffi_helpers-0.3.0 (c (n "ffi_helpers") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bkz97ban1wivf6kqm6ia8fipz7inxg0agk44lwgr71s3rfsssy0")))

