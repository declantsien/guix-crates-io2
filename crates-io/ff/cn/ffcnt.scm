(define-module (crates-io ff cn ffcnt) #:use-module (crates-io))

(define-public crate-ffcnt-0.1.0 (c (n "ffcnt") (v "0.1.0") (d (list (d (n "btrfs") (r "^1.2.1") (d #t) (k 0)))) (h "0igjvagvsl6zcsjzcz7xabz08snmw891px9if4037x8rl6vg1ziy")))

(define-public crate-ffcnt-0.1.1 (c (n "ffcnt") (v "0.1.1") (d (list (d (n "btrfs") (r "^1.2.1") (d #t) (k 0)))) (h "0zym2m1jfxwz4y77nspbkfr3c4jqkgia7yqcxv3bvzanhy81l86x")))

(define-public crate-ffcnt-0.1.2 (c (n "ffcnt") (v "0.1.2") (d (list (d (n "btrfs") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)))) (h "0xv87gcnb4yxrwm26jnz4zilzfd19k3cqq9yb8g6yaipgbaq4x2l")))

(define-public crate-ffcnt-0.1.3 (c (n "ffcnt") (v "0.1.3") (d (list (d (n "btrfs") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)))) (h "1asda82r9xgbq6li8bf60yzvh6x88dgqv9svmrpb0abgzm3s72h5")))

(define-public crate-ffcnt-0.2.0 (c (n "ffcnt") (v "0.2.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.0") (d #t) (k 0)))) (h "15hnls0h3g88m7hn1m3xns507s4j0947sgxavdv5vxyi9im6dibd")))

(define-public crate-ffcnt-0.2.1 (c (n "ffcnt") (v "0.2.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.0") (d #t) (k 0)))) (h "0d9mrf7gi5zyq0lj0zmfcyycs7y9b75qdkgv7f5cfpj7a54rf003") (f (quote (("system_alloc"))))))

(define-public crate-ffcnt-0.2.2 (c (n "ffcnt") (v "0.2.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.2") (d #t) (k 0)))) (h "0axl4nz1dn73r7wr4jqy5lhg4waw6x6yyyyjyasxfdsbasx3pkfv") (f (quote (("system_alloc"))))))

(define-public crate-ffcnt-0.3.0 (c (n "ffcnt") (v "0.3.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.3") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.2") (d #t) (k 0)))) (h "12afykbykzy0wwh3n6jb7d0hc74w3qkklx2pvsqdxgvd86cmda95")))

(define-public crate-ffcnt-0.3.1 (c (n "ffcnt") (v "0.3.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.2") (d #t) (k 0)))) (h "18qc8jlk2xi41bz1ynsybdj2k85lq080smmvfxcyp18mwknf1clb")))

(define-public crate-ffcnt-0.3.2 (c (n "ffcnt") (v "0.3.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.5") (d #t) (k 0)) (d (n "platter-walk") (r "^0.1.3") (d #t) (k 0)))) (h "0iyqamg05lhxmixyp4k12k5gjr6mal5wi82ibr5ghxxwjf5gcrqy")))

