(define-module (crates-io ff -c ff-carl) #:use-module (crates-io))

(define-public crate-ff-carl-0.1.0 (c (n "ff-carl") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 0)))) (h "0i1nxlam1k4rparcp8jvd5wimax0b0qmp64i64gqfra3fbs34872") (y #t)))

(define-public crate-ff-carl-0.1.1 (c (n "ff-carl") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "x509-parser") (r "^0.15.1") (d #t) (k 0)))) (h "0lmlrkswki5i4j775gznjmr2xmjh0scc1lrdihwbqvq3cs8gl0ga")))

