(define-module (crates-io ff -c ff-cl-gen) #:use-module (crates-io))

(define-public crate-ff-cl-gen-0.1.0 (c (n "ff-cl-gen") (v "0.1.0") (d (list (d (n "ff") (r "^0.2.0") (d #t) (k 0) (p "fff")) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 2) (p "fil-ocl")) (d (n "paired") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1340k2p6s4q1fnzpzj43mbijiiamzs5967pj24mb1zh2ihzbw22p")))

(define-public crate-ff-cl-gen-0.1.1 (c (n "ff-cl-gen") (v "0.1.1") (d (list (d (n "ff") (r "^0.2.0") (d #t) (k 0) (p "fff")) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 2) (p "fil-ocl")) (d (n "paired") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0havjc71vpld08n6b4ijddam1k830ly4hwfdklzmx78jwc9qn2qj")))

(define-public crate-ff-cl-gen-0.1.2 (c (n "ff-cl-gen") (v "0.1.2") (d (list (d (n "ff") (r "^0.2.0") (d #t) (k 0) (p "fff")) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 2) (p "fil-ocl")) (d (n "paired") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "01dfm221xmcz2icg23v9hw9qf7ivyv5n2c8shgk3kvzn342bp9cz")))

(define-public crate-ff-cl-gen-0.1.3 (c (n "ff-cl-gen") (v "0.1.3") (d (list (d (n "ff") (r "^0.2.0") (d #t) (k 0) (p "fff")) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 2) (p "fil-ocl")) (d (n "paired") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1xpdy4dazhc2lp0rlblnswkf8h152622srfanc73xmb721ly8l43")))

(define-public crate-ff-cl-gen-0.2.0 (c (n "ff-cl-gen") (v "0.2.0") (d (list (d (n "ff") (r "^0.2.0") (d #t) (k 0) (p "fff")) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 2) (p "fil-ocl")) (d (n "paired") (r "^0.18.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ps67lb5rr9zrzkdspcsipfgfr65j8ij4lcjpmdbswy1rd64yzy3")))

(define-public crate-ff-cl-gen-0.3.0 (c (n "ff-cl-gen") (v "0.3.0") (d (list (d (n "ff") (r "^0.3.0") (d #t) (k 0) (p "fff")) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "ocl") (r "^0.19.4") (d #t) (k 2) (p "fil-ocl")) (d (n "paired") (r "^0.22.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1f961zi3hzahz6z1qlmhv9zd24i12l9dsrxmlw6slpkf2haz1vzx")))

