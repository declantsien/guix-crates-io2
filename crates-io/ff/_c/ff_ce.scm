(define-module (crates-io ff _c ff_ce) #:use-module (crates-io))

(define-public crate-ff_ce-0.6.0 (c (n "ff_ce") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0hn8jiffbyf34y2ljc022ny09c4lv7srllfbdbjbakf6bpmxqvlg") (f (quote (("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.7.0 (c (n "ff_ce") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1aqlay2jgc1962gwiizw51pz8xrgx1h58srj74mcgn5yyx7822vg") (f (quote (("derive_serde" "derive" "ff_derive_ce/derive_serde") ("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.7.1 (c (n "ff_ce") (v "0.7.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1dgxg1hjhjwayrqgmm9j731jnnharxc4rbqkmr7lfjqap2hixbqq") (f (quote (("derive_serde" "derive" "ff_derive_ce/derive_serde") ("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.8.0 (c (n "ff_ce") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "053sr4ds9hdv0skz9qg7b89c02qy5j3zzppmdxg2nhvv5akrl19h") (f (quote (("derive_serde" "derive" "ff_derive_ce/derive_serde") ("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.9.0 (c (n "ff_ce") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1z8r08z4f461vbih9wq39iay0wd1r1nq2xp9rbcdc2rvqniadh43") (f (quote (("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.10.0 (c (n "ff_ce") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0kxiqcalnbn5fk2rba5li5d02mzib55nwv8v68v4mkfi1c6vyzdv") (f (quote (("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.10.1 (c (n "ff_ce") (v "0.10.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "086kw9jx4nsh2b0ixn6cxx4g4fbx994djll982hg81dk9hmn0f5z") (f (quote (("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.10.2 (c (n "ff_ce") (v "0.10.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "12mz2h8vza5hcxq1kzzrlbbxnpl6kkz8zqv2l4ysb62fdrjddkj0") (f (quote (("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.10.3 (c (n "ff_ce") (v "0.10.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1xn8h974i2kh3v7cp8pd9x34669xvzbxj4pcwmwd26zxfmzqk4j5") (f (quote (("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.11.0 (c (n "ff_ce") (v "0.11.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1w2zz0hxg742sl8yl7jp24fx47pall0h8m5p5airik6h28n6hfhx") (f (quote (("derive" "ff_derive_ce") ("default"))))))

(define-public crate-ff_ce-0.12.0 (c (n "ff_ce") (v "0.12.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0izyqk56w70iyprvvxpnwslvvbcmikvcj4vmgryr03dcifypq41q") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "ff_derive_ce" "ff_derive_ce/asm"))))))

(define-public crate-ff_ce-0.12.1 (c (n "ff_ce") (v "0.12.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1ys8bbw7z28cvl6bj4gqmwm9fszys5bsydmci1pryvk90xzphiyc") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "ff_derive_ce" "ff_derive_ce/asm")))) (y #t)))

(define-public crate-ff_ce-0.13.0 (c (n "ff_ce") (v "0.13.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1w8hg5c4l2vqivly4wb42dn9cl54br6q9j2awql3z3gj81ycjskn") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "ff_derive_ce" "ff_derive_ce/asm"))))))

(define-public crate-ff_ce-0.13.1 (c (n "ff_ce") (v "0.13.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "0.10.*") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1rbz2nwgf0h9d7z52pb91n39w0vbhlnxd7ibppqb9zqyav0814ff") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "derive" "ff_derive_ce/asm"))))))

(define-public crate-ff_ce-0.14.0 (c (n "ff_ce") (v "0.14.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1g9dr1fz6fa65jh5d3giiw9adcyhl21kvk6m4j199za7yz1mxzfh") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "derive" "ff_derive_ce/asm"))))))

(define-public crate-ff_ce-0.14.1 (c (n "ff_ce") (v "0.14.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1zkfsyxifk1kv254l90wy5b358rlsps8z82cp1f4jg5ghyidipa6") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "derive" "ff_derive_ce/asm"))))))

(define-public crate-ff_ce-0.14.2 (c (n "ff_ce") (v "0.14.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "03bf2s23i38ir5hsifvdj53sdl5wdpvcnj3a242jhdh000s8h8mq") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "derive" "ff_derive_ce/asm"))))))

(define-public crate-ff_ce-0.14.3 (c (n "ff_ce") (v "0.14.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive_ce") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1g1z9q6ii8ryy282sg9rywn86v81dwsy7bkwa2f5nfj46518wlsv") (f (quote (("derive" "ff_derive_ce") ("default") ("asm_derive" "derive" "ff_derive_ce/asm"))))))

