(define-module (crates-io ff ts ffts-sys) #:use-module (crates-io))

(define-public crate-ffts-sys-0.1.0 (c (n "ffts-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.55") (d #t) (k 2)))) (h "0j1bmxlz1ijxmcgijlpl82c82hsgq1p5wpfa4zkbc7lhh6acv32q")))

(define-public crate-ffts-sys-0.1.1 (c (n "ffts-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.55") (d #t) (k 2)))) (h "1hxr18843rrsf9wwhr05qb71pl9s8ffx0i6ikw5ajh60gsim1xd4")))

(define-public crate-ffts-sys-0.1.2 (c (n "ffts-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.55") (d #t) (k 2)))) (h "10pvmbxywaka84kmipqgp587qyslwrbf8wq7np45jiikgijvf279")))

(define-public crate-ffts-sys-0.1.3 (c (n "ffts-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.55") (d #t) (k 2)))) (h "0zh1mcxi6xabq5vba4s35adyfwqbnzpvh5jlbf4cq67412slii96")))

