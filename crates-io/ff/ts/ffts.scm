(define-module (crates-io ff ts ffts) #:use-module (crates-io))

(define-public crate-ffts-0.1.0 (c (n "ffts") (v "0.1.0") (d (list (d (n "ffts-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "115cmq8qyw32d2nhkpmvbwxm69n8cs904m6k2f6r1k381sjfa94k")))

(define-public crate-ffts-0.1.1 (c (n "ffts") (v "0.1.1") (d (list (d (n "ffts-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "09yfx0s7k24zik2h93nnaq6vmgjs1rg408a469rlwqhm1yzy3dv1")))

(define-public crate-ffts-0.1.3 (c (n "ffts") (v "0.1.3") (d (list (d (n "ffts-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "0fp7dldnkichpck4rhmb87f5lbvjf9g1j30vri94v1l0dbjxx6s2")))

