(define-module (crates-io ff f_ fff_derive) #:use-module (crates-io))

(define-public crate-fff_derive-0.2.0 (c (n "fff_derive") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04d1jricgsdz6jkapygm8l3r73nfw5svkw6ia5q9ylvkipi0x1an")))

(define-public crate-fff_derive-0.2.1 (c (n "fff_derive") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rphp73m9qslawgm4kbiq7sylpwyx35gb46lnpwnpmsndwamykk2")))

(define-public crate-fff_derive-0.2.2 (c (n "fff_derive") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0n25k34b99ff58s32799dm9wl7difng12fqvxlskng9jpsgkhjw4")))

(define-public crate-fff_derive-0.3.0 (c (n "fff_derive") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0dbmfhwcdz3l0nyxr405scx4qkgr7dj5zcvab99vcazyfzyvpp3q")))

