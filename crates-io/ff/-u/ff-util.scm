(define-module (crates-io ff -u ff-util) #:use-module (crates-io))

(define-public crate-ff-util-0.1.1 (c (n "ff-util") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1fzi1xswmiv6vbvadg41wln4bgp9g2j4xwbzfzpv9vgwndhpv1nm")))

(define-public crate-ff-util-0.1.2 (c (n "ff-util") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1d52j38120l5yh9a6i7gcr5kqjikjrq3krg7ab0hp1xc2986sa5w")))

