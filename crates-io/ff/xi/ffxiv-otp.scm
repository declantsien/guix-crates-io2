(define-module (crates-io ff xi ffxiv-otp) #:use-module (crates-io))

(define-public crate-ffxiv-otp-1.0.0 (c (n "ffxiv-otp") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^5.4.1") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "totp-rs") (r "^3.1.0") (d #t) (k 0)))) (h "0kqiw1w5mgm0c3lkwimsq1sxivrhnfbg4d2bsgml52i8kz54l3dv")))

