(define-module (crates-io ff xi ffxiv-crafting) #:use-module (crates-io))

(define-public crate-ffxiv-crafting-0.1.0 (c (n "ffxiv-crafting") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pa9dxyzn41gfcngjdvf9k3xdjl5l88g5kays3l94hz0s1r3piga") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-0.1.1 (c (n "ffxiv-crafting") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xaq1i0rzr3gs0g75kqkq3bc6zh6dcl5j6y27i5f52wqa72srd7i") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-0.1.2 (c (n "ffxiv-crafting") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04k3lc4989y1favkyjcbz16gsa5qm1zbawjj4lfq4m11z9zpc98f") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-0.1.3 (c (n "ffxiv-crafting") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gpv5plfb8vnpkbqpi7q8yzvsi3pjcnv9056b8c03szzsip9ppb9") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.0 (c (n "ffxiv-crafting") (v "6.0.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r5mfrkjip5rqfysbdd9jxgmlfn4l6814z3v6lrbc97bw4x18nxi") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.1 (c (n "ffxiv-crafting") (v "6.0.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "041vaa56d0ghgi2d6gcym9rpfphcqkfj7n6zwmz2kknrba38987w") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.2 (c (n "ffxiv-crafting") (v "6.0.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q7fldl53scsv6sar9105iqp5bnccdrzxhaxav3fyhq9ssdbm25z") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.3 (c (n "ffxiv-crafting") (v "6.0.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1klpqar41i8wc1ys7jqz77wygnbfqvcjm2l9qyr1zpfzijq9fyqc") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.4 (c (n "ffxiv-crafting") (v "6.0.4") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17bjsb2hg0w5nhvbgbakwhvz4wna9xyvl25kks4kgm112p1b3x93") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.5 (c (n "ffxiv-crafting") (v "6.0.5") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dz5pyyihhhs65iw5cj9cxa6hx9mdadni4cvh1pxbm1wzpy2irkn") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.6 (c (n "ffxiv-crafting") (v "6.0.6") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13shs84pwmzysysnal234kis095j9cdkvhh804fpp2canmdx48q5") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.7 (c (n "ffxiv-crafting") (v "6.0.7") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xva8m2mmyz9asss3wr5qzyixyfw3zvcf87flng4inqsny3l1k5i") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.8 (c (n "ffxiv-crafting") (v "6.0.8") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ymk6fh3j3mb7wfd1wg6i84am04h8i56kj4zqzfwlvj3n3g6sff7") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.9 (c (n "ffxiv-crafting") (v "6.0.9") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1asqvn2mr60qirmalwgkzzdbri0llwfb5g3dazqzjzj0sqmskayk") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.11 (c (n "ffxiv-crafting") (v "6.0.11") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1blkzi57f62x31127xdisvxvsn9c4jl67fr7w513jb6cajqdsnzm") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.0.12 (c (n "ffxiv-crafting") (v "6.0.12") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k9a8xggwa1pr6sdkx9p00h5a7ywaphr98cnlkqgkbnr6qzrh8j5") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.1.0 (c (n "ffxiv-crafting") (v "6.1.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "033k4c0r6cj7svi42syahrvsd8811xbmz4z4fg8232gxbp4r559h") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.2.0 (c (n "ffxiv-crafting") (v "6.2.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k14gmn7b57ca0ivlp41rb0ysyzh9gsbk076k0rziswzfdfgdia2") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.2.1 (c (n "ffxiv-crafting") (v "6.2.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00qgbw1mhl30vfxbshx3bj6jiif0bxmr07q8jahdnm4d2280qlc9") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.2.2 (c (n "ffxiv-crafting") (v "6.2.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12xfinri9jh8w026fxdr2ngb5r869rpb0v0fyrrmh91axwgb2f11") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.2.3 (c (n "ffxiv-crafting") (v "6.2.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vxql2aww20zcka2pms64xr2frl0x2pn6v4a2zpgvavh5ya212w4") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.3.0 (c (n "ffxiv-crafting") (v "6.3.0") (d (list (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gkhr8qqxz16944gphl1xmwfikxhrd2igwd810sa9p0ap42ipi1s") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.3.1 (c (n "ffxiv-crafting") (v "6.3.1") (d (list (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1akb4bs2scai5qx2dpar2f22w3gql5yssp3wmkca8rll2fhy0nyn") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.3.2 (c (n "ffxiv-crafting") (v "6.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19rkwwafk7hvxx32lgf11raar89y8w6awvjz9rkp2yr71s55wdwd") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.3.3 (c (n "ffxiv-crafting") (v "6.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02w5d70klq2i0lxi24jipfdkq6l44862wjzpvd828743pwbsxnzy") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.5.0 (c (n "ffxiv-crafting") (v "6.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wphlxqrpphv4xgs017vsq451n6c04il77gl30hs2fr7j979199n") (f (quote (("serde-support" "serde"))))))

(define-public crate-ffxiv-crafting-6.5.1 (c (n "ffxiv-crafting") (v "6.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0606ji37ih2ay2zh5mjpz8kzaxczjy83ra4w18305cv82hi4xnny") (f (quote (("serde-support" "serde"))))))

