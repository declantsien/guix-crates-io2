(define-module (crates-io ff fo ffforf) #:use-module (crates-io))

(define-public crate-ffforf-0.1.0 (c (n "ffforf") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "humansize") (r "^2.0.0") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulp") (r "^0.2.2") (d #t) (k 0)))) (h "0y1w8w2ihph0ygc2748pq7ml9c2dz12iz23r0839x6xs06y410px")))

(define-public crate-ffforf-0.2.0 (c (n "ffforf") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fffx") (r "^0.1.1") (d #t) (k 0)) (d (n "humansize") (r "^2.0.0") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulp") (r "^0.4.0") (d #t) (k 0)))) (h "1fabbxf4a1ap662cwvcdhzgjkjmhn0ilfvr4fr2qrimdxqx2kc0a")))

(define-public crate-ffforf-0.3.0 (c (n "ffforf") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "humansize") (r "^2.1.3") (d #t) (k 2)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "needletail") (r "^0.5.1") (d #t) (k 0)) (d (n "pulp") (r "^0.18.9") (d #t) (k 0)))) (h "1ff8mg9s1hmsvgrq2w3w68pjlpd9s5dyp4195j88ns52bkmq2a8x")))

