(define-module (crates-io ff mm ffmmlc) #:use-module (crates-io))

(define-public crate-ffmmlc-0.1.0 (c (n "ffmmlc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ffmml") (r "^0.1") (d #t) (k 0)))) (h "152f2wxwkbr7cihm7fkpkg8fvlzvgqcw1a4nhbkda0fnw9sby1mv")))

