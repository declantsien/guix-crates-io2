(define-module (crates-io ff pb ffpb) #:use-module (crates-io))

(define-public crate-ffpb-0.1.0 (c (n "ffpb") (v "0.1.0") (d (list (d (n "kdam") (r "^0.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1sq3k0sqsp3ng7vn5wwjhx8jbpw90vs4k1y9lnkb0qam0mf1j09r")))

(define-public crate-ffpb-0.1.1 (c (n "ffpb") (v "0.1.1") (d (list (d (n "kdam") (r "^0.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "16fj76c81cz8lmb3g7mnzd244whjbbgah5z8ik9fv7m3aakq9654")))

(define-public crate-ffpb-0.1.2 (c (n "ffpb") (v "0.1.2") (d (list (d (n "kdam") (r "^0.5") (f (quote ("rich"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "1fppcifwv2ry6zzyx9vi1j9x57060rr5xzqvbwwpa010rxw9wqpf")))

