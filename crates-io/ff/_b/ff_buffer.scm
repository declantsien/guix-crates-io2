(define-module (crates-io ff _b ff_buffer) #:use-module (crates-io))

(define-public crate-ff_buffer-0.1.0 (c (n "ff_buffer") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dirs") (r "^5.0.1") (d #t) (k 1)))) (h "03mj00mqry0hb5lp9382ixp3hvzhp023r3wx3gndrf0ahbmxa4bk") (f (quote (("crosslto")))) (l "libffbuffer")))

