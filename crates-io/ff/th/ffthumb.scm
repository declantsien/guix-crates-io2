(define-module (crates-io ff th ffthumb) #:use-module (crates-io))

(define-public crate-ffthumb-0.1.0 (c (n "ffthumb") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0vlm3jdbjv1nf2wc2pdm5nq2apxdwjcv118caym8mzsccsvv2j2f") (s 2) (e (quote (("bindgen" "dep:bindgen"))))))

(define-public crate-ffthumb-0.2.0 (c (n "ffthumb") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0ni5h369z3nzk4lsf6n73p7zldy70b53s0fzibczvnfxxkjjwy8c")))

(define-public crate-ffthumb-0.2.1 (c (n "ffthumb") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "10zzycb97pfmrld1p85zjd1jsiynkc4p112szdg8df2w7gvs4hr9")))

