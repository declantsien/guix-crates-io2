(define-module (crates-io ff fl ffflaij23kap1p-crate-io-test) #:use-module (crates-io))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.0 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.0") (h "064baigfw5yhfhk168dmlqmpl6288kafb50c9hg8q2fqbxbnkpfb")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.1 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.1") (h "1481z4qh1ds8sghic78h8729ip4six71kay32wg4pqdscz0vk9is")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.2 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.2") (h "13hma58wsv1m84fgx2ckby5n6sjn6vsx0cxsqlh800m0xk6dlwj7")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.3 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.3") (h "07h5h2hiwzvkyxisjwv08y3dbbhz8fdkchyrpdh9ksnxdvalh40a")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.4 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.4") (h "1xsm15byl2f5gvlkv5jnq527zwnwm7m1m9q1kb833z6sz6xy6d2y")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.4-dev (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.4-dev") (h "1w8p4i2l02khvsm3wm7azx0w1yf2wfm442cxjqcdhljafs89c8xj")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.5-dev.0 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.5-dev.0") (h "1j422b441m6lr3b9a4pkxl1qf0igvcxxd8bzpjipqf5rp2jrfad1")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.5-dev.1 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.5-dev.1") (h "1igg4shja73y0wa734l8hz4md20w9xw6f3li3a4aba8p5c46ngn4")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.5-dev.2 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.5-dev.2") (h "1bbb4cra40ijp3236dxdala93ppyv737za9pv59dmidmpvbh5vx6")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.5-dev.3 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.5-dev.3") (h "0705db663294rxb7ybpsggzism8cg676h2bgwhppsbcq4mpibcxb")))

(define-public crate-ffflaij23kap1p-crate-io-test-0.1.5-dev.4 (c (n "ffflaij23kap1p-crate-io-test") (v "0.1.5-dev.4") (h "1c30hhdg31gv1yylanzxlcxj45mrgpmsh0am2bcgds82wzlzfpa9")))

