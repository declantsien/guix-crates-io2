(define-module (crates-io ff tp fftp) #:use-module (crates-io))

(define-public crate-fftp-0.1.0 (c (n "fftp") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (k 0)) (d (n "fork") (r "^0.1") (d #t) (k 0)) (d (n "igd") (r "^0.12") (f (quote ("aio" "tokio"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "io-std" "io-util" "fs" "net" "sync"))) (d #t) (k 0)))) (h "069hxar9phf0kn6z0nhv59kq2hcsplsr00g04cbp3q0mir3l8zg6")))

(define-public crate-fftp-0.2.0 (c (n "fftp") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (k 0)) (d (n "igd") (r "^0.12") (f (quote ("aio" "tokio"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "io-std" "io-util" "fs" "net" "sync"))) (d #t) (k 0)))) (h "1yxy3bz57pf114vp2js6ribbhqjqj0qs3qg80inw9zfxadd2la5s")))

