(define-module (crates-io ff me ffmetadata) #:use-module (crates-io))

(define-public crate-ffmetadata-0.1.0 (c (n "ffmetadata") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1r09znfc01csn454ijhqav293vr8li3lr22ap497nqijvb5xv0yx")))

(define-public crate-ffmetadata-0.1.1 (c (n "ffmetadata") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "11w36qbk4sd6dbhfk8d9kx80zkqpgg43d5c7zz7b2prvgykcbixb")))

(define-public crate-ffmetadata-0.1.2 (c (n "ffmetadata") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "13w0li8v3dsi4n38sz4bcx0ldv10xwilcjznxlvcn0y7r7hlisas")))

