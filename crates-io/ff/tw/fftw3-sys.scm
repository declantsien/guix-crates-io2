(define-module (crates-io ff tw fftw3-sys) #:use-module (crates-io))

(define-public crate-fftw3-sys-0.0.1 (c (n "fftw3-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "~0.0.1") (d #t) (k 0)))) (h "1fgn8a0cr4ayzf5il898rsq9cfcxzsa64cmi687np3kqq1blspk1")))

(define-public crate-fftw3-sys-0.0.2 (c (n "fftw3-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "pkg-config") (r "^0") (d #t) (k 1)))) (h "0iy04jviaf7cxm624m44gkrz1af1m1dhiawlri647v1pjc2syf0b")))

