(define-module (crates-io ff tw fftw-src) #:use-module (crates-io))

(define-public crate-fftw-src-0.1.0 (c (n "fftw-src") (v "0.1.0") (h "0wx5zwwx40fmkjb2cxmmn08b44hwb1w7cxjsin27gp7x167hlax5")))

(define-public crate-fftw-src-0.2.0 (c (n "fftw-src") (v "0.2.0") (h "0685bmbzdd1shvqzqxdckvd4pfkp7rs7l7p9976g6yfcz5xjcipy")))

(define-public crate-fftw-src-0.2.1 (c (n "fftw-src") (v "0.2.1") (h "0p05bhmnjwf6077r9rxs76q7h7pjc3jm510i3x77hqdhdxm6g0f8")))

(define-public crate-fftw-src-0.2.2 (c (n "fftw-src") (v "0.2.2") (h "194fm7ps58mn7ag3z7vza16bsjbbpcfmkpp9zlgk700ca30rh3aa")))

(define-public crate-fftw-src-0.2.3 (c (n "fftw-src") (v "0.2.3") (d (list (d (n "md5") (r "^0.4") (d #t) (k 1)))) (h "09jqfzypa47lswq58ci3llwbwiszwfxp7np9mzf02mk25imw63hn") (l "fftw3")))

(define-public crate-fftw-src-0.3.0 (c (n "fftw-src") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 1)) (d (n "ftp") (r "^3.0") (d #t) (k 1)) (d (n "md5") (r "^0.6") (d #t) (k 1)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "0w6h6i7mmnw76p97y22vc4gy17hqmnls8s8jbyg97f1x4173zm1n") (l "fftw3")))

(define-public crate-fftw-src-0.3.1 (c (n "fftw-src") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 1)) (d (n "ftp") (r "^3.0") (d #t) (k 1)) (d (n "md5") (r "^0.6") (d #t) (k 1)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "0grz9qshc42b59h2ynajfblh9zdc78dfnnzigphbh4rdkcvjvs2d") (l "fftw3")))

(define-public crate-fftw-src-0.3.2 (c (n "fftw-src") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)) (d (n "ftp") (r "^3.0") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "15val8xw01q2kc781788jh2i3dlhhlcgp1ysnisyza9mmg25ang3") (l "fftw3")))

(define-public crate-fftw-src-0.3.3 (c (n "fftw-src") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "~1.2.0") (d #t) (k 1)) (d (n "ftp") (r "^3.0") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "0z2s74p6j0ghn2030fjh9vnjhv27663qqcvxxis1xsdh193n52f0") (l "fftw3")))

(define-public crate-fftw-src-0.8.0 (c (n "fftw-src") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fs_extra") (r "~1.3.0") (d #t) (k 1)) (d (n "ftp") (r "^3.0") (d #t) (k 1)) (d (n "zip") (r "^0.6") (d #t) (k 1)))) (h "08hyj4gdqr49nlcwbh2nlszx6kg35zsg0g4c4lglpmk9wm18d00q") (l "fftw3")))

