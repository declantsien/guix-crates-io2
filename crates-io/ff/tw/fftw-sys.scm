(define-module (crates-io ff tw fftw-sys) #:use-module (crates-io))

(define-public crate-fftw-sys-0.1.0 (c (n "fftw-sys") (v "0.1.0") (d (list (d (n "fftw-src") (r "^0.1.0") (d #t) (k 0)))) (h "0k6zfpbjnfv68hf8cpvrpxa8bqs9m441fddann4d75r02wy8ly2j")))

(define-public crate-fftw-sys-0.2.0 (c (n "fftw-sys") (v "0.2.0") (d (list (d (n "fftw-src") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.37") (d #t) (k 0)))) (h "0cnfwh22km2dxniay617akp4pwlmnmfmfjl2jf6bkrwxxh2aqy67") (f (quote (("system") ("source" "fftw-src") ("default" "source"))))))

(define-public crate-fftw-sys-0.3.0 (c (n "fftw-sys") (v "0.3.0") (d (list (d (n "fftw-src") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "intel-mkl-src") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "num-complex") (r "^0.1.37") (d #t) (k 0)))) (h "0bafrqx6gaqw22j9mn1sr4p54aihyxvkx63mznh5h6f3bkvjxy9n") (f (quote (("system") ("source" "fftw-src") ("intel-mkl" "intel-mkl-src") ("default" "source"))))))

(define-public crate-fftw-sys-0.3.1 (c (n "fftw-sys") (v "0.3.1") (d (list (d (n "fftw-src") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "intel-mkl-src") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "1np2k130d85bffdxzdymc3ahd3qv2qz5fb5cqicdhdky1zhk51wg") (f (quote (("system") ("source" "fftw-src") ("intel-mkl" "intel-mkl-src") ("default" "source")))) (y #t)))

(define-public crate-fftw-sys-0.4.0 (c (n "fftw-sys") (v "0.4.0") (d (list (d (n "fftw-src") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "intel-mkl-src") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "1mc2dnsqzjz32rdq0n71pdbxy870mryrh94d358avf4ss459bysv") (f (quote (("system") ("source" "fftw-src") ("intel-mkl" "intel-mkl-src") ("default" "source"))))))

(define-public crate-fftw-sys-0.5.0 (c (n "fftw-sys") (v "0.5.0") (d (list (d (n "fftw-src") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "intel-mkl-src") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "0rpcp4knj2z1hk5m01ikwlxc5hzr1zlvxafxkkjb4d010c9w12cl") (f (quote (("system") ("source" "fftw-src") ("intel-mkl" "intel-mkl-src") ("default" "source"))))))

(define-public crate-fftw-sys-0.6.0 (c (n "fftw-sys") (v "0.6.0") (d (list (d (n "fftw-src") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "intel-mkl-src") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "num-complex") (r "^0.3.0") (d #t) (k 0)))) (h "1zdkk3y824wjfyzcdf2ymz8phl61xf3iw16d21vg3hjwd4frbqz8") (f (quote (("system") ("source" "fftw-src") ("intel-mkl" "intel-mkl-src") ("default" "source"))))))

(define-public crate-fftw-sys-0.8.0 (c (n "fftw-sys") (v "0.8.0") (d (list (d (n "fftw-src") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "intel-mkl-src") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "13f2y5sfj4r1j03wdwcm75bw4qad0kncp8n5iryymbv6am53yc53") (f (quote (("system") ("source" "fftw-src") ("intel-mkl" "intel-mkl-src") ("default" "source"))))))

