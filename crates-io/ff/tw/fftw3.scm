(define-module (crates-io ff tw fftw3) #:use-module (crates-io))

(define-public crate-fftw3-0.0.1 (c (n "fftw3") (v "0.0.1") (d (list (d (n "fftw3-sys") (r "~0.0.1") (d #t) (k 0)) (d (n "num") (r "~0.0.4") (d #t) (k 0)) (d (n "strided") (r "^0.2.1") (d #t) (k 0)))) (h "151q2j64p75965p0if7kxfd34984siq7fg80xvh7v6aak8ra5fnz")))

(define-public crate-fftw3-0.0.2 (c (n "fftw3") (v "0.0.2") (d (list (d (n "fftw3-sys") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "strided") (r "^0") (d #t) (k 0)))) (h "137wd8ab313cx9j7cns7dyih5fkprk5hvp5638an8mdj323ny4xb")))

