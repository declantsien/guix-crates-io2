(define-module (crates-io ff t4 fft4r) #:use-module (crates-io))

(define-public crate-fft4r-0.1.0 (c (n "fft4r") (v "0.1.0") (d (list (d (n "kissfft-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)))) (h "09girwvzrwpdy9zmxycwmw3b7ra0mxkf8d2607s852shwzp2q7jk")))

