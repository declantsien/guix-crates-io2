(define-module (crates-io ff cl ffcli) #:use-module (crates-io))

(define-public crate-ffcli-0.1.0 (c (n "ffcli") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)))) (h "1fh3hjwpd5gpkknj70c4d9mdm9qqi5m8lqbngjygdxnvh0by688j")))

(define-public crate-ffcli-0.1.1 (c (n "ffcli") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0fhnllbwwam43w6g1gx8nril1fvxy25wdqvgim9wjq7wqvkg7khs")))

(define-public crate-ffcli-0.1.2 (c (n "ffcli") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0nzcd4njxymmv7ss8azczqh1vmvif7llnkdgi73bhy6iijz6kx7y")))

(define-public crate-ffcli-0.1.3 (c (n "ffcli") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1jfd093g5cx9lllrk2vvc0sna24pfbcdl9zk3cblbp8frhg09zav")))

(define-public crate-ffcli-0.1.4 (c (n "ffcli") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0z72vxx6wn266znxs5dg8z5a9ldimd4irhgv6jx54qfns2238dqy")))

(define-public crate-ffcli-0.1.5 (c (n "ffcli") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1ag61wrp1jyl9acg6y4987h47f050bzz2gpwhjs3fwmrpkn91jbw")))

(define-public crate-ffcli-0.1.6 (c (n "ffcli") (v "0.1.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0x31c731p48ax09x50bhdpxv2xdjsz1chnw60w41ry1mw7xxx42z")))

