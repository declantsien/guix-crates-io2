(define-module (crates-io ff mp ffmpeg-cli) #:use-module (crates-io))

(define-public crate-ffmpeg-cli-0.1.0 (c (n "ffmpeg-cli") (v "0.1.0") (d (list (d (n "futures") (r "0.3.*") (d #t) (k 0)) (d (n "futures-core") (r "0.3.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("net" "io-util" "rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1vrdvvnwy61qs7iv6slb2fhd5jy2ggxdypgm6dz6ia5ikzafn67h")))

