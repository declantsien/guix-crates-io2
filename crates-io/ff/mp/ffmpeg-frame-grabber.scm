(define-module (crates-io ff mp ffmpeg-frame-grabber) #:use-module (crates-io))

(define-public crate-ffmpeg-frame-grabber-0.1.0 (c (n "ffmpeg-frame-grabber") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "image-visualizer") (r "^0.1.1") (d #t) (k 2)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)))) (h "002p5ds4k07gzn5l28fp4lpwsd76npagm8ps6w4v0qkd0plv6axc")))

