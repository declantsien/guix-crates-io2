(define-module (crates-io ff mp ffmpeg_cmdline_utils) #:use-module (crates-io))

(define-public crate-ffmpeg_cmdline_utils-0.1.0 (c (n "ffmpeg_cmdline_utils") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hfx4hxhwkcdjalprdzqz0zas38dz7ck5z8yxhsn3wd5bcgnn59g")))

(define-public crate-ffmpeg_cmdline_utils-0.1.1 (c (n "ffmpeg_cmdline_utils") (v "0.1.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "119hj82w7jqk9rx5z601xrjl75abdbghbsafnmn2py4p97xvdjmm")))

(define-public crate-ffmpeg_cmdline_utils-0.1.2 (c (n "ffmpeg_cmdline_utils") (v "0.1.2") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mv1jja3smw3vz8lr0b7gvv1i55wvrglrcqdk543ibp6narn65az")))

(define-public crate-ffmpeg_cmdline_utils-0.1.3 (c (n "ffmpeg_cmdline_utils") (v "0.1.3") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nw35jrqiwkqcfwfr7qll123w4mad833bwp2f130ns7yjqjk8ra4")))

