(define-module (crates-io ff mp ffmpeg4-ffi) #:use-module (crates-io))

(define-public crate-ffmpeg4-ffi-0.3.8 (c (n "ffmpeg4-ffi") (v "0.3.8") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nw5lqy9zf56viinhwsr8brg9kgb642ckf4mfs65advjkywvxmmy")))

