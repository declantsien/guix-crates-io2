(define-module (crates-io ff mp ffmpeg-stringify) #:use-module (crates-io))

(define-public crate-ffmpeg-stringify-0.1.0 (c (n "ffmpeg-stringify") (v "0.1.0") (d (list (d (n "map-macro") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1h21vk6h3hhyyakb7r31n1gfn2jpdg4dgvxrvn4yvbd2vxz7bjxs")))

(define-public crate-ffmpeg-stringify-0.1.1 (c (n "ffmpeg-stringify") (v "0.1.1") (d (list (d (n "map-macro") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0avh9qnvxplh58aba70lndnv5ifiywfasfbwrsw6fka8lwgygv22")))

(define-public crate-ffmpeg-stringify-0.2.1 (c (n "ffmpeg-stringify") (v "0.2.1") (d (list (d (n "map-macro") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1q1gky9akhhj878yc4shaq87dhc4a5mnfcyqm9s5jsis2gv8bvzm")))

