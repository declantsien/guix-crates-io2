(define-module (crates-io ff mp ffmpeg-next-io) #:use-module (crates-io))

(define-public crate-ffmpeg-next-io-0.1.0 (c (n "ffmpeg-next-io") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0") (o #t) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^6") (d #t) (k 0)) (d (n "futures") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "15n4rwk73qvpikkw3974md7zc1aig879r4icpm0di08jhcm8p948") (f (quote (("default" "channel")))) (y #t) (s 2) (e (quote (("tokio" "dep:tokio" "channel") ("crossbeam-channel" "dep:crossbeam-channel" "channel") ("channel" "dep:bytes"))))))

