(define-module (crates-io ff mp ffmpeg-wasi) #:use-module (crates-io))

(define-public crate-ffmpeg-wasi-0.1.0 (c (n "ffmpeg-wasi") (v "0.1.0") (h "0fiwqdr5b0xwqa86q4ygq8mwx94vjv9f3sdqphkz5pcrx5pfdwqw")))

(define-public crate-ffmpeg-wasi-0.1.1 (c (n "ffmpeg-wasi") (v "0.1.1") (h "034f9cdz8jdhvjybsy8rbykyxa3zvya6nq6ff4a5pws9w2n4m76x")))

(define-public crate-ffmpeg-wasi-0.1.2 (c (n "ffmpeg-wasi") (v "0.1.2") (h "131291am2wnby15wxva9l0xlq4551pkcvsdr1mmryyhcpk81yjlr")))

(define-public crate-ffmpeg-wasi-0.1.3 (c (n "ffmpeg-wasi") (v "0.1.3") (h "1dvfprvfdggnvdz1yjn2bb427xar6h7xnzbcb04ngrwzx5l0iz1d")))

(define-public crate-ffmpeg-wasi-0.1.4 (c (n "ffmpeg-wasi") (v "0.1.4") (h "1w81nkc5i01fl76k3mh463qdygdpa1x2b6lvx6xwcjxal802365p")))

(define-public crate-ffmpeg-wasi-0.1.5 (c (n "ffmpeg-wasi") (v "0.1.5") (h "1pfgy4i25xra6cmrdb9agfcifz3mrcrvdka3is76ngld98b54c0w")))

(define-public crate-ffmpeg-wasi-0.1.6 (c (n "ffmpeg-wasi") (v "0.1.6") (h "17dq20298fzcfk2gsdn8hh85mgw6v2lff4j7kabhjmbpgs29jdy7")))

(define-public crate-ffmpeg-wasi-0.1.7 (c (n "ffmpeg-wasi") (v "0.1.7") (h "08xiw5f5cw1ws6n4fk89nxx6279zlwspp9cfxpjdlvbnid9cv6si")))

(define-public crate-ffmpeg-wasi-0.1.8 (c (n "ffmpeg-wasi") (v "0.1.8") (h "0d17ssrflkdr9g3phx8wvg41bvmqmz3d5g1hmcqhz73szn3fhvrw")))

(define-public crate-ffmpeg-wasi-0.1.9 (c (n "ffmpeg-wasi") (v "0.1.9") (h "1ydija4zmqbz9wjivw5wkca20rp9wnbnkbzxlp8zinhfpjbi4l5v")))

(define-public crate-ffmpeg-wasi-0.1.10 (c (n "ffmpeg-wasi") (v "0.1.10") (h "17k2jgsrqvbviyj3j3fqx41yy20i8mzpa1kz12b7q0mxv28vhrfj")))

(define-public crate-ffmpeg-wasi-0.1.11 (c (n "ffmpeg-wasi") (v "0.1.11") (h "1pqm64bjxbg47952zgj838wdpx7vm4plc836bp06zl086chl60af")))

(define-public crate-ffmpeg-wasi-0.1.12 (c (n "ffmpeg-wasi") (v "0.1.12") (h "1jfhy8fw1ljlcb368xf4lyymmmp4n4g6prvblzjm2b5p852kbvhy")))

(define-public crate-ffmpeg-wasi-0.1.13 (c (n "ffmpeg-wasi") (v "0.1.13") (h "1iw000qdy8l6kwgsb1asbbzm349ck595fn47xbk92d1j3y8qc72f")))

(define-public crate-ffmpeg-wasi-0.1.14 (c (n "ffmpeg-wasi") (v "0.1.14") (h "0rgziifvaf518b3pmkar3xr82lw4sav5yxfs6dqsynrg0b0jgb8r")))

