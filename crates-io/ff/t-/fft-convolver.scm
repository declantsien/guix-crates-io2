(define-module (crates-io ff t- fft-convolver) #:use-module (crates-io))

(define-public crate-fft-convolver-0.1.0 (c (n "fft-convolver") (v "0.1.0") (d (list (d (n "realfft") (r "^3.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "15vs8y62v6zn0m0r2wnj7kvnhrcspb4rsw2s7wl8p61hbxp5xm27")))

(define-public crate-fft-convolver-0.2.0 (c (n "fft-convolver") (v "0.2.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "realfft") (r "^3.0.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1z74p8x9li18xikm8z8vw0l5j1l8zz4arwppfc0p2bcmsdrh9kxx")))

