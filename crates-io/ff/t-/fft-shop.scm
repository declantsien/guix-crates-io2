(define-module (crates-io ff t- fft-shop) #:use-module (crates-io))

(define-public crate-fft-shop-0.1.0 (c (n "fft-shop") (v "0.1.0") (h "0kbc6wjrlkx1jgpbhir2wwcfgmgrs3dv7hh4vfkymlm8fbr0yzwp")))

(define-public crate-fft-shop-0.1.1 (c (n "fft-shop") (v "0.1.1") (h "0jr04haja776k7n6q9svvw34yi385qhhl7p5xlfxpvsgjrm01cx8")))

