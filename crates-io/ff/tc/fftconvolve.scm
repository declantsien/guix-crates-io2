(define-module (crates-io ff tc fftconvolve) #:use-module (crates-io))

(define-public crate-fftconvolve-0.1.0 (c (n "fftconvolve") (v "0.1.0") (d (list (d (n "easyfft") (r "^0.2.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0yll627xm19qdl19ddk7ih0vshpcpsc1xvzgmby04ffnmgxb6l0f")))

(define-public crate-fftconvolve-0.1.1 (c (n "fftconvolve") (v "0.1.1") (d (list (d (n "easyfft") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "132krq0728mzghdi35dg8g5mllvvbcp04043wkwbzb78wnpw7m2b")))

