(define-module (crates-io ff in ffind) #:use-module (crates-io))

(define-public crate-ffind-0.1.0 (c (n "ffind") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1lv5prcmkq3s4av4kfmq2rx1kbbmvrjk7h28xk3cn903c0pcj7kg")))

