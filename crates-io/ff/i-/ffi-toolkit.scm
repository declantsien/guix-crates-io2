(define-module (crates-io ff i- ffi-toolkit) #:use-module (crates-io))

(define-public crate-ffi-toolkit-0.1.0 (c (n "ffi-toolkit") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hgzrw2axgv1c160j9dhi5wzps6n9yxdqy89n3x3idjsip3h0wij")))

(define-public crate-ffi-toolkit-0.1.1 (c (n "ffi-toolkit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ycc4qgm3z2wpip9cynf7mc04qnyz3x8s8mnf5as31l78hpynml4")))

(define-public crate-ffi-toolkit-0.2.0 (c (n "ffi-toolkit") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jcckjn54p4khg8vqa7cwak8cibhj45ksxiqnn6kd673ljzbzy1j")))

(define-public crate-ffi-toolkit-0.3.0 (c (n "ffi-toolkit") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wmmzbkf40vc8q1kjwyha9x5jzvyv36q213mv0cpwccbpyvcb1jz")))

(define-public crate-ffi-toolkit-0.4.0 (c (n "ffi-toolkit") (v "0.4.0") (d (list (d (n "drop_struct_macro_derive") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wnrv1y3k9w0pxad61x7r608qg0bj8nayz4jznz8cw3353zbg5aw")))

(define-public crate-ffi-toolkit-0.4.1 (c (n "ffi-toolkit") (v "0.4.1") (d (list (d (n "drop_struct_macro_derive") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07d7i1ymwlq3rq109xqg3phw78pyb56nvfniw5d8laqriwg6swbv")))

(define-public crate-ffi-toolkit-0.5.0 (c (n "ffi-toolkit") (v "0.5.0") (d (list (d (n "drop_struct_macro_derive") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nk0gqmw5523yaygkamasym6zd0ymdnglqdbsq5cm9cxrqmc4bmr")))

