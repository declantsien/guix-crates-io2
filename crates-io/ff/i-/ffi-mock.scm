(define-module (crates-io ff i- ffi-mock) #:use-module (crates-io))

(define-public crate-ffi-mock-0.1.0 (c (n "ffi-mock") (v "0.1.0") (d (list (d (n "ffi-mock-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xzh60ld886r8fb2mgi9wz7jaifala0nfn0h2y8jab8xmar5gzl4")))

