(define-module (crates-io ff i- ffi-export) #:use-module (crates-io))

(define-public crate-ffi-export-0.1.0 (c (n "ffi-export") (v "0.1.0") (d (list (d (n "ffi-export-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0agims9gx3krlih3c6ji9r97vh1lqkfb11sl0df0alwb55cbfbiv")))

(define-public crate-ffi-export-0.2.0 (c (n "ffi-export") (v "0.2.0") (d (list (d (n "ffi-export-proc-macro") (r "^0.2.0") (d #t) (k 0)))) (h "01xr3apnjb1858dd36r96hkq8sa58a4z53kfxxrz4vq9dm5wfz7c")))

