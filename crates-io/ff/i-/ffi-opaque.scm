(define-module (crates-io ff i- ffi-opaque) #:use-module (crates-io))

(define-public crate-ffi-opaque-0.1.0 (c (n "ffi-opaque") (v "0.1.0") (h "1g69akzpyfgg5d9bzdckybpz0591ksb250xvm3i8a996bscf6jsk")))

(define-public crate-ffi-opaque-1.0.0 (c (n "ffi-opaque") (v "1.0.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1y1dgmpgaaxgn1f71ai06rbf8dapfx9kynhir423bycrjr5qnb5c")))

(define-public crate-ffi-opaque-2.0.0 (c (n "ffi-opaque") (v "2.0.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "07dmswp3yszyf52wnh514k3382ivbv110f4n249g8z78m121lx8a")))

(define-public crate-ffi-opaque-2.0.1 (c (n "ffi-opaque") (v "2.0.1") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "06mqaid8ycf97nc6i7p5g9mbr8r9ysdnz56rrabrmvpjlxhaqm7c")))

