(define-module (crates-io ff i- ffi-closure) #:use-module (crates-io))

(define-public crate-ffi-closure-1.0.0 (c (n "ffi-closure") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)))) (h "1f65dxbp5dp0pz4agvd3lknpkkbvzkvvb77vhjq62nnp7vril5j2") (f (quote (("nightly"))))))

(define-public crate-ffi-closure-1.1.0 (c (n "ffi-closure") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "docfg") (r "^0.1.0") (d #t) (k 0)))) (h "11mv6imxy9pcmiczsfhxi2vqfkcianfq93pvfacbm6imrgf1yl9y") (f (quote (("nightly"))))))

