(define-module (crates-io ff i- ffi-convert) #:use-module (crates-io))

(define-public crate-ffi-convert-0.1.0 (c (n "ffi-convert") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ffi-convert-derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nj7mfad64dl5rwhvkbpkps6lcgmn5q2wfqvr6ja8faqfrjsbjfj")))

(define-public crate-ffi-convert-0.1.1 (c (n "ffi-convert") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ffi-convert-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lyylia9yxa7skk45fbcba7ivfqnk5a0cbgy79cqr9axx8af72lh")))

(define-public crate-ffi-convert-0.1.2 (c (n "ffi-convert") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ffi-convert-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xhv90zckjim2nd0yh132l8cbdpm84q14wk6wcw7wiz4lqgp97c8")))

(define-public crate-ffi-convert-0.2.0 (c (n "ffi-convert") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ffi-convert-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10all08qy1hhq3gqbmv5gakz8y4ikghnsg93r3b0bhy540jw9xfw")))

(define-public crate-ffi-convert-0.2.1 (c (n "ffi-convert") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ffi-convert-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z75viacjiyzzb7l75ybii6l0zkhmma0v3r5g7jywv9aysf3d6gk")))

(define-public crate-ffi-convert-0.2.2 (c (n "ffi-convert") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "ffi-convert-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02465w5b6g4y80yvyg36j68cfxmlk4ilyhfar54jq0azvnqx9gg2")))

(define-public crate-ffi-convert-0.3.0 (c (n "ffi-convert") (v "0.3.0") (d (list (d (n "ffi-convert-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0cqsnhxnwszjigy3xh8xza5cdw0xx8fy4rhswih0zbnbwc4hmivh")))

(define-public crate-ffi-convert-0.4.0 (c (n "ffi-convert") (v "0.4.0") (d (list (d (n "ffi-convert-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0hkniyvrlzv1k7sz1vwyr7f6hj8dcla08p3ibgl4d06a6kpgb2f6")))

(define-public crate-ffi-convert-0.5.0 (c (n "ffi-convert") (v "0.5.0") (d (list (d (n "ffi-convert-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1xjabfisj63ysq295rb12idm7pmax7rbwj0qvg44i6cgzp4b5s1n")))

(define-public crate-ffi-convert-0.6.0 (c (n "ffi-convert") (v "0.6.0") (d (list (d (n "ffi-convert-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "17g97z8i4agj2ai3pnp9iw7qsr4fbf1iad0wjj4h56vps5mc794w")))

(define-public crate-ffi-convert-0.6.1 (c (n "ffi-convert") (v "0.6.1") (d (list (d (n "ffi-convert-derive") (r "^0.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "14xckbq7zr2107l2f6khv2hjyl9w7wy239q62nfkk2l04hdrv5w3")))

