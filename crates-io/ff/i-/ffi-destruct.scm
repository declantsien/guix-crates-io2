(define-module (crates-io ff i- ffi-destruct) #:use-module (crates-io))

(define-public crate-ffi-destruct-0.1.0 (c (n "ffi-destruct") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15wm2v5hmgq17ksjnjv4jamzh98rqm448vfjj4qgcw72fsnvg99x")))

(define-public crate-ffi-destruct-0.1.1 (c (n "ffi-destruct") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cgsd9bj7a25hnp686rmzdg2nqv0vdlzp3zlh7ydii4ic4cvbcym")))

(define-public crate-ffi-destruct-0.1.2 (c (n "ffi-destruct") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r6sxaarbqybnfdx2mqpccw1p355daqm1k1gdnnlz9w77z6q2y6b")))

(define-public crate-ffi-destruct-0.1.3 (c (n "ffi-destruct") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x7hagzwfdh4iq83sipd7n15fg0in6v8ld3fc1q130yw6yq724kg")))

