(define-module (crates-io ff i- ffi-convert-derive) #:use-module (crates-io))

(define-public crate-ffi-convert-derive-0.1.0 (c (n "ffi-convert-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0w3140mgwxqcfz99iji79qd9m10r8jv6a6vi1bx3ccqbkv3brfbx")))

(define-public crate-ffi-convert-derive-0.1.1 (c (n "ffi-convert-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "03j3hw9zw063j5x0b92g2qgflxkimcwxyjg54qwg43657jz06mnl")))

(define-public crate-ffi-convert-derive-0.1.2 (c (n "ffi-convert-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1j2w8nkqlhbl4li2lvyih28vf9zwlkkf2vsdb2f06vrk79pm24px")))

(define-public crate-ffi-convert-derive-0.2.0 (c (n "ffi-convert-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0325qmwymbn5fw7xj4s2mxb0yjvzip1ymm3crlin4kkbsx6ybs70")))

(define-public crate-ffi-convert-derive-0.2.1 (c (n "ffi-convert-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0n4wry29ic3cflswnh88biyvh8r8nnkrvm3vjka5m86bargppl3a")))

(define-public crate-ffi-convert-derive-0.2.2 (c (n "ffi-convert-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wg2pqrrhpmvpjgqrc8009nq1lsgim7h16gpiv2ha29jdg892cj9")))

(define-public crate-ffi-convert-derive-0.3.0 (c (n "ffi-convert-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0j8332cmxk3q4j7rp10smzb23amh66ylqc3h01ls541p8ax6nb6j")))

(define-public crate-ffi-convert-derive-0.4.0 (c (n "ffi-convert-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0dvvaziqq5rmf6wvg7z44n09jcynnp76kjgl86ad0l190whphf70")))

(define-public crate-ffi-convert-derive-0.5.0 (c (n "ffi-convert-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1wsv2rgcvd97qv9355r8scsz071dyj0zbry2jgakw4h91f72rmff")))

(define-public crate-ffi-convert-derive-0.6.0 (c (n "ffi-convert-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1111gidvqzqbzbg9q4fy9c6cb1s360hy831zf4ri1q54dnvri9xd")))

(define-public crate-ffi-convert-derive-0.6.1 (c (n "ffi-convert-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1fmgabjp69vn585ybl3wpr4rrf248sfgaga8wqryr40fxq0si7py")))

