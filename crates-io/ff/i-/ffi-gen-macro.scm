(define-module (crates-io ff i- ffi-gen-macro) #:use-module (crates-io))

(define-public crate-ffi-gen-macro-0.1.0 (c (n "ffi-gen-macro") (v "0.1.0") (h "09h0x1xhna9c23vav0n8kx44208dyzwp464v9y3f8p0b6mdc70q7")))

(define-public crate-ffi-gen-macro-0.1.1 (c (n "ffi-gen-macro") (v "0.1.1") (h "17fsblsmaq69nrgy2hzw8l6q22prmd0xvns3qchdbjjvgw1lljrw")))

(define-public crate-ffi-gen-macro-0.1.2 (c (n "ffi-gen-macro") (v "0.1.2") (d (list (d (n "ffi-gen") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "09pzaspw1zv3wchvqp14yvwyzlw00dd417zx7l9jwjsfalglry39")))

