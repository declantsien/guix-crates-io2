(define-module (crates-io ff i- ffi-pool) #:use-module (crates-io))

(define-public crate-ffi-pool-0.1.0 (c (n "ffi-pool") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)) (d (n "objpool") (r "^0.2.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)))) (h "1121qa9xfl3khciqjb1hmk8nzp426srw5h4qf2p90vijjdgldsg3")))

(define-public crate-ffi-pool-0.1.1 (c (n "ffi-pool") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)) (d (n "objpool") (r "^0.2.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.0") (d #t) (k 0)))) (h "1gh2v6dadlav1xvs7wcgqvcqlskjw492639z4zd6p2l0k4gjkd40")))

