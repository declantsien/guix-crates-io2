(define-module (crates-io ff t2 fft2d) #:use-module (crates-io))

(define-public crate-fft2d-0.1.0 (c (n "fft2d") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (f (quote ("jpeg" "png"))) (k 2)) (d (n "nalgebra") (r "^0.29.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "rustdct") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "show-image") (r "^0.10.1") (f (quote ("image"))) (d #t) (k 2)))) (h "0lks0ncffa2a6ifhysfcd7bx292sjalmnfvvgwvzhdhfqjinizwc")))

(define-public crate-fft2d-0.1.1 (c (n "fft2d") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (f (quote ("jpeg" "png"))) (k 2)) (d (n "nalgebra") (r "^0.29.0") (f (quote ("std"))) (o #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rustdct") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "show-image") (r "^0.10.1") (f (quote ("image"))) (d #t) (k 2)))) (h "0h7b791yczz2sqkrybvx51k8zsmw9xw8gq7gs22lws1kxi32nj0w") (f (quote (("parallel" "rayon") ("default"))))))

