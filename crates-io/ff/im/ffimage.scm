(define-module (crates-io ff im ffimage) #:use-module (crates-io))

(define-public crate-ffimage-0.1.0 (c (n "ffimage") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b122n0kmmgkv84q67aggl3fyzbj4fcf3vjxfpakxdmrj62dpsb6") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.2.0 (c (n "ffimage") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ik1skhgn1fnx5s3j013z1bid5h7rgn4sg9r2dxr4fz5mkny15sh") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.3.0 (c (n "ffimage") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ymdz7vwjy9k554zv5wfy9jmn8z89shr0qjlrrc5vsab7p91348s") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.4.0 (c (n "ffimage") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "140z0fb7fp2lpblr892cwx01dlywr2gz613465i0g7y1jqw2xw8q") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.5.0 (c (n "ffimage") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11sgqvlywynlnnc8hks73x7xxhw8xlwj9i4g1icmwzzc2qmwams9") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.6.0 (c (n "ffimage") (v "0.6.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1dc56kyg7g3miyv4g425mcsgjr4ffjrlf5pkn4070yl4r4npnfk7") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.7.0 (c (n "ffimage") (v "0.7.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ir7rcyzd2vcqp3znymzs6qr06xkjkva4xnvkbxd0pw3mymjphwh") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.7.1 (c (n "ffimage") (v "0.7.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1psnhilmizl5101z6kz2nmsny3n1h02kqspz47025qh1ah1q59hj") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.8.0 (c (n "ffimage") (v "0.8.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1mya0qw70gplziamh2dkx4qyaq197zr011lw6fffwl4l5a238w0c") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.8.1 (c (n "ffimage") (v "0.8.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1p8kcf6mcvdvx8lprghas2ikrbfk08ipw0mmdv8xqcqcl1qs0g7r") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.8.2 (c (n "ffimage") (v "0.8.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1klzxnwma91pn1lydl3p3k8azw8lkwlr7zcjvkj24p32jmzm63r4") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.9.0 (c (n "ffimage") (v "0.9.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)))) (h "08cwk3s2br882dxvmg07qqvnw79fna4ppvpx56hnhg0q7k51n5c1") (f (quote (("default" "rayon"))))))

(define-public crate-ffimage-0.10.0 (c (n "ffimage") (v "0.10.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0djxl5cfcals4b6zdhnifdmmj8qbi4qfpjpra0niq75f0zx742pd")))

