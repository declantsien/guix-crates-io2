(define-module (crates-io ff _d ff_derive_wasm_unknown_unknown) #:use-module (crates-io))

(define-public crate-ff_derive_wasm_unknown_unknown-0.12.2 (c (n "ff_derive_wasm_unknown_unknown") (v "0.12.2") (d (list (d (n "addchain") (r "^0.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xlz6chixwdxnps8ns6gz28xmvqqzbyssxvdf2scx6wwma6jmwhl") (f (quote (("bits"))))))

