(define-module (crates-io ff _d ff_derive-zeroize) #:use-module (crates-io))

(define-public crate-ff_derive-zeroize-0.6.2 (c (n "ff_derive-zeroize") (v "0.6.2") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0fmmi19lfchbs1b178mp197rhspry4da42kldgr0n2hdpicl0kdj")))

