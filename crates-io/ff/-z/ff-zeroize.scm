(define-module (crates-io ff -z ff-zeroize) #:use-module (crates-io))

(define-public crate-ff-zeroize-0.6.1 (c (n "ff-zeroize") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "zeroize") (r "^1.1") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0zdmkzihvsf3nbf4gzjvp9fdprp1mjfxqhz0glhmbhl4yqmm2f64") (f (quote (("derive" "ff_derive") ("default"))))))

(define-public crate-ff-zeroize-0.6.2 (c (n "ff-zeroize") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "zeroize") (r "^1.1") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "17vb8whi6h79zms06p1h7yhmapwmz57qhzbp7p6fy4lfhf2l73dz") (f (quote (("derive" "ff_derive") ("default"))))))

(define-public crate-ff-zeroize-0.6.3 (c (n "ff-zeroize") (v "0.6.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ff_derive-zeroize") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "zeroize") (r "^1.1") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "1vi9n1v2bg44683h9ygv75w62xla67valvjirqba6njix2i6j8f0") (f (quote (("derive" "ff_derive-zeroize") ("default"))))))

