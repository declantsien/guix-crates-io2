(define-module (crates-io ff -d ff-derive-num) #:use-module (crates-io))

(define-public crate-ff-derive-num-0.1.0 (c (n "ff-derive-num") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0y1sxpqmmmkzkn278bms92ngmbways1jajap1pzapmb0wslm87q2")))

(define-public crate-ff-derive-num-0.1.1 (c (n "ff-derive-num") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "084l96rqyy9hgmfpjlifigmzkgw0n0251irgqxd9hqci4il7bf9z")))

(define-public crate-ff-derive-num-0.1.2 (c (n "ff-derive-num") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0bwssklvfr2jijif1356ravpizpd2mwgi2d6qiqp9frd5a6d041g")))

(define-public crate-ff-derive-num-0.2.0 (c (n "ff-derive-num") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1x2cxnc4dqdzrs1cpir6shx10wi3q5g2fylj3r40dgl3p4d4a5g7")))

