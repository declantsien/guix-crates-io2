(define-module (crates-io ff -p ff-particles) #:use-module (crates-io))

(define-public crate-ff-particles-0.1.0 (c (n "ff-particles") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bdimyicp4k1c6shwy21jrlzzggldl1mmkqn2nia8c2rlklfx6d0")))

(define-public crate-ff-particles-0.1.1 (c (n "ff-particles") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0csql8v5i8sv09xpxksk40jk8kxn25cnyxxw1flabf1bcw19cmvk")))

(define-public crate-ff-particles-0.1.2 (c (n "ff-particles") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h2522gq90r32m09aj70nfms157ssm68j9wv6rl54g5p6g20aq25")))

