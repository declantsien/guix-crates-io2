(define-module (crates-io ff -p ff-profile) #:use-module (crates-io))

(define-public crate-ff-profile-0.1.0 (c (n "ff-profile") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)))) (h "1rp1ymvhksrrx44islay76x00bf8h153jaxafcf4l8rhhivq1lfm")))

