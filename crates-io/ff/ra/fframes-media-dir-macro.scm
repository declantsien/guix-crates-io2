(define-module (crates-io ff ra fframes-media-dir-macro) #:use-module (crates-io))

(define-public crate-fframes-media-dir-macro-1.0.0-beta.1 (c (n "fframes-media-dir-macro") (v "1.0.0-beta.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "fframes-media-loaders") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1wz6dhm2indb486z9wn2p8jbnr03dfkqwcfnp8fpza6yj7d3mrkw") (f (quote (("nightly") ("metadata"))))))

