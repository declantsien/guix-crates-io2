(define-module (crates-io ff #{14}# ff14_avatar) #:use-module (crates-io))

(define-public crate-ff14_avatar-0.1.0 (c (n "ff14_avatar") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "1yvp8lfaqlzrzl74dw10mhm579qwyzhk9v1zn3i5r3n1l94pzh53")))

(define-public crate-ff14_avatar-0.1.1 (c (n "ff14_avatar") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "1insakz5assdclrrm02jcn2d70vx5yigq2a4ldpvgbhzzqaxi7v0")))

(define-public crate-ff14_avatar-0.1.2 (c (n "ff14_avatar") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "18rh8mb3vv0mf76a25p19pj0r0b2ijvbirz2mh16a090wnwbps7s")))

(define-public crate-ff14_avatar-0.1.3 (c (n "ff14_avatar") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "1vc7w72yqqq3fq5qidzkc2nr7h0fx0bhq8nfn16z9pm44f9pyvn1")))

(define-public crate-ff14_avatar-0.1.4 (c (n "ff14_avatar") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "1bp8r1ak9x5maj64wxac3ccq17xb8ik1a1ad9m61a8zf73bb6pkp")))

(define-public crate-ff14_avatar-0.1.5 (c (n "ff14_avatar") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "0hjcz477mfv761xdna9z5rvq64cisnbf0saiq11ly5h7pmd1vl0h")))

(define-public crate-ff14_avatar-0.1.6 (c (n "ff14_avatar") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "0wy68sji03vlbmdqmkvd2wq4ixds2fz7akz8xgvlmfh49h4cp5gr")))

(define-public crate-ff14_avatar-0.1.7 (c (n "ff14_avatar") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 0)))) (h "0ih4k24zhlg8kj14y05z1h47fbify3wvdnvf2zbc67iglhnyy2y3")))

