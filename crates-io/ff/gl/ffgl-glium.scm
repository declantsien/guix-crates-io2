(define-module (crates-io ff gl ffgl-glium) #:use-module (crates-io))

(define-public crate-ffgl-glium-0.1.0 (c (n "ffgl-glium") (v "0.1.0") (d (list (d (n "ffgl-core") (r "^0.2.2") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glium") (r "^0.33.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("log"))) (d #t) (k 0)))) (h "1j91lj4q6s4px94qihy4n42qhvkm5p83kwcgfpj07jqj9c0bxzmf")))

(define-public crate-ffgl-glium-0.1.1 (c (n "ffgl-glium") (v "0.1.1") (d (list (d (n "ffgl-core") (r "^0.2.5") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glium") (r "^0.33.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("log"))) (d #t) (k 0)))) (h "1li4fdaw0w70vlsmm5zghr6vb2630yx2wgnfzbryn4455za0i9i3")))

(define-public crate-ffgl-glium-0.1.2 (c (n "ffgl-glium") (v "0.1.2") (d (list (d (n "ffgl-core") (r "^0.2.6") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glium") (r "^0.33.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("log"))) (d #t) (k 0)))) (h "14vjs8wpfhgilr8mckjyl1jh2x7xf9k9j6nj171bzdlhnjim1fzw")))

