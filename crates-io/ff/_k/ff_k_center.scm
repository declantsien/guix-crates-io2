(define-module (crates-io ff _k ff_k_center) #:use-module (crates-io))

(define-public crate-ff_k_center-1.2.2 (c (n "ff_k_center") (v "1.2.2") (d (list (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "pyo3") (r "^0.14.4") (f (quote ("extension-module" "abi3-py36"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1k8yijf5kb1kbb6swqpxy6zhx2m8cfga8h2f11qwlwiywvph17zl")))

