(define-module (crates-io ff to fftools) #:use-module (crates-io))

(define-public crate-fftools-1.0.0 (c (n "fftools") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (f (quote ("eq-separator" "short-space-opt" "combined-flags"))) (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)))) (h "1pfl1rr3ik61zvmzch5iaavvc8b025hjmq02wxizb1gq4v3bvi18")))

