(define-module (crates-io ff is ffishim) #:use-module (crates-io))

(define-public crate-ffishim-0.1.0 (c (n "ffishim") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "heck") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kghgwxrlb8mpx7gnnalssp1m9qf41i3fcmw03p7fzqix6mwk08z")))

(define-public crate-ffishim-0.1.1 (c (n "ffishim") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "heck") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1lgdq3kpqh7cnrvqyyyda41cbbm7y45cjmkz2pqw2xv535b93nkh")))

(define-public crate-ffishim-0.1.2 (c (n "ffishim") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "heck") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00vpp1mra8455jwldmjd89aqfch07jgi9gj6r20v33wyqhxzk17r")))

