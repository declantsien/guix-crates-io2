(define-module (crates-io ff is ffishim_derive) #:use-module (crates-io))

(define-public crate-ffishim_derive-0.1.0 (c (n "ffishim_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "ffishim") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "022wsqa7vd936ka6gbxrvz2w3m2n5nx04qsyyfld01w61f1yra9y")))

(define-public crate-ffishim_derive-0.1.1 (c (n "ffishim_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "ffishim") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "00yi2c6y5xyrwqhnq8j4sk7rchfx9kq584a82r2xc8ljslh3h8gp")))

(define-public crate-ffishim_derive-0.1.2 (c (n "ffishim_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "ffishim") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0if920cf2z67wnj87gcbxmcz8880nng0q72bfw99vd06zjzfa8lf")))

