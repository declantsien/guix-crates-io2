(define-module (crates-io ff ms ffms2-sys) #:use-module (crates-io))

(define-public crate-ffms2-sys-0.1.0 (c (n "ffms2-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "07iggscnqkrxr20j1sb5dfwnjla91cwbgl34c6lh36nkisn3xfx5")))

(define-public crate-ffms2-sys-0.2.0 (c (n "ffms2-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0jmbwc2dfn7r54q69ryqfislrm7m2g20v677kp6dbrw56ygssr59")))

