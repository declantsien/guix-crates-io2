(define-module (crates-io ff nn ffnn) #:use-module (crates-io))

(define-public crate-ffnn-0.1.0 (c (n "ffnn") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c7s610lj0s28bwpfkx7cb5wly9ps59dxczydkdwh1jv94h1hnqd")))

