(define-module (crates-io ff pr ffprobe) #:use-module (crates-io))

(define-public crate-ffprobe-0.1.0 (c (n "ffprobe") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1wjlw7f6362i947hx7dpc8zqc7cxjqvnlnwlpdgz42ld2avhfxyc") (f (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.2.0 (c (n "ffprobe") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "195265rcq3bxnv52c5zqy1lgghnn9d6cas942i510ihkvipvgy3a") (f (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3.0 (c (n "ffprobe") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0si1zilg30j4wlypj0y2p2qrdak06fbqpygrppjr7752f1qdafgl") (f (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3.1 (c (n "ffprobe") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0yhhqzx13944dcbk9f12z8bfk8k091c3x7ry71vhy78xk70yfbz5") (f (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3.2 (c (n "ffprobe") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1b0lnjgd75p5fqqm9s72sglgsbgh4j1ri8dc9864173hldjd6la1") (f (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3.3 (c (n "ffprobe") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "190hd7bwpmlzd9glyzv5zfk4jvwwdjhby05zrrszqadh9abh7mmm") (f (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.4.0 (c (n "ffprobe") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0b55x6s5bmlwv0fh2zwqbwqizzlxbjwxnamvnlfibb7rw4szizlg") (f (quote (("__internal_deny_unknown_fields"))))))

