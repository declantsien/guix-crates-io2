(define-module (crates-io ff iz ffizz-header) #:use-module (crates-io))

(define-public crate-ffizz-header-0.3.0 (c (n "ffizz-header") (v "0.3.0") (d (list (d (n "ffizz-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "linkme") (r "^0.3.3") (d #t) (k 0)))) (h "18mmafdyjqg6mwyah5bilqd7iv5cb8bqp7i6xg27gq5a3x4zss2r")))

(define-public crate-ffizz-header-0.3.2 (c (n "ffizz-header") (v "0.3.2") (d (list (d (n "ffizz-macros") (r "^0.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "linkme") (r "^0.3.3") (d #t) (k 0)))) (h "12s145jncjyvn9331lr623g499fmxz9zgdq865rvf54zm1vmr336")))

(define-public crate-ffizz-header-0.3.3 (c (n "ffizz-header") (v "0.3.3") (d (list (d (n "ffizz-macros") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "linkme") (r "^0.3.3") (d #t) (k 0)))) (h "09f8cs66nnlmkig9skc4k5v7mv2nzaps1zsm1i8xb7wj5bxr6sh4")))

(define-public crate-ffizz-header-0.3.4 (c (n "ffizz-header") (v "0.3.4") (d (list (d (n "ffizz-macros") (r "^0.3.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "linkme") (r "^0.3.3") (d #t) (k 0)))) (h "01nfrnmk1q906mlxsvszzlhyaskprhk0qnl0qzgmwaycvkl3lsva")))

(define-public crate-ffizz-header-0.4.0 (c (n "ffizz-header") (v "0.4.0") (d (list (d (n "ffizz-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "linkme") (r "^0.3.3") (d #t) (k 0)))) (h "0d4cc24x2hnnynv3s7fg0gwbcgyxdv54b9k3yvd94nxvq92amyss")))

(define-public crate-ffizz-header-0.4.1 (c (n "ffizz-header") (v "0.4.1") (d (list (d (n "ffizz-macros") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "linkme") (r "^0.3.3") (d #t) (k 0)))) (h "0l5fbap4166fvb37wib3iph6v5rjzhznari966grjvlg8121abxn")))

(define-public crate-ffizz-header-0.5.0 (c (n "ffizz-header") (v "0.5.0") (d (list (d (n "ffizz-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "linkme") (r "^0.3.3") (d #t) (k 0)))) (h "0f45p6ma5k46bsrcqq4fgl8n6fd1i497x5wx0lwwd90ay3lm46la")))

