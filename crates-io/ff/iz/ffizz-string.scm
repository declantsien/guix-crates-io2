(define-module (crates-io ff iz ffizz-string) #:use-module (crates-io))

(define-public crate-ffizz-string-0.3.0 (c (n "ffizz-string") (v "0.3.0") (d (list (d (n "ffizz-passby") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.131") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "00ssrxqgz0393dy926nhs87jdjivrr295138wq1fxyjspfxi43dk")))

(define-public crate-ffizz-string-0.3.2 (c (n "ffizz-string") (v "0.3.2") (d (list (d (n "ffizz-header") (r "^0.3.2") (d #t) (k 0)) (d (n "ffizz-passby") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.131") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0p1l6m3rdyy9l3fr09jmzjqdkw4aml695hmv4ygpni7blrzixryx")))

(define-public crate-ffizz-string-0.3.3 (c (n "ffizz-string") (v "0.3.3") (d (list (d (n "ffizz-header") (r "^0.3.3") (d #t) (k 0)) (d (n "ffizz-passby") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.131") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "17wg8004ggfb13snqvx91bvhcm0dl85vd1gf1dxd2bddpq73g8dj")))

(define-public crate-ffizz-string-0.3.4 (c (n "ffizz-string") (v "0.3.4") (d (list (d (n "ffizz-header") (r "^0.3.4") (d #t) (k 0)) (d (n "ffizz-passby") (r "^0.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.131") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0xgjxjl5lldi7ckwyj67c3f54ksvrx0x5w78nk3wjnx598rs05c2")))

(define-public crate-ffizz-string-0.4.0 (c (n "ffizz-string") (v "0.4.0") (d (list (d (n "ffizz-header") (r "^0.4.0") (d #t) (k 0)) (d (n "ffizz-passby") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0qvgv3zvj7znpfbnyi3waik0ldkk8nryiyjgmi8xpdbxp5pc8nip")))

(define-public crate-ffizz-string-0.4.1 (c (n "ffizz-string") (v "0.4.1") (d (list (d (n "ffizz-header") (r "^0.4.1") (d #t) (k 0)) (d (n "ffizz-passby") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1gwvq30awh1mli549337afmfl3a8xqqcxyynjnq8r60xpsza78wq")))

(define-public crate-ffizz-string-0.5.0 (c (n "ffizz-string") (v "0.5.0") (d (list (d (n "ffizz-header") (r "^0.5.0") (d #t) (k 0)) (d (n "ffizz-passby") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1mnz3hw3b01xykzbhlwlxc8k5b12813hfgca8r7aym762xr335za")))

