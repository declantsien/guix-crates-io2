(define-module (crates-io ff iz ffizz-macros) #:use-module (crates-io))

(define-public crate-ffizz-macros-0.3.0 (c (n "ffizz-macros") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nhrwqy30l4jqfkqm0b5zg4zcvc4dm6rd6pwlwlc625j1s4ikcx5")))

(define-public crate-ffizz-macros-0.3.2 (c (n "ffizz-macros") (v "0.3.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lh4p5f210iwfwkq9aj1ncp3kv5drz0b8zlvy5lc4s9cxs8c01r9")))

(define-public crate-ffizz-macros-0.3.3 (c (n "ffizz-macros") (v "0.3.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ljarc2nbj6gahabr5x8g1m1ksc9158q7aisjbxj84n4rz2zyhbq")))

(define-public crate-ffizz-macros-0.3.4 (c (n "ffizz-macros") (v "0.3.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "065cfynk8sxsjk48fbg46rr0a39zp2pvj4kjkw4sxg5pzl1kfq2n")))

(define-public crate-ffizz-macros-0.4.0 (c (n "ffizz-macros") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0267yvbs6c1p81ilv0l8000h0aiwzyr3d7nzfkc1n8ynw6xfb7xk")))

(define-public crate-ffizz-macros-0.4.1 (c (n "ffizz-macros") (v "0.4.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s7vc1kw74isxykwsh79rikmp62d45hx1kajfic3i78gb1xx62v7")))

(define-public crate-ffizz-macros-0.5.0 (c (n "ffizz-macros") (v "0.5.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "013sipayrcdnf7s3hdsg43dizipic1jc5c2zy323xavvapm8q85g")))

