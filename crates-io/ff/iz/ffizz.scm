(define-module (crates-io ff iz ffizz) #:use-module (crates-io))

(define-public crate-ffizz-0.4.1 (c (n "ffizz") (v "0.4.1") (d (list (d (n "ffizz-header") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "ffizz-macros") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "ffizz-passby") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "ffizz-string") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0xrlnvy4s0pm4qdn19mv9srnv0sd8zmr463v9ljagx1dq6mkmy8r") (s 2) (e (quote (("string" "dep:ffizz-string" "passby" "header") ("passby" "dep:ffizz-passby") ("macros" "dep:ffizz-macros") ("header" "dep:ffizz-header" "macros"))))))

(define-public crate-ffizz-0.5.0 (c (n "ffizz") (v "0.5.0") (h "0iy95vvcvqrjmdfrqz9ifqxkzhfdjnz00g2lpz886cz4n5anzrmp")))

