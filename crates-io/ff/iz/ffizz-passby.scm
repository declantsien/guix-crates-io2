(define-module (crates-io ff iz ffizz-passby) #:use-module (crates-io))

(define-public crate-ffizz-passby-0.1.0 (c (n "ffizz-passby") (v "0.1.0") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1hzcbcvm3szck2mfqgbjc4k04baz49iqk3lsbpyizaya1b1advp4")))

(define-public crate-ffizz-passby-0.1.1 (c (n "ffizz-passby") (v "0.1.1") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "19ajxcifgp5c6ny18jf3j9zi54x62w6cf013wjlacvl16drvbm82")))

(define-public crate-ffizz-passby-0.2.0 (c (n "ffizz-passby") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1f9c1jjg2559sldkkfp3b3dgj33j34wfbc8g4zflxc0gg17pf3q6")))

(define-public crate-ffizz-passby-0.3.0 (c (n "ffizz-passby") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1381cvzcrswnrpmpafaxrdsrfd45vcg5rxpps1clppp869rvgxkv")))

(define-public crate-ffizz-passby-0.3.1 (c (n "ffizz-passby") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0vavgv9j00rz2rbayrz3x4dbwvbpr91k84hybnp5gnkvasrq8y0p")))

(define-public crate-ffizz-passby-0.3.2 (c (n "ffizz-passby") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0nl8rlgxdpl3a6hwlkx52i7yhh6afs698c14ri96p1zl7pvdzagy")))

(define-public crate-ffizz-passby-0.3.3 (c (n "ffizz-passby") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0yha0jwd04wy06cbyj803sz82kh935rvqc6fphyy2d1b8sraxv5x")))

(define-public crate-ffizz-passby-0.3.4 (c (n "ffizz-passby") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "021cc929vmcvcfn755l2wq8zbgdz1ndv5x1nvb34jbyf7myslmbp")))

(define-public crate-ffizz-passby-0.4.0 (c (n "ffizz-passby") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "13g9g6l1mr5x5mk2xff467gnv7qpjach22zgxz391ax4qx21i7f4")))

(define-public crate-ffizz-passby-0.4.1 (c (n "ffizz-passby") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1c75p9k8174bpdrhb8c5p3nv0n9qgf7kvf26ia1sg14648c6dnkx")))

(define-public crate-ffizz-passby-0.5.0 (c (n "ffizz-passby") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.129") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1h6rm0yr7mf5qylffhpc22g4ih3zywm3mqdd5qw6zhpzdqphrgjy")))

