(define-module (crates-io ff fx fffx) #:use-module (crates-io))

(define-public crate-fffx-0.1.0 (c (n "fffx") (v "0.1.0") (d (list (d (n "bytelines") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (f (quote ("zlib-ng"))) (k 0)) (d (n "humansize") (r "^2.0.0") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1aqk0jw4x7hlhia72rjiwjf15qik3211bv8xjaj298yp41vydg14")))

(define-public crate-fffx-0.1.1 (c (n "fffx") (v "0.1.1") (d (list (d (n "bytelines") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (f (quote ("zlib-ng"))) (k 0)) (d (n "humansize") (r "^2.0.0") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0rx3mgwvl4rfhwclmzzg66mrfh52dqzgxkn8wv55xiwvcnlcfj5l")))

(define-public crate-fffx-0.1.2 (c (n "fffx") (v "0.1.2") (d (list (d (n "bytelines") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (k 0)) (d (n "humansize") (r "^2.0.0") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0bym6195nbgp3zc9pp2vyx60jzpf8jfy2h7j6kz8d19qh3frijpp")))

(define-public crate-fffx-0.1.3 (c (n "fffx") (v "0.1.3") (d (list (d (n "bytelines") (r "=2.2.2") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (k 0)) (d (n "humansize") (r "^2.0.0") (d #t) (k 2)) (d (n "simdutf8") (r "^0.1.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "09pn702bg51bycd80lgf8qlcarmf3my6f5bysrb9vzawgawj0g7k")))

