(define-module (crates-io ff -g ff-group-tests) #:use-module (crates-io))

(define-public crate-ff-group-tests-0.12.0 (c (n "ff-group-tests") (v "0.12.0") (d (list (d (n "group") (r "^0.12") (d #t) (k 0)) (d (n "k256") (r "^0.11") (f (quote ("bits"))) (d #t) (k 2)) (d (n "p256") (r "^0.11") (f (quote ("bits"))) (d #t) (k 2)))) (h "0v1c6fls0n727c3lzxndfpixl57qaamhfqhn35vl6bnyydnzj1an")))

(define-public crate-ff-group-tests-0.12.1 (c (n "ff-group-tests") (v "0.12.1") (d (list (d (n "group") (r "^0.12") (d #t) (k 0)) (d (n "k256") (r "^0.12") (f (quote ("bits"))) (d #t) (k 2)) (d (n "p256") (r "^0.12") (f (quote ("bits"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0izxllg7mv479r6w0p2g9jr8hygjk1bk8wmh2wjkx2mvb6dsnr2d")))

(define-public crate-ff-group-tests-0.12.2 (c (n "ff-group-tests") (v "0.12.2") (d (list (d (n "group") (r "^0.12") (d #t) (k 0)) (d (n "k256") (r "^0.12") (f (quote ("bits"))) (d #t) (k 2)) (d (n "p256") (r "^0.12") (f (quote ("bits"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1wm7dg9caix8pxxsjlwfc87wrxai2dkycslscp2xaqjrn3qz0jpp")))

(define-public crate-ff-group-tests-0.13.0 (c (n "ff-group-tests") (v "0.13.0") (d (list (d (n "bls12_381") (r "^0.8") (d #t) (k 2)) (d (n "group") (r "^0.13") (d #t) (k 0)) (d (n "k256") (r "^0.13") (f (quote ("bits"))) (d #t) (k 2)) (d (n "p256") (r "^0.13") (f (quote ("bits"))) (d #t) (k 2)) (d (n "pasta_curves") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)))) (h "05kypadrlwwlvggjr98pahv2i77h195vb910q5v51r8aa0ywlaa5")))

(define-public crate-ff-group-tests-0.13.1 (c (n "ff-group-tests") (v "0.13.1") (d (list (d (n "bls12_381") (r "^0.8") (d #t) (k 2)) (d (n "ff") (r "^0.13") (f (quote ("bits"))) (d #t) (k 0)) (d (n "group") (r "^0.13") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (f (quote ("std" "arithmetic" "bits"))) (k 2)) (d (n "p256") (r "^0.13.1") (f (quote ("std" "arithmetic" "bits"))) (k 2)) (d (n "pasta_curves") (r "^0.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (d #t) (k 0)))) (h "1zn14xhwyhsd8dl5d7ghx51kffrm9p2gkpys2vvvh11jzja04m2c") (r "1.60")))

