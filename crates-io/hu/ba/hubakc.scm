(define-module (crates-io hu ba hubakc) #:use-module (crates-io))

(define-public crate-hubakc-0.1.5 (c (n "hubakc") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https-native" "proxy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1k9ajkdybymdvy9fvxcghr4b3017bixadqnyrknv7f3hw4xlik5f")))

