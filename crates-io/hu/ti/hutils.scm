(define-module (crates-io hu ti hutils) #:use-module (crates-io))

(define-public crate-hutils-0.1.0 (c (n "hutils") (v "0.1.0") (h "1k0ac5kbmlr33d8hgzrk06a8327jvxfyrpgka69isbql4a4i8w82")))

(define-public crate-hutils-0.1.1 (c (n "hutils") (v "0.1.1") (h "1ib4bn9vrrpvwysrk0zk46b00pajvlfbnfign0w1zdgw08lq4s2b")))

