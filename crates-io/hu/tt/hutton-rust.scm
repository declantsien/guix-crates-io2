(define-module (crates-io hu tt hutton-rust) #:use-module (crates-io))

(define-public crate-hutton-rust-0.1.0 (c (n "hutton-rust") (v "0.1.0") (h "1rq1ij0pg5fq8zsa4id7dl4zszbmwpqy82m1119a04653alhx8yx")))

(define-public crate-hutton-rust-0.1.1 (c (n "hutton-rust") (v "0.1.1") (h "1mzffw5gqdlqac1b09nba59nb3nh52f9j3kdqwla4dmj5mpj7cbm")))

