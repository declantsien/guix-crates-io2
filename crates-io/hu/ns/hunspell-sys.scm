(define-module (crates-io hu ns hunspell-sys) #:use-module (crates-io))

(define-public crate-hunspell-sys-0.1.0 (c (n "hunspell-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1w9rsys2jj863vkn8jlh27lk506vrlzagmk5jsnii53p5mnzhxgw")))

(define-public crate-hunspell-sys-0.1.1 (c (n "hunspell-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0mw9lpnsn8019n7gv49wx2qabiymvvix6yz2m31av525nm5qgfdh")))

(define-public crate-hunspell-sys-0.1.2 (c (n "hunspell-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.2.1") (d #t) (k 1)) (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1wzl46yb00lhszrl7fyg15ks5xj84f9d2s4ahzvpvb4kvvdb9n0r")))

(define-public crate-hunspell-sys-0.1.3 (c (n "hunspell-sys") (v "0.1.3") (d (list (d (n "autotools") (r "^0.2.1") (d #t) (k 1)) (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1784869wkkgmk2lx1bf8wynbsyn7g90bdwx84bwfkvydj8byijvh")))

(define-public crate-hunspell-sys-0.2.0 (c (n "hunspell-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1fl6f5is5c4df9b9m53qahkjncjkyigjkkbqmg39j2xzkify69p6") (f (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.2.1 (c (n "hunspell-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1knx937ic4wnlfcrx9dn0763705z8ydnmxs11jpyc7r1x5wdhhxq") (f (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.2.2 (c (n "hunspell-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0kmlknlz3irlfyrdzv850z2jlh18spj39gk3lyaasqqvsbildlhm") (f (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.3.0 (c (n "hunspell-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.58") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "17igwhlhk07fgz21dbnj78ing9gs92bj4f7hrsk7xhym478161br") (f (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.3.1 (c (n "hunspell-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.61.0") (f (quote ("logging" "which-rustfmt"))) (k 1)) (d (n "cc") (r "^1.0.58") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1v3x1gicafv2jlq1vr9hf9vgsxaiinflzni0bizcnyp4ji9n0a4j") (f (quote (("static_libclang" "bindgen/static") ("static") ("default" "bindgen/runtime") ("bundled" "cc"))))))

