(define-module (crates-io hu ns hunspell) #:use-module (crates-io))

(define-public crate-hunspell-0.1.0 (c (n "hunspell") (v "0.1.0") (h "1pj68n5wgyzd2j93g05hjfqkq4mpb9r6kc8fcpab0vd6669hm84z")))

(define-public crate-hunspell-0.1.1 (c (n "hunspell") (v "0.1.1") (h "0h92svciilalj4lssn8dkm0njlym6f8nh8r76zxnjdvh7n4lwd9a")))

(define-public crate-hunspell-0.1.2 (c (n "hunspell") (v "0.1.2") (h "1cg06k6ccdlk0vp270ir7slwcrj5npgv89v04mf1mm5d34cli8cj")))

