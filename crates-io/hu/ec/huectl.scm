(define-module (crates-io hu ec huectl) #:use-module (crates-io))

(define-public crate-huectl-0.5.1 (c (n "huectl") (v "0.5.1") (d (list (d (n "envconfig") (r "^0.8") (d #t) (k 0)) (d (n "envconfig_derive") (r "^0.8") (d #t) (k 0)) (d (n "huelib") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1fp4blmmwgn64i9m70nnaj0rq8i442jpjhfnr2ljs493jgl4bl18")))

(define-public crate-huectl-0.5.2 (c (n "huectl") (v "0.5.2") (d (list (d (n "envconfig") (r "^0.8") (d #t) (k 0)) (d (n "envconfig_derive") (r "^0.8") (d #t) (k 0)) (d (n "huelib") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02q0c0h84ncy9w4lviqww2yq1q0vxvgxhv9yrj24aj1im4i2qvzh")))

