(define-module (crates-io hu bp hubpack_derive) #:use-module (crates-io))

(define-public crate-hubpack_derive-0.1.0 (c (n "hubpack_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1342pxnvdv4p91i04k67pdz1jknz9jjc43m4h84zajrkkf0p9g9m")))

(define-public crate-hubpack_derive-0.1.1 (c (n "hubpack_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0nhjrinvml9yvswdrnbrzmbp12dmz2092c7pis0yhvpimwh874hg")))

