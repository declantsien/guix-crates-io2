(define-module (crates-io hu bp hubpack) #:use-module (crates-io))

(define-public crate-hubpack-0.1.0 (c (n "hubpack") (v "0.1.0") (d (list (d (n "hubpack_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 2)))) (h "135q4bx6dzyjmjj5qmzfhq7qic3sjc8i7shml97v75sdm7ifbq3h")))

(define-public crate-hubpack-0.1.1 (c (n "hubpack") (v "0.1.1") (d (list (d (n "hubpack_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 2)))) (h "1lqmny19c98v3227hj73jwsipsa8f9zib3bz4wzdg7dla473r3xz")))

(define-public crate-hubpack-0.1.2 (c (n "hubpack") (v "0.1.2") (d (list (d (n "hubpack_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 2)))) (h "00s3abngbfldqdfbvv94c11962884y1rkam31dggc6g5x95bi831")))

