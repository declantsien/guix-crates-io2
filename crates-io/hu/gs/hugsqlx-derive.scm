(define-module (crates-io hu gs hugsqlx-derive) #:use-module (crates-io))

(define-public crate-hugsqlx-derive-0.1.1 (c (n "hugsqlx-derive") (v "0.1.1") (d (list (d (n "hugsqlx-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "0rmzfjig5wvi88f836qj1ncanfgagkqrfrsdk22qp2pg4i52wccz") (f (quote (("sqlite" "hugsqlx-core/sqlite") ("postgres" "hugsqlx-core/postgres") ("mysql" "hugsqlx-core/mysql"))))))

(define-public crate-hugsqlx-derive-0.2.0 (c (n "hugsqlx-derive") (v "0.2.0") (d (list (d (n "hugsqlx-core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "0d77ibzcsqz7wcp8576n9mn5d3nbdwsv3ry0ccaw9m208sr53w7k") (f (quote (("sqlite" "hugsqlx-core/sqlite") ("postgres" "hugsqlx-core/postgres") ("mysql" "hugsqlx-core/mysql"))))))

(define-public crate-hugsqlx-derive-0.3.0 (c (n "hugsqlx-derive") (v "0.3.0") (d (list (d (n "hugsqlx-core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)))) (h "13rqk53g39q21pvmsc131n6hf7w2j5zs39xng28gpg15y5md5pwr") (f (quote (("sqlite" "hugsqlx-core/sqlite") ("postgres" "hugsqlx-core/postgres") ("mysql" "hugsqlx-core/mysql"))))))

