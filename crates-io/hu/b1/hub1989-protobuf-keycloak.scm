(define-module (crates-io hu b1 hub1989-protobuf-keycloak) #:use-module (crates-io))

(define-public crate-hub1989-protobuf-keycloak-0.0.1 (c (n "hub1989-protobuf-keycloak") (v "0.0.1") (d (list (d (n "grpc-build") (r "^5.0.0") (d #t) (k 1)) (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)))) (h "0g4qwhdq3zqmxds2c0z6nqk3afrff56c7g1vgim363nlhilc2k9g") (y #t)))

(define-public crate-hub1989-protobuf-keycloak-0.0.2 (c (n "hub1989-protobuf-keycloak") (v "0.0.2") (d (list (d (n "grpc-build") (r "^5.0.0") (d #t) (k 1)) (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)))) (h "0ri3yz93k34izc3v8pjhp74vl8ysphnllshwvw4s2qild73f7sh8") (y #t)))

(define-public crate-hub1989-protobuf-keycloak-0.0.3 (c (n "hub1989-protobuf-keycloak") (v "0.0.3") (d (list (d (n "grpc-build") (r "^5.0.0") (d #t) (k 1)) (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)))) (h "1al6zkydr3s4hia0riryl7w5a0gizn2ywjpfh11mpzd9ihbc80g2")))

(define-public crate-hub1989-protobuf-keycloak-0.0.4 (c (n "hub1989-protobuf-keycloak") (v "0.0.4") (d (list (d (n "grpc-build") (r "^5.0.0") (d #t) (k 1)) (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)))) (h "11klb7ybybgf9imap2ylicibk3ghgkd3kl27nf6qcckjcdlccci5")))

(define-public crate-hub1989-protobuf-keycloak-0.0.5 (c (n "hub1989-protobuf-keycloak") (v "0.0.5") (d (list (d (n "grpc-build") (r "^5.0.0") (d #t) (k 1)) (d (n "grpc-build-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (d #t) (k 0)))) (h "06l7x9j8nrf5rakzj0168xknkzknv0y2g10ca50nqg1dpf8sbchq")))

(define-public crate-hub1989-protobuf-keycloak-0.0.6 (c (n "hub1989-protobuf-keycloak") (v "0.0.6") (d (list (d (n "grpc-build") (r "^6.0.0") (d #t) (k 1)) (d (n "grpc-build-core") (r "^0.3.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "tonic") (r "^0.10.0") (d #t) (k 0)))) (h "0g7ym0pzxx8w0nmmxb4k9nmxv7z8rgim419dc5l4mhgy1lzan8qp")))

