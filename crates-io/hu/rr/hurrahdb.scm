(define-module (crates-io hu rr hurrahdb) #:use-module (crates-io))

(define-public crate-hurrahdb-0.1.0 (c (n "hurrahdb") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xnv021mx53ljm398a0wyil0m6cbnynqd9ip152g01wqpkssqax9")))

