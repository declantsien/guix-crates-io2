(define-module (crates-io hu mp humphrey) #:use-module (crates-io))

(define-public crate-humphrey-0.1.0 (c (n "humphrey") (v "0.1.0") (h "0hv4w08iypp5xi85828y0yf84p2vp7m2fmcq1qimr7055wdjkv5r")))

(define-public crate-humphrey-0.1.1 (c (n "humphrey") (v "0.1.1") (h "1pn5zqinchqhbyj34nq04386bzc8cdh7m2ba027pwf47hp3xkf3k")))

(define-public crate-humphrey-0.1.2 (c (n "humphrey") (v "0.1.2") (h "1bf7yjhf9dkix1yv7zf3iw49pdmg7ji44fas6x8hm74dla9zwhl0")))

(define-public crate-humphrey-0.1.3 (c (n "humphrey") (v "0.1.3") (h "04cvv55hxyq0hk2h8v543yblwalwnsvysc69vij7jp4194jk6xpf")))

(define-public crate-humphrey-0.2.0 (c (n "humphrey") (v "0.2.0") (h "0l303v07hvah9j7mnqz4czqqnmd7j4vd0a4mzly042cy662wm6c3")))

(define-public crate-humphrey-0.2.1 (c (n "humphrey") (v "0.2.1") (h "0v8w52y409sx6nb5yhi92qph9bbdjigcxrvcjdr8m1x9l2xbj9vk")))

(define-public crate-humphrey-0.2.2 (c (n "humphrey") (v "0.2.2") (h "0qc8w5ka7q4ypglfl9cwqb3rnd0z6vglm6sv2zyp6mb5lsjysjp7")))

(define-public crate-humphrey-0.2.3 (c (n "humphrey") (v "0.2.3") (h "1khxrxvsml92zpr6aaik4r1br1s584cfirqbg6m6j3aqhjrjziq4")))

(define-public crate-humphrey-0.3.0 (c (n "humphrey") (v "0.3.0") (h "0izi5sl28v97sffg32k0gnqkkjx8l6dfhwz75frfmsyb6f0ih7q1")))

(define-public crate-humphrey-0.4.0 (c (n "humphrey") (v "0.4.0") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0yl22xdpc4l0ah5v2nw82p0yy1q4ivfnd8xcls4lyq1snymp8gfl") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.4.1 (c (n "humphrey") (v "0.4.1") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0ir4glgib3dppgfzzywi0lh5mpcmry2bj9vl9yb6i70fd1na68x1") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.4.2 (c (n "humphrey") (v "0.4.2") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "009ngca1pnc1w7352iy2hw20c24pfvv2hvsk2a2viabr6ikrjfz9") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.4.3 (c (n "humphrey") (v "0.4.3") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0h8djbf3a055xznxkfi12h0hsns18ffzh2j1gxifyd969pflqy5v") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.4.4 (c (n "humphrey") (v "0.4.4") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1np18wsikn7236cw6zpv4mcayfx89l1xw438sa295isbwbvhjhji") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.5.0 (c (n "humphrey") (v "0.5.0") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1lq5m2lgazqpzwl752l7zfivjq417hx85ilj3jyi479f7w16c99g") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.5.1 (c (n "humphrey") (v "0.5.1") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0k1gxk4plwhlnhgmlw27r91m6aav7ql8ryyj28zvr0nm3y72ys7z") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.5.2 (c (n "humphrey") (v "0.5.2") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1j77ml0dr1p6n24lb9wgdq35bxpzvxmnc28xs8cflxx9j98i0574") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.5.3 (c (n "humphrey") (v "0.5.3") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "15wllc2nqfwabrbcbzmicm8dx8xxx77qy7d8csp7wkyac2lx72w2") (f (quote (("tls" "rustls" "rustls-pemfile"))))))

(define-public crate-humphrey-0.5.4 (c (n "humphrey") (v "0.5.4") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "009xsbpn78avf8xra8blflh0wknwimz0nc2lqifql589zn1fj5yb") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile"))))))

(define-public crate-humphrey-0.5.5 (c (n "humphrey") (v "0.5.5") (d (list (d (n "rustls") (r "^0.20.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "05g419fxmq61fcab21kkl81av33wvg0ahwdkw8ax6gjqbndy8jap") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile"))))))

(define-public crate-humphrey-0.6.0 (c (n "humphrey") (v "0.6.0") (d (list (d (n "rustls") (r "^0.20.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "11mvn0mcrilk7zxvp6rhy57wrz0zj3j6d8k0ir56aq50i9lrmsqs") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile"))))))

(define-public crate-humphrey-0.6.1 (c (n "humphrey") (v "0.6.1") (d (list (d (n "rustls") (r "^0.20.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1hfxwhiywsv1hiz2dza52piwrv0273dq5i36m2gkpjakqs2v52cp") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile"))))))

(define-public crate-humphrey-0.6.2 (c (n "humphrey") (v "0.6.2") (d (list (d (n "rustls") (r "^0.20.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0kchr9xcyyaq4199ibc6x6mx293xyhac5vx1zmwbgybblxwpz3sy") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile")))) (y #t)))

(define-public crate-humphrey-0.6.3 (c (n "humphrey") (v "0.6.3") (d (list (d (n "rustls") (r "^0.20.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1x7ivzvw623z026mr07z0kmckcjj3xpdw8pad250ry5bn6h7vy97") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile"))))))

(define-public crate-humphrey-0.6.4 (c (n "humphrey") (v "0.6.4") (d (list (d (n "rustls") (r "^0.20.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1nd295vh32x4l479pc2qgrzpi9yyyxpv51rj2w860lxk2i1msb7w") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile"))))))

(define-public crate-humphrey-0.6.5 (c (n "humphrey") (v "0.6.5") (d (list (d (n "rustls") (r "^0.20.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1z6s7sb24wb4iqf4wmzx8jb40py2rr9psiqidb5gns2csnqxq7k4") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile"))))))

(define-public crate-humphrey-0.7.0 (c (n "humphrey") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.21.0") (f (quote ("tls12"))) (o #t) (k 0)) (d (n "rustls-native-certs") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.24.1") (o #t) (d #t) (k 0)))) (h "06sv00cdspp9gy0gjs08gwbv97fw815ixa8r2zvx5zp26ykx44h8") (f (quote (("tls" "rustls" "rustls-native-certs" "rustls-pemfile")))) (s 2) (e (quote (("tokio" "dep:tokio" "futures" "tokio-rustls"))))))

