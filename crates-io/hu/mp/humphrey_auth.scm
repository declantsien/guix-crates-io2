(define-module (crates-io hu mp humphrey_auth) #:use-module (crates-io))

(define-public crate-humphrey_auth-0.1.0 (c (n "humphrey_auth") (v "0.1.0") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "humphrey") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rg3sdz104s4b393nllj5m8dijx342i4zsrahs6qb4qmkp97344s")))

(define-public crate-humphrey_auth-0.1.1 (c (n "humphrey_auth") (v "0.1.1") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "humphrey") (r "^0.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l796lvb6gq80s8rnj9pg3h6ww477zzfby62bqbdx2phzzjnncic")))

(define-public crate-humphrey_auth-0.1.2 (c (n "humphrey_auth") (v "0.1.2") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "humphrey") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mvkwdd85phlbjrm49si5pnx6y6hm0nh0pz5vxn7ppz138dl68dl")))

(define-public crate-humphrey_auth-0.1.3 (c (n "humphrey_auth") (v "0.1.3") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "humphrey") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g21q9fsy5c5w06jmhnqxi231ikrrqh2ccaciaqk96mh4mhamzbk")))

(define-public crate-humphrey_auth-0.1.4 (c (n "humphrey_auth") (v "0.1.4") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "humphrey") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "humphrey_json") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dmxfd6lfmwp6199ghksprs21kvziw3zp6dgsrj10i190975iz7a") (f (quote (("json" "humphrey_json") ("default" "humphrey")))) (s 2) (e (quote (("humphrey" "dep:humphrey"))))))

(define-public crate-humphrey_auth-0.1.5 (c (n "humphrey_auth") (v "0.1.5") (d (list (d (n "argon2") (r "^0.3") (d #t) (k 0)) (d (n "humphrey") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "humphrey_json") (r ">=0.1.1") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0d4xbbn4vxxwj59f4n405bjvh2yxqi3w67c54565074yn8kxl9y9") (f (quote (("json" "humphrey_json") ("default" "humphrey")))) (s 2) (e (quote (("humphrey" "dep:humphrey"))))))

