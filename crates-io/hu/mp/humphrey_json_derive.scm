(define-module (crates-io hu mp humphrey_json_derive) #:use-module (crates-io))

(define-public crate-humphrey_json_derive-0.1.0 (c (n "humphrey_json_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hrv34l95k7g9rj5p6z6cyp3k6agpnhjp0rk678lxrbx73dkjgs1")))

(define-public crate-humphrey_json_derive-0.1.1 (c (n "humphrey_json_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hk86g5h21kyiz39qyxw91866ic87k2899wxxqh4l5wpy7883100")))

