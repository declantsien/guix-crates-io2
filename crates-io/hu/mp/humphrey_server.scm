(define-module (crates-io hu mp humphrey_server) #:use-module (crates-io))

(define-public crate-humphrey_server-0.1.0 (c (n "humphrey_server") (v "0.1.0") (d (list (d (n "humphrey") (r "^0.1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1r730lqm9i5y6dzpclfg2aym8qk26wsmj0wj37f442s3sq7lqdwq") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.1.1 (c (n "humphrey_server") (v "0.1.1") (d (list (d (n "humphrey") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0d555zjnfj969jkd61jkn315n4zm3agk4nbnvklhkqwxyp05bghg") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.2.0 (c (n "humphrey_server") (v "0.2.0") (d (list (d (n "humphrey") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1j0nla6m5qbmrv21vl176alljh711g0d74vyqiy9dli5sy00gc8s") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.2.1 (c (n "humphrey_server") (v "0.2.1") (d (list (d (n "humphrey") (r "^0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "003i6zia93gwvmlg5f39w13dqj6vfkxka4jx489k7adfr0w5k72c") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.2.2 (c (n "humphrey_server") (v "0.2.2") (d (list (d (n "humphrey") (r "^0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1z2jigjmqgxbc6ccan555cn2860s4clmfdvgaxmd9drrhgh09537") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.2.3 (c (n "humphrey_server") (v "0.2.3") (d (list (d (n "humphrey") (r "^0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0gca8a1j0k3gwajra6cx613j7rd1vllr3afzmbfaqsk4q6wabxsa") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.2.4 (c (n "humphrey_server") (v "0.2.4") (d (list (d (n "humphrey") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0y8i77dhzgb60daajl2rnchkf9yzm5xmwv8ml4bapj2rs75xbpf6") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.2.5 (c (n "humphrey_server") (v "0.2.5") (d (list (d (n "humphrey") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0apf9alvnnzf75x166dxd2483zfd6vhly5d8g2bg4z1pdayfr0aq") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.2.6 (c (n "humphrey_server") (v "0.2.6") (d (list (d (n "humphrey") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0gnmg677n8174b33hbjv9fxr0m7mybn500xqcyfsa9drszdlc1sh") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.3.0 (c (n "humphrey_server") (v "0.3.0") (d (list (d (n "humphrey") (r "^0.3") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1wiq67db712clk1j374sqzvzpc2685f63rw8ps65yd7d9140z1gv") (f (quote (("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.4.0 (c (n "humphrey_server") (v "0.4.0") (d (list (d (n "humphrey") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1anxrpwknrf67m3fiq9rmwyzpi33wl9slz0ylv1lzy9z0x78iab1") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.4.1 (c (n "humphrey_server") (v "0.4.1") (d (list (d (n "humphrey") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0jgi94c6cq375ij8md9hbfpj94i7dajqclw00nlkzg3ly7fmgs1k") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.4.2 (c (n "humphrey_server") (v "0.4.2") (d (list (d (n "humphrey") (r "^0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "15df41yr9q3ijwkfmvh0qgqflikn6vlh7118sq8m6mc448gnwv1v") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.4.3 (c (n "humphrey_server") (v "0.4.3") (d (list (d (n "humphrey") (r "^0.4.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1ibpsrg40lpr9q0vmxl0y84rppg10nnbfqmwkcc6nhb5yvmzdr8y") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.5.0 (c (n "humphrey_server") (v "0.5.0") (d (list (d (n "humphrey") (r "^0.5") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1p9ms3i3w49h6kqx0fjghqnpckaj489rx88sg3w80nwlrrfsdhrm") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.5.1 (c (n "humphrey_server") (v "0.5.1") (d (list (d (n "humphrey") (r "^0.5.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0ikgm9xqskbk4z7z4fsf7599mnd61af07ip2qvw6y6m2w6bjrg9b") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.6.0 (c (n "humphrey_server") (v "0.6.0") (d (list (d (n "humphrey") (r "^0.6.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1mwmqx8s07aj8bb1i0rypxmsgrwhkm0s2hj6rcrr9dlfc1xms83a") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

(define-public crate-humphrey_server-0.6.1 (c (n "humphrey_server") (v "0.6.1") (d (list (d (n "humphrey") (r "^0.7.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0hydrygjybs54k392sfgbcp8p88ps4761bdv2vbx0w9dnrkgz023") (f (quote (("tls" "humphrey/tls") ("plugins" "libloading"))))))

