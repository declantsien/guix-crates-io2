(define-module (crates-io hu mp humphrey_json) #:use-module (crates-io))

(define-public crate-humphrey_json-0.1.0 (c (n "humphrey_json") (v "0.1.0") (h "130r4fn0k4p2bvrcic9v933zh8i2hgzady6w79zjdxp1cgip64ij")))

(define-public crate-humphrey_json-0.1.1 (c (n "humphrey_json") (v "0.1.1") (h "0f7g9hgwr34hkfsr1249xc3bb9vd79a7agz6pms6wjj2k10mh1qk")))

(define-public crate-humphrey_json-0.2.0 (c (n "humphrey_json") (v "0.2.0") (d (list (d (n "humphrey_json_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "064x0jqmdmk70ykizqg7zc90fkg388q57w7lhb85nljmdxl1150h") (f (quote (("derive" "humphrey_json_derive") ("default" "derive"))))))

(define-public crate-humphrey_json-0.2.1 (c (n "humphrey_json") (v "0.2.1") (d (list (d (n "humphrey_json_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1zy64ffpy9xg9ih5nzmhnskbil7fd4pvy65xdgp1wrysg540lh8x") (f (quote (("derive" "humphrey_json_derive") ("default" "derive"))))))

(define-public crate-humphrey_json-0.2.2 (c (n "humphrey_json") (v "0.2.2") (d (list (d (n "humphrey_json_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "02hjs1x0gk8bx1cy3wdaf73kyxmzl2kx2hm8zak65011471hk9n6") (f (quote (("derive" "humphrey_json_derive") ("default" "derive"))))))

