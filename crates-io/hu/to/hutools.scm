(define-module (crates-io hu to hutools) #:use-module (crates-io))

(define-public crate-hutools-0.0.1 (c (n "hutools") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)))) (h "17arq92ajsmhlaxfpn0d7fvbw0yqjjiynm9xk65xncg6xcn2g85n")))

(define-public crate-hutools-0.0.2 (c (n "hutools") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chronoutil") (r "^0.2.3") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1dli988d5ifdwqnq19kszrcza2svwabf4vds1hzcfk09hrgy75zb")))

