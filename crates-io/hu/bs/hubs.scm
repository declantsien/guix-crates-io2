(define-module (crates-io hu bs hubs) #:use-module (crates-io))

(define-public crate-hubs-0.1.0 (c (n "hubs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1d82cax3ly38a1k59pq5i776w5yj7a1s6hybdvdiqdhcprvqg9cf")))

(define-public crate-hubs-0.1.1 (c (n "hubs") (v "0.1.1") (h "1m0ph7snfmrbldcf2q58nbrz0vf96ym3b6pcy8dc8vi2fncbfwwf")))

