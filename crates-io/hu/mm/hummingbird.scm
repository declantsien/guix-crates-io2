(define-module (crates-io hu mm hummingbird) #:use-module (crates-io))

(define-public crate-hummingbird-0.1.0 (c (n "hummingbird") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "13fbbh4wk025hy3m27izr5jx2ph8i8cf8rn3akdcnvz6h69frxab")))

(define-public crate-hummingbird-0.1.1 (c (n "hummingbird") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "079ybgvcg685ml3fj67cwb5r18nfncllbhn8zsx8l57wkwaa6ljf")))

(define-public crate-hummingbird-0.1.2 (c (n "hummingbird") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0ckah7asg67wannskpqlca7n27fywhmx5wqvfn2fn5bbcpl43nnl")))

(define-public crate-hummingbird-0.1.3 (c (n "hummingbird") (v "0.1.3") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0i3qcps6pbvhlj9f5zv3jbbcix8fzwrxmxkizaky5xl22v00qgym")))

