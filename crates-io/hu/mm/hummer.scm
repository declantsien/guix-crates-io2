(define-module (crates-io hu mm hummer) #:use-module (crates-io))

(define-public crate-hummer-0.1.0 (c (n "hummer") (v "0.1.0") (d (list (d (n "hex-simd") (r "^0.5") (d #t) (k 0)))) (h "0yjj0yskl7838n77pf6w9jpb6l8l232qyzhm8dw2nlhpqshfbif4")))

(define-public crate-hummer-0.2.0 (c (n "hummer") (v "0.2.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)))) (h "1v3n4yxlhr8izvnxmcq06pzgpw5p9zqy5s0c17i41hxf0dds8g1p")))

