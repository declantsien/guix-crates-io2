(define-module (crates-io hu nc huncomma) #:use-module (crates-io))

(define-public crate-huncomma-0.1.0 (c (n "huncomma") (v "0.1.0") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "021nswflvkanm4yc1aq12lgzkjd3czhzaj7ciza0pi9izvs9gi9y")))

(define-public crate-huncomma-0.2.0 (c (n "huncomma") (v "0.2.0") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "0ng8sbkif51lx85njahkd62hw8ib0iqv4ywhnss3zhlcaxl3rrib")))

(define-public crate-huncomma-0.2.1 (c (n "huncomma") (v "0.2.1") (d (list (d (n "logos") (r "^0.11.4") (d #t) (k 0)))) (h "1nrmi0w0pwy1csqh5n3qwypw4diwnfy9dmzj4sqkfs4gw1zz3xkb")))

