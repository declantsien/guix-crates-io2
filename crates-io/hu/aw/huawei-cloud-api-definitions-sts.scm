(define-module (crates-io hu aw huawei-cloud-api-definitions-sts) #:use-module (crates-io))

(define-public crate-huawei-cloud-api-definitions-STS-0.1.20240419 (c (n "huawei-cloud-api-definitions-STS") (v "0.1.20240419") (d (list (d (n "chrono") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regress") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typify") (r "^0") (d #t) (k 0)))) (h "03nggh9jilqwh5lmrjfw7z7x9jlch6cr9bp0r2f3a7w90bqywcvx") (f (quote (("full" "DecodeAuthorizationMessage" "GetCallerIdentity") ("GetCallerIdentity") ("DecodeAuthorizationMessage"))))))

