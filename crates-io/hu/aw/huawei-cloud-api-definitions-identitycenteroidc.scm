(define-module (crates-io hu aw huawei-cloud-api-definitions-identitycenteroidc) #:use-module (crates-io))

(define-public crate-huawei-cloud-api-definitions-IdentityCenterOIDC-0.1.20240402 (c (n "huawei-cloud-api-definitions-IdentityCenterOIDC") (v "0.1.20240402") (d (list (d (n "chrono") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regress") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "typify") (r "^0") (d #t) (k 0)))) (h "1mzkjs9fkcsw533hyvny1205xfcp4bkbgcmmniyjlbc188blkb92") (f (quote (("full" "CreateToken" "RegisterClient" "StartDeviceAuthorization") ("StartDeviceAuthorization") ("RegisterClient") ("CreateToken"))))))

