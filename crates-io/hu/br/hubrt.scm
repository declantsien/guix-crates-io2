(define-module (crates-io hu br hubrt) #:use-module (crates-io))

(define-public crate-hubrt-0.1.0 (c (n "hubrt") (v "0.1.0") (d (list (d (n "async-channel") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "time"))) (o #t) (d #t) (k 0)))) (h "16gyc6s3dv088dgaydgnhblbg36gyrpqhm8h4ap398s8l1gy082j") (f (quote (("sync" "crossbeam") ("default" "sync") ("async" "tokio" "async-channel")))) (y #t)))

(define-public crate-hubrt-0.1.1 (c (n "hubrt") (v "0.1.1") (d (list (d (n "async-channel") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "time"))) (o #t) (d #t) (k 0)))) (h "1wd7dgibilagpm72fngbgk4vr7ar4s3rxnwq09p8wj4nnrzs8a4m") (f (quote (("sync" "crossbeam") ("default" "sync") ("async" "tokio" "async-channel")))) (y #t)))

(define-public crate-hubrt-0.1.2 (c (n "hubrt") (v "0.1.2") (d (list (d (n "async-channel") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "time"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1h7i373f78d2p4znq27nh6ldc5hjgi8anamaqba0c3qw3bx08i7r") (f (quote (("sync" "crossbeam") ("default" "sync") ("async" "tokio" "async-channel")))) (y #t)))

