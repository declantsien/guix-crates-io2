(define-module (crates-io hu n- hun-offsetof) #:use-module (crates-io))

(define-public crate-hun-offsetof-0.1.0 (c (n "hun-offsetof") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "034yncpn4b04ff48arq4k77wmf0fxs228vpralryli3480ymlrhw")))

(define-public crate-hun-offsetof-0.1.1 (c (n "hun-offsetof") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pyp34dsd12ndjngjfmzhi0scy2jwwi0c68yasqpjslcz9x8ac5a")))

