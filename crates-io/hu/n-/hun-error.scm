(define-module (crates-io hu n- hun-error) #:use-module (crates-io))

(define-public crate-hun-error-0.1.0 (c (n "hun-error") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0x077qwzzyk1xp9qfyl3kmbn5q2a61qzhd1lbsbypisqqwnfpx1w")))

(define-public crate-hun-error-0.1.1 (c (n "hun-error") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0lqkgs4w0356ibd4wi1ds4ciirn72636ikrc1l6qijzrfmaa6yq9")))

