(define-module (crates-io hu ll hull) #:use-module (crates-io))

(define-public crate-hull-0.1.0 (c (n "hull") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.5") (d #t) (k 0)) (d (n "duct") (r "^0.10") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "1z8f2ac4pwqys9kgd7zpwf86wmws9qp14y9shzw43ph2qjl9y2m7")))

(define-public crate-hull-0.1.1 (c (n "hull") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.5") (d #t) (k 0)) (d (n "duct") (r "^0.10") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "03g634iyqrs9g87bss7knb2l8znmziq17q12g8jf2jkgbainzz2d")))

(define-public crate-hull-0.1.2 (c (n "hull") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.5") (d #t) (k 0)) (d (n "duct") (r "^0.10") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "0qhczsk00hzri6i3x4zrpd9a6bwvyapwpxywlr80f9m4jkg645iq")))

(define-public crate-hull-0.1.3 (c (n "hull") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "duct") (r "^0.10") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^1.0") (d #t) (k 0)))) (h "02kg2wm0y9lhcf7j5qb0yixacfrc772wy5ai82zay178kc93dyxl")))

