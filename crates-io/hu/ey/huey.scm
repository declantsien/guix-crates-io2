(define-module (crates-io hu ey huey) #:use-module (crates-io))

(define-public crate-huey-0.1.0 (c (n "huey") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0qr5yqpajjrwi3xzpk4x10bpkf6jq7zyg9krmqz212112mi3nfky")))

(define-public crate-huey-0.1.1 (c (n "huey") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1s6y8aiskghd2yrgfkaxh94hsn5017xl24rhpx954cnf848f9q24")))

(define-public crate-huey-0.2.0 (c (n "huey") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "19ybji65b02ammjllylnm9a4rmqkzxmfv12y8mj7clz1s2pq3wgc")))

(define-public crate-huey-0.2.1 (c (n "huey") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1iva3858jnsrvc92zq1qyjfarldxnnwfv357q8p9ba9a2ldlwpdm")))

