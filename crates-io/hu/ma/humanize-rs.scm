(define-module (crates-io hu ma humanize-rs) #:use-module (crates-io))

(define-public crate-humanize-rs-0.1.0 (c (n "humanize-rs") (v "0.1.0") (h "06vssjb6nvd82xlraanmgnxw3rh3scrdw0n203pk1szidiz8b974")))

(define-public crate-humanize-rs-0.1.1 (c (n "humanize-rs") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hsswz62pncx4c02034h7prbdpqy4pym9v4f6pg1j3s9qgbqd51v")))

(define-public crate-humanize-rs-0.1.2 (c (n "humanize-rs") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0di851s7jagcil5vi9n0a9iagvf85h9prfdablfmahsm6lymhhpq")))

(define-public crate-humanize-rs-0.1.3 (c (n "humanize-rs") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09g232awab5yvvzydq616sgap7lrr03x9dwlmm0affhx8c89615q")))

(define-public crate-humanize-rs-0.1.4 (c (n "humanize-rs") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14lcsf62gkpx2zx0gz7z3h4ba9fkiva7w18am83mcsd3fhl7f1ag")))

(define-public crate-humanize-rs-0.1.5 (c (n "humanize-rs") (v "0.1.5") (h "19p85mr3mvbmpvwkfkhrc7gj4375cnr0lvvaspc1bi5hp3g04sq1")))

