(define-module (crates-io hu ma humantime-serde) #:use-module (crates-io))

(define-public crate-humantime-serde-0.1.0 (c (n "humantime-serde") (v "0.1.0") (d (list (d (n "humantime") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0k4qxj3dbrljm3cwhjz1i54z245n7fpnqxfkir3096qd9bccs1n5")))

(define-public crate-humantime-serde-0.1.1 (c (n "humantime-serde") (v "0.1.1") (d (list (d (n "humantime") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0rahz4k5jfsrly0591sy4f5dhmq1bz5rdrglsggck2y10nlfhncg")))

(define-public crate-humantime-serde-1.0.0 (c (n "humantime-serde") (v "1.0.0") (d (list (d (n "humantime") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0nj6j5yszxksf3dllz6nyrcc22dkk10qyv1ax01pl6y8w98p7igi")))

(define-public crate-humantime-serde-1.0.1 (c (n "humantime-serde") (v "1.0.1") (d (list (d (n "humantime") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0n208zzy69f7pgwcm1d0id4nzhssxn3z3zy7ki3dpkaazmnaad5c")))

(define-public crate-humantime-serde-1.1.0 (c (n "humantime-serde") (v "1.1.0") (d (list (d (n "humantime") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "11dvlxn5hnskc15zyz9jvqi47caqj3v3120fq2qminpdlr755pr1")))

(define-public crate-humantime-serde-1.1.1 (c (n "humantime-serde") (v "1.1.1") (d (list (d (n "humantime") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0310ri4zb33qbwqv0n51xysfjpnwc6rgxscl5i09jgcjlmgdp8sp")))

