(define-module (crates-io hu ma human-time) #:use-module (crates-io))

(define-public crate-human-time-0.1.0 (c (n "human-time") (v "0.1.0") (d (list (d (n "human-time-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1caqq7x77fc0w8cnmsdfsmh0rd9mfj4698sc0bs7h8kfc3l6bgby")))

(define-public crate-human-time-0.1.1 (c (n "human-time") (v "0.1.1") (d (list (d (n "human-time-macros") (r "^0.1.0") (d #t) (k 0)))) (h "18hwn56qsch2xdmix2q4yv84z7mgkww72n8rdjjyfr4zd79c6l2r")))

(define-public crate-human-time-0.1.2 (c (n "human-time") (v "0.1.2") (d (list (d (n "human-time-macros") (r "^0") (d #t) (k 0)))) (h "00nmqxaf4hrk4ah81llfkivxpqbj5jfka50n2km6pdlc4b7yk51d")))

(define-public crate-human-time-0.1.3 (c (n "human-time") (v "0.1.3") (d (list (d (n "human-time-macros") (r "^0.1.6") (d #t) (k 0)))) (h "0vx8s66rasiwfgj0b2l8cd6lmn7q11y5a9my4gyrg0nwdaa3f1if")))

(define-public crate-human-time-0.1.4 (c (n "human-time") (v "0.1.4") (d (list (d (n "human-time-macros") (r "^0.1.6") (d #t) (k 0)))) (h "0hrh2sfjxwidr5paipmcyw4rf2czk2iy651fgn8fhlv2g4q9nv50")))

(define-public crate-human-time-0.1.5 (c (n "human-time") (v "0.1.5") (d (list (d (n "human-time-macros") (r "^0.1.7") (d #t) (k 0)))) (h "1p1b08q8vp7b66ssc6kb9g1jhd0kcczr0la6q61f0k9v9hk5vw0a")))

(define-public crate-human-time-0.1.6 (c (n "human-time") (v "0.1.6") (d (list (d (n "human-time-macros") (r "^0.1.7") (d #t) (k 0)))) (h "0g3zvqhnxpg1fsmz7cp8w9lhgwn4iblq938hygmxbl3vabm25615")))

