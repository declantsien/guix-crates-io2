(define-module (crates-io hu ma human-fanout) #:use-module (crates-io))

(define-public crate-human-fanout-0.1.0 (c (n "human-fanout") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "spl-math") (r "^0.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.5.0") (d #t) (k 0)))) (h "0jbmc1qqv344crwh6n5mc5wk9ccy3zjjm0k8463r1yxk7zfmkxh3") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

