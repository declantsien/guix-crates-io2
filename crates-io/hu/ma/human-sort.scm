(define-module (crates-io hu ma human-sort) #:use-module (crates-io))

(define-public crate-human-sort-0.1.0 (c (n "human-sort") (v "0.1.0") (h "1w4s8bm91j5nn1qicv11xs90xf4xs9i7jab89sf0gl2cvzf237dq")))

(define-public crate-human-sort-0.1.1 (c (n "human-sort") (v "0.1.1") (h "0z3sz4jmdlkyj53mg40sw403sxqh3sm23a6hhns30acnxv6jg42a")))

(define-public crate-human-sort-0.1.2 (c (n "human-sort") (v "0.1.2") (h "1j11cvd3kfffm8xdghaawkdwq1qd4z748lssik2hrl04x783js0r")))

(define-public crate-human-sort-0.1.3 (c (n "human-sort") (v "0.1.3") (h "0vyvpl1bn37crm12xsb4pyar0igyanflymbss34dvg5gribpz7mr")))

(define-public crate-human-sort-0.1.4 (c (n "human-sort") (v "0.1.4") (h "1irha5f198h9f7axpfgjprw11wbk5vdd8mbni36m9w1a5ny5nby0")))

(define-public crate-human-sort-0.1.5 (c (n "human-sort") (v "0.1.5") (h "048hg44rk5g8niciqky73wclh642rif0gmn9ianrxdcga9z03qfk")))

(define-public crate-human-sort-0.1.6 (c (n "human-sort") (v "0.1.6") (h "175kmna4hy03a3k8fvq85y6k986hr6hg8ll6icq9pi7n89fhnwkr")))

(define-public crate-human-sort-0.1.9 (c (n "human-sort") (v "0.1.9") (h "0i9rdliw4z3g7gh8hq2pj1y55ik44j1f5jsqy1gz44gckwd8ibqk")))

(define-public crate-human-sort-0.1.10 (c (n "human-sort") (v "0.1.10") (h "03kc735zpgv700y4fkvjznnh8hndmfiw3mdf298js3rphvh1dd5n")))

(define-public crate-human-sort-0.1.11 (c (n "human-sort") (v "0.1.11") (h "1ccniiv263p0b4dpk2sd64rvslk9m73b85qfm1a9dnvjaah7779y")))

(define-public crate-human-sort-0.1.12 (c (n "human-sort") (v "0.1.12") (h "13zm0283771r5nhzbd99hsgc6lmr83yab4ilswn5z86sf660yapl")))

(define-public crate-human-sort-0.1.13 (c (n "human-sort") (v "0.1.13") (h "117pmm42qykm6zijvaybsyi2id47pf2fl99f7alp8zdh1m2yvxhs")))

(define-public crate-human-sort-0.1.14 (c (n "human-sort") (v "0.1.14") (h "0qsspp1drm7a2lf86g350539ams2vq6pdpw8mjwkkcbjqb4l6cmx")))

(define-public crate-human-sort-0.2.0 (c (n "human-sort") (v "0.2.0") (h "1rvl4fn7vlq5p9cawd6a0yff0h8kj9lspxdiayr0jmi68s2mgqas")))

(define-public crate-human-sort-0.2.1 (c (n "human-sort") (v "0.2.1") (h "0lbv14iwwwsfyh3wmzwx60lqw17r79611gj6fn9h00q5rsa6a4gm")))

(define-public crate-human-sort-0.2.2 (c (n "human-sort") (v "0.2.2") (h "06b2rj4hd4h4rf3mp5qk89ymlxh5iv3cpmrfgramwvay634hj2hl")))

