(define-module (crates-io hu ma humanbool) #:use-module (crates-io))

(define-public crate-humanbool-1.0.0 (c (n "humanbool") (v "1.0.0") (h "1l3x3vdmzzvn51cywh018d4i4g08hg402rkh2ziq8v4f8iyz2xqv")))

(define-public crate-humanbool-1.3.0 (c (n "humanbool") (v "1.3.0") (h "1fns4hd232hq1mpkvz4f7ck2c856wr5kz4hk057rf6srs7327pdg")))

