(define-module (crates-io hu ma human_bytes) #:use-module (crates-io))

(define-public crate-human_bytes-0.1.0 (c (n "human_bytes") (v "0.1.0") (h "0nn0yq0f0qab34l7fhw50apzczbqlyan2yvi3l6rrkai4ygslz53")))

(define-public crate-human_bytes-0.2.0 (c (n "human_bytes") (v "0.2.0") (d (list (d (n "lexical") (r "^5.2") (o #t) (d #t) (k 0)))) (h "01q34xjrmi27s05vr8dbgrrsqfykhwsmqix0nzpxcr056h41b8xx") (f (quote (("fast" "lexical") ("default"))))))

(define-public crate-human_bytes-0.2.1 (c (n "human_bytes") (v "0.2.1") (d (list (d (n "lexical") (r "^5.2") (o #t) (d #t) (k 0)))) (h "0iwb71k2ipqhxg4m547f4qgf63hkfvpzd70yp9z1y7dnxxi1jacy") (f (quote (("fast" "lexical") ("default"))))))

(define-public crate-human_bytes-0.3.0 (c (n "human_bytes") (v "0.3.0") (d (list (d (n "lexical") (r "^5.2") (o #t) (d #t) (k 0)))) (h "1qhdvdsy16c4pfbnq45yjqmc61wny8bvzysiki6zy9x2nibqa5yw") (f (quote (("fast" "lexical") ("default") ("bibytes"))))))

(define-public crate-human_bytes-0.3.1 (c (n "human_bytes") (v "0.3.1") (d (list (d (n "lexical") (r "^6.0") (o #t) (d #t) (k 0)))) (h "05w8yb616p26fnjb9iqdlgb2nzx6dlcsl1n3q522x57c77fd9848") (f (quote (("fast" "lexical") ("default") ("bibytes"))))))

(define-public crate-human_bytes-0.4.0 (c (n "human_bytes") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0xkl4g6f7m1zcgyln8z3m782qv50gdqk4zx34c2xjhj5hc7sak3q") (f (quote (("si-units") ("fast" "ryu") ("default" "si-units") ("build-binary" "anyhow" "atty" "lexopt"))))))

(define-public crate-human_bytes-0.4.1 (c (n "human_bytes") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lexopt") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1sv4ki5ndm6hzhprzmwivqp6z2lm626f0rdnm0yqp3l3dhcjid9r") (f (quote (("si-units") ("fast" "ryu") ("default" "si-units") ("build-binary" "anyhow" "atty" "lexopt"))))))

(define-public crate-human_bytes-0.4.2 (c (n "human_bytes") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "atty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lexopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1rn522p855dccrprpabcn8f1rssfx67hm34dnjbmblcaya4v1qi7") (f (quote (("si-units") ("fast" "ryu") ("default" "si-units") ("build-binary" "anyhow" "atty" "lexopt"))))))

(define-public crate-human_bytes-0.4.3 (c (n "human_bytes") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lexopt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ryu") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13ilqakgjjfk5rjn7c329xwi9z0i10k8ichcckvsn92hafj5bwli") (f (quote (("si-units") ("fast" "ryu") ("default" "si-units") ("build-binary" "anyhow" "lexopt"))))))

