(define-module (crates-io hu ma human-escrow) #:use-module (crates-io))

(define-public crate-human-escrow-0.1.0 (c (n "human-escrow") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "human-common") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "solana-program") (r "^1.9.6") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.9.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.9.6") (d #t) (k 2)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.3.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "007c7ip60zm94vxsw9p45cs9yilv85ry398b2c37h3zhxanvbckv") (f (quote (("test-bpf") ("no-entrypoint"))))))

