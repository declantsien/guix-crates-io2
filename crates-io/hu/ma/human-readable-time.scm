(define-module (crates-io hu ma human-readable-time) #:use-module (crates-io))

(define-public crate-human-readable-time-0.0.1 (c (n "human-readable-time") (v "0.0.1") (h "1pwpx7rjkmj2d5pw5jc897vhirfqnsmcacq0sllgqzv8a63fccvp") (f (quote (("std") ("default" "std"))))))

(define-public crate-human-readable-time-0.1.0 (c (n "human-readable-time") (v "0.1.0") (h "1qb2fl3k2f1bwjf8d8jyg87izz7yjm2hz0wv094sg625j16j8zij") (f (quote (("std") ("default" "std"))))))

(define-public crate-human-readable-time-0.1.1 (c (n "human-readable-time") (v "0.1.1") (h "092a21nhn0l948inhff2v61c47sdhziq9ffqk6az0mrab1cn1nl8") (f (quote (("std") ("default" "std"))))))

(define-public crate-human-readable-time-0.1.2 (c (n "human-readable-time") (v "0.1.2") (h "0qk6hzb23x8nna3d9lhyy5jyiwjr6j2vs711dcv1bnck9igyrs9f") (f (quote (("std") ("default" "std"))))))

(define-public crate-human-readable-time-0.2.0 (c (n "human-readable-time") (v "0.2.0") (h "0991zn0f1w61bsckqf7yap53y2qida99zw0qyrrcha7jvgh2cxgn")))

(define-public crate-human-readable-time-0.3.0 (c (n "human-readable-time") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)))) (h "0vwgw81mqh3dbgvsw9nc7vas12yjppqq4qp12rhnr5hrmkd6iwgs")))

(define-public crate-human-readable-time-0.4.0 (c (n "human-readable-time") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std" "derive"))) (k 2)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)))) (h "1c7z5x87bi5rggcb6hxcw2n5kr7xn4nrk4w2c701x4dqcx9jgvzw") (f (quote (("default" "chrono"))))))

