(define-module (crates-io hu ma human_ids) #:use-module (crates-io))

(define-public crate-human_ids-0.1.0 (c (n "human_ids") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)))) (h "08p93mrvlrhxfkk6x3rzixfkjn7rwi28s74y53pn2mjh7bi3bpxh")))

(define-public crate-human_ids-0.1.1 (c (n "human_ids") (v "0.1.1") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)))) (h "1z48105rxq4mlvizckqg3nfnkydw3nc2q0lba49s11narzdm44nr")))

