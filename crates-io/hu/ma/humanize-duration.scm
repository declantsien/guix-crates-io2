(define-module (crates-io hu ma humanize-duration) #:use-module (crates-io))

(define-public crate-humanize-duration-0.0.2 (c (n "humanize-duration") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "0d050cp47zj2jw8s62rm2vw7wxpfxw6rc565s693b28kggzqhssh") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-humanize-duration-0.0.3 (c (n "humanize-duration") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "139gsqzzbfwpjiwp23d31kb86ldbs9qla5c411fppr121jmi3vzf") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-humanize-duration-0.0.4 (c (n "humanize-duration") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "1akbgn93075q2s07p8y1dp27f4v0q8wczj2fw8kxznfxch56sh3s") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-humanize-duration-0.0.5 (c (n "humanize-duration") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "0a4lq1f7gx599vxcm8mpdnyl56rq76p6lnxdqz8wlz9450vi8afv") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-humanize-duration-0.0.6 (c (n "humanize-duration") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "175ffy50lh6df2dilfi2inq33yfvm3wawddjabz6k0shzallf8h8") (f (quote (("default")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

