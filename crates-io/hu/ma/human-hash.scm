(define-module (crates-io hu ma human-hash) #:use-module (crates-io))

(define-public crate-human-hash-0.1.0 (c (n "human-hash") (v "0.1.0") (d (list (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0h88h9jlkj59rp1ri8wwpwnhd93ss0wnwyn7a1pkky69csy7hryl")))

(define-public crate-human-hash-0.1.1 (c (n "human-hash") (v "0.1.1") (d (list (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "07pzgnf49rsz4q82bfjhmibzws9z9wk6yzx58q481gxdlqkfc33b")))

(define-public crate-human-hash-0.2.0 (c (n "human-hash") (v "0.2.0") (d (list (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bfx05zdh062vxvkqbp0n1zl9cc9kv3yafakxl90wvdxrf4jxnyv")))

(define-public crate-human-hash-0.2.1 (c (n "human-hash") (v "0.2.1") (d (list (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0119vnrfabs88vkzcr5d2b2mdjsr8swfj5dn8jvqgznygvbvxr2h")))

(define-public crate-human-hash-0.3.0 (c (n "human-hash") (v "0.3.0") (d (list (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "02khg1ym6275i83dv72jqf0g5c42ciljc031h8av63rwhh9rdmqp")))

(define-public crate-human-hash-0.4.0 (c (n "human-hash") (v "0.4.0") (d (list (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yplpg277asgy5p50zdz68jvc44ml6bvxjp9vb3j3n04y4agqnpf")))

