(define-module (crates-io hu ma human-size) #:use-module (crates-io))

(define-public crate-human-size-0.1.0 (c (n "human-size") (v "0.1.0") (h "0zm7l69amfymlzvklzajhvgxymq7wbzvigqahnxdx0j4n6zbdg1c")))

(define-public crate-human-size-0.1.1 (c (n "human-size") (v "0.1.1") (h "12m5hwb2aln8qvggl6c0nb478s8vik3bi96j7qfdv0ypy48vql6z")))

(define-public crate-human-size-0.2.0 (c (n "human-size") (v "0.2.0") (h "0ahm3g3siahgx4nmqi330hl51d8mw54r9yl32ns6k6v3avsp63n3")))

(define-public crate-human-size-0.3.0 (c (n "human-size") (v "0.3.0") (h "07900f5y1xxh031c621acx1bf7npjvw4r4jcb2zw5q3b2prqv63x")))

(define-public crate-human-size-0.4.0 (c (n "human-size") (v "0.4.0") (h "180l52pr7nc84d1fzpp9yf4p20p6cllpxbclpljpcdpp3s06xv2v")))

(define-public crate-human-size-0.4.1 (c (n "human-size") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.105") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.105") (k 2)))) (h "0s03ay6bmz0c90n25yiwi1hxv050di6zp3za6i8dr7shd8h9s2zr") (f (quote (("enable-serde" "serde") ("default"))))))

(define-public crate-human-size-0.4.2 (c (n "human-size") (v "0.4.2") (d (list (d (n "serde") (r "^1.0.105") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.105") (k 2)))) (h "094p7r835il51rgawylmjrfx33vnjgc4cmhsk1k2s72f9fbg9vk2") (f (quote (("enable-serde" "serde") ("default"))))))

(define-public crate-human-size-0.4.3 (c (n "human-size") (v "0.4.3") (d (list (d (n "serde") (r "^1.0.105") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.105") (k 2)))) (h "1sza13s9vrhgqnci78wn58g8601bpcipibk3dhbb6f8sijgbg54r") (f (quote (("enable-serde" "serde") ("default"))))))

