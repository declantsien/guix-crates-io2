(define-module (crates-io hu ma humantime_to_duration) #:use-module (crates-io))

(define-public crate-humantime_to_duration-0.1.0 (c (n "humantime_to_duration") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1vmkdlr2lr5f3dm3gcr5lbg8d7xwbpsia6f723md87lmbiim4qx9") (y #t)))

(define-public crate-humantime_to_duration-0.1.1 (c (n "humantime_to_duration") (v "0.1.1") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "020xw5qjkzpk6i7k8jw8h7jqfbjf74wxk14iikhx9m12v3mw0dzr") (y #t)))

(define-public crate-humantime_to_duration-0.1.2 (c (n "humantime_to_duration") (v "0.1.2") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1z2llddsip7ka5lvsmndajpshv2ghikh2x2kasfhhs7nw60lhmid") (y #t)))

(define-public crate-humantime_to_duration-0.1.3 (c (n "humantime_to_duration") (v "0.1.3") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1kpvyvahzl7avw6wlqkpg2yl0jmffnhc7dcfjri847a2rlann9s6") (y #t)))

(define-public crate-humantime_to_duration-0.2.0 (c (n "humantime_to_duration") (v "0.2.0") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1y9n05d3dbkvl4wdjds17whvkvpc9hq37dys5zjym2vwl33n9wxw") (y #t)))

(define-public crate-humantime_to_duration-0.2.1 (c (n "humantime_to_duration") (v "0.2.1") (d (list (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1vfb0gjal92jssz21n3syd0rch2qs66pkmsiq7271k11bxj68ivi")))

(define-public crate-humantime_to_duration-0.3.0 (c (n "humantime_to_duration") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "14n71dpaq52whfpxj9ayvmc95a2cc03wl60pcw0svfx2f2wp99p0") (y #t)))

(define-public crate-humantime_to_duration-0.3.1 (c (n "humantime_to_duration") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "alloc" "clock"))) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "1i1p827y4pz18khzd47fd4k6msmckjjg6i91wrscpp3d14rs500s")))

