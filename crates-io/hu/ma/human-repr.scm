(define-module (crates-io hu ma human-repr) #:use-module (crates-io))

(define-public crate-human-repr-0.1.0 (c (n "human-repr") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "111fdr9mlpai7dls4pignxxr05irbligm9zf2irh3iv22wvkqvgf") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.1.1 (c (n "human-repr") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1r44f0kj90v40fr2269r7rfh7bn1sz05rjqlj2sa2yvazw02ygh0") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.1.2 (c (n "human-repr") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0injakz1d4gmx431m0ddx99a0dqlx7jpksr2cw62mjm941jziw2i") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.1.3 (c (n "human-repr") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0hhsc9b0w8img6vc19cgvw05nd5911nk529f17gl0na1inchy9ll") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.1.4 (c (n "human-repr") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1667c43igi3scigzac11cyv3k0x49adzi8n3846mgf3f6c4rpl49") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.1.5 (c (n "human-repr") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0q4slf1b7k7niwpglpnb7hdgmf951hh903w5cza1n78nsdpl24b2") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.2.0 (c (n "human-repr") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vgcgxh6lh64kr0kwnr2154cl25xi3ipz7wp1b6sbrgzvin68vi9") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.3.0 (c (n "human-repr") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "17sv9a60nkzyscnyyx14v680hxa87zdihk92ymagvw7x70r73f78") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.3.1 (c (n "human-repr") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0p27hadv28dkw8dr0c7wpzf9a9dd5ylqxl0fhiiv6s266q040bq6") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.4.0 (c (n "human-repr") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1q4sig2q9bacsdri9g6mf33m0zlk7rpr2628j3hdww44v1qy2jbi") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.5.0 (c (n "human-repr") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1qhc6l5l4h12gd6drj9xs92gkxg0fghgyivl630qfkysbdd466gk") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.6.0 (c (n "human-repr") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0i2m7jdjhaslgp0h2gc06byrnh7sh3h67y453sjs406qrjxmz83k") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.7.0 (c (n "human-repr") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ikh13lx8xlj5m7p77k0lxv14jfvzsb78bxnh4r1x6kx6lxfkrd3") (f (quote (("nospace") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-0.8.0 (c (n "human-repr") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "195nzyr29wa5kal5vlhg3lzrlh0gm9s5v2yrpdbvhjvxbqr8yd02") (f (quote (("space") ("iec" "1024") ("default" "space") ("1024"))))))

(define-public crate-human-repr-0.9.0 (c (n "human-repr") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1x17b7y163f34vqsp5q4y3lnjlv6yl41hv6f45hmvl66xzrlsy5w") (f (quote (("space") ("iec" "1024") ("default" "space") ("1024"))))))

(define-public crate-human-repr-0.10.0 (c (n "human-repr") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19xg44sbvkk7bcfm1433n2qsdl4d4bhqspv6zwpmi1wdc6qzimlx") (f (quote (("space") ("iec" "1024") ("default") ("1024"))))))

(define-public crate-human-repr-0.10.1 (c (n "human-repr") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0smwhx15vs52jzzykjgjz9f219bnxarkwmdk7sicn9x2i1hi1ljg") (f (quote (("space") ("iec" "1024") ("default") ("1024"))))))

(define-public crate-human-repr-0.11.0 (c (n "human-repr") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1nzpr7m152rxhb7jxjpj8h8zaimc3ag9mgb6pc8i9sx7zv1fidai") (f (quote (("space") ("iec" "1024") ("default") ("1024"))))))

(define-public crate-human-repr-1.0.0 (c (n "human-repr") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1lcb4420g9dj064gmkwsa88sx89nmlsfk6nxvsmckk67dfffg9s3") (f (quote (("space") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-1.0.1 (c (n "human-repr") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cdx16rs7l73cn10aq1g1s3na0r60jccp93mljxq9dh866g4iyyh") (f (quote (("space") ("iec" "1024") ("1024"))))))

(define-public crate-human-repr-1.1.0 (c (n "human-repr") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1wfzxwp10chg8244fx8yhh8bd9cp3jazi4rnb6pkqlb1ay57g2zm") (f (quote (("space") ("iec" "1024") ("1024"))))))

