(define-module (crates-io hu ma human) #:use-module (crates-io))

(define-public crate-human-0.1.0 (c (n "human") (v "0.1.0") (d (list (d (n "cala_core") (r "^0.1") (d #t) (k 0)) (d (n "pasts") (r "^0.4") (d #t) (k 0)) (d (n "stick") (r "^0.10") (d #t) (k 0)))) (h "1dhb5m1s36vi56q9pkk488qmk3v199v7xc1g8kixxvg338hmgq36")))

(define-public crate-human-0.2.0 (c (n "human") (v "0.2.0") (d (list (d (n "devout") (r "^0.2") (d #t) (k 2)) (d (n "pasts") (r "^0.7") (d #t) (k 2)) (d (n "stick") (r "^0.11") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "Element" "Node" "InputEvent" "KeyboardEvent" "MouseEvent" "WheelEvent" "HtmlElement" "HtmlInputElement" "HtmlCollection" "AddEventListenerOptions"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1l600nlnrilhbi2lswz60xfln0pgawk5q5azkz2z3v8z87i0cdm3")))

