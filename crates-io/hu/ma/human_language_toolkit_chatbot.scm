(define-module (crates-io hu ma human_language_toolkit_chatbot) #:use-module (crates-io))

(define-public crate-human_language_toolkit_chatbot-0.1.0 (c (n "human_language_toolkit_chatbot") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0hp9rlzr7clnchh6ndvgczhz10fw3ddszvsbdvh0gdw0gqab2rcw")))

(define-public crate-human_language_toolkit_chatbot-0.1.1 (c (n "human_language_toolkit_chatbot") (v "0.1.1") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ah0fp4lwc4l86vcfa74rcrpfdplx9absn4d4lmjs3iv7f8mr4ah")))

