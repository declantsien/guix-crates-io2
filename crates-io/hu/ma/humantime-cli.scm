(define-module (crates-io hu ma humantime-cli) #:use-module (crates-io))

(define-public crate-humantime-cli-0.1.0 (c (n "humantime-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "1k7dfppjrq36da0imzp7m70ka9hl47132dzawc801rhscdln3nci")))

(define-public crate-humantime-cli-0.2.0 (c (n "humantime-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "0pya8q6yqwz1m8y1xha7hwq3ayfh5nh9jc661c3wmq316bf9nh80")))

