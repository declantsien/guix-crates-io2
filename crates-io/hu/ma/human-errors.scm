(define-module (crates-io hu ma human-errors) #:use-module (crates-io))

(define-public crate-human-errors-0.1.0 (c (n "human-errors") (v "0.1.0") (h "1141if4ifzh8vvxwn8277g4xh3wbalc3gs9nbd1rjhjqv33zcq75")))

(define-public crate-human-errors-0.1.1 (c (n "human-errors") (v "0.1.1") (h "02ghcdg2nrmb29fgjx89bck8ny6khmf7h1zizi062kvrp3shy38q")))

(define-public crate-human-errors-0.1.2 (c (n "human-errors") (v "0.1.2") (h "13rj8615krihrvw3s23mly3r0lzca44d323cidjln6xngapdb8n1")))

(define-public crate-human-errors-0.1.3 (c (n "human-errors") (v "0.1.3") (h "1pygabwji03kdaqkkhqjxd2rfc9j3crf2zi9pm73h9l668zxzb24")))

