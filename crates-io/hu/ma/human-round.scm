(define-module (crates-io hu ma human-round) #:use-module (crates-io))

(define-public crate-human-round-0.1.0 (c (n "human-round") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.27.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "anchor-spl") (r "^0.27.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "spl-math") (r "^0.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.5.0") (d #t) (k 0)))) (h "08jfwcm4jz077nlhxa6762xh33w46vwlsskshpail9n32zhvmg9n") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

