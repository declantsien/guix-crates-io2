(define-module (crates-io hu ma human-ids-bin) #:use-module (crates-io))

(define-public crate-human-ids-bin-0.1.1 (c (n "human-ids-bin") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo" "env"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "cargo" "env"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2.15") (d #t) (k 1)) (d (n "human_ids") (r "^0") (d #t) (k 0)))) (h "1zv2s3wkpf62kls1lvy81l2ify5xpfrlz6lsfi6idz4iq2l17vhg")))

