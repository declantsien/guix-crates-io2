(define-module (crates-io hu ma human-common) #:use-module (crates-io))

(define-public crate-human-common-0.1.0 (c (n "human-common") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.6") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.9.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.9.6") (d #t) (k 2)) (d (n "spl-associated-token-account") (r "^1.0.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token") (r "^3.3.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1f9c3srlrfvdv9klqpvnmbv7hdkkymcrqhzv94nbi9s16sx8zf88")))

