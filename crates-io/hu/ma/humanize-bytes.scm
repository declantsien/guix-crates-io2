(define-module (crates-io hu ma humanize-bytes) #:use-module (crates-io))

(define-public crate-humanize-bytes-0.1.0 (c (n "humanize-bytes") (v "0.1.0") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "0kvn0aa1hy44ghsjyyqcpk88fvak652w11x756cardd909qmsf24") (f (quote (("default" "binary" "decimal") ("decimal") ("binary"))))))

(define-public crate-humanize-bytes-1.0.0 (c (n "humanize-bytes") (v "1.0.0") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "0b8jv6zgs0n7yrhirykbqqq5i4qp27nfzh1cwrq1wdvvadb2hqvs") (f (quote (("default" "binary" "decimal") ("decimal") ("binary"))))))

(define-public crate-humanize-bytes-1.0.1 (c (n "humanize-bytes") (v "1.0.1") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "1daw3yrsg9lrwcxrbfl3lp648kjdzssgwb7wz57lq82nynpfq0j5") (f (quote (("default" "binary" "decimal") ("decimal") ("binary"))))))

(define-public crate-humanize-bytes-1.0.2 (c (n "humanize-bytes") (v "1.0.2") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "04vlql1iyfawn9dxvrynyw5v2ymkcslrp8ss6fj48mlj2xvb982y") (f (quote (("default" "binary" "decimal") ("decimal") ("binary"))))))

(define-public crate-humanize-bytes-1.0.3 (c (n "humanize-bytes") (v "1.0.3") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "16w4i54vyvykmwschnnbvh8574vm2y5b3wmczawmqy0bqjbxx9sn")))

(define-public crate-humanize-bytes-1.0.4 (c (n "humanize-bytes") (v "1.0.4") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "1xhr1dvbjf9xnyhbc82pyxrm9d7m1x4lnfk2l36ngaxd1w6b0qk8")))

(define-public crate-humanize-bytes-1.0.5 (c (n "humanize-bytes") (v "1.0.5") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "10865yn7ga35yqhg8shkqybd8k8qv38ic41ibr788kz6z87mm3sw")))

(define-public crate-humanize-bytes-1.0.6 (c (n "humanize-bytes") (v "1.0.6") (d (list (d (n "smartstring") (r "^1.0.1") (k 0)))) (h "16gpipcr402xkpgjsf5igpkalfhx61jp6yawb6xfcqdpa0y0k4ha")))

