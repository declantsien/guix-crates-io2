(define-module (crates-io hu ma humane_commands) #:use-module (crates-io))

(define-public crate-humane_commands-0.1.0 (c (n "humane_commands") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "1ysv0zw381blr7886ra85fkz8p16yaxgmiz0hy4zls3rzkmlzzyp") (f (quote (("output") ("opt_arg") ("ok" "thiserror") ("default" "opt_arg" "output" "ok"))))))

(define-public crate-humane_commands-0.1.1 (c (n "humane_commands") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "1d79fh68g7v7mga1vc3fgdn4q20wsccppzssb0l3hpkpvpiy0hbf") (f (quote (("output") ("opt_arg") ("ok" "thiserror") ("default" "opt_arg" "output" "ok"))))))

(define-public crate-humane_commands-0.1.2 (c (n "humane_commands") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.50") (o #t) (d #t) (k 0)))) (h "02v5swcwsjzwvhbbqz85mcsmicp094w96afk8kr20inwq4ydkx6a") (f (quote (("output") ("opt_arg") ("ok" "thiserror") ("default" "opt_arg" "output" "ok"))))))

