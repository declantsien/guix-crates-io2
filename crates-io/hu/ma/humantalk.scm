(define-module (crates-io hu ma humantalk) #:use-module (crates-io))

(define-public crate-humantalk-0.1.0 (c (n "humantalk") (v "0.1.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)))) (h "19s8l0213yaj8x49mf51x9gk78ayvkmhk90k1prx07nbllkg6llp")))

(define-public crate-humantalk-0.1.1 (c (n "humantalk") (v "0.1.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)))) (h "0811k87207zn41rza31h07knkyyxkswln9fm4gp551bgava03zhm")))

(define-public crate-humantalk-0.1.2 (c (n "humantalk") (v "0.1.2") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "thetime") (r "^0.5.6") (d #t) (k 0)))) (h "1kxli2hfw6hl6xnm21yl2frmvpl6n8z7pyz7671d29dsbkgrc3vr")))

