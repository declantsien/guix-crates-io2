(define-module (crates-io hu ma human_regex) #:use-module (crates-io))

(define-public crate-human_regex-0.1.0 (c (n "human_regex") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1i56zihrnkys44q05z8khyasq1p6jy8qqszpdqmr2g4d1qmsalp7")))

(define-public crate-human_regex-0.1.1 (c (n "human_regex") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1x812pdcn9qwy7qx3pb851l7fzgngd3nc1g0z8xd5yh23hiasam5")))

(define-public crate-human_regex-0.1.2 (c (n "human_regex") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06rvhw7gfi1kqjdp7faia8iml28l9vpapybpyp737bgnkyv9bvf2")))

(define-public crate-human_regex-0.1.3 (c (n "human_regex") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1w4vpc0lza03ix0gdfbs88ghpivlmjch1psnw0xfzgpi0baxhy21")))

(define-public crate-human_regex-0.2.0 (c (n "human_regex") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "009qxl4283js8zhy4qq1bag4w228zmmjy261qb0scs1sdm306c7i")))

(define-public crate-human_regex-0.2.1 (c (n "human_regex") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15mj3nbbfra5m9dslmh9dj9irabjxcmlkn8vw206lpd0kbdvlm1c")))

(define-public crate-human_regex-0.2.2 (c (n "human_regex") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stop-words") (r "^0.6.2") (d #t) (k 2)))) (h "0bs7y64mrz8d0cz6b1ixflnz800cxyy00sh08xbxdpql7vpqc4yh")))

(define-public crate-human_regex-0.2.3 (c (n "human_regex") (v "0.2.3") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "stop-words") (r "^0.6.2") (d #t) (k 2)))) (h "0f4j8fhxnd0asgp72ngqljqi3mrmqgbkf5qpvxcx2c0ygzv6mdrs")))

(define-public crate-human_regex-0.3.0 (c (n "human_regex") (v "0.3.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "stop-words") (r "^0.7.0") (d #t) (k 2)))) (h "0n47wi8skmhdqjpz1m18ljp4lsgm13pwdkyr1qsg0gkfk28mz4xx")))

