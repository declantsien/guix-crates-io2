(define-module (crates-io hu ma humanize-url) #:use-module (crates-io))

(define-public crate-humanize-url-0.0.0 (c (n "humanize-url") (v "0.0.0") (h "181bbqr5lw2s3iy0brd9j37py2zqgkdcs1p31bfa85wwjf5nn9ga")))

(define-public crate-humanize-url-0.1.0 (c (n "humanize-url") (v "0.1.0") (d (list (d (n "unrestrictive-url") (r "^0.1.0") (d #t) (k 0)))) (h "0v89yga6z4qf802185a9w5d83syh64h59ig369d2d1l2lqggna8y")))

