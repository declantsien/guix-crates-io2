(define-module (crates-io hu ma humantime) #:use-module (crates-io))

(define-public crate-humantime-0.1.0 (c (n "humantime") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0mla12v3w9lxw593fxvzc2cx1g2s54p0nxs2bk0lg0gj9lgrslqz")))

(define-public crate-humantime-0.1.1 (c (n "humantime") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "07ip4mdkxlii7ny0xyxp968ziqsgp7m8a6qxgc19yd5xb3468j14")))

(define-public crate-humantime-0.1.2 (c (n "humantime") (v "0.1.2") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1fhw7bqmra9qyllgfnlicbpp3xpl12n6sxk8km87ml6qygvjdns4")))

(define-public crate-humantime-0.1.3 (c (n "humantime") (v "0.1.3") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0kyzfcai5wijyn7rq5qcnv2qd96cgcdqldg8bhyfw2adyy64jab6")))

(define-public crate-humantime-1.0.0 (c (n "humantime") (v "1.0.0") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0q0rvdxpg7wv81p4pmk8mlpslz7p5yf8x0fb3yg5cm1azgzri4hf")))

(define-public crate-humantime-1.1.0-beta.2 (c (n "humantime") (v "1.1.0-beta.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "0va32igy8y0pyg0jcp2yw02ngmsm1b5iawi7gy5jf8bpbfmcmd33")))

(define-public crate-humantime-1.1.0-beta.3 (c (n "humantime") (v "1.1.0-beta.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "0lx0mgxcfvqp2xk93qlng2vxj8az41wjyz7mb01xjl3zfmbima0c")))

(define-public crate-humantime-1.1.0-beta.4 (c (n "humantime") (v "1.1.0-beta.4") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "12b62f0pg7mczpfcciq9cb038whikplab51h9zqk41k4gqj5inak")))

(define-public crate-humantime-1.1.0 (c (n "humantime") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "0hi053c8qdaih6a7b8m8xdjmwzfgdvmcyvax3d14qh730ldf0sak")))

(define-public crate-humantime-1.1.1 (c (n "humantime") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "17lc30pd77s11ikf8b30am6cawcclcy70flw1m52lzq0wyizv104")))

(define-public crate-humantime-1.2.0 (c (n "humantime") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "057ilhy6vc9iqhhby5ymh45m051pgxwq2z437gwkbnqhw7rfb9rw")))

(define-public crate-humantime-1.3.0 (c (n "humantime") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "0krwgbf35pd46xvkqg14j070vircsndabahahlv3rwhflpy4q06z")))

(define-public crate-humantime-2.0.0 (c (n "humantime") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1nljdc33ib52rqrv9y9w08dba3g1h1jlxd47h273qbak0qrwbdmr")))

(define-public crate-humantime-2.0.1 (c (n "humantime") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0yivhqyi8xik2j6sd3q45ybakjx8jsx5632dx9xjn0birh4dj6iw")))

(define-public crate-humantime-2.1.0 (c (n "humantime") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1r55pfkkf5v0ji1x6izrjwdq9v6sc7bv99xj6srywcar37xmnfls")))

