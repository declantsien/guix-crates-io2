(define-module (crates-io hu ma humanity) #:use-module (crates-io))

(define-public crate-humanity-0.1.0 (c (n "humanity") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "03kfdr5xlrbbijbg5pfk0k2nlqj7kj14fcyxa3k7n0k4z07i35jj")))

