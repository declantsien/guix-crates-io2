(define-module (crates-io hu ma human_format) #:use-module (crates-io))

(define-public crate-human_format-1.0.0 (c (n "human_format") (v "1.0.0") (d (list (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)))) (h "1jmk8digsijbszfk1xd1qbb452sa1fnwaqy3x0hs03yn2fz0ywqm")))

(define-public crate-human_format-1.0.1 (c (n "human_format") (v "1.0.1") (d (list (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)))) (h "110b4f1ivi84aqsvxyrmmk3yxfmnlf0s8px57q1is4qwmyjz8sp8")))

(define-public crate-human_format-1.0.2 (c (n "human_format") (v "1.0.2") (d (list (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)))) (h "0b8453g5mb0bf39zfs1n8m5h6i48ymn6546mvs68b0cnmfzrfm1x")))

(define-public crate-human_format-1.0.3 (c (n "human_format") (v "1.0.3") (d (list (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)))) (h "1c3w2lm41gmnd82acjsi6y50ym2sh5fdb6abghysmaaqsxhf5k46")))

(define-public crate-human_format-1.1.0 (c (n "human_format") (v "1.1.0") (d (list (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)))) (h "1f45y1vib7z96ij5xv623rxpdm3ap4bj11j84jqjg7a5iir1yfsw")))

