(define-module (crates-io hu ma humansize) #:use-module (crates-io))

(define-public crate-humansize-0.1.0 (c (n "humansize") (v "0.1.0") (h "0d8094s27svp0il3my7vjb2ypqkn8q32y9908ccmarjsxs3r2fjp")))

(define-public crate-humansize-0.1.1 (c (n "humansize") (v "0.1.1") (h "0xwgm218hnsk7r6hrmhcq5lcc9ffblls1fakpqc1ypzzg36fij9y")))

(define-public crate-humansize-1.0.0 (c (n "humansize") (v "1.0.0") (h "107xkp5xpfz8h6b4sz6vhhs5x4z40ja8hkdbkhmf2jai1863x5lv")))

(define-public crate-humansize-1.0.1 (c (n "humansize") (v "1.0.1") (h "1lqb3g59xzqand81rcmvig1wi8r99xl4fnsirsfp818bwzk13llj")))

(define-public crate-humansize-1.0.2 (c (n "humansize") (v "1.0.2") (h "0y1p3xnpkkjppfgpb03dm7qj3f0473w1a4as58qhq2vrn2yh966r")))

(define-public crate-humansize-1.1.0 (c (n "humansize") (v "1.1.0") (h "0piadmwjah1jv6q288im4za9szlgalzjyq2811w35i6gg9ib5jmn")))

(define-public crate-humansize-1.1.1 (c (n "humansize") (v "1.1.1") (h "09nh6xyssghjajvip9crd79i4a40nw8r4bdwwg3dg5l7rfb6ja82")))

(define-public crate-humansize-2.0.0 (c (n "humansize") (v "2.0.0") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "03ppa9zwcb7arh69gpdqzqk443s3izpggvihghand8l77c70j4lf") (f (quote (("no_alloc") ("impl_style"))))))

(define-public crate-humansize-2.1.0 (c (n "humansize") (v "2.1.0") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "16md0kxn6sfw8whdmgka5wn5zdqdzl0yxwr1n97x6jpk2rsq6rm8") (f (quote (("no_alloc") ("impl_style"))))))

(define-public crate-humansize-2.1.1 (c (n "humansize") (v "2.1.1") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "0gimz44jsyrdivrkl2qjcbs8hpbxg7aand02wbjfxdq9ghpnds3r") (f (quote (("no_alloc") ("impl_style")))) (r "1.56")))

(define-public crate-humansize-2.1.2 (c (n "humansize") (v "2.1.2") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "0mi7dng641z01j3ggb30ivm1y05slj9aj4cz43jwrjqfswmjws2f") (f (quote (("no_alloc") ("impl_style")))) (r "1.56")))

(define-public crate-humansize-2.1.3 (c (n "humansize") (v "2.1.3") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)))) (h "1msxd1akb3dydsa8qs461sds9krwnn31szvqgaq93p4x0ad1rdbc") (f (quote (("no_alloc") ("impl_style")))) (r "1.56")))

