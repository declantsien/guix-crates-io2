(define-module (crates-io hu ma human-time-macros) #:use-module (crates-io))

(define-public crate-human-time-macros-0.1.0 (c (n "human-time-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0dak44nld0ndrqncpzlg5nqikwkgsfyd71dxqsycriznrvpixr4w")))

(define-public crate-human-time-macros-0.1.1 (c (n "human-time-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "07cls66kx1gc10x4mm3ad77mw54yjff9vdfgadk42lan72iw3qdk")))

(define-public crate-human-time-macros-0.1.2 (c (n "human-time-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "17p2bqa8m1cpam42rafhrj3g2ik52c1lgrghkanw6pvbd4qg0qig")))

(define-public crate-human-time-macros-0.1.3 (c (n "human-time-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "17z1i96kmf4qsiqxkl4dxcifi3lw093ddprcpzm3raibpafa4ykq")))

(define-public crate-human-time-macros-0.1.4 (c (n "human-time-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0lxmbl99qgyrinlfh6yngr18f1yxhp9h0cgrbg8jhgvv6gbqbbj8")))

(define-public crate-human-time-macros-0.1.5 (c (n "human-time-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "03d0rfddz4lgdaz6h14clj7rcg5aqi60x0j7xp5d7ndgp0z4x80l")))

(define-public crate-human-time-macros-0.1.6 (c (n "human-time-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1135xh5db6v3j9356c3cbk8xaiicjwqznyy4m7k5ij83yp9vm2g6")))

(define-public crate-human-time-macros-0.1.7 (c (n "human-time-macros") (v "0.1.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1nsg67qbvsj55g4cmzxsksk5wgw4sg2khn6npf589g7qrpy6bblg")))

(define-public crate-human-time-macros-0.1.8 (c (n "human-time-macros") (v "0.1.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "02aawhws53d4qm580bij4kh82j4hlh93599zxls055pxbgw2jhd0")))

