(define-module (crates-io hu e- hue-bridge) #:use-module (crates-io))

(define-public crate-hue-bridge-0.1.0-alpha.1 (c (n "hue-bridge") (v "0.1.0-alpha.1") (d (list (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "15x01y0bn3x85b6yw7jwbxb4m91p3ign5zmfppqhjl7xqspngzk8")))

(define-public crate-hue-bridge-0.1.0-alpha.2 (c (n "hue-bridge") (v "0.1.0-alpha.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0f28jzjnrl2r4mjabbrzii091pm57lcy1xsisasm00fxi33bf3ry")))

