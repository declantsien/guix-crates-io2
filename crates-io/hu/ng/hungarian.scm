(define-module (crates-io hu ng hungarian) #:use-module (crates-io))

(define-public crate-hungarian-0.1.0 (c (n "hungarian") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.1.8") (d #t) (k 0)))) (h "04pkwmyjb1kdwmba2nwwdc21fqxqymiicibdk4m7r87x12laa6df")))

(define-public crate-hungarian-1.0.0 (c (n "hungarian") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.1.8") (d #t) (k 0)))) (h "0h4fzlnc4c8la45mhsw0cbms6534w84mglbl2psmyxrr3aw7j88j")))

(define-public crate-hungarian-1.1.0 (c (n "hungarian") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.1.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "19vvcsis5ygyadxr2nh55nvvkfb16apb6pp4gy5b754v5akcnw44")))

(define-public crate-hungarian-1.1.1 (c (n "hungarian") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pathfinding") (r "^2.0") (d #t) (k 2)))) (h "0va294d5l1964rqlkzw2ljwpiv6ilj37g1qx33cj39jffbmwa9zv")))

