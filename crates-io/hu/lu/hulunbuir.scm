(define-module (crates-io hu lu hulunbuir) #:use-module (crates-io))

(define-public crate-hulunbuir-0.1.0 (c (n "hulunbuir") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "07wwhfxnb52276750nvm9n25g8sjnambdlin2hs9sfvyrsmz1hjj")))

(define-public crate-hulunbuir-0.2.0 (c (n "hulunbuir") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)))) (h "05jcs7lln91v0ba9450kr3al8dmyjjhqgm49ffbkplagfmnngvgz")))

(define-public crate-hulunbuir-0.2.1 (c (n "hulunbuir") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1rf016297mfj44sy3fhq3h5xcr6gvz8y4dx8m6fzcphxf51z68cm")))

(define-public crate-hulunbuir-0.2.2 (c (n "hulunbuir") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "10is5y5rriib1xjpyqmm5l7qdwb2gr45w0w184vk2v7522rss3p4")))

(define-public crate-hulunbuir-0.2.3 (c (n "hulunbuir") (v "0.2.3") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0c4bw3mq436v5klvkyp20c327vj66w76zbyx8c81hm660kr2knr3")))

(define-public crate-hulunbuir-0.2.4 (c (n "hulunbuir") (v "0.2.4") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1girhn5cynvm0lmsa50mczzq2axbys3mnpbhy6hy315hhfmcn0v5")))

(define-public crate-hulunbuir-0.3.0 (c (n "hulunbuir") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0nwyvl88ni65jw7mh2rndqry5xlp16sam07lcdrf3sxzh1bmrl0c")))

