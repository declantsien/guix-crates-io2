(define-module (crates-io hu ge hugealloc) #:use-module (crates-io))

(define-public crate-hugealloc-0.1.0 (c (n "hugealloc") (v "0.1.0") (d (list (d (n "allocator-api2") (r "^0.2") (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("mm"))) (t "cfg(not(miri))") (k 0)) (d (n "sptr") (r "^0.3") (d #t) (k 0)))) (h "1hhhrgif0zw44ns8f672vc5yssz54lljdmnhdlvdpcp3l1yjvcnr")))

