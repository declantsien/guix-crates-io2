(define-module (crates-io hu ge hugepage-rs) #:use-module (crates-io))

(define-public crate-hugepage-rs-0.1.0 (c (n "hugepage-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01d5j2ximcvxr5w1vng0c56rlwy0z0dqxpjr6n5l6fvq95001ddy")))

