(define-module (crates-io hu nk hunktool) #:use-module (crates-io))

(define-public crate-hunktool-0.2.1 (c (n "hunktool") (v "0.2.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0brwsbwfxa9ph8xcxiz28sq3m3sqpx9kfvzn55lnlq3h8hvg7l08")))

(define-public crate-hunktool-0.3.0 (c (n "hunktool") (v "0.3.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1alrywc4gici4ys44gpd2flcm71w5kghczw74yns1p1vdhmrr9g9")))

(define-public crate-hunktool-0.4.0 (c (n "hunktool") (v "0.4.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0g0l5pcprn58smsawwwfpfywiaczwgc8aw5inldrq483pjv4qxqm")))

(define-public crate-hunktool-0.4.1 (c (n "hunktool") (v "0.4.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "simple-error") (r "^0.3.0") (d #t) (k 0)))) (h "06c7js8czfmbrq58p2bbdk7pprjddh03zrs6lmi88k5shawiwk92")))

