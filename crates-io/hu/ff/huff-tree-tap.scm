(define-module (crates-io hu ff huff-tree-tap) #:use-module (crates-io))

(define-public crate-huff-tree-tap-0.0.1 (c (n "huff-tree-tap") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0wimg811hkv1v2n8k81pin8r4zc3jz3rpwb2kkwh7qdrdz3saiiv")))

(define-public crate-huff-tree-tap-0.0.2 (c (n "huff-tree-tap") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "05mk2kjnd2f502a61284b0iskcxalsvhmjbak7icmv3mnsgpkifr")))

(define-public crate-huff-tree-tap-0.0.3 (c (n "huff-tree-tap") (v "0.0.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11hzsy2189bis0wsyh2072ajbwa5kk8zcc6i3wxgywjzkpv5zkj5")))

(define-public crate-huff-tree-tap-0.0.4 (c (n "huff-tree-tap") (v "0.0.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1iffx2awdffr5mkqadlfixrsb1266jkl3zs686kr3glxcvl7qbxa")))

