(define-module (crates-io hu ff huff) #:use-module (crates-io))

(define-public crate-huff-1.0.0 (c (n "huff") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "huff_coding") (r "^0.0.1") (d #t) (k 0)))) (h "1k1a6705j5and3ksdf8q4w63zpjx3pkac3vckq498k8h4lyh7y9w") (y #t)))

(define-public crate-huff-1.0.1 (c (n "huff") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "huff_coding") (r "^0.0.1") (d #t) (k 0)))) (h "0ywsxldp46bjbrq82cnld2ak3qlzc9rvi1lfan6qgr1algvarm1r") (y #t)))

(define-public crate-huff-1.0.2 (c (n "huff") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "huff_coding") (r "^0.0.1") (d #t) (k 0)))) (h "1ridnfd5z3qhby98aqjryyqcchn3582l0a7p068s28bx61b9x35m") (y #t)))

(define-public crate-huff-1.0.3 (c (n "huff") (v "1.0.3") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "huff_coding") (r "^0.0.1") (d #t) (k 0)))) (h "1hjfmv14849diwyncvmhbdxnlb4gd6snl4zc01g75alc0q46afvc")))

(define-public crate-huff-1.0.4 (c (n "huff") (v "1.0.4") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "huff_coding") (r "^1.0.0") (d #t) (k 0)))) (h "10l0x0qwxhcmxv333brqs8d924wihb57af0j80b2bc9lly27qvz8") (y #t)))

(define-public crate-huff-1.0.5 (c (n "huff") (v "1.0.5") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "huff_coding") (r "^1.0.0") (d #t) (k 0)))) (h "16ahx2wi49n9jnd4rgkdlmvbm4xv8k47kwxqvsc0vqrk99kf008p")))

(define-public crate-huff-1.0.6 (c (n "huff") (v "1.0.6") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "huff_coding") (r "^1.0.0") (d #t) (k 0)))) (h "066nc9y8idqrspwyihxfd9g59391mr1279b3nwk2ra2va5g0vz95")))

