(define-module (crates-io hu ff huff_utils) #:use-module (crates-io))

(define-public crate-huff_utils-0.1.0 (c (n "huff_utils") (v "0.1.0") (d (list (d (n "codemap-diagnostic") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0gg6wn0840d5vf7sach8pz6drm2gh15qy120ycwm61sajsrb604x")))

