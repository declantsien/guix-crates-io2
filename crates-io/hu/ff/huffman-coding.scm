(define-module (crates-io hu ff huffman-coding) #:use-module (crates-io))

(define-public crate-huffman-coding-0.1.0 (c (n "huffman-coding") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)) (d (n "bitstream-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (o #t) (d #t) (k 0)))) (h "1kz1nijcc1n6yvy5khbk81060pjrjgc1ydqzlvxn7fzjgyski1rw") (f (quote (("default-features") ("bin" "clap"))))))

(define-public crate-huffman-coding-0.1.1 (c (n "huffman-coding") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)) (d (n "bitstream-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (o #t) (d #t) (k 0)))) (h "01yf50a4g8z8m350gdzaq2mqy5wsbv7n1zgdxclxq5bp01v61jxa") (f (quote (("default-features") ("bin" "clap"))))))

(define-public crate-huffman-coding-0.1.2 (c (n "huffman-coding") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)) (d (n "bitstream-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (o #t) (d #t) (k 0)))) (h "1kf4sdyaz0jmd40d5rv22b2xxspjqr244zdqrxxc29apv559fhxx") (f (quote (("default-features") ("bin" "clap"))))))

