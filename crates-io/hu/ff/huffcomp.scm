(define-module (crates-io hu ff huffcomp) #:use-module (crates-io))

(define-public crate-huffcomp-0.1.16 (c (n "huffcomp") (v "0.1.16") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1ammv4z5lbsrib5pw1w005zw9c1y4ajdh4sfgbfywn8jak8xfdc6")))

(define-public crate-huffcomp-0.1.18 (c (n "huffcomp") (v "0.1.18") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1mljrmj91r328yb4r94rcyg81n3f3jyjkig90w9b4w202rsci77g")))

(define-public crate-huffcomp-0.1.19 (c (n "huffcomp") (v "0.1.19") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1gxch2s617k13b5fqrrizm9yxf7srsw81iwl7bwfm9kfwa79lqbp")))

(define-public crate-huffcomp-0.1.20 (c (n "huffcomp") (v "0.1.20") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "0m0c93v21xnc8pismyr0gfivn0ql7faljchrk3apabzbi40f72pq")))

(define-public crate-huffcomp-0.1.21 (c (n "huffcomp") (v "0.1.21") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1l0bkqkp084897pvb5wni6qp1d8h12qrcga4n02f00888rnp24ry")))

(define-public crate-huffcomp-0.1.22 (c (n "huffcomp") (v "0.1.22") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "1wbjd0an831cwvg3aq8y26fz9lrf84g66skicwx071vh9r6d1rfp")))

(define-public crate-huffcomp-0.1.23 (c (n "huffcomp") (v "0.1.23") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "02ypk9rn0a3hq54ymjwifwi6c3i327d5l7d3qw89y8a34i8c6bnn")))

(define-public crate-huffcomp-0.1.24 (c (n "huffcomp") (v "0.1.24") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)))) (h "0si7bmssa5shdx6mmi4ibfnq983ccrgdmcdqg2d29cnhapylczci")))

