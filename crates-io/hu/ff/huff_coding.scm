(define-module (crates-io hu ff huff_coding) #:use-module (crates-io))

(define-public crate-huff_coding-0.0.0 (c (n "huff_coding") (v "0.0.0") (d (list (d (n "bitvec") (r "^0.20.1") (d #t) (k 0)))) (h "1msrprz2w09vigkigsjdrv1vgy8467fncsjm471w8cp9119n79fs")))

(define-public crate-huff_coding-0.0.1 (c (n "huff_coding") (v "0.0.1") (d (list (d (n "bitvec") (r "^0.20.1") (d #t) (k 0)))) (h "1ambn07g6jqab5a241l2psiq6waijg3f9ahgi4mmv2z3ijz4dkzy")))

(define-public crate-huff_coding-1.0.0 (c (n "huff_coding") (v "1.0.0") (d (list (d (n "bitvec") (r "^0.20.1") (d #t) (k 0)) (d (n "funty") (r "=1.1.0") (d #t) (k 0)))) (h "1vc6xkcbr867x760rg8k0zd25ynvgwb8s74r8s1dd1xp30r8wdkg")))

