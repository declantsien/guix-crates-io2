(define-module (crates-io hu ff huffman-rust) #:use-module (crates-io))

(define-public crate-huffman-rust-0.1.0 (c (n "huffman-rust") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "15d1iqncicmn2hiyhsq4l44kfa8366ym0hhm3h8dm89zjmwkic1a")))

(define-public crate-huffman-rust-0.1.1-alpha (c (n "huffman-rust") (v "0.1.1-alpha") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "1afp534a2rrvy1idszx6g04ddbii5bzfy20hkgnmsc9cxxya6j7d")))

(define-public crate-huffman-rust-0.1.1 (c (n "huffman-rust") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)))) (h "1qsz3w1hivh1a1l8c31jnfs63bk2hf08dgh00agircx7a1kscs3c")))

