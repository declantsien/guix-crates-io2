(define-module (crates-io hu ff huffman) #:use-module (crates-io))

(define-public crate-huffman-0.0.1 (c (n "huffman") (v "0.0.1") (d (list (d (n "bitstream") (r "^0.1") (d #t) (k 0)))) (h "1zq9fm893ml8fzljzjin2hzr01d1wxyvcmp7sys47x4kh0h67qlh")))

(define-public crate-huffman-0.0.2 (c (n "huffman") (v "0.0.2") (d (list (d (n "bitreader") (r "^0.1") (d #t) (k 0)))) (h "1gg6n5dra4jqbvzc2m9ibs2nkx5jacmpwzq7vz9pgc2vsli8mc8k") (f (quote (("nightly"))))))

(define-public crate-huffman-0.0.3 (c (n "huffman") (v "0.0.3") (d (list (d (n "bitreader") (r "^0.1") (d #t) (k 0)))) (h "1mmhyizph979swi1p0fdlk3s0hxi5jxgmzn28v018ss6a1jlmqph") (f (quote (("nightly"))))))

