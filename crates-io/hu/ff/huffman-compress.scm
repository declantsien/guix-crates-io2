(define-module (crates-io hu ff huffman-compress) #:use-module (crates-io))

(define-public crate-huffman-compress-0.1.0 (c (n "huffman-compress") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1vn4ydn86n2scsg7g0qh44kpikwc95xyhh7m0imsa9g6fqrcmqkk")))

(define-public crate-huffman-compress-0.1.1 (c (n "huffman-compress") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "15418sni551qb9174hdbgl38n4bxc678shn0vxjyvjggg5qw3km2")))

(define-public crate-huffman-compress-0.2.0 (c (n "huffman-compress") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1v486dvjjc2ga2wryxias1id49r5vn0prslm3w7wbw8jy34zi7nz")))

(define-public crate-huffman-compress-0.3.0 (c (n "huffman-compress") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "01ckxcpp5m2kwp2mr8nxvx85h8qisyhnckj0cs14ykzyksm37d44")))

(define-public crate-huffman-compress-0.3.1 (c (n "huffman-compress") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1ix4ih06h65c3k4bvnn6dvb8m2zq577z2y0c22ibywf12jcqrxkm")))

(define-public crate-huffman-compress-0.3.2 (c (n "huffman-compress") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1wr5fzgk2062in3yd3ac5m9yvc2dcqnw3amx3y89dbgym0l94js1")))

(define-public crate-huffman-compress-0.4.0 (c (n "huffman-compress") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1c30wg1xipv24l5m9xl8yn3n0m4krj1a71yn7vfmvd6ssn1jc39p")))

(define-public crate-huffman-compress-0.5.0 (c (n "huffman-compress") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1h0y27ml61v5xpg7gmxam8z6d3s7dsasabpg5n0s9y3ckvf3pxs7")))

(define-public crate-huffman-compress-0.6.0 (c (n "huffman-compress") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "18yl72psgsdz798i11r6hypjhl5vhgjsj1gc2na3m6vsqlgbvld3")))

(define-public crate-huffman-compress-0.6.1 (c (n "huffman-compress") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0xvys77r8231xnv6hm4qkw65k4d1rrrdj1gdy2wr2q9mjbwhylnx")))

