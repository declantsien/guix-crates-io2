(define-module (crates-io hu ff huffman-codec) #:use-module (crates-io))

(define-public crate-huffman-codec-0.1.0 (c (n "huffman-codec") (v "0.1.0") (h "18g8grd1j4n7r20h5aqikm02dm4wqxwbill6al1sqa9s8hr8w6mv")))

(define-public crate-huffman-codec-0.1.1 (c (n "huffman-codec") (v "0.1.1") (h "0x3hg8j425w2kvrj095xlp8xnmj4m30v5lqcncrgqsg1jib3xipl")))

(define-public crate-huffman-codec-0.1.2 (c (n "huffman-codec") (v "0.1.2") (h "0lz3nax03zf4ix3z5qpydcmffkqgqym2hxnd4s83dq5cc1vrra6m")))

(define-public crate-huffman-codec-0.1.3 (c (n "huffman-codec") (v "0.1.3") (h "0rg7yj0k81fv3s88mkwd6m7gs23v5nivvzz6jqq27aqbfly8l70m")))

(define-public crate-huffman-codec-0.1.4 (c (n "huffman-codec") (v "0.1.4") (h "0b374rvhhgwl704kylvwd7hdiqh3njcrkdrf9dl4zcdsqkhp922i") (f (quote (("nightly-features") ("default"))))))

(define-public crate-huffman-codec-0.1.5 (c (n "huffman-codec") (v "0.1.5") (h "1z46iwfrlsxxacq8dhva6iplyj2vwmcvz8jpd5lzfsv9ch9awzxr") (f (quote (("nightly-features") ("default"))))))

(define-public crate-huffman-codec-0.1.6 (c (n "huffman-codec") (v "0.1.6") (h "14xlf90hd1drjw7yqjwhckrck6h71f58ms6rrwmws6ggd278i05n") (f (quote (("nightly-features") ("default"))))))

