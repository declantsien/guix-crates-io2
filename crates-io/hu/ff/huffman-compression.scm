(define-module (crates-io hu ff huffman-compression) #:use-module (crates-io))

(define-public crate-huffman-compression-0.1.0 (c (n "huffman-compression") (v "0.1.0") (h "0bbpq0l58iq8w6hl0jachxwl5dlp8lncph45jyfkla592xczy0w3")))

(define-public crate-huffman-compression-0.1.1 (c (n "huffman-compression") (v "0.1.1") (h "1v320g4ak0zshmblgxb5ndvk14axai141yb0gxzppxcb9cnc2jrf")))

(define-public crate-huffman-compression-0.1.2 (c (n "huffman-compression") (v "0.1.2") (h "1fpsrvslhijs7zx11bm3x1pr8i7ykrhyj9ll723rad04fgrzcdzv")))

