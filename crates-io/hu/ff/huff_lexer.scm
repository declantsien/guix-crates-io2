(define-module (crates-io hu ff huff_lexer) #:use-module (crates-io))

(define-public crate-huff_lexer-0.3.0 (c (n "huff_lexer") (v "0.3.0") (d (list (d (n "huff_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "1mdhlwyi1zl92sczmszzbmanvpr5d1889qnmhcjhlgaxhgmpvg98")))

