(define-module (crates-io hu ff huff_rs) #:use-module (crates-io))

(define-public crate-huff_rs-0.1.0 (c (n "huff_rs") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.0") (d #t) (k 0)) (d (n "bitvec") (r "^0.21.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "readonly") (r "^0.2.0") (d #t) (k 0)) (d (n "throbber") (r "^0.1") (d #t) (k 0)))) (h "0ghhmic9ph7h521pf7i9iz5xqcj07v0ijkpw0jaj4azqqqy78xfs")))

(define-public crate-huff_rs-0.1.1 (c (n "huff_rs") (v "0.1.1") (d (list (d (n "ahash") (r "^0.7.0") (d #t) (k 0)) (d (n "bitvec") (r "^0.21.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "readonly") (r "^0.2.0") (d #t) (k 0)) (d (n "throbber") (r "^0.1") (d #t) (k 0)))) (h "13qm0wqpry4vasykd266vx1rm4yp3dv9gr6xfkfz3fyqg8kl8gx6")))

(define-public crate-huff_rs-0.1.2 (c (n "huff_rs") (v "0.1.2") (d (list (d (n "ahash") (r "^0.7.0") (d #t) (k 0)) (d (n "bitvec") (r "^0.21.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "readonly") (r "^0.2.0") (d #t) (k 0)) (d (n "throbber") (r "^0.1") (d #t) (k 0)))) (h "1kxvdf3ir7laz506bac1m6r8cb3q59n8d62sgxamk6n8khz4r4cz") (y #t)))

(define-public crate-huff_rs-0.1.3 (c (n "huff_rs") (v "0.1.3") (d (list (d (n "ahash") (r "^0.7.0") (d #t) (k 0)) (d (n "bitvec") (r "^0.21.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "readonly") (r "^0.2.0") (d #t) (k 0)) (d (n "throbber") (r "^0.1") (d #t) (k 0)))) (h "0plc4x1g81mvaciry9fqkh2lry920jad4xnqqn9qrr1d6ab89fdz")))

