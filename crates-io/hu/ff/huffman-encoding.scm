(define-module (crates-io hu ff huffman-encoding) #:use-module (crates-io))

(define-public crate-huffman-encoding-0.1.0 (c (n "huffman-encoding") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rkvsw1nd554ymfqr4sljv7ii7lhs2w72b559aqjns4paaywffhg")))

(define-public crate-huffman-encoding-0.1.1 (c (n "huffman-encoding") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xmjs6s68pnirkcxwx5gy10h5y73v288nym78lxx19k32mjmyfg0")))

(define-public crate-huffman-encoding-0.1.2 (c (n "huffman-encoding") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09m9ckpq9jidijdb0h3k5pwcp05vhqvqzyqdzkqhxj3y2n9xlkb8")))

