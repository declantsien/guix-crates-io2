(define-module (crates-io hu b7 hub75-pio) #:use-module (crates-io))

(define-public crate-hub75-pio-0.1.0 (c (n "hub75-pio") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "pio") (r "^0.2.0") (d #t) (k 0)) (d (n "pio-proc") (r "^0.2.1") (d #t) (k 0)) (d (n "rp2040-hal") (r "^0.6.0") (d #t) (k 0)))) (h "0kcdg5ax2wy8dxh26j1l4h455av6mh0ayx5s8p8pbsvxq870a02c")))

