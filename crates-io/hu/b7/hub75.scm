(define-module (crates-io hu b7 hub75) #:use-module (crates-io))

(define-public crate-hub75-0.1.0 (c (n "hub75") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.5.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1r88jhf1j0k9sza7j64lxhx59ddyqn8kyp05pdxp7mdbgp3fi2xn")))

(define-public crate-hub75-0.1.1 (c (n "hub75") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.5.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0g470wxkq8rzvchsp98qjaqhar2cdbjyqpzkw2101lwrxc50ccqq")))

