(define-module (crates-io hu d- hud-slice-by-8) #:use-module (crates-io))

(define-public crate-hud-slice-by-8-1.0.3 (c (n "hud-slice-by-8") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1gi99qc6h71kb8mm0qdybgnp1k905wqjvvizswfy125g0fbm7y48") (y #t)))

(define-public crate-hud-slice-by-8-1.0.5 (c (n "hud-slice-by-8") (v "1.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0bkyx85mkmyc62w9avz68h7w6rsz3941yp2b2rdvy0svk53lgs8g")))

(define-public crate-hud-slice-by-8-1.0.7 (c (n "hud-slice-by-8") (v "1.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m5f8apbjgxlrrjjgd60hm50k1qpg6xm9d52p8p216zz3bwy4zc1")))

(define-public crate-hud-slice-by-8-1.0.8 (c (n "hud-slice-by-8") (v "1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w1d2vcv6zbjh0h2z2vyvcrq1asshsqwl4xidjm39f0p1ia4938m")))

