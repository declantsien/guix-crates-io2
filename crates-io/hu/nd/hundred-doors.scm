(define-module (crates-io hu nd hundred-doors) #:use-module (crates-io))

(define-public crate-hundred-doors-0.1.0 (c (n "hundred-doors") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "0maj9y0wffrb49wrgy003y6c99lxsjidffp5pvlhq4vsln2j6r69")))

(define-public crate-hundred-doors-0.1.1 (c (n "hundred-doors") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "1yqkadg6qd3h70ssfv8zi1q2vzarjgkl80f4pxjikclapf55brz2")))

(define-public crate-hundred-doors-0.1.2 (c (n "hundred-doors") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "0w2mjfzf8ydg9drnzmipbb26ixywfwz60yiipk647crlny7rmzb1")))

(define-public crate-hundred-doors-0.1.3 (c (n "hundred-doors") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "0p8fmlizi021dl0khh3k8i71q17iam23j1m7n2p0q42n4hsk5wvn")))

