(define-module (crates-io hu la hulahoop) #:use-module (crates-io))

(define-public crate-hulahoop-0.1.0 (c (n "hulahoop") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)))) (h "0g8knkzzqhbifsba748ns1s1rjl0dpx68iz4px3yq83bmc8jpjy1") (s 2) (e (quote (("fxhash" "dep:rustc-hash"))))))

(define-public crate-hulahoop-0.2.0 (c (n "hulahoop") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 2)))) (h "14micny2rv420ra6hgqsryib6827m36rixc8acch9lqmnmb6a6fv") (s 2) (e (quote (("fxhash" "dep:rustc-hash"))))))

