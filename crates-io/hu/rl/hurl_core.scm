(define-module (crates-io hu rl hurl_core) #:use-module (crates-io))

(define-public crate-hurl_core-1.2.0 (c (n "hurl_core") (v "1.2.0") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0kkasynacqd28i30pwz271vfmxsxps6dpdhc39jxp51wznrn37v8") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.3.0 (c (n "hurl_core") (v "1.3.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0gi2zzxxavza1f8zcpv9phsbm6ld9z7y8kyd9c7i1g4qjgadz5ar") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.3.1 (c (n "hurl_core") (v "1.3.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0nd615jiabwn49pbh51k85i1icccr2zzb7lapjyrgj7iaxmxf1kp") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.4.0 (c (n "hurl_core") (v "1.4.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "199nipzaxv0hp9iisf3akhglblm5qz9sz3578knyn99iqxv5y15z") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.5.0 (c (n "hurl_core") (v "1.5.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0sw6hrrycznb3g6laxwf3ccl85llbiyqx9v6ib99d9s17bgzngk7") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.6.0 (c (n "hurl_core") (v "1.6.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "1bxkr8drmfsq7mllrkink3w0azpgrsdlgn2gkl60miwscva1q7sc") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.6.1 (c (n "hurl_core") (v "1.6.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "06lgn5rl3d8828k69kmy0gabsvx0ngdmb8lawvswragsxydz4sx6") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.7.0 (c (n "hurl_core") (v "1.7.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0zj58mmj81mpcx9dykll904g8apg2vnwlw2a54zanlzqdn86mz4i") (f (quote (("strict"))))))

(define-public crate-hurl_core-1.8.0 (c (n "hurl_core") (v "1.8.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0fpiv4pjj45x8vggl4s42amsfl2s7mhw7fji238jlzl13qi6m7ip") (f (quote (("strict"))))))

(define-public crate-hurl_core-2.0.0 (c (n "hurl_core") (v "2.0.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0r8qp6j7z6amm2a4m97kxmh16l97n3b3v5jrq2k2n1kzck0ksvns") (f (quote (("strict"))))))

(define-public crate-hurl_core-2.0.1 (c (n "hurl_core") (v "2.0.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "02np3kgq1rd5w2hdqjb619qkkq0w0f0hpygpvnccf303s46z4ym1") (f (quote (("strict"))))))

(define-public crate-hurl_core-3.0.0 (c (n "hurl_core") (v "3.0.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "01798vcwlch5lsf41xicpgjq1kdrkn69b724zwrl8hwqq7kxzinm") (f (quote (("strict"))))))

(define-public crate-hurl_core-3.0.1 (c (n "hurl_core") (v "3.0.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "16qwhxbfz9007wrz4cw18xcvxvgh2688hxkdjdjfbzbszcxzjj5n") (f (quote (("strict"))))))

(define-public crate-hurl_core-4.0.0 (c (n "hurl_core") (v "4.0.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "sxd-document") (r "^0.3.2") (d #t) (k 0)))) (h "0l3wwmd1k3clnc6l7rxxqyn09ma8k47l4a45pcyzqrsj89lq3bgc") (f (quote (("strict"))))))

(define-public crate-hurl_core-4.1.0 (c (n "hurl_core") (v "4.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "libxml") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "00cizf207vanizqhs8dck0sm1jzb5l5mzqvh76rd1aa4lrmyjlm6") (f (quote (("strict"))))))

(define-public crate-hurl_core-4.2.0 (c (n "hurl_core") (v "4.2.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "libxml") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1rb6rjv3nflfbnaya51hzz8xiqpxhx041piqgzndynmdycda43z1")))

(define-public crate-hurl_core-4.3.0 (c (n "hurl_core") (v "4.3.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "libxml") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1wxywjrjs4q3z4ql1692b5jv35975gchyj3wa4jgccxxwjcx5lxm")))

