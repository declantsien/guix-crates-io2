(define-module (crates-io hu ed huedump) #:use-module (crates-io))

(define-public crate-huedump-0.1.0 (c (n "huedump") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)))) (h "18dica00bdfgkilw5afsr8vnw0s1xqlmavaqi0vnr2vh19zcvldw")))

(define-public crate-huedump-0.1.1 (c (n "huedump") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)))) (h "12f129a7p09j4jgrr2hvm8ccr59qy870wg2hwk3xs05xc25wmf1h")))

(define-public crate-huedump-0.1.2 (c (n "huedump") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)))) (h "0gsyhmlcm4z4jz3vv1hp3rdplrnj1jrjwi5xnrqra8n0i4ami6hg")))

