(define-module (crates-io hu ni huniq) #:use-module (crates-io))

(define-public crate-huniq-2.0.0 (c (n "huniq") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "0viyr2rjhbqzjlykfsdsvrgvnh8p00w7am5cjswvh0agpjmkdahh")))

(define-public crate-huniq-2.0.1 (c (n "huniq") (v "2.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "1b80rcyi8fcpwlvi6iyraiggck255x5sb042s6mmljgxy23rks6r")))

(define-public crate-huniq-2.0.3 (c (n "huniq") (v "2.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "15nibqjw03pwymwncvivr424bcnc7025zcrbwl34lqpfy058jvq6")))

(define-public crate-huniq-2.1.0 (c (n "huniq") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.48") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 1)) (d (n "sysconf") (r "^0.3.4") (d #t) (k 0)))) (h "1vwdhm1z5y56vy11av9pmkaw9kzlcspk5xgwsqr6q4mddlnyv91w")))

(define-public crate-huniq-2.2.1 (c (n "huniq") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.48") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 1)) (d (n "sysconf") (r "^0.3.4") (d #t) (k 0)))) (h "1y97hs43k727ysx89wc1604pd4kb8sln2sd1f8rks87k8844zjb9")))

(define-public crate-huniq-2.3.0 (c (n "huniq") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.26") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.48") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "shell-words") (r "^0.1.0") (d #t) (k 1)) (d (n "sysconf") (r "^0.3.4") (d #t) (k 0)))) (h "1lv3mjw9149khy2gxjav2dw6419hd93ib16bqpx9wcpvxsxfzgx6")))

(define-public crate-huniq-2.4.1 (c (n "huniq") (v "2.4.1") (d (list (d (n "ahash") (r "^0.4.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "sysconf") (r "^0.3.4") (d #t) (k 0)))) (h "0z6m3fi7prljywnd98ckgw2h5l07d233bj2lh9qv5mzrjr0jc8d4")))

(define-public crate-huniq-2.4.2 (c (n "huniq") (v "2.4.2") (d (list (d (n "ahash") (r "^0.4.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (d #t) (k 0)) (d (n "sysconf") (r "^0.3.4") (d #t) (k 0)))) (h "0mx4xii1gy9sw2yfl7a323npq22c5m8a6ykz1wq1vs1lqp1p53c5")))

(define-public crate-huniq-2.5.0 (c (n "huniq") (v "2.5.0") (d (list (d (n "ahash") (r "^0.5.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1igfx3npky925klkk4f5di47frgm0zssipdzm6zjfh2j39krc1vc")))

(define-public crate-huniq-2.6.0 (c (n "huniq") (v "2.6.0") (d (list (d (n "ahash") (r "^0.5.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0dmjhzz764bjzfknrz3d0dykip6pbj2z8v96mm90xmbbzk1k04g8")))

(define-public crate-huniq-2.7.0 (c (n "huniq") (v "2.7.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "clap") (r "^3.1.9") (d #t) (k 0)))) (h "13wklf2lp5cz5i3m3345nfb01s05kb881zkp16s07z7hqxvnygl9")))

