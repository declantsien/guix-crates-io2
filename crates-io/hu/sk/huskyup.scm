(define-module (crates-io hu sk huskyup) #:use-module (crates-io))

(define-public crate-huskyup-0.1.0 (c (n "huskyup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1m0svjpmh2rakji6x54d17y3w38akh42hs92a92zcyssgs6y8lzq")))

(define-public crate-huskyup-0.1.1 (c (n "huskyup") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)))) (h "1mb00xacpgq386j0xlz3fi7l55fd3f9x92lnhgzq3gqx8zxcbgp3")))

