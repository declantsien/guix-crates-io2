(define-module (crates-io hu sk husk) #:use-module (crates-io))

(define-public crate-husk-0.1.0 (c (n "husk") (v "0.1.0") (h "008igja8zx5yhyvzl9bsii9kikd4bi4p0qy7781s0z4gp0hsjwjx")))

(define-public crate-husk-0.1.1 (c (n "husk") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1bwdyfjg2ycfkg7syr3vmzbx0zb5ajm5kfcmqv8vanlq7jr58d3c")))

(define-public crate-husk-0.1.2 (c (n "husk") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0m8hygj57d4mpiq2hw78dd9c7hx7k53cglgyzav698b2w7s4vra9")))

(define-public crate-husk-0.1.3 (c (n "husk") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1ddg7k0pdqx1wz494cfdkh28b9x0yp0p34w9ha8p11sa9gbsixih")))

