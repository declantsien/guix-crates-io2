(define-module (crates-io hu st hustle) #:use-module (crates-io))

(define-public crate-hustle-1.2.0 (c (n "hustle") (v "1.2.0") (d (list (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "04szzg08carjlmm5kr93kaagrf3y4m1x5x25hfhkpxygvsnhvll0")))

(define-public crate-hustle-1.2.1 (c (n "hustle") (v "1.2.1") (d (list (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "01df1qlb39hp39migm8pbd4vs1gdmzy97mv782g4msqgr4f5f0pi")))

(define-public crate-hustle-1.2.2 (c (n "hustle") (v "1.2.2") (d (list (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1b5lf6znsnk39kkc0qzadxmfqw9vfr6s3lg4nwgl9j2qkwdm1304")))

(define-public crate-hustle-1.2.3 (c (n "hustle") (v "1.2.3") (d (list (d (n "main_error") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "17p83w916vv8wij6anda1wb8r5swmx4pw7i7d71hwwq8r0b8n0qh")))

