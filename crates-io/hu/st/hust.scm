(define-module (crates-io hu st hust) #:use-module (crates-io))

(define-public crate-hust-0.1.0 (c (n "hust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_xml") (r "^0.9") (d #t) (k 0)))) (h "1p2d8psya2h4qbj22ayzr1zvan9sk30f7xqd9rymzbm1614plw7r")))

(define-public crate-hust-0.1.1 (c (n "hust") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_xml") (r "^0.9") (d #t) (k 0)))) (h "1vnnss36nrdvakd7kikgkls4k3si0hszns3gzrzaj01f597xbd21")))

(define-public crate-hust-0.2.0 (c (n "hust") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q0y64qb06y4q7d5msbmnhkdqw10qkxg4m6v60iz15kfpcbp021s")))

(define-public crate-hust-0.2.1 (c (n "hust") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12bp84xgdnb354956b5vl5gl883kygll8idq60mbs6dfcblak0gr")))

