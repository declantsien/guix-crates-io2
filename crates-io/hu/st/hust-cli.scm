(define-module (crates-io hu st hust-cli) #:use-module (crates-io))

(define-public crate-hust-cli-0.1.0 (c (n "hust-cli") (v "0.1.0") (d (list (d (n "hust") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1kswhcchpdzhl3vf84w9j95anxpv8bndl9658qfk653fl738pfgq")))

(define-public crate-hust-cli-0.1.1 (c (n "hust-cli") (v "0.1.1") (d (list (d (n "hust") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0y9c6pzvr7lavmxmkabvi3023qfv72xa7g8n54ma8bgzmvg9pdsc")))

(define-public crate-hust-cli-0.1.2 (c (n "hust-cli") (v "0.1.2") (d (list (d (n "hust") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ybbsnqz89f1kq09n6v7i4902dmjgdhxk0j3mv7q9yjwaprrnxn9")))

(define-public crate-hust-cli-0.2.0 (c (n "hust-cli") (v "0.2.0") (d (list (d (n "hust") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "15wy0vjr0av1n701vxlw3gkkv4jvwng7l37w6ddq97q80kbqk415")))

(define-public crate-hust-cli-0.2.1 (c (n "hust-cli") (v "0.2.1") (d (list (d (n "hust") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "09hw2gwargvx1m91p4799ccakzm2cpln10vfppr37m5xfjlg5ny3")))

