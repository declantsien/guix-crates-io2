(define-module (crates-io hu i- hui-glium) #:use-module (crates-io))

(define-public crate-hui-glium-0.0.1 (c (n "hui-glium") (v "0.0.1") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "glium") (r "^0.34") (d #t) (k 0)) (d (n "hui") (r "=0.0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14yp567d0805nngmlmsmwwlqabch8klxa8jhqqx8fll8qhydnwy2") (y #t)))

(define-public crate-hui-glium-0.0.2 (c (n "hui-glium") (v "0.0.2") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "glium") (r "^0.34") (d #t) (k 0)) (d (n "hui") (r "^0.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bld2ga3hasqciqz2ir0ay1z4b4d857dsa7mzx93kkx5nn36vmn9") (y #t)))

(define-public crate-hui-glium-0.1.0-alpha.1 (c (n "hui-glium") (v "0.1.0-alpha.1") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "glium") (r "^0.34") (d #t) (k 0)) (d (n "hui") (r "^0.1.0-alpha") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0n5c6fz7h8v4k7hbnfbyfk5nd91ilq6c015zyamai1crrhayhbjf")))

(define-public crate-hui-glium-0.1.0-alpha.3 (c (n "hui-glium") (v "0.1.0-alpha.3") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "glium") (r "^0.34") (k 0)) (d (n "hui") (r "=0.1.0-alpha.3") (f (quote ("builtin_font"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lv4nb2gskm8b71fbxssv1gp4zgfm9d4r15x6i90v1sxvvh28aif")))

(define-public crate-hui-glium-0.1.0-alpha.4 (c (n "hui-glium") (v "0.1.0-alpha.4") (d (list (d (n "glam") (r "^0.27") (d #t) (k 0)) (d (n "glium") (r "^0.34") (k 0)) (d (n "hui") (r "=0.1.0-alpha.4") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0j9sbrqpwgy0lk5vsdlhi6fibzq64favvspmvgq6afrc78g325sw") (r "1.75")))

