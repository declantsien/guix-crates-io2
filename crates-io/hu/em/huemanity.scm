(define-module (crates-io hu em huemanity) #:use-module (crates-io))

(define-public crate-huemanity-0.1.0 (c (n "huemanity") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0axm4f6xnhhi72ziddv5r9pizqkpqf0pma9x5zj010akj4n64bnr")))

(define-public crate-huemanity-0.1.1 (c (n "huemanity") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "04z8l63whc3f32vw3v1w06arnr94as2ijvligi6iqv7f20hg5d93")))

(define-public crate-huemanity-0.1.2 (c (n "huemanity") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0k1v7bipa3qdp3mpx3wmbk5sjs6alh103llk8xard9dqngiw5nsx")))

(define-public crate-huemanity-0.1.3 (c (n "huemanity") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1lzmqh241qprmzb7j5whdjckjsg82cwcyib32li4lg7fssrkzqnc")))

(define-public crate-huemanity-0.1.4 (c (n "huemanity") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "17rg78mb28fxrbb0va495xxrczns9rrzwicqxhahcc23x027r7xp")))

(define-public crate-huemanity-0.1.5 (c (n "huemanity") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1vv3jswrr4lh10nbjqklm9d98h2risnyr987il7pz1y375r5zg4j")))

(define-public crate-huemanity-0.1.6 (c (n "huemanity") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.1.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0c6wxg79shgp4kja7q2krh3gzp8z96qr4sqnw5mxhq7xx5xjf53q")))

