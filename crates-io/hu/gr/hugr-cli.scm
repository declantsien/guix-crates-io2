(define-module (crates-io hu gr hugr-cli) #:use-module (crates-io))

(define-public crate-hugr-cli-0.1.0 (c (n "hugr-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "hugr-core") (r "^0.1.0") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "047y6nwgvxy91znz39xdc881ibnlq6dqb453m094fy8g3xcl9c5z") (r "1.75")))

