(define-module (crates-io hu nt hunter-media) #:use-module (crates-io))

(define-public crate-hunter-media-0.1.0 (c (n "hunter-media") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "gstreamer") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "gstreamer-app") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.21.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1cr4j7a0id7mjz1xvsflr99i69fvmril1kr9h1mpa21wmm99wffr") (f (quote (("video" "gstreamer" "gstreamer-app") ("default" "video")))) (y #t)))

