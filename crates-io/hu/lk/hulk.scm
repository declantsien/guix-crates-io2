(define-module (crates-io hu lk hulk) #:use-module (crates-io))

(define-public crate-hulk-0.1.0 (c (n "hulk") (v "0.1.0") (h "12mrklxc20y3xwgx9q53cl2prcsqxssbxaqid5h864yczc22bg14") (y #t)))

(define-public crate-hulk-0.1.1 (c (n "hulk") (v "0.1.1") (d (list (d (n "comrak") (r "^0.12.1") (d #t) (k 0)))) (h "1drwiip73fsfn35gwvd8753hzll4y6q4vcpphb2afjn5ncip1h8p") (y #t)))

(define-public crate-hulk-0.1.2 (c (n "hulk") (v "0.1.2") (d (list (d (n "comrak") (r "^0.12.1") (d #t) (k 0)))) (h "10icq47mrz3z7hsc2cx7izsghdzq0anzpcc9glwcspzm899dnk21") (y #t)))

(define-public crate-hulk-0.1.3 (c (n "hulk") (v "0.1.3") (d (list (d (n "comrak") (r "^0.12.1") (d #t) (k 0)))) (h "1awjm3r0a6aqmppx5p92jp3b84xbzhyw9mnjqm5mjqgj0mqcg9rj") (y #t)))

(define-public crate-hulk-0.1.4 (c (n "hulk") (v "0.1.4") (d (list (d (n "comrak") (r "^0.12.1") (d #t) (k 0)))) (h "1vdgygw9nnqqc86nn97wsbyadm8dsrl031jj271smn8jm5w0mx37") (y #t)))

(define-public crate-hulk-0.1.5 (c (n "hulk") (v "0.1.5") (d (list (d (n "brown") (r "^0.0.5") (d #t) (k 0)) (d (n "comrak") (r "^0.12.1") (d #t) (k 0)))) (h "1v6ybdfzwhid3ixp70q90nhg7miy6nhs061zggxafnlyzdm9jbw7") (y #t)))

(define-public crate-hulk-0.1.6 (c (n "hulk") (v "0.1.6") (d (list (d (n "brown") (r "^0.0.7") (d #t) (k 0)) (d (n "comrak") (r "^0.12.1") (d #t) (k 0)))) (h "187f3bhkmb4jyhf17hbngspz547cm967xmivin87r6ksfahfnzw3") (y #t)))

(define-public crate-hulk-0.1.7 (c (n "hulk") (v "0.1.7") (d (list (d (n "brown") (r "^0.1.3") (d #t) (k 0)) (d (n "comrak") (r "^0.12.1") (d #t) (k 0)))) (h "1h8nvrjxrsn5mbgjawr1w9kg4nr60wjvr90lhvpnqgibblnpqxpa")))

(define-public crate-hulk-0.1.8 (c (n "hulk") (v "0.1.8") (d (list (d (n "brown") (r "^0.2.0") (d #t) (k 0)) (d (n "comrak") (r "^0.12.1") (d #t) (k 0)) (d (n "fltk") (r "^1.2") (d #t) (k 0)) (d (n "fltk-flow") (r "^0.1.2") (d #t) (k 0)) (d (n "qndr") (r "^0.1.7") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0r0x1wlmj0kk5gb4lw4zkp4cd9qdwmdvn0m76xqp1bf5pa8qin1v")))

(define-public crate-hulk-0.1.9 (c (n "hulk") (v "0.1.9") (d (list (d (n "brown") (r "^0.2.0") (d #t) (k 0)) (d (n "comrak") (r "^0.12.1") (d #t) (k 0)) (d (n "fltk") (r "^1.2") (d #t) (k 0)) (d (n "fltk-flow") (r "^0.1.2") (d #t) (k 0)) (d (n "qndr") (r "^0.1.7") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "08fcy5bffpsh09dxx5qv26hx1igbgfsq5gpn1aa0rqqws4aahvw1")))

