(define-module (crates-io hu lk hulk-rs) #:use-module (crates-io))

(define-public crate-hulk-rs-1.2.1 (c (n "hulk-rs") (v "1.2.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n579krrblcfd9j814ivypvg9ybgk4hqzfa0cq9m2n487lpy4y44")))

(define-public crate-hulk-rs-1.2.2 (c (n "hulk-rs") (v "1.2.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z8pv834hfhir7bhwkhpwhdpbgk4xbhrdipx13bhxxxiv0g50cx0")))

(define-public crate-hulk-rs-1.2.3 (c (n "hulk-rs") (v "1.2.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w3gm2fkzpx29zwhg4k0224rk1yihjaankilbafhqcmqisqlsvlx")))

