(define-module (crates-io hu e_ hue_persistence) #:use-module (crates-io))

(define-public crate-hue_persistence-0.1.2 (c (n "hue_persistence") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "philipshue") (r "^0.0") (f (quote ("ssdp" "unstable"))) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "ssdp") (r "^0.3") (d #t) (k 0)) (d (n "syslog") (r "^3.2") (d #t) (k 0)))) (h "19d40yb05k07k3799i1kllwv4v53s2nvhg3601w0xqc5d61jvcr0")))

(define-public crate-hue_persistence-0.2.0 (c (n "hue_persistence") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "philipshue") (r "^0.0") (f (quote ("ssdp" "unstable"))) (k 0)) (d (n "ssdp") (r "^0.3") (d #t) (k 0)) (d (n "syslog") (r "^3.2") (d #t) (k 0)))) (h "15lvsfgrxvyw70jnjcjv6hnynb33wk446c28064y08ampn1mj8j9")))

(define-public crate-hue_persistence-0.2.2 (c (n "hue_persistence") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "philipshue") (r "^0.2.0") (f (quote ("ssdp" "unstable"))) (k 0)) (d (n "ssdp") (r "^0.5.0") (d #t) (k 0)) (d (n "syslog") (r "^3.2") (d #t) (k 0)))) (h "1j46ngjp6jba23ha2gv824qyqddwm9hpb0g1kyzy8lqkbr0lvpq6")))

(define-public crate-hue_persistence-0.2.5 (c (n "hue_persistence") (v "0.2.5") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "philipshue") (r "^0.2") (f (quote ("ssdp" "unstable"))) (k 0)) (d (n "ssdp") (r "^0.5.0") (d #t) (k 0)) (d (n "syslog") (r "^3.2") (d #t) (k 0)))) (h "0w7f3swbzlrlwnjh6fxpz997mk2cdsysfbjribix1wbnz71660an")))

