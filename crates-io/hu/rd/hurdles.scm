(define-module (crates-io hu rd hurdles) #:use-module (crates-io))

(define-public crate-hurdles-0.1.0 (c (n "hurdles") (v "0.1.0") (h "0j4a38z5nkpys9a07wfbkg6v8g1rs75r5hgllklgicifaqp0srkl") (f (quote (("nightly"))))))

(define-public crate-hurdles-0.1.1 (c (n "hurdles") (v "0.1.1") (d (list (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)))) (h "1bvby8bfqzfwy3922f3g8pg9q8s5fdj9cr36kqhhwgviib2gswyx") (f (quote (("nightly"))))))

(define-public crate-hurdles-0.1.2 (c (n "hurdles") (v "0.1.2") (d (list (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)))) (h "0kb1cdajb8wpd1wyqqkvl9ipbch895vx3bzliy4ji7kcr2j3pm63") (f (quote (("nightly"))))))

(define-public crate-hurdles-1.0.0 (c (n "hurdles") (v "1.0.0") (d (list (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)))) (h "12cir0rbd5dj6r3vrzlbfp3jlnkn4cna9675r9li8qbvrz9ksizc") (f (quote (("nightly"))))))

(define-public crate-hurdles-1.0.1 (c (n "hurdles") (v "1.0.1") (d (list (d (n "parking_lot_core") (r "^0.4") (d #t) (k 0)))) (h "1i7rd38hpmq1majraf0zi60k5v6hxdq9qxf5ijik411104zyg9bv") (f (quote (("nightly"))))))

