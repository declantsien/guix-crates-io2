(define-module (crates-io hu eb huebridge) #:use-module (crates-io))

(define-public crate-huebridge-0.1.0 (c (n "huebridge") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b36k36qkr1c1qiada004n0rh1l6zhvdkkrr4lps2xkalfsgsinc")))

(define-public crate-huebridge-0.2.0 (c (n "huebridge") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1h8pvs1w387sj2k38rfm5mlia8kla86mhy7w8pkynlq7fmq74pyi")))

(define-public crate-huebridge-0.3.0 (c (n "huebridge") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1v25lsswf8r53zzs7gly5v63r9yw0nbcgqlc03mdh1qvg020r3y1")))

