(define-module (crates-io hu go hugox) #:use-module (crates-io))

(define-public crate-hugox-0.1.0 (c (n "hugox") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14hyxxvnmp95d9p66b84kq0gpkx5xkh0ra84hbzgbd8803spl1kg")))

