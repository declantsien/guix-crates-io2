(define-module (crates-io hu go hugo-build) #:use-module (crates-io))

(define-public crate-hugo-build-0.115.1-beta1 (c (n "hugo-build") (v "0.115.1-beta1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "106q98l7fj9g4liqdfv0mjq52z5ii86rdj20584wlhmg6mcijvnj")))

(define-public crate-hugo-build-0.115.1-beta2 (c (n "hugo-build") (v "0.115.1-beta2") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1axn6jf1mlyw273awb5kkwa5fzs5ffm4nx5v3cxx45f7sbzznynz")))

(define-public crate-hugo-build-0.115.2 (c (n "hugo-build") (v "0.115.2") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0mm8phd3bv489awwvxl2rnzr8w8vlm3ljdq9a76pp9j8nncqshgh")))

(define-public crate-hugo-build-0.115.4 (c (n "hugo-build") (v "0.115.4") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1244vlkd90zj46n4d1xvny76f0nc7i9c7fz2niiaz915ydvndz60")))

(define-public crate-hugo-build-0.116.0 (c (n "hugo-build") (v "0.116.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0gsglw4jqz7kp23madxxkx21y8ym1w95nhvrd5gfglmcgjg9bgbm")))

(define-public crate-hugo-build-0.116.1 (c (n "hugo-build") (v "0.116.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1kyvjc8hms4ws19wxhx6i4m8jig3h0abq3y4nx34rdry89yw4jwd")))

(define-public crate-hugo-build-0.117.0 (c (n "hugo-build") (v "0.117.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1m4wkdf4m0rbs56fkaxpx652257la1lpp2r0kh9hy0kzadgv0glk")))

(define-public crate-hugo-build-0.118.2 (c (n "hugo-build") (v "0.118.2") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0a5n2pzkdz9x4dqq9782ay951wgc8v38g3v2vl5balzvh6cgg1h2")))

(define-public crate-hugo-build-0.119.0 (c (n "hugo-build") (v "0.119.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1aqwzcg4lc47ja9f0cxsr6c4l4hinv8qxsaxaxqw95kfy3z4rvbm")))

(define-public crate-hugo-build-0.120.3 (c (n "hugo-build") (v "0.120.3") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0d6893yqd3xygx568k39n6nhhillgxg48xpwbg5ad23rz0jjal5v")))

(define-public crate-hugo-build-0.120.4 (c (n "hugo-build") (v "0.120.4") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1lh7an4d1gb0wbisiyb81hvfc2wbs21zyyw6hq38ary7h9n2p7a5")))

(define-public crate-hugo-build-0.121.0 (c (n "hugo-build") (v "0.121.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0bzfj5gfcl90rn2glz8prmncq3jd65vxvml44igr9wh12fznw5sc")))

(define-public crate-hugo-build-0.121.1 (c (n "hugo-build") (v "0.121.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0w16sbppsi2yirbsdsw9k62yac2c0mcm9v4cvqpqri9lpg11ck9f")))

(define-public crate-hugo-build-0.122.0 (c (n "hugo-build") (v "0.122.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "19152jzv9haq5y66zx98jh5k8qq49pi1pqqafis59qzchcykc647")))

(define-public crate-hugo-build-0.123.0 (c (n "hugo-build") (v "0.123.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1ckban78axgcbkdskz5aw2kbhpyr079mgibv0di5xfh71ihsp98d")))

(define-public crate-hugo-build-0.123.1 (c (n "hugo-build") (v "0.123.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1v337rgqgj62w63crll505393lrci6c76wx7bh6jas8rs79cmamw")))

(define-public crate-hugo-build-0.123.2 (c (n "hugo-build") (v "0.123.2") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1dgl94i4snm55w4lphnww1mksw7qmvm0d3zcksvs7lcbh6hv0ays")))

(define-public crate-hugo-build-0.123.3 (c (n "hugo-build") (v "0.123.3") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "10nh9319shrn8r0xpa182l01qns1b2g2xzhbvjjv4zsph4x9qb11")))

(define-public crate-hugo-build-0.123.4 (c (n "hugo-build") (v "0.123.4") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0i4kzjab1wa8xnlvz2kknizgincxpi9z5kwijvnvb7wzqwddivwi")))

(define-public crate-hugo-build-0.123.5 (c (n "hugo-build") (v "0.123.5") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0ijv5hp78bkm9kxafpgzi8qhn8870dys8va6nhvb4lrr11bgga8l")))

(define-public crate-hugo-build-0.123.6 (c (n "hugo-build") (v "0.123.6") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "132gafz9077zzwn6bsn98b69aqz69p0ydshw2lrp510anwlg2503")))

(define-public crate-hugo-build-0.123.7 (c (n "hugo-build") (v "0.123.7") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "10jc63y3xdfghnnmmpsk6lmns8ir7jl1x3lv1n0ys6samvk3v8jy")))

(define-public crate-hugo-build-0.123.8 (c (n "hugo-build") (v "0.123.8") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1f7n7mv280nicgkvpibkmlq7q372abv5s5bfyyb87jx6b3wdim03")))

(define-public crate-hugo-build-0.124.0 (c (n "hugo-build") (v "0.124.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1xnryirz7agv3l79dk7rz6l59pzz10n9hag05wv03xaqxa9w2l3v") (r "1.65")))

(define-public crate-hugo-build-0.124.1 (c (n "hugo-build") (v "0.124.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0514vjg7332mhshkj3yqljixlgrffqxvlicnvq5lagmv7r5qxsxp") (r "1.65")))

(define-public crate-hugo-build-0.125.0 (c (n "hugo-build") (v "0.125.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "071819pxa6787l6dd3bz084qiacv37kbhrvvg1r68q1j7m0bq4pj") (r "1.65")))

(define-public crate-hugo-build-0.125.1 (c (n "hugo-build") (v "0.125.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0qcaaxwkl04fk7ighzp2fl1acvznvfs21mclgy2j37klsjlzx0kk") (r "1.65")))

(define-public crate-hugo-build-0.125.2 (c (n "hugo-build") (v "0.125.2") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "12h5y8d7vd90nhqpmfpgf592s9vn7zg09gs2a72kcasysv499ac6") (r "1.65")))

(define-public crate-hugo-build-0.125.3 (c (n "hugo-build") (v "0.125.3") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "12q7r29xb5v25hnbwsicf4inygdmv5jcyhx16f48rlnshk290rdv") (r "1.65")))

(define-public crate-hugo-build-0.125.4 (c (n "hugo-build") (v "0.125.4") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1kxjd726hfazsrlbdxjxrsdhar1brpqcl9pzdmsrk7w2734l1mik") (r "1.65")))

(define-public crate-hugo-build-0.125.5 (c (n "hugo-build") (v "0.125.5") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1ryvmf7rhb9qchmfsrhv1y9lhb4511aazijya5amg9l3z2i00x8i") (r "1.65")))

(define-public crate-hugo-build-0.125.6 (c (n "hugo-build") (v "0.125.6") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1y1ysbgvxyl74amlqv02rhp5cwpnpvaqvix9hbxzh99pd5d2f3ly") (r "1.65")))

(define-public crate-hugo-build-0.125.7 (c (n "hugo-build") (v "0.125.7") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0zda3qaa41f782qddkvlmnxy4wnnbplwa29p3djr41fqr45c4g7g") (r "1.65")))

(define-public crate-hugo-build-0.126.0 (c (n "hugo-build") (v "0.126.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1dypm054rpsygzn2afh0abihi9hl5c9b3rxw15bfqlx59yasak5p") (r "1.65")))

(define-public crate-hugo-build-0.126.1 (c (n "hugo-build") (v "0.126.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1lqlafh36bcrpgyy6qid5mfzhjip8zfbr55x21zxlc522aq83r6g") (r "1.65")))

