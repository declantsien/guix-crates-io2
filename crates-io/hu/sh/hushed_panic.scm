(define-module (crates-io hu sh hushed_panic) #:use-module (crates-io))

(define-public crate-hushed_panic-0.1.0 (c (n "hushed_panic") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0wh2cch6rwz08f7bh8ns9jp45h28im33dzxhynvxdcbkilyf5mn6") (y #t)))

(define-public crate-hushed_panic-0.1.1 (c (n "hushed_panic") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0i5iqxij9l8vylhc86m4kcqbgjaplhxgg17ixkhyv1b9xz3rbr70")))

