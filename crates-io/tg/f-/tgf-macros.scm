(define-module (crates-io tg f- tgf-macros) #:use-module (crates-io))

(define-public crate-tgf-macros-0.0.2 (c (n "tgf-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10xhz0jdwarlzwqf5lrkscwz998qy79s83yfk8cs34sc9yw1pm4m")))

