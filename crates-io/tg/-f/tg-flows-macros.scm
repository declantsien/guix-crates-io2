(define-module (crates-io tg -f tg-flows-macros) #:use-module (crates-io))

(define-public crate-tg-flows-macros-0.1.0 (c (n "tg-flows-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wfbbn3lj7kk553090z9hj7419kmr1im6d90l4vaccsaqyywhfcc")))

