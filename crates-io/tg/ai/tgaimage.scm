(define-module (crates-io tg ai tgaimage) #:use-module (crates-io))

(define-public crate-tgaimage-0.1.0 (c (n "tgaimage") (v "0.1.0") (h "0h4dy38s41m92zc6casjry83j7h37wzn78sa2lzp80if89j6w08v")))

(define-public crate-tgaimage-0.1.1 (c (n "tgaimage") (v "0.1.1") (h "16qd2bsijv4h89y7r5nb6zqvhiqjypd6ljxk30bcm70rjpy3cjmm")))

