(define-module (crates-io tg -s tg-sys) #:use-module (crates-io))

(define-public crate-tg-sys-0.1.0 (c (n "tg-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1svbr86qbj4i7krq5ax0nqbkxm4zmsq1qc32nm9vmyw3xmscfwms") (l "tg")))

(define-public crate-tg-sys-0.1.1 (c (n "tg-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "089qam150p796fkrapynyaalnjz9msk8jsgqiqhnlac4h9miyz84") (f (quote (("default" "atomics") ("atomics")))) (l "tg")))

