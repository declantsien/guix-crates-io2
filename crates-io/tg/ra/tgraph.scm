(define-module (crates-io tg ra tgraph) #:use-module (crates-io))

(define-public crate-tgraph-0.1.0 (c (n "tgraph") (v "0.1.0") (d (list (d (n "console_engine") (r "^2.3.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "0bwwsnx57hkqgxg4pg5p5h7cy2a62kvfm18x48dzvflwnh35ay6y") (y #t)))

(define-public crate-tgraph-0.1.3 (c (n "tgraph") (v "0.1.3") (d (list (d (n "console_engine") (r "^2.3.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "09yaaa4zpn31619xnyygl3n9wr7s5ckd2jcyzcml69qfpak2rzdx")))

(define-public crate-tgraph-0.2.2 (c (n "tgraph") (v "0.2.2") (d (list (d (n "console_engine") (r "^2.3.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "06zw4l1cvyr174kqp18p8afxp48cg84vslz33s14jkvp2ylsk016")))

