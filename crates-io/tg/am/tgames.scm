(define-module (crates-io tg am tgames) #:use-module (crates-io))

(define-public crate-tgames-0.1.0 (c (n "tgames") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1i874qdnxqgh2s53dckbl81b14ldj3i9hy9k5ndfpzwkpc05504k")))

(define-public crate-tgames-0.1.1 (c (n "tgames") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "17yn7byjxidgz9r3jjhinx5dcgsyvcrz1c66jq5ji2kzi5xw525f")))

(define-public crate-tgames-0.1.2 (c (n "tgames") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1x5pgxsk1m2rlx4v0s1j45ibp7sdm1kn5aaxi2d24wgzz1libhm1")))

(define-public crate-tgames-0.2.0 (c (n "tgames") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "0wz9ghrh9zcij6awr9axnsdj20nhbjrf0nkx3w2ldxxwbgm2ywk8")))

(define-public crate-tgames-0.2.1 (c (n "tgames") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "0s6y9lcm28g040im3wpza07rl318in7pnn1qjdw0km496hx0fpxs")))

(define-public crate-tgames-0.3.0 (c (n "tgames") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "05xx5zhvy4q3z0vqnykllix8j293s3n507h7wmmq2bi34n9nxafa")))

(define-public crate-tgames-0.3.1 (c (n "tgames") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "1zapqmnd9zp7cl8r2sq3bq4ns6wjzpfbfil51g7cxcp7wmvhk4lp")))

(define-public crate-tgames-0.3.2 (c (n "tgames") (v "0.3.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.1") (d #t) (k 0)))) (h "0agi6y4cbbzqk74ch824aq68f1yninhh80sa4blmbv591vg966dy")))

