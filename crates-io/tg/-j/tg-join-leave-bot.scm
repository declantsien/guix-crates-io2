(define-module (crates-io tg -j tg-join-leave-bot) #:use-module (crates-io))

(define-public crate-tg-join-leave-bot-0.1.0 (c (n "tg-join-leave-bot") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "pw-telegram-bot-fork") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0vf9lpfpphhz4ahph9k8q417ibg005ykvbd5mmwj15j5ivcbjckz")))

