(define-module (crates-io tg op tgopher) #:use-module (crates-io))

(define-public crate-tgopher-0.1.0 (c (n "tgopher") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1gqka0wx50pa099hmxhqz9046jan1hdqxkja05hckpkwv7g313nz") (y #t)))

(define-public crate-tgopher-0.1.1 (c (n "tgopher") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1wnq7jxbfgx5rpj40mm2fb5hqkd8y0zjy8xmzpy1c8zsmqsci298")))

