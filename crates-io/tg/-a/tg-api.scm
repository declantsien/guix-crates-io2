(define-module (crates-io tg -a tg-api) #:use-module (crates-io))

(define-public crate-tg-api-0.2.0 (c (n "tg-api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)))) (h "0jqzbv8skpkn5sv7lszmg67kc7v6ravdskh5gkxx92a7n0y0r4jy")))

(define-public crate-tg-api-0.2.1 (c (n "tg-api") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)))) (h "0aprc8prq78knk8k1fmf3vc6b6gi5h614750izv6vfhw9jgha1s7")))

(define-public crate-tg-api-0.3.0 (c (n "tg-api") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0wv8rnzrn1whfgsmp4fpqkblmixdb35r3h1mz2ly1g700v5x08ar")))

(define-public crate-tg-api-0.3.1 (c (n "tg-api") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0hx328np1kbsbw9j6pvkhfavfvl15z23i0vh4ckl94dxvnfq39m1")))

(define-public crate-tg-api-0.3.2 (c (n "tg-api") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1il6vnwgd6nqcqlfqwg6x9c8zawnwg4bd1i8zs39sh734q3hhymv")))

(define-public crate-tg-api-0.4.0 (c (n "tg-api") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1fi70vakcgzxrb5mjrk3n7hxfwg840cqgdm8d4pr0cga55f5fz3n")))

(define-public crate-tg-api-0.4.1 (c (n "tg-api") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "15j1l25dxfm6ywy45wvm8m9xz7dh7bzlkgfmlrh8is7fsl90smzk")))

(define-public crate-tg-api-0.4.2 (c (n "tg-api") (v "0.4.2") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1cqgqn1aannzsfhmppavcv2knyiyphwds8w39jimxv026wwmp6b2")))

(define-public crate-tg-api-0.4.3 (c (n "tg-api") (v "0.4.3") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0p101sba34d9gdz1cbbchnqwhdh7rij25y4d4576k6jrachd3g74")))

