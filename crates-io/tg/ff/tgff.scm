(define-module (crates-io tg ff tgff) #:use-module (crates-io))

(define-public crate-tgff-0.0.1 (c (n "tgff") (v "0.0.1") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "0pa7ymsmyqnybybbykbnhg6bjmf83ia1rri34kgi91jxp60gy1n0")))

(define-public crate-tgff-0.0.2 (c (n "tgff") (v "0.0.2") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 2)))) (h "0kf8hbqlixs07vfmr5ipwa37hzxx7wgxg76gnbd6y76j95skizkn")))

(define-public crate-tgff-0.0.3 (c (n "tgff") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 2)))) (h "1jjpka04b9n9csw7338izcb85phzvwp62h12fjaidrl87rg208a1")))

(define-public crate-tgff-0.0.5 (c (n "tgff") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)))) (h "1bilhm5xx579pn33zs7fiq4x1a75ck19vnm6xwjk061w1r6dxjc2")))

(define-public crate-tgff-0.0.6 (c (n "tgff") (v "0.0.6") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)))) (h "032qvlql5lv5zji77zr7655b272l85vgc8y7qzvywkl8va47dh3z")))

(define-public crate-tgff-0.0.7 (c (n "tgff") (v "0.0.7") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)))) (h "1z9x6acpxjpi339g659c2z6j9isxjlhbjvkfmlksc7rpxxfaz4f9")))

(define-public crate-tgff-0.0.8 (c (n "tgff") (v "0.0.8") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)))) (h "19adrgm014xh61n84gjmzk7app01066j51yvm4g4n1y2ri1d2922")))

(define-public crate-tgff-0.0.9 (c (n "tgff") (v "0.0.9") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)))) (h "0c4k5vgchnsw6vz4v0rkbig4baravzpam0ymk09a1cqjgbvd8ayj")))

(define-public crate-tgff-0.1.0 (c (n "tgff") (v "0.1.0") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1yhsaydsb5vqlg9aafpk9ini58wyji7jfx6xklg8cjydas7z9nxr")))

(define-public crate-tgff-0.1.1 (c (n "tgff") (v "0.1.1") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1rfpjgpc21mg91b9sbpgcg4qa83xhy654l4m81zklpdlmgfc3480")))

(define-public crate-tgff-0.1.2 (c (n "tgff") (v "0.1.2") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "15ijfxrs5czwf565p56gdzwag8b1b7ka9r94r96dq40944sjwrwf")))

(define-public crate-tgff-0.1.3 (c (n "tgff") (v "0.1.3") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "0yw2gcx92md8ilrraxsrv1hl7jn2dlpa5wapl0rhrd6rmpiw8vq3")))

(define-public crate-tgff-0.1.4 (c (n "tgff") (v "0.1.4") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1hbax5wzid7ivjjbldsx3c2v9b3mjhyrhh54b2knn8zn8vkl2zqi")))

(define-public crate-tgff-0.1.5 (c (n "tgff") (v "0.1.5") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1qx902pmdn3v1p6l6h46v1jp30l9gqaf1jnsd18v7kdim8lykjap")))

(define-public crate-tgff-0.1.6 (c (n "tgff") (v "0.1.6") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1jwmzhv6xgr4s871j6h7lysb2df62isnlrwi0rdbv8sv6sp5fj02")))

(define-public crate-tgff-0.1.7 (c (n "tgff") (v "0.1.7") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1hn8gm4kx3dgrwis7lkcail62v10jpmfp440150ja62dsrzfpgfx")))

(define-public crate-tgff-0.1.8 (c (n "tgff") (v "0.1.8") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "0055akv8xvslsdy4wqlhpnng4i8wmjzkcq9fghypg6j929baq7qm")))

(define-public crate-tgff-0.1.9 (c (n "tgff") (v "0.1.9") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0c60jd0fpnsrmx2jwag21cbhnzsyvw7ck9kx4rjrwybyks8pibvk")))

(define-public crate-tgff-0.1.10 (c (n "tgff") (v "0.1.10") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "1p5z17jlzhbw92h45pzi059sg5f35c9i0s8rqw3bcvv886m68cn3")))

