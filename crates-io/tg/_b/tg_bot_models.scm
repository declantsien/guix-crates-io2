(define-module (crates-io tg _b tg_bot_models) #:use-module (crates-io))

(define-public crate-tg_bot_models-0.1.0 (c (n "tg_bot_models") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11s52mqwbbj3gfpifkfc661gjmdyvwczy51phv7yvbd9k6ac62am")))

(define-public crate-tg_bot_models-0.2.0 (c (n "tg_bot_models") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rmvy85ppd59fm98ifhfyy5i6fai74b3m9y3vy52zfl7a0k9ks65")))

(define-public crate-tg_bot_models-0.3.0 (c (n "tg_bot_models") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vnd19r08w9xazmzbfckzrcpzn1haq6llisfadgpf4f12057322z")))

(define-public crate-tg_bot_models-0.4.0 (c (n "tg_bot_models") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dp9v4jh31mfxx4d496mvi2ynh70zc1vmdc7zmh8a73d5605f3dq")))

(define-public crate-tg_bot_models-0.5.0 (c (n "tg_bot_models") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lxwasxyasgd38ihzpyanx60754l4q8r855r73p9kh6ylcnnkji9")))

