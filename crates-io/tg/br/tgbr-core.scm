(define-module (crates-io tg br tgbr-core) #:use-module (crates-io))

(define-public crate-tgbr-core-0.1.0 (c (n "tgbr-core") (v "0.1.0") (d (list (d (n "ambassador") (r "^0.2.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0daww4jc22a7g2jq7caclqrrkxvr9vrcaiyrr2nql1lwm9n2qza7")))

