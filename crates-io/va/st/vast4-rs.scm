(define-module (crates-io va st vast4-rs) #:use-module (crates-io))

(define-public crate-vast4-rs-1.0.0 (c (n "vast4-rs") (v "1.0.0") (d (list (d (n "hard-xml") (r "^1.21") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)))) (h "1aq5m8slwai2rfqfkga3llm5a1c3f8c63r4y8y3d1jhcfj331r8j")))

(define-public crate-vast4-rs-1.0.1 (c (n "vast4-rs") (v "1.0.1") (d (list (d (n "hard-xml") (r "^1.23.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)))) (h "0km8rmdar4szbysk4gza3yndh43ari6g2hx3y9ajdrns9v5b7chf")))

(define-public crate-vast4-rs-1.0.2 (c (n "vast4-rs") (v "1.0.2") (d (list (d (n "hard-xml") (r "^1.23.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)))) (h "1f22j5ahh29rrjdi94hacqdai8vwn6dlj6vgvyy1lkwcpmppd8fj")))

(define-public crate-vast4-rs-1.0.3 (c (n "vast4-rs") (v "1.0.3") (d (list (d (n "hard-xml") (r "^1.23.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)))) (h "16xy7bfgd8bwypmlkh5cch4qb7vniaf2hzij306agnfn0s3hkzdc")))

(define-public crate-vast4-rs-1.0.4 (c (n "vast4-rs") (v "1.0.4") (d (list (d (n "hard-xml") (r "^1.23.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)))) (h "0kqq66m4ib88krrsxfidp775gkyb1smsbxf2g5q5bwlqv0zqqii7")))

(define-public crate-vast4-rs-1.0.6 (c (n "vast4-rs") (v "1.0.6") (d (list (d (n "hard-xml") (r "^1.23.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)))) (h "1s1n9wdvlc7x91plldcaqf016v08gbdmrs9k40w5hl7yavcn0krm")))

