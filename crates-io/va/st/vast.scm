(define-module (crates-io va st vast) #:use-module (crates-io))

(define-public crate-vast-0.1.0 (c (n "vast") (v "0.1.0") (d (list (d (n "pretty") (r "^0.10.0") (d #t) (k 0)))) (h "1aq44byqycgkgdmw94d2mvz0q72h6m36sx8d4xfbijzdy41iq04b")))

(define-public crate-vast-0.2.0 (c (n "vast") (v "0.2.0") (d (list (d (n "pretty") (r "^0.10.0") (d #t) (k 0)))) (h "197i4d16rzgj315kgp3xk2vqcil9vf63bw549kz2vybpx0qd1bac")))

(define-public crate-vast-0.2.1 (c (n "vast") (v "0.2.1") (d (list (d (n "pretty") (r "^0.10.0") (d #t) (k 0)))) (h "0gzvr9dkqfg70ic2g0l47awpp8xad35q3v9kp4p01r3wlq34m4ai")))

(define-public crate-vast-0.2.2 (c (n "vast") (v "0.2.2") (d (list (d (n "pretty") (r "^0.10.0") (d #t) (k 0)))) (h "110r5w4415qga71pxikhms5qmcp4c0llxhph28c8l89mh8b46xhh")))

(define-public crate-vast-0.3.0 (c (n "vast") (v "0.3.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)))) (h "0fr0avaqlxsxlrs7ryxgg1xm050l0i0ynxfff3kf6238bw0zzwy1")))

(define-public crate-vast-0.3.1 (c (n "vast") (v "0.3.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)))) (h "1z4n9d1nya0745bgq9a7572mmw8j51n4brvklcivfhz37vgnl0pk")))

(define-public crate-vast-0.3.2 (c (n "vast") (v "0.3.2") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pretty") (r "^0.10") (d #t) (k 0)))) (h "0z7prdkjd7zxgpzy01rfckw564aw4ya1qwclqjs8lgwpdpxwldmd")))

(define-public crate-vast-0.3.3 (c (n "vast") (v "0.3.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty") (r "^0.11") (d #t) (k 0)))) (h "0clfw0cck4n0pl8cgixr8giw0iyvzvdn69gl9ihypibi1vns9nhs")))

