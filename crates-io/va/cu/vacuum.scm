(define-module (crates-io va cu vacuum) #:use-module (crates-io))

(define-public crate-vacuum-0.1.0 (c (n "vacuum") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "structopt") (r "^0.2.12") (d #t) (k 0)) (d (n "yansi") (r "^0.4.0") (d #t) (k 0)))) (h "1qks75w327f5536himzbwab3ncwnlrfqkww5ar1lgz4fagd7na3n")))

(define-public crate-vacuum-0.1.1 (c (n "vacuum") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "structopt") (r "^0.2.12") (d #t) (k 0)) (d (n "yansi") (r "^0.4.0") (d #t) (k 0)))) (h "1ihx69fwlx994raza1fzc3gx8r6gw4ghs8mlnrsq0dvkgk9zb36s")))

(define-public crate-vacuum-0.1.2 (c (n "vacuum") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "ureq") (r "^1.3.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1wbfkvm63798b9j895zapijpgixwwpfwqd9d31cw620i07py2bvs")))

