(define-module (crates-io va rw varweeks_millis) #:use-module (crates-io))

(define-public crate-varweeks_millis-1.0.15 (c (n "varweeks_millis") (v "1.0.15") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "041gkdysv3h0hl9qygqs6fx09yfdqf400wyhwkqc5f20n67id3jv")))

(define-public crate-varweeks_millis-1.0.19 (c (n "varweeks_millis") (v "1.0.19") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0kxmdzlazqni4qwgjr9mi5nback9f48zagpklasrxv855psz7cbm")))

