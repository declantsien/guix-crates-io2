(define-module (crates-io va pk vapkey-cli) #:use-module (crates-io))

(define-public crate-vapkey-cli-0.1.0 (c (n "vapkey-cli") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "panic-hook") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-crypto") (r "^0.4.2") (f (quote ("publickey"))) (d #t) (k 0)) (d (n "tetsy-wordlist") (r "^1.3.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)) (d (n "vapkey") (r "^0.4.0") (d #t) (k 0)))) (h "0388f5fsxhlljcp75ir3kj5qhkclcgwbxf0yg34drgjfvbqbysj7")))

