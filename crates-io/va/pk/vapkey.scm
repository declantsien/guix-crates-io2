(define-module (crates-io va pk vapkey) #:use-module (crates-io))

(define-public crate-vapkey-0.4.0 (c (n "vapkey") (v "0.4.0") (d (list (d (n "edit-distance") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-crypto") (r "^0.4.2") (f (quote ("publickey"))) (d #t) (k 0)) (d (n "tetsy-wordlist") (r "^1.3.1") (d #t) (k 0)))) (h "1ximzzbi7l0ypjmnxwg5hrxa2wzi4nf3d2snqy1wl01vw4183hnk")))

