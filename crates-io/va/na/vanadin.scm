(define-module (crates-io va na vanadin) #:use-module (crates-io))

(define-public crate-vanadin-0.1.0 (c (n "vanadin") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("string"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (f (quote ("parse"))) (k 0)) (d (n "vanadin-tasks") (r "^0.1.0") (d #t) (k 0)))) (h "0l429qvmcibgdz1qjh8lyhnqvz6clh05z34zbrw5s76jbcxndlvi")))

