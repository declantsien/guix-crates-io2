(define-module (crates-io va x- vax-number) #:use-module (crates-io))

(define-public crate-vax-number-1.0.0 (c (n "vax-number") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1l5alv3ryd30i7dr9zvcmbs4h2bz6sy96bfa1hnar68dxcqslfd7")))

