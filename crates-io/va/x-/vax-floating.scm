(define-module (crates-io va x- vax-floating) #:use-module (crates-io))

(define-public crate-vax-floating-0.1.0 (c (n "vax-floating") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "forward_ref") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ya5f50aimwrfv1n7p8c684k8zbrm8gi2mdb8045vs12l4kxc8ld") (s 2) (e (quote (("proptest" "bitflags" "dep:proptest" "rand")))) (r "1.67")))

