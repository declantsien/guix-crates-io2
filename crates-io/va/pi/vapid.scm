(define-module (crates-io va pi vapid) #:use-module (crates-io))

(define-public crate-vapid-0.6.0 (c (n "vapid") (v "0.6.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0rkhmjnnvhvq2ydidw4nwvp5xzx9i67lpmfkqla77fa9hhwiw5mn")))

