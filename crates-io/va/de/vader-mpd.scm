(define-module (crates-io va de vader-mpd) #:use-module (crates-io))

(define-public crate-vader-mpd-0.1.0 (c (n "vader-mpd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "audiotags") (r "^0.4.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("crossterm"))) (k 0)))) (h "1dlg8iy2yrcfj8svyxhsxag4jcvnv8fm78dbr27w09mgm1i4ydqh")))

