(define-module (crates-io va de vadeen_osm) #:use-module (crates-io))

(define-public crate-vadeen_osm-0.1.0 (c (n "vadeen_osm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "0qgqp6lqi0dkyg4ai87x88jvp65b7ivc0w1zdlr7aq181arahj5g")))

(define-public crate-vadeen_osm-0.1.1 (c (n "vadeen_osm") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "1vzxhpmq9dpj2if2kivxvhvgpdzarvfhlpr2pf1vcj7rxk9a4h3j")))

(define-public crate-vadeen_osm-0.1.2 (c (n "vadeen_osm") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "004pw0jrwixmwcb9m8vx5l71x871hs0rbv9n852m27qm4jbyv3p9")))

(define-public crate-vadeen_osm-0.1.3 (c (n "vadeen_osm") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "0dmg12vqc9wf2k7bki96fdi60abjzg2aldchr65vzl62jjlf0wql")))

(define-public crate-vadeen_osm-0.2.0 (c (n "vadeen_osm") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "1ygjp9gy6i4l6hnndpmmxkhg363rw3qcxki75caf2vkza7m34v7f")))

(define-public crate-vadeen_osm-0.2.1 (c (n "vadeen_osm") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "0171qlknzjl6fdk1f4zq0h99znyc63ja8imrjm3s6j9jns9wkja7")))

(define-public crate-vadeen_osm-0.3.0 (c (n "vadeen_osm") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "1fv1b774svy5n5cfiy44jxd9cy17731pyy9mjg1yrw3lmk4ddvin")))

(define-public crate-vadeen_osm-0.4.1 (c (n "vadeen_osm") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)))) (h "1hbn3zwiqqx02rfi81lwzfls0hd276im32wzg5jxhrpw7szc24nb")))

(define-public crate-vadeen_osm-0.4.2 (c (n "vadeen_osm") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20") (d #t) (k 0)))) (h "18h61i9nikbxlmbjq2yianssqx1gq7vp387h538m478h5575v3ks")))

