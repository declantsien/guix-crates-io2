(define-module (crates-io va de vadersay) #:use-module (crates-io))

(define-public crate-vadersay-0.6.2 (c (n "vadersay") (v "0.6.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0008q657gy8n9vssk9aa90553mn5dzac11lfzkxkj5cnn6kyr5nc")))

(define-public crate-vadersay-0.6.3 (c (n "vadersay") (v "0.6.3") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "092lgl1g27fxdilcgk0hanfir35mxfd9xxq9k9brwncl3cdik7qg")))

(define-public crate-vadersay-0.6.4 (c (n "vadersay") (v "0.6.4") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "019yq706yyzdzzik2z3dm69nvmhvczw6hd32ixfmgqbfq9kmfbpv")))

