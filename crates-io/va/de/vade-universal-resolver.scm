(define-module (crates-io va de vade-universal-resolver) #:use-module (crates-io))

(define-public crate-vade-universal-resolver-0.0.4 (c (n "vade-universal-resolver") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.31") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "=1.7.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "vade") (r "^0.1.0") (d #t) (k 0)))) (h "0a1g0qjcnfaabjp0m5b97lwpgh0y3bqcj604zmc1kpyha09q2f7z") (f (quote (("sdk"))))))

