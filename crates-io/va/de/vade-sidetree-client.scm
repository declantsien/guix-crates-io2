(define-module (crates-io va de vade-sidetree-client) #:use-module (crates-io))

(define-public crate-vade-sidetree-client-0.1.0 (c (n "vade-sidetree-client") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.3") (d #t) (k 0)) (d (n "multihash") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_jcs") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std" "float_roundtrip"))) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "1inxqc2fa9l80isz4f7f4d1p5rpk1gkwsfa39xibx46is2x08c21")))

