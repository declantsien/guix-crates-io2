(define-module (crates-io va de vader_sentiment) #:use-module (crates-io))

(define-public crate-vader_sentiment-0.1.0 (c (n "vader_sentiment") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "1x2vv0fnn88mbfimhgy7apm2n6dkzw6zvv3qdsx1zjcybpl8azhw")))

(define-public crate-vader_sentiment-0.1.1 (c (n "vader_sentiment") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "04piryjjy0nysbjr9l2p2akwsigj5wb1snx0mxi3vxgspdjznybz")))

