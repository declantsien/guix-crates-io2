(define-module (crates-io va lu valued-enums) #:use-module (crates-io))

(define-public crate-valued-enums-1.0.0 (c (n "valued-enums") (v "1.0.0") (h "14vwa1g1gx4maqzmyk15034sggq5wlnlp6q5difqfb81z6szip0w")))

(define-public crate-valued-enums-1.0.1 (c (n "valued-enums") (v "1.0.1") (h "09s8z9dbyjmqya69abg4gm3lil06sfksmj13dm0h1h3b5jka91rc")))

(define-public crate-valued-enums-1.0.2 (c (n "valued-enums") (v "1.0.2") (h "1n0rrll4aqq1rd0ldjx80dg8nllwn9bvv1nkfls7x7j360i9hqx4")))

(define-public crate-valued-enums-1.1.3 (c (n "valued-enums") (v "1.1.3") (h "0zl64klvnh8r45grg9hl4774ik431v6zk8wpl19p1chj9ghg313b")))

(define-public crate-valued-enums-1.1.4 (c (n "valued-enums") (v "1.1.4") (h "08fckrmxzk2pdb4ybkh02lbqrd6q2zw796ykir02gz05c8pbi9q6")))

(define-public crate-valued-enums-1.1.5 (c (n "valued-enums") (v "1.1.5") (h "07ba5ph1hba1yywaffbn7s0c3xpyhya358iq4grlvrx808mvryi4")))

(define-public crate-valued-enums-1.1.6 (c (n "valued-enums") (v "1.1.6") (h "13dv7wixx0jan0k7lcbqhvxynbdxnqylqcn8shbq6mvl7ync5b2c")))

(define-public crate-valued-enums-1.1.7 (c (n "valued-enums") (v "1.1.7") (h "09bkvhhzq241yks3h91ymxgmbsp1db0mlrsbc8a8s1vbx6lvx49k")))

(define-public crate-valued-enums-1.1.8 (c (n "valued-enums") (v "1.1.8") (h "00y02v8jxhd9nvf0zmjs16hx6mz8qsyvf2q5g0fapvy0lq9sv2vb")))

(define-public crate-valued-enums-1.1.9 (c (n "valued-enums") (v "1.1.9") (h "0hrhdqqvzxp5nj69l1y90qlx2b64hamfay7xz975hly77sjp079g")))

