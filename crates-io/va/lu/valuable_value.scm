(define-module (crates-io va lu valuable_value) #:use-module (crates-io))

(define-public crate-valuable_value-1.0.0 (c (n "valuable_value") (v "1.0.0") (d (list (d (n "arbitrary") (r "^1.0.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "atm_parser_helper") (r "^1.0.0") (d #t) (k 0)) (d (n "atm_parser_helper_common_syntax") (r "^2.0.0") (f (quote ("arbitrary"))) (d #t) (k 0)) (d (n "itoa") (r "^1.0.1") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strtod2") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r8jm105fv0z41g2hn4dhicam42rgj5rndpv94ciy4g32dfnfy1j")))

