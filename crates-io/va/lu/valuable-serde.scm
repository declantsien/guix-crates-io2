(define-module (crates-io va lu valuable-serde) #:use-module (crates-io))

(define-public crate-valuable-serde-0.1.0 (c (n "valuable-serde") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.103") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "valuable") (r "^0.1") (k 0)) (d (n "valuable") (r "^0.1") (f (quote ("derive"))) (d #t) (k 2)))) (h "01q9ifpd1mk1ic2g8lagp35djzb8i7cm8skk4rkf5ayd63zwz1aj") (f (quote (("std" "alloc" "valuable/std" "serde/std") ("default" "std") ("alloc" "valuable/alloc" "serde/alloc")))) (r "1.51.0")))

