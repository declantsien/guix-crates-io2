(define-module (crates-io va lu value-enum) #:use-module (crates-io))

(define-public crate-value-enum-0.5.1 (c (n "value-enum") (v "0.5.1") (h "1lc56c63937h46mqahr2csk5bayi2dgqiq2zxz90p5fdvn8dmxfz")))

(define-public crate-value-enum-0.5.2 (c (n "value-enum") (v "0.5.2") (h "0ax64pa492a45jchz58j2gz9yzmzan7lsy35qp267d2wvh1s4l3i")))

(define-public crate-value-enum-0.6.0 (c (n "value-enum") (v "0.6.0") (h "0vsd9ah48hcykdc2ifj6168bjvxn4pn2yszqdrgx770fz87ysqz7")))

(define-public crate-value-enum-1.0.0 (c (n "value-enum") (v "1.0.0") (h "13rarm5rfjdpk9na3sryly7d0as59b7z49irxj0py33z251nyvcm")))

