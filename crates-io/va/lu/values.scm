(define-module (crates-io va lu values) #:use-module (crates-io))

(define-public crate-values-0.1.0 (c (n "values") (v "0.1.0") (h "198igzsi42ida3fydl70fgkvnnqdblqhlan68szq6n1qs0rikd5k")))

(define-public crate-values-0.1.1 (c (n "values") (v "0.1.1") (h "1y9kp375vsn1g3dnmf27l6xc7cg49lbfzi8jdpvnls5z4jj4sifr")))

(define-public crate-values-0.1.2 (c (n "values") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.46") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.19") (d #t) (k 0)))) (h "114qqrqcds06yhlpdyk0av29rvwq3mir0yyczmldp2jgr75pd5sk")))

(define-public crate-values-0.1.3 (c (n "values") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3.46") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.19") (d #t) (k 0)))) (h "063rlkb4gixxznpd5ns2fas0fczj6c3y9h1si6nryf9i1yxzpp7a")))

(define-public crate-values-0.1.4 (c (n "values") (v "0.1.4") (d (list (d (n "js-sys") (r "^0.3.46") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.19") (d #t) (k 0)))) (h "15rhzxwdcw777hjhpd3gyiqa4162b34bvjm7k7f7d3w17f2k39cy")))

(define-public crate-values-0.1.5 (c (n "values") (v "0.1.5") (d (list (d (n "js-sys") (r "^0.3.46") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.19") (d #t) (k 0)))) (h "0hywwnv3frizb03gp5nxxawd6kpm6if48855j9d8gkk0zymhy1yv")))

(define-public crate-values-0.1.6 (c (n "values") (v "0.1.6") (d (list (d (n "js-sys") (r "^0.3.46") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.69") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.19") (d #t) (k 0)))) (h "1mq86qrqgybh3wqwd8srmdm0djndnxfps8g6x26zcbxxlwj7xjwm")))

