(define-module (crates-io va lu valuable) #:use-module (crates-io))

(define-public crate-valuable-0.0.0 (c (n "valuable") (v "0.0.0") (h "0gnan3fymwcp4x2i63xpcszrw5qh76m0a68n7frv9pcm1c92yyra")))

(define-public crate-valuable-0.1.0 (c (n "valuable") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "valuable-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0v9gp3nkjbl30z0fd56d8mx7w1csk86wwjhfjhr400wh9mfpw2w3") (f (quote (("std" "alloc") ("derive" "valuable-derive") ("default" "std") ("alloc")))) (r "1.51.0")))

