(define-module (crates-io va lu value-box) #:use-module (crates-io))

(define-public crate-value-box-1.0.0 (c (n "value-box") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0w5hpx5ss06flw5l7bx6ln6raxnw5228c827czqbbr37sklmp9kb")))

(define-public crate-value-box-1.0.1 (c (n "value-box") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0v6jgr5yr8274g5iacif8z8ymlg8w8cgl7jrpdp58aqc6q78fxwz")))

(define-public crate-value-box-1.1.0 (c (n "value-box") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "1qij5r93w13523w38kayvixwdd84y24917v77a7p3r84c829xkk4")))

(define-public crate-value-box-1.1.1 (c (n "value-box") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0i8b7plg38n4jxh42kcacqafml82qkw2gmpxrv4xykipi9zqzh5w")))

(define-public crate-value-box-1.1.2 (c (n "value-box") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "192nqs6d02qh2rl0l7c0y19v8w6lwk17rq827cghvmd1dz40qdhi")))

(define-public crate-value-box-1.1.3 (c (n "value-box") (v "1.1.3") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0ab5gxgs6k97yyamkr1apms9wn3g2spypskpyxmzcrbhfndh5dsk")))

(define-public crate-value-box-2.0.0 (c (n "value-box") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0hkvda7lkj6ab8ghh60d83yj1xg292408spgbmqmm0wzzd1q888b") (f (quote (("default"))))))

(define-public crate-value-box-2.1.0 (c (n "value-box") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "1v3c777d60jigfhxrsl4jybi8r05d3x1nwk15q2x4ild2fb0gmkv") (f (quote (("default"))))))

(define-public crate-value-box-2.1.1 (c (n "value-box") (v "2.1.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "1nx3mj36i8wj4fwkhspkngrgpr9cq7s4ikv5c6hplm9hljd5aq1q") (f (quote (("default"))))))

(define-public crate-value-box-2.2.0 (c (n "value-box") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0asfycilrybkrhpq9iwxdmqq3a5rpgfzqgwvm7ab91154wil4a1y") (f (quote (("default"))))))

(define-public crate-value-box-2.2.1 (c (n "value-box") (v "2.2.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "1q93h47ljqqfkb488ghgf7917jnjqhs3m1yr4kxwxyf9dya69g79") (f (quote (("default"))))))

(define-public crate-value-box-2.2.2 (c (n "value-box") (v "2.2.2") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "1cym6rzjwsd7rms4kr9wrrsfixmp5ab1hnmb07fdg6934hj0yww6") (f (quote (("default"))))))

(define-public crate-value-box-2.2.3 (c (n "value-box") (v "2.2.3") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0i8ka3n9nkjcqfv45wldshd51z85q7jsj8v94qyfy398xhnl96hl") (f (quote (("default"))))))

(define-public crate-value-box-2.2.4 (c (n "value-box") (v "2.2.4") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "1r1vc8yqwr24m6sgqsy1c5q7mvk17647xmqpjclzw9ygif7gq3dq") (f (quote (("default")))) (y #t)))

(define-public crate-value-box-2.3.0 (c (n "value-box") (v "2.3.0") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0r2zfpqwnh1gqr60q9j51iy9jmjsf7kmir8mv8dvd0l3ffz7rllm") (f (quote (("default"))))))

(define-public crate-value-box-2.3.1 (c (n "value-box") (v "2.3.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0rij86s2xdk5d8jzdvg681m03kkjkxrsb6agcwcq3mmhsgy81lnc") (f (quote (("default"))))))

(define-public crate-value-box-2.3.2 (c (n "value-box") (v "2.3.2") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "0px8qp2k5ybm4s19vwlrk2afqrxfl2b705wlv8avmgh711gp60d7") (f (quote (("default"))))))

(define-public crate-value-box-2.3.3 (c (n "value-box") (v "2.3.3") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phlow") (r "^1") (o #t) (d #t) (k 0)) (d (n "phlow-extensions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "user-error") (r "^1.2") (d #t) (k 0)))) (h "1p40glhpapi656f28bcddb776xbv52qbkjl9yy18p9zhv5p7ciks") (f (quote (("default"))))))

