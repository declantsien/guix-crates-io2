(define-module (crates-io va lu value_from_type_macros) #:use-module (crates-io))

(define-public crate-value_from_type_macros-1.0.0 (c (n "value_from_type_macros") (v "1.0.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)) (d (n "value_from_type_traits") (r "^1.0.0") (d #t) (k 2)))) (h "1bva67bl7mjk69ddriq8bw304lyglzajr6vylgkim1n61rdbr297")))

(define-public crate-value_from_type_macros-1.0.1 (c (n "value_from_type_macros") (v "1.0.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)) (d (n "value_from_type_traits") (r "^1.0.0") (d #t) (k 2)))) (h "11ls6dwp707n9xs5sdc1ch1753ashj6g0x0j929364ral4qb3x3p")))

(define-public crate-value_from_type_macros-1.0.2 (c (n "value_from_type_macros") (v "1.0.2") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)) (d (n "value_from_type_traits") (r "^1.0.0") (d #t) (k 2)))) (h "1d36xbidafx3v2gqryi9mvphqyx1qv54qvn60bvpg9896v68qkpy")))

(define-public crate-value_from_type_macros-1.0.3 (c (n "value_from_type_macros") (v "1.0.3") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)) (d (n "value_from_type_traits") (r "^1.0.0") (d #t) (k 2)))) (h "1klzbfzj4vg4fmc4ssqry8pi9j6l7x99xbr7snii5gwwd2163ya2")))

(define-public crate-value_from_type_macros-1.0.4 (c (n "value_from_type_macros") (v "1.0.4") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^0.13.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "value_from_type_traits") (r "^1.0.0") (d #t) (k 2)))) (h "0cvsv71j43n19kg65qj2285xbvd9qxnxkc90285vvvhzpjhhr1fx")))

(define-public crate-value_from_type_macros-1.0.5 (c (n "value_from_type_macros") (v "1.0.5") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^0.13.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "value_from_type_traits") (r "^1.0.0") (d #t) (k 2)))) (h "0p2dwkwlvxgf5bhqzzf3271hdslqwjf9nmj7z9chp4x5ibb0wrb3")))

(define-public crate-value_from_type_macros-1.0.6 (c (n "value_from_type_macros") (v "1.0.6") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^0.13.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "value_from_type_traits") (r "^1.0.0") (d #t) (k 2)))) (h "1lp3si9g933cra9cn7j99pn8c42ks6r8zq3gci5yqrf1ycbd3gqh")))

