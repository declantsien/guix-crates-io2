(define-module (crates-io va lu value_from_type_traits) #:use-module (crates-io))

(define-public crate-value_from_type_traits-1.0.0 (c (n "value_from_type_traits") (v "1.0.0") (h "1q50n0zq2a8wsdx9ykqg0315j8fj7y71kmyls3np5szlwrgf5brn")))

(define-public crate-value_from_type_traits-1.0.1 (c (n "value_from_type_traits") (v "1.0.1") (h "0z4dn5jlyypa26hy79lf72k0d622hsq5m8b1lsm3ia8hc4k8wway")))

(define-public crate-value_from_type_traits-1.0.2 (c (n "value_from_type_traits") (v "1.0.2") (h "0ymr3dnic9samifimw9q73bq9hp7gvpigm5bjr8qq2g77sdqvlbv")))

