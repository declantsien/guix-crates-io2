(define-module (crates-io va lu valu3-derive) #:use-module (crates-io))

(define-public crate-valu3-derive-0.1.4 (c (n "valu3-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "valu3") (r "^0.1") (d #t) (k 2)))) (h "1rv774ml7r9q3m3a03ma6jy21rmgpf1kmadgin30ywh7hxn4l2xv")))

(define-public crate-valu3-derive-0.1.5 (c (n "valu3-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "valu3") (r "^0.1") (d #t) (k 2)))) (h "1kmwlcr2wf7l0g9hnx77j74gkwxx9xwyddcb3iwf728asdpbbaza")))

(define-public crate-valu3-derive-0.1.6 (c (n "valu3-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "valu3") (r "^0.1") (d #t) (k 2)))) (h "07l5y3vjsdnpwfvmwxwsvsih7p623dc2zny3nll6w6ph4cfcilz1")))

(define-public crate-valu3-derive-0.1.7 (c (n "valu3-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "valu3") (r "^0.1") (d #t) (k 2)))) (h "04gpyva89f4znsmjg2f530yry33971gfw6ly5bgap2brcyzwfpv0")))

(define-public crate-valu3-derive-0.1.8 (c (n "valu3-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "valu3") (r "^0.1") (d #t) (k 2)))) (h "11c3bazsavmjf1fprmrbvgc4d6l8r8h25cdlbsr27xy4jr2sxxd7")))

(define-public crate-valu3-derive-0.2.0 (c (n "valu3-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1ikq90pqvwx3cxzg1j0gq6lq96809yg8k3k9jijvcz09vq25x0gs")))

(define-public crate-valu3-derive-0.3.8 (c (n "valu3-derive") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0wzax4zn1083k1ml7amgk1cymhwzs2dqqqrpjcp0qk7wm92h7jc7")))

(define-public crate-valu3-derive-0.3.9 (c (n "valu3-derive") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0j756xx0hhyc1gkl7yih75a69vymvasi95d9sxw8vnmqlglbssjh")))

(define-public crate-valu3-derive-0.3.10 (c (n "valu3-derive") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1gahs4a4j2vd96y9wx3swgr8gjyjmb6h9zp0ncwj4xbpndxxi7x4")))

(define-public crate-valu3-derive-0.3.11 (c (n "valu3-derive") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1bd125m1y4i7qvc7jwda7pnwwhwrb0bxmkq9hlplpr69naq2kc1a")))

(define-public crate-valu3-derive-0.3.12 (c (n "valu3-derive") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "08d7br432n8sp68vibvgax08wkxfb42x1l6mg1pvr36q6icg3y37")))

(define-public crate-valu3-derive-0.3.13 (c (n "valu3-derive") (v "0.3.13") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1i7ipprlfsc8zg63d4xc2idw8s90nl9a0f9z9j9b9s0vb40vfp89")))

(define-public crate-valu3-derive-0.3.14 (c (n "valu3-derive") (v "0.3.14") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixpkcw2px4ywanng3irw7g7pf7fcd38bpbmdga58c62sxdlm1l8")))

(define-public crate-valu3-derive-0.4.0 (c (n "valu3-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "03k3z0l934g6ndznpg64wbji84w2wnqjlxpby67hhblgvhsf3h4d")))

(define-public crate-valu3-derive-0.4.1 (c (n "valu3-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1q2cxviwzc6vvbba7qjd30kn0a7w69mmvm997bqm93v5r81h2i39")))

(define-public crate-valu3-derive-0.4.2 (c (n "valu3-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1kji6by98ms3rzl5psfavfnb597dv5pfazdck33v7nb7pqpyfcpf")))

(define-public crate-valu3-derive-0.4.3 (c (n "valu3-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "14ra1wbd1dhx4hdym8ymcrq1vn91ckr55vsc5k2d4930gdj1kg8y")))

(define-public crate-valu3-derive-0.4.4 (c (n "valu3-derive") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1lq3ky45z6ajfdx3hkfw9wq20w5xglf07f17ph3dbbchd6989xhz")))

(define-public crate-valu3-derive-0.4.5 (c (n "valu3-derive") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0pp8db9dbvc35hj06mmx6f2apssws6ax08qfx76rvnzq8y69x7ka")))

