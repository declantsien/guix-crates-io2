(define-module (crates-io va lu value_unit) #:use-module (crates-io))

(define-public crate-value_unit-0.1.0 (c (n "value_unit") (v "0.1.0") (h "1q1m1289gjrich6yvl0gwq8by9v6vhdd561wqnz024fr0vq5xx45")))

(define-public crate-value_unit-0.1.1 (c (n "value_unit") (v "0.1.1") (h "0c3rv413crlkmgkfpa92mvdxh72pd1m1r8wiiq1k673ig76vbm0b")))

(define-public crate-value_unit-0.1.2 (c (n "value_unit") (v "0.1.2") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1cmzssrniqk839cm2v15211yybvi6dyvn14jvjp40v05cjfkl4dh")))

(define-public crate-value_unit-0.1.3 (c (n "value_unit") (v "0.1.3") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0yziqm49mgym54lh20apy4xlhxsnx6wnpqiwmbz3bch0jrsh5qrx")))

