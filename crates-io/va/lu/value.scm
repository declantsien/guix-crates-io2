(define-module (crates-io va lu value) #:use-module (crates-io))

(define-public crate-value-0.1.0 (c (n "value") (v "0.1.0") (h "1ghf9bbpnvg84agkh2ziz85gflar7n4ch7iv571p8kb7h1h4fsnq") (f (quote (("std") ("default" "std"))))))

(define-public crate-value-0.1.1 (c (n "value") (v "0.1.1") (h "1r31jxzmjmxr8lrbzvdqixjxh519rjdshjf1mygmkar90yg5k7nl") (f (quote (("std") ("default" "std"))))))

