(define-module (crates-io va lu valu3-parquet) #:use-module (crates-io))

(define-public crate-valu3-parquet-0.4.3 (c (n "valu3-parquet") (v "0.4.3") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "parquet") (r "^50.0.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "valu3") (r "^0") (d #t) (k 0)))) (h "0awjgjbx0jk96vibnk717ysz7xsg3a0drchm0zfkddsqa2cmjvyq")))

(define-public crate-valu3-parquet-0.4.4 (c (n "valu3-parquet") (v "0.4.4") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "parquet") (r "^50.0.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "valu3") (r "^0") (d #t) (k 0)))) (h "0ysjn8m3j6ncq750yyk9nrbd57rwpjhvz89agg02h3zgn1ya8rjb")))

(define-public crate-valu3-parquet-0.4.5 (c (n "valu3-parquet") (v "0.4.5") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "parquet") (r "^50.0.0") (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "valu3") (r "^0") (d #t) (k 0)))) (h "0r6l3f093z9db52kr043p20c28bdy0zz29gkf4f238624pgcv34j")))

