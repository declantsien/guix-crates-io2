(define-module (crates-io va lu valuable-derive) #:use-module (crates-io))

(define-public crate-valuable-derive-0.1.0 (c (n "valuable-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cjvqljzsj891cjzlwv0ihrv4m0n5211a6pr6b7cz42ich66ji4x") (r "1.51.0")))

