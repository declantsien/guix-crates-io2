(define-module (crates-io va lu valued) #:use-module (crates-io))

(define-public crate-valued-0.1.0 (c (n "valued") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "giftwrap") (r "^0.2.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0rsv7wkplaycv04zv5cr91k8vi5g7wm39k05kv6mkz749nv514j3")))

(define-public crate-valued-0.1.1 (c (n "valued") (v "0.1.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "giftwrap") (r "^0.2.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0wf7shdl8hzc0h3y19yx01s8jji30im7m1a9il8hap1808s08bsv")))

