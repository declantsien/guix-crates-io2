(define-module (crates-io va lu valuetypes) #:use-module (crates-io))

(define-public crate-valuetypes-0.1.0 (c (n "valuetypes") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1jvqjpq7wzv1rzakizqspxmcw377yirkc6cv3vwz07vx4m8l55xz")))

(define-public crate-valuetypes-0.1.1 (c (n "valuetypes") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1f38fbjijra3dciyvcqkqzym2zfcci9yjpjl1qbk2c4kim8ga95h")))

