(define-module (crates-io va lu valuable_futures) #:use-module (crates-io))

(define-public crate-valuable_futures-0.1.0 (c (n "valuable_futures") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tk-easyloop") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.9") (d #t) (k 2)))) (h "0lgdcjp7yr83yjysznj5md8pfxgwif74f81zmfdadz66xqwp7s15")))

(define-public crate-valuable_futures-0.1.1 (c (n "valuable_futures") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tk-easyloop") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.9") (d #t) (k 2)))) (h "139sc7r41aik3cim3n70q3slmr6fv5qmp4619kpifn7a7ccd89ny")))

