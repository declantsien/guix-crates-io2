(define-module (crates-io va lu value_pool) #:use-module (crates-io))

(define-public crate-value_pool-0.1.0 (c (n "value_pool") (v "0.1.0") (h "1fzz9ykh8gaq2ssff9mcxsn8l5pzgmxf861a3sapqhi3rgcascdw") (f (quote (("unsafe") ("default"))))))

(define-public crate-value_pool-0.2.0 (c (n "value_pool") (v "0.2.0") (d (list (d (n "nonmax") (r "^0.5.5") (d #t) (k 0)))) (h "01dqgxgspfr5c61p4r4dnwq4n4dx4zpdkyiy1bbg75p196d7cckn") (f (quote (("unsafe") ("default"))))))

