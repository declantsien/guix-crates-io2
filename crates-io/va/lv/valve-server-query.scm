(define-module (crates-io va lv valve-server-query) #:use-module (crates-io))

(define-public crate-valve-server-query-0.3.1 (c (n "valve-server-query") (v "0.3.1") (h "09i4hck7nnfxp5an3r7q8dccqkzszy7z85x1vfzdpqmii3nwlw35")))

(define-public crate-valve-server-query-0.3.2 (c (n "valve-server-query") (v "0.3.2") (h "0i1lgws024cl01jsy64whmlz0vcllyrk9a0im5q88ha4blsi4x0h")))

(define-public crate-valve-server-query-0.3.7 (c (n "valve-server-query") (v "0.3.7") (h "0v2cwyi74wblahz6icb350rmmfx2j0ccgjkc1frvblxaq48d3ab0")))

(define-public crate-valve-server-query-0.3.8 (c (n "valve-server-query") (v "0.3.8") (h "1dqmdsa7ajrp3pckzjw2ipa6wlx4vqibh4nbbyr3x63if7nxh6l4")))

(define-public crate-valve-server-query-0.3.9 (c (n "valve-server-query") (v "0.3.9") (h "0jml0lb921cpxfz1ljwn4aaw0p65a7mz24rsc6ajh4yxrz9arhz0")))

(define-public crate-valve-server-query-0.3.10 (c (n "valve-server-query") (v "0.3.10") (h "0jrhb9iidhsds20hk8q54bn9mb9n82i208mi743cs3lsi4kg3jaw")))

(define-public crate-valve-server-query-0.3.11 (c (n "valve-server-query") (v "0.3.11") (h "0zfxjk23v9r1v7x4s2bblj5xfnn5qlpal17s55223w1zy6hjv42k")))

(define-public crate-valve-server-query-0.4.0 (c (n "valve-server-query") (v "0.4.0") (h "16ld6ja04cw1kzcb6zkn1p8xx3q36b8sc4dyjdnn859b1998yhr5")))

(define-public crate-valve-server-query-0.4.5 (c (n "valve-server-query") (v "0.4.5") (d (list (d (n "console_log") (r "^0.2.0") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0il5wa6zw34r61v3fw44v3s0v21s3l31rs15j632ya6a8r1spibh")))

(define-public crate-valve-server-query-0.4.6 (c (n "valve-server-query") (v "0.4.6") (d (list (d (n "console_log") (r "^0.2.0") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^2.3.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0l23arbki4jiim12i54q2aj99az9l97d29dr88x6vi761a183p34")))

(define-public crate-valve-server-query-0.4.7 (c (n "valve-server-query") (v "0.4.7") (h "1yp9p41gnvfx1kcng4ap920psdhcrqz47a4d24szi6a5hgwfkcwb")))

(define-public crate-valve-server-query-0.4.8 (c (n "valve-server-query") (v "0.4.8") (h "0pikzbfaaqiimmijqx06sl6pvimaxp4q964wsj89jm5z70kqgq54")))

(define-public crate-valve-server-query-0.4.9 (c (n "valve-server-query") (v "0.4.9") (h "0k38z7wpfg9x5p09vsmbs1a1aqrld8rshsvahbrkjvh6qrwx47g1")))

