(define-module (crates-io va lv valve-rcon) #:use-module (crates-io))

(define-public crate-valve-rcon-0.1.0 (c (n "valve-rcon") (v "0.1.0") (h "1svbbfmvrllcqfdzb4sym3qiphchz122m3kp2s82113c6l631sqs")))

(define-public crate-valve-rcon-0.1.1 (c (n "valve-rcon") (v "0.1.1") (h "175z99zwfgfdlm6p7b9cfn6f4mp47q7gnf76jk2d2rd13l8qkmda")))

(define-public crate-valve-rcon-0.1.2 (c (n "valve-rcon") (v "0.1.2") (h "02i1q1q73gq13vrw6606a3sxlgl2479x2bipf7w5zmkyvas3srl2")))

