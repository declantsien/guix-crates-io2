(define-module (crates-io va ug vaughan) #:use-module (crates-io))

(define-public crate-vaughan-0.1.0 (c (n "vaughan") (v "0.1.0") (d (list (d (n "polars") (r "^0.39.2") (f (quote ("lazy" "test" "parquet" "ndarray" "diff" "is_in" "serde" "avx512"))) (d #t) (k 0)))) (h "134ppzbxx076q23n6nvmchhldc6mqd10bv1lbvmaaajzvz0hjhbf") (r "1.76")))

