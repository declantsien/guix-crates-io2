(define-module (crates-io va lq valq) #:use-module (crates-io))

(define-public crate-valq-0.1.0 (c (n "valq") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 2)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "059bmg0wgqfd86swd01bmclnv8j86kl37d2q4834xbbr0g5561dh")))

