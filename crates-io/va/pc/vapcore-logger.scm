(define-module (crates-io va pc vapcore-logger) #:use-module (crates-io))

(define-public crate-vapcore-logger-0.0.0 (c (n "vapcore-logger") (v "0.0.0") (h "03slsqp9iii7mwdfia5krn50r7c27vannlv2fmnnd96k2kqfr8x7") (y #t)))

(define-public crate-vapcore-logger-1.12.0 (c (n "vapcore-logger") (v "1.12.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "18rr65s2dyrknlrcsc6hjgqms3v8n54wqgfvzag3s6mw586a1n3z")))

