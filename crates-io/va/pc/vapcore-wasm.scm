(define-module (crates-io va pc vapcore-wasm) #:use-module (crates-io))

(define-public crate-vapcore-wasm-0.1.0 (c (n "vapcore-wasm") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tetsy-vm") (r "^0.1.0") (d #t) (k 0)) (d (n "tetsy-wasm") (r "^0.31.1") (d #t) (k 0)) (d (n "twasm-utils") (r "^0.6.1") (d #t) (k 0)) (d (n "twasmi") (r "^0.3.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "1yd8xbd9fszf09nfvb9nf01cwdhl2md583cypmv8s1zs900vca1y")))

