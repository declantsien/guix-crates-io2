(define-module (crates-io va pc vapcore-accounts) #:use-module (crates-io))

(define-public crate-vapcore-accounts-0.1.0 (c (n "vapcore-accounts") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-crypto") (r "^0.4.2") (f (quote ("publickey"))) (d #t) (k 0)) (d (n "vapkey") (r "^0.4.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 2)) (d (n "vapstore") (r "^0.2.1") (d #t) (k 0)))) (h "1kfs9vlm0aw4gksckaalym5r387i88rgigb0cdmlhlg7ncc6d32m")))

