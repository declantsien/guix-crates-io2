(define-module (crates-io va pc vapcore-db) #:use-module (crates-io))

(define-public crate-vapcore-db-0.1.0 (c (n "vapcore-db") (v "0.1.0") (d (list (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.3.2") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 0)) (d (n "tetsy-rlp-derive") (r "^0.0.1") (d #t) (k 0)) (d (n "tetsy-util-mem") (r "^0.3.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "0baah6phgmv7bkacpc6lh4nprd4js7bfws3sgscmhjzwvmdwnlc9")))

