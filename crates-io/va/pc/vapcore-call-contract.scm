(define-module (crates-io va pc vapcore-call-contract) #:use-module (crates-io))

(define-public crate-vapcore-call-contract-0.1.0 (c (n "vapcore-call-contract") (v "0.1.0") (d (list (d (n "bytes") (r "^0.1") (d #t) (k 0) (p "tetsy-bytes")) (d (n "types") (r "^0.1.0") (d #t) (k 0) (p "common-types")) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "1fls9f4slb8xn3nn05wv6ff3n0ynr2v4wmaqzb0cfl37afxir8gb")))

