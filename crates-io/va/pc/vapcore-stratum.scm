(define-module (crates-io va pc vapcore-stratum) #:use-module (crates-io))

(define-public crate-vapcore-stratum-1.12.0 (c (n "vapcore-stratum") (v "1.12.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core") (r "^14.2.1") (d #t) (k 0)) (d (n "tetsy-jsonrpc-tcp-server") (r "^14.2.1") (d #t) (k 0)) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 2)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "1f00m84yb5yz78sm106i69lscli9mbvdlkwwv4m00yj2kfn3sz78")))

