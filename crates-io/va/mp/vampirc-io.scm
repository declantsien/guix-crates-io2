(define-module (crates-io va mp vampirc-io) #:use-module (crates-io))

(define-public crate-vampirc-io-0.2.0 (c (n "vampirc-io") (v "0.2.0") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.18") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "vampirc-uci") (r "^0.8.2") (d #t) (k 0)))) (h "0k6ynqsvdw3vx3ii4qks62k2xf9n7k348fas2bbjyq8rbwq6ymhq")))

(define-public crate-vampirc-io-0.2.1 (c (n "vampirc-io") (v "0.2.1") (d (list (d (n "async-std") (r "^0.99.7") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.18") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "vampirc-uci") (r "^0.8.2") (d #t) (k 0)))) (h "0zr7v8cra43qrq28ngvxgdx39k1ddx3nb1513c9h2mhwrlikma8f")))

(define-public crate-vampirc-io-0.3.0 (c (n "vampirc-io") (v "0.3.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "vampirc-uci") (r "^0.9") (d #t) (k 0)))) (h "1vivp6d9m47jwal9yfmdc68lg28j4sjyyfa7lmavnlphchx12j41")))

