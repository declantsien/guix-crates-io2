(define-module (crates-io va mp vampirc-uci) #:use-module (crates-io))

(define-public crate-vampirc-uci-0.5.0 (c (n "vampirc-uci") (v "0.5.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0b00yni6paddir7nhb80b11q60wvskr7c62q0h9yrsr2fa8a3if9")))

(define-public crate-vampirc-uci-0.6.0 (c (n "vampirc-uci") (v "0.6.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "18c65w2sch5j2xaahw4sqxkcq8d382vdz8b90wd2w5pvgzzdgizk")))

(define-public crate-vampirc-uci-0.7.0 (c (n "vampirc-uci") (v "0.7.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0xxqqnfxwr6kfc1g0g71v5bxpw8my8rbqd8gw68djjr9fbryp11a")))

(define-public crate-vampirc-uci-0.7.5 (c (n "vampirc-uci") (v "0.7.5") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1fzqp193fin86pl36vaf3q327jy85hwl97in5v53p6x9nlgj454j")))

(define-public crate-vampirc-uci-0.8.0 (c (n "vampirc-uci") (v "0.8.0") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "11ahwlal307irkqgg8asc3xjc1261sbs39368bklz26ca9xjb659")))

(define-public crate-vampirc-uci-0.8.1 (c (n "vampirc-uci") (v "0.8.1") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1cp4l9lj23avlvvcf5y8zvv625r5nxzskppag6dqsw406ivgzmi2")))

(define-public crate-vampirc-uci-0.8.2 (c (n "vampirc-uci") (v "0.8.2") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0mz624r3xg10c62jzxi54rfc6xajljs5r5i7lxix14a2zvps5635")))

(define-public crate-vampirc-uci-0.8.3 (c (n "vampirc-uci") (v "0.8.3") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "038winps4118vvain25acxklvkycydg93xwcsqi5pqi03lrsdkn4")))

(define-public crate-vampirc-uci-0.9.0 (c (n "vampirc-uci") (v "0.9.0") (d (list (d (n "chess") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0a7mkvgjcdkssl2cxzidag1v52grg47ccffaik1a8zc85f3kls0x")))

(define-public crate-vampirc-uci-0.10.0 (c (n "vampirc-uci") (v "0.10.0") (d (list (d (n "chess") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1y330jrm56j76dkwzvfddzzqifb7hgqgnvqpcw27sys7dqylvss8") (y #t)))

(define-public crate-vampirc-uci-0.10.1 (c (n "vampirc-uci") (v "0.10.1") (d (list (d (n "chess") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1asijl9vw3drmq3ny3yqlzjrfprcdf9jfhn5fkrb4ckf4pqil9fn")))

(define-public crate-vampirc-uci-0.11.0 (c (n "vampirc-uci") (v "0.11.0") (d (list (d (n "chess") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "07lwmrgq7w7lk4lhygknqqgjjhfzl92mdsjjhf9hlgcl6b8xykkb")))

(define-public crate-vampirc-uci-0.11.1 (c (n "vampirc-uci") (v "0.11.1") (d (list (d (n "chess") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0572qks012zdxgjzcbh6q8fr7jisf12qg5nzh5bl16fyjpqnmlff")))

