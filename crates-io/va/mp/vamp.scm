(define-module (crates-io va mp vamp) #:use-module (crates-io))

(define-public crate-vamp-0.1.0 (c (n "vamp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0gals88km4j1m59a0kbkzcihq4gk5vv9g38a2yxhllzaz14alakd")))

