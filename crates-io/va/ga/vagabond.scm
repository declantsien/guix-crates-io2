(define-module (crates-io va ga vagabond) #:use-module (crates-io))

(define-public crate-vagabond-0.1.0 (c (n "vagabond") (v "0.1.0") (d (list (d (n "cdrs") (r "^2.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "kankyo") (r "^0.2.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "16y9sn42r5z9fpws3jy8jgm231qq95g4k2mgx8571n72va0pail8")))

(define-public crate-vagabond-0.1.1 (c (n "vagabond") (v "0.1.1") (d (list (d (n "cdrs") (r "^2.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "kankyo") (r "^0.2.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "020grlw1l3q6myxrlqh8kf06nwj75hjx57gy3qczzbj246px0akk")))

