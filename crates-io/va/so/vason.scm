(define-module (crates-io va so vason) #:use-module (crates-io))

(define-public crate-vason-0.0.1 (c (n "vason") (v "0.0.1") (d (list (d (n "minifb") (r "^0.23.0") (d #t) (k 2)))) (h "00pkvp3fiab2lapwpa5h1h1mzlxq29k7ppagrlhjj054r4rvnhsa") (y #t) (r "1.58.1")))

(define-public crate-vason-0.0.2 (c (n "vason") (v "0.0.2") (d (list (d (n "minifb") (r "^0.23.0") (d #t) (k 2)))) (h "06a0kcjc50yxg5szc0j5c8ibk97zma0qzr3n68wygc5hbmqpkmyl") (r "1.58.1")))

(define-public crate-vason-0.0.3 (c (n "vason") (v "0.0.3") (d (list (d (n "minifb") (r "^0.23.0") (d #t) (k 2)))) (h "0yr2840s4vwpf6cmn1k54n13mc4xljjabjbpmjaq6dx83dvy8cds") (r "1.56.1")))

