(define-module (crates-io va #{10}# va108xx) #:use-module (crates-io))

(define-public crate-va108xx-0.1.0 (c (n "va108xx") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0qff8xjb6pc67r1ng7ara603zc7qjn2rx7ky861zy3zw6hbc1pjx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.1.1 (c (n "va108xx") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "05ns9v3w7b6wq91abl0r8jr36r3adk5d6axmvwlqqgbbi61hpsgg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.1.2 (c (n "va108xx") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bq9jpj5s0d831kqhv9n60pcljrkwpmksar3rml6qm92k5y83w63") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.1.3 (c (n "va108xx") (v "0.1.3") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1p2gzgx0zj1559nv7c5pqrld4mvl11qrzx65wrgibdbjhv00qs6c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.2.0 (c (n "va108xx") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nwfar5xqsd7kq309ipr9pn56c48l0aqlvbwi0zl8j6dfalqdkk2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.2.1 (c (n "va108xx") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1bbaxv3la7y6k3614n111qs9viyf8qa487dmckwlg7293lm5vss6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.2.2 (c (n "va108xx") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1cndhg9g1r1b3wg8dyc5kjgmzrkbrg2f0dxql3wfbam8syi4gxdd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.2.3 (c (n "va108xx") (v "0.2.3") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xlmy0pza6wl4r0vwd2p1dx2zxwndfw9mk0si27rwk9aj2rqrxqs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va108xx-0.2.4 (c (n "va108xx") (v "0.2.4") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10p4jd69p8aiz5c9k3nwpisj0hpqwn1f1h9wkd78rsgxg3bcqr2z") (f (quote (("rt" "cortex-m-rt/device"))))))

