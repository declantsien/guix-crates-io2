(define-module (crates-io va rl varlen) #:use-module (crates-io))

(define-public crate-varlen-0.1.0 (c (n "varlen") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.9.1") (o #t) (d #t) (k 0)) (d (n "svgbobdoc") (r "^0.3.0-alpha") (o #t) (d #t) (k 0)) (d (n "varlen_macro") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0hkqihfl0h73pn2a8mn3fbm08lrf87v664knjm7lh18qhzpahgfb") (f (quote (("macro" "varlen_macro") ("doc" "svgbobdoc/enable"))))))

(define-public crate-varlen-0.1.1 (c (n "varlen") (v "0.1.1") (d (list (d (n "bumpalo") (r "^3.9.1") (o #t) (d #t) (k 0)) (d (n "svgbobdoc") (r "^0.3.0-alpha") (o #t) (d #t) (k 0)) (d (n "varlen_macro") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01far8l2b36y1s1q8r9l38dkbb1ykj878kzklbvsnjbagd785wd7") (f (quote (("macro" "varlen_macro") ("doc" "svgbobdoc/enable"))))))

(define-public crate-varlen-0.1.2 (c (n "varlen") (v "0.1.2") (d (list (d (n "bumpalo") (r "^3.9.1") (o #t) (d #t) (k 0)) (d (n "svgbobdoc") (r "^0.3.0-alpha") (o #t) (d #t) (k 0)) (d (n "varlen_macro") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0iy2a26yyyrzhywpkl8hymisg3k701k6qpcnq7dbhp7b0adisspq") (f (quote (("macro" "varlen_macro") ("doc" "svgbobdoc/enable"))))))

