(define-module (crates-io va rl varlen_macro) #:use-module (crates-io))

(define-public crate-varlen_macro-0.1.0 (c (n "varlen_macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("derive" "parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1vnmr7agc67gg9sd74fd3rgds4v827hx7snnj6bx63w55nqd8195")))

(define-public crate-varlen_macro-0.1.1 (c (n "varlen_macro") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("derive" "parsing" "extra-traits" "full"))) (d #t) (k 0)) (d (n "varlen") (r "^0.1.0") (d #t) (k 2)))) (h "1jlw5lqf9m5bpm45b5vb20lzavdchdw38wn2j3kvww0rhaznbvk8")))

(define-public crate-varlen_macro-0.1.2 (c (n "varlen_macro") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("derive" "parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)) (d (n "varlen") (r "^0.1.0") (d #t) (k 2)))) (h "0csg6f61sq2niy5h1b7mv1rfyg30a2y13grcij55vd6bp1x9pwp4")))

(define-public crate-varlen_macro-0.1.3 (c (n "varlen_macro") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("derive" "parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)) (d (n "varlen") (r "^0.1.0") (d #t) (k 2)))) (h "11ncfh6m2l6bh468wcy4fdqr87ihh80x2dy6a3afd114c2a3qcvd")))

