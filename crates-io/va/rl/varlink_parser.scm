(define-module (crates-io va rl varlink_parser) #:use-module (crates-io))

(define-public crate-varlink_parser-0.1.0 (c (n "varlink_parser") (v "0.1.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14jqwc3manjkr0xslf2qfnclavlg2pfl9f8xb4rsszlj8rxh06j3")))

(define-public crate-varlink_parser-0.1.1 (c (n "varlink_parser") (v "0.1.1") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1705jm7wqgg4wgds5fa0cfj3ibql9fbsnxg8bmgymx90k52649r2")))

(define-public crate-varlink_parser-0.1.2 (c (n "varlink_parser") (v "0.1.2") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mrd29k16nzgl7sxy6pznxck4pqibq5c801pkcjxgfamdbv1zqi2")))

(define-public crate-varlink_parser-0.1.3 (c (n "varlink_parser") (v "0.1.3") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0lx025mri764vs9mhb0wbraiflpxnq4j7xjgykn5hwqa2j0m0qdb")))

(define-public crate-varlink_parser-0.1.4 (c (n "varlink_parser") (v "0.1.4") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1x5dbcrzh54gs7hwxvhlah5qgav12321ggnz0b5ls09wvy6kkdxh")))

(define-public crate-varlink_parser-0.1.5 (c (n "varlink_parser") (v "0.1.5") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0l06pp3zqardl74yd4ljvga88nx3inqxsc40mgw33a8l7f2as4df")))

(define-public crate-varlink_parser-0.1.6 (c (n "varlink_parser") (v "0.1.6") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wqg1hcnj89sn7v41g8siwm1c3jm1xy1kj2l23lwbzm30kn51yxx")))

(define-public crate-varlink_parser-0.1.7 (c (n "varlink_parser") (v "0.1.7") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0zdc4n5zbzhqiq0qg9cjils0zzdk13r7bk5ra9y7f392sxssfpm4")))

(define-public crate-varlink_parser-0.2.0 (c (n "varlink_parser") (v "0.2.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "15zzh4yr16xhj4jnm5xashm2ibvpgcpgp2cb74q1qzambakx4cpz")))

(define-public crate-varlink_parser-0.3.0 (c (n "varlink_parser") (v "0.3.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0jzhpn5rw92a0f52afp5yg2wk364b0lrkh3a5g7c1vhpiji7w0aq")))

(define-public crate-varlink_parser-0.6.0 (c (n "varlink_parser") (v "0.6.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1f67w7cd158dv9r032m57hnbx177mn6hcp12y9vmkv38abs6bwqv")))

(define-public crate-varlink_parser-0.8.0 (c (n "varlink_parser") (v "0.8.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1ivlbz5r21fm8lfj9gax6n9gx1m83y4bx02yf56chksw2s0xqsc4")))

(define-public crate-varlink_parser-0.8.1 (c (n "varlink_parser") (v "0.8.1") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0bj66n6jf6d6i9r932rjz7285wspqr0kc984gfggh6qflmwy89h1")))

(define-public crate-varlink_parser-1.0.0 (c (n "varlink_parser") (v "1.0.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0q7nmf5lznwflkblmi9vbwkwqi9cikis7y3129mf5m02njhnx2bq")))

(define-public crate-varlink_parser-1.0.5 (c (n "varlink_parser") (v "1.0.5") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "02lpaddd9xhjqw8lahs63s61wp65ydmilv5ry9lwh8bbkkw6imxm") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-1.1.0 (c (n "varlink_parser") (v "1.1.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "0s1mxfxs6sk01gnlci9y10vzxkk5wd6imkn0gv71plmx4c0nisky") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-2.0.0 (c (n "varlink_parser") (v "2.0.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "07z0y5wpdb6qsvsng734rjhvr7iispmrrffhiagkhclxh3mac87j") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-2.1.0 (c (n "varlink_parser") (v "2.1.0") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "0k4nkxcx2bn5yvxpzang602d1yl7z5s5dridmn0bnac4156acr04") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-2.1.1 (c (n "varlink_parser") (v "2.1.1") (d (list (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "0wa1n4lv9j76b28pxc1nn9mpn1phb49lviffxmznawism3cy150v") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-2.2.0 (c (n "varlink_parser") (v "2.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "1gc3lfw474w947ns08gmklbjhyn3jhqbh9skd7g1x09s32v6yr07") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-2.2.1 (c (n "varlink_parser") (v "2.2.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "1cmj885mg2gp3vrdnagfb0559kv9vrryfrgdd63bszki7vwhpv2x") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-2.2.2 (c (n "varlink_parser") (v "2.2.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "1ihp0565zbwwdf8dak319ydmzixf7vq62mp1kcx5zqmh617vjb5j") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-3.0.0 (c (n "varlink_parser") (v "3.0.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "chainerror") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "1f6v861qkws6clzql5xhzrdi5bwjxb53ylkbg6rcb9vfip12110k") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-4.0.0 (c (n "varlink_parser") (v "4.0.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "chainerror") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "1xaik10p2pf7adrcwa7sizw67yyyphq816rcsm1hrix9n39l6j8r") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-4.0.1 (c (n "varlink_parser") (v "4.0.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "bytes") (r "^0") (d #t) (k 0)) (d (n "chainerror") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "0fkhbkik2q5nccrp3bixpz4gqg03mvs67wn6yq93xsnawxrrsya1") (f (quote (("dynamic_peg" "peg"))))))

(define-public crate-varlink_parser-4.0.2 (c (n "varlink_parser") (v "4.0.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "chainerror") (r "^0.4.2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "1xlmzhnz32zlalbssp71m3xmvnby12bnpm5ds5j6wssslhdwh042") (f (quote (("dynamic_peg" "peg") ("default"))))))

(define-public crate-varlink_parser-4.0.3 (c (n "varlink_parser") (v "4.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chainerror") (r "^0.4.3") (d #t) (k 0)) (d (n "peg") (r "^0.5") (o #t) (d #t) (k 1)))) (h "01isr3vlilzx0qqd0phc45apf6aw3jqd82208098k8j8hs6prw8z") (f (quote (("dynamic_peg" "peg") ("default"))))))

(define-public crate-varlink_parser-4.0.4 (c (n "varlink_parser") (v "4.0.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chainerror") (r "^0.4.3") (d #t) (k 0)) (d (n "peg") (r "^0.6") (d #t) (k 0)))) (h "1cg4icr44p4w5aqskz49a0b2zqlbf0bxfxkrjqjk3wxh3pnmhcm6")))

(define-public crate-varlink_parser-4.1.0 (c (n "varlink_parser") (v "4.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chainerror") (r "^0.7.0") (d #t) (k 0)) (d (n "peg") (r "^0.6") (d #t) (k 0)))) (h "1wqcybasifgjzxwsix5yd4xv6hih3sjbgzvqqz2xkawyk1841f41")))

(define-public crate-varlink_parser-4.2.0 (c (n "varlink_parser") (v "4.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chainerror") (r "^0.7.0") (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)))) (h "07ljmrp6ixzvy5752w2bdz788f1zxxvl8jn8nv6k7jwc3qy9zyrm")))

