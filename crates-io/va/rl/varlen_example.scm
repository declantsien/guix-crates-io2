(define-module (crates-io va rl varlen_example) #:use-module (crates-io))

(define-public crate-varlen_example-0.1.1 (c (n "varlen_example") (v "0.1.1") (d (list (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)) (d (n "varlen") (r "^0.1") (f (quote ("macro"))) (d #t) (k 0)))) (h "09h5xchnc04f893jzkwbchzjqp3ilq7g1mvjgy0nanm5v3w8vw37")))

(define-public crate-varlen_example-0.1.2 (c (n "varlen_example") (v "0.1.2") (d (list (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)) (d (n "varlen") (r "^0.1.2") (f (quote ("macro"))) (d #t) (k 0)))) (h "0qz7z8dd9dl4vgjgcjc9bn1j43is74lgx3fhzjhs9chwypzmamj1")))

