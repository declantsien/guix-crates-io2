(define-module (crates-io va rl varlink_derive) #:use-module (crates-io))

(define-public crate-varlink_derive-0.1.0 (c (n "varlink_derive") (v "0.1.0") (d (list (d (n "varlink_generator") (r "^7.1") (d #t) (k 0)))) (h "0d8hnx7j8yn4n9gnirgv0gayjmd5d4sapkd88ddrpvyr11i7fjkq")))

(define-public crate-varlink_derive-8.0.0 (c (n "varlink_derive") (v "8.0.0") (d (list (d (n "varlink_generator") (r "^8.0") (d #t) (k 0)))) (h "0b3zbhyvi78p2jrbiyh1glhhk49h9s8psw6z8igrih3330j9j8bi")))

