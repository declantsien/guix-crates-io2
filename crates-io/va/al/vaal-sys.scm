(define-module (crates-io va al vaal-sys) #:use-module (crates-io))

(define-public crate-vaal-sys-0.1.0 (c (n "vaal-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rck6app82hjcwylcx483hmzbszkgrg3jrnj02w52aqvj4jjkdwp")))

(define-public crate-vaal-sys-0.2.1 (c (n "vaal-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s1pxfh6l8bc0gv6klibsyx3krzj414x41aihqgr1lhya7p12vxq")))

(define-public crate-vaal-sys-0.2.2 (c (n "vaal-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1776vf0r5628vq9c90h3c3qalj1h73nyqqx9x04vidr90zda24nx")))

(define-public crate-vaal-sys-0.3.0 (c (n "vaal-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03zh1daicbdxgdwxag9wm5830m0fkqsws13bgplafm567klvv0ny")))

(define-public crate-vaal-sys-0.3.1 (c (n "vaal-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0aqb99px8fxslmck4npwsc8r6plmfhncb6j7sk0ifah3h4nlfajv")))

(define-public crate-vaal-sys-0.3.2 (c (n "vaal-sys") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02rgf3lvlskl0zs7l0iwnzqa7v97a6hygnfskmlx6m8a21hwy2ls")))

(define-public crate-vaal-sys-0.3.3 (c (n "vaal-sys") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02rc9000zlb7gg3m5czy2dh6ip12jzhsrynla1kjzncccnhzj9s3")))

(define-public crate-vaal-sys-0.3.4 (c (n "vaal-sys") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lj2ah9rs4r9r9hybw07i0mv33x2d3pqvfxqbjq61nl39nwg8k3w")))

