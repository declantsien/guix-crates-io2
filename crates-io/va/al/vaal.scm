(define-module (crates-io va al vaal) #:use-module (crates-io))

(define-public crate-vaal-0.1.0 (c (n "vaal") (v "0.1.0") (d (list (d (n "deepviewrt") (r "^0.1.0") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1kkxl7ycbysf0zr8d8p1kn1qvxr29rll59rm7waxgwlsr1qjawfs")))

(define-public crate-vaal-0.2.1 (c (n "vaal") (v "0.2.1") (d (list (d (n "deepviewrt") (r "^0.2.0") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.2.1") (d #t) (k 0)))) (h "03f502yxkhwx8q26acm2wf0gpn23mfcshd90wmicndg02x6yb8vd")))

(define-public crate-vaal-0.2.2 (c (n "vaal") (v "0.2.2") (d (list (d (n "deepviewrt") (r "^0.2.6") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.2.2") (d #t) (k 0)))) (h "01mgg3ghvci627viw6wcr4sa6hlcfz505nbqsvalagx5xlya7iv1")))

(define-public crate-vaal-0.3.0 (c (n "vaal") (v "0.3.0") (d (list (d (n "deepviewrt") (r "^0.3.0") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0gd2f9vm89rh407s2wa5xh21hnvhk2q9s2dykljhibgi9w2pbb8l")))

(define-public crate-vaal-0.3.1 (c (n "vaal") (v "0.3.1") (d (list (d (n "deepviewrt") (r "^0.4.0") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0vcr0b79rfnf75h2bldaqh8m35ncjlhahxyab0pmccs781k6dsq9")))

(define-public crate-vaal-0.3.2 (c (n "vaal") (v "0.3.2") (d (list (d (n "deepviewrt") (r "^0.4.0") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.3.2") (d #t) (k 0)))) (h "040h5ad039wzlpyqxjn08m4bfglr1qsgpqrc5kn9v7i6zchmqsvn")))

(define-public crate-vaal-0.3.3 (c (n "vaal") (v "0.3.3") (d (list (d (n "deepviewrt") (r "^0.5.0") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.3.3") (d #t) (k 0)))) (h "1df1g4j9x0wc0cslr6021g8wjd3sfa02jlsini83jkx2naq9qfc0")))

(define-public crate-vaal-0.3.4 (c (n "vaal") (v "0.3.4") (d (list (d (n "deepviewrt") (r "^0.5.0") (d #t) (k 0)) (d (n "vaal-sys") (r "^0.3.4") (d #t) (k 0)))) (h "0ksayss48k14kv6szqx5xjzkm3fbkxfjd68m1znlcayc6h7331pd")))

