(define-module (crates-io va le valence_server_common) #:use-module (crates-io))

(define-public crate-valence_server_common-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_server_common") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (f (quote ("multi-threaded"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)) (d (n "valence_protocol") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1s3v1a4pnqzi17qqgx0lipazys2qwcliwds0rcayddn21fasp9v3")))

