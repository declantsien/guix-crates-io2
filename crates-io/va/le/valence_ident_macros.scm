(define-module (crates-io va le valence_ident_macros) #:use-module (crates-io))

(define-public crate-valence_ident_macros-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_ident_macros") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0f5xc4g9qjhvp43brbb9dq8v09xm2jzma4y4k56bn50x990223sy")))

