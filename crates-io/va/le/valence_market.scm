(define-module (crates-io va le valence_market) #:use-module (crates-io))

(define-public crate-valence_market-0.1.0 (c (n "valence_market") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "mongodb") (r "^2.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.173") (f (quote ("derive"))) (d #t) (k 0)) (d (n "valence_core") (r "^0.1.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (d #t) (k 0)))) (h "1mdrv8jdjl748alampryp75plaj6lsfkz7dv0qpyfdbqj8yrxs36")))

(define-public crate-valence_market-0.1.1 (c (n "valence_market") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "mongodb") (r "^2.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.173") (f (quote ("derive"))) (d #t) (k 0)) (d (n "valence_core") (r "^0.1.1") (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (d #t) (k 0)))) (h "0svbh4b2cyg7r73549vdahbcc1qbp2g7pr2b3xwgpim7lld9ym30")))

