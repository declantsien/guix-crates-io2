(define-module (crates-io va le vale-derive) #:use-module (crates-io))

(define-public crate-vale-derive-0.0.0 (c (n "vale-derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "08y3lysrymkscwmn6fampb3rwagg1mi9fvhcv0545fx5wn2vbs5f")))

