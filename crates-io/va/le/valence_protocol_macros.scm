(define-module (crates-io va le valence_protocol_macros) #:use-module (crates-io))

(define-public crate-valence_protocol_macros-0.0.1 (c (n "valence_protocol_macros") (v "0.0.1") (h "09dbsk6l4w6lv2hyhikbc4axqy6bfqjfanyi3rmdwp8xp44j8v9y")))

(define-public crate-valence_protocol_macros-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_protocol_macros") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0jlbpx51s4lmy21kwyklp3kdcrwaf5s5rlq98mrgmmdhy3c98gv8")))

