(define-module (crates-io va le valence_boss_bar) #:use-module (crates-io))

(define-public crate-valence_boss_bar-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_boss_bar") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (f (quote ("multi-threaded"))) (k 0)) (d (n "bitfield-struct") (r "^0.3.1") (d #t) (k 0)) (d (n "valence_entity") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "valence_server") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "0rxv3n1vn36r4fw3w6qvhb3xmki5k3gdcrka46bhv7xlviy3xaf6")))

