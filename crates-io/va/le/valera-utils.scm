(define-module (crates-io va le valera-utils) #:use-module (crates-io))

(define-public crate-valera-utils-0.1.0 (c (n "valera-utils") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.69") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0") (d #t) (k 0)))) (h "1ixvyl15nq58allwdrqglnac943j8iwrw2r8lcsq45f7kd377hcm") (r "1.72")))

(define-public crate-valera-utils-0.1.1 (c (n "valera-utils") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.69") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "socks" "blocking"))) (d #t) (k 0)) (d (n "rodio") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0f53731dh52xzsnzd0vmvd8mv1av3h3iyr1rbh3b8w3sksizbgvs") (r "1.74")))

