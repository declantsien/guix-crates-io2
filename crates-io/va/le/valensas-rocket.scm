(define-module (crates-io va le valensas-rocket) #:use-module (crates-io))

(define-public crate-valensas-rocket-0.1.0 (c (n "valensas-rocket") (v "0.1.0") (d (list (d (n "rocket") (r "=0.5.0-rc.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "valensas-actuator") (r "^0.1.0") (d #t) (k 0)))) (h "02j88vs308l5dl2gdf4r2zixjnqy8kkvhcxv0pmz9xcvk77vdwc0")))

(define-public crate-valensas-rocket-0.1.1 (c (n "valensas-rocket") (v "0.1.1") (d (list (d (n "rocket") (r "=0.5.0-rc.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "valensas-actuator") (r "^0.1.2") (d #t) (k 0)))) (h "06gn0yb5vay6hwr0nzcg5h9qa3drw9ng31yxaifxl4hv58yfjkk4")))

(define-public crate-valensas-rocket-0.1.2 (c (n "valensas-rocket") (v "0.1.2") (d (list (d (n "rocket") (r "=0.5.0-rc.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 2)) (d (n "valensas-actuator") (r "^0.1.3") (d #t) (k 0)))) (h "1m78z1bqhxnq293qvgx9gd89a0l6585yl20ap5sassmmp1bz350y")))

(define-public crate-valensas-rocket-0.1.3 (c (n "valensas-rocket") (v "0.1.3") (d (list (d (n "rocket") (r "=0.5.0-rc.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (d #t) (k 2)) (d (n "valensas-actuator") (r "^0.2.0") (d #t) (k 0)))) (h "1lwvl5mr8ndghawzqp595x46xqgihbanxyi8rk2950gqwmk9vgzm")))

