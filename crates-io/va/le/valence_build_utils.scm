(define-module (crates-io va le valence_build_utils) #:use-module (crates-io))

(define-public crate-valence_build_utils-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_build_utils") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "anyhow") (r "^1.0.70") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1g5rpw070g0c8n3n3q2wdq79j5snxji9w4di94hgz5jgplcaxqac")))

