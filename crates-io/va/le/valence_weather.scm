(define-module (crates-io va le valence_weather) #:use-module (crates-io))

(define-public crate-valence_weather-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_weather") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (f (quote ("multi-threaded"))) (k 0)) (d (n "valence_server") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "0g72l6dx31x5v4krj4hzwxp25f5l492gpm7sapl00kydask1fpjh")))

