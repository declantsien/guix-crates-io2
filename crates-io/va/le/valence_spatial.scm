(define-module (crates-io va le valence_spatial) #:use-module (crates-io))

(define-public crate-valence_spatial-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_spatial") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (d #t) (k 0)))) (h "16xccdsp2apkykdc5665qf8xayfdr0l9l8bk5qr751fzz7qa4xb6")))

