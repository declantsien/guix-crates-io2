(define-module (crates-io va le valence_lang) #:use-module (crates-io))

(define-public crate-valence_lang-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_lang") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "anyhow") (r "^1.0.70") (f (quote ("backtrace"))) (d #t) (k 1)) (d (n "heck") (r "^0.4.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 1)) (d (n "valence_build_utils") (r "^0.2.0-alpha.1") (d #t) (k 1)))) (h "0yvz39a7xd98w61r0pg7iyhybcvsi255vbylrz1r4zp8d2b44mw9")))

