(define-module (crates-io va le valera) #:use-module (crates-io))

(define-public crate-valera-0.1.0 (c (n "valera") (v "0.1.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fdx8fp99bi7wcln3ajfnsj8xcsw4c9wb4x6rnlbj1j5pkxjykpk") (r "1.72")))

