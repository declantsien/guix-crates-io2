(define-module (crates-io va le valence_text) #:use-module (crates-io))

(define-public crate-valence_text-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_text") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "anyhow") (r "^1.0.70") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "valence_ident") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "valence_nbt") (r "^0.6.1") (f (quote ("uuid"))) (d #t) (k 0)))) (h "1d9fyf4fpdcwrii58hb0i1b3dgw7yjsap9bkbgy36qsx8rrd7ll1")))

