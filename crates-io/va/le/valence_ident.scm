(define-module (crates-io va le valence_ident) #:use-module (crates-io))

(define-public crate-valence_ident-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_ident") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "valence_ident_macros") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1l3v2s3gl33ia2mwhvg4msknhb57i0dzbxz14wg6dc4k32czaxcc")))

