(define-module (crates-io va le vale) #:use-module (crates-io))

(define-public crate-vale-0.0.0 (c (n "vale") (v "0.0.0") (d (list (d (n "rkt") (r "^0.4") (o #t) (d #t) (k 0) (p "rocket")) (d (n "rkt_contrib") (r "^0.4") (d #t) (k 2) (p "rocket_contrib")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "vale-derive") (r "^0.0.0") (d #t) (k 0)))) (h "0nzpyi78dkla1ngspl20v1i3av3m74p1fljxmzn64pw1sf0d2qay") (f (quote (("rocket" "rkt") ("default" "rocket"))))))

