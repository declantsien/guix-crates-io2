(define-module (crates-io va le valence_scoreboard) #:use-module (crates-io))

(define-public crate-valence_scoreboard-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_scoreboard") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (f (quote ("multi-threaded"))) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "valence_server") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1y2qn1fpd5b36d2sckxvhxcdxi0nj7avf1d1n7cf102m500flr2w")))

