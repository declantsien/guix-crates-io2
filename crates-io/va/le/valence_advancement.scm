(define-module (crates-io va le valence_advancement) #:use-module (crates-io))

(define-public crate-valence_advancement-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_advancement") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (f (quote ("multi-threaded"))) (k 0)) (d (n "bevy_hierarchy") (r "^0.11") (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "valence_server") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "0ilm4y6mb5jss0shzk7r4d2sllprapk1a8a20k2r6xchba7h806m")))

