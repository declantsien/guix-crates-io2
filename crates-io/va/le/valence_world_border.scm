(define-module (crates-io va le valence_world_border) #:use-module (crates-io))

(define-public crate-valence_world_border-0.2.0-alpha.1+mc.1.20.1 (c (n "valence_world_border") (v "0.2.0-alpha.1+mc.1.20.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (f (quote ("multi-threaded"))) (k 0)) (d (n "valence_server") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1g8mrlmd23y49nb2pag282jqxk9xa2wm8m0306hq2ms5vcbcqpjl")))

