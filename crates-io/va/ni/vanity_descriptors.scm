(define-module (crates-io va ni vanity_descriptors) #:use-module (crates-io))

(define-public crate-vanity_descriptors-0.0.1 (c (n "vanity_descriptors") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitcoin") (r "^0.28.1") (d #t) (k 0)) (d (n "miniscript") (r "^7.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1ixbs8jl4332wapkj6m7q8zih1gnjladgjwbcr7avw98vzx7zl1h")))

(define-public crate-vanity_descriptors-0.0.2 (c (n "vanity_descriptors") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitcoin") (r "^0.28.1") (d #t) (k 0)) (d (n "miniscript") (r "^7.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "00f4330hijl1afb68bmgb3rvkwxm76srszhqjxx3l0drkf0vjicg")))

