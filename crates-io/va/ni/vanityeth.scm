(define-module (crates-io va ni vanityeth) #:use-module (crates-io))

(define-public crate-vanityeth-0.1.1 (c (n "vanityeth") (v "0.1.1") (d (list (d (n "ethers") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "15pqr8ns1agzr3alsm2crgw911g217xbbwl1d0c6lw4n2f8j2lrq")))

(define-public crate-vanityeth-0.1.2 (c (n "vanityeth") (v "0.1.2") (d (list (d (n "ethers") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "0lp2mr40s49hwzqky47apnfmi3j1nrna2nsna01pdqp8ha7gbzyg")))

(define-public crate-vanityeth-0.1.4 (c (n "vanityeth") (v "0.1.4") (d (list (d (n "ctrlc") (r "^3.2.0") (d #t) (k 0)) (d (n "ethers") (r "^0.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "1zfbrsq0rf60r81pv03cr1fdzb08b96jqdqbapfd7rjy9nnd81hv")))

(define-public crate-vanityeth-0.1.5 (c (n "vanityeth") (v "0.1.5") (d (list (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "ethers") (r "^0.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1phfj92grlx3x9zn7fvqsq7890lrbfj2mgankxghhs2qmjabphwg")))

