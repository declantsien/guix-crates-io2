(define-module (crates-io va ni vanityhash) #:use-module (crates-io))

(define-public crate-vanityhash-0.1.0 (c (n "vanityhash") (v "0.1.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "1fgpz5kxs7qd7d7an4hqyrxq9jnzh6avqwjszgdyzdcp1bzqyc2j")))

(define-public crate-vanityhash-0.2.0 (c (n "vanityhash") (v "0.2.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "00lmp1b3fd5ghycx477dskl8l2i28nbvp9v0aam7h3mb250y7x3v")))

(define-public crate-vanityhash-0.3.0 (c (n "vanityhash") (v "0.3.0") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.7") (d #t) (k 0)))) (h "0i6a5w7dvvwyibhwzp5hm9n54kff0hdldhv78hpgngc6lim2a0a2")))

