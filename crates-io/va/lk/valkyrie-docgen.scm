(define-module (crates-io va lk valkyrie-docgen) #:use-module (crates-io))

(define-public crate-valkyrie-docgen-0.0.1 (c (n "valkyrie-docgen") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1wp65kqzdcsfdzr3sbbbv1aa4mfxfsnqfpch0lk2xvi7bcch0dlz")))

(define-public crate-valkyrie-docgen-0.0.2 (c (n "valkyrie-docgen") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0yfd566dgbhmbbk26wiz8k39vqi8qc88z07pnn33mfvywk7nvd97")))

(define-public crate-valkyrie-docgen-0.0.3 (c (n "valkyrie-docgen") (v "0.0.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1swm4768mihiv4m133bix29cpq89anwcy0a3irsn2a122dmd41d6")))

(define-public crate-valkyrie-docgen-0.0.4 (c (n "valkyrie-docgen") (v "0.0.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "02ykggm69dq32w5blizvq9dx9b3rbiva5rw2fdcj4qx38pj16n0r")))

(define-public crate-valkyrie-docgen-0.0.5 (c (n "valkyrie-docgen") (v "0.0.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0bn84pd73wnfrzidlrynci3a4c1i6zaw93j95757x0mcb4cyg108")))

