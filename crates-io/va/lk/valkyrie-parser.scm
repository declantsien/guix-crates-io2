(define-module (crates-io va lk valkyrie-parser) #:use-module (crates-io))

(define-public crate-valkyrie-parser-0.2.0 (c (n "valkyrie-parser") (v "0.2.0") (d (list (d (n "nyar-error") (r "^0.1.4") (d #t) (k 0)) (d (n "nyar-hir") (r "^0.2.2") (d #t) (k 0)) (d (n "peginator") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.6") (d #t) (k 0)))) (h "0f40ffd3sr12c9rw6ccvlql9q4n7lhsfsdxwfjhbbhrnsybppr8p")))

(define-public crate-valkyrie-parser-0.2.1 (c (n "valkyrie-parser") (v "0.2.1") (d (list (d (n "peginator") (r "^0.6.0") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.6") (d #t) (k 0)) (d (n "valkyrie-errors") (r "^0.0.1") (f (quote ("peginator"))) (d #t) (k 0)))) (h "029aazvna78ycnbv3nyn9z8ms25z289ci2d4qbfq870a6rj535zc")))

(define-public crate-valkyrie-parser-0.2.2 (c (n "valkyrie-parser") (v "0.2.2") (d (list (d (n "bit-set") (r "^0.5.3") (d #t) (k 0)) (d (n "lispify") (r "^0.0.1") (d #t) (k 0)) (d (n "pex") (r "^0.1.6") (f (quote ("regex"))) (d #t) (k 0)) (d (n "pratt") (r "^0.4.0") (d #t) (k 0)) (d (n "valkyrie-ast") (r "^0.0.11") (f (quote ("pretty-print" "std"))) (d #t) (k 0)) (d (n "valkyrie-error") (r "^0.0.3") (f (quote ("pex" "pratt"))) (d #t) (k 0)))) (h "0ildi794hmmcv5hhl9r586d6gnvaip1rx0b6d7p5jbay3r8cn93f")))

(define-public crate-valkyrie-parser-0.2.3 (c (n "valkyrie-parser") (v "0.2.3") (d (list (d (n "bit-set") (r "^0.5.3") (d #t) (k 0)) (d (n "lispify") (r "^0.0.1") (d #t) (k 0)) (d (n "pex") (r "^0.1.17") (f (quote ("regex"))) (d #t) (k 0)) (d (n "pratt") (r "^0.4.0") (d #t) (k 0)) (d (n "valkyrie-ast") (r "0.1.*") (f (quote ("pretty-print" "std"))) (d #t) (k 0)) (d (n "valkyrie-error") (r "0.1.*") (d #t) (k 0)))) (h "10g8lfcm8d6hfr0qrkhjw7jshq2kn87rxddi78wc2fi9s6i46ikp")))

(define-public crate-valkyrie-parser-0.2.4 (c (n "valkyrie-parser") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "nyar-error") (r "^0.1.4") (d #t) (k 0)) (d (n "pratt") (r "^0.4.0") (d #t) (k 0)) (d (n "yggdrasil-rt") (r "^0.0.13") (d #t) (k 0)))) (h "027kvm14g899xyb1v3yj24x3j93s5iq05gyq31fr578p73bqncv7")))

(define-public crate-valkyrie-parser-0.2.5 (c (n "valkyrie-parser") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "nyar-error") (r "^0.1.12") (f (quote ("pratt" "yggdrasil-rt" "dashu"))) (d #t) (k 0)) (d (n "pratt") (r "^0.4.0") (d #t) (k 0)) (d (n "valkyrie-ast") (r "0.1.*") (d #t) (k 0)) (d (n "yggdrasil-rt") (r "^0.0.16") (d #t) (k 0)))) (h "0fw38nwwf2rd1k02yd05xqr3mcwrkykc0fzwh2wxbnl1cl6q8sj1")))

