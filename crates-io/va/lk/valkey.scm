(define-module (crates-io va lk valkey) #:use-module (crates-io))

(define-public crate-valkey-0.0.1 (c (n "valkey") (v "0.0.1") (h "1vgwyp8a299fls03i1gmz0i1n12k4yjx5kq4bww7nk84p7mqfs4h") (y #t) (r "1.65")))

(define-public crate-valkey-0.0.2 (c (n "valkey") (v "0.0.2") (h "0v4jgqdzblh5dab23sadcz9v5i7k3gr6zp3gbga9pv6vkfvn9g6p") (r "1.65")))

