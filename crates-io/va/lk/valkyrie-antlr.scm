(define-module (crates-io va lk valkyrie-antlr) #:use-module (crates-io))

(define-public crate-valkyrie-antlr-0.0.0 (c (n "valkyrie-antlr") (v "0.0.0") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "valkyrie-ast") (r "0.1.*") (f (quote ("pretty-print" "std" "lispify"))) (d #t) (k 0)) (d (n "valkyrie-error") (r "0.1.*") (d #t) (k 0)))) (h "08fkcga4sah9rdz3pl2jbkai9m1n8hyzdcdvrgywmzb6s3yhbqkv")))

(define-public crate-valkyrie-antlr-0.0.1 (c (n "valkyrie-antlr") (v "0.0.1") (d (list (d (n "antlr-rust") (r "^0.3.0-beta") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "valkyrie-ast") (r "^0.1.9") (f (quote ("pretty-print" "std" "lispify"))) (d #t) (k 0)) (d (n "valkyrie-error") (r "^0.1.4") (d #t) (k 0)))) (h "0s32i9zs0xk7dypim95zx27br3g4r6ikzl5spmdqzc9qhy5f8yjf")))

