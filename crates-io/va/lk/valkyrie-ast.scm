(define-module (crates-io va lk valkyrie-ast) #:use-module (crates-io))

(define-public crate-valkyrie-ast-0.0.0 (c (n "valkyrie-ast") (v "0.0.0") (h "143yvkglsfymd4qjzlr5k0fa9f0jxjrmxds647gi3gpindczd7ng") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.1 (c (n "valkyrie-ast") (v "0.0.1") (d (list (d (n "syntax-error") (r "^0.0.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "valkyrie-types") (r "^0.0.1") (d #t) (k 0)))) (h "01xkszwrkjisifyi9bs7ykcfs1grr2anpsnfkmdywc5lyd59d45c") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.2 (c (n "valkyrie-ast") (v "0.0.2") (d (list (d (n "syntax-error") (r "^0.0.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "valkyrie-types") (r "^0.0.1") (d #t) (k 0)))) (h "1ymws57z5p8qrkzf5zmkqhqpx497wdqzp7bhvqp07rap08m3nsxq") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.3 (c (n "valkyrie-ast") (v "0.0.3") (h "1b26bqlcbfgfkwpwcf0m2abnvys8a2bbfv9lmcwrhsb5rsfvmyph") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.4 (c (n "valkyrie-ast") (v "0.0.4") (h "01r0gbbcxi0fv9ydr8nqcvf1rs4bls302kz58qbm1p8jkl0nm97y") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.5 (c (n "valkyrie-ast") (v "0.0.5") (h "117a7pm2150x55lmlmaz6mlqbqa0bwv9gzhwdbn7hx1b7d8064r9") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.6 (c (n "valkyrie-ast") (v "0.0.6") (h "196y42vg2zqix61cf2l781nfk611zlyyxbyhzbbxrxsff6rhx5c5") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.7 (c (n "valkyrie-ast") (v "0.0.7") (d (list (d (n "indentation") (r "^0.1.2") (d #t) (k 0)))) (h "1jj8idxvcyd85fgzz7aidj92m88n4mi4cpnjq29apanvlbr87g12") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.8 (c (n "valkyrie-ast") (v "0.0.8") (d (list (d (n "indentation") (r "^0.1.5") (d #t) (k 0)))) (h "1ylx220a8aan0y1i0qiqrpq6yk43g4aj4np04hlja25r8vhkshnv") (f (quote (("default"))))))

(define-public crate-valkyrie-ast-0.0.9 (c (n "valkyrie-ast") (v "0.0.9") (d (list (d (n "pretty-print") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1y5sgr0pi0k2g1ki7pfv7d10z3wsk0jrs832sgdyililmihd9637") (f (quote (("std" "pretty-print/std") ("default" "pretty-print"))))))

(define-public crate-valkyrie-ast-0.0.10 (c (n "valkyrie-ast") (v "0.0.10") (d (list (d (n "pretty-print") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "1frhnimgl9wq86gflbfppw9wf78zz7xasgm2q49g2n2gsa4rbinh") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.0.11 (c (n "valkyrie-ast") (v "0.0.11") (d (list (d (n "pretty-print") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "0bxmgigkviwihbppmvhk2flzywmsmnxiljc73g48zn0xz3gx1whs") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.0.12 (c (n "valkyrie-ast") (v "0.0.12") (d (list (d (n "pretty-print") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "151fpkbyic7ms759gik8aq71xhk4n8dp8d8q73bnnfv4rh2ki0wy") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.0.13 (c (n "valkyrie-ast") (v "0.0.13") (d (list (d (n "pretty-print") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "13b61jl3jfwkp3ah6jfp7pz8nxcpvxgzwfvwcgy1fqxph469bf0w") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.0 (c (n "valkyrie-ast") (v "0.1.0") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "0wfsv5xi8kglj0k8yafaxjn1c8ng0vbaz9wbkafcz76zvc6lg4sg") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.1 (c (n "valkyrie-ast") (v "0.1.1") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "0imd4ligsgcp7x9aaxviwaaki2xybyw0wb6mwkj8bdvb0jdmmcda") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.2 (c (n "valkyrie-ast") (v "0.1.2") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "06s8kzxnb76610xw7fiwqy81gdgvmlvbhjvrs9axcsgq07i92czm") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.3 (c (n "valkyrie-ast") (v "0.1.3") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "18b9si9afx855x0w28v32c3g38jhk62xlvnf1q28nrmb5z8j2kwg") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.4 (c (n "valkyrie-ast") (v "0.1.4") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "06rkrd2ql1pg9pp002rmry7spsr6zldsmsmy1nk87ak83zqyrnpp") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.5 (c (n "valkyrie-ast") (v "0.1.5") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "07p5vr1h5gy6j7b63wsbb3jwjpibydf7d6rsw6fisfbxgymgd284") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.6 (c (n "valkyrie-ast") (v "0.1.6") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "1lddrdn3krpwalwccc8niakpcl39csmw992vwf2p56vp2c0xmqiw") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.7 (c (n "valkyrie-ast") (v "0.1.7") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (k 0)))) (h "16c8ilglc6kj71lx19bzkmnlwsz0jcd6s06n42ccj82a7xsnqkvz") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.8 (c (n "valkyrie-ast") (v "0.1.8") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "lispify") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (k 0)))) (h "1857yhvpmi3n552aadjs0npd1yy31mbk7lxybn3khxv3d9m8jpha") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.9 (c (n "valkyrie-ast") (v "0.1.9") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "lispify") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (k 0)))) (h "158vffjr6yw29wbzs3yhza2y8r6lvqcf3a8snv815l185akjvajb") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.10 (c (n "valkyrie-ast") (v "0.1.10") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "lispify") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (f (quote ("pratt"))) (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (k 0)))) (h "00wkjl1v096krdwwz7ggkax9rj878gd698h7mlvlf7qpv0gkfsl2") (f (quote (("default" "pretty-print")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive" "num-bigint/serde"))))))

(define-public crate-valkyrie-ast-0.1.11 (c (n "valkyrie-ast") (v "0.1.11") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "lispify") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "nyar-error") (r "0.1.*") (f (quote ("pratt"))) (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (o #t) (k 0)))) (h "17iq64202mrpkji3q0gg20jwhgs9gim2jq2mf8kml4vm4mn8x3wi") (f (quote (("default")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

(define-public crate-valkyrie-ast-0.1.12 (c (n "valkyrie-ast") (v "0.1.12") (d (list (d (n "deriver") (r "^0.0.0") (d #t) (k 0)) (d (n "lispify") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "nyar-error") (r "^0.1.12") (f (quote ("pratt"))) (d #t) (k 0)) (d (n "pretty-print") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (o #t) (k 0)))) (h "13h58yj51fkiav0akv7vws58iaqcrkx4jm4yrwck0fn3pfm8bc0r") (f (quote (("default")))) (s 2) (e (quote (("std" "pretty-print?/std" "serde?/std") ("serde" "dep:serde" "serde/derive"))))))

