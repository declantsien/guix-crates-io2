(define-module (crates-io va lk valkeyre) #:use-module (crates-io))

(define-public crate-valkeyre-0.1.0 (c (n "valkeyre") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "08nfrh60z62w9k04w4bpk6lgg24ymila2wgwl3djcc29n42vsax9")))

(define-public crate-valkeyre-0.1.1 (c (n "valkeyre") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "1v4849ac4016nz69g5rl1r1szf5rbmiaqam4z3i234fcirzjsx1y")))

