(define-module (crates-io va lk valkyrie-qualifier) #:use-module (crates-io))

(define-public crate-valkyrie-qualifier-0.1.0 (c (n "valkyrie-qualifier") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "1wpb0haj8ynf7swdqksaydjg7dlgksxjwnmxkksli8h2bp1r0p9z") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.0 (c (n "valkyrie-qualifier") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "0gx7s50in2qaqxcpyyyr00h06113kz3hg1dbpr7rgsbqz6p9grqd") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.1 (c (n "valkyrie-qualifier") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "19a33sn5sj13zx1rbp4rxli6n7i6wdwlsf183szhhv671czb8va0") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.2 (c (n "valkyrie-qualifier") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "0m6jf51s0xhv5kxim4g7mhpyzyqbvilbqyab4hq0ypyx24brvgq0") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.3 (c (n "valkyrie-qualifier") (v "1.0.3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "1wkmwdjiyqkzyad6zffcc6hil894wbvx0bmfhg8v46j0i0zisyc6") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.4 (c (n "valkyrie-qualifier") (v "1.0.4") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "0wpsgqbrm8vsi47gficjiha6xra5rignk504775x2q1l5mza3s1a") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.5 (c (n "valkyrie-qualifier") (v "1.0.5") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "1srn5vv73pvwh61pg3d4vp0rg0d3q1rbk6jii9rgkawl7gr1frsf") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.7 (c (n "valkyrie-qualifier") (v "1.0.7") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "080kgdrz98cywj3ax1fifg4dq6dd8pvwfdlxxa778dmaknvfgn3c") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.8-beta.0 (c (n "valkyrie-qualifier") (v "1.0.8-beta.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "1w1d78hkf489f329g73x1cavkqdsjvcw26p40r6gqgfwny6hna5x") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-valkyrie-qualifier-1.0.8-beta.1 (c (n "valkyrie-qualifier") (v "1.0.8-beta.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (k 0)))) (h "1njqcx9wbf96c9a4gqzxrkja11vnbsynraava79j6qnx2yag988m") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

