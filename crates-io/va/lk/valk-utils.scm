(define-module (crates-io va lk valk-utils) #:use-module (crates-io))

(define-public crate-valk-utils-0.1.0 (c (n "valk-utils") (v "0.1.0") (h "1xivhwjjlxs2968657a28hhxgyxnfd0hdj4j5y77yfvk9w1nbhfm")))

(define-public crate-valk-utils-0.1.1 (c (n "valk-utils") (v "0.1.1") (h "0n04v64bb3amh9h11wbg4h15sqx1rl4wb2rkflap6r81yk7x9kz3") (y #t)))

(define-public crate-valk-utils-0.1.2 (c (n "valk-utils") (v "0.1.2") (h "0k1lw2vfnjzqjdwc1ii5icfn59qgfac8s8xndk8ahd8wc3g9n6za")))

