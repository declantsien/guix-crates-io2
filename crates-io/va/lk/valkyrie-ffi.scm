(define-module (crates-io va lk valkyrie-ffi) #:use-module (crates-io))

(define-public crate-valkyrie-ffi-0.0.0 (c (n "valkyrie-ffi") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "nyar-wasm") (r "^0.0.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "wit-parser") (r "^0.207.0") (d #t) (k 0)))) (h "0gmk5h8i5vpk1bml100qb1siyj3vkc4nnffrcv4bn1w0pi3aj77s")))

