(define-module (crates-io va rb varbincode) #:use-module (crates-io))

(define-public crate-varbincode-0.1.0 (c (n "varbincode") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17vgwalm7a1lryjj565p4rnx5h3hr8vwm5504r92s7j41f8gl4mp")))

