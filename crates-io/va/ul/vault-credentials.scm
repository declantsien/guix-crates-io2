(define-module (crates-io va ul vault-credentials) #:use-module (crates-io))

(define-public crate-vault-credentials-0.1.0 (c (n "vault-credentials") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01wbarjpdmanf3iyx66mh5k0p3ajfr8hk9lxlck29gapwmc0q51m")))

(define-public crate-vault-credentials-0.2.0 (c (n "vault-credentials") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hw9fqkaq00dmzwyfcypvpg2bf1b9g5safi6asj5swqvvpc40qiq")))

(define-public crate-vault-credentials-0.3.0 (c (n "vault-credentials") (v "0.3.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14p555yzmm9cgbwrbx7nxxldh2fyli2b79i8kjgynkdg08is0vcq")))

(define-public crate-vault-credentials-0.4.0 (c (n "vault-credentials") (v "0.4.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bllr0ghz2rdr0l3ghfbpyx4b9z5jfgf973aidgmdvc6lq6ww3k1")))

(define-public crate-vault-credentials-0.4.1 (c (n "vault-credentials") (v "0.4.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bvc2hz80p2730hgs4akyb6sq3q1nvm2hi3sx8fl7b31mgbfl72b")))

(define-public crate-vault-credentials-1.0.0 (c (n "vault-credentials") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "029kwj2b5h1kalcp47b7rwn2jk2rp72ma39a04c8ykwainvrnp7p")))

(define-public crate-vault-credentials-1.0.1 (c (n "vault-credentials") (v "1.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fm38lw3gvzw03rczj95dm1y7wpkd6iy04llxp9zp1d6n6mx2bxs")))

(define-public crate-vault-credentials-1.0.2 (c (n "vault-credentials") (v "1.0.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.63") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z473ry7kcb0l3wrrmdkik4f756nccmyrmgr1qlfq2jdp80c6hy0")))

