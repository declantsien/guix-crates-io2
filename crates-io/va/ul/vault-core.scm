(define-module (crates-io va ul vault-core) #:use-module (crates-io))

(define-public crate-vault-core-0.1.0 (c (n "vault-core") (v "0.1.0") (d (list (d (n "argon2") (r "^0.4.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.9.0") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "14zapczm06z0mbxbsfnmy20yr2jxf41n4jhcpx5rb6z5g9kcds9n") (y #t)))

(define-public crate-vault-core-0.1.1 (c (n "vault-core") (v "0.1.1") (d (list (d (n "argon2") (r "^0.4.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.9.0") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.10.1") (f (quote ("stream"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "0bsz99xzp1wv71xwjhdnd8abl9ykfimw6sbix50jkfildmgzpvk6")))

