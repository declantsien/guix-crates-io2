(define-module (crates-io va ul vault-vars) #:use-module (crates-io))

(define-public crate-vault-vars-0.0.1 (c (n "vault-vars") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "merge-struct") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)))) (h "1034r7ymxp7bm2249rpj332zj1hg28gqv8zsgkn4f5l8mnbms7vr")))

(define-public crate-vault-vars-0.0.2 (c (n "vault-vars") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "merge-struct") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)))) (h "0k86xnmkvlz28jqdpwwfpbkajq325zzhw5wxnrdsk3w7y1wg2sns")))

(define-public crate-vault-vars-0.0.4 (c (n "vault-vars") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "merge-struct") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)))) (h "1wi6ym7i4cg72x5ssdapskr2qm65dl26jx4yqi5nw8d702r168qr")))

