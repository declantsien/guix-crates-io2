(define-module (crates-io va ul vault-ordbok) #:use-module (crates-io))

(define-public crate-vault-ordbok-0.1.0 (c (n "vault-ordbok") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.4.7") (d #t) (k 0)) (d (n "ureq") (r "^1.2.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1i3lsfb88gdxi2av1ain9h6bkvfjgp2ljchk0idyrn61hvjf89bb")))

