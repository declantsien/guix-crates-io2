(define-module (crates-io va ul vaultrs-test) #:use-module (crates-io))

(define-public crate-vaultrs-test-0.1.0 (c (n "vaultrs-test") (v "0.1.0") (d (list (d (n "dockertest") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "vaultrs") (r "^0.4.0") (d #t) (k 0)))) (h "12s919zppvnnjvhhrgvsg8qpqznrr62y1p2bqra4clvqq1ssdsli")))

(define-public crate-vaultrs-test-0.2.0 (c (n "vaultrs-test") (v "0.2.0") (d (list (d (n "dockertest") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)))) (h "0i24na71c07lnz0v22sl3ppk4gbf2i7p61yhb0ialk34gka5437y")))

(define-public crate-vaultrs-test-0.2.1 (c (n "vaultrs-test") (v "0.2.1") (d (list (d (n "dockertest") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)))) (h "0f64xjj2bwnh7y651mh4al28ndv54dz23vbqdfidhn49fffq1xi1")))

(define-public crate-vaultrs-test-0.2.2 (c (n "vaultrs-test") (v "0.2.2") (d (list (d (n "dockertest") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls"))) (k 0)))) (h "1mal1vllqsfaahdjfxjrx90pn30ls5rzsgs22g8a10s52pqh4yy7")))

