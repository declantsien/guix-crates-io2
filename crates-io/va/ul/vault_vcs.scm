(define-module (crates-io va ul vault_vcs) #:use-module (crates-io))

(define-public crate-vault_vcs-1.2.1 (c (n "vault_vcs") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0r2jd6ddy3d7kqd79njwqnl9g8dyr92zj2ka8njzgwbj8ib17mm2")))

(define-public crate-vault_vcs-1.2.2 (c (n "vault_vcs") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0j7ajlfq5zcyqm6bq1jzw6zqcqc146ndjiklgmwmwmzfhsv8d9q3")))

