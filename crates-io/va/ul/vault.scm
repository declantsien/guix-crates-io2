(define-module (crates-io va ul vault) #:use-module (crates-io))

(define-public crate-vault-0.1.0 (c (n "vault") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wsmn7gjm2qwf5kyga5qrji6a7gbrsjbngmkr2vw26vhpixhw7fj")))

(define-public crate-vault-0.1.1 (c (n "vault") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cibdfvimr1d204kch4i20akz665w1fbfpa18xsji3k51h8nhswb")))

(define-public crate-vault-0.1.2 (c (n "vault") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "01qyssn8c9bi8cz4ns4mzyhlk058dv1icz4cjrmj1qn7qf3xixpz")))

(define-public crate-vault-0.1.3 (c (n "vault") (v "0.1.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1rkmfp2q3c7bpcgnjlp81r0qwmzrl2n6rfyqi2zm0n54ax2fayim")))

(define-public crate-vault-0.1.4 (c (n "vault") (v "0.1.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "19mp5ag8q8y8r5s74ximzh02320x0iraxx5v09cq6xcs870xcdcw")))

(define-public crate-vault-0.1.5 (c (n "vault") (v "0.1.5") (d (list (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "0lfm0i47ngqlkiaxrdfsvhnd84nh6z3xg89293c1cjy377xbvcwk") (f (quote (("ffi" "libc"))))))

(define-public crate-vault-0.2.0 (c (n "vault") (v "0.2.0") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "01pmli3vd7j9nkd8s9i0c7cdmpgili8s2cj0a7chhz7gw0dk8ifs") (f (quote (("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-0.2.1 (c (n "vault") (v "0.2.1") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "1l7d3q3y3327gaqg4zfmw5v114lxxlszjsjgkzkzfyp2gjgzh5n7") (f (quote (("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-0.2.2 (c (n "vault") (v "0.2.2") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "0gdk7x8aywrg0mb0fxhsrraifh1j777apyd3gflphxmq4h6bdr3h") (f (quote (("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-0.3.0 (c (n "vault") (v "0.3.0") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "1iplsm69hbyy2ilisl3494z49w3xzc7qil4g46j348ygbxnlsbp8") (f (quote (("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-0.3.1 (c (n "vault") (v "0.3.1") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "1msj2s73wrmc9c9jddxmnhsbwrrdhw31qf4am7817aaa1fwcp1qb") (f (quote (("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-0.3.2 (c (n "vault") (v "0.3.2") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "0qlah7msrg41icf3bi13a2q3f79nn1cvchw1ziyhjv7cqvk8xwyh") (f (quote (("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-0.4.0 (c (n "vault") (v "0.4.0") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (d #t) (k 0)))) (h "10c3swys26zpw9sv924za5kr7wdzh3crvcfk0wbqhf12zyqy1x1b") (f (quote (("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-1.0.0 (c (n "vault") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1sr0cnzixkn3p31dyqn6c3i1fxhr30cx821jk4mffmbgyybyyd8j") (f (quote (("parse-archive" "zip") ("parse-all" "parse-archive") ("ffi" "libc") ("dev" "clippy"))))))

(define-public crate-vault-2.0.0 (c (n "vault") (v "2.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)))) (h "16mqcfi55xwpbh4hnx75db715f1n2zpk8ps8p4mw83x18p91c812") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-2.1.0 (c (n "vault") (v "2.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wkqg7s2mb09b7vc11cac7na977lh5zfmm4v6qpfp3607ljzdj6r") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-2.1.1 (c (n "vault") (v "2.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05sczyw0q47xdirfy8071kcija92kbpxp7bv9xmxfch1diccqwxx") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-3.0.0 (c (n "vault") (v "3.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ipzyjl5bp0qk0azp4rh4pjxb3msy3x6fj4hpkbq0npgnly9a3pi") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-3.0.1 (c (n "vault") (v "3.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02mj5qgprzy507qajgm10zz2ahl3mcf1rwcv4x6yvgi0n4jkvwap") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-4.0.0 (c (n "vault") (v "4.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10lv9xc1g3af28n0p77ks2rr4aaibaf88nvrxgjq7xwkr3hq9igv") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-5.0.0 (c (n "vault") (v "5.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1khrl9qmjpy7ncv45p41wsyb87sqgcph8pnmvh0l7b68cjlh4xf6") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-5.1.0 (c (n "vault") (v "5.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04c1wz4hm3yl41kbgg0li6i1fiyxl8np1di2cwx2pqi7mpyi9yak") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-5.1.1 (c (n "vault") (v "5.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g1ykpmxi2981zn1ngc0qwr6nv73hy9f2bap6ddrdfcc8maajm32") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-6.0.0 (c (n "vault") (v "6.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n993gsgk0l3h9gckp29x37qjxw0c1cyjjl91qgciw8s6glggljn") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-7.0.0 (c (n "vault") (v "7.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n0v704x3xg11mjkhw87vs9671ynq3d3bfbppw5qa2amwqpcx0y8") (f (quote (("trace" "nom-tracable/trace"))))))

(define-public crate-vault-7.1.0 (c (n "vault") (v "7.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "10kd0d1vdcwzdchjmbrcvbakywpsgl9hjx79cpa05sjysjj9hi68") (f (quote (("trace" "nom-tracable/trace")))) (s 2) (e (quote (("serde" "dep:serde" "uuid/serde"))))))

(define-public crate-vault-8.0.0 (c (n "vault") (v "8.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "magnus") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-tracable") (r "^0.9") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "12nqvifs8jlvrvmdjscgy0j5ahkbx3l2fiiq7nhkdjn1kbaja720") (f (quote (("trace" "nom-tracable/trace")))) (s 2) (e (quote (("serde" "dep:serde" "uuid/serde"))))))

