(define-module (crates-io va ul vault-cli) #:use-module (crates-io))

(define-public crate-vault-cli-0.1.0 (c (n "vault-cli") (v "0.1.0") (d (list (d (n "cmd-args") (r "^0.2.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "vault-core") (r "^0.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "0lzgyi6v1lsy0879ggfij8gxd5x4748f5w8dkbjldjfz63p2n6i4") (y #t)))

(define-public crate-vault-cli-0.1.1 (c (n "vault-cli") (v "0.1.1") (d (list (d (n "cmd-args") (r "^0.2.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "vault-core") (r "^0.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "14imny5snsyz9zzz7zkwm0qzmw18nfmviizh34nvw3596r1bm1hm") (y #t)))

(define-public crate-vault-cli-0.1.2 (c (n "vault-cli") (v "0.1.2") (d (list (d (n "cmd-args") (r "^0.2.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "vault-core") (r "^0.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "0jbgq5mrl8bq97ak1bxaff5cq6ivz9835ab05kd0mm5237inqrdv") (y #t)))

(define-public crate-vault-cli-0.1.3 (c (n "vault-cli") (v "0.1.3") (d (list (d (n "cmd-args") (r "^0.2.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "vault-core") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "0cnsjn7s4x9qgkfw8w2ai51006aqjra837j3k6kvxb1gls9zpwk7")))

