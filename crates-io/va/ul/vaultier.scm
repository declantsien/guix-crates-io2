(define-module (crates-io va ul vaultier) #:use-module (crates-io))

(define-public crate-vaultier-0.1.1 (c (n "vaultier") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "vaultrs") (r "^0.7") (d #t) (k 0)))) (h "1lb7acd4ib11mj2xc0nybb4x9wqhw75idx2hbab1kd1ryw58nwdq")))

(define-public crate-vaultier-0.2.0 (c (n "vaultier") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "vaultrs") (r "^0.7") (d #t) (k 0)))) (h "0p03liwa716a5s8jc2q511787y2iq4kwvrxixwpvyk7ay4nra2zn") (f (quote (("write") ("token") ("read") ("full" "token" "auth" "read" "write") ("default" "token" "read") ("auth"))))))

(define-public crate-vaultier-0.2.1 (c (n "vaultier") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "vaultrs") (r "^0.7") (d #t) (k 0)))) (h "177wvfb17n41p8bb3xmz13lv6ww2wbsil1vajw6jlaqclbmrg9m1") (f (quote (("write") ("token") ("read") ("full" "token" "auth" "read" "write") ("default" "token" "read") ("auth"))))))

(define-public crate-vaultier-0.3.0 (c (n "vaultier") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "vaultrs") (r "^0.7") (d #t) (k 0)))) (h "13zgvkwrz2c6w0ca8az7r16p8k9i3pm70n11csx02f4f18v1q1n5") (f (quote (("write") ("token") ("read") ("full" "token" "auth" "read" "write") ("default" "token" "read") ("auth"))))))

