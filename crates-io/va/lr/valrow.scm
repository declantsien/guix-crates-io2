(define-module (crates-io va lr valrow) #:use-module (crates-io))

(define-public crate-valrow-0.0.0-2024-02-12 (c (n "valrow") (v "0.0.0-2024-02-12") (d (list (d (n "abistr") (r "^0.1") (d #t) (k 2)))) (h "1asghhisv983s6dbyg5kfmm87vwadplspwza4pyw7cpr137yb1ia") (f (quote (("std" "alloc") ("intrinsic") ("default" "core") ("core" "intrinsic") ("alloc" "core")))) (y #t) (r "1.71.0")))

(define-public crate-valrow-0.0.0-2024-02-13 (c (n "valrow") (v "0.0.0-2024-02-13") (d (list (d (n "abistr") (r "^0.1") (d #t) (k 2)) (d (n "ialloc") (r "^0.0.0-2023-05-28") (d #t) (k 2)))) (h "0yym41bc42z0n0zd6afkzviyh2g32hxi1hh4p4jr90sscp5d4w37") (f (quote (("std" "alloc") ("intrinsic") ("default" "core") ("core" "intrinsic") ("alloc" "core")))) (r "1.71.0")))

