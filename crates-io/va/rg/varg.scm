(define-module (crates-io va rg varg) #:use-module (crates-io))

(define-public crate-varg-0.0.1 (c (n "varg") (v "0.0.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mr1zc2g266wv7ins3ih9dr0nrjapmp1fc5psji4yv4gbkhbqzmi")))

(define-public crate-varg-0.0.2 (c (n "varg") (v "0.0.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0scb928703ipiy5xq7lxmc6iv7l3nci0rx7c9mhj8m7ybmf2qdnh")))

(define-public crate-varg-0.0.3 (c (n "varg") (v "0.0.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qhk0jm0x5a3a4jf7j0r3p447sp9k30wk8apl1mhay9yy4c6cxl6")))

(define-public crate-varg-0.0.4 (c (n "varg") (v "0.0.4") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07s7sqhdybk4zw6qcz5n3bxp3f12kg136yisvj2mslwc24z59brd")))

(define-public crate-varg-0.0.5 (c (n "varg") (v "0.0.5") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bwclg7xz5xdps0fcybagkqzsnc2zdm1fsam3swzklqn5jclac3m")))

