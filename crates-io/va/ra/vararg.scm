(define-module (crates-io va ra vararg) #:use-module (crates-io))

(define-public crate-vararg-0.1.0 (c (n "vararg") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0g72wq372panadgny0m7gnmfik5xm4m7ml7smia8hvn2rz5zzsnc")))

(define-public crate-vararg-0.1.1 (c (n "vararg") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1n3b0b2ar1m9bsg2n74z97fb2jk2hvm39xblciix8vl8827hsdpn")))

