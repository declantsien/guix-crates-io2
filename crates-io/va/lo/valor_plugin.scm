(define-module (crates-io va lo valor_plugin) #:use-module (crates-io))

(define-public crate-valor_plugin-0.4.0-alpha.2 (c (n "valor_plugin") (v "0.4.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0ngm6xgwrgp5v0jrh0hbf45883s5p9h4660mnmd0dkmb634ccskj")))

(define-public crate-valor_plugin-0.5.0-beta.0 (c (n "valor_plugin") (v "0.5.0-beta.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0pyf1s53cz6smmf32qj6xnw6v8ja0va5jxih6rvln2hjjda50iyr")))

(define-public crate-valor_plugin-0.5.1-beta.0 (c (n "valor_plugin") (v "0.5.1-beta.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "13cjmj750pgqqyzlm636mj91l9bqwbmwr92d2pfd3pi1gg4nh5ym")))

