(define-module (crates-io va lo valor-lib) #:use-module (crates-io))

(define-public crate-valor-lib-0.3.4 (c (n "valor-lib") (v "0.3.4") (d (list (d (n "http-types") (r ">=2.8.0, <3.0.0") (d #t) (k 0)) (d (n "path-tree") (r ">=0.1.12, <0.2.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)) (d (n "vlugin") (r ">=0.3.0, <0.4.0") (d #t) (k 0) (p "valor-vlugin")))) (h "01m5w9ylvwx4pw3frc3y3a09awglrmd0gbxgbmxrjb7bkmscbmap") (f (quote (("web") ("native")))) (y #t)))

