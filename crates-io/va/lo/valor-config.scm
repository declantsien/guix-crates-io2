(define-module (crates-io va lo valor-config) #:use-module (crates-io))

(define-public crate-valor-config-0.0.0 (c (n "valor-config") (v "0.0.0") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde-types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "10prkk1h92a07r2kqxn8gbm0hixh6qm3y7lqkcs9hajhvsiazhd8") (f (quote (("default"))))))

(define-public crate-valor-config-0.1.0 (c (n "valor-config") (v "0.1.0") (d (list (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.16") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde-types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "valkyrie-errors") (r "^0.0.3") (f (quote ("toml" "json5"))) (d #t) (k 0)))) (h "0yvi3sm7d0k5wjgb14w5ahdh4lvbwjsgz7108862jy025j7rr98n") (f (quote (("default"))))))

