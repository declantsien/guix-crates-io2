(define-module (crates-io va lo valora_derive) #:use-module (crates-io))

(define-public crate-valora_derive-0.1.0 (c (n "valora_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "05p3z1qlmr8xjwis3jk6b6xn7hpraqg6hfbvpzrdcs60b7mhp3i3")))

