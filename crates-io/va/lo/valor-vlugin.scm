(define-module (crates-io va lo valor-vlugin) #:use-module (crates-io))

(define-public crate-valor-vlugin-0.3.3 (c (n "valor-vlugin") (v "0.3.3") (d (list (d (n "proc-macro2") (r ">=1.0.24, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k7nh9m0mjg2bxbv40naanvg3xn8q93wvjkbw4xw6gva888lpl39") (y #t)))

