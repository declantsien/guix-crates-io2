(define-module (crates-io va lo valor-rs) #:use-module (crates-io))

(define-public crate-valor-rs-0.2.0-alpha (c (n "valor-rs") (v "0.2.0-alpha") (d (list (d (n "fast-async-mutex") (r "^0.6.3") (d #t) (k 0)) (d (n "http-types") (r "^2.6.0") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "path-tree") (r "^0.1.12") (d #t) (k 0)))) (h "0sniy0v22k8pn93vhjfqwxcmb65pf9jlmcpwbgd44py68hk3zbwa") (y #t)))

(define-public crate-valor-rs-0.3.3 (c (n "valor-rs") (v "0.3.3") (d (list (d (n "http-types") (r ">=2.8.0, <3.0.0") (d #t) (k 0)) (d (n "path-tree") (r ">=0.1.12, <0.2.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)) (d (n "vlugin") (r ">=0.3.0, <0.4.0") (d #t) (k 0) (p "valor-vlugin")))) (h "0x70bd6wh69df6mw72mxpcc0hhaws43cj34y5rrpbmii4xi7l7dv") (f (quote (("web") ("native")))) (y #t)))

