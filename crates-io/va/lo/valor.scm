(define-module (crates-io va lo valor) #:use-module (crates-io))

(define-public crate-valor-0.0.0-test (c (n "valor") (v "0.0.0-test") (d (list (d (n "cgmath") (r "^0.15.0") (d #t) (k 0)) (d (n "gfx") (r "^0.16.1") (d #t) (k 0)) (d (n "gfx_device_gl") (r "^0.14.3") (d #t) (k 0)) (d (n "gfx_text") (r "^0.18.0") (d #t) (k 0)) (d (n "gfx_window_glutin") (r "^0.17.0") (d #t) (k 0)) (d (n "glutin") (r "^0.9.2") (d #t) (k 0)) (d (n "id_tree") (r "^1.1.3") (d #t) (k 0)))) (h "0z2xlsfrml0cgx6h2avv3qzg0f01skrhkq0sk5n5dlgiyvgjlbpg")))

