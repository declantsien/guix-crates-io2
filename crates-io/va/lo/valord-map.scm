(define-module (crates-io va lo valord-map) #:use-module (crates-io))

(define-public crate-valord-map-0.1.0 (c (n "valord-map") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "etcd-client") (r "^0.12.4") (f (quote ("tls"))) (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1mjhfg4mx8av0ddc75vhahxn26ymqcf2kamkx4scid9hahcgspsp") (f (quote (("watcher" "tokio/sync"))))))

(define-public crate-valord-map-0.2.0 (c (n "valord-map") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "etcd-client") (r "^0.12.4") (f (quote ("tls"))) (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1lxcg30fjpl7297kp4dlmljygl3rxmbgys3z25snqr0h0cdalijm")))

(define-public crate-valord-map-0.2.1 (c (n "valord-map") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "etcd-client") (r "^0.12.4") (f (quote ("tls"))) (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1r6gp9way614h9y9vhl3gl9fbd2c512yb7sbr2ickf3dfqzwhc2a")))

(define-public crate-valord-map-0.2.2 (c (n "valord-map") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "etcd-client") (r "^0.12.4") (f (quote ("tls"))) (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0zlzlvyc7ji922mgmyx295a2w552xqgdwaaks7h4sw5rhx3y0mlg")))

(define-public crate-valord-map-0.3.0 (c (n "valord-map") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "etcd-client") (r "^0.12.4") (f (quote ("tls"))) (d #t) (k 2)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0n1p5kx7cs1pxl4bnlvcppgrzr92q574y0sb88xxv6ax0bn5sajg")))

(define-public crate-valord-map-0.4.0 (c (n "valord-map") (v "0.4.0") (d (list (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "18294kpqqmpdr688cj5046q7fcpz12qnlckc5191d4km9nsh59rp")))

