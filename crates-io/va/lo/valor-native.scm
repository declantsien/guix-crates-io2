(define-module (crates-io va lo valor-native) #:use-module (crates-io))

(define-public crate-valor-native-0.3.4 (c (n "valor-native") (v "0.3.4") (d (list (d (n "async-std") (r ">=1.7.0, <2.0.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r ">=2.1.1, <3.0.0") (d #t) (k 0)) (d (n "kv-log-macro") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "libloading") (r ">=0.6.5, <0.7.0") (d #t) (k 0)) (d (n "tide") (r ">=0.15.0, <0.16.0") (d #t) (k 0)) (d (n "uuid") (r ">=0.8.1, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "valor") (r ">=0.3.4, <0.4.0") (f (quote ("native"))) (d #t) (k 0) (p "valor-lib")))) (h "0hy39s0ci1nvcaw5334c3hxwhzg7gwdl2k710vwbkvsa869rffpc") (y #t)))

