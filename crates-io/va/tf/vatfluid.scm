(define-module (crates-io va tf vatfluid) #:use-module (crates-io))

(define-public crate-vatfluid-0.1.0 (c (n "vatfluid") (v "0.1.0") (h "00bwkr6zpa7f51avbajfl4ywxgqj2xkb6376l1x6ps8pv30r8dss")))

(define-public crate-vatfluid-0.2.0 (c (n "vatfluid") (v "0.2.0") (h "1bj8im6a59pywajdc8ifv88x1xp2zc91sl75zwj464wakwcyjwdc")))

(define-public crate-vatfluid-0.3.0 (c (n "vatfluid") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.1.0") (d #t) (k 2)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "03gwmjfqqhg283rv51ykamb5rjgdsignxz9nmk6r4j2r9msnvazi")))

