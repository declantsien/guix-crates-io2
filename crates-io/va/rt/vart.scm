(define-module (crates-io va rt vart) #:use-module (crates-io))

(define-public crate-vart-0.1.0 (c (n "vart") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0z4pmrikxd4djs5dd05iyf77qvlxvsw1942w43v89yca5pr1ppi9")))

(define-public crate-vart-0.1.1 (c (n "vart") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0slzmrf4q4zgp09jl1jbaqss42sxp7kbmk5fbawn9bgaqbifnwz2")))

(define-public crate-vart-0.1.2 (c (n "vart") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nm4sk88iawpzlfavc6q2xavfnqhmjpxl6qjyr75lgwz84xcfkdx") (y #t)))

(define-public crate-vart-0.2.0 (c (n "vart") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1p2srg8xslpclwgkhsw4fypgykyjyh30qm0pa8b3sx629f7q4mjx")))

(define-public crate-vart-0.2.1 (c (n "vart") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0w75kbf16azyv5bz3f6cfqh0ix09mnmanpm28sh8y2hd2jvxn1vc")))

