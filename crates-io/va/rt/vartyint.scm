(define-module (crates-io va rt vartyint) #:use-module (crates-io))

(define-public crate-vartyint-0.1.0 (c (n "vartyint") (v "0.1.0") (h "01s2k9l0z3s49jb4h5jh9bli7hvnyvicbyfw7i6znfdx2byvq783")))

(define-public crate-vartyint-0.2.0-rc1 (c (n "vartyint") (v "0.2.0-rc1") (h "1yy16bahighi4fdv9lj02xcpyjcm805ggn71lwm5x9qz773grjfx")))

(define-public crate-vartyint-0.2.0-rc2 (c (n "vartyint") (v "0.2.0-rc2") (h "1xs5i176iblnq0hsbsmi53xy122fdmifsvbgillx2drl8smpcf5h")))

(define-public crate-vartyint-0.2.0 (c (n "vartyint") (v "0.2.0") (h "0jnllq8r87sman0ss7hxaafasic72caxxx1zxg55mnvxrpj1z17c")))

(define-public crate-vartyint-0.3.0-rc1 (c (n "vartyint") (v "0.3.0-rc1") (h "0klg5v1fa81s1m9ghxrfld9dgqd2ir8y5br3wy9fc35dq6pxz7cr")))

(define-public crate-vartyint-0.3.0 (c (n "vartyint") (v "0.3.0") (h "0yky0a7vq89w29knzx6lx841rkvj9b7dkyn5k3qqi91gdjjazskx")))

