(define-module (crates-io va ru varuint) #:use-module (crates-io))

(define-public crate-varuint-0.1.0 (c (n "varuint") (v "0.1.0") (h "0v3vqql24kjwav3m4axl1qnafw8fm8cq0d5c3xrmis6a51yx6gmb")))

(define-public crate-varuint-0.1.1 (c (n "varuint") (v "0.1.1") (h "07fknqghhskzc259985fijgwf8dwp3g719frm3w0jvdwnwrpwkr5")))

(define-public crate-varuint-0.2.0 (c (n "varuint") (v "0.2.0") (h "06ipai4mrp8fn1xjwkb11llmsqdp9ij9inqi9g413iqjajlg4mn6")))

(define-public crate-varuint-0.3.0 (c (n "varuint") (v "0.3.0") (h "050njs70ny1ffijlkz0n98p4c4ig1n4cf28cic21mpl39n3fxm0c")))

(define-public crate-varuint-0.3.1 (c (n "varuint") (v "0.3.1") (h "1zqzlbrxyl62gig7k76r8h9aj6xws1fanhgh041xkwrmvyja3gcm")))

(define-public crate-varuint-0.3.2 (c (n "varuint") (v "0.3.2") (h "15ykfmilxd2lzj9myzmy87bjnbhyvk89zqjcpw3n65k0q8npb8ln")))

(define-public crate-varuint-0.3.3 (c (n "varuint") (v "0.3.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1y4mb5rs7d6rj48p4c9zs8hpacvcwgsrd7whgj5api5h4l08hprf") (f (quote (("serde-support" "serde" "serde_derive") ("default"))))))

(define-public crate-varuint-0.3.4 (c (n "varuint") (v "0.3.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1sj2rjl9l7i36v0hmsx1irqgh3ri2w1vz4ncg01pgb54vqpm1lpq") (f (quote (("serde-support" "serde" "serde_derive") ("default"))))))

(define-public crate-varuint-0.4.0 (c (n "varuint") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0ldbm53li3vk964a8scx8w4mjfkrqqp0khirlvvd673c4vybddyl") (f (quote (("serde-support" "serde" "serde_derive") ("default"))))))

(define-public crate-varuint-0.5.0 (c (n "varuint") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1jjpp897s6xp60pv811kqvk1krzz3wjnffa2h8zz5b876nq6ga6j") (f (quote (("serde-support" "serde" "serde_derive") ("default"))))))

(define-public crate-varuint-0.5.1 (c (n "varuint") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "12vnny2yaqhiirr50aifa3pv9pq3pmgn72k2zfkv6n940bww1in8") (f (quote (("serde-support" "serde" "serde_derive") ("default"))))))

(define-public crate-varuint-0.6.0 (c (n "varuint") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1ijapykikfcnjffpq8bgi235l6rkg4v8l2kby87s1ar11yxi8r49") (f (quote (("serde-support" "serde" "serde_derive") ("default"))))))

(define-public crate-varuint-0.7.0 (c (n "varuint") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "02j9qjc4b468jqzcc6cz6wcdxkhnyls9q6dnsg9czy4qqrxzmhxx") (f (quote (("serde-support" "serde" "serde_derive") ("default"))))))

(define-public crate-varuint-0.7.1 (c (n "varuint") (v "0.7.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0783ndh4cr1gmas173cayxgmn47dnrvd33qs0i4f5bhs02zvd1xa") (f (quote (("default")))) (s 2) (e (quote (("serde-support" "dep:serde" "dep:serde_derive"))))))

