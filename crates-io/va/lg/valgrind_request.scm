(define-module (crates-io va lg valgrind_request) #:use-module (crates-io))

(define-public crate-valgrind_request-1.0.0 (c (n "valgrind_request") (v "1.0.0") (h "1sh56qh7ch1qml72mayi3p0qv7rh84nrimn2llf1nxc4j9b98yy9")))

(define-public crate-valgrind_request-1.1.0 (c (n "valgrind_request") (v "1.1.0") (h "16m941mac859gvw1fg99nhvmz02cwj4chfa4wd816gj72jdi7yyh")))

