(define-module (crates-io va lg valgrind) #:use-module (crates-io))

(define-public crate-valgrind-0.1.0 (c (n "valgrind") (v "0.1.0") (h "0qwrm1xaqlma6sn4272pf1z9qf57g4czqj94435f0s9fqavj38b8")))

(define-public crate-valgrind-0.1.1 (c (n "valgrind") (v "0.1.1") (h "0mvcfadlbf8i8skbrkp8717pzpab5icrpsfhhdbk7cwv504v30mz")))

(define-public crate-valgrind-0.1.2 (c (n "valgrind") (v "0.1.2") (h "109dxz5iv5x78a8nadciv85rmzrq8nniwvzc1p3jrhb2qnv5804q")))

