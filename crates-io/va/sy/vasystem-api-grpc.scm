(define-module (crates-io va sy vasystem-api-grpc) #:use-module (crates-io))

(define-public crate-vasystem-api-grpc-0.1.0 (c (n "vasystem-api-grpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0ay4grlxh9anv1gbaad6171fk5k18qks3kgvznaap7r5fr4gzwi7")))

(define-public crate-vasystem-api-grpc-0.1.12 (c (n "vasystem-api-grpc") (v "0.1.12") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0bnwl1bl46waxln2ls99v2ahiickbrjq3k18y6gk512ya7h3al3n")))

(define-public crate-vasystem-api-grpc-0.1.13 (c (n "vasystem-api-grpc") (v "0.1.13") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0npxlgi8s5fsx04mgqp7na57lbdlpijyhr7ffi9kaq1glglicgvc")))

(define-public crate-vasystem-api-grpc-0.1.14 (c (n "vasystem-api-grpc") (v "0.1.14") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0aczs6q3dfb3fkadld0iafpa7p53jw7z6naw35da561jdz12b9b0")))

(define-public crate-vasystem-api-grpc-0.1.15 (c (n "vasystem-api-grpc") (v "0.1.15") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "0lwl0ff0l9ibw1qc5j938v0dra1bmp5rl80r3c6d44fg5bcqhrcd")))

(define-public crate-vasystem-api-grpc-0.1.16 (c (n "vasystem-api-grpc") (v "0.1.16") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "08y1fyvjfd89ni2wzcsrcqnja467sa0n9ib6psp3ga7mjfrm50ji")))

(define-public crate-vasystem-api-grpc-0.1.17 (c (n "vasystem-api-grpc") (v "0.1.17") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 1)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "0092xx8q22m8pf23f6nk91lmq5fskar223y52rxxi1gbrbgbfja5")))

