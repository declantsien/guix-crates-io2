(define-module (crates-io va li valid_rust_char) #:use-module (crates-io))

(define-public crate-valid_rust_char-0.1.0 (c (n "valid_rust_char") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 1)))) (h "0plc2mzd11vb4zman0lrmm59jz6l2h7m0xnfnmwq595wrriw20aa") (y #t)))

(define-public crate-valid_rust_char-0.1.1 (c (n "valid_rust_char") (v "0.1.1") (h "1pl598s9ygvfjn3r2zhvq5nv70q50n1ynyhvyr5zpfa2qr89azs3")))

