(define-module (crates-io va li validaten) #:use-module (crates-io))

(define-public crate-validaten-0.1.0 (c (n "validaten") (v "0.1.0") (d (list (d (n "checkluhn") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "idna") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0v2qla1ifkz0qgals24xgy7c17lfy2lh70y3l6yybmsj34qz0y13") (f (quote (("validaten-all" "crypto" "hashes" "creditcard" "networks" "internet") ("networks" "regex") ("internet" "idna" "lazy_static" "regex") ("hashes" "lazy_static" "regex") ("crypto" "lazy_static" "regex") ("creditcard" "lazy_static" "checkluhn" "regex"))))))

