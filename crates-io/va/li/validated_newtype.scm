(define-module (crates-io va li validated_newtype) #:use-module (crates-io))

(define-public crate-validated_newtype-0.1.0 (c (n "validated_newtype") (v "0.1.0") (d (list (d (n "serde") (r "~1.0") (o #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "0gcvdv55zzxs7za7bkknf8m6qfn3zxs412ywn6x01bnh2bcqkjk3") (f (quote (("default" "serde"))))))

(define-public crate-validated_newtype-0.1.1 (c (n "validated_newtype") (v "0.1.1") (d (list (d (n "serde") (r "~1.0") (o #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "0bz15xcdsdz39b4gc198xsjbswp3j9habdxmc27y9j8jvjiiq3qn") (f (quote (("default" "serde"))))))

