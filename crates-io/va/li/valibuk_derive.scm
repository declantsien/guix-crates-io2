(define-module (crates-io va li valibuk_derive) #:use-module (crates-io))

(define-public crate-valibuk_derive-0.1.0 (c (n "valibuk_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)) (d (n "valibuk_core") (r "^0.1.0") (d #t) (k 0)))) (h "1s8m1zrg8wpfv4ibilwbdmbpwggd7a5yhphp8yh2g0g49cw41qgg")))

(define-public crate-valibuk_derive-0.2.0 (c (n "valibuk_derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)) (d (n "valibuk_core") (r "^0.2.0") (d #t) (k 0)))) (h "1ccb05wrgvf3ffmfndvx0n9c2ibg2x3ns7w9nc0s80nmd7x1sqv6")))

