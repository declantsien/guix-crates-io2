(define-module (crates-io va li validiter) #:use-module (crates-io))

(define-public crate-validiter-0.1.0 (c (n "validiter") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cmpcag0sj2cc3hwvbczl168grv7n5brvli7hlg25w0m3pjrrxh5")))

(define-public crate-validiter-0.1.1 (c (n "validiter") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10qix54wn172p45qyyipsql1l6n74cnhym182rcf277c7by7pccd")))

