(define-module (crates-io va li validations) #:use-module (crates-io))

(define-public crate-validations-0.1.0 (c (n "validations") (v "0.1.0") (h "1zayq1sa9sb8mi4r7y0y9w53ybi0r7v59807id6y5mw6bn162kf5")))

(define-public crate-validations-0.1.1 (c (n "validations") (v "0.1.1") (h "1ar5lp65zgk5vp7mzamk54rdsw2vydffprqa20mab0jillqm3wdl")))

