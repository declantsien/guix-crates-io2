(define-module (crates-io va li validate-graphql-schema) #:use-module (crates-io))

(define-public crate-validate-graphql-schema-0.1.0 (c (n "validate-graphql-schema") (v "0.1.0") (d (list (d (n "async-graphql-parser") (r "^6.0.9") (d #t) (k 0)) (d (n "async-graphql-value") (r "^6.0.9") (d #t) (k 0)))) (h "0z97q7ax12585krpxqz35c3i87694d2l1i2yyivr41fhvb1q0cz9")))

