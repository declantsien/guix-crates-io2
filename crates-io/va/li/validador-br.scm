(define-module (crates-io va li validador-br) #:use-module (crates-io))

(define-public crate-validador-br-0.1.0 (c (n "validador-br") (v "0.1.0") (h "0c0dssqfyafad0dmnxhgxfahs8cazpdc04f5wd309lh70mgxyyls")))

(define-public crate-validador-br-0.1.1 (c (n "validador-br") (v "0.1.1") (h "1q3gw4knym23yg2w77ag265v0fc49019nzd9ad21mj5bwy91jna3")))

(define-public crate-validador-br-0.1.2 (c (n "validador-br") (v "0.1.2") (h "0xf8d4bqzfn8l1cmd97wz76gnvhxgchiakjj7j2855znyafpv9ls")))

(define-public crate-validador-br-0.1.3 (c (n "validador-br") (v "0.1.3") (h "10c28x6lrrdr70i36mrpx7ywg4kr56s3rhczwllcjnlggsa6dzyc")))

