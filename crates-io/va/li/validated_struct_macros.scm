(define-module (crates-io va li validated_struct_macros) #:use-module (crates-io))

(define-public crate-validated_struct_macros-0.1.0 (c (n "validated_struct_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1z43mzc5incgvj321dqf3jsxf7nnbcyfbaq4q5dgn3dkrh514dd1") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-0.1.1 (c (n "validated_struct_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0yqz6pma6mnkgxghyhzq27gnll4ywsz1i5flccw8lajm0p9m0gqm") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-0.1.2 (c (n "validated_struct_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0gqzk0y4ac3y55qv8k8lyfcc3mcqbx4mxwi5mhlbl5a7k7yjv1dy") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-0.1.4 (c (n "validated_struct_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1nzgf10lqbywgqv4w8hvp3wkxl88q5cgqxl0xc7g005zz79gxfkm") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-0.1.5 (c (n "validated_struct_macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "08v92sg0bx1v6by79md8d4j6ybzkn14vcs1593j3ddhfc2q7jfgf") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-0.1.8 (c (n "validated_struct_macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0sr9qlzvafws29q2b2x7wzn52l1xzsvr207bxzprflhwj7g9khin") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-0.1.9 (c (n "validated_struct_macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "09dk5j6rg5sdrc30y4qj0qqwfwppnm8jwlaq0gamlbkmrgzqm5vc") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-0.1.10 (c (n "validated_struct_macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0vsprvvl2qd75dygb3pm3g3m6lijs2c984ykgvbcfa7cvlx9ys6a") (f (quote (("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-1.0.0 (c (n "validated_struct_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0gw09rdp9pb34jmf2q165mlj2hrlq18hp3byjybyjqfb20l7fbdw") (f (quote (("serde_json") ("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-1.1.0 (c (n "validated_struct_macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0vxh4z56arx5nlipq7w09xzl7szzkqf9p9siz6kk0vpsgcqi9hxa") (f (quote (("serde_json") ("serde") ("dot_separator")))) (y #t)))

(define-public crate-validated_struct_macros-2.0.0 (c (n "validated_struct_macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1fsib2kc49pdl6r2wc5cc6ggxy7k5yrkzf46zmxfc66v8k7pqhpj") (f (quote (("serde_json") ("serde") ("dot_separator"))))))

(define-public crate-validated_struct_macros-2.1.0 (c (n "validated_struct_macros") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1d7d46y732v9aa5wgacnm6kra17cabcz98y2546yzadgh2ll8i4x") (f (quote (("serde_json") ("serde") ("dot_separator"))))))

