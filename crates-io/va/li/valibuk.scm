(define-module (crates-io va li valibuk) #:use-module (crates-io))

(define-public crate-valibuk-0.1.0 (c (n "valibuk") (v "0.1.0") (d (list (d (n "valibuk_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15b29s1ldb6i5wm7a4pylh67116mwggc0ggb434rvz9cfda6h0q0")))

(define-public crate-valibuk-0.2.0 (c (n "valibuk") (v "0.2.0") (d (list (d (n "valibuk_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0wsqzzvv133iqnfdgi6vv58kxgmvmvvs690nmcl3xh2y3j66biip")))

