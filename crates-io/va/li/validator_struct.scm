(define-module (crates-io va li validator_struct) #:use-module (crates-io))

(define-public crate-validator_struct-0.1.0 (c (n "validator_struct") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02ch6nknvq36z2384l64ch83fdi1zmliqbkaxhj3ir18z413idf3")))

(define-public crate-validator_struct-0.2.0 (c (n "validator_struct") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17ryhqd8r6kckv1njfiqkmn876a9vlwq1vppylbp2hb3dlh98828")))

(define-public crate-validator_struct-0.2.1 (c (n "validator_struct") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hqp0glqhfpkbc06566qrr2sz93fnl1853i3wy94xw6gd1da8v03")))

(define-public crate-validator_struct-0.2.2 (c (n "validator_struct") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xc3c1canza090qkrrci8cv5va6wlrxpm31n42nb1hc9i7d315hg")))

(define-public crate-validator_struct-0.3.0 (c (n "validator_struct") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)) (d (n "validator") (r "^0.18.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lwbnbgi6pf7a4vbifqyhpxcrv6sk3cxzya8xxh7kq1gsfy451q6")))

(define-public crate-validator_struct-0.3.1 (c (n "validator_struct") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)) (d (n "validator") (r "^0.18.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w8s7wa0krmxqxi9aliws3sbdlxdhsv5na0jm45148wzj0a52max")))

