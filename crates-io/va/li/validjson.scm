(define-module (crates-io va li validjson) #:use-module (crates-io))

(define-public crate-validjson-0.1.0 (c (n "validjson") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "valico") (r "^3.1.0") (d #t) (k 0)))) (h "1635km4l37akb3amy7ggci4ckwcf17ymqnmjcbhn8ih67mvjxgkm")))

(define-public crate-validjson-0.1.1 (c (n "validjson") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "valico") (r "^3.1.0") (d #t) (k 0)))) (h "1smr16r9pcsilnz93w3fa142kp383davzc677wkkn2n2zy2dp3rc")))

(define-public crate-validjson-0.1.2 (c (n "validjson") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "valico") (r "^3.1.0") (d #t) (k 0)))) (h "0ymdya2cwsvjchsgl2m7lf8x0bq49bgdfsc07anrjg6m065rgxdv")))

(define-public crate-validjson-0.1.3 (c (n "validjson") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "valico") (r "^3.1.0") (d #t) (k 0)))) (h "13i89sk99a565898jwqp2j9xbikiszjsxrdlxp09mbiwa8qk76j7")))

(define-public crate-validjson-0.1.4 (c (n "validjson") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "valico") (r "^3.1.0") (d #t) (k 0)))) (h "09h37p5gwrg31mj93d7jb4ibcadgsy06vms66m6d9br0s1hry6b5")))

(define-public crate-validjson-0.1.5 (c (n "validjson") (v "0.1.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "valico") (r "^3.1.0") (d #t) (k 0)))) (h "16334n75dj3ffds3ca7i2v753awx6pp150qhw6khakp5v24c3x8h")))

(define-public crate-validjson-0.1.6 (c (n "validjson") (v "0.1.6") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "valico") (r "^3.1.0") (d #t) (k 0)))) (h "0fzxxlmfflw8czqf9pqlcix2zcd7ad12fvzxn87nrjwkbp2hk15h")))

