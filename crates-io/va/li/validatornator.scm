(define-module (crates-io va li validatornator) #:use-module (crates-io))

(define-public crate-validatornator-0.1.0 (c (n "validatornator") (v "0.1.0") (h "1rzfg0mhd6z4m9g47aa31pl6dqzn4lk3kgli6bypypcn8bnxbdck")))

(define-public crate-validatornator-0.2.0 (c (n "validatornator") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "16ypx1zgapqhqaz6zqfw6p1jjk5v7grgqwg8lh4719s9f5h0jkmi")))

(define-public crate-validatornator-0.3.0 (c (n "validatornator") (v "0.3.0") (d (list (d (n "rust_decimal") (r "^1.35.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.34.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "10za4l28bclj4xbxrg3ggmv3mf84m77vzcnhrs008ssmx1whqxv5")))

(define-public crate-validatornator-0.4.0 (c (n "validatornator") (v "0.4.0") (d (list (d (n "rust_decimal") (r "^1.35.0") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.34.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1hs84hq6cxw6pm6bczklgqvw5r9w6dyb6id5fc2kb32q3ldrc9p0")))

