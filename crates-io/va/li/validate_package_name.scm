(define-module (crates-io va li validate_package_name) #:use-module (crates-io))

(define-public crate-validate_package_name-0.1.0 (c (n "validate_package_name") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0cqjr123cvv01c7gy8qwzmgb077xh739ggmaw186yvqcjlshcvkn") (y #t)))

(define-public crate-validate_package_name-0.1.1 (c (n "validate_package_name") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1x8jygsg5jw67y1mpkba370zq1xn14q24bv85dw2xprcw5cvql51")))

(define-public crate-validate_package_name-0.1.2 (c (n "validate_package_name") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0fnmlf8y0za53xa3k8mm74z76kkymx2956zskcdglphh7vg3f7dx")))

