(define-module (crates-io va li validador_crate_rust) #:use-module (crates-io))

(define-public crate-validador_crate_rust-0.1.1 (c (n "validador_crate_rust") (v "0.1.1") (h "1b7y7g71dfymz9707gjq72wj84qzarjgxfn7bby6wmxi6qxvsha9")))

(define-public crate-validador_crate_rust-1.0.0 (c (n "validador_crate_rust") (v "1.0.0") (h "0n79c3f3wi50skcbllxwaqym3ldac392qbwqqc2ib0kiqpchknf8")))

