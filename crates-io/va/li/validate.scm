(define-module (crates-io va li validate) #:use-module (crates-io))

(define-public crate-validate-0.0.1 (c (n "validate") (v "0.0.1") (h "01v187ppn827qkigmhxcssb1xfvm0aw5578whb51zgvp31g5q4gr")))

(define-public crate-validate-0.1.0 (c (n "validate") (v "0.1.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "0zrl56n293r3bxc6bgfg4nlmzwfaallbs9spz7jgfwx5x6qq23aj")))

(define-public crate-validate-0.2.0 (c (n "validate") (v "0.2.0") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "1a5r12a941mcqwny60fbphxd5d5wa6162p7gqk0n84ihigq8giqq")))

(define-public crate-validate-0.2.1 (c (n "validate") (v "0.2.1") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "1ws7v1kd72xhgs3wc2350dcmawnda06lc27620022g3ldz5kpab9")))

(define-public crate-validate-0.3.0 (c (n "validate") (v "0.3.0") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "18myhcd70d61fhivyj7gb9fgxkwxqysnh27p2rmkx6087c37pf0p")))

(define-public crate-validate-0.3.1 (c (n "validate") (v "0.3.1") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "04r974q5hxv5546939ywnczz9g1w3xvbh54w3vcn930xz0pjbm9k")))

(define-public crate-validate-0.4.0 (c (n "validate") (v "0.4.0") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "1775a9bpjs2jx6ybbpqagv1fhal596vgrc4dbh2va7f5ss66zjy5")))

(define-public crate-validate-0.4.1 (c (n "validate") (v "0.4.1") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "1v4x5n9mxxnfsfc5f8dyi8jklsgi6jkzr35dl0amxhcm378nlkgr")))

(define-public crate-validate-0.5.0 (c (n "validate") (v "0.5.0") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "0yi80wffd45jpkpsp3g3dxmb9lwwgm6a9a95fzhaj0wkzjy2p67m")))

(define-public crate-validate-0.6.0 (c (n "validate") (v "0.6.0") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "1fk955acpskha50kbhcrjfm9i9kj4vxkhqyqskh1a79b7925y5cl")))

(define-public crate-validate-0.6.1 (c (n "validate") (v "0.6.1") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "0r04wgwp1h78dipyzd4xscmps4aaf3d5x9430acnh0grajkjj60a")))

