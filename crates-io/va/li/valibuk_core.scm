(define-module (crates-io va li valibuk_core) #:use-module (crates-io))

(define-public crate-valibuk_core-0.1.0 (c (n "valibuk_core") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)) (d (n "assert_tokens_eq") (r "^0.1.0") (d #t) (k 2)) (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)))) (h "0vbh56f17dgmfqxc313cfvnwpqvb0zx87qgbqah9d4dfzab5cd47")))

(define-public crate-valibuk_core-0.2.0 (c (n "valibuk_core") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)) (d (n "assert_tokens_eq") (r "^0.1.0") (d #t) (k 2)) (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)))) (h "1hv5dq0x8i41jnsf18j0ygfsn73x6arrhkm83snbrmsffk6xyd4l")))

