(define-module (crates-io va li validator_types) #:use-module (crates-io))

(define-public crate-validator_types-0.11.0 (c (n "validator_types") (v "0.11.0") (h "12vkzhn5yy4kvb2w2akablyraiv182syhv61n5v1zb2hg7d29lxd") (f (quote (("unic") ("phone") ("card"))))))

(define-public crate-validator_types-0.12.0 (c (n "validator_types") (v "0.12.0") (h "02j7b8v5hr1jxya19ibb3gb607g9vhfsypnx3lnaycziimh815md") (f (quote (("unic") ("phone") ("card"))))))

(define-public crate-validator_types-0.14.0 (c (n "validator_types") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14d2lqqxksyy8bpn2qh3wcnnx2047i0fdfpkylr7ccj23mzdknfy") (f (quote (("unic") ("phone") ("card"))))))

(define-public crate-validator_types-0.15.0 (c (n "validator_types") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ad3ilkwdx1vcwi7vbapkz8kqp9gdrfb34qlzjmlfs19jd1g7pfj") (f (quote (("unic") ("phone") ("card"))))))

(define-public crate-validator_types-0.16.0 (c (n "validator_types") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qqisv2ckmfbh7b06n8f9kg0bp7qpbl380ciqm4ihlbj03ivy6hi") (f (quote (("unic") ("phone") ("card"))))))

