(define-module (crates-io va li validated) #:use-module (crates-io))

(define-public crate-validated-0.1.0 (c (n "validated") (v "0.1.0") (d (list (d (n "nonempty") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "053rs1k3smkwyj8kgn90vq1sapvf1fwalp0ss5bx6ay64xdz2mfx")))

(define-public crate-validated-0.1.1 (c (n "validated") (v "0.1.1") (d (list (d (n "nonempty") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "05hwsmf6qic71sih01vp6gc60xlfbkxypwdljk6qr1pam9i50pkg")))

(define-public crate-validated-0.2.0 (c (n "validated") (v "0.2.0") (d (list (d (n "nonempty") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1bh30j9j0hfqb0lgc5vz7s3l8p0gxb0l15ajmnqf9k2agcqrw6x9")))

(define-public crate-validated-0.3.0 (c (n "validated") (v "0.3.0") (d (list (d (n "nonempty") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1dxrwsk540fhws1s4cczwmxh2gj99vfk79sswvf40p17z6cxm93h")))

(define-public crate-validated-0.4.0 (c (n "validated") (v "0.4.0") (d (list (d (n "nonempty-collections") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1aa052cny3w8sdslc30p6snm7y3l09lgbzzij3dsi8s4y9jh0kvk")))

(define-public crate-validated-0.4.1 (c (n "validated") (v "0.4.1") (d (list (d (n "nonempty-collections") (r "^0.2.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1lrq7rg3zack625b50lak5y9xwm191n9akfh8javxb54k3jzymb6")))

