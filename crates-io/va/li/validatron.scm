(define-module (crates-io va li validatron) #:use-module (crates-io))

(define-public crate-validatron-0.1.0 (c (n "validatron") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "validatron_derive") (r "^0.1.0") (d #t) (k 0)))) (h "04a32w6mayna2qpvkcwdk1rc6q4n5wqfr74dxva4wqsxfpflky8v") (f (quote (("use-serde" "serde") ("default" "use-serde"))))))

(define-public crate-validatron-0.2.0 (c (n "validatron") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "validatron_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1h0km6719b6lfhvrxpzc3aardqychqhv42p22sq0hdvkncyn58bz") (f (quote (("use-serde" "serde") ("default" "use-serde"))))))

(define-public crate-validatron-0.2.1 (c (n "validatron") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "validatron_derive") (r "^0.2.1") (d #t) (k 0)))) (h "1i1vrfxjy926bkxbvas2n6w9m3kq5cf5h3k0xkpcfsjz8r62pvxh") (f (quote (("use-serde" "serde") ("default" "use-serde"))))))

(define-public crate-validatron-0.3.0 (c (n "validatron") (v "0.3.0") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "validatron_derive") (r "^0.3.0") (d #t) (k 0)))) (h "0zakn9rcb8b78ndsh4ryc57aprlgqkh6g1ci4pqpxg0s32gpxczw") (f (quote (("use-serde" "serde") ("use-indexmap" "indexmap") ("default" "use-serde"))))))

(define-public crate-validatron-0.4.0 (c (n "validatron") (v "0.4.0") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "validatron_derive") (r "^0.4.0") (d #t) (k 0)))) (h "0gqkbzddvcg6b7m38hk1a4xjxkssvxap5s02jx8xxph06lr4zxqd") (f (quote (("use-serde" "serde") ("use-indexmap" "indexmap") ("default" "use-serde"))))))

(define-public crate-validatron-0.5.0 (c (n "validatron") (v "0.5.0") (d (list (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "validatron_derive") (r "^0.5.0") (d #t) (k 0)))) (h "1ckgyg07q8irgbwgdcwhk5ziqgkdaxln634y6m0mhwd5asg4gakr") (f (quote (("use-serde" "serde") ("use-indexmap" "indexmap") ("default" "use-serde"))))))

