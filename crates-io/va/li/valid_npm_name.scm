(define-module (crates-io va li valid_npm_name) #:use-module (crates-io))

(define-public crate-valid_npm_name-0.1.0 (c (n "valid_npm_name") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1xmznkkwhanbvdzcg4vjnspcapg4sbj4knz70kk45bfk0cpk247d")))

(define-public crate-valid_npm_name-0.1.1 (c (n "valid_npm_name") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "17dli3ws1hcym19w1r68m7hwa7zyw43fzzmq9y15gv7pf5j3g3q1")))

