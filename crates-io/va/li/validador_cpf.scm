(define-module (crates-io va li validador_cpf) #:use-module (crates-io))

(define-public crate-validador_cpf-1.0.0 (c (n "validador_cpf") (v "1.0.0") (h "1djnzk90dlx6l9ywca53c76ap27qalkaf5n4hwss7xhf28s6h0by") (y #t)))

(define-public crate-validador_cpf-1.0.1 (c (n "validador_cpf") (v "1.0.1") (h "1mkapkmiacncvr4y4j0ba16d9jivb172ql1jqkpxjqvjrgldp0yr") (y #t)))

(define-public crate-validador_cpf-1.0.2 (c (n "validador_cpf") (v "1.0.2") (h "0lni3zg0qkmmrdcfg0ndv9zb6pyaahnvnb6d4brs5c5h3zqn2an2") (y #t)))

(define-public crate-validador_cpf-1.0.3 (c (n "validador_cpf") (v "1.0.3") (h "1vbm0jf2piip4wkfl0dzjjhdlxaz5w6866zs4g42s4831llpila7")))

