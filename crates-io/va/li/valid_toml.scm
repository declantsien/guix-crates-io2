(define-module (crates-io va li valid_toml) #:use-module (crates-io))

(define-public crate-valid_toml-0.0.1 (c (n "valid_toml") (v "0.0.1") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "shareable") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "~0.1") (d #t) (k 0)))) (h "1989mjd7cr4cv04n0ijhry075m522kjmm7xgbhbsac8y7z39ijgk")))

(define-public crate-valid_toml-0.0.2 (c (n "valid_toml") (v "0.0.2") (d (list (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "shareable") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "~0.1") (d #t) (k 0)))) (h "1pn4x9mx1p6lnd6wywismjqvfr3rnri08pwn4mbcjwzynflz73rg")))

