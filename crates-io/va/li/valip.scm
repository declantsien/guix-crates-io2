(define-module (crates-io va li valip) #:use-module (crates-io))

(define-public crate-valip-0.1.0 (c (n "valip") (v "0.1.0") (h "1ddi5xvjgq221gg3wmkpcwg4njsci809x8ckq4m350aryp3j95q0")))

(define-public crate-valip-0.2.0 (c (n "valip") (v "0.2.0") (h "1wl9fyyg9ryz80zr3cjkk7zpdascyv54mly1xns1fjynmqnd8iln") (y #t)))

(define-public crate-valip-0.2.1 (c (n "valip") (v "0.2.1") (h "0cm1ab83rsi96bfbnmnf2229l36blcnsyzmgbqb2rnb2m3rvjpwp")))

(define-public crate-valip-0.3.0-rc1 (c (n "valip") (v "0.3.0-rc1") (h "1s5y6xgs2aafjf5mvj1hw89hz085mr83hcgap713hwqanmnr3mmx") (y #t)))

(define-public crate-valip-0.3.0-rc2 (c (n "valip") (v "0.3.0-rc2") (h "1zhqzgg3s32ys1sjnj3jbg7jxhs2hfdpa790793saj23nvfdfqs9")))

(define-public crate-valip-0.3.0 (c (n "valip") (v "0.3.0") (h "0yfgi7h1gx2i6fm2avqi6d2bpjr5r4pkfd29lwl6ggxmq1fgigqr")))

(define-public crate-valip-0.4.0-rc1 (c (n "valip") (v "0.4.0-rc1") (h "1hdhv1zrh6h8lz8z3aw6dkvl819qd34mmg3xxzmdmhkv819qh6rl")))

(define-public crate-valip-0.4.0 (c (n "valip") (v "0.4.0") (h "10dy82h6pirsh7gn6n06qn70nli5hjrv88kqbi78y9fg3zwb3fnz")))

