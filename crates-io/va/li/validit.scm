(define-module (crates-io va li validit) #:use-module (crates-io))

(define-public crate-validit-0.1.0 (c (n "validit") (v "0.1.0") (d (list (d (n "anyerror") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "0xbsnlx1v7zwm2g5gzvx8539v13i85lnywxy30bn1v94g00jndz3") (f (quote (("default" "macros") ("backtrace" "anyerror/backtrace")))) (s 2) (e (quote (("macros" "dep:anyerror"))))))

(define-public crate-validit-0.1.1 (c (n "validit") (v "0.1.1") (d (list (d (n "anyerror") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "0zm8zlgg1d4q5dra7c6bmxxjfbc4zjdx8hpchzjrv4z98cqfgv7b") (f (quote (("default" "macros") ("backtrace" "anyerror/backtrace")))) (s 2) (e (quote (("macros" "dep:anyerror"))))))

(define-public crate-validit-0.2.0 (c (n "validit") (v "0.2.0") (d (list (d (n "anyerror") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "09vj5rxa4j4nm15ca2bkiqlr22sa6ma9kqd816l5z5i4xiy1vr1n") (f (quote (("default" "macros") ("backtrace" "anyerror/backtrace")))) (y #t) (s 2) (e (quote (("macros" "dep:anyerror"))))))

(define-public crate-validit-0.2.1 (c (n "validit") (v "0.2.1") (d (list (d (n "anyerror") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "0fb1827plyjimrm4ii3bp7jjfxq958mzcgy6c8m1qyqykyjb9lf0") (f (quote (("default" "macros") ("backtrace" "anyerror/backtrace")))) (y #t) (s 2) (e (quote (("macros" "dep:anyerror"))))))

(define-public crate-validit-0.2.2 (c (n "validit") (v "0.2.2") (d (list (d (n "anyerror") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "08s7phrn4s4bvl4rrrmqzybv0530rfz71af62yayjl3gfjy10npr") (f (quote (("default" "macros") ("backtrace" "anyerror/backtrace")))) (s 2) (e (quote (("macros" "dep:anyerror"))))))

(define-public crate-validit-0.2.3 (c (n "validit") (v "0.2.3") (d (list (d (n "anyerror") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "1i13ghwbp02fphsxzn4h2fi9dzz3hxzj84225hham4qkqzvi4mif") (f (quote (("default" "macros") ("bench") ("backtrace" "anyerror/backtrace")))) (s 2) (e (quote (("macros" "dep:anyerror"))))))

(define-public crate-validit-0.2.4 (c (n "validit") (v "0.2.4") (d (list (d (n "anyerror") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "08002wimwvdmjkj5dy3w29zq2xcym409gm5l0q61d75f7sgx9ym1") (f (quote (("default" "macros") ("bench") ("backtrace" "anyerror/backtrace")))) (s 2) (e (quote (("macros" "dep:anyerror"))))))

