(define-module (crates-io va li validators-options) #:use-module (crates-io))

(define-public crate-validators-options-0.21.0 (c (n "validators-options") (v "0.21.0") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "1yyhssdmxznr60jsaknzs7736nksvf4cf314h85g9s2nla7gd0fd") (y #t)))

(define-public crate-validators-options-0.21.1 (c (n "validators-options") (v "0.21.1") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "1w75c9clvj30qpsn4ygnfrw468pkrgkw83680fs0hg6w7g56pxvy")))

(define-public crate-validators-options-0.22.0 (c (n "validators-options") (v "0.22.0") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "1qyr3fbj4xnfga3p812ixk66a24xgp06sr4rg9sscmfw5zs52l07")))

(define-public crate-validators-options-0.22.1 (c (n "validators-options") (v "0.22.1") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "118nrdz1qz4ym2jrr1jiy65fv00wy02zjar4hpzaqjlp3ksf30wx")))

(define-public crate-validators-options-0.23.0 (c (n "validators-options") (v "0.23.0") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "1zxmqg25d8076xhq626sm2p5dbjhr5mjcnxslvkc2w11k7kqdd36")))

(define-public crate-validators-options-0.23.1 (c (n "validators-options") (v "0.23.1") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "1jizp4c39b9zhsn5c3dnh0kl8xb66h47pyc723gq0g1i1yc8skpb")))

(define-public crate-validators-options-0.24.0 (c (n "validators-options") (v "0.24.0") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "08r5dmhjk5qyhxb12f13awnz16w40n2h1x3d5b4nn2w1q9l3n9xd")))

(define-public crate-validators-options-0.24.4 (c (n "validators-options") (v "0.24.4") (d (list (d (n "educe") (r "^0.4") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)))) (h "02w58pcp2iqiwsb498hbzhaikqqpmdhpmr41mqkiya0jd17axvbh") (r "1.61")))

