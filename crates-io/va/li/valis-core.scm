(define-module (crates-io va li valis-core) #:use-module (crates-io))

(define-public crate-valis-core-0.1.0 (c (n "valis-core") (v "0.1.0") (h "1vjk3cv6ys5151dzbisl2lf6dacr2swc7ag543glwmpnn4k823pm")))

(define-public crate-valis-core-0.1.1 (c (n "valis-core") (v "0.1.1") (h "0h25waz7mspj37nkyzzwy14kbsjhqilpz05q4bmlck1scpal8j4k")))

(define-public crate-valis-core-0.1.2 (c (n "valis-core") (v "0.1.2") (h "15x2icqz7sl1dbf3fr636ws73p45sidxsbcb39b62ghb1ydnhszn")))

(define-public crate-valis-core-0.1.3 (c (n "valis-core") (v "0.1.3") (h "0a0f1dasnzr9nx47vz5j9s6h4p0mswf4wpzxhb0swb0j8hfx700n")))

(define-public crate-valis-core-0.1.4 (c (n "valis-core") (v "0.1.4") (d (list (d (n "globmatch") (r "^0.2.3") (d #t) (k 0)))) (h "15bcxdksdn6waj2qrwgik2r7wri17xbfqdmifcibdadpjn8rn7cp")))

(define-public crate-valis-core-0.1.5 (c (n "valis-core") (v "0.1.5") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "globmatch") (r "^0.2.3") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0f7ay1m8cga98p8jyfk8hgv19g9x3khm13xx5z21xdd7q9m1dsbr")))

(define-public crate-valis-core-0.1.6 (c (n "valis-core") (v "0.1.6") (d (list (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "globmatch") (r "^0.2.3") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0mv8ax7sp1bmgyjvvc669h45c2kpbyr86psiq7k10nvwhyn7pgvj")))

(define-public crate-valis-core-0.1.7 (c (n "valis-core") (v "0.1.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "globmatch") (r "^0.2.3") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (f (quote ("builtin-lua54"))) (d #t) (k 0)))) (h "1qivjdgc0yc7n9x3dl036ylvi92v98cd9d77h51zckcxrhv8fnm8")))

(define-public crate-valis-core-0.1.8 (c (n "valis-core") (v "0.1.8") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "globmatch") (r "^0.2.3") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "kdbx-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (f (quote ("builtin-lua54"))) (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "145cb28l1jsc4vl2p1hdbb1kccdr5qpbdgv09v9a1afagb7w849g")))

(define-public crate-valis-core-0.1.9 (c (n "valis-core") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.17.1") (d #t) (k 0)) (d (n "globmatch") (r "^0.2.3") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "kdbx-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("serde_json" "blocking" "json"))) (d #t) (k 0)) (d (n "rlua") (r "^0.19.4") (f (quote ("builtin-lua54"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)) (d (n "sea-orm") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "16kfvfkyxm7zx5rz7jwp0ba6r1skj735w4s1ls27z08j3a6j5m30")))

