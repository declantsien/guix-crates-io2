(define-module (crates-io va li validated-slice) #:use-module (crates-io))

(define-public crate-validated-slice-0.1.0 (c (n "validated-slice") (v "0.1.0") (h "12m8b5gngx47bqgp3m3bpwpndxicp33gawjr6q69lxn5nw9lhjza")))

(define-public crate-validated-slice-0.1.1 (c (n "validated-slice") (v "0.1.1") (h "1ac5rfr70vzxc4k0mlqh4dxpjxbxnd8xn7q2s5gghjfamca8x8lb")))

(define-public crate-validated-slice-0.2.0 (c (n "validated-slice") (v "0.2.0") (h "1hdwq3wili063a83x7x93fhfz0n14hix0cd7v217spnk9yd0p395")))

