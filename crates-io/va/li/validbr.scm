(define-module (crates-io va li validbr) #:use-module (crates-io))

(define-public crate-validbr-0.1.0 (c (n "validbr") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zqwfngmq3vdqcv4sc90gbqpl5gv5r3rhwmdc4yjgqnwz5sjckj0") (f (quote (("complete" "serde" "rand"))))))

(define-public crate-validbr-0.2.0 (c (n "validbr") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j5r3dgbmn07ac2v4j1hfd7pqjkm3d8625djlfz1370n2zgdzpzf") (f (quote (("complete" "serde" "rand"))))))

(define-public crate-validbr-0.2.1 (c (n "validbr") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kbngvsifdd182ay5if6mw6vsj0hcgk9dhnv37pa5p59s532ar8r") (f (quote (("complete" "serde" "rand"))))))

