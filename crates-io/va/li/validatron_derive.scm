(define-module (crates-io va li validatron_derive) #:use-module (crates-io))

(define-public crate-validatron_derive-0.1.0 (c (n "validatron_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j1w1r03g8m6fkmhqwvsz00pgrm9jyq799jn9644bmlg3qdgnpvd")))

(define-public crate-validatron_derive-0.2.0 (c (n "validatron_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wgl45lahyiy2hqw7dpmp7vrhnwac6i69nmzmbhjr6rw16yipz94")))

(define-public crate-validatron_derive-0.2.1 (c (n "validatron_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13snr2ldlwbi6cyjwzsdfzw39qrg1ndhfa9rk41y0mdwy7lxf46f")))

(define-public crate-validatron_derive-0.3.0 (c (n "validatron_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mmysiwh73mymsgny1294rcn40m5xim9z40hys5yr8ghb9sbgwjl")))

(define-public crate-validatron_derive-0.4.0 (c (n "validatron_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17y2lh87y7i7lzr0qilr5zxbs0x3bdw0jms5y51h672c7i6lm85k")))

(define-public crate-validatron_derive-0.5.0 (c (n "validatron_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fm70gmc5fmvwza8lmkw1basfrqb2z13w7z5cnaxpf60d92x55iw")))

