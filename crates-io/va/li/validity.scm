(define-module (crates-io va li validity) #:use-module (crates-io))

(define-public crate-validity-0.1.0 (c (n "validity") (v "0.1.0") (h "1i0r4apwr79mj1bmihg5i9mfdicjgczlkd58ppkd81sayjd15mwk")))

(define-public crate-validity-0.1.1 (c (n "validity") (v "0.1.1") (h "1q7bmcvb6zm24g7v8ixiwh1ix194v2fdhmdaa4bz35p8vr9sh657")))

(define-public crate-validity-0.1.2 (c (n "validity") (v "0.1.2") (h "13acy0y8jsagb3mpb45z0pn1ldc3wai6nqi2zbsfngj1hipz8h3p") (f (quote (("test-mock"))))))

(define-public crate-validity-0.2.0 (c (n "validity") (v "0.2.0") (h "1rvq26p701aax1a1rhcar7r812z46pv8rcysxvxjc84jf4bvzqkp") (f (quote (("test-mock"))))))

(define-public crate-validity-0.3.0 (c (n "validity") (v "0.3.0") (h "1l6firrh3h71csi1qc8zqkhb289839jl325n3lvj22nv514nair2") (f (quote (("test-mock"))))))

(define-public crate-validity-0.3.1 (c (n "validity") (v "0.3.1") (h "13rlb9kqf8z684jwasa151y9w18vwgzpzkw9pzpfw5vz70zd19a9") (f (quote (("test-mock"))))))

