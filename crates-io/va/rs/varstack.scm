(define-module (crates-io va rs varstack) #:use-module (crates-io))

(define-public crate-varstack-0.1.0 (c (n "varstack") (v "0.1.0") (h "1hc3ibqp0r0s43r5cs456rlc3isfvizml37q3i86yq6gm2jqmh7w")))

(define-public crate-varstack-0.2.0 (c (n "varstack") (v "0.2.0") (h "0pblby1sqy9z8xdy4y7gvkzbcb99a2ip5sw88wl8b67vys2k5w5h")))

(define-public crate-varstack-0.2.1 (c (n "varstack") (v "0.2.1") (h "04pqk7g9kyhx0sjjdl9xkx4px2dwhqpcq4vq3a1jmw3rc7yzr35f")))

(define-public crate-varstack-0.2.2 (c (n "varstack") (v "0.2.2") (h "1qgr82aznjyfvznjcpkk9nmz4hhqgzz2mg94faq7w8n1s4sjc2q5")))

(define-public crate-varstack-0.2.3 (c (n "varstack") (v "0.2.3") (h "17bf3mv4nxrak5s3dxck76q99nhdwbrhlyqp64hx3529a681j5gk")))

(define-public crate-varstack-0.2.4 (c (n "varstack") (v "0.2.4") (h "0il6fqjwk6r03d80yr000l95rc6zbdiw09zqbxdhi80v7wayxwh3")))

