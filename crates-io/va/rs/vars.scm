(define-module (crates-io va rs vars) #:use-module (crates-io))

(define-public crate-vars-0.1.0 (c (n "vars") (v "0.1.0") (h "09wl8p6plb7lng6nxd8fnrd6cxhh3c1ldpgz67b01zlxx0m9xlpp")))

(define-public crate-vars-0.2.0 (c (n "vars") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "vars_macro") (r "^0.1") (d #t) (k 0)))) (h "0if3g5bj5lpa3gs20r9aqgp9cn4kjh2jfwk8l0b2b965vp7hsaa3")))

