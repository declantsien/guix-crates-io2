(define-module (crates-io va rf varflags_attribute) #:use-module (crates-io))

(define-public crate-varflags_attribute-0.0.1 (c (n "varflags_attribute") (v "0.0.1") (d (list (d (n "bitworks") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0kchkdkrhsrlp77ms8b6xsp2vp79hzd4nxpg96a18dihx2bqkijn")))

(define-public crate-varflags_attribute-0.0.2 (c (n "varflags_attribute") (v "0.0.2") (d (list (d (n "bitworks") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0vv322yhf6lm6cc7gy9n29ln4ddshdfy87bg1z0lrhi9h60ig635")))

(define-public crate-varflags_attribute-0.0.3 (c (n "varflags_attribute") (v "0.0.3") (d (list (d (n "bitworks") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "16ym0rl3ys6ls0a9f1j175z4q4ap4wnhw5v6x9h4xlxgi0s0zx2n")))

