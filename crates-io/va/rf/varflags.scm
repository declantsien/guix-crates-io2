(define-module (crates-io va rf varflags) #:use-module (crates-io))

(define-public crate-varflags-0.0.1 (c (n "varflags") (v "0.0.1") (d (list (d (n "bitworks") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "varflags_attribute") (r "^0.0.2") (d #t) (k 0)))) (h "0dzgdfr1bs3a0whpvglf05pm9pn7z37c9jlbj19db0493qia9i03") (s 2) (e (quote (("serde" "dep:serde" "bitworks/serde"))))))

(define-public crate-varflags-0.0.2 (c (n "varflags") (v "0.0.2") (d (list (d (n "bitworks") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "varflags_attribute") (r "^0.0.3") (d #t) (k 0)))) (h "1nbq58zxc6gv27w9swpk1njfz1w7a91an4qaam20x222sxav2kx1") (s 2) (e (quote (("serde" "dep:serde" "bitworks/serde"))))))

