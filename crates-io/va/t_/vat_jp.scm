(define-module (crates-io va t_ vat_jp) #:use-module (crates-io))

(define-public crate-vat_jp-0.1.0 (c (n "vat_jp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 2)))) (h "1c6n12mc6a5l6nmxm9qjk61i3y5k7qbayar5pldc3zjkv2r7yqph")))

(define-public crate-vat_jp-0.1.1 (c (n "vat_jp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 2)))) (h "153kanj2hfwy76scdsfik08ybm6ri4m51mbx5pip4jbifavvm9yf")))

(define-public crate-vat_jp-0.1.2 (c (n "vat_jp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "cargo-watch") (r "^8.4.0") (d #t) (k 2)))) (h "15kswnps563wz51vfh555bja3c115j1nsy1rbas2p8w56xn7lcvb")))

