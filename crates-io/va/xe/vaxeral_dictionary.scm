(define-module (crates-io va xe vaxeral_dictionary) #:use-module (crates-io))

(define-public crate-vaxeral_dictionary-0.1.0 (c (n "vaxeral_dictionary") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vv0ans1c60hgyiw39xn84vaz4g2gi2kah0dy4nc8pn3fqdhskq9")))

(define-public crate-vaxeral_dictionary-0.1.1 (c (n "vaxeral_dictionary") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "13vqvngjvssm95ah77200skl254rmbkwmg57hjylk4xbv9wjrmy1")))

