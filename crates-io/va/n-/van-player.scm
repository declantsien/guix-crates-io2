(define-module (crates-io va n- van-player) #:use-module (crates-io))

(define-public crate-van-player-0.1.0 (c (n "van-player") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "cursive") (r "^0.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmpv") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "macros" "parsing"))) (d #t) (k 0)))) (h "1r48pjvrgvnjpx5b1ca10055dp8m0i1c79qa4s0f9wm65y6n5jpd")))

(define-public crate-van-player-0.1.1 (c (n "van-player") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18") (d #t) (k 0)) (d (n "van-core") (r "^0.1") (d #t) (k 0)))) (h "03fskvskxczkykhk90hx6agkydwhzr2a96gfz162fcddmkrng7jz")))

