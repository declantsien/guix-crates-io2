(define-module (crates-io va rc varcon) #:use-module (crates-io))

(define-public crate-varcon-0.3.0 (c (n "varcon") (v "0.3.0") (d (list (d (n "varcon-core") (r "^1.0") (d #t) (k 0)))) (h "03qb4a3hn17x505449v361jml1i2wxhmgzhwzs12ziv9ih448z8s") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags"))))))

(define-public crate-varcon-0.4.0 (c (n "varcon") (v "0.4.0") (d (list (d (n "varcon-core") (r "^1.0") (d #t) (k 0)))) (h "1bdjjaw5s27ix2nz5dp7pzm2bn91kqrdhxs9q8f8vw5w50v8fxxw") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags"))))))

(define-public crate-varcon-0.5.0 (c (n "varcon") (v "0.5.0") (d (list (d (n "varcon-core") (r "^2.0") (d #t) (k 0)))) (h "1wl4cmkjck5mr1zb6pllwkq560s9gm4dg4bk3hrzlssnada5qdgy") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags"))))))

(define-public crate-varcon-0.5.1 (c (n "varcon") (v "0.5.1") (d (list (d (n "varcon-core") (r "^2.0") (d #t) (k 0)))) (h "0g42yq6a0snpj9bdda1ly7klyaw4ya6r5bpsvayad9qlir012wh1") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags"))))))

(define-public crate-varcon-0.6.0 (c (n "varcon") (v "0.6.0") (d (list (d (n "varcon-core") (r "^2.0") (d #t) (k 0)))) (h "0dbffw0hnmr2xrwlcy695mx2wszy128ipnk3jwvjp7zhb4vmfqrg") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags"))))))

(define-public crate-varcon-0.6.1 (c (n "varcon") (v "0.6.1") (d (list (d (n "varcon-core") (r "^2.0") (d #t) (k 0)))) (h "1ry32y2avlbh04si0y5f2sbf3mc4cjjbiyl99nsbxrzybj4z90gl") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags"))))))

(define-public crate-varcon-0.6.2 (c (n "varcon") (v "0.6.2") (d (list (d (n "varcon-core") (r "^2.0") (d #t) (k 0)))) (h "1dslmp86gqx954aswdyqwmw56qczvvm8lpz3hy2iqlwm3az7fk2s") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.59.0")))

(define-public crate-varcon-0.6.3 (c (n "varcon") (v "0.6.3") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.3.1") (f (quote ("path"))) (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (f (quote ("parser"))) (d #t) (k 2)))) (h "0amvqd94gdq5chfp2wshy05fwis7r3r0x9mh7fhrm7c2fv5lc4wm") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.60.0")))

(define-public crate-varcon-0.6.4 (c (n "varcon") (v "0.6.4") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.3.1") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^2.2") (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (f (quote ("parser"))) (d #t) (k 2)))) (h "0cd5dqpdpza9ms83dpbbh1gay39v6awy22224rpfwnrmdqk0q08p") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.60.0")))

(define-public crate-varcon-0.6.5 (c (n "varcon") (v "0.6.5") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.0") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^2.2") (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (f (quote ("parser"))) (d #t) (k 2)))) (h "0dxig5sfz4g20h43kr62r4x2arq0ldvhjqk4b8xblz68yrkg458y") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.64.0")))

(define-public crate-varcon-0.6.6 (c (n "varcon") (v "0.6.6") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.0") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^2.2") (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (f (quote ("parser"))) (d #t) (k 2)))) (h "1awjx702hma2gzsprwz6v08akh9jvbj5rhyfpfagf1lx1clqlcbm") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.64.0")))

(define-public crate-varcon-0.6.7 (c (n "varcon") (v "0.6.7") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.3") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^2.2") (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (f (quote ("parser"))) (d #t) (k 2)))) (h "0pf1w7pm72m4xncmccczybps0fn5vy8iw3agjrrmgb590rdy6qyk") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.64.0")))

(define-public crate-varcon-0.6.8 (c (n "varcon") (v "0.6.8") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.8") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^2.2") (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (f (quote ("parser"))) (d #t) (k 2)))) (h "0p6qj10jkqvd0gv45ng1pjzc56p74xax5kfbss2nifid2yvbkbjq") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.64.0")))

(define-public crate-varcon-0.6.9 (c (n "varcon") (v "0.6.9") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.11") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^2.2") (d #t) (k 0)) (d (n "varcon-core") (r "^2.2") (f (quote ("parser"))) (d #t) (k 2)))) (h "0bc02szcipcmgyv5k75dv038rsahfi9wx6mdzcl6nwpgi5chckjp") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.64.0")))

(define-public crate-varcon-0.7.0 (c (n "varcon") (v "0.7.0") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.11") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^4.0") (d #t) (k 0)) (d (n "varcon-core") (r "^4.0") (f (quote ("parser"))) (d #t) (k 2)))) (h "1bi2x94csz1cpl1dsl4ssgqzqphxp6prllgz0p4clzqk872342h0") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.64.0")))

(define-public crate-varcon-0.7.1 (c (n "varcon") (v "0.7.1") (d (list (d (n "codegenrs") (r "^2.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.12") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^4.0") (d #t) (k 0)) (d (n "varcon-core") (r "^4.0") (f (quote ("parser"))) (d #t) (k 2)))) (h "0c0r3kdlp723j6bla08vzxahr39dy7syxfirg8jvnykk9gyqv5m6") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.70.0")))

(define-public crate-varcon-0.7.2 (c (n "varcon") (v "0.7.2") (d (list (d (n "codegenrs") (r "^3.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.14") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^4.0") (d #t) (k 0)) (d (n "varcon-core") (r "^4.0") (f (quote ("parser"))) (d #t) (k 2)))) (h "1xfbvx2f3gbzarnc9q4xhyjkc7gidqm1snd2kx2b189gpbb8mqii") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.70.0")))

(define-public crate-varcon-0.7.3 (c (n "varcon") (v "0.7.3") (d (list (d (n "codegenrs") (r "^3.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.4.15") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^4.0") (d #t) (k 0)) (d (n "varcon-core") (r "^4.0") (f (quote ("parser"))) (d #t) (k 2)))) (h "1q42fpv8v6x77c4iji3k82gmdz7kb209zl6hf8qdvxq126c16vhy") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.75")))

(define-public crate-varcon-0.7.4 (c (n "varcon") (v "0.7.4") (d (list (d (n "codegenrs") (r "^3.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.5.7") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^4.0") (d #t) (k 0)) (d (n "varcon-core") (r "^4.0") (f (quote ("parser"))) (d #t) (k 2)))) (h "1xcab2m34b1z8m8jdspdyb4y7z5x6gbyanzcyq081m47li0rs1hi") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.75")))

(define-public crate-varcon-0.7.5 (c (n "varcon") (v "0.7.5") (d (list (d (n "codegenrs") (r "^3.0") (d #t) (k 2)) (d (n "snapbox") (r "^0.5.9") (f (quote ("path"))) (d #t) (k 2)) (d (n "varcon-core") (r "^4.0") (d #t) (k 0)) (d (n "varcon-core") (r "^4.0") (f (quote ("parser"))) (d #t) (k 2)))) (h "1nj6pk6sgmff61dzlgw383j0s2zvd71xf6fp37wkj33xrgxzlkbx") (f (quote (("flags" "varcon-core/flags") ("default" "all") ("all" "flags")))) (r "1.75")))

