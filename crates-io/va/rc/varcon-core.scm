(define-module (crates-io va rc varcon-core) #:use-module (crates-io))

(define-public crate-varcon-core-1.1.0 (c (n "varcon-core") (v "1.1.0") (d (list (d (n "enumflags2") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6") (o #t) (d #t) (k 0)))) (h "0ypg9rm7bj1g3kknnhw5ydnyx2462gghl2c2bmav32cw9ixrd7m8") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-1.2.0 (c (n "varcon-core") (v "1.2.0") (d (list (d (n "enumflags2") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6") (o #t) (d #t) (k 0)))) (h "1vz0cgqkqfgg4bdlcfsjcsh5wjpimx43p88mpljxz19fkikf3nzp") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.0.0 (c (n "varcon-core") (v "2.0.0") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6") (o #t) (d #t) (k 0)))) (h "1riabcil8adn7mjaavj70xns2w6d38qlz2bxh6jvdd1gzg6mzkhl") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.1.0 (c (n "varcon-core") (v "2.1.0") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6") (o #t) (d #t) (k 0)))) (h "0x0p69ak1k6x8l8xnxy8b26mdifd0wgq2skd0c64i8wp75k46aaz") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.1.1 (c (n "varcon-core") (v "2.1.1") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6") (o #t) (d #t) (k 0)))) (h "1ra55ihmag06w0qbgdkawki31xzfx5wsqx02l86db5dv4bnhf787") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.2.0 (c (n "varcon-core") (v "2.2.0") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6") (o #t) (d #t) (k 0)))) (h "18nhgw97kfh86xjv0hbw95gjmajgbg5rmp0jlwm7k0dz3c5ifrvs") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.2.1 (c (n "varcon-core") (v "2.2.1") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1ha6fwh2lw08r1gik228zx8nydv06f1ap73qqpkd8qy5ipipglj7") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.2.2 (c (n "varcon-core") (v "2.2.2") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "04ir8qkmsih6brkx4v8vazx173p2b4jnr84xbxk1sb8csviijxx1") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.2.3 (c (n "varcon-core") (v "2.2.3") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "0fx887p5qnjp96x4i0zyddwx1k4xnmkv6pwvr11bl41sd0gfgafs") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default"))))))

(define-public crate-varcon-core-2.2.4 (c (n "varcon-core") (v "2.2.4") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1r2fkj671r70y2ch84irnx5h37qr9a01m9qyxynfz4j9cfzyl03q") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default")))) (r "1.59.0")))

(define-public crate-varcon-core-2.2.5 (c (n "varcon-core") (v "2.2.5") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "0rf9c4y0sd6wr6s12sijg34jwn2d9419hixcy4drb7b9hfb26m2p") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default")))) (r "1.60.0")))

(define-public crate-varcon-core-2.2.6 (c (n "varcon-core") (v "2.2.6") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "1gsp3gd8kfa26jnan7dvl4i327bkc1kw8gkmkg5nxpda6zj3llzk") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-2.2.7 (c (n "varcon-core") (v "2.2.7") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)))) (h "0pjjz8d7w98q8yzdwyhl58l4xqp3kd76kajzfmw0mi928mka2k91") (f (quote (("parser" "nom") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-2.2.8 (c (n "varcon-core") (v "2.2.8") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1iy091z1s7p03bgvxd44qgfz5gyd1nvvflx22c7cw1qjfnj8qd29") (f (quote (("parser" "winnow") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-2.2.9 (c (n "varcon-core") (v "2.2.9") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "0fz6d2ij46w8p6y1vdj685cikvhwrpmi644sclk419p84j8sgzfh") (f (quote (("parser" "winnow") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-2.2.10 (c (n "varcon-core") (v "2.2.10") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1h5pgg75ijk2vj5pxddm62sh2mg8mj97kgvcyc46yq79xvzcda9j") (f (quote (("parser" "winnow") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-2.2.11 (c (n "varcon-core") (v "2.2.11") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "09vxkm5b2vvr5f1vxr620svyis10837bxxw1pl218x7g5c5q1lm6") (f (quote (("parser" "winnow") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-2.2.12 (c (n "varcon-core") (v "2.2.12") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "0zas1jkci1v6yfw4mcm00c2c50yvd3n0g55d3i238axxrfkdsvd7") (f (quote (("parser" "winnow") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-3.0.0 (c (n "varcon-core") (v "3.0.0") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.4.9") (o #t) (d #t) (k 0)))) (h "00dnyk5q1rslw8fw8350m9xw1bf0sn542jcdkv2p3dk48zw1n4lm") (f (quote (("parser" "winnow") ("flags" "enumflags2") ("default")))) (r "1.64.0")))

(define-public crate-varcon-core-4.0.0 (c (n "varcon-core") (v "4.0.0") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0j2vgywmjvlld66nhwzq8wf63jy0gn4a3p5xrszgzqr1ar2qxmbb") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.64.0")))

(define-public crate-varcon-core-4.0.1 (c (n "varcon-core") (v "4.0.1") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.5.15") (o #t) (d #t) (k 0)))) (h "1z241f72i1zcjji5hasfcdbyazf1csv5w33v1dzpdvpjypla3mya") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.70.0")))

(define-public crate-varcon-core-4.0.2 (c (n "varcon-core") (v "4.0.2") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.5.17") (o #t) (d #t) (k 0)))) (h "1azss9scd52by47rclyhfryi80j95qwvg6gdi3yrhp4555hp8bgm") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.70.0")))

(define-public crate-varcon-core-4.0.3 (c (n "varcon-core") (v "4.0.3") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.5.19") (o #t) (d #t) (k 0)))) (h "1xybzl116rx4zxrb01348irnw259hvgnmr2qcgnngrar9dz50xzh") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.70.0")))

(define-public crate-varcon-core-4.0.4 (c (n "varcon-core") (v "4.0.4") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.5.31") (o #t) (d #t) (k 0)))) (h "0y5ijcsfpxmq1amk9i36d1z0gcl5j19655zw54w8qhpglbsvvnii") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.75")))

(define-public crate-varcon-core-4.0.5 (c (n "varcon-core") (v "4.0.5") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0a196a4l7krgq05p2d9270qq63sf6df8h5ipmj3jf9qxg3i523zj") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.75")))

(define-public crate-varcon-core-4.0.6 (c (n "varcon-core") (v "4.0.6") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (o #t) (d #t) (k 0)))) (h "0plggkp4klsz6d2zglnik3w97acrlprays01vy40hn1vkl46104h") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.75")))

(define-public crate-varcon-core-4.0.7 (c (n "varcon-core") (v "4.0.7") (d (list (d (n "enumflags2") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (o #t) (d #t) (k 0)))) (h "07pwbsabj2s8pgrl4l2a453816a64r76h8x5wf0izmncjxzn0rpp") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:winnow") ("flags" "dep:enumflags2")))) (r "1.75")))

