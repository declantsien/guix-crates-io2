(define-module (crates-io va #{41}# va416xx) #:use-module (crates-io))

(define-public crate-va416xx-0.1.0 (c (n "va416xx") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10h733r2p17x8b42sni5zd8yix5xli9vfcwjcvqclzmhpmgldzln") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va416xx-0.1.1 (c (n "va416xx") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07zqvkxp25rpp6gyishjf1zgww9mv7ydrcnr6c2vn43i24hcgw87") (f (quote (("rt" "cortex-m-rt/device"))))))

