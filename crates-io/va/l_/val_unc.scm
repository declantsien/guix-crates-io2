(define-module (crates-io va l_ val_unc) #:use-module (crates-io))

(define-public crate-val_unc-0.7.0 (c (n "val_unc") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10ss0v0yrw37x2kbrxbq06h41rlfnaycx7yfvga0r5iczigqqslc")))

(define-public crate-val_unc-0.7.1 (c (n "val_unc") (v "0.7.1") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p03qqp36bvqj8f3jiqvcn9zx01gnm109cbfcnp9nmz6zgian6wf")))

(define-public crate-val_unc-0.8.0 (c (n "val_unc") (v "0.8.0") (d (list (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08pzg1lb9w27pdy0h870ypfvh7hcsd5yjcik5xr92b6ycdrrw1vg")))

