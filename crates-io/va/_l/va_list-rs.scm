(define-module (crates-io va _l va_list-rs) #:use-module (crates-io))

(define-public crate-va_list-rs-0.0.1 (c (n "va_list-rs") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m35gn4b538agifgblrg273gw8jm71lw3aqbr24dyn9ywlzxpi7c")))

(define-public crate-va_list-rs-0.0.4 (c (n "va_list-rs") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ghbwqdfjavwfipnf2q3ynqyz7fwfv7ygzgbh9h9vwiprm91r63x")))

