(define-module (crates-io va _l va_list) #:use-module (crates-io))

(define-public crate-va_list-0.0.1 (c (n "va_list") (v "0.0.1") (d (list (d (n "va_list-test") (r "*") (d #t) (k 2)))) (h "1vxmsr4r0cjwlsr6ah666037sr78h7ch3d75b088i5zxlg163p9m")))

(define-public crate-va_list-0.0.2 (c (n "va_list") (v "0.0.2") (d (list (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "1lh96sxa7hf4p9b3s42fnnjkhc7a7w7xwx73h89r5inqidy9bc2h") (f (quote (("no_std"))))))

(define-public crate-va_list-0.0.3 (c (n "va_list") (v "0.0.3") (d (list (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "0amncdfyp98bw670bss8sliab1837q4p2xyxly0gkdvzx0dcyj9g") (f (quote (("no_std"))))))

(define-public crate-va_list-0.1.0 (c (n "va_list") (v "0.1.0") (d (list (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "0a1ipnjsmdc3653cdz4az4ds0725y2jilj8jq7dx0pp5mqpqcrbk") (f (quote (("no_std"))))))

(define-public crate-va_list-0.1.1 (c (n "va_list") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "0k7jmb9h27nfb8wd7rc6k88y77kg1vmddhlp31v3f6nkr4si1pdn") (f (quote (("no_std"))))))

(define-public crate-va_list-0.1.2 (c (n "va_list") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "195m0fwgzn6y850kf7m47f9kcj15rmjd23spy4n2kv50jkq9xcsb") (f (quote (("no_std"))))))

(define-public crate-va_list-0.1.3 (c (n "va_list") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "19g78bc8lpfaf27818irfrkxbjb15wf9ygvbvd7i74nss58ldjwk") (f (quote (("no_std"))))))

(define-public crate-va_list-0.1.4 (c (n "va_list") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "va_list-helper") (r "^0.0.2") (d #t) (k 2)))) (h "1nin5pskdbv8kpambwwncis6cdsfpqafb36hghw8m5sgfkpxa2rm") (f (quote (("no_std"))))))

