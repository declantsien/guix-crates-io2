(define-module (crates-io va cp vacp2p_pmtree) #:use-module (crates-io))

(define-public crate-vacp2p_pmtree-1.0.0 (c (n "vacp2p_pmtree") (v "1.0.0") (d (list (d (n "ark-serialize") (r "=0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "=0.3.4") (d #t) (k 2)) (d (n "rayon") (r "=1.7.0") (d #t) (k 0)) (d (n "sled") (r "=0.34.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "=2.0.2") (f (quote ("keccak"))) (d #t) (k 2)))) (h "0r6645gb21gnwz15xrxzamhdkmcg91x79sqqld77gmjjvs88grxy")))

(define-public crate-vacp2p_pmtree-2.0.1 (c (n "vacp2p_pmtree") (v "2.0.1") (d (list (d (n "ark-serialize") (r "=0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "=0.3.4") (d #t) (k 2)) (d (n "rayon") (r "=1.7.0") (d #t) (k 0)) (d (n "sled") (r "=0.34.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "=2.0.2") (f (quote ("keccak"))) (d #t) (k 2)))) (h "0c15ikc892n6wgf2hcr12dvqxa0afwilsd92fv0lzjmy29c3l44x")))

(define-public crate-vacp2p_pmtree-2.0.2 (c (n "vacp2p_pmtree") (v "2.0.2") (d (list (d (n "ark-serialize") (r "=0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "=0.3.4") (d #t) (k 2)) (d (n "rayon") (r "=1.7.0") (d #t) (k 0)) (d (n "sled") (r "=0.34.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "=2.0.2") (f (quote ("keccak"))) (d #t) (k 2)))) (h "1xn1i3g73a9gpmpxdpwxzaacgd0pacljgm71vc9d846a0vsr68k3")))

