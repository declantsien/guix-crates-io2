(define-module (crates-io va ri variante) #:use-module (crates-io))

(define-public crate-variante-0.1.0 (c (n "variante") (v "0.1.0") (h "00m0jz1q73sr82p8w3ln6vhm1dlw4q7hv41s0yzfqjxaxm7ccjb7") (y #t)))

(define-public crate-variante-0.1.1 (c (n "variante") (v "0.1.1") (h "0sh4g1i9xx2wbdbp63bkmf76wcdqncwasskkkry54fpsh7566482") (y #t)))

(define-public crate-variante-0.2.0 (c (n "variante") (v "0.2.0") (h "1ihjlq76l8vx02y95zfnif9wb7lr8z459f97myfrzqgmzdsy74w8")))

