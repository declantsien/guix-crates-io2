(define-module (crates-io va ri variable_size_byte_writer) #:use-module (crates-io))

(define-public crate-variable_size_byte_writer-0.1.0 (c (n "variable_size_byte_writer") (v "0.1.0") (h "1r1awyxh1fb8b9lmphngdk8f2xvfwryi2gqbyxpvwhyarkxrpwzm")))

(define-public crate-variable_size_byte_writer-0.1.1 (c (n "variable_size_byte_writer") (v "0.1.1") (h "1va3ypfncl65m21wqxg07z6px5fri52650cg8ax29g2fb85b7i9z")))

(define-public crate-variable_size_byte_writer-0.1.2 (c (n "variable_size_byte_writer") (v "0.1.2") (h "172wzl79zq56jrblsjmhi22pz8zn0izni0q5fih8wkhv1gyvpjz1")))

(define-public crate-variable_size_byte_writer-0.1.3 (c (n "variable_size_byte_writer") (v "0.1.3") (h "18cix48yl0l3av9r7pdm2i19gcqf2v80ppzd5xbqqn1j3dd468wl")))

(define-public crate-variable_size_byte_writer-0.1.4 (c (n "variable_size_byte_writer") (v "0.1.4") (h "01qzmi34pf4g89iz86nyl7ay8xwpk8cj54rnncbqx0yzsgj0n5hs")))

(define-public crate-variable_size_byte_writer-0.1.5 (c (n "variable_size_byte_writer") (v "0.1.5") (d (list (d (n "typenum") (r "^1.9.0") (d #t) (k 0)) (d (n "typenum_loops") (r "^0.2.0") (d #t) (k 0)))) (h "0pqa6wwx1lhglsjp4iczy57n78bwyhdcqiwji8pyv5s8dk4zzm17")))

