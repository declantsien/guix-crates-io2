(define-module (crates-io va ri varintrs) #:use-module (crates-io))

(define-public crate-varintrs-0.1.0 (c (n "varintrs") (v "0.1.0") (h "1m9ym59fsd8a8vcxbdyyvv8m2x5khv4b0qh84pl5s74bq29lj2li")))

(define-public crate-varintrs-0.2.0 (c (n "varintrs") (v "0.2.0") (h "17h1vm1h01493g7la009zjv573ylyq19wgrxxdgmb3vzk609nz79")))

(define-public crate-varintrs-0.2.1 (c (n "varintrs") (v "0.2.1") (h "174nwjsym5yyw0mwg5c4fi9sc7bsczgivh8f2m3z0d2m5afx3rws")))

