(define-module (crates-io va ri varisat-utils) #:use-module (crates-io))

(define-public crate-varisat-utils-0.1.0 (c (n "varisat-utils") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "varisat") (r "^0.2.2") (d #t) (k 0)))) (h "0r2cmcz0lx6fd7l9hl2z1wknd6i209hsdyh530xf0s6n4h369ma2")))

(define-public crate-varisat-utils-0.2.0 (c (n "varisat-utils") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "varisat") (r "^0.2.2") (d #t) (k 0)))) (h "11i0rvmk5acyc2ri3d7hlvv2k742fjjhsq9m203j1ddrzadhv6wa")))

