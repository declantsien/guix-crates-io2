(define-module (crates-io va ri variance) #:use-module (crates-io))

(define-public crate-variance-0.1.0 (c (n "variance") (v "0.1.0") (h "02d6cpd4l9kbhjiy3pnjaxhdligmnpc9p0qhkxi3mayx4fs51m8z")))

(define-public crate-variance-0.1.1 (c (n "variance") (v "0.1.1") (h "1wdkq0yblcvq3apsxw7aghlzwsjxsrx15aicf47yfs8a0l1qw59y")))

(define-public crate-variance-0.1.2 (c (n "variance") (v "0.1.2") (h "16fwsdi19lbx4k6svvkv0w31v8a02hgqi8psmyi5dj967babnki2")))

(define-public crate-variance-0.1.3 (c (n "variance") (v "0.1.3") (h "01bvsxnfavqxq0hy0x12dx4hrs0xv17qiskr2f3n75mm3yzc5grs")))

