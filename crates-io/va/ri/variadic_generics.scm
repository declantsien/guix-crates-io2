(define-module (crates-io va ri variadic_generics) #:use-module (crates-io))

(define-public crate-variadic_generics-0.1.0 (c (n "variadic_generics") (v "0.1.0") (h "157mllz1q3cll5xchriwmib45ml0abcpghbh3ikd6vgxmjildfbv")))

(define-public crate-variadic_generics-0.1.1 (c (n "variadic_generics") (v "0.1.1") (h "06sgrwagsaa7v0yzjnahl9nkx1m3gicyd2hfsb3jmwmz685vrqiz")))

(define-public crate-variadic_generics-0.1.2 (c (n "variadic_generics") (v "0.1.2") (h "1ldmdll3k7c5ym4vsv77srlffbrspaajm2n1dicllykx344svz6y")))

