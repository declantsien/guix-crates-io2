(define-module (crates-io va ri variant_counter_derived) #:use-module (crates-io))

(define-public crate-variant_counter_derived-0.1.0 (c (n "variant_counter_derived") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jahqyhbzxrrswzxhyih9wjb8swvkb1bsp2l32fhakvbijcjmnl9")))

(define-public crate-variant_counter_derived-0.2.0 (c (n "variant_counter_derived") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bc0n94cg6n1jwj267kx32zrrbciaahprc2m4ny677fwp3x7hri0")))

(define-public crate-variant_counter_derived-0.2.1 (c (n "variant_counter_derived") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17gmm9jainpbhz9mvnpa6080i36n7gcd2k7jzv3sg7lgpiy479l5") (f (quote (("erase") ("check"))))))

(define-public crate-variant_counter_derived-0.2.2 (c (n "variant_counter_derived") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18k1hx54cc5scf2as0lscigdry5i05lif840s5263a4qjfwz3p30") (f (quote (("erase") ("check"))))))

(define-public crate-variant_counter_derived-0.3.0 (c (n "variant_counter_derived") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zj3m0c74cpxdbjz5qac7nf60m850qrdms6pblx38k27xaf5rm6d") (f (quote (("erase") ("check"))))))

(define-public crate-variant_counter_derived-0.4.0 (c (n "variant_counter_derived") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "036gnjw5fjgs1p4dfq1vcqyrys6bnnrmr126sayidkfabr9m59p9") (f (quote (("std") ("stats") ("full" "std" "check" "erase" "stats") ("erase") ("default" "std") ("check"))))))

