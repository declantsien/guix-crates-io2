(define-module (crates-io va ri variants-struct) #:use-module (crates-io))

(define-public crate-variants-struct-0.1.0 (c (n "variants-struct") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0r2nzmw2mz1ivlv0dd9hgvrkh54l9gsjirmg3h4qikw07i5krw2d")))

(define-public crate-variants-struct-0.1.1 (c (n "variants-struct") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (k 0)) (d (n "check_keyword") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "117ykci5y6bpvdp30769vgpddjrax8g0ryp35cf0f2yci63j62cl")))

