(define-module (crates-io va ri variantly) #:use-module (crates-io))

(define-public crate-variantly-0.1.0 (c (n "variantly") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ww8mhvlh0can8lqkpxdywy808kj50kq935hnk02xk9cnh3gj2kv")))

(define-public crate-variantly-0.2.0 (c (n "variantly") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g0k6d8nmdkjy2fx2j641gjhcskhrr1mgpi3l77bxpapd1md3qd8")))

(define-public crate-variantly-0.3.0 (c (n "variantly") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "16pszl3zwg53x5s5i75pw3dh8cr0w7h810cgh8zfyffvyysj793w")))

(define-public crate-variantly-0.4.0 (c (n "variantly") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mpg42wd420s80b99rqv1v4c4gdz5zbw0cwvksfig6m73cs358vj")))

