(define-module (crates-io va ri variable-provider-pkg) #:use-module (crates-io))

(define-public crate-variable-provider-pkg-0.2.0 (c (n "variable-provider-pkg") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.3.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "rhaki-cw-plus") (r "^0.6.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0jk3x5052y30xh2n49sjimdm909mdpz4kkd1l9v0m4grj9zyd6v8")))

(define-public crate-variable-provider-pkg-0.2.1 (c (n "variable-provider-pkg") (v "0.2.1") (d (list (d (n "cosmwasm-schema") (r "^1.3.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "rhaki-cw-plus") (r "^0.6.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0945lg9y4dx6z60zq4kq4krzn7cjk9mm541jb3cjw5380g90v7jk")))

(define-public crate-variable-provider-pkg-0.2.2 (c (n "variable-provider-pkg") (v "0.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.3.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "rhaki-cw-plus") (r "^0.6.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "023rmknf7xh5sbbk8r03zqgfbpa1lyq1yvqan2rkhfacp6jfm6iq")))

