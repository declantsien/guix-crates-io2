(define-module (crates-io va ri variant) #:use-module (crates-io))

(define-public crate-variant-0.1.0 (c (n "variant") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "mersenne_twister") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r ">= 0.3, < 0.5") (d #t) (k 0)))) (h "0swbji1fszpry3sv6zazdcwnl4wnwrsxj2mrb5dl7bgbjsym4qgz")))

