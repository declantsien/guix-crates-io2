(define-module (crates-io va ri variadics) #:use-module (crates-io))

(define-public crate-variadics-0.0.0 (c (n "variadics") (v "0.0.0") (h "0127j4bvgdbm9lc1bd81nlh7qxzv4rhg7wzd6601hgfxbfm5nwpb")))

(define-public crate-variadics-0.0.1 (c (n "variadics") (v "0.0.1") (h "0z7jqvhqslcz9gj9pj5swk52l0dhhvharls1ldhjch1d615kgi8c")))

(define-public crate-variadics-0.0.2 (c (n "variadics") (v "0.0.2") (h "1127ic9yib3v7dwp95b8v9wm4nybgvs1571dsvr8nmrpi18hyl64")))

(define-public crate-variadics-0.0.3 (c (n "variadics") (v "0.0.3") (d (list (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fjyxvwymca95kf1l84nxypwg0vyjyzkn075sbw8fv5faasg0hd4")))

(define-public crate-variadics-0.0.4 (c (n "variadics") (v "0.0.4") (d (list (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "033zcx6fcirkdbas6qif9bm2bmhnwzw8x6qigcd08bbi9nvij55p")))

