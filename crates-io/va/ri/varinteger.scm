(define-module (crates-io va ri varinteger) #:use-module (crates-io))

(define-public crate-varinteger-1.0.1 (c (n "varinteger") (v "1.0.1") (h "02yd0cyqssqjbjpzidsqlyqnybchf9wyimcc3q6gr3gmcbnac5fk")))

(define-public crate-varinteger-1.0.2 (c (n "varinteger") (v "1.0.2") (h "1mdrsnlzy7lyamb9zjm631rlfb96k3inr1xwmcyy4m8n4q7kpqnn")))

(define-public crate-varinteger-1.0.3 (c (n "varinteger") (v "1.0.3") (h "0al4rb6jyd2w0g6cgzji9sqxivbc5p5sh9xi5qmhd88wdni3kpyd")))

(define-public crate-varinteger-1.0.4 (c (n "varinteger") (v "1.0.4") (h "0ni58dgmgn579vfnd61ds1i9zf6flr9m11wc4n90j3dvlab3ps10")))

(define-public crate-varinteger-1.0.5 (c (n "varinteger") (v "1.0.5") (h "10pkzz0lwf324w3x8za1l110s4vzcvc8h5nfj6lbzzr1zf4l1il5")))

(define-public crate-varinteger-1.0.6 (c (n "varinteger") (v "1.0.6") (h "04hq308aan69c998pj0xywk0aa4ghwh82sv536v8pw2gz6wrv8ky")))

