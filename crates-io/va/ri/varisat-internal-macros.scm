(define-module (crates-io va ri varisat-internal-macros) #:use-module (crates-io))

(define-public crate-varisat-internal-macros-0.2.1 (c (n "varisat-internal-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.2") (d #t) (k 0)))) (h "07685pqa0g0yknf29kicgqb6fysvsrkmdlpdn0i3vxddkcyw8xhf")))

(define-public crate-varisat-internal-macros-0.2.2 (c (n "varisat-internal-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "13a0297kq3qhk6wa59sd44zjlhn0qs358ia8g2m6dl236mvwwbk0")))

