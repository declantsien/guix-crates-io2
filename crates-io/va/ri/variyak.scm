(define-module (crates-io va ri variyak) #:use-module (crates-io))

(define-public crate-variyak-0.1.1 (c (n "variyak") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "1mca346y730iqvmx95jnahrlfgbybprh17p96a9z4ajwq2wfing5")))

(define-public crate-variyak-0.1.2 (c (n "variyak") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "049xwp6vsvm6x0wi0jg9siriivgkl1vwflcgmdg7s1br5gh5y51k")))

(define-public crate-variyak-0.2.0 (c (n "variyak") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "seq-macro") (r "^0.2") (d #t) (k 0)))) (h "1aw5xv0cmgavm4xgkyhb08sa18hgs6r4lxxxrzrf6a6hs6va8k3y")))

