(define-module (crates-io va ri various_data_file) #:use-module (crates-io))

(define-public crate-various_data_file-0.1.0 (c (n "various_data_file") (v "0.1.0") (d (list (d (n "file_mmap") (r "^0.3") (d #t) (k 0)))) (h "1b8zn7v392as8mpw238nm2addb4qfgdl233fbzald06y50hida7i") (y #t)))

(define-public crate-various_data_file-0.2.0 (c (n "various_data_file") (v "0.2.0") (d (list (d (n "file_mmap") (r "^0.3") (d #t) (k 0)))) (h "0zn40wrdhdkyf53kb1c8a0ahcg8172g6w0lkcqw14hifs7w6b9s0") (y #t)))

(define-public crate-various_data_file-0.2.1 (c (n "various_data_file") (v "0.2.1") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)))) (h "1dfyd0rc1v27387mivsdrhsl9ibfmr0ckdc5zrh911jswj943whf") (y #t)))

(define-public crate-various_data_file-0.3.0 (c (n "various_data_file") (v "0.3.0") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)))) (h "177sb75zvx6pcy8qzm3pd9lgz8sx5d8hcad5yk6xzrv7nvf4jl4q") (y #t)))

(define-public crate-various_data_file-0.3.1 (c (n "various_data_file") (v "0.3.1") (d (list (d (n "file_mmap") (r "^0.4") (d #t) (k 0)))) (h "16q0k98g7gp1wqrd2rxf25rhx77jqqk84wyr3gg0i3ayj57bjr63") (y #t)))

(define-public crate-various_data_file-0.4.0 (c (n "various_data_file") (v "0.4.0") (d (list (d (n "file_mmap") (r "^0.5") (d #t) (k 0)))) (h "119h6s84hsrxd24zlri9hs1nhidksv49an3rnfs77mbl7h493ych") (y #t)))

(define-public crate-various_data_file-0.5.0 (c (n "various_data_file") (v "0.5.0") (d (list (d (n "file_mmap") (r "^0.6") (d #t) (k 0)))) (h "0ssf084vsa8kz218dc11wcv6dzqni3p56pkiab13jnibf1k5c3ym") (y #t)))

(define-public crate-various_data_file-0.5.1 (c (n "various_data_file") (v "0.5.1") (d (list (d (n "file_mmap") (r "^0.6") (d #t) (k 0)))) (h "1ajbcjxxidkcga9bbpyxnhbzzv35pkgvv6r7nssdckhw7c5h8i6n") (y #t)))

(define-public crate-various_data_file-0.5.2 (c (n "various_data_file") (v "0.5.2") (d (list (d (n "file_mmap") (r "^0.7") (d #t) (k 0)))) (h "0j2ccxj71cg1886x6njrigxc8nyyphqzgds5mc6r81yckxj8mnkf") (y #t)))

(define-public crate-various_data_file-0.6.0 (c (n "various_data_file") (v "0.6.0") (d (list (d (n "file_mmap") (r "^0.7") (d #t) (k 0)))) (h "1idv2bdrni9l368fwrrfydncmj9p7v0ar7wv38fkb3axl9qwi8hf") (y #t)))

(define-public crate-various_data_file-0.7.0 (c (n "various_data_file") (v "0.7.0") (d (list (d (n "file_mmap") (r "^0.8") (d #t) (k 0)))) (h "0fr7im69fsyzyf9c57l2rj4d082jszps1kb434bv9pd1mdnzgcs4") (y #t)))

(define-public crate-various_data_file-0.7.1 (c (n "various_data_file") (v "0.7.1") (d (list (d (n "file_mmap") (r "^0.8") (d #t) (k 0)))) (h "029iqm8wb2nwgzmpjv95ickankdynd8scg3ncf8kgsyrfghi0k3l") (y #t)))

(define-public crate-various_data_file-0.8.0 (c (n "various_data_file") (v "0.8.0") (d (list (d (n "file_mmap") (r "^0.8") (d #t) (k 0)))) (h "0zffwvxhva9d41h5b9vhgi62yghfk7iplf73cfw7f3mdxzir0j5b") (y #t)))

(define-public crate-various_data_file-0.8.1 (c (n "various_data_file") (v "0.8.1") (d (list (d (n "file_mmap") (r "^0.8") (d #t) (k 0)))) (h "0naxb2nbzmfs8vm3jmwcq5m97df5kyal1wrsxbys2k6mq12ypk7b") (y #t)))

(define-public crate-various_data_file-0.8.2 (c (n "various_data_file") (v "0.8.2") (d (list (d (n "file_mmap") (r "^0.9") (d #t) (k 0)))) (h "10vla7dxmjchilw8m3yqnvbmxdl9k5rdmmnhspr3shrcraf091vm") (y #t)))

(define-public crate-various_data_file-0.8.3 (c (n "various_data_file") (v "0.8.3") (d (list (d (n "file_mmap") (r "^0.10") (d #t) (k 0)))) (h "1y9hjhmnl8rcfxv57cjym038hf4jqbnrg6vfd4ymychcl9aj1mv2") (y #t)))

(define-public crate-various_data_file-0.9.0 (c (n "various_data_file") (v "0.9.0") (d (list (d (n "file_mmap") (r "^0.11") (d #t) (k 0)))) (h "0l5i4v263fzvzh3dww3hjg68bnvvzbzdkcjgw05rk5i5lq5gjy8h") (y #t)))

(define-public crate-various_data_file-0.9.1 (c (n "various_data_file") (v "0.9.1") (d (list (d (n "file_mmap") (r "^0.12") (d #t) (k 0)))) (h "171x9fq57vxh8v721gjna6f5nwiydmcp79n33rsxs7ps5ww6vg8n") (y #t)))

(define-public crate-various_data_file-0.9.2 (c (n "various_data_file") (v "0.9.2") (d (list (d (n "file_mmap") (r "^0.12") (d #t) (k 0)))) (h "0j77wz0x2v8vggqzidfqm55hl9pwkjpx2h6gydjvq7w3b1gan3hx") (y #t)))

(define-public crate-various_data_file-0.9.3 (c (n "various_data_file") (v "0.9.3") (d (list (d (n "file_mmap") (r "^0.12") (d #t) (k 0)))) (h "0sgwjv5mql5bpjs23qfg6n9fiylzk1brxj0fjm86q3s30z9wpdzb") (y #t)))

(define-public crate-various_data_file-0.9.4 (c (n "various_data_file") (v "0.9.4") (d (list (d (n "file_mmap") (r "^0.12") (d #t) (k 0)))) (h "12hwda9gp1l55yjx81g8z5frxwgabx63cv6mdz9x3awlznksiyph") (y #t)))

(define-public crate-various_data_file-0.9.5 (c (n "various_data_file") (v "0.9.5") (d (list (d (n "file_mmap") (r "^0.12") (d #t) (k 0)))) (h "1xwil0rac9r5wp3vy6d37hqv9msbgddsyax19a3kpn4s648gchj5") (y #t)))

(define-public crate-various_data_file-0.10.0 (c (n "various_data_file") (v "0.10.0") (d (list (d (n "file_mmap") (r "^0.13") (d #t) (k 0)))) (h "1gcv33ssijijlyml8crbh45cchg8k3wlfkllkmbaj1az5lw2z5g1") (y #t)))

(define-public crate-various_data_file-0.10.1 (c (n "various_data_file") (v "0.10.1") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)))) (h "03gi81k0ycmb1g84wk78x1hnkg84gy53ly5bgy5yvdr1hj138p43") (y #t)))

(define-public crate-various_data_file-0.10.2 (c (n "various_data_file") (v "0.10.2") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)))) (h "0r87km1ccwdzdzispxwzvs5jlyz8gxxqixpqx64cgd70d3bgl7cc") (y #t)))

(define-public crate-various_data_file-0.10.3 (c (n "various_data_file") (v "0.10.3") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)))) (h "0hk45l58h12pw1zlmd9p06j4svj35byvlsymcwb3q76l1xbpmf3s") (y #t)))

(define-public crate-various_data_file-0.10.4 (c (n "various_data_file") (v "0.10.4") (d (list (d (n "file_mmap") (r "^0.14") (d #t) (k 0)))) (h "18h783rzr6qnaawpazkjjlgry6y67hlp41qb8zn78ddm4zq30zqi") (y #t)))

(define-public crate-various_data_file-0.10.5 (c (n "various_data_file") (v "0.10.5") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "0nga3vk4abm6frqaxxsw0wb6a56h03cmw81vn04bczhmc8c6c30a")))

(define-public crate-various_data_file-0.10.6 (c (n "various_data_file") (v "0.10.6") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "14ja3gcfar7qgga67vlaax6x3pvf7b71l1f325v5plal5gdi93qr")))

(define-public crate-various_data_file-0.10.7 (c (n "various_data_file") (v "0.10.7") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "01l82qhyhy9b9x9ab221q6cwyydr6pvpq2pn7nwga19nm5dd92hs")))

(define-public crate-various_data_file-0.10.8 (c (n "various_data_file") (v "0.10.8") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "06kbyxm0prryv8lprvxg4lqskkx7nwfjspkk94gfmlns6p6hvp28")))

(define-public crate-various_data_file-0.11.0 (c (n "various_data_file") (v "0.11.0") (d (list (d (n "file_mmap") (r "^0.15") (d #t) (k 0)))) (h "0nwzn85x3585vhq10p9iwx40n5va6886v5bdxjjvbjiqsw7ri81n")))

(define-public crate-various_data_file-0.12.0 (c (n "various_data_file") (v "0.12.0") (d (list (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "06r4gswrqa6v7m6j6xk5gjbid47sz6h841qcyn7av0cafqrfc5an")))

(define-public crate-various_data_file-0.12.1 (c (n "various_data_file") (v "0.12.1") (d (list (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "1jim4wdmj1xf9crrjscpnrbvzg9z3pwmcvp3yxlm5fgjjl6gy0vz")))

(define-public crate-various_data_file-0.12.2 (c (n "various_data_file") (v "0.12.2") (d (list (d (n "file_mmap") (r "^0.16") (d #t) (k 0)))) (h "1vmjjgzz73r60zdjlg4mq8cs4zpmi8van3vgi8hpiif19lxy3f1g")))

(define-public crate-various_data_file-0.12.3 (c (n "various_data_file") (v "0.12.3") (d (list (d (n "file_mmap") (r "^0.17") (d #t) (k 0)))) (h "0i2zhdhvm10zw6n08xsvvas7xr6awyd3cdvjvj02gjh5nkyrixll")))

(define-public crate-various_data_file-0.12.4 (c (n "various_data_file") (v "0.12.4") (d (list (d (n "file_mmap") (r "^0.18") (d #t) (k 0)))) (h "1cs3pgfg026xppshpa5ryrndw21nnk4051a03ba2vfv34q2cw30j")))

(define-public crate-various_data_file-0.12.5 (c (n "various_data_file") (v "0.12.5") (d (list (d (n "file_mmap") (r "^0.18") (d #t) (k 0)))) (h "0hdrz13hsk0x4qwkdgzbvm370xgwl5hs270d4a3dp2zjfvx100jc")))

(define-public crate-various_data_file-0.12.6 (c (n "various_data_file") (v "0.12.6") (d (list (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "1y5c719jsp39n04l1jaj2xs6602gbbhb4rkv8aalkx90nsyxklhq")))

(define-public crate-various_data_file-0.12.7 (c (n "various_data_file") (v "0.12.7") (d (list (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "1zqlpn39bas5a1sz0nhd62fg2qmagy0bxlgw4gddnp9clsw0xxli")))

(define-public crate-various_data_file-0.13.0 (c (n "various_data_file") (v "0.13.0") (d (list (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "1yjam897djb0622mc3zib3977rxng5djdmv3jbsl8dhwshyyai2l")))

(define-public crate-various_data_file-0.13.1 (c (n "various_data_file") (v "0.13.1") (d (list (d (n "file_mmap") (r "^0.19") (d #t) (k 0)))) (h "034jhh50qdvrr74xwckc20xfqjifjpk82v4wpamq9z6i7pzimdx8")))

(define-public crate-various_data_file-0.13.3 (c (n "various_data_file") (v "0.13.3") (d (list (d (n "file_mmap") (r "^0.19.5") (d #t) (k 0)))) (h "08hvm74ryj0a34c4c3hscdhgiqzgvhfs8rc5lng5bqawbp4nfalw")))

(define-public crate-various_data_file-0.14.0 (c (n "various_data_file") (v "0.14.0") (d (list (d (n "file_mmap") (r "^0.19.6") (d #t) (k 0)))) (h "1c8nr90gy3q4z47m5yw382vfp8zafvqmjvwnlas1kf8wqlqvn0km")))

(define-public crate-various_data_file-0.15.0 (c (n "various_data_file") (v "0.15.0") (d (list (d (n "file_mmap") (r "^0.19.6") (d #t) (k 0)))) (h "1cljvl4gfaa8wpv93jxilx0vs09vi6illbndlszxaqrb2ffjdhfb")))

(define-public crate-various_data_file-0.15.1 (c (n "various_data_file") (v "0.15.1") (d (list (d (n "file_mmap") (r "^0.19.6") (d #t) (k 0)))) (h "1z9gy4lwyprw6zwmg94d3k6fpwbpg2gvs4gli9fsif42zwim7qbf")))

(define-public crate-various_data_file-0.16.0 (c (n "various_data_file") (v "0.16.0") (d (list (d (n "file_mmap") (r "^0.19.6") (d #t) (k 0)))) (h "0gigx2yaaw5qxpnslxr87g062xhpz9w6xg6mai98s5y79r4cj7wq")))

(define-public crate-various_data_file-0.16.1 (c (n "various_data_file") (v "0.16.1") (d (list (d (n "file_mmap") (r "^0.19.6") (d #t) (k 0)))) (h "1hfrggk3g12lgc1l5bya5v0c91qvaaf5g87hxhynj212cpn7c3c4")))

(define-public crate-various_data_file-0.17.0 (c (n "various_data_file") (v "0.17.0") (d (list (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)))) (h "0xc0l90m61nxdsq8lvl5389pk3r669m4hyd499a80fbcbb9r6lml")))

(define-public crate-various_data_file-0.18.0 (c (n "various_data_file") (v "0.18.0") (d (list (d (n "file_mmap") (r "^0.20.0") (d #t) (k 0)))) (h "07w93mc7apmzi7h3l8yrw0pgsrkajlhi5sc2sbr793c89v2ra6vj")))

