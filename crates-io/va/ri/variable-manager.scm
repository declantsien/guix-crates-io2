(define-module (crates-io va ri variable-manager) #:use-module (crates-io))

(define-public crate-variable-manager-1.0.0 (c (n "variable-manager") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.2.0") (d #t) (k 0)) (d (n "rhaki-cw-plus") (r "^0.6.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "variable-manager-pkg") (r "^1.0.0") (d #t) (k 0)))) (h "1i2mw4z1x2w3ajyysnfalciqzl0ssjnvi3c7a1jpspsgqvwfnk9w") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

