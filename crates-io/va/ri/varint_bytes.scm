(define-module (crates-io va ri varint_bytes) #:use-module (crates-io))

(define-public crate-varint_bytes-0.1.0 (c (n "varint_bytes") (v "0.1.0") (d (list (d (n "bytes") (r ">=0.6.0, <0.7.0") (d #t) (k 0)))) (h "0hlqrqq568jbdq1vx7xp457cbgbpy5h8y3yraa11mgzwiqhwy7a2")))

