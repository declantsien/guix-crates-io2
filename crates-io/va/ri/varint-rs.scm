(define-module (crates-io va ri varint-rs) #:use-module (crates-io))

(define-public crate-varint-rs-1.0.0 (c (n "varint-rs") (v "1.0.0") (h "03ybnbbjzdkv294vj01k166gssqh6mzqy6621wlwgqk2wyj5xkxd")))

(define-public crate-varint-rs-1.0.1 (c (n "varint-rs") (v "1.0.1") (h "1plramknggpj3xyjsbw0xcp4m9qxacsmiiim6csybv117165ykam")))

(define-public crate-varint-rs-1.0.2 (c (n "varint-rs") (v "1.0.2") (h "1bkp4vr4l5885h1g1in8rbkjw033xq11b0yc33zq9zwrvrqqd494")))

(define-public crate-varint-rs-1.1.0 (c (n "varint-rs") (v "1.1.0") (h "012nffa5kjxmfxz1pq51bbkam37q84dii8a05sgmasvjb9q5r0zh")))

(define-public crate-varint-rs-1.1.1 (c (n "varint-rs") (v "1.1.1") (h "1cgrgwp0gjvy53c1l1cirsl2knwpng36ihwza2dz65v15rcrgrbg")))

(define-public crate-varint-rs-1.1.2 (c (n "varint-rs") (v "1.1.2") (h "1ykxj2rb1g9xyhiiks10isnglvilpaigg35mbhg9x31xr83i03hz")))

(define-public crate-varint-rs-1.1.3 (c (n "varint-rs") (v "1.1.3") (h "0y89r1db0lqdc8x4sbhmk153036v9qjgal7wcbw7z1b66zg04m96")))

(define-public crate-varint-rs-1.1.4 (c (n "varint-rs") (v "1.1.4") (h "0zprkaj44qi2sv87h4adhnz2pq83h38w7sanrc6zwgq74kpwm192")))

(define-public crate-varint-rs-1.1.5 (c (n "varint-rs") (v "1.1.5") (h "17xl3b6v9pfb794icbgxmjl2qv0jjgxyis7yn11aswr8r5mq7iqi")))

(define-public crate-varint-rs-2.0.0 (c (n "varint-rs") (v "2.0.0") (h "1c4x5dq98ywp0rqzsf4jsia0j7iwsq8zx533wkmvd44528xx2bg7")))

(define-public crate-varint-rs-2.0.1 (c (n "varint-rs") (v "2.0.1") (h "0skdhxrzbdzgad3739iann1ph8xjxfcfj062pk90s2yckbjd86yi")))

(define-public crate-varint-rs-2.1.0 (c (n "varint-rs") (v "2.1.0") (h "12p8s8ckjlsjmbl0lv6wl709f1gj6kqd1bn651b02949hmwxgbdl") (f (quote (("zigzag") ("signed" "zigzag") ("io") ("default" "signed" "io"))))))

(define-public crate-varint-rs-2.1.1 (c (n "varint-rs") (v "2.1.1") (h "0gvry8ps603zcilc67l2qdp3s5w96zda9wzddx2xk8yg628lqlv0") (f (quote (("zigzag") ("signed" "zigzag") ("io") ("default" "signed" "io"))))))

(define-public crate-varint-rs-2.1.2 (c (n "varint-rs") (v "2.1.2") (h "1nfxix1rjfca5c757vsw4qjjkhjcrfnn8d1sn8ss6mixpqq72l1q") (f (quote (("zigzag") ("signed" "zigzag") ("io") ("default" "signed" "io"))))))

(define-public crate-varint-rs-2.1.3 (c (n "varint-rs") (v "2.1.3") (h "1vscw9lvrbh7hb5fqcygyip9xbgkkhi8478zcjd6l8gxc09973h5") (f (quote (("zigzag") ("signed" "zigzag") ("io") ("default" "signed" "io"))))))

(define-public crate-varint-rs-2.1.4 (c (n "varint-rs") (v "2.1.4") (h "00a1h9fxsn307vqa2kraz3gvcm01cprpzd62ipknzhz1245mz7vr") (f (quote (("zigzag") ("signed" "zigzag") ("io") ("default" "signed" "io"))))))

(define-public crate-varint-rs-2.2.0 (c (n "varint-rs") (v "2.2.0") (h "08sb5hcbysq339sk15xfrrndvq1a7vdx6q23gai362b2s1ra2m4g") (f (quote (("std") ("signed") ("default" "std" "signed"))))))

