(define-module (crates-io va ri varianteq) #:use-module (crates-io))

(define-public crate-varianteq-0.1.0 (c (n "varianteq") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "16p7x7qyvp2snw72577pr46xm880gkn3zyzazn8blldlysbsfwh5")))

(define-public crate-varianteq-0.1.1 (c (n "varianteq") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "05nl2kg9sjnanchmlnr4pcy0z4s1mf67my0wcg18k0vc74vi5m99")))

(define-public crate-varianteq-0.2.0 (c (n "varianteq") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "1j27djjyycg2pnrrv4lq8w7hrr3j8b48kk2lymq5y5lkyx3c1h2x")))

(define-public crate-varianteq-0.3.0 (c (n "varianteq") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "1i7slnwz8n19bny7089hpn81mzlv29qy17r5w9v03232w5x3rvmq")))

(define-public crate-varianteq-0.4.0 (c (n "varianteq") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "174hkpljqsj8n7bc54cfpff385ixwh0hayyh8p5gp7fwzm5y17ww")))

(define-public crate-varianteq-0.4.1 (c (n "varianteq") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "07h0k7nhdmvahysg33ypcpp4wfp1va09lwpl0c1n234pln9nyghm")))

(define-public crate-varianteq-0.5.0 (c (n "varianteq") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "08jw3hzgwb8src440mq07hb3zw3cgparmx2gcjpj7vb3ljsrs1cy")))

