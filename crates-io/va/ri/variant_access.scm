(define-module (crates-io va ri variant_access) #:use-module (crates-io))

(define-public crate-variant_access-0.1.0 (c (n "variant_access") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "variant_access_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.1.0") (d #t) (k 0)))) (h "0l3kqm31ak0s6rhr6zxm6gbhk34d8i0mg54vi698wr5nz2l2k101")))

(define-public crate-variant_access-0.1.1 (c (n "variant_access") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "variant_access_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.1.1") (d #t) (k 0)))) (h "0cljh8dg1d3n6q5xjnf8nm2jf33l40004fbfapy0wjqlw72ilhkh")))

(define-public crate-variant_access-0.2.0 (c (n "variant_access") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)) (d (n "variant_access_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.2.0") (d #t) (k 0)))) (h "0jzlw38whnk0p28zp15r8lg43xdmzhlc03llgxdckr8jnysjkrh1")))

(define-public crate-variant_access-0.2.2 (c (n "variant_access") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)) (d (n "variant_access_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.2.2") (d #t) (k 0)))) (h "0f5p2s1lfy5nmikdynk9awmcic4phl8pbrq55fb4sx5blmppwbf3")))

(define-public crate-variant_access-0.3.0 (c (n "variant_access") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)) (d (n "variant_access_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.3.0") (d #t) (k 0)))) (h "0qsp2hyhlip813x0mfsbijbqh4m3xa4z2cym2ql8vz6ah2dzsq3h")))

(define-public crate-variant_access-0.3.1 (c (n "variant_access") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)) (d (n "variant_access_derive") (r "^0.3.1") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.3.1") (d #t) (k 0)))) (h "1w781gqm203z2rsnkb9y7s5qlgzn6kg75s8i6hx3x1k5aja10i25")))

(define-public crate-variant_access-0.4.0 (c (n "variant_access") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)) (d (n "variant_access_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.4.0") (d #t) (k 0)))) (h "0cgp25fgrg5ldm6fm07vxpsk8wva8rvhshnvxaa71jbkq1msdhyw")))

(define-public crate-variant_access-0.4.1 (c (n "variant_access") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)) (d (n "variant_access_derive") (r "^0.4.1") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.4.1") (d #t) (k 0)))) (h "10v0h4qb1jp2wp2w1vhzmvxxl5f5zjaypcs0ciqwf9imh4h41whs")))

