(define-module (crates-io va ri variant_count) #:use-module (crates-io))

(define-public crate-variant_count-1.0.0 (c (n "variant_count") (v "1.0.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1c36b0l70v7rrdjvlcl3h5x78pxggqxily0xk21x2x117d9lld9a")))

(define-public crate-variant_count-1.1.0 (c (n "variant_count") (v "1.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "095i0s2cjxq2lwf1fcxgi82rq9ri8wixxx5bj8ll4qy41bwgmqma")))

