(define-module (crates-io va ri varint-compression) #:use-module (crates-io))

(define-public crate-varint-compression-0.1.0 (c (n "varint-compression") (v "0.1.0") (h "1g31zrmk7293jqjpzl9vs9ybd4kv3ilnp9dl9k6fw9dd5cvlyxwp")))

(define-public crate-varint-compression-0.2.0 (c (n "varint-compression") (v "0.2.0") (h "1p3ymsks5l098ssg1ss45i0j7wbqanba2ydrsxx7nynhlqhpb7zf")))

