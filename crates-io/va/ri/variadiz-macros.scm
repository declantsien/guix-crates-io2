(define-module (crates-io va ri variadiz-macros) #:use-module (crates-io))

(define-public crate-variadiz-macros-0.1.0 (c (n "variadiz-macros") (v "0.1.0") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14mk61k80p2pwvk5wik32bchfsfbgyb9b3zx56dmcwv9mrpjs88r")))

(define-public crate-variadiz-macros-0.1.1 (c (n "variadiz-macros") (v "0.1.1") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d67ixgynwa1ybjqaqmqm4wxy345p4sdm31x8bjs20fb01867y4q")))

(define-public crate-variadiz-macros-0.1.3 (c (n "variadiz-macros") (v "0.1.3") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07cx0031p7la007kyn6q48y3rjfdvjnlj7k3mlldxq9a71yi2m2d")))

(define-public crate-variadiz-macros-0.1.4 (c (n "variadiz-macros") (v "0.1.4") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xx2s4wgxq544lljxfzy2jnj68hjqd1a8kzwm8046kin425j8hkm")))

(define-public crate-variadiz-macros-0.2.0 (c (n "variadiz-macros") (v "0.2.0") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hc3ryzb5718qq3hrmfglslv34gnh8qsj2psjh8w5r6rbi90qbal")))

(define-public crate-variadiz-macros-1.0.0 (c (n "variadiz-macros") (v "1.0.0") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wr6yxhab9m7av81c3imac5ghdhlxhd5hr6dgx5444zh47iz955w")))

(define-public crate-variadiz-macros-1.1.0 (c (n "variadiz-macros") (v "1.1.0") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pbxfya1fvdb298drpz48z7nzl846b92c3v6r3bzyjmhy1yfk8mi")))

(define-public crate-variadiz-macros-1.1.1 (c (n "variadiz-macros") (v "1.1.1") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0dsk1r5d9b5kvbs0lpc7rcbaf0a7vv5p6aphzi8h514skx22pc4h")))

(define-public crate-variadiz-macros-1.1.2 (c (n "variadiz-macros") (v "1.1.2") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1cl1vb9w211msdf388i5a217abs85813s6qbk07ikg0kxflfmz18")))

(define-public crate-variadiz-macros-1.1.3 (c (n "variadiz-macros") (v "1.1.3") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0a2cbpcgkjy93pkyjxf9fi6yy691dnmr47yc996mf4xzs3ll4rpi")))

(define-public crate-variadiz-macros-1.1.4 (c (n "variadiz-macros") (v "1.1.4") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "14bfgbs02lnfqpggzdlbd0hv63ia19c68zlwxwr9i1samar260ic")))

(define-public crate-variadiz-macros-1.1.5 (c (n "variadiz-macros") (v "1.1.5") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0w5sm23pklyv400r4bvgjjadj9843fyfsffr3cwaza3f09cp5i5g")))

(define-public crate-variadiz-macros-1.2.0 (c (n "variadiz-macros") (v "1.2.0") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "18lw4hr04s4pb98lvp2mfh1p5lwf3wfy8brdydz5i86i3ard1mc3")))

(define-public crate-variadiz-macros-1.2.1 (c (n "variadiz-macros") (v "1.2.1") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0y14kmjkddwa97g5rr580b3nwq9ch69s8gjyzav4w1aki5yrk6jm")))

(define-public crate-variadiz-macros-1.3.0 (c (n "variadiz-macros") (v "1.3.0") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1h44wff8i5m3lmfqxqz5jw4c0rsii37r85xdr0wxnyr9f453pavm")))

(define-public crate-variadiz-macros-1.3.1 (c (n "variadiz-macros") (v "1.3.1") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1m43krh69afzsn9jkqi47zm5dwakzghsj9rqnwi1p42lzx76qg86")))

(define-public crate-variadiz-macros-1.4.0-alpha (c (n "variadiz-macros") (v "1.4.0-alpha") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1gls5czf8chjdwz3fj3m5xyx474afxdlfyfnc4lzmvykycmh11f8")))

(define-public crate-variadiz-macros-1.4.0 (c (n "variadiz-macros") (v "1.4.0") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1xxi258pq06h997kwbnarqd9agrf1n6hq2mhaghm43kn0ib35033")))

