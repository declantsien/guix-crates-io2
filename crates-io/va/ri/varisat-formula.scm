(define-module (crates-io va ri varisat-formula) #:use-module (crates-io))

(define-public crate-varisat-formula-0.2.1 (c (n "varisat-formula") (v "0.2.1") (d (list (d (n "proptest") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "15wrxfrdkynkr025yj061crd6gm7xyqdbbbpbj2rwxnyqsslmm3q") (f (quote (("proptest-strategies" "proptest") ("internal-testing" "proptest" "rand") ("default"))))))

(define-public crate-varisat-formula-0.2.2 (c (n "varisat-formula") (v "0.2.2") (d (list (d (n "proptest") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "08256rfjdmvfxjjw162r6l5ipfd46in9vx1sdmnhgndzp51map1r") (f (quote (("proptest-strategies" "proptest") ("internal-testing" "proptest" "rand") ("default"))))))

