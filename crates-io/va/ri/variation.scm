(define-module (crates-io va ri variation) #:use-module (crates-io))

(define-public crate-variation-0.1.0 (c (n "variation") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "09msssq0cwq8s5asf9c27fsz3knwycff7b9s1ifwly751yyp16x8")))

(define-public crate-variation-0.1.1 (c (n "variation") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "1r2jxpwzwv317c7pr6iyy4z324dd5jcfbc4ldkz2jik1cfkzwbmx")))

