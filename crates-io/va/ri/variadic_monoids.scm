(define-module (crates-io va ri variadic_monoids) #:use-module (crates-io))

(define-public crate-variadic_monoids-0.1.0 (c (n "variadic_monoids") (v "0.1.0") (d (list (d (n "tuple_list") (r "^0.1.2") (d #t) (k 0)))) (h "17xmbjpg2k6scgw6gksyp1xplinc2z5c3n7y0bd86abf0zckzdnz")))

(define-public crate-variadic_monoids-0.1.1 (c (n "variadic_monoids") (v "0.1.1") (d (list (d (n "tuple_list") (r "^0.1.2") (d #t) (k 0)))) (h "08gypr2a3kddlsvk6qb076njy7rafn4rgvw6nxfdzy4z2jwsdkra")))

