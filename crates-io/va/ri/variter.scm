(define-module (crates-io va ri variter) #:use-module (crates-io))

(define-public crate-variter-0.1.0 (c (n "variter") (v "0.1.0") (h "0r0y2gvrkgz9hwd5dssjpswid66dgzix6vdrb94m8slribg9mf83") (f (quote (("std") ("default" "std"))))))

(define-public crate-variter-0.1.1 (c (n "variter") (v "0.1.1") (h "0m085y082av6xzmpk7cx7b3zyn5qskf99lm5175l9wsjy1kazcdf") (f (quote (("std") ("default" "std"))))))

(define-public crate-variter-0.2.0 (c (n "variter") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0nis9s6awxshxx7qqhajgjsxcfarlf7p5mzs7i9wl5x9q60h49cg") (f (quote (("std") ("foreign_impls") ("default" "std" "foreign_impls"))))))

(define-public crate-variter-0.2.1 (c (n "variter") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0gaqg8lnjhwzw5fyj4s0na54m9ayjqqmvzwnwlx0sjan8k8k45jv") (f (quote (("std") ("foreign_impls") ("default" "std" "foreign_impls"))))))

(define-public crate-variter-0.2.2 (c (n "variter") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "113c15w84y5cbjflahlc6zcixgmyzmrcz03hxxbhap0qbjjhn63s") (f (quote (("std") ("foreign_impls") ("default" "std" "foreign_impls"))))))

(define-public crate-variter-0.3.0 (c (n "variter") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1drjsb2s941x8bspa68xhs76jdrx8xmnbps4zxxzdgbjcfwy9l9r") (f (quote (("std") ("foreign_impls") ("default" "std" "foreign_impls"))))))

