(define-module (crates-io va ri varint-simd) #:use-module (crates-io))

(define-public crate-varint-simd-0.1.0 (c (n "varint-simd") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1pdd466xw3sknqp4gjp8fy9pnv81wdhjw21qy23adajxg21z0ddi") (y #t)))

(define-public crate-varint-simd-0.1.1 (c (n "varint-simd") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0v9njwiy3pvzmhxjbadrrirsiwd421y3jbw4c0wl41vxmsmqyfw1")))

(define-public crate-varint-simd-0.2.0 (c (n "varint-simd") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0f9prq3fm80566km0463z92rp7xi2vxs9ijjnvk5sv0ghm4ffd8i") (f (quote (("native-optimizations") ("dangerously-force-enable-pdep-since-i-really-know-what-im-doing"))))))

(define-public crate-varint-simd-0.2.1 (c (n "varint-simd") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0qhdqfqr0fdp2dy1dw6yvzjzirsha8ciqnqblzc5w8rzdlrxnllf") (f (quote (("native-optimizations") ("dangerously-force-enable-pdep-since-i-really-know-what-im-doing"))))))

(define-public crate-varint-simd-0.3.0 (c (n "varint-simd") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^2.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)))) (h "16bsp43n6x287bkr6idxn21hbcfdawy64i6awzjnxdcc8zq37lcx") (f (quote (("native-optimizations") ("dangerously-force-enable-pdep-since-i-really-know-what-im-doing"))))))

(define-public crate-varint-simd-0.4.0 (c (n "varint-simd") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "integer-encoding") (r "^2.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)))) (h "152s3lzh20h2f157n3d06fhlpdlnnxsbygq5vvq04q70fwi8sswx") (f (quote (("std") ("native-optimizations") ("default" "std") ("dangerously-force-enable-pdep-since-i-really-know-what-im-doing"))))))

