(define-module (crates-io va ri vari) #:use-module (crates-io))

(define-public crate-vari-0.1.0 (c (n "vari") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "118lqyxw1c57bmm55fy2ihhpi5agcwl1srprwq4s9b5052x8pqk9")))

(define-public crate-vari-0.1.1 (c (n "vari") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0xa3gxghzv4g1h084q8y47vh69nvqixkgx8jlg146cfkyiiq0hbk")))

(define-public crate-vari-0.1.2 (c (n "vari") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1vifqyrvb7y1mmqipm3rynjrwz6n534z4bdbr9hdib08ks0yiq20")))

(define-public crate-vari-0.1.3 (c (n "vari") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pdd985rdmyvzycsmn762wai1z07nygd5wx90j9zk5ipypdh10fa")))

(define-public crate-vari-0.1.4 (c (n "vari") (v "0.1.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0i40jb0d440w0fpz3vvpbsglbchx74dznrs0829s37q1pr4w5lnq")))

(define-public crate-vari-0.1.5 (c (n "vari") (v "0.1.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0v3afivjhy6db1xb7x09y4ap99jscika06178d3zaczphrhpgj8b")))

(define-public crate-vari-0.1.6 (c (n "vari") (v "0.1.6") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0adywlxq5wqgaamw6p2bmr6nf4k0mrn7gdnfx79a7cws0g3sbmv0")))

(define-public crate-vari-0.1.7 (c (n "vari") (v "0.1.7") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0s2s8cmhvcdy4jcm6aykarwvw9hz1a1pakbnghdql964dwwkvwcj")))

(define-public crate-vari-0.1.8 (c (n "vari") (v "0.1.8") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0xnkid7fdj3j88kq3i048wv0f2hljalw4scivyr1d2azkvdc4z6k")))

(define-public crate-vari-0.2.0 (c (n "vari") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "07j5yf3mvh7ks4m88gb7yb0rcdvw8zgbaqy23hpc4al2ss6k2dia") (y #t)))

(define-public crate-vari-0.2.1 (c (n "vari") (v "0.2.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1kyry3idmlwmlfd1qqsgg36lvxqv7xm2q8fk7gm36nggnnyz5hk8")))

