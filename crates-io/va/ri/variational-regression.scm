(define-module (crates-io va ri variational-regression) #:use-module (crates-io))

(define-public crate-variational-regression-0.1.0 (c (n "variational-regression") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "special") (r "^0.8.1") (d #t) (k 0)))) (h "0ybgxs8nc9vi99b74w77gkrs6kijhpdrslkvsm4829wr5rpnkxwb")))

