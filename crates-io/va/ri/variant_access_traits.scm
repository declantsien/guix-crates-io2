(define-module (crates-io va ri variant_access_traits) #:use-module (crates-io))

(define-public crate-variant_access_traits-0.1.0 (c (n "variant_access_traits") (v "0.1.0") (h "1qimh3vxn47jpp2djy1pardlya6b7hr1likwi76bqzpazi3s9g49")))

(define-public crate-variant_access_traits-0.1.1 (c (n "variant_access_traits") (v "0.1.1") (h "121nn9w5m3xkqxsaxzqhqsmchalscaabkgjqa7j9ar9dnmah63c3")))

(define-public crate-variant_access_traits-0.2.0 (c (n "variant_access_traits") (v "0.2.0") (h "1k63pnvlx86ph0dkf5cbpcy0d9pl8lzzdxh9va9apjy1an9b8yrq")))

(define-public crate-variant_access_traits-0.2.1 (c (n "variant_access_traits") (v "0.2.1") (h "15bhp2vf8bcmr7402ip1ixdlhnwh58hq6g1n0fbjfpzp94rczymv")))

(define-public crate-variant_access_traits-0.2.2 (c (n "variant_access_traits") (v "0.2.2") (h "0zab08qpqsvxpra16py3pqzympi3yrd4mv9i3260x7xp56mc27vw")))

(define-public crate-variant_access_traits-0.3.0 (c (n "variant_access_traits") (v "0.3.0") (h "0ck8h8gq9zqyppzsdgl7d4l9igf8549zykzjnirhrcycnmrbqaqp")))

(define-public crate-variant_access_traits-0.3.1 (c (n "variant_access_traits") (v "0.3.1") (h "03852rc9xmapl0pjp95d5kqwjkmfmll8yw8l9gpi0kk5gi6ja1cd")))

(define-public crate-variant_access_traits-0.4.0 (c (n "variant_access_traits") (v "0.4.0") (h "1ahm350gjq0lhrc4i69jh8yykfhy30q98dbs8qynm85kq69jk6qh")))

(define-public crate-variant_access_traits-0.4.1 (c (n "variant_access_traits") (v "0.4.1") (h "0gk41mmfbx220b4x4w71lisgz6wz9jaxz2k2kkfi52dvhdd5rmvl")))

