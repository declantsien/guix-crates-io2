(define-module (crates-io va ri varint) #:use-module (crates-io))

(define-public crate-varint-0.0.0 (c (n "varint") (v "0.0.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "0ay7fvaslaxdqv4cpgiw5j93pj8b8byijcddbpc3pqdz4lj1an5q")))

(define-public crate-varint-0.1.0 (c (n "varint") (v "0.1.0") (h "079jbiz00ig922bhjyajhm857xw8wp6gby2dp2xb5i0lp72lrlsp")))

(define-public crate-varint-0.2.0 (c (n "varint") (v "0.2.0") (d (list (d (n "bit_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1g2b7459w9lxy2qqhfxyzan9938yjldklf7sz6gv4irji9vqlnkn")))

(define-public crate-varint-0.9.0 (c (n "varint") (v "0.9.0") (d (list (d (n "bit_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "io_operations") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0yw8cnnmlqfvwb7h92axzk9h77zhwsq9j6b8j0kwgs8b9rkyayy3") (f (quote (("nightly") ("io-operations" "io_operations"))))))

