(define-module (crates-io va ri variadiz) #:use-module (crates-io))

(define-public crate-variadiz-0.1.0 (c (n "variadiz") (v "0.1.0") (d (list (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^0.1.0") (d #t) (k 0)))) (h "12xjl9ifzqs2q86zv9jf37zfz9wpjhpjy748xy5x5w74rdczr0ya")))

(define-public crate-variadiz-0.1.1 (c (n "variadiz") (v "0.1.1") (d (list (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0h5wq7dnqlvgr6pcvzj8gkjyw6bw95fmri20bf8fhr4fbl8g4xqh")))

(define-public crate-variadiz-0.1.2 (c (n "variadiz") (v "0.1.2") (d (list (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1ngin2nncxgh6vzqp584v5iib14r235p6iilvb0vhjrc744z64cy")))

(define-public crate-variadiz-0.1.3 (c (n "variadiz") (v "0.1.3") (d (list (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^0.1.3") (d #t) (k 0)))) (h "14k7fwvr31fzhsx8vf5i16zb6ldnm60syp058c7c0pnn7myxnnzk")))

(define-public crate-variadiz-0.1.4 (c (n "variadiz") (v "0.1.4") (d (list (d (n "tuplez") (r "^0.9.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^0.1.4") (d #t) (k 0)))) (h "0dkvkpgi6xb5qm1pc9552mprfyfsaxz7vmd5hpxw2vhih3k35920")))

(define-public crate-variadiz-0.2.0 (c (n "variadiz") (v "0.2.0") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^0.1.4") (d #t) (k 0)))) (h "0v9ml7nxwhl9q1iw8ingmk36cns8pcaxs3sqilg9bx6c5jxncr6r")))

(define-public crate-variadiz-0.3.0 (c (n "variadiz") (v "0.3.0") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1hirnn1454fycfq5g18plzb13vhjnhmkv5cac071zzv7c042xihw")))

(define-public crate-variadiz-1.0.0 (c (n "variadiz") (v "1.0.0") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.0.0") (d #t) (k 0)))) (h "1j6xzrlmx810pn0khgir95c7h8qw7dn122bxgm778hjjy9rznxk6")))

(define-public crate-variadiz-1.0.1 (c (n "variadiz") (v "1.0.1") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0hbcazqsllpxhskj75cl57s9rh1x2isdrwcrd95sznxzy1n0diw6")))

(define-public crate-variadiz-1.0.2 (c (n "variadiz") (v "1.0.2") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.0.0") (d #t) (k 0)))) (h "19cvhlcw9irk4gcppnm12hscb53bwj3wnxxgpzcm2gdy2j6l8c8c")))

(define-public crate-variadiz-1.0.3 (c (n "variadiz") (v "1.0.3") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.0.0") (d #t) (k 0)))) (h "1g3bn0b3p80j472xprrzs1z09g5jpr0dn5jci9lm03yiqy5g9m2r")))

(define-public crate-variadiz-1.0.4 (c (n "variadiz") (v "1.0.4") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0bdgy1qh7qfr4zg1jy4z4ip49g14b672pmribw0q3g14kclhsl49")))

(define-public crate-variadiz-1.1.0 (c (n "variadiz") (v "1.1.0") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.1.0") (d #t) (k 0)))) (h "15vfl1cfgdqzgh761ybwicpij5jckwv9p1xjn644i6ipyfzhc3va")))

(define-public crate-variadiz-1.1.1 (c (n "variadiz") (v "1.1.1") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.1.0") (d #t) (k 0)))) (h "0q4a5r9l73hb8daz1ahpp9wwgwzidwsn9rbrbqrkgmcmjvcsryiz")))

(define-public crate-variadiz-1.1.2 (c (n "variadiz") (v "1.1.2") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.1.1") (d #t) (k 0)))) (h "0br85scadkxrgplxhj4a9pw7k5zibp7h9a0s45wphnawrvkb896r")))

(define-public crate-variadiz-1.1.3 (c (n "variadiz") (v "1.1.3") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.1.2") (d #t) (k 0)))) (h "1pdql4gc89mb5y7xfk953d3rl46g9w2j66nql3hbf83gyx0npz4y")))

(define-public crate-variadiz-1.1.4 (c (n "variadiz") (v "1.1.4") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.1.3") (d #t) (k 0)))) (h "1a2x8ksxdw03slwrhxai4fzgsl68n99mwyi3mh1axq4jp2m7h5vc")))

(define-public crate-variadiz-1.2.0 (c (n "variadiz") (v "1.2.0") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.2.0") (d #t) (k 0)))) (h "1h3n3v84ddc7wl3mc1vhm1wmf6ncg412xk2xxdal527c1ll188mg")))

(define-public crate-variadiz-1.3.0 (c (n "variadiz") (v "1.3.0") (d (list (d (n "tuplez") (r ">=0.8.0") (d #t) (k 0)) (d (n "variadiz-macros") (r "^1.3.0") (d #t) (k 0)))) (h "1gz28qw5mq80w0nbhnz3h9n42p5qh5zi4l76n1xjp0qk6sr2ymsf")))

(define-public crate-variadiz-1.3.1 (c (n "variadiz") (v "1.3.1") (d (list (d (n "tuplez") (r ">=0.14.11") (k 0)) (d (n "variadiz-macros") (r "^1.3.0") (d #t) (k 0)))) (h "142g3bg00k7s3g3n1jbdl4ynpvsdmryqrp3nyx5aqm3njjjmbn7v")))

(define-public crate-variadiz-1.3.2 (c (n "variadiz") (v "1.3.2") (d (list (d (n "tuplez") (r ">=0.14.11") (k 0)) (d (n "variadiz-macros") (r "^1.3.0") (d #t) (k 0)))) (h "01wkdgn6r0jpglxj6a5ckc41gjnaahszs7b26bcpfn1h3kw9v13q")))

(define-public crate-variadiz-1.4.0-alpha (c (n "variadiz") (v "1.4.0-alpha") (d (list (d (n "tuplez") (r ">=0.14.14") (k 0)) (d (n "variadiz-macros") (r "^1.4.0-alpha") (d #t) (k 0)))) (h "1j5m3hs4ai6vw6ys3y3y6dp78br1pwlhpjadyss9mzwd4nhmwxr4")))

(define-public crate-variadiz-1.4.0 (c (n "variadiz") (v "1.4.0") (d (list (d (n "tuplez") (r ">=0.14.14") (k 0)) (d (n "variadiz-macros") (r "^1.4.0") (d #t) (k 0)))) (h "1c0d3gxkp28jmx2rrjhynqk5hkmmbcvdmhwh1zp8b59836nvl8f7")))

