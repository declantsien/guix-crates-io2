(define-module (crates-io va ri variant-map) #:use-module (crates-io))

(define-public crate-variant-map-0.1.0 (c (n "variant-map") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)) (d (n "variant-map-derive") (r "^0.1.0") (o #t) (k 0)))) (h "12vc6mk7sx79fglzd9w680cldm8w03racc87drvck30h3axdmcjz") (f (quote (("struct-map" "derive") ("macros")))) (s 2) (e (quote (("derive" "dep:variant-map-derive"))))))

(define-public crate-variant-map-0.1.1 (c (n "variant-map") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)) (d (n "variant-map-derive") (r "^0.1.0") (o #t) (k 0)))) (h "0ialj563k5l5g2pr7v4w1zqlk58sw4dvbyk2l1kzxdpy344bz17c") (f (quote (("struct-map" "derive") ("macros")))) (s 2) (e (quote (("derive" "dep:variant-map-derive"))))))

