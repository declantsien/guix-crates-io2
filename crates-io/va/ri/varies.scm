(define-module (crates-io va ri varies) #:use-module (crates-io))

(define-public crate-varies-0.1.0 (c (n "varies") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a1y51hxygrv77q268knqgsay7xl6pprdyapaarkmmiranzly9s4")))

(define-public crate-varies-0.1.1 (c (n "varies") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vw3j6gaf3hgdfqw69avd5fbqydby4mwvpg4a2q6bmvjl7im2w3f")))

