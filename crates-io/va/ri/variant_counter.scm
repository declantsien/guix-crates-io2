(define-module (crates-io va ri variant_counter) #:use-module (crates-io))

(define-public crate-variant_counter-0.1.0 (c (n "variant_counter") (v "0.1.0") (d (list (d (n "variant_counter_derived") (r "^0.1.0") (d #t) (k 0)))) (h "0z54b4v4c6mi9qlrw1kv65bmkdazdfll8218bppjrmgzfas8d0d5")))

(define-public crate-variant_counter-0.2.0 (c (n "variant_counter") (v "0.2.0") (d (list (d (n "variant_counter_derived") (r "^0.2.0") (d #t) (k 0)))) (h "1nz5im15yzx56m435qz6cfirfyk957wrkms7369nnswa241xaj2m")))

(define-public crate-variant_counter-0.2.1 (c (n "variant_counter") (v "0.2.1") (d (list (d (n "variant_counter_derived") (r "^0.2.0") (d #t) (k 0)) (d (n "variant_counter_derived") (r "^0.2.0") (f (quote ("check" "erase"))) (d #t) (k 2)))) (h "1ari0sbh1avp8dwddac1xv4h0s17smw5ddlyjs91djr89191wxjg") (f (quote (("erase" "variant_counter_derived/erase") ("check" "variant_counter_derived/check"))))))

(define-public crate-variant_counter-0.2.2 (c (n "variant_counter") (v "0.2.2") (d (list (d (n "variant_counter_derived") (r "^0.2") (d #t) (k 0)) (d (n "variant_counter_derived") (r "^0.2") (f (quote ("check" "erase"))) (d #t) (k 2)))) (h "10lnbnrad1288yk2axn7cn7q0qlhsdmwl6r4fsbkqidly89jmniy") (f (quote (("erase" "variant_counter_derived/erase") ("check" "variant_counter_derived/check"))))))

(define-public crate-variant_counter-0.3.0 (c (n "variant_counter") (v "0.3.0") (d (list (d (n "variant_counter_derived") (r "^0.3") (d #t) (k 0)) (d (n "variant_counter_derived") (r "^0.3") (f (quote ("check" "erase"))) (d #t) (k 2)))) (h "0bah7w2rxc69q6qrrxpscwaawvgxsapi4k2rl2f65wqjs8i5j3zx") (f (quote (("erase" "variant_counter_derived/erase") ("check" "variant_counter_derived/check"))))))

(define-public crate-variant_counter-0.4.0 (c (n "variant_counter") (v "0.4.0") (d (list (d (n "variant_counter_derived") (r "^0.4") (d #t) (k 0)) (d (n "variant_counter_derived") (r "^0.4") (f (quote ("full"))) (d #t) (k 2)))) (h "0mcx8i7yq7vfkiwwqb39svxdkhvsj09nwx4nccksb80slxyb89gy") (f (quote (("std" "variant_counter_derived/std") ("stats" "variant_counter_derived/stats") ("full" "variant_counter_derived/full") ("erase" "variant_counter_derived/erase") ("default" "std") ("check" "variant_counter_derived/check"))))))

