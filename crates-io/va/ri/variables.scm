(define-module (crates-io va ri variables) #:use-module (crates-io))

(define-public crate-variables-0.1.0 (c (n "variables") (v "0.1.0") (h "1rdzlcvs2s4gw59wmh83njkf5711f0axw6skg3j76mv8zxqsg4h5") (y #t)))

(define-public crate-variables-0.1.1 (c (n "variables") (v "0.1.1") (h "0m9bipgzd0kki9slrjwl1mih54ab5gp8dzb4nl2xsip18797ky87") (y #t)))

(define-public crate-variables-0.1.2 (c (n "variables") (v "0.1.2") (h "084vm2bpl1yqfvq2cy54fy1n4pknlpqh3vm6halnxv8ashli8y63") (y #t)))

(define-public crate-variables-0.1.3 (c (n "variables") (v "0.1.3") (h "0znw3jfm9wdignya7bhcivijb3spgxmwp065x0b2nygq2b6vdspl") (y #t)))

