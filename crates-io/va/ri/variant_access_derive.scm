(define-module (crates-io va ri variant_access_derive) #:use-module (crates-io))

(define-public crate-variant_access_derive-0.1.0 (c (n "variant_access_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.1.0") (d #t) (k 0)))) (h "0i0wks3r0c0r0kq516fsnc99s3b91wq0icm999s1psxdxzrdprfw")))

(define-public crate-variant_access_derive-0.1.1 (c (n "variant_access_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.1.1") (d #t) (k 0)))) (h "153bmaammlwadpabyka5glv79w1sv9pzdnd6pb3w0k1mvzy26333")))

(define-public crate-variant_access_derive-0.2.0 (c (n "variant_access_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.2.0") (d #t) (k 0)))) (h "133dikm30hkhazkavn768z6zxrsj6f2c43zjyxgwb6jrfyzwxfl2")))

(define-public crate-variant_access_derive-0.2.2 (c (n "variant_access_derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.2.1") (d #t) (k 0)))) (h "1w46bd54wg691dj1g04rn0agp7rgwikl6vhyqwgjvl0pabhgr6y3")))

(define-public crate-variant_access_derive-0.3.0 (c (n "variant_access_derive") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.3.0") (d #t) (k 0)))) (h "1y0297pk8bqjlas3lpk3lvnafk20m79xzx868xs9739wiwmcgzw0")))

(define-public crate-variant_access_derive-0.3.1 (c (n "variant_access_derive") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.3.1") (d #t) (k 0)))) (h "1v0bvzqd1pvxsyzmpwnbkg4w5flyhwgavlb3gd4spxz82yc1akrm")))

(define-public crate-variant_access_derive-0.4.0 (c (n "variant_access_derive") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.4.0") (d #t) (k 0)))) (h "1acq1dy95imd7xqn2ziqshgs6nicz6iwi07n0lkhq2yqq3iwgc2c")))

(define-public crate-variant_access_derive-0.4.1 (c (n "variant_access_derive") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "variant_access_traits") (r "^0.4.1") (d #t) (k 0)))) (h "0d6qfg42215nrw7789jxi9i3j2l53r0ww5wjnj0ysm5qmzzkblpb")))

