(define-module (crates-io va ri varisat-internal-proof) #:use-module (crates-io))

(define-public crate-varisat-internal-proof-0.2.1 (c (n "varisat-internal-proof") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proptest") (r "^0.9.3") (d #t) (k 2)) (d (n "varisat-formula") (r "= 0.2.1") (d #t) (k 0)))) (h "1bgrdzsglm0prji1rsz3s20y8m6ff3fkm0di1x0q31aa7bq56xas")))

(define-public crate-varisat-internal-proof-0.2.2 (c (n "varisat-internal-proof") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "varisat-formula") (r "=0.2.2") (d #t) (k 0)))) (h "01yj4zalzp6x6wa0yr3xl8v1q51xh1vgjr3dnxvz12h1r5xvnqv1")))

