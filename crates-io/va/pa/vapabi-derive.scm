(define-module (crates-io va pa vapabi-derive) #:use-module (crates-io))

(define-public crate-vapabi-derive-9.0.1 (c (n "vapabi-derive") (v "9.0.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "vapabi") (r "^9.0.1") (d #t) (k 0)))) (h "09n3mdas8w8y9x3cmmmagd7b3kn2sq17p3l2073h82l2hpb2d2a6")))

