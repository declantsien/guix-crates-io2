(define-module (crates-io va pa vapash-engine) #:use-module (crates-io))

(define-public crate-vapash-engine-0.0.0 (c (n "vapash-engine") (v "0.0.0") (h "0cl632ckv8gg1li5abd7p52s6qky8h514xdvyzqvfyqf8yry70j3")))

(define-public crate-vapash-engine-0.1.0 (c (n "vapash-engine") (v "0.1.0") (d (list (d (n "block-reward") (r "^0.1.0") (d #t) (k 0)) (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "enjen") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mashina") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 2)) (d (n "tetsy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 2)) (d (n "tetsy-unexpected") (r "^0.1.0") (d #t) (k 0)) (d (n "vapash") (r "^1.12.0") (d #t) (k 0)) (d (n "vapjson") (r "^0.1.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "025cj1n82qchm7kkd217x0mi9gww2yvy8rl1qi5ww19jsjy6m184")))

