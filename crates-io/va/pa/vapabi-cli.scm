(define-module (crates-io va pa vapabi-cli) #:use-module (crates-io))

(define-public crate-vapabi-cli-9.0.1 (c (n "vapabi-cli") (v "9.0.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (k 0)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.0") (d #t) (k 0)) (d (n "vapabi") (r "^9.0.1") (d #t) (k 0)))) (h "0105053d227fcdsim93p45h4g85b7r790d822kr2rms19gjdz2ni") (f (quote (("backtrace" "error-chain/backtrace"))))))

