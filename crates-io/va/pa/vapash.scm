(define-module (crates-io va pa vapash) #:use-module (crates-io))

(define-public crate-vapash-0.0.0 (c (n "vapash") (v "0.0.0") (h "080riqz6l1dsbwkbjx0vg9h3zs40aw76lw2n047pifsl1hbyyh8v")))

(define-public crate-vapash-1.12.0 (c (n "vapash") (v "1.12.0") (d (list (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "either") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "03fsq4sasgiy8yyzxgjj3nf4jzx215546q4fmxg5sz9dgmnys1gj") (f (quote (("default") ("bench"))))))

