(define-module (crates-io va pa vapabi) #:use-module (crates-io))

(define-public crate-vapabi-9.0.1 (c (n "vapabi") (v "9.0.1") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "hex-literal") (r "^0.2.0") (d #t) (k 2)) (d (n "paste") (r "^0.1.5") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "00b3sqjlzfzlc9w80g35l1x1q2sd1wrwzvskw6bswp5qhckfkv9f") (f (quote (("backtrace" "error-chain/backtrace"))))))

