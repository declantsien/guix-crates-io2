(define-module (crates-io va po vapor_archive) #:use-module (crates-io))

(define-public crate-vapor_archive-0.1.0 (c (n "vapor_archive") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "blake2") (r "^0.8.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.5.2") (d #t) (k 0)))) (h "0biqiv6kapp532m5ry1biaxl0k3bik8s3lvympd58jq0van5nspq")))

