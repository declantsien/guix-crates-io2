(define-module (crates-io va po vaporetto_tantivy) #:use-module (crates-io))

(define-public crate-vaporetto_tantivy-0.3.0 (c (n "vaporetto_tantivy") (v "0.3.0") (d (list (d (n "ruzstd") (r "^0.2.4") (d #t) (k 2)) (d (n "tantivy") (r "^0.16") (d #t) (k 0)) (d (n "vaporetto") (r "^0.3.0") (d #t) (k 0)) (d (n "vaporetto_rules") (r "^0.3.0") (d #t) (k 0)))) (h "1varrcm1vwrrq9d5iiaif5zda0xymskpwa2nx4wbzg8m9203js5d")))

(define-public crate-vaporetto_tantivy-0.4.0 (c (n "vaporetto_tantivy") (v "0.4.0") (d (list (d (n "ruzstd") (r "^0.2.4") (d #t) (k 2)) (d (n "tantivy") (r "^0.17") (d #t) (k 0)) (d (n "vaporetto") (r "^0.4.0") (d #t) (k 0)) (d (n "vaporetto_rules") (r "^0.4.0") (d #t) (k 0)))) (h "0m746d5kvi0cpclrgavfbgh1x2c4k9aby6claayyl10b2rqwrz8y")))

(define-public crate-vaporetto_tantivy-0.5.0 (c (n "vaporetto_tantivy") (v "0.5.0") (d (list (d (n "ruzstd") (r "^0.2.4") (d #t) (k 2)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "vaporetto") (r "^0.5.0") (d #t) (k 0)) (d (n "vaporetto_rules") (r "^0.5.0") (d #t) (k 0)))) (h "1g3fx5qxsml20szihfy4q201vhl22n1qassj75a6knydpra5b27k")))

(define-public crate-vaporetto_tantivy-0.5.1 (c (n "vaporetto_tantivy") (v "0.5.1") (d (list (d (n "ruzstd") (r "^0.2.4") (d #t) (k 2)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "vaporetto") (r "=0.5.1") (d #t) (k 0)) (d (n "vaporetto_rules") (r "=0.5.1") (d #t) (k 0)))) (h "12bh0k2rxrh934wwshhbyc8rzalsd1bhxry9yg59lp38hf70la9x") (r "1.58")))

(define-public crate-vaporetto_tantivy-0.6.0 (c (n "vaporetto_tantivy") (v "0.6.0") (d (list (d (n "ruzstd") (r "^0.3.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.0") (d #t) (k 0)) (d (n "vaporetto_rules") (r "=0.6.0") (d #t) (k 0)))) (h "1fjj73rv6a2xz6rwbjq5n2gcqw9xysgpxjwvpca9c1cq5bvc22k0") (r "1.60")))

(define-public crate-vaporetto_tantivy-0.6.1 (c (n "vaporetto_tantivy") (v "0.6.1") (d (list (d (n "ruzstd") (r "^0.3.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.1") (d #t) (k 0)) (d (n "vaporetto_rules") (r "=0.6.1") (d #t) (k 0)))) (h "1n0pq63ly0ln7x1dmw7qlk746wd0920yrnf08gr8yv0xammwvnrc") (r "1.60")))

(define-public crate-vaporetto_tantivy-0.19.0 (c (n "vaporetto_tantivy") (v "0.19.0") (d (list (d (n "ruzstd") (r "^0.3.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.19") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.1") (d #t) (k 0)) (d (n "vaporetto_rules") (r "=0.6.1") (d #t) (k 0)))) (h "0mx1r5kvxwys65lcpy6bjrq7q0d8q395pa2wcglznszrb5khrq6f") (r "1.64")))

(define-public crate-vaporetto_tantivy-0.19.1 (c (n "vaporetto_tantivy") (v "0.19.1") (d (list (d (n "ruzstd") (r "^0.3.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.19") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.2") (d #t) (k 0)) (d (n "vaporetto_rules") (r "=0.6.2") (d #t) (k 0)))) (h "1fm7iz9rq34vh6xswslx6830srd2akp1c1gz1244cfk4dhgdw691") (r "1.64")))

(define-public crate-vaporetto_tantivy-0.19.2 (c (n "vaporetto_tantivy") (v "0.19.2") (d (list (d (n "ruzstd") (r "^0.3.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.19") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.3") (d #t) (k 0)) (d (n "vaporetto_rules") (r "=0.6.3") (d #t) (k 0)))) (h "1lslsw6c6vnqbha18jj0c97i5jk303crvzl7adfmvqs12za1ikfa") (r "1.64")))

(define-public crate-vaporetto_tantivy-0.20.0 (c (n "vaporetto_tantivy") (v "0.20.0") (d (list (d (n "ruzstd") (r "^0.4.0") (d #t) (k 2)) (d (n "tantivy") (r "^0.20") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.3") (d #t) (k 0)) (d (n "vaporetto_rules") (r "=0.6.3") (d #t) (k 0)))) (h "199c0s0fjk4k7al4bn44rp3vwdmn9k37ysxd4ngzkza877d4pmww") (r "1.65")))

