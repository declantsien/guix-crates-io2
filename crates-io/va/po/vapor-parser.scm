(define-module (crates-io va po vapor-parser) #:use-module (crates-io))

(define-public crate-vapor-parser-0.1.0 (c (n "vapor-parser") (v "0.1.0") (d (list (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1905l3gxa5p4ha53d4mz8ywlh3w9w89nk2n4i1ccbmjpb00a13hl")))

