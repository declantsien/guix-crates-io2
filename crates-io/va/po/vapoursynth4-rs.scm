(define-module (crates-io va po vapoursynth4-rs) #:use-module (crates-io))

(define-public crate-vapoursynth4-rs-0.1.0 (c (n "vapoursynth4-rs") (v "0.1.0") (d (list (d (n "vapoursynth4-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1j0mvlr0pira51isgy632rvy35hv5j33ifbwjzk1cw572wpdyiir")))

(define-public crate-vapoursynth4-rs-0.2.0 (c (n "vapoursynth4-rs") (v "0.2.0") (d (list (d (n "const-str") (r "^0.5.6") (d #t) (k 2)) (d (n "testresult") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "vapoursynth4-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0qb5qzy3w7zzbqm49p73jhp6zg7wh78ghdjl7jfd4rdwlf1513v9")))

