(define-module (crates-io va po vaporlight) #:use-module (crates-io))

(define-public crate-vaporlight-0.1.0 (c (n "vaporlight") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vv5dn65zbxcabf8vgknbfxzcwlqv1h4a8826slfvldr5cjkxsck")))

