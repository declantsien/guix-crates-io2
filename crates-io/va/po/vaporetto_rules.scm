(define-module (crates-io va po vaporetto_rules) #:use-module (crates-io))

(define-public crate-vaporetto_rules-0.1.3 (c (n "vaporetto_rules") (v "0.1.3") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "vaporetto") (r "^0.1") (d #t) (k 0)))) (h "0ypya3jn9630dp6qnw0m417vsva5ympxszbqm2gcpzqf9jfnpvf8")))

(define-public crate-vaporetto_rules-0.1.4 (c (n "vaporetto_rules") (v "0.1.4") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "vaporetto") (r "^0.2.0") (d #t) (k 0)))) (h "10na7dlz3jh3vkyg4csw540hb1bg2m9sh281szp5b9fas0y6jnhh")))

(define-public crate-vaporetto_rules-0.3.0 (c (n "vaporetto_rules") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "vaporetto") (r "^0.3.0") (d #t) (k 0)))) (h "0dzl11kvnj8zb16gjm6kdgkn3nfs25drl6c7mybzci0v9ignricr")))

(define-public crate-vaporetto_rules-0.4.0 (c (n "vaporetto_rules") (v "0.4.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "vaporetto") (r "^0.4.0") (k 0)))) (h "0n73jcl2kbnfij557i149czi2zjcgy0xffyyv35i0j2nxw6mkw01")))

(define-public crate-vaporetto_rules-0.5.0 (c (n "vaporetto_rules") (v "0.5.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "vaporetto") (r "^0.5.0") (f (quote ("alloc"))) (k 0)) (d (n "vaporetto") (r "^0.5.0") (d #t) (k 2)))) (h "1ldcr4247jgmi1lkma8d2gbhmfzbigin8kmbjq0wwy10va4sm1cz")))

(define-public crate-vaporetto_rules-0.5.1 (c (n "vaporetto_rules") (v "0.5.1") (d (list (d (n "hashbrown") (r "^0.12.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "vaporetto") (r "=0.5.1") (f (quote ("alloc"))) (k 0)) (d (n "vaporetto") (r "=0.5.1") (d #t) (k 2)))) (h "01l2ssjxrj41gh4lv0djqp42wl4l08kh00a6pax52cmn3b8yc3p2") (r "1.58")))

(define-public crate-vaporetto_rules-0.6.0 (c (n "vaporetto_rules") (v "0.6.0") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.0") (f (quote ("alloc"))) (k 0)) (d (n "vaporetto") (r "=0.6.0") (d #t) (k 2)))) (h "1a6rq0kd9d5gc5kd8jqd3kh0jbw5phb4rv75y12nrr0j6ax1yq4l") (r "1.60")))

(define-public crate-vaporetto_rules-0.6.1 (c (n "vaporetto_rules") (v "0.6.1") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.1") (f (quote ("alloc"))) (k 0)) (d (n "vaporetto") (r "=0.6.1") (d #t) (k 2)))) (h "1d5s56a35gx6mayk9inngbwsix32hr85ymlkqvl60wsxzjpv13r1") (r "1.60")))

(define-public crate-vaporetto_rules-0.6.2 (c (n "vaporetto_rules") (v "0.6.2") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.2") (f (quote ("alloc"))) (k 0)) (d (n "vaporetto") (r "=0.6.2") (d #t) (k 2)))) (h "0gm1wbwj9fnjia19wh6zlr030s9ib976n7a21cwsw7k2s8jl7vmm") (r "1.64")))

(define-public crate-vaporetto_rules-0.6.3 (c (n "vaporetto_rules") (v "0.6.3") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "vaporetto") (r "=0.6.3") (f (quote ("alloc"))) (k 0)) (d (n "vaporetto") (r "=0.6.3") (d #t) (k 2)))) (h "1cvy7m57l2xdbxz8mgship7v04mk28090jry9zgkqkq180dr68zf") (r "1.64")))

