(define-module (crates-io va po vapor) #:use-module (crates-io))

(define-public crate-vapor-0.1.0 (c (n "vapor") (v "0.1.0") (d (list (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "15x750gigypn3q1gbajg9picdz4vbdb35ymsqbpryhh3k583pysm")))

(define-public crate-vapor-0.2.0 (c (n "vapor") (v "0.2.0") (d (list (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)))) (h "1bh0xn0kfg978g02g3h7rqbnwhml4kq5vrx0rgl5w8rd0v6fbbgc")))

