(define-module (crates-io va po vapore) #:use-module (crates-io))

(define-public crate-vapore-0.3.0 (c (n "vapore") (v "0.3.0") (d (list (d (n "alpaca-finance") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.25") (d #t) (k 0)))) (h "0j29kzq1dzsjvs3yrjdrvqxk0p75ffknld6wimlx7j1gnf9z754k")))

(define-public crate-vapore-0.3.1 (c (n "vapore") (v "0.3.1") (d (list (d (n "alpaca-finance") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.25") (d #t) (k 0)))) (h "1inryfqkssrci79xqfr14p4hwvhxw802y33ax6z3fpakjvzx685y")))

(define-public crate-vapore-0.3.3 (c (n "vapore") (v "0.3.3") (d (list (d (n "alpaca-finance") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "custom_error") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.25") (d #t) (k 0)))) (h "0j6shgsygnm5qajlzr94xvc0im431vzbiq62v2y3wn7819rzcjkp")))

(define-public crate-vapore-0.4.0 (c (n "vapore") (v "0.4.0") (d (list (d (n "alpaca-finance") (r "^0.2.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.25") (d #t) (k 0)))) (h "0zw3cv8p3dmchxhj047qjla0m25dymrbhw2bg6v9q7aw2l1drcgi")))

(define-public crate-vapore-0.4.1 (c (n "vapore") (v "0.4.1") (d (list (d (n "alpaca-finance") (r "^0.2.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.25") (d #t) (k 0)))) (h "02bqg9h6pq2isbkzacmf1bjkg2pwrfqya026bi422grrrxqc3r87")))

(define-public crate-vapore-0.5.0 (c (n "vapore") (v "0.5.0") (d (list (d (n "alpaca-finance") (r "^0.2.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.25") (d #t) (k 0)) (d (n "yahoo_finance_api") (r "^1.2.2") (d #t) (k 0)))) (h "0qra0p8f76x9sfg2c649zx2bpx89c3k54ax82ni9pjvvhlx7qh41")))

(define-public crate-vapore-0.5.1 (c (n "vapore") (v "0.5.1") (d (list (d (n "alpaca-finance") (r "^0.2.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.25") (d #t) (k 0)) (d (n "yahoo_finance_api") (r "^1.2.2") (d #t) (k 0)))) (h "1bspgxbdsilyhq4hrlk19ng5bqx5mq5w53ywdwsy15xxl6qwsji5")))

