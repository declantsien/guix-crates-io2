(define-module (crates-io va po vapoursynth4-sys) #:use-module (crates-io))

(define-public crate-vapoursynth4-sys-0.1.0 (c (n "vapoursynth4-sys") (v "0.1.0") (h "0b72nq49mkzpk9x97j8mvfmyszbcn89wj0069nxlbrx3sbdmdfgd") (f (quote (("vsscript-41") ("vsscript") ("vs-graph") ("default" "vsscript" "vsscript-41" "vs-graph"))))))

(define-public crate-vapoursynth4-sys-0.2.0 (c (n "vapoursynth4-sys") (v "0.2.0") (h "0n31pwpg4dpmkyn6s9chznxfhfm9y124axdlh8wwn53zzf5xbi0x") (f (quote (("vsscript-41") ("vsscript") ("vs-graph") ("default" "vsscript" "vsscript-41" "vs-graph"))))))

