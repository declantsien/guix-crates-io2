(define-module (crates-io va ti vatican) #:use-module (crates-io))

(define-public crate-vatican-0.0.1 (c (n "vatican") (v "0.0.1") (h "1f2h7mkrq3515fn1vn733cmfk3z6p7xy91bcqjwbay2xgpy1n3w4")))

(define-public crate-vatican-0.0.2 (c (n "vatican") (v "0.0.2") (h "1xpgja2lpqh3clkskcxlslymsim2ghs0gsbs183cjr40325anmyk")))

(define-public crate-vatican-0.0.3 (c (n "vatican") (v "0.0.3") (h "04iggyp6wsnna0mfycl7xlx96nixwvbnziq3zqqjz1ainywiw3pn")))

