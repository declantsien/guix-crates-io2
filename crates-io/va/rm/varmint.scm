(define-module (crates-io va rm varmint) #:use-module (crates-io))

(define-public crate-varmint-0.1.0 (c (n "varmint") (v "0.1.0") (h "0cxj0kwz906kcssbh2n8a1d9awwhi5ivs2p7hdcrzv0lq6wk9h3z") (y #t)))

(define-public crate-varmint-0.1.1 (c (n "varmint") (v "0.1.1") (h "0bksx052m9br7xrwwzhvqqzq9c83nlqqwbbdr0cihziw8gpj7ka3") (y #t)))

(define-public crate-varmint-0.1.2 (c (n "varmint") (v "0.1.2") (h "115fap6jc77vjzmjmss85s0wfx49pxvifqnp1n9ckbc6ixp9f4aj") (y #t)))

(define-public crate-varmint-0.1.3 (c (n "varmint") (v "0.1.3") (h "1ms5vlnx2wm39kyxv8c35mw48nhi159d9bbl4g3z60jlrq24p5ip") (y #t)))

