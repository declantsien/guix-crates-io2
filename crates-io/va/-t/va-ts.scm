(define-module (crates-io va -t va-ts) #:use-module (crates-io))

(define-public crate-va-ts-0.0.1 (c (n "va-ts") (v "0.0.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "url") (r "~1.7") (d #t) (k 0)))) (h "0pz1sblr12a9w1mypnr7h7m2g35jssds744rwgpp8xyk9j4r9rhm")))

(define-public crate-va-ts-0.0.2 (c (n "va-ts") (v "0.0.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "url") (r "~1.7") (d #t) (k 0)))) (h "09ynlw3bcjnsdfz271sq84xl3dlacvsx9fmg97c4g2prhzsiqvy8")))

(define-public crate-va-ts-0.0.3 (c (n "va-ts") (v "0.0.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "url") (r "~1.7") (d #t) (k 2)))) (h "0bmq715bh7d7yn4wmcdd9y9hqgb6drma5i38grs47bq7xvm4rqxi")))

(define-public crate-va-ts-0.0.4 (c (n "va-ts") (v "0.0.4") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "encoding_rs") (r "~0.8") (d #t) (k 0)) (d (n "url") (r "~2.2") (d #t) (k 2)))) (h "02a8z3vma2vibhwgfah7nqawy4gw47lcckanlnja1yi177ynqdpd")))

