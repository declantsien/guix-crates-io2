(define-module (crates-io va lh valheim-asm) #:use-module (crates-io))

(define-public crate-valheim-asm-0.1.0 (c (n "valheim-asm") (v "0.1.0") (d (list (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1k208a3xx3qiyjfkfn0dgc3h5rxdnf0pnbw36y1ijmpv4mgpgjk7")))

(define-public crate-valheim-asm-0.2.0 (c (n "valheim-asm") (v "0.2.0") (d (list (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "0pgi5vvy257aa8f084837hjcxkly6vhvw3cgmmrdjjvmz74vijms")))

