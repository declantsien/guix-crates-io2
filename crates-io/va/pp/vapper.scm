(define-module (crates-io va pp vapper) #:use-module (crates-io))

(define-public crate-vapper-0.1.0 (c (n "vapper") (v "0.1.0") (d (list (d (n "fast_vk") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0l5sxgnsvmq5w1fsmjli3ghklra65vkdnw06g8d61x7d566fvk59")))

