(define-module (crates-io va ne vanessa) #:use-module (crates-io))

(define-public crate-vanessa-0.1.0 (c (n "vanessa") (v "0.1.0") (h "1pf66acig7yxmpwjkvfidya70jr14j2q9mkn01rmhmaawwr56mj8") (f (quote (("workers") ("multilog" "file-log") ("file-log") ("default" "workers" "file-log")))) (y #t)))

(define-public crate-vanessa-0.1.1-alpha (c (n "vanessa") (v "0.1.1-alpha") (h "13ppc4w8nrrc39yy1x1zf2g2ih51s6qi81svkp3b4dklsl2xd5qv") (f (quote (("workers") ("multilog" "file-log") ("file-log") ("default" "workers" "file-log"))))))

