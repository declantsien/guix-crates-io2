(define-module (crates-io va d- vad-sys) #:use-module (crates-io))

(define-public crate-vad-sys-0.1.0 (c (n "vad-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0ygp3n3k6knw0i5ijhs6kg1dnk9a2frh904q2p3lb3vsanldqnc4") (y #t) (l "vad")))

(define-public crate-vad-sys-0.1.1 (c (n "vad-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "170y84jflkir3yrxkfz7bb12s3532bcxwn9vl8z49ydk5rdn3zq0") (y #t) (l "vad")))

