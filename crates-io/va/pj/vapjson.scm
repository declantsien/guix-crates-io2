(define-module (crates-io va pj vapjson) #:use-module (crates-io))

(define-public crate-vapjson-0.1.0 (c (n "vapjson") (v "0.1.0") (d (list (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "07vy5k41nj1rrmivb3nsa2dwdwp5flf74psdcjb1inmy7r0dn5bs") (f (quote (("test-helpers"))))))

