(define-module (crates-io va r_ var_int) #:use-module (crates-io))

(define-public crate-var_int-0.1.0 (c (n "var_int") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1dnnzdy21y3vizcb8jx1zdbdjr5c701s3zljgmp8hhka51xm7vpc")))

(define-public crate-var_int-0.1.1 (c (n "var_int") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "129yzwnbd963qv12pp1qiyqm7nxrl6wxy1ckpm22xzkzs0zkr7i7")))

