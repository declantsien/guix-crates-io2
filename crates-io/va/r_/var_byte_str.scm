(define-module (crates-io va r_ var_byte_str) #:use-module (crates-io))

(define-public crate-var_byte_str-0.1.0 (c (n "var_byte_str") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "bitvec_serde") (r "^0.17") (f (quote ("serde"))) (o #t) (d #t) (k 0) (p "bitvec")) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0iac3ipja88ay0y25xa3amv0ns6iqi1vs5n7mcxw2hjl1jnxiwpc") (f (quote (("serialize" "bitvec_serde" "serde") ("default" "bitvec"))))))

(define-public crate-var_byte_str-0.1.1 (c (n "var_byte_str") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "bitvec_serde") (r "^0.17") (f (quote ("serde"))) (o #t) (d #t) (k 0) (p "bitvec")) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0h8j83l1bg7knvxqmwmg1k5xypxl92gcbc2zpvkydkc55xzqd8pi") (f (quote (("serialize" "bitvec_serde" "serde") ("default" "bitvec")))) (y #t)))

(define-public crate-var_byte_str-0.1.2 (c (n "var_byte_str") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "bitvec_serde") (r "^0.17") (f (quote ("serde"))) (o #t) (d #t) (k 0) (p "bitvec")) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "12qd8s31l6g69rxrv6wawywcw78axx4rcggh1sjqdngf4x5gsw67") (f (quote (("serialize" "bitvec_serde" "serde") ("default" "bitvec"))))))

