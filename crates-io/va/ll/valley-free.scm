(define-module (crates-io va ll valley-free) #:use-module (crates-io))

(define-public crate-valley-free-0.1.0 (c (n "valley-free") (v "0.1.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 2)))) (h "1w86l7r4d1gic0kgwpdpdx6xpifj8i5d939vhciaiv4a5k1yrn9s")))

(define-public crate-valley-free-0.1.1 (c (n "valley-free") (v "0.1.1") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 2)))) (h "0knh4h6jx01a9b32hqbnik8xc5cbkhigq86kwn79s00r67m8y63n")))

(define-public crate-valley-free-0.2.0 (c (n "valley-free") (v "0.2.0") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (d #t) (k 0)))) (h "12nd297ii3kw2nlb6b9xik5481jscxq53s3hd76vslsp2pgz9g9l") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

