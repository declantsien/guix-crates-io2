(define-module (crates-io va sp vasputils) #:use-module (crates-io))

(define-public crate-vasputils-0.1.0 (c (n "vasputils") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "09r55kgyjfmwymi4yjqf19nscgkkhjkpz5igmj6d2pb0br8qhg6v") (y #t)))

(define-public crate-vasputils-0.0.2 (c (n "vasputils") (v "0.0.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1pzm9ni1jcbc0plgjh2kkpvrhhjjz4gi9q4qivwcg8s59516hc19")))

(define-public crate-vasputils-0.0.3 (c (n "vasputils") (v "0.0.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0snr36jbm81m4xw0jy70fr9v71y7pj5ifaw6j05yxxblly9spp0z")))

