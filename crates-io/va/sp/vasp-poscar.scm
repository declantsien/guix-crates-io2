(define-module (crates-io va sp vasp-poscar) #:use-module (crates-io))

(define-public crate-vasp-poscar-0.1.0 (c (n "vasp-poscar") (v "0.1.0") (d (list (d (n "dtoa") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "left-pad") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "1q5pdajxdqcqzdham8hmckngy33hr2l7pc2vg0ipy6g1paazlfw3")))

(define-public crate-vasp-poscar-0.1.1 (c (n "vasp-poscar") (v "0.1.1") (d (list (d (n "dtoa") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "left-pad") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "1i1jwbvhxlf1lls2xkp4ch5kd8g3yc703dsqfb4lfwqxsfrmmapl")))

(define-public crate-vasp-poscar-0.2.0 (c (n "vasp-poscar") (v "0.2.0") (d (list (d (n "dtoa") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "0grcxkh1diy5k5z2gl2n6rxr4xd2caddajnh8z86av1w62w9lphx")))

(define-public crate-vasp-poscar-0.3.0 (c (n "vasp-poscar") (v "0.3.0") (d (list (d (n "dtoa") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0j9l1l6n7w7nwlkzsbj824ff622asjacg1p9j5c9bjkjb2qfjyib")))

(define-public crate-vasp-poscar-0.3.1 (c (n "vasp-poscar") (v "0.3.1") (d (list (d (n "dtoa") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "032pa57ig04xq8zwg0yj1rliw59ar9qkmqk1p3vgrx1mkrldfn2l")))

(define-public crate-vasp-poscar-0.3.2 (c (n "vasp-poscar") (v "0.3.2") (d (list (d (n "dtoa") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "050hiddlvrkli633f50zwyf96fp0gjrxglhj7fhad6c81ghgj5v7")))

