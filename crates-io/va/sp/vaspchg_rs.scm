(define-module (crates-io va sp vaspchg_rs) #:use-module (crates-io))

(define-public crate-vaspchg_rs-0.1.0 (c (n "vaspchg_rs") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "vasp-poscar") (r "^0.3.2") (d #t) (k 0)))) (h "17ymfsrh2l652fbiz6dagq2bm52i2jy52x6y1cbr3h1nz2w2qb7g")))

(define-public crate-vaspchg_rs-0.1.1 (c (n "vaspchg_rs") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "vasp-poscar") (r "^0.3.2") (d #t) (k 0)))) (h "1ckj2khw231dj9q9lb0hkq5jwb7blqws1k7s4z3j81xkc5i3jms3")))

