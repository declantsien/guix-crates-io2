(define-module (crates-io va rn varnish) #:use-module (crates-io))

(define-public crate-varnish-0.0.1 (c (n "varnish") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1xbqb151kjrdixd3axxz4l3gmm272ylkqg56q44k30v0hi56g21i")))

(define-public crate-varnish-0.0.2 (c (n "varnish") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1zx1jyjk8cdkyrlrh6z3dfk6x973nybaf3anrcjssff0ay3apvfd")))

(define-public crate-varnish-0.0.3 (c (n "varnish") (v "0.0.3") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.3") (d #t) (k 0)))) (h "1502z7ccbi35d5bv7b54q5pmyz9z92xpfgappg0qx6xk9c8xvdar")))

(define-public crate-varnish-0.0.4 (c (n "varnish") (v "0.0.4") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.4") (d #t) (k 0)))) (h "06m4yad9r58gq855arqq2d3m7xcxvya6qzcpafw184crzs0dbvyq")))

(define-public crate-varnish-0.0.5 (c (n "varnish") (v "0.0.5") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.5") (d #t) (k 0)))) (h "077ckkwkkqv8flrk9hx5g0v618ajgsysfdirbg20kky0yjingzzy")))

(define-public crate-varnish-0.0.6 (c (n "varnish") (v "0.0.6") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.6") (d #t) (k 0)))) (h "1bccj8fi8bw6cj9g931yg41b95pjzjps0zq3vsqb66grnj62pa4k")))

(define-public crate-varnish-0.0.7 (c (n "varnish") (v "0.0.7") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.7") (d #t) (k 0)))) (h "0qdidxqhs6wr19fbblj285464ifcq4h6m42g7ij15mg29fxaz0x5")))

(define-public crate-varnish-0.0.8 (c (n "varnish") (v "0.0.8") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0ijsb8g2x747s3blagyznv3lkhgl22rrk57r6xa68i2d82bq17ih")))

(define-public crate-varnish-0.0.9 (c (n "varnish") (v "0.0.9") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.9") (d #t) (k 0)))) (h "0p26jb13l403709xgzjpyhgjnfnh2d88c8v000gycminpajwf3dh")))

(define-public crate-varnish-0.0.10 (c (n "varnish") (v "0.0.10") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.10") (d #t) (k 0)))) (h "1k1jf7ykkq4ifvp671vhw37l49mwri4kmrgdpd4idsbrmxwp3741")))

(define-public crate-varnish-0.0.11 (c (n "varnish") (v "0.0.11") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.11") (d #t) (k 0)))) (h "0g6n1mx4452xq4i8pakclzf31bsvwkp1cl98wl1ikayf4pi7a2fp")))

(define-public crate-varnish-0.0.12 (c (n "varnish") (v "0.0.12") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.12") (d #t) (k 0)))) (h "0xgc9zp9x224qca63kx3ry017pzwzvbx741gms9bp10ccvlwmzr0")))

(define-public crate-varnish-0.0.13 (c (n "varnish") (v "0.0.13") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.13") (d #t) (k 0)))) (h "150g5j5zpvr893khqc3r24qp719388aqzcyxiqzp5j4ly3ghcnad")))

(define-public crate-varnish-0.0.14 (c (n "varnish") (v "0.0.14") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.14") (d #t) (k 0)))) (h "0yg3gjbsg2rczqqimiyfyc3msa7ivka6fpl5kwnq22xgx2yqcnp6")))

(define-public crate-varnish-0.0.15 (c (n "varnish") (v "0.0.15") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.15") (d #t) (k 0)))) (h "1c046z5gqhhflbmijr04wmgx8wj04z8i00rkj7szyivzppax22h1")))

(define-public crate-varnish-0.0.16 (c (n "varnish") (v "0.0.16") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.16") (d #t) (k 0)))) (h "0axwm8610r518sj4kvy29p88dza0b5wwc5hijl15kkrgayh2yblm")))

(define-public crate-varnish-0.0.17 (c (n "varnish") (v "0.0.17") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.17") (d #t) (k 0)))) (h "1byxd0snz0az4xh7zp1xh5b5bx44ddjpwhpwq6j4g9kyrh3r33jp")))

(define-public crate-varnish-0.0.18 (c (n "varnish") (v "0.0.18") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.18") (d #t) (k 0)))) (h "0r3p4prazzmxyh2hfl7fd4xvqjlwj6cmk2bffwy2yj1m5ag5vj4p")))

(define-public crate-varnish-0.0.19 (c (n "varnish") (v "0.0.19") (d (list (d (n "pkg-config") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "varnish-sys") (r "^0.0.19") (d #t) (k 0)))) (h "0fd1zphq1qnq6564c1nhv28h4lp5h5z68898i51nm10r5m2zzplq")))

