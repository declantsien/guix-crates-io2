(define-module (crates-io va rn varnamer) #:use-module (crates-io))

(define-public crate-varnamer-0.1.0 (c (n "varnamer") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "1nkw5m45irz4jrag307k46gdz67xaz3gjjsg9ba7dgkik57gv9n1")))

