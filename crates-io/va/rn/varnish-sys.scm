(define-module (crates-io va rn varnish-sys) #:use-module (crates-io))

(define-public crate-varnish-sys-0.0.1 (c (n "varnish-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1kzadd33phm7a873xqvaigmfg3x0jfh07pg7zf60ra1mpy9l3d9r")))

(define-public crate-varnish-sys-0.0.2 (c (n "varnish-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0wlh1yyjqwzgpz59vkcvxi22x04992ic8ljwd7c9gp5wnmcji3yg")))

(define-public crate-varnish-sys-0.0.3 (c (n "varnish-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1bz9w4w76y1rgk59l3w4k47nqszgq33vl5pyg5zk4ynd874c9bif")))

(define-public crate-varnish-sys-0.0.4 (c (n "varnish-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0c0ql81gi0hyd4nd95pi96zsq265rlj6fjrd0mp8pj0j9pkkq70d")))

(define-public crate-varnish-sys-0.0.5 (c (n "varnish-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1wp748n9c2k9zjfrd0acalvhblll5l8yb46fcdal71jsm3mvg5p8")))

(define-public crate-varnish-sys-0.0.6 (c (n "varnish-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1mg49hnayjd996rz3gr7ipnjm1j7d36ija4phwydsjy539q1yfzn")))

(define-public crate-varnish-sys-0.0.7 (c (n "varnish-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0jky0lngym2sdnjh4gzapa5s4r2n9clf6m52gsh3bq4c06g3xlix")))

(define-public crate-varnish-sys-0.0.8 (c (n "varnish-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0w8wq7yy8grbds9kyvmnqkkrpn139dphsgfpwbv5kv903kif6wb8")))

(define-public crate-varnish-sys-0.0.9 (c (n "varnish-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "11s0cpspxrrida08xgnbgl0rq3sl3l8jk2rf2ia2agmw35rl3bzj")))

(define-public crate-varnish-sys-0.0.10 (c (n "varnish-sys") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "08v49m6iqbypk0jma38myx22p3a57a7xmkz2mcibqn6nadax45qp")))

(define-public crate-varnish-sys-0.0.11 (c (n "varnish-sys") (v "0.0.11") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1yqy92axdj6swjinpdaqy7fs2qfjz97m6yw2i5ajncnc8d16dmfc")))

(define-public crate-varnish-sys-0.0.12 (c (n "varnish-sys") (v "0.0.12") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "16nlis1fhf1b7vxfklhg2hxvfh34gs3j2m0kppv4n9rmvbs637dv")))

(define-public crate-varnish-sys-0.0.13 (c (n "varnish-sys") (v "0.0.13") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "1w36wpj8j3ds7cpzyfq9l3hs9hj1qqq6xlbngx0lplsxzqpjsmnj")))

(define-public crate-varnish-sys-0.0.14 (c (n "varnish-sys") (v "0.0.14") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "03gamdwgz2xsq4rk1xlpcmbz425f0mza28xbspnvvb8zkqdj3hhj")))

(define-public crate-varnish-sys-0.0.15 (c (n "varnish-sys") (v "0.0.15") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "08ya4z356q415rkycvwp77cays374m75pikqzg6sd7hcq7vzyccg")))

(define-public crate-varnish-sys-0.0.16 (c (n "varnish-sys") (v "0.0.16") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "06d4c7xd5jg4gwyds1038pdw7jjzjwz4xqchjajg1dr3vb49mi8r")))

(define-public crate-varnish-sys-0.0.17 (c (n "varnish-sys") (v "0.0.17") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "02wljii3m9pdxr90dm4cgg4qypdqfn70xh0x6njfdn7599ci44py")))

(define-public crate-varnish-sys-0.0.18 (c (n "varnish-sys") (v "0.0.18") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0srss2rzcidg7fj3965j4hcvgwpmh4ciclrvxnxlqs09cs0qx840")))

(define-public crate-varnish-sys-0.0.19 (c (n "varnish-sys") (v "0.0.19") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.22") (d #t) (k 1)))) (h "0in60s4wz4rm4r253nv7q9dmf48zarx0r65m2vv0qss87jd7f6pj")))

