(define-module (crates-io nb ts nbtscanner) #:use-module (crates-io))

(define-public crate-nbtscanner-0.0.1 (c (n "nbtscanner") (v "0.0.1") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^2.29.1") (d #t) (k 0)))) (h "0yihywwn5ay7w8d5kff52483g8z1fw7zknmngy28r11yvpcfmd26")))

