(define-module (crates-io nb d- nbd-netlink) #:use-module (crates-io))

(define-public crate-nbd-netlink-0.1.0 (c (n "nbd-netlink") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "07fs52402zsncpr1vl18rj7myn8ipclrdk1bg2ljlpj4ic06xc4m")))

(define-public crate-nbd-netlink-0.1.1 (c (n "nbd-netlink") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "neli") (r "^0.4.4") (d #t) (k 0)))) (h "0ajlnqxcscj3w3k7ip0yl9z602yap6yfx8h5b5hc0f4i2h1lcp2h")))

(define-public crate-nbd-netlink-0.2.0 (c (n "nbd-netlink") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "neli") (r "^0.5.2") (d #t) (k 0)))) (h "0gd9xq88k2gpl45iypl9cgcybhxnqkkywmqiay4vkmk846cx4l7s")))

(define-public crate-nbd-netlink-0.3.0 (c (n "nbd-netlink") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "neli") (r "^0.6.1") (d #t) (k 0)) (d (n "neli-proc-macros") (r "^0.1.2") (d #t) (k 0)))) (h "05g77g6nnh19lfrn0fnn68kv5s2nycasxyx3ccgg9d7aadss2rp1")))

