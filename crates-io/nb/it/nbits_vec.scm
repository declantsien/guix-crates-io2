(define-module (crates-io nb it nbits_vec) #:use-module (crates-io))

(define-public crate-nbits_vec-0.1.0 (c (n "nbits_vec") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.33") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "typenum") (r "^1.1") (d #t) (k 0)))) (h "11ag4c5g7kxg93d9gzamxij9q2mbmqx1n12m27h7zwgs6v6yb1mx") (f (quote (("nightly" "clippy") ("default"))))))

(define-public crate-nbits_vec-0.1.1 (c (n "nbits_vec") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.33") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "typenum") (r "^1.1") (d #t) (k 0)))) (h "10hb60v0r5xh2ia5c422v5zpcvkrzkjwhvymdw769zn7l2gdflrr") (f (quote (("nightly" "clippy") ("default"))))))

