(define-module (crates-io nb in nbindgen) #:use-module (crates-io))

(define-public crate-nbindgen-0.0.1 (c (n "nbindgen") (v "0.0.1") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("clone-impls" "extra-traits" "full" "parsing" "printing"))) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1sxbj491pi097sysz0kliawr3k2klnkdix9m42ha4pmwgraiw03v") (f (quote (("default" "clap"))))))

