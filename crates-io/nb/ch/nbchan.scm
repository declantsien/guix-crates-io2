(define-module (crates-io nb ch nbchan) #:use-module (crates-io))

(define-public crate-nbchan-0.1.0 (c (n "nbchan") (v "0.1.0") (h "1bi3d88g9zhz16hab56kx6hlsbi0ayl76y834w788h0wpv54gkav")))

(define-public crate-nbchan-0.1.1 (c (n "nbchan") (v "0.1.1") (h "11ll2phd1753h557chnca168fkc63saj0vhjpbfcfpq41p65y15d")))

(define-public crate-nbchan-0.1.2 (c (n "nbchan") (v "0.1.2") (h "14wbs2zhk1ld5g7hqn2m0ipqq7mzzbficd7sksyzhryz629kmmbn")))

(define-public crate-nbchan-0.1.3 (c (n "nbchan") (v "0.1.3") (h "0q4pgwm8vvz857zc331dmgg8lf2avjrq4ix7y9lir52fvcgddcbj")))

