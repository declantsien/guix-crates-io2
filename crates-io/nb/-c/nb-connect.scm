(define-module (crates-io nb -c nb-connect) #:use-module (crates-io))

(define-public crate-nb-connect-1.0.0 (c (n "nb-connect") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(unix)") (k 0)) (d (n "polling") (r "^1.0.2") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "ws2tcpip"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jmb03058fd6ryjqg2fc0kdlpfrgnp80dvvi42f54i0g75mwfiz8") (y #t)))

(define-public crate-nb-connect-1.0.1 (c (n "nb-connect") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(unix)") (k 0)) (d (n "polling") (r "^1.0.2") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "ws2tcpip"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wkg359i3b1sfiv7zzg4yahyycvwcqpjqzm6zskx0rl4p6p4f7vh") (y #t)))

(define-public crate-nb-connect-1.0.2 (c (n "nb-connect") (v "1.0.2") (d (list (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(unix)") (k 0)) (d (n "polling") (r "^2.0.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "ws2tcpip"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1649m71wc0cg1rqgl8vbh0489znkhpwgl0isjd5x8mz470ash8w1") (y #t)))

(define-public crate-nb-connect-1.0.3 (c (n "nb-connect") (v "1.0.3") (d (list (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(unix)") (k 0)) (d (n "polling") (r "^2.0.0") (d #t) (k 2)) (d (n "socket2") (r "^0.3.19") (f (quote ("unix"))) (d #t) (k 0)))) (h "0kyzppd0b3pyg0cnwnfsn0xxv1r5886nlh2hzzhrwff23ggn20v7")))

(define-public crate-nb-connect-1.0.4 (c (n "nb-connect") (v "1.0.4") (d (list (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(unix)") (k 0)) (d (n "polling") (r "^2.0.0") (d #t) (k 2)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)))) (h "0fgzwinbdwcij9xj7rv29krdsam11kpi8nwb7p7dw281lc1s35jx") (y #t)))

(define-public crate-nb-connect-1.1.0 (c (n "nb-connect") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(unix)") (k 0)) (d (n "polling") (r "^2.0.0") (d #t) (k 2)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)))) (h "0pd4kwvasgklpvyfc6z3a3vsm06cfjl14vg2qarv4pp9xvkh16d1")))

(define-public crate-nb-connect-1.2.0 (c (n "nb-connect") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.77") (d #t) (t "cfg(unix)") (k 0)) (d (n "polling") (r "^2.0.0") (d #t) (k 2)) (d (n "socket2") (r "^0.4.0") (f (quote ("all"))) (d #t) (k 0)))) (h "05fadcyxaz0r8pbh498y24xay3b2gb73ih3fj7hwylggqq6m9fxi")))

