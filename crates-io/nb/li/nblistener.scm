(define-module (crates-io nb li nblistener) #:use-module (crates-io))

(define-public crate-nblistener-0.1.0 (c (n "nblistener") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1ahwgpcr13xhhzfzdfbbksl6s5n7720ysh2xqjc6mlb0w15sswwv")))

(define-public crate-nblistener-0.1.1 (c (n "nblistener") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pnxszz3432jq7nhj6cjw8p5ymbx5l46rrsgjxdsbi9wkp6q483n")))

