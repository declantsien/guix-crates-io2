(define-module (crates-io nb -t nb-to-query-derive) #:use-module (crates-io))

(define-public crate-nb-to-query-derive-0.1.0 (c (n "nb-to-query-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "13vdz0nc1llq812yv236apcblgl7ncrkxdgw2ldnhi8yga3rabjv")))

(define-public crate-nb-to-query-derive-0.1.1 (c (n "nb-to-query-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0ha1k351rm4af194ybbil4ws87xw1vxhk6pp6h1y1lqmqldz0gli")))

