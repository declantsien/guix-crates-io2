(define-module (crates-io nb -t nb-to-query) #:use-module (crates-io))

(define-public crate-nb-to-query-0.1.0 (c (n "nb-to-query") (v "0.1.0") (d (list (d (n "nb-to-query-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0gn2gjzrbm7kn1k04kh89z9ffz0iklnrn8ynlgl07k5q9nglmsk0")))

(define-public crate-nb-to-query-0.1.1 (c (n "nb-to-query") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nb-to-query-derive") (r "^0.1.1") (d #t) (k 0)))) (h "03x46a10fy11aja8xm5yn28r5v4hq9h85pyqnlcz4nh4axws0l9s") (f (quote (("chrono"))))))

