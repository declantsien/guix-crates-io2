(define-module (crates-io nb te nbted) #:use-module (crates-io))

(define-public crate-nbted-1.4.0 (c (n "nbted") (v "1.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0164k90k5dl7km7zvl5j0j9ma09m1cn97yvx5d1dnpda38j622s8")))

(define-public crate-nbted-1.4.1 (c (n "nbted") (v "1.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1d6qlvh67jm6dxa6yqsxsihcw2dwsazr9l1386zy3vbdp047vikj")))

(define-public crate-nbted-1.4.2 (c (n "nbted") (v "1.4.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "15syzz9031ccx63ikyl6hcb62mr1r7kw2avhaism3ywpliiff18k")))

(define-public crate-nbted-1.5.0 (c (n "nbted") (v "1.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0yslfmqj22id1fcffcd2xm3ybdd9qrxda6id8v8m14k3j1zxwnc6")))

