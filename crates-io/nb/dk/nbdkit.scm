(define-module (crates-io nb dk nbdkit) #:use-module (crates-io))

(define-public crate-nbdkit-0.1.0 (c (n "nbdkit") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "errno") (r "^0.2.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "mockall") (r "^0.8.2") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "046dlw7l65f80lw92kl4095373pks2amj5xn3diw836a8xz8z8jn") (f (quote (("nightly-docs"))))))

(define-public crate-nbdkit-0.2.0 (c (n "nbdkit") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "mockall") (r "^0.8.2") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "0xn96di96hf4q7kgrynmvddqpidfwsd7lr1pc9n49jg161ish50p")))

(define-public crate-nbdkit-0.3.0 (c (n "nbdkit") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "memoffset") (r "^0.6.3") (d #t) (k 2)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)) (d (n "nix") (r "^0.24.1") (f (quote ("net" "socket"))) (o #t) (k 0)))) (h "1dg630p3661abc4qqpy21h7aml4v9l3nq9wbmqvjm2rnj1bhk6id")))

