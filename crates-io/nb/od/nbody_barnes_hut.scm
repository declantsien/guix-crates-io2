(define-module (crates-io nb od nbody_barnes_hut) #:use-module (crates-io))

(define-public crate-nbody_barnes_hut-0.1.0 (c (n "nbody_barnes_hut") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1s403pywpg1nl2sg5v3bicka3csbknp48b05sbkxaz66p88vzgpm")))

