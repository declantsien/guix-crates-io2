(define-module (crates-io nb p- nbp-api) #:use-module (crates-io))

(define-public crate-nbp-api-0.1.0 (c (n "nbp-api") (v "0.1.0") (d (list (d (n "iso_currency") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1i8s6hhs7kjh00qbkp0rmjj6ij7yiyh9yn35dcvipnkirz991q5y")))

