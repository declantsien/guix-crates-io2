(define-module (crates-io nb #{3_}# nb3_import) #:use-module (crates-io))

(define-public crate-nb3_import-1.1.1 (c (n "nb3_import") (v "1.1.1") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "progress") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-log") (r "^1.5.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5.13") (f (quote ("mysql" "runtime-tokio-rustls" "macros" "chrono"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fxx5a0fwsdp54w95shacdsy58zykisp5937aakg1lfwh73hsdyd")))

(define-public crate-nb3_import-1.1.2 (c (n "nb3_import") (v "1.1.2") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "progress") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-log") (r "^1.5.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.5.13") (f (quote ("mysql" "runtime-tokio-rustls" "macros" "chrono"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c14zl7gdwl4qq4mq2vnp6b7nxbpldq1v75k4mjnp0h66zmya2d7")))

