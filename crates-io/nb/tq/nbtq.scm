(define-module (crates-io nb tq nbtq) #:use-module (crates-io))

(define-public crate-nbtq-0.1.0 (c (n "nbtq") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (k 0)) (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0nzssj4hwfbz9rnykl57frw6v8i9n1vbx3z4alcf9v8hwviin307")))

