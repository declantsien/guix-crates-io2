(define-module (crates-io nb ib nbib) #:use-module (crates-io))

(define-public crate-nbib-0.1.0 (c (n "nbib") (v "0.1.0") (h "0d1s86rv8fqp9sivfhl82xdz79p60dsah2q577k2s1rlwsbjlr4y")))

(define-public crate-nbib-0.1.1 (c (n "nbib") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0wlza66s821s1skvc2fflwvfpw0w8hf4q9ymw2fs68m6fv7pr8g6")))

