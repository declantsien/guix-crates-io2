(define-module (crates-io nb -e nb-executor) #:use-module (crates-io))

(define-public crate-nb-executor-0.1.0 (c (n "nb-executor") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "05ma741i3dy5ah7lr22hf90dmm1ryyjwbrp012jgg6bdxg0gy3il") (y #t)))

(define-public crate-nb-executor-0.1.1 (c (n "nb-executor") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "13kl2zak8z18vzjipf12n04s1xqwwwqipphghspi8rzw9wm0ih94")))

(define-public crate-nb-executor-0.1.2 (c (n "nb-executor") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "19klciq9v2q9j7yy654hspsrxnv79692966bl6fph9lml3kav4hg")))

(define-public crate-nb-executor-0.2.0 (c (n "nb-executor") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1xki0y7dk1jdld7i7dvc3fp1yhbvw7sldn7gh7rzvnargippgjj3")))

(define-public crate-nb-executor-0.2.1 (c (n "nb-executor") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0y0kgpmjyfp4abrhz2l2ba9mb013gdjb68igcrq35m8805jncmv0")))

(define-public crate-nb-executor-0.3.0 (c (n "nb-executor") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (f (quote ("mpmc_large"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1gbvxw3k02wzwr47czrq5m7hh9afdq6bi4r6v45wsyz4xdrn9dnm")))

(define-public crate-nb-executor-0.3.1 (c (n "nb-executor") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (f (quote ("mpmc_large"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1yhkap78b3izg8di0qan6sf7hm2k65az3zcdvghq8zwc38rkrn4l")))

(define-public crate-nb-executor-0.3.2 (c (n "nb-executor") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (f (quote ("mpmc_large"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1fivhlmzgzjqpakyw2104m4cf67j2483n9cmr1xab0pi1n073d5v")))

(define-public crate-nb-executor-0.4.0 (c (n "nb-executor") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (f (quote ("mpmc_large"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "039s20fbaa8rr64ifh78ac19amqfsf78nvd68hwrsic197pj4dhh")))

(define-public crate-nb-executor-0.4.1 (c (n "nb-executor") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (f (quote ("mpmc_large"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "171d14m9ni428wjzhjlyigl8wzlpp6l50hkmgl8bsybn7xrb729r")))

(define-public crate-nb-executor-0.4.2 (c (n "nb-executor") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "heapless") (r "^0.7.10") (f (quote ("mpmc_large"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0jfcrgm8cmp0nhb5czga2rnq6bmr7b2vp1bxzjjiq83zsxd6ghlp")))

