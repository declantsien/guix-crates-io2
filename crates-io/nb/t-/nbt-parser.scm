(define-module (crates-io nb t- nbt-parser) #:use-module (crates-io))

(define-public crate-nbt-parser-1.0.0 (c (n "nbt-parser") (v "1.0.0") (d (list (d (n "combine") (r "^3.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libflate") (r "^0.1.15") (d #t) (k 0)))) (h "0ka61bswwl7kpzi7806nfgcj0fpy4m1gqhss28jhp2ik3ydqkjy6")))

