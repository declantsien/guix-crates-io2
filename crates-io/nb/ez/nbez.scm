(define-module (crates-io nb ez nbez) #:use-module (crates-io))

(define-public crate-nbez-0.1.0 (c (n "nbez") (v "0.1.0") (d (list (d (n "gfx") (r "^0.11.0") (d #t) (k 2)) (d (n "gfx_window_glutin") (r "^0.11.0") (d #t) (k 2)) (d (n "glutin") (r "^0.5.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "191s108axzb1arn10rhidwk6vfplnqjpl6azlwqbrfmz9gsb5imp")))

