(define-module (crates-io nb ez nbezier) #:use-module (crates-io))

(define-public crate-nbezier-0.2.1 (c (n "nbezier") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)))) (h "1v0zddww69ajknb8z7nxqwn386648mw74lpgjzhgxphz19m4ac59") (f (quote (("draw-svg" "draw") ("draw") ("default"))))))

