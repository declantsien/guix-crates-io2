(define-module (crates-io nb #{2n}# nb2nl) #:use-module (crates-io))

(define-public crate-nb2nl-0.1.0 (c (n "nb2nl") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0i8vyq8wnirmz5d4y0xa35pd8qqd6ligzn3kqa1hk5skpnl5d8iq")))

(define-public crate-nb2nl-0.2.0 (c (n "nb2nl") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "ordslice") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1yq3vc2kzvzwzazrvnm1az1g29pnb2h69czn9ssyqvgagfi0751s")))

