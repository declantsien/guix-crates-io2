(define-module (crates-io nb -s nb-serde-query) #:use-module (crates-io))

(define-public crate-nb-serde-query-0.1.0 (c (n "nb-serde-query") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1y1y60vxzmgbvj2kjmh5c2y3mfn26kqr30f6800cgbj2zhd3gzyh")))

(define-public crate-nb-serde-query-0.2.0 (c (n "nb-serde-query") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "13vpdl279dhk7j6rdcmh52saigcl0d2ac5qn5bfl93zrj3zp4fly")))

(define-public crate-nb-serde-query-0.2.1 (c (n "nb-serde-query") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1925vacpvss8br699zsxqa70dz5bgcc5k5anlps9nz41b466zfcx")))

(define-public crate-nb-serde-query-0.3.0 (c (n "nb-serde-query") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0m0gd7qmcg6x4jpdqiv1rzpbrdk2zxymdf01vjyp1yynwnxi42aa")))

(define-public crate-nb-serde-query-0.3.1 (c (n "nb-serde-query") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1lk23f47617nksf1nlywhvv4i26l4lpdn5zl3p5dvpf6yyladybi")))

(define-public crate-nb-serde-query-0.3.2 (c (n "nb-serde-query") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "00aqzvw1lf6drhxxbc7gc206vgls7aqdrakgphbicyg4zndzlqxf")))

(define-public crate-nb-serde-query-0.3.3 (c (n "nb-serde-query") (v "0.3.3") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0qjv9sp9d299wk6clr8i7q22ba3jwjy6n25yh56rdg463fjbskzn") (f (quote (("actix-web"))))))

