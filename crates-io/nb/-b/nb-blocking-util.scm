(define-module (crates-io nb -b nb-blocking-util) #:use-module (crates-io))

(define-public crate-nb-blocking-util-0.10.0 (c (n "nb-blocking-util") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qswh0qlzrnycr7bb7mqlkwx8844icw1ddwg72z8f67wl464vnz1")))

(define-public crate-nb-blocking-util-0.10.1 (c (n "nb-blocking-util") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "125n59j2bdc0yjsa6yc9l0r6b0m1az1v7wk34inpkn215h40iqh6")))

