(define-module (crates-io nb ss nbssh) #:use-module (crates-io))

(define-public crate-nbssh-0.1.0 (c (n "nbssh") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "1camqn0sr5sv7ld1yb4yckqr0fjfzn0428y7spd6jvhkmcn8hxpp")))

(define-public crate-nbssh-0.2.0 (c (n "nbssh") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "020vaihlyxn9bisr8r9dcbsy8jblnpml2vhhnyka3yq0d4y3dbcm")))

(define-public crate-nbssh-0.3.0 (c (n "nbssh") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "1w0pbkrkfczydhai9vm4i52vhsv2vkxryq9f4kvibiy96x81pv2c")))

(define-public crate-nbssh-0.4.0 (c (n "nbssh") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "0s9xjbb5l9hbvfb2cpsqkd769acl9mv9pgmb20brbf1cqgf1jgmh")))

(define-public crate-nbssh-0.5.0 (c (n "nbssh") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "0665dxlxaqivhm0qz50kfyrhwcliv9g63dds7yhkb1l6wm5dgc5l")))

(define-public crate-nbssh-0.6.0 (c (n "nbssh") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "1vrb9xsa5ymn780899zw0hmrdf875v0yngngm3v9xix91j2f9v1l")))

(define-public crate-nbssh-0.7.0 (c (n "nbssh") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "0jdr2729qsf0mhsdrmikbmdr71jrg3v39a400zqyymk8zcwwxsr5")))

(define-public crate-nbssh-0.8.0 (c (n "nbssh") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "0k68xi964b61xsmxmmbshlajd6i12jxx617mam63lshgbsb8il0l")))

(define-public crate-nbssh-1.0.0 (c (n "nbssh") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.1") (d #t) (k 0)))) (h "10badsb8dhqd70z3bg71f300asxj7qkn1mdyap4d1wkb9wg0lkjw")))

(define-public crate-nbssh-2.0.0 (c (n "nbssh") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10nkrh6a8isb373cxagknnww4gxah7c0fc4d57a45bg9varad9k2")))

(define-public crate-nbssh-2.0.1 (c (n "nbssh") (v "2.0.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hr36jj7y8gvqf77i788rvibqxpzm7vqa458n0safz4mw8fj4mkj")))

(define-public crate-nbssh-3.0.0 (c (n "nbssh") (v "3.0.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0375jvsbvs5yhkyqqs95j4v0ljgkad5jwbwbj5b0vlljl5cgzjmr")))

(define-public crate-nbssh-3.0.2 (c (n "nbssh") (v "3.0.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0za3kkhw1kjkimsd5z3lyk3hahj7g39625f0k99ajjsj4rrw736a")))

