(define-module (crates-io nb ds nbdserve) #:use-module (crates-io))

(define-public crate-nbdserve-0.1.0 (c (n "nbdserve") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "nbd") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (k 0)))) (h "0wmih34128bk5fyga6ka0j1vq29v1im5syb15p3vdy00y3w5km2s")))

(define-public crate-nbdserve-0.1.1 (c (n "nbdserve") (v "0.1.1") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "nbd") (r "^0.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (k 0)))) (h "0k6dqdvd384sa39cr5c4nv5jn0v7ms1k5lsi97lk11acvqwdliw8")))

