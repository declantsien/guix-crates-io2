(define-module (crates-io nb lo nblock) #:use-module (crates-io))

(define-public crate-nblock-0.1.0 (c (n "nblock") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "spinning_top") (r "^0.3.0") (d #t) (k 0)))) (h "0157cpv658k0vmpsicf033s0pjr8a68wg7d4jsbd75i1zs08gi1m")))

(define-public crate-nblock-0.1.1 (c (n "nblock") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "spinning_top") (r "^0.3.0") (d #t) (k 0)))) (h "1qfmv0s5xf4xqp3k6bfi05k8dqp2jcjliqaf693yyc2xw0yrxk28")))

