(define-module (crates-io nb -f nb-from-env) #:use-module (crates-io))

(define-public crate-nb-from-env-0.1.0 (c (n "nb-from-env") (v "0.1.0") (d (list (d (n "from-env-derive") (r "^0.1") (d #t) (k 0)))) (h "14kap4jkhljs9wpi18kqrflpgzq3r88az1kimkdirn6rb7sb97mx")))

(define-public crate-nb-from-env-0.1.1 (c (n "nb-from-env") (v "0.1.1") (d (list (d (n "from-env-derive") (r "^0.1") (d #t) (k 0)))) (h "11qjs19ychkgklw0f89an5hd5v9w0f71lj9kjrlrzbd96paqpnjy")))

(define-public crate-nb-from-env-0.2.0 (c (n "nb-from-env") (v "0.2.0") (d (list (d (n "from-env-derive") (r "^0.2") (d #t) (k 0)))) (h "0mpmf32bay08n0ylibk1cslrmfmyb3iyn00ixv8n966fv2mp8f6v")))

(define-public crate-nb-from-env-0.2.1 (c (n "nb-from-env") (v "0.2.1") (d (list (d (n "from-env-derive") (r "^0.2") (d #t) (k 0)))) (h "1qlbgl1v8rgqaryqwmf7vw6zmz706pcbsq8qz5c88a3j5xq2fhlq")))

