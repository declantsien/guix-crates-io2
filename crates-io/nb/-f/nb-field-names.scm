(define-module (crates-io nb -f nb-field-names) #:use-module (crates-io))

(define-public crate-nb-field-names-0.1.0 (c (n "nb-field-names") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1rabhrm793bryvgxkgk6vcs2zj4ihn0lphqzr7y6zs51h20f90xv")))

