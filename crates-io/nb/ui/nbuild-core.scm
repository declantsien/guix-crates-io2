(define-module (crates-io nb ui nbuild-core) #:use-module (crates-io))

(define-public crate-nbuild-core-0.1.0 (c (n "nbuild-core") (v "0.1.0") (d (list (d (n "cargo-lock") (r "^9.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "target-spec") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0sir2rsvb17n5f8zxm9hzpdvslsg2zmjijxichh5227lrc9qij9a")))

(define-public crate-nbuild-core-0.1.1 (c (n "nbuild-core") (v "0.1.1") (d (list (d (n "cargo-lock") (r "^9.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "target-spec") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1xawwxnihawra63bxy5zaipvpidcjrp9d697jmpg48kg7l99wvv6")))

(define-public crate-nbuild-core-0.1.2 (c (n "nbuild-core") (v "0.1.2") (d (list (d (n "cargo-lock") (r "^9.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "target-spec") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "03jakbv93v0aw1wszwm2z1dv3lqxqyq5nmbxmqqm02j5lfqb2njy")))

