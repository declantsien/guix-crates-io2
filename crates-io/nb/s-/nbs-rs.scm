(define-module (crates-io nb s- nbs-rs) #:use-module (crates-io))

(define-public crate-nbs-rs-0.1.0 (c (n "nbs-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "131zi3al7wgr4fyr9gzvws59vwxcbifdnzl7kgs1zih0kwzqwsak")))

(define-public crate-nbs-rs-0.1.1 (c (n "nbs-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "19p0n2xzmdym9s78bl9r99yxx637blrqrf12lkv21s0dbrin4mrr")))

