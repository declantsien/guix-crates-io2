(define-module (crates-io ej ec eject) #:use-module (crates-io))

(define-public crate-eject-0.1.0 (c (n "eject") (v "0.1.0") (d (list (d (n "nix") (r "^0.25.0") (f (quote ("ioctl"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_Security" "Win32_System_IO" "Win32_System_Ioctl"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xa7p4qkmxiw9qf2m3p7r3ld506qc0qb0k8p6lib7lmdfy456nz8")))

(define-public crate-eject-0.1.1 (c (n "eject") (v "0.1.1") (d (list (d (n "memoffset") (r "^0.6.5") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (f (quote ("ioctl"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_Security" "Win32_System_IO" "Win32_System_Ioctl" "Win32_Storage_IscsiDisc"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rw8mr05bxfagkg53aczhw8m27sawclnlbav9g5mijb5fcl1v2gh")))

