(define-module (crates-io ej di ejdict_rs_core) #:use-module (crates-io))

(define-public crate-ejdict_rs_core-0.0.1 (c (n "ejdict_rs_core") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zaksabzv2390g4yf26vlh93l7hc86wjfh1brxzwid4vh81wh6lh")))

(define-public crate-ejdict_rs_core-0.0.2 (c (n "ejdict_rs_core") (v "0.0.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jg5iwa81iajada9g5jjwhi08irfvjzrqdkfwjp9mck18m9wp0ps")))

(define-public crate-ejdict_rs_core-0.0.3 (c (n "ejdict_rs_core") (v "0.0.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1234mdpqzixllg7rrmiavb31v22yddfs3j5z0cn3f161f3828z6a")))

