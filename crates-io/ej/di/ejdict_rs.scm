(define-module (crates-io ej di ejdict_rs) #:use-module (crates-io))

(define-public crate-ejdict_rs-0.0.1 (c (n "ejdict_rs") (v "0.0.1") (d (list (d (n "ejdict_rs_core") (r "^0.0.1") (d #t) (k 0)) (d (n "ejdict_rs_core") (r "^0.0.1") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "1kd5r2nxsz7bjpdc06hj2m746r6shdqlhymlnz9i8d14x5wdpwfz") (y #t)))

(define-public crate-ejdict_rs-0.0.2 (c (n "ejdict_rs") (v "0.0.2") (d (list (d (n "ejdict_rs_core") (r "^0.0.2") (d #t) (k 0)) (d (n "ejdict_rs_core") (r "^0.0.2") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "029lvbmk4qwqddmywza590cxp6fzkd7ri993yiylqc5aqmwpan1j") (y #t)))

(define-public crate-ejdict_rs-0.0.3 (c (n "ejdict_rs") (v "0.0.3") (d (list (d (n "ejdict_rs_core") (r "^0.0.2") (d #t) (k 0)) (d (n "ejdict_rs_core") (r "^0.0.2") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "1bcnn37k0w073rdk2mly4l0cy4k40lrfyfaqfarl7qn63icjqczy")))

(define-public crate-ejdict_rs-0.0.4 (c (n "ejdict_rs") (v "0.0.4") (d (list (d (n "ejdict_rs_core") (r "^0.0.3") (d #t) (k 0)) (d (n "ejdict_rs_core") (r "^0.0.3") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "0sc4f4gyq2zlxkvl3fqv2hyga7l4nn8dr13dcyn8qw4j741i8502")))

