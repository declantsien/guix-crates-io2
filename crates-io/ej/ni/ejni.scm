(define-module (crates-io ej ni ejni) #:use-module (crates-io))

(define-public crate-ejni-0.0.1 (c (n "ejni") (v "0.0.1") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0hq23mi5gnjbl5x31cb2kqymc5v0bs5lyaywngxzlpxdpp02qlcq")))

(define-public crate-ejni-0.1.0 (c (n "ejni") (v "0.1.0") (d (list (d (n "jni") (r "^0.19") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (f (quote ("invocation"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jaxnnhk5vhdwfplnxbrbrz45k9f6b4af6ga4gwrxfzsmdf8q5vi")))

