(define-module (crates-io ej db ejdb-sys) #:use-module (crates-io))

(define-public crate-ejdb-sys-0.1.0 (c (n "ejdb-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qyb7fxjpy9ghxsmqp9fw8ix56sxpk96gzncdr15zl0w0268b46z")))

(define-public crate-ejdb-sys-0.2.0 (c (n "ejdb-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gb0mwnrcpdrz0lm3d78f4515hp2b50arwzxycqwvgdak8a5rl9r")))

(define-public crate-ejdb-sys-0.3.0 (c (n "ejdb-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0giy6cz7qrh8vjp0wgjnjrhgq5syg5x1wax1cwa4wffvzk8djg6n") (l "ejdb")))

