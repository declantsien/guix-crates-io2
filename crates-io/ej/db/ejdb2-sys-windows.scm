(define-module (crates-io ej db ejdb2-sys-windows) #:use-module (crates-io))

(define-public crate-ejdb2-sys-windows-2.7.2 (c (n "ejdb2-sys-windows") (v "2.7.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06qb5c3nqxsgppkvzjng3f6qcs2xwv09hfffp0cq9nvdjvkai3hn") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.0 (c (n "ejdb2-sys-windows") (v "2.61.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wi68x0cjrjax88nhbshvjlqczzsb9hq9fbyjwqpax6srb0c5hdh") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.1 (c (n "ejdb2-sys-windows") (v "2.61.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1p41hqk08vi8hj7x9nsv7zr1jajd12fm418a3bmnqazgxa8cgap8") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.2 (c (n "ejdb2-sys-windows") (v "2.61.2") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04rccr65s9yjfhli34d0cy6p8smcl328bgm1qjbra1in3dxfhdzn") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.3 (c (n "ejdb2-sys-windows") (v "2.61.3") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1v0d8kyhl0j35bc5zj0f8f88fc7ak9xxayj3yl3x639b9ywc58b6") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.5 (c (n "ejdb2-sys-windows") (v "2.61.5") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1npkl3c0pmdzl0y711mjpqwhdlabbi3smdqvkv28q731ajb5ash7") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.6 (c (n "ejdb2-sys-windows") (v "2.61.6") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1g0wa46i02nygd56a3s6ykkawr57365h05gjx62b9dzn73pz1j3y") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.7 (c (n "ejdb2-sys-windows") (v "2.61.7") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lflqsxd08jysz608rdg40a7ip8s4vz11vn2rcd3kzfcyd1dzymd") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.8 (c (n "ejdb2-sys-windows") (v "2.61.8") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1blanbjf2hg31f3f69vb1pz4w8idd5q2v84x0dqjndqll6brjdla") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.9 (c (n "ejdb2-sys-windows") (v "2.61.9") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0bn0w5j23zr4h73cpmkxx8nncli9an03ppg3syjwrrhh0q04y8lm") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.10 (c (n "ejdb2-sys-windows") (v "2.61.10") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mg06dxd16g6lp3sci4kx5jbcz4baay4wc7bkq9sbmcz770i45rp") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.11 (c (n "ejdb2-sys-windows") (v "2.61.11") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15jkk0clfqjmxigg9bxf3l8d63ydyfgchx5lz5hbv9nrvilkddy6") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.12 (c (n "ejdb2-sys-windows") (v "2.61.12") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03y737i54ys65xwm5lxv0da5q0y7wsjq3lhx5r5rzvfmfczk4rfq") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.13 (c (n "ejdb2-sys-windows") (v "2.61.13") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02q8aaqwd7ir84bby1k83y4c9rm31q5hl7hmlxhqwcfgfl5xbx09") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.14 (c (n "ejdb2-sys-windows") (v "2.61.14") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13ggzs7wpriqg29ijdwkqwa42pvzhj1xbxpin0pgfya26ndg4a09") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.15 (c (n "ejdb2-sys-windows") (v "2.61.15") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m3n0lxxha9xzn9cx441w1qxmwwrjfndr8476rj9nz6j57rjcscs") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.16 (c (n "ejdb2-sys-windows") (v "2.61.16") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1h278yhmia0hzl884ii1akc72hy52ay949f9sncvsg6pl2myjrz0") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.17 (c (n "ejdb2-sys-windows") (v "2.61.17") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "012jpzfgs2hhiffv8hb8wmi2dv46lg517ir87dn39b6lgn784rc9") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.18 (c (n "ejdb2-sys-windows") (v "2.61.18") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yg23jzmvg0i71vqsyi21wbg0zfz8m9draxr17fydzljmminpl6i") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.19 (c (n "ejdb2-sys-windows") (v "2.61.19") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mpz6f43mhgx1hhmvyyy99wvg9mvl892zw9lijv0455m3p4a6gkf") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.20 (c (n "ejdb2-sys-windows") (v "2.61.20") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0z68nhg996ak5kqwvl5c5m5a1fcq0r9i5hv4f3q2gxz29lqgpvgv") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.21 (c (n "ejdb2-sys-windows") (v "2.61.21") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ap8jl769r08djbkqxc88sh29ac4n0h8r248l6hxzsrckmfq90fj") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.22 (c (n "ejdb2-sys-windows") (v "2.61.22") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "084yqa6iwavq5siwy2rwp7pi6h7ibdp2il6k55ybkj2nk4n6hfvy") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.23 (c (n "ejdb2-sys-windows") (v "2.61.23") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0199lyv0v9557rd4rfpgfy0fqirvj8ybxcwvpf36kfjfv6bjd66w") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.24 (c (n "ejdb2-sys-windows") (v "2.61.24") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hy9rybivwzk5gcc0f9w4bcgfz76192vxmwk3cq25cqnavn236zv") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.25 (c (n "ejdb2-sys-windows") (v "2.61.25") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "14m6vh3nxjz5b012ddimrvrfyqcz27c4wh0gf8ndn20j40ykys25") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.26 (c (n "ejdb2-sys-windows") (v "2.61.26") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1v4k27rbnsq16sgzghvcf7fddm69pb15w3xx0f29kw37scmnfkr3") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.27 (c (n "ejdb2-sys-windows") (v "2.61.27") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "172c9axzmkfg13hszn2y82n0xiddc6j7hampqpjh598iall73vfp") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.28 (c (n "ejdb2-sys-windows") (v "2.61.28") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i5kvxaxr72gpc6r294s2wwkdl49kzsfz57n83bsf7bdmj21rh32") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.29 (c (n "ejdb2-sys-windows") (v "2.61.29") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1b23l0crxcj7ldv0847ib3q1kszzz0lw3kfbi68f9dnv0cbwlrla") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.30 (c (n "ejdb2-sys-windows") (v "2.61.30") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1finx8fnmxdz0jja6vn0q9lbq4scl8xzf4mnixnjccfff3lf9iyf") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.31 (c (n "ejdb2-sys-windows") (v "2.61.31") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kwxcxnnxdzbv9gaf0fn89df3m9nd06f4qq28y93s3i21yk9xx2z") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.33 (c (n "ejdb2-sys-windows") (v "2.61.33") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "077vbkcsl8p5spx3vbjfwici121a3s73ix1ag7lcc4jqdsmr7c57") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.34 (c (n "ejdb2-sys-windows") (v "2.61.34") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hwavcxqxi3dcfm5m67ic112vvgp8305xxqr2aw5hbgz0a7l6nq8") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.35 (c (n "ejdb2-sys-windows") (v "2.61.35") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0m6rw8gixxmlc68pcy7ljcb3vxp563vg5vwx91sbvs2z88kv19ns") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.36 (c (n "ejdb2-sys-windows") (v "2.61.36") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pp1jsmzxgg291989n72i7v4gxjj3z996q7wr0bpsf4kwsbmqc9y") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.37 (c (n "ejdb2-sys-windows") (v "2.61.37") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rvyv8yk5v468vms3majnkgk31miy8vfjd45lm95kahxylbwvrfc") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.38 (c (n "ejdb2-sys-windows") (v "2.61.38") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06i5ig3mfspbk2zkh6a5yxbvjwzlsznvc3x3ysa9w3pczdrqhrg7") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.39 (c (n "ejdb2-sys-windows") (v "2.61.39") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ccmasab60q25akfp4jg8israr2n7qdx1nag3agqx4cz6r6izbin") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.40 (c (n "ejdb2-sys-windows") (v "2.61.40") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1x4qpp7hlr7j2h8rfv47p8bh6n5b342kqzz4mmzxmnhbpnr87xbl") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.41 (c (n "ejdb2-sys-windows") (v "2.61.41") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1iijhc8wg8skcmvi4zx60z454vj28d9yfwrbz2kbhjhj8c1vfid5") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.42 (c (n "ejdb2-sys-windows") (v "2.61.42") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0br060rzgm8awwzirxw14npkxk1y055r7w8ky8b7kh4h7fappl9l") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.43 (c (n "ejdb2-sys-windows") (v "2.61.43") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qkf919sk35fn8jdpvphwl7x6g5gi78x9qx82q65an05c2dkyqh6") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.44 (c (n "ejdb2-sys-windows") (v "2.61.44") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1g2gh8v8m0iqq63zr6nf8dbmiy7j7mgk9p908bf9zkpvni0y4dzw") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.45 (c (n "ejdb2-sys-windows") (v "2.61.45") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05vyhsb4z9accs979fb6xnmdp84pzm86s8z3dkm96lqk6viz4r3r") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.46 (c (n "ejdb2-sys-windows") (v "2.61.46") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1k2p8n1phdn9y9ky7cgn4pr8f50rsw58g0vlv20d592vqzb977xr") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.47 (c (n "ejdb2-sys-windows") (v "2.61.47") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1piha2j7gny4zl897n4bln6slfp131ccvjcdj379rk6wqhyjcri1") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.48 (c (n "ejdb2-sys-windows") (v "2.61.48") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0b6g9bp7svddb9lv3jl29ajlrib1xj62c6499yw06y192996w9s1") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.49 (c (n "ejdb2-sys-windows") (v "2.61.49") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "001x0sh0dqk62119xyhgmxw5mvbknmx9p0yh3xd5bw6zcrn4g472") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.50 (c (n "ejdb2-sys-windows") (v "2.61.50") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "196w3249pfj6gbm0l4in8hm7hgmkvsjpghqwmq65s5gaf8nyi2yq") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.51 (c (n "ejdb2-sys-windows") (v "2.61.51") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0812wrqd1qkwvpfrc3jjj1dcam5h69s3r4k9nm1hq3fqlyyjciq8") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.52 (c (n "ejdb2-sys-windows") (v "2.61.52") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gqjm6iwm58xmcr5hz6j4zh6d0qwlfrrjnqa1ryb66dh3nilzvmy") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.53 (c (n "ejdb2-sys-windows") (v "2.61.53") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m91r17whlj474sczy67v3w040frg63sw4kdq3fadk11m7rp88w2") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.55 (c (n "ejdb2-sys-windows") (v "2.61.55") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i6kn3z35m1g5d1hrm96vjfkjslmbj04fcw275giy0q6nd7ww7pi") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.56 (c (n "ejdb2-sys-windows") (v "2.61.56") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kamwvhswlfcfw1pgkxcd4jqha4r99wwjmq13jih7dy025xawnvr") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.57 (c (n "ejdb2-sys-windows") (v "2.61.57") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "055a7wij29hwa7d17dgdim4rq72x2qlgq08lmgrdqfdgsfr9avjj") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.58 (c (n "ejdb2-sys-windows") (v "2.61.58") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1d7ndxzs3ypycwf433d1lq4pa0dkaj0ni43v6z12cb0xw5w6r222") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.59 (c (n "ejdb2-sys-windows") (v "2.61.59") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "052nwsg78z4kaw16mq14bjvj1n5jrcx0cy8p4qxxn9yw35zzbvpq") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.60 (c (n "ejdb2-sys-windows") (v "2.61.60") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "16mpn3y122pns596200gkmkypqiv5xqrb38sgad514n2lyafbs1x") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.61 (c (n "ejdb2-sys-windows") (v "2.61.61") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pcvl4a62rlgyx3jr6jdyf445ljakjnnxx4jv261fd6iygl8nrqy") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.62 (c (n "ejdb2-sys-windows") (v "2.61.62") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1j5m11raqm3zcp3cgdkx00sin3l23r3njnmv41dar0d4ibhqwapk") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.63 (c (n "ejdb2-sys-windows") (v "2.61.63") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19c138qxccdr149wpnqsgbp30ygpglsdwkipvhvz9x3d96a49djr") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.64 (c (n "ejdb2-sys-windows") (v "2.61.64") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0a3l3qnqjr0xbqz2m6f37n4kjc9w32nypqf7kl165slasb3a4245") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.65 (c (n "ejdb2-sys-windows") (v "2.61.65") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0414zvkb6pbqkggr4wlabyndlrp8dflapf51yhwdbn66c2ignijh") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.66 (c (n "ejdb2-sys-windows") (v "2.61.66") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s5pc5qan2nmldzrwnnss9bf4m35fjampmslbk1f99fv3jp8r3dj") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.67 (c (n "ejdb2-sys-windows") (v "2.61.67") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0l21m12729pf1ca19qbakfh32h20zpp2y2qgw4lcmxaqxxgzrisd") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.68 (c (n "ejdb2-sys-windows") (v "2.61.68") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17q31q7xc8dcpsjpwn6xmin5g96s7w732059iz1p6rzszsnk4rgj") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.69 (c (n "ejdb2-sys-windows") (v "2.61.69") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "070b5q0pc1pdnl76i8pbl55a0ngmvc17qbim5yzam9na9iyqd9ns") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.70 (c (n "ejdb2-sys-windows") (v "2.61.70") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lj803q3dwyxrfp8hygglx5i1mmxqdsxw6dafmx1qfqwavnh41qn") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.71 (c (n "ejdb2-sys-windows") (v "2.61.71") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0653qm74qp5ixmpkb7yl2yi46rwj42qi4y1vxiw7i6chh3wmhc00") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.72 (c (n "ejdb2-sys-windows") (v "2.61.72") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1q8mn5003lp7x0i5g32bybhp4ic7day0c4l5bz812dk73ssz7v26") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.74 (c (n "ejdb2-sys-windows") (v "2.61.74") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1v9sydy1w6l59x30ncsw2qrqbvb9lb347j6xd5b5pjk270v9xakn") (l "ejdb2")))

(define-public crate-ejdb2-sys-windows-2.61.75 (c (n "ejdb2-sys-windows") (v "2.61.75") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ryy37nqlalyxqg05mx9psmc8kciqx7kl9xkql7z9r3c58l7xwbw") (l "ejdb2")))

