(define-module (crates-io ej db ejdb) #:use-module (crates-io))

(define-public crate-ejdb-0.1.0 (c (n "ejdb") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "bson") (r "^0.1") (d #t) (k 0)) (d (n "ejdb-sys") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0rz2hw4i4q15caj3v2wyvj5kp9ih2fk32x54wyj5313bxpnk45zd")))

(define-public crate-ejdb-0.1.1 (c (n "ejdb") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "bson") (r "^0.1") (d #t) (k 0)) (d (n "ejdb-sys") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "00i6098bkqz0s3sh2342g3vmgfacsag0vv7fiqi8a8750mps9z1v")))

(define-public crate-ejdb-0.1.2 (c (n "ejdb") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "bson") (r "^0.1") (d #t) (k 0)) (d (n "ejdb-sys") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0j9yf0b7fh3qv27115k22pzas9s3r7b9ayw3i0dzzw54sfkl6df3")))

(define-public crate-ejdb-0.2.0 (c (n "ejdb") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "bson") (r "^0.2") (d #t) (k 0)) (d (n "ejdb-sys") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1n9m77sk2krpapa0p9w2amgnvmz579zxpvdyh57gmf4f36drn2ma")))

(define-public crate-ejdb-0.3.0 (c (n "ejdb") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "bson") (r "^0.3") (d #t) (k 0)) (d (n "ejdb-sys") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1rs7i7fjqqsmv4mlg27wwlpjiyfi8nrj2yn149hzfmckmrq59xmc")))

(define-public crate-ejdb-0.4.0 (c (n "ejdb") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bson") (r "^0.13") (d #t) (k 0)) (d (n "ejdb-sys") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1i7bvlnp6mhs8mn2pwmb8df15gpbarxladcqhpqq9k4xxgfmna36")))

(define-public crate-ejdb-0.4.1 (c (n "ejdb") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bson") (r "^0.13") (d #t) (k 0)) (d (n "ejdb-sys") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "10g9zpwfhg10igch6iahj4g1l471yq00vbkqylw03glf1ld76zim")))

