(define-module (crates-io ej db ejdb2-sys) #:use-module (crates-io))

(define-public crate-ejdb2-sys-2.61.0 (c (n "ejdb2-sys") (v "2.61.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gz3p8p0pq0zrbcj9g14crd0gq83irl6dgivxlyywwfs6020f03f") (l "ejdb2")))

