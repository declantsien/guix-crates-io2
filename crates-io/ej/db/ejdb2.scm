(define-module (crates-io ej db ejdb2) #:use-module (crates-io))

(define-public crate-ejdb2-0.0.1 (c (n "ejdb2") (v "0.0.1") (d (list (d (n "ejdb2-sys") (r "^2.61.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "02h1s1ssl554556jjvxqw3ndz0z3bfqgpfg098v7xanqayx23xcn")))

(define-public crate-ejdb2-0.0.2 (c (n "ejdb2") (v "0.0.2") (d (list (d (n "ejdb2-sys") (r "^2.61.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16z3jqkmk4l3b79wj6snkjlrrwcy520c874pq8avn37212rn2hgv")))

(define-public crate-ejdb2-0.0.3 (c (n "ejdb2") (v "0.0.3") (d (list (d (n "ejdb2-sys") (r "^2.61.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1dy2l5dwb2756zj20ibmmlgb0w9hgdyxjvh1rkaa2lja6v2dcaba")))

(define-public crate-ejdb2-0.0.4 (c (n "ejdb2") (v "0.0.4") (d (list (d (n "ejdb2-sys") (r "^2.61.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0lp1dqw7wchcpj4gcvhzq70j9sx98w26fbnhnqz02gw5lmsjpq0x")))

(define-public crate-ejdb2-0.0.5 (c (n "ejdb2") (v "0.0.5") (d (list (d (n "ejdb2-sys") (r "^2.61.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ysyf6y486pzpryivdl4c268pxk0k0x7hwyzgq6in54xssvjdn9s")))

(define-public crate-ejdb2-0.0.6 (c (n "ejdb2") (v "0.0.6") (d (list (d (n "ejdb2-sys") (r "^2.61.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zqlq7glkma0bcizk858ll4d435wk0yv3nfsdd0ksdh1lr1gkcg2")))

