(define-module (crates-io dn d_ dnd_spellbook_maker) #:use-module (crates-io))

(define-public crate-dnd_spellbook_maker-0.1.0 (c (n "dnd_spellbook_maker") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "printpdf") (r "^0.6.0") (f (quote ("embedded_images"))) (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "07j9iqnixr2xy2n3d13cypz425nxvff67libc9i20cdp88wpd4pl")))

