(define-module (crates-io dn d_ dnd_dice_roller) #:use-module (crates-io))

(define-public crate-dnd_dice_roller-0.1.0 (c (n "dnd_dice_roller") (v "0.1.0") (d (list (d (n "dice-command-parser") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ig6wnw9air1gyja8ili6cswnw2a2yrkmai2vnjpwijhrjprh2r7")))

(define-public crate-dnd_dice_roller-0.2.0 (c (n "dnd_dice_roller") (v "0.2.0") (d (list (d (n "dice-command-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08s28qn4wpsg6jqgf5rd1n22rzqwfk73ls1sqacafxk9slx9nxw4")))

(define-public crate-dnd_dice_roller-0.2.1 (c (n "dnd_dice_roller") (v "0.2.1") (d (list (d (n "dice-command-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i7aa35hrcf8q3xa1w6yjnpjq61nzxlzkyyl7lwgx3h0969lw6q1")))

(define-public crate-dnd_dice_roller-0.3.0 (c (n "dnd_dice_roller") (v "0.3.0") (d (list (d (n "dice-command-parser") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z12pcys5bpfmyj704fg1vlwy98x4jnlwxxsb9540irh86k0q8hz")))

(define-public crate-dnd_dice_roller-0.4.0 (c (n "dnd_dice_roller") (v "0.4.0") (d (list (d (n "dice-command-parser") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j3vm93b3d8p5bdbyp5m6987327xlcsa89iwfrjd5szfj509kvrv")))

(define-public crate-dnd_dice_roller-0.5.0 (c (n "dnd_dice_roller") (v "0.5.0") (d (list (d (n "dice-command-parser") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bn8dky5cjh7f440yf8cn9wysr0kv0iilj4zdn8gvv76m7hgbwwk")))

(define-public crate-dnd_dice_roller-0.5.1 (c (n "dnd_dice_roller") (v "0.5.1") (d (list (d (n "dice-command-parser") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19kygmg7n8v9fi3admm3y4ngmyzf1vxb3d1clvbn4lavs5f786q8")))

