(define-module (crates-io dn d_ dnd_wiki_markdown) #:use-module (crates-io))

(define-public crate-dnd_wiki_markdown-0.1.11 (c (n "dnd_wiki_markdown") (v "0.1.11") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "shoulda") (r "^0.1.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0lw4rh04l807bszgppm5fvr903jnkdmfc695rasg39j5pgxsml73")))

