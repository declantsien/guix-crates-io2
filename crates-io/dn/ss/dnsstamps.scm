(define-module (crates-io dn ss dnsstamps) #:use-module (crates-io))

(define-public crate-dnsstamps-0.1.0 (c (n "dnsstamps") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "1i50z5a2dgfybn7vi3b50nr9x5z9007yaxjcvl3jxx003lc8lkdy")))

(define-public crate-dnsstamps-0.1.1 (c (n "dnsstamps") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "0zjw1j203abzkrr6xx2nngl9cm5y3dc4m913mim949j15lksjv11")))

(define-public crate-dnsstamps-0.1.2 (c (n "dnsstamps") (v "0.1.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "01klhpdz1gzsd3hy883q8ym7a39yin5qaf7jmr062vl39mgbk78j")))

(define-public crate-dnsstamps-0.1.3 (c (n "dnsstamps") (v "0.1.3") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "05vs7c77713ah9l43i0xwx2s6x6maqw71ars21hlgyizv77xb2ri")))

(define-public crate-dnsstamps-0.1.4 (c (n "dnsstamps") (v "0.1.4") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "0b2ncj2pbwx6cdfy4mi2bzgclwfpr8ahrkxr8a600wvbsih5wmkj")))

(define-public crate-dnsstamps-0.1.5 (c (n "dnsstamps") (v "0.1.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)))) (h "1rcg7mwvkngaa11p8v2xpcqflg8vl92il8zv7ajhxrkpky9ijv9s")))

(define-public crate-dnsstamps-0.1.6 (c (n "dnsstamps") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "ct-codecs") (r "^1.0.0") (d #t) (k 0)))) (h "0ac4isbm5rzylvkj08m5ha9ck2rz2hsl0r7pyb0422gzg6adw2gk")))

(define-public crate-dnsstamps-0.1.7 (c (n "dnsstamps") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1.1") (d #t) (k 0)))) (h "1cf4g7c9zsvsgxxaq1kwlldpx6dgd2k8vnic98j8fb83xnxi4zbz")))

(define-public crate-dnsstamps-0.1.8 (c (n "dnsstamps") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1.1") (d #t) (k 0)))) (h "1lir26rlfv2hi84kxgqjgcw480parygmk2gfyfch0xg2hhalvjww")))

(define-public crate-dnsstamps-0.1.9 (c (n "dnsstamps") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1.1") (d #t) (k 0)))) (h "1pn34890w3vi8q0jw65yxkl4a8p4ncza17glsxhnzi2sc05cmsi5")))

