(define-module (crates-io dn dx dndx-forked-unix-udp-sock) #:use-module (crates-io))

(define-public crate-dndx-forked-unix-udp-sock-0.6.1 (c (n "dndx-forked-unix-udp-sock") (v "0.6.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec" "net"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0lblm13qc4ry9sjp4ar9ggy230x1y3w5l76nvxkwb1kp51613055")))

