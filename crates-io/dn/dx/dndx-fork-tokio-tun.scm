(define-module (crates-io dn dx dndx-fork-tokio-tun) #:use-module (crates-io))

(define-public crate-dndx-fork-tokio-tun-0.3.16 (c (n "dndx-fork-tokio-tun") (v "0.3.16") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "0j2fqpk65azp3mqz7s849mmwq6plsy0jfg5m6sgz1pi7z190jwkk")))

(define-public crate-dndx-fork-tokio-tun-0.4.0 (c (n "dndx-fork-tokio-tun") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1g9vb7vs7w287h9m0d03zygpcdxk76k48m9391zvv5s5x0vw3140")))

(define-public crate-dndx-fork-tokio-tun-0.5.1 (c (n "dndx-fork-tokio-tun") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 2)))) (h "1q7hxdzbpj9i54rw8z9kgmhnrh2g55fr3js6x8605xgjprsj3zcl")))

