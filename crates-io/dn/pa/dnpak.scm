(define-module (crates-io dn pa dnpak) #:use-module (crates-io))

(define-public crate-dnpak-0.1.0 (c (n "dnpak") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.22") (f (quote ("zlib-ng-compat"))) (k 0)))) (h "009l5l83k6h6h76qx5ahpi316nizn4ikca4m424gj8wwchb3fr65")))

(define-public crate-dnpak-1.1.0 (c (n "dnpak") (v "1.1.0") (d (list (d (n "flate2") (r "^1.0.22") (f (quote ("zlib-ng-compat"))) (k 0)))) (h "0b5r6fscq5gbf2axqnyamxp370d9y1i83xnh3y8cdvv8fxh6ahmy")))

(define-public crate-dnpak-1.2.0 (c (n "dnpak") (v "1.2.0") (d (list (d (n "flate2") (r "^1.0.22") (f (quote ("zlib-ng-compat"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "14girhp8kvw3hhy47gn5cd35mjw6cbpn5xwmk6ff303w8vfyj4vq")))

(define-public crate-dnpak-1.3.0 (c (n "dnpak") (v "1.3.0") (d (list (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0hgq5b46r6jyxnqsy1s9b7cv1r7yjpbynqpf634ipw931jk6i07x")))

(define-public crate-dnpak-1.3.1 (c (n "dnpak") (v "1.3.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "01fd1i91z7gqqmk584cy2fq9yzlykkf84gmdv8gync0cqdng3dgc")))

(define-public crate-dnpak-1.3.2 (c (n "dnpak") (v "1.3.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0pqfg72nzvlc6rbcvp6g7lsd53bzzfyd6fb0cd9q5gjiwl9lwarr")))

(define-public crate-dnpak-1.3.3 (c (n "dnpak") (v "1.3.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1mx2wqnjn0s7ysk9mnc1qs1lmpbqagh7h9fzbr3fal4ayfc75v7z")))

(define-public crate-dnpak-1.3.4 (c (n "dnpak") (v "1.3.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "19sfa77k308x6i5p7kygj0rkhr906cvxyv9z55p3hc60cslplzgw")))

(define-public crate-dnpak-2.0.0 (c (n "dnpak") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0m695a7wh5gscg42qcz10y6whg51mpck1rdf1gk80dzbw2d7a3fh")))

(define-public crate-dnpak-2.0.1 (c (n "dnpak") (v "2.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1j12h8sl8m121gcyvspnzcd9fwhcckfnmvhqnipsn4lzqs1wsl5d")))

(define-public crate-dnpak-2.1.1 (c (n "dnpak") (v "2.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1qfbrwghq264nds2kkmilxgsx5m4sxl4jy9ln1s745j291kmpzcz")))

(define-public crate-dnpak-2.1.2 (c (n "dnpak") (v "2.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (f (quote ("zlib"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1jb1pxy0zazm7mm0b4iv53g2078glclwnxkv7sih8xrafwx13vx9")))

