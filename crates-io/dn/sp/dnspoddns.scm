(define-module (crates-io dn sp dnspoddns) #:use-module (crates-io))

(define-public crate-dnspoddns-0.1.2 (c (n "dnspoddns") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "string" "env"))) (d #t) (k 0)) (d (n "dnspod-lib") (r "^0.1.9") (f (quote ("clap"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)))) (h "1dzyj5dj4zpnzd8hqkgsvyj280zqzpvpnyym8gkccaj9yra4vqzj") (y #t)))

