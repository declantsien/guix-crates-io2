(define-module (crates-io dn sp dnsparse) #:use-module (crates-io))

(define-public crate-dnsparse-0.1.0 (c (n "dnsparse") (v "0.1.0") (h "1lmlw8afhffiqd4hlv887wl1bzdfbidp8aq4bvg2m1jq0fvphr0p")))

(define-public crate-dnsparse-0.1.1 (c (n "dnsparse") (v "0.1.1") (h "14rbv1rpnjnypfqk7npgzrg4gq8c6286kaqdf5alc59kasr1m12b")))

(define-public crate-dnsparse-0.2.0 (c (n "dnsparse") (v "0.2.0") (h "1cld6qz9fdnrnfarfml66kfk5wz9i54vh807p20mxkcdddwl8i8j") (f (quote (("std"))))))

(define-public crate-dnsparse-0.3.0 (c (n "dnsparse") (v "0.3.0") (h "15ssbhzpvkva44xf5wp616hbrkpb4vwnvf8yd34jmrpa9cpqkd8q") (f (quote (("std"))))))

