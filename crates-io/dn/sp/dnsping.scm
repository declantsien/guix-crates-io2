(define-module (crates-io dn sp dnsping) #:use-module (crates-io))

(define-public crate-dnsping-0.1.0 (c (n "dnsping") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "socks") (r "^0.3.2") (d #t) (k 0)))) (h "1fgwvkpjirci3kq3286fsgc981a8l819hhbpfcdmyc6clmyp88bw")))

(define-public crate-dnsping-0.1.1 (c (n "dnsping") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "socks") (r "^0.3.2") (d #t) (k 0)))) (h "0glinn56mmn13m9acxy85cchy5800rpsy04abcz1bcqspswvbg2c")))

(define-public crate-dnsping-0.2.0 (c (n "dnsping") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.4") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.3") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "socks") (r "^0.3.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "11d8zsk81fd1klm5b8pbwasx1vlpyp9fbvbylyph93il3nvv9s49")))

