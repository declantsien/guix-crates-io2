(define-module (crates-io dn sp dnspod-cli) #:use-module (crates-io))

(define-public crate-dnspod-cli-0.1.0 (c (n "dnspod-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "string" "env"))) (d #t) (k 0)) (d (n "dnspod-lib") (r "^0.1.2") (f (quote ("clap"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "113gjrm7q6d92w5bc3wwmrp6hi1nv4cg65clbv8wqcqibh2qhzhj")))

(define-public crate-dnspod-cli-0.1.1 (c (n "dnspod-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "string" "env"))) (d #t) (k 0)) (d (n "dnspod-lib") (r "^0.1.2") (f (quote ("clap"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1224qzj41l58f62lp4qzjihlqwvqim9xa1g18wl34bbniiyj5nlq")))

(define-public crate-dnspod-cli-0.1.2 (c (n "dnspod-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "string" "env"))) (d #t) (k 0)) (d (n "dnspod-lib") (r "^0.1.8") (f (quote ("clap"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)))) (h "0a958z4rsy1g1g5v426r8xd1wm9fqz92gk8jqbcpv8zl8af20kjb")))

(define-public crate-dnspod-cli-0.1.3 (c (n "dnspod-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "string" "env"))) (d #t) (k 0)) (d (n "dnspod-lib") (r "^0.1.11") (f (quote ("clap"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)))) (h "1gpc6i4zxg349s6rr5s1381677f36qfgc124bxi4xic5x6zhhzfz")))

