(define-module (crates-io dn ot dnoted) #:use-module (crates-io))

(define-public crate-dnoted-0.1.0 (c (n "dnoted") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y7vma88s21sgq6h6nsl59n48l7nsb7y14wzyz26jjciskcrk3h4")))

