(define-module (crates-io dn ot dnoted-edc) #:use-module (crates-io))

(define-public crate-dnoted-edc-0.1.0 (c (n "dnoted-edc") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "048s4vknd1c2rlj9cgfhkpnfzz18x7fkfiw0pyn1bj47pp7xz7kd")))

