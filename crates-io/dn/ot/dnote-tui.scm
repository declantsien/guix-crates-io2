(define-module (crates-io dn ot dnote-tui) #:use-module (crates-io))

(define-public crate-dnote-tui-0.1.0 (c (n "dnote-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "tui") (r "^0.22.0") (d #t) (k 0) (p "ratatui")))) (h "0fs8dc69yaw0swxdpxlc8ka8hiz6j3d6805bljn1zz6xqncmr4gv")))

(define-public crate-dnote-tui-0.1.1 (c (n "dnote-tui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "tui") (r "^0.23.0") (d #t) (k 0) (p "ratatui")))) (h "0krsv4pyh5vi7wk3zn9ckdihxydyzgyri6db2pn96z1wvla148gm")))

(define-public crate-dnote-tui-0.2.0 (c (n "dnote-tui") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "tui") (r "^0.23.0") (d #t) (k 0) (p "ratatui")))) (h "10lrwf5fjal45120gpg4bp2p5nnjc5aqk7h4xg0ra8qlcmp3k2q0")))

(define-public crate-dnote-tui-0.2.1 (c (n "dnote-tui") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "tui") (r "^0.23.0") (d #t) (k 0) (p "ratatui")))) (h "1za8kakp9i21djzc8ym38cb9a9xhqcbnb1wg2kjpi1qbhjjj9ikb")))

