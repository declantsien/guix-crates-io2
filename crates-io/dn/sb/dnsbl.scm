(define-module (crates-io dn sb dnsbl) #:use-module (crates-io))

(define-public crate-dnsbl-0.1.0 (c (n "dnsbl") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (f (quote ("tokio-runtime" "dns-over-rustls"))) (k 0)))) (h "00sqprnvshh4i769wckrdszm5cd8iinphqar8pna31fq9b1v2iv3")))

(define-public crate-dnsbl-0.2.0 (c (n "dnsbl") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20") (f (quote ("tokio-runtime" "dns-over-rustls"))) (k 0)))) (h "0ipp2q8jc8llnf44m1vrw7ffdfis0dsg7wq4869a0i1g8slicmcf")))

