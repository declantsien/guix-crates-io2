(define-module (crates-io dn sc dnscat-client) #:use-module (crates-io))

(define-public crate-dnscat-client-0.1.0 (c (n "dnscat-client") (v "0.1.0") (d (list (d (n "dnscat") (r "^0.1") (f (quote ("client-cli"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "025plv7vz8bf8vgl4kxqn6hjij3gmcr72488wlr8ysg8ni796jla")))

(define-public crate-dnscat-client-0.1.1 (c (n "dnscat-client") (v "0.1.1") (d (list (d (n "dnscat") (r "^0.1") (f (quote ("client-cli"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1vds2l05l3dvhdfjr67mrbsgzfd7l4l0v3rbmbfkk9z7rmlhy5dy")))

