(define-module (crates-io dn sc dnsclient) #:use-module (crates-io))

(define-public crate-dnsclient-0.1.0 (c (n "dnsclient") (v "0.1.0") (d (list (d (n "dnssector") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0dlg6cgr9bg5fxcx87waiahxwrasj894i45p0bja5dl7aiq3xvck")))

(define-public crate-dnsclient-0.1.1 (c (n "dnsclient") (v "0.1.1") (d (list (d (n "dnssector") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0bsgzkbnpz1iky4696ljbl64jmw46phczf4dw41j070w3ivd9haj")))

(define-public crate-dnsclient-0.1.2 (c (n "dnsclient") (v "0.1.2") (d (list (d (n "dnssector") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1rybxjpmnq43b60pqnf9bca11a5wljka9pjnlb6gqj4188l2zbgb")))

(define-public crate-dnsclient-0.1.3 (c (n "dnsclient") (v "0.1.3") (d (list (d (n "async-std") (r "^0.99") (o #t) (k 0)) (d (n "dnssector") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "09gszbbzngqasn6p62km68ncx0cjiz37ajxs5k93frqwx4pz4bpj") (f (quote (("async" "async-std"))))))

(define-public crate-dnsclient-0.1.4 (c (n "dnsclient") (v "0.1.4") (d (list (d (n "async-std") (r "^0.99") (d #t) (k 0)) (d (n "dnssector") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 0)))) (h "0lbv19ajqjpmrql0gxjrlg8nif8n3c1nhyb7vnbax5awwiacp2gc")))

(define-public crate-dnsclient-0.1.5 (c (n "dnsclient") (v "0.1.5") (d (list (d (n "async-std") (r "^1.1") (d #t) (k 0)) (d (n "dnssector") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "05zk2mddgj51x3ddg44535b2qny29jlg737syi16limrnfyxbv15")))

(define-public crate-dnsclient-0.1.6 (c (n "dnsclient") (v "0.1.6") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "dnssector") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "12ljhh11xc9aw1vjck2v467jcy8d8h1r4giwyk7crm542f3zsn4f")))

(define-public crate-dnsclient-0.1.7 (c (n "dnsclient") (v "0.1.7") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "dnssector") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "08w4rpvlxq567sic7r4w3q2hfgah8h1pxnk4jmpgbvjhwdr6931i")))

(define-public crate-dnsclient-0.1.8 (c (n "dnsclient") (v "0.1.8") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0jbfd799mh61zp52j6n4cm1p25clwq9si9hib8l5rwag3zxls6vy") (f (quote (("default" "async") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.9 (c (n "dnsclient") (v "0.1.9") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "023sgm4n9l11pmj28vkm7r1f45lqcnmvkhmjkxfwwxm8l7cqxlii") (f (quote (("default" "async") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.10 (c (n "dnsclient") (v "0.1.10") (d (list (d (n "async-std") (r "^1.9") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "07v2s9sf8y8353vbsq8av8llz5n4sd1zkj5jly99fxv527g6008h") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.11 (c (n "dnsclient") (v "0.1.11") (d (list (d (n "async-std") (r "^1.9") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1m19bz4adpqr0qgrdaldvsfg1bjm059nvid8wylix1w5s6zda8zw") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.12 (c (n "dnsclient") (v "0.1.12") (d (list (d (n "async-std") (r "^1.9") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1vzlmfrsq76dj09ap8wj8l26pcxp17pqz600k1pg0ylnqk6gqpmf") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.13 (c (n "dnsclient") (v "0.1.13") (d (list (d (n "async-std") (r "^1.9") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0qqazcdhj3qbjihrs6r34j8x9rjpc9aamk33mwfmymrrhag1lq0m") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.14 (c (n "dnsclient") (v "0.1.14") (d (list (d (n "async-std") (r "^1.9") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1dxf6k9xx9h5v9r3bcrc787rvn5h8b18f6y7m88gk26zq8dakb27") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.15 (c (n "dnsclient") (v "0.1.15") (d (list (d (n "async-std") (r "^1.9") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1iwa4nsvbwqb65949c0pfhs4hhxsc6fsls3jvawanvl88ycl598c") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.16 (c (n "dnsclient") (v "0.1.16") (d (list (d (n "async-std") (r "^1.10") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0db31pg9p844d24g31lpvfr74dj06n2a9d2gvgg33cw5cd83xmxj") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.17 (c (n "dnsclient") (v "0.1.17") (d (list (d (n "async-std") (r "^1.10") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1knrsijq6fpxa0vkljib1326zpy600rkgj7y9j4vk2ky5hs598cy") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.18 (c (n "dnsclient") (v "0.1.18") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "18agfhmb44vyzq1w3a33jvfhkkngamnqr3vm1lfr3i2v8qxlpwg2") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

(define-public crate-dnsclient-0.1.19 (c (n "dnsclient") (v "0.1.19") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("unstable"))) (o #t) (d #t) (k 0)) (d (n "dnssector") (r "^0.2.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "102xqivak8f746wi000d0qk7vcnkqcjhfig4b9sdhxgdi0znnkwd") (f (quote (("default" "async") ("async-tokio" "tokio") ("async" "async-std"))))))

