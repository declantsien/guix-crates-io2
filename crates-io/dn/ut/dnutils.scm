(define-module (crates-io dn ut dnutils) #:use-module (crates-io))

(define-public crate-dnutils-0.1.0 (c (n "dnutils") (v "0.1.0") (d (list (d (n "dnpak") (r "^1.3.0") (d #t) (k 0)))) (h "1s13k8vb5pbfgnj8fky9cigrlhlgawpdzm2as3igs16g9cnws55r")))

(define-public crate-dnutils-0.1.1 (c (n "dnutils") (v "0.1.1") (d (list (d (n "dnpak") (r "^1.3.1") (d #t) (k 0)))) (h "0bmp232hgg1vz2ia6licdv09zc8l02070dal5shcdpb2r4rdsq52")))

(define-public crate-dnutils-0.1.1-1 (c (n "dnutils") (v "0.1.1-1") (d (list (d (n "dnpak") (r "^1.3.1") (d #t) (k 0)))) (h "1nb6wl4kcbb66rcdgayss37fx5gjdm5623lc4laz0bgsfpb305p6")))

(define-public crate-dnutils-0.1.2 (c (n "dnutils") (v "0.1.2") (d (list (d (n "dnpak") (r "^1.3.1") (d #t) (k 0)))) (h "0sjla0bpf61haj2p339gi1w8yjpk6j86h2322a54y9aakvy6qjsl")))

(define-public crate-dnutils-0.1.3 (c (n "dnutils") (v "0.1.3") (d (list (d (n "dnpak") (r "^1.3.3") (d #t) (k 0)))) (h "1d2dgcnfcbwbhrp46fdx542ard8fwa88nd5aq2j53nfqyzmvv33w")))

(define-public crate-dnutils-0.1.4 (c (n "dnutils") (v "0.1.4") (d (list (d (n "dnpak") (r "^1.3.3") (d #t) (k 0)))) (h "1p3qsmai4chas5c5q1mvs74j4clcscqxk85n70yx4gx59dlpdi0h")))

(define-public crate-dnutils-0.1.5 (c (n "dnutils") (v "0.1.5") (d (list (d (n "dnpak") (r "^2.0.1") (d #t) (k 0)))) (h "05wagg0pzjgg016n2yal52ifvkm3p8hlbmizsrspxw8db2ph16r2")))

(define-public crate-dnutils-0.1.6 (c (n "dnutils") (v "0.1.6") (d (list (d (n "dnpak") (r "^2.0.1") (d #t) (k 0)))) (h "0amjzyqn24gma5zvd3k6wdsjazmj15wnw5h5bnv4pwvw53h9j24d")))

(define-public crate-dnutils-0.1.7 (c (n "dnutils") (v "0.1.7") (d (list (d (n "dnpak") (r "^2.1.1") (d #t) (k 0)))) (h "1c54vn6ga3ifapplryvf8gdr54a0s2kpfj0dp08lcm2wrs3nlswm")))

(define-public crate-dnutils-0.1.8 (c (n "dnutils") (v "0.1.8") (d (list (d (n "dnpak") (r "^2.1.1") (d #t) (k 0)))) (h "1c4fv7mpnvdg37id729v639zljgqgfdzrx87wkb134wfwln054n6")))

(define-public crate-dnutils-0.1.9 (c (n "dnutils") (v "0.1.9") (d (list (d (n "dnpak") (r "^2.1.1") (d #t) (k 0)))) (h "1m9g6gpdm1m5inxxcsdj03k5zb5rhvlm4zxd0rwy7acxi5rcz37c")))

(define-public crate-dnutils-0.1.10 (c (n "dnutils") (v "0.1.10") (d (list (d (n "dnpak") (r "^2.1.1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "native-windows-gui") (r "^1.0.12") (d #t) (k 0)))) (h "00agz09ji7nd3036jd7xqcmhmc5fwf94fv0xjac96bwrbd9d8d42")))

(define-public crate-dnutils-0.1.11 (c (n "dnutils") (v "0.1.11") (d (list (d (n "dnpak") (r "^2.1.1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "native-windows-gui") (r "^1.0.12") (d #t) (k 0)))) (h "157g8yrl3sdn0xhr79lzswplglirbwsyawc8zp28cpg97g2h5ciz") (y #t)))

(define-public crate-dnutils-0.1.11-1 (c (n "dnutils") (v "0.1.11-1") (d (list (d (n "dnpak") (r "^2.1.1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "native-windows-gui") (r "^1.0.12") (d #t) (k 0)))) (h "1lrszs2a2729fcsh4zjcnyc4ylkm87sk5zvjy0v1b67yqwqkw6kd") (y #t)))

(define-public crate-dnutils-0.1.12 (c (n "dnutils") (v "0.1.12") (d (list (d (n "dnpak") (r "^2.1.1") (d #t) (k 0)) (d (n "md-5") (r "^0.10.1") (d #t) (k 0)) (d (n "native-windows-gui") (r "^1.0.12") (d #t) (k 0)))) (h "1jw2c83iy0a2bqbdl91d17qpr15hlk2988haklv95plwvp68zmhc")))

