(define-module (crates-io dn ld dnld) #:use-module (crates-io))

(define-public crate-dnld-0.1.0 (c (n "dnld") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)))) (h "083jn0xr2n0mi5mfkf9qqakmxlphr86xdhkb552m8qsvlfdn8324")))

(define-public crate-dnld-0.1.1 (c (n "dnld") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)))) (h "029g5iaifrh6cfb7ir7bhb2ssw9bqlw2k069f9kjpwcy11srlxam")))

(define-public crate-dnld-0.1.2 (c (n "dnld") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0khlmg16a5mnx2bvx9ynrx5f0cwklxvlvrg4mqb256pnrafsqdf8")))

