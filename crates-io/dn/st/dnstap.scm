(define-module (crates-io dn st dnstap) #:use-module (crates-io))

(define-public crate-dnstap-0.1.4 (c (n "dnstap") (v "0.1.4") (d (list (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "framestream") (r "^0.1.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "protobuf") (r "^1.2.2") (d #t) (k 0)))) (h "1yfmqksjqmivxjllkhxjbpqvldczbxmp4x47s2lril680ckj3y0d")))

(define-public crate-dnstap-0.1.5 (c (n "dnstap") (v "0.1.5") (d (list (d (n "framestream") (r "^0.1.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.16") (d #t) (k 0)) (d (n "protobuf") (r "^1.2.2") (d #t) (k 0)))) (h "0ipkqdf45h5am6rjq8a1jncjfqz83csnfj8cdrj785ba9qqh05jr")))

(define-public crate-dnstap-0.1.6 (c (n "dnstap") (v "0.1.6") (d (list (d (n "framestream") (r "^0.2.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.25") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2") (d #t) (k 1)))) (h "1fnvkap6pn12886pp6c09k86gj6h47nwvwsb0wwyy7301s6f9wmq")))

(define-public crate-dnstap-0.1.7 (c (n "dnstap") (v "0.1.7") (d (list (d (n "framestream") (r "^0.2.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "protobuf") (r "^3.0.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.0.1") (d #t) (k 1)))) (h "02l9m1bf7vkq01kj5b6w481ck93qn2dq47j1pw8a8f8d23fp8pgf")))

