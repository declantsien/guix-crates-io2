(define-module (crates-io dn dt dndtrigger) #:use-module (crates-io))

(define-public crate-dndtrigger-0.1.0 (c (n "dndtrigger") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "service-manager") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01z4wrkdw4aj40nalif7dpl4757292dppwi53flz1pwpndwd319v")))

