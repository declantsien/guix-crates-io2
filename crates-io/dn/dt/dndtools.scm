(define-module (crates-io dn dt dndtools) #:use-module (crates-io))

(define-public crate-dndtools-0.1.0 (c (n "dndtools") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0316ri7bvx1i0i19b4zzlahbgwf911dnk4y75j452d03x8q50awb")))

