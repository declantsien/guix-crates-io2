(define-module (crates-io dn or dnorm) #:use-module (crates-io))

(define-public crate-dnorm-0.1.0 (c (n "dnorm") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kzcxiviv5ixaw00akrnzxr8c7207vkfmng2hcjs7c9r4biryvvi")))

