(define-module (crates-io dn sr dnsresolv) #:use-module (crates-io))

(define-public crate-dnsresolv-0.1.0 (c (n "dnsresolv") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (d #t) (k 0)))) (h "1chip9n527pq0fjd0gh182l5lqhszynkkswqv4lvabdnkmr7b5xi")))

(define-public crate-dnsresolv-0.1.1 (c (n "dnsresolv") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (d #t) (k 0)))) (h "09wcx8525nminf0v6hh70wzh41dlamprs0ifn93jxg39cawmp3ng")))

(define-public crate-dnsresolv-0.1.2 (c (n "dnsresolv") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (d #t) (k 0)))) (h "0w4zqpq335b8f5yaz6wg99i1xz4hb4dwi8hhkniq93q5nyim1siv")))

