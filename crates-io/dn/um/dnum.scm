(define-module (crates-io dn um dnum) #:use-module (crates-io))

(define-public crate-dnum-0.1.0 (c (n "dnum") (v "0.1.0") (h "19ksp4c18m92vpknhzvkhb1lkzmhrj7wjkk3xrbw65224z7zvi91")))

(define-public crate-dnum-0.3.0 (c (n "dnum") (v "0.3.0") (h "0wh6qzhxjk3yh927gyhsi2x8mfzmh9x3bb0l1wd1p6as1bppvs6s") (y #t)))

