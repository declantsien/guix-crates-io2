(define-module (crates-io dn s- dns-sd) #:use-module (crates-io))

(define-public crate-dns-sd-0.1.0 (c (n "dns-sd") (v "0.1.0") (h "1r23a61hswsryq6rkrkpr55mswfgxs3v4w6v6kyzabvdymgh99la")))

(define-public crate-dns-sd-0.1.1 (c (n "dns-sd") (v "0.1.1") (d (list (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)))) (h "032wqplaa45swqa4b2i7r45i696b25n6zrkmrkbn8kyk3j0w3qql")))

(define-public crate-dns-sd-0.1.2 (c (n "dns-sd") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)))) (h "0dycskrmhmc9il79qi3r11xr88fmjn8kv8pbqga8r2wmg1g2bix7")))

(define-public crate-dns-sd-0.1.3 (c (n "dns-sd") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3.6") (d #t) (k 1)))) (h "11r0jymjshfnn3sh2nqjhrikk4r5rr1g36sip9iqy8i0xafm0j6p")))

