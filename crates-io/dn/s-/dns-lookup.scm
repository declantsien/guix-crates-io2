(define-module (crates-io dn s- dns-lookup) #:use-module (crates-io))

(define-public crate-dns-lookup-0.1.0 (c (n "dns-lookup") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "0ha7p8qa19aygdnxjgrz5x9y9gc117bnp429ha46waf8vshgidwn")))

(define-public crate-dns-lookup-0.2.0 (c (n "dns-lookup") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1lc10mkm5xplb9yj1hg29brrz2gc2wynz4zdd8b6sf0qrprxd805")))

(define-public crate-dns-lookup-0.2.1 (c (n "dns-lookup") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "1lffs3fw0qkc1k4w4vyhb8bdcrqb95df18zvr568y5d21xr0rvys")))

(define-public crate-dns-lookup-0.3.0 (c (n "dns-lookup") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1irk9jzv8c14zgp752niwnmr5qam7gn2hc28y6rqx4hhf8xjsh5b")))

(define-public crate-dns-lookup-0.3.1 (c (n "dns-lookup") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pwfnyshlwdpmxjq3xan0f33vw5grq80w80yz3h0kr6fpsr18nqv")))

(define-public crate-dns-lookup-0.3.2 (c (n "dns-lookup") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z7fmhzy0kms4fjp8v6lmbzwg2gqr5zwl8x14zk637b5mdcxhf0h")))

(define-public crate-dns-lookup-0.4.0 (c (n "dns-lookup") (v "0.4.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v6n1jf1ny4q6jci6114adj64qd3a3fwjha7h655423cm3x9w7ga") (f (quote (("default"))))))

(define-public crate-dns-lookup-0.5.0 (c (n "dns-lookup") (v "0.5.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1asrhw5zsp9jpawn1fb4rqr9r5sabd019fdpnmwwlzw94v1m4jsv") (f (quote (("default"))))))

(define-public crate-dns-lookup-0.6.0 (c (n "dns-lookup") (v "0.6.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vqacl9qxa5wrf1wcf6hav7874yfhsjz8qxn6vlxkyixmmkdw839") (f (quote (("default"))))))

(define-public crate-dns-lookup-0.7.0 (c (n "dns-lookup") (v "0.7.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0f4mbbfwl6rp71g6sabwsqnnbjw17lz6fa4d8ddngq659kwsrg9a")))

(define-public crate-dns-lookup-0.7.1 (c (n "dns-lookup") (v "0.7.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "19imzwsmi718d22aiwbc2vxwy5hjlw5p6wmyldj7mc95yw4436w9")))

(define-public crate-dns-lookup-0.8.0 (c (n "dns-lookup") (v "0.8.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1qansjl2xcnqkpdp9jgp7c8pymyka63bxva7hk6ia7c1g7i6907p")))

(define-public crate-dns-lookup-0.8.1 (c (n "dns-lookup") (v "0.8.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1bqpznvq43xi2cqzd5h6mjnd4k46rr8rrd7nkib7r87059i5arh7")))

(define-public crate-dns-lookup-0.9.0 (c (n "dns-lookup") (v "0.9.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "0v266k6z1xgcf7pkxg2af922zz3gqyjgr1pj02ydw7n8041l3lma")))

(define-public crate-dns-lookup-0.9.1 (c (n "dns-lookup") (v "0.9.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "17q36n4qy5235vzhgsm5pp1c0c0z6n4ijjlg883wfhcji5j0g0al")))

(define-public crate-dns-lookup-1.0.0 (c (n "dns-lookup") (v "1.0.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "0z90rhadfyqdlxc99a6zncx6bnnkwdx5w8cpmzf3a218zmm51i5k")))

(define-public crate-dns-lookup-1.0.1 (c (n "dns-lookup") (v "1.0.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "1gvjdcwj803w9gd2bfy8f3n1skzw9r2586sfqx4641qbhrq8d60k")))

(define-public crate-dns-lookup-1.0.2 (c (n "dns-lookup") (v "1.0.2") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "1kl2iybcydnakkjwkj08z534bbm7nfaz7p03xbjkbmdbhgljg94f")))

(define-public crate-dns-lookup-1.0.3 (c (n "dns-lookup") (v "1.0.3") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "0y1zz5nmn8al83zalsjmi1yglj9qgx099j81mnkqbrpp2lya5gjc")))

(define-public crate-dns-lookup-1.0.4 (c (n "dns-lookup") (v "1.0.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "101lh2vfwgghisnsi9hk4s4iqbisbq5a7k6p851a9bpvzmgn6scg")))

(define-public crate-dns-lookup-1.0.5 (c (n "dns-lookup") (v "1.0.5") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "1p3r1cw72lngz40hw7m13fablqv46l5wv04crg54x36i3yb8hg89")))

(define-public crate-dns-lookup-1.0.6 (c (n "dns-lookup") (v "1.0.6") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "0m3lrwm0fizys1nw1p9jj2r023l2mklidfr0cyv5wk03lzimqk7b")))

(define-public crate-dns-lookup-1.0.7 (c (n "dns-lookup") (v "1.0.7") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "0xd0ldqdrgs4k76pgdqcqd6vnp33ajqma5w6pdz5fac7v6nv4f41")))

(define-public crate-dns-lookup-1.0.8 (c (n "dns-lookup") (v "1.0.8") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("windef" "ws2def" "ws2tcpip"))) (t "cfg(windows)") (k 0)))) (h "0wk877zla9gdns5f1zgrxwzpi0abj2ld2n54a6dqsln4ab4szv2k")))

(define-public crate-dns-lookup-2.0.0 (c (n "dns-lookup") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.5.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Networking_WinSock" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vasd88dywi4jdr2pvl9whnhxhyvkpdw5z6g9chq46939bfziwi1")))

(define-public crate-dns-lookup-2.0.1 (c (n "dns-lookup") (v "2.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.5.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Networking_WinSock" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04da38lyhv579f51vp8qiy7qzjzq5pxlmynm9d6n06jpfnvhapis")))

(define-public crate-dns-lookup-2.0.2 (c (n "dns-lookup") (v "2.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.5.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Networking_WinSock" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04l84gqi4n3jcvbwfk9rqvly4b7l9qlkfch1mi0yg7cykykjlcwg")))

(define-public crate-dns-lookup-2.0.3 (c (n "dns-lookup") (v "2.0.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.5.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Networking_WinSock" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ia14hv2yg0in4awljm2wsxrn1yl6w6r8shj9sbxlsn9ip6s63wd")))

(define-public crate-dns-lookup-2.0.4 (c (n "dns-lookup") (v "2.0.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "socket2") (r "^0.5.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Networking_WinSock" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z74n2zij2gahycabm0gkmkyx574h76gwk7sz93yqpr3qa3n0xp5")))

