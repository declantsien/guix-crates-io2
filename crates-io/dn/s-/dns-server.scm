(define-module (crates-io dn s- dns-server) #:use-module (crates-io))

(define-public crate-dns-server-0.1.0 (c (n "dns-server") (v "0.1.0") (d (list (d (n "fixed-buffer") (r "^0.3.1") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "permit") (r "^0.1.4") (d #t) (k 0)) (d (n "prob-rate-limiter") (r "^0.1.0") (d #t) (k 0)))) (h "1r2l40smksdgwnnxxv30blzbjnlz2zy5yrqzy0ppjh0r8a59r1y4")))

(define-public crate-dns-server-0.2.0 (c (n "dns-server") (v "0.2.0") (d (list (d (n "fixed-buffer") (r "^0.3.1") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "permit") (r "^0.1.4") (d #t) (k 0)) (d (n "prob-rate-limiter") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 2)))) (h "152kxr84r0jral9bldw9hp76syqaz1msqvdgp8nmb6g81ggqlxmz") (y #t)))

(define-public crate-dns-server-0.2.1 (c (n "dns-server") (v "0.2.1") (d (list (d (n "fixed-buffer") (r "^0.3.1") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "permit") (r "^0.1.4") (d #t) (k 0)) (d (n "prob-rate-limiter") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 2)))) (h "0d2any1in5j06y6p5v6j1aafrp8q4xvyqr1n8lffv1khmyvqgg87")))

(define-public crate-dns-server-0.2.2 (c (n "dns-server") (v "0.2.2") (d (list (d (n "fixed-buffer") (r "^0.3.1") (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "permit") (r "^0.1.4") (d #t) (k 0)) (d (n "prob-rate-limiter") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 2)))) (h "0yz2mxpcff9nnqbds39p8qrscvjlkrnyl8m0qaw5wzyf6nbw5pw2")))

(define-public crate-dns-server-0.2.3 (c (n "dns-server") (v "0.2.3") (d (list (d (n "fixed-buffer") (r "^0.3.1") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "permit") (r "^0.1.4") (d #t) (k 0)) (d (n "prob-rate-limiter") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 2)))) (h "0p68ywp377lg6bn7il9h1bd6fi3d82pq64v97sbagl7zziff3v4p")))

