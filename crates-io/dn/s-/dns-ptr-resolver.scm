(define-module (crates-io dn s- dns-ptr-resolver) #:use-module (crates-io))

(define-public crate-dns-ptr-resolver-1.0.0 (c (n "dns-ptr-resolver") (v "1.0.0") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustdns") (r "^0.4.0") (d #t) (k 0)) (d (n "trust-dns-client") (r "^0.22.0") (k 0)))) (h "0fx2ayf2vmip15a7xpf6xyh4wwgklnh74cfr2g1m9pfszravy9z9") (r "1.67.0")))

(define-public crate-dns-ptr-resolver-1.1.0 (c (n "dns-ptr-resolver") (v "1.1.0") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustdns") (r "^0.4.0") (d #t) (k 0)) (d (n "trust-dns-client") (r "^0.22.0") (k 0)) (d (n "weighted-rs") (r "^0.1.3") (d #t) (k 0)))) (h "1l0k62zic4zfhgqxg66sbk8crbll80yjwxkgjk9cpnr7g8pc84cp") (r "1.67.0")))

