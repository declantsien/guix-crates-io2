(define-module (crates-io dn s- dns-parser) #:use-module (crates-io))

(define-public crate-dns-parser-0.1.0 (c (n "dns-parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "ip") (r "^1.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)))) (h "1x9gzynrmahi2qr2mj4zxlppvrrpw72ma7871by1ixqsifbig8bs")))

(define-public crate-dns-parser-0.2.0 (c (n "dns-parser") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "ip") (r "^1.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)))) (h "1fibp3hnxikjy6aicdgd33jqpr81k0r8y5hccrzw5w8inv0kxhmh")))

(define-public crate-dns-parser-0.2.1 (c (n "dns-parser") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "ip") (r "^1.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)))) (h "083pz81v2lfy11kwl3lsvqg5lhzh5hd0l8b09kg9rsv17wyd524y")))

(define-public crate-dns-parser-0.3.0 (c (n "dns-parser") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "ip") (r "^1.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^0.2.1") (d #t) (k 0)))) (h "1nz7z48rl6rsx2mrm7dffmhnlrliky21pppdgy8jxv7drdn9cvkm")))

(define-public crate-dns-parser-0.3.1 (c (n "dns-parser") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "01sf1lnmff9q01sr393q29pawg12l4rspsv8mmzh3r96l0c7lz7h")))

(define-public crate-dns-parser-0.3.2 (c (n "dns-parser") (v "0.3.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0z823hzng6bab5bsn3grvfrzyqs1qjhaxvfhyj15lh4gm4dm9xfg")))

(define-public crate-dns-parser-0.3.3 (c (n "dns-parser") (v "0.3.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0kh1h97ha377mq936kmpn05pri6m2x6xja21cr131xm7zh3mn6zq")))

(define-public crate-dns-parser-0.3.4 (c (n "dns-parser") (v "0.3.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0n14pf728l78hfc80vpdz5fr0s9fq2wh1vkq5pyv6rkvqkzsd74c")))

(define-public crate-dns-parser-0.3.5 (c (n "dns-parser") (v "0.3.5") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "07946wx3a94v80l821rjd9qkjpp6rs66bjbclqs0ajglzmlr4xp2")))

(define-public crate-dns-parser-0.4.0 (c (n "dns-parser") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1l5h7dm6lnh1xgbb3w7iy18sx4vb49qg5rq3sklff6fp1c0l04m0")))

(define-public crate-dns-parser-0.5.0 (c (n "dns-parser") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "11j6f2rgln240s0rsw2lx0c5k1d4bhf872nm1c9mgjpfwsx6i7rc")))

(define-public crate-dns-parser-0.5.1 (c (n "dns-parser") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1vmybx1d1z5ginpfcdhvsvjhh7qv8730ag2lxiq5qmj0izs56qqj")))

(define-public crate-dns-parser-0.6.0 (c (n "dns-parser") (v "0.6.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "17zax1wyqb0spvjbbajnfmfgkaf7nirlgin8bc2ww7xassm347xi")))

(define-public crate-dns-parser-0.7.0 (c (n "dns-parser") (v "0.7.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0qbdgcz5p8gg9gvigkrwm7472s7l2hqd6yybippa7w0h6zf73mxb")))

(define-public crate-dns-parser-0.7.1 (c (n "dns-parser") (v "0.7.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0g95vi8sq2ajz4mv1f0xaiig2j8cdjzq7jr37pa158xfc1khy0pp")))

(define-public crate-dns-parser-0.8.0 (c (n "dns-parser") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 2)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1spv0psvxc31qg9xnqf0gmjclyiim7vp23r2b1gzf1ix8zlkply4") (f (quote (("with-serde" "serde" "serde_derive"))))))

