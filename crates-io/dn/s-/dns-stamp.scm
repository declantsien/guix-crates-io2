(define-module (crates-io dn s- dns-stamp) #:use-module (crates-io))

(define-public crate-dns-stamp-0.1.0 (c (n "dns-stamp") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "0dd9sgff8wwc8jv79igdd13k5n8vs5h5qlwj1jqrl4s8lmis3xwz")))

