(define-module (crates-io dn s- dns-fun) #:use-module (crates-io))

(define-public crate-dns-fun-0.1.0 (c (n "dns-fun") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rink-core") (r "^0.6.2") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "trust-dns-server") (r "^0.21.2") (d #t) (k 0)))) (h "1vb2rnmpq1ac46r3gbayswmhxbyazil4zwv8362d13lss6g7q7ln")))

