(define-module (crates-io dn s- dns-parser-joe) #:use-module (crates-io))

(define-public crate-dns-parser-joe-0.1.0 (c (n "dns-parser-joe") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0nfsgvga6ra4gbqdla7sk2sgx240lvlxvvzpk4bc5vsl8r40026p")))

(define-public crate-dns-parser-joe-0.1.1 (c (n "dns-parser-joe") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1jynk29hj7yppk4wcdj9h2xf3qwk3lpmcpcnkdrfncg72yas13kx")))

