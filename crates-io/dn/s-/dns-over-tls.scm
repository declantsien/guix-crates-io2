(define-module (crates-io dn s- dns-over-tls) #:use-module (crates-io))

(define-public crate-dns-over-tls-0.1.0 (c (n "dns-over-tls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tokio") (r "^0.2.20") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio-util") (r "^0.3.1") (f (quote ("codec" "udp"))) (d #t) (k 0)))) (h "04cwxmj6c4n7jq5vf5kv4waggmijyn7aqjk0b0ywwrimwk1crj3x")))

