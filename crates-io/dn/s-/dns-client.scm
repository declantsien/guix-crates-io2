(define-module (crates-io dn s- dns-client) #:use-module (crates-io))

(define-public crate-dns-client-0.0.1 (c (n "dns-client") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dnsclient") (r "^0.1.7") (d #t) (k 0)))) (h "044pxhplslbp1cbky99n1w6yf48lskwp1vdj21ic17swxb4hdyd3")))

(define-public crate-dns-client-0.0.2 (c (n "dns-client") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dnsclient") (r "^0.1.7") (d #t) (k 0)))) (h "05ph4xxc0b83bx7bg6iyr01x7rij125vs41gsaa91331jm218pzd")))

