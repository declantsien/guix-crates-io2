(define-module (crates-io dn s- dns-stamp-parser) #:use-module (crates-io))

(define-public crate-dns-stamp-parser-1.0.0 (c (n "dns-stamp-parser") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0wlxc28a0m509nzjksm880ykwa667xcbm10iww9cpx6b62nq5mkb")))

(define-public crate-dns-stamp-parser-1.0.1 (c (n "dns-stamp-parser") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)))) (h "1y3bpnwn91axkqhpybb80nam4shm5wf8w7sb4rm9xm4c8diwz09n")))

(define-public crate-dns-stamp-parser-1.1.0 (c (n "dns-stamp-parser") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0wr3bnjdfxvkcnyjg431ipykhfpm3v4k60m1xg2wwfzz53xr5k5p")))

(define-public crate-dns-stamp-parser-2.0.0 (c (n "dns-stamp-parser") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1lfw43ag7p5d74i0vp3wnzry96wg2vpijmvpks054ivzid3j95wy") (f (quote (("resolve" "trust-dns-resolver") ("default"))))))

(define-public crate-dns-stamp-parser-2.0.1 (c (n "dns-stamp-parser") (v "2.0.1") (d (list (d (n "base64") (r "~0.13.0") (d #t) (k 0)) (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.23") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "~0.20.0") (o #t) (d #t) (k 0)))) (h "1n00jywmlfflrf2ihz2nahbmslbc6ya59ih959c9k3h1l8hq6dw8") (f (quote (("resolve" "trust-dns-resolver") ("default"))))))

(define-public crate-dns-stamp-parser-3.0.0 (c (n "dns-stamp-parser") (v "3.0.0") (d (list (d (n "base64") (r "~0.13.0") (d #t) (k 0)) (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.23") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "~0.20.0") (o #t) (d #t) (k 0)))) (h "1v8hxvv1f7a6dpd7vcb13my5abvj83r4nmk461pmv24fs5qga5xc") (f (quote (("resolve" "trust-dns-resolver") ("default"))))))

