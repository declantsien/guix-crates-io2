(define-module (crates-io dn s- dns-protocol) #:use-module (crates-io))

(define-public crate-dns-protocol-0.1.0 (c (n "dns-protocol") (v "0.1.0") (d (list (d (n "memchr") (r "^2.5.0") (k 0)))) (h "1y14ayykh5ywv8lcasbpxhapx16gl2gpyz1jlv93clhvkbyb7xmg") (f (quote (("std") ("default" "std"))))))

(define-public crate-dns-protocol-0.1.1 (c (n "dns-protocol") (v "0.1.1") (d (list (d (n "memchr") (r "^2.5.0") (k 0)))) (h "0x2aajbz3fdnffpgjygjb5qvg0ajd0yd5sikdrv50lbfhj6nqrb6") (f (quote (("std") ("default" "std"))))))

