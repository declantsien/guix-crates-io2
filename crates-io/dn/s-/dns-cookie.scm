(define-module (crates-io dn s- dns-cookie) #:use-module (crates-io))

(define-public crate-dns-cookie-0.1.0 (c (n "dns-cookie") (v "0.1.0") (d (list (d (n "no-std-net") (r "^0.5.0") (k 0)) (d (n "siphasher") (r "^0.3.7") (k 0)) (d (n "time") (r "^0.3.4") (k 0)))) (h "0r9kaw82z689abj91lqkws2vc5ma551bi2nl2h6niqxi6v65irl5") (f (quote (("std") ("default" "std"))))))

(define-public crate-dns-cookie-0.2.0 (c (n "dns-cookie") (v "0.2.0") (d (list (d (n "no-std-net") (r "^0.5.0") (o #t) (k 0)) (d (n "siphasher") (r "^0.3.7") (k 0)) (d (n "time") (r "^0.3.4") (k 0)))) (h "1bz53p53frivdkyjn6wy3hdf69mfq4laj8x4ykkhkizm1g7wj50i")))

(define-public crate-dns-cookie-0.3.0 (c (n "dns-cookie") (v "0.3.0") (d (list (d (n "no-std-net") (r "^0.5.0") (o #t) (k 0)) (d (n "siphasher") (r "^0.3.7") (k 0)) (d (n "time") (r "^0.3.4") (k 0)))) (h "131l5zlwnz8019icrg07mc12wz5b9g112mmm8jr2ilp4701yg8b0")))

