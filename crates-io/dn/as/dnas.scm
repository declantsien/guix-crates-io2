(define-module (crates-io dn as dnas) #:use-module (crates-io))

(define-public crate-dnas-0.1.0 (c (n "dnas") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19q30ki91avn4hp3adr405slzw9hk5g9k1ga0gsywjb5qj2pj4fz")))

