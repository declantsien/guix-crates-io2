(define-module (crates-io dn s_ dns_in_a_weekend) #:use-module (crates-io))

(define-public crate-dns_in_a_weekend-0.1.0 (c (n "dns_in_a_weekend") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structure") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0959nbfj0f759mf2azvg1y4lqcmgqqacw4mvy04hpw7vnarapmpw")))

(define-public crate-dns_in_a_weekend-0.1.1 (c (n "dns_in_a_weekend") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structure") (r "^0.1.2") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "19w9w4r7vq8li53186lxqb364pcskbxcldzzwis7p76ssp1fb3wr")))

