(define-module (crates-io dn s_ dns_online) #:use-module (crates-io))

(define-public crate-dns_online-0.1.0 (c (n "dns_online") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.19") (f (quote ("static-curl" "static-ssl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0lfsg50xw5bz7bmxcfaay9qywgjyvvyb93274xcaimg0hhd9kyrl")))

(define-public crate-dns_online-0.1.1 (c (n "dns_online") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.19") (f (quote ("static-curl" "static-ssl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "169mfykm9ylhza18j2qyc87bb5j7rhr7wjya2sig592j2k099kry")))

(define-public crate-dns_online-0.1.2 (c (n "dns_online") (v "0.1.2") (d (list (d (n "curl") (r "^0.4.22") (f (quote ("static-curl" "static-ssl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "17rjsdmw1ng8wj1mxp1611idkrza9pn5dficwlrpal99g9rg3fvc")))

(define-public crate-dns_online-0.2.1 (c (n "dns_online") (v "0.2.1") (d (list (d (n "curl") (r "^0.4.22") (f (quote ("static-curl" "static-ssl"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "12kjgz90214xr4my5n9lg9rfhw1349v4fzxqp65ricv948wnnabq")))

