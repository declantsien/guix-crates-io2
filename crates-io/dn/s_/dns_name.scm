(define-module (crates-io dn s_ dns_name) #:use-module (crates-io))

(define-public crate-dns_name-1.0.0 (c (n "dns_name") (v "1.0.0") (d (list (d (n "trust-dns-proto") (r "^0.22.0") (f (quote ("dnssec"))) (d #t) (k 0)))) (h "0wcz45rs60zd20ny4sydsqsmrr871vvbd6mxm8k7k5qip61zvy00")))

(define-public crate-dns_name-1.0.1 (c (n "dns_name") (v "1.0.1") (d (list (d (n "hickory-proto") (r "^0.24.0") (f (quote ("dnssec"))) (d #t) (k 0)))) (h "0klzn3gch1cq199r8hzxvjvkzz7apbsg9nyxzq0b60l623687ihf")))

