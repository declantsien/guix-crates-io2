(define-module (crates-io dn s_ dns_common_derive) #:use-module (crates-io))

(define-public crate-dns_common_derive-0.1.0 (c (n "dns_common_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "0z4p574rjqx41fhiy5m4jgj71ci00jwfanbip9mqqf4knz7pyr9p")))

(define-public crate-dns_common_derive-0.2.0 (c (n "dns_common_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "0h6zx4hi73hc8r0lsf9yrksca64g0jzlbx6r3s8b6i5ky2ag2jzp")))

(define-public crate-dns_common_derive-0.2.1 (c (n "dns_common_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "1l0872y3czcpbdpm7px0z2njx71jbbna79c1naalav5dwzfjm3qf")))

