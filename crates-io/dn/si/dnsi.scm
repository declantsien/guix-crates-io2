(define-module (crates-io dn si dnsi) #:use-module (crates-io))

(define-public crate-dnsi-0.1.0 (c (n "dnsi") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (f (quote ("alloc" "clock"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "unstable-doc"))) (d #t) (k 0)) (d (n "domain") (r "^0.10") (f (quote ("resolv" "unstable-client-transport"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1b9l39hp4n80jhppny9mam5scyjiai7fy00qsdgi2860bs7vh2xx") (r "1.74.0")))

