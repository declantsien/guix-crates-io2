(define-module (crates-io dn tk dntk-matrix) #:use-module (crates-io))

(define-public crate-dntk-matrix-0.1.0 (c (n "dntk-matrix") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0hzxbby3lngjxnffrpv206vynf5kba5id4x9l36v1d961wjp4mrp")))

(define-public crate-dntk-matrix-0.1.1 (c (n "dntk-matrix") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0yzwzaw653zzzfx283qxnycc703vlvr2zvlp83d1fdayifykh008")))

(define-public crate-dntk-matrix-0.1.2 (c (n "dntk-matrix") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "12iw7a3vnmg6cy9sp20qgx8ll8v2dsnwj06v5dlx19z3awhvi05h")))

