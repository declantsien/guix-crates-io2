(define-module (crates-io o2 o- o2o-impl) #:use-module (crates-io))

(define-public crate-o2o-impl-0.1.0 (c (n "o2o-impl") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0f98bk3bwmi7pk03spmm4a61h7k3vj03mm4f9gzr04hs3drmcni7")))

(define-public crate-o2o-impl-0.1.1 (c (n "o2o-impl") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1cbzrsj2id3gbayfhjhg2543d68l2vqvrcr11ibpsgwmq8cimkf1")))

(define-public crate-o2o-impl-0.2.0 (c (n "o2o-impl") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0dl2h3iaarrnw6abl25kg044csb67hjc5md27irkcb617744dxf4")))

(define-public crate-o2o-impl-0.2.1 (c (n "o2o-impl") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0a6gvlc1gnxfm692nk9gdavq74zsi4vgxn5ys562ki3ljaia236s")))

(define-public crate-o2o-impl-0.2.2 (c (n "o2o-impl") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1wmpr0g7a57y1hg16ydcmpd6r0yjghwqinw5vficg7pqkgz1wgd8")))

(define-public crate-o2o-impl-0.2.3 (c (n "o2o-impl") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0yvkzc0b88fyl0r2iwg4ww1wq3f3jcqlf30zzimcq8mfc23ikj0r")))

(define-public crate-o2o-impl-0.2.4 (c (n "o2o-impl") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "0pxypj2nrsk15cavn5ldwnngwl83p7y8n2sd61611p2xpp4hx8gg")))

(define-public crate-o2o-impl-0.2.5 (c (n "o2o-impl") (v "0.2.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "0zgnacsfy2vq9vayjg05yf72c89w1arvbndx6psc27nh1bfmvk1g")))

(define-public crate-o2o-impl-0.3.0 (c (n "o2o-impl") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "0x0986m7n568xwfawsclz602ywf4apxr7lnrg59bss5fk9pxc9qb")))

(define-public crate-o2o-impl-0.3.1 (c (n "o2o-impl") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "0d1rxmgrcgf3r6yasipcls3apkxx5b9dac4iks5rj94diwjs980s")))

(define-public crate-o2o-impl-0.4.0 (c (n "o2o-impl") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "08icsl50h79lg2dvvs544kizal3w9l0lf0sz9k4pbh6n10rlcl7s")))

(define-public crate-o2o-impl-0.4.1 (c (n "o2o-impl") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "1yry0my8wxddhr10fhw3s7wcsh8m83m4qp70n348j61am10a3801")))

(define-public crate-o2o-impl-0.4.2 (c (n "o2o-impl") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "1nfwcj2hbjiw6svz8dc5mxmrw5dazzyn0najav901wbi95765dcd")))

(define-public crate-o2o-impl-0.4.3 (c (n "o2o-impl") (v "0.4.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)) (d (n "test-case") (r "^3") (d #t) (k 2)))) (h "1asb842akqlydhx373wcvq4cqimzyb8liv9y4rnrmk3a5xkdch1y")))

