(define-module (crates-io o2 o- o2o-macros) #:use-module (crates-io))

(define-public crate-o2o-macros-0.1.0 (c (n "o2o-macros") (v "0.1.0") (d (list (d (n "o2o-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1qdhq9lv2p5bvvbibbfm0ckcrgjhsy9i9836nbzi1lj89df4bzzp")))

(define-public crate-o2o-macros-0.1.1 (c (n "o2o-macros") (v "0.1.1") (d (list (d (n "o2o-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0zv4vh71zldsx881bwj4nqz6h7sgd0a3nqfzkaznnkv8ggczbdlh")))

(define-public crate-o2o-macros-0.2.0 (c (n "o2o-macros") (v "0.2.0") (d (list (d (n "o2o-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "06n50yziwajqzzl76k1fkd2waav0y8kbwf6q03zcs8q6bapa412k")))

(define-public crate-o2o-macros-0.2.1 (c (n "o2o-macros") (v "0.2.1") (d (list (d (n "o2o-impl") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1ssfgsf0cb28295w0n0l65xy5ifd16ks56knjhrvflvqh0nnbak4")))

(define-public crate-o2o-macros-0.2.2 (c (n "o2o-macros") (v "0.2.2") (d (list (d (n "o2o-impl") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1ns5prq320xfjfa0bg8sqqi8q5pgw7zz1kfifh20p11k6kz72cw3")))

(define-public crate-o2o-macros-0.2.3 (c (n "o2o-macros") (v "0.2.3") (d (list (d (n "o2o-impl") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "08h4wpbbkkk7y5pbfg3yk1kx51b7miqdck4x4cv73kap1bf9lavd")))

(define-public crate-o2o-macros-0.2.4 (c (n "o2o-macros") (v "0.2.4") (d (list (d (n "o2o-impl") (r "^0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "1v1px80pj2hjd9vqllrx5dqn3krfkysaqbln0wfc6i9fqzggh7w8")))

(define-public crate-o2o-macros-0.2.5 (c (n "o2o-macros") (v "0.2.5") (d (list (d (n "o2o-impl") (r "^0.2.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "0z7y4daxs4yw80c1v8h9hlvpqn1dm8kzzc1vfb5h58a7ak303par")))

(define-public crate-o2o-macros-0.3.0 (c (n "o2o-macros") (v "0.3.0") (d (list (d (n "o2o-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "0i5k77x73wkp722794kf59zxzd2mzhqpd5qaybzl56snvksw02bi")))

(define-public crate-o2o-macros-0.3.1 (c (n "o2o-macros") (v "0.3.1") (d (list (d (n "o2o-impl") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "01l6fiqlfig3gy6y81c1lwmnczv79xj0bmi7s35rlhrjkiqc62m3")))

(define-public crate-o2o-macros-0.4.0 (c (n "o2o-macros") (v "0.4.0") (d (list (d (n "o2o-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "0ihgrpysxz7cksg6r23r65d8x8dknbyd04qsyc4mxcaa9za56iwf")))

(define-public crate-o2o-macros-0.4.1 (c (n "o2o-macros") (v "0.4.1") (d (list (d (n "o2o-impl") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "1qam3v1ckryqgafs0gkc06gcfy7x1zd6cbxfj0mjrn7mkb491v1a")))

(define-public crate-o2o-macros-0.4.2 (c (n "o2o-macros") (v "0.4.2") (d (list (d (n "o2o-impl") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "1w6br80k9finahngsckvgafanxzakqpv9c1x5grbgnv9rh4w7j58")))

(define-public crate-o2o-macros-0.4.3 (c (n "o2o-macros") (v "0.4.3") (d (list (d (n "o2o-impl") (r "^0.4.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (d #t) (k 0)))) (h "1vrnrjvhnmn5kbvdw1d5689jizpwgqh8z8l89rhjjmqggl5rb74s")))

