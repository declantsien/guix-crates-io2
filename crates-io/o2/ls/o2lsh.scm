(define-module (crates-io o2 ls o2lsh) #:use-module (crates-io))

(define-public crate-o2lsh-0.1.0 (c (n "o2lsh") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.7") (d #t) (k 0)) (d (n "revord") (r "^0.0.2") (d #t) (k 0)))) (h "1crvcdnxmldlpcqw0rna0gnr6l71qn56f658s1fdl7klppa79gm2")))

