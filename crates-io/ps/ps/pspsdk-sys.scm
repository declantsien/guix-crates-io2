(define-module (crates-io ps ps pspsdk-sys) #:use-module (crates-io))

(define-public crate-pspsdk-sys-0.0.1 (c (n "pspsdk-sys") (v "0.0.1") (h "11d2l6hk7b5jin8z4kbg380jsjgd2mbmxwv72y3mhwf23yxkxr0p") (y #t)))

(define-public crate-pspsdk-sys-0.0.2 (c (n "pspsdk-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0kv6ai4pqnn14k68cr50bwyirh6d13ynj8n774qf9x94gvh7p91k") (y #t)))

(define-public crate-pspsdk-sys-0.0.3 (c (n "pspsdk-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.69") (k 0)))) (h "16zln77dlxblvlg1iarx4drplp4ns5lf8xmxzv0nsh7c1xxvah3x") (y #t)))

