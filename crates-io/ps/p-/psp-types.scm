(define-module (crates-io ps p- psp-types) #:use-module (crates-io))

(define-public crate-psp-types-0.1.0 (c (n "psp-types") (v "0.1.0") (d (list (d (n "lsp-types") (r "^0.93") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1pjrx36246gaknxq3pcq405bxl55z94wkmhhcxkkavzyg9l2mpby")))

(define-public crate-psp-types-0.1.1 (c (n "psp-types") (v "0.1.1") (d (list (d (n "lsp-types") (r "^0") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vwxcn5sad7aaayhziramql2nazx9ccypm41x2ikqq0iwwkljbyh")))

