(define-module (crates-io ps p- psp-logger) #:use-module (crates-io))

(define-public crate-psp-logger-0.1.0 (c (n "psp-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.21") (k 0)) (d (n "psp") (r "^0.3.7") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "00iqrmv035n0fbkdpn87xq8s83k6sj2fb9iigyg070wla18jyxyx")))

(define-public crate-psp-logger-0.1.1 (c (n "psp-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.21") (k 0)) (d (n "psp") (r "^0.3.8") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0iimj2d6v1fdla77y543h2063dbj1nxa0vbs0ilp9pzq88ai6pmy")))

