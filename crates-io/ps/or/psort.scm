(define-module (crates-io ps or psort) #:use-module (crates-io))

(define-public crate-psort-0.0.1 (c (n "psort") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.1") (d #t) (k 0)))) (h "00grwnkm0sljwigf4xghisy7idgcv1w0h2yq1ai8yd32jhmh2wrv")))

(define-public crate-psort-0.1.0 (c (n "psort") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.1") (d #t) (k 0)))) (h "0afyl9992ms6g4nb1533r9vmdz4n952r5ghgx5dvq2gqf2vf7ssm")))

(define-public crate-psort-0.1.1 (c (n "psort") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.1") (d #t) (k 0)))) (h "0j4fmm0c4lichzybnnz3mb4z74hkwqp2awd4m5kd8k3is4qs5mpn")))

(define-public crate-psort-0.1.2 (c (n "psort") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.1") (d #t) (k 0)))) (h "068bawgm3l225bcabh6wigr1pbv86a69qz76mmiicrrd6560vx19")))

(define-public crate-psort-0.2.0 (c (n "psort") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.1") (d #t) (k 0)))) (h "1d54s54zx731fzbl891ci722mwvwghrkpwvrsldwy047bh5lfmqf")))

(define-public crate-psort-0.2.2 (c (n "psort") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3.1") (d #t) (k 0)))) (h "0vh8x9bc2b2gz82apg7q2r85ncc5rj80wiwr38iaqc14cy0jydrs")))

