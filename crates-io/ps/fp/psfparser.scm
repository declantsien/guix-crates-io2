(define-module (crates-io ps fp psfparser) #:use-module (crates-io))

(define-public crate-psfparser-0.1.0 (c (n "psfparser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "float_eq") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)))) (h "0bwgb877yhhk3hdzrkry44amswr81dn2d00qw99qfmypmrm1y7l2")))

(define-public crate-psfparser-0.1.1 (c (n "psfparser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "float_eq") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)))) (h "0qz6ria3c16sd307jbgixyjg1q8xjsqx8sah0gnilsydv7fc7qyw")))

(define-public crate-psfparser-0.1.2 (c (n "psfparser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "float_eq") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)))) (h "010y3lak22hkmzcmgwjlm78mr0qfyjryhwcljd7bayb5fkh4kvdd")))

