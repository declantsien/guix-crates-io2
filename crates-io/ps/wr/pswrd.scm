(define-module (crates-io ps wr pswrd) #:use-module (crates-io))

(define-public crate-pswrd-0.0.1 (c (n "pswrd") (v "0.0.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "rpassword") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0a168agzmbjzb58210cid026kaqp8ian17ffdi6k2d5c91q5zm15")))

(define-public crate-pswrd-0.1.0 (c (n "pswrd") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "rpassword") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1r0xq1av1g9nz6lbcc6yy3d017npjc9ya35nsjcgs3l6xb9krwhc")))

(define-public crate-pswrd-0.1.1 (c (n "pswrd") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "rpassword") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "03c54cln26ywzmqsknhv63mfzvpc2766qd3c1va07y3kz6pfdzfi")))

(define-public crate-pswrd-1.0.0 (c (n "pswrd") (v "1.0.0") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1i6yarb5xpfkc19adbg37s36icjiqdny9hjw4g0cvq0cdm3kc8i3")))

