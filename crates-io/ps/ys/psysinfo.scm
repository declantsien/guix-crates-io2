(define-module (crates-io ps ys psysinfo) #:use-module (crates-io))

(define-public crate-psysinfo-0.1.0 (c (n "psysinfo") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15.1") (d #t) (k 0)))) (h "0fpf17f1lkpnvq84hj1fy1xhfx5r9i9dalhrh3r7pdhp45zydj7r")))

(define-public crate-psysinfo-0.1.1 (c (n "psysinfo") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15.1") (d #t) (k 0)))) (h "0srvzdvsvqrzgd894hhlglphylmn03262w9dxl965h2jbgbcdizy")))

(define-public crate-psysinfo-0.1.2 (c (n "psysinfo") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15.1") (d #t) (k 0)))) (h "183im6vz527ykkjvmp5piwjmp9vn6fmny2ywqmngf0x3x93ggsy6")))

(define-public crate-psysinfo-0.1.3 (c (n "psysinfo") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.15.1") (d #t) (k 0)))) (h "045a2kxnmzwxsa219ja0x4p96za39a4x1d3mvvqkvlhjm7hn0pgb")))

