(define-module (crates-io ps pr psprompt) #:use-module (crates-io))

(define-public crate-psprompt-0.1.2 (c (n "psprompt") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.9") (d #t) (k 0)))) (h "1rdb2r5klfirl92bspxiag2m60hj8gs09my1vz2lamd3kw03az7h")))

