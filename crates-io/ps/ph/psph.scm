(define-module (crates-io ps ph psph) #:use-module (crates-io))

(define-public crate-psph-0.0.1 (c (n "psph") (v "0.0.1") (d (list (d (n "cmn") (r "^0.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "dtt") (r "^0.0.1") (d #t) (k 0)) (d (n "hsh") (r "^0.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "vrd") (r "^0.0.1") (d #t) (k 0)))) (h "1mwznf4x8l9nq2b46ihxijr892y36nklqfv2d53q76ahqni0yr17") (f (quote (("default")))) (r "1.66.1")))

