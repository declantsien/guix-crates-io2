(define-module (crates-io ps i- psi-lang) #:use-module (crates-io))

(define-public crate-psi-lang-0.3.0 (c (n "psi-lang") (v "0.3.0") (d (list (d (n "mimalloc") (r "^0.1.24") (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_consume") (r "^1.0.6") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.6") (d #t) (k 0)))) (h "1dc6grqiz24mfp5jvbm6lna12q11f8b8ll4qq2llb2izz0rxyns2")))

