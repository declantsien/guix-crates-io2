(define-module (crates-io ps i- psi-core) #:use-module (crates-io))

(define-public crate-psi-core-0.0.0 (c (n "psi-core") (v "0.0.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1n8sz3zzmzl0xkrxy2lbgldw7mz1cqkmyykpaj75a6sjpcyr2ymp") (f (quote (("default"))))))

