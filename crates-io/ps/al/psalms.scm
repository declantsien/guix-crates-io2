(define-module (crates-io ps al psalms) #:use-module (crates-io))

(define-public crate-psalms-0.0.1 (c (n "psalms") (v "0.0.1") (h "1cbgwyyghkiv7wgizfpzdfk1p0cj8z58rpgn44rhiw624gk451v7")))

(define-public crate-psalms-0.0.2 (c (n "psalms") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "dashmap") (r "^3.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml-loader") (r "^0.1.1") (d #t) (k 0)))) (h "1x4g01n7g1gha0ayhfzvvh2s46gqy9jc32fanx723q2paj6bqi21")))

(define-public crate-psalms-0.0.3 (c (n "psalms") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "dashmap") (r "^3.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "014raf5317r21gfvp46iqp055jzc539laifbj8wh9xjh3adrdyb5")))

(define-public crate-psalms-0.0.4 (c (n "psalms") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1xlaca8r9cnv1k4qy5hjiaa563m295b1ba9wy4lwmdaqrkidfl27")))

(define-public crate-psalms-0.0.5 (c (n "psalms") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "01y6bywcyafz1kj1q74fjmk6lqblcprk3b0mbin4vxx8h592h1h0")))

(define-public crate-psalms-0.0.6 (c (n "psalms") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1vmca6567prq29a5fqb4nnr5a4srqylnxzh2d4nsfwhhn6qajwh2")))

(define-public crate-psalms-0.0.7 (c (n "psalms") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)))) (h "0wjrmsw22ah1avm2yrc9sywn3nrpdksns6sbj08y1f1fnwrg51rd")))

