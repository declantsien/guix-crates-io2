(define-module (crates-io ps al psalm) #:use-module (crates-io))

(define-public crate-psalm-0.0.1 (c (n "psalm") (v "0.0.1") (h "15jbm4wza46b46xshfpjry2i7zsl5z4a04j0ymip6qmdcyasw3m7")))

(define-public crate-psalm-0.0.2 (c (n "psalm") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "15g7j4frvjx3agw0dbldm8j06q46alxyxxfsyvriwsx064wx1b6v")))

(define-public crate-psalm-0.0.3 (c (n "psalm") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "1hikibhibag4vjrdrv2n3jv3xy1w02p4939i9g9bkgq33jl08xrx")))

(define-public crate-psalm-0.0.4 (c (n "psalm") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "0v7vkc3x5fbjjdiflicv0agpfslyynpx3p9jbi661l2r1vv4dy3g")))

(define-public crate-psalm-0.0.5 (c (n "psalm") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "0209c9616b0si78xdq93f9q8pf6nbgvlazl46a5flpnaprzfibnq")))

(define-public crate-psalm-0.1.0 (c (n "psalm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)))) (h "0q9xfnk2brdyqgx992shrzfh0wia0xqlr8zc387ddlbgfa7jhpja")))

(define-public crate-psalm-0.1.1 (c (n "psalm") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pager") (r "^0.15.0") (d #t) (k 0)))) (h "1mzv2sgsxlyydasfcg5lrr6jv750jkjv0kbhdv6w39msjx0zisr1")))

