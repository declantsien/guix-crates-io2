(define-module (crates-io ps ap psapi-sys) #:use-module (crates-io))

(define-public crate-psapi-sys-0.0.1 (c (n "psapi-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0babgpplhgbzlb1di3rxd6mlq4nbcs7isglgickb7w0nz7cabjzc")))

(define-public crate-psapi-sys-0.1.0 (c (n "psapi-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "0y14g8qshsfnmb7nk2gs1rpbrs1wrggajmzp4yby4q6k0wd5vkdb")))

(define-public crate-psapi-sys-0.1.1 (c (n "psapi-sys") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1c0bnkngrhghgffn3mqssasrcw4420gw5sx8fxq2jpy28bhwfw8z")))

