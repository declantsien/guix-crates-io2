(define-module (crates-io ps co pscontroller-rs) #:use-module (crates-io))

(define-public crate-pscontroller-rs-0.1.0 (c (n "pscontroller-rs") (v "0.1.0") (d (list (d (n "bit_reverse") (r "^0.1.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "1sf054mwwxrdf1rljf8k63cdck353713akp24x0q7pdqfwn827sr")))

(define-public crate-pscontroller-rs-0.4.0 (c (n "pscontroller-rs") (v "0.4.0") (d (list (d (n "bit_reverse") (r "^0.1.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "0z5iyr3rbzl2sv02fdv011y3kwhjfj63cbxcca712il0y7v2s58m")))

(define-public crate-pscontroller-rs-0.5.0 (c (n "pscontroller-rs") (v "0.5.0") (d (list (d (n "bit_reverse") (r "^0.1.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "0dwqb7fyagacc4qgnhzl8x26cd0vv02vfc93bxqpb70pwdnr41kg")))

(define-public crate-pscontroller-rs-0.6.0 (c (n "pscontroller-rs") (v "0.6.0") (d (list (d (n "bit_reverse") (r "^0.1.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "00j7qamiylv919jmn8j7jcp2w8pgssn1smaaraljwmd3wmpbxba4")))

