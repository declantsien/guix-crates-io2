(define-module (crates-io ps he pshell) #:use-module (crates-io))

(define-public crate-pshell-1.0.0 (c (n "pshell") (v "1.0.0") (d (list (d (n "sysinfo") (r "^0.23.9") (d #t) (k 0)))) (h "1dzpv648qggv6im5w7x2vyc82xs306wzfghbw8xw5qvbfbml5kwq") (r "1.58")))

(define-public crate-pshell-1.0.1 (c (n "pshell") (v "1.0.1") (d (list (d (n "sysinfo") (r "^0.23.9") (d #t) (k 0)))) (h "0cngzyhr96xp7xnfgd1rdsb2zcrjn46iiks9hx7svadzr510y1n6") (r "1.58")))

(define-public crate-pshell-1.0.3 (c (n "pshell") (v "1.0.3") (d (list (d (n "sysinfo") (r "^0.24.2") (d #t) (k 0)))) (h "1zwggrd3751qkjmxkn34s1vnf1sj6crdp5mjj5pv80fzs4b33cjd") (r "1.58")))

(define-public crate-pshell-1.0.4 (c (n "pshell") (v "1.0.4") (d (list (d (n "sysinfo") (r "^0.25.1") (d #t) (k 0)))) (h "0sg6kqzf50xd9b7bhb5r8r6h9x6iw2k977vpj43l76lbbblw4pz1") (r "1.58")))

(define-public crate-pshell-1.0.5 (c (n "pshell") (v "1.0.5") (d (list (d (n "sysinfo") (r "^0.26.2") (d #t) (k 0)))) (h "07rd0vc57az431q9jq6dd4kr2rww1wknf2zjqnmwqfgz2d94djv2") (r "1.58")))

(define-public crate-pshell-1.0.7 (c (n "pshell") (v "1.0.7") (d (list (d (n "sysinfo") (r "^0.27.1") (d #t) (k 0)))) (h "152gf9y5rl7dv6qb53siwpjv7397wxc8yb2wrsgp77isqaj3lyfs") (r "1.58")))

(define-public crate-pshell-1.0.8 (c (n "pshell") (v "1.0.8") (d (list (d (n "sysinfo") (r "^0.27.1") (d #t) (k 0)))) (h "1rqa4sjbciq9k3vsdgcy1ymzv031wnj1l24kr47r53rkmvfg3yh9") (r "1.58")))

(define-public crate-pshell-1.0.9 (c (n "pshell") (v "1.0.9") (d (list (d (n "sysinfo") (r "^0.28.0") (d #t) (k 0)))) (h "00gjxvxsj0byz7kqmjdvg5r290kqdykrpab79w25g88cv87i3fwc") (r "1.58")))

(define-public crate-pshell-1.0.10 (c (n "pshell") (v "1.0.10") (d (list (d (n "sysinfo") (r "^0.29.0") (d #t) (k 0)))) (h "0f2v3dd84i8ardym76qm0gw07mz94h41y9aaf393qgm21cxkblr3") (r "1.58")))

(define-public crate-pshell-1.0.11 (c (n "pshell") (v "1.0.11") (d (list (d (n "sysinfo") (r "^0.30") (d #t) (k 0)))) (h "18lhcj43fjlz395wgg4hng8ddnyrvlx0x5d40xqf98w8g8nhrjak") (r "1.58")))

(define-public crate-pshell-1.0.12 (c (n "pshell") (v "1.0.12") (d (list (d (n "sysinfo") (r "^0.30") (d #t) (k 0)))) (h "1yb4s6jcjj74knngiv0p0h79x3cg3qjszdrvwik19whr66qmfc8g") (r "1.58")))

