(define-module (crates-io ps y- psy-macros) #:use-module (crates-io))

(define-public crate-psy-macros-1.0.0 (c (n "psy-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0xjzza0jivwkib986h4xqpipjym760di5vm8hm08q2m4xh47x886")))

