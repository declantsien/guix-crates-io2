(define-module (crates-io ps y- psy-math) #:use-module (crates-io))

(define-public crate-psy-math-1.0.0 (c (n "psy-math") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0cpymphg8gpyq5zwhx776xvi91zyr3w37llw48ava9vgmlhvxbsn")))

(define-public crate-psy-math-1.0.1 (c (n "psy-math") (v "1.0.1") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0dxa38vn6dgf9xn1vrp9d9wyaxydsgrc03f0cmikb03r77a4xxq1")))

(define-public crate-psy-math-1.0.2 (c (n "psy-math") (v "1.0.2") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "1c8xlhkzs1gyhmczgy3jq20w7zjmagp2a4kphfm2xx3vi94kwhxz")))

