(define-module (crates-io ps -h ps-hash) #:use-module (crates-io))

(define-public crate-ps-hash-0.1.0-1 (c (n "ps-hash") (v "0.1.0-1") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "ps-base64") (r "^0.1.0-1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0mcc4ywjy1yw6snp5nq7hpyg08js6lv37c7nph6d9ivcywkxzg08")))

(define-public crate-ps-hash-0.1.0-2 (c (n "ps-hash") (v "0.1.0-2") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "ps-base64") (r "^0.1.0-1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0qv6k5y2wizn28g31kk09f95iyzc65nq4ya4i7vjkyljfv53yqmm")))

(define-public crate-ps-hash-0.1.0-3 (c (n "ps-hash") (v "0.1.0-3") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "ps-base64") (r "^0.1.0-1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1p8agbfkm0lskc3apmj2633f9q918zsh8mfdwyvyqlzwd74zvy2n")))

