(define-module (crates-io ps mo psmond) #:use-module (crates-io))

(define-public crate-psmond-0.1.0 (c (n "psmond") (v "0.1.0") (d (list (d (n "daemonize") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "0581sfdmxam1s8dj0cls830xhs82bzqm9732dyycmgnrznixw54l")))

