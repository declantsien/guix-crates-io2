(define-module (crates-io ps #{2-}# ps2-mouse) #:use-module (crates-io))

(define-public crate-ps2-mouse-0.1.0 (c (n "ps2-mouse") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "1c69yh258yg5slridkql86d1g2yd4sgwpaql5jpcyi18syq2f8ry")))

(define-public crate-ps2-mouse-0.1.1 (c (n "ps2-mouse") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "03mac6lqp456435nmmzk6mlhk15b7j0ddyv5iix5gjkfizgnvy7d")))

(define-public crate-ps2-mouse-0.1.2 (c (n "ps2-mouse") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.11.0") (d #t) (k 0)))) (h "1akn2izfw7z2b8pnngrnn8bwdq7b63hjbhafimzimlg3gmvsl39l")))

(define-public crate-ps2-mouse-0.1.3 (c (n "ps2-mouse") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.12.2") (d #t) (k 0)))) (h "01p54fm307qzxiyr0xkwn53pssfbsx4dyy1ca4jigbb1z5s6y8j5")))

(define-public crate-ps2-mouse-0.1.4 (c (n "ps2-mouse") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.2") (d #t) (k 0)))) (h "18043vg2nni9kh8adkck7bw8lsr8s2linpbnbhai3m3pz9ifm8gp")))

