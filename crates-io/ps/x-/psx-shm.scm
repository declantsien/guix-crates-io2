(define-module (crates-io ps x- psx-shm) #:use-module (crates-io))

(define-public crate-psx-shm-0.1.0 (c (n "psx-shm") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xp3zmfcpcy5jmbkqq4g70yfz994jzsihr7slzfcjf2i4pjp8b28")))

