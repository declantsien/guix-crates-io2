(define-module (crates-io ps -c ps-cypher) #:use-module (crates-io))

(define-public crate-ps-cypher-0.1.0-1 (c (n "ps-cypher") (v "0.1.0-1") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "ps-base64") (r "^0.1.0-1") (d #t) (k 0)) (d (n "ps-deflate") (r "^0.1.0-3") (d #t) (k 0)) (d (n "ps-hash") (r "^0.1.0-3") (d #t) (k 0)))) (h "034sjla2akq8l1n3gh7ssr87h7sv7d22qji44c1ag9waci7ni1dm")))

(define-public crate-ps-cypher-0.1.0-2 (c (n "ps-cypher") (v "0.1.0-2") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "ps-base64") (r "^0.1.0-1") (d #t) (k 0)) (d (n "ps-deflate") (r "^0.1.0-3") (d #t) (k 0)) (d (n "ps-hash") (r "^0.1.0-3") (d #t) (k 0)))) (h "00zjxl12vl2p16rsh4pj5wysw2gspkvrg7riji8d2k4kibfr483x")))

