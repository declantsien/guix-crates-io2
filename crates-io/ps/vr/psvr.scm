(define-module (crates-io ps vr psvr) #:use-module (crates-io))

(define-public crate-psvr-0.1.0 (c (n "psvr") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "0f3rlqqndj2f36qm3kq2zyqxhy1grbigcwj9hj9wlwxz4jsdkn2x")))

(define-public crate-psvr-0.1.1 (c (n "psvr") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "10g0gj0rvlz3adrfhxc0mhsf8vih5hz6q7d2bkjpqirh3blgrd3l")))

(define-public crate-psvr-0.3.0 (c (n "psvr") (v "0.3.0") (d (list (d (n "ahrs") (r "^0.3") (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "delta") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hidapi") (r "^1.2") (d #t) (k 0)) (d (n "hmdee_core") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)))) (h "146nfhx5s5sc0av8lcf5zb7143826r2il2anxqcwhh7fbccij5f0")))

