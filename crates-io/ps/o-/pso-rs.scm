(define-module (crates-io ps o- pso-rs) #:use-module (crates-io))

(define-public crate-pso-rs-0.1.0 (c (n "pso-rs") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "04ryjb9zg4csf3d992a0f53flsk3h6q3198w3kpxg94qrfv1zgdy")))

(define-public crate-pso-rs-0.2.0 (c (n "pso-rs") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0h5p7iqa8d3pm6zfsxsmll237jy7lj0wn9bh8v2d4mxl8hvs9nw0")))

(define-public crate-pso-rs-0.3.0 (c (n "pso-rs") (v "0.3.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "06fvjvv081fvw2r20faww8mmnn0833wy3mhgc55n8xsbccybij6x")))

(define-public crate-pso-rs-0.3.1 (c (n "pso-rs") (v "0.3.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1b0298sn2kwws7l31lk58q4432gwfyy1ak4w223y7zsafqghpd6x")))

(define-public crate-pso-rs-0.4.0 (c (n "pso-rs") (v "0.4.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "02rsq7yd0mkkaahz07p7rx3ipkmva962ablgsj27sqja203xpad0")))

(define-public crate-pso-rs-0.4.1 (c (n "pso-rs") (v "0.4.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0ba9sbjcv2m3xr50py0mwl9q0af4q8q85xxdyp9qswnhjzgfsxxq")))

(define-public crate-pso-rs-0.4.2 (c (n "pso-rs") (v "0.4.2") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "09hz7bdra0vpvbhwd22nj0k18b1rygrx271bvpv32jri2wiinl9b")))

(define-public crate-pso-rs-1.0.0 (c (n "pso-rs") (v "1.0.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1bm6gpvfmgn0728vxvbjxi4pb9pwkxj1c969mrzxpx0gagz6q1pv")))

(define-public crate-pso-rs-1.1.0 (c (n "pso-rs") (v "1.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1svsmbsls0802cphn2xx0aq1h3gp9xv5c7s99gfsn297fzq4yb6i")))

(define-public crate-pso-rs-1.2.0 (c (n "pso-rs") (v "1.2.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0yqlpallbam5fx97wjzl2kdnqfkdx13ggabkwjp17i1x7aaw73q5")))

(define-public crate-pso-rs-2.0.0 (c (n "pso-rs") (v "2.0.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "153i5y67kgvwwhymx416nhzpnxmda34i7kjx299phhlgdjwx8in7")))

(define-public crate-pso-rs-2.0.1 (c (n "pso-rs") (v "2.0.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0knpqy5v17rs8dsqyj9wkda44wk4j4482pkljl05yzij3sz18f3k")))

(define-public crate-pso-rs-2.0.2 (c (n "pso-rs") (v "2.0.2") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1aqbrpd1jzjxq789cr6i0rqmzqyxq42fps42c7l1qy9ngslry6ay")))

(define-public crate-pso-rs-2.1.2 (c (n "pso-rs") (v "2.1.2") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1yr79zqv21y5i0bnvlmll0ydis2ix794a3jw4nvn142qjqg79km5")))

(define-public crate-pso-rs-2.2.2 (c (n "pso-rs") (v "2.2.2") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "03gpl3mf69zr8f8y1i42rbc3j4n3wxqp27a4pc24cwxh1m19k1xj")))

(define-public crate-pso-rs-2.2.3 (c (n "pso-rs") (v "2.2.3") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0vrbsvfwzkkijsbvwkb7nxjsp6mnp04zv69176ci70vbipr5i6m8")))

(define-public crate-pso-rs-2.2.4 (c (n "pso-rs") (v "2.2.4") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0k6w93dlbj5rf6bq5vc8nv2yf7q4krgppniklrfhxiqkabw7ddka")))

