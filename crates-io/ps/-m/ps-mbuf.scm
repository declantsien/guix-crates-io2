(define-module (crates-io ps -m ps-mbuf) #:use-module (crates-io))

(define-public crate-ps-mbuf-0.1.0-1 (c (n "ps-mbuf") (v "0.1.0-1") (h "1qvp8giz5n9z8wm85zlbd4rpb0klblmrg52pbxb7389qr2vzlrrz")))

(define-public crate-ps-mbuf-0.1.0-2 (c (n "ps-mbuf") (v "0.1.0-2") (h "0cag7kz6shiqjgq23gpx9gaibha639p3ilbb838i9lcd6cdj8jay")))

