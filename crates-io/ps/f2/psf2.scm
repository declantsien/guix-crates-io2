(define-module (crates-io ps f2 psf2) #:use-module (crates-io))

(define-public crate-psf2-0.1.0 (c (n "psf2") (v "0.1.0") (h "0hc8fp9lx8a59zklzdfnxmi98v9i6xxymh3vfm8y5bz9s36h9b23")))

(define-public crate-psf2-0.1.1 (c (n "psf2") (v "0.1.1") (h "09r2zj30bs63krrcfkf986zq3wxiyilmnfy7s64p3vbz7h8j6sdi") (f (quote (("std") ("default" "std"))))))

(define-public crate-psf2-0.2.0 (c (n "psf2") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1vk14569mnklmbjg22s9gdpfz7xjhpfhfv5k4qzpxyj23b3yr880") (f (quote (("std") ("default" "std"))))))

(define-public crate-psf2-0.3.0 (c (n "psf2") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1ixpi0yyg02pmi0jr7afl60n7bqnvm4bz9vfar03gj2hmzlzy5vc") (f (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.3.1 (c (n "psf2") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0zcagrr4i7i2sfy57x1pcsym57p9af0r3aqgic80bwp4hsn1ib91") (f (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.3.2 (c (n "psf2") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1mssvf2mr1n6rfwrkp3bc95danr2xbwvqv0dxwrvlq9xcf5vx54l") (f (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.3.3 (c (n "psf2") (v "0.3.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0ls0hjvqx6swyfmb32cidsmcdzvgffmnf5ypw6y9pynh0296c6xs") (f (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.4.0 (c (n "psf2") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("inline-more"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0mslzwnyd0p9z784454pb4xmpm2djg5lj76nhvzi7haqxqr62vnv") (f (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

