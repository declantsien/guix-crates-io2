(define-module (crates-io ps eq pseqsid) #:use-module (crates-io))

(define-public crate-pseqsid-0.1.1 (c (n "pseqsid") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1nlgmflwrvx9zsjflpwnkz4fshc3mlvyiglny0knl9mppin2vczr")))

(define-public crate-pseqsid-0.2.1 (c (n "pseqsid") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0iq4bb0z7mhaybkhis4fc0q13jr5mhg44fjm8kf95mr6g1n2i9iw")))

(define-public crate-pseqsid-1.0.0 (c (n "pseqsid") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0pgzjk2cdk18gvppj5b6hkayn9f0k0ddhrl6dc49b8kmsnqd6h2d")))

(define-public crate-pseqsid-1.0.1 (c (n "pseqsid") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "01p0bnf9cxv66375cy7s3yb91b9ykabsvpr4y2d6d4sl7jqf4bs4")))

(define-public crate-pseqsid-1.0.2 (c (n "pseqsid") (v "1.0.2") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1xpchbnsgjnxhflbgsi22iwcacl1nlc5lk4r3hcjvxglqfghi2jh")))

