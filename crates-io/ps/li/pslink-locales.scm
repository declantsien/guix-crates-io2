(define-module (crates-io ps li pslink-locales) #:use-module (crates-io))

(define-public crate-pslink-locales-0.4.1-beta.2 (c (n "pslink-locales") (v "0.4.1-beta.2") (d (list (d (n "fluent") (r "^0.15") (d #t) (k 0)) (d (n "pslink-shared") (r "^0.4.1-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1inpq8drp1wvk0bczdyh03j9bgcw8hyg2x5w09kcpczfkvw7afln")))

