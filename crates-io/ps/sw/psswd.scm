(define-module (crates-io ps sw psswd) #:use-module (crates-io))

(define-public crate-psswd-0.1.3 (c (n "psswd") (v "0.1.3") (d (list (d (n "age") (r "^0.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "016q5lx0b6n62jiwn8ysvjbw996s4kcj8v452l7cq9yifw6l9s3x")))

(define-public crate-psswd-0.1.5 (c (n "psswd") (v "0.1.5") (d (list (d (n "age") (r "^0.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1z1xsyn81y8fkxj77nzg7hz2ir302gkafvmbi4s05brslfc3brq2")))

