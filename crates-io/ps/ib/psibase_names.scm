(define-module (crates-io ps ib psibase_names) #:use-module (crates-io))

(define-public crate-psibase_names-0.0.0 (c (n "psibase_names") (v "0.0.0") (h "0zcihpjbqi0xdw9mcqbymwm4jqzxwjpqlf6bx015h0b77jv3b53w")))

(define-public crate-psibase_names-0.1.0 (c (n "psibase_names") (v "0.1.0") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "1d9w58jw1q6za1j1a8ja01b2flnj6zcwf9xn3nw4r0f2r0qaxaar") (r "1.64")))

(define-public crate-psibase_names-0.1.1 (c (n "psibase_names") (v "0.1.1") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "0dcg88fpwvyhn5nhyfpr1vajrgg7m8m18xxh2lx659rqj4sx3ji8") (r "1.64")))

(define-public crate-psibase_names-0.1.2 (c (n "psibase_names") (v "0.1.2") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "0ml7d127jmhrz9ilmi3frgqp14bqx5kqzn77hsdc855z0r6b1qly") (r "1.64")))

(define-public crate-psibase_names-0.1.3 (c (n "psibase_names") (v "0.1.3") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "1kmr5sh5bn9xnf4nz2r2skr8md5xval836s28xcg3m4qq2ksyzc5") (r "1.64")))

(define-public crate-psibase_names-0.1.4 (c (n "psibase_names") (v "0.1.4") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "10inni4v17knpkbqqisi9pazc7yb43v7jwjbdmx73g66wsqspm6j") (r "1.64")))

(define-public crate-psibase_names-0.1.5 (c (n "psibase_names") (v "0.1.5") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "0lwz833v15p2hxx4k2l3vdfcmdnpnayf2ilnq64kzbanzvd90269") (r "1.64")))

(define-public crate-psibase_names-0.2.0 (c (n "psibase_names") (v "0.2.0") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "19zzllb1bl09fy70ysrdx1kbvx4lx253m1fydpzb6i68bxai496i") (r "1.64")))

(define-public crate-psibase_names-0.3.0 (c (n "psibase_names") (v "0.3.0") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "186i2n1xfqchysyyy74gf3w4bk6b5xdq4spmlb0fdq6jax88gihb") (r "1.64")))

(define-public crate-psibase_names-0.6.0 (c (n "psibase_names") (v "0.6.0") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "0zf7bf5mb824mximdp0ad2cdgdnwv33sakcgjbcmwmhb486bxa3v") (r "1.64")))

(define-public crate-psibase_names-0.8.0 (c (n "psibase_names") (v "0.8.0") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "1fnscsgyrqqhf1d25hnk158pn8k9lwfgylsngg9hrybc78vfy2i4") (r "1.64")))

(define-public crate-psibase_names-0.9.0 (c (n "psibase_names") (v "0.9.0") (d (list (d (n "seahash") (r "^2.0.0") (d #t) (k 0)))) (h "0jfcczyf94slgq00kg42a8ys7qdq1mxma8fxs1rlkhyxbxbvl8px") (r "1.64")))

