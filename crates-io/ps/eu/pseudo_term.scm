(define-module (crates-io ps eu pseudo_term) #:use-module (crates-io))

(define-public crate-pseudo_term-1.0.0 (c (n "pseudo_term") (v "1.0.0") (d (list (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "wgpu_text") (r "^0.8") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "1x1xiy4mcb9kk6l74zikw5i320ijmkqjnd4xnww30ck4fiahpgw5")))

(define-public crate-pseudo_term-2.0.0 (c (n "pseudo_term") (v "2.0.0") (d (list (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "wgpu_text") (r "^0.8") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0zpg05fz1s4z7dy9shfwsqrl8iakgn30d7zym1y9p8szxx10nl1l")))

(define-public crate-pseudo_term-3.0.0 (c (n "pseudo_term") (v "3.0.0") (d (list (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "wgpu_text") (r "^0.8") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0sxjj50z5gvf055dminnalkbvmslsm33cy5vm80w1n44fmydk778")))

