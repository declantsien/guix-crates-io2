(define-module (crates-io ps eu pseudo_interpreter) #:use-module (crates-io))

(define-public crate-pseudo_interpreter-1.0.0 (c (n "pseudo_interpreter") (v "1.0.0") (h "02dlfg7zgfmirlhp9mxlz0x1551x32nlhnl7iwqy3h7xp62ycwlg")))

(define-public crate-pseudo_interpreter-1.0.1 (c (n "pseudo_interpreter") (v "1.0.1") (h "0iwwdjxnb1n18k0zqqbmwn99n61cd6wzvc0fxgrdigf9zfsjkm78")))

(define-public crate-pseudo_interpreter-1.1.1 (c (n "pseudo_interpreter") (v "1.1.1") (h "0y4fyl6j6w3k4bdqbq067a4gjvzc73i2f46z3hxcbml2g70621k5")))

(define-public crate-pseudo_interpreter-1.1.2 (c (n "pseudo_interpreter") (v "1.1.2") (h "1p5hal9w8yhw6y9yd9lp402xy1r5h02pv7i15mphw5fcd36b95hw")))

(define-public crate-pseudo_interpreter-1.1.4 (c (n "pseudo_interpreter") (v "1.1.4") (h "0iyrq7z4xhvxyaq0ad2limzlm8p99f39y435alj2a9qwlip4dkj7")))

(define-public crate-pseudo_interpreter-1.1.5 (c (n "pseudo_interpreter") (v "1.1.5") (h "03kcwh5h07j90lbkhjdpk7z4zmdaxsxi7pgzk8237wjfpqis5s98")))

(define-public crate-pseudo_interpreter-1.1.6 (c (n "pseudo_interpreter") (v "1.1.6") (h "1wnq5nvd1c6pwwgzh9ak183hdz6dwq4kz3xrdxx0mvk64v39wvzi")))

(define-public crate-pseudo_interpreter-1.1.7 (c (n "pseudo_interpreter") (v "1.1.7") (h "16ajn54dcj7xa318q82gd415aqxydmik5ybwpw5afn6zmyz8yx3y")))

(define-public crate-pseudo_interpreter-1.2.0 (c (n "pseudo_interpreter") (v "1.2.0") (h "1xp3b90wm01jdbmg1vlxxm304n3gxbr2ndripiaqac46yvb0gnp5")))

