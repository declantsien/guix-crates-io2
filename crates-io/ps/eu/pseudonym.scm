(define-module (crates-io ps eu pseudonym) #:use-module (crates-io))

(define-public crate-pseudonym-0.1.0 (c (n "pseudonym") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0aygcsm77lg3m8f0zv5avaz0g9ry0mc5ij7gl5awbmg4bhyyvclh")))

(define-public crate-pseudonym-0.2.0 (c (n "pseudonym") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1nlhmgkwvmici8j1svgr34sikgbblsnp3l3dj0gsj4r5ci8nvfsk")))

(define-public crate-pseudonym-0.2.1 (c (n "pseudonym") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1rgsxhibwk32j828b0wqz78bzbwy42dw9grl5wip9dg1fxcyvsrk")))

(define-public crate-pseudonym-0.2.2 (c (n "pseudonym") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "101bgymc9x01isvwg8pf2k3a2r2fkn2c6142f8h4hsviwfyljyfj")))

(define-public crate-pseudonym-0.2.3 (c (n "pseudonym") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "0m2wnjrr0gyaa5m5yg1sj955p2j2x2mz0pz8y2c49yslw38ax7rj")))

