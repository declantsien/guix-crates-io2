(define-module (crates-io ps eu pseudocode) #:use-module (crates-io))

(define-public crate-pseudocode-0.1.0 (c (n "pseudocode") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1rmz0d81v2y0c0h222jaqc4mwrrdixn8dq1di6fryk4s1nibf566")))

(define-public crate-pseudocode-0.1.1 (c (n "pseudocode") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "08917c47b2hq2c34s80mpzdcjwlv15rg8il9d90xib66yk1yv5a2")))

(define-public crate-pseudocode-0.1.2 (c (n "pseudocode") (v "0.1.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0zr6fy3sinazj0pj2a67kq0q871n4j8ikpjx5h6xpnl3fyr108q1")))

(define-public crate-pseudocode-0.1.3 (c (n "pseudocode") (v "0.1.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1ivr8rqchwqrrfwv2r0adk7cp3kvrgx9q5i3dqmar0kjm7c10cc9")))

(define-public crate-pseudocode-0.1.4 (c (n "pseudocode") (v "0.1.4") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "06s07g9himyj8q9yzx6shhasr7s210g38g9wq2mgks4091m8nx5r")))

(define-public crate-pseudocode-0.1.5 (c (n "pseudocode") (v "0.1.5") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "00pb52a5bzdl5w11yxrx9bmx4w0x9zs5fz9c90i32v0vindx7c3q")))

(define-public crate-pseudocode-0.1.6 (c (n "pseudocode") (v "0.1.6") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "10xdi4j9250r4gni0vcgfy8zpdvgdrsdrwayrq09bka9vixn4xp8")))

(define-public crate-pseudocode-0.1.7 (c (n "pseudocode") (v "0.1.7") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1d634r8v6ir1c1k31kapv7dgql06zc69mbpl6ksyv93a9fg28l2p")))

(define-public crate-pseudocode-0.1.8 (c (n "pseudocode") (v "0.1.8") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0b2ci88bk19z4z9ksmc7j8zayrj42v0ig34q57rhmcf16w9vdrwv")))

