(define-module (crates-io ps eu pseudotex) #:use-module (crates-io))

(define-public crate-pseudotex-1.0.0 (c (n "pseudotex") (v "1.0.0") (d (list (d (n "clap") (r ">=2.33.0, <2.34.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r ">=0.19.0, <0.20.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r ">=0.19.0, <0.20.0") (d #t) (k 0)) (d (n "regex") (r ">=0.2.1, <0.3.0") (d #t) (k 0)))) (h "1habq9ylrjf774kksgzwsy0d3q3v0ij54rbripdhszga7qas75z9") (y #t)))

(define-public crate-pseudotex-1.0.1 (c (n "pseudotex") (v "1.0.1") (d (list (d (n "clap") (r ">=2.33.0, <2.34.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r ">=0.19.0, <0.20.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r ">=0.19.0, <0.20.0") (d #t) (k 0)) (d (n "regex") (r ">=0.2.1, <0.3.0") (d #t) (k 0)))) (h "1kpcz13jw64lbsqkhr2vrg85lk3sdrnp83kl7jkzz42nxcq24znv")))

(define-public crate-pseudotex-1.0.2 (c (n "pseudotex") (v "1.0.2") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0j07qfd1jfcjkdsa4qf2w8jxwsr8pp2ydfkihc68zxzm72zk8g2i")))

(define-public crate-pseudotex-1.1.0 (c (n "pseudotex") (v "1.1.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0pgrhpg8r44z4bnxjhq23kzjh5779hc8mxvi6zwsps99yirxqb0h")))

(define-public crate-pseudotex-1.1.1 (c (n "pseudotex") (v "1.1.1") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0jiygrcbnxqjsb812249j2snmnhfzwyvxj2ijjpjzhlsdq7d4jqq")))

(define-public crate-pseudotex-1.1.2 (c (n "pseudotex") (v "1.1.2") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1lbacf5xv7ckc4yp7wq762j9ap7yv58glvwmb8zi4d6wwp1zl2q3")))

(define-public crate-pseudotex-1.1.3 (c (n "pseudotex") (v "1.1.3") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0m58xgyiyip8yrq6mzb2f3v53wmf6b5wqiswhn1j34jsd6hcpckl")))

(define-public crate-pseudotex-1.1.4 (c (n "pseudotex") (v "1.1.4") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1qzcqxxpb15i08kc5hdna0nna7f1iwp4mqyrjkm4sqy9m9v4kdii")))

(define-public crate-pseudotex-1.1.5 (c (n "pseudotex") (v "1.1.5") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1vy3hhxil5f50li15bgs3bgnbdil7yh2pikz06lg9cxlgb35yhns")))

