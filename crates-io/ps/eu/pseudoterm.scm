(define-module (crates-io ps eu pseudoterm) #:use-module (crates-io))

(define-public crate-pseudoterm-0.1.0 (c (n "pseudoterm") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1.40") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "14vy4nah90xprgdnw2by5kn3qamjyfqbk724v7wji87af5rgzp06")))

(define-public crate-pseudoterm-0.1.1 (c (n "pseudoterm") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1.40") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "08akglkfrgjqp91msgvcywckbg9da4qy9b3lyywjc9jbjpn60qxq")))

