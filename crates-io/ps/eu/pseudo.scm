(define-module (crates-io ps eu pseudo) #:use-module (crates-io))

(define-public crate-pseudo-0.0.1 (c (n "pseudo") (v "0.0.1") (h "0xba4bahgl3fda5f0jhrhkyp232yr70dkn5cwpzyv83lwxznbh49")))

(define-public crate-pseudo-0.0.2 (c (n "pseudo") (v "0.0.2") (h "0cs36jb54vfh5sqlmw86rzn8rbr932b0xffcj6d1kjkah4f45bh9")))

(define-public crate-pseudo-0.0.3 (c (n "pseudo") (v "0.0.3") (h "0yr7pv28m2cam858rkikby7isqsq8h0cvy09hqn9dylnxdh7d1k3")))

(define-public crate-pseudo-0.1.0 (c (n "pseudo") (v "0.1.0") (h "195bi9bknjlcn7xwm260g29zvjgyrv1a10z62jrypd0pxyy3czz8")))

(define-public crate-pseudo-0.2.0 (c (n "pseudo") (v "0.2.0") (h "16wfz4ap8myxaaswjb9j5vdlbh2yp5s85qms3pjmyv42yfcr15d8")))

