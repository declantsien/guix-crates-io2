(define-module (crates-io ps eu pseudolocalize) #:use-module (crates-io))

(define-public crate-pseudolocalize-0.2.0 (c (n "pseudolocalize") (v "0.2.0") (h "1s2ajqvbdgmk7khhv9zs1z9jkdn8sr841s8j49b6nrx01sl3bqc3")))

(define-public crate-pseudolocalize-0.2.1 (c (n "pseudolocalize") (v "0.2.1") (h "17n6023dv8f41vhjf79gy8y8c3gq1gsy8wkd0wfzkzmsl8vwrp5n")))

(define-public crate-pseudolocalize-0.2.2 (c (n "pseudolocalize") (v "0.2.2") (h "07gimp10ck9vrnjahiqggkivrlxa0ln9gpxdsv6sczpck5ghfivn")))

(define-public crate-pseudolocalize-0.3.0 (c (n "pseudolocalize") (v "0.3.0") (h "1pazypxcmpsgixszg58wk4icqsbbns2332nrizz81dgnzh5yyrv7")))

(define-public crate-pseudolocalize-0.3.1 (c (n "pseudolocalize") (v "0.3.1") (h "0jyxy1ikd9wiww9f25pvng4gfhhylxw2clfsnh15h4sp15l9qcsb")))

