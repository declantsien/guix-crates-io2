(define-module (crates-io ps eu pseudotcp) #:use-module (crates-io))

(define-public crate-pseudotcp-0.0.1 (c (n "pseudotcp") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1mi900pi4l9ycpqfh03rg3sd6cgd0avwv54xcwahjs8agagpjn6w")))

(define-public crate-pseudotcp-0.1.0 (c (n "pseudotcp") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "09z8gmbxjnqjf3m6wb65jlwnic7n9dcvjif9bypg3ijlfx53b4vr")))

(define-public crate-pseudotcp-0.1.1 (c (n "pseudotcp") (v "0.1.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "condition_variable") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0106s148ybwzf4xqq85mxqd35ip3414hpljd6j0jv6p13hxgnawg")))

(define-public crate-pseudotcp-0.1.2 (c (n "pseudotcp") (v "0.1.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "condition_variable") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1vimgqx44i2z0cirn5kq6sigkr7ldvp3sdrm0gc2g2jv3r3hjkk6")))

(define-public crate-pseudotcp-0.1.3 (c (n "pseudotcp") (v "0.1.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "condition_variable") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0jlv8xc45gkllh5aaklaqgfmhs98rsfaa5fshdickhq5jpxl1j1f")))

(define-public crate-pseudotcp-0.2.0 (c (n "pseudotcp") (v "0.2.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "condition_variable") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "01nsy01mrh45261chls0xq34shv1haka088f0ijiq6d5b8q9x0qi")))

