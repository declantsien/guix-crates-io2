(define-module (crates-io ps ar psarc-lib) #:use-module (crates-io))

(define-public crate-PSArc-lib-0.0.0 (c (n "PSArc-lib") (v "0.0.0") (h "0c7gj9xv3sk9qrd12h4ybszdf13wc6x67f55swkla9bp5mn13r6y")))

(define-public crate-PSArc-lib-0.0.1 (c (n "PSArc-lib") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (f (quote ("rust_backend"))) (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17ayg1gvf5ni9xfhg5c4iyi78vjgiafwlsij4bsl5i59h5i4ky77") (s 2) (e (quote (("serde" "dep:serde"))))))

