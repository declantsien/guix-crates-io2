(define-module (crates-io ps -d ps-datachunk) #:use-module (crates-io))

(define-public crate-ps-datachunk-0.1.0-1 (c (n "ps-datachunk") (v "0.1.0-1") (d (list (d (n "ps-cypher") (r "^0.1.0-2") (d #t) (k 0)) (d (n "ps-hash") (r "^0.1.0-3") (d #t) (k 0)) (d (n "ps-mbuf") (r "^0.1.0-2") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.44") (f (quote ("validation"))) (d #t) (k 0)))) (h "04nsv41rrhzl9pah69rzyn60qbprv8dbhli3hzn4mj9ki3v5fyh8")))

