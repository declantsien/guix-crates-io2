(define-module (crates-io ps -d ps-driver-deserializer) #:use-module (crates-io))

(define-public crate-ps-driver-deserializer-0.1.0 (c (n "ps-driver-deserializer") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r4k7l6dicnwj962cnvml8vhnp15rsz2lpv9qdp77sr5jz903phr")))

(define-public crate-ps-driver-deserializer-0.1.1 (c (n "ps-driver-deserializer") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ddfk0bvw7p89ms57xd82zjbl7psp19h4dyfvd0wl849lwzqayld")))

(define-public crate-ps-driver-deserializer-0.1.2 (c (n "ps-driver-deserializer") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sjqfaic0yjifcmivzkhk8lsl1d93q6p787pwmwbl9i3nh47bwgv")))

(define-public crate-ps-driver-deserializer-0.1.3 (c (n "ps-driver-deserializer") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iq9gmr7mchs99dcc4j093zb9q7rppi87wckh2hirqyh0xwj0chg")))

(define-public crate-ps-driver-deserializer-0.1.4 (c (n "ps-driver-deserializer") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rg487bzl75hklf76lhdg34wgxynvg3vv0sjmrcx59991bjh2nwm")))

