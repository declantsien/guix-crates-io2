(define-module (crates-io ps -d ps-deflate) #:use-module (crates-io))

(define-public crate-ps-deflate-0.1.0-1 (c (n "ps-deflate") (v "0.1.0-1") (d (list (d (n "libdeflater") (r "^1.20.0") (d #t) (k 0)))) (h "0llq1j29w2yyqp6vmh49idhmqn0al6hrv2kbaqqj9zmi3zvxhrcv")))

(define-public crate-ps-deflate-0.1.0-2 (c (n "ps-deflate") (v "0.1.0-2") (d (list (d (n "libdeflater") (r "^1.20.0") (d #t) (k 0)))) (h "1yysknv5sviw3jg60zdfisz3h4xasingw64925yggn7w41dwvjb2")))

(define-public crate-ps-deflate-0.1.0-3 (c (n "ps-deflate") (v "0.1.0-3") (d (list (d (n "libdeflater") (r "^1.20.0") (d #t) (k 0)))) (h "0nc3wrpj0r24kakaj71r69lxgrzyww2qyx4ybf450a4iyghj90hi")))

