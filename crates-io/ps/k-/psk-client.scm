(define-module (crates-io ps k- psk-client) #:use-module (crates-io))

(define-public crate-psk-client-0.1.0 (c (n "psk-client") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 0)))) (h "14kyfak742kq1pwxnjj2wh5jsykidwiqh4frhzg75xwf83p9pfqn") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.1 (c (n "psk-client") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "1dxnydv7g35dimraampcz2r374ics3qnxslhmqxw9in3gsyqvy16") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.2 (c (n "psk-client") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "0pdpsjz2r1zxafiimsbj75h65jlqvddr5frrv5k9j22agpdng7in") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.3 (c (n "psk-client") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "1027kwz0bn0nrbbzvq4s3vayfaykajsral4y3kyhamir34ss2x6r") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.4 (c (n "psk-client") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.19") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "1w7s9spgp97mx75ma2167wq0l9n60gs70800j9qs2dr9lqg7d97j") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.5 (c (n "psk-client") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "0b77805smz3m3n4pv1zi6vavjdpib62ls37fjn5xzd4zgiaigcis") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.6 (c (n "psk-client") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "162plpzqanxih8l7szyh7bz7zj1543mp6900gzl17qvbzzmv8q8v") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.7 (c (n "psk-client") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.23") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "1zbhjdvx8b4f00h9iphfsirx4wx87vi0wbz1x0wmh64qvwzjyb1a") (f (quote (("vendored-openssl" "openssl/vendored"))))))

(define-public crate-psk-client-0.1.8 (c (n "psk-client") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "openssl-errors") (r "^0.1.0") (d #t) (k 0)))) (h "0q2n5wcsyxj9ah949jn4xf1svjbxarwi63dwiimxapf4mb15v2is") (f (quote (("vendored-openssl" "openssl/vendored"))))))

