(define-module (crates-io ps in psina) #:use-module (crates-io))

(define-public crate-psina-0.1.0 (c (n "psina") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0ixkn9a18q2253d2yvr88lyk79vwdv047nf2bfi7iqk9mr7p9lq7") (y #t)))

(define-public crate-psina-0.1.3 (c (n "psina") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1dxskjvv6k9a6pj3n2zw5xwr7wg8ls56ki5zq91nsmnvw9cmc6dz") (y #t)))

(define-public crate-psina-0.1.5 (c (n "psina") (v "0.1.5") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0yfcs4s2kryha3lwm5c8jmygh15a61rjx1ha1s9dpzwpnxi3aa7p")))

(define-public crate-psina-0.1.7 (c (n "psina") (v "0.1.7") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0nyzygm2wcg9yysiwd60a381gjvp6x0sw2hf3qd253arawhqx6ji")))

