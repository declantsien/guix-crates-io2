(define-module (crates-io ps p3 psp34-full) #:use-module (crates-io))

(define-public crate-psp34-full-0.2.1 (c (n "psp34-full") (v "0.2.1") (d (list (d (n "ink") (r "^4.3") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.9") (f (quote ("derive"))) (o #t) (k 0)))) (h "1q65drgj2b2bqmggbcd0vn4b7dnwml34xfna66pgzjgnxk0k2hc3") (f (quote (("test-only") ("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("enumerable") ("default" "std" "contract" "enumerable") ("contract"))))))

