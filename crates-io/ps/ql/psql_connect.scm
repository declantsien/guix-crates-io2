(define-module (crates-io ps ql psql_connect) #:use-module (crates-io))

(define-public crate-psql_connect-0.1.0 (c (n "psql_connect") (v "0.1.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "error-chain") (r "~0.11.0") (d #t) (k 0)))) (h "10y5kjsxbkp3fkvkpkxabdc7aw7pbybd2a3kk6ab7102jwsk8mhx")))

