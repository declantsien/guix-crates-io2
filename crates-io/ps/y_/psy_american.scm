(define-module (crates-io ps y_ psy_american) #:use-module (crates-io))

(define-public crate-psy_american-0.2.6 (c (n "psy_american") (v "0.2.6") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (f (quote ("dex"))) (d #t) (k 0)) (d (n "psyfi-serum-dex-permissioned") (r "^0.5.6") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.13") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "13gs9sgigfmd632jcwk9qk974x8fpirnl0v25735r2pq8jlxlnas") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-psy_american-0.2.7 (c (n "psy_american") (v "0.2.7") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (f (quote ("dex"))) (d #t) (k 0)) (d (n "psyfi-serum-dex-permissioned") (r "^0.5.7") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.13") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0282852vgdgbqvd5qfri7l4vimd4lcak68i5a7ngb40w7q0vla29") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

