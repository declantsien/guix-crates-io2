(define-module (crates-io ps rd psrdada-sys) #:use-module (crates-io))

(define-public crate-psrdada-sys-0.1.0 (c (n "psrdada-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "page_size") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0slbim4ahg6l5hk0lyljizhi3gxsjaq41s0wjc9zy1c83bvslcda") (f (quote (("cuda")))) (l "psrdada") (r "1.57.0")))

(define-public crate-psrdada-sys-0.1.1 (c (n "psrdada-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "page_size") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1iama27q5djzsng1cfdsy6slydp6hxlsjgj5jchvjs3g3xbwwq09") (f (quote (("cuda")))) (l "psrdada") (r "1.57.0")))

(define-public crate-psrdada-sys-0.2.0 (c (n "psrdada-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "page_size") (r "^0.4") (d #t) (k 2)))) (h "1picxgsn795zasrzgrdpwh44sirisxss4kq8774lmfikvy53ydzd") (l "psrdada") (r "1.57.0")))

(define-public crate-psrdada-sys-0.2.1 (c (n "psrdada-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "page_size") (r "^0.4") (d #t) (k 2)))) (h "1fc07xv9jh66clrn8sy2ns9l4kiwpbyyrz0617s39wp42indgcxy") (l "psrdada") (r "1.57.0")))

(define-public crate-psrdada-sys-0.2.2 (c (n "psrdada-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "page_size") (r "^0.4") (d #t) (k 2)))) (h "0ij33v97riw5j71mhyr08m546v42hj6gng4i5zwvf2f61jmhsqkk") (l "psrdada") (r "1.57.0")))

(define-public crate-psrdada-sys-0.3.0 (c (n "psrdada-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "page_size") (r "^0.4") (d #t) (k 2)))) (h "1b1xhf37dzx126yxv1q25njyzg3qsw6f2z0yfx6kyia8hn403j23") (l "psrdada") (r "1.57.0")))

(define-public crate-psrdada-sys-0.4.0 (c (n "psrdada-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "page_size") (r "^0.6") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10rj4rrgcp2ra72j1z45mhvisx3g1kj46wrqgcrk3xhl9v87169i") (l "psrdada") (r "1.57.0")))

