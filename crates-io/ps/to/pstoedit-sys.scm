(define-module (crates-io ps to pstoedit-sys) #:use-module (crates-io))

(define-public crate-pstoedit-sys-0.1.0 (c (n "pstoedit-sys") (v "0.1.0") (h "1cn7mjsr4frqn6arx9j4zd8ld9nysn9vq80jc04dc3aynqfwhf9z") (l "pstoedit")))

(define-public crate-pstoedit-sys-0.1.1 (c (n "pstoedit-sys") (v "0.1.1") (h "1vm900676gh3hvv8k7sh257545630f1i7nlzlqlmg83nnnvrlx1n") (f (quote (("pstoedit_4_01" "pstoedit_4_00") ("pstoedit_4_00")))) (l "pstoedit")))

