(define-module (crates-io ps to pstoedit) #:use-module (crates-io))

(define-public crate-pstoedit-0.1.0 (c (n "pstoedit") (v "0.1.0") (d (list (d (n "pstoedit-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0jqp9b3c0c3268fcbgcyipamd3vjkddhkbxlh22q2cgywdzkxqx9")))

(define-public crate-pstoedit-0.1.1 (c (n "pstoedit") (v "0.1.1") (d (list (d (n "pstoedit-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1yakr3xbs8v2323frm9cvq8kjz690zwxlklkz72g8zbjzjn7002m") (f (quote (("pstoedit_4_00" "pstoedit-sys/pstoedit_4_00"))))))

