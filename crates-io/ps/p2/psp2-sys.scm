(define-module (crates-io ps p2 psp2-sys) #:use-module (crates-io))

(define-public crate-psp2-sys-0.1.0 (c (n "psp2-sys") (v "0.1.0") (h "135mcw2mxlkjbl5sz2f27dh4jfsm0584aqj832n5kar9aq6knsmw") (f (quote (("unsafe") ("default"))))))

(define-public crate-psp2-sys-0.1.1 (c (n "psp2-sys") (v "0.1.1") (h "0977mglz4ja6idfcg0bbs7m9xj6sw1vk9s037cbslhh5bbglj75h") (f (quote (("unsafe") ("default"))))))

(define-public crate-psp2-sys-0.2.0 (c (n "psp2-sys") (v "0.2.0") (h "0h7rm43g4g4zgpiyvsbn08vxgigph1jmp2wmqz8hvny5q833x36h") (f (quote (("unsafe") ("default"))))))

(define-public crate-psp2-sys-0.2.1 (c (n "psp2-sys") (v "0.2.1") (h "05mcipznq03wak6v0hx5779l2x9137faz81nhkw0zqrn6sfm3fir") (f (quote (("unsafe") ("dox") ("default"))))))

(define-public crate-psp2-sys-0.2.2 (c (n "psp2-sys") (v "0.2.2") (h "1zqjryp1ippybabl927p67n9lcq9yr7k19sgsk34z9hj72m29lmg") (f (quote (("unsafe") ("dox") ("default"))))))

