(define-module (crates-io ps p2 psp22-full) #:use-module (crates-io))

(define-public crate-psp22-full-0.3.0 (c (n "psp22-full") (v "0.3.0") (d (list (d (n "ink") (r "^4.3.0") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.9") (f (quote ("derive"))) (o #t) (k 0)))) (h "17iny8jmijrips7d3l3mrli6pfilb31m3qnck87hns4hpgx1il59") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

