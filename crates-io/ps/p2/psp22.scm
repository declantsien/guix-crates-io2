(define-module (crates-io ps p2 psp22) #:use-module (crates-io))

(define-public crate-psp22-0.2.0 (c (n "psp22") (v "0.2.0") (d (list (d (n "ink") (r "^4.3") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.9") (f (quote ("derive"))) (o #t) (k 0)))) (h "1q3zqd4g9pabrbi0c92rwnxi6vq7j99d4r56yi4wpghbywzyl6kh") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

(define-public crate-psp22-0.2.1 (c (n "psp22") (v "0.2.1") (d (list (d (n "ink") (r "^4.3") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.9") (f (quote ("derive"))) (o #t) (k 0)))) (h "094yyy4vxydbxydlvfjnjgmkp9rpjyni5ppbggq5jmy25cb1c2i8") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

(define-public crate-psp22-0.2.2 (c (n "psp22") (v "0.2.2") (d (list (d (n "ink") (r "^4.3") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.9") (f (quote ("derive"))) (o #t) (k 0)))) (h "1531yrbb3m6q3c4k4m7ch4pvxkvrlsbqagrc3d8bj5rf3kidm6ss") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

(define-public crate-psp22-0.3.0 (c (n "psp22") (v "0.3.0") (d (list (d (n "ink") (r "^5.0.0") (k 0)))) (h "15dkq543wzphvdvcklcflxp02x9508rkcmlgya8ra2akx2axwg74") (f (quote (("std" "ink/std") ("ink-as-dependency") ("default" "std" "contract") ("contract"))))))

(define-public crate-psp22-1.0.0 (c (n "psp22") (v "1.0.0") (d (list (d (n "ink") (r "^4.3") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.9") (f (quote ("derive"))) (o #t) (k 0)))) (h "05nhlzp3hwbyqh5wyjn1m6b0mpql0w5v8dwiqh5qzgk582b0z88p") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

(define-public crate-psp22-2.0.0 (c (n "psp22") (v "2.0.0") (d (list (d (n "ink") (r "^5.0.0") (k 0)))) (h "0np2ijl5m9wfm9nazrng6m0f81w387q3v89kn9nid1gcqa0rzqik") (f (quote (("std" "ink/std") ("ink-as-dependency") ("default" "std"))))))

