(define-module (crates-io ps yc psyche-host) #:use-module (crates-io))

(define-public crate-psyche-host-0.2.4 (c (n "psyche-host") (v "0.2.4") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1hz1x2al9zmis5h0m25dkf3l1w2ksly57g9w6l1jl44da3mz3b34")))

(define-public crate-psyche-host-0.2.5 (c (n "psyche-host") (v "0.2.5") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "028ib4yc8g0glfc8rag45i5h92a1rh4ymrz27z0giwvg46w1zgli")))

(define-public crate-psyche-host-0.2.6 (c (n "psyche-host") (v "0.2.6") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "12b0gxrdlyfzjmfzk7gzg7k3j48r7w3z8hfgmhbci18bxvs8w37v")))

(define-public crate-psyche-host-0.2.7 (c (n "psyche-host") (v "0.2.7") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1591kp6j68q47wxbbn9y30a1wyypfkywg79ibdlzvrsif0814p4k")))

(define-public crate-psyche-host-0.2.8 (c (n "psyche-host") (v "0.2.8") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "018djnm0c77dw74ycna9ndsxgb9fc8dslsiyir5zf32iri1zvxd1")))

(define-public crate-psyche-host-0.2.9 (c (n "psyche-host") (v "0.2.9") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "085j213aw6vnfcd9fsrkwzyp9mbwn662db33p2vapr6skppn75ps")))

(define-public crate-psyche-host-0.2.10 (c (n "psyche-host") (v "0.2.10") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0njwxdbmb6pwh9r1pd85zq24as2lxizfk8pvg90plh5rg20zfzv2")))

(define-public crate-psyche-host-0.2.11 (c (n "psyche-host") (v "0.2.11") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1f9wy90c8y4qzmq0g9x75grpsykjyy025yd9r6f3dn6a7x1c8wd5")))

(define-public crate-psyche-host-0.2.12 (c (n "psyche-host") (v "0.2.12") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "08aaz265g36v3pxiadwlzbzng6w557ymf1xyc4kvwp29swjwabpm")))

(define-public crate-psyche-host-0.2.13 (c (n "psyche-host") (v "0.2.13") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "16036351knp1ivc9aa6zjk461rh15svf9fqxkrr1jjar3nhhvsfj")))

(define-public crate-psyche-host-0.2.14 (c (n "psyche-host") (v "0.2.14") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0m27k64yb2q48v5l2q7jvqd7c59h2n52ap7imkazk7blip73900y")))

(define-public crate-psyche-host-0.2.15 (c (n "psyche-host") (v "0.2.15") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0gr3493kawk8psssb5zraqxrdjsk1wmai9ifwmbiprpby2p30615")))

(define-public crate-psyche-host-0.2.16 (c (n "psyche-host") (v "0.2.16") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0n0287xz99m7rng863wx0hl2b30irxcl0sc077m7bir1114v1kmz")))

(define-public crate-psyche-host-0.2.17 (c (n "psyche-host") (v "0.2.17") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0krkwrw3q1iq4f2g86f0mpcikacw1h3wnwyq4hlc584zjsz2cpnv")))

(define-public crate-psyche-host-0.2.18 (c (n "psyche-host") (v "0.2.18") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "06pp6imhrg88jdvp0zyy2kwa1b9gs4d4mlz4w1v791kddk1h931l") (f (quote (("parallel" "psyche-core/parallel"))))))

(define-public crate-psyche-host-0.2.19 (c (n "psyche-host") (v "0.2.19") (d (list (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1ml0nvx74j6bzh0n3k81jc93fpz1cwhbviak9ssb1w7wnf8xwksf") (f (quote (("parallel" "psyche-core/parallel"))))))

