(define-module (crates-io ps yc psyche-amethyst) #:use-module (crates-io))

(define-public crate-psyche-amethyst-0.2.17 (c (n "psyche-amethyst") (v "0.2.17") (d (list (d (n "amethyst") (r "^0.10") (d #t) (k 0)) (d (n "psyche") (r "^0.2") (d #t) (k 0)))) (h "1f3nd7g3bp0grlr8m5qpbgvgk8dqgfpmh3xkz0wb6sbww12vgwmm")))

(define-public crate-psyche-amethyst-0.2.18 (c (n "psyche-amethyst") (v "0.2.18") (d (list (d (n "amethyst") (r "^0.10") (d #t) (k 0)) (d (n "psyche") (r "^0.2") (d #t) (k 0)))) (h "156cb19il1nwdw238ydd4xbfv2ac3igx64bv7hyirqfrwsfv38i2") (f (quote (("parallel" "psyche/parallel"))))))

(define-public crate-psyche-amethyst-0.2.19 (c (n "psyche-amethyst") (v "0.2.19") (d (list (d (n "amethyst") (r "^0.10") (d #t) (k 0)) (d (n "psyche") (r "^0.2") (d #t) (k 0)))) (h "1arxnvxg1vv0lpp9vflbp8kvys8kqknd1v823nqgb6nfyzbk33p3") (f (quote (("parallel" "psyche/parallel"))))))

