(define-module (crates-io ps yc psyche-serde) #:use-module (crates-io))

(define-public crate-psyche-serde-0.1.0 (c (n "psyche-serde") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0cngmpvbz8qx1crk9wh2h64pky723bd0li3x38pkpwmckjns94kp")))

(define-public crate-psyche-serde-0.2.0 (c (n "psyche-serde") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "14mj06s7ws4x533gycff7rf0mp3fx20byljizac3ajsmwhip2ysy")))

(define-public crate-psyche-serde-0.2.1 (c (n "psyche-serde") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "12vza9xy3hap813n6snjgjzarqdf9vf0r39kkv0xaz8q94z9x91i")))

(define-public crate-psyche-serde-0.2.2 (c (n "psyche-serde") (v "0.2.2") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "125c15q17bbkagzlv63y4ydn8763yhs4hl4p555nm8wpf7im1vbd")))

(define-public crate-psyche-serde-0.2.3 (c (n "psyche-serde") (v "0.2.3") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0fbmmia7d8knpnxdzhh9ijqwjkhvcwcasdm1v4hj04jdmvpawps5")))

(define-public crate-psyche-serde-0.2.4 (c (n "psyche-serde") (v "0.2.4") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "13x4141zvjrck9m5vj8ldji8qzzgxwxcig3h51cf713n583j9fw7")))

(define-public crate-psyche-serde-0.2.5 (c (n "psyche-serde") (v "0.2.5") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ng456fvjvacv6zd87ynhl7pp944i0a8sg2rb18x89h13svc1fzw")))

(define-public crate-psyche-serde-0.2.6 (c (n "psyche-serde") (v "0.2.6") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0hcn9bhjbnjw6457rfq1qrsn8xd4338bnxagik8d820iwybnlpvl")))

(define-public crate-psyche-serde-0.2.7 (c (n "psyche-serde") (v "0.2.7") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1xg1dlpmnvnpxp4paizd4x59fnsvxbaxjgj0q2jrrbzvan31a0qb")))

(define-public crate-psyche-serde-0.2.8 (c (n "psyche-serde") (v "0.2.8") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ma7fc4s6kgyac6jk2cgkkr438i4rhbgsjgjbyjybh0a4s3ywjfr")))

(define-public crate-psyche-serde-0.2.9 (c (n "psyche-serde") (v "0.2.9") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "00zzscbp26vv7sa9j20kdin9c0zkarvlybr5vx3gpifb3qs6nnzk")))

(define-public crate-psyche-serde-0.2.10 (c (n "psyche-serde") (v "0.2.10") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "187cnvpq1y775lzgnc4r4gakm0hw2nfyg1xw61ajyaz4y165jsr7")))

(define-public crate-psyche-serde-0.2.11 (c (n "psyche-serde") (v "0.2.11") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1b8jk1839hyshcajycw39f0h8gw75kxp3rs6lhbjmcvkfjjqvv16")))

(define-public crate-psyche-serde-0.2.12 (c (n "psyche-serde") (v "0.2.12") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0qr7hmyg98ys4sa9g8lwwq173m92vqc16h6qqiw825zm7ndv954s")))

(define-public crate-psyche-serde-0.2.13 (c (n "psyche-serde") (v "0.2.13") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ylddjjrsf6aix00pxl3l1mvj5fa2mmvy000ad7qfdbxffp9c1bw")))

(define-public crate-psyche-serde-0.2.14 (c (n "psyche-serde") (v "0.2.14") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1qmahicck7s0bvv9qgplqrbrdwzl37kxw1fnbfz3s11p51k2sya4")))

(define-public crate-psyche-serde-0.2.15 (c (n "psyche-serde") (v "0.2.15") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1h4i83i5kf21igri4q86c7hixfs7pxivrz5b2km229fqsgf31c9s")))

(define-public crate-psyche-serde-0.2.16 (c (n "psyche-serde") (v "0.2.16") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "05n409crxw11009ya8f1l6pcmpdcqq2g1yy45sq52jx4kxhmvnqi")))

(define-public crate-psyche-serde-0.2.17 (c (n "psyche-serde") (v "0.2.17") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0n9r7i7k2bwwwyxx99p4q7dsim8wih7yz97g18jg9sv7rqmvpv3s")))

(define-public crate-psyche-serde-0.2.18 (c (n "psyche-serde") (v "0.2.18") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "09wf3b4vdbyqapnr05sdbl49r9as8pqlzha07rgzdagfikdrsypj") (f (quote (("parallel" "psyche-core/parallel"))))))

(define-public crate-psyche-serde-0.2.19 (c (n "psyche-serde") (v "0.2.19") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "18nk6h3cg4cd489cflvnf86nlg61pqn025c51m21flzhq5mj578d") (f (quote (("parallel" "psyche-core/parallel"))))))

