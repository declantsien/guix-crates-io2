(define-module (crates-io ps yc psyche-graphics) #:use-module (crates-io))

(define-public crate-psyche-graphics-0.2.6 (c (n "psyche-graphics") (v "0.2.6") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "06g7gkglq8bwy3xk84g3qfa426gmbaac5cwrrbdzy14rvzyxn56g")))

(define-public crate-psyche-graphics-0.2.7 (c (n "psyche-graphics") (v "0.2.7") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0q28xb23bclp4fhfy2a84s9glw22g7xvpn2ljsqm7a6nr208fzc9")))

(define-public crate-psyche-graphics-0.2.8 (c (n "psyche-graphics") (v "0.2.8") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0vw3pyfw9jvqvr3qs4sy21r114r8l456k7b1dwblz6ryzd7b4lql")))

(define-public crate-psyche-graphics-0.2.9 (c (n "psyche-graphics") (v "0.2.9") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1qn91bpg07ayc82x6akq230jh8zn5vcciklcbkmndrrfswlxb84w")))

(define-public crate-psyche-graphics-0.2.10 (c (n "psyche-graphics") (v "0.2.10") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "05awxsvvdmp3bsm1ydms20siq5cvdjjjgzskpcm0hvqr3nrb7gz8")))

(define-public crate-psyche-graphics-0.2.11 (c (n "psyche-graphics") (v "0.2.11") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0za50x27bgyhsn8ymkq5dglhcrs2yjifhiwnx142csqdnm86i3jz")))

(define-public crate-psyche-graphics-0.2.12 (c (n "psyche-graphics") (v "0.2.12") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1m90xfmbrhh3yisazw799a5w4mcwk9ycmfsp4grnnafmp2bwf4yh")))

(define-public crate-psyche-graphics-0.2.13 (c (n "psyche-graphics") (v "0.2.13") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1prnpmc9dc2h30wsvqz3f89dqz07xkwd19sppar256jb1w0rqs2m")))

(define-public crate-psyche-graphics-0.2.14 (c (n "psyche-graphics") (v "0.2.14") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0xj6skcp4fsy4zi8mahyr3k2mhcdxgm1d07k92rc8gb3kq16fivv")))

(define-public crate-psyche-graphics-0.2.15 (c (n "psyche-graphics") (v "0.2.15") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0rizrby5zzald35a9vcmkr3gb00bgrrngz56mf5l73hj2j02g6c3")))

(define-public crate-psyche-graphics-0.2.16 (c (n "psyche-graphics") (v "0.2.16") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0cikswyyy95zb0xww6dnpxjz86vcgiz1n9vb5b8byv9zjc54jy5a")))

(define-public crate-psyche-graphics-0.2.17 (c (n "psyche-graphics") (v "0.2.17") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "078zgas4gixrfxly0pg7bbr2q0i1vxi8ffs4agsmkvdsggy0xfpn")))

(define-public crate-psyche-graphics-0.2.18 (c (n "psyche-graphics") (v "0.2.18") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "1i5w8gam46fzr5byzrv9mm1fb7341i5s8mscfsh69llq4r38y8w2") (f (quote (("parallel" "psyche-core/parallel"))))))

(define-public crate-psyche-graphics-0.2.19 (c (n "psyche-graphics") (v "0.2.19") (d (list (d (n "obj-exporter") (r "^0.2") (d #t) (k 0)) (d (n "psyche-core") (r "^0.2") (d #t) (k 0)))) (h "0nbjx3pvxgarnnkdpws6q2j31v24njjn0xwxl0wnwhy7i9sl8xww") (f (quote (("parallel" "psyche-core/parallel"))))))

