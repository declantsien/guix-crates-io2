(define-module (crates-io ps yc psyche-core) #:use-module (crates-io))

(define-public crate-psyche-core-0.1.0 (c (n "psyche-core") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0kmkyj2sv6x3a0n3i2csi4ckwc801rr5hyz9miy9508nzhaz5ry3")))

(define-public crate-psyche-core-0.2.0 (c (n "psyche-core") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0g7csipg0m9zg98grh3x1481wg0r8wpl7cj4zdykks1r3dfafwp6")))

(define-public crate-psyche-core-0.2.1 (c (n "psyche-core") (v "0.2.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1zbijijiq5a1mq83nvdh8q0vh7lsksdg0s82dsx23palshfz7a9q")))

(define-public crate-psyche-core-0.2.2 (c (n "psyche-core") (v "0.2.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1asxlq2l607zvg529wdyad6s50v0mqqd2z7c9xjc7rqrj79s8fy7")))

(define-public crate-psyche-core-0.2.3 (c (n "psyche-core") (v "0.2.3") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05y9wmjkn9gbgy6gs2xr5zwah0q68c5y5vz7va82rhd2abs54klg")))

(define-public crate-psyche-core-0.2.4 (c (n "psyche-core") (v "0.2.4") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0kjingkvxagjyhhbv2haw19apzkywll7v7f9rklri7na9mn0fx8g")))

(define-public crate-psyche-core-0.2.5 (c (n "psyche-core") (v "0.2.5") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1axlpf1mqgwdhx8nspfadw30kvjg7iv8whrwnzicgnzzm3lpbdka")))

(define-public crate-psyche-core-0.2.6 (c (n "psyche-core") (v "0.2.6") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "01yc1zp8dbfz8fl9gdg78nby2bp1yn2yyrimka7lwrikh54cj4gz")))

(define-public crate-psyche-core-0.2.7 (c (n "psyche-core") (v "0.2.7") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0kjiwnybgd68my00s5bd06aphm4m86ysx6wmi4mgb7sw1ra0bwdm")))

(define-public crate-psyche-core-0.2.8 (c (n "psyche-core") (v "0.2.8") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1c9jvjkyd7w4fys64x998g91wibk3f2f4hg14y0mjccmd171j6ja")))

(define-public crate-psyche-core-0.2.9 (c (n "psyche-core") (v "0.2.9") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0j9n7wmrx8ph6k1qzdwa0438spcvh955ycr8avhqsgn8yn7hq8z1")))

(define-public crate-psyche-core-0.2.10 (c (n "psyche-core") (v "0.2.10") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1qrrgs0b90lj8b9s0cnp46vjb4yfdp9wpf4ba76jvilg4rpzvxvq")))

(define-public crate-psyche-core-0.2.11 (c (n "psyche-core") (v "0.2.11") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "19lfb4s00056z7yb2wqsj3c9xlpjjnkgsfiamyifgk7708ryxbxd")))

(define-public crate-psyche-core-0.2.12 (c (n "psyche-core") (v "0.2.12") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0xz2jidbwm4rh603vy9nkikgpmb5i1nlb1r0agr27wicrs4m0shc")))

(define-public crate-psyche-core-0.2.13 (c (n "psyche-core") (v "0.2.13") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1s44dz699s3c7n90d361qhh5dydf4bap1sxv0hl87bhdrbnkwb1v")))

(define-public crate-psyche-core-0.2.14 (c (n "psyche-core") (v "0.2.14") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0mzpj9s5r76n804czc8mvsxzns6a91s1gx5q64225if7f70zg479")))

(define-public crate-psyche-core-0.2.15 (c (n "psyche-core") (v "0.2.15") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "010kavq6lv3ra9xnd3r4xmpv69hfmsl9zqvyp3h5k3fa67cql3ag")))

(define-public crate-psyche-core-0.2.16 (c (n "psyche-core") (v "0.2.16") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1yz3kgl5hprb2wr2bk90yywnyscbv7a8d7zajar9c1xlkb7nj16c")))

(define-public crate-psyche-core-0.2.17 (c (n "psyche-core") (v "0.2.17") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "16ba1fmdy2glj1vdf87qy0v180rhg21vl811m3ha5s3f5dy8xh2c")))

(define-public crate-psyche-core-0.2.18 (c (n "psyche-core") (v "0.2.18") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1n0zi34va97p93yhxwlv9pa2cc0ynrnpy5f9c9isv91092j245wk") (f (quote (("parallel" "rayon"))))))

(define-public crate-psyche-core-0.2.19 (c (n "psyche-core") (v "0.2.19") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "18y9klns3fli1cwbmfv5gmvirsdvwc9gvdxfa08qi3bdqiz65jcn") (f (quote (("parallel" "rayon"))))))

