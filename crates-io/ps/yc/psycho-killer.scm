(define-module (crates-io ps yc psycho-killer) #:use-module (crates-io))

(define-public crate-psycho-killer-0.1.0 (c (n "psycho-killer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "18srb45d7znyawdjkp2flcara9i9mb4f6f8nys836gzkmd5y4xck")))

(define-public crate-psycho-killer-0.1.1 (c (n "psycho-killer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "1pf8zs9s47pma9nnszp3m29w7ila9bx16i63v6d1jdgjcmb703ch")))

(define-public crate-psycho-killer-0.2.0 (c (n "psycho-killer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "1khgjbhg20c0rh48p91ccxdzyqr6lda9kxgcd0vlr4il0pm5jmr9")))

(define-public crate-psycho-killer-0.3.0 (c (n "psycho-killer") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "1wihwh4wha29820iqj2gfmwj37nsl13dgfh3ag2bk59fl1rgp3dw")))

(define-public crate-psycho-killer-0.3.1 (c (n "psycho-killer") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "1p4ak5pli2y81imss41v9nhpkflkrwdykbg3dyiyl0zh3my89arg")))

(define-public crate-psycho-killer-0.3.3 (c (n "psycho-killer") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "1704bm19xalr5zd6mmyrgdnbg0yzmlnq4sr69akp6b4s8zknhns9")))

(define-public crate-psycho-killer-0.4.0 (c (n "psycho-killer") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "0khmxbn6y3qzrzr6riiq32wg9j0s8hsa9ynjd4gd4fg70k9pxlgr")))

(define-public crate-psycho-killer-0.4.1 (c (n "psycho-killer") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)))) (h "1z46c6b7blhj209ilwk0sqag6aqwr8kr8pyhlsv9bdxadyvf9q05")))

