(define-module (crates-io ps yc psychonaut_wiki_api) #:use-module (crates-io))

(define-public crate-psychonaut_wiki_api-0.1.0 (c (n "psychonaut_wiki_api") (v "0.1.0") (d (list (d (n "gql_client") (r "^1.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "gzip" "deflate" "brotli" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "16hf3y4qnigf643ymchhp09ywp18zhyjllw04jhmag4grnyvbks3")))

