(define-module (crates-io ps g- psg-core) #:use-module (crates-io))

(define-public crate-psg-core-0.1.0 (c (n "psg-core") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1705mvdpzzn1hijh6d86rmhlbyyfnr6lf9q1c30dy50hs7l69nq8")))

(define-public crate-psg-core-0.1.1 (c (n "psg-core") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "178nv58v3shas7xy6n92inml3mng8b73sfl77m1bkr2d4mdwvp5x")))

(define-public crate-psg-core-0.1.2 (c (n "psg-core") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h4dji5jk8dyfr8j8vrfzjid86ak2hqsqz8karsnaqqsyd6gyich")))

