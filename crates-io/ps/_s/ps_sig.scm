(define-module (crates-io ps _s ps_sig) #:use-module (crates-io))

(define-public crate-ps_sig-0.1.0 (c (n "ps_sig") (v "0.1.0") (d (list (d (n "amcl_wrapper") (r "^0.1.6") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1gwm89jb5hszfv7bhn41kk8l4632fw17s8pl0clkmijlpp2sdk6y") (f (quote (("default" "G1G2") ("G2G1") ("G1G2"))))))

(define-public crate-ps_sig-0.1.1 (c (n "ps_sig") (v "0.1.1") (d (list (d (n "amcl_wrapper") (r "^0.1.6") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1p0ixk0q8dzp3dxhm4cfnm6qwqbrgbc3q7xrz9lhhavpr0p8g5vb") (f (quote (("default" "G1G2") ("G2G1") ("G1G2"))))))

(define-public crate-ps_sig-0.1.2 (c (n "ps_sig") (v "0.1.2") (d (list (d (n "amcl_wrapper") (r "^0.1.7") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0w61y25ggd1fnn336wid9qafagnf7mzkgi7y1gpdwz154q59hmiv") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

(define-public crate-ps_sig-0.1.4 (c (n "ps_sig") (v "0.1.4") (d (list (d (n "amcl_wrapper") (r "^0.1.7") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1g2kxvvi2jybvg2cyyx8ppfiipjccfwv4jqlwpyk0crfcqqnks00") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

(define-public crate-ps_sig-0.2.0 (c (n "ps_sig") (v "0.2.0") (d (list (d (n "amcl_wrapper") (r "^0.2.3") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19knpwxh5hdbfikqqk2c040mdrp2bdgasz7vdcpjmshd69nmjyk9") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

