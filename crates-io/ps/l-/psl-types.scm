(define-module (crates-io ps l- psl-types) #:use-module (crates-io))

(define-public crate-psl-types-2.0.0 (c (n "psl-types") (v "2.0.0") (h "1xb8k9wvl5x165zf2inrpcijpz3kq23fa3rcal96492ypmsw8rb9")))

(define-public crate-psl-types-2.0.1 (c (n "psl-types") (v "2.0.1") (h "1690xcrz040xnhs477in75m08vxg39kgbs4ra3m2ywkajf8w6xs1")))

(define-public crate-psl-types-2.0.2 (c (n "psl-types") (v "2.0.2") (h "1zwhpz6mc6n26bwy0fl9mvga7vwrp2zdzg7dh9kbkpycv6bak6lf")))

(define-public crate-psl-types-2.0.3 (c (n "psl-types") (v "2.0.3") (h "0y63cwb8ybzag9lwckcd5jpxb9ncx2isjxkajpjqsmdg65na2dmq")))

(define-public crate-psl-types-2.0.4 (c (n "psl-types") (v "2.0.4") (h "1g74rwcikp4fgdfq2ymzdmm1wkd57xkqx93965rlpz1djj60ixk4")))

(define-public crate-psl-types-2.0.5 (c (n "psl-types") (v "2.0.5") (h "0bf9a350nndbddkr8y9wqpd930lc09q0mdmmq4im7g0n6sv1nbg1")))

(define-public crate-psl-types-2.0.6 (c (n "psl-types") (v "2.0.6") (h "1lrfw5qdyhv7svppags160vrpra89xkb4l2znml51cn0hcbda19s")))

(define-public crate-psl-types-2.0.7 (c (n "psl-types") (v "2.0.7") (h "0kqssn9wgqpl1m26ynv7cvxanfhyjvssi2a3jc2nzpbw7q3ricv6")))

(define-public crate-psl-types-2.0.8 (c (n "psl-types") (v "2.0.8") (h "132v80bc2p1irbc32snsx3yv7j1nq006k616zz4hz2a750rqbs81")))

(define-public crate-psl-types-2.0.9 (c (n "psl-types") (v "2.0.9") (h "1yya9l9pmk7nswx8255bx31zbf31wad91y2ra1mn4s4yvxszdy2a")))

(define-public crate-psl-types-2.0.10 (c (n "psl-types") (v "2.0.10") (h "0w74li516dsalxmsk5mfcqbgdbg0dl04qdv2iggszjly5p3agvg8")))

(define-public crate-psl-types-2.0.11 (c (n "psl-types") (v "2.0.11") (h "1b3cz1q07iy744a39smykra2j83nv8vmni6la37wnx3ax17jkjrk")))

