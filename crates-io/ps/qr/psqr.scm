(define-module (crates-io ps qr psqr) #:use-module (crates-io))

(define-public crate-psqr-0.1.0 (c (n "psqr") (v "0.1.0") (h "0ggaknim0p043w8xpsmk2fb2zcvrlwna46jpn85188yam7c0fpi8")))

(define-public crate-psqr-0.1.1 (c (n "psqr") (v "0.1.1") (h "1nai5xhskxyj3f9r5cvz4102225y4yy98sq256hxl1d8lsa7a3l3")))

