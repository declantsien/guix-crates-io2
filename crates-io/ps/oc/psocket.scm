(define-module (crates-io ps oc psocket) #:use-module (crates-io))

(define-public crate-psocket-0.1.0 (c (n "psocket") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05qdg2qjcxym24xgpps28yj959nig8qqflchd2kj82n0lp9vg34n")))

(define-public crate-psocket-0.1.2 (c (n "psocket") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s5iq36pm7bv2fc8b75wm984hlhjgy4jdcfjp7nj8j85hrrgf73c")))

(define-public crate-psocket-0.1.3 (c (n "psocket") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kv565r46nm8vkk4dcha7q9zwzrz3w8lq4g10xrpqbfjj2c78kpf")))

(define-public crate-psocket-0.1.4 (c (n "psocket") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11r5xqcs8dg04p14pq1cyp9byx2mxji9gxc5qrbia84jmh5d710h")))

(define-public crate-psocket-0.1.5 (c (n "psocket") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ay37v0c9cx2ib0z8pf2ffmdw28nwkijyyhjvrrj5sybhlhb2623")))

