(define-module (crates-io ps sh pssh-rs) #:use-module (crates-io))

(define-public crate-pssh-rs-0.1.0 (c (n "pssh-rs") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "vergen") (r "^6") (d #t) (k 1)))) (h "1a80mgvf4rq40816fsglyfpmlpc9i4b3jssgya2nycjng7436nff")))

(define-public crate-pssh-rs-0.2.0 (c (n "pssh-rs") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "vergen") (r "^6") (d #t) (k 1)))) (h "059mxqpczksb0nf7jnj4wi7bsh45pw41yl6jmifhfzc7c6drkrj6")))

(define-public crate-pssh-rs-0.2.1 (c (n "pssh-rs") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "13bdgbhz4yv3kvk1k2bn3h1qagi3q9pmz3xqqv8c4mahsi06rzaq")))

(define-public crate-pssh-rs-0.2.2 (c (n "pssh-rs") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "060q4v1q3brpcdn98fdn1qdby9373ff6si4bid3hla2caahyrcb4")))

(define-public crate-pssh-rs-0.2.3 (c (n "pssh-rs") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "047w6i064yyg1mv2j7pkpl45s1ycmgjx1gx6qrc444dcilylp8q8")))

(define-public crate-pssh-rs-0.2.4 (c (n "pssh-rs") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1xpnp2nb9ii5s21ycgxqxxwkf4xjgzjhb1rpavcf0378db39l5jz")))

(define-public crate-pssh-rs-0.2.5 (c (n "pssh-rs") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0vhcy78xf3gnwlhi9jcd7qy6q98a3p64a5cn2aprgxyfbvvamfv6")))

(define-public crate-pssh-rs-0.2.6 (c (n "pssh-rs") (v "0.2.6") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0n8ricmg5l71ign7wwjc6pwz1pgdnj8cssq7q7qy4rf8c1hn3p0p")))

(define-public crate-pssh-rs-0.2.7 (c (n "pssh-rs") (v "0.2.7") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0sqy37xsa2i6zg8kjaxqxxqxyv84zgx9qvv7jjq29wqgkcl4lfbp")))

