(define-module (crates-io ps yn psync) #:use-module (crates-io))

(define-public crate-psync-0.1.0 (c (n "psync") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "os_type") (r "^2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "02m180ypl4j5scw6gwpp8v8agg30l80aaphkg38f7aymrzdmslnz")))

