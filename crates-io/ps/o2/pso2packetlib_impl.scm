(define-module (crates-io ps o2 pso2packetlib_impl) #:use-module (crates-io))

(define-public crate-pso2packetlib_impl-0.1.0 (c (n "pso2packetlib_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("printing"))) (d #t) (k 0)))) (h "0xxfqm2qs6rrrzcv2azgbarmnx53q6ngrgj77v6fq5m9b4nkp3l7")))

