(define-module (crates-io ps i_ psi_device_tree) #:use-module (crates-io))

(define-public crate-psi_device_tree-2.1.0 (c (n "psi_device_tree") (v "2.1.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09rjac0m1x1c1xlmdl6k8wwaaq6j578ys6z4g2cr76iph6cnrj5x") (f (quote (("string-dedup"))))))

(define-public crate-psi_device_tree-2.2.0 (c (n "psi_device_tree") (v "2.2.0") (d (list (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0izn2kzik1bhs4raf2wsaf0qsqxk0gylhz0xh2phbh1g3qyzjrd3") (f (quote (("string-dedup"))))))

