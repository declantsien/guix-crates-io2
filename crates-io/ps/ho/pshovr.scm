(define-module (crates-io ps ho pshovr) #:use-module (crates-io))

(define-public crate-pshovr-0.1.0 (c (n "pshovr") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "redacted_debug") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 2)))) (h "194rgr5w14jn80i5l9jiw80a168479y5qcab1hf5as0y2qxf1bsm")))

(define-public crate-pshovr-0.2.0 (c (n "pshovr") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "redacted_debug") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "05zgcvxadgwj94jbpilldh0mxwjy3ln328mvmb2sij1driv2bywx")))

(define-public crate-pshovr-0.3.0 (c (n "pshovr") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "redacted_debug") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1m61zmf4afhrr4q74m1zi81ikzhmxg30a5qq3cwdq0yy6zwn0vyi") (f (quote (("async"))))))

(define-public crate-pshovr-0.4.0 (c (n "pshovr") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "redacted_debug") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0wm38a5ija3460vrjdss6n382b4580ydqgkfx21yl6dlh972bj5k") (f (quote (("async"))))))

