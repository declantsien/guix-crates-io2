(define-module (crates-io ps h- psh-db) #:use-module (crates-io))

(define-public crate-psh-db-0.1.0 (c (n "psh-db") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "psh") (r "^0.4.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "1gv28vkzwkwm0krawmbb3vp4chacy9a3pvcaas85adsjxyyrz7rf")))

(define-public crate-psh-db-0.2.0 (c (n "psh-db") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "psh") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0dcps1zjy5jz6sa6rndzh2vkkha9311d5bw0r41k7gh33ry5hsda")))

