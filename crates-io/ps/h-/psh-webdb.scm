(define-module (crates-io ps h- psh-webdb) #:use-module (crates-io))

(define-public crate-psh-webdb-0.1.0 (c (n "psh-webdb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "psh") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Storage" "Window"))) (d #t) (k 0)))) (h "1v0jdydz9b3dircmgm1rqqv9a6cj0ypadxpy15klbhmj58azfv9j")))

(define-public crate-psh-webdb-0.2.0 (c (n "psh-webdb") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "psh") (r "^0.5.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Storage" "Window"))) (d #t) (k 0)))) (h "0m6al1m270pi07x2m102igng2s325sl8bzphnhkdz3w7qbhladhb")))

