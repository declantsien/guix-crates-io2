(define-module (crates-io ps h- psh-cli) #:use-module (crates-io))

(define-public crate-psh-cli-0.4.0 (c (n "psh-cli") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "psh") (r "^0.4.0") (d #t) (k 0)) (d (n "psh-db") (r "^0.1.0") (d #t) (k 0)))) (h "1n3gaacrpamg17yl34qnkrsyz19wmqbxzcxpbdg3d4kl1b2gkr9b")))

(define-public crate-psh-cli-0.5.0 (c (n "psh-cli") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "psh") (r "^0.5.0") (d #t) (k 0)) (d (n "psh-db") (r "^0.2.0") (d #t) (k 0)))) (h "0yh9f9ckmq4hgn5y9sicsy7rwliwn1qf4q81xdbczlhk8kql33pw")))

(define-public crate-psh-cli-0.5.1 (c (n "psh-cli") (v "0.5.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "psh") (r "^0.5.0") (d #t) (k 0)) (d (n "psh-db") (r "^0.2.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "17nci7ff2ss7pnb962n7qbdc91h8aqxqgkhrf2i4bha79pwp2wgv")))

