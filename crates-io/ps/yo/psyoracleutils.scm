(define-module (crates-io ps yo psyoracleutils) #:use-module (crates-io))

(define-public crate-psyoracleutils-0.1.0 (c (n "psyoracleutils") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pyth-sdk-solana") (r "^0.8.0") (d #t) (k 0)) (d (n "switchboard-v2") (r "^0.4.0") (d #t) (k 0)))) (h "0d72nxyd9yj36zlgrmiqxvlkwbc3vfnx98x39gaw0gim18xccn7b") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("localnet") ("devnet-deploy") ("default") ("cpi" "no-entrypoint"))))))

