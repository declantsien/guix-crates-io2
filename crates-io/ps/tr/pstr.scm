(define-module (crates-io ps tr pstr) #:use-module (crates-io))

(define-public crate-pstr-0.0.0 (c (n "pstr") (v "0.0.0") (d (list (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)))) (h "00ajnjj9m644rrr0gvy5rm38lrww41nns5wxg3mqphdikqikzdyi")))

(define-public crate-pstr-0.1.0 (c (n "pstr") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)))) (h "02f62br8kxlxxkg5mqk2f0lcmbq14x0n4wzy8prq3c2ic1qd0mda")))

(define-public crate-pstr-0.2.0 (c (n "pstr") (v "0.2.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0i373pww69xbyrz4d5j82rs6kpgah8bqxi43l46z202ajlhkwvrv")))

(define-public crate-pstr-0.2.1 (c (n "pstr") (v "0.2.1") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1d7v0k8g821i513isnkl6sxmj0xyzvckb39kw0bj8j8zviq37c5f")))

(define-public crate-pstr-0.2.2 (c (n "pstr") (v "0.2.2") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0gck81d76l9nw8vj279789kg5frihfcqjv8nk4fwzyh6md4v4fni")))

(define-public crate-pstr-0.2.3 (c (n "pstr") (v "0.2.3") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1hdxcs95np62k2rj21ykg7zrdacww5yl1gzxc4bdbqi4bha5s62f")))

(define-public crate-pstr-0.3.0 (c (n "pstr") (v "0.3.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0lib6by061sgr9x611k2dlj0mhn891z98jg45yhm3za16vfvfnb8")))

(define-public crate-pstr-0.4.0 (c (n "pstr") (v "0.4.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0zaq4qygk5pfsnfgjk559zdcmy0wfg1zl1jigml8wwdi7h2hxs3h")))

(define-public crate-pstr-0.5.0 (c (n "pstr") (v "0.5.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "1qfjzn8zwxpzmxddknc8khh70dk90rws6qr1iyi5q1p4367lrfqy")))

(define-public crate-pstr-0.5.1 (c (n "pstr") (v "0.5.1") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0azr9sh2shvl4qpan0xx3g7msjanhiccnw3rl3c9nn9rb9iax191")))

(define-public crate-pstr-0.6.0 (c (n "pstr") (v "0.6.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "17cy6v7cf6qv3x9yn26icm6pmz2zfhnyrhr1c5an33svxf53p4s0")))

(define-public crate-pstr-0.7.0 (c (n "pstr") (v "0.7.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)))) (h "0hwwn95jij8n9pkw9axvks9kza7q850qnzkl0ljnnisi9c5fh1kz")))

