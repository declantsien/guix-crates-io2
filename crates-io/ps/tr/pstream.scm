(define-module (crates-io ps tr pstream) #:use-module (crates-io))

(define-public crate-pstream-0.1.0 (c (n "pstream") (v "0.1.0") (d (list (d (n "crc32fast") (r "~1.3") (d #t) (k 0)) (d (n "libc") (r "~0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)) (d (n "vlock") (r "~0.2") (d #t) (k 0)))) (h "0gdqqylw4fndp65xz1hwiig08k7zxayfdnrqcln9hhb5l7165h71") (f (quote (("io-filesystem") ("default"))))))

(define-public crate-pstream-0.1.1 (c (n "pstream") (v "0.1.1") (d (list (d (n "crc32fast") (r "~1.3") (d #t) (k 0)) (d (n "libc") (r "~0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)) (d (n "vlock") (r "~0.2") (d #t) (k 0)))) (h "1v7w4rkky4pddb9jfwbinyk96hh3l98w8pc10im5cvdzagg6ngm4") (f (quote (("io-filesystem") ("default"))))))

