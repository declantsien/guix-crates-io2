(define-module (crates-io ps st psst) #:use-module (crates-io))

(define-public crate-psst-0.1.0 (c (n "psst") (v "0.1.0") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "rprompt") (r "~1.0") (d #t) (k 0)) (d (n "toml") (r "~0.4") (d #t) (k 0)) (d (n "xdg") (r "~2.1") (d #t) (k 0)))) (h "1n2219v85ic21k9j7inhhxbsnrx88crq9q0sm13vxx18lqhami0s")))

(define-public crate-psst-0.1.1 (c (n "psst") (v "0.1.1") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "rprompt") (r "~1.0") (d #t) (k 0)) (d (n "toml") (r "~0.5") (d #t) (k 0)) (d (n "xdg") (r "~2.2") (d #t) (k 0)))) (h "1s0r93smk70j9pp5ipbf40b3m7jligl444krndd560s2ivamxh1j")))

(define-public crate-psst-0.1.2 (c (n "psst") (v "0.1.2") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "rprompt") (r "~2.0") (d #t) (k 0)) (d (n "toml") (r "~0.7") (d #t) (k 0)) (d (n "xdg") (r "~2.4") (d #t) (k 0)))) (h "19hzq2v7vzjzrnyibbpqawcmw5wjv64vd8ldlmallyj007la5plb")))

