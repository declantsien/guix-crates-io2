(define-module (crates-io ps xm psxmem) #:use-module (crates-io))

(define-public crate-psxmem-0.1.0 (c (n "psxmem") (v "0.1.0") (d (list (d (n "deku") (r "^0.13.1") (d #t) (k 0)) (d (n "gif") (r "^0.11.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0vdlma2mbl4l35xldgf17bxlzqm4xdhwh3z44422pja341363ylx")))

(define-public crate-psxmem-0.1.1 (c (n "psxmem") (v "0.1.1") (d (list (d (n "deku") (r "^0.13.1") (d #t) (k 0)) (d (n "gif") (r "^0.11.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0nxfw7jaf1zskdk6yybblwz2hky2h9zn4mw2dbzc04lxzxf4krp1")))

(define-public crate-psxmem-0.1.2 (c (n "psxmem") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "deku") (r "^0.13.1") (d #t) (k 0)) (d (n "gif") (r "^0.11.4") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0fmhn0cm93npxl40jm87fz4w68fapf5dgx45dx5lif7zah2275z8")))

(define-public crate-psxmem-0.1.3 (c (n "psxmem") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "deku") (r "^0.16.0") (d #t) (k 0)) (d (n "gif") (r "^0.13.1") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1r04s17wjb3d2pbcfh4wfr823ba3q7j6rj0lf7abjkmn1d38wz72")))

