(define-module (crates-io ps -b ps-base64) #:use-module (crates-io))

(define-public crate-ps-base64-0.1.0-1 (c (n "ps-base64") (v "0.1.0-1") (h "08ayrgidlmcflydmp4np1lb7hpd8g4zr7460fgb2l60mykr4yw7y")))

(define-public crate-ps-base64-0.1.0-2 (c (n "ps-base64") (v "0.1.0-2") (h "1wkrqkkadqhw93glq70yaijibvbr1dcva3s315rpjz7gs7qby0nw")))

