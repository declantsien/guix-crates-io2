(define-module (crates-io fz _t fz_tod_r) #:use-module (crates-io))

(define-public crate-fz_tod_r-0.1.0 (c (n "fz_tod_r") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b65k4shazk8rvsi1bgdz2acshb0pyva0xl8mg45i4q0gnm22l7h")))

(define-public crate-fz_tod_r-0.1.1 (c (n "fz_tod_r") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "033h6sq5srayv22ai2mr02cnxhnpi70iq11mpf1vlqrkdf1law0k")))

(define-public crate-fz_tod_r-1.0.0 (c (n "fz_tod_r") (v "1.0.0") (d (list (d (n "fp_server") (r "^1.2") (d #t) (k 0)))) (h "0jkm27118yng5fi8vpzrprpgq6s33m3cs6r4mbg0j5gbzl7dv3rb")))

(define-public crate-fz_tod_r-1.1.0 (c (n "fz_tod_r") (v "1.1.0") (d (list (d (n "fp_server") (r "^1.2") (d #t) (k 0)))) (h "10hr1wphsxki7729krj5mx495zqd1nbjj7cf4j45rixsh0y0qwi9")))

(define-public crate-fz_tod_r-1.1.1 (c (n "fz_tod_r") (v "1.1.1") (d (list (d (n "fp_server") (r "^1.2") (d #t) (k 0)))) (h "10b8dcc0lipg57pkknb7g8iac32swr1gvivnjs6fzw66i5mqpymn")))

(define-public crate-fz_tod_r-1.1.2 (c (n "fz_tod_r") (v "1.1.2") (d (list (d (n "fp_server") (r "^1.2") (d #t) (k 0)))) (h "1gkzdfz4979wcy7xkwiccas4q8m42256676d3a8gmazbg5dpgx3d")))

