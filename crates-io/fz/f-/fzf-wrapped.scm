(define-module (crates-io fz f- fzf-wrapped) #:use-module (crates-io))

(define-public crate-fzf-wrapped-0.1.0 (c (n "fzf-wrapped") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)))) (h "0mcdijx8isk7qd375s9i0grmc5m8arqmi03366y33ch2cg3k6byi")))

(define-public crate-fzf-wrapped-0.1.1 (c (n "fzf-wrapped") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)))) (h "0n2kbjfvxzig7y6czyi4zkqb1c32dl7bkfjyv20lclsfr638akp5")))

(define-public crate-fzf-wrapped-0.1.2 (c (n "fzf-wrapped") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)))) (h "1vnzbyqz7mw2l5nbkldj2sdsi081fqimp9hw0rfdvxhgfmkwhasg")))

(define-public crate-fzf-wrapped-0.1.3 (c (n "fzf-wrapped") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)))) (h "01hfgixrr0zjz289y6grhgzpbp9nvnw229c52cn1qbwkvricxlg3")))

