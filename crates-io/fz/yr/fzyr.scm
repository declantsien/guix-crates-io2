(define-module (crates-io fz yr fzyr) #:use-module (crates-io))

(define-public crate-fzyr-0.1.0 (c (n "fzyr") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.2") (d #t) (k 0)))) (h "0l3m4g8ia3shk188x5n47d9b5jy0m4xdp9dmgf8gyvamw4hl75d0")))

(define-public crate-fzyr-0.1.1 (c (n "fzyr") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.2") (d #t) (k 0)))) (h "0x560y86v76y8q1zr1nngw5iii7ammrr2njs0q597kmvhkc8gm4z")))

(define-public crate-fzyr-0.1.2 (c (n "fzyr") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.11.2") (d #t) (k 0)))) (h "04k7hb7k54mwb9z9ba9hcrgxpnsjafm8a9md42h8ianxnffiq3az")))

