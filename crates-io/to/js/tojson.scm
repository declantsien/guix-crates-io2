(define-module (crates-io to js tojson) #:use-module (crates-io))

(define-public crate-tojson-0.1.0 (c (n "tojson") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0z2rkqcpvsnvl76ryx28bwmpvlzpibfz8dw6g7l0lkz63q6181an")))

(define-public crate-tojson-0.1.5 (c (n "tojson") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0hcx5acxvq4rlhhpvnvhwd58lk876hcn3l4c7fjjz40fnpgvfwlb")))

(define-public crate-tojson-0.1.6 (c (n "tojson") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "09w0132gmnbw04v9rfa8rzlwin26bd1vs1p8ymkrjzna64xnv19j")))

(define-public crate-tojson-0.2.6 (c (n "tojson") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1djagb782920l0g3sgpmcp3nbnzldd4iblsddv1a9vb4jriwdwaj")))

(define-public crate-tojson-0.2.7 (c (n "tojson") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0mizv022vpqzs1a9q05fqiz6nc2iw131aq2a0rv22iy8qskcfrry")))

(define-public crate-tojson-0.3.0 (c (n "tojson") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1q88k5iiilvgwadk01qch50a595dlajb0b83k4h1r2d8yhdi1qln")))

(define-public crate-tojson-0.3.1 (c (n "tojson") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1z3pv8hnq4q766r7f8hdl7v9ipw4927s9nj8wdlp6fp2gf2zk3bd")))

(define-public crate-tojson-0.3.2 (c (n "tojson") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1xzaxfwc6czfimdnn7p6hn4jhdzbphnwc4qc3lrz75l1g19gvpr3")))

