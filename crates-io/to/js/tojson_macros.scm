(define-module (crates-io to js tojson_macros) #:use-module (crates-io))

(define-public crate-tojson_macros-0.1.0 (c (n "tojson_macros") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "07pcv3sqhck948y717fa2cblp3cq5ybh2xhhpr2m17028r8yw7g6")))

(define-public crate-tojson_macros-0.1.1 (c (n "tojson_macros") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "13ijjgz8xn5iw64mal35l4ij016r7x0jk7981wjqw9wb498fjanm")))

(define-public crate-tojson_macros-0.1.2 (c (n "tojson_macros") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0wmjb43adks4x53xr2dza6ha4492bfn9gi372j74nxl0lyr1dpmq")))

(define-public crate-tojson_macros-0.1.3 (c (n "tojson_macros") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0msan45db2pv2lsffbcb3clgwvp02ih41a3yvccrigzg81x1xgg0")))

(define-public crate-tojson_macros-0.1.4 (c (n "tojson_macros") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "18ifvmns59jy450i0cxqxh4rm2czs4y2v01a6vksbwr40fh1vfjf")))

(define-public crate-tojson_macros-0.1.5 (c (n "tojson_macros") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0rsp4bndwfvl5mgv326iikmxl9pwhjziyb2gy6l42xcrp43kf1d7")))

(define-public crate-tojson_macros-0.1.6 (c (n "tojson_macros") (v "0.1.6") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "14d2rmg7kbz2sisg1nd3rx8nhxmsigjmllmszbmg2f8wr9qnh0d2")))

(define-public crate-tojson_macros-0.2.0 (c (n "tojson_macros") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0cqcfn8xhvam28ar96f7v2s92iacg02n3344dglak1raw95ryp2n")))

(define-public crate-tojson_macros-0.2.1 (c (n "tojson_macros") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "02jpc7jfdpkq5whmr8gyfdz581ap0jwrzgsxnja45spxx2ja3w7q")))

(define-public crate-tojson_macros-0.2.2 (c (n "tojson_macros") (v "0.2.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0ll0b5bgdnvxpfzdzq9p3f1rf173dqdw8pyyvc07g92ar1iiwhi8")))

(define-public crate-tojson_macros-0.2.3 (c (n "tojson_macros") (v "0.2.3") (d (list (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 2)))) (h "1f6pdplr8d9fyqwa9wsm9hnidk13c9crk10kyj3v86pkd2n5ksc1")))

(define-public crate-tojson_macros-0.2.4 (c (n "tojson_macros") (v "0.2.4") (d (list (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 2)))) (h "12252hqm8j61pk5w18lynsxv8ls25l7wa2faagshwi73hnzk4hp3")))

(define-public crate-tojson_macros-0.3.0 (c (n "tojson_macros") (v "0.3.0") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 2)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "0nqm6nj6b9hp3qdldmvxa78mayjg65bpdhx0rhr0d4mi625qc7y3")))

(define-public crate-tojson_macros-0.3.1 (c (n "tojson_macros") (v "0.3.1") (d (list (d (n "quote") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 2)) (d (n "syn") (r "^0.8") (d #t) (k 0)))) (h "08v3nznxrmx41lgmk7vqdywni1xjykpih9paybxw4j75j7gldlfg")))

(define-public crate-tojson_macros-0.3.2 (c (n "tojson_macros") (v "0.3.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 2)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "18bwjgrymjx8n5h73zi1ydhxkvchwxg1zf9ghpji10kdjdr92r2g")))

(define-public crate-tojson_macros-0.3.3 (c (n "tojson_macros") (v "0.3.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 2)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "02arnsgrcfcpz6vk9sbnjjskq21nrmgpd7ydnrjgswxb4hi05ppk")))

(define-public crate-tojson_macros-0.3.4 (c (n "tojson_macros") (v "0.3.4") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0bdkhyrgbsj5m9gsk77ahiacrlnimdrzzyak14mv4056s2k1c5km")))

