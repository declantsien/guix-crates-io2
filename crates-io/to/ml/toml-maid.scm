(define-module (crates-io to ml toml-maid) #:use-module (crates-io))

(define-public crate-toml-maid-0.1.0 (c (n "toml-maid") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "toml_edit") (r "^0.9.1") (d #t) (k 0)))) (h "0m9k38z1nkl2dxwcp1i607v5hs2llfych0dn3hw2lw936r13qzv1")))

(define-public crate-toml-maid-0.2.0 (c (n "toml-maid") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "toml_edit") (r "^0.9.1") (d #t) (k 0)))) (h "1gw826b13p5vqd54mgy0mi7dp6v47f1db5zg8dkcx7vpizj2g9zk")))

(define-public crate-toml-maid-0.3.0 (c (n "toml-maid") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "toml_edit") (r "^0.9.1") (d #t) (k 0)))) (h "0cpfbwdfnav7pcw4yc68n6k65by9bjd5i3rxkhwb5ywnxi041nzb")))

