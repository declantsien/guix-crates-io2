(define-module (crates-io to ml toml-span) #:use-module (crates-io))

(define-public crate-toml-span-0.1.0 (c (n "toml-span") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "0bgm9dabghvnrfcjkkc4fx83l62gjp3kf7nbzzh2i909lmpqki9j") (s 2) (e (quote (("serde" "dep:serde") ("reporting" "dep:codespan-reporting"))))))

(define-public crate-toml-span-0.2.0 (c (n "toml-span") (v "0.2.0") (d (list (d (n "codespan-reporting") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "1giagkahdzva4j3y4h4wl4n26yn00wnh6gzaah535z6iws6b779n") (s 2) (e (quote (("serde" "dep:serde") ("reporting" "dep:codespan-reporting"))))))

