(define-module (crates-io to ml toml-cfg) #:use-module (crates-io))

(define-public crate-toml-cfg-0.1.0 (c (n "toml-cfg") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1kxci2ncr2chgkq0556fb008p38sv0sf4q9dhadqkb6blbn4s23r")))

(define-public crate-toml-cfg-0.1.1 (c (n "toml-cfg") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1lghpk9mfz5h8b0qhih1kl543wfzv2sy1g5amiywq6j1k7snsdym")))

(define-public crate-toml-cfg-0.1.2 (c (n "toml-cfg") (v "0.1.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "10h2b28zmlwww7ljgj05y2k2pjmmk7y4x31ir0jynnlksyr096zq")))

(define-public crate-toml-cfg-0.1.3 (c (n "toml-cfg") (v "0.1.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1pqbajcna3pbw1g5168d55rqi013k7z7n988sa0vflklb04zbnwi")))

(define-public crate-toml-cfg-0.2.0 (c (n "toml-cfg") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "07d2wvji3c5aknhdiqm8rvpdc54nwvmc7s4jdqamq4yxillqgib8")))

