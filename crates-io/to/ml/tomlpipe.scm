(define-module (crates-io to ml tomlpipe) #:use-module (crates-io))

(define-public crate-tomlpipe-0.1.0 (c (n "tomlpipe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "1v9y4lkcp5nkkqxnk31chs9di7c3vkgsr0x0jsnasl4963zmj8hj")))

(define-public crate-tomlpipe-0.2.0 (c (n "tomlpipe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "1hj2l2cywlpj8az4adj4skzr3j086m6b580jb2j76x361y7gpc62")))

(define-public crate-tomlpipe-0.3.0 (c (n "tomlpipe") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.14") (d #t) (k 0)))) (h "19absnpai6ni0lx91bnfy510g5w91qgb1afd8nx8wvmiq2mag9jd")))

