(define-module (crates-io to ml toml_conf) #:use-module (crates-io))

(define-public crate-toml_conf-0.1.1 (c (n "toml_conf") (v "0.1.1") (d (list (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1grmfhijrq6l96igj5xxp5093qwgy9sgkfr4ypgj5i267xzha2s1")))

(define-public crate-toml_conf-0.1.4 (c (n "toml_conf") (v "0.1.4") (d (list (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0g34lykh3j8v1i4l6gfzgbs3g9bzmb732y2z58pkam9fd9fkw2w3")))

(define-public crate-toml_conf-0.1.5 (c (n "toml_conf") (v "0.1.5") (d (list (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1qvn7dsbcfgyk72kjllf5inkax9vm33d0wi7s21lcay79s6van6w")))

(define-public crate-toml_conf-0.1.6 (c (n "toml_conf") (v "0.1.6") (d (list (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0l9y3wp5aa9r90fdq55a68csk231grr94jbwsbwjym8rzyxrzmmg")))

(define-public crate-toml_conf-0.1.7 (c (n "toml_conf") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0r89rlsb2676r6797g4vaqm42vbbv9zavg8bgj1qva4jzg2z5ldx")))

(define-public crate-toml_conf-0.1.8 (c (n "toml_conf") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1rybcrqmvk597hhk5bjxjg6ramjl5k92dnndpkpkmhmb2xlald5p")))

(define-public crate-toml_conf-0.1.9 (c (n "toml_conf") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1x3pqzgw54a3qj9bmndbn001arm904gjzi2zzg538h2nkib6jakm")))

(define-public crate-toml_conf-0.1.10 (c (n "toml_conf") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "13jpclcr81sr563h0f3ywgcxh0v44iay9wnvw8dly07avbm78ky4")))

(define-public crate-toml_conf-0.1.11 (c (n "toml_conf") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0myram9c8091n50hs9l62lm1apqzs0anqaqdvkgfja4gz5nrg22i")))

(define-public crate-toml_conf-0.1.15 (c (n "toml_conf") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1pklac6l0wnzkcwqdx5yxs18c8z0l5whcz17aqf21x1dajdsasjp")))

