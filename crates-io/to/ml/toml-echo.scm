(define-module (crates-io to ml toml-echo) #:use-module (crates-io))

(define-public crate-toml-echo-0.1.0 (c (n "toml-echo") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1fjlnk8yq0p11j2zhmjrkz2s8bhzby0qh1z2dyr6mp7wnpvgp7yw")))

(define-public crate-toml-echo-0.2.0 (c (n "toml-echo") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1f5xc3nvqygahgld3ny85kkkvddqscv73xbl1z7i9jhza6fbgi80")))

(define-public crate-toml-echo-0.3.0 (c (n "toml-echo") (v "0.3.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19drnnqwhdd946sg57r5njna69ldwi10f1x96a2ynq005lgs0kkg")))

(define-public crate-toml-echo-0.4.0 (c (n "toml-echo") (v "0.4.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)))) (h "1qnj3zb683l71ybpjjlxis6hv9cm462chnbhknnkrqs0ww88c4l5")))

