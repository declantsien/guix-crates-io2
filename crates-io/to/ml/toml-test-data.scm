(define-module (crates-io to ml toml-test-data) #:use-module (crates-io))

(define-public crate-toml-test-data-1.0.0 (c (n "toml-test-data") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6") (d #t) (k 0)))) (h "1p2762k71hssn07vsw0h114agp05hfyrymdqdvzsp1cfhcn69swr")))

(define-public crate-toml-test-data-1.1.0 (c (n "toml-test-data") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.6.2") (d #t) (k 0)))) (h "10wxgdr1bpgpkjy7niy42dpc88sh2jibj5d4pd6y20hngz82cii6") (r "1.60.0")))

(define-public crate-toml-test-data-1.1.1 (c (n "toml-test-data") (v "1.1.1") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "199kvlbqrxssv64sznw3acwf162vpr0zz1sb5im3pa9jyavk6zkj") (r "1.60.0")))

(define-public crate-toml-test-data-1.2.0 (c (n "toml-test-data") (v "1.2.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "18bclnb5jzyxg6bkx10yiri3kv9nfzmnrfk7zkcz3mxh5aajmcgz") (r "1.60.0")))

(define-public crate-toml-test-data-1.3.0 (c (n "toml-test-data") (v "1.3.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "0ii6llfpz9fbcag2jkfz87s3cmphrnhkr9fln01fhph0ssv53wwk") (r "1.60.0")))

(define-public crate-toml-test-data-1.3.1 (c (n "toml-test-data") (v "1.3.1") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "00x5xdz4mwgx3yrfc6i93kq4i7wm5ld3qf8ih82xkylyqvvg0h23") (r "1.66.0")))

(define-public crate-toml-test-data-1.4.0 (c (n "toml-test-data") (v "1.4.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "0bgdwyjsqgpwwi5s1w483a1g3qrwmq0l3742k07575qzcc6sx0h0") (r "1.67")))

(define-public crate-toml-test-data-1.5.0 (c (n "toml-test-data") (v "1.5.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "1z4xrmfzkyx7ck7vmf53hr5a18x3rdrcrqj3i089c7fk9rjv7mxn") (r "1.67")))

(define-public crate-toml-test-data-1.6.0 (c (n "toml-test-data") (v "1.6.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "0xjs1nibky2jhly7dxm5bm48cwwhmaz9inrx45dsx4fpqz0h0l3l") (r "1.67")))

(define-public crate-toml-test-data-1.7.0 (c (n "toml-test-data") (v "1.7.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "0bqkpaq5szcphiqg9wdr0xjgdjqgrd9lng8jddy5grgghdjy14dd") (r "1.67")))

(define-public crate-toml-test-data-1.8.0 (c (n "toml-test-data") (v "1.8.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "0pf13igrg0rm5fmy1sj57y9m313wyfwwjrqxgp3fhg41kvcvmdf6") (r "1.69")))

(define-public crate-toml-test-data-1.8.1 (c (n "toml-test-data") (v "1.8.1") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "1p2298varvj1v13cb2nild7zws4p7llgzqdvykaxbj5ns27q4lwm") (r "1.65")))

(define-public crate-toml-test-data-1.9.0 (c (n "toml-test-data") (v "1.9.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "085w6r0c3kzdcw7y3k3wq9nk3a5pjkicv41qldzzk5nz5anzls33") (r "1.65")))

(define-public crate-toml-test-data-1.10.0 (c (n "toml-test-data") (v "1.10.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "0jcc3b1aq97v7ps9n7fplc1l4k10pkxb7w0ih7h6q5pf1p3lh16c") (r "1.65")))

(define-public crate-toml-test-data-1.11.0 (c (n "toml-test-data") (v "1.11.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "135hxcwxfmakdhczgfvxvkbfy8qr3kkxv3yjxwkhc6z12pxbwggf") (r "1.65")))

(define-public crate-toml-test-data-1.12.0 (c (n "toml-test-data") (v "1.12.0") (d (list (d (n "include_dir") (r "^0.7.0") (d #t) (k 0)))) (h "0xmj2y978dzi0xp63h8ismrqjdc16n9b7g7jl6ny9352w797pdya") (r "1.65")))

