(define-module (crates-io to ml toml-fmt) #:use-module (crates-io))

(define-public crate-toml-fmt-0.1.0 (c (n "toml-fmt") (v "0.1.0") (d (list (d (n "toml") (r "^0.5.1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0z876w3cfpgarbf8n89kfmpqlz6p3hw97qm8nb7mp77g6m556nj4")))

