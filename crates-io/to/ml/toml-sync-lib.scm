(define-module (crates-io to ml toml-sync-lib) #:use-module (crates-io))

(define-public crate-toml-sync-lib-0.1.0 (c (n "toml-sync-lib") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15sxm515mg34hg0gpvpvskb3winc6zkkcc414k95yybvqdaxz215")))

