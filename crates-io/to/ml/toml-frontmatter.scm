(define-module (crates-io to ml toml-frontmatter) #:use-module (crates-io))

(define-public crate-toml-frontmatter-0.1.0 (c (n "toml-frontmatter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "insta") (r "^1.24.1") (f (quote ("toml"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "04f7ll7imxjw4vrgas5nrp7nxn6bn99r9dfp40f0j0zddy35x1pm")))

