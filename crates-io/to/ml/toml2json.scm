(define-module (crates-io to ml toml2json) #:use-module (crates-io))

(define-public crate-toml2json-0.9.11 (c (n "toml2json") (v "0.9.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0k0kszp2c4w301n7d157ri3r928a1b0jz0g6fcn5ka0arvm3p25s")))

(define-public crate-toml2json-0.9.12 (c (n "toml2json") (v "0.9.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "14h6n5pnhc35gy5j6n05a9dfqy1d0cdkxrf929iq932n5zarcfz9")))

(define-public crate-toml2json-1.0.0 (c (n "toml2json") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1qiy92cls6198b2x1wjj3d1yq3jbk929zpysfdvvcc36h0m0zggz")))

(define-public crate-toml2json-1.1.0 (c (n "toml2json") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1v2093d89prw5m9g61pf8yl1x5v679mwq5c53rnszgdc4ykabfh6")))

(define-public crate-toml2json-1.2.0 (c (n "toml2json") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "049pj4x413rfh0facl7w2l1n7pg8li51l58bjhn9bdidhz1pl43v")))

(define-public crate-toml2json-1.3.0 (c (n "toml2json") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1g74g5h94xnc63h06jhb470i9z5li7zcw4xmlxwx0bh4sqddwiv0")))

(define-public crate-toml2json-1.3.1-rc.1 (c (n "toml2json") (v "1.3.1-rc.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1v05i2gfyrxp2ia4dhxx6g2kc8089x162chcgvahg7yj2m6d0grk")))

(define-public crate-toml2json-1.3.1 (c (n "toml2json") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0a3rbnx02iasd9cbs4f54w94m2d4sv63rdlndpbqjsssipdwzq64")))

