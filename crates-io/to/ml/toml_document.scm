(define-module (crates-io to ml toml_document) #:use-module (crates-io))

(define-public crate-toml_document-0.1.0 (c (n "toml_document") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0kr8ycllsy3y95rhnpfy03n6w2bdbcdq9rkj6d1f17nhz58ylxmd")))

(define-public crate-toml_document-0.1.1 (c (n "toml_document") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0r01sfx7493irraa1x54an13grhcz65myacazvr35xx8ilqyfz68")))

(define-public crate-toml_document-0.1.2 (c (n "toml_document") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1gl9nnblgykj4vcqz7i5ccwx596wyajdhjpq3qdspb6c5jcyczlp")))

(define-public crate-toml_document-0.1.3 (c (n "toml_document") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0qlbwdrx83siqc3pl5iyys4gb5p283fsq3awxv3ljxym3zj5nqda")))

