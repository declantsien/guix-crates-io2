(define-module (crates-io to ml toml_schema) #:use-module (crates-io))

(define-public crate-toml_schema-0.1.0 (c (n "toml_schema") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "05f6kmfa9zaib1fych4r0k5shg31isni09rhfd24bj9szzj4xvnn")))

(define-public crate-toml_schema-0.2.0 (c (n "toml_schema") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0r74kqgnndvbagcs3vlasgbcs5sgw6lxhpjqb7zgyzvfx0rb0ax2")))

(define-public crate-toml_schema-0.2.1 (c (n "toml_schema") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1s2ifrh8kyq4v0hjrf5szn0sy78hggpbf01y6m1i65bcz8h3l58z")))

