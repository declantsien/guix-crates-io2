(define-module (crates-io to ml tomlconf) #:use-module (crates-io))

(define-public crate-tomlconf-1.0.0 (c (n "tomlconf") (v "1.0.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0xndxjbnsndp1fwpjvbpmzbbkj5rvyzw0n8bkvx662r8fg9jqi1j") (f (quote (("nightly") ("default"))))))

(define-public crate-tomlconf-1.1.0 (c (n "tomlconf") (v "1.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "15v42wbm0j2vwhxrgd6g4h0hr61vabwv8srxm1z2hy5hmnqn9afl") (f (quote (("nightly") ("default"))))))

(define-public crate-tomlconf-1.1.1 (c (n "tomlconf") (v "1.1.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1zg3rd500q224k3kifgzkb1yvwqh453g8m4mwc126f4yj4xnixc8") (f (quote (("nightly") ("default"))))))

