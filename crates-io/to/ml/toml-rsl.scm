(define-module (crates-io to ml toml-rsl) #:use-module (crates-io))

(define-public crate-toml-rsl-0.1.0 (c (n "toml-rsl") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "188sciylkscfwq58bql26w7nn55zidgy397i7vka53a49vrwrj88") (f (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-toml-rsl-0.1.1 (c (n "toml-rsl") (v "0.1.1") (d (list (d (n "crypto-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "ssh2-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "tar-rsl") (r "^0.1.0") (d #t) (k 0)) (d (n "xml-rsl") (r "^0.1.0") (d #t) (k 0)))) (h "14rfka9gl4ynhy3dbg27f2bvzm3smpvgdfyjjl87f2qllakwmcmh") (f (quote (("preserve_order" "indexmap") ("default")))) (y #t)))

