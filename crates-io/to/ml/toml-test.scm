(define-module (crates-io to ml toml-test) #:use-module (crates-io))

(define-public crate-toml-test-0.1.0 (c (n "toml-test") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "0c9dqhd1y99z1f6k6baf5nm8yp36xwz2vkbfdah6vz60p42jwkmx")))

(define-public crate-toml-test-0.2.0 (c (n "toml-test") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "1wy81v0p4hjdv8y10cqq11ixdf20qsp6z4j32cwdr98jc8zhhkr0")))

(define-public crate-toml-test-0.2.1 (c (n "toml-test") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "06dgnzmvc4mbzjjsbqgn4azx412jb4kqcilnwkxl23562xf9njf0")))

(define-public crate-toml-test-0.2.2 (c (n "toml-test") (v "0.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "1fh3gimbgyf7mnvlr53qbr5y85jqq5yrl2kvq25shl12dpd9wn04")))

(define-public crate-toml-test-0.2.3 (c (n "toml-test") (v "0.2.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "0i5ln66rghh1z0xm2g9nx754ilfqnjs43qjqlm4p4q1bz2ywz2wr")))

(define-public crate-toml-test-0.2.4 (c (n "toml-test") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "028n7b0ydxbgzh8wd3ac2l6vayyzm7dnif8gn6v27xk5sz8187zz")))

(define-public crate-toml-test-0.2.5 (c (n "toml-test") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "09sj6y2ilxkw6p8x0xi3ybmnf75s5h8zn2dc4049ivdzkgx6rn1k")))

(define-public crate-toml-test-0.3.0 (c (n "toml-test") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "0vrr7mz8b6v9pj0fsr32slmlkx3gbwzl1gishsmdxcpah3rsgd7j")))

(define-public crate-toml-test-0.3.1 (c (n "toml-test") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "1n0q8s27568xvkczyvhlannbf79k4rl5gmrl5wvazzlf8ii4ab7d")))

(define-public crate-toml-test-0.3.2 (c (n "toml-test") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "0s5vxg0302d3y4zigpnhi2grarhwb6wwbnsdqf7j91c3xkmyw84v") (r "1.60.0")))

(define-public crate-toml-test-0.3.3 (c (n "toml-test") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "01z5v5gdk61l86ibmhp66xjj12qq2cz7v4kcds9pbaabv7bk9h5f") (r "1.60.0")))

(define-public crate-toml-test-0.3.4 (c (n "toml-test") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "1hvrlzcn6x07dq6h9sgkjbi75wz7an7g0kqgsvbdp88ag5b14d9p") (r "1.60.0")))

(define-public crate-toml-test-0.3.5 (c (n "toml-test") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "042dgaqjk1v715my4rziqdadylsad31ny4y6askx878xvbw0r3s8") (r "1.60.0")))

(define-public crate-toml-test-0.3.6 (c (n "toml-test") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "015v6fysgc2l3hwi38crf8q4drj50d78hmhi2nrr2r9xr8czlzd4") (r "1.66.0")))

(define-public crate-toml-test-0.3.7 (c (n "toml-test") (v "0.3.7") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("std"))) (k 0)) (d (n "ryu") (r "^1.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "00apszbv7wlji2qzbjkg567pkmd0piky22yk0vx1zd0k00lzcvzp") (r "1.66.0")))

(define-public crate-toml-test-1.0.0 (c (n "toml-test") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("std"))) (k 0)) (d (n "ryu") (r "^1.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "1h0lfd4bsix6c7mqlqg9r73dgc34b8kmh5vym20y2cgv6ll8khqy") (r "1.67")))

(define-public crate-toml-test-1.0.1 (c (n "toml-test") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("std"))) (k 0)) (d (n "ryu") (r "^1.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "toml-test-data") (r "^1") (d #t) (k 2)))) (h "09fgp2g9swp1930gfv6qvlg78ca3p19ix9aklym8j1kdj8hnh7q9") (r "1.65")))

