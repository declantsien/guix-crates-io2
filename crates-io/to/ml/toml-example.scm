(define-module (crates-io to ml toml-example) #:use-module (crates-io))

(define-public crate-toml-example-0.0.1 (c (n "toml-example") (v "0.0.1") (d (list (d (n "toml-example-derive") (r "=0.0.1") (d #t) (k 0)))) (h "0rw4j9j1vp16glsbswv6z3c6hmzgylxkiddki0bzb4v7f8mw5iki") (y #t)))

(define-public crate-toml-example-0.1.0 (c (n "toml-example") (v "0.1.0") (d (list (d (n "toml-example-derive") (r "=0.1") (d #t) (k 0)))) (h "1vjzzmx1zmgirp987ykls7yan30xmf3gpiyb2bb99da8a2maqakw")))

(define-public crate-toml-example-0.2.0 (c (n "toml-example") (v "0.2.0") (d (list (d (n "toml-example-derive") (r "=0.2") (d #t) (k 0)))) (h "0li0rj25kdb4q88ys869rbxmh9kj7cxv89iwfzz8mcwlj2dd6p49")))

(define-public crate-toml-example-0.3.0 (c (n "toml-example") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.3") (d #t) (k 0)))) (h "1a8jlc9rkck3hqlcsdgpmrig46rd1iy168snw364xqlg9sg5z3dd")))

(define-public crate-toml-example-0.4.0 (c (n "toml-example") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.4") (d #t) (k 0)))) (h "0hz3fz468sjzgw9h32fg0q0xb9dhx7csk8bidx4p683hw4dhmvbn")))

(define-public crate-toml-example-0.4.1 (c (n "toml-example") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.4") (d #t) (k 0)))) (h "1vdn18h57q5sz35rcpwx36z4hka58qp8prj8xwm53cn8n0bfcj4v")))

(define-public crate-toml-example-0.5.0 (c (n "toml-example") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.5") (d #t) (k 0)))) (h "1lk9nwjpg3hzs148ldqz02fsg37mbaqmx2djgj1ps4m3dc2fch52") (y #t)))

(define-public crate-toml-example-0.5.1 (c (n "toml-example") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.5") (d #t) (k 0)))) (h "1dxry1cqi14y9ynnvlbyp95rlji6i6m7hzgmh7nxxgb5p5s14w4i") (y #t)))

(define-public crate-toml-example-0.6.0 (c (n "toml-example") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.6") (d #t) (k 0)))) (h "0bxckwrf8xx4807fv1pl2rpnblsmnmmaz3kai9rgx0n9iqd9d0zs") (y #t)))

(define-public crate-toml-example-0.6.1 (c (n "toml-example") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.6.1") (d #t) (k 0)))) (h "10lg76kcrdy8sks5xajpp4njylw5yfjw9rmw51w7fg6f3239s5s2")))

(define-public crate-toml-example-0.7.0 (c (n "toml-example") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.7") (d #t) (k 0)))) (h "112d4qgfl086cph40amdra251zpsg4lwl9przs1827jk37r3bpyy") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.7.1 (c (n "toml-example") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.7.1") (d #t) (k 0)))) (h "1ibzfy10gz18xadgasxhqwcdm23vp33hm4ykq4hwmww9q1xap33f") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.7.2 (c (n "toml-example") (v "0.7.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.7.2") (d #t) (k 0)))) (h "08z514daqag4gl3nh1i5ll8xdibz41y9b427k6lmhl1vcrm6inlz") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.8.0 (c (n "toml-example") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.8.0") (d #t) (k 0)))) (h "0fcxnbzddyqxg5fvy20h5mpfp743p8vdx4rb9fv5imgqjvcp3978") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.8.1 (c (n "toml-example") (v "0.8.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.8.1") (d #t) (k 0)))) (h "14kvhgfqqpnhycainhvyyzxhi9h63962xpjs3l5n2dg9v17x6bnv") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.8.3 (c (n "toml-example") (v "0.8.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.8.3") (d #t) (k 0)))) (h "0msx4pb542vjdvb8899b4zjpn1cn4gh7qfnb3hvwrbpzgrii28z2") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.8.4 (c (n "toml-example") (v "0.8.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.8.4") (d #t) (k 0)))) (h "1l0fw0ncy5wf1px7il9pr6m3cagvv70m5v0k564sjyc5sf55wrck") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.9.0 (c (n "toml-example") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.9.0") (d #t) (k 0)))) (h "050j4cn6ay9rdlxkv1nmv7s5hdhkggi24xvg2qag6xnrj7jd2db7") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.10.0 (c (n "toml-example") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.10.0") (d #t) (k 0)))) (h "0mdag89b79761gf0k5vcyiy9w7ms6ym24jpz2fhj306j1mjfdznj") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.10.1 (c (n "toml-example") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.10.1") (d #t) (k 0)))) (h "1hwzzyk4kw1jnijn5ngsl57g2ia9hnb1kipi2nbq3dk3gbgfg0wv") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.10.2 (c (n "toml-example") (v "0.10.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.10.2") (d #t) (k 0)))) (h "0qpq844wvl0msbgi3bqng947mfv38gxl18adv2y3grrhd7kli9im") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.10.3 (c (n "toml-example") (v "0.10.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.10.3") (d #t) (k 0)))) (h "0blykg8dl5lminilsmhla6180hv31nfa6qhhx47lnnmczfja2xxr") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.11.0 (c (n "toml-example") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.11.0") (d #t) (k 0)))) (h "0l2clmhdr8d23skpyjzy7a6ywr8rdapg76d37cwgwzd8j83qzcz6") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

(define-public crate-toml-example-0.11.1 (c (n "toml-example") (v "0.11.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 2)) (d (n "toml-example-derive") (r "=0.11.1") (d #t) (k 0)))) (h "0crqyj8py8bwykrx9nzyxpwnzpwxn6vr6ypla0v972zpd2nqi3v2") (f (quote (("serde" "toml-example-derive/serde") ("default" "serde"))))))

