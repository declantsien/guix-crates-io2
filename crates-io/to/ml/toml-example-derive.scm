(define-module (crates-io to ml toml-example-derive) #:use-module (crates-io))

(define-public crate-toml-example-derive-0.0.1 (c (n "toml-example-derive") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1q017bdhh7xp33rhy7gq32wv7gvwqyx0k7az4p5iz8hmb86ffry2")))

(define-public crate-toml-example-derive-0.1.0 (c (n "toml-example-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03wphwidvwgjpwh3jkvxka6si5d6yl72gwg9dw7jhw9zaf05jfyn")))

(define-public crate-toml-example-derive-0.2.0 (c (n "toml-example-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0g0gwg93x2hsdzcwmwyj0rqgjq2g9hfsyfww389hslm37zwirdj6")))

(define-public crate-toml-example-derive-0.3.0 (c (n "toml-example-derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "18z074xgwg7h6p5xk54kyr3pwnvmx592mc9wa479sqqarmw9rshy")))

(define-public crate-toml-example-derive-0.4.0 (c (n "toml-example-derive") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0pxs8a1kd1apfdj2if8p9vi2winsl69qx6g2wrsl958j6rr1ypsj")))

(define-public crate-toml-example-derive-0.4.1 (c (n "toml-example-derive") (v "0.4.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1rxvkx1ffqh4xksdvj29g7jas86p3fd8kh7k4ps8b0c45m8b4ks0")))

(define-public crate-toml-example-derive-0.5.0 (c (n "toml-example-derive") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1pkfx4ibaxvxn44rmvayq49bhxp3cgh46f8xkg1j74ys81bnhswx")))

(define-public crate-toml-example-derive-0.5.1 (c (n "toml-example-derive") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1y8hq0jb6vqzgqxv50lyp0r6jv62qdywnivpgf4vfk3hvn923s5p")))

(define-public crate-toml-example-derive-0.6.0 (c (n "toml-example-derive") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "14xpnglh9y37gbpd2hf01s92i9js9jjgxac7mj7x92axpdgg52jp")))

(define-public crate-toml-example-derive-0.6.1 (c (n "toml-example-derive") (v "0.6.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0a93qgccm1jysi1vaf6qr8szc4557cxdqzhw1payrqddysfqj9rh")))

(define-public crate-toml-example-derive-0.7.0 (c (n "toml-example-derive") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1zf7795wqlm0wxncllc18ayqyp0kplp4zxwig44qj40vgw0hbgdc") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.7.1 (c (n "toml-example-derive") (v "0.7.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "18jiwplzl2yrk9j6a6lxd4a8xp88jdxhf3r1l39152wj1ylwpplk") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.7.2 (c (n "toml-example-derive") (v "0.7.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "19bxa7pdrx6lvgw1xq8khz61w325vw6szb7r09w1sanl30c0qrz5") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.8.0 (c (n "toml-example-derive") (v "0.8.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1dq0ml3db9lsx0f33m5b7v4di1zzfbasrz879dp51vzbb5v95yxi") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.8.1 (c (n "toml-example-derive") (v "0.8.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "06qqzd7ci7xrxqd5idjb98wr7lv89vr0qwf3dimdns3qk1akvzcn") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.8.3 (c (n "toml-example-derive") (v "0.8.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1604r2sq057fmc0gmg4q48zvnhc52lid69mivfg9jlpzxh7bvqxy") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.8.4 (c (n "toml-example-derive") (v "0.8.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "013rmxn0ynx7k2mdnm403mvnyjvb0047ya4jksa5vrckffakharg") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.9.0 (c (n "toml-example-derive") (v "0.9.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1dnxbiymwbmkga9m252z15qmdxmx6j7cbjkm44w7l2gfa6asxj75") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.10.0 (c (n "toml-example-derive") (v "0.10.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "141x3rlsxh46zgwxn04bbcvdc1f9gwiafpqx1ff8xcw7l68m9hdy") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.10.1 (c (n "toml-example-derive") (v "0.10.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0pycaj4v8rgmkimvj3fbby27kgqfd91dlgpkjs2l6g26g2qprr0x") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.10.2 (c (n "toml-example-derive") (v "0.10.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0nw3f0pm2p6qg5cnk6r6z19kp8c4cgf4ndff9gmkhh8pqjrrn190") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.10.3 (c (n "toml-example-derive") (v "0.10.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1p2skaa2c4jvlnz0w49148a9xynmra7ivdc3mvnjzra4q71hw1z0") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.11.0 (c (n "toml-example-derive") (v "0.11.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1n48j191yflgpkcqpkhvsnggfpr9g55rkqzsw27a4yhkimdhwjly") (f (quote (("serde"))))))

(define-public crate-toml-example-derive-0.11.1 (c (n "toml-example-derive") (v "0.11.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0jcr35g4cwjifqaw09gs13qymlwm8y13wdih04cwcbcf8wz72p13") (f (quote (("serde"))))))

