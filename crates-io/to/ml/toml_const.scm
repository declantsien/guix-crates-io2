(define-module (crates-io to ml toml_const) #:use-module (crates-io))

(define-public crate-toml_const-0.1.0 (c (n "toml_const") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1g7r1z4nxywkc7i2vk60l7gdncq2bsnbf4ax1165lhw3afrgj5sc") (r "1.56")))

(define-public crate-toml_const-0.1.1 (c (n "toml_const") (v "0.1.1") (d (list (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0qhw89gpc9gd01cv76l12n02chj5a7j8z4pdk8niyx4hgfqzhmhb") (r "1.56")))

