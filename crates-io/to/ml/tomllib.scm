(define-module (crates-io to ml tomllib) #:use-module (crates-io))

(define-public crate-tomllib-0.1.0 (c (n "tomllib") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "nom") (r "^1.2.0") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "regex") (r "^0.1.48") (d #t) (k 0)))) (h "1nhs8wi0fyajsqygk3nv2pb2ai3m5yhgrmmz377r5w21l0h311v2")))

(define-public crate-tomllib-0.1.1 (c (n "tomllib") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "nom") (r "^1.2.0") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "regex") (r "^0.1.48") (d #t) (k 0)))) (h "0mqdnjk0xzr0z8bkzg2jcgi13qivdyr0hjc6lr00gg6zp3zapv3q")))

(define-public crate-tomllib-0.1.2 (c (n "tomllib") (v "0.1.2") (d (list (d (n "csv") (r "^0.14.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "nom") (r "^1.2.0") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pirate") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.48") (d #t) (k 0)))) (h "03kh1sq9g5p2xp3zl45582qbwlrgr43mh9qb88jwnh70h8b6mh0y")))

