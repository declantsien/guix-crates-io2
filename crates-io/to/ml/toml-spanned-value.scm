(define-module (crates-io to ml toml-spanned-value) #:use-module (crates-io))

(define-public crate-toml-spanned-value-0.1.0 (c (n "toml-spanned-value") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.4") (d #t) (k 0)))) (h "17sdrw6h69cdi2zn0s9wd93wl80whsbnvhabnkz6c3ks5cqr26ca") (f (quote (("preserve_order" "indexmap") ("default"))))))

