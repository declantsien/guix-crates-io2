(define-module (crates-io to ml toml-test-harness) #:use-module (crates-io))

(define-public crate-toml-test-harness-0.1.0 (c (n "toml-test-harness") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.3.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.0") (d #t) (k 0)))) (h "08vfhf5f918lbjvcr21198bybkxpxs5r86147gfdmh9nihkw6sry")))

(define-public crate-toml-test-harness-0.2.0 (c (n "toml-test-harness") (v "0.2.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.3.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.2") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.0") (d #t) (k 0)))) (h "1b54mjrixk3imi669w5k0rd7zlw1l6iqxb0ncg14q1pwlf1m7gn3")))

(define-public crate-toml-test-harness-0.3.0 (c (n "toml-test-harness") (v "0.3.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.3.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.0") (d #t) (k 0)))) (h "07w0sxlsrhf1ym8mlisdkahj5r38c6391vz0rfdxs30hxhbc2va1")))

(define-public crate-toml-test-harness-0.4.0 (c (n "toml-test-harness") (v "0.4.0") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.5.2") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.0") (d #t) (k 0)))) (h "1ywcnmhyrl13dw7mfyp0nx29nki7gh3b5z04yyhag1fi48jfdg2z") (r "1.60.0")))

(define-public crate-toml-test-harness-0.4.1 (c (n "toml-test-harness") (v "0.4.1") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.5.2") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.0") (d #t) (k 0)))) (h "1hb7nlrns8c101ks5gjfwlb33nqfaahidnnl9y6j70y4bkm87whp") (r "1.60.0")))

(define-public crate-toml-test-harness-0.4.2 (c (n "toml-test-harness") (v "0.4.2") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.5.2") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.1") (d #t) (k 0)))) (h "03jmxy6haq13qyvpskljsz05vw3xxazrfhph1y6zbid15rwz3n4x") (r "1.60.0")))

(define-public crate-toml-test-harness-0.4.3 (c (n "toml-test-harness") (v "0.4.3") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.3") (d #t) (k 0)))) (h "0xgn9a7q3s46fdxw6jajcbwxc0y3a186mgq561mzw8h9f6jzs00f") (r "1.60.0")))

(define-public crate-toml-test-harness-0.4.4 (c (n "toml-test-harness") (v "0.4.4") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.3") (d #t) (k 0)))) (h "0knh0xqyf11r60rxq0lgqf0mhgf57cqp3x096dwh8hlzx0z2gdia") (r "1.66.0")))

(define-public crate-toml-test-harness-0.4.5 (c (n "toml-test-harness") (v "0.4.5") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.3") (d #t) (k 0)))) (h "15h274kr5sig6in68bw85nkkaylcgz38nmayl78kmlv5b72cqikk") (r "1.66.0")))

(define-public crate-toml-test-harness-0.4.6 (c (n "toml-test-harness") (v "0.4.6") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.3") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.3") (d #t) (k 0)))) (h "0qp29lwr9p03l2f0ajmp2qlx2h8w3jjqj7kqraxzcc6b7l2y6y9b") (r "1.66.0")))

(define-public crate-toml-test-harness-0.4.7 (c (n "toml-test-harness") (v "0.4.7") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.0") (d #t) (k 0)) (d (n "toml-test") (r "^0.3.7") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.3.1") (d #t) (k 0)))) (h "0iijvb5lkr3mkmpmrbap6jcp03w318z4df0a4kpblxly5njmkx39") (r "1.66.0")))

(define-public crate-toml-test-harness-0.4.8 (c (n "toml-test-harness") (v "0.4.8") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.6.0") (d #t) (k 0)) (d (n "toml-test") (r "^1.0.0") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.4.0") (d #t) (k 0)))) (h "0pd02rdsq2bdw5cm89mqm0sw3yfpl4iddwg9ji31prfyc7bvir0v") (r "1.67")))

(define-public crate-toml-test-harness-0.4.9 (c (n "toml-test-harness") (v "0.4.9") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 0)) (d (n "toml-test") (r "^1.0.1") (d #t) (k 0)) (d (n "toml-test-data") (r "^1.8.1") (d #t) (k 0)))) (h "04xaf20jhbxbqh26jyf8shfhbxaa8i9k61qcnv8m9k148j74na5i") (r "1.65")))

