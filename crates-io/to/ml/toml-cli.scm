(define-module (crates-io to ml toml-cli) #:use-module (crates-io))

(define-public crate-toml-cli-0.1.0 (c (n "toml-cli") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "0q57nsb1rnzshwrwy8phs429ai8lq8sig7qv9vyy3pjxxs4w68hm")))

(define-public crate-toml-cli-0.2.0 (c (n "toml-cli") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "1lpafl6k6fzkxg7jbhpc9x1lq30mpkfc73vm5imi7353qck77bcd")))

(define-public crate-toml-cli-0.2.1 (c (n "toml-cli") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15") (d #t) (k 0)))) (h "0vcr9qc9v95j9pmp8bhwhlg0380qgz2ixk6aa3fm2ba454v1fy6c")))

(define-public crate-toml-cli-0.2.2 (c (n "toml-cli") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15") (d #t) (k 0)))) (h "178wb2z5rgvlclxw9xwbz1k3z41v8ql4pkj7y64w78aj6lz62x8n")))

(define-public crate-toml-cli-0.2.3 (c (n "toml-cli") (v "0.2.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "toml_edit") (r "^0.15") (d #t) (k 0)))) (h "1s192dwq3bgidnbi8iax09bn6r03dxjkg7vvwabsfzr2viabjdcl")))

