(define-module (crates-io to ml toml2lua) #:use-module (crates-io))

(define-public crate-toml2lua-0.1.0 (c (n "toml2lua") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.9") (d #t) (k 0)))) (h "0hnmb645848a8kkkcv1q5xfp3gnsl53d5fzyzy2p1mn6nlz6dvwg")))

