(define-module (crates-io to ml toml-db) #:use-module (crates-io))

(define-public crate-toml-db-0.1.0 (c (n "toml-db") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1bqbpbwg1g89x7gcy6x8mfvvxqkana8wf4v8bc2r51gy56lcm9wd")))

