(define-module (crates-io to ml tomlplate) #:use-module (crates-io))

(define-public crate-tomlplate-0.1.0 (c (n "tomlplate") (v "0.1.0") (h "0h81yn57w9kj1gi2r3xbv07kb5zx4mpxzjysw24i1jzq3sc3bham")))

(define-public crate-tomlplate-0.2.0 (c (n "tomlplate") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "17xqbw9vxwmwkyqdbdq9lhvsy8cnp1f4m8drc7zjhfibp7ras053")))

(define-public crate-tomlplate-0.4.0 (c (n "tomlplate") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03jr1rgkyy3sci2qap5cq9g35im6szg8mnvzg7hkfm2lpg1h3m4p")))

