(define-module (crates-io to ml toml-loader) #:use-module (crates-io))

(define-public crate-toml-loader-0.1.1 (c (n "toml-loader") (v "0.1.1") (d (list (d (n "quick-error") (r "^0.1.3") (d #t) (k 0)) (d (n "tempfile") (r "~1.1.1") (d #t) (k 2)) (d (n "toml") (r "^0.1.22") (d #t) (k 0)))) (h "0qppk6aldqyxwmazxfp4pw6rk7inskpwg58aw80n3n4d6hy1y34n")))

