(define-module (crates-io to ml toml-base-config) #:use-module (crates-io))

(define-public crate-toml-base-config-0.1.0 (c (n "toml-base-config") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0arihjk9p769mrks00d0y89dyi7b4fwxscdqji6785jhkzz602qm")))

