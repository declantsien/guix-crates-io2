(define-module (crates-io to ml toml_macros) #:use-module (crates-io))

(define-public crate-toml_macros-0.1.0 (c (n "toml_macros") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0p7cds3d9szf5bhfb5n8wlwqyb2m3g8da33dfy8ndcan8p9s7az9")))

