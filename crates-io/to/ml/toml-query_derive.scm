(define-module (crates-io to ml toml-query_derive) #:use-module (crates-io))

(define-public crate-toml-query_derive-0.9.0 (c (n "toml-query_derive") (v "0.9.0") (d (list (d (n "darling") (r "^0.8") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0dakicg6gil2h0arb11c7ipi9l42p13qzrcfbkkwfwy2bqjcm69w")))

(define-public crate-toml-query_derive-0.9.2 (c (n "toml-query_derive") (v "0.9.2") (d (list (d (n "darling") (r "^0.8") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "16yxd9d0kmrq93jsmkviz8d57kxil6z87x1jzjri4pmwgz6am2sj")))

(define-public crate-toml-query_derive-0.10.0 (c (n "toml-query_derive") (v "0.10.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1wsd95j5j7lzk6lykrw4v5hi55ijcxp19h154r6ilc1mp6mqhfdy")))

