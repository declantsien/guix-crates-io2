(define-module (crates-io to ml tomlenv) #:use-module (crates-io))

(define-public crate-tomlenv-0.1.0 (c (n "tomlenv") (v "0.1.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "15sb8dq7fspliww6smidv3xabdnmvl5fj2vjl6aa03kgzw2fz13c")))

(define-public crate-tomlenv-0.2.0 (c (n "tomlenv") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "04z3jjwf0ig37iypqcj8gwp8v57lg05867qsg0jhwrkj8xqvx8rx")))

(define-public crate-tomlenv-0.2.1 (c (n "tomlenv") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 2)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "166myf5zfjdd1n7029sdxl6ryy7rjzkz6h79flxcv1f60m96z9sc")))

(define-public crate-tomlenv-0.3.0 (c (n "tomlenv") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "0p0s3dwqpdk2zj8fyy1w1vfagjm2jdb9lz0rswqmk0f8q6bgy88n")))

(define-public crate-tomlenv-0.3.1 (c (n "tomlenv") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "0v0v0bahv0qjb2jj7ih8275qwqnl8hlkmxcl172n8f7iyr8saang")))

(define-public crate-tomlenv-0.3.2 (c (n "tomlenv") (v "0.3.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1ajjh1z1abvms1mma7hzx6v1sl4fgvjl3xzmixc4ys7fn0pbvrrd")))

(define-public crate-tomlenv-0.4.0 (c (n "tomlenv") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 2)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1jlvai8mr3s19xbxzs88h1y6hm2q3ysgngil9dlk4nwlcf4zpjj1") (y #t)))

(define-public crate-tomlenv-0.4.1 (c (n "tomlenv") (v "0.4.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 2)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "0d9wq8gvh5c7di2v01zsi4gv3lqhry1wy55mjvv0zz6ql0489a4l")))

(define-public crate-tomlenv-0.4.2 (c (n "tomlenv") (v "0.4.2") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 2)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1kcnj5gb0f21c464g2s81f7d9vc76fvryd1p1rwhw3b3n5gn404h")))

