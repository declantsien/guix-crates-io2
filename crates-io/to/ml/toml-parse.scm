(define-module (crates-io to ml toml-parse) #:use-module (crates-io))

(define-public crate-toml-parse-0.1.1 (c (n "toml-parse") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.5") (d #t) (k 0)) (d (n "rowan") (r "^0.9") (d #t) (k 0)))) (h "047nzs4qj4wampxmala7n9rqlbq0hqa9wf0bhf36hnsmr9dwx8g2")))

(define-public crate-toml-parse-0.1.5 (c (n "toml-parse") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.5") (d #t) (k 0)) (d (n "rowan") (r "^0.9") (d #t) (k 0)))) (h "1fjgvnk1nmsn8rbqx42dybx3wwpfl3i7drnsharr5j3a4n3849kx")))

(define-public crate-toml-parse-0.2.0 (c (n "toml-parse") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.5") (d #t) (k 0)) (d (n "rowan") (r "^0.9") (d #t) (k 0)))) (h "18vsa6sllvicbv5vb3z95jhvw5dcwdn4ab8l64xfbky231niqczi")))

(define-public crate-toml-parse-0.2.5 (c (n "toml-parse") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.5") (d #t) (k 0)) (d (n "rowan") (r "^0.9") (d #t) (k 0)))) (h "170jkcxhkg8cs3zsaiz5myvzb8vrdib5ai85mj840q6g1qpb84fr")))

(define-public crate-toml-parse-0.2.7 (c (n "toml-parse") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.5") (d #t) (k 0)) (d (n "rowan") (r "^0.9") (d #t) (k 0)))) (h "011pzb1q3kz3hzm31qjx66rzabk6i71bjigg2cqg5142ni6c4rvv")))

(define-public crate-toml-parse-0.2.8 (c (n "toml-parse") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.6.1") (d #t) (k 0)) (d (n "rowan") (r "^0.10.0") (d #t) (k 0)))) (h "06a872hpsb83dkxxw0r9z67wxmayizna10ma1xqvmmf3vf1jnd2s")))

(define-public crate-toml-parse-0.2.9 (c (n "toml-parse") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.6.1") (d #t) (k 0)) (d (n "rowan") (r "^0.10.0") (d #t) (k 0)))) (h "1l7grfzvadf1pmya0fbqhfs7znq0mhhzjimzy4pqiphdcfdc44yb")))

(define-public crate-toml-parse-0.2.10 (c (n "toml-parse") (v "0.2.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.6.1") (d #t) (k 0)) (d (n "rowan") (r "^0.10.0") (d #t) (k 0)))) (h "0v1cyhci1mj3nvj9zs3k5jv5lr3wbzsbsnjsqvcgjxkrr9sisnai")))

(define-public crate-toml-parse-0.2.11 (c (n "toml-parse") (v "0.2.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "muncher") (r "^0.6.1") (d #t) (k 0)) (d (n "rowan") (r "^0.10.0") (d #t) (k 0)))) (h "0vnlx1fz7a2ric6c7h8m5bbdbw4a0ql2a5r5ph4m5mhxwjy9df5g")))

