(define-module (crates-io to ml toml_const_cli) #:use-module (crates-io))

(define-public crate-toml_const_cli-0.1.1 (c (n "toml_const_cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "toml_const") (r "^0.1") (d #t) (k 0)))) (h "15pmykv2r47di7mc57kl1m0rfbb2sqf74jfmyysalda3rlk3i632") (r "1.56")))

