(define-module (crates-io to ml toml-cli2) #:use-module (crates-io))

(define-public crate-toml-cli2-0.3.0 (c (n "toml-cli2") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15") (d #t) (k 0)))) (h "1fxgydnwp1yqbycvgk5g5bddpjp2zsv76dkamiiwbnhd9x6v554g") (y #t)))

(define-public crate-toml-cli2-0.3.1 (c (n "toml-cli2") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15") (d #t) (k 0)))) (h "0c1qx96sfhis5qz4jicwlbx6mxshn8cg4a3bqjlad25230iracrj") (y #t)))

(define-public crate-toml-cli2-0.3.2 (c (n "toml-cli2") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml_edit") (r "^0.15") (d #t) (k 0)))) (h "0gg71fwbd0q02ksvr9azwzyhyk6s0048m2ghb3zhhn5ywqj6f2mp")))

