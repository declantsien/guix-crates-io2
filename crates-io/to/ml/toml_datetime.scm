(define-module (crates-io to ml toml_datetime) #:use-module (crates-io))

(define-public crate-toml_datetime-0.5.0 (c (n "toml_datetime") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1zcjvygkix0hm7nv7i6ag4fd0l1pglga1wyq2l8zgy0fgpjm32w0") (r "1.60.0")))

(define-public crate-toml_datetime-0.5.1 (c (n "toml_datetime") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1xcw3kyklh3s2gxp65ma26rgkl7505la4xx1r55kfgcfmikz8ls5") (r "1.60.0")))

(define-public crate-toml_datetime-0.6.0 (c (n "toml_datetime") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "0dpd50m0v54h2hh4hdsn2v1gq6j57gghg2sdpp584qavp2lk2vw8") (r "1.60.0")))

(define-public crate-toml_datetime-0.6.1 (c (n "toml_datetime") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "08lndxs1bval859mlas0k4f032s26c9k6pzd589m02z1vqpfvf1s") (r "1.60.0")))

(define-public crate-toml_datetime-0.6.2 (c (n "toml_datetime") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "03x10v3h7vvgamdfmk2njahdhynq4pgiy5mrqvgc592v5wqsjxjs") (r "1.64.0")))

(define-public crate-toml_datetime-0.6.3 (c (n "toml_datetime") (v "0.6.3") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "0jsy7v8bdvmzsci6imj8fzgd255fmy5fzp6zsri14yrry7i77nkw") (r "1.64.0")))

(define-public crate-toml_datetime-0.6.4 (c (n "toml_datetime") (v "0.6.4") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "0d7kk7yfnm38jjj7vribjq7nnhf80166gzw5b4ray9gd320hgk2i") (r "1.67")))

(define-public crate-toml_datetime-0.6.5 (c (n "toml_datetime") (v "0.6.5") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1wds4pm2cn6agd38f0ivm65xnc7c7bmk9m0fllcaq82nd3lz8l1m") (r "1.67")))

(define-public crate-toml_datetime-0.6.6 (c (n "toml_datetime") (v "0.6.6") (d (list (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)))) (h "1grcrr3gh7id3cy3j700kczwwfbn04p5ncrrj369prjaj9bgvbab") (r "1.65")))

