(define-module (crates-io to ml tomlq-rs) #:use-module (crates-io))

(define-public crate-tomlq-rs-0.1.0 (c (n "tomlq-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "19k78qzdylyl5dr5vjyv0pvg8n6hcsff0qi03q4ynncl4icvlvvx")))

