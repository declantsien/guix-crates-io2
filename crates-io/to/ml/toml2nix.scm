(define-module (crates-io to ml toml2nix) #:use-module (crates-io))

(define-public crate-toml2nix-0.1.0 (c (n "toml2nix") (v "0.1.0") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1fgq7zd5g91gfbvmafl6vyjayqra3wb0bfvkrnhhd6fdqqm6l40x")))

(define-public crate-toml2nix-0.1.1 (c (n "toml2nix") (v "0.1.1") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0qa5win38ygzd2yl8868wmnvyz4rcli5f086z8zp4ph9wdhkj7y2")))

