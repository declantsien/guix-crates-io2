(define-module (crates-io to ml toml-config) #:use-module (crates-io))

(define-public crate-toml-config-0.1.0 (c (n "toml-config") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "~1.1.1") (d #t) (k 2)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "154vlr8lp2sl0902qz621cziyb4wyjy3n32c4q9fa67wnv87dsa5")))

(define-public crate-toml-config-0.1.1 (c (n "toml-config") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "~1.1.1") (d #t) (k 2)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1pa11gzhbx1lb6f1zkf2sarkf71vmv5ghizlzplh72h9wsqpfjgv")))

(define-public crate-toml-config-0.2.0 (c (n "toml-config") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "~1.1.1") (d #t) (k 2)) (d (n "toml") (r "^0.1") (k 0)))) (h "0yzc4hh65sra0ypsbimxly5hlwmzkawrqnd7vqql2dxvipakdb6a") (f (quote (("serde-serialization" "serde" "serde_macros" "toml/serde") ("default" "rustc-serialize" "toml/default"))))))

(define-public crate-toml-config-0.3.0 (c (n "toml-config") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "~1.1.1") (d #t) (k 2)) (d (n "toml") (r "^0.1") (k 0)))) (h "1xp1pxmyj48xmzzcbwl8y0nxcx86vn0dlqvy9app52kh7hyymkwh") (f (quote (("serde-serialization" "serde" "serde_macros" "toml/serde") ("default" "rustc-serialize" "toml/default"))))))

(define-public crate-toml-config-0.4.0 (c (n "toml-config") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "~1.1.1") (d #t) (k 2)) (d (n "toml") (r "^0.1") (k 0)))) (h "0w98d1kazspffqrxkng29667nybg0pzl7l4kmyi6cy387cdfbr9x") (f (quote (("serde-serialization" "serde" "serde_macros" "toml/serde") ("default" "rustc-serialize" "toml/default"))))))

