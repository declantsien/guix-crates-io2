(define-module (crates-io to ml toml-sync) #:use-module (crates-io))

(define-public crate-toml-sync-0.1.0 (c (n "toml-sync") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml-sync-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0pasxyjf8r6f62jaf6jpkc0wgnba1iclcvbfs1hg1pxmsny7kvj9")))

