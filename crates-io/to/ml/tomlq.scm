(define-module (crates-io to ml tomlq) #:use-module (crates-io))

(define-public crate-tomlq-0.1.0 (c (n "tomlq") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0zkjrbycbbv0r0wspyw7hjsv5agn8r35jj34w65hivmmmy9jwr11")))

