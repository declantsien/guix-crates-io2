(define-module (crates-io to ml tomlcli) #:use-module (crates-io))

(define-public crate-tomlcli-0.1.0 (c (n "tomlcli") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "12kzj8yqmy59jr6digvwwlfklr19alh9rymwn9ylrbph1nbi4mjn")))

(define-public crate-tomlcli-0.2.0 (c (n "tomlcli") (v "0.2.0") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "16hql3d53kh54vfdaknimzzvdr9mjwfrxaf3biqrf9i68n8cr9r1")))

