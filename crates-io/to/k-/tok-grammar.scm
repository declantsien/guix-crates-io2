(define-module (crates-io to k- tok-grammar) #:use-module (crates-io))

(define-public crate-tok-grammar-0.1.0 (c (n "tok-grammar") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0h1smmh5l4sx5bvgccqn0cn48m07fy2g1vyrhqadbvw8m66q71q4")))

(define-public crate-tok-grammar-0.1.1 (c (n "tok-grammar") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1raa52nvwnzhpvhk7mq9hyxacnrnakqbyq1ssy1046qbdf66rkjs")))

(define-public crate-tok-grammar-0.1.2 (c (n "tok-grammar") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0hf7dpwmr6v2ffq859klicbf834g686mvcwbmjy1df3ichb9639l")))

