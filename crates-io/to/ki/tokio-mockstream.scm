(define-module (crates-io to ki tokio-mockstream) #:use-module (crates-io))

(define-public crate-tokio-mockstream-1.0.0 (c (n "tokio-mockstream") (v "1.0.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1j82v236ywz7wq12ss8qb1649jrmy5kvsqrh0s17a5h65412fx5k")))

(define-public crate-tokio-mockstream-1.1.0 (c (n "tokio-mockstream") (v "1.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0mg1i39cl8x32wxwbn74hlirks8a6f3g0gfzkb0n0zwbxwvc9gs1")))

