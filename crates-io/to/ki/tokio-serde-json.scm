(define-module (crates-io to ki tokio-serde-json) #:use-module (crates-io))

(define-public crate-tokio-serde-json-0.0.0 (c (n "tokio-serde-json") (v "0.0.0") (h "0qvf6faisda2x12z4md83d8n9ibssf8cyc4pfz81jv5yi8g9094p")))

(define-public crate-tokio-serde-json-0.1.0 (c (n "tokio-serde-json") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 2)) (d (n "tokio-serde") (r "^0.2") (d #t) (k 0)))) (h "060gb6cy07nl15bb9whkk8zhcb8n1wxjws3153g4z7ljv2kyb60v")))

(define-public crate-tokio-serde-json-0.2.0 (c (n "tokio-serde-json") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-serde") (r "^0.3") (d #t) (k 0)))) (h "0rs884085j2c0bccyag2h4jnpdxx0qricvgx32ixdmkyi8qf1w4f")))

(define-public crate-tokio-serde-json-0.3.0 (c (n "tokio-serde-json") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "tokio-serde") (r "^0.4.0") (d #t) (k 0)))) (h "1j2j3yq2i3n0pwyid83fd6779n8gpcvprmbw7fc1h691r2nh9pgb")))

