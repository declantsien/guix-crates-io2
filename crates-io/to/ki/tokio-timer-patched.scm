(define-module (crates-io to ki tokio-timer-patched) #:use-module (crates-io))

(define-public crate-tokio-timer-patched-0.1.3 (c (n "tokio-timer-patched") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)))) (h "0bl6vbfmpm451y408fb05q89d3l6znm1rvdrzhgqnwmz3ivllxll")))

