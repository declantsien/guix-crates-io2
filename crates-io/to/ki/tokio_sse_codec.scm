(define-module (crates-io to ki tokio_sse_codec) #:use-module (crates-io))

(define-public crate-tokio_sse_codec-0.0.1 (c (n "tokio_sse_codec") (v "0.0.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "000dn2n6wc4z9fgb4qy7pbh2is31dxlq6iv4hqx7agihr2ck5k8x") (y #t)))

(define-public crate-tokio_sse_codec-0.0.2 (c (n "tokio_sse_codec") (v "0.0.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1lscwv0pg286lq6niikwwipf4ivb8a37p5sd938ysac8zzqqivy1")))

