(define-module (crates-io to ki tokio-shield) #:use-module (crates-io))

(define-public crate-tokio-shield-0.1.0 (c (n "tokio-shield") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread" "time" "sync"))) (k 2)))) (h "0290gr4k3cvrn9m4b0sy669jxv18jj4bpsi09w6mn7ijinvqxfpy") (r "1.56.1")))

(define-public crate-tokio-shield-0.1.1 (c (n "tokio-shield") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.28") (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread" "time" "sync"))) (k 2)))) (h "0lbp9gdrjrny2gsmy11vqwdz2b4xi4wpqx9c0miz389fy61cnxrp") (r "1.63.0")))

