(define-module (crates-io to ki tokio-async-io) #:use-module (crates-io))

(define-public crate-tokio-async-io-0.1.0 (c (n "tokio-async-io") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver" "macros" "rt-core" "rt-util"))) (d #t) (k 2)))) (h "0gvf9xzpqxm6x4q6v4dmzn4vz1hvmnzw24qagpdr1agk4kvdjji9")))

