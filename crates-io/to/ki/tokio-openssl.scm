(define-module (crates-io to ki tokio-openssl) #:use-module (crates-io))

(define-public crate-tokio-openssl-0.1.0 (c (n "tokio-openssl") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0x1x0h6ayllgvwkrddbnxj25rr6902bjhabygg87s8jxg6ayriqm")))

(define-public crate-tokio-openssl-0.1.1 (c (n "tokio-openssl") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0dqr0v2xlf5m1z68zpyxs9d71pfg7w985nrbf39if0ka7rg1g2n0")))

(define-public crate-tokio-openssl-0.1.2 (c (n "tokio-openssl") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0x6ydagcnyfw5k9wyipfny36zf9hf47vngw4g6kxkkg81bk5ahqi")))

(define-public crate-tokio-openssl-0.1.3 (c (n "tokio-openssl") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0s5is9dyja2vbj3wdilgzg4vx7b5qijjwdp7fjzhxj5fqccx2k1n")))

(define-public crate-tokio-openssl-0.1.4 (c (n "tokio-openssl") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "openssl") (r "^0.9.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0zvbj5whnn9x6bgf16hkgikhb47hrqbzcn2aizx2vasda6m0anv7")))

(define-public crate-tokio-openssl-0.2.0 (c (n "tokio-openssl") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "10ly2rifid3ik9drfbp297v7s8mm3j5acg3ids9vwdbk4j5cv23y")))

(define-public crate-tokio-openssl-0.2.1 (c (n "tokio-openssl") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.6") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "19kq3426s8vdpm7m8yamvlw2kl5cfnpm7slnsziksf93sqgswij6")))

(define-public crate-tokio-openssl-0.3.0 (c (n "tokio-openssl") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.6") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "19zx58jz0vkxppa3pmqnq0b90mqsycikr5nrcy6i1bkhn53647bp")))

(define-public crate-tokio-openssl-0.4.0-alpha.1 (c (n "tokio-openssl") (v "0.4.0-alpha.1") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.17") (d #t) (k 2)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.1") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.1") (d #t) (k 0)))) (h "1y6mzhhmh735i9krqvg0nn0znnjpgjl51bik287zrib53cyf29r1")))

(define-public crate-tokio-openssl-0.4.0-alpha.2 (c (n "tokio-openssl") (v "0.4.0-alpha.2") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.18") (d #t) (k 2)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.2") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.2") (d #t) (k 0)))) (h "0zzhb720bmjkcg5q53yp9mimx8frnbrk9il6rya16zc6pwmzbfw8")))

(define-public crate-tokio-openssl-0.4.0-alpha.4 (c (n "tokio-openssl") (v "0.4.0-alpha.4") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.18") (d #t) (k 2)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.4") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.4") (d #t) (k 0)))) (h "1h649wxs0shqc7sk2fnf9nbanfghg9viy1c1n43npyzncarn2jl9")))

(define-public crate-tokio-openssl-0.4.0-alpha.5 (c (n "tokio-openssl") (v "0.4.0-alpha.5") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.18") (d #t) (k 2)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.5") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.5") (d #t) (k 0)))) (h "1s04y85qlcgaly4ylx7liyw7klrnv5xk63xykhddzz96ywmycdmj")))

(define-public crate-tokio-openssl-0.4.0-alpha.6 (c (n "tokio-openssl") (v "0.4.0-alpha.6") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "0grbggfspanx968r8i1511f85kg117qir3irn298a50axiqg1301")))

(define-public crate-tokio-openssl-0.4.0 (c (n "tokio-openssl") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "15751d47984ncvllagz35ldl10ifr8555wixvsg6k3i0yk2hhjrw")))

(define-public crate-tokio-openssl-0.5.0 (c (n "tokio-openssl") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.58") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "0zwk7y0ynj7nn9jj4hh5d13xch41xkxazl4214qzlm51sg15q7nh")))

(define-public crate-tokio-openssl-0.6.0 (c (n "tokio-openssl") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "13z16vhdk7cxqna1v2i779yyvf2qyyam7wba32p60i585bnxs2ix")))

(define-public crate-tokio-openssl-0.6.1 (c (n "tokio-openssl") (v "0.6.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0khjqv8wap79ki7h0l91rs8j0b4ix097lb40b4s1x9sa19ffq6xc")))

(define-public crate-tokio-openssl-0.6.2 (c (n "tokio-openssl") (v "0.6.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1zh938wbjljqjklaycifcxgwjkslkp0r37nxkhswhkas8k4dsk7j")))

(define-public crate-tokio-openssl-0.6.3 (c (n "tokio-openssl") (v "0.6.3") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "12l7a01sid095zmdkcmjnds9hwfcyjn9539r3c6b5w89g3xrz3y0")))

(define-public crate-tokio-openssl-0.6.4 (c (n "tokio-openssl") (v "0.6.4") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10.61") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0gfnskg4fgpmv3j2q7lvax6c4wqq151zf7vzynngc9vpysfvgykg")))

