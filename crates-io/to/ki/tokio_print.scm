(define-module (crates-io to ki tokio_print) #:use-module (crates-io))

(define-public crate-tokio_print-0.1.0 (c (n "tokio_print") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "safety_breaker") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0915xn9yn4ym29wdmsnminwiji7y0g5lgk1w0s33sk01nqs996vl")))

