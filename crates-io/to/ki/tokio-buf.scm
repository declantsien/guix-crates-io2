(define-module (crates-io to ki tokio-buf) #:use-module (crates-io))

(define-public crate-tokio-buf-0.0.0 (c (n "tokio-buf") (v "0.0.0") (h "0s5m7qlidk09h0r80kj8c1mki1kbbs4bg6wcn2xl0vdi9p7j4am6")))

(define-public crate-tokio-buf-0.1.0 (c (n "tokio-buf") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.10") (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1.23") (d #t) (k 0)))) (h "1klv3sa75r2pla458x7jvr2q0j1j9j9ks3pnx606v3am1sj4afj7") (f (quote (("util" "bytes/either" "either") ("default" "util"))))))

(define-public crate-tokio-buf-0.1.1 (c (n "tokio-buf") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.10") (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.1") (d #t) (k 2)))) (h "0inwrkh8knqy44mr9h2i305zyy4pxhfy90y0gr5rm1akdks21clg") (f (quote (("util" "bytes/either" "either") ("default" "util"))))))

(define-public crate-tokio-buf-0.2.0-alpha.1 (c (n "tokio-buf") (v "0.2.0-alpha.1") (d (list (d (n "bytes") (r "^0.4.10") (d #t) (k 0)) (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.1") (d #t) (k 2)))) (h "1bz2yb77kxq4006j6cjdkl14n21pi0c0mdw20ywj9yd70y7lap2z")))

