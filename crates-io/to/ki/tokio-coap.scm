(define-module (crates-io to ki tokio-coap) #:use-module (crates-io))

(define-public crate-tokio-coap-0.0.1 (c (n "tokio-coap") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "12j0i3dna52mg537v79pw1lc97rjymjz1hhj337aq5f1pb0bzqwj")))

(define-public crate-tokio-coap-0.1.1 (c (n "tokio-coap") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)))) (h "0clgxzqk7hw815668l58b306yrmzhammljlyiwk6cm7gv3rx002z")))

(define-public crate-tokio-coap-0.2.0 (c (n "tokio-coap") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.6") (d #t) (k 0)))) (h "0wn66baph9i08k07qbbyz68nllpsifjax1zfhssadfc58s9rjks6")))

