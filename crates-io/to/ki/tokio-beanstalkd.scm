(define-module (crates-io to ki tokio-beanstalkd) #:use-module (crates-io))

(define-public crate-tokio-beanstalkd-0.1.0 (c (n "tokio-beanstalkd") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "11gf2fy64166h6vn0why87yshzlmc7zwp4k0shnskn8f6a6c0xgs")))

(define-public crate-tokio-beanstalkd-0.2.0 (c (n "tokio-beanstalkd") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "18k8zzms30d4hbrr5ybqmx0mpjz1w1xz7m8nvbjjbc76jiiks8x1")))

(define-public crate-tokio-beanstalkd-0.4.0 (c (n "tokio-beanstalkd") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1h1qzqxmfxypfqamgdb6mpxciq6wy87688hl83ghj6vvd0l72hyz")))

(define-public crate-tokio-beanstalkd-0.4.1 (c (n "tokio-beanstalkd") (v "0.4.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1m4dzvsa3nqkwvdjalbrch219k1cilw7z58jam588pg243gpgz9c")))

(define-public crate-tokio-beanstalkd-0.4.2 (c (n "tokio-beanstalkd") (v "0.4.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0z2iqszsc7l8jdswaysdql3cwdvzmq7rnisn9vn3c0ypp7xm99d2")))

