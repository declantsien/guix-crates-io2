(define-module (crates-io to ki tokio-by-hand) #:use-module (crates-io))

(define-public crate-tokio-by-hand-0.0.0 (c (n "tokio-by-hand") (v "0.0.0") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.9") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0jhfsm2yw4dsmd5yr7hj0dc68anwssgydyp3aj5in148nrbxxpx7")))

