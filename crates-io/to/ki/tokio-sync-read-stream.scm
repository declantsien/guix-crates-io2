(define-module (crates-io to ki tokio-sync-read-stream) #:use-module (crates-io))

(define-public crate-tokio-sync-read-stream-0.1.0 (c (n "tokio-sync-read-stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "0k4sf8kmn3kwxv163f2fsb4bn13n3xx9k6acnlgq2hf29gaj49wn")))

