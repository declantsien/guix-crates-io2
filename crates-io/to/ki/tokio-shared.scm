(define-module (crates-io to ki tokio-shared) #:use-module (crates-io))

(define-public crate-tokio-shared-0.1.0 (c (n "tokio-shared") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1bxkq4j49yyimb8xbr48jq76cr1x9cgc0swqz21kmjy0n9lp3s7f") (f (quote (("default"))))))

(define-public crate-tokio-shared-0.1.1 (c (n "tokio-shared") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05jgmpnarxyif6a97plyq9ipfmi0yp170h2njn6gzbq116im2pqv") (f (quote (("default"))))))

