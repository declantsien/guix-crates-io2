(define-module (crates-io to ki tokio-anon-pipe) #:use-module (crates-io))

(define-public crate-tokio-anon-pipe-0.1.0 (c (n "tokio-anon-pipe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (f (quote ("macros" "rt" "io-util"))) (d #t) (k 2)))) (h "1wnfnzwd4hbqik9la0jv5a9gjr9wkhz78kdhxkrp6bx97zp7nw93")))

(define-public crate-tokio-anon-pipe-0.1.1 (c (n "tokio-anon-pipe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (f (quote ("macros" "rt" "io-util"))) (d #t) (k 2)))) (h "1i688v6h1q802p6vrqb5gfa9nbs0izs75q4bcpn4pcy4a1zp4gsh")))

