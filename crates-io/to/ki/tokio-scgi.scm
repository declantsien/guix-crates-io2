(define-module (crates-io to ki tokio-scgi) #:use-module (crates-io))

(define-public crate-tokio-scgi-0.1.0 (c (n "tokio-scgi") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "tokio") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-sync") (r "^0.1.6") (d #t) (k 2)))) (h "1bqvh2pqvs6l6hisg9x9qnizrd5r1lr5qlay1kjnd0dflpr3wnjm")))

(define-public crate-tokio-scgi-0.2.0 (c (n "tokio-scgi") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-codec") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "051vzsl8d5c5db7szp9ng2kbn14lq2linqqf0kihrhbawwblkq2a")))

(define-public crate-tokio-scgi-0.2.0-1 (c (n "tokio-scgi") (v "0.2.0-1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-codec") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "0pz8mcxk5sjmi2r18wf920rnq5pc4f1ayifj91mj1hlkgj94cw9a") (y #t)))

(define-public crate-tokio-scgi-0.2.1 (c (n "tokio-scgi") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-codec") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "1bl1sd3l2clbkzlgambg3n55alkl2427vwrw9ln72mz982w14g4j")))

(define-public crate-tokio-scgi-0.2.3 (c (n "tokio-scgi") (v "0.2.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "11mf44rfnlivq3rjd516a64dzbyigs5ybcf7gyyxdh42csr9r69n")))

(define-public crate-tokio-scgi-0.2.4 (c (n "tokio-scgi") (v "0.2.4") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0l49l8z5q2vfj062spfnp1l94v93r5lm8jyzsxzgbh2kjq876msm")))

