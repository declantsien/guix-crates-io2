(define-module (crates-io to ki tokio-named-pipe) #:use-module (crates-io))

(define-public crate-tokio-named-pipe-0.0.0 (c (n "tokio-named-pipe") (v "0.0.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio-named-pipes") (r "^0.1.6") (d #t) (k 0)) (d (n "miow") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("io-driver" "io-util"))) (d #t) (k 0)) (d (n "tonic") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "1slxlyyl6ydaagy2wzlkqp9rgfpz7z6pfkwb24asi1l5ryjnhzy1") (f (quote (("default"))))))

