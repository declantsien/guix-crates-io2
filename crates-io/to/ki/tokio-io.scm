(define-module (crates-io to ki tokio-io) #:use-module (crates-io))

(define-public crate-tokio-io-0.1.0 (c (n "tokio-io") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0ww47j6g54hhk0ds5c0bk3h42j5ad917s8lm97j6igpi8pg8y9va")))

(define-public crate-tokio-io-0.1.1 (c (n "tokio-io") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0x87pvc7ss5p0awddij4g8ndxzxbq85h79i9ya0j5f8v6kqmvxa8")))

(define-public crate-tokio-io-0.1.2 (c (n "tokio-io") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1ny2x067jms2iy4hiw03sq22xsga3zl22m2vlq7plf7p76bwxhy2")))

(define-public crate-tokio-io-0.1.3 (c (n "tokio-io") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1bq2kzp5kv7wa8px4zbx9lydjnanyzplryh5wi17wrxmmpkq7axl")))

(define-public crate-tokio-io-0.1.4 (c (n "tokio-io") (v "0.1.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0hspwb4k6gb055xvgk3fv7a74rhjhg3dg2ii0fzrv4kq64hawjji")))

(define-public crate-tokio-io-0.1.5 (c (n "tokio-io") (v "0.1.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ksfkzv6h1qaisvazrnhgm53n88p1yny5h3y55qby8i2fx42flxr")))

(define-public crate-tokio-io-0.1.6 (c (n "tokio-io") (v "0.1.6") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fiian2sywzw5gckaznyak7v62p084rrbq9qhjvddck4dwrfpyba")))

(define-public crate-tokio-io-0.1.7 (c (n "tokio-io") (v "0.1.7") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08fcakd0hj1lwx90b3daypc818l9aqa1xalb5cq6vwh6x1g67jd5")))

(define-public crate-tokio-io-0.1.8 (c (n "tokio-io") (v "0.1.8") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jrlpmdnbwcfam1s4mgh6h4cbzknhh3bkc3imj33r1i5fzgc4v4d")))

(define-public crate-tokio-io-0.1.9 (c (n "tokio-io") (v "0.1.9") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.1") (d #t) (k 2)))) (h "131ybvabkxyalnfqb9wnb7p3gk8d4dq4ack2mfqmlg7czgzqb2lb")))

(define-public crate-tokio-io-0.1.10 (c (n "tokio-io") (v "0.1.10") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.1") (d #t) (k 2)))) (h "08fsvjpszcvq8p8f6fkwiamdpmcva5n12y275j40rknmf05gx4kk")))

(define-public crate-tokio-io-0.1.11 (c (n "tokio-io") (v "0.1.11") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.1") (d #t) (k 2)))) (h "03w2mvrsmsi874cyligyz1ai7yl7d7wqi5z156xjxksw7yfynfmm")))

(define-public crate-tokio-io-0.1.12 (c (n "tokio-io") (v "0.1.12") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.1") (d #t) (k 2)))) (h "09jrz1hh4h1vj45qy09y7m7m8jsy1hl6g32clnky25mdim3dp42h")))

(define-public crate-tokio-io-0.2.0-alpha.1 (c (n "tokio-io") (v "0.2.0-alpha.1") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.17") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.1") (d #t) (k 2)) (d (n "tokio-test") (r "= 0.2.0-alpha.1") (d #t) (k 2)))) (h "1xy1fvjb0jrrm9i2lk3cmyd5jzdzd0yv1834q2ckh6fqbc10w7mb") (f (quote (("util" "memchr"))))))

(define-public crate-tokio-io-0.2.0-alpha.2 (c (n "tokio-io") (v "0.2.0-alpha.2") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "= 0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "1lqfag8hmjf4cdp0lx2zak0159wirfnanc5yr7lahp0apsqdawi3") (f (quote (("util" "memchr" "pin-utils"))))))

(define-public crate-tokio-io-0.2.0-alpha.3 (c (n "tokio-io") (v "0.2.0-alpha.3") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "= 0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "152bs65qvllpyanh8f9a629mmglrxx5mzxskry7rlwb071adpa3h") (f (quote (("util" "memchr" "pin-utils"))))))

(define-public crate-tokio-io-0.2.0-alpha.4 (c (n "tokio-io") (v "0.2.0-alpha.4") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "= 0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "0p8jma1zzgprzd84ii45xiss36qi7nr36gixwidznszv8rhw8cch") (f (quote (("util" "memchr" "pin-utils"))))))

(define-public crate-tokio-io-0.2.0-alpha.5 (c (n "tokio-io") (v "0.2.0-alpha.5") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "= 0.4.0-alpha.11") (d #t) (k 0)) (d (n "pin-utils") (r "= 0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "10mqhz2ajs4d99a8jkw15w6zqid8iq2gccq94irp0plp81hg5sha") (f (quote (("util" "memchr" "pin-utils"))))))

(define-public crate-tokio-io-0.2.0-alpha.6 (c (n "tokio-io") (v "0.2.0-alpha.6") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1i92ichh2m7i23vdr51gnf5hxngv7d1clwjan1h0dwrxakaq89qi") (f (quote (("util" "memchr" "pin-project"))))))

(define-public crate-tokio-io-0.1.13 (c (n "tokio-io") (v "0.1.13") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1.1") (d #t) (k 2)))) (h "0x06zyzinans1pn90g6i150lgixijdf1cg8y2gipjd09ms58dz2p")))

