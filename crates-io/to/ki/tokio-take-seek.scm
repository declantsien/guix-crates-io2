(define-module (crates-io to ki tokio-take-seek) #:use-module (crates-io))

(define-public crate-tokio-take-seek-0.1.0 (c (n "tokio-take-seek") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0m5q9j41fmq9zv4ivhmmqfm9v0ih2wfslm7sarmv44id50f17m1m")))

