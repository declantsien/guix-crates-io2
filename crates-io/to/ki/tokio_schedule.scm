(define-module (crates-io to ki tokio_schedule) #:use-module (crates-io))

(define-public crate-tokio_schedule-0.1.0 (c (n "tokio_schedule") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)))) (h "08aap75jpz6vrxigf58jidpc0v90bl4gqwa9xl7lqv1ff0csx28r")))

(define-public crate-tokio_schedule-0.1.1 (c (n "tokio_schedule") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)))) (h "183wc20x7dxl7swx6yrr5ajk28kp0wkbn49flmnqvkjj1axq3f47")))

(define-public crate-tokio_schedule-0.2.0 (c (n "tokio_schedule") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1hzkh75g093ml8yx3fyn2y0qi8scjb42c71jgwz7hhq6dai4bpnh")))

(define-public crate-tokio_schedule-0.2.1 (c (n "tokio_schedule") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "149ccw4mnqz4d2cq0h3qcm4vinxs15dsbd70map3kqbms58vfjyi")))

(define-public crate-tokio_schedule-0.3.0 (c (n "tokio_schedule") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1s9iy3xma40jiqvqbjdfib9i01q4rbigv1966hcajb83wmqz748v")))

(define-public crate-tokio_schedule-0.3.1 (c (n "tokio_schedule") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0mi20bkc4ij56s5v8jn0dhm95jb4nfmgpidwgq9rvhsq2smi8jhv")))

