(define-module (crates-io to ki tokio-stdin) #:use-module (crates-io))

(define-public crate-tokio-stdin-0.1.0 (c (n "tokio-stdin") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 2)))) (h "12nrs4kk2a0xhm0rdqvjz5mwmya3m9sin55rdy335jx7nya35yfi")))

(define-public crate-tokio-stdin-0.1.1 (c (n "tokio-stdin") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 2)))) (h "1gg2d2wf7j3brl9xrhcb0v1iq64vyhxdfg51486zkqnmfvnrwjpw")))

(define-public crate-tokio-stdin-0.1.2 (c (n "tokio-stdin") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 2)))) (h "0p3gnn1lry9ajapg5yg07mzc6srglxx24a4dsfl9xnr7kfxvlmam")))

