(define-module (crates-io to ki tokio-cron) #:use-module (crates-io))

(define-public crate-tokio-cron-0.1.0 (c (n "tokio-cron") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("fmt"))) (d #t) (k 2)))) (h "0hjqq4kyvjccrwg4b3fcrg1lp79b5vkmm5rrrkmnim2iws1yfb96")))

(define-public crate-tokio-cron-0.1.1 (c (n "tokio-cron") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("fmt"))) (d #t) (k 2)))) (h "106m9rnn2d2i35nqd8874jql05n3dyb4wzrnfiik8kf2x6s4a4il")))

(define-public crate-tokio-cron-0.1.2 (c (n "tokio-cron") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("fmt"))) (d #t) (k 2)))) (h "1zk9hwl63iv3pl960xqgqziq1snxa40achmzp096vqg16bzyhi5r")))

(define-public crate-tokio-cron-0.1.3 (c (n "tokio-cron") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("fmt"))) (d #t) (k 2)))) (h "0m8sc09hj74rm3q76xng91igsizbgbm8i6wba6g41vzd58bqz7mj")))

