(define-module (crates-io to ki tokio-bincodec) #:use-module (crates-io))

(define-public crate-tokio-bincodec-0.1.0 (c (n "tokio-bincodec") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 2)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.11") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.4") (f (quote ("codec"))) (d #t) (k 0)))) (h "1rm9kqvicnyz5diq39mm7g5nfi1mflqs31cazalk3yxi3rfvsjgv")))

