(define-module (crates-io to ki tokio-xattr) #:use-module (crates-io))

(define-public crate-tokio-xattr-0.2.1-alpha.1 (c (n "tokio-xattr") (v "0.2.1-alpha.1") (d (list (d (n "blocking_xattr") (r "^0.2.2") (d #t) (k 0) (p "xattr")) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "= 0.2.0-alpha.4") (d #t) (k 2)) (d (n "tokio-executor") (r "= 0.2.0-alpha.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio-io") (r "= 0.2.0-alpha.4") (f (quote ("util"))) (d #t) (k 0)) (d (n "tokio-test") (r "= 0.2.0-alpha.4") (d #t) (k 2)))) (h "1yl0k23b9v136ivpyqqi6rgl4bgy7ax7v91a2arqdn0lcjnw67dm")))

