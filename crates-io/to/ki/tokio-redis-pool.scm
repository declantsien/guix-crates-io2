(define-module (crates-io to ki tokio-redis-pool) #:use-module (crates-io))

(define-public crate-tokio-redis-pool-0.1.0 (c (n "tokio-redis-pool") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.20") (d #t) (k 2)) (d (n "tokio-resource-pool") (r "^0.2.0") (d #t) (k 0)))) (h "0mrg2pvnaxj5advv0l5w2isqq3mxf8w2xx4wkxm6gni4flf1n8rs")))

(define-public crate-tokio-redis-pool-0.1.1 (c (n "tokio-redis-pool") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.20") (d #t) (k 2)) (d (n "tokio-resource-pool") (r "^0.2.0") (d #t) (k 0)))) (h "065dw3m3csb6ihmniga4zyx9finwjz8jjp8bx3mq041q11qrrkxr")))

(define-public crate-tokio-redis-pool-0.2.0 (c (n "tokio-redis-pool") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.21") (d #t) (k 2)) (d (n "tokio-resource-pool") (r "^0.3.0") (d #t) (k 0)))) (h "14bhpv3yzzx66a8bmq2xxalyvmldl5bhdcj72iphds1bvx6h15l3")))

(define-public crate-tokio-redis-pool-0.2.1 (c (n "tokio-redis-pool") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "redis") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.21") (d #t) (k 2)) (d (n "tokio-resource-pool") (r "^0.3.1") (d #t) (k 0)))) (h "0a2hzdwrwcpyyh3j2zzkrhppbs28zs2ygar40hpcgzvs8kj8ciyn")))

(define-public crate-tokio-redis-pool-0.3.0 (c (n "tokio-redis-pool") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-resource-pool") (r "^0.4.1") (d #t) (k 0)))) (h "1kcv7m96f12wrzy1xf9mzp4kfv5w8n3cspdmw66777qn1imljmz1")))

