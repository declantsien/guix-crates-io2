(define-module (crates-io to ki tokio-postgres-migration) #:use-module (crates-io))

(define-public crate-tokio-postgres-migration-0.1.0 (c (n "tokio-postgres-migration") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "192jw9lal9i6w953knwqh4fy8w4vj8srryphgnxfrfkac48vsk1z")))

