(define-module (crates-io to ki tokio-simplified) #:use-module (crates-io))

(define-public crate-tokio-simplified-0.1.0 (c (n "tokio-simplified") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0jgfi0inpdy8cgrh66l8bv96nqj82k4kmmvyj2vbd73v5xpka8bs")))

(define-public crate-tokio-simplified-0.1.1 (c (n "tokio-simplified") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "1k3ih517v7ina8ll31b39lhiwl81r60xgfl94aq9r87p72dmv8mi")))

(define-public crate-tokio-simplified-0.1.2 (c (n "tokio-simplified") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "1p7zxr0rbvbd2pbr03kh3bcgj3vpkb80mlhm3x6gm6w4hfzxnhax")))

(define-public crate-tokio-simplified-0.1.3 (c (n "tokio-simplified") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0dagn9fsymdsdvhpdg4hj837n409z12l7r0ryxm4i8wf05z27jnp")))

(define-public crate-tokio-simplified-0.1.5 (c (n "tokio-simplified") (v "0.1.5") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0dpnzkw16ws75a9h21rk7pnrbg6pl67qhffq1k9wkj0jaz6x7n94")))

(define-public crate-tokio-simplified-0.1.6 (c (n "tokio-simplified") (v "0.1.6") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "13rw9xkgb7wgrwlvvh2l8bc4maq7nhq64bs7jylk3pkk0ww4gcay")))

(define-public crate-tokio-simplified-0.1.7 (c (n "tokio-simplified") (v "0.1.7") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "16n6y652mmxg8p40jhf31wpdvyysfvkaw6j2fzwz1c8nhv482fka")))

(define-public crate-tokio-simplified-0.1.8 (c (n "tokio-simplified") (v "0.1.8") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0ylghczh92lqkxb9jkz42rpw8ck4qrp0zafk1r1njl3w3jp07f27")))

(define-public crate-tokio-simplified-0.1.9 (c (n "tokio-simplified") (v "0.1.9") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "1wy6h23vc2ymnplg6kj5w25zcv82r8lqa0923bnzjnbjq4qypcz9")))

(define-public crate-tokio-simplified-0.1.10 (c (n "tokio-simplified") (v "0.1.10") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.19") (d #t) (k 0)))) (h "1vskyj84qjxc2bv2ygzd65v6dx9wm33s5ldjp6f9kfi3fcy6rq9s")))

(define-public crate-tokio-simplified-0.1.11 (c (n "tokio-simplified") (v "0.1.11") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "1v6mrnq1wlrvkmf5gs2zyx7alick837vlnr352klkf304zn3b298")))

(define-public crate-tokio-simplified-0.1.12 (c (n "tokio-simplified") (v "0.1.12") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0lhmymr47a9fx8m2dsbjn1firqhzm5anfx6hp8fffv62ylbd304n")))

(define-public crate-tokio-simplified-0.1.13-pre (c (n "tokio-simplified") (v "0.1.13-pre") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0vq2ynqw8qnnj35pw76kdvqpdkg0xlnz02n8fm7mrgx666qicq8w")))

(define-public crate-tokio-simplified-0.1.13 (c (n "tokio-simplified") (v "0.1.13") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "001w1gr6i40sfxffbsvisk3v4yrwwdwckl8ff5rkxjh55rfw37hy")))

(define-public crate-tokio-simplified-0.1.14 (c (n "tokio-simplified") (v "0.1.14") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "futures-promises") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "0nma13fi4411ygahwz6mfja3abchndsps2hnpa7l4pyx13r37p3c")))

(define-public crate-tokio-simplified-0.1.15 (c (n "tokio-simplified") (v "0.1.15") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "futures-promises") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)))) (h "086klkib0sik5rc9crm1sbp8y9ypa9snmgvsxyp6h5wmskcqcxb1")))

(define-public crate-tokio-simplified-0.2.0-pre (c (n "tokio-simplified") (v "0.2.0-pre") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "futures-promises") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.19") (d #t) (k 0)))) (h "13idfrk2r8db3gym8ni8b41jvp57i9c94hkvmbf6c2majv7fjpjc")))

(define-public crate-tokio-simplified-0.2.0 (c (n "tokio-simplified") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "futures-promises") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.19") (d #t) (k 0)))) (h "00iqxd26wbqcbgsrqn8ldfvrj4ripjdpjsd4bcaqn0b43s47nxvf")))

(define-public crate-tokio-simplified-0.2.1 (c (n "tokio-simplified") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "futures-promises") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.19") (d #t) (k 0)))) (h "1lmbnmcq79mvz1hqfv3kghzgbcyvi84awmp4vh0asbf3az22l8gp")))

(define-public crate-tokio-simplified-0.2.2 (c (n "tokio-simplified") (v "0.2.2") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "futures-promises") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.19") (d #t) (k 0)))) (h "1bly90ym8s18ywfxiazjc1h6gn96ba7innvl7qy7qj8p4in5kpmj") (f (quote (("very_big_channels") ("big_channels"))))))

