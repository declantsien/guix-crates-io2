(define-module (crates-io to ki tokio-os-timer) #:use-module (crates-io))

(define-public crate-tokio-os-timer-0.1.0 (c (n "tokio-os-timer") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "0fk4nbsyh6m9lanfr082939m9p331rf558k6mc30hikyimjf18n2")))

(define-public crate-tokio-os-timer-0.1.1 (c (n "tokio-os-timer") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "07q08qh1lc7r95lyb4nnyyki7pmjdlij3x4j0ym38likq4c3drii")))

(define-public crate-tokio-os-timer-0.1.2 (c (n "tokio-os-timer") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "1q16p6bs0khskr5pyimf3iisy57fiy335f22ak2r6xbvkf3iv75c")))

(define-public crate-tokio-os-timer-0.1.3 (c (n "tokio-os-timer") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "1rbr10ysyrz3a0mwnid1870pbbirmj2rzm3v735hqm5mfbbb7j0p")))

(define-public crate-tokio-os-timer-0.1.4 (c (n "tokio-os-timer") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "0nv65r4dfinmyd8r53dd67hmm4wji0zn19z74764rqz80an29gfd")))

(define-public crate-tokio-os-timer-0.1.5 (c (n "tokio-os-timer") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "168d0r5qpwwnc0nw0k3h7fbd7vr78kh7b4c5fabbmfc51x7pq4dc")))

(define-public crate-tokio-os-timer-0.1.6 (c (n "tokio-os-timer") (v "0.1.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "08wiajkjyvyr9wl51xkrayvga8xcnkrdx4rkd2fnz6nnbnki1rz3")))

(define-public crate-tokio-os-timer-0.1.7 (c (n "tokio-os-timer") (v "0.1.7") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "0ccfyggdx9knsw9s5gni2sz37awhjp9lh73i06nb9d1zmd2k8agv")))

(define-public crate-tokio-os-timer-0.1.8 (c (n "tokio-os-timer") (v "0.1.8") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "0akbczy27pfjsmilv6nhp408jypsfys5k6afdxnaxs6600nj6wxm")))

