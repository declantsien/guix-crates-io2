(define-module (crates-io to ki tokio-mock-task) #:use-module (crates-io))

(define-public crate-tokio-mock-task-0.1.0 (c (n "tokio-mock-task") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)))) (h "0d92ny7v8pm52zck0i6z5bwa7mdfkf96j9w3k5g3cha50mghrdya")))

(define-public crate-tokio-mock-task-0.1.1 (c (n "tokio-mock-task") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)))) (h "1y7q83qfk9ljjfvs82b453pmz9x1v3d6kr4x55j8mal01s6790dw")))

