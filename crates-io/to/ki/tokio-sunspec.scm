(define-module (crates-io to ki tokio-sunspec) #:use-module (crates-io))

(define-public crate-tokio-sunspec-0.1.0 (c (n "tokio-sunspec") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.6") (f (quote ("tcp" "server" "tcp-server-unstable"))) (k 0)))) (h "02y8hv558243bg9b7kjmkxllxw8f9l5qd7547znd86p2g0vrzk1r")))

(define-public crate-tokio-sunspec-0.2.0 (c (n "tokio-sunspec") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.6") (f (quote ("tcp" "rtu" "server" "tcp-server-unstable"))) (k 0)) (d (n "tokio-serial") (r "^5.4.1") (d #t) (k 0)))) (h "1s0ram36cvh2qz7raapvmj1xcrj6ifvxl95wqxr8c3yvpisx681i")))

(define-public crate-tokio-sunspec-0.2.1 (c (n "tokio-sunspec") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.6") (f (quote ("tcp" "rtu" "server" "tcp-server-unstable"))) (k 0)) (d (n "tokio-serial") (r "^5.4.1") (d #t) (k 0)))) (h "13pcdcl0al670880d0njmzpgw9yfdllcx5lwy75c2wn0p97nvgdh")))

(define-public crate-tokio-sunspec-0.2.2 (c (n "tokio-sunspec") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.7.1") (f (quote ("tcp" "rtu" "server" "tcp-server-unstable"))) (o #t) (k 0)) (d (n "tokio-serial") (r "^5.4.4") (o #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("time"))) (d #t) (k 2)))) (h "0i04yw113s1l1f3ng0psb8f5v61nyzp4hcp63amn54dn8ns1q7rb") (f (quote (("tcp" "tokio-modbus/tcp") ("default" "tcp" "rtu")))) (s 2) (e (quote (("rtu" "tokio-modbus/rtu" "dep:tokio-serial"))))))

