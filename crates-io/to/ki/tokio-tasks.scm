(define-module (crates-io to ki tokio-tasks) #:use-module (crates-io))

(define-public crate-tokio-tasks-0.1.0 (c (n "tokio-tasks") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ordered-locks") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync" "time" "rt"))) (k 0)))) (h "1y3xf3absv4i2m0lypf1r9fb6bw75nwpkrw7ll2vlppmhhl7hvgk") (f (quote (("pause"))))))

(define-public crate-tokio-tasks-0.1.1 (c (n "tokio-tasks") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ordered-locks") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync" "time" "rt"))) (k 0)))) (h "0vb6jg4qq97swjx4qqjvqxvwv0p62fglkmvzx1853h124slrdnnj") (f (quote (("pause"))))))

(define-public crate-tokio-tasks-0.1.2 (c (n "tokio-tasks") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ordered-locks") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync" "time" "rt"))) (k 0)))) (h "00d8xhvdx52xj21n1hp95m4hp5xdy29bxbf7i1b2chka7frcj517") (f (quote (("pause"))))))

(define-public crate-tokio-tasks-0.1.3 (c (n "tokio-tasks") (v "0.1.3") (d (list (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ordered-locks") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync" "time" "rt"))) (k 0)))) (h "169x6c6gpq5xj7cg76vf8br7spsx5pgqdpispbsv2ws0la4idbr7") (f (quote (("runtoken-id") ("pause"))))))

(define-public crate-tokio-tasks-0.2.0 (c (n "tokio-tasks") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-locks") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (k 0)))) (h "1kq7x10ddawmn8can402vd287ximzmfl51is5f0bhx0ps2n8vvjp") (f (quote (("runtoken-id") ("pause"))))))

(define-public crate-tokio-tasks-0.2.1 (c (n "tokio-tasks") (v "0.2.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-locks") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt"))) (k 0)))) (h "1xm5yl67hli69xvv59jxjkrr003vbfifyzkgs5nnzm1ydlp7x554") (f (quote (("runtoken-id") ("pause"))))))

