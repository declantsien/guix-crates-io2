(define-module (crates-io to ki tokio-stream-ext) #:use-module (crates-io))

(define-public crate-tokio-stream-ext-0.1.4 (c (n "tokio-stream-ext") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (d #t) (k 0)))) (h "0zgik37lldypj8hcm1ngriqwy7ps5kkgnsp9y0hlhznli6pfh8ap")))

(define-public crate-tokio-stream-ext-0.1.5 (c (n "tokio-stream-ext") (v "0.1.5") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (d #t) (k 2)))) (h "0kr26nygj6zs520sncrl19pjr4bxp5d51vmwn3g9kr7jd0s62q0i")))

