(define-module (crates-io to ki tokio-icmp) #:use-module (crates-io))

(define-public crate-tokio-icmp-0.1.0 (c (n "tokio-icmp") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21.0") (d #t) (k 0)) (d (n "socket2") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.0") (d #t) (k 0)))) (h "0qj40vrdfqf59s5102ms7rbj7d2qhngfaw4wshjl4c327v6kcrbh")))

