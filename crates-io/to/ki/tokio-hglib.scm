(define-module (crates-io to ki tokio-hglib) #:use-module (crates-io))

(define-public crate-tokio-hglib-0.1.0 (c (n "tokio-hglib") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "1310j0n2cbzl9vlqa1cb3qqs859qv62n9c4sw7q4mfdqr2djf420")))

(define-public crate-tokio-hglib-0.2.0 (c (n "tokio-hglib") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0v8y41navy2q47n5m96vpkpfz7hmpcs4dr6sxmfakj36p0y8q4wa")))

(define-public crate-tokio-hglib-0.3.0 (c (n "tokio-hglib") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "io-util" "io-std" "uds" "process" "macros" "stream"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1jjba4qnhvlnnh8ykmxvbin20saz9xh26i04l5kvy7li8ifjnzld")))

(define-public crate-tokio-hglib-0.4.0 (c (n "tokio-hglib") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "io-util" "io-std" "net" "process" "macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "02pxq423552i15rz6rjhbfvwhivfpkjl1krk179m5qpn537bkwiv")))

