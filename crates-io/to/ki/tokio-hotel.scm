(define-module (crates-io to ki tokio-hotel) #:use-module (crates-io))

(define-public crate-tokio-hotel-0.1.0 (c (n "tokio-hotel") (v "0.1.0") (h "0yg6b3xnyyy6y7s66wamkar1rf39dg1z6x065sk857mg4lbqmhvi")))

(define-public crate-tokio-hotel-0.1.1 (c (n "tokio-hotel") (v "0.1.1") (h "0bz0zkjmn01nym7d3rz89dmrkmv474kajmj6bx0wh5z71yarym98")))

(define-public crate-tokio-hotel-0.1.2 (c (n "tokio-hotel") (v "0.1.2") (h "1klj0d19z1mx55qsnhr9sil9piagdbs1jf3if48w8pml8kk4n4z0")))

(define-public crate-tokio-hotel-0.1.3 (c (n "tokio-hotel") (v "0.1.3") (h "0xngvc8lyrwxirw9h6fbpjvx8m08yjrwc6k83hwnc0hsy674l83x")))

