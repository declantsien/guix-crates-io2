(define-module (crates-io to ki tokio-tls-gmssl) #:use-module (crates-io))

(define-public crate-tokio-tls-gmssl-0.1.0 (c (n "tokio-tls-gmssl") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (k 2)) (d (n "futures") (r "^0.3.0") (f (quote ("async-await"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "native-tls-gmssl") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "io-util" "net"))) (d #t) (k 2)))) (h "1s30hvw2im2iyh0y3km8y2j5945vn44s0wx38dr3niasnc27w6y6") (f (quote (("vendored" "native-tls-gmssl/vendored"))))))

