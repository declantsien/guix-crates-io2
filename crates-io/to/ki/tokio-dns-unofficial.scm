(define-module (crates-io to ki tokio-dns-unofficial) #:use-module (crates-io))

(define-public crate-tokio-dns-unofficial-0.1.0 (c (n "tokio-dns-unofficial") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0chfs2rblgil3pjcgsa75h0bfkia7fxc35lcqzilfa1ar5nqdhw7")))

(define-public crate-tokio-dns-unofficial-0.1.1 (c (n "tokio-dns-unofficial") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1bbaci04nlkhjbczbh1baa1rhvr6szadz5lxd3fva49acmm3kjim")))

(define-public crate-tokio-dns-unofficial-0.2.0 (c (n "tokio-dns-unofficial") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "04knh7fq6ap5d0ffi5lw0a838rf03q3wm01w64zq4vwqlxjyn98p")))

(define-public crate-tokio-dns-unofficial-0.3.0 (c (n "tokio-dns-unofficial") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1yl8yh1x6famx2w9p2lz7h4ymz6gdd7yr9ph8v0pys2jrw21lb0i")))

(define-public crate-tokio-dns-unofficial-0.3.1 (c (n "tokio-dns-unofficial") (v "0.3.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0a5kibfargm40c1cn4bhq9vrb33difllhgpczbrg4fy5l8ngd6xv")))

(define-public crate-tokio-dns-unofficial-0.1.2 (c (n "tokio-dns-unofficial") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0xl3yaf9rcvvqskq1b656lq1z30g4qqfv9k662gr4q7nps49nl5y")))

(define-public crate-tokio-dns-unofficial-0.4.0 (c (n "tokio-dns-unofficial") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0xd9zlb0wz3h84qlgkihmw7mjn256siqk4rsxys93sslvf1m9il2")))

