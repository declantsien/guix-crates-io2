(define-module (crates-io to ki tokio-copy-with-buffer) #:use-module (crates-io))

(define-public crate-tokio-copy-with-buffer-0.1.0 (c (n "tokio-copy-with-buffer") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0k5as1m2qq9kxlrbqnl7hj50vh1gj2iy0j8djzdazws2d56znyys")))

