(define-module (crates-io to ki tokio-command-fds) #:use-module (crates-io))

(define-public crate-tokio-command-fds-0.2.1 (c (n "tokio-command-fds") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "nix") (r ">=0.22") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("process"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("process" "rt" "macros"))) (d #t) (k 2)))) (h "1ig9bgfdly57dy351sk5w2s7j718wjabhmf5y8sv06dlmvrlbg3f")))

