(define-module (crates-io to ki tokio-ctrlc-error) #:use-module (crates-io))

(define-public crate-tokio-ctrlc-error-0.1.0 (c (n "tokio-ctrlc-error") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)) (d (n "tokio-signal") (r "^0.2") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.11") (d #t) (k 2)))) (h "17yskkh5sjamnc7mp0rkwqjdrc0gcsmwmmzz8rw5sb1bi5jl4ci1")))

