(define-module (crates-io to ki tokio-stream-extra) #:use-module (crates-io))

(define-public crate-tokio-stream-extra-0.0.1 (c (n "tokio-stream-extra") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.11") (f (quote ("io-util"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1bq7ipg5srhp1prkidxy4f9c53xa9cjcl0vxgzddnxh20kv1jpc6")))

(define-public crate-tokio-stream-extra-0.0.2 (c (n "tokio-stream-extra") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.11") (f (quote ("io-util"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0d984714lfgpd8svrfjnm2aji6j6fhc90pj8c2is1mqx524f8kiz")))

(define-public crate-tokio-stream-extra-0.0.3 (c (n "tokio-stream-extra") (v "0.0.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.11") (f (quote ("io-util"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1g85zazg4fbh38f97ykcyx3g9f8m06w25h14s5ddlniji126zjfx")))

