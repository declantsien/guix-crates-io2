(define-module (crates-io to ki tokio-async-write-utility) #:use-module (crates-io))

(define-public crate-tokio-async-write-utility-0.1.0 (c (n "tokio-async-write-utility") (v "0.1.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-pipe") (r "^0.2.5") (d #t) (k 2)))) (h "15826ggd3gs4zdvb5lkx2y8ckn32qw3wpf5dmkjrn1lm2d4khbsx")))

