(define-module (crates-io to ki tokio-splice) #:use-module (crates-io))

(define-public crate-tokio-splice-0.1.0 (c (n "tokio-splice") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("net" "macros" "rt" "signal"))) (d #t) (k 2)))) (h "019843d2kmya8rb676fws8rbyb1p0djb8l81iz150lkvsf2wcy12")))

(define-public crate-tokio-splice-0.2.0 (c (n "tokio-splice") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.142") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("net" "macros" "rt" "signal"))) (d #t) (k 2)))) (h "1dl7iiszc52spnn460hxyc37s72hw4hywq0l76lmmyy8pxpcsy3f")))

