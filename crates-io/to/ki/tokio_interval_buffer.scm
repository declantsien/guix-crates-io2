(define-module (crates-io to ki tokio_interval_buffer) #:use-module (crates-io))

(define-public crate-tokio_interval_buffer-0.1.0 (c (n "tokio_interval_buffer") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "irc") (r "^0.13") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1lbq7g95v2q4x5vz9hlva091zcndgkz4i4ygi57x8igwy772sd04")))

(define-public crate-tokio_interval_buffer-0.2.0 (c (n "tokio_interval_buffer") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "irc") (r "^0.15") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1jz66qakn30wgjfjza2mqw4cz2190cpj92jh9mxli7k04f3ras3w")))

