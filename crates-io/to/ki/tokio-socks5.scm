(define-module (crates-io to ki tokio-socks5) #:use-module (crates-io))

(define-public crate-tokio-socks5-0.1.0 (c (n "tokio-socks5") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns") (r "^0.9.2") (k 0)))) (h "140h29ayx2m26pjkm4knvbfacdp1qirpdxpgl0fdjibyl22p3fb0")))

(define-public crate-tokio-socks5-0.1.1 (c (n "tokio-socks5") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0n20k3z3llqcgi0p72xx1y27j5pq7v3r34pyrykgrkgwyjdwm0s2")))

(define-public crate-tokio-socks5-0.1.2 (c (n "tokio-socks5") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0q1a0ivxymmj1g5m2yfd8pnnhqrqcyq060dyy3y2dy9d9cmp2bgf")))

(define-public crate-tokio-socks5-0.1.3 (c (n "tokio-socks5") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "16k2a4wfm1zmm8khip5gbxks5rcd1dlb01c9b93j920037ww48qa")))

(define-public crate-tokio-socks5-0.1.4 (c (n "tokio-socks5") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.5") (d #t) (k 2)))) (h "1wc04fsqr5b9q5wyc7ys66s5kx8q3fjqksibnfdv32rsc6afxpxd")))

