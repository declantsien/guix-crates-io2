(define-module (crates-io to ki tokio-current-thread) #:use-module (crates-io))

(define-public crate-tokio-current-thread-0.0.0 (c (n "tokio-current-thread") (v "0.0.0") (h "001m663xcn1nnj7dvp3m81bq8625kjgd27j0a4qaada452ixr1w3")))

(define-public crate-tokio-current-thread-0.1.0 (c (n "tokio-current-thread") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "1q8ha9ncba9s70ki90z8sp3bhx36mjmxczdpyddl5pibjrjm4y4z")))

(define-public crate-tokio-current-thread-0.1.1 (c (n "tokio-current-thread") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "19x83yrnqqs2d5dkgjzybpys1zfvbqhhkgbnh1hnzhcad2cvipwg")))

(define-public crate-tokio-current-thread-0.1.2 (c (n "tokio-current-thread") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "1w3ip3nd0q4iq1ccdh0d12aac8wllnyj0s7kx34sh28hnl4dpnip")))

(define-public crate-tokio-current-thread-0.1.3 (c (n "tokio-current-thread") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (d #t) (k 0)))) (h "13336s21ramki8yq2gqj2bxhbhp5m3xnm5wa8dnlj2igjn8cs3zr")))

(define-public crate-tokio-current-thread-0.1.4 (c (n "tokio-current-thread") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (d #t) (k 0)))) (h "1xjqzdrm42a054jpf2yjsaz4bsmzrf6n350cxc3fqmbq4v68l71k")))

(define-public crate-tokio-current-thread-0.1.5 (c (n "tokio-current-thread") (v "0.1.5") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.5") (d #t) (k 0)))) (h "18x9jchjw9pz0w75kbwm970348qa86glxjkglh11k8pfh13b0mn7")))

(define-public crate-tokio-current-thread-0.1.6 (c (n "tokio-current-thread") (v "0.1.6") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.7") (d #t) (k 0)))) (h "0hx4c8v88kk0ih8x5s564gsgwwf8n11kryvxm72l1f7isz51fqni")))

(define-public crate-tokio-current-thread-0.2.0-alpha.1 (c (n "tokio-current-thread") (v "0.2.0-alpha.1") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "tokio-executor") (r "= 0.2.0-alpha.1") (d #t) (k 0)) (d (n "tokio-sync") (r "= 0.2.0-alpha.1") (d #t) (k 2)))) (h "11wld61mh95pjmw2pw2rv414h1a51zbj4xkpb0bs0jp6xy1hklsf")))

(define-public crate-tokio-current-thread-0.1.7 (c (n "tokio-current-thread") (v "0.1.7") (d (list (d (n "futures") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.7") (d #t) (k 0)))) (h "03p2w316ha0irgzvy37njx9hl71133gcrmrq4801w4rzm0r0xpmi")))

