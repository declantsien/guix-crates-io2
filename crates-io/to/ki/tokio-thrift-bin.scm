(define-module (crates-io to ki tokio-thrift-bin) #:use-module (crates-io))

(define-public crate-tokio-thrift-bin-0.1.0 (c (n "tokio-thrift-bin") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tokio-thrift-codegen") (r "^0.1") (d #t) (k 0)))) (h "1gmv3d86q3iv73r6dgdfw0xlvjxl8vsbk7wkjbf2s32szqmnf31y")))

