(define-module (crates-io to ki tokio-notify-drop) #:use-module (crates-io))

(define-public crate-tokio-notify-drop-0.9.0 (c (n "tokio-notify-drop") (v "0.9.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync"))) (d #t) (k 0)))) (h "1cfrxd8y0c84aq3gbqipb9x25ck856w0fwvq6vw9aqx3mbihjlnp")))

(define-public crate-tokio-notify-drop-0.9.1 (c (n "tokio-notify-drop") (v "0.9.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync"))) (d #t) (k 0)))) (h "1skdf07r48lr19r9rrl3mb4d5f8j825zrxb8gfr3k3nb36pw6dp8")))

(define-public crate-tokio-notify-drop-0.9.2 (c (n "tokio-notify-drop") (v "0.9.2") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync"))) (d #t) (k 0)))) (h "0zw7h1agy558kvhqfz59g13lw8k7w19v8j744y76hz3pbk0k0hz7")))

(define-public crate-tokio-notify-drop-0.9.3 (c (n "tokio-notify-drop") (v "0.9.3") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync"))) (d #t) (k 0)))) (h "1vvjyl4n2a89r9sxx0796wsd3qyxqzmr0b5dywrinsp5787a37ap")))

(define-public crate-tokio-notify-drop-0.9.4 (c (n "tokio-notify-drop") (v "0.9.4") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync"))) (d #t) (k 0)))) (h "1rjk1dk40by8hzcpcgxbj9wqb1h4s3gkdldzb616a4w478gk52bi")))

(define-public crate-tokio-notify-drop-0.10.0 (c (n "tokio-notify-drop") (v "0.10.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync"))) (d #t) (k 0)))) (h "1hc35z4gd5sf586nzyxzm2dy2q52kjngz5400i7i716c9pr18pwz")))

(define-public crate-tokio-notify-drop-1.0.0 (c (n "tokio-notify-drop") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync"))) (d #t) (k 0)))) (h "13z206vaknjvhvm0r3yxpb1dgajc7gj5n5v5bkmadwplacrqbf0j")))

(define-public crate-tokio-notify-drop-2.0.0 (c (n "tokio-notify-drop") (v "2.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^5.0.0") (d #t) (k 0)))) (h "18bnmw6b7sb8sg7n9j7gfc6662vspvr3vw3m2m6id4ila5d95x27")))

