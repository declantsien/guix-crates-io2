(define-module (crates-io to ki tokio-jsoncodec) #:use-module (crates-io))

(define-public crate-tokio-jsoncodec-0.1.0 (c (n "tokio-jsoncodec") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.9") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1.0") (d #t) (k 0)))) (h "0gca32qacr81a8mlvrnyx0rbsnh9ai6wx4jxhqmhdk1m2x53f1aa")))

