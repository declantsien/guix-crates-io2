(define-module (crates-io to ki tokio-async-attributes) #:use-module (crates-io))

(define-public crate-tokio-async-attributes-1.0.0 (c (n "tokio-async-attributes") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-async-std") (r "^1.5.1") (f (quote ("attributes"))) (d #t) (k 2)))) (h "1qm7smyqkhqd7awpcf5bhgjiap0mjqax169f7qga50px7nnd2lmj")))

