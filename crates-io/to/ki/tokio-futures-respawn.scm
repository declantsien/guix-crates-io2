(define-module (crates-io to ki tokio-futures-respawn) #:use-module (crates-io))

(define-public crate-tokio-futures-respawn-0.1.0 (c (n "tokio-futures-respawn") (v "0.1.0") (d (list (d (n "tokio") (r "^1.27") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "10gznan0b5msv2gsfh0hcdjmw60zhy8aryarf59hfk4dxgsm6bh8") (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-tokio-futures-respawn-0.1.1 (c (n "tokio-futures-respawn") (v "0.1.1") (d (list (d (n "tokio") (r "^1.27") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "1v6nvzi26bn9s763fxbhc6dxam6xd4r4n3k61l166d8a6517x263") (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-tokio-futures-respawn-0.1.2 (c (n "tokio-futures-respawn") (v "0.1.2") (d (list (d (n "tokio") (r "^1.27") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "1dyfvpia0mb0qwq5wmkh3vlzms5a59gpgf2wzn9hmwhkqic3z5h8") (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-tokio-futures-respawn-0.1.3 (c (n "tokio-futures-respawn") (v "0.1.3") (d (list (d (n "tokio") (r "^1.27") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 2)))) (h "070j3n0g0j53rv7na2ra2729ml9x38yv0rpv91l8w9483d0pss59") (s 2) (e (quote (("tracing" "dep:tracing"))))))

