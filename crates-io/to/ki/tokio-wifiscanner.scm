(define-module (crates-io to ki tokio-wifiscanner) #:use-module (crates-io))

(define-public crate-tokio-wifiscanner-0.1.0 (c (n "tokio-wifiscanner") (v "0.1.0") (h "0hsz4myf8hplmqrjl7xr0m43gwq1mn0y86v6k58cqz5929h4i2s4")))

(define-public crate-tokio-wifiscanner-0.2.0 (c (n "tokio-wifiscanner") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.0.2") (f (quote ("process"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros" "rt" "process"))) (d #t) (k 2)))) (h "1z1y2b1q027wmk1wj1xz7yjhxpnxar88mpznb1n59673fk4z2379")))

(define-public crate-tokio-wifiscanner-0.2.1 (c (n "tokio-wifiscanner") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.0.2") (f (quote ("process"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros" "rt" "process"))) (d #t) (k 2)))) (h "0ppyaad1d972nhzw3wz27k3g2b9fsa95ns8cc0im762il1k1nfzb")))

