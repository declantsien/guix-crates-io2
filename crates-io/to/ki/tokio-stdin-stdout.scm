(define-module (crates-io to ki tokio-stdin-stdout) #:use-module (crates-io))

(define-public crate-tokio-stdin-stdout-0.1.0 (c (n "tokio-stdin-stdout") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0q6n4i5z03kzcrwgz27nmjhfss3069wl5vmm7xg3dzpxb8v3pg89")))

(define-public crate-tokio-stdin-stdout-0.1.1 (c (n "tokio-stdin-stdout") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0zbrlgcqwpsh6qnx20q83in70yiw04gyxxfi1gy5g9r1rf7ac5di")))

(define-public crate-tokio-stdin-stdout-0.1.2 (c (n "tokio-stdin-stdout") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1fryn85qhigr734703yz9pjqlnkd91k70n1kx7xx2vviqvhm3dx7")))

(define-public crate-tokio-stdin-stdout-0.1.3 (c (n "tokio-stdin-stdout") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "153v7ph1x14r8vz6bv1l0sazyym3s5a7zhn89a2xvvk2lf7365id")))

(define-public crate-tokio-stdin-stdout-0.1.4 (c (n "tokio-stdin-stdout") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1qw6i93a54g62anlfqqdfv3i6brh6jfx5jca895gdcps33p0x5h6")))

(define-public crate-tokio-stdin-stdout-0.1.5 (c (n "tokio-stdin-stdout") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "05qcf9nigsra3m1qcvij5ws6p2sn8fa7ypm6iqpsa3ri0p981i0z")))

