(define-module (crates-io to ki tokio-global) #:use-module (crates-io))

(define-public crate-tokio-global-0.1.0 (c (n "tokio-global") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0nsnylyl41sr8b54ng4qhz0c0f2lvg1lyjfdv07nxfmxcjznk2xl") (f (quote (("single") ("multi") ("default"))))))

(define-public crate-tokio-global-0.2.0 (c (n "tokio-global") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "10bfp95xjlphn6ls4cp2fnxwjpacpifbnmgv1zdhmcava6g9kh90") (f (quote (("single") ("multi") ("default"))))))

(define-public crate-tokio-global-0.3.0 (c (n "tokio-global") (v "0.3.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "net" "io-util"))) (d #t) (k 2)))) (h "17lr1l4p153ki89d1qp014x6bprwx17flxmlaks2vch88y0qr4zx")))

(define-public crate-tokio-global-0.4.0 (c (n "tokio-global") (v "0.4.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "net" "io-util"))) (d #t) (k 2)))) (h "12nzpksbfr6rwdw4rlz153qdfmkql23r75sqw3zj9zrs1szcb1jw")))

