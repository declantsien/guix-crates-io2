(define-module (crates-io to ki tokio-resol-vbus) #:use-module (crates-io))

(define-public crate-tokio-resol-vbus-0.1.0 (c (n "tokio-resol-vbus") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "resol-vbus") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-serial") (r "^3.1.0") (d #t) (k 2)))) (h "0q4ifz8ck6f6adja14f3fnxpld2qc7d85sly5q9dgka7gbjz8ixp")))

