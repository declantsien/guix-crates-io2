(define-module (crates-io to ki tokio-u8-codec) #:use-module (crates-io))

(define-public crate-tokio-u8-codec-0.1.0 (c (n "tokio-u8-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1vjsipl0z5wrwny16glvmkk1nrc9kvglqssg66b7dh395yfc0rra")))

