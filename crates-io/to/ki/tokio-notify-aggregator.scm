(define-module (crates-io to ki tokio-notify-aggregator) #:use-module (crates-io))

(define-public crate-tokio-notify-aggregator-0.1.0 (c (n "tokio-notify-aggregator") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zdip87whi3r1mjld3ng6592xz7bfdgjsq1n5i6k1i6yry7i7pld") (y #t)))

(define-public crate-tokio-notify-aggregator-0.1.1 (c (n "tokio-notify-aggregator") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ikd34h5qydb28dsp3b6cb48byj55kq1ndxjl81wk30xqfspcrkm")))

