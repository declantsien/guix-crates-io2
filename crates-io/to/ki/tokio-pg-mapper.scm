(define-module (crates-io to ki tokio-pg-mapper) #:use-module (crates-io))

(define-public crate-tokio-pg-mapper-0.1.0 (c (n "tokio-pg-mapper") (v "0.1.0") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "1g6rcx6wybnr3azca51g1zmrcg76gljmv64n9n6kkwqfcy9k152r") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.1 (c (n "tokio-pg-mapper") (v "0.1.1") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "0rflpm94xdvpzyad3qjy3lzwix98df7l9z8abzcxwdgg7zl9qfjf") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.3 (c (n "tokio-pg-mapper") (v "0.1.3") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "0zq1i0j9s1k2si1h5n0l5jr0fn19x37cih7nnjn93m036sn5dj99") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.4 (c (n "tokio-pg-mapper") (v "0.1.4") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "12xj9zh04rkgqrdrx3i5lz875zcxnn5wrs87k65dlybx66jfkxgj") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.5 (c (n "tokio-pg-mapper") (v "0.1.5") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "0fbrssidz4b185d3mnjs47aibd44axhw3wr6nhjc63v8rxlima31") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.6 (c (n "tokio-pg-mapper") (v "0.1.6") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "08vwy4ix3wgv58xr6avyyd9vlvkbj2n3aa6w1ab3hh6d7kv5hqah") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.7 (c (n "tokio-pg-mapper") (v "0.1.7") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "0fipw5yz2dlq40xw49qlc03kq2v3wx3xlay8hxgnnnnawr7sckzb") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.8 (c (n "tokio-pg-mapper") (v "0.1.8") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.4") (d #t) (k 0)))) (h "16d7rzjrd76d7lzni4865anb6g5a1v1i65hq5f042pl142g5gqf9") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

(define-public crate-tokio-pg-mapper-0.1.9 (c (n "tokio-pg-mapper") (v "0.1.9") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "1gnqn8c90lgfwhak6fl9a6i0716hljm1hai7771rghpzclcla9q9") (f (quote (("derive" "tokio-pg-mapper-derive")))) (y #t)))

(define-public crate-tokio-pg-mapper-0.2.0 (c (n "tokio-pg-mapper") (v "0.2.0") (d (list (d (n "tokio-pg-mapper-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "1f7axj26yw4cir6mqv48ahm9caginaxp4g2mpkx3yf366n7vgwlk") (f (quote (("derive" "tokio-pg-mapper-derive"))))))

