(define-module (crates-io to ki tokio-task-tracker) #:use-module (crates-io))

(define-public crate-tokio-task-tracker-1.0.0 (c (n "tokio-task-tracker") (v "1.0.0") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "1abfk3rmkd83jm75prl8apjvr9a92yf3rrzrzkhwldqmlpgcxbxs")))

(define-public crate-tokio-task-tracker-1.0.1 (c (n "tokio-task-tracker") (v "1.0.1") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "02c07d81pks24w1g9kxfgcp7qvxmarj07z9k6m6b92y475r5s70p")))

(define-public crate-tokio-task-tracker-1.1.0 (c (n "tokio-task-tracker") (v "1.1.0") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "18x0walwq5gjvlrayfc4ivwsvz2983qvibcldvbjlb2sdknikzid")))

(define-public crate-tokio-task-tracker-1.1.1 (c (n "tokio-task-tracker") (v "1.1.1") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "01ddgkwm9a6akpwbmllsa3bqf2n4mbvgnzipfm3y1mm4y8qsbp4x")))

(define-public crate-tokio-task-tracker-1.2.0 (c (n "tokio-task-tracker") (v "1.2.0") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "05pm964g8drpn6xfxny6rsmjk6y7d5xsdaci8v5bqgngmb6l1lvg")))

(define-public crate-tokio-task-tracker-1.3.0 (c (n "tokio-task-tracker") (v "1.3.0") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "1lz1i272j68cz1ni76297yzskx2xqp33yprii4cpxyxbwkl32pf3")))

(define-public crate-tokio-task-tracker-1.3.1 (c (n "tokio-task-tracker") (v "1.3.1") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "1szf0y0dc25canglzmap20dcysw0dh8zk8qkycd7zrrk22ywa858")))

(define-public crate-tokio-task-tracker-1.3.2 (c (n "tokio-task-tracker") (v "1.3.2") (d (list (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "signal" "time" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "15jjl880s9n0nla365sgshbkqvdc0sb5k8q3vxhb3vcpqmppl2bc")))

