(define-module (crates-io to ki tokio-timer-plus) #:use-module (crates-io))

(define-public crate-tokio-timer-plus-0.1.3 (c (n "tokio-timer-plus") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)))) (h "0fh1y2hh8dv11hj5rgjq8qlixkrnv6yg0s9p59sdlh6yj341gfwd")))

