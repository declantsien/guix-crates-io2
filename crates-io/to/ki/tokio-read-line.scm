(define-module (crates-io to ki tokio-read-line) #:use-module (crates-io))

(define-public crate-tokio-read-line-0.1.0 (c (n "tokio-read-line") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0qyd5hggjxkgh3l4k20n737p05p0mi61wn9zqsr1jv0m1q0a266d")))

