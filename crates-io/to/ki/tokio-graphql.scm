(define-module (crates-io to ki tokio-graphql) #:use-module (crates-io))

(define-public crate-tokio-graphql-0.1.0 (c (n "tokio-graphql") (v "0.1.0") (d (list (d (n "bitflags") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "153hxfiiqf25gmlgw9pp6ixzf4zj7mb93y7hxx0wch3v3944fzch")))

