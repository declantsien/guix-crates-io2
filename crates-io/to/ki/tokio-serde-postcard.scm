(define-module (crates-io to ki tokio-serde-postcard) #:use-module (crates-io))

(define-public crate-tokio-serde-postcard-0.1.0 (c (n "tokio-serde-postcard") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("alloc" "use-std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio-serde") (r "^0.8") (d #t) (k 0)))) (h "0aa3k1kc1nwkgfaqrhvm5hypqhx7lc412x5fqjdf0anr9hrlgg59")))

