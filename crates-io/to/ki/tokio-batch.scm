(define-module (crates-io to ki tokio-batch) #:use-module (crates-io))

(define-public crate-tokio-batch-0.1.0 (c (n "tokio-batch") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)))) (h "0bb5y4dk9kahlm8l8q8w153j86zir2058l2jghljch4d3bcq5ais")))

(define-public crate-tokio-batch-0.1.1 (c (n "tokio-batch") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)))) (h "177nfjdcfqi810z63bbvrqrpsfyilc9s9rzkwzqv87j520cqmdx0")))

(define-public crate-tokio-batch-0.2.0 (c (n "tokio-batch") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tokio") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.8") (d #t) (k 0)))) (h "19xcrg7nrvima4l9r5179d5l5fk9432449682sbqdvfkfi3jcbsn")))

(define-public crate-tokio-batch-0.4.0 (c (n "tokio-batch") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("compat"))) (d #t) (k 0)) (d (n "futures01") (r "^0.1.29") (d #t) (k 0) (p "futures")) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.11") (d #t) (k 0)))) (h "13k38kjv749ba7lk2z7x58cc8yba37lmlm9vd418agjgdjwmb0b6")))

(define-public crate-tokio-batch-0.5.0 (c (n "tokio-batch") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("compat"))) (d #t) (k 0)) (d (n "futures-timer") (r "^1.0.2") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "0abhns76mqxjh43qzd9nvwjgzhmh94c70qw8nazfrs1b33g3dfis")))

(define-public crate-tokio-batch-0.5.1 (c (n "tokio-batch") (v "0.5.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("compat"))) (d #t) (k 0)) (d (n "futures-timer") (r "^2.0.2") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "1irp2piyns5qwvsrbrv0h9bh2yzxy91301slx7zw555fray4azfs")))

