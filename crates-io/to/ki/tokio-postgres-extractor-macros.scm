(define-module (crates-io to ki tokio-postgres-extractor-macros) #:use-module (crates-io))

(define-public crate-tokio-postgres-extractor-macros-0.7.0 (c (n "tokio-postgres-extractor-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1f0wxmfkydww6816shiw37hj0rcn5wh1w9j4j2s062awpkmpxack")))

