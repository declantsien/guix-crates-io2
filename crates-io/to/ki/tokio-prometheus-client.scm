(define-module (crates-io to ki tokio-prometheus-client) #:use-module (crates-io))

(define-public crate-tokio-prometheus-client-0.1.0 (c (n "tokio-prometheus-client") (v "0.1.0") (d (list (d (n "prometheus-client") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-metrics") (r "^0.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1g9ayf64zhd66320w8q64h8yf5nccwkgwrdz33l5c8s9zkbfn9ff")))

(define-public crate-tokio-prometheus-client-0.1.1 (c (n "tokio-prometheus-client") (v "0.1.1") (d (list (d (n "prometheus-client") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-metrics") (r "^0.3.1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1n3m77aviymrqkv2a6jzpwk2050wczxaz77rbb24mcd1cjb5spac")))

