(define-module (crates-io to ki tokio_env) #:use-module (crates-io))

(define-public crate-tokio_env-0.0.1 (c (n "tokio_env") (v "0.0.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1mgrw0ylbbdax07g4ilcwx4w587shsiy1ilgsh4dp0drsy7ng4g1")))

(define-public crate-tokio_env-0.0.2 (c (n "tokio_env") (v "0.0.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "111yhhgzzzchvkk775lb6bywlvrhh9nb69l1dmld4223g83m9x4j")))

(define-public crate-tokio_env-0.1.0 (c (n "tokio_env") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio_env_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0aml2ym14xjli6k067qdaqm9lmcq0xiwb56d568icg2nhs2j4r9p")))

