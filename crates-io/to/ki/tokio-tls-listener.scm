(define-module (crates-io to ki tokio-tls-listener) #:use-module (crates-io))

(define-public crate-tokio-tls-listener-0.1.0 (c (n "tokio-tls-listener") (v "0.1.0") (d (list (d (n "rustls-pemfile") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net"))) (k 0)) (d (n "tokio-rustls") (r "^0.24") (k 0)))) (h "005s6skxvafrh0f733bb10byjfrn9640jxfj66lcvb01rgvzazgv") (f (quote (("tls12" "tokio-rustls/tls12") ("logging" "tokio-rustls/logging") ("early-data" "tokio-rustls/early-data") ("dangerous_configuration" "tokio-rustls/dangerous_configuration"))))))

