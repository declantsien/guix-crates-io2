(define-module (crates-io to ki tokio-terminal-resize) #:use-module (crates-io))

(define-public crate-tokio-terminal-resize-0.1.0 (c (n "tokio-terminal-resize") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (f (quote ("futures-01"))) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2") (d #t) (k 0)))) (h "1i2fq3jp19gbr1pd77b0qkpq06hnlcnz66nyi5qsx8llpyjia6ch")))

