(define-module (crates-io to ki tokio-socks-unofficial) #:use-module (crates-io))

(define-public crate-tokio-socks-unofficial-0.1.0 (c (n "tokio-socks-unofficial") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "futures-await") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "05vpdssffkx8n497hzw8nsfrq3w7rlcdik7jasqw34wmha4xva0r")))

(define-public crate-tokio-socks-unofficial-0.1.1 (c (n "tokio-socks-unofficial") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "futures-await") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "18nqx9z2xpp6pr31r0bx1pm5z6gwdhlkygdakjzxkzsmsc4fczii")))

(define-public crate-tokio-socks-unofficial-0.1.2 (c (n "tokio-socks-unofficial") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.4.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "futures-await") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "159pdafm540qxj1mpb9bl59mkh2si3wpkrbg0zwflhd77sckzn0f")))

(define-public crate-tokio-socks-unofficial-0.1.3 (c (n "tokio-socks-unofficial") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.4.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "futures-await") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "05bczgvk5pdnid5plv6rfb7l06bikrxgmvk6l3n15pc7aqhpnxj3")))

