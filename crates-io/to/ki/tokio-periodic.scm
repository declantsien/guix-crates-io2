(define-module (crates-io to ki tokio-periodic) #:use-module (crates-io))

(define-public crate-tokio-periodic-0.1.0 (c (n "tokio-periodic") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0z9pqpxix19q64n8kgy8dfn463vab5b97h2cvrfbyf28d74ww8fc")))

(define-public crate-tokio-periodic-0.1.1 (c (n "tokio-periodic") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "mio") (r "^0.6.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "13sdnl4mxdr645j522blrqf0m58g9lpviy9f8jrf2x5bs1f7la7v")))

