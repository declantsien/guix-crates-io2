(define-module (crates-io to ki tokio-easy-timer) #:use-module (crates-io))

(define-public crate-tokio-easy-timer-0.1.0 (c (n "tokio-easy-timer") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt" "rt-multi-thread" "time" "macros"))) (d #t) (k 0)))) (h "1chfzsj5aanb21y475f69762dmr49560q76q4jpksb9pgm9payac")))

(define-public crate-tokio-easy-timer-0.1.1 (c (n "tokio-easy-timer") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.11") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "teloxide") (r "^0.10") (f (quote ("macros" "auto-send"))) (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("rt" "rt-multi-thread" "time" "macros"))) (d #t) (k 0)))) (h "1ai6r1n5j50rpgcdpxmqw2my4zn2c049gsavzdfg8gyyrjalvq15")))

