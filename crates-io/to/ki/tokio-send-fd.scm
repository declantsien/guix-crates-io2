(define-module (crates-io to ki tokio-send-fd) #:use-module (crates-io))

(define-public crate-tokio-send-fd-0.9.0 (c (n "tokio-send-fd") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "passfd") (r "^0.1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("net" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "1q5wrs9ij5jcvqj5zn6m96rvj5pm1bnbmrmfgajpf23m9arzhqv3")))

(define-public crate-tokio-send-fd-0.9.1 (c (n "tokio-send-fd") (v "0.9.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "passfd") (r "^0.1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("net" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "18q93nc810mwmz3avrd7fsbp8jypnnykrmca151vhp6zcjp79m55")))

(define-public crate-tokio-send-fd-0.9.2 (c (n "tokio-send-fd") (v "0.9.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "passfd") (r "^0.1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("net" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "04hz45np1pxilkpmyn0ydg9v6xcmal5dlxrkkcsyzz5g40jqkgvr")))

(define-public crate-tokio-send-fd-0.9.3 (c (n "tokio-send-fd") (v "0.9.3") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "passfd") (r "^0.1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("net" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "102ksl2vjx51vijj37h8xspy4s03hfs6v6vzxvjh3j29av9nryrp")))

(define-public crate-tokio-send-fd-0.9.4 (c (n "tokio-send-fd") (v "0.9.4") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "passfd") (r "^0.1.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("net" "rt" "macros" "io-util"))) (d #t) (k 0)))) (h "0i6mwnczjxzpkz3h9827saqwyzgilp6z0arfn2wyh3jlb4gy2bn9")))

