(define-module (crates-io to ki tokio_modbus-winets) #:use-module (crates-io))

(define-public crate-tokio_modbus-winets-0.1.0 (c (n "tokio_modbus-winets") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "sungrow-winets") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.5.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "10gy5ca0chhpjkxy9svbc8csbggn274ykpcw6kjxwhwrl8sp5mr0")))

(define-public crate-tokio_modbus-winets-0.2.0 (c (n "tokio_modbus-winets") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "sungrow-winets") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.5.3") (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1qaizfhw4djc1w29nd8q1b0f1pjwck80grbrvcj0a1q1xbpwg8y7")))

