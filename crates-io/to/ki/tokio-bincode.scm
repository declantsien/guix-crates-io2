(define-module (crates-io to ki tokio-bincode) #:use-module (crates-io))

(define-public crate-tokio-bincode-0.1.0 (c (n "tokio-bincode") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "1cgm1jnqk8jbkfmqv9qg4kbrfcplj4plxwb5h031hmb0haadbahc")))

