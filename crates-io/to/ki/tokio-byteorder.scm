(define-module (crates-io to ki tokio-byteorder) #:use-module (crates-io))

(define-public crate-tokio-byteorder-0.1.0 (c (n "tokio-byteorder") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (f (quote ("io"))) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 2)))) (h "12pvg0vl12w70wn3mjdcjncw8pz348g6ps2hr1y5c5mg1p2dajyr")))

(define-public crate-tokio-byteorder-0.2.0 (c (n "tokio-byteorder") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full" "macros"))) (d #t) (k 2)))) (h "0rvm82bi0q6qzanlf2js2jh5xs19n1afmkwclfipmcqb1mxz4imc")))

(define-public crate-tokio-byteorder-0.3.0 (c (n "tokio-byteorder") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "macros"))) (d #t) (k 2)))) (h "0jmsg84w0xnlci4hy780i84q3gbi58bnkmdfr0bgs7qxmvl4gwsw")))

