(define-module (crates-io to ki tokio-inotify) #:use-module (crates-io))

(define-public crate-tokio-inotify-0.2.0 (c (n "tokio-inotify") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "inotify") (r "^0.2.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1rw57nsky4lyhsgag309fgjywx0lgpilmk4vpn87jn7v7mvh87fy")))

(define-public crate-tokio-inotify-0.2.1 (c (n "tokio-inotify") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "inotify") (r "^0.2.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "05gp52m2lg59w7fm1h6k587jbmz0bi1ym9n3sk3662p1mxa4h3j3")))

(define-public crate-tokio-inotify-0.2.2 (c (n "tokio-inotify") (v "0.2.2") (d (list (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "inotify") (r "^0.2.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "0jyj9kzjw8hrigr43kb1gy7gzc962k781r1nvl3pc1ihk3x2bcrr")))

(define-public crate-tokio-inotify-0.3.0 (c (n "tokio-inotify") (v "0.3.0") (d (list (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "inotify") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1ic1qznjly6cgy3n11645nmisby3fbvlbdbcc5631wnmhv1cy50g")))

(define-public crate-tokio-inotify-0.4.0 (c (n "tokio-inotify") (v "0.4.0") (d (list (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "inotify") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "13c6c8c7cybsk4bfhhnqxqxic2njyk1f475as6qgi4mwv9wwj40w")))

(define-public crate-tokio-inotify-0.4.1 (c (n "tokio-inotify") (v "0.4.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "inotify") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "193l1f7x17kq411k6fspx1ac8lqdvpgk4i3hq5429wrn8icmaavg")))

