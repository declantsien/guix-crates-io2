(define-module (crates-io to ki tokio-ioext) #:use-module (crates-io))

(define-public crate-tokio-ioext-0.1.0 (c (n "tokio-ioext") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0g26k89q37v6r9sancrvsla3s64hxsvs0hi71qdx0xhf3qafh0vn")))

