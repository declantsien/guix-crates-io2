(define-module (crates-io to ki tokio-util-codec-compose) #:use-module (crates-io))

(define-public crate-tokio-util-codec-compose-0.1.0 (c (n "tokio-util-codec-compose") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0hamgj47684l9j42yryyixafql7i60daavddffwys0ngzqxi7xmf")))

(define-public crate-tokio-util-codec-compose-0.1.1 (c (n "tokio-util-codec-compose") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1dg60ysqqac7g812fyq1v23aiig0qg4f9xzyssnacfw3bpypbajm")))

