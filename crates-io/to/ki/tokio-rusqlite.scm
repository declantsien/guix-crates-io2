(define-module (crates-io to ki tokio-rusqlite) #:use-module (crates-io))

(define-public crate-tokio-rusqlite-0.1.0 (c (n "tokio-rusqlite") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0l4y8012pvfa7bacfvsfn9csh3ppc0a29bxkqldp0r40qlz6847m")))

(define-public crate-tokio-rusqlite-0.2.0 (c (n "tokio-rusqlite") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0gxh7vi8781i5jvigj724jj53b9mbkhh67al4pcwrm55ch4071y8")))

(define-public crate-tokio-rusqlite-0.3.0 (c (n "tokio-rusqlite") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1rda3c59v0zrg2r0zw87yca375d54sx0wpbbxwl19mkq8y6gq7d3")))

(define-public crate-tokio-rusqlite-0.4.0 (c (n "tokio-rusqlite") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1b7yq4xlpdq0h01h5nbs4bwhar4k5himi50cx6p7y4gzynan79ks")))

(define-public crate-tokio-rusqlite-0.5.0 (c (n "tokio-rusqlite") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0f8xnb6c47306wp22i15hm76nflg7bry36z5h59lawn8s2c5qy6w")))

(define-public crate-tokio-rusqlite-0.5.1 (c (n "tokio-rusqlite") (v "0.5.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09xihas30j72dda5pi3hw5693y79ffby7bs9cpy8kw144iqmzk62")))

