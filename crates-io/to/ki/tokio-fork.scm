(define-module (crates-io to ki tokio-fork) #:use-module (crates-io))

(define-public crate-tokio-fork-0.1.0 (c (n "tokio-fork") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("signal"))) (d #t) (k 0)))) (h "09d2n575nh9irl16rw2nr5kh245v7c4w58q6bdywgixi3hn1dkm3")))

(define-public crate-tokio-fork-0.2.0 (c (n "tokio-fork") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("signal"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "signal"))) (d #t) (k 2)))) (h "00av8jnix454j69wxras1jj8izpbrj1gkzinyvpywzg8s2h34ba6")))

(define-public crate-tokio-fork-0.2.1 (c (n "tokio-fork") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("signal"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "signal"))) (d #t) (k 2)))) (h "1yrbvqvvk4m6fxnvs633d23pmzh73fm0jhxzkc48im5rfihr33q0")))

