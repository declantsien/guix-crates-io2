(define-module (crates-io to ki tokio-into-sink) #:use-module (crates-io))

(define-public crate-tokio-into-sink-0.1.0 (c (n "tokio-into-sink") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-sink") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("fs" "rt"))) (d #t) (k 2)))) (h "1xb269nh7vzwgd17wh5yzk1dmmw306bpmasizw05k6cgh6xxypi0")))

