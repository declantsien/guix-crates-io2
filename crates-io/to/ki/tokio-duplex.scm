(define-module (crates-io to ki tokio-duplex) #:use-module (crates-io))

(define-public crate-tokio-duplex-0.1.0 (c (n "tokio-duplex") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (d #t) (k 0)))) (h "1j7686jdqrr5h1dc6y0czpxrm6x1bjr5si4x7kmgzr0cfmnvhw82") (y #t)))

(define-public crate-tokio-duplex-1.0.0 (c (n "tokio-duplex") (v "1.0.0") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (d #t) (k 0)))) (h "1nwhvlkj5zg4xl3myw51mqqvcj1f6vyyjd4v20cfhhcs61931rgc")))

(define-public crate-tokio-duplex-1.0.1 (c (n "tokio-duplex") (v "1.0.1") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (d #t) (k 0)))) (h "1cyy726dw3raf2p053lsd025spnzjim44hh774bl48vrpvmn1n8a")))

