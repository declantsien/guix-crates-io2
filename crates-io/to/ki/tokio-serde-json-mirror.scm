(define-module (crates-io to ki tokio-serde-json-mirror) #:use-module (crates-io))

(define-public crate-tokio-serde-json-mirror-0.1.0 (c (n "tokio-serde-json-mirror") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 2)) (d (n "tokio-serde") (r "^0.2") (d #t) (k 0)))) (h "0xzbz1ixgj18lnh9mqbpj61s8jh27g53z3zp307g1cyskm2j5izl")))

