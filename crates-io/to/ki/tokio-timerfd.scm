(define-module (crates-io to ki tokio-timerfd) #:use-module (crates-io))

(define-public crate-tokio-timerfd-0.1.0 (c (n "tokio-timerfd") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "timerfd") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "1s1bqgalif6bb31ri9cmgpyg958301c1ynkfyn8jgja22paqr1wf")))

(define-public crate-tokio-timerfd-0.2.0 (c (n "tokio-timerfd") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "timerfd") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "150hlnpr15d3qjrqk66aq51k883g2fy68ynzn51khycvkapcvvl7")))

