(define-module (crates-io to ki tokio-reactor-trait) #:use-module (crates-io))

(define-public crate-tokio-reactor-trait-0.0.0 (c (n "tokio-reactor-trait") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "reactor-trait") (r "^0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "04sd1xsi4q6hn7lik65yqhfiwv4h8v8788dgjly79r69bxr1k5m1")))

(define-public crate-tokio-reactor-trait-0.1.0 (c (n "tokio-reactor-trait") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "time"))) (k 0)))) (h "12pqrp4jvgvpr2v959bqqqi9z8gqfc7cbhcgkw7r0zhwccj4b2jg")))

(define-public crate-tokio-reactor-trait-0.2.0 (c (n "tokio-reactor-trait") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "time"))) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "0kqpsndanwmpci9gyl2xknnjyx7s01c9fnilh7sd58c1ll8gjik8")))

(define-public crate-tokio-reactor-trait-1.0.0 (c (n "tokio-reactor-trait") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "time"))) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1x5cnnkiyj3gmy51kz06a96nsnkcc223j672aymazzxvhqjv9brr")))

(define-public crate-tokio-reactor-trait-1.0.1 (c (n "tokio-reactor-trait") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "time"))) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1d1rwhm17av5fx8cf4hqv451f8cvbdavawmaf10hl5gyg6nvc449")))

(define-public crate-tokio-reactor-trait-1.1.0 (c (n "tokio-reactor-trait") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "time"))) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "11i4p722kjav483fikrj8d726gkqqhkr4d6xsswcpnbbydr1lj79") (r "1.56.0")))

