(define-module (crates-io to ki tokio-dbus-xml) #:use-module (crates-io))

(define-public crate-tokio-dbus-xml-0.0.14 (c (n "tokio-dbus-xml") (v "0.0.14") (d (list (d (n "tokio-dbus-core") (r "^0.0.14") (d #t) (k 0)) (d (n "xmlparser") (r "^0.13.6") (d #t) (k 0)))) (h "0j527ivdn49kfp5fhaiql2whd6kn8ni9scdq3rxzramr60yxaniv") (r "1.66")))

(define-public crate-tokio-dbus-xml-0.0.15 (c (n "tokio-dbus-xml") (v "0.0.15") (d (list (d (n "tokio-dbus-core") (r "^0.0.15") (d #t) (k 0)) (d (n "xmlparser") (r "^0.13.6") (d #t) (k 0)))) (h "12088gbbjziy0p5nmhs103iaplzicxly8j5riwrghr7gvfb932r5") (r "1.66")))

(define-public crate-tokio-dbus-xml-0.0.16 (c (n "tokio-dbus-xml") (v "0.0.16") (d (list (d (n "tokio-dbus-core") (r "=0.0.16") (d #t) (k 0)) (d (n "xmlparser") (r "^0.13.6") (d #t) (k 0)))) (h "13pz6yd5nspqrzavsxhcmf6r6ld90f21n81pwlm29nd9gp082brl") (r "1.66")))

(define-public crate-tokio-dbus-xml-0.0.17 (c (n "tokio-dbus-xml") (v "0.0.17") (d (list (d (n "tokio-dbus-core") (r "=0.0.17") (d #t) (k 0)) (d (n "xmlparser") (r "^0.13.6") (d #t) (k 0)))) (h "136prbdvgwz8x8jpji8z7yh1hn7b6kcyg8743fxhsfn57qnydb7z") (r "1.66")))

