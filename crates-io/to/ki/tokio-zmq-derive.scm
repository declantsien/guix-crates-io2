(define-module (crates-io to ki tokio-zmq-derive) #:use-module (crates-io))

(define-public crate-tokio-zmq-derive-0.1.0 (c (n "tokio-zmq-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)) (d (n "tokio-zmq") (r "^0.1") (d #t) (k 2)))) (h "07xa943hfma7xk64kwh4q9vdn5rglnjc1bsk6vana80f946fij7h")))

(define-public crate-tokio-zmq-derive-0.1.1 (c (n "tokio-zmq-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)) (d (n "tokio-zmq") (r "^0.1") (d #t) (k 2)))) (h "06lf70j2s4z1pd2r9xxl0qkfz3iipz3026gbgqd02ji9bjfsm40l")))

(define-public crate-tokio-zmq-derive-0.1.2 (c (n "tokio-zmq-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)) (d (n "tokio-zmq") (r "^0.1") (d #t) (k 2)))) (h "007iycd6sw5dbbzra6n33xr8isqwpm6zjw5jsx3vwpp8c0zibqjy")))

(define-public crate-tokio-zmq-derive-0.4.0 (c (n "tokio-zmq-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)) (d (n "tokio-zmq") (r "^0.1") (d #t) (k 2)))) (h "1bd3zny46mlwjqbigmj70nlrxhnzdc62cyycgc26hbi77wpr252f")))

(define-public crate-tokio-zmq-derive-0.4.1 (c (n "tokio-zmq-derive") (v "0.4.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)))) (h "0bnrigbby0wf92b3ix2zhwsj9d09psml57gar702ad1kw67krlc5")))

(define-public crate-tokio-zmq-derive-0.4.2 (c (n "tokio-zmq-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "03fapsc5h9pf4819pkkj8sxf2662s1rf37bcw39ma7l8mkalwlkj")))

(define-public crate-tokio-zmq-derive-0.5.0-alpha.0 (c (n "tokio-zmq-derive") (v "0.5.0-alpha.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "1mbw29qh4bsp1qay5mz1i8pv5l0fg6in32cm42a0qg69f5chjjkc")))

(define-public crate-tokio-zmq-derive-0.5.0-alpha.1 (c (n "tokio-zmq-derive") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0rlb9ppx0nf787cbdab3rgnx57wyjfzdd7xm6qvcclajqpracqkn")))

(define-public crate-tokio-zmq-derive-0.3.2 (c (n "tokio-zmq-derive") (v "0.3.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)))) (h "1dbc9x04sjz04877kr9ra6ry02ykpvpfad0lhpbp0jsdnzq74jnb")))

(define-public crate-tokio-zmq-derive-0.5.0 (c (n "tokio-zmq-derive") (v "0.5.0") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0bdzknz75a5fs0nz5jzk2j72x1q1lazxqdhrcsid4sb6ffg37iir")))

(define-public crate-tokio-zmq-derive-0.5.2 (c (n "tokio-zmq-derive") (v "0.5.2") (d (list (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "16yzhfn98hzah9xdrlhga408y68kn5760g30zhknlhk8jqfy5sk7")))

