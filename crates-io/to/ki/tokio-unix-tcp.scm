(define-module (crates-io to ki tokio-unix-tcp) #:use-module (crates-io))

(define-public crate-tokio-unix-tcp-0.1.0 (c (n "tokio-unix-tcp") (v "0.1.0") (d (list (d (n "mio") (r "^0.8.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-uds") (r "^0.2.7") (d #t) (t "cfg(unix)") (k 0)))) (h "16dgr2mnza1mbkfgiv13r7jlh14p4lcxzcxk38w8wp9l9l40k1q6") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tokio-unix-tcp-0.2.0 (c (n "tokio-unix-tcp") (v "0.2.0") (d (list (d (n "mio") (r "^0.8.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio-uds") (r "^0.2.7") (d #t) (t "cfg(unix)") (k 0)))) (h "1qih5ha0m50iamyafs5r2kn82si3v78wfafwijpwzv0jwdh4cs95") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

