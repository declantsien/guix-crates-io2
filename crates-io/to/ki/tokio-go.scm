(define-module (crates-io to ki tokio-go) #:use-module (crates-io))

(define-public crate-tokio-go-0.1.0 (c (n "tokio-go") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "rt-multi-thread" "macros" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1nsgflmdhrxgawvf7darzva7f0zn36yslyvg6n8jh0nmys8mdiqm") (y #t)))

(define-public crate-tokio-go-0.1.1 (c (n "tokio-go") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "rt-multi-thread" "macros" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1nwv424p0hvp0nrqlki02qnlgi97128l3hk1bljkm0shffvyjlqc") (y #t)))

(define-public crate-tokio-go-0.1.2 (c (n "tokio-go") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "rt-multi-thread" "macros" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1ny13splgp6prp9jm0wdj3w0frc6zmw49gq1qjxan437pn7fy0v3") (y #t)))

(define-public crate-tokio-go-0.1.3 (c (n "tokio-go") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "rt-multi-thread" "macros" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1n7njx47gapcy22fjsxidhmaa2vh31vhqsn505qca8qpylkr0my3") (y #t)))

(define-public crate-tokio-go-0.1.4 (c (n "tokio-go") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "rt-multi-thread" "macros" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1vj3r7pbpidw49yj2yfk1wj3qg9i81jgnf530naq7fw8jdxviyn8")))

