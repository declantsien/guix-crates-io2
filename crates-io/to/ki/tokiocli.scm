(define-module (crates-io to ki tokiocli) #:use-module (crates-io))

(define-public crate-tokiocli-0.1.0 (c (n "tokiocli") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ly3j22sblmlcixa0ggp7ww5zlmb0n6nb4q5ynlqll2pfi9ckhda")))

(define-public crate-tokiocli-0.1.1 (c (n "tokiocli") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xvqwhj6rqij7vz8ync33dg1lyli9dl9i22wz3vg0h40vigvvg99")))

(define-public crate-tokiocli-0.1.2 (c (n "tokiocli") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13gqnxxxmbx7bywkdnmgs6lybx4p7zsc21g0xlq64v1pvvqjdzpl")))

(define-public crate-tokiocli-0.1.3 (c (n "tokiocli") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l7gn4qxykvvpcrm0gcz4aklbd793l40hi6v7fgg745axwd3azw5")))

