(define-module (crates-io to ki tokio-rev-lines) #:use-module (crates-io))

(define-public crate-tokio-rev-lines-0.1.0 (c (n "tokio-rev-lines") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "test-util" "macros"))) (d #t) (k 2)))) (h "1g10kj6rnm39nifcbgb1xz9xqpsfv7359b88i649ifvzglcihcmp")))

(define-public crate-tokio-rev-lines-0.1.1 (c (n "tokio-rev-lines") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "test-util" "macros"))) (d #t) (k 2)))) (h "1s1qxmnk71cdj14vf3h0xby2v4knz99l5iry0x0xcf2ajj97a4vr")))

(define-public crate-tokio-rev-lines-0.2.0 (c (n "tokio-rev-lines") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "test-util" "macros" "fs"))) (d #t) (k 2)))) (h "0jnradqc385bixk56azn0l8xbk1yimzabqyxh0jdhhr4incwwlq7")))

(define-public crate-tokio-rev-lines-0.2.1 (c (n "tokio-rev-lines") (v "0.2.1") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "test-util" "macros" "fs"))) (d #t) (k 2)))) (h "0mgzh13nxj3wdzz9n7clc2islpkd21g2fiblq7g10fpv1px45bxz")))

