(define-module (crates-io to ki tokio-aead) #:use-module (crates-io))

(define-public crate-tokio-aead-0.0.0 (c (n "tokio-aead") (v "0.0.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chacha20-poly1305-aead") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.1") (d #t) (k 2)))) (h "0v7skjcmb4z9isy3lrchpk49ndq1yj41qrw50a8hf8nz7id35vln")))

