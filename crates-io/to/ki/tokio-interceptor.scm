(define-module (crates-io to ki tokio-interceptor) #:use-module (crates-io))

(define-public crate-tokio-interceptor-0.1.0 (c (n "tokio-interceptor") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "07ijgk3xms37scn9bzgs3320y6i57k91hhdhf7jyvm19xjb4l4h5")))

