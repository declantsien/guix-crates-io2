(define-module (crates-io to ki tokio-childstream) #:use-module (crates-io))

(define-public crate-tokio-childstream-0.1.0 (c (n "tokio-childstream") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("process" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("io"))) (d #t) (k 0)))) (h "0lz08fp8pls34ri3fr4h5zs9w76hx0mqhhywsf035xpawpd4kjjq")))

(define-public crate-tokio-childstream-0.1.1 (c (n "tokio-childstream") (v "0.1.1") (d (list (d (n "bytelinebuf") (r "^0.1.0") (f (quote ("stream"))) (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("process" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("io"))) (d #t) (k 0)))) (h "04ka0hpr5vx2vmlbf1frrx3qinbvvqsbljs6ag2iafk63dzpmq3x")))

