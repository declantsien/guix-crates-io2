(define-module (crates-io to ki tokio-pipe) #:use-module (crates-io))

(define-public crate-tokio-pipe-0.1.0 (c (n "tokio-pipe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "io-util"))) (d #t) (k 2)))) (h "17yb1msjp1sl4zs84ygnvk6r4rb67mc9pbq1g083kgaswfwlmbia")))

(define-public crate-tokio-pipe-0.1.1 (c (n "tokio-pipe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "io-util"))) (d #t) (k 2)))) (h "1ddjf7gamcz11lnqfksr60sikir79vg6lfp52gafgikk51jzwfp7")))

(define-public crate-tokio-pipe-0.1.2 (c (n "tokio-pipe") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "io-util"))) (d #t) (k 2)))) (h "0awgh60201wsd5p8ma9f8jnk4m2l4qvajbfsgb51s02cvbi8w0fa")))

(define-public crate-tokio-pipe-0.1.3 (c (n "tokio-pipe") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "io-util"))) (d #t) (k 2)))) (h "0fyf19jhvskziraasdx2pgz1kgk49g05ac92n5rv8fgdc738w9qx")))

(define-public crate-tokio-pipe-0.1.4 (c (n "tokio-pipe") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "io-util"))) (d #t) (k 2)))) (h "0k118205hnhwwknmhgac07dnw0wrhmdbj3cl5s3yz2warx2pq8zy")))

(define-public crate-tokio-pipe-0.2.0 (c (n "tokio-pipe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros" "rt-multi-thread" "io-util"))) (d #t) (k 2)))) (h "120l7z7572s9b386h38fr01gpqjbwwn4lkk4fq4ca27vga56wsbq")))

(define-public crate-tokio-pipe-0.2.1 (c (n "tokio-pipe") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("macros" "rt-multi-thread" "io-util"))) (d #t) (k 2)))) (h "0xv9996yb8y482vkb8n0lqzczii46wnf4rxd0w4bn3wmw36d1zym")))

(define-public crate-tokio-pipe-0.2.3 (c (n "tokio-pipe") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread" "io-util" "process"))) (d #t) (k 2)))) (h "02nwd5sdn17m93dr79db9f21yn6srvdjlzl49w29nip8hlz0lip7") (r "1.45")))

(define-public crate-tokio-pipe-0.2.4 (c (n "tokio-pipe") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "1d5dq7lzhs5abm7zx6jqnxk49d7h688xpdcq5cbdc36n2017sdwn") (r "1.45")))

(define-public crate-tokio-pipe-0.2.5 (c (n "tokio-pipe") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "0ggnm03996i2b5p664gy1ph9h8pqndbjfik29ha2a51z8qx1y2k9") (y #t) (r "1.45")))

(define-public crate-tokio-pipe-0.2.6 (c (n "tokio-pipe") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "1gb31fyd5fv46lqkim8sf1iah8hz85ak50b977jayybqci59zvxs") (r "1.45")))

(define-public crate-tokio-pipe-0.2.7 (c (n "tokio-pipe") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "1zba7ari18a0m7gpx0zla27x63zwliqz7k4d3gr6ag2h1q6f5skl") (r "1.45")))

(define-public crate-tokio-pipe-0.2.8 (c (n "tokio-pipe") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "0nziwgpp47k01yms5vzj0vg5is4w0ark8va9m07agckcr1vvplvi") (r "1.46")))

(define-public crate-tokio-pipe-0.2.9 (c (n "tokio-pipe") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "1jqm3xz3s1wp0aby9hgb8w3fpsladdp0s2pmmhknhd13za7sgl4r") (r "1.46")))

(define-public crate-tokio-pipe-0.2.10 (c (n "tokio-pipe") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "0sxafa1alyqcl8v1nm84lrnaybybgss70ymi07k8ynqn823dbrrc") (r "1.46")))

(define-public crate-tokio-pipe-0.2.11 (c (n "tokio-pipe") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "03xkn7zm2z910gp7fx16ik13w6gyf5fqhi6zsm0g1gb41djsbyxw") (r "1.49")))

(define-public crate-tokio-pipe-0.2.12 (c (n "tokio-pipe") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread" "io-util" "process" "time"))) (d #t) (k 2)))) (h "1117ahamrgc23qc6g22i1cflfpg3pfs498581gxbhqdxzx5sh4zj") (r "1.49")))

