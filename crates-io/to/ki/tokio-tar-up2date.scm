(define-module (crates-io to ki tokio-tar-up2date) #:use-module (crates-io))

(define-public crate-tokio-tar-up2date-0.3.1 (c (n "tokio-tar-up2date") (v "0.3.1") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1mf2bnl5gn0qgz4cfai8xw7cxqbj30cr3cbc9vs4ily3kq8xlsd3") (f (quote (("default" "xattr"))))))

