(define-module (crates-io to ki tokio-extra-macros) #:use-module (crates-io))

(define-public crate-tokio-extra-macros-0.1.0 (c (n "tokio-extra-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1gip5s7anna3mqfk19kqq55r6xc4qki12d0qbbfay0nxy6kf55ny")))

(define-public crate-tokio-extra-macros-0.2.0 (c (n "tokio-extra-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1hg2z9d3n93llqx48ri2z7vlxsxwbxp7rj9q69hlzlnxwg0agw86")))

(define-public crate-tokio-extra-macros-0.2.1 (c (n "tokio-extra-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "19b950srqz5wf8rs8vqijm74jzpnjismqz13p65gx4m1fjmddc73")))

