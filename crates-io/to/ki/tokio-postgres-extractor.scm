(define-module (crates-io to ki tokio-postgres-extractor) #:use-module (crates-io))

(define-public crate-tokio-postgres-extractor-0.7.0 (c (n "tokio-postgres-extractor") (v "0.7.0") (d (list (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-postgres") (r "^0.7.8") (d #t) (k 0)) (d (n "tokio-postgres-extractor-macros") (r "=0.7.0") (d #t) (k 0)))) (h "17dpmza4dx24fy5g7vhf7icwnlimrxs0l8bxwd9jrhj9q628rdgs")))

