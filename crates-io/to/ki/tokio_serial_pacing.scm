(define-module (crates-io to ki tokio_serial_pacing) #:use-module (crates-io))

(define-public crate-tokio_serial_pacing-0.1.0 (c (n "tokio_serial_pacing") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("time" "macros" "rt" "io-util"))) (d #t) (k 2)) (d (n "tokio-serial") (r "^5.4.4") (d #t) (k 0)))) (h "1qvarmizhf164sc9cdlp2j8rrn5m38xqwd8vm9s8j820higk7w2n")))

(define-public crate-tokio_serial_pacing-0.1.1 (c (n "tokio_serial_pacing") (v "0.1.1") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("time" "macros" "rt" "io-util"))) (d #t) (k 2)) (d (n "tokio-serial") (r "^5.4.4") (d #t) (k 0)))) (h "197w42zl1g2316g7cypxxsklyvnqsnjm8a847ysabj41zyy8x90l")))

