(define-module (crates-io to ki tokio_based) #:use-module (crates-io))

(define-public crate-tokio_based-1.0.0 (c (n "tokio_based") (v "1.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)))) (h "1l1fzjab5w0fjji9nq9zma9z9rg36d0lfz8v6qxnyjg1gp6bw6n7")))

(define-public crate-tokio_based-2.0.0 (c (n "tokio_based") (v "2.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net"))) (d #t) (k 2)))) (h "051jgfd7r3wmaz8m9fri38fndm0jfpih80x4bd1ydyd9jqj37c6w")))

