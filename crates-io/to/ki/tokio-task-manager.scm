(define-module (crates-io to ki tokio-task-manager) #:use-module (crates-io))

(define-public crate-tokio-task-manager-0.1.0 (c (n "tokio-task-manager") (v "0.1.0") (d (list (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "sync" "time" "macros"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19ncd2w0rb9hag1k1iid93hy0fp3w7yxyyfzggkpizvij7x81l39")))

(define-public crate-tokio-task-manager-0.2.0 (c (n "tokio-task-manager") (v "0.2.0") (d (list (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "signal" "sync" "time" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("io-util" "rt-multi-thread" "signal" "sync" "time" "macros" "net"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "187vw7fsdkyajlrq6dyaclsw89ii9f1ips15bmwjpr7xmyqw1qvg")))

