(define-module (crates-io to ki tokio-file-unix) #:use-module (crates-io))

(define-public crate-tokio-file-unix-0.1.0 (c (n "tokio-file-unix") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "mio") (r "^0.6.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "14ak478c6phzy6wwns1cbwxy1k4w9l8cm9zi05qaad70wky6mhpj")))

(define-public crate-tokio-file-unix-0.2.0 (c (n "tokio-file-unix") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "mio") (r "^0.6.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "1hqgb3w7fmk9iiacjc1m2ih78y5bxqjamdmcl72nkrj1z0azpzwy")))

(define-public crate-tokio-file-unix-0.3.0 (c (n "tokio-file-unix") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (d #t) (k 0)))) (h "0f9dk83rhsjvj3kwklin0iwfr08x4w7jb0kvxk7m03lkiqjxnqap")))

(define-public crate-tokio-file-unix-0.4.0 (c (n "tokio-file-unix") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (d #t) (k 0)))) (h "1ixqgh5m6pq8hxqj0nvpadxgnnzb4vn4vwxrbhjxaf2dl2pl87xw")))

(define-public crate-tokio-file-unix-0.4.1 (c (n "tokio-file-unix") (v "0.4.1") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (d #t) (k 0)))) (h "0fjbrh7zpkx46d4w4ip3fbwy16blpcsb3061xx8vvxq4y79xqdky")))

(define-public crate-tokio-file-unix-0.4.2 (c (n "tokio-file-unix") (v "0.4.2") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (d #t) (k 0)))) (h "0f7hibsp36xr5wzs2ybzykkdvjagmzmwqm2fd4c03ij7hz42zrry")))

(define-public crate-tokio-file-unix-0.5.0 (c (n "tokio-file-unix") (v "0.5.0") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.5") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.1") (d #t) (k 0)))) (h "0xida3p5hqfkn5hf6h5gx93bjydlpr19vqdc3r65ps29zawa5ry7")))

(define-public crate-tokio-file-unix-0.5.1 (c (n "tokio-file-unix") (v "0.5.1") (d (list (d (n "bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.5") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.1") (d #t) (k 0)))) (h "131fxaygphd1mg82mlbla9gi6bgfcqg69qj6gmh7551p46jf4hkp")))

(define-public crate-tokio-file-unix-0.6.0 (c (n "tokio-file-unix") (v "0.6.0") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "actix-web") (r "^3.3.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("io-util" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3.0") (f (quote ("codec"))) (d #t) (k 2)))) (h "0nribgjc99hdc08qgbrznwmqssnjm54j9hf9xsmhz0qj19dkp8fc")))

