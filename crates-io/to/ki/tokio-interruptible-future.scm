(define-module (crates-io to ki tokio-interruptible-future) #:use-module (crates-io))

(define-public crate-tokio-interruptible-future-0.9.0 (c (n "tokio-interruptible-future") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0cwf4wn8qmls1s1xlprd2jska6lizfll45m4lfrjfb6lfr01xl7m")))

(define-public crate-tokio-interruptible-future-0.9.1 (c (n "tokio-interruptible-future") (v "0.9.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0mv2mmzbgc5imyyblzkh6zy5qk2r13rll8fzyx2xrdaqa8w2ag2m")))

(define-public crate-tokio-interruptible-future-0.9.2 (c (n "tokio-interruptible-future") (v "0.9.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0bk8haz7z976pq3vp605jsq1xw56vknnl2xlkl86szhqnf1f1zps")))

(define-public crate-tokio-interruptible-future-0.9.3 (c (n "tokio-interruptible-future") (v "0.9.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1mvalga2dmlbmsn4x0m1b4is4g1cnyiwmm6d4qw7dwfnmk3a8w5c")))

(define-public crate-tokio-interruptible-future-0.9.4 (c (n "tokio-interruptible-future") (v "0.9.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0rqnakyqp5j43hqzyfh76mjmyhx1f9yx18zqxrcwrfp9bcd97his")))

(define-public crate-tokio-interruptible-future-0.9.5 (c (n "tokio-interruptible-future") (v "0.9.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1lkbrr30fm3cax6k1wnnwync90l88i98xdbzranajqixf975yvm6")))

(define-public crate-tokio-interruptible-future-0.9.6 (c (n "tokio-interruptible-future") (v "0.9.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1gfdwrb7qh8zl1d5k2643g4ml13dmbrnywy00vxq4vx31pjnly7c")))

(define-public crate-tokio-interruptible-future-0.10.0 (c (n "tokio-interruptible-future") (v "0.10.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0dx2hzjqfzy381x79r2gx511xyx0fzvr5j3cq16gspbpm3n915w0")))

(define-public crate-tokio-interruptible-future-0.10.1 (c (n "tokio-interruptible-future") (v "0.10.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1iprps5gkm72lasbpm7yggc1shy5b16n7l59yiskf82m3b57kbap")))

(define-public crate-tokio-interruptible-future-0.10.4 (c (n "tokio-interruptible-future") (v "0.10.4") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1wlj584j6y4qwi9p2a1w34yf86gj8amv7884rsf28liansfrbah7")))

(define-public crate-tokio-interruptible-future-0.11.0 (c (n "tokio-interruptible-future") (v "0.11.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1grvh8qwvi9pblk89hv3yxz0410ld8xnsrdvf9phri4xkq0hcw65")))

(define-public crate-tokio-interruptible-future-0.11.1 (c (n "tokio-interruptible-future") (v "0.11.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0lxrv71whvgvik7af06ixg8mdssp4w5fw8c8b02c3xz3m8agqlrs")))

(define-public crate-tokio-interruptible-future-0.11.2 (c (n "tokio-interruptible-future") (v "0.11.2") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "14f2ydfcdsy567wviw81nn30pzp1ic5j7ap2nzi5c5dz5w2z9bvz")))

(define-public crate-tokio-interruptible-future-1.0.0 (c (n "tokio-interruptible-future") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1jd57q7fr0g98azxrlz0jgra1l3cqwfrcm6bdcf4fm0c9f0f4scg")))

(define-public crate-tokio-interruptible-future-2.0.0 (c (n "tokio-interruptible-future") (v "2.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0igv56z1ljdhrs0ra6h9s8qpsqzcw09nv1481mg7708ii3dqf8yx")))

(define-public crate-tokio-interruptible-future-3.0.0 (c (n "tokio-interruptible-future") (v "3.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0c64r595fngbxzzaddkd344biqy8jm18h4wawwvrh80y1x8pbk37")))

(define-public crate-tokio-interruptible-future-3.0.1-test.0 (c (n "tokio-interruptible-future") (v "3.0.1-test.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0k2lbcvkyygl3sm37348jlvnshdqgzc0l4ajrwqnn49n5wpvcysg")))

(define-public crate-tokio-interruptible-future-3.0.2 (c (n "tokio-interruptible-future") (v "3.0.2") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "02wp2iip3g2vy92knh1xj8pk9ppdimk92dzc4bjqrxad6qxgij2z")))

(define-public crate-tokio-interruptible-future-3.0.4 (c (n "tokio-interruptible-future") (v "3.0.4") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1sg7wd9wpjvq1jzfyn8g4sg28x3jpqi6j6hpa6f5xasmhq9cwbi6")))

(define-public crate-tokio-interruptible-future-4.0.0 (c (n "tokio-interruptible-future") (v "4.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1ll64k7z4g0ii32612nm5332bqzh7xgxxxx6qrmz83f27zhbn6q9")))

(define-public crate-tokio-interruptible-future-4.0.1 (c (n "tokio-interruptible-future") (v "4.0.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0r6ai41baimf4s1s75qf0b2vpvp3zpgzrn4k6ylblymy8q5axvyj")))

(define-public crate-tokio-interruptible-future-5.0.0 (c (n "tokio-interruptible-future") (v "5.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "17fjrnzi4g3gka4mynrzb8nlqif9mrcc1i6i5bphiv9v12rbymmh")))

(define-public crate-tokio-interruptible-future-6.0.0 (c (n "tokio-interruptible-future") (v "6.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "14hnslf7hrpracrh16w6ymf8zqcrxvxya367nxivw64jm3i3naai")))

(define-public crate-tokio-interruptible-future-7.0.0 (c (n "tokio-interruptible-future") (v "7.0.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1g9cs61xm6h21kda0ka4x6asi0hyjml7h01v64bx70qfacadsazf")))

(define-public crate-tokio-interruptible-future-8.0.0 (c (n "tokio-interruptible-future") (v "8.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0f8z1wjsdyb3agshz809dk1pv6w3vmqg2w5vy0l9vy1i5ql854nq")))

(define-public crate-tokio-interruptible-future-8.1.0 (c (n "tokio-interruptible-future") (v "8.1.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "0ccwawf2jy3svrgbdgfnyxnixm14j8fg40j784kfxw9cb6z05lpw")))

(define-public crate-tokio-interruptible-future-8.2.0 (c (n "tokio-interruptible-future") (v "8.2.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("sync" "macros"))) (d #t) (k 0)))) (h "1s96hyyk3r7rcd6ybrl5x405rghlgb2nbr5c92mhnygl30g3hj6k")))

