(define-module (crates-io to ki tokio_safe_block_on) #:use-module (crates-io))

(define-public crate-tokio_safe_block_on-0.1.0 (c (n "tokio_safe_block_on") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("blocking" "rt-core" "rt-util" "rt-threaded"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1vzyfwy34420ynz8phzgy0nb9l5y7q0wik9vcxnk2bm7i9sw1yyc")))

(define-public crate-tokio_safe_block_on-0.1.1 (c (n "tokio_safe_block_on") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "blocking" "rt-core" "rt-util" "rt-threaded"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1vysgvsjlp841gh3ssymh3i7qr7x824c99s4axwj1anyn55drr8f")))

(define-public crate-tokio_safe_block_on-0.1.2 (c (n "tokio_safe_block_on") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "blocking" "rt-core" "rt-util" "rt-threaded"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0z90lcxvl429yknx81jjdbb1xv1kpyjy35mbscp4705gzg3bfvww")))

(define-public crate-tokio_safe_block_on-0.2.0 (c (n "tokio_safe_block_on") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "blocking" "rt-core" "rt-util" "rt-threaded"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "045ngasc6k496a6q8ahvlybcq49sxczzdwfvsph53abr8k020rx0")))

