(define-module (crates-io to ki tokio-timer-futures2) #:use-module (crates-io))

(define-public crate-tokio-timer-futures2-0.2.0 (c (n "tokio-timer-futures2") (v "0.2.0") (d (list (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)))) (h "0cjk5y70kcxx8sa78887kyhcrvwd24pm56i30yiz10skcqwwpz1w")))

(define-public crate-tokio-timer-futures2-0.2.1 (c (n "tokio-timer-futures2") (v "0.2.1") (d (list (d (n "futures-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)))) (h "1qsi1dwv8rwwy0bny5a11c3inyy59gr9jn7446kax61ypapwjpq2")))

