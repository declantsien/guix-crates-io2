(define-module (crates-io to ki tokio-fd) #:use-module (crates-io))

(define-public crate-tokio-fd-0.1.0 (c (n "tokio-fd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "mio") (r "^0.6.21") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("io-driver"))) (d #t) (k 0)))) (h "0h5cnih62k5grj1ffqmqhww1abcl2ph0d6rcan5bk6z75a6652d6")))

(define-public crate-tokio-fd-0.2.0 (c (n "tokio-fd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "tokio") (r "^0.3.4") (f (quote ("net"))) (d #t) (k 0)))) (h "0nw45lasmzil67ihlmhkdm447fiixl6h3cmdqn993hkydx2ig6nm") (y #t)))

(define-public crate-tokio-fd-0.2.1 (c (n "tokio-fd") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "tokio") (r "^0.3.4") (f (quote ("net"))) (d #t) (k 0)))) (h "0qd83i5l56ch1v4pmpg0nmj2fakqgwkn9jy6xx2smdpnzxn03gy1")))

(define-public crate-tokio-fd-0.3.0 (c (n "tokio-fd") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (d #t) (k 0)))) (h "07b4fy9y0gyjx589qcylfiszwp0c0rn13xlbz6pln2k1jywg1vaw")))

