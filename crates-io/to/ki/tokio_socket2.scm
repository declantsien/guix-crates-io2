(define-module (crates-io to ki tokio_socket2) #:use-module (crates-io))

(define-public crate-tokio_socket2-0.1.0 (c (n "tokio_socket2") (v "0.1.0") (d (list (d (n "socket2") (r "^0.4.7") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2n4z2jq7asdq76ldn4y1fnw7cy0a0fcv94byrs8ms1v35h4kn8")))

(define-public crate-tokio_socket2-0.1.1 (c (n "tokio_socket2") (v "0.1.1") (d (list (d (n "socket2") (r "^0.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("io-util" "net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1czlnm3f0nrli6a3czsdzf3h9cahivfk4v5vsfczl1avkjx7wi0i")))

