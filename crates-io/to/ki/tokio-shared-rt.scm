(define-module (crates-io to ki tokio-shared-rt) #:use-module (crates-io))

(define-public crate-tokio-shared-rt-0.1.0 (c (n "tokio-shared-rt") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "net" "parking_lot" "rt" "macros" "time" "io-util"))) (d #t) (k 2)) (d (n "tokio-shared-rt-macro") (r "^0.1") (d #t) (k 0)))) (h "11bxqkf178jnl5sp2irng46qmff5mc0r7lckrqbbp842qqzb0sqs")))

