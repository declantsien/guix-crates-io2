(define-module (crates-io to ki tokio-simple-scheduler) #:use-module (crates-io))

(define-public crate-tokio-simple-scheduler-0.1.0 (c (n "tokio-simple-scheduler") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("time" "macros" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt" "time" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0rw5r9wxcsgcx79n1hhfbv0fs0y9n4v98c2zyrqqplrc4qm780wf")))

(define-public crate-tokio-simple-scheduler-0.1.1 (c (n "tokio-simple-scheduler") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("time" "macros" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt" "time" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1pgw1fckykjqgxzmfr75gqigjwk0c7pamarx9j7i9hnk2sqljdsx")))

(define-public crate-tokio-simple-scheduler-0.1.2 (c (n "tokio-simple-scheduler") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("time" "macros" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "time" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0m50ljdd3sp9pxk375rvj0mk6aj0z78dif44kg843s83dwf7y1sf")))

