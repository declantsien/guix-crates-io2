(define-module (crates-io to ki tokio_smux) #:use-module (crates-io))

(define-public crate-tokio_smux-0.1.0 (c (n "tokio_smux") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("net" "rt" "macros" "io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "06n1x2ibjk05vyfjdp0ps4bkzkjms4jq9jysq4vmq8ss07zlzpw7")))

(define-public crate-tokio_smux-0.1.1 (c (n "tokio_smux") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("net" "rt" "macros" "io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1rgwnfnqjh2lv9chd0f9q2chbf2qdiqfqcs7i5adzg5rndickzn0")))

(define-public crate-tokio_smux-0.2.0 (c (n "tokio_smux") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("net" "rt" "macros" "io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "05vicqxwlng3nm0mjgb5vy96k63mkai1i7nsrx00744ldds2cn0a")))

