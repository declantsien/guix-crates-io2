(define-module (crates-io to ki tokio_spf_validator) #:use-module (crates-io))

(define-public crate-tokio_spf_validator-1.0.0 (c (n "tokio_spf_validator") (v "1.0.0") (d (list (d (n "cidr") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trust-dns-resolver") (r "^0.20.3") (d #t) (k 0)))) (h "054g6s7j0is6wj1g5q92jh0p835axhsj1rzfawgci2wj4plipsam")))

(define-public crate-tokio_spf_validator-1.0.1 (c (n "tokio_spf_validator") (v "1.0.1") (d (list (d (n "cidr") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trust-dns-resolver") (r "^0.20.3") (d #t) (k 0)))) (h "07fxkm0jqikfcx82p1qdmc0vdc3sifm7i2g8xbpfsifg9m48vksa")))

(define-public crate-tokio_spf_validator-1.0.2 (c (n "tokio_spf_validator") (v "1.0.2") (d (list (d (n "cidr") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trust-dns-resolver") (r "^0.20.3") (d #t) (k 0)))) (h "10bydarhdda5acmp7f3njc49p60r85i97bzixv12wk64kvf683zq")))

(define-public crate-tokio_spf_validator-1.0.3 (c (n "tokio_spf_validator") (v "1.0.3") (d (list (d (n "cidr") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trust-dns-resolver") (r "^0.20.3") (d #t) (k 0)))) (h "141kklmzya819bd37dmdqhmzwd6wd14n29fn8chy2v0fspzmfzz5")))

