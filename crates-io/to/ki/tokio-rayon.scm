(define-module (crates-io to ki tokio-rayon) #:use-module (crates-io))

(define-public crate-tokio-rayon-0.1.0 (c (n "tokio-rayon") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync" "macros" "rt" "time" "rt-multi-thread"))) (k 2)) (d (n "tokio-test") (r "^0.4.1") (d #t) (k 2)))) (h "0paxq280xmn7dq0d8xwzb1ldisw8wj5m986qc9zaj28b7ng52b5d")))

(define-public crate-tokio-rayon-1.0.0 (c (n "tokio-rayon") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync" "macros" "rt" "time" "rt-multi-thread"))) (k 2)) (d (n "tokio-test") (r "^0.4.1") (d #t) (k 2)))) (h "0d28r19wqz5b2w35myhabxmaisbnim2hniks5cvwi7a83ca5npvd")))

(define-public crate-tokio-rayon-2.0.0 (c (n "tokio-rayon") (v "2.0.0") (d (list (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync" "macros" "rt" "time" "rt-multi-thread"))) (k 2)) (d (n "tokio-test") (r "^0.4.1") (d #t) (k 2)))) (h "140cfm502m7c5qchld8vxsbfd3fwxg69673d88nfnq95s87chhbv")))

(define-public crate-tokio-rayon-2.1.0 (c (n "tokio-rayon") (v "2.1.0") (d (list (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("sync" "macros" "rt" "time" "rt-multi-thread"))) (k 2)) (d (n "tokio-test") (r "^0.4.1") (d #t) (k 2)))) (h "14vn8mgi0wbh7sy8f3jwyamqg66mgc9l8cpqg2vh7pdiw1v3mwvw")))

