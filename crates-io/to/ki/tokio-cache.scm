(define-module (crates-io to ki tokio-cache) #:use-module (crates-io))

(define-public crate-tokio-cache-0.2.0-alpha.0 (c (n "tokio-cache") (v "0.2.0-alpha.0") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "im") (r "^13.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.1") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "04ambs05mz2b8xl638lfm1y41yga8fz68xwzr9hs7mq3h2fgpiyk")))

(define-public crate-tokio-cache-0.2.0-alpha.1 (c (n "tokio-cache") (v "0.2.0-alpha.1") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "im") (r "^13.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.5") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.2.0-alpha.5") (d #t) (k 0)))) (h "1057nj46if7i9x1ldvnpjmz9cdbfa08i5sjj9xfk2g8i2r3bzrrh")))

