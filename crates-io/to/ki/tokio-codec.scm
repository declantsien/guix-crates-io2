(define-module (crates-io to ki tokio-codec) #:use-module (crates-io))

(define-public crate-tokio-codec-0.0.0 (c (n "tokio-codec") (v "0.0.0") (h "0wq3qsz1p3sjw2zf2ignfb3qri3qcfswa4ja183cbhqvaf7zqbvf")))

(define-public crate-tokio-codec-0.1.0 (c (n "tokio-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.7") (d #t) (k 0)))) (h "1jwcvmhmj3j2k840kydyymgg5fgz54nfv7brrdgyjb0wp12rc7l8")))

(define-public crate-tokio-codec-0.1.1 (c (n "tokio-codec") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.7") (d #t) (k 0)))) (h "17y3hi3dd0bdfkrzshx9qhwcf49xv9iynszj7iwy3w4nmz71wl2w")))

(define-public crate-tokio-codec-0.2.0-alpha.1 (c (n "tokio-codec") (v "0.2.0-alpha.1") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.17") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.17") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.1") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.1") (d #t) (k 0)) (d (n "tokio-test") (r "= 0.2.0-alpha.1") (d #t) (k 2)))) (h "0plk2ksi9lkr435940r018ndnw72cxckb0mnfp24cavclv5mll30")))

(define-public crate-tokio-codec-0.2.0-alpha.2 (c (n "tokio-codec") (v "0.2.0-alpha.2") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "= 0.2.0-alpha.2") (d #t) (k 0)))) (h "1vkxw5a53vn5ydaxkir0nnfp2x4fikgfrf33smkkm51ldg6q7bpn")))

(define-public crate-tokio-codec-0.2.0-alpha.3 (c (n "tokio-codec") (v "0.2.0-alpha.3") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "= 0.2.0-alpha.3") (d #t) (k 0)))) (h "0c73cd7a2y0a868d3whmqfxfjxpz0pqc94csnikkmrm6pslnbmym")))

(define-public crate-tokio-codec-0.2.0-alpha.4 (c (n "tokio-codec") (v "0.2.0-alpha.4") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "= 0.2.0-alpha.4") (d #t) (k 0)))) (h "15qvlfcfh8rgbbppgff446zcxkdmar5a9jqwwyr48ll5r9rdkk5l")))

(define-public crate-tokio-codec-0.2.0-alpha.5 (c (n "tokio-codec") (v "0.2.0-alpha.5") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "= 0.2.0-alpha.5") (d #t) (k 0)))) (h "0kccp2ga0bcqy6g3n1hvhz6gkn1506kvihqiahb19823wd0qihky")))

(define-public crate-tokio-codec-0.2.0-alpha.6 (c (n "tokio-codec") (v "0.2.0-alpha.6") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-sink-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "0ykqx22rmw0k49y5302wshsaxjnpnwf4j4w8s92l1gc43vyj4pcz")))

(define-public crate-tokio-codec-0.1.2 (c (n "tokio-codec") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.7") (d #t) (k 0)))) (h "0swpfngcb331lzggk6j68yks6w0bnw35vpl4hv8p03msc239kci5")))

