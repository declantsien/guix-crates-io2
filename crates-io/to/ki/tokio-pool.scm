(define-module (crates-io to ki tokio-pool) #:use-module (crates-io))

(define-public crate-tokio-pool-0.1.0 (c (n "tokio-pool") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.1") (d #t) (k 0)))) (h "08zm1iyiccbcl27y5zaw9aw0pb7zk592y2gkhf2xp5rxx53anvpm") (y #t)))

