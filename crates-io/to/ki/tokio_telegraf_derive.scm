(define-module (crates-io to ki tokio_telegraf_derive) #:use-module (crates-io))

(define-public crate-tokio_telegraf_derive-0.1.0 (c (n "tokio_telegraf_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04wpcjzr8vqdamfnv7hc5qnmsnwxnz5lhd64by55vzbq280sgr14")))

