(define-module (crates-io to ki tokio-js-set-interval) #:use-module (crates-io))

(define-public crate-tokio-js-set-interval-1.0.0 (c (n "tokio-js-set-interval") (v "1.0.0") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1r3yfjb6xl8gyl99673zbyar2sws0x5366pyjvmgdkl0daszhxy6")))

(define-public crate-tokio-js-set-interval-1.0.1 (c (n "tokio-js-set-interval") (v "1.0.1") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1gprqqrkvfrccmkbrj2diajiddzj86yc6c61y45i6bb2b0di1y46")))

(define-public crate-tokio-js-set-interval-1.0.2 (c (n "tokio-js-set-interval") (v "1.0.2") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0d0qrl6zqmb8lm8hpwxcqqv2mbvzcn8n97x6f99hv5f9lyfd7zih")))

(define-public crate-tokio-js-set-interval-1.0.3 (c (n "tokio-js-set-interval") (v "1.0.3") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17sq8qrc3m2yj8zyml712ynpqx0zn3jn4nccxr9ql416m8m5ayj5")))

(define-public crate-tokio-js-set-interval-1.1.0 (c (n "tokio-js-set-interval") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0gy7b0apqb9pf5rlv1j4p79ihpxaqhfc21hcxx0f9knz8xzfq0fz")))

(define-public crate-tokio-js-set-interval-1.2.0 (c (n "tokio-js-set-interval") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1yq5bmzi4pb4qc77xr38kiwiq2mwbvd1zcb6bc1q6hxidq2p4glb")))

(define-public crate-tokio-js-set-interval-1.2.1 (c (n "tokio-js-set-interval") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09i5pf27pm2fn80rzq920m3m7hckjpxhag795w8w3smqpclj97sp")))

(define-public crate-tokio-js-set-interval-1.3.0 (c (n "tokio-js-set-interval") (v "1.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11phv63hyrm4d8y5y0g9p6g6ahp5n9f3397j3d5l5nd7y6aw99g1") (r "1.66")))

