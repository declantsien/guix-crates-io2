(define-module (crates-io to ki tokio-borrow-stdio) #:use-module (crates-io))

(define-public crate-tokio-borrow-stdio-0.1.0 (c (n "tokio-borrow-stdio") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "17z2zzhyifggjllh9cqipwrm4a4czbcvwlb73vn3646yp0hdjb9k")))

