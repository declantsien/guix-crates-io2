(define-module (crates-io to ki tokio-i2c) #:use-module (crates-io))

(define-public crate-tokio-i2c-0.1.0 (c (n "tokio-i2c") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "i2c-linux-sys") (r "^0.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1") (d #t) (k 0)))) (h "1r1vaq8g9vba50mq9qrgspp8zhscydq3913cl1wxy8n7pw5i6ma3")))

