(define-module (crates-io to ki tokio-newrelic) #:use-module (crates-io))

(define-public crate-tokio-newrelic-0.1.0 (c (n "tokio-newrelic") (v "0.1.0") (d (list (d (n "antidote") (r "^1.0.0") (d #t) (k 0)) (d (n "diesel") (r "^1.4.5") (f (quote ("r2d2" "chrono" "postgres"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "newrelic") (r "^0.2") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "r2d2-diesel") (r "^1.0.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("rt-core" "rt-util" "time"))) (d #t) (k 0)))) (h "0xhpp2ybp9l2rydbpvmahx2gy1kfkfz9w78v4h5a7wqvjw9vma7n")))

