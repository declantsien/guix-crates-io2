(define-module (crates-io to ki tokio-ssh2) #:use-module (crates-io))

(define-public crate-tokio-ssh2-0.0.1 (c (n "tokio-ssh2") (v "0.0.1") (d (list (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0s9wwbw77jv1lz41nr65afkrxp3pa5qkr9i75ibf50wry91j5myl")))

