(define-module (crates-io to ki tokio-adjustable-timeout) #:use-module (crates-io))

(define-public crate-tokio-adjustable-timeout-0.1.0 (c (n "tokio-adjustable-timeout") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0n12hiyna6r43k6vi8c1i0968mk6mrcm0p97y9ar3w4a45yvpff8")))

(define-public crate-tokio-adjustable-timeout-0.1.1 (c (n "tokio-adjustable-timeout") (v "0.1.1") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "164jrzzmgnhbcb95djkxzh8q25yvczqlq4x502jayqisnszj5a25")))

