(define-module (crates-io to ki tokio-boring-imp) #:use-module (crates-io))

(define-public crate-tokio-boring-imp-2.1.4 (c (n "tokio-boring-imp") (v "2.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "boring") (r ">=1.1.0, <3.0.0") (d #t) (k 0) (p "boring-imp")) (d (n "boring-sys") (r ">=1.1.0, <3.0.0") (d #t) (k 0) (p "boring-sys-imp")) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0xzz1yc5vadiq8jpym0rikw38rhv99qsnkbijvp0blff0y6n4l52") (f (quote (("fips" "boring/fips")))) (y #t)))

(define-public crate-tokio-boring-imp-2.1.5 (c (n "tokio-boring-imp") (v "2.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "boring-imp") (r ">=1.1.0, <3.0.0") (d #t) (k 0)) (d (n "boring-sys-imp") (r ">=1.1.0, <3.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0dsl78lm40kbkw4fs1fznl0iyvwmdi79nv6wrn90xn6db9mc6919") (f (quote (("fips" "boring-imp/fips"))))))

