(define-module (crates-io to ki tokio-futures-byteorder) #:use-module (crates-io))

(define-public crate-tokio-futures-byteorder-0.2.0 (c (n "tokio-futures-byteorder") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3.4") (d #t) (k 2)) (d (n "futures-io") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full" "macros"))) (d #t) (k 2)))) (h "1cw32vw8qbx18swknyx8rxlc0y8yqvhwdzqsy4js97gz97rdlmhp") (f (quote (("futures" "futures-io") ("default" "tokio"))))))

