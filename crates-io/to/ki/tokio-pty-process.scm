(define-module (crates-io to ki tokio-pty-process) #:use-module (crates-io))

(define-public crate-tokio-pty-process-0.1.0 (c (n "tokio-pty-process") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.1") (d #t) (k 0)))) (h "1zik1kcnmzm8y2xi19yx99ba7cq86ssqhgip6xdi9b35kd92aqlr")))

(define-public crate-tokio-pty-process-0.2.0 (c (n "tokio-pty-process") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2.1") (d #t) (k 0)))) (h "1v6apvkp0nsia3l31impy35h6znfn9anfq5g0wmm61riam7500b3")))

(define-public crate-tokio-pty-process-0.3.0 (c (n "tokio-pty-process") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2.1") (d #t) (k 0)))) (h "0fncx9jnw3c6mr0fmam09h96rdrx14iszpglhhkvlh74xy78j46r")))

(define-public crate-tokio-pty-process-0.3.1 (c (n "tokio-pty-process") (v "0.3.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2.1") (d #t) (k 0)))) (h "0cad3d3gyp1yd3hzk5b7hl2bm0vhbpkp59jwhsgvpvl70w031xk1")))

(define-public crate-tokio-pty-process-0.3.2 (c (n "tokio-pty-process") (v "0.3.2") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2.1") (d #t) (k 0)))) (h "1kkj59svdmdm0vj48rlaqynbrw2ramy6px5j507csh72f0k8z3r7")))

(define-public crate-tokio-pty-process-0.4.0 (c (n "tokio-pty-process") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2.1") (d #t) (k 0)))) (h "0xaig4k60s2msx6lk29gm4l3vd2cxkwdi5g16isjghp8c8xikqxr")))

