(define-module (crates-io to ki tokio-snappy) #:use-module (crates-io))

(define-public crate-tokio-snappy-0.1.0 (c (n "tokio-snappy") (v "0.1.0") (d (list (d (n "bytes") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "snap") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (d #t) (k 0)))) (h "1178nn6zqqwhqqx1i7wrd1pl6c3bf64h93k39r3yrv1lbryxad0j")))

(define-public crate-tokio-snappy-0.2.0 (c (n "tokio-snappy") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "snap") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0xprga85ihhyrycss13yinm3690h6yvkiq6z1i617rw7x6gapc7w")))

