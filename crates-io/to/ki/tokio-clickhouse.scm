(define-module (crates-io to ki tokio-clickhouse) #:use-module (crates-io))

(define-public crate-tokio-clickhouse-0.0.1 (c (n "tokio-clickhouse") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.20") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "1ashkn6ndr63z9c0abmw093j1lfzn60k6sz6ybgdpqa2xanbggc3")))

