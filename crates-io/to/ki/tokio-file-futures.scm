(define-module (crates-io to ki tokio-file-futures) #:use-module (crates-io))

(define-public crate-tokio-file-futures-0.1.0 (c (n "tokio-file-futures") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 0)))) (h "1l0yhl56pq3lkhbhsidy5mvv3l0yis2mh6sgbk1jhhabg0252q1p")))

