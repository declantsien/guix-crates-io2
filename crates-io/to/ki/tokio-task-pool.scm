(define-module (crates-io to ki tokio-task-pool) #:use-module (crates-io))

(define-public crate-tokio-task-pool-0.1.0 (c (n "tokio-task-pool") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v710gyg6ll41pha57i5aa5jxcwdaib8kqpxdghymxwmbmzamiyx")))

(define-public crate-tokio-task-pool-0.1.1 (c (n "tokio-task-pool") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kdg2n0c7szw0yp4s7rax4xfng2p0sq08bqz28svf524zsc9w952")))

(define-public crate-tokio-task-pool-0.1.2 (c (n "tokio-task-pool") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f09ykw3hn9a5hl0pszizxp082rzw4pkzlfnm4vzw5yk1yldkvwk")))

(define-public crate-tokio-task-pool-0.1.3 (c (n "tokio-task-pool") (v "0.1.3") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0njldcpnff4z6jxnngw5wq9i01q172nhzw9ld6hgrf77yvk59d8a")))

(define-public crate-tokio-task-pool-0.1.4 (c (n "tokio-task-pool") (v "0.1.4") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09qph8q294vjmryrbsx43gxw5ri8k9vi87zhgfbi96bbffj2zvb0")))

(define-public crate-tokio-task-pool-0.1.5 (c (n "tokio-task-pool") (v "0.1.5") (d (list (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lw9zwkzymdzc462rf3mb4m5bndy6h8lvszh72vfnlm0ckb7kfbm")))

