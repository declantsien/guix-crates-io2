(define-module (crates-io to ki tokio-utils) #:use-module (crates-io))

(define-public crate-tokio-utils-0.0.0 (c (n "tokio-utils") (v "0.0.0") (h "1zcp5bzmpigz56hgxsb9wpdn4i24ld5mz2h45ryyfir6mkymv64b")))

(define-public crate-tokio-utils-0.1.0 (c (n "tokio-utils") (v "0.1.0") (d (list (d (n "async-stdin") (r "^0.3.1") (d #t) (k 0)) (d (n "async-throttle") (r "^0.3.1") (d #t) (k 0)) (d (n "shutdown-async") (r "^0.1.0") (d #t) (k 0)) (d (n "tub") (r "^0.3.6") (d #t) (k 0)))) (h "0vldfs33ajvccl6spqy1z7v38h00ays6q66j2795h04yw9imjyij")))

(define-public crate-tokio-utils-0.1.1 (c (n "tokio-utils") (v "0.1.1") (d (list (d (n "async-stdin") (r "^0.3.1") (d #t) (k 0)) (d (n "async-throttle") (r "^0.3.2") (d #t) (k 0)) (d (n "shutdown-async") (r "^0.1.1") (d #t) (k 0)) (d (n "tub") (r "^0.3.7") (d #t) (k 0)))) (h "1zsr8128r2kvmfcxlrbi5fgpr9kd631swmb52rfalz7anbyv6jdc")))

(define-public crate-tokio-utils-0.1.2 (c (n "tokio-utils") (v "0.1.2") (d (list (d (n "async-stdin") (r "^0.3.1") (d #t) (k 0)) (d (n "async-throttle") (r "^0.3.2") (d #t) (k 0)) (d (n "shutdown-async") (r "^0.1.1") (d #t) (k 0)) (d (n "tub") (r "^0.3.7") (d #t) (k 0)))) (h "1s2wiy4f8bmxcc9pbyc1glfsxwiw1qv7b5lbwh7sals18rgzfxfy")))

