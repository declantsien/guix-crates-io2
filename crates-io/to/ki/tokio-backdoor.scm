(define-module (crates-io to ki tokio-backdoor) #:use-module (crates-io))

(define-public crate-tokio-backdoor-0.1.0 (c (n "tokio-backdoor") (v "0.1.0") (h "0rmjggh579yv763l6mc60r0nllns9wv6flbs6dfq1s7fpx3f8h3c")))

(define-public crate-tokio-backdoor-0.1.1 (c (n "tokio-backdoor") (v "0.1.1") (h "0n93i2n3z442cha1m75d7z6x9fzl4w70ywvhn7rmisygg26zq7a1")))

