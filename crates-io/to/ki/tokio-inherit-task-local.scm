(define-module (crates-io to ki tokio-inherit-task-local) #:use-module (crates-io))

(define-public crate-tokio-inherit-task-local-0.1.0 (c (n "tokio-inherit-task-local") (v "0.1.0") (d (list (d (n "const-random") (r "^0.1.18") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0qdr6j2bz2fkrzr6mbpj9ldgwn6j4pm2isfy1f927790gw2ncawh")))

(define-public crate-tokio-inherit-task-local-0.2.0 (c (n "tokio-inherit-task-local") (v "0.2.0") (d (list (d (n "const-random") (r "^0.1.18") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1b3pbv3m8q7rlh45159g3ahf96hj11b7dy7prxwl5x6zmj2v2bfl")))

