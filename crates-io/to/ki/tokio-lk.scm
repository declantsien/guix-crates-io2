(define-module (crates-io to ki tokio-lk) #:use-module (crates-io))

(define-public crate-tokio-lk-0.1.0 (c (n "tokio-lk") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "08fz8dw5zf7mxla0fpf9hzc285z5c64195xydv37bil8qgrm4kwb") (y #t)))

(define-public crate-tokio-lk-0.1.1 (c (n "tokio-lk") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "1rkxc10jx0hdxhj448zsprgw14ik3byxvm70pys25v047x8cyz3n") (y #t)))

(define-public crate-tokio-lk-0.1.2 (c (n "tokio-lk") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "1hd1qyd5csmhq9f3sh3kxli1xzy8f65x8f3qbp32c2nr3lqjf1l0")))

(define-public crate-tokio-lk-0.1.3 (c (n "tokio-lk") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dashmap") (r "^3.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "0ap2wx0jaj23klgnq6kimk3fia6kmyg0gwp23xf3x74wgvn7bc35")))

(define-public crate-tokio-lk-0.2.0 (c (n "tokio-lk") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dashmap") (r "^3.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "rt-threaded" "rt-util" "time" "macros"))) (d #t) (k 0)))) (h "1ggrcx3917hr8xndyazq391xybib0545hgc29ly2kjrpd6wca91c")))

(define-public crate-tokio-lk-0.2.1 (c (n "tokio-lk") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dashmap") (r "^3.7.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.1") (f (quote ("nightly" "inline-more" "ahash" "ahash-compile-time-rng"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "rt-threaded" "rt-util" "time" "macros"))) (d #t) (k 0)))) (h "1vjv521wcaf42ih3zx9wgn1ir2rk017pnijs61275g3wsh6jy2ad") (f (quote (("default" "hashbrown") ("all" "dashmap" "hashbrown"))))))

(define-public crate-tokio-lk-0.2.2 (c (n "tokio-lk") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dashmap") (r "^3.7.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.1") (f (quote ("nightly" "inline-more" "ahash" "ahash-compile-time-rng"))) (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "rt-threaded" "rt-util" "time" "macros"))) (d #t) (k 0)))) (h "1agh3awicg2jmk38d89vyh31x09cqd6pqkc025plhaihm40zb656") (f (quote (("default" "hashbrown") ("all" "dashmap" "hashbrown"))))))

