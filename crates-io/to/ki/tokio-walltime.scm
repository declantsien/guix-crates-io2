(define-module (crates-io to ki tokio-walltime) #:use-module (crates-io))

(define-public crate-tokio-walltime-0.1.0 (c (n "tokio-walltime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal"))) (d #t) (k 0)))) (h "1pbvylkpdn8k228lwf7h25d2j0aczylghkiirw7hq81jmvxmb07i")))

(define-public crate-tokio-walltime-0.1.1 (c (n "tokio-walltime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal"))) (d #t) (k 0)))) (h "13y9gm9d2h5dabjwbka492ymz64ghpl642rcksip0fzb50g8x7wj")))

(define-public crate-tokio-walltime-0.1.2 (c (n "tokio-walltime") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal"))) (d #t) (k 0)))) (h "0djmv5wl009f8q2x4njnwhfiyi1nfncnmfi6c18745szw2hj5wii")))

(define-public crate-tokio-walltime-0.1.3 (c (n "tokio-walltime") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "rt" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0kw4idkzmk9zqxlzdcfbjn4s0jxfy610vxr00y89jki1mrm8wzcc")))

