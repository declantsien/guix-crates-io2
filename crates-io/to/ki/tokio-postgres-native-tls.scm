(define-module (crates-io to ki tokio-postgres-native-tls) #:use-module (crates-io))

(define-public crate-tokio-postgres-native-tls-0.1.0-rc.1 (c (n "tokio-postgres-native-tls") (v "0.1.0-rc.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.4.0-rc.1") (k 0)) (d (n "tokio-tls") (r "^0.2.1") (d #t) (k 0)))) (h "02wmijrsg1gzkbadxck819cz4vrwvncyfbz570bnnbv9i1lmdqa5") (f (quote (("runtime" "tokio-postgres/runtime") ("default" "runtime"))))))

