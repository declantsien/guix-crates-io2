(define-module (crates-io to ki tokio-macros) #:use-module (crates-io))

(define-public crate-tokio-macros-0.0.0 (c (n "tokio-macros") (v "0.0.0") (h "1xlbiv7hvfp0l55xrvqm9725pig5q7dnivl5364z0mz1kdwx9pgw")))

(define-public crate-tokio-macros-0.2.0-alpha.1 (c (n "tokio-macros") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.1") (f (quote ("rt-full"))) (k 2)))) (h "1rinbfbcxn0wr78iqidvjd3ax5qqcj8nhn8sakl5ly711537knh4")))

(define-public crate-tokio-macros-0.2.0-alpha.2 (c (n "tokio-macros") (v "0.2.0-alpha.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04nd0qkx33ql8bd16ppldvkhmqivkfnjqlwnifvrnxcg00175jvj")))

(define-public crate-tokio-macros-0.2.0-alpha.3 (c (n "tokio-macros") (v "0.2.0-alpha.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f1c42wnrfajqbgz98jh92rrj83l3vk4jx8hgbmi25zxl47m459z")))

(define-public crate-tokio-macros-0.2.0-alpha.4 (c (n "tokio-macros") (v "0.2.0-alpha.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01gm7x693pj0bk4pw8nlxi7x8sih3mhlkszln6bc16ms7n87943w")))

(define-public crate-tokio-macros-0.2.0-alpha.5 (c (n "tokio-macros") (v "0.2.0-alpha.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0idc6n5rh8jbxppsgdyx6wnln9r52ziqs70rkhamjw8qx1h04axh")))

(define-public crate-tokio-macros-0.2.0-alpha.6 (c (n "tokio-macros") (v "0.2.0-alpha.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0kln2i8s7b34sv040f65cc8kz4fw0z5dzw71ficxkbfd9cviddl6")))

(define-public crate-tokio-macros-0.2.0 (c (n "tokio-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0zm8k893z4mrpvbl70vy5waddhnkpnawmdn9xkfccdcm85qmlyfm")))

(define-public crate-tokio-macros-0.2.1 (c (n "tokio-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1g222gwq4pdyccgbcpkicbdrd6dd0ca0gcabc4scxc5s14dc5rkx")))

(define-public crate-tokio-macros-0.2.2 (c (n "tokio-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0i3gqkscsc3jx00hs0d27k1hq1hi3ksiyj37d6a0aqj8f87zqi8k") (y #t)))

(define-public crate-tokio-macros-0.2.3 (c (n "tokio-macros") (v "0.2.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ylsnl247y9mz5v61s00iaz6vp0kr3pi9dfaipparcixi8k1z9jh")))

(define-public crate-tokio-macros-0.2.4 (c (n "tokio-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0z0gn951glmyhjb2pyyvgj4l8rg7xc74p44rv7rjlk2xgpnygcgl")))

(define-public crate-tokio-macros-0.2.5 (c (n "tokio-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1283aq0l7rnh79zzqk4r34dgimvwcymrzmg1yah9ai2nmb3arhzh")))

(define-public crate-tokio-macros-0.3.0 (c (n "tokio-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0gqhaknfscjwgnk1sy3cad7b5gjgb4dd4y6z7safr9n7crxsm36l")))

(define-public crate-tokio-macros-0.3.1 (c (n "tokio-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0iaw1k9qcn3fjgl4qcqwl8if8jjdkldam4cn0j8qybfwnpdhzlr1")))

(define-public crate-tokio-macros-0.2.6 (c (n "tokio-macros") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ni60vnrf32r3wfhlahmnds1phx5d1xfbmyq9j0mz8kkzh5s0kg4")))

(define-public crate-tokio-macros-0.3.2 (c (n "tokio-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1dvd3gji7a0i1kgck2lwgbcbklk3qb1bsqgd2v9amj63kyjzzps6")))

(define-public crate-tokio-macros-1.0.0 (c (n "tokio-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "150l6wfcqw2rcjaf22qk3z6ca794x0s2c68n5ar18cfafllpsla2")))

(define-public crate-tokio-macros-1.1.0 (c (n "tokio-macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0mys5zm2gcdgc0iq8nqipzn703q26x1bpw59m04shikgacdb3xya")))

(define-public crate-tokio-macros-1.2.0 (c (n "tokio-macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0drws4ji260pmbv5afb72mb1ql6qamkm0wk40j3gpnj173s3v7n4")))

(define-public crate-tokio-macros-1.3.0 (c (n "tokio-macros") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "045igm2h1mfjakbi68hrl07dflgs2rfvvjff17ylxgjf3zk3nisl")))

(define-public crate-tokio-macros-1.4.0 (c (n "tokio-macros") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0bfhr8a2g5jf44ajhvbkmym9xk1v6dav7zrvqh2zjsfljiw49bbb") (y #t)))

(define-public crate-tokio-macros-1.4.1 (c (n "tokio-macros") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1sz5pq7yzm40x935zvrhfqpf7s838wlkks0rrnd63hlryk498iqm")))

(define-public crate-tokio-macros-1.5.0 (c (n "tokio-macros") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1zd0zklw6bwzhnfbz04ypr8pkj1nzdm7qddx77wqvdm7mfp8bpdj")))

(define-public crate-tokio-macros-1.5.1 (c (n "tokio-macros") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "15chrgc62ydsq05i0dp0imqx1ggvfl0glyj6g72i4qma86q86hqi")))

(define-public crate-tokio-macros-1.6.0 (c (n "tokio-macros") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0pmsxblxda3mx8cwi2fzpyzzlbc85fm6cmjiyx1pjhvpl2mw3vy9")))

(define-public crate-tokio-macros-1.7.0 (c (n "tokio-macros") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ds34qsfvgf63cjgdx3gr4pl7i76fifyar15ksbillcc8hpzfmxm") (r "1.46")))

(define-public crate-tokio-macros-1.8.0 (c (n "tokio-macros") (v "1.8.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11140lnx88qycdx8ynxgk0317gnw1qsy16ydlgvpx67vfnlzj94p") (r "1.49")))

(define-public crate-tokio-macros-1.8.1 (c (n "tokio-macros") (v "1.8.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1b3hgphz8i71vdrg5i03abny44xd73y9psp81kfshhjdnyxyrj2y") (y #t) (r "1.49")))

(define-public crate-tokio-macros-1.8.2 (c (n "tokio-macros") (v "1.8.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1y3dphh8i4971wcfqxjhd662nain1i86rsf3y79mazr8vq7w0rnj") (r "1.49")))

(define-public crate-tokio-macros-2.0.0 (c (n "tokio-macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1kni17i1sbyz8rvn83g48z4058z8ckcb7lgfvpbfk1brr2yp79b1") (r "1.56")))

(define-public crate-tokio-macros-2.1.0 (c (n "tokio-macros") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0pk7y9dfanab886iaqwcbri39jkw33kgl7y07v0kg1pp8prdq2v3") (r "1.56")))

(define-public crate-tokio-macros-2.2.0 (c (n "tokio-macros") (v "2.2.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0fwjy4vdx1h9pi4g2nml72wi0fr27b5m954p13ji9anyy8l1x2jv") (r "1.63")))

