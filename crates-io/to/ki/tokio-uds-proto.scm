(define-module (crates-io to ki tokio-uds-proto) #:use-module (crates-io))

(define-public crate-tokio-uds-proto-0.0.1 (c (n "tokio-uds-proto") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "002y1p8lnh58graflnwija650xih5jxy74v9v26jhzdi90vckv0a")))

(define-public crate-tokio-uds-proto-0.0.2 (c (n "tokio-uds-proto") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "0lvfh1xi5skcpgxh33cdpdcfg0kqw5rnahlfv8krnsg0kq2aj25g")))

(define-public crate-tokio-uds-proto-0.1.0-rc1 (c (n "tokio-uds-proto") (v "0.1.0-rc1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "0fh0r05wi3ngb337vwlhakk1vl4drdxrh8c6gqs66glvv5gh8c6p")))

(define-public crate-tokio-uds-proto-0.1.0-rc2 (c (n "tokio-uds-proto") (v "0.1.0-rc2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "01fd0zyygbsg33b55akynjk4ndkpbmmcldl16dyv05d15a6x3ckm")))

(define-public crate-tokio-uds-proto-0.1.0 (c (n "tokio-uds-proto") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "1fa78pbkjqn81czn8fdxqcaw356wg5jrklsh6dciwqbpzvbrm96w")))

(define-public crate-tokio-uds-proto-0.1.1 (c (n "tokio-uds-proto") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (d #t) (k 2)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.1") (d #t) (k 0)))) (h "0nysk77xwl8z3d5bwmhz92ffcjams0czh9a2nn7a9lk0yy1jz14k")))

