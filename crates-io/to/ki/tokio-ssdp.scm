(define-module (crates-io to ki tokio-ssdp) #:use-module (crates-io))

(define-public crate-tokio-ssdp-0.1.0 (c (n "tokio-ssdp") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "httparse") (r "^1.5.1") (d #t) (k 0)) (d (n "httpdate") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "socket2") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "05qfhv6kjms83mc8iiiqyn82jf5g350h4ppz7mvmbkpa67s0whax")))

