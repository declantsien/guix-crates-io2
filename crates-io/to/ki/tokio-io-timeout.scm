(define-module (crates-io to ki tokio-io-timeout) #:use-module (crates-io))

(define-public crate-tokio-io-timeout-0.1.0 (c (n "tokio-io-timeout") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "0vxxa0c577bw7yqy4mmn3ab4kfbqp7d4yi3xjh8zmym1c9svcfy2")))

(define-public crate-tokio-io-timeout-0.1.1 (c (n "tokio-io-timeout") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "16jh3km47wvd7g0mqw7dskk4ikjhsy4m7s04d627yfi0x57b1a97")))

(define-public crate-tokio-io-timeout-0.1.2 (c (n "tokio-io-timeout") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "1i00sxhxavki8g38v191m3wmb2sm2s27kyln2b2r9l2cip0pg06a")))

(define-public crate-tokio-io-timeout-0.1.3 (c (n "tokio-io-timeout") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "0rsix8dd1r6vfx98jlyss014pvgb4vgvfmssvxf6cf7b0sabvnvq")))

(define-public crate-tokio-io-timeout-0.1.4 (c (n "tokio-io-timeout") (v "0.1.4") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)))) (h "1j3hfggqq5ah7hhfa2f60b5fmlzqzl17wvmc2s4w3w8a5miay2dy") (y #t)))

(define-public crate-tokio-io-timeout-0.2.0 (c (n "tokio-io-timeout") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)))) (h "0vqx6amn2hwijhx4jl4sjq1sqcp4vscih8w5j69g6s2hsw8adhdy")))

(define-public crate-tokio-io-timeout-0.3.0 (c (n "tokio-io-timeout") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio") (r "^0.1.5") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.0") (d #t) (k 0)))) (h "1c9781id5pdlq2hfw2j91246q2rg6q84q1g8pxl33hvwqs56r6dw")))

(define-public crate-tokio-io-timeout-0.3.1 (c (n "tokio-io-timeout") (v "0.3.1") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio") (r "^0.1.5") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.0") (d #t) (k 0)))) (h "0a9i298smfb9k5q8m2p9g7myp7i9l5br0kv8mhprimyg2lgyhp0k")))

(define-public crate-tokio-io-timeout-0.4.0-alpha.1 (c (n "tokio-io-timeout") (v "0.4.0-alpha.1") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.4") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.4") (d #t) (k 0)) (d (n "tokio-timer") (r "= 0.3.0-alpha.4") (d #t) (k 0)))) (h "0v9sqqarzxyxnf4nmf7qfn9d23gc8wimw4k175h423c1h283nvbf")))

(define-public crate-tokio-io-timeout-0.4.0-alpha.2 (c (n "tokio-io-timeout") (v "0.4.0-alpha.2") (d (list (d (n "bytes") (r "^0.4.4") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 2)) (d (n "tokio-io") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-timer") (r "= 0.3.0-alpha.6") (d #t) (k 0)))) (h "1rl4slcpjzxyjnfnrbj7bb18fki51vpkisfipc4r4qzz6yb1brl5")))

(define-public crate-tokio-io-timeout-0.4.0 (c (n "tokio-io-timeout") (v "0.4.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "05b2l10y1vjd63xa4c2mw13m1zgamfvf5lfi5s8sr9n8f8ra944k")))

(define-public crate-tokio-io-timeout-0.5.0 (c (n "tokio-io-timeout") (v "0.5.0") (d (list (d (n "tokio") (r "^0.3") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "1yz87c3lj1i32xka1pvd9j8a3nnbzd21v280h0ib9c168gdacm36")))

(define-public crate-tokio-io-timeout-1.0.0 (c (n "tokio-io-timeout") (v "1.0.0") (d (list (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1knx75r3f2vkpslja5xkl79fmbk00pq3y213yf63z28skdj7h0ma")))

(define-public crate-tokio-io-timeout-1.0.1 (c (n "tokio-io-timeout") (v "1.0.1") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1r61s5fgiq6cvdzvhwjjrsv3rqvj0zj2hgx01zss92jml4hn7h32")))

(define-public crate-tokio-io-timeout-1.1.0 (c (n "tokio-io-timeout") (v "1.1.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0xz0m7blbp4nwv0ac292qrjncgvclykbp7433pnv9k4z3n3lahgr")))

(define-public crate-tokio-io-timeout-1.1.1 (c (n "tokio-io-timeout") (v "1.1.1") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1s9dlkzsn0yng98zdmkddjx11jwawkxk3pbi8laxwh72dc89zi4h")))

(define-public crate-tokio-io-timeout-1.2.0 (c (n "tokio-io-timeout") (v "1.2.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1gx84f92q1491vj4pkn81j8pz1s3pgwnbrsdhfsa2556mli41drh")))

