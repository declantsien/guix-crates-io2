(define-module (crates-io to ki tokio-trace-core) #:use-module (crates-io))

(define-public crate-tokio-trace-core-0.0.0 (c (n "tokio-trace-core") (v "0.0.0") (h "0zm3pcs0d6yrc3y6xjjrqk94fq08nddaa2vyw7x6l6nsl0h2xxqg")))

(define-public crate-tokio-trace-core-0.1.0 (c (n "tokio-trace-core") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1lwx1m1x5jaf4p8gczhnyrcsnid4cxba92z4b8cdqc4qvvd9w31m")))

(define-public crate-tokio-trace-core-0.2.0 (c (n "tokio-trace-core") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "04y6c2r4ddzk02xb3hn60s9a1w92h0g8pzmxwaspqvwmsrba5j59")))

