(define-module (crates-io to ki tokio-io-rewind) #:use-module (crates-io))

(define-public crate-tokio-io-rewind-0.1.0 (c (n "tokio-io-rewind") (v "0.1.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1nflj6544d37jqfivmqhh59ns803pvnwhhbxp5hn6flabj2i4j2g")))

(define-public crate-tokio-io-rewind-0.1.1 (c (n "tokio-io-rewind") (v "0.1.1") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "10x60mnvpyp8i333f944kzfvwij63vkwb095jh3j57yxvwgxxlgs")))

(define-public crate-tokio-io-rewind-0.1.2 (c (n "tokio-io-rewind") (v "0.1.2") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "02y7x0hif7xf2jypyvzip4mi3vmxcaph142j9yksczi32bwr4am9")))

(define-public crate-tokio-io-rewind-0.1.3 (c (n "tokio-io-rewind") (v "0.1.3") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0h4g1i3ij44vhis9zv2sdmkamax0xn8s4qbifwfixmkf66ajnka9")))

