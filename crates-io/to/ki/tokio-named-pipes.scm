(define-module (crates-io to ki tokio-named-pipes) #:use-module (crates-io))

(define-public crate-tokio-named-pipes-0.1.0 (c (n "tokio-named-pipes") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "mio-named-pipes") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1bjy59wdl2anl22w6qyzkff1afv7ynayfpms10iqna2j6142sa4x")))

