(define-module (crates-io to ki tokio-too-busy) #:use-module (crates-io))

(define-public crate-tokio-too-busy-0.1.0 (c (n "tokio-too-busy") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "0v4kvnx2gx8hzjfcl1bl3i56qb66pfl1pj6r0sb0nnk25zn32rh5")))

