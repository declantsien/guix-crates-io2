(define-module (crates-io to ki tokio-sando) #:use-module (crates-io))

(define-public crate-tokio-sando-0.1.0 (c (n "tokio-sando") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "04zfdgwq5jfwkv0m231049pm3i596l0rsyxi107xh5rhn71j7sgl")))

