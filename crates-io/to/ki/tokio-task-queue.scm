(define-module (crates-io to ki tokio-task-queue) #:use-module (crates-io))

(define-public crate-tokio-task-queue-1.0.1 (c (n "tokio-task-queue") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^4.0.0") (d #t) (k 0)))) (h "0bgb220xp0wx6wvz5qpy3j4qw0xbpqv32zid4ysxcfnszy2h20cy")))

(define-public crate-tokio-task-queue-1.1.0 (c (n "tokio-task-queue") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^4.0.0") (d #t) (k 0)))) (h "0lpdc0qlbgpbrh5k1lbasyp6vid3wxfkjcv1dlp8b267hz8pi3mk")))

(define-public crate-tokio-task-queue-2.0.0 (c (n "tokio-task-queue") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^4.0.0") (d #t) (k 0)))) (h "1hh4y23h39sc65m8vq56h6xp77k1s5k50myr7lk7i50x2jjv59sc")))

(define-public crate-tokio-task-queue-3.0.0 (c (n "tokio-task-queue") (v "3.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^8.0.0") (d #t) (k 0)))) (h "1bivr70m4aw8byvsyxki7xnasdmwv9349pvpamy8wvlgg41a23j4")))

(define-public crate-tokio-task-queue-3.1.0 (c (n "tokio-task-queue") (v "3.1.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^8.0.0") (d #t) (k 0)))) (h "188siwl3vv40w19m4dgss8gwiwlgk0nj0bwb3w7dwcfgsv4gbkwf")))

(define-public crate-tokio-task-queue-4.0.0 (c (n "tokio-task-queue") (v "4.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^8.0.0") (d #t) (k 0)))) (h "07vvcd76chbrxagk5ni05a0m1pwvwf2xf4fnzkn2kr0zz73al85s")))

(define-public crate-tokio-task-queue-4.0.2 (c (n "tokio-task-queue") (v "4.0.2") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^8.0.0") (d #t) (k 0)))) (h "1dlsgjfjd30r4a666bli72vkl3pqb17ify1g4r4sdi04chpbs4lm")))

(define-public crate-tokio-task-queue-5.0.0 (c (n "tokio-task-queue") (v "5.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^8.0.0") (d #t) (k 0)))) (h "1hv4fvpsnmlx4cklvf62vn3i0iaz8shfq71dqzrgwnq6drccpiy8") (y #t)))

(define-public crate-tokio-task-queue-6.0.0 (c (n "tokio-task-queue") (v "6.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^8.0.0") (d #t) (k 0)))) (h "0f0q8va8fvdwg1gxkd7ha7l14l57z9mx2yhlshk07s3clc7zf3cw")))

(define-public crate-tokio-task-queue-7.0.0 (c (n "tokio-task-queue") (v "7.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "sync" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-interruptible-future") (r "^8.0.0") (d #t) (k 0)))) (h "1myg7ijp4jacmync6ckf564wsakqpi7967pv8qxg24bia04ar246")))

