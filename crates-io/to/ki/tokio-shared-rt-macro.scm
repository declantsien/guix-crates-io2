(define-module (crates-io to ki tokio-shared-rt-macro) #:use-module (crates-io))

(define-public crate-tokio-shared-rt-macro-0.1.0 (c (n "tokio-shared-rt-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p2is13ymyirmnzhbw0kxvhslaymvk1ksd4pmg8b1159wfa9mr0z")))

