(define-module (crates-io to ki tokio-stdout) #:use-module (crates-io))

(define-public crate-tokio-stdout-0.1.0 (c (n "tokio-stdout") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-fmt-encoder") (r "^0.2") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 2)))) (h "14r5d81d45a0yci9cd117d63hjxfkgyy7lk32cfyy3mghg97dgd8")))

