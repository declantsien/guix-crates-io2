(define-module (crates-io to ki tokio01-test) #:use-module (crates-io))

(define-public crate-tokio01-test-0.1.0 (c (n "tokio01-test") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "0brk94ms6dvrq9s39d26k8qx2swx6xpcsvbkks0s9py30w4rzn00")))

(define-public crate-tokio01-test-0.1.1 (c (n "tokio01-test") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "1f8v1jyhid8r5k778acybnb0xgfpm0sz7zsv2xyjy6yix5n78lpm")))

