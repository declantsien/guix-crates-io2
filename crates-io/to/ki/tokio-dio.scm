(define-module (crates-io to ki tokio-dio) #:use-module (crates-io))

(define-public crate-tokio-dio-0.1.0 (c (n "tokio-dio") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (f (quote ("use_std" "align"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "15y6xwcc925npk18fz5hkdas907ilqi3lbbfmkxkx5hz7rrdpi2h")))

