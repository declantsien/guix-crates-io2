(define-module (crates-io to ki tokio-actor) #:use-module (crates-io))

(define-public crate-tokio-actor-0.1.0 (c (n "tokio-actor") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (d #t) (k 0)))) (h "0w4vs0812yyfdjilbzxmx4cz2vakmhyz74badq9lnd9sbhm2aszm")))

(define-public crate-tokio-actor-0.1.1 (c (n "tokio-actor") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (d #t) (k 0)))) (h "1sc2i2hgj6r4iakf2fnb6ikk0dhh3issg9a6w6mas2jkgs73vb22")))

