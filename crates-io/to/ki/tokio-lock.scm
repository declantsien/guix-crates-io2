(define-module (crates-io to ki tokio-lock) #:use-module (crates-io))

(define-public crate-tokio-lock-0.1.0-dev (c (n "tokio-lock") (v "0.1.0-dev") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 0)))) (h "043sgycbmncwfhh6zxwz12d7bkzr9i2mccaqqa1nfgv9as559r69")))

(define-public crate-tokio-lock-0.2.0-dev (c (n "tokio-lock") (v "0.2.0-dev") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 0)))) (h "05v588r3h5qnixgnyxyk0mr7ncd1cdn0npiv3csn48dgr1dg3r43")))

(define-public crate-tokio-lock-1.0.0 (c (n "tokio-lock") (v "1.0.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 0)))) (h "1wz5863x3rrfa9krijk9b09l8mc4jcly56wlvprjblnm0z82x060")))

(define-public crate-tokio-lock-1.0.1 (c (n "tokio-lock") (v "1.0.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 0)))) (h "122w7kzfzwb1sln2mh1bkgpf3czzryvkam8yipdqrfyyl4wv7ghk")))

(define-public crate-tokio-lock-1.1.0 (c (n "tokio-lock") (v "1.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 0)))) (h "1xsdlmssmfmws7amzkcc145s7hgkn8769dcmscy1h9c4frxwqviy")))

