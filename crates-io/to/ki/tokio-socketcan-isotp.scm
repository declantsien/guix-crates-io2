(define-module (crates-io to ki tokio-socketcan-isotp) #:use-module (crates-io))

(define-public crate-tokio-socketcan-isotp-0.1.0 (c (n "tokio-socketcan-isotp") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)))) (h "11h3zy75x3lcf19jzcg2zm862g84acjbiis5bk1azd34hvaq5i3m")))

(define-public crate-tokio-socketcan-isotp-0.2.0 (c (n "tokio-socketcan-isotp") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)))) (h "0bgaasqwszqjylbhxjdg2ih7j83r72knlw7by4sxa7zfsa23qlx9")))

