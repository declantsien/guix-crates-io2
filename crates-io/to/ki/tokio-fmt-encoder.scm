(define-module (crates-io to ki tokio-fmt-encoder) #:use-module (crates-io))

(define-public crate-tokio-fmt-encoder-0.1.0 (c (n "tokio-fmt-encoder") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1bvn9cli1z1s2cwik101vgxfx6hb4i3syk36k82wiajs5m29ic3a")))

(define-public crate-tokio-fmt-encoder-0.2.0 (c (n "tokio-fmt-encoder") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1pk1amp2325ssy7lx5gbxnbaa3agl9ibfdl5vvh2xzzy29ndw6yj")))

