(define-module (crates-io to ki tokio-io-pool) #:use-module (crates-io))

(define-public crate-tokio-io-pool-0.1.0 (c (n "tokio-io-pool") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0yrz60znf6jzrv9sf3zm93z57nj4f6cca6c3ps8j9qwca7a96n8c")))

(define-public crate-tokio-io-pool-0.1.1 (c (n "tokio-io-pool") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "01hlrwmwn90k5ikpnq9ky5p7bfvhbbmz9lnjdrpjca1vq6fwngjw")))

(define-public crate-tokio-io-pool-0.1.2 (c (n "tokio-io-pool") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "1l6fdlv1s4j79rjv4jdlhzdk7c13z0kb6v0wrz6q0sdkwwjq2i1h")))

(define-public crate-tokio-io-pool-0.1.3 (c (n "tokio-io-pool") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "0jn3vqiym6gqy4fbb1pvkliw88zichn4bd1j0q2ynj1kw7xg9alb")))

(define-public crate-tokio-io-pool-0.1.4 (c (n "tokio-io-pool") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "152xyj8y3d9v8c91zpkfwxvx8ccyhyngrvxd6il8h68hwwvz4b1h")))

(define-public crate-tokio-io-pool-0.1.5 (c (n "tokio-io-pool") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1") (d #t) (k 2)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "0za1952lq8wf230gpxq73zlnzrnv9xbapybi12q2mld3p1f1rcwx")))

(define-public crate-tokio-io-pool-0.1.6 (c (n "tokio-io-pool") (v "0.1.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-current-thread") (r "^0.1") (d #t) (k 2)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 0)))) (h "17lrjj7lcw13wchpbvr8cynmypd29h40clf9qxabh6fxva40kwm5")))

(define-public crate-tokio-io-pool-0.2.0-alpha.1 (c (n "tokio-io-pool") (v "0.2.0-alpha.1") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.2.0-alpha.4") (d #t) (k 0)))) (h "0pgav2l9cj45dy5k3qny9mplsf0c4l31pxihmmr0q7dc9nm4cdzi")))

(define-public crate-tokio-io-pool-0.2.0-alpha.2 (c (n "tokio-io-pool") (v "0.2.0-alpha.2") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "futures-util-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.5") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.2.0-alpha.5") (d #t) (k 0)))) (h "0g4h5lm1ygd4wskr0fr6himfgfl7nj0wscd3p3yxs8mjy2dhyqrh")))

(define-public crate-tokio-io-pool-0.2.0-alpha.3 (c (n "tokio-io-pool") (v "0.2.0-alpha.3") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-executor") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "0svl3fs9sc3m98nkbmv33ad8a6w83xqqqpc5dmx0w2ddj8jr596b")))

(define-public crate-tokio-io-pool-0.2.0-alpha.4 (c (n "tokio-io-pool") (v "0.2.0-alpha.4") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-executor") (r "= 0.2.0-alpha.6") (d #t) (k 0)))) (h "0wm9qayfjq4cclp28d6byis6sp3yc6ypjsqmk6jfw4qajmn85ini")))

