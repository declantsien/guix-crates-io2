(define-module (crates-io to ki tokio-netstring) #:use-module (crates-io))

(define-public crate-tokio-netstring-0.1.0 (c (n "tokio-netstring") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "09gk5fdndn0q35h5g9f2s0r7m20pwizgz1pd3lc3xvkqgkwdvc84")))

(define-public crate-tokio-netstring-0.1.1 (c (n "tokio-netstring") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1dspa40lw3s81s144s3xlk0wkrmh30xii8qbxi1qj9ji6cdzk8b0")))

(define-public crate-tokio-netstring-0.1.2 (c (n "tokio-netstring") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1apl4mmyydyf8xf51mry1j5imzlh2hm3ipgqczpix6mms9xlcfp2")))

(define-public crate-tokio-netstring-0.1.3 (c (n "tokio-netstring") (v "0.1.3") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0iidpy9nhap46chbr1shldcra79hgpcvsy7h9bnpys2v7kcyi8xd")))

