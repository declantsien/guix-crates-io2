(define-module (crates-io to ki tokio_interval) #:use-module (crates-io))

(define-public crate-tokio_interval-0.1.0 (c (n "tokio_interval") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("time" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros"))) (d #t) (k 2)))) (h "1ggzrlgi8zsdfwsq1rjgc8nrrj38w4q3p7dv3h42jd3m0c2d6qsy") (y #t)))

(define-public crate-tokio_interval-0.1.1 (c (n "tokio_interval") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("time" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros"))) (d #t) (k 2)))) (h "1b4kc11d1wcgnyagxwakxpji92z5c6yb6c58w5sazh5n7zvcs5fm") (y #t)))

(define-public crate-tokio_interval-0.1.2 (c (n "tokio_interval") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("time" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros"))) (d #t) (k 2)))) (h "0xzzx38n594i3zyvdmahcyvhlwrkna9n4qk9llih91n83k61vhp6") (y #t)))

(define-public crate-tokio_interval-0.1.3 (c (n "tokio_interval") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("time" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros"))) (d #t) (k 2)))) (h "1lff592jsgb7cmycrjhjxmkbbgmcg8wd3j8mk6c5hywp5l71v2m0")))

(define-public crate-tokio_interval-0.1.4 (c (n "tokio_interval") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0sh3k793smqafhx9l70irzg8pklbbnzzdw12011bkibnvpryh9gd")))

