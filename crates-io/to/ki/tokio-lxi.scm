(define-module (crates-io to ki tokio-lxi) #:use-module (crates-io))

(define-public crate-tokio-lxi-0.1.0 (c (n "tokio-lxi") (v "0.1.0") (d (list (d (n "tokio") (r "^0.1.15") (d #t) (k 0)))) (h "07dyf2ax78yskww3qrgfnxmk3369704gvdib7zr11587yckpvl9d")))

(define-public crate-tokio-lxi-0.2.0 (c (n "tokio-lxi") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "13zd270q12qmrad7h2wrj9xcd0sc6bmylcl3xvwzq0qj7b54c81v")))

(define-public crate-tokio-lxi-0.2.1 (c (n "tokio-lxi") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (f (quote ("io"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("io-util" "tcp"))) (o #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("full"))) (d #t) (k 2)))) (h "1bgpdccb23pncpsyai0df3hic5s7vbxr4xk274g61bk5pmzasna6") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util" "async-std") ("default" "runtime-tokio"))))))

(define-public crate-tokio-lxi-0.2.2 (c (n "tokio-lxi") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "0iy6h1vdl7j7c050dg48rfdbyqlgqzwwylzp2jka28azcm8syy3h") (f (quote (("runtime-tokio" "tokio") ("runtime-async-std" "futures-util" "async-std") ("default" "runtime-tokio"))))))

