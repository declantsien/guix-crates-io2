(define-module (crates-io to ki tokio-eventfd) #:use-module (crates-io))

(define-public crate-tokio-eventfd-0.1.0 (c (n "tokio-eventfd") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1") (d #t) (k 0)))) (h "1vhpnv3ngmk6k8h5mjv6x6cgpryyw6r1wm5pqbxjkci2cis2vmam")))

(define-public crate-tokio-eventfd-0.2.0 (c (n "tokio-eventfd") (v "0.2.0") (d (list (d (n "futures-lite") (r "^1.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "test-util" "macros" "io-util" "time"))) (d #t) (k 2)))) (h "0l8gsfxpk7ljrq1r7i7npiwphpb33ipx8kqzmhdndvq5mxaa6nj3")))

(define-public crate-tokio-eventfd-0.2.1 (c (n "tokio-eventfd") (v "0.2.1") (d (list (d (n "futures-lite") (r "^1.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "rt" "test-util" "macros" "io-util" "time"))) (d #t) (k 2)))) (h "1w2474dcan38j8s46spr8vfi2j1kpy21i7nip7bz0f9ai444smyq")))

