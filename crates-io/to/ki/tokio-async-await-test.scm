(define-module (crates-io to ki tokio-async-await-test) #:use-module (crates-io))

(define-public crate-tokio-async-await-test-0.1.0 (c (n "tokio-async-await-test") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.10") (f (quote ("tokio-compat"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (f (quote ("async-await-preview"))) (d #t) (k 0)))) (h "0s1qcpnypzdyr3s4mlzjp16yq92ld0q7sdg648xc566qimp2zsgd")))

