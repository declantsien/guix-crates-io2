(define-module (crates-io to ki tokio-dbus-macros) #:use-module (crates-io))

(define-public crate-tokio-dbus-macros-0.1.1 (c (n "tokio-dbus-macros") (v "0.1.1") (h "0kgg37drb1syx3bi488wyiplcrw5ycqw8pwx2k696rhb9s0j1a3w") (r "1.66")))

(define-public crate-tokio-dbus-macros-0.1.2 (c (n "tokio-dbus-macros") (v "0.1.2") (h "0sx4mkp2n9aj0ihaldp9gnmvg5inwlykj7zcgpci8l05izdzcyxp") (r "1.66")))

(define-public crate-tokio-dbus-macros-0.1.3 (c (n "tokio-dbus-macros") (v "0.1.3") (h "0mwrn2p1wh25i21i0jqrk45hpjyq7bp0mlqnzb120c169gfbzrf3") (r "1.66")))

(define-public crate-tokio-dbus-macros-0.1.4 (c (n "tokio-dbus-macros") (v "0.1.4") (h "0abm1fap76clmai16b9zbjl3kx2hs4vir7mby2vbg382z0kxwajk") (r "1.66")))

