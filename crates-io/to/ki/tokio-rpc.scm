(define-module (crates-io to ki tokio-rpc) #:use-module (crates-io))

(define-public crate-tokio-rpc-0.1.0 (c (n "tokio-rpc") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "protobuf") (r "^1.2") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "1zp080c6ny0jhbisslsfbarcrqn88w1brpi4zw6n8d0fxq9a2fcs")))

(define-public crate-tokio-rpc-0.1.1 (c (n "tokio-rpc") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "protobuf") (r "^1.2") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "14g9rb3pdxnpka1g3vjis1ffw2bnl17mdc4nn44bnmgzyy8aqsl4")))

