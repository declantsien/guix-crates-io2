(define-module (crates-io to ki tokio-serde-msgpack) #:use-module (crates-io))

(define-public crate-tokio-serde-msgpack-0.1.0 (c (n "tokio-serde-msgpack") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 2)) (d (n "tokio-serde") (r "^0.2") (d #t) (k 0)))) (h "17x1wkgf39dhmn6b1xlxq7842y4ai4fkvx7bnh3w64j730mzk7bj")))

(define-public crate-tokio-serde-msgpack-0.2.0 (c (n "tokio-serde-msgpack") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1.6") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-stdin-stdout") (r "^0.1") (d #t) (k 2)))) (h "1v5blhyiy4idfb8racmjxs31sbs6gh9iw0n9favjfgx12xqcxwbw")))

