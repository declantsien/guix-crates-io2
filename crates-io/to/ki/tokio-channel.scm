(define-module (crates-io to ki tokio-channel) #:use-module (crates-io))

(define-public crate-tokio-channel-0.0.0 (c (n "tokio-channel") (v "0.0.0") (h "0gb6q90hxn1glyk6srfg5d2l4vipv12wm9yb2cddw6x5hwidcvkn")))

(define-public crate-tokio-channel-0.1.0 (c (n "tokio-channel") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)))) (h "1yhwk7pfrkd7vkkcjr9hi28mnp5kid1wrk7p00b57p8nfjw0y3ak") (f (quote (("async-await-preview"))))))

