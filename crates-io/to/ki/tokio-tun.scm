(define-module (crates-io to ki tokio-tun) #:use-module (crates-io))

(define-public crate-tokio-tun-0.1.0 (c (n "tokio-tun") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0lan5y70p80hz8jbl8hxksskvrgh1g17mc4zp00qqbgz1pw65g3k")))

(define-public crate-tokio-tun-0.2.0 (c (n "tokio-tun") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "05w83nvd7pjxnqka7l6fsnnd5rig2r1sjxy6llv751mwjywybaz9")))

(define-public crate-tokio-tun-0.2.1 (c (n "tokio-tun") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0irk8csvnavzrkp63ajd5xl49l7ifwafxg2ma02nwmx4h3xsz2xc")))

(define-public crate-tokio-tun-0.2.2 (c (n "tokio-tun") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("io-driver"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1ir534akbxpwgfj4y7wsj54l6rv0z9dpc8z7ywizzwr4254mmk8j")))

(define-public crate-tokio-tun-0.2.3 (c (n "tokio-tun") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "1j8hbgsf3563526j9d598li99yx38kg557k71d39gm46pnk2gc44")))

(define-public crate-tokio-tun-0.3.0 (c (n "tokio-tun") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ik0jd3761fnwshmrz1ix8zidycj04vdizdlyi5n49qzlfra3h17")))

(define-public crate-tokio-tun-0.3.1 (c (n "tokio-tun") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0x79zf26sla1nbbyzr87cbpg3ipix5g1zj4l5av1005r9lw81hvw")))

(define-public crate-tokio-tun-0.3.2 (c (n "tokio-tun") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wklx7gjjq7xxysiqp8mvxvp854jjrbr34rxi7sh1mb6h40ww69f") (y #t)))

(define-public crate-tokio-tun-0.3.3 (c (n "tokio-tun") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1cmpi2cxn99l42ly4hglrq1a14bp4nwvd3dbgrsiddcv2p5r4vch")))

(define-public crate-tokio-tun-0.3.4 (c (n "tokio-tun") (v "0.3.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0x8ij4m2wbgifc4lwfdjkak8cafcyb95fnqk759jmhif1539ijxb")))

(define-public crate-tokio-tun-0.3.5 (c (n "tokio-tun") (v "0.3.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zd7rvyi72d5553f97gq1ikkph7wqyzddislvn7rm3q1mp01c42c")))

(define-public crate-tokio-tun-0.3.6 (c (n "tokio-tun") (v "0.3.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1mig8l3d072liyrmyld3k3ndlap0wi36f77laa5fpkfa5zh3nz7b")))

(define-public crate-tokio-tun-0.3.7 (c (n "tokio-tun") (v "0.3.7") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "12d2v03wy0kcgvy3psv8pb9xzaq0vyb7bxxd7bkynyp84cqgd4il")))

(define-public crate-tokio-tun-0.3.8 (c (n "tokio-tun") (v "0.3.8") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 2)))) (h "058rhd25h7p1jkip8bv7gl83w4drgfb7d0x6lyjfxv80aksmpa74")))

(define-public crate-tokio-tun-0.3.9 (c (n "tokio-tun") (v "0.3.9") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("full"))) (d #t) (k 2)))) (h "146298l83a1nwwdw66aab6n3q84dsbd8yipj93rj59llag9n0hml")))

(define-public crate-tokio-tun-0.3.10 (c (n "tokio-tun") (v "0.3.10") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 2)))) (h "1c0yzgj407l5m6qz426qcgj0icjwpr5z86vzpdyzj2fw3drxvqqk")))

(define-public crate-tokio-tun-0.3.11 (c (n "tokio-tun") (v "0.3.11") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("full"))) (d #t) (k 2)))) (h "0cz5562rvk3y65hhyssdw3larlli5pb7mllzwz0n5zwpl7jp3g62")))

(define-public crate-tokio-tun-0.3.12 (c (n "tokio-tun") (v "0.3.12") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("full"))) (d #t) (k 2)))) (h "16xbzvj7rkfk2r5w1q6g4l296hhpjzk4ii2aw7bh456lva8961jb")))

(define-public crate-tokio-tun-0.3.13 (c (n "tokio-tun") (v "0.3.13") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("full"))) (d #t) (k 2)))) (h "12ksj99z9hawbf2iyzvnx1gdbix37i9x5dp4vfwm4xmkjpqqdivk")))

(define-public crate-tokio-tun-0.3.14 (c (n "tokio-tun") (v "0.3.14") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 2)))) (h "0hf9a6asmzhxdm3wnsm4j5qdsqbfvzdm8v3aknq0bv7a3cxw5q0d")))

(define-public crate-tokio-tun-0.3.15 (c (n "tokio-tun") (v "0.3.15") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 2)))) (h "1yvqgpwkrarrgd23cwdvs607mfr101vwgmsgi37z74xx4cbvjkxk")))

(define-public crate-tokio-tun-0.3.16 (c (n "tokio-tun") (v "0.3.16") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("full"))) (d #t) (k 2)))) (h "1vaxwkvmi192sxl8cmqm1ar2zpl50di7hl7dgw7z3mdzkdzn5mlk")))

(define-public crate-tokio-tun-0.4.0 (c (n "tokio-tun") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1rbb50wbwjgcid40k922x686pj5a77q18z6w72nag5srmcg8zn3w")))

(define-public crate-tokio-tun-0.5.0 (c (n "tokio-tun") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("full"))) (d #t) (k 2)))) (h "0pgc4smbgl2xgjh9cjyshrzkblkz1i3apcdxdnwhrakchk21s2cx")))

(define-public crate-tokio-tun-0.5.1 (c (n "tokio-tun") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 2)))) (h "06dkpd65ajws74s355drxyfcnf9s86brgmlxq1abmy434mfkqrls")))

(define-public crate-tokio-tun-0.5.2 (c (n "tokio-tun") (v "0.5.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 2)))) (h "1sk20sipmkzkpv2khif3gkns345f8jchjm59qbaa2dgj80yrk5zv")))

(define-public crate-tokio-tun-0.5.3 (c (n "tokio-tun") (v "0.5.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl"))) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 2)))) (h "05qha8j2g27mf51awgp9ar5q7l151ih98hl6nihkld5irv6p02jq")))

(define-public crate-tokio-tun-0.5.4 (c (n "tokio-tun") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0n0gkfd83msknnqahfwshhb5j5cii2dmgd24grfwbcnbblk7ikh7")))

(define-public crate-tokio-tun-0.6.0 (c (n "tokio-tun") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (f (quote ("ioctl"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0bfi2gbbadzkkma5c05mzpnrjm5n5qvj8y2ny49mfr0gcisya7qh")))

(define-public crate-tokio-tun-0.6.1 (c (n "tokio-tun") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("ioctl"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "056p620qk9jbmn7csl006jq4j60gkmm777isbqzdkb2rc26jnlmm")))

(define-public crate-tokio-tun-0.7.0 (c (n "tokio-tun") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("ioctl"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "16l9x7a5m6hg23bgn26b9wyqzjg7df4x0a8plj6l214y84jqr9c1")))

(define-public crate-tokio-tun-0.8.0 (c (n "tokio-tun") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1n7sd2q3xhjb32kgraf2l65k1z6myi56x5sinwbx5yk9c60300yj")))

(define-public crate-tokio-tun-0.9.0 (c (n "tokio-tun") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1szqgwj71ra7xxlinwk9xnzycgvb0qfbzdw87dagrfl1h0wvzq4y")))

(define-public crate-tokio-tun-0.9.1 (c (n "tokio-tun") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1jckaccz3mzfq8zrqm59nvdwmchxixh4c7yn9hgvlxx50la7v9n4")))

(define-public crate-tokio-tun-0.10.0 (c (n "tokio-tun") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ikb0r293qnq1nm26x55p5v3ggpzgpdrkwkbpg0g583vfj41hka1")))

(define-public crate-tokio-tun-0.11.0 (c (n "tokio-tun") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0cn9is5ym9ay3sjqimqzqxq2457v1jspq9vya431mgd48g2188iy")))

(define-public crate-tokio-tun-0.11.1 (c (n "tokio-tun") (v "0.11.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1g9f0yr87bl2faajhigj9nkgdy73b2mjfcxky77zmbj876fgjlcq")))

(define-public crate-tokio-tun-0.11.2 (c (n "tokio-tun") (v "0.11.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0s167d35q8wk89xang7fflf6qd1sw7lxdwdid0x9lxw67vrzlbmz")))

(define-public crate-tokio-tun-0.11.3 (c (n "tokio-tun") (v "0.11.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "032kmiqg9qk8brj319wbwlywljv1lp7avi8qqxyp9yhcpkybb52r")))

(define-public crate-tokio-tun-0.11.4 (c (n "tokio-tun") (v "0.11.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("ioctl"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1d86fbbnp9jpgk471293dcmbv0lhw62vax55ynqr0i2ip899kmv5")))

