(define-module (crates-io to ki tokio-bits) #:use-module (crates-io))

(define-public crate-tokio-bits-0.1.0 (c (n "tokio-bits") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 0)))) (h "1kszwhhxspmy3w2x1zd85rq22vy1r67svip2hl10yw8g7kjyjr5r")))

(define-public crate-tokio-bits-0.1.1 (c (n "tokio-bits") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 0)))) (h "1sa1r5kdad990gk02y33fbi2yg4gnpwilgp6p6fidwmrz623vqkk")))

(define-public crate-tokio-bits-0.1.2 (c (n "tokio-bits") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)))) (h "1xahvcpgndzl4c1ix64jvxiwwvldw0si6985sznxp827s0k0m73y")))

