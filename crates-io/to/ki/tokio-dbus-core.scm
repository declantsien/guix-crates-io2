(define-module (crates-io to ki tokio-dbus-core) #:use-module (crates-io))

(define-public crate-tokio-dbus-core-0.0.14 (c (n "tokio-dbus-core") (v "0.0.14") (h "1f9f15fn1sfcl201cl6km2lr81yhxsj5yn7wsbnssqd13p6icq5r") (r "1.66")))

(define-public crate-tokio-dbus-core-0.0.15 (c (n "tokio-dbus-core") (v "0.0.15") (h "10f9zyi7bfd07xi68xzyr0j405h1v88p17w54mc6fds9i59h8k99") (r "1.66")))

(define-public crate-tokio-dbus-core-0.0.16 (c (n "tokio-dbus-core") (v "0.0.16") (h "0x7ysrbd0f64hb53fadwrnswp23sb33vk4i04sgjvskz5m93ha3p") (r "1.66")))

(define-public crate-tokio-dbus-core-0.0.17 (c (n "tokio-dbus-core") (v "0.0.17") (h "12bhg032ajahaycn8yh46gczcghybarj6dxkydw2y400wwh7ymly") (r "1.66")))

