(define-module (crates-io to ki tokio-non-async) #:use-module (crates-io))

(define-public crate-tokio-non-async-0.1.0 (c (n "tokio-non-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "blocking"))) (d #t) (k 2)))) (h "1q5y3j1g5sm9mch4544l2ggqhfd6la3csmx08yvs7axhagm0xir4")))

(define-public crate-tokio-non-async-0.1.1 (c (n "tokio-non-async") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "blocking"))) (d #t) (k 2)))) (h "0y9s1cw8jmn7rrk4jb97syvamy7n1vblk5g7zvbrykyfrxm0agvj")))

(define-public crate-tokio-non-async-0.1.2 (c (n "tokio-non-async") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "blocking"))) (d #t) (k 2)))) (h "0h0dkgqd82qyrv7n3qhvp83phdgvzjcqhbz3fv1haxfp9b64ab5s")))

