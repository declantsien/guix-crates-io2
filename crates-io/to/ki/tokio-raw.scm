(define-module (crates-io to ki tokio-raw) #:use-module (crates-io))

(define-public crate-tokio-raw-0.0.1 (c (n "tokio-raw") (v "0.0.1") (h "1zdgmydz3rf2mxnar9dj1v0g87am78rmin7g1p06yrrj4q6pkzkl")))

(define-public crate-tokio-raw-0.0.2 (c (n "tokio-raw") (v "0.0.2") (d (list (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "mio") (r "^0.6.21") (d #t) (k 0)) (d (n "socket2") (r "^0.3.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("io-driver"))) (k 0)))) (h "0i26flk9np75cxfhr26x9dg9dx1427q73sprq499zyx1r4cbnf4l")))

(define-public crate-tokio-raw-0.0.3 (c (n "tokio-raw") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "mio") (r "^0.6.21") (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.20") (f (quote ("io-driver"))) (k 0)) (d (n "tokio") (r "^0.2.20") (f (quote ("macros"))) (d #t) (k 2)))) (h "0wbh2bj5rcf3n637pp7wxp1psvwfcwb5fglnndrg12bq24bi1gnv")))

