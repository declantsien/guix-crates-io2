(define-module (crates-io to ki tokio_telegraf) #:use-module (crates-io))

(define-public crate-tokio_telegraf-0.1.0 (c (n "tokio_telegraf") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio_telegraf_derive") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1gxv78qy0khcpn1c1dvrnd3vgn9xy3jva88w6j6na9c53y7jgss3")))

(define-public crate-tokio_telegraf-0.2.0 (c (n "tokio_telegraf") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio_telegraf_derive") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1pk0rbwbcvx38is9d006k73zzmgwdg3ciwkzwrrb4nx3yb6lbbpl")))

(define-public crate-tokio_telegraf-0.3.0 (c (n "tokio_telegraf") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio_telegraf_derive") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0x4qsm4nc22rki3067j99vjyna9bcsid982j5d3w1qlf30avg9by")))

(define-public crate-tokio_telegraf-0.4.0 (c (n "tokio_telegraf") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio_telegraf_derive") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1pv5yfchnr84q5h5czbpn4smz61k00afn7jz0gd8w8bl2a3xdb7i") (y #t)))

(define-public crate-tokio_telegraf-0.5.0 (c (n "tokio_telegraf") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio_telegraf_derive") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0pw1v8zfy0qx9dp5ycr0wxxag6k43mkg582kagzw58yjrkw7j16k")))

