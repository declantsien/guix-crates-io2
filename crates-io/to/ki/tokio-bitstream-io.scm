(define-module (crates-io to ki tokio-bitstream-io) #:use-module (crates-io))

(define-public crate-tokio-bitstream-io-0.0.1 (c (n "tokio-bitstream-io") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1x1i0iml58gid9c6mjsil4h7b6srz691ch4a3avzc410c9bxbb5a")))

(define-public crate-tokio-bitstream-io-0.0.2 (c (n "tokio-bitstream-io") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kb6virbmya7r1jqnx2cmi1dlvr5hywscls1wlxhfrbby83gs4hq")))

(define-public crate-tokio-bitstream-io-0.0.3 (c (n "tokio-bitstream-io") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zv09c3bmm2b9az8svy7b1rqdn1kymn1wqkxmxpdvw5758vmvyq5")))

(define-public crate-tokio-bitstream-io-0.0.4 (c (n "tokio-bitstream-io") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1lz7rzcsdw3hnc9xdc9x1df200bfrc54w6r42xc4qvh8bflfbcaa")))

(define-public crate-tokio-bitstream-io-0.0.5 (c (n "tokio-bitstream-io") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wg9477kzy6814izlr5dagb581yxwqy27yxqlwz2srr5m1v41rd4")))

(define-public crate-tokio-bitstream-io-0.0.6 (c (n "tokio-bitstream-io") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "08cq81fmaknxzs1symd83p110wl1jni0314jhalpj19wir9jicw4")))

(define-public crate-tokio-bitstream-io-0.0.7 (c (n "tokio-bitstream-io") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1.63") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0rg7irgsnbnbgxk8qbc4cxaa6rnd8kssk5hjvpsq2bvariw9895f")))

