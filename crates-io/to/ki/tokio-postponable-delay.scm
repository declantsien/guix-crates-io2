(define-module (crates-io to ki tokio-postponable-delay) #:use-module (crates-io))

(define-public crate-tokio-postponable-delay-0.1.0 (c (n "tokio-postponable-delay") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("time" "macros" "rt-threaded"))) (d #t) (k 0)))) (h "01yxh8jb5q1hmrs4rplj68qkyvgrl15jxrnpwawd05rf7bvx7vkk")))

