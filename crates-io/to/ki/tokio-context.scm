(define-module (crates-io to ki tokio-context) #:use-module (crates-io))

(define-public crate-tokio-context-0.1.0 (c (n "tokio-context") (v "0.1.0") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "1y122xkcc4df9lp4k1j1wsbpmv3yfjindbm21n8ypjvpvklqb27l")))

(define-public crate-tokio-context-0.1.1 (c (n "tokio-context") (v "0.1.1") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0c5a4njgnm7jsq48yim2gdqj7pgc492pi6qljsxlva75jkab3v8x")))

(define-public crate-tokio-context-0.1.2 (c (n "tokio-context") (v "0.1.2") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0r8np1yr505n7b8f0khl7bp75zabapqiqp9mnzh6wa4kdf5a1w7l")))

(define-public crate-tokio-context-0.1.3 (c (n "tokio-context") (v "0.1.3") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("macros" "sync" "time" "rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0n1rxsnr1v8wdyss8rvlzlnj46ac2n896qkcf8drmjnm9lwviw7a")))

