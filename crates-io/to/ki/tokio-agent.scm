(define-module (crates-io to ki tokio-agent) #:use-module (crates-io))

(define-public crate-tokio-agent-0.1.0 (c (n "tokio-agent") (v "0.1.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zmjh0lbs4cx1vnr0z1ghp9cyqn0hxinjwx5kmjfk1wv418ahz16") (r "1.49")))

