(define-module (crates-io to ki tokio-condvar) #:use-module (crates-io))

(define-public crate-tokio-condvar-0.1.0 (c (n "tokio-condvar") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("sync" "macros" "rt"))) (d #t) (k 0)))) (h "17bg0lrs2mnr90p1x6kw3rivrkkcmny2d0zwz6dyyh252w4kn8yp")))

