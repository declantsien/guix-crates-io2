(define-module (crates-io to ki tokio-linux-zio) #:use-module (crates-io))

(define-public crate-tokio-linux-zio-0.1.0 (c (n "tokio-linux-zio") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "iovec") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "05gq3shjr5qg4rny32yh5gwv8b226p343ay2hpjm4ic2mjj4zr6y")))

(define-public crate-tokio-linux-zio-0.3.0 (c (n "tokio-linux-zio") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0bj53fm9zlfda4z1wybwnr4xrgbi0w2lgazm1354gl26p07qgl7a")))

(define-public crate-tokio-linux-zio-0.3.1 (c (n "tokio-linux-zio") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "165idb9v3h94hkyxwarg0v5mv77xk26nz5642afck3yqdz1kfd7n")))

