(define-module (crates-io to ki tokio-serde-cbor) #:use-module (crates-io))

(define-public crate-tokio-serde-cbor-0.1.0 (c (n "tokio-serde-cbor") (v "0.1.0") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.5") (d #t) (k 0)) (d (n "tokio-io") (r "~0.1") (d #t) (k 0)))) (h "0n2qczyahdp45k4qvwldnfj3ayfdbcgqyz255h73gr11z625dhba")))

(define-public crate-tokio-serde-cbor-0.2.0 (c (n "tokio-serde-cbor") (v "0.2.0") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.6.0-alpha") (d #t) (k 0)) (d (n "tokio-io") (r "~0.1") (d #t) (k 0)))) (h "0vhn72f1kyph2d1gnaxq6fcd17z03i974dnnp4hvgx2pbsix4vd6")))

(define-public crate-tokio-serde-cbor-0.2.1 (c (n "tokio-serde-cbor") (v "0.2.1") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.6.0") (d #t) (k 0)) (d (n "tokio-io") (r "~0.1") (d #t) (k 0)))) (h "10z0fmrc6dsymmqks2rddx0r725y1lbj035h9r0gdac7zri8jl9r")))

(define-public crate-tokio-serde-cbor-0.3.0 (c (n "tokio-serde-cbor") (v "0.3.0") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.9") (d #t) (k 0)) (d (n "tokio-io") (r "~0.1") (d #t) (k 0)))) (h "04dbjp1ddyc3plycqgpph121ahcrlhzs16ss618navhcwnpzkvx8")))

(define-public crate-tokio-serde-cbor-0.3.1 (c (n "tokio-serde-cbor") (v "0.3.1") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.9") (d #t) (k 0)) (d (n "tokio-io") (r "~0.1") (d #t) (k 0)))) (h "1k2mfl8zyp9ryhl6aqmsmm9j2p8mb1d43c1lg42w14c4rhpq1cpb")))

(define-public crate-tokio-serde-cbor-0.3.2 (c (n "tokio-serde-cbor") (v "0.3.2") (d (list (d (n "bytes") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.10") (d #t) (k 0)) (d (n "tokio") (r "~0.1") (d #t) (k 2)) (d (n "tokio-io") (r "~0.1") (d #t) (k 0)))) (h "0wynaxshrs189hrmqrawnkn6az5991mbjyiy1z6g5r5im8wf94sn")))

(define-public crate-tokio-serde-cbor-0.4.0 (c (n "tokio-serde-cbor") (v "0.4.0") (d (list (d (n "bytes") (r "~0.5") (d #t) (k 0)) (d (n "futures") (r "~0.3") (d #t) (k 2)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.11.1") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("dns" "macros" "rt-core" "tcp"))) (d #t) (k 2)) (d (n "tokio-util") (r "~0.2") (f (quote ("codec"))) (d #t) (k 0)))) (h "06xlmw6rr5vrf4sbdj2iacbd81n5aidzk4ml8la15x5w8z3c1ddz")))

(define-public crate-tokio-serde-cbor-0.5.0 (c (n "tokio-serde-cbor") (v "0.5.0") (d (list (d (n "bytes") (r "~0.6") (d #t) (k 0)) (d (n "futures") (r "~0.3") (d #t) (k 2)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.11.1") (d #t) (k 0)) (d (n "tokio") (r "~0.3") (f (quote ("macros" "rt" "net"))) (d #t) (k 2)) (d (n "tokio-util") (r "~0.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "00ab5l92vkd7wysgjb9kg7a623yl0fp7p3209459hn64wl5arjfs")))

(define-public crate-tokio-serde-cbor-0.6.0 (c (n "tokio-serde-cbor") (v "0.6.0") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "futures") (r "~0.3") (d #t) (k 2)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.11.1") (d #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("macros" "rt" "net"))) (d #t) (k 2)) (d (n "tokio-util") (r "~0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "13jnhagc7xn2lwg7jwhpixynbabhfy5s9h64qywhj99f69701j13")))

(define-public crate-tokio-serde-cbor-0.7.0 (c (n "tokio-serde-cbor") (v "0.7.0") (d (list (d (n "bytes") (r "~1") (d #t) (k 0)) (d (n "futures") (r "~0.3") (d #t) (k 2)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_cbor") (r "~0.11.1") (d #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("macros" "rt" "net"))) (d #t) (k 2)) (d (n "tokio-util") (r "~0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "01lma96gbqxqxgg3lkwsdqy4bd6xzly80jrfqrhs88zas4s99aw2")))

