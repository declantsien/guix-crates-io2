(define-module (crates-io to ki tokio-timeit-middleware) #:use-module (crates-io))

(define-public crate-tokio-timeit-middleware-0.1.0 (c (n "tokio-timeit-middleware") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "16kw96j9rbbqdq39q7iz2r2qpbzidwchsah0bl3sh5hr506qp56j")))

