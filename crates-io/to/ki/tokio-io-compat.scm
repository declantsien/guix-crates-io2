(define-module (crates-io to ki tokio-io-compat) #:use-module (crates-io))

(define-public crate-tokio-io-compat-0.1.0 (c (n "tokio-io-compat") (v "0.1.0") (d (list (d (n "tokio") (r "^1.12") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("test-util" "io-util" "macros"))) (d #t) (k 2)))) (h "1rdzs4sggllhxxh0hzy0q8i9hjbm6bs5zv7gg4wm2cr88fb1zyw0")))

(define-public crate-tokio-io-compat-0.1.1 (c (n "tokio-io-compat") (v "0.1.1") (d (list (d (n "tokio") (r "^1.13") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("test-util" "io-util" "macros"))) (d #t) (k 2)))) (h "14xahqhni6kvqlm7kwjggxn1c9vv5lvzvhjk6yzqavw0kyc1a3mh")))

