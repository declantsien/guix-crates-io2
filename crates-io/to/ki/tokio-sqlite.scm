(define-module (crates-io to ki tokio-sqlite) #:use-module (crates-io))

(define-public crate-tokio-sqlite-0.1.0 (c (n "tokio-sqlite") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0x2cwmzkc46a130wqx9d9mz6dc59fyn51pm3dz7zqhqzfb78b0b2")))

(define-public crate-tokio-sqlite-0.1.1 (c (n "tokio-sqlite") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0r71djpninr75m06djr14jijm034w97lagr21ijrparzr77fylsi")))

(define-public crate-tokio-sqlite-0.1.2 (c (n "tokio-sqlite") (v "0.1.2") (d (list (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1z0hnjqmvdqcq4sn0nqcm9fx5pb5a6dvkrp4rfyk5az1ifxj53lw")))

(define-public crate-tokio-sqlite-0.1.3 (c (n "tokio-sqlite") (v "0.1.3") (d (list (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1wkcg3482b08c8zdl3rn7nfnr9073ydvkz540kp6w5jfzl04rfd8")))

(define-public crate-tokio-sqlite-0.1.4 (c (n "tokio-sqlite") (v "0.1.4") (d (list (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)))) (h "0hac4yxi5c0w2xc7z2cnwpj0nd8nxvcy237m6swz28kxikvhd1nl")))

