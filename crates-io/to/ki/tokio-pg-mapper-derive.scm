(define-module (crates-io to ki tokio-pg-mapper-derive) #:use-module (crates-io))

(define-public crate-tokio-pg-mapper-derive-0.1.0 (c (n "tokio-pg-mapper-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "1vlm4s6lms3ybg0713x53pc8l6vikypmgknbk31slig1d4z8h3hg")))

(define-public crate-tokio-pg-mapper-derive-0.1.4 (c (n "tokio-pg-mapper-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "1makr0j4y7rmdq7crf319j4w1q6mjhgkpri4fqk5krd8l448gk7g")))

(define-public crate-tokio-pg-mapper-derive-0.1.5 (c (n "tokio-pg-mapper-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.5.1") (d #t) (k 0)))) (h "0576sjndap1567cljxlnhm252207zaw5jnx781bh77rv7fnlas9v")))

(define-public crate-tokio-pg-mapper-derive-0.1.6 (c (n "tokio-pg-mapper-derive") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "1rdg5kxw5a4ddd4ag5j2kg92i8nlzjppd0d42ppz6bhmxmpchg5n") (y #t)))

(define-public crate-tokio-pg-mapper-derive-0.2.0 (c (n "tokio-pg-mapper-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "0216lqzywrs0ybc30wzvs4mv87q0azn0ryzhbaf0dd3frmbgfj45")))

