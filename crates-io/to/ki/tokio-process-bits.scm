(define-module (crates-io to ki tokio-process-bits) #:use-module (crates-io))

(define-public crate-tokio-process-bits-0.1.0 (c (n "tokio-process-bits") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio-process") (r "^0.1.4") (d #t) (k 0)))) (h "09199xglfa3gmizxpdiz7vyxbjs4mv95ybs4x5zqzifh7z2d7246")))

(define-public crate-tokio-process-bits-0.1.2 (c (n "tokio-process-bits") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio-process") (r "^0.1.4") (d #t) (k 0)))) (h "04zm6a62l7s670g7f9pzrs2dshfv898ir9d4wk06xv2g6r09zrqi")))

(define-public crate-tokio-process-bits-0.1.3 (c (n "tokio-process-bits") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio-process") (r "^0.1.4") (d #t) (k 0)))) (h "0ss28lwdn63s6xfzhiwipk39cqb5b91wf2ja0nzj1037p5w24k7r")))

(define-public crate-tokio-process-bits-0.1.4 (c (n "tokio-process-bits") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio-process") (r "^0.1.4") (d #t) (k 0)))) (h "04p2d9f03az9rsfw53z7gs5da5bimvs8758xdqhk3fr30rrk1in6")))

