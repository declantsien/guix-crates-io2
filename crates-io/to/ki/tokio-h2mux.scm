(define-module (crates-io to ki tokio-h2mux) #:use-module (crates-io))

(define-public crate-tokio-h2mux-0.0.1 (c (n "tokio-h2mux") (v "0.0.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "h2") (r "^0.3.21") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "11g6a8vcy8pdhkm0vgwsmgfdcx000zq5667rda1p8cic1p1cbr9y")))

(define-public crate-tokio-h2mux-0.0.2 (c (n "tokio-h2mux") (v "0.0.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "h2") (r "^0.3.21") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1my066jy5va4790x1x3kpmqfq0ggj3ln2ni0l3lrfzdd79v5z530")))

(define-public crate-tokio-h2mux-0.0.3 (c (n "tokio-h2mux") (v "0.0.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "h2") (r "^0.3.21") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1abs4p6cjwgd7l43pvjv5mnzjkciqhsfzahnyzvsvpyf5hrn5j71")))

