(define-module (crates-io to ki tokio-either) #:use-module (crates-io))

(define-public crate-tokio-either-0.1.0 (c (n "tokio-either") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "0b9zn5xz8ff5b2kz4j0k9yz0mwdrv0b6wz16g4aqmvvc8cj2i977") (y #t)))

(define-public crate-tokio-either-0.1.1 (c (n "tokio-either") (v "0.1.1") (d (list (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "1l5wwnagd4wjbqi6108lh9ak9mjnx1c9hmbw9c0haqnp3b70vdfj") (y #t)))

(define-public crate-tokio-either-0.1.2 (c (n "tokio-either") (v "0.1.2") (d (list (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "1dssfwzkwblkjx4l3mmjy4z7jp23wvk4a4qkvnd2fpf5yklzn8g5") (y #t)))

(define-public crate-tokio-either-0.1.3 (c (n "tokio-either") (v "0.1.3") (d (list (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "0irh2jsdb994p9a73xqikqzp0nphpfhs6hx5liawbf8ndkm4mq0r") (y #t)))

(define-public crate-tokio-either-0.1.4 (c (n "tokio-either") (v "0.1.4") (d (list (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 0)))) (h "12cl2hv2ffiqvcdfa4bpp8wbbkl2wnm5sr0b156vzxd5bpmil34k") (y #t)))

