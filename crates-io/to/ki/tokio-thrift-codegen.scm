(define-module (crates-io to ki tokio-thrift-codegen) #:use-module (crates-io))

(define-public crate-tokio-thrift-codegen-0.1.0 (c (n "tokio-thrift-codegen") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.22") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0mxbl7mjsbm1vrbvly0bq0qv632vi3d71x2y78p6d7r48ddp540r")))

