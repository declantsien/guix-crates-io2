(define-module (crates-io to ki tokio-gpiod) #:use-module (crates-io))

(define-public crate-tokio-gpiod-0.2.0 (c (n "tokio-gpiod") (v "0.2.0") (d (list (d (n "gpiod-core") (r "^0.2.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "net"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wdayzavbva4p7i1qzfcw8y2razm1r3b3cy1bb4vn8h6skck9arg") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap")))) (y #t)))

(define-public crate-tokio-gpiod-0.2.1 (c (n "tokio-gpiod") (v "0.2.1") (d (list (d (n "gpiod-core") (r "^0.2.1") (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "net"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1j8h7m7vxifh8k3sbiy038nfi8sbvyv3wpjfjxj684qss8v1d5yr") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-tokio-gpiod-0.2.2 (c (n "tokio-gpiod") (v "0.2.2") (d (list (d (n "gpiod-core") (r "^0.2.2") (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "net"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0ll6p64gxvjb0dzmbvrjg1n91lrwj9mgla83b05dzl08l1m9p5lq") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-tokio-gpiod-0.2.3 (c (n "tokio-gpiod") (v "0.2.3") (d (list (d (n "gpiod-core") (r "^0.2.3") (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "net"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "000nnn548h42i1gwn0q1cc2aqyh181n97j7y2mxlcvmmqzwcp5cz") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-tokio-gpiod-0.3.0 (c (n "tokio-gpiod") (v "0.3.0") (d (list (d (n "gpiod-core") (r "^0.3") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "net"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)))) (h "0ivzqpjsqdrv2czrd16naf5hljh4nnp74vshpv9arb57440gl5ff") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

