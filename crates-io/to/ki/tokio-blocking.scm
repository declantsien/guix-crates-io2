(define-module (crates-io to ki tokio-blocking) #:use-module (crates-io))

(define-public crate-tokio-blocking-0.1.0 (c (n "tokio-blocking") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.6") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1j7crjp2j985pq56cknkbgw0gilnrv69d05cncc2falv6xhw9d4h")))

(define-public crate-tokio-blocking-0.1.1 (c (n "tokio-blocking") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.6") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15v0yjq4p7k8128972mim897piy7hwqh8fzsi16p57nch2g4955x")))

