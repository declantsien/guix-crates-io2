(define-module (crates-io to ki tokio-tongsuo) #:use-module (crates-io))

(define-public crate-tokio-tongsuo-0.6.3 (c (n "tokio-tongsuo") (v "0.6.3") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0) (p "tongsuo")) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0) (p "tongsuo-sys")) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1v71ss3y80jd91gq3vb6vwxdp884mhf3b4chrc4wqajjd09damqn")))

(define-public crate-tokio-tongsuo-0.6.4 (c (n "tokio-tongsuo") (v "0.6.4") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10.32") (k 0) (p "tongsuo")) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0) (p "tongsuo-sys")) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0pzdyfhll59wyb228mrvd01xdj18rwgby8c8mln4lns11g0h451m")))

(define-public crate-tokio-tongsuo-0.6.5 (c (n "tokio-tongsuo") (v "0.6.5") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10.61") (k 0) (p "tongsuo")) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0) (p "tongsuo-sys")) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0xj7sdf0z83i5g77wvqxdlhzcd1wdp7sw2zyjxhd1pxj0g58d91v")))

