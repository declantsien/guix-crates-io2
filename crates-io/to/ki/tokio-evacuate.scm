(define-module (crates-io to ki tokio-evacuate) #:use-module (crates-io))

(define-public crate-tokio-evacuate-1.0.0 (c (n "tokio-evacuate") (v "1.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "1v227jn46z4zwwlbbhsp3ppdavq489fhsz6lxhn817gfil814c2q")))

(define-public crate-tokio-evacuate-1.1.0 (c (n "tokio-evacuate") (v "1.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "07icqjqyq901qi5q1kwc8r4w1w92750yzy388i33z4ckgmgr5dzs")))

(define-public crate-tokio-evacuate-1.1.1 (c (n "tokio-evacuate") (v "1.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "1n4hq84kh2q6hv8ji3gbfw4qf6dvk6ihprqj6swv26cva4pwpcl4")))

(define-public crate-tokio-evacuate-2.0.0 (c (n "tokio-evacuate") (v "2.0.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (f (quote ("reactor" "timer" "tokio-current-thread"))) (d #t) (k 2)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "tokio-sync") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "0a8jhbjslg5ncwvar1zbdwpl2mx8hix5pmd25n6xxh10v1zbl94r")))

