(define-module (crates-io to ki tokio-postgres-openssl) #:use-module (crates-io))

(define-public crate-tokio-postgres-openssl-0.1.0-rc.1 (c (n "tokio-postgres-openssl") (v "0.1.0-rc.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-openssl") (r "^0.3") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.4.0-rc.1") (k 0)))) (h "1vhapfwyy1iajfmbs4j68lm3gm9wzbmsibw9zpq6dk5gcg8pgk65") (f (quote (("runtime" "tokio-postgres/runtime") ("default" "runtime"))))))

