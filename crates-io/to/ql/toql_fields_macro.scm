(define-module (crates-io to ql toql_fields_macro) #:use-module (crates-io))

(define-public crate-toql_fields_macro-0.3.0 (c (n "toql_fields_macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "toql_field_list_parser") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1glhwmgylrn6zns45qvgrs2w5lq6gzl48gqfn85cpgc5lhzdrih5")))

(define-public crate-toql_fields_macro-0.4.0 (c (n "toql_fields_macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "toql_field_list_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1jr3pj3k5a5h2wlawbr5lki22g1235xy561c6c418psql20rqvxn")))

