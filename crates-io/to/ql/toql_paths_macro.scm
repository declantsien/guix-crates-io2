(define-module (crates-io to ql toql_paths_macro) #:use-module (crates-io))

(define-public crate-toql_paths_macro-0.3.0 (c (n "toql_paths_macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "toql_field_list_parser") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0dmrbm4v6x9i09kcgylfmf3x993wrxiq53vmhngd9vvgva37vv1y")))

(define-public crate-toql_paths_macro-0.4.0 (c (n "toql_paths_macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "toql_field_list_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0c5ni2k7jfc5d4ld31l89g11cqs52j0vhha5ianf9akpns4mw146")))

