(define-module (crates-io to ql toql_mysql) #:use-module (crates-io))

(define-public crate-toql_mysql-0.1.0 (c (n "toql_mysql") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^14.2") (d #t) (k 0)) (d (n "toql_core") (r "^0.1.0") (f (quote ("mysqldb"))) (d #t) (k 0)))) (h "0idchkanwbz935wsg5hwlcph8yg7dpkl43r3wmgbs861kpzd0pkd")))

(define-public crate-toql_mysql-0.1.1 (c (n "toql_mysql") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^14") (d #t) (k 0)) (d (n "toql_core") (r "^0.1") (f (quote ("mysqldb"))) (d #t) (k 0)))) (h "0a94hapc29hh52z7bk2mqnhaan6cr3zyjhvn4rpi94pkizwaxq5p")))

(define-public crate-toql_mysql-0.1.2 (c (n "toql_mysql") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^14") (d #t) (k 0)) (d (n "toql_core") (r "^0.1") (f (quote ("mysqldb"))) (d #t) (k 0)))) (h "1qjnr197dw61biv1qxjqck1j08f5v1w769j2njfm0fy80qx9qaql")))

