(define-module (crates-io to ql toql_mysql_async) #:use-module (crates-io))

(define-public crate-toql_mysql_async-0.4.1 (c (n "toql_mysql_async") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "mysql_async") (r "^0.27") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "toql") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06lwj7firsa41qbhwga6rix5ag9fzsh05rahxb3m5271bp9q8y3v")))

(define-public crate-toql_mysql_async-0.4.2 (c (n "toql_mysql_async") (v "0.4.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "mysql_async") (r "^0.29") (d #t) (k 0)) (d (n "mysql_common") (r "^0.28") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "toql") (r "^0.4.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1z0r3aj6rw40cbrvfyrs2c633q18sqp0cdsjgq3z2gh3qdcdhplh")))

