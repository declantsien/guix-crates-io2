(define-module (crates-io to ql toql_field_list_parser) #:use-module (crates-io))

(define-public crate-toql_field_list_parser-0.3.0 (c (n "toql_field_list_parser") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0i4v2s5dr8bdlvdlcv8hviqmayngwrikdxcjimqg8cvv0nw5djn6")))

(define-public crate-toql_field_list_parser-0.4.0 (c (n "toql_field_list_parser") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1xbiy3piy5sypz9mhvspkjpjfiw5hndwg4hxn9mhl0ivpcgrchlj")))

