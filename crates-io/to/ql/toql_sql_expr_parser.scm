(define-module (crates-io to ql toql_sql_expr_parser) #:use-module (crates-io))

(define-public crate-toql_sql_expr_parser-0.3.0 (c (n "toql_sql_expr_parser") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0ij5p4042i52nc5d7m7l44yvxvfxgw83nr3ic6iwngldr1cw4ykz")))

(define-public crate-toql_sql_expr_parser-0.4.0 (c (n "toql_sql_expr_parser") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "04inn8kwcwr97b1mnxgc213jar2r2iddamk40gkhz84y21jx25iy")))

