(define-module (crates-io to ql toql_query_parser) #:use-module (crates-io))

(define-public crate-toql_query_parser-0.3.0 (c (n "toql_query_parser") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0pf7ki2pd00k43wy3r6fxxyy2sv9z3dqi4jmyabxmwrnj2w1h29a")))

(define-public crate-toql_query_parser-0.3.1 (c (n "toql_query_parser") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0avs0hk3dfksyy52s9kq6zqbwqrik8gjl5myp2fnw0dzxwzw61xn")))

(define-public crate-toql_query_parser-0.4.0 (c (n "toql_query_parser") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1s12cidr8axw09jka6g83cvh1l6cg647z3bivgry406x1g9hc8cq")))

