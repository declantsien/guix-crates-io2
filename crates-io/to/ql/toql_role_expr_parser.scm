(define-module (crates-io to ql toql_role_expr_parser) #:use-module (crates-io))

(define-public crate-toql_role_expr_parser-0.3.0 (c (n "toql_role_expr_parser") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "12bbsczsaz8xlrlswmwaryiirf7c1g0kjv844rilj2wbgdrynwh2")))

(define-public crate-toql_role_expr_parser-0.4.0 (c (n "toql_role_expr_parser") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "152nsmbq43jg56n0kq8f4szd68rdgiq7qdphaf9y5sm7r70mlf7x")))

