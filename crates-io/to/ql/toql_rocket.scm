(define-module (crates-io to ql toql_rocket) #:use-module (crates-io))

(define-public crate-toql_rocket-0.1.0 (c (n "toql_rocket") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^14.2") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "toql_core") (r "^0.1") (f (quote ("rocketweb"))) (d #t) (k 0)) (d (n "toql_mysql") (r "^0.1") (d #t) (k 0)))) (h "1gsx27npqj5glmqllsrrm4v8ajafh1ziz8h728r8iy6scbcy8acd") (f (quote (("mysqldb" "mysql"))))))

(define-public crate-toql_rocket-0.1.1 (c (n "toql_rocket") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^14.2") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "toql_core") (r "^0.1") (f (quote ("rocketweb"))) (d #t) (k 0)) (d (n "toql_mysql") (r "^0.1") (d #t) (k 0)))) (h "0p1hshax5fc2cxy9p599n30nlzpb0b77mvcssz67vys9x6x326bw") (f (quote (("mysqldb" "mysql"))))))

(define-public crate-toql_rocket-0.1.2 (c (n "toql_rocket") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mysql") (r "^14") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "toql_core") (r "^0.1") (f (quote ("rocketweb"))) (d #t) (k 0)) (d (n "toql_mysql") (r "^0.1") (d #t) (k 0)))) (h "05g0gzp0wdj9pv48xry2r0dcdcvdrdnrayldd22vi5anmxkvwkzc") (f (quote (("mysqldb" "mysql"))))))

(define-public crate-toql_rocket-0.1.3 (c (n "toql_rocket") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "rocket_contrib") (r "^0.4") (f (quote ("mysql_pool" "json"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "toql") (r "^0.1") (d #t) (k 0)))) (h "1qx1zn21gblcl1iy1zcid3dh4bd6cz71h65j1z3ailc6shjh899g") (f (quote (("mysql" "toql/mysql"))))))

(define-public crate-toql_rocket-0.1.4 (c (n "toql_rocket") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "rocket_contrib") (r "^0.4") (f (quote ("mysql_pool" "json"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "toql") (r "^0.1") (d #t) (k 0)))) (h "1p44y1xlbk4bg9zn5826q2pck5mb2f7rip4pkqckpan1nzv38vnm") (f (quote (("mysql" "toql/mysql"))))))

(define-public crate-toql_rocket-0.1.5 (c (n "toql_rocket") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 2)) (d (n "rocket_contrib") (r "^0.4") (f (quote ("mysql_pool" "json"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "toql") (r "^0.1") (d #t) (k 0)))) (h "0c4alwvfj6naxpx75g5kvj7j154ipyxd8wqvfapwhcy4biw399vj") (f (quote (("mysql" "toql/mysql"))))))

(define-public crate-toql_rocket-0.3.0 (c (n "toql_rocket") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "toql") (r "^0.3") (d #t) (k 0)))) (h "0cv06rqc0gbivzffi3gq8d64bhs8j9fh7513w3gwqnfan86jp2kw")))

