(define-module (crates-io to ql toql_sql_expr_macro) #:use-module (crates-io))

(define-public crate-toql_sql_expr_macro-0.3.0 (c (n "toql_sql_expr_macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "toql_sql_expr_parser") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1h4r32fawj102ihkiiwvi8b1sy3iihzzllf4b3ph9qbi5wi8l2pr")))

(define-public crate-toql_sql_expr_macro-0.4.0 (c (n "toql_sql_expr_macro") (v "0.4.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "toql_sql_expr_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0h75jgp0yg5068cnh5dfr65pxd35q0mrx1byqsdscs06dmgbpakv")))

