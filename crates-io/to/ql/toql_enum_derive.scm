(define-module (crates-io to ql toql_enum_derive) #:use-module (crates-io))

(define-public crate-toql_enum_derive-0.4.0 (c (n "toql_enum_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "levenshtein") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "020vgvp5mj7g5lx9r7g9mnql2l7g60hjfb9qhqk794r93ml2b9v6") (f (quote (("serde"))))))

