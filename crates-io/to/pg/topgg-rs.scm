(define-module (crates-io to pg topgg-rs) #:use-module (crates-io))

(define-public crate-topgg-rs-0.1.0 (c (n "topgg-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gmcq7dvmag4pml0jsj1z33cn364khf1aqc06hpwjg741hhg22j4")))

(define-public crate-topgg-rs-0.2.0 (c (n "topgg-rs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "003y88mgdd7l2szvc28sycdm676rj8g69dd210np21drdc48h0ib")))

(define-public crate-topgg-rs-0.2.1 (c (n "topgg-rs") (v "0.2.1") (d (list (d (n "governor") (r "^0.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v62yvvr5qjmyjla36xg4ah5jaj9gklls8v8ssbkahlnjqb1lwca")))

(define-public crate-topgg-rs-0.3.0 (c (n "topgg-rs") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.12") (d #t) (k 0)) (d (n "governor") (r "^0.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "0y7dr3gwn5ypc604jhpvdsh6k7r608giir3kk5h9lx49s0mk2c2y")))

