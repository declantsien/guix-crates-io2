(define-module (crates-io to bo toboggan-lib) #:use-module (crates-io))

(define-public crate-toboggan-lib-0.1.0 (c (n "toboggan-lib") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "tarpc") (r "^0.30.0") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "0i8kjay77fw5x7mzjyyhvr1hpwcqg2451kbp23i35g3s552kwjaw") (y #t)))

