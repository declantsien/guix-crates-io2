(define-module (crates-io to bo toboggan-kv) #:use-module (crates-io))

(define-public crate-toboggan-kv-0.1.0 (c (n "toboggan-kv") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (t "cfg(not(any(target_arch = \"wasm32\")))") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0yy4jy9frxp9d06sn8rgf39sir9n1ndw2cq77cw9wf164b8pblwb")))

(define-public crate-toboggan-kv-0.1.1 (c (n "toboggan-kv") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (t "cfg(not(any(target_arch = \"wasm32\")))") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1v63kc46xiv5mczy340pn7cw54mq3p615n8nv2n9p4ns4qrmvd8i")))

(define-public crate-toboggan-kv-0.1.2 (c (n "toboggan-kv") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (t "cfg(not(any(target_arch = \"wasm32\")))") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "00biy3yhkx38yqf9bkk19h0nkgh2742d2k3haw3kzjzaf5l9sq0m")))

