(define-module (crates-io to rd tordir) #:use-module (crates-io))

(define-public crate-tordir-0.1.0 (c (n "tordir") (v "0.1.0") (h "05f7c8rg3qv0b9a2m1z17fd0qhdm1wxyvn60rmricskk1gcxs4rh")))

(define-public crate-tordir-0.1.1 (c (n "tordir") (v "0.1.1") (h "0hkgw7ahr4cgz6cv67wxz6ff8jkdd6ijhkvp9mwds63wqga85a76")))

(define-public crate-tordir-0.1.2 (c (n "tordir") (v "0.1.2") (h "1z3hmp2ijawls9rfg9hsm44x6ywkxc7y5qxdr6zlgfmq2w8pvdnh")))

(define-public crate-tordir-0.1.3 (c (n "tordir") (v "0.1.3") (h "128cbl0m0wqlwysa0ydhyqs88f75bcivl2djrwhw61jxmwrcyb62")))

