(define-module (crates-io to rd tord) #:use-module (crates-io))

(define-public crate-tord-0.1.0 (c (n "tord") (v "0.1.0") (h "0cagl8qrcadg8p6hxkvv8bg90ncvw8ca8nbaijn2k9hh7zxbpymg")))

(define-public crate-tord-0.1.1 (c (n "tord") (v "0.1.1") (h "0d6vp3l7jhbkry824nrz0y9s8bkkdsk5p3p5b6s48n3dfn8nsbg5")))

(define-public crate-tord-0.1.2 (c (n "tord") (v "0.1.2") (h "1x1nng9fn1hvl0jzh95x20xjv1yz9s7achn79xvx14pajnkk4lm5")))

(define-public crate-tord-0.1.3 (c (n "tord") (v "0.1.3") (h "1m3iqfml54p4shsbvjzq198bx7pr1mqzqi5wx7qix02id6cgnc8k")))

(define-public crate-tord-0.1.4 (c (n "tord") (v "0.1.4") (h "0sr6js5l5bhj01w7vv2ph8gwcc5mf7n7rf1l6qi146cxw4g1qr40")))

(define-public crate-tord-0.1.5 (c (n "tord") (v "0.1.5") (h "0q1bzbmp3c4g99qvgm52y461ncb3ygndx2dj9br8ll6vhabhh5vs")))

(define-public crate-tord-0.1.6 (c (n "tord") (v "0.1.6") (h "0xm3p73nar9ips7r68jmqhvz7m4lgspifpf82vlwzlava2zdg9q0")))

(define-public crate-tord-0.1.7 (c (n "tord") (v "0.1.7") (h "1shd1d9bisqn6m4bxbcb0kgc5q6r95hvblv6c7x2vfhcnp7znjy4")))

