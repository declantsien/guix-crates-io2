(define-module (crates-io to na tonality) #:use-module (crates-io))

(define-public crate-tonality-0.0.1 (c (n "tonality") (v "0.0.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zg5g7774w3wa1c1f150jz50v28kpk76vb7260858hci7q9y6yn7")))

(define-public crate-tonality-0.0.2 (c (n "tonality") (v "0.0.2") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ga8wj86il8hwv8i6zhlkaw6yclys2x03lsfwc3zyaqn7nsccvqq")))

(define-public crate-tonality-0.0.3 (c (n "tonality") (v "0.0.3") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0lxyw68rwj0a2zlv72d9v4qnnldahax3lflm2l63jxg3nnni23p2")))

(define-public crate-tonality-0.1.0 (c (n "tonality") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "138w2hpnz1ngv29x3gl888ny0fsrm82az2agg1x73vbwymrlzxaf")))

(define-public crate-tonality-0.1.1 (c (n "tonality") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.6") (d #t) (k 2)))) (h "1n77fyf78gwp43dxcj64mm0h8imlhp50f79i63236var1pfhgs48")))

