(define-module (crates-io to na tonal) #:use-module (crates-io))

(define-public crate-tonal-0.1.0 (c (n "tonal") (v "0.1.0") (h "02bxw49kz19v4x53dddqqn4x32mbdx921985cg95cx1391nls548")))

(define-public crate-tonal-0.1.1 (c (n "tonal") (v "0.1.1") (h "163qar1mffwm8sa2lr39dgrdkgrhfq4a84dnnkf9wm9hf8pc280d")))

(define-public crate-tonal-0.1.2 (c (n "tonal") (v "0.1.2") (h "0wn4mdqywnq8pr08f1cbfv71l4y44ribvgbvp6mck53hx26am8r7")))

(define-public crate-tonal-0.1.3 (c (n "tonal") (v "0.1.3") (h "1x52fp00d8rxim3j4rcbp5nnpxg81ix9bzszylrfz9jik2gcag1b")))

