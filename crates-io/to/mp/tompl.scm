(define-module (crates-io to mp tompl) #:use-module (crates-io))

(define-public crate-tompl-0.0.1 (c (n "tompl") (v "0.0.1") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0qay8mfd6fwni6ziqryaydyyqr3fplwpavzkpydkb6sgrzx507j4") (y #t)))

