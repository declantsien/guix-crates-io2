(define-module (crates-io to po topojson) #:use-module (crates-io))

(define-public crate-topojson-0.1.0 (c (n "topojson") (v "0.1.0") (d (list (d (n "geojson") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1g1lc8i1wxq08xzmndys1np11yp951nr3gcqin7sx7ha2c2r7l8f")))

(define-public crate-topojson-0.2.0 (c (n "topojson") (v "0.2.0") (d (list (d (n "geojson") (r "^0.16.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "01psc8jp3l4f2172facpic0f178ywhxg28gj8j0ikcfjs1k68hi5")))

(define-public crate-topojson-0.3.0 (c (n "topojson") (v "0.3.0") (d (list (d (n "geojson") (r ">=0.16.0, <0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1vc63v4g8g29mlxxgd41yrqfnv1jd8s1p674qcd0xlm7b3cnpzra")))

(define-public crate-topojson-0.4.0 (c (n "topojson") (v "0.4.0") (d (list (d (n "geojson") (r ">=0.16.0, <0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1s3fb0681v78kdfpfdp74yiljf33n3zgms1l4yqbxswjkxr1ibqi")))

(define-public crate-topojson-0.5.0 (c (n "topojson") (v "0.5.0") (d (list (d (n "geojson") (r ">=0.16.0, <0.23.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1sslp2ah78vppfjhfj8fp1ssbfx22lypdm49vbah8c44n2ghz7lx")))

(define-public crate-topojson-0.5.1 (c (n "topojson") (v "0.5.1") (d (list (d (n "geojson") (r ">=0.16.0, <0.24.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0wrbchn89m7q1znaskvrynv6cxp2r4v6ga2xkba0398qr68h9va1")))

