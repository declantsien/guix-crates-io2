(define-module (crates-io to po topology-traits) #:use-module (crates-io))

(define-public crate-topology-traits-0.1.0 (c (n "topology-traits") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1imr30n8xdncvg6lyw5jmkxgdpmp3zv9gsm5f42n4m5drcy9kf1i") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std")))) (y #t)))

(define-public crate-topology-traits-0.1.1 (c (n "topology-traits") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1v2jj854k8x3qw1l1kjjvvgdsd8vrf3hjfw8aaz58pd1q1m0fd3g") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-topology-traits-0.1.2 (c (n "topology-traits") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0cj03w5h0yp3mfrpry5llyj5clmmj8q6xz9vbl8k07jk52sdmj60") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

