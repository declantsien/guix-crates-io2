(define-module (crates-io to po topograph) #:use-module (crates-io))

(define-public crate-topograph-0.0.0 (c (n "topograph") (v "0.0.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "06ym217sqiz6zaczn2zpz7n0fz48jpdazqybff85zliz2s8mbrrw")))

(define-public crate-topograph-0.1.0 (c (n "topograph") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0rnj8i4qqmwc2awpfrwbl79cl08wrn6437g9vffnkpy8qk9vi67r")))

(define-public crate-topograph-0.2.0-alpha.1 (c (n "topograph") (v "0.2.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "12zbgxd808km3h0gymvxjm3mszzmciifwid5hqmr5qm1xk4piwjx")))

(define-public crate-topograph-0.2.1-alpha.1 (c (n "topograph") (v "0.2.1-alpha.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0g0696ly1p0gzibs7pwc262205knmylb31hvlwsx8bx5sfmd4fkz")))

(define-public crate-topograph-0.3.0-alpha.1 (c (n "topograph") (v "0.3.0-alpha.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zapzv6y0m2k1y0ypv5ihqijj4cf7vwwpkpbj5l53iardddb12m2")))

(define-public crate-topograph-0.3.1-alpha.1 (c (n "topograph") (v "0.3.1-alpha.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "dispose") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0j3mncv8yalklvq56kd2xg3zr7mx15k0s8g8zm7bqgkb8zhdkwc9")))

