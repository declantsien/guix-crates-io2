(define-module (crates-io to po topo-macro) #:use-module (crates-io))

(define-public crate-topo-macro-0.8.0 (c (n "topo-macro") (v "0.8.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "07zxb2jn0cg57siqpgpx9cw8g1dmijqwvjj3cy1x2yns73xp8hym")))

(define-public crate-topo-macro-0.8.1 (c (n "topo-macro") (v "0.8.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1dzk3agaryq6ilfmviidbqaa75slffn81khh34fbsplnsnl69k61")))

(define-public crate-topo-macro-0.8.2 (c (n "topo-macro") (v "0.8.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00rld7hahylmi6gn1jr9z7kz2qk6yzk08rhv94l5hw3b019ga7fb")))

(define-public crate-topo-macro-0.9.0 (c (n "topo-macro") (v "0.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0symm6fmqkpb9r1k6q3pngx4imaq6d5lgg8w7kskr6wwpaqkcb61")))

(define-public crate-topo-macro-0.10.0 (c (n "topo-macro") (v "0.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wmq6286l61j2k6vhkmyq12wk7rkf70jidmxjc505s4bhdawn85l")))

