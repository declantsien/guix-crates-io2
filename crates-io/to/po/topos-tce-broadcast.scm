(define-module (crates-io to po topos-tce-broadcast) #:use-module (crates-io))

(define-public crate-topos-tce-broadcast-0.1.0 (c (n "topos-tce-broadcast") (v "0.1.0") (d (list (d (n "tce_transport") (r "^0.1.0") (d #t) (k 0) (p "topos-tce-transport")) (d (n "topos-core") (r "^0.1.0") (k 0)) (d (n "topos-p2p") (r "^0.1.0") (d #t) (k 0)))) (h "1dwfg3nwffl9i38w655ix0d9k0bsjbxiwaagjx1rzv8qzs0dc858")))

