(define-module (crates-io to po topos-tce-gatekeeper) #:use-module (crates-io))

(define-public crate-topos-tce-gatekeeper-0.1.0 (c (n "topos-tce-gatekeeper") (v "0.1.0") (d (list (d (n "topos-commands") (r "^0.1.0") (d #t) (k 0)) (d (n "topos-core") (r "^0.1.0") (d #t) (k 0)) (d (n "topos-p2p") (r "^0.1.0") (d #t) (k 0)))) (h "0r8a6amrrwiafcqc941zw4g32wdgyg8312gyjld8n55pjm7h2zfz")))

