(define-module (crates-io to po topological_peak_detection) #:use-module (crates-io))

(define-public crate-topological_peak_detection-0.1.0 (c (n "topological_peak_detection") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "100i25grly5ydx4s02q099qlxr31xymfib8s9jnvsw5akg9lyfq8")))

(define-public crate-topological_peak_detection-0.2.0 (c (n "topological_peak_detection") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0d9f1xx2kwn13raz5pjzby1qp6vss0qsfcrl1j6bz0vph4zw0w68")))

