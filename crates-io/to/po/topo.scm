(define-module (crates-io to po topo) #:use-module (crates-io))

(define-public crate-topo-0.1.0 (c (n "topo") (v "0.1.0") (h "1h19ldbx6q2jyafgwb9hz2c7rjqd83qzldksyqnlfk1s2qg1dryr")))

(define-public crate-topo-0.8.0 (c (n "topo") (v "0.8.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "topo-macro") (r "^0.8.0") (d #t) (k 0)))) (h "1al0pzppnfwzfnwsyzqfaypz2walvnys5mgl2xadlpwdirj1bxpw")))

(define-public crate-topo-0.8.1 (c (n "topo") (v "0.8.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "topo-macro") (r "= 0.8.1") (d #t) (k 0)))) (h "143adryin843cizm01lrs7sjqxm35fpr5j7ip79idp0hl7c38z1m")))

(define-public crate-topo-0.8.2 (c (n "topo") (v "0.8.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "owning_ref") (r "^0.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)) (d (n "topo-macro") (r "^0.8.1") (d #t) (k 0)))) (h "0v84m8fqa6w1rwhnpvwszg97ip65yh3hbzm8i7czg5mkl22nfi34")))

(define-public crate-topo-0.9.0 (c (n "topo") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit") (r "^0.9.0") (d #t) (k 0)) (d (n "topo-macro") (r "^0.8.2") (d #t) (k 0)))) (h "15l1mj2kcv30vgraalj2g62i21fqs2h17i7rfic67gbz65ahx144")))

(define-public crate-topo-0.9.1 (c (n "topo") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit") (r "^0.9.0") (d #t) (k 0)) (d (n "topo-macro") (r "^0.8.2") (d #t) (k 0)))) (h "0y2jd2ypdijx0pjlg9dh9lsxx1qbh4s5pcjdivlm0mr6jyl458nv")))

(define-public crate-topo-0.9.2 (c (n "topo") (v "0.9.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "illicit") (r "^0.9.0") (d #t) (k 0)) (d (n "topo-macro") (r "^0.8.2") (d #t) (k 0)))) (h "16rznkfp4zq7m3k0svf3hadar3s0spasgpx93sl3rgzx4jbc9j4s")))

(define-public crate-topo-0.9.3 (c (n "topo") (v "0.9.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit") (r "^0.9.2") (d #t) (k 0)) (d (n "topo-macro") (r "^0.8.2") (d #t) (k 0)))) (h "0di3l7d1dsa6clapyhrrvkh34bh5w5z15j2czh95g3ac0qwnfbwk")))

(define-public crate-topo-0.9.4 (c (n "topo") (v "0.9.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "illicit") (r "^0.9.2") (d #t) (k 0)) (d (n "topo-macro") (r "^0.9.0") (d #t) (k 0)))) (h "0hzjvhwvy8v9hkjknqzs8n12a2rqk6zykw5y5m9d2rnmazrixmcj")))

(define-public crate-topo-0.10.0 (c (n "topo") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "illicit") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.18") (d #t) (k 0)) (d (n "topo-macro") (r "^0.10.0") (d #t) (k 0)))) (h "0k70xm9fhmix4vcxj1cqx9mfy3m4b38zqqfkxy61axddb1pz2mv8")))

(define-public crate-topo-0.11.0 (c (n "topo") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.8.0") (d #t) (k 0)) (d (n "illicit") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.18") (d #t) (k 0)) (d (n "topo-macro") (r "^0.10.0") (d #t) (k 0)))) (h "15f47170dmpdlrv3wb197amx2mil5gm51mxf5h69fxq3rfsgxzb1")))

(define-public crate-topo-0.12.0 (c (n "topo") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "hash_hasher") (r "^2.0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.8.0") (d #t) (k 0)) (d (n "illicit") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.18") (d #t) (k 0)) (d (n "topo-macro") (r "^0.10.0") (d #t) (k 0)))) (h "1yi33xdpqk080j3l6w7pqw0ihrnjf54abi8d1blfwkfny7wiqzdk")))

(define-public crate-topo-0.13.0 (c (n "topo") (v "0.13.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dyn-cache") (r "^0.10.0") (d #t) (k 0)) (d (n "illicit") (r "^1.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "topo-macro") (r "^0.10.0") (d #t) (k 0)))) (h "1s9jmv6vwb20fh68gyw1cf4v79wd7l85q1ji6071221m89ivx2sp")))

(define-public crate-topo-0.13.1 (c (n "topo") (v "0.13.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dyn-cache") (r "^0.12.1") (d #t) (k 0)) (d (n "illicit") (r "^1.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "topo-macro") (r "^0.10.0") (d #t) (k 0)))) (h "15f9ka455x5vsid6zgbip0q8zwb6i35jbd9kbn17i4bsym5kmsqd") (f (quote (("wasm-bindgen" "dyn-cache/wasm-bindgen" "parking_lot/wasm-bindgen") ("default"))))))

(define-public crate-topo-0.13.2 (c (n "topo") (v "0.13.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dyn-cache") (r "^0.12.1") (d #t) (k 0)) (d (n "illicit") (r "^1.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "topo-macro") (r "^0.10.0") (d #t) (k 0)))) (h "1yamyc247kigvmd42v4rmjlahr105cknlfd88kpjxhgn8mh3dcmk") (f (quote (("wasm-bindgen" "dyn-cache/wasm-bindgen" "parking_lot/wasm-bindgen") ("default"))))))

