(define-module (crates-io to po topopt) #:use-module (crates-io))

(define-public crate-topopt-0.1.0 (c (n "topopt") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)))) (h "0h0bpcb02zxissc0g6i0jghs98zdyd3shpv0dabxg3fhlygglqzq")))

(define-public crate-topopt-0.1.1 (c (n "topopt") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)))) (h "07hgdwrxwff11d6c0binnwh6ar4p3fiqzy8y4qjhia5mjg96853r")))

(define-public crate-topopt-0.1.2 (c (n "topopt") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.6.0") (d #t) (k 0)))) (h "0plhnnqbsprjc1fhss091vjlzpwdkxpqjpzii8vsf7kd537h0izk")))

(define-public crate-topopt-0.1.3 (c (n "topopt") (v "0.1.3") (d (list (d (n "mocktave") (r "^0.1.3") (d #t) (k 2)) (d (n "nalgebra") (r ">=0.30.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "nalgebra-sparse") (r ">=0.9.0") (d #t) (k 0)))) (h "04mklkz8bpnnf2m2zlfjgpvp7kibgra4nvfw9cs9bqr3a6bbn0df")))

(define-public crate-topopt-0.1.4 (c (n "topopt") (v "0.1.4") (d (list (d (n "mocktave") (r "^0.1.3") (d #t) (k 2)) (d (n "nalgebra") (r ">=0.30.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "nalgebra-sparse") (r ">=0.9.0") (d #t) (k 0)))) (h "0mhringdna53481zyx4wiln13jw9vk8kqlk2g3zqcsryvh6j0lws")))

(define-public crate-topopt-0.1.5 (c (n "topopt") (v "0.1.5") (d (list (d (n "mocktave") (r "^0.1.4") (d #t) (k 2)) (d (n "nalgebra") (r ">=0.30.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "nalgebra-sparse") (r ">=0.9.0") (d #t) (k 0)))) (h "0q7gpv6dy5f2gvcw8pf040w6ag9sjhfkkfxsfnlv3z7lf69k4nkl")))

