(define-module (crates-io to zn tozny_auth) #:use-module (crates-io))

(define-public crate-tozny_auth-1.1.0 (c (n "tozny_auth") (v "1.1.0") (d (list (d (n "chrono") (r "~0.2.1") (d #t) (k 0)) (d (n "hyper") (r "~0.3.0") (d #t) (k 0)) (d (n "rand") (r "~0.2.1") (d #t) (k 0)) (d (n "rust-crypto") (r "~0.2.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.1") (d #t) (k 0)) (d (n "url") (r "~0.2.18") (d #t) (k 0)))) (h "1q7xmw4h4hlihz8qcwfgphyn9qi9k7w9g9yfwh5prjzw7ipay2kp")))

