(define-module (crates-io to _m to_method) #:use-module (crates-io))

(define-public crate-to_method-1.0.0 (c (n "to_method") (v "1.0.0") (h "1mwyknl4bwdbd0lgjhswzahh060z6cy43jb0npf0i6rw8yrr0p91")))

(define-public crate-to_method-1.1.0 (c (n "to_method") (v "1.1.0") (h "1s72l06fnb5kv6vm5ds0lilg1dyciyyis09ypi5kij0mrbpcxi67")))

