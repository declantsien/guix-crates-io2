(define-module (crates-io to is toison) #:use-module (crates-io))

(define-public crate-toison-1.0.0 (c (n "toison") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "human_format") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "thousands") (r "^0.2") (d #t) (k 0)))) (h "0pldm79kjlci0zmdsfp4f3r97yn4cycff43ds078v4p0mf4mmk30")))

(define-public crate-toison-1.1.0 (c (n "toison") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "human_format") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "thousands") (r "^0.2") (d #t) (k 0)))) (h "0vj976jr1a89zbi3jiyxshbribb2i770dsc9k3ravaiqymip5vgy")))

