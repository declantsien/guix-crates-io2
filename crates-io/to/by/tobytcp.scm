(define-module (crates-io to by tobytcp) #:use-module (crates-io))

(define-public crate-tobytcp-0.8.0 (c (n "tobytcp") (v "0.8.0") (h "1lg23xri1wh130w4355qcm1y5zd7mmg4nhv9nj2rlpk2xdwba4dk") (y #t)))

(define-public crate-tobytcp-0.8.1 (c (n "tobytcp") (v "0.8.1") (h "145kfnk7qf27mzj81sv5flnarnlfd6d9ymmlv7h4f0jcnxc25ka5") (y #t)))

(define-public crate-tobytcp-0.9.0 (c (n "tobytcp") (v "0.9.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1p5nd9yz7j45305qjp4qp3x0nmmjfz1ldcn5xhq68yb6w4vfm0zl") (y #t)))

(define-public crate-tobytcp-0.9.1 (c (n "tobytcp") (v "0.9.1") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "13xsy9v9frk4zh6rvj653g01m8q1sk09rvs829j79cklih88hix7") (y #t)))

(define-public crate-tobytcp-0.9.2 (c (n "tobytcp") (v "0.9.2") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "08kkhm8mml0zispc8arkr88gazah6kwxn81zwxcavaj075qainyf") (y #t)))

(define-public crate-tobytcp-0.9.3 (c (n "tobytcp") (v "0.9.3") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v84k7d9wxsnajh3nx10if85z86173cizg9hj82rx0l7r4dd8sqr")))

(define-public crate-tobytcp-0.10.0 (c (n "tobytcp") (v "0.10.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0a3zxjks178hhydaw44i4z9ncwshx438ka0l8jzy1yigxaav2q95")))

(define-public crate-tobytcp-0.10.1 (c (n "tobytcp") (v "0.10.1") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "04zxri3xq42i1ah2szgmm7vpgv89fx6c1gadsqbxpzsbrxx11rcz")))

(define-public crate-tobytcp-0.11.0 (c (n "tobytcp") (v "0.11.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bljwk7jpc3g6mv800xw292jdm460hm2775xx55crzyr10ld2qkm")))

(define-public crate-tobytcp-0.12.0 (c (n "tobytcp") (v "0.12.0") (h "1baam0d50wya24k9p6xcaqmgrnyqfcpzp4ld2pv7v17cls3bv3pw")))

(define-public crate-tobytcp-0.13.0 (c (n "tobytcp") (v "0.13.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat" "io-compat"))) (d #t) (k 0)) (d (n "romio") (r "^0.3.0-alpha.8") (d #t) (k 2)) (d (n "runtime") (r "^0.3.0-alpha.5") (d #t) (k 2)))) (h "09466plpw3lndh0gad9frprfqrcjjxry4xysm82rd14nr90kmsbc")))

