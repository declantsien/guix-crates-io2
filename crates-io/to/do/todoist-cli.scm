(define-module (crates-io to do todoist-cli) #:use-module (crates-io))

(define-public crate-todoist-cli-0.1.0 (c (n "todoist-cli") (v "0.1.0") (h "17gv6ps0wwmv07yhckxgg7sfrp0pydq356i3y84d04iwmha13411")))

(define-public crate-todoist-cli-0.2.0 (c (n "todoist-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "httpmock") (r "^0.6.5") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lfn1qh29akpi6p6q9fxkrzryrj4mnnld857h2z38c4pmd6cynm2")))

