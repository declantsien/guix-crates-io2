(define-module (crates-io to do todo_cli_jfc) #:use-module (crates-io))

(define-public crate-todo_cli_jfc-0.1.0 (c (n "todo_cli_jfc") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0igr8is6himnfl4v2bgwjhzpnpg28q3m2rc7ry4cigvvd95ac065")))

(define-public crate-todo_cli_jfc-0.1.1 (c (n "todo_cli_jfc") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1dia0jwn5ip3zpd7a16s9cizb9zpn2c2fx6jlyjsbpybzzj1bx4y")))

(define-public crate-todo_cli_jfc-0.1.3 (c (n "todo_cli_jfc") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0snp53qrp5c1p1yf5b7di96lrgkq7yci8yjqhbvdfvfmgjxf3xfq")))

(define-public crate-todo_cli_jfc-0.1.4 (c (n "todo_cli_jfc") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1xq0a7nsldipgmfbn8adf05gd1fh1y3jllack4s9qss5fhp5qyxg")))

