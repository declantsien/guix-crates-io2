(define-module (crates-io to do todo_finder_lib) #:use-module (crates-io))

(define-public crate-todo_finder_lib-0.1.0 (c (n "todo_finder_lib") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "09xxwf0xh0pbcz94fgwbh9rlh88v98vy3dv30yhq4iyxpw1mz849")))

(define-public crate-todo_finder_lib-0.1.1 (c (n "todo_finder_lib") (v "0.1.1") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "09cqbvxvgjj5j60nvm026lzgbkcb10iq4dlxx1gl0pf0rkaxmywj")))

