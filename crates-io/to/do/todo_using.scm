(define-module (crates-io to do todo_using) #:use-module (crates-io))

(define-public crate-todo_using-0.2.1 (c (n "todo_using") (v "0.2.1") (h "1dya85dn4yg8pylwyxh7yrbbcyvci4638mpd339rm0yc0m35ffmx")))

(define-public crate-todo_using-0.2.2 (c (n "todo_using") (v "0.2.2") (h "0i92shwk4bbg0k1rr7lxvlr69d7ixggqa93d59n55sww4003ypx8")))

