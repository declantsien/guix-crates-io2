(define-module (crates-io to do todoist) #:use-module (crates-io))

(define-public crate-todoist-0.1.0 (c (n "todoist") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.43") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.16") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1sy9yp17nxs67qhzzd1j606d2y9ahm8nasq6w09ls5xd23r690ds")))

