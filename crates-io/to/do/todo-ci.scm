(define-module (crates-io to do todo-ci) #:use-module (crates-io))

(define-public crate-todo-ci-0.1.0 (c (n "todo-ci") (v "0.1.0") (d (list (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grep") (r "^0.2.8") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "15qw1pahl4zakgnikwkbsd0mmwsj5zfgsiybcbg4mlh1z562a3cb")))

(define-public crate-todo-ci-0.1.1 (c (n "todo-ci") (v "0.1.1") (d (list (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grep") (r "^0.2.8") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "04cv5sfyrwh4q8lys0j2s5mw5axfnp274y396d1d5yzw13i8m0ss")))

(define-public crate-todo-ci-0.1.2 (c (n "todo-ci") (v "0.1.2") (d (list (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grep") (r "^0.2.8") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "1q0hhvzi7c6gw7h9icpfb3d5dqw17qv0gpbhf2z0qj5zkykx2c97")))

(define-public crate-todo-ci-0.2.0 (c (n "todo-ci") (v "0.2.0") (d (list (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "globset") (r "^0.4.9") (d #t) (k 0)) (d (n "grep") (r "^0.2.8") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)))) (h "1vc75v9510g3sx7xr5ikqi020wm3iwnv93a66iw9q8mw1ava07k1")))

(define-public crate-todo-ci-0.3.0 (c (n "todo-ci") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "bunt") (r "^0.2.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "globset") (r "^0.4.9") (d #t) (k 0)) (d (n "grep") (r "^0.2.10") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "00jp9qdcpbxh2vl3xvlb7c1wjrq1cdps32aby97vx53wc6iyh5j1")))

