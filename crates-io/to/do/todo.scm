(define-module (crates-io to do todo) #:use-module (crates-io))

(define-public crate-todo-0.1.0 (c (n "todo") (v "0.1.0") (h "1xj0ds8h8pjphy2w2ymdh7g3p0wp82mv2m6igqkpk76cvlv2j448") (y #t)))

(define-public crate-todo-0.2.0 (c (n "todo") (v "0.2.0") (h "14vxxlwq02gxcwpdmrzhrnxbwfxcbhyc7ncqdsx3fmwwx4kmhk8y")))

(define-public crate-todo-0.3.0 (c (n "todo") (v "0.3.0") (h "1qsc77cyadk6gli2982233pz9d5ad1c2h1l2v8q1458rykj4b13j")))

