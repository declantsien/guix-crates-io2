(define-module (crates-io to do todo-rust) #:use-module (crates-io))

(define-public crate-todo-rust-1.0.0 (c (n "todo-rust") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "0z5sr5kbbcmzn9qkm2xvzcq0xjk68f127s248vfn6izfp7cz9afy")))

(define-public crate-todo-rust-1.0.1 (c (n "todo-rust") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "xdg") (r "^2.5.2") (d #t) (k 0)))) (h "1vlkydyac91lkswn5byiysbgb9h05phyqy36z0jjvs910mxk6wqy")))

