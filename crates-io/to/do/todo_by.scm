(define-module (crates-io to do todo_by) #:use-module (crates-io))

(define-public crate-todo_by-0.1.0 (c (n "todo_by") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1q7a0c3652vnjw90r3lpjlr624zvcyrfc1vk25wk999vnri427rc")))

(define-public crate-todo_by-0.2.0 (c (n "todo_by") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "18idhcj1bw01xd0q9w3w7ammwck02h1a5599ksxnzj4cmgwr2hfa")))

(define-public crate-todo_by-0.2.1 (c (n "todo_by") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1zpmvplm56jp5d94v7bkk9f2dj8as3fk14sp2v0b9ysprhhv7r8f")))

(define-public crate-todo_by-0.3.0 (c (n "todo_by") (v "0.3.0") (d (list (d (n "cargo_toml") (r "^0.15.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1i0d89g4vl77dcndd6paaf5nj5xl69k3bs54abn4265bfydm49cy")))

