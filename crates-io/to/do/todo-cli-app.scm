(define-module (crates-io to do todo-cli-app) #:use-module (crates-io))

(define-public crate-todo-cli-app-0.1.0 (c (n "todo-cli-app") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "08qbp7qpwrwx929lsfr2r0j4w45hsf5adv00jgpsz4m1ayqw2r9z")))

(define-public crate-todo-cli-app-0.2.0 (c (n "todo-cli-app") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f9m5j65g1vij02wf45akwrrrvhwixiydg0zvnmzz10m56hhrr45")))

(define-public crate-todo-cli-app-0.2.1 (c (n "todo-cli-app") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j42zz89isxsq72jx7a0sph7z90gyyf3hva8jgparf0v2cwlv9nx")))

(define-public crate-todo-cli-app-0.3.0 (c (n "todo-cli-app") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "16vaiqh3m4if5whxxmz3pj9ssb7y855vdc8dmr5f685l9hpbr06y")))

(define-public crate-todo-cli-app-0.3.1 (c (n "todo-cli-app") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p0ns2w7izq2cawk6g79j3dh3pgxvhpr85fqsyvz7w5zg4lgg0rg")))

(define-public crate-todo-cli-app-0.3.2 (c (n "todo-cli-app") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l0nq4vi93nbk5r014bilv6141ask5qp78yq5z5fw67bhy33ff3n")))

(define-public crate-todo-cli-app-0.3.3 (c (n "todo-cli-app") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hvihs71jj2qqiida0p9jnm8rkp1ccnzmngpvz7g46az1564x0ji")))

(define-public crate-todo-cli-app-0.4.0 (c (n "todo-cli-app") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0n0wcl6d1pp2n49q1i3kwfy2jf5b4kgvjf57g6vhd6784j5jcbnk")))

(define-public crate-todo-cli-app-0.5.0 (c (n "todo-cli-app") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0zn0m7kr1p61wvlbyb0hdipirxknr5cpc72mqsvk6sd8pnl5p51w")))

(define-public crate-todo-cli-app-0.5.1 (c (n "todo-cli-app") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "001h4cggvagfs9mx7bb8i0f2zyw52ha4jkn74ai0cd8kzcy8jlsk")))

