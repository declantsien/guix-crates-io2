(define-module (crates-io to do todoit) #:use-module (crates-io))

(define-public crate-todoit-0.1.0 (c (n "todoit") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "12gj246ynbjhvws2sxvfjswfv8cjvkwi0cs59xd210cyq3l5aaw3")))

(define-public crate-todoit-0.1.1 (c (n "todoit") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "00sj1syjqay8v2xzs1gx5m4zdcspi8ak2byzbhfn070dimnkphg6")))

