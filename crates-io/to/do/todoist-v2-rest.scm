(define-module (crates-io to do todoist-v2-rest) #:use-module (crates-io))

(define-public crate-todoist-v2-rest-0.1.0 (c (n "todoist-v2-rest") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0f9ma2cc8adhphafblkiy6acldn6agrqa8zyj28jqk8nxx8dhhxz")))

(define-public crate-todoist-v2-rest-0.1.1 (c (n "todoist-v2-rest") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1irfhvylv4ypij8y6zqivd4b314w295c4hb9pz9wacsl7i5cih8j")))

(define-public crate-todoist-v2-rest-0.2.0 (c (n "todoist-v2-rest") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0xa5y9rjjxr5qvw32gf1wf128djf1k80mx79sryanrvm63f9r10v")))

