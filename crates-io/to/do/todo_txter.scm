(define-module (crates-io to do todo_txter) #:use-module (crates-io))

(define-public crate-todo_txter-0.1.0 (c (n "todo_txter") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0v309ya4jh6zb6mi7pfaj8nmbg003hcq1l8jpfkprl3vynk83jyk")))

(define-public crate-todo_txter-0.1.1 (c (n "todo_txter") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "18izpfrwcggpd9m7hhfchip4p92l25w10qii76kxbvg4nfvyjsai")))

