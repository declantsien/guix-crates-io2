(define-module (crates-io to do todo-tui) #:use-module (crates-io))

(define-public crate-todo-tui-0.1.0 (c (n "todo-tui") (v "0.1.0") (d (list (d (n "coord2d") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "036hj23a06zidaf4xchjgk1sdv8z2s2x6hafvmbhb07c50g9rsd9")))

(define-public crate-todo-tui-0.1.1 (c (n "todo-tui") (v "0.1.1") (d (list (d (n "coord2d") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "1p79xbhhx8clsgps6nvg59kiifd74jbi0ylz278igy7jmw7ab6wi")))

(define-public crate-todo-tui-0.1.4 (c (n "todo-tui") (v "0.1.4") (d (list (d (n "coord2d") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "0zk3h0px719ayprc9yxkk0cj853gpkaaz8nqgbs420hlh9rhhgcm")))

(define-public crate-todo-tui-0.1.5 (c (n "todo-tui") (v "0.1.5") (d (list (d (n "coord2d") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "0sqdxrcwiakys76pmrih45hgm4aqgiq2sgy2n0v6p6511m2rg63s")))

(define-public crate-todo-tui-0.1.6 (c (n "todo-tui") (v "0.1.6") (d (list (d (n "coord2d") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "ncurses") (r "^5.101.0") (d #t) (k 0)))) (h "02748m1gi7j1qjw8ybjkh56zhi6gps183hwq5q0v85mpvqxl91fk")))

