(define-module (crates-io to do todoist_rest) #:use-module (crates-io))

(define-public crate-todoist_rest-0.0.1-dev (c (n "todoist_rest") (v "0.0.1-dev") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.25") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.25") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (d #t) (k 0)))) (h "0c33rys828gvrirwgsmyxvy45cljlyghixqaqjxkqw2j77z6ch40")))

(define-public crate-todoist_rest-0.0.2-dev (c (n "todoist_rest") (v "0.0.2-dev") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.25") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.25") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (d #t) (k 0)))) (h "0w9zizmaca0knsdyh11krj779a064rwblfdqmqlcm43nypxkp644")))

