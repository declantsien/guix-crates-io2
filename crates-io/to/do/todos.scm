(define-module (crates-io to do todos) #:use-module (crates-io))

(define-public crate-todos-0.1.0 (c (n "todos") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i4imcysn7bgicr1y6qni3mr6x3myk3manck995fnm4dshvrb070")))

