(define-module (crates-io to do todotui) #:use-module (crates-io))

(define-public crate-todotui-0.0.1 (c (n "todotui") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "rc"))) (d #t) (k 0)) (d (n "tui-realm-stdlib") (r "^1.3") (d #t) (k 0)) (d (n "tuirealm") (r "^1.9") (d #t) (k 0)))) (h "1xk27glnwpm6glxggwgnqzv1r5yik22c1z7q00cbdk743r0br1q8")))

(define-public crate-todotui-0.0.2 (c (n "todotui") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "postcard") (r "^1.0") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "rc"))) (d #t) (k 0)) (d (n "tui-realm-stdlib") (r "^1.3") (d #t) (k 0)) (d (n "tuirealm") (r "^1.9") (d #t) (k 0)))) (h "1qndlqz6k75z4ysk1l5brjz5qnjw3wi7l18i640w7pb452844ffc")))

