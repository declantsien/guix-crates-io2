(define-module (crates-io to do todo_list) #:use-module (crates-io))

(define-public crate-todo_list-0.1.0 (c (n "todo_list") (v "0.1.0") (d (list (d (n "ncurses") (r "^5.86.0") (d #t) (k 0)))) (h "1x4fwr5irijmniqa1sm7m2kc7sdw8wlmvb4cl9psigz427mrnaz0")))

(define-public crate-todo_list-1.0.1 (c (n "todo_list") (v "1.0.1") (d (list (d (n "ncurses") (r "^5.86.0") (d #t) (k 0)))) (h "01d85c45p84a6rmlqbs863ys5kmy7lv4vxd43q0sawzxxsmdi8ss")))

(define-public crate-todo_list-1.0.2 (c (n "todo_list") (v "1.0.2") (d (list (d (n "ncurses") (r "^5.86.0") (d #t) (k 0)))) (h "1x1p58m1vnhwdgryngrgg0bgsqy7a2xf2p6v98fipdag5f100894")))

(define-public crate-todo_list-1.1.0 (c (n "todo_list") (v "1.1.0") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "0bc7qy23a2iq2fcvnss4fbf4n7j3wjz72p6vvhhn382bznsv0qfg")))

(define-public crate-todo_list-1.1.1 (c (n "todo_list") (v "1.1.1") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "1rsh843f9pfjxvkhgkd1rp1cm2dwssbiczrbab3ik5n5hsbgyxvm")))

(define-public crate-todo_list-1.1.2 (c (n "todo_list") (v "1.1.2") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "0laf97464g2mmcrw8n9hql5nviwhd9nchmnqz0dnbxagcz0hdc1w")))

(define-public crate-todo_list-1.2.0 (c (n "todo_list") (v "1.2.0") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bii63rwqinikm81dr5cdbljrs7ynyzv5wmldvkr7cny4ngda62p")))

(define-public crate-todo_list-1.2.1 (c (n "todo_list") (v "1.2.1") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lj23zmdbaxh59d8sqwds4742mnknxcli3z0j64j5w9kfcasagw2")))

(define-public crate-todo_list-1.2.2 (c (n "todo_list") (v "1.2.2") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mfxy2ykapr54n36n5l7y991dhfpsn4sdkdbyqz67xli9dgzpa83")))

(define-public crate-todo_list-1.2.3 (c (n "todo_list") (v "1.2.3") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17fdpqk1jbm5pm1mii58jfi1dqw7a8h2zh0k2k1j95bxkzrh4fai")))

(define-public crate-todo_list-1.2.4 (c (n "todo_list") (v "1.2.4") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c52iadplfwsxsl9fsn8z1wp6590v3ampkr1mqnwg3wmjqwl5glz")))

(define-public crate-todo_list-1.2.5 (c (n "todo_list") (v "1.2.5") (d (list (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kdyqbfxf49s6kfhqssqrkbz5sq605zxs829k1fh11qdqfc3cz6s")))

