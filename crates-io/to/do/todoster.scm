(define-module (crates-io to do todoster) #:use-module (crates-io))

(define-public crate-todoster-0.1.0 (c (n "todoster") (v "0.1.0") (d (list (d (n "regex") (r "^1.9.4") (f (quote ("unicode-perl" "unicode-case"))) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0i62msrd76rqyz5l1qrvnhsr3wiszz3hbqpvjvhil4hq9dvwj6wk")))

