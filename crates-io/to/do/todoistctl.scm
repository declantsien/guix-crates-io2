(define-module (crates-io to do todoistctl) #:use-module (crates-io))

(define-public crate-todoistctl-0.2.0 (c (n "todoistctl") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron" "ron_conf"))) (k 0)) (d (n "libtodoist") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kjb7q8cxvc543kgcvcw11dcy2m9zjzw9jbwm5swv3m2lqbb9qqs")))

(define-public crate-todoistctl-0.3.0 (c (n "todoistctl") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron" "ron_conf"))) (k 0)) (d (n "libtodoist") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rqibax0wnlid5qsw3xlh7knkg5wpa74bmkkwp7bbn4ay0dsd2wy")))

(define-public crate-todoistctl-0.3.4 (c (n "todoistctl") (v "0.3.4") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron" "ron_conf"))) (k 0)) (d (n "libtodoist") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1gvg99w2c1dcjnx0f5f0wnyy1k549b0rb0f8r1mj29k1xs39h5si")))

(define-public crate-todoistctl-0.3.6 (c (n "todoistctl") (v "0.3.6") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron" "ron_conf"))) (k 0)) (d (n "libtodoist") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16k3w020gq0mfibac622hr4rwrn1ycxcarzv57ym0rdwv1dd2cmp")))

(define-public crate-todoistctl-0.3.7 (c (n "todoistctl") (v "0.3.7") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron" "ron_conf"))) (k 0)) (d (n "libtodoist") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hhrj63whp2hia5qxv3xcnyn7j9m3mnfxgpnf7x9vwl1hicdxs40")))

(define-public crate-todoistctl-0.4.0 (c (n "todoistctl") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (f (quote ("ron" "ron_conf"))) (k 0)) (d (n "libtodoist") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ls31vr5yrax8miz24gnr4d2qjsdrvfljqgjqf9fdb7nzwyx5l7r")))

