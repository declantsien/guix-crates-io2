(define-module (crates-io to do todocommander) #:use-module (crates-io))

(define-public crate-todocommander-0.1.0 (c (n "todocommander") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "1gi0g242ykdryx1bgfg0j89vp0q946wxfks6xx3irmppgdw1c6xf")))

(define-public crate-todocommander-0.2.0 (c (n "todocommander") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)))) (h "1pjmzb4fszin9204z1bapbvj1cpy81mbdk5lrzgwphijwlh47xfq")))

(define-public crate-todocommander-0.2.1 (c (n "todocommander") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)))) (h "122w1sqamghfws0iwfchig7wdnxk93zz1w191vx13fjgixg94862")))

