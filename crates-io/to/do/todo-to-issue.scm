(define-module (crates-io to do todo-to-issue) #:use-module (crates-io))

(define-public crate-todo-to-issue-0.1.0 (c (n "todo-to-issue") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "03mnp4flkflsibkm46m1j99pz98acim80gpnll277bsl00sn9w9y")))

(define-public crate-todo-to-issue-0.1.1 (c (n "todo-to-issue") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0kla5p4qa6i6n1fbca9lj4wcc76gbw68m4w5s9s7iv8a9srvy68k")))

