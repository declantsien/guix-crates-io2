(define-module (crates-io to do todotxt) #:use-module (crates-io))

(define-public crate-todotxt-0.1.0 (c (n "todotxt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)))) (h "1dpw6gyx7lbi7xi176g9hlzzy9h17ymwqxdnznq56dy00kcrir1g")))

(define-public crate-todotxt-0.2.0 (c (n "todotxt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)))) (h "1f2q2skgfqdmyjvjqcx9i2pqbh3yhllwhk3xj9x9gmbjyck09a9f")))

(define-public crate-todotxt-0.3.0 (c (n "todotxt") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)))) (h "1cwby84zjjnm49v7hls43mwqqckmzn4bpic0fxf5p9g6aad0klr4")))

