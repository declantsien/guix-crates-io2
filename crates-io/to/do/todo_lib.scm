(define-module (crates-io to do todo_lib) #:use-module (crates-io))

(define-public crate-todo_lib-0.2.0 (c (n "todo_lib") (v "0.2.0") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^1.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "197gljh0ycm72k4886xwwdgm481yvdla9xg5xf11rmrfc8xslzi1")))

(define-public crate-todo_lib-0.2.1 (c (n "todo_lib") (v "0.2.1") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^1.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "17fi2656z0d7s0p1d0pqvly7x8j8y8pi88wnlz05lpgdb4x36xk0")))

(define-public crate-todo_lib-0.2.2 (c (n "todo_lib") (v "0.2.2") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^1.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "004sk9ljwah8ymb26bdzrxgrv0g9dyyjnkb57fwi9cc0hvk8gqn2")))

(define-public crate-todo_lib-0.2.3 (c (n "todo_lib") (v "0.2.3") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^1.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "1znxffhy6gjcbccljsf58wx1y3c0jvpgzgm9zaq5rcxanmxmpvlv")))

(define-public crate-todo_lib-0.3.0 (c (n "todo_lib") (v "0.3.0") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^1.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "1xs2adci2ddicndj3dnzc6zsjkw9cd8d153p1kv80ff6qx3wmdq9")))

(define-public crate-todo_lib-0.3.1 (c (n "todo_lib") (v "0.3.1") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^1.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "1h7h43jl2n2z2ijwpkhlprgg5dsd2y1vj35k4bkdss8g9dpppqh6")))

(define-public crate-todo_lib-0.3.2 (c (n "todo_lib") (v "0.3.2") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^1.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "16v46fhikkc7wb2cbzw0dlkflcqpxr5ivin3x93liqap3ghi7364")))

(define-public crate-todo_lib-0.3.4 (c (n "todo_lib") (v "0.3.4") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^2.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "1grs93mpzw01scl2f2b0d794p19mxnvmlm1h9dy7swbrf4kxganr")))

(define-public crate-todo_lib-0.4.0 (c (n "todo_lib") (v "0.4.0") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^2.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "0lj537yyns0b8v5js8s7wz6nd67pvmxgnqv22vylcn8q3x0krmd8")))

(define-public crate-todo_lib-0.5.0 (c (n "todo_lib") (v "0.5.0") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^2.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "1b8hvp96w2cqsz1wy1nbrppjspkcxnv3q1cdkl57v8h5ni6gxsrr")))

(define-public crate-todo_lib-0.5.1 (c (n "todo_lib") (v "0.5.1") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^2.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "1b62i61cqdkas2c2xspgjbdh4s0qq6r6s4x4plwy2yq7mmfq7rxr")))

(define-public crate-todo_lib-0.5.2 (c (n "todo_lib") (v "0.5.2") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^2.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "0nmdw8i13qyv7hy16nhnqm02i325w1f53ai55ryl2q4yix1d3jxw")))

(define-public crate-todo_lib-1.0.0 (c (n "todo_lib") (v "1.0.0") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^2.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "1xw1pflif4i9qh20sdpl2i2imcxsqicnkc1ksq3p8m8ba0mjmzgg")))

(define-public crate-todo_lib-1.0.1 (c (n "todo_lib") (v "1.0.1") (d (list (d (n "caseless") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "todo-txt") (r "^2.0") (f (quote ("extended"))) (d #t) (k 0)))) (h "15ix835nc3lxy2mq67l2f5y0k3l7yyxr62dglg0ls9lpwz6qla97")))

(define-public crate-todo_lib-2.0.0 (c (n "todo_lib") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1fsyya2ay6azbdaxx3hngdl5lx2w2ryr9mhiyahxqb8j5ppbd80f")))

(define-public crate-todo_lib-2.1.0 (c (n "todo_lib") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0fsds9k8mwjg89b1q93rwzbk2g9h25nz8vbml7ynxhlrxfg3y9p2") (y #t)))

(define-public crate-todo_lib-2.1.1 (c (n "todo_lib") (v "2.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0lgsfd8v7dlvwgbmhbnq8vry7g9ziwpvk1a8d78sgm178w3s9q46")))

(define-public crate-todo_lib-3.0.0 (c (n "todo_lib") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "163r1an2z30jms9zwg5v5vwzpinv99dx8k8fibxl89c5x9dg1fgy")))

(define-public crate-todo_lib-3.0.1 (c (n "todo_lib") (v "3.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0xh5cr496rsy7hbgrzw8cq4fbcp3byirwdflxgn96ayp67ij5kbh")))

(define-public crate-todo_lib-3.0.2 (c (n "todo_lib") (v "3.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1y65xzg4xiirv0nshcdn1msqqmrz80m9xgmiqafh1ysmg0gay2bk")))

(define-public crate-todo_lib-4.0.0 (c (n "todo_lib") (v "4.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1md4v9vmcj5wkcdkvxvn6v63dg57qw9fhj1mrqm1clvjkb71694l")))

(define-public crate-todo_lib-5.0.0 (c (n "todo_lib") (v "5.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0092c9agghii49bspmfbyd4j5rwsca6ci56z40nlk450yd6dpmbn")))

(define-public crate-todo_lib-5.1.0 (c (n "todo_lib") (v "5.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ygfwzk234vxn5l5jbk3qrqdis3ps7xlbyz3d66rf1932ci5wgxg")))

(define-public crate-todo_lib-5.1.1 (c (n "todo_lib") (v "5.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wy0qwdiwvqyjvv86mbxs6v3dwj6m48ic05yiyw5mj45028fmmvj")))

(define-public crate-todo_lib-5.1.2 (c (n "todo_lib") (v "5.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v5fhwfgcxaznxbkfanbdwcga44419aa9bxnrmasscjr08xyhqad")))

(define-public crate-todo_lib-6.0.0 (c (n "todo_lib") (v "6.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14sgk440icqix6n165ij7igvy7hmgf58bs967i0ryhx51xy1vr8x")))

(define-public crate-todo_lib-6.0.1 (c (n "todo_lib") (v "6.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ldixm86q1s44fmyk82dkay74kx3fqsgz3dj9yww4a9rml7l3qgl")))

(define-public crate-todo_lib-6.1.0 (c (n "todo_lib") (v "6.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09fydxnbqwd7xl7n3c5w8rmrcb09xdi5xzkq389i0cgp67i2iqaj")))

(define-public crate-todo_lib-6.1.1 (c (n "todo_lib") (v "6.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cphq00fyax93kflfcxxprhdf8pw2ydql8an6iasis2a1gbldlqa")))

(define-public crate-todo_lib-6.1.2 (c (n "todo_lib") (v "6.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gcwklvd8i5kq4n96qlhgrapv7hghl7rrpgws63321626m2r4apk")))

(define-public crate-todo_lib-6.1.3 (c (n "todo_lib") (v "6.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gdglk5kccjcijca8ql4hq1n0fbcsbqpdm5cw4xvp993bgmpv8bi")))

(define-public crate-todo_lib-7.0.0 (c (n "todo_lib") (v "7.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mnvr5rm39yy3avqr45gi96dbrn6k7wyjxgs4rbffj4dbhz3ykpr")))

(define-public crate-todo_lib-7.0.1 (c (n "todo_lib") (v "7.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04q3qqvv0xacnxr5pwb5l8av9j9zvf0p53azivp07zi63vy6vvhf")))

(define-public crate-todo_lib-7.1.0 (c (n "todo_lib") (v "7.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gvxyqfclg5vfa49iq991mmcyfjp9fv10hk5h7pb5lcd9y239a85")))

