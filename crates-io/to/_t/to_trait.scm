(define-module (crates-io to _t to_trait) #:use-module (crates-io))

(define-public crate-to_trait-0.1.0 (c (n "to_trait") (v "0.1.0") (h "0xkcnxb9rqn2inf4xwalbbkn1id4z27ax2fx11291bz3jyp003gr")))

(define-public crate-to_trait-0.1.1 (c (n "to_trait") (v "0.1.1") (h "05jv5m22m5176mvcrfc0g40gb5lyxcq80syh7d0qv2y6icwhn9l1")))

