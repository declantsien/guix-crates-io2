(define-module (crates-io to _t to_tokenstream) #:use-module (crates-io))

(define-public crate-to_tokenstream-0.1.0 (c (n "to_tokenstream") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)))) (h "02sklyzaf6kp8pf4l60ga5sq3lwc0ryhg1f1n1an1ix0y5bh0vbr")))

(define-public crate-to_tokenstream-0.1.1 (c (n "to_tokenstream") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)))) (h "0pwag2nk7bkbf19icgbbj4b0v72rg3q9l8ihldrpxfk3yckbj7jf")))

(define-public crate-to_tokenstream-0.1.2 (c (n "to_tokenstream") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)))) (h "0s7hywv8j3lr05cg32qijv1iibbr8xsglgzcvwsxi9kkgdflb1ng")))

(define-public crate-to_tokenstream-0.1.3 (c (n "to_tokenstream") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)))) (h "1jppcssxgqbd697i136s7k7jibxysd89gk9lqgz70v4a89hidb95")))

