(define-module (crates-io to _d to_default) #:use-module (crates-io))

(define-public crate-to_default-0.1.0 (c (n "to_default") (v "0.1.0") (h "1x2sqb5c5mrawqzlwh5s5gpsgjcx08jdgrqmpdl23w24gpyj1cmm")))

(define-public crate-to_default-0.1.1 (c (n "to_default") (v "0.1.1") (h "1x656m8bd5nwh89c1hr8rg36q6sj17b5ghpv6dx9y682ipjmq5jk")))

(define-public crate-to_default-0.1.2 (c (n "to_default") (v "0.1.2") (h "0z2samkd3mb7h901x7d2snpc0qq7c3x2qv6m168f0vpxghznbhbx")))

