(define-module (crates-io to -a to-arraystring) #:use-module (crates-io))

(define-public crate-to-arraystring-0.1.0 (c (n "to-arraystring") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "itoa") (r "^1.0.10") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (d #t) (k 0)))) (h "1fvhj50ssgdq735v9hpr6wvmff9xxk89x47mb0lz4fxb96lgp4ib")))

(define-public crate-to-arraystring-0.1.1 (c (n "to-arraystring") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "itoa") (r "^1.0.10") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (d #t) (k 0)))) (h "0qd6mzs8rhlsq5pmx4jivh54918qlg7jlfvbg56r0ipvnw1s9dpb") (r "1.56")))

(define-public crate-to-arraystring-0.1.2 (c (n "to-arraystring") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "itoa") (r "^1.0.10") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (d #t) (k 0)))) (h "10n2akcfndlhyki6wp988cabb77jarfi6pxa2x02flwny64ps6l1") (r "1.56")))

(define-public crate-to-arraystring-0.1.3 (c (n "to-arraystring") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "itoa") (r "^1.0.10") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "ryu") (r "^1.0.17") (d #t) (k 0)))) (h "1d300yb6p0l31y3yh14dv2g47czha4f919ccfqj5q2yb5cj4c2rl") (r "1.56")))

