(define-module (crates-io to bj tobj) #:use-module (crates-io))

(define-public crate-tobj-0.0.1 (c (n "tobj") (v "0.0.1") (h "051ja0wbf7laiyfcqmppv57642q9y5z3ydqp00790la8kkjmnra3")))

(define-public crate-tobj-0.0.2 (c (n "tobj") (v "0.0.2") (h "1923mv35hr1np208fkjm9172salw4nl7r520yp51wypgyz9phzlh")))

(define-public crate-tobj-0.0.3 (c (n "tobj") (v "0.0.3") (h "05hzjz5qr5s45c9fkb8qjhmglsizlgjk6bvd2bb7p054vdnbqrn9")))

(define-public crate-tobj-0.0.4 (c (n "tobj") (v "0.0.4") (h "12pp2cjdpcgfns4ljl6510xdllhf92vxj2ymxkbcrvpyqiwgxr68") (f (quote (("unstable"))))))

(define-public crate-tobj-0.0.5 (c (n "tobj") (v "0.0.5") (h "0my1ssdqx36b9a6avx5dpcgw86giyndnjyzsb3zv5bspj1m7jn0a") (f (quote (("unstable"))))))

(define-public crate-tobj-0.0.6 (c (n "tobj") (v "0.0.6") (h "1zmxsc5axqz44mmhk3s6zswmaz0hxkwnqf9p603jskxrgsgnz6rl") (f (quote (("unstable"))))))

(define-public crate-tobj-0.0.7 (c (n "tobj") (v "0.0.7") (h "13s9pxnlsvg1j28k1426ryax165n7w63bzdylqi5nmmpmnr1d2w9") (f (quote (("unstable"))))))

(define-public crate-tobj-0.0.8 (c (n "tobj") (v "0.0.8") (h "05gw488phfklrk0fjyrid2b915d4kcpznl6lazmyg7ip0057hzwv") (f (quote (("unstable"))))))

(define-public crate-tobj-0.0.9 (c (n "tobj") (v "0.0.9") (h "046976jnn67a65wfr00mfzywx913fsyysxi4156rdm4m5pjy0l4x") (f (quote (("unstable"))))))

(define-public crate-tobj-0.1.0 (c (n "tobj") (v "0.1.0") (h "0v60lk1xaaqr0s1bbm3wlyirmpddyy84isnjipjsv0wbfqsq7h5c") (f (quote (("unstable"))))))

(define-public crate-tobj-0.1.1 (c (n "tobj") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "0a6fqjpb1h9g6xfl2piqzdjl7pblbpj7l6h8qn561jkqwjcf593k") (f (quote (("unstable" "clippy"))))))

(define-public crate-tobj-0.1.2 (c (n "tobj") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.91") (o #t) (d #t) (k 0)))) (h "1f1ns2lk69s1b26gm92qdys6phslw2gk03b0nd1nv8c7gy4c240b") (f (quote (("unstable" "clippy"))))))

(define-public crate-tobj-0.1.3 (c (n "tobj") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.125") (o #t) (d #t) (k 0)))) (h "0bcl7v446933x4jpmrxj6qmniym16z69pviz9z0dv0r12p7ijm7l") (f (quote (("unstable" "clippy"))))))

(define-public crate-tobj-0.1.4 (c (n "tobj") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0.125") (o #t) (d #t) (k 0)))) (h "1q75sfg5ij5jp1gig6l6s5ri8scs096ycmp7nqd249mav0mngcir") (f (quote (("unstable" "clippy"))))))

(define-public crate-tobj-0.1.5 (c (n "tobj") (v "0.1.5") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)))) (h "0nmhvci3iii7xxz23s78ccbhpwlxmv8bvh2dakpdgnhs8flinsv3") (f (quote (("unstable" "clippy"))))))

(define-public crate-tobj-0.1.6 (c (n "tobj") (v "0.1.6") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)))) (h "0cfwx9jkfrv2rzws93b0xcv0v2513dbvip66jbxwwvzgmgg0rsy5") (f (quote (("unstable" "clippy"))))))

(define-public crate-tobj-0.1.7 (c (n "tobj") (v "0.1.7") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)))) (h "1ccxna3ygsy2cn4bad45s58a36lw4kdg45qq96w43lrjj0cy1dl1") (f (quote (("unstable" "clippy"))))))

(define-public crate-tobj-0.1.8 (c (n "tobj") (v "0.1.8") (h "0n8483ga8qis92ix3mkbmngvw6c6waxwx964wjchqpbj3ll5hhah")))

(define-public crate-tobj-0.1.9 (c (n "tobj") (v "0.1.9") (h "0jcx769flwhxv4a1kg3hx5zvjix1f34dxj6bghypn3np6r9m5q7i")))

(define-public crate-tobj-0.1.10 (c (n "tobj") (v "0.1.10") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1kckb5h3jnfrfkjyz656hxbfqxqg9f5dbrasy2jik8f2nnc82q53")))

(define-public crate-tobj-0.1.11 (c (n "tobj") (v "0.1.11") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "073bw312y4w5a48vd49nnm978sibbvjpj4a7f9x6nikw7gng9mhy")))

(define-public crate-tobj-0.1.12 (c (n "tobj") (v "0.1.12") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "0abpp8d6jwnanz55haxklxvv2ywi6048pcwfhvyivxsbp7ix6sgy")))

(define-public crate-tobj-1.0.0 (c (n "tobj") (v "1.0.0") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "07g200hh6zkb4zx0vdn0qflfyqml4z0hphfk1pszi8lmvsphp79b")))

(define-public crate-tobj-2.0.0 (c (n "tobj") (v "2.0.0") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1nszw50xf4kbz8ljmbscw6w4flzq8vg1izyqm8fgynz4gav8yx22")))

(define-public crate-tobj-2.0.1 (c (n "tobj") (v "2.0.1") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1zcb46xcjby23mhgqynr3i588qm13iiz36gziikbffv52ik2c2cc")))

(define-public crate-tobj-2.0.2 (c (n "tobj") (v "2.0.2") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1w9wzg9cdnl7crx378l6dj433jh127in3wllbh4dsz5isl610wk1")))

(define-public crate-tobj-2.0.3 (c (n "tobj") (v "2.0.3") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1zvskpn6gjwpgkrkapxrxvcgvgg9c4bhqhm8psxw801mqbsk3bhq")))

(define-public crate-tobj-2.0.4 (c (n "tobj") (v "2.0.4") (d (list (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "06w8ix9nk38wli0pjfl22hiyy07byhdcz18nkwhzg775x1dzas14")))

(define-public crate-tobj-3.0.0 (c (n "tobj") (v "3.0.0") (d (list (d (n "ahash") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "0l8020r0iij58dvmyf987z1vnj6kkijqikg8x688lg1n93w0yp78") (f (quote (("reordering") ("merging") ("default" "ahash"))))))

(define-public crate-tobj-3.0.1 (c (n "tobj") (v "3.0.1") (d (list (d (n "ahash") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "0w3daf6ms604drxsjj72hwkn8alngbpc7ha7jwl1z1cvd8y771c9") (f (quote (("reordering") ("merging") ("default" "ahash"))))))

(define-public crate-tobj-3.1.0 (c (n "tobj") (v "3.1.0") (d (list (d (n "ahash") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "1q9hxhdzbqcrpnmja98mnd5890jf3x1njg0q1lgpj8yg5j29ryc9") (f (quote (("reordering") ("merging") ("default" "ahash"))))))

(define-public crate-tobj-3.2.0 (c (n "tobj") (v "3.2.0") (d (list (d (n "ahash") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1b4dkfkc944ycx44zx271hxjwarq3hw3mhzdzv9nk70kq7mm6yi6") (f (quote (("reordering") ("merging") ("default" "ahash") ("async"))))))

(define-public crate-tobj-3.2.1 (c (n "tobj") (v "3.2.1") (d (list (d (n "ahash") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "19212j92glni13nd8drgnvzm0l4mafkwrbmgd2l4i6pns2bq7yfd") (f (quote (("reordering") ("merging") ("default" "ahash") ("async"))))))

(define-public crate-tobj-3.2.2 (c (n "tobj") (v "3.2.2") (d (list (d (n "ahash") (r "^0.7.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0rhfbv4hb6m5dsz22js0cjh4497957729fab207mcwq5166pwirk") (f (quote (("reordering") ("merging") ("default" "ahash") ("async"))))))

(define-public crate-tobj-3.2.3 (c (n "tobj") (v "3.2.3") (d (list (d (n "ahash") (r "^0.7.6") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1mi71ij3yxymssnar8yms4z7aldbhd2qvh7py3rqzzf4phxfxb6y") (f (quote (("reordering") ("merging") ("default" "ahash") ("async") ("arbitrary" "arbitrary/derive"))))))

(define-public crate-tobj-3.2.4 (c (n "tobj") (v "3.2.4") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.2.3") (o #t) (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "16svnhss0sczgx96qhryf1qwkyx3vd64afxs62gy1qj9ga4dw2rd") (f (quote (("use_f64") ("reordering") ("merging") ("default" "ahash") ("async") ("arbitrary" "arbitrary/derive"))))))

(define-public crate-tobj-3.2.5 (c (n "tobj") (v "3.2.5") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "142zslvlq8mpcz1id0c6naczyj2rpwzwsfp6kp8vm28j543i4f2p") (f (quote (("use_f64") ("reordering") ("merging") ("default" "ahash") ("async") ("arbitrary" "arbitrary/derive"))))))

(define-public crate-tobj-4.0.0 (c (n "tobj") (v "4.0.0") (d (list (d (n "ahash") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "07qd7bk1qldrpw05azdzdnrgy1aqypmayzcifvyc87i50sxf6l5l") (f (quote (("use_f64") ("reordering") ("merging") ("default" "ahash") ("async") ("arbitrary" "arbitrary/derive"))))))

(define-public crate-tobj-4.0.1 (c (n "tobj") (v "4.0.1") (d (list (d (n "ahash") (r "^0.8.7") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "08q2fxnjagakhbzn7b0dkg3hs6c2i39cxg0dbvwz5c050kna6z2z") (f (quote (("use_f64") ("reordering") ("merging") ("default" "ahash") ("async") ("arbitrary" "arbitrary/derive"))))))

(define-public crate-tobj-4.0.2 (c (n "tobj") (v "4.0.2") (d (list (d (n "ahash") (r "^0.8.7") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1s3jhcfyid07simdg468p8haizv5cj5sa48cdidwdr19byh4pgf3") (f (quote (("use_f64") ("reordering") ("merging") ("default" "ahash") ("async") ("arbitrary" "arbitrary/derive"))))))

