(define-module (crates-io to as toast-cell) #:use-module (crates-io))

(define-public crate-toast-cell-0.1.0 (c (n "toast-cell") (v "0.1.0") (d (list (d (n "type-factory") (r "^0.2") (d #t) (k 0)))) (h "1zi1c1l1j4lfi67xlm5l39w01cg0v8nk5blxpxgp1dan178qbmvx") (y #t) (r "1.61")))

(define-public crate-toast-cell-0.1.1 (c (n "toast-cell") (v "0.1.1") (d (list (d (n "type-factory") (r "^0.2") (d #t) (k 0)))) (h "095xbnxvxg5zwm229fp1krb97fsvz6jr88qp7j4gjavf2q51kxqw") (y #t) (r "1.61")))

(define-public crate-toast-cell-0.2.0 (c (n "toast-cell") (v "0.2.0") (d (list (d (n "type-factory") (r "^0.1") (d #t) (k 0)))) (h "0wc9y70bjqijy0lr259wdn89a2lj0csa2q1sgfp3ap0pcyapl2dy") (y #t) (r "1.75")))

(define-public crate-toast-cell-0.2.1 (c (n "toast-cell") (v "0.2.1") (d (list (d (n "type-factory") (r "^0.1") (d #t) (k 0)))) (h "1z3sw8j40y1jvdg0yisdzmksj3ppiw3sxlad8p00cy229l7qzhgb") (y #t) (r "1.75")))

(define-public crate-toast-cell-0.3.0 (c (n "toast-cell") (v "0.3.0") (d (list (d (n "type-factory") (r "^0.3") (d #t) (k 0)))) (h "038xnhgkzl2wbr9f2dq4acan2vqj005v15339zvpvz6nr5cya99c") (r "1.61")))

