(define-module (crates-io to as toastify) #:use-module (crates-io))

(define-public crate-toastify-0.3.4 (c (n "toastify") (v "0.3.4") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "notify-rust") (r "^3.5") (d #t) (k 0)))) (h "024rplr2kjh8n3y0mck9vzsi5q6aidccarfxc4g8g18a0g9anr6g")))

(define-public crate-toastify-0.4.0 (c (n "toastify") (v "0.4.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "notify-rust") (r "^3.6") (d #t) (k 0)))) (h "0cnfcz0vnbk5vlr48q1r859dcpnpcnc9zx3c4pg5akvnzsh10899")))

(define-public crate-toastify-0.5.0 (c (n "toastify") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.5") (d #t) (k 0)))) (h "0xqcfww2kql40jq8h7si8ws7hfi6jfiqgbspcs7pcasvkmc4bjk5")))

(define-public crate-toastify-0.5.1 (c (n "toastify") (v "0.5.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.5") (k 0)))) (h "0aljpm7574gpx4qmi179zywq9v9cq6bmk8lam1cirxwsbzbakhz1") (f (quote (("z" "notify-rust/z") ("images" "notify-rust/images") ("default" "z") ("d" "notify-rust/d"))))))

(define-public crate-toastify-0.5.2 (c (n "toastify") (v "0.5.2") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.5") (k 0)))) (h "1mfwvwwd5arag1xmijalb348ibsnmjj6as15hbbh36pald51j556") (f (quote (("z" "notify-rust/z") ("images" "notify-rust/images") ("default" "z") ("d" "notify-rust/d"))))))

(define-public crate-toastify-0.5.3 (c (n "toastify") (v "0.5.3") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.5") (k 0)))) (h "0z9f298aby3jxkyianq3q2928zi3fybnz6146adskv26rnw0rwpk") (f (quote (("z" "notify-rust/z") ("images" "notify-rust/images") ("default" "z") ("d" "notify-rust/d"))))))

(define-public crate-toastify-0.5.4 (c (n "toastify") (v "0.5.4") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4.5") (k 0)))) (h "0iawb5f2r2a4r9jwqwvlz89hpslyr7v5lyxnqkx8zcd0kf0qwmxv") (f (quote (("z" "notify-rust/z") ("images" "notify-rust/images") ("default" "z") ("d" "notify-rust/d"))))))

