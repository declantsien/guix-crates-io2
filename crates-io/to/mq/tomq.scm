(define-module (crates-io to mq tomq) #:use-module (crates-io))

(define-public crate-tomq-0.1.1 (c (n "tomq") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0nmbwhl9yyv286fr7igrmicq5vl4c98pz62vqzrgg2g8f623ad6k") (f (quote (("threaded") ("default" "threaded"))))))

(define-public crate-tomq-0.1.2 (c (n "tomq") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0mw8110k7g6fdprv8r8nw0vg4iw1wrbrfsy95zwak1mqrg386xdy") (f (quote (("threaded") ("default" "threaded"))))))

