(define-module (crates-io to uc touch_event) #:use-module (crates-io))

(define-public crate-touch_event-0.1.0 (c (n "touch_event") (v "0.1.0") (d (list (d (n "atomic") (r "^0.5.3") (d #t) (k 0)) (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "19mx0fbh38ys52rj8wxw9d0z2wa9f81r4z82mwvnbljqpdjfqdgp")))

(define-public crate-touch_event-0.1.1 (c (n "touch_event") (v "0.1.1") (d (list (d (n "atomic") (r "^0.5.3") (d #t) (k 0)) (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "061b4n0zcy2v17sdmqrzwma5sdbjrzm6795ajnngzf7ipkv80zc0")))

(define-public crate-touch_event-0.1.2 (c (n "touch_event") (v "0.1.2") (d (list (d (n "atomic") (r "^0.5.3") (d #t) (k 0)) (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "1b668xmfy12yszgdv9l8wvfwjdm4hn6diiqji8hjbr7lx8107jbn")))

(define-public crate-touch_event-0.1.3 (c (n "touch_event") (v "0.1.3") (d (list (d (n "atomic") (r "^0.5.3") (d #t) (k 0)) (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "12i77cz7jayrbddlnamxf5gpbsyxkwkipd31xrbv58ghn5wigw0l")))

