(define-module (crates-io to uc touchnews) #:use-module (crates-io))

(define-public crate-toucHNews-0.2.0 (c (n "toucHNews") (v "0.2.0") (d (list (d (n "hn") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "rubrail") (r "^0.4") (d #t) (k 0)))) (h "1b6m84xw21bkm6yl4x9fz7aqff1jf895b5v3fpd6p529m3gz9vii")))

(define-public crate-toucHNews-0.2.1 (c (n "toucHNews") (v "0.2.1") (d (list (d (n "hn") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "rubrail") (r "^0.4") (d #t) (k 0)))) (h "11af5192n8pnw1iy9pizm9wmsgmy8v97i5wk90saiv5d651wsm9l")))

(define-public crate-toucHNews-0.3.0 (c (n "toucHNews") (v "0.3.0") (d (list (d (n "fruitbasket") (r "^0.3") (f (quote ("logging"))) (d #t) (k 0)) (d (n "hn") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "rubrail") (r "^0.4") (d #t) (k 0)))) (h "1sw2dwh9ahfs2ar2yml7s4jp77dyjxyi1g3vfjr183zg5kl9h8v6")))

(define-public crate-toucHNews-0.3.1 (c (n "toucHNews") (v "0.3.1") (d (list (d (n "fruitbasket") (r "^0.3") (f (quote ("logging"))) (d #t) (k 0)) (d (n "hn") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "rubrail") (r "^0.5") (d #t) (k 0)))) (h "0b24fddb64xciqbdm75zmv0q1r5nz8crgb4c1qj9y7b17ql0432x")))

(define-public crate-toucHNews-0.3.2 (c (n "toucHNews") (v "0.3.2") (d (list (d (n "fruitbasket") (r "^0.5") (f (quote ("logging"))) (d #t) (k 0)) (d (n "hn") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "rubrail") (r "^0.5") (d #t) (k 0)))) (h "1v0vfd2ph5n136vvxl4kjwi4559hifz8ggg37dj4nz914hfapgwc")))

(define-public crate-toucHNews-0.3.3 (c (n "toucHNews") (v "0.3.3") (d (list (d (n "fruitbasket") (r "^0.5") (f (quote ("logging"))) (d #t) (k 0)) (d (n "hn") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "rubrail") (r "^0.5") (d #t) (k 0)))) (h "1f05zspakl2srya2z7207h5s9rl4p41vgykvdggqmqx5x9mdx6rl")))

(define-public crate-toucHNews-0.4.0 (c (n "toucHNews") (v "0.4.0") (d (list (d (n "fruitbasket") (r "^0.6") (f (quote ("logging"))) (d #t) (k 0)) (d (n "hn") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "open") (r "^1.2") (d #t) (k 0)) (d (n "rubrail") (r "^0.7") (d #t) (k 0)))) (h "1n8hrbadjvpdxif2x3l2vihh6zlxcw91mmpgc1vm3rpbbcggixg0")))

