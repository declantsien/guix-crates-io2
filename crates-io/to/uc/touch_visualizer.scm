(define-module (crates-io to uc touch_visualizer) #:use-module (crates-io))

(define-public crate-touch_visualizer-0.1.0 (c (n "touch_visualizer") (v "0.1.0") (d (list (d (n "piston") (r "^0.31.2") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.20.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.20.1") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.39.0") (d #t) (k 2)) (d (n "pistoncore-input") (r "^0.17.0") (d #t) (k 0)) (d (n "pistoncore-sdl2_window") (r "^0.40.0") (d #t) (k 2)))) (h "149p65w2gb9q60h2vqgf5q69qq47lqay128rkz8vgxx2mw7sz1xl") (y #t)))

