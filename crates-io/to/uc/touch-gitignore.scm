(define-module (crates-io to uc touch-gitignore) #:use-module (crates-io))

(define-public crate-touch-gitignore-0.1.0 (c (n "touch-gitignore") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1cdqayx9f7b7xkzbayd9dc7p7akmrx0cxc1g1x6cchjxyxjrpdyi")))

