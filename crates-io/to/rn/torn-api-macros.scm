(define-module (crates-io to rn torn-api-macros) #:use-module (crates-io))

(define-public crate-torn-api-macros-0.1.0 (c (n "torn-api-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pp1qkcvwi1psqk5p5h7f84w9wgq8p7f6yp8rwq114cry1sygnhj")))

(define-public crate-torn-api-macros-0.1.1 (c (n "torn-api-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05481k3n4y96dqjbcy3vcim3i53b2v8jpk7a8b4i0x0mawif0453")))

(define-public crate-torn-api-macros-0.1.2 (c (n "torn-api-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "178f7jifjivwkn7nh66aixmdxkg7rpjsyfr9v1c4ypccac8f84c7")))

(define-public crate-torn-api-macros-0.2.0 (c (n "torn-api-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "000k2gg0jcqalj116g41a6nlvpc3gczbijz0ab40wr4a49n20nvw")))

(define-public crate-torn-api-macros-0.3.0 (c (n "torn-api-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0s2sgzw96p6rm3fha6k1id57h89lirr0m00ykdkm6k1pzxirfr1j")))

(define-public crate-torn-api-macros-0.3.1 (c (n "torn-api-macros") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "089qqvyfg9vnnabscx59321cwk2hrdy76zrc24g3fd9dscl1hh04")))

(define-public crate-torn-api-macros-0.3.2 (c (n "torn-api-macros") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "02kx9cfda6bjqvg7bc7hhdcwcpn416qh6c3zyphswwm7hxbnhvgj")))

