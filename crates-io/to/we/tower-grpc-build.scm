(define-module (crates-io to we tower-grpc-build) #:use-module (crates-io))

(define-public crate-tower-grpc-build-0.0.0 (c (n "tower-grpc-build") (v "0.0.0") (h "0hagfiylzcr8w27rwj4lpzg16wlsbmkyszqqggx6jz3wzzfzjzgm")))

(define-public crate-tower-grpc-build-0.1.0 (c (n "tower-grpc-build") (v "0.1.0") (d (list (d (n "codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.5") (d #t) (k 0)))) (h "1pw14k4bdbkmlj0qzqvk4qi43q62skkqmjibylf90yxw4qx0aql8") (f (quote (("tower-hyper"))))))

