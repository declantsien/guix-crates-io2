(define-module (crates-io to we tower-otel) #:use-module (crates-io))

(define-public crate-tower-otel-0.1.0 (c (n "tower-otel") (v "0.1.0") (d (list (d (n "http") (r "^0.2.10") (d #t) (k 0)) (d (n "opentelemetry") (r "^0.21.0") (k 0)) (d (n "opentelemetry-http") (r "^0.10.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3.2") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (k 0)) (d (n "tracing-opentelemetry") (r "^0.22") (k 0)))) (h "1v9j0ln02bs3355vshrkmgnp7r4p7q9yy9pkszy9ga58h4jy9wp0")))

(define-public crate-tower-otel-0.2.0 (c (n "tower-otel") (v "0.2.0") (d (list (d (n "http") (r "^1") (d #t) (k 0)) (d (n "opentelemetry") (r "^0.21.0") (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3.2") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (k 0)) (d (n "tracing-opentelemetry") (r "^0.22") (k 0)))) (h "10bvv27ljfp9d0kwwc3x2lqcbzr978jr81lgvccc2ymi391axry3")))

