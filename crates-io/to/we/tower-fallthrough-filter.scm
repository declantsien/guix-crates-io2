(define-module (crates-io to we tower-fallthrough-filter) #:use-module (crates-io))

(define-public crate-tower-fallthrough-filter-0.0.1 (c (n "tower-fallthrough-filter") (v "0.0.1") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 2)) (d (n "axum-test") (r "^14.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "00ikzj8k07w4i5lvpskrm6rgncdznv465jiayjjvnd4ladlwmb37")))

(define-public crate-tower-fallthrough-filter-0.0.2 (c (n "tower-fallthrough-filter") (v "0.0.2") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 2)) (d (n "axum-test") (r "^14.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0536gsaq70q4kfqdqp2vpk36xjbki7w21vp7b5mbrcjqddag9p6f")))

(define-public crate-tower-fallthrough-filter-0.0.3 (c (n "tower-fallthrough-filter") (v "0.0.3") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 2)) (d (n "axum-test") (r "^14.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.4") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "07xp5df9rbqcjzxj52r9f8gg62sx1ixkpvlb34qlx6akcr40hwgs") (f (quote (("default") ("async" "futures")))) (s 2) (e (quote (("futures" "dep:pin-project"))))))

