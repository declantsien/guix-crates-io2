(define-module (crates-io to we tower-vm) #:use-module (crates-io))

(define-public crate-tower-vm-0.1.0 (c (n "tower-vm") (v "0.1.0") (h "0d78jv6gdr6qb08c237n63r44abdp5z8n92k5wnj655dkhn8l5ih")))

(define-public crate-tower-vm-0.2.0 (c (n "tower-vm") (v "0.2.0") (h "1dp9hypydqk1l529s2sid4ykk61kdfbjbma9vway252cld94x5g0")))

(define-public crate-tower-vm-0.3.0 (c (n "tower-vm") (v "0.3.0") (h "189zh2fl5isjid3fpzfk35pzxbvvng49vrj1l3swlz6qbyby66ms")))

