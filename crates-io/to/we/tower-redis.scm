(define-module (crates-io to we tower-redis) #:use-module (crates-io))

(define-public crate-tower-redis-0.1.0 (c (n "tower-redis") (v "0.1.0") (d (list (d (n "redis") (r "^0.21") (f (quote ("connection-manager" "tokio-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "02x7mcl71p1j2ciml59ix5743ima9vlrx347kxmyxyy07x9wvx8f")))

(define-public crate-tower-redis-0.2.0 (c (n "tower-redis") (v "0.2.0") (d (list (d (n "redis") (r "^0.21") (f (quote ("connection-manager" "tokio-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (k 0)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)))) (h "1qp2079j5yal06l20m0zdysp4bql3ahsbxa4vhgbczim84x1fp35") (f (quote (("util" "tower/util") ("default"))))))

(define-public crate-tower-redis-0.2.1 (c (n "tower-redis") (v "0.2.1") (d (list (d (n "redis") (r "^0.23") (f (quote ("connection-manager" "tokio-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (k 0)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)))) (h "1h6anhmd625y72s11xnmv6ia79ci27m3s6p6iraajrm97cipbby8") (f (quote (("util" "tower/util") ("default"))))))

(define-public crate-tower-redis-0.3.0 (c (n "tower-redis") (v "0.3.0") (d (list (d (n "redis") (r "^0.25") (f (quote ("connection-manager" "tokio-comp"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (k 0)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)))) (h "1g9hk3bb0w0q8icg1dkp9q4k08jkln02ig8m1l1vzzi6n2yb87fb") (f (quote (("util" "tower/util") ("default" "util"))))))

