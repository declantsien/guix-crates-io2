(define-module (crates-io to we tower-async-service) #:use-module (crates-io))

(define-public crate-tower-async-service-0.1.0 (c (n "tower-async-service") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1v77wlsr6cmcd41kjykji6mg7jwiqn9fa2bw7yqfllvks031iiqd")))

(define-public crate-tower-async-service-0.1.1 (c (n "tower-async-service") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "013grzll82q0q59rpm0f0sliq4szb91rs2wpyylyz9zlqgbibc7d")))

(define-public crate-tower-async-service-0.2.0 (c (n "tower-async-service") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1bvh983i5rkz2idl6r0h21dc1951y50w17pndsrbjp732g736vfk")))

