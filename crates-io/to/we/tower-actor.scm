(define-module (crates-io to we tower-actor) #:use-module (crates-io))

(define-public crate-tower-actor-0.1.0 (c (n "tower-actor") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lzxfvyj4g5m4r4aqgm7rfr7hxlzvlp5lkaz6c40li775vlfb0mq")))

