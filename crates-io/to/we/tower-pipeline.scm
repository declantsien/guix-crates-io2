(define-module (crates-io to we tower-pipeline) #:use-module (crates-io))

(define-public crate-tower-pipeline-0.1.0 (c (n "tower-pipeline") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "0bmvp13q9i29zf4zvhjsqbkw6xdkishdffngzqhm9kxm9gf9l10v")))

