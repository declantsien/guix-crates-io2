(define-module (crates-io to we tower-default-headers) #:use-module (crates-io))

(define-public crate-tower-default-headers-0.1.1 (c (n "tower-default-headers") (v "0.1.1") (d (list (d (n "axum") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "1fb6rif32pmbsrwk25xi83n5lxjdpapnpnddjyffyayllnzz7i7j")))

