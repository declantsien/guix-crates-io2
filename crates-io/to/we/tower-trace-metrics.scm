(define-module (crates-io to we tower-trace-metrics) #:use-module (crates-io))

(define-public crate-tower-trace-metrics-0.1.0 (c (n "tower-trace-metrics") (v "0.1.0") (d (list (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "metrics") (r "^0.20.1") (d #t) (k 0)) (d (n "tower-http") (r "^0.3.0") (f (quote ("trace"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "182rcklxiikvqdkkq0zff663l4ms8kp2wgjn41q6a0gkd6d7v51c")))

