(define-module (crates-io to we tower-make) #:use-module (crates-io))

(define-public crate-tower-make-0.1.0-alpha.1 (c (n "tower-make") (v "0.1.0-alpha.1") (d (list (d (n "tokio-io") (r "= 0.2.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "053pk6jpmcq3g5dxlcrr4xf9anqnzdjdb173crdhbd6l49nrsfvy") (f (quote (("io" "tokio-io"))))))

(define-public crate-tower-make-0.1.0-alpha.2 (c (n "tower-make") (v "0.1.0-alpha.2") (d (list (d (n "tokio-io") (r "= 0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "1hqhj3zp8ycczq6j2babrzrykxrpc52yclb905qfcaly8415v27s") (f (quote (("io" "tokio-io"))))))

(define-public crate-tower-make-0.3.0-alpha.1 (c (n "tower-make") (v "0.3.0-alpha.1") (d (list (d (n "tokio-io") (r "= 0.2.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "142b9nianirkla3dllnlmc8355q2klhlfv651mwivqj03zhklldz") (f (quote (("io" "tokio-io"))))))

(define-public crate-tower-make-0.3.0-alpha.2 (c (n "tower-make") (v "0.3.0-alpha.2") (d (list (d (n "tokio-io") (r "^0.2.0-alpha.5") (o #t) (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "0dp0m3jm6xxzqcz62xqba2xfn0hnaxys58rsyp042iaf5aygdagz") (f (quote (("io" "tokio-io"))))))

(define-public crate-tower-make-0.3.0-alpha.2a (c (n "tower-make") (v "0.3.0-alpha.2a") (d (list (d (n "tokio-io") (r "^0.2.0-alpha.6") (o #t) (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "1kc4nkf3fkm25b6mdh6r2a3adqm422day3hii1fsrr6d83flfv9i") (f (quote (("io" "tokio-io"))))))

(define-public crate-tower-make-0.3.0 (c (n "tower-make") (v "0.3.0") (d (list (d (n "tokio") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "0rf9rnjiwffd6k12qkhhilj6l584ckvx9zvp92zn80sach6kfl6f") (f (quote (("connect" "tokio"))))))

