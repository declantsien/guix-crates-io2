(define-module (crates-io to we tower-ready-cache) #:use-module (crates-io))

(define-public crate-tower-ready-cache-0.3.0 (c (n "tower-ready-cache") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)) (d (n "tower-test") (r "^0.3") (d #t) (k 2)))) (h "1mr8fyz5phx5j8zs9x2a3lpdxis03zsiqa4ymz0432mn0053s65j")))

(define-public crate-tower-ready-cache-0.3.1 (c (n "tower-ready-cache") (v "0.3.1") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)) (d (n "tower-test") (r "^0.3") (d #t) (k 2)))) (h "07qyb5mv4xjhvwjxhkmivhahrb8wnf0cfcn85rz2d0al1ribdasf")))

