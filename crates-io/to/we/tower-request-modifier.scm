(define-module (crates-io to we tower-request-modifier) #:use-module (crates-io))

(define-public crate-tower-request-modifier-0.1.0 (c (n "tower-request-modifier") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.2") (d #t) (k 0)) (d (n "tower-test") (r "^0.1") (d #t) (k 2)))) (h "1x9a259hxjvkiz5qkrinm0pq7gpfrhh059whc5f7lpxs05x2bbzn")))

