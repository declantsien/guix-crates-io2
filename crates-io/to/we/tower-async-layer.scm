(define-module (crates-io to we tower-async-layer) #:use-module (crates-io))

(define-public crate-tower-async-layer-0.1.0 (c (n "tower-async-layer") (v "0.1.0") (h "0lpkhz8i593hpcq7hkbalxkysypjwmdvncvn2rwibcd5kvmc83va")))

(define-public crate-tower-async-layer-0.1.1 (c (n "tower-async-layer") (v "0.1.1") (h "1l2mambblgg5hpws7hvgkfq35szwfsmyn43fp1jypx4skcb3ggqc")))

(define-public crate-tower-async-layer-0.2.0 (c (n "tower-async-layer") (v "0.2.0") (h "0f9j1gcr1xwjgpsksjgibgf3ggkykrvjsnsak4z320isva9d4h1n")))

