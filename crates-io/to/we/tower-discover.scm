(define-module (crates-io to we tower-discover) #:use-module (crates-io))

(define-public crate-tower-discover-0.0.0 (c (n "tower-discover") (v "0.0.0") (h "1qd57k50kp5fhb9km62bk1kxnhaxl2pd0w6dl68544myhvv68mfd")))

(define-public crate-tower-discover-0.1.0 (c (n "tower-discover") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)))) (h "0iyy5g61aqryqp4ab6j8ca9xxb070dbhxz8qbpb690gphqi679vk")))

(define-public crate-tower-discover-0.3.0-alpha.1 (c (n "tower-discover") (v "0.3.0-alpha.1") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "pin-project") (r "= 0.4.0-alpha.11") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "1k7r5x3sr6f34i9scmzcrcgpvkc397wzmp6jbdikq2iwb8sihpcx")))

(define-public crate-tower-discover-0.3.0-alpha.2 (c (n "tower-discover") (v "0.3.0-alpha.2") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "1sd2d0wjphmggxvakrpsw0x4jd2a700ji8f96mvsmrvc50hnxa7v")))

(define-public crate-tower-discover-0.3.0 (c (n "tower-discover") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "0aiwpxjsnhz1560w9whxpy6d0cxv6s0z5pwmqsf2ckf5qc050sqg")))

