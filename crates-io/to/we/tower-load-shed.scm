(define-module (crates-io to we tower-load-shed) #:use-module (crates-io))

(define-public crate-tower-load-shed-0.1.0 (c (n "tower-load-shed") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1.1") (d #t) (k 2)) (d (n "tower-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)) (d (n "tower-test") (r "^0.1.0") (d #t) (k 2)))) (h "0sfpikmg7x9nrinp1f5vy89rcm31xjpb5fc7vc245n33zddszyq4")))

(define-public crate-tower-load-shed-0.3.0-alpha.1 (c (n "tower-load-shed") (v "0.3.0-alpha.1") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.18") (d #t) (k 2)) (d (n "pin-project") (r "= 0.4.0-alpha.11") (f (quote ("project_attr"))) (d #t) (k 0)) (d (n "tokio-test") (r "= 0.2.0-alpha.4") (d #t) (k 2)) (d (n "tower-layer") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "tower-test") (r "= 0.3.0-alpha.1") (d #t) (k 2)))) (h "09nml7ps8f35vag1yp9g2hspxdklk6gb92a6nj9gycb0lslzxzx0")))

(define-public crate-tower-load-shed-0.3.0-alpha.2 (c (n "tower-load-shed") (v "0.3.0-alpha.2") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tower-layer") (r "= 0.3.0-alpha.2") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "1q9dxdq6n97jlgnxvcmwcmg57xrm5k2zpz7x402rdmipg798inm2")))

(define-public crate-tower-load-shed-0.3.0 (c (n "tower-load-shed") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)) (d (n "tower-test") (r "^0.3") (d #t) (k 2)))) (h "0kmfy08nq1hjj444rdw9qrwkxbhda4i6jjzbbwqxqwq1j0iiw0lz")))

