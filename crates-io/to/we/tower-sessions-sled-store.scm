(define-module (crates-io to we tower-sessions-sled-store) #:use-module (crates-io))

(define-public crate-tower-sessions-sled-store-0.1.0 (c (n "tower-sessions-sled-store") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 2)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower-sessions") (r "^0.10.4") (k 0)) (d (n "tower-sessions") (r "^0.10.4") (f (quote ("axum-core"))) (d #t) (k 2)))) (h "1i8hgs9inq82qrl0s6xcyw5sviz8khzz69xypaah2jkccsr2gf6r")))

