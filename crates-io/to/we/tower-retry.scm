(define-module (crates-io to we tower-retry) #:use-module (crates-io))

(define-public crate-tower-retry-0.0.0 (c (n "tower-retry") (v "0.0.0") (h "18p7xakmx02ydpkidd863gy2q3s0xbq515h5l2q9n67vmi3qalha")))

(define-public crate-tower-retry-0.1.0 (c (n "tower-retry") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.2") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2.4") (d #t) (k 0)) (d (n "tower-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)) (d (n "tower-test") (r "^0.1.0") (d #t) (k 2)))) (h "1xrn23vz8biqn5jjkfrd19biwhcbk0whjyraxmvg4qah2a40bs09")))

(define-public crate-tower-retry-0.3.0-alpha.1 (c (n "tower-retry") (v "0.3.0-alpha.1") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.18") (d #t) (k 2)) (d (n "pin-project") (r "= 0.4.0-alpha.11") (f (quote ("project_attr"))) (d #t) (k 0)) (d (n "tokio-test") (r "= 0.2.0-alpha.4") (d #t) (k 2)) (d (n "tokio-timer") (r "= 0.3.0-alpha.4") (d #t) (k 0)) (d (n "tower-layer") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "tower-test") (r "= 0.3.0-alpha.1") (d #t) (k 2)))) (h "08f67c0cq4xn66dg5xcx4825q8lsdhp5miyfk3qagrw11yvamax7")))

(define-public crate-tower-retry-0.3.0-alpha.2 (c (n "tower-retry") (v "0.3.0-alpha.2") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio-timer") (r "= 0.3.0-alpha.6") (d #t) (k 0)) (d (n "tower-layer") (r "= 0.3.0-alpha.2") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "0626m2qvwiy7zwf2cgn2bx2ghlrqaraf43n5206abmbhhn4ppcwb")))

(define-public crate-tower-retry-0.3.0 (c (n "tower-retry") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (k 2)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "test-util"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)) (d (n "tower-test") (r "^0.3") (d #t) (k 2)))) (h "0lm9nv4qcvk44nr443xkkpcnaklfzq4b6clj9myrby52m9b7jwp6")))

