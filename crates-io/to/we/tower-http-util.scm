(define-module (crates-io to we tower-http-util) #:use-module (crates-io))

(define-public crate-tower-http-util-0.1.0 (c (n "tower-http-util") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "http") (r "^0.1.16") (d #t) (k 0)) (d (n "http-body") (r "^0.1") (d #t) (k 0)) (d (n "http-connection") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-buf") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)))) (h "1hzg3qng3m5zh8ynvjnml62cn25zfqvjpraymb6rk95x4fgafas4")))

