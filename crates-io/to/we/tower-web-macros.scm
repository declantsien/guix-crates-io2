(define-module (crates-io to we tower-web-macros) #:use-module (crates-io))

(define-public crate-tower-web-macros-0.1.0 (c (n "tower-web-macros") (v "0.1.0") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0436dk8fbj6mnl4w9fz5r9ihwdwxaly86apq85rgw1a3dqjid1x4")))

(define-public crate-tower-web-macros-0.2.0 (c (n "tower-web-macros") (v "0.2.0") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0smykr9plqzbgmbazpmrjkg1x6c8d174gmk64yhah6h7387vj82l")))

(define-public crate-tower-web-macros-0.2.1 (c (n "tower-web-macros") (v "0.2.1") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0kj0icliclzny5vivaa5ykwvvg7y62p1xp6f7svavb1j4kaf3mi1")))

(define-public crate-tower-web-macros-0.3.0 (c (n "tower-web-macros") (v "0.3.0") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0965yx1cy5faprfrbhmhhjwk3iyyb490a2wfl5c72lsj8cjh21y3")))

(define-public crate-tower-web-macros-0.3.1 (c (n "tower-web-macros") (v "0.3.1") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "182mvlw1801kqciqxffjdmznkpimhb8mxsknm2nmrixbiw05gb96")))

(define-public crate-tower-web-macros-0.3.2 (c (n "tower-web-macros") (v "0.3.2") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "167w01qw3zijrrp0a1bg14zw4pbkrkmbb4qargdrzfdlz9kv0ws6")))

(define-public crate-tower-web-macros-0.3.3 (c (n "tower-web-macros") (v "0.3.3") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1gyazwg6crff7hv1fg1l8yfyw4v0cyfbknzdw9k7pn1isrla9380")))

(define-public crate-tower-web-macros-0.3.4 (c (n "tower-web-macros") (v "0.3.4") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "03gsjyb719m8h4w8kvv47b3ikhr3zld55abs8li1z5q5ycsz3kw2")))

(define-public crate-tower-web-macros-0.3.5 (c (n "tower-web-macros") (v "0.3.5") (d (list (d (n "http") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "12vs8yblf1j36inbsv09vl23raj6sgsximl5wppmcxxp82ixl70n")))

