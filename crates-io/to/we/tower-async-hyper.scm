(define-module (crates-io to we tower-async-hyper) #:use-module (crates-io))

(define-public crate-tower-async-hyper-0.1.0 (c (n "tower-async-hyper") (v "0.1.0") (d (list (d (n "http") (r "^1") (d #t) (k 2)) (d (n "http-body") (r "^1") (d #t) (k 0)) (d (n "hyper") (r "^1.0") (f (quote ("http1" "http2" "server"))) (d #t) (k 0)) (d (n "hyper-util") (r "^0.1") (f (quote ("server" "server-auto" "tokio"))) (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower-async-service") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "17y53q0bc2snyglm1cawr0syzfdrw0alpmxqb579jdhadkbdmr4p")))

