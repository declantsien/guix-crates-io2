(define-module (crates-io to we tower-async-test) #:use-module (crates-io))

(define-public crate-tower-async-test-0.1.0 (c (n "tower-async-test") (v "0.1.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower-async-layer") (r "^0.1") (d #t) (k 0)) (d (n "tower-async-service") (r "^0.1") (d #t) (k 0)))) (h "1lvqsd1ahwviam6c17905hfzhjcrih3s8h6lh956i8cm212marrf")))

(define-public crate-tower-async-test-0.1.1 (c (n "tower-async-test") (v "0.1.1") (d (list (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower-async-layer") (r "^0.1") (d #t) (k 0)) (d (n "tower-async-service") (r "^0.1") (d #t) (k 0)))) (h "0w70ffzv1p9g4w3h67mbf2scrnbfx8yrpkn1inh3n2470zl9b0s7")))

(define-public crate-tower-async-test-0.2.0 (c (n "tower-async-test") (v "0.2.0") (d (list (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tower-async-layer") (r "^0.2") (d #t) (k 0)) (d (n "tower-async-service") (r "^0.2") (d #t) (k 0)))) (h "1ymw56k70ysahkrjrnxappwq9xgbyb4hnviifqaghnh4hlqjvy6j")))

