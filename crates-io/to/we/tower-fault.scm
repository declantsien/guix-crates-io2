(define-module (crates-io to we tower-fault) #:use-module (crates-io))

(define-public crate-tower-fault-0.0.5 (c (n "tower-fault") (v "0.0.5") (d (list (d (n "axum") (r "^0.4") (d #t) (k 2)) (d (n "lambda_http") (r "^0.5") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt" "macros"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 0)))) (h "1f0b47cva09xi5fqqc84c3ds4m76ciy46ig2iy7cdixqrcwcy7z9") (f (quote (("latency" "tokio") ("full" "error" "latency") ("error" "tokio") ("default" "full"))))))

