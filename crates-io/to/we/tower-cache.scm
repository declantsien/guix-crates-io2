(define-module (crates-io to we tower-cache) #:use-module (crates-io))

(define-public crate-tower-cache-0.0.1 (c (n "tower-cache") (v "0.0.1") (d (list (d (n "lru") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 0)))) (h "0n5r8nx2ncwgg2hiawv6q08svgv9rvs5vw2hf8yiz7mdsdyq4bys") (f (quote (("default" "lru"))))))

