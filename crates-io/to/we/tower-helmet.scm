(define-module (crates-io to we tower-helmet) #:use-module (crates-io))

(define-public crate-tower-helmet-0.1.0 (c (n "tower-helmet") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)))) (h "1by6zd45vlygd01js2ldhy4gigs7q57dn8nzisadypa4s0mz2vz1")))

(define-public crate-tower-helmet-0.2.0 (c (n "tower-helmet") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)))) (h "0s7k2v7ri2gxz5ifwkpdw38fkfnfl2vmp1ysb4yjizf9ng0a105j")))

(define-public crate-tower-helmet-0.2.1 (c (n "tower-helmet") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)))) (h "0lcngawi6b2lfky4w436yi4i6b5lznqds3v2brgx2mhcspj661bs")))

(define-public crate-tower-helmet-0.3.0 (c (n "tower-helmet") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.18") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3.1") (d #t) (k 0)) (d (n "tower-service") (r "^0.3.1") (d #t) (k 0)))) (h "1k3wwydq643hs0kr49ckldcqph46c8s4787x1j440dl6i30b4xps")))

