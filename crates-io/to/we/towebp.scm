(define-module (crates-io to we towebp) #:use-module (crates-io))

(define-public crate-towebp-0.1.0 (c (n "towebp") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "webp") (r "^0.1") (d #t) (k 0)))) (h "15l135vrvdx9506lvxzl8w2s1q9rfij438dvbya8b6l5mwp6a0wn")))

(define-public crate-towebp-0.2.0 (c (n "towebp") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "webp") (r "^0.2") (d #t) (k 0)))) (h "0gy91zqs2x6m3zbvfg6gqh51gcdk6ndsca65vs4cgzdqgl2lszx6")))

(define-public crate-towebp-0.2.1 (c (n "towebp") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.1") (d #t) (k 0)))) (h "0xlw7afhhmz9q0sa2pcikj3sc4j0cjab5q4ykw3vawlz7528rhgg")))

