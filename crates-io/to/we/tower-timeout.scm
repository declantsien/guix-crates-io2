(define-module (crates-io to we tower-timeout) #:use-module (crates-io))

(define-public crate-tower-timeout-0.0.0 (c (n "tower-timeout") (v "0.0.0") (h "0mmsqzh0yg26cgifvgq3y3r4pjx54qrjp4l5fyk3jlmk69ppygkw")))

(define-public crate-tower-timeout-0.1.0 (c (n "tower-timeout") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.6") (d #t) (k 0)) (d (n "tower-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)))) (h "0c9wygqkjjynggf7l6fl7qndrfp5phd6dp28f739sn4783n7k8fs")))

(define-public crate-tower-timeout-0.1.1 (c (n "tower-timeout") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2.6") (d #t) (k 0)) (d (n "tower-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)))) (h "1abx8h96cyjsfnd9j8iwv8v7smclq466z30ajh8ghmnhzg1bn1jw")))

(define-public crate-tower-timeout-0.3.0-alpha.1 (c (n "tower-timeout") (v "0.3.0-alpha.1") (d (list (d (n "pin-project") (r "= 0.4.0-alpha.11") (d #t) (k 0)) (d (n "tokio-timer") (r "= 0.3.0-alpha.4") (d #t) (k 0)) (d (n "tower-layer") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "17rmg7qi2lgazrn82xfw42qrscw6s4x376gxnkp6kri7qxp6c1p4")))

(define-public crate-tower-timeout-0.3.0-alpha.2 (c (n "tower-timeout") (v "0.3.0-alpha.2") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio-timer") (r "= 0.3.0-alpha.6") (d #t) (k 0)) (d (n "tower-layer") (r "= 0.3.0-alpha.2") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "1zrm5f323d8c796q0f6r7lm9zv74i6d11jy395wlq9jplzl7sgx4")))

(define-public crate-tower-timeout-0.3.0 (c (n "tower-timeout") (v "0.3.0") (d (list (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "07mkwjr9hqdr4w84h2ansanl0ba8iihc1bpa4f497gjpncj8jyqj")))

