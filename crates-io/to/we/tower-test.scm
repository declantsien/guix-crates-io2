(define-module (crates-io to we tower-test) #:use-module (crates-io))

(define-public crate-tower-test-0.0.0 (c (n "tower-test") (v "0.0.0") (h "0r39gw68yg6ik5qpr0zlc3pj52cd6nw0s2by24lvwqaq77741a75")))

(define-public crate-tower-test-0.1.0 (c (n "tower-test") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tokio-sync") (r "^0.1.3") (d #t) (k 0)) (d (n "tower") (r "^0.1.0") (d #t) (k 2)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)))) (h "1pzvyq6n795pl4hi4pmd8na24g98x49b798439l8zig26r8b584r")))

(define-public crate-tower-test-0.3.0-alpha.1 (c (n "tower-test") (v "0.3.0-alpha.1") (d (list (d (n "futures-executor-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "pin-project") (r "= 0.4.0-alpha.11") (d #t) (k 0)) (d (n "tokio-sync") (r "= 0.2.0-alpha.4") (d #t) (k 0)) (d (n "tokio-test") (r "= 0.2.0-alpha.4") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "1qqw6lbzl6ja02yykpgbr6hvwqlldw2vhwr1pgqgdag2f8px0x9m")))

(define-public crate-tower-test-0.3.0-alpha.2 (c (n "tower-test") (v "0.3.0-alpha.2") (d (list (d (n "futures-executor-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "futures-util-preview") (r "= 0.3.0-alpha.19") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio-sync") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-test") (r "= 0.2.0-alpha.6") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "148j51az19zsm7ikg8lc6b21ks4f0w1y27ma2863gixp6a232w72")))

(define-public crate-tower-test-0.3.0 (c (n "tower-test") (v "0.3.0") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.2") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "1j2k07g3z8ascq7r30bmw3b75v8lhd63mhfl60y59a74q71bp94v")))

(define-public crate-tower-test-0.4.0 (c (n "tower-test") (v "0.4.0") (d (list (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 0)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "19zgjwzr9216yg1ayrnsly06lqdv96m2z1xq0bmf9fgazxrnfm54")))

