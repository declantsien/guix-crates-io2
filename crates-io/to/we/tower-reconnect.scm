(define-module (crates-io to we tower-reconnect) #:use-module (crates-io))

(define-public crate-tower-reconnect-0.0.0 (c (n "tower-reconnect") (v "0.0.0") (h "19p5n01nx1bx78ffbfrk3sw5ha3yslriwaayfd4zkk7wcvvim2nl")))

(define-public crate-tower-reconnect-0.3.0-alpha.1 (c (n "tower-reconnect") (v "0.3.0-alpha.1") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "pin-project") (r "= 0.4.0-alpha.11") (d #t) (k 0)) (d (n "tower-make") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)))) (h "09i5j09h0g1m1c2m2y4dgksg911assgl3pimrbwgyrm56qik057z")))

(define-public crate-tower-reconnect-0.3.0-alpha.2 (c (n "tower-reconnect") (v "0.3.0-alpha.2") (d (list (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "tower-make") (r "= 0.3.0-alpha.2a") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 0)))) (h "01qr7zs5b31vzlanxympwws6yablya90sb452s05alfrhpvhyfzc")))

