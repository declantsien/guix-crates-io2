(define-module (crates-io to we tower-service) #:use-module (crates-io))

(define-public crate-tower-service-0.0.0 (c (n "tower-service") (v "0.0.0") (h "183kgndrj00aig5pw6n9qsyx9k8qmwl0c943z57isx7m6pay9z6l")))

(define-public crate-tower-service-0.1.0 (c (n "tower-service") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)))) (h "11y9s8b7zb21k2h3g1nxa6dh7rzb72i1d1fssk9y7gzifypp4bxk")))

(define-public crate-tower-service-0.2.0 (c (n "tower-service") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)))) (h "1ykvhag9p3rc8r34l2z2p71mj5cz9i4idzbdvvw34dyj6y3ckh1c")))

(define-public crate-tower-service-0.3.0-alpha.1 (c (n "tower-service") (v "0.3.0-alpha.1") (h "10rbh8ggpsn7mn4rmd3ax1cvw0xpx44dbmmmz6ip97317kw05ckv")))

(define-public crate-tower-service-0.3.0-alpha.2 (c (n "tower-service") (v "0.3.0-alpha.2") (h "1ymx3qibg9dy03xdm5zpjms8a0pq76ivy654pr1wwrnrdhwkgzv3")))

(define-public crate-tower-service-0.3.0 (c (n "tower-service") (v "0.3.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 2)))) (h "0q4q53w82w1wd71x7vbspg2l3jicb6al2w1qdwxmnjrz8jzvd1z9")))

(define-public crate-tower-service-0.3.1 (c (n "tower-service") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)) (d (n "tower-layer") (r "^0.3") (d #t) (k 2)))) (h "1iih764s3f6vlkspfmr72fkrs2lw1v3wiqmc6bd5zq1hdlfzs39n")))

(define-public crate-tower-service-0.3.2 (c (n "tower-service") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)) (d (n "tower-layer") (r "^0.3") (d #t) (k 2)))) (h "0lmfzmmvid2yp2l36mbavhmqgsvzqf7r2wiwz73ml4xmwaf1rg5n")))

