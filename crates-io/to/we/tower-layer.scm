(define-module (crates-io to we tower-layer) #:use-module (crates-io))

(define-public crate-tower-layer-0.1.0 (c (n "tower-layer") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "tower-service") (r "^0.2.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "0a434nxhhfcy2n2s7f0fny2imvvdqrnh7pm63pscip071khhgpqd")))

(define-public crate-tower-layer-0.3.0-alpha.1 (c (n "tower-layer") (v "0.3.0-alpha.1") (d (list (d (n "futures-core-preview") (r "= 0.3.0-alpha.18") (d #t) (k 0)) (d (n "tower-service") (r "= 0.3.0-alpha.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "0sm55rmc9s5n8q4c6zmqxjags67wsps17qn70zk0kycmckilv70g")))

(define-public crate-tower-layer-0.3.0-alpha.2 (c (n "tower-layer") (v "0.3.0-alpha.2") (d (list (d (n "tower-service") (r "= 0.3.0-alpha.2") (d #t) (k 2)))) (h "1qnjs9mcbsf2h6nq0fc9fhs8a4aip8479pwxaw3ppmb4pqjqcia4")))

(define-public crate-tower-layer-0.3.0 (c (n "tower-layer") (v "0.3.0") (d (list (d (n "tower-service") (r "^0.3.0") (d #t) (k 2)))) (h "1p6i9rn5d98wsx6hi4hbxh2xqh2clwz0blcm6jrqiciq4rpnapd3")))

(define-public crate-tower-layer-0.3.1 (c (n "tower-layer") (v "0.3.1") (d (list (d (n "tower") (r "^0.3") (d #t) (k 2)) (d (n "tower-service") (r "^0.3.0") (d #t) (k 2)))) (h "0qiskpgz6zzy3a894vyr9yzq0i7q142rciggc3wv1rizdm3cjfrl")))

(define-public crate-tower-layer-0.3.2 (c (n "tower-layer") (v "0.3.2") (d (list (d (n "tower") (r "^0.4") (d #t) (k 2)) (d (n "tower-service") (r "^0.3.0") (d #t) (k 2)))) (h "1l7i17k9vlssrdg4s3b0ia5jjkmmxsvv8s9y9ih0jfi8ssz8s362")))

