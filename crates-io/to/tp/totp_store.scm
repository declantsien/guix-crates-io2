(define-module (crates-io to tp totp_store) #:use-module (crates-io))

(define-public crate-totp_store-1.0.0 (c (n "totp_store") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1s2rbpfz45fr0ra8mhx6mgs02gxb00glr5yn8i2f6nx8msln1bf5") (y #t)))

