(define-module (crates-io to tp totp_embed) #:use-module (crates-io))

(define-public crate-totp_embed-1.0.3 (c (n "totp_embed") (v "1.0.3") (d (list (d (n "digest") (r "^0.10") (k 0)) (d (n "hmac") (r "^0.12") (k 0)) (d (n "koibumi-base32") (r "^0.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.10") (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0p9af3z5ffgri54gj5jg9b0zd8kyz5ljvr7lyzzrna4m2s45a8hk") (f (quote (("std" "hmac/std" "digest/std" "sha-1/std" "sha2/std") ("default" "std")))) (y #t)))

(define-public crate-totp_embed-1.0.4 (c (n "totp_embed") (v "1.0.4") (d (list (d (n "digest") (r "^0.10") (k 0)) (d (n "hmac") (r "^0.12") (k 0)) (d (n "koibumi-base32") (r "^0.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.10") (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0lv4nbx0gwysz95phgym52x7h0krj8x5wwp6p1jbs0x50kzkkngx") (f (quote (("std" "hmac/std" "digest/std" "sha-1/std" "sha2/std")))) (y #t)))

(define-public crate-totp_embed-1.0.5 (c (n "totp_embed") (v "1.0.5") (d (list (d (n "digest") (r "^0.10") (k 0)) (d (n "hmac") (r "^0.12") (k 0)) (d (n "koibumi-base32") (r "^0.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.10") (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0pkwwks391vmlgi71sj34l75669gnrddbx20f5babij8cy7h4sr7") (f (quote (("std" "hmac/std" "digest/std" "sha-1/std" "sha2/std"))))))

