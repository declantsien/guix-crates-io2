(define-module (crates-io to tp totpgen) #:use-module (crates-io))

(define-public crate-totpgen-0.1.0 (c (n "totpgen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1hr1h028yh7qldhkd9ycp3s5iy851sqjp5gd78w3ygxnxjr22v0h")))

(define-public crate-totpgen-0.1.1 (c (n "totpgen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1syhcr31vqbip20mphbix3h84x7siyc9lr970g0lzd76d7d3jf42")))

(define-public crate-totpgen-0.1.2 (c (n "totpgen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1haskk2wqhqkdm67m7bpbv412nrxsyw2jgs2a3cac18y9kiql943")))

