(define-module (crates-io to tp totper) #:use-module (crates-io))

(define-public crate-totper-0.1.0 (c (n "totper") (v "0.1.0") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "totp-lite") (r "^2.0") (d #t) (k 0)))) (h "1x6b867dwkvnic7ljidx475ghyqmlacyzn6n9lfdpmwf39n9brq8")))

(define-public crate-totper-0.1.1 (c (n "totper") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "totp-lite") (r "^2.0") (d #t) (k 0)))) (h "0cnig9j6jjn2fw5zf7gdi6q83lxa9v5x6d4wrikrk0fsip3fi75m")))

(define-public crate-totper-0.1.2 (c (n "totper") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "totp-lite") (r "^2.0") (d #t) (k 0)))) (h "0nsyl381k93xslg3yminkl1qd40wzjdkj0gi3zhy1x7016z2c0wa")))

