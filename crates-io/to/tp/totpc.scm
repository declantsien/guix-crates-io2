(define-module (crates-io to tp totpc) #:use-module (crates-io))

(define-public crate-totpc-1.0.0 (c (n "totpc") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0mp88v4jjgx8ylx3m1gk468f27cf96xzwdj38rnlqj7j0nrfsmx0")))

(define-public crate-totpc-1.0.1 (c (n "totpc") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1l28adra4cbik408c479kskbk4f1ssvsmj02j9yzpp97myz2pj7h")))

(define-public crate-totpc-1.1.0 (c (n "totpc") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1wyz1rh52rwbck8xj4lqfwd1l8xz4r6p1zjk2n29k2wmqr3wfsp2")))

