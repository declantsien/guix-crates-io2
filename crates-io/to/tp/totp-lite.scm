(define-module (crates-io to tp totp-lite) #:use-module (crates-io))

(define-public crate-totp-lite-1.0.0 (c (n "totp-lite") (v "1.0.0") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0wi9lvbmxhshbvlk9n5qjngxjdqsw60v3q1bbk7ybv0had7ry43z")))

(define-public crate-totp-lite-1.0.1 (c (n "totp-lite") (v "1.0.1") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hmac") (r "^0.9") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1av4vrq8a7j6csdyggl63p8pzbm94q74hcn95mnnddjqbgvh14ka")))

(define-public crate-totp-lite-1.0.2 (c (n "totp-lite") (v "1.0.2") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hmac") (r "^0.10") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "10v3qpfkq4khirv3m63fdj7whxmgzk2mkqcs9pkiabvw9jqmx2bd")))

(define-public crate-totp-lite-1.0.3 (c (n "totp-lite") (v "1.0.3") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hmac") (r "^0.11") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "12ql4pi9q7sf5651588wia2l5h4mil3kv9jrrkib5gvlpvl0k05i")))

(define-public crate-totp-lite-1.1.0 (c (n "totp-lite") (v "1.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "koibumi-base32") (r "^0.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0lyklc1zs5ss89ds6y1iks98ibj9h00cfkbz7azilr0qkdzjh4ds") (y #t)))

(define-public crate-totp-lite-2.0.0 (c (n "totp-lite") (v "2.0.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "koibumi-base32") (r "^0.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1yi9s6firixay11rahqshdv07ih8i27fxqqrrshfk3wwbn3rdi2w")))

(define-public crate-totp-lite-2.0.1 (c (n "totp-lite") (v "2.0.1") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "koibumi-base32") (r "^0.0") (d #t) (k 2)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1hvnpv7nl79jp96w6g2j7l6xskl5qlx3h0qqf9zry68pvcs33r7q")))

