(define-module (crates-io to tp totp) #:use-module (crates-io))

(define-public crate-totp-0.1.0 (c (n "totp") (v "0.1.0") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "oath") (r "^0.1.4") (d #t) (k 0)))) (h "0c0zq1k12y168kb7rgxlx1x310dwdqm2klxqmglx34xq5fd4fcpj")))

(define-public crate-totp-0.2.0 (c (n "totp") (v "0.2.0") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "oath") (r "^0.1.4") (d #t) (k 0)))) (h "0qs7jcbw7vj9l3dvhx244hip5aiirl9a11r1x29xy7lwm5x9v3cz")))

(define-public crate-totp-0.2.1 (c (n "totp") (v "0.2.1") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "oath") (r "^0.1.4") (d #t) (k 0)))) (h "04iw3a3r2mm3nmqhrk8b88zblpsl7l2nwjwz6xwhj8hgkb3gv3mi")))

