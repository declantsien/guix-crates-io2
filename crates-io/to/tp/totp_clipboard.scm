(define-module (crates-io to tp totp_clipboard) #:use-module (crates-io))

(define-public crate-totp_clipboard-0.1.0 (c (n "totp_clipboard") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.4.6") (d #t) (k 0)) (d (n "gtk") (r "^0.2.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.4") (d #t) (k 0)) (d (n "libappindicator") (r "^0.3.0") (d #t) (k 0)) (d (n "libreauth") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hgifsvf89i5qy6q3hjxsqhdh5wy6vjm8fzs1ccx9gb5q3dqvnv7")))

(define-public crate-totp_clipboard-0.2.0 (c (n "totp_clipboard") (v "0.2.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "gtk") (r "^0.8.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.2") (d #t) (k 0)) (d (n "libappindicator") (r "^0.5.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1c7h39k6q0syjs92r5l0bjp1lza8j2db3hsnd6aijq6sjj800ry9")))

(define-public crate-totp_clipboard-0.2.1 (c (n "totp_clipboard") (v "0.2.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "gtk") (r "^0.8.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.2") (d #t) (k 0)) (d (n "libappindicator") (r "^0.5.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1navmbnc08kldd2g1q8g4szladzw2xmrc7rw67bmc9cjbwn5b022")))

(define-public crate-totp_clipboard-0.2.2 (c (n "totp_clipboard") (v "0.2.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "gtk") (r "^0.15") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.15") (d #t) (k 0)) (d (n "libappindicator") (r "^0.7.1") (d #t) (k 0)) (d (n "libreauth") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1xsz7mbxnswdqvpnjy6xwmlm54fqxxysni456ci859y8i63bg02w")))

