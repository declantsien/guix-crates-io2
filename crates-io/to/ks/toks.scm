(define-module (crates-io to ks toks) #:use-module (crates-io))

(define-public crate-toks-0.1.0 (c (n "toks") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.18.0") (d #t) (k 0)))) (h "1rh03c99r8qc36hvak8q0p7i2imlyp6dvg2rvibnwzxvir6sir2g")))

(define-public crate-toks-0.1.1 (c (n "toks") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.18.0") (d #t) (k 0)))) (h "15caqpdq6qwf5silzs8k7n8i1wpli18cqycyvqmd4r6m0xk5yj32")))

(define-public crate-toks-0.2.0 (c (n "toks") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.19.0") (d #t) (k 0)))) (h "1990r0pwm9dbhh49gpag3k8pnypagx7hs2b16rrs4fawcsxla098")))

(define-public crate-toks-0.3.0 (c (n "toks") (v "0.3.0") (d (list (d (n "html5ever") (r "^0.20.0") (d #t) (k 0)))) (h "0f2lppm03ql8f9lp5paz4q9al7vlgksckygxn58vkjhnzvyrc923")))

(define-public crate-toks-0.4.0 (c (n "toks") (v "0.4.0") (d (list (d (n "html5ever") (r "^0.21.0") (d #t) (k 0)))) (h "0dh895s3f6dw03khrg0d66954l4rqjdk50pw7fprbggkf5ij6xv5")))

(define-public crate-toks-0.5.0 (c (n "toks") (v "0.5.0") (d (list (d (n "html5ever") (r "^0.22.0") (d #t) (k 0)))) (h "0sjn899dvaad238ca4dd2v0dzgwk7j66crbf9xqk0k5gpz5df1gd")))

(define-public crate-toks-0.6.0 (c (n "toks") (v "0.6.0") (d (list (d (n "html5ever") (r "^0.22.2") (d #t) (k 0)))) (h "02dr40nwklr6w8rhgkrylzrgaiv5i8kq7pjrfc0cnxm3zfawqz8p")))

(define-public crate-toks-0.7.0 (c (n "toks") (v "0.7.0") (d (list (d (n "html5ever") (r "^0.22.3") (d #t) (k 0)))) (h "15x3w3qcs8l4xj3v74i3rq4h7skx61swimsfq6zk5r5zssqh23w2")))

(define-public crate-toks-0.8.0 (c (n "toks") (v "0.8.0") (d (list (d (n "html5ever") (r "^0.22.5") (d #t) (k 0)))) (h "1c9y5f76fz08ls8b7r6zpywh8mwwln1x4gwarpc2nld9hkbwdly3")))

(define-public crate-toks-0.9.0 (c (n "toks") (v "0.9.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "1ph8nhb6zwlag3an62vnv979sd570621kd6ka9pz02khgrvykm8d")))

(define-public crate-toks-1.0.0 (c (n "toks") (v "1.0.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)))) (h "032bpmfcw23hv80pgdys8xx936zw4drbd88wzfxz38gi111nrag9")))

(define-public crate-toks-1.1.0 (c (n "toks") (v "1.1.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1wm0yb4frnvl13s4zs3rra82f7ya6ffi50drk18rnqqwngrm29ga")))

(define-public crate-toks-1.2.0 (c (n "toks") (v "1.2.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)))) (h "1f39wzgclzjalad14f032dmdjhd9gj6smwrfav1s2i9nhzgjnn4b")))

