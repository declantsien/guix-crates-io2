(define-module (crates-io to pk topk8) #:use-module (crates-io))

(define-public crate-topk8-0.0.1 (c (n "topk8") (v "0.0.1") (d (list (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pkcs8") (r "^0.8.0") (f (quote ("alloc" "pem" "std"))) (d #t) (k 0)) (d (n "rsa") (r "^0.5.0") (f (quote ("alloc" "pem" "std"))) (d #t) (k 0)) (d (n "sec1") (r "^0.2.1") (f (quote ("alloc" "pkcs8" "pem" "std"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "184q68w0jq9d46238v4i3a2nx929mnkzkk3379xmspdi2qkpq5hh")))

