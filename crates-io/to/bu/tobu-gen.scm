(define-module (crates-io to bu tobu-gen) #:use-module (crates-io))

(define-public crate-tobu-gen-0.1.0 (c (n "tobu-gen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "tobu-format") (r "^0.1.0") (d #t) (k 0)))) (h "1hqhmnr0km3f6ph4j7458b7xlnm9bjhfd5grxp7l8y24r04514z7") (y #t)))

