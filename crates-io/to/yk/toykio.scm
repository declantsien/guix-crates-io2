(define-module (crates-io to yk toykio) #:use-module (crates-io))

(define-public crate-toykio-0.2.0 (c (n "toykio") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "15fml22629y0kz6i3jdcs62gkmmifcg84yg6as33jb722hcdivi7") (y #t)))

(define-public crate-toykio-0.2.1 (c (n "toykio") (v "0.2.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "00an44x9nkd7bp3ry5y0qk694jfbs1ldrjjslyffdici8fz3s8l3") (y #t)))

(define-public crate-toykio-0.2.2 (c (n "toykio") (v "0.2.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)))) (h "1ywx58ar6vjvjhhc8phblqz8c4ki4waa178karj2aba53djdcwp6") (y #t)))

