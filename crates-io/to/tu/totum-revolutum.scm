(define-module (crates-io to tu totum-revolutum) #:use-module (crates-io))

(define-public crate-totum-revolutum-0.2.2 (c (n "totum-revolutum") (v "0.2.2") (h "0ggc6fizly4bcqy0d4rwp79vrfgl636r9ca8563j6idhlcdwbjba")))

(define-public crate-totum-revolutum-0.2.3 (c (n "totum-revolutum") (v "0.2.3") (h "1bhl0061cjqhkn20pqsf7fmphxwch8jhkwzhx4y94vk3xi3liq5a")))

(define-public crate-totum-revolutum-0.2.4 (c (n "totum-revolutum") (v "0.2.4") (h "1ywkydyra8j509ampljsmg0bh9m9h1in9ygwqh8cqmx79jc5x16g") (r "1.56.1")))

(define-public crate-totum-revolutum-0.2.5 (c (n "totum-revolutum") (v "0.2.5") (h "190nnwnbq86fvnpmx3l6z3f90lqf4z4pbd69b21pk2pnrx3fnkw4") (r "1.56.1")))

