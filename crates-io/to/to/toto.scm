(define-module (crates-io to to toto) #:use-module (crates-io))

(define-public crate-toto-0.1.0 (c (n "toto") (v "0.1.0") (h "0vb5409givxd44868zifnvfippg8d85hni8hl4k2f5wx8vna9h9g")))

(define-public crate-toto-1.0.0 (c (n "toto") (v "1.0.0") (h "0xdd8553a0f0w1qc36h3yb7b0bd5b0yxzwlgjpyn9f0cx9ikdz7c")))

