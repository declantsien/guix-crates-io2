(define-module (crates-io to ns tonsoe) #:use-module (crates-io))

(define-public crate-tonsoe-0.0.1-alpha (c (n "tonsoe") (v "0.0.1-alpha") (h "1jd7ln8cgrqsl2wh0v9lkk6r9m90rwywmd9xvjihcm3g5cwbg3vw") (y #t)))

(define-public crate-tonsoe-0.0.1-pre-dev-1 (c (n "tonsoe") (v "0.0.1-pre-dev-1") (h "04zbdxbh3v87qmx6j0bvr6x831pa57d5mkiyib9fd51y83drypx8") (y #t)))

(define-public crate-tonsoe-0.0.1-pre-dev-2 (c (n "tonsoe") (v "0.0.1-pre-dev-2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.17.2") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "1lf46s0qxlmq590nkdv6i8wwylyl2iayxp57nkw6sxl2iry0qabc") (y #t)))

