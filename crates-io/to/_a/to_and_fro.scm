(define-module (crates-io to _a to_and_fro) #:use-module (crates-io))

(define-public crate-to_and_fro-0.1.0 (c (n "to_and_fro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "178cqz93n6rpg0gc5gldfb65pqwg095a534c0jp63xpli3c7g8yl")))

(define-public crate-to_and_fro-0.2.0 (c (n "to_and_fro") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1diy3mfbmydyd5g31mm11sh9cay03adcj9x7p24xwj9qgf380mkz")))

(define-public crate-to_and_fro-0.2.1 (c (n "to_and_fro") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1xlh5ibki7pnmnmyi13w70afh3vnfx69dqxy7rx2h722smrliw6w")))

(define-public crate-to_and_fro-0.3.0 (c (n "to_and_fro") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1m9hbqjd0szwcq946g8sskz4w6c7vpp4hjz17m166zkmirf194w1")))

(define-public crate-to_and_fro-0.3.1 (c (n "to_and_fro") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "066ljc31f52r0jv14vj7yr8ds7xk09wa7b1jvyybqpa9nhp0r2d5")))

(define-public crate-to_and_fro-0.3.2 (c (n "to_and_fro") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "082d4ph8z64fgrzrfv1d06cmabg6hr4k5bif9frjc1vhkjz5wa63")))

(define-public crate-to_and_fro-0.3.3 (c (n "to_and_fro") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ls95h89dvvphqxs02s3pb51jfnj87qniw2sl53ikvnmgcqasx62")))

(define-public crate-to_and_fro-0.3.4 (c (n "to_and_fro") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0pschqcrbgn0g0ixwnlb9gxhahd5yasmqmvkyxkfni9frz0q45fd")))

(define-public crate-to_and_fro-0.3.5 (c (n "to_and_fro") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0505vyvzrvqmrh7axldyfws9lv4n3gcw29z1wsk082kkd6yxrri6")))

(define-public crate-to_and_fro-0.4.0 (c (n "to_and_fro") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "14rfszj0364k1lp9qd13pffdjgfqkzbfvx437gxkaf50scc8frjs")))

(define-public crate-to_and_fro-0.5.0 (c (n "to_and_fro") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1myjhmrm2q87n1lpg9m2dws0vpsgiy4cc25yrsbx110qw37cn1q0")))

(define-public crate-to_and_fro-0.5.1 (c (n "to_and_fro") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1iydj62z9r3mhxsjk0id066qan2fi12q168azdf6fmbl4i4n4gb3")))

(define-public crate-to_and_fro-0.5.2 (c (n "to_and_fro") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0vlm8kgxlxb8h6mxb6qfxvr4v3f3pj302km8xks85ivznaywkp9f")))

(define-public crate-to_and_fro-0.5.3 (c (n "to_and_fro") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11n4pd28i974ipw3ad2dzicl8bxhr0a7wj46rqzlwwqwmpv6cfys")))

