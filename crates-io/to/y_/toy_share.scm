(define-module (crates-io to y_ toy_share) #:use-module (crates-io))

(define-public crate-toy_share-0.1.4 (c (n "toy_share") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "05xnc633vv37c5yqjskhbxvqc65lgmalhjvrznmm3sd4lq4vrswy")))

