(define-module (crates-io to y_ toy_xcb) #:use-module (crates-io))

(define-public crate-toy_xcb-0.2.0-beta.0 (c (n "toy_xcb") (v "0.2.0-beta.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "xcb") (r "^1.0.0-beta") (f (quote ("xlib_xcb" "xkb"))) (d #t) (k 0)) (d (n "xkbcommon") (r "^0.5.0-beta") (f (quote ("x11"))) (d #t) (k 0)))) (h "1sjcq1ws7hyzkgi606sxs486q358k8f8ivm80zh90v324yxycrf1")))

