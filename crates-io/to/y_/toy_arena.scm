(define-module (crates-io to y_ toy_arena) #:use-module (crates-io))

(define-public crate-toy_arena-0.1.0 (c (n "toy_arena") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "0yqn9wvaiarqp6xmpsrxfx6gi0pl7xz3vn9wjkxmlm8hrnzlsid9")))

(define-public crate-toy_arena-0.1.1 (c (n "toy_arena") (v "0.1.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1fq79l2x5n3n61cvcapv57aw4pxc321vxyz5lzvbd9kdqa5d7ni1")))

(define-public crate-toy_arena-0.1.2 (c (n "toy_arena") (v "0.1.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "16jfr904v3wvmm6i9zqfh760q7qya0ab4wsdaxl5mppc2vi2hpnc")))

(define-public crate-toy_arena-0.1.3 (c (n "toy_arena") (v "0.1.3") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1s5wv2fysjq2naagj84yych4z9n0xlwiwbwrfgrvnhmay36q3r86")))

(define-public crate-toy_arena-0.2.0 (c (n "toy_arena") (v "0.2.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "1fmxapm57ylw8mcghnip9mrpf2yicvlcnap0pssv5q2i9089zspj")))

