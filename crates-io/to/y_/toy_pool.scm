(define-module (crates-io to y_ toy_pool) #:use-module (crates-io))

(define-public crate-toy_pool-0.1.0 (c (n "toy_pool") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "00ggv9i11s0xsjpa0sy8j0qpzl1wcgbwghwjs3c12jgyzr62npkn")))

(define-public crate-toy_pool-0.1.1 (c (n "toy_pool") (v "0.1.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "05hixqjcsafngikr7rc2zy5q4wxyldql6fsmjh5yxza4yv4sddnv")))

