(define-module (crates-io to p_ top_n_tail) #:use-module (crates-io))

(define-public crate-top_n_tail-0.1.0 (c (n "top_n_tail") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "0kirwcazxsymk94pl68v5bvv8d65h1bav1f6r01l5s0z3mr32n6i")))

(define-public crate-top_n_tail-0.1.1 (c (n "top_n_tail") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lrb9xvsh501a49rmgz2mpccfjjncdqnbr5jw677vmvkqgsvb76b")))

