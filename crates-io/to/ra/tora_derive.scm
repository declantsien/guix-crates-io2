(define-module (crates-io to ra tora_derive) #:use-module (crates-io))

(define-public crate-tora_derive-0.1.0 (c (n "tora_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (d #t) (k 0)))) (h "0zns73wsidy6nfxma5az10cssmapn27xz9m7fbzpayyrlf5cwvpk")))

(define-public crate-tora_derive-0.1.1 (c (n "tora_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "0m6fzx94271r8n0mikaagnz4b69g56lwpz1424qg7m7dvlj7xk8j")))

(define-public crate-tora_derive-0.1.2 (c (n "tora_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "14pyzxkirz5ccczsgf7jikwyd1m6r3asnbwrdgzqpvmfyc8rkxxj")))

(define-public crate-tora_derive-0.1.3 (c (n "tora_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (f (quote ("full"))) (d #t) (k 0)))) (h "0z25nlc389p9al3h238bqgc72blr6l8lf44rh28bnqbwnxhnfbnk")))

