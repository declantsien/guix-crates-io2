(define-module (crates-io to ra tora) #:use-module (crates-io))

(define-public crate-tora-0.1.0 (c (n "tora") (v "0.1.0") (d (list (d (n "tora_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0bmgykfaimdxgcpsmfhqvnxdg66yz8xs152s229iciv5jbxmyp0m") (f (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1.1 (c (n "tora") (v "0.1.1") (d (list (d (n "tora_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0mwkg8m14bj74lvdp5a4qapwd45fa8f646mi3f0416nwpa99pqby") (f (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1.2 (c (n "tora") (v "0.1.2") (d (list (d (n "tora_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1s96vdfgcdmcpygz626dlgf12f62r629wn4zs6iqddjjs01ahnw1") (f (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1.3 (c (n "tora") (v "0.1.3") (d (list (d (n "tora_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "181l2d2fgmbvn1r9px969kkrl99spw995jni9p5ri4x61qaigc9p") (f (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1.4 (c (n "tora") (v "0.1.4") (d (list (d (n "tora_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1xxpmfh5rr4gh77nz70narymn786b7pkhr5iz0ysa4apb0lrawww") (f (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1.5 (c (n "tora") (v "0.1.5") (d (list (d (n "tora_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "01kmr5r4kp17lr92zmzww7w5mbgxx3h9fsdba6gfj13j8scws6ml") (f (quote (("read_impl") ("dyn_impl") ("derive" "tora_derive") ("default" "tora_derive" "read_impl" "dyn_impl"))))))

