(define-module (crates-io to r- tor-hash-passwd) #:use-module (crates-io))

(define-public crate-tor-hash-passwd-1.0.0 (c (n "tor-hash-passwd") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha1") (r "=0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "08lrj2fnajvgy4vgyv8hyy48k54hj7hn140j5rz2i84nsq1ra4i7")))

(define-public crate-tor-hash-passwd-1.0.1 (c (n "tor-hash-passwd") (v "1.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha1") (r "=0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04kfy92w8p71kl12kyn7qswrdhpmx21i4h5lbnfw3h3nl51wv0wb")))

