(define-module (crates-io to r- tor-units) #:use-module (crates-io))

(define-public crate-tor-units-0.0.0 (c (n "tor-units") (v "0.0.0") (d (list (d (n "derive_more") (r "^0.99.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0yj0mkf813cm97zl4q3wd0q60sg69b0kg3jikci436idwqp4b2lp")))

(define-public crate-tor-units-0.0.1 (c (n "tor-units") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.0") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "135yfwypqh2ww0gapkr117mlsqqix9sndla595brimm8nv5562bz")))

(define-public crate-tor-units-0.0.2 (c (n "tor-units") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.99.0") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0irrxjwimlfmhw6sixj8l2xxaw2k29l3xzqrj0jkbbr4c9yyks0z")))

(define-public crate-tor-units-0.0.3 (c (n "tor-units") (v "0.0.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i32bggpy16275mafkjggc5wrc3wjimfycgv1gchdamrwki0mkg3")))

(define-public crate-tor-units-0.1.0 (c (n "tor-units") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09v4382wdpbbd464380bj1a1bbxhakgbcbw8yg4p3w0jbx58h22l")))

(define-public crate-tor-units-0.2.0 (c (n "tor-units") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0drv3270apqs3553zp29n3sq0kgafny8v29pvpygkny97mkhnzl1")))

(define-public crate-tor-units-0.3.0 (c (n "tor-units") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sfr7r18xd5rms24l1d6p5nygs06a9m6aircbzjx67xb0880fnvv") (r "1.56")))

(define-public crate-tor-units-0.3.1 (c (n "tor-units") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a0yiy9c7miyij3b6bpv15zk9kd6sbqwjfvasw0db8qn1590f5z0") (r "1.56")))

(define-public crate-tor-units-0.3.2 (c (n "tor-units") (v "0.3.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p9sn776jyzdbsszwjla4d40bb5rvh9n46qlkl5rdffc5dwr8894") (r "1.56")))

(define-public crate-tor-units-0.4.0 (c (n "tor-units") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c7bq062by578dh7bxrw90d3abhbvcxjjhhnv86kqlz3bhr0niql") (r "1.60")))

(define-public crate-tor-units-0.4.1 (c (n "tor-units") (v "0.4.1") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jxkhwc9djxhndb9h8r3y7946d8ab3lza9yp4zc1ln2fbmqg7h3p") (r "1.60")))

(define-public crate-tor-units-0.5.0 (c (n "tor-units") (v "0.5.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w8a6p0gjbkm01glk47diz0a7qd9mka4wv8am26p7fp8mp8k3m19") (r "1.60")))

(define-public crate-tor-units-0.6.0 (c (n "tor-units") (v "0.6.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15j9ls07db5rb39q0kaydpyp82y7qbqjqxnwd3msj3dzrwhw9akn") (r "1.65")))

(define-public crate-tor-units-0.6.1 (c (n "tor-units") (v "0.6.1") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p67ksdgqvvjkq7cs3wqw19avl5wi6ca3mx52amvc0cggp2qpmyn") (f (quote (("full")))) (r "1.65")))

(define-public crate-tor-units-0.6.2 (c (n "tor-units") (v "0.6.2") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dkxs1pif51ar36q8qscxh8691dpj9fycvrkxh8v1ys2jyn8jlwn") (f (quote (("full")))) (r "1.65")))

(define-public crate-tor-units-0.6.3 (c (n "tor-units") (v "0.6.3") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kz8cy7w5xgv8ziix4n8dj2q8w6547xad5qlm9nl20jqvridp20r") (f (quote (("full")))) (r "1.65")))

(define-public crate-tor-units-0.6.4 (c (n "tor-units") (v "0.6.4") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ghdrxg3rw329ny2k14l91mmjfv2xb2bq2hr8b5mqc7s76dnc959") (f (quote (("full")))) (r "1.65")))

(define-public crate-tor-units-0.6.5 (c (n "tor-units") (v "0.6.5") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sll9hmxwh3737j7pqjd4dc54z33gcgphb7r2fw36xnwfrk24n8d") (f (quote (("full")))) (r "1.70")))

(define-public crate-tor-units-0.17.0 (c (n "tor-units") (v "0.17.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cfansdibdnrxh0181jqhkk5cj5x1hc3vlklng73k1lwg0hd30db") (f (quote (("full")))) (r "1.70")))

(define-public crate-tor-units-0.18.0 (c (n "tor-units") (v "0.18.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wkrcpq6fjxvmvhicl96xis2bxnl3s1hzaap658dc09x60f4a407") (f (quote (("full")))) (r "1.70")))

