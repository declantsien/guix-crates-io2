(define-module (crates-io to r- tor-stream) #:use-module (crates-io))

(define-public crate-tor-stream-0.1.0 (c (n "tor-stream") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "socks") (r "^0.3.0") (d #t) (k 0)))) (h "0df2r923m4ryjzn0jibwp7879mfrdymhfc1my99c9fz44rkfrvjl")))

(define-public crate-tor-stream-0.1.1 (c (n "tor-stream") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "socks") (r "^0.3.0") (d #t) (k 0)))) (h "0kabyjhzhiacvbydvkwxx76f6s2l8dsggvx7lfp0ggx25yy5wfjw")))

(define-public crate-tor-stream-0.2.0 (c (n "tor-stream") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "socks") (r "^0.3.0") (d #t) (k 0)))) (h "0ij7cwwrq4n9siibk9lak3gp2mzxl167kwwrqbwc02qfr6gi0raq")))

(define-public crate-tor-stream-0.3.0 (c (n "tor-stream") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "socks") (r "^0.3") (d #t) (k 0)))) (h "1053maywqbpckn4g25m5zj0wmwd048j4wnkii0m5x1w17ldwxzdc")))

