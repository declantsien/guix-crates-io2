(define-module (crates-io to r- tor-congestion) #:use-module (crates-io))

(define-public crate-tor-congestion-0.3.0 (c (n "tor-congestion") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tor-error") (r "^0.3.2") (d #t) (k 0)) (d (n "tor-netdir") (r "^0.6.0") (d #t) (k 0)) (d (n "tor-units") (r "^0.3.1") (d #t) (k 0)))) (h "1s8rw1gc1l4cpbg38k1f2ccyazkv33m18wkcdzk538h0fgx8czcx") (r "1.56")))

(define-public crate-tor-congestion-0.4.0 (c (n "tor-congestion") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tor-error") (r "^0.4.0") (d #t) (k 0)) (d (n "tor-netdir") (r "^0.7.0") (d #t) (k 0)) (d (n "tor-units") (r "^0.4.0") (d #t) (k 0)))) (h "1v92vrx2nzif2i48v1ch5dk1vgqr9bgp2ir9knyhl25nbapvyf9n") (r "1.60")))

(define-public crate-tor-congestion-0.4.1 (c (n "tor-congestion") (v "0.4.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tor-error") (r "^0.4.1") (d #t) (k 0)) (d (n "tor-netdir") (r "^0.7.1") (d #t) (k 0)) (d (n "tor-units") (r "^0.4.1") (d #t) (k 0)))) (h "055kjymd42v6bk3bkhni3vjmif2hlgzarzka5ndzqn5jnl49jbv0") (r "1.60")))

(define-public crate-tor-congestion-0.4.2 (c (n "tor-congestion") (v "0.4.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tor-error") (r "^0.4.1") (d #t) (k 0)) (d (n "tor-netdir") (r "^0.8.0") (d #t) (k 0)) (d (n "tor-units") (r "^0.5.0") (d #t) (k 0)))) (h "065ij9mcyc9bknwdinrpa907wm7bnyanyqd4wvzfyjjayzspkzd0") (r "1.60")))

(define-public crate-tor-congestion-0.5.0 (c (n "tor-congestion") (v "0.5.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tor-error") (r "^0.5.0") (d #t) (k 0)) (d (n "tor-netdir") (r "^0.9.0") (d #t) (k 0)) (d (n "tor-units") (r "^0.6.0") (d #t) (k 0)))) (h "150899qr2s7ag5v533n0rizc1xjiah6fz819y6xa0znpbnriaclk") (r "1.65")))

(define-public crate-tor-congestion-0.5.1 (c (n "tor-congestion") (v "0.5.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tor-error") (r "^0.5.1") (d #t) (k 0)) (d (n "tor-netdir") (r "^0.9.1") (d #t) (k 0)) (d (n "tor-units") (r "^0.6.1") (d #t) (k 0)))) (h "0wfhnlls5cwskf5843j7y1pgb9n0b48va38h4p9v486l8jd0cs04") (f (quote (("full" "tor-error/full" "tor-netdir/full" "tor-units/full")))) (r "1.65")))

(define-public crate-tor-congestion-0.5.2 (c (n "tor-congestion") (v "0.5.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tor-error") (r "^0.5.2") (d #t) (k 0)) (d (n "tor-netdir") (r "^0.9.2") (d #t) (k 0)) (d (n "tor-units") (r "^0.6.1") (d #t) (k 0)))) (h "0wn9z9hfs55sbmsafl32vcvkwpq5sj0x8iv33jwar44p6v0igsgs") (f (quote (("full" "tor-error/full" "tor-netdir/full" "tor-units/full")))) (r "1.65")))

