(define-module (crates-io to r- tor-v3-vanity-core) #:use-module (crates-io))

(define-public crate-tor-v3-vanity-core-0.1.0 (c (n "tor-v3-vanity-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "ed25519-compact") (r "^0.1.6") (k 0)) (d (n "rustacuda_core") (r "^0.1") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1") (d #t) (k 0)))) (h "13q6zijhjj70zgjk68cwjcpdys43vkw802mnczmk35n5r9grsnqk")))

