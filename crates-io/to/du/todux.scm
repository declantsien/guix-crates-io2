(define-module (crates-io to du todux) #:use-module (crates-io))

(define-public crate-todux-0.1.0 (c (n "todux") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "tui") (r "^0.15") (d #t) (k 0)))) (h "0dj7032j0r31ap5llcb4ka4w3d5rf8dpvw593hnq42gf724g0kx4")))

(define-public crate-todux-0.1.1 (c (n "todux") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "tui") (r "^0.15") (d #t) (k 0)))) (h "09n3b2fyfgxsghqg5xfliymlvx1bqwn4n2ynkd0wgbg5g2h1mlpg")))

