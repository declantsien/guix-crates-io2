(define-module (crates-io to nl tonlib-sys) #:use-module (crates-io))

(define-public crate-tonlib-sys-0.1.0 (c (n "tonlib-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "12lmq7xlk5g9zqy22lab3rdxywc955mxrb6ixm64fm86siw1la5a") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (y #t)))

(define-public crate-tonlib-sys-0.1.1 (c (n "tonlib-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "197hqg54h0qj6vkhnmp4qhrzphl6nxd8rhmv0qajmysqimjxl2c9") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (y #t)))

(define-public crate-tonlib-sys-0.1.2 (c (n "tonlib-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "12mkcaqif34dg3q68y6ybispn9p06cadbbwmznnvhfkxrd0by94m") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2023.6.0 (c (n "tonlib-sys") (v "2023.6.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "053zw54kd1f0d1m5rzln1z52a311pb45yxdai0i9h344lxa26z3d") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2023.6.1 (c (n "tonlib-sys") (v "2023.6.1") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "1qdjiy8vmaz554aky1rili2vf1jkgaixx5dkjxpbp9c28xnypjix") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2023.6.2 (c (n "tonlib-sys") (v "2023.6.2") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "0cr1n5kfmmalzn5v3ls07jxvbdmrasn3mmvi728vrj9s1w5jvhxh") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2023.6.3 (c (n "tonlib-sys") (v "2023.6.3") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "1nx2lza7r7bd5hli30gwym5wy6jpnby0716m3n1cr0fz22xxn53w") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2023.6.4 (c (n "tonlib-sys") (v "2023.6.4") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "1n75nq527kjjhyw6hjc83rkp9sjs0bwm7ihf4afbgk5jh5ji54id") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2023.11.0 (c (n "tonlib-sys") (v "2023.11.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "04j9pxh6y9x9wdpm368a9y252y9fixkj9sqrad0f1iwz2z488ppk") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2023.12.0 (c (n "tonlib-sys") (v "2023.12.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "0lj089f11nh0x28iisrwvakakkc82asc69rsx5h51fwgr2j6wgyj") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2024.1.0 (c (n "tonlib-sys") (v "2024.1.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "18hk4wsbp7ais4iwjdwarcxx32pl7jx5wdy0bc4d0a7y63bya991") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2024.3.0 (c (n "tonlib-sys") (v "2024.3.0") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "0h5fq39xagvihkj6vcf0bnbhbxkj2vp915sahx14py8sglw7gzfb") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2024.3.1 (c (n "tonlib-sys") (v "2024.3.1") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "17i178ipxd3z7c1d5sz4pc0p76kld868cg7bnr9vlzqdiwh14yza") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2024.3.2 (c (n "tonlib-sys") (v "2024.3.2") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "07v6467znk5l542ihkqlc6wbg8jxfyc58zk4brz3wgrlqlg54qws") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2024.3.3 (c (n "tonlib-sys") (v "2024.3.3") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "num_cpus") (r "^1") (o #t) (d #t) (k 1)))) (h "1sc869f1mim1a057l45lfdh9jkyb1ryxdq92z365b111w7ygq0gd") (f (quote (("shared-tonlib") ("default" "cmake" "num_cpus"))))))

(define-public crate-tonlib-sys-2024.3.4 (c (n "tonlib-sys") (v "2024.3.4") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0hp8qn66cbpfll92mnkl8p7yiqc0inv3rc7bdfpmiwnjxbjyflhy") (f (quote (("shared-tonlib") ("default" "cmake"))))))

(define-public crate-tonlib-sys-2024.3.5 (c (n "tonlib-sys") (v "2024.3.5") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "0a5k7cwyddrmqjxap7nph6g1qfk0937975qdl2a7vfx3slcp1zmp") (f (quote (("shared-tonlib") ("default" "cmake"))))))

(define-public crate-tonlib-sys-2024.3.6 (c (n "tonlib-sys") (v "2024.3.6") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1ljqvgg4w6vrdprj0a778n0qi8vv84w5p0gyyw7i4y9cwanwajxk") (f (quote (("shared-tonlib") ("no_avx512") ("default" "cmake"))))))

(define-public crate-tonlib-sys-2024.3.7 (c (n "tonlib-sys") (v "2024.3.7") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1hirv8561jslgyps19n887af3ir4zhk48xsa2vf32zfy02cf7f8l") (f (quote (("shared-tonlib") ("no_avx512") ("default" "cmake"))))))

