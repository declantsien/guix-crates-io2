(define-module (crates-io to x_ tox_binary_io) #:use-module (crates-io))

(define-public crate-tox_binary_io-0.1.0 (c (n "tox_binary_io") (v "0.1.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (o #t) (d #t) (k 0)))) (h "0y3cbs5s9qh6zwzbxll8kwa23is0h75931rqimja7i9kq4h9fnxl") (f (quote (("sodium" "sodiumoxide") ("default"))))))

(define-public crate-tox_binary_io-0.1.1 (c (n "tox_binary_io") (v "0.1.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (o #t) (d #t) (k 0)))) (h "1bdpq34h56r4xv1vycldi8g88n5gid6m9zvl08jsaxmvqywqi4pz") (f (quote (("sodium" "sodiumoxide") ("default"))))))

