(define-module (crates-io to x_ tox_crypto) #:use-module (crates-io))

(define-public crate-tox_crypto-0.1.0 (c (n "tox_crypto") (v "0.1.0") (d (list (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "1qf7yym6h5a35vi513ligkwasyhiwdd4hyf0pqjl6yxkf20ibxw8")))

(define-public crate-tox_crypto-0.1.1 (c (n "tox_crypto") (v "0.1.1") (d (list (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "0pswnhl1hsmajv2c3y6y0xwxkabix3x9chyr6lz2416wgddiv58k")))

