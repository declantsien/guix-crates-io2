(define-module (crates-io to ol toolsdoit) #:use-module (crates-io))

(define-public crate-toolsdoit-0.1.0 (c (n "toolsdoit") (v "0.1.0") (h "1c015q61ld25h8w0z808sjpk47h0rrzl2md7hyzzdrjdrvbvy6pf") (y #t)))

(define-public crate-toolsdoit-1.0.0 (c (n "toolsdoit") (v "1.0.0") (h "0k2jcnbxnkczavwjgrzkjgnzjlgkx42bfcpa966y0gx6bgyax96w") (y #t)))

