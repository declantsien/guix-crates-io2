(define-module (crates-io to ol toolbox) #:use-module (crates-io))

(define-public crate-toolbox-0.1.0 (c (n "toolbox") (v "0.1.0") (h "0nkraqfxjxrmpywa6blcjssasxjvhylnv3c3rzqvm03j202a9m4s") (y #t)))

(define-public crate-toolbox-0.1.1 (c (n "toolbox") (v "0.1.1") (h "173frfnqbac4m6izvi0nqzv2gqchh3ss93nszhccxxrnycrnv8d5") (y #t)))

(define-public crate-toolbox-0.1.2 (c (n "toolbox") (v "0.1.2") (h "18j39z3pd03hwvkbv4fj5ssgvf1a7gsbc29lg8xim9bbfsvb0n4x") (y #t)))

(define-public crate-toolbox-0.0.3 (c (n "toolbox") (v "0.0.3") (h "1grpns8sg16dcbman78k4lwn4fzd5mw2dynrqz2br4cz9vchh9h5")))

(define-public crate-toolbox-0.0.4 (c (n "toolbox") (v "0.0.4") (h "0s6i3shaz8k7fv54fdh8ydvr6by433y8lxcsqzcgd5fcnhdd79wz")))

(define-public crate-toolbox-0.0.5 (c (n "toolbox") (v "0.0.5") (h "0lf7m0mj8dax5lm4ji5lhj083536jv72c91wq48spfs53kdywgqb")))

(define-public crate-toolbox-0.0.6 (c (n "toolbox") (v "0.0.6") (h "1cp2d9wq44nlz86wnc1mj9amijhxhfg9xgs9qkh2p1a8fg56a3m5")))

(define-public crate-toolbox-0.1.3 (c (n "toolbox") (v "0.1.3") (h "0dndblay0dvmxf621np0nn2d8nw0bfzc6hfmbzwyx5kbkdz2zcl6")))

(define-public crate-toolbox-0.1.4 (c (n "toolbox") (v "0.1.4") (h "0cki4w0632l2av258cd3f2xzn20w9xfvddnljihcc0667mifpg4i") (y #t)))

(define-public crate-toolbox-0.1.5 (c (n "toolbox") (v "0.1.5") (h "1fx956g511wfrimdm5fc76idkpvf6n2ac3wzlzzn4265lpi9mpgj")))

(define-public crate-toolbox-0.1.6 (c (n "toolbox") (v "0.1.6") (h "1c3qll51ibf426l3qdazsfzzi4b1b52bqfib861rgr48agf06p4q")))

(define-public crate-toolbox-0.1.7 (c (n "toolbox") (v "0.1.7") (h "1ca5fnjs9i6xwlsnvhz43ri0lz2a73z32l6vg4didpcx91kizazx")))

(define-public crate-toolbox-0.2.0 (c (n "toolbox") (v "0.2.0") (h "0zpbb7xslwv21srqlhr9y955cvqjaqcckf7fcsxpl8d654r1ayi9")))

(define-public crate-toolbox-0.2.1 (c (n "toolbox") (v "0.2.1") (h "0jm1bj15qklckfhg8imx0hhfkmg776cy5427rlcka8fvl6qbhvqf")))

(define-public crate-toolbox-0.2.2 (c (n "toolbox") (v "0.2.2") (h "1awkg0ddy13rnx24dnx33c74jm9c8mv4rhmp8ia9m3vpqz8wq52f")))

(define-public crate-toolbox-0.2.3 (c (n "toolbox") (v "0.2.3") (h "075wdwpab3rzapx45id3gc0q3knq4cddi8qwshw2b5vf174ibf4g")))

(define-public crate-toolbox-0.2.4 (c (n "toolbox") (v "0.2.4") (h "1mb4a9yjbm96snzsfvjmzm5z8lc17hvgd4yvdm5rdvmy306wrbkg")))

(define-public crate-toolbox-0.2.5 (c (n "toolbox") (v "0.2.5") (h "1bp3n48dqlpdh5063v5zxfvx5sisgb4fpnsrrpzp29n7iszaa3nw")))

(define-public crate-toolbox-0.2.6 (c (n "toolbox") (v "0.2.6") (h "154djgxym9c3g65f2nc41xavfr85h4ml322amdlcfxz0v4ajwy8f")))

(define-public crate-toolbox-0.2.7-final (c (n "toolbox") (v "0.2.7-final") (d (list (d (n "alg_ds") (r "^0.2") (d #t) (k 0)) (d (n "vfsys") (r "^0.1") (d #t) (k 0)))) (h "1vd3by0a26flrxf9ykh7vhqjvqmxm50i47a0dy5vr9y5w0y12adc") (y #t)))

(define-public crate-toolbox-0.2.7 (c (n "toolbox") (v "0.2.7") (d (list (d (n "alg_ds") (r "^0.2") (d #t) (k 0)) (d (n "vfsys") (r "^0.1") (d #t) (k 0)))) (h "1pix6ww0knyyg206976pjc627p6fhlx51g8yj5g1kr8drc3yiqz4")))

(define-public crate-toolbox-0.2.8 (c (n "toolbox") (v "0.2.8") (d (list (d (n "alg_ds") (r "^0.2") (d #t) (k 0)) (d (n "vfsys") (r "^0.1") (d #t) (k 0)))) (h "1zww68xd5z4kga16js31ksv91mpkdwdvp8r0a00r9hvp7vwpsfma")))

