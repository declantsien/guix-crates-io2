(define-module (crates-io to ol toolstr_colored) #:use-module (crates-io))

(define-public crate-toolstr_colored-2.1.1 (c (n "toolstr_colored") (v "2.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01dc2baxdika4jccal02qnx4xy46ykr5cri2ksfkn4wvxmv52cwd") (f (quote (("no-color")))) (r "1.70")))

