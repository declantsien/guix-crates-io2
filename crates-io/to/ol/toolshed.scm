(define-module (crates-io to ol toolshed) #:use-module (crates-io))

(define-public crate-toolshed-0.1.0 (c (n "toolshed") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "1lslach746ygwcss4y0qb637qgsh4r6pxm4nkj17hlyhlh3whsq4")))

(define-public crate-toolshed-0.2.0 (c (n "toolshed") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "1gid4ik9hbhj26idkqrkb1wsm2dn4xzdrn718z97016i65q61sw6")))

(define-public crate-toolshed-0.2.1 (c (n "toolshed") (v "0.2.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "1rbzaaibfgqd1rblb2z7zlvs6q1qphx4w6ilmqfj2vka16raj31r")))

(define-public crate-toolshed-0.2.2 (c (n "toolshed") (v "0.2.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "0i3rxndhyj6myw1y189wwap32swy6p0sza38szpxj5qs93kqjx3z")))

(define-public crate-toolshed-0.2.3 (c (n "toolshed") (v "0.2.3") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1l0mrcnfpa31fjqk2zs6k1aq04nncin1lz7s1dlgiid9a0pc1bjg") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.3.0 (c (n "toolshed") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xl7p9yb9rj0yxbabjv7vsjs1i1m2xd5r75y16v9kmxcbymnqsjq") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.3.1 (c (n "toolshed") (v "0.3.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13hvj34qdbvm8falrjp72fg1z4n6h4wycdfhfaa8r4qk3ixad60b") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.3.2 (c (n "toolshed") (v "0.3.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0w85mis8frwv9x0fx6jlkm2shddlf8c1npfivphrcmg03dy71ndb") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.4.0 (c (n "toolshed") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1izfbaskp0apcznm5ihxc2lamyx1lq13r8v35vvklrn767hl2125") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.5.0 (c (n "toolshed") (v "0.5.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0zyvyibxvm1q9fkvf1j74pq3nhv2a8zi3ma6wsbzfxfi2ibpa50y") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.6.0 (c (n "toolshed") (v "0.6.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0aaj23ibbrmixz8drz4s397kqx7213j0l7yq28hrbmbn2s3sn57c") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.6.1 (c (n "toolshed") (v "0.6.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14kd24xjqywk08hvvn0hr9bk4j5x363pwf9y1daplzlyzm8ihgi6") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.6.2 (c (n "toolshed") (v "0.6.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cr2i4b4x173pbf8pz0pxavmb714rpgk2nql4kq6bhx8s3wc31nn") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.6.3 (c (n "toolshed") (v "0.6.3") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1a44l7ii03pp52jq801rrn4d8f0cw3irxl3lcx4bpkqlpynp58jl") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.7.0 (c (n "toolshed") (v "0.7.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yjjj03qp8056zcc85i6cf4zc84mmqhsf25fd8knxsha92qqli7g") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.8.0 (c (n "toolshed") (v "0.8.0") (d (list (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gyv8zaq1rlpraafzp9srg2janz9a6x2sr3rbz4c09im4wqjqald") (f (quote (("impl_serialize" "serde") ("default"))))))

(define-public crate-toolshed-0.8.1 (c (n "toolshed") (v "0.8.1") (d (list (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "120nc1l94c8d97i5y41ah4rm4fpk93cndr07cm01rqjhhp84bngs") (f (quote (("impl_serialize" "serde") ("default"))))))

