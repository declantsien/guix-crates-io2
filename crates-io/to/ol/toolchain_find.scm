(define-module (crates-io to ol toolchain_find) #:use-module (crates-io))

(define-public crate-toolchain_find-0.1.0 (c (n "toolchain_find") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "15svsxhhi401qf4zmfn4jxmxcixx02z22gggms1lfh4svqyklw2m")))

(define-public crate-toolchain_find-0.1.1 (c (n "toolchain_find") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0rygzsbp7j1wpi683jc9pc6phhd7krkwmgprpk9r7mrkxm4l8wc3")))

(define-public crate-toolchain_find-0.1.2 (c (n "toolchain_find") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1nw2hqqahksms77nkylz9rxx5pqnkhws0xqyachmvn5mcmyjm62h")))

(define-public crate-toolchain_find-0.1.3 (c (n "toolchain_find") (v "0.1.3") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1hswdk72qn5xsj1gq7a0jjsbfl14fldnlyisax1y5s9wj2d0bzj3")))

(define-public crate-toolchain_find-0.1.4 (c (n "toolchain_find") (v "0.1.4") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0ifan5r9174bv8y184hz4lhavpsxc0pqkfz3q927246nx8vsyn74")))

(define-public crate-toolchain_find-0.2.0 (c (n "toolchain_find") (v "0.2.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "04rlfi180z2gr20a4bvzj8i3wd1ziy0r77giqr3pm8772156b1ay")))

(define-public crate-toolchain_find-0.3.0 (c (n "toolchain_find") (v "0.3.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1v8k64hqbp3r144jwafqqykxj83481xkv9x60krnyy4d9rn7937s")))

(define-public crate-toolchain_find-0.4.0 (c (n "toolchain_find") (v "0.4.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0aiz2pzmqgscrxq56r5dxqg4f08b7l16215grld6x5m2y2kwkj7b")))

