(define-module (crates-io to ol tool-derive) #:use-module (crates-io))

(define-public crate-tool-derive-0.2.0 (c (n "tool-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "1rmvb6wjkmg4bnfgnq4kq5xgyrkkizq5ps3z7a3x25jzjiab14cz")))

(define-public crate-tool-derive-0.2.1 (c (n "tool-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (d #t) (k 0)))) (h "01rk6hgih2slwp2igj36hb3jnsbkx0akcwqjqvd269gji0kl4c9j")))

