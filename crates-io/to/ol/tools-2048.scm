(define-module (crates-io to ol tools-2048) #:use-module (crates-io))

(define-public crate-tools-2048-0.1.0 (c (n "tools-2048") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "174h2ifk7vxf3yjh9czgpj3d6g5622yklicixgkdzbn1br9mkd84")))

(define-public crate-tools-2048-0.1.1 (c (n "tools-2048") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pc04yhd1j15m6k030w5y9i3c4llrf1v0qji8f5ma0czgcqfsdhy")))

(define-public crate-tools-2048-0.1.2 (c (n "tools-2048") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1baiaxi4fmx4ka7l51mk9hlg4fs37czn8scycpx46hv86ik3sb8a")))

(define-public crate-tools-2048-0.2.0 (c (n "tools-2048") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m2kpp35c8qnw78cpzdlvvz40xgdnb3bcallns91rh462429hajx")))

(define-public crate-tools-2048-0.3.0 (c (n "tools-2048") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tinypool") (r "^0.2.0") (d #t) (k 0)))) (h "0avdm037kirhvdyaan17vvj4dqrdgyw076xrm9z7scp7fsxp6h6p")))

(define-public crate-tools-2048-0.3.1 (c (n "tools-2048") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tinypool") (r "^0.2.0") (d #t) (k 0)))) (h "15w16hz5lhy9fbpkni56fgb9s8p1ak97f1byidl2ddiwvnqf2kpd")))

(define-public crate-tools-2048-0.3.2 (c (n "tools-2048") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tinypool") (r "^0.2.0") (d #t) (k 0)))) (h "0g25796qvvn23bjnaqlx1m5hylkxds347ysfx76zbw591qzl2jpc")))

(define-public crate-tools-2048-0.4.0 (c (n "tools-2048") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tinypool") (r "^0.2.0") (d #t) (k 0)))) (h "0biby3a0zsm0k252d4abgw5wlig7ha8dhsbfymlc9i56mc8n6ggw")))

(define-public crate-tools-2048-0.4.1 (c (n "tools-2048") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tinypool") (r "^0.2.0") (d #t) (k 0)))) (h "0pwa7rcddc7i4dpz2smf37nx1m8pk1hy648gn310pjwad5zvb4gi")))

