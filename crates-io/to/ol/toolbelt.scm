(define-module (crates-io to ol toolbelt) #:use-module (crates-io))

(define-public crate-toolbelt-0.0.1 (c (n "toolbelt") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.17.0") (f (quote ("swizzle"))) (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "09xr4v47dgx2d3mh63cgshbiss8nk4dsfykwgqq4p28akivjvf98")))

(define-public crate-toolbelt-0.0.2 (c (n "toolbelt") (v "0.0.2") (d (list (d (n "cgmath") (r "^0.17.0") (f (quote ("swizzle"))) (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "11b8x6wvh4ss16823pidiy3laj9j9miiwkxcdgqarq5xds75dgfs")))

(define-public crate-toolbelt-0.0.3 (c (n "toolbelt") (v "0.0.3") (d (list (d (n "cgmath") (r "~0.18.0") (f (quote ("swizzle" "serde"))) (d #t) (k 0)) (d (n "noise") (r "~0.7.0") (d #t) (k 0)) (d (n "num") (r "~0.4.0") (d #t) (k 0)) (d (n "rustversion") (r "~1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "~2.3.2") (d #t) (k 0)))) (h "1qn4avhvyp9l2l5mxcy610d28k3kwv6pssdza2lm3k8jpi4lngrr") (y #t)))

(define-public crate-toolbelt-0.0.4 (c (n "toolbelt") (v "0.0.4") (d (list (d (n "cgmath") (r "~0.18.0") (f (quote ("swizzle" "serde"))) (d #t) (k 0)) (d (n "noise") (r "~0.7.0") (d #t) (k 0)) (d (n "num") (r "~0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "~2.3.2") (d #t) (k 0)))) (h "1n8s7cfi9i22b8mw9h1anpvj2lgwjkznpig2a9502i01g7cbhjjl") (y #t)))

(define-public crate-toolbelt-0.0.5 (c (n "toolbelt") (v "0.0.5") (d (list (d (n "cgmath") (r "~0.18.0") (f (quote ("swizzle" "serde"))) (d #t) (k 0)) (d (n "noise") (r "~0.7.0") (d #t) (k 0)) (d (n "num") (r "~0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "~2.3.2") (d #t) (k 0)))) (h "0svz05mzgxxginn8yfwgjygixsjm1f4hvn9mip6wcpa6hz2ab4lp") (y #t)))

(define-public crate-toolbelt-0.0.6 (c (n "toolbelt") (v "0.0.6") (d (list (d (n "cgmath") (r "~0.18.0") (f (quote ("swizzle" "serde"))) (d #t) (k 0)) (d (n "noise") (r "~0.7.0") (d #t) (k 0)) (d (n "num") (r "~0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "~2.3.2") (d #t) (k 0)))) (h "0yzrmq16mfqbi6m6rf6vz023vmr2vmz4wcz460mg4r71x66gnpdb")))

