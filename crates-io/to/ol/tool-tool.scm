(define-module (crates-io to ol tool-tool) #:use-module (crates-io))

(define-public crate-tool-tool-0.1.0 (c (n "tool-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "atomicwrites") (r "^0.2.5") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.14") (d #t) (k 0)) (d (n "http_req") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "tar") (r "^0.4.28") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 0)))) (h "0csixziy047p7swghlh6q108gn2rpv8617r2qpbnww01fpzrb2sx")))

