(define-module (crates-io to ol toolchain) #:use-module (crates-io))

(define-public crate-toolchain-0.1.0-alpha.1 (c (n "toolchain") (v "0.1.0-alpha.1") (d (list (d (n "target-lexicon") (r "^0.12.4") (d #t) (k 0)) (d (n "target-lexicon-macros") (r "^0.1.0-alpha.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (d #t) (k 0)) (d (n "time-macros") (r "^0.2.4") (d #t) (k 2)))) (h "1y18gjb4nzaiv60imds9z5vxgl6zhjaasjf074nslpg5imyi290q")))

