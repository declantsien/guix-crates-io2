(define-module (crates-io to ol tool) #:use-module (crates-io))

(define-public crate-tool-0.1.0 (c (n "tool") (v "0.1.0") (h "1pzbagb38n406c0p080ckvnmb3fy6bgjigvrc9iixbw8kq2xvas4")))

(define-public crate-tool-0.1.1 (c (n "tool") (v "0.1.1") (h "01ymh58pb8lc14hikwh8d0m5h50ncjjj9amfav1s2nx7wrfr5j7z")))

(define-public crate-tool-0.1.3 (c (n "tool") (v "0.1.3") (h "0ijhkxipkq0bq126fqyw9rv5975cm4nhb0z57qqh0xdajjlm39lk")))

(define-public crate-tool-0.1.4 (c (n "tool") (v "0.1.4") (h "1g57i9n3rfp5nz8yspwwz649grg76bg24vqjywr4ydznnwk5lxx1") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tool-0.1.5 (c (n "tool") (v "0.1.5") (h "157j5jcfal1r1c3n2h8dh3c72v2gggl5s43k6ngcfzpwqhszp96g") (f (quote (("use_std") ("unstable") ("default" "use_std"))))))

(define-public crate-tool-0.2.0 (c (n "tool") (v "0.2.0") (h "0gwb006zkjh3z62xg29z78nf04w0a5qani123iqqv9s180r9ydqa") (f (quote (("use_std") ("unstable") ("default" "use_std"))))))

(define-public crate-tool-0.2.1 (c (n "tool") (v "0.2.1") (h "17mxjfk6bxqagxqgcs1qbrpn3l0xsmdn99lw87c8qxfzc6vwz9l3") (f (quote (("use_std") ("unstable") ("default" "use_std"))))))

