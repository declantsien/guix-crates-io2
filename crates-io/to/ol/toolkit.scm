(define-module (crates-io to ol toolkit) #:use-module (crates-io))

(define-public crate-toolkit-0.1.0 (c (n "toolkit") (v "0.1.0") (h "098pfdhaxd4nvn0vldbqky1kskisj36sia6vg135cvnmqb7ywmg4")))

(define-public crate-toolkit-0.1.1 (c (n "toolkit") (v "0.1.1") (h "1ggxrvqsn4gjar3x9z6h1j42mn75fc02j45fhwiw5s6biza77isj")))

