(define-module (crates-io to ol toolz) #:use-module (crates-io))

(define-public crate-toolz-0.1.0 (c (n "toolz") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "15kd4ja9nsgiqjq7s97b18wvcf6c3bdbjkg2spkxap744hkjf99y")))

(define-public crate-toolz-0.1.1 (c (n "toolz") (v "0.1.1") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "1wi78nxpds5yfpyfkrnh2621rawwrkvmix3m4wwvzx707zypwhiv")))

(define-public crate-toolz-0.1.1001 (c (n "toolz") (v "0.1.1001") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "12hjg7618sqmmnmmg0265g69mdsmrgp751ix047ry3s60xahll5x")))

(define-public crate-toolz-0.1.1002 (c (n "toolz") (v "0.1.1002") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "09j9w3rsnra2xqfs1zrzr6gy6ylid73c3qhhsrnx442zmwnsjp1m")))

(define-public crate-toolz-0.1.1003 (c (n "toolz") (v "0.1.1003") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "0mdnh9q12raq5kyjhyanxwg9ipwqqgxhdy82df3mqs45gibi32bw")))

