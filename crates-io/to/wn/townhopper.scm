(define-module (crates-io to wn townhopper) #:use-module (crates-io))

(define-public crate-townhopper-0.1.0 (c (n "townhopper") (v "0.1.0") (d (list (d (n "chrono") (r ">= 0.4") (d #t) (k 0)) (d (n "chrono-tz") (r ">= 0.5") (d #t) (k 0)) (d (n "csv") (r ">= 1.0") (d #t) (k 0)) (d (n "log") (r ">= 0.4") (d #t) (k 0)) (d (n "regex") (r ">= 1.1") (d #t) (k 0)) (d (n "rusqlite") (r ">= 0.16") (d #t) (k 0)) (d (n "serde") (r ">= 1.0") (d #t) (k 0)) (d (n "serde_derive") (r ">= 1.0") (d #t) (k 0)) (d (n "unidecode") (r ">= 0.3") (d #t) (k 0)))) (h "1fm0jlhpw9f16zynhhxdm0y8iafifvarpsklzfzb07i8v76y96xq")))

(define-public crate-townhopper-0.1.1 (c (n "townhopper") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3") (d #t) (k 0)))) (h "1jhsjl80b2yriqlz8v4wyibd1sa265zk8f2999q6gcj6w592hhp7")))

