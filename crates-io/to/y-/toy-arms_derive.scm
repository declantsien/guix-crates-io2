(define-module (crates-io to y- toy-arms_derive) #:use-module (crates-io))

(define-public crate-toy-arms_derive-0.1.0 (c (n "toy-arms_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.78") (d #t) (k 0)))) (h "1fiqb2h265akndikvhkmf9drgh0f0q6b2sf2f73i6022fcpix7j6")))

(define-public crate-toy-arms_derive-0.1.5 (c (n "toy-arms_derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.78") (d #t) (k 0)))) (h "1mzyxjpr3napymazgwbngbkmd243d05klkkkys5cf64cjswl6wm2")))

