(define-module (crates-io to y- toy-rpc-macros) #:use-module (crates-io))

(define-public crate-toy-rpc-macros-0.1.0 (c (n "toy-rpc-macros") (v "0.1.0") (d (list (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.51, <2.0.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "16kfgqlcgd3pqlw28zyd2hpfm9p0z2n5hw5zzs8i2v36nl9gmlm6")))

(define-public crate-toy-rpc-macros-0.2.0 (c (n "toy-rpc-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1i7i2ykq3mz1dbfwmgm3qgh8wbi1zniqw3zpymngc0zd7sxb4gz4")))

(define-public crate-toy-rpc-macros-0.2.1 (c (n "toy-rpc-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0npwmm0l4vm0hdnqpqb2sfji8w3w8gcaxx6r61vqnb3dj1ld1722")))

(define-public crate-toy-rpc-macros-0.3.0 (c (n "toy-rpc-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "11ylyly7220lbvc2l36k485hdwr65p1pfyh33yg3j0xxvlgmw7q9")))

(define-public crate-toy-rpc-macros-0.4.0 (c (n "toy-rpc-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0vmpwnf6dms9c17fciyi6dj65szikc4c246yimswdfy5dlipdvjd")))

(define-public crate-toy-rpc-macros-0.4.1 (c (n "toy-rpc-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.51") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1l0n6g8zrhkpflwg69gkcs50r6515icvhjgw3xqsmwkbjz0qssl4")))

(define-public crate-toy-rpc-macros-0.5.0-alpha (c (n "toy-rpc-macros") (v "0.5.0-alpha") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "19jdvn6xzz0wd9189g73ga4y0nv6lgg78xhcqg491s0lgb9h7r2n") (f (quote (("server") ("client"))))))

(define-public crate-toy-rpc-macros-0.5.0-beta (c (n "toy-rpc-macros") (v "0.5.0-beta") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0rkiyk5pafxlv75ylm28al8wlnjqb08rbkg2bfanz567zlk547dk") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.5.0 (c (n "toy-rpc-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1m4yprqnnymiair21bvjkka9vxzbkfpf9h8hqjhprrhxgrd8x1vq") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.6.0-alpha (c (n "toy-rpc-macros") (v "0.6.0-alpha") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1nr0i5zq4w0q77n2lj2fnw65kmrya6w5cp5nmpd31pjnsp41gwpk") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.6.0-beta (c (n "toy-rpc-macros") (v "0.6.0-beta") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "18jnk66c0ga0vw399im3v8crcmfmcvwkl1jab3vygqcwh0xsd9ir") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.6.0 (c (n "toy-rpc-macros") (v "0.6.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1xzr568zj0bz5hq97bdlgqk9gz7rnbdy27nqf6wyjvn4adwx59ff") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.6.1 (c (n "toy-rpc-macros") (v "0.6.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "11in81il4i1winkpnrly4ig4bzfbr2b7y2jvp6l171ad0mdz870r") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.6.2 (c (n "toy-rpc-macros") (v "0.6.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0mmbffzwl3jx67n03wr0di7fsgb8gh16wabzh1zpskcnj9wgwv4l") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.6.3 (c (n "toy-rpc-macros") (v "0.6.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1palxyfdj3nc83p2gj9cpsn3k7q1b4jya7vj9wdmq8mimr8p7c9p") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.7.0-alpha.1 (c (n "toy-rpc-macros") (v "0.7.0-alpha.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0i9yx8gpaax7cq4pp709ap6f290xyiq2jwvyl94ral5l9j9gvv40") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.7.0-alpha.2 (c (n "toy-rpc-macros") (v "0.7.0-alpha.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0j230xja5pdz9wbjx2p563a9ji0kwcf6873sl9hw3vg5alhx5h0q") (f (quote (("server") ("runtime") ("default") ("client"))))))

(define-public crate-toy-rpc-macros-0.7.0-alpha.3 (c (n "toy-rpc-macros") (v "0.7.0-alpha.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0z3if2qcr4ynwbwa0wvkxq6i52blk5jn39gq30riswvx7jrx3yhj") (f (quote (("server") ("runtime") ("default") ("client"))))))

