(define-module (crates-io to y- toy-rpc-macros-ha421) #:use-module (crates-io))

(define-public crate-toy-rpc-macros-ha421-0.7.0-alpha.3 (c (n "toy-rpc-macros-ha421") (v "0.7.0-alpha.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1z51z925law8ijm93x15nqfajxvn79hfn20rk0akll7x31hyksxx") (f (quote (("server") ("runtime") ("default") ("client"))))))

