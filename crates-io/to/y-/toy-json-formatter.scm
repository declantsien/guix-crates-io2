(define-module (crates-io to y- toy-json-formatter) #:use-module (crates-io))

(define-public crate-toy-json-formatter-0.1.0 (c (n "toy-json-formatter") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "1x7xxyhcsk9fz6vc6mi7p6jy8lfh47288yas7mrr967am69a2hi4")))

(define-public crate-toy-json-formatter-0.1.1 (c (n "toy-json-formatter") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "14nyj4n86pscgs2mf4xnhvh086qqmii3nw14y8gl83r6w2lvnqfz")))

(define-public crate-toy-json-formatter-0.1.2 (c (n "toy-json-formatter") (v "0.1.2") (h "1qys8glpqva7z1n9wq8f5y44ghdvf89580j1m346kqz3g7xgvq4n")))

(define-public crate-toy-json-formatter-0.1.3 (c (n "toy-json-formatter") (v "0.1.3") (h "18m4hkvvcypchf66cs1chzbanjgz5d0blxdzn1zhlybykq69wp0h")))

(define-public crate-toy-json-formatter-0.1.4 (c (n "toy-json-formatter") (v "0.1.4") (h "0cqhmf5snh1kfwxcw3znrbkgy71h9kjvk131fby76x0p4sbbi9jv")))

(define-public crate-toy-json-formatter-0.1.5 (c (n "toy-json-formatter") (v "0.1.5") (h "1c3cal561k18ddxfjja0bw6c3d4196gwvavf0xhyi4pr7bj389p1")))

(define-public crate-toy-json-formatter-0.1.6 (c (n "toy-json-formatter") (v "0.1.6") (h "0xgyf3gg0yyfmxz3ypcyi7pcl1aja2n8s5c20g2qm8y18sk3dnxa")))

(define-public crate-toy-json-formatter-0.1.7 (c (n "toy-json-formatter") (v "0.1.7") (h "0pwk5kss3wnzy8803q0g4vmis8kd55r1cjxbfb3m4wnzpilpfzg8")))

(define-public crate-toy-json-formatter-0.1.8 (c (n "toy-json-formatter") (v "0.1.8") (h "1v2hyn49ykv4h37h3cmi6xjyvmsisxc953qaxf1dwc4s1nc7mv10")))

(define-public crate-toy-json-formatter-0.1.9 (c (n "toy-json-formatter") (v "0.1.9") (h "0hzkg9wmz843bw147778z6718k2phgfw0j3y9y83ih4h3bvcmclk")))

(define-public crate-toy-json-formatter-0.2.0 (c (n "toy-json-formatter") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (o #t) (d #t) (k 0)))) (h "0q782gjfqjh7qp20frmva8fx9hkgvlma4zz56vqhp71ixw0ldr8f") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-toy-json-formatter-0.2.1 (c (n "toy-json-formatter") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (o #t) (d #t) (k 0)))) (h "1jxq1hm028r001bgqhk48ff2vk0nqg0spsr3f2m4jp5zzj7v7cb7") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-toy-json-formatter-0.3.0 (c (n "toy-json-formatter") (v "0.3.0") (h "0lsl9xg1zdjf5kqk72ypi2f16p49bclighrrr4wyaivcfjh2swcn") (f (quote (("default"))))))

(define-public crate-toy-json-formatter-0.3.1 (c (n "toy-json-formatter") (v "0.3.1") (h "13waj0sfwdlmmn83v6jhykv1db5ywg53szkw4c48xg69qn78nwmb") (f (quote (("default"))))))

(define-public crate-toy-json-formatter-0.3.2 (c (n "toy-json-formatter") (v "0.3.2") (h "0mx0b5qnwmk8mkn8833q6ch4n827hkna5fdfcw8yim6y3zy4za8k") (f (quote (("default"))))))

(define-public crate-toy-json-formatter-0.3.3 (c (n "toy-json-formatter") (v "0.3.3") (h "1rh9qjjv90lh3p5q6q1d9h2x0f3hd4f25qfca1grkl8hvy2q2124") (f (quote (("default"))))))

(define-public crate-toy-json-formatter-0.3.4 (c (n "toy-json-formatter") (v "0.3.4") (h "0nvsm5nx5b88sq1clvh7hn8kk2ifwsqpxy6za3a68qbk8zzkpbgc") (f (quote (("default"))))))

(define-public crate-toy-json-formatter-0.3.5 (c (n "toy-json-formatter") (v "0.3.5") (h "00azizml2yf9y5xpryh5bnmgfjir68kr2lz4amyk72r8jqgsq4gv") (f (quote (("default"))))))

