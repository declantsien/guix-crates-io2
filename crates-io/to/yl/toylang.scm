(define-module (crates-io to yl toylang) #:use-module (crates-io))

(define-public crate-toylang-0.1.0 (c (n "toylang") (v "0.1.0") (h "0j1gidlvchdrjh9qj7q7n392zdibnklbr4zm0cii97b9pxb0xdmc")))

(define-public crate-toylang-0.1.1 (c (n "toylang") (v "0.1.1") (h "0i1bgngx4afz3s4hzln566c62cywa05kca9x7j0jgrndr3dffbfl")))

(define-public crate-toylang-0.1.2 (c (n "toylang") (v "0.1.2") (h "0x9211lqci96wya64d83ckg5av5fx4z7k901wq8cxmhd808p2w4p")))

(define-public crate-toylang-0.1.3 (c (n "toylang") (v "0.1.3") (d (list (d (n "toy_runtime") (r "0.1.*") (d #t) (k 0)) (d (n "toyc") (r "0.1.*") (d #t) (k 0)))) (h "0im7z7ca81m6vhjcg3xp0a4agmkm7wy3ilmi22sf2x3l3njsly3a")))

(define-public crate-toylang-0.1.4 (c (n "toylang") (v "0.1.4") (d (list (d (n "toy_runtime") (r "0.1.*") (d #t) (k 0)) (d (n "toyc") (r "0.1.*") (d #t) (k 0)))) (h "0kfw8x0jxf4nkcdjr9lyn7zw87lkvi4fn2k6d9nksiiz59krzxm7")))

