(define-module (crates-io to pa topaco_tasks) #:use-module (crates-io))

(define-public crate-topaco_tasks-0.1.0 (c (n "topaco_tasks") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1cls0n99jr342rmz72d67bm8gwd0y2w94lki0h1mx3bj3ljd18nc") (y #t)))

