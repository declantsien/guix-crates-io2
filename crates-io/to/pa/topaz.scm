(define-module (crates-io to pa topaz) #:use-module (crates-io))

(define-public crate-topaz-0.1.0 (c (n "topaz") (v "0.1.0") (d (list (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "147il2v4yl2r5f4mcp73fwjnfzbkmkc47spx0zj51ifwaj661d9k")))

(define-public crate-topaz-0.1.1 (c (n "topaz") (v "0.1.1") (d (list (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "1lcx3im8ngr8xciajmj0mzg15zj1pbyb1fxvyvhy5a0zhgyybnji")))

(define-public crate-topaz-0.2.0 (c (n "topaz") (v "0.2.0") (d (list (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "07ngc4ia05jmwa65fgwnpnzksiikpifm1az3gdgk51shqnd0dgrm")))

(define-public crate-topaz-0.3.0 (c (n "topaz") (v "0.3.0") (d (list (d (n "specs") (r "^0.10.0") (d #t) (k 0)))) (h "07a97mm9127kmnz37bakw7h3ns00arijxp7pis124gagnwg3mddh")))

(define-public crate-topaz-0.4.0 (c (n "topaz") (v "0.4.0") (d (list (d (n "specs") (r "^0.10.0") (d #t) (k 0)))) (h "040v17yzk1qnai5l24j2pzqs4mscgdc483kzds05hg20jqnzbxqf")))

