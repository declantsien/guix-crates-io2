(define-module (crates-io to ku toku_server) #:use-module (crates-io))

(define-public crate-toku_server-0.1.1 (c (n "toku_server") (v "0.1.1") (d (list (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "tcp"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)) (d (n "toku_connection") (r "^0.2.0") (d #t) (k 0)) (d (n "toku_protocol") (r "^0.2.0") (d #t) (k 0)))) (h "0vqjc5c8rkhv1nkxryq2j7q5qf2lqb0k5ll8sr4vbl8lawfppcly")))

