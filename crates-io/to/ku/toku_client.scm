(define-module (crates-io to ku toku_client) #:use-module (crates-io))

(define-public crate-toku_client-0.1.1 (c (n "toku_client") (v "0.1.1") (d (list (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "tcp"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)) (d (n "toku_connection") (r "^0.2.0") (d #t) (k 0)) (d (n "toku_protocol") (r "^0.2.0") (d #t) (k 0)))) (h "13f4a596cbrrjrdbxcmwzwhb44qbwfqg4x3052zvr4lbhipkq0x0")))

