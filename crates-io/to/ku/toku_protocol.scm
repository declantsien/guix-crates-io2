(define-module (crates-io to ku toku_protocol) #:use-module (crates-io))

(define-public crate-toku_protocol-0.2.0 (c (n "toku_protocol") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)))) (h "0yp15vg6j4ai01ccjc60q45sxmkcz7fxzalnr1q6d5wlssc5wc6r")))

