(define-module (crates-io to ku toku_connection) #:use-module (crates-io))

(define-public crate-toku_connection-0.2.0 (c (n "toku_connection") (v "0.2.0") (d (list (d (n "backoff") (r "^0.1.2") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "tcp" "time" "stream"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.2") (f (quote ("codec"))) (d #t) (k 0)) (d (n "toku_protocol") (r "^0.2.0") (d #t) (k 0)))) (h "0zlkachxydyj64vcr88p3ii72hnyyqf5m2m1r87aqlkjvbzr9a3w")))

