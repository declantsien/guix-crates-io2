(define-module (crates-io to ky tokyocabinet-sys) #:use-module (crates-io))

(define-public crate-tokyocabinet-sys-0.1.0 (c (n "tokyocabinet-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vergen") (r "^0.1") (d #t) (k 1)))) (h "1gqal0z6daib0inadl7m2k8jc2jssk150s30w95r7qsf0v0yryhl")))

