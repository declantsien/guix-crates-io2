(define-module (crates-io to ky tokyodoves) #:use-module (crates-io))

(define-public crate-tokyodoves-0.1.0 (c (n "tokyodoves") (v "0.1.0") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y55n6ry23zs20p07b3x2cn54152pzyvwavqz5d1mj1b3z3g942g")))

(define-public crate-tokyodoves-0.1.1 (c (n "tokyodoves") (v "0.1.1") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08zjrmij3d9vjncppwmh1smq2pplv1m5kqjliya7hdq9d8a9vbcl")))

(define-public crate-tokyodoves-0.1.2 (c (n "tokyodoves") (v "0.1.2") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q5vwz32ak9qzdiizvgqmgwk7kh65i22pwsh6gwjdpsgfjbz0msp")))

(define-public crate-tokyodoves-0.1.3 (c (n "tokyodoves") (v "0.1.3") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wndjz1ry5an0c94z3jbpxwy49qc8xsh4cwknpy9lcmw3qjya6lq")))

(define-public crate-tokyodoves-0.1.4 (c (n "tokyodoves") (v "0.1.4") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07dl4ld00m1wd4f17vl5gaxf0fkqlv77ascv9xrszgaqfjdadz4n")))

(define-public crate-tokyodoves-0.1.5 (c (n "tokyodoves") (v "0.1.5") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17hlaxvjijfv73l8rqxq53ik1y3yy4hmfd5q7xbhcbh9xfkafaw9")))

(define-public crate-tokyodoves-0.1.6 (c (n "tokyodoves") (v "0.1.6") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13qh0lq9s4wrqsjskbplnjcx5rzj79ajg0iqql7cwsh98bylrszi")))

(define-public crate-tokyodoves-0.1.7 (c (n "tokyodoves") (v "0.1.7") (d (list (d (n "array-macro") (r "^2.1.5") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cmmi690a7angdxlqi5ygyfrd6rcwz674qr819cv3gn2hs9sn9rj")))

(define-public crate-tokyodoves-1.0.0 (c (n "tokyodoves") (v "1.0.0") (d (list (d (n "array-macro") (r "^2.1.8") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0q20yk1jdq223nza7w27qni9kcaa2yr24iz7fqcidzy53x6j4md0") (f (quote (("game") ("default") ("analysis" "game"))))))

(define-public crate-tokyodoves-1.0.1 (c (n "tokyodoves") (v "1.0.1") (d (list (d (n "array-macro") (r "^2.1.8") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1l071k5gj79qa5vpivysly6rm2jhnlkypqgm6wqa1syqm1w9imzg") (f (quote (("game") ("default") ("analysis" "game"))))))

(define-public crate-tokyodoves-1.0.2 (c (n "tokyodoves") (v "1.0.2") (d (list (d (n "array-macro") (r "^2.1.8") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0hinhf438091rs6d4m26wp39766c0ffgwkw2ncd9mian6nqm8fpc") (f (quote (("game") ("default") ("analysis" "game"))))))

