(define-module (crates-io to ky tokyo) #:use-module (crates-io))

(define-public crate-tokyo-0.1.0 (c (n "tokyo") (v "0.1.0") (h "08vfw8wf4wsnc4v0cmbz3jybrlf6w5z69q5g1c98rcq6wl8ra5i8")))

(define-public crate-tokyo-0.2.0 (c (n "tokyo") (v "0.2.0") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1g8pyrddip9905dsp4rxqsqw2nbl752zwg1384sh32lszzjlfz69")))

(define-public crate-tokyo-1.0.0 (c (n "tokyo") (v "1.0.0") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1iqn3pl0r1bkff04ri8slfq2q0bb7i395acnk34q8p611x2k6ia8")))

