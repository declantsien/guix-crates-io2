(define-module (crates-io to le tolerance) #:use-module (crates-io))

(define-public crate-tolerance-1.0.0 (c (n "tolerance") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "12wxw7m1pclv1qz8kpjx1nq4pvr1qsk19rs29wdzc96yxwr4rwz5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tolerance-1.0.1 (c (n "tolerance") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1s66m2ggw2g0ppda01qzl2l31hlpyx68x12fn2fgxvy6xcki39pr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tolerance-1.0.2 (c (n "tolerance") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1byzyxdsvzp1zs8anpvxaw31clpj5scz8744cjk2zw0pkwmqaly0") (s 2) (e (quote (("serde" "dep:serde"))))))

