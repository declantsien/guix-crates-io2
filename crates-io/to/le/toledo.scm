(define-module (crates-io to le toledo) #:use-module (crates-io))

(define-public crate-toledo-0.1.0 (c (n "toledo") (v "0.1.0") (d (list (d (n "ascii_converter") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "tiny_http") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "14n33ysj2i142li5bxcswjwdsrkkbh9a76xg4d4lyiahcgk8hnx9")))

(define-public crate-toledo-0.1.1 (c (n "toledo") (v "0.1.1") (d (list (d (n "ascii_converter") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serialport") (r "^4") (d #t) (k 0)) (d (n "tiny_http") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1j5y8jbxrh2i20ac9hq48dmq06d7z3z12qccv128pqfzmylc6sk5")))

