(define-module (crates-io to uk touka) #:use-module (crates-io))

(define-public crate-touka-0.1.0 (c (n "touka") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "0nw92kxxahm3ba7nswyzk9wxkyjw0xhv9yipyxs6d8ddzxrd18sw")))

(define-public crate-touka-0.2.0 (c (n "touka") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "16fngrm4linb3126x84dc2i3bsi4ara7w86wnl1jwdr9qxjc19r5")))

(define-public crate-touka-0.2.1 (c (n "touka") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "1q5cv5snc4i31zsynhpc0sw73ndfdikwaklyvyg6ngjvxbr6izbk")))

(define-public crate-touka-0.2.2 (c (n "touka") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "0nigrm347r29m09lcaz9dza90n9hfqvn5lymkvmgh39kswm7papc")))

(define-public crate-touka-0.2.3 (c (n "touka") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "0a3ya7x0j5sgbdapj5mk5jldd2pw148hbkz185whgwbyjwdvgdvd")))

(define-public crate-touka-0.2.4 (c (n "touka") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "0i8yxpixm0221k842lb26dmzbj2rgnlnqvkqac27ik6gvrsrk4sv")))

(define-public crate-touka-0.2.5 (c (n "touka") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "11cnd7l9j7mdagm619bdmgz8qyayw304h3azyvnhs1gnhsyj3szw")))

(define-public crate-touka-0.2.7 (c (n "touka") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1cnf9z2xnwcp6fbjb60alpfch9gkzh6dl6lbfwf418lhlj6vkmj5")))

(define-public crate-touka-0.3.0 (c (n "touka") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "imboard") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1f3jrv1la48c6nmmkah41xrv8936nwzcy7gq59qviy0qz2df5igr")))

(define-public crate-touka-0.3.3 (c (n "touka") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "imboard") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0a9hz8s871k8wd95893cxndn36148cqnq526l20cxxmva90m0zdr")))

(define-public crate-touka-0.3.4 (c (n "touka") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "imboard") (r "^0.2.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0y1vz018p38m8bci7vfff4jwvcqdapgismldwlrpszqd9rcjz52l")))

(define-public crate-touka-0.3.5 (c (n "touka") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)) (d (n "imboard") (r "^0.2.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1k76r8jdwviy4pw7xpyph79ka2zzadmr3csjnyh12x2nshdwmp6j")))

