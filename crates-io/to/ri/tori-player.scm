(define-module (crates-io to ri tori-player) #:use-module (crates-io))

(define-public crate-tori-player-0.1.0 (c (n "tori-player") (v "0.1.0") (d (list (d (n "ac-ffmpeg") (r "^0.18.1") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rb") (r "^0.4.1") (d #t) (k 0)) (d (n "rubato") (r "^0.13.0") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all-codecs"))) (d #t) (k 0)))) (h "0ca98hyyfc8hd4kccgxb3mggi8m2j5b5a2n9nbzwd309ci6srdap")))

