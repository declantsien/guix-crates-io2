(define-module (crates-io to ri torito-rs) #:use-module (crates-io))

(define-public crate-torito-rs-0.0.1 (c (n "torito-rs") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)))) (h "1kd42dzf9s6a0r2whh3q9jh4bwl1zkk60idldya1mnffga9adjwr")))

(define-public crate-torito-rs-0.1.0 (c (n "torito-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mhlyr5rzhj77mv8ac9314rcq6kcyhmiwa2ll2d3a4ijqxsvjmvn")))

