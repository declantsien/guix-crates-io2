(define-module (crates-io to ri torin) #:use-module (crates-io))

(define-public crate-torin-0.1.0 (c (n "torin") (v "0.1.0") (d (list (d (n "dioxus-core") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dioxus-native-core") (r "^0.4") (f (quote ("dioxus"))) (o #t) (d #t) (k 0)) (d (n "euclid") (r "^0.22.7") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "17z7m7gqiy1vr5v15fn6ng15mm2ldvk4c98rgfsyc90f0wazgmgf") (f (quote (("default" "dioxus")))) (s 2) (e (quote (("dioxus" "dep:dioxus-native-core" "dep:dioxus-core"))))))

(define-public crate-torin-0.2.0 (c (n "torin") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "euclid") (r "^0.22.9") (d #t) (k 0)) (d (n "freya-native-core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0nm6zdvl866pqbickp3gz77085f9fldmn6hfc62pgyiczbvvj7jv") (f (quote (("default" "dioxus")))) (s 2) (e (quote (("dioxus" "dep:freya-native-core"))))))

