(define-module (crates-io to ro torophy) #:use-module (crates-io))

(define-public crate-torophy-0.1.0 (c (n "torophy") (v "0.1.0") (d (list (d (n "glium") (r "^0.26") (d #t) (k 2)) (d (n "imgui") (r "^0.3.0") (d #t) (k 2)) (d (n "imgui-glium-renderer") (r "^0.3.0") (d #t) (k 2)) (d (n "imgui-winit-support") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "04zvwp1mjzqfxqdq4q6s17l08l2q45nl5yy06fdivfy1ncarxmn4")))

(define-public crate-torophy-0.1.1 (c (n "torophy") (v "0.1.1") (d (list (d (n "glium") (r "^0.26") (d #t) (k 2)) (d (n "imgui") (r "^0.3.0") (d #t) (k 2)) (d (n "imgui-glium-renderer") (r "^0.3.0") (d #t) (k 2)) (d (n "imgui-winit-support") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1y80psygq7bfmx420rxj9y3s0jjrx6fb1qmrbmq3zl98n9lv1xrr")))

