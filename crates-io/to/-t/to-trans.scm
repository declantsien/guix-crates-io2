(define-module (crates-io to -t to-trans) #:use-module (crates-io))

(define-public crate-to-trans-0.1.0 (c (n "to-trans") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0ap2j01zkk83m7ixag2pjd0yxsh4b9kagdispxhsv7jf5wm3352x")))

(define-public crate-to-trans-0.2.0 (c (n "to-trans") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "021qjl1zksic104cybzb7qlqlh4ada4vgi7jmnk2kh5j87r4b1s4")))

