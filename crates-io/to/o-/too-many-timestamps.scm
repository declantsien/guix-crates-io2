(define-module (crates-io to o- too-many-timestamps) #:use-module (crates-io))

(define-public crate-too-many-timestamps-0.1.0 (c (n "too-many-timestamps") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)))) (h "1gc6nmdd5hp8blasdwypxj0dbxdy4b8d3a1nchfjd5yqgaqysbfj")))

