(define-module (crates-io to te totems) #:use-module (crates-io))

(define-public crate-totems-0.1.0 (c (n "totems") (v "0.1.0") (h "1x6r515swclck3nqm9s17w2pi6ikws0jl7r7gw0q8hgglk81xyj4")))

(define-public crate-totems-0.1.1 (c (n "totems") (v "0.1.1") (h "1xzjm3zmh99bxr3vxvk9n0r45b3rphjvcrdb3vd8chym6vqsnfk6")))

(define-public crate-totems-0.1.2 (c (n "totems") (v "0.1.2") (h "1p4z279i7gva2387dgdbfd4wxcqq40c5r32i1i9gr3j1zs7idz5k")))

(define-public crate-totems-0.1.3 (c (n "totems") (v "0.1.3") (h "10jyv3h226vw3h8sfwn2srhr1a794379zcck2anqchblq4ws1hsg")))

(define-public crate-totems-0.1.4 (c (n "totems") (v "0.1.4") (h "19w7xi50bwhrj38fm01d3s6g1wh53g0jb5dzkhxp1jm0d0zd649b")))

(define-public crate-totems-0.1.5 (c (n "totems") (v "0.1.5") (h "1a5lgzm608cqd19h1wgy4a9gk0vh1wwfpgcyjspw3qs4jwlldla4")))

(define-public crate-totems-0.1.6 (c (n "totems") (v "0.1.6") (h "1lppa47wrwraskd0k4v8w8483aralyjd8vljqbjx9ys84465vm87")))

(define-public crate-totems-0.1.7 (c (n "totems") (v "0.1.7") (h "08vwliy6n4qq1jijf5qbykssw3ic158r1m0wiwazkrgsa4f5hcg4")))

(define-public crate-totems-0.1.8 (c (n "totems") (v "0.1.8") (h "1041p07hnsfjckdapvmh9ly6v7q6wry4gak1rbr8i1vr196xhnss")))

(define-public crate-totems-0.1.9 (c (n "totems") (v "0.1.9") (h "0z6dfwpmvih93jyf0aslhrabr6xcpk70wdhhwnda2ibri39skkjs")))

(define-public crate-totems-0.2.0 (c (n "totems") (v "0.2.0") (h "1xwaicxyyfmjcyhw7pmb541gzmq58wgkmqp3dll7ndab2sbz6512")))

(define-public crate-totems-0.2.1 (c (n "totems") (v "0.2.1") (h "1nhxn6f881pakm7ms7vga7mlk2pndmh2pdfq3811cq549vy5b0yg")))

(define-public crate-totems-0.2.2 (c (n "totems") (v "0.2.2") (h "013nx2r7kdj08nh12jjsc1qv46prhaqd1pm4sv20b9b8xrvz47f2")))

(define-public crate-totems-0.2.3 (c (n "totems") (v "0.2.3") (h "1cmamqqn01wa1nrahiz7z3wvm001sr3pz4fkbb3v5rqikqg479wc")))

(define-public crate-totems-0.2.4 (c (n "totems") (v "0.2.4") (h "187sy6xpzmqz5kdqppk21kvpsnvcqrzicvink9k25w19yixa6z6g")))

(define-public crate-totems-0.2.5 (c (n "totems") (v "0.2.5") (h "01h15kpak44cibjdj05z1s2xbrc7x7jc38vi73l6vrzjv0bjxqa0")))

(define-public crate-totems-0.2.6 (c (n "totems") (v "0.2.6") (h "1mfhlg0fl58prpibz4h5kgifhcqsjifwszd63hcwrp210lpv9x7a")))

(define-public crate-totems-0.2.7 (c (n "totems") (v "0.2.7") (h "0sp1ay6ql1clflq8haffk1mld85xw81lay51ps8rcwxlni4xxlff")))

