(define-module (crates-io to rs tors_to_json) #:use-module (crates-io))

(define-public crate-tors_to_json-0.1.0 (c (n "tors_to_json") (v "0.1.0") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)))) (h "1pdilz8dzqvjnx3wp9sgzqmr0i8d806dbf4h95hmyj7mffs8m4yc")))

(define-public crate-tors_to_json-0.2.0 (c (n "tors_to_json") (v "0.2.0") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)))) (h "0s9im8aqb9cppinhx1xngljybzxarf0czjck1r8ari7hbibi5kjz")))

(define-public crate-tors_to_json-0.2.1 (c (n "tors_to_json") (v "0.2.1") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)))) (h "0m4gxh0j84bhs8x8v68bcs2sdnx9dzk9zki8sh8ndh5gvyk72px1")))

(define-public crate-tors_to_json-0.2.2 (c (n "tors_to_json") (v "0.2.2") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)))) (h "0h08128lmb6zcmxmsw08sdx03szs41l6bz2d15pq86shb5a93qpa")))

