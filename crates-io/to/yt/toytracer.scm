(define-module (crates-io to yt toytracer) #:use-module (crates-io))

(define-public crate-ToyTracer-0.0.1 (c (n "ToyTracer") (v "0.0.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "piston") (r "^0.53") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.43") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.82") (d #t) (k 0)) (d (n "piston_window") (r "^0.128") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.71") (d #t) (k 0)))) (h "1pncjpajbdr89lcx6z0gc04pm3hiwcr5k7smmwgsll21a4xd78ny")))

