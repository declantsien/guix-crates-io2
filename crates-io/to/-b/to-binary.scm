(define-module (crates-io to -b to-binary) #:use-module (crates-io))

(define-public crate-to-binary-0.1.0 (c (n "to-binary") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "19hcp1hlf7s005i2xpbi1jkm29rg062hikc9p8fq6jz5b0vx8cd3")))

(define-public crate-to-binary-0.2.0 (c (n "to-binary") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1691mj9wahp387j09x7624r84f6qwkl96yk02fiybdh243mp4gnw")))

(define-public crate-to-binary-0.2.1 (c (n "to-binary") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "03f6yixvw595rpamhpwbw56n2dy3xfrsndabhz7chbrrz2aw7gqh")))

(define-public crate-to-binary-0.3.0 (c (n "to-binary") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0a02fr2l5c3943995wj5z7qaj6dqb8dn1yfp6ssbvv2b5x41q6c8")))

(define-public crate-to-binary-0.4.0 (c (n "to-binary") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1qxv4c46vp6nb9jiid0vv07vj0bl9fjyiw41rpximza8r0mma924")))

