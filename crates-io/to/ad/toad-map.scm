(define-module (crates-io to ad toad-map) #:use-module (crates-io))

(define-public crate-toad-map-0.2.0 (c (n "toad-map") (v "0.2.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.1") (k 0)))) (h "1lv6qx4gbg091rckbhawk0ybvjd4g56z1yy8x15waqjghlf3dyb0") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-map-0.2.1 (c (n "toad-map") (v "0.2.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.1") (k 0)))) (h "05565byj4yfrrk0js4sf7xwkp22vadrhmqyff53d03yavgznp4bq") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-map-0.2.2 (c (n "toad-map") (v "0.2.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "10h5akrm8ksfp373imz6gfgr751sk5pgvh216w87fy539x27vgha") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-map-0.2.3 (c (n "toad-map") (v "0.2.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "060959xxynnfzjynjvd3xnpgdvc8higclxy6inipx0aa05x58ga2") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-map-1.0.0 (c (n "toad-map") (v "1.0.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "01g7yjlmbcjqyaidriiginqm73lgmrkfc6pm1m31i5mn4lzi8ywl") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc")))) (y #t)))

(define-public crate-toad-map-1.0.0-beta.1 (c (n "toad-map") (v "1.0.0-beta.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "15ixbxbsa0aa5nvww1i1bd2fjimkvmyc26a489abv81m705682r3") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-map-1.0.0-beta.2 (c (n "toad-map") (v "1.0.0-beta.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "152rqjwyz3qrkcr8lxzwrjmpvhjsv3wfhp3sl4scw675jzl3250h") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-map-1.0.0-beta.3 (c (n "toad-map") (v "1.0.0-beta.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "1wnqhkf5laism8nv917w012d7m801d1qw4m806nwmnr5zqrzlyvn") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

