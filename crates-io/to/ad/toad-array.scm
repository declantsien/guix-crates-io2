(define-module (crates-io to ad toad-array) #:use-module (crates-io))

(define-public crate-toad-array-0.2.1 (c (n "toad-array") (v "0.2.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.0") (k 0)))) (h "1vs38vqnn22qdqhy9q5sfipzqv12wam9klnsgd5k2f08gdq2qvzx") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.2.2 (c (n "toad-array") (v "0.2.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.1") (k 0)))) (h "0dra4r7fbzr7vbnm4ncqj79n2jbvfxqink0qdv8qcybgi2k223ml") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.2.3 (c (n "toad-array") (v "0.2.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "13spyd78ndy8inxqk000pnjjk5rziyd3rzslx0gyggwc7g8jagzw") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.3.0 (c (n "toad-array") (v "0.3.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "00lvmg8ml02yp1qncmxj5cank5grci5da2vf869zl6s4v5qkc591") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.4.0 (c (n "toad-array") (v "0.4.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "0gwcksv13fhrrds3djl87cr4syyi66ggd42ya7xlpzbkh4mgxn42") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.5.0 (c (n "toad-array") (v "0.5.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "0npm0nsszd4yw7svsflnf7pij2jzgggvy8xgp0vzp50la2hh0jk8") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.6.0 (c (n "toad-array") (v "0.6.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "05h0y7z0a1j2fscz6aqh134f1lgvkdhvbwpxm2cm33w7g2syfwlz") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.7.0 (c (n "toad-array") (v "0.7.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "0wzdvwpfzky6r6ybh4a9prvslsm9ym276h302x8lqz3g2sz4p1ll") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-0.8.0 (c (n "toad-array") (v "0.8.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "1rlgcpsxgphlb4z2z5rgz369nghm2lvg994yl0bx57isfrjrrj4h") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-1.0.0 (c (n "toad-array") (v "1.0.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "1gmvv2z4s7125dsi1k60bvin7bhrramnz9pdyz9gwdfafrvzzxz4") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc")))) (y #t)))

(define-public crate-toad-array-1.0.0-beta.1 (c (n "toad-array") (v "1.0.0-beta.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "0dz3vmqhx7pc8pwplfbp8j5m69kyy44wgrv7a7bxcwm4j8hssv16") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-1.0.0-beta.2 (c (n "toad-array") (v "1.0.0-beta.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "1015yvnqm2c4j6zywgsanlsw87yhxr9fzx2x8d0z5jvixvw8y6w5") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

(define-public crate-toad-array-1.0.0-beta.3 (c (n "toad-array") (v "1.0.0-beta.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)) (d (n "toad-len") (r "^0.1.2") (k 0)))) (h "1gh2y4zqlpzf0873swjaz3nwzrn5yf5l9hqv2hvi96369ry5c5ry") (f (quote (("test") ("std" "alloc" "toad-len/std") ("docs") ("default" "std") ("alloc" "toad-len/alloc"))))))

