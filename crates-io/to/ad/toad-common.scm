(define-module (crates-io to ad toad-common) #:use-module (crates-io))

(define-public crate-toad-common-0.8.0 (c (n "toad-common") (v "0.8.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1sjfljba4r5sx2pfd7sxr7w2lgi2y07nl15k399wfpgyy5bg1ac8")))

(define-public crate-toad-common-0.9.0 (c (n "toad-common") (v "0.9.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0qz4snpai0qfvvjkl8qbz4hwwm5bf000p6ndj4247na8s7616vfp")))

(define-public crate-toad-common-0.10.0 (c (n "toad-common") (v "0.10.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1z4dq1blajbis81qfn9232jnz82ab0mn60pywnl71iilfp35a5nm") (y #t)))

(define-public crate-toad-common-0.11.0 (c (n "toad-common") (v "0.11.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1vp6bicn5gp07yj1zqcdv65g0dads02cx3jsnjdadyyni37pv864")))

(define-public crate-toad-common-0.11.1 (c (n "toad-common") (v "0.11.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0p6yg6l1mpwxa03caf95d928lp9qmm6bd6lrpmwa1ay2sw1v005k") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-0.11.2 (c (n "toad-common") (v "0.11.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "07294j9x43cpr2pf4m1ddsy36c9lxxznmci62gyq7dsr0k6d8imd") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-0.11.3 (c (n "toad-common") (v "0.11.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "01j3p568811srz5yly3ibafkkz61bmq6c1lpxfgx3fn22dw30jv2") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-0.12.0 (c (n "toad-common") (v "0.12.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0ic8gvsngiqcx5dpq3i7ikb7jzy5l5s5jqk3czck3g50j1fhcwr8") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-0.13.0 (c (n "toad-common") (v "0.13.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1ahw5sj04rs7mskp0b7scaac30jvnzmsd2gjhfni1y64ryjy4phf") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-0.14.0 (c (n "toad-common") (v "0.14.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "168r51qfiy904hi455ylxqac3m810z0nr8s53xsp281x8283vggg") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-0.15.0 (c (n "toad-common") (v "0.15.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1yli1bx4h0mjjra2z0mlg6r093wil7cjsylkz80v3l28v8z8g89y") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-1.0.0 (c (n "toad-common") (v "1.0.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0xpy3gwfk2bhwqqry7xv41847a67m73saymsg2f9ccswbknd9ghz") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-toad-common-1.0.0-beta.1 (c (n "toad-common") (v "1.0.0-beta.1") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0hzljb07w3ds63izkf2s35kws5igzwg80z1ml96n4xfikbas9d89") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-1.0.0-beta.2 (c (n "toad-common") (v "1.0.0-beta.2") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "08rsxjyqxprp6wsbfgafvvk6a1pzf4sxvz1f04bwgacidl3rdwb7") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-common-1.0.0-beta.3 (c (n "toad-common") (v "1.0.0-beta.3") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0v3dgniwxcgwxwymwxsdhblwdzpphckn302ppv3hhvgvpcd26dqz") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

