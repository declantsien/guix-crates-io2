(define-module (crates-io to ad toad-stem) #:use-module (crates-io))

(define-public crate-toad-stem-0.1.0 (c (n "toad-stem") (v "0.1.0") (h "0z4jxypm2mb75m6ipggm8nlnyrnxkkhks7c6zd6v49nndv27qjl3") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-stem-1.0.0 (c (n "toad-stem") (v "1.0.0") (h "0f16hz95z502gjyw2q5b6pcmk2qnalzi3ixcchxg252fs0c4ap2l") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-toad-stem-1.0.0-beta.1 (c (n "toad-stem") (v "1.0.0-beta.1") (h "02isazxg7x3k8hg7izsl3krdc088bmxklxj73frag186iynrjmcd") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-stem-1.0.0-beta.2 (c (n "toad-stem") (v "1.0.0-beta.2") (h "1bcy155vwcm63w3gz1dd3cg6yrsmpd4r7x4v05cyzd7yv66zs1fa") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-stem-1.0.0-beta.3 (c (n "toad-stem") (v "1.0.0-beta.3") (h "02lza262j4bsqf1mgq7c8ja87pfi0j2fwncdh03v861db9wznw0r") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

