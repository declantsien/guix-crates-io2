(define-module (crates-io to ad toad-hash) #:use-module (crates-io))

(define-public crate-toad-hash-0.2.0 (c (n "toad-hash") (v "0.2.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)))) (h "1zvh3r5icgkkkg84vafvzax7mjr59if5xg5lrjg5wky2v6k9pc7x") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-hash-0.3.0 (c (n "toad-hash") (v "0.3.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)))) (h "0fv4dg8i7hzr4hda5bl0925gfss6m4cb613dwz2b6jvjnnh7z56z") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-hash-1.0.0 (c (n "toad-hash") (v "1.0.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)))) (h "1hnlpbzwj7iynysrhf047r1s88q9aj6wjz8mlxxz1dw7wndjk053") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-toad-hash-1.0.0-beta.1 (c (n "toad-hash") (v "1.0.0-beta.1") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)))) (h "1gpz2w5qz4zifwgfl97f8gbckr7jzxi22yz7vm5di5jp30i9qd2a") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-hash-1.0.0-beta.2 (c (n "toad-hash") (v "1.0.0-beta.2") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)))) (h "0rbc3v0v3mjghbawfn5haidm0g315w9n65m0fi0r2xv79qh5rmy6") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-hash-1.0.0-beta.3 (c (n "toad-hash") (v "1.0.0-beta.3") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)))) (h "0j3r9f5a13pps87h5xaj37929nxmwrbicsf11qjyqwchj2mnar2b") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

