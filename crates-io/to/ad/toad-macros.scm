(define-module (crates-io to ad toad-macros) #:use-module (crates-io))

(define-public crate-toad-macros-0.2.0 (c (n "toad-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16kpmy84ds9czpmm3kfr9rx5sa4ykmi96lhdj8xy3dr5kh39cx3v")))

(define-public crate-toad-macros-1.0.0 (c (n "toad-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jwv11qk9i9mgqq4qy3744gkk02rgvknbv83q8snz9raxmkqhw3i") (y #t)))

(define-public crate-toad-macros-1.0.0-beta.1 (c (n "toad-macros") (v "1.0.0-beta.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18r0cybb35s9xx8r4c77l949fkhwfqj67f1014wz2h0ybip6l5qm")))

(define-public crate-toad-macros-1.0.0-beta.2 (c (n "toad-macros") (v "1.0.0-beta.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ym0sm1kx71sq4fl6gvix3dkrmpvgwlcqgf8jdy49pybk910hxwr")))

(define-public crate-toad-macros-1.0.0-beta.3 (c (n "toad-macros") (v "1.0.0-beta.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jxb4dldmfbbygcid95gv1i08b67pz7vp2ar8rc1qv6h02cbjyr5")))

