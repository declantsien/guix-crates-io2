(define-module (crates-io to ad toad-async) #:use-module (crates-io))

(define-public crate-toad-async-1.0.0-beta.1 (c (n "toad-async") (v "1.0.0-beta.1") (h "0fiwpj1qkgmhin1hcisvkdqwhl6pdqf7pm7cg557rwglifjix2qr") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-async-1.0.0-beta.2 (c (n "toad-async") (v "1.0.0-beta.2") (h "0rdy711kal0rvww5a880vrm63yr3pqipgymfwzf37x70gzg8r8ra") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

