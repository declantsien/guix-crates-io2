(define-module (crates-io to ad toado) #:use-module (crates-io))

(define-public crate-toado-0.3.0 (c (n "toado") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "16pj0bqrk72vflxry6ppvc64lpjp3ynni0fplh2fvd1r3r3x4ixs")))

(define-public crate-toado-0.4.0 (c (n "toado") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1n51glvamdqj12xfv8cq86injk0sdgp437rdyf5yvdhkcb9y7cck")))

(define-public crate-toado-0.5.0 (c (n "toado") (v "0.5.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0zamrwr842sla85sjzwkgfb7kp6a1ln4xav6b63hv201l3f6p5n0")))

(define-public crate-toado-0.11.4 (c (n "toado") (v "0.11.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "147vd213p1ffmgnl2v6plw7m8z1y27l9v0v5nja51szc93sg8i79")))

