(define-module (crates-io to ad toad-cursor) #:use-module (crates-io))

(define-public crate-toad-cursor-0.2.0 (c (n "toad-cursor") (v "0.2.0") (h "0gn3wx7hgpljkgavrkbbv7qfcwhvmsl6i7shz3dax7vibbg8vvhd") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-cursor-1.0.0 (c (n "toad-cursor") (v "1.0.0") (h "07ixn480idmz9sgdz57fh5rslax7i71qn4nw406kz98ialf0d7mw") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-toad-cursor-1.0.0-beta.1 (c (n "toad-cursor") (v "1.0.0-beta.1") (h "12qm4lw0fg48mnxy9bsjb75dx8zc81vf2ld5jlrfxsdqhxnm8rnk") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-cursor-1.0.0-beta.2 (c (n "toad-cursor") (v "1.0.0-beta.2") (h "06v7f3rbxd5d68sy4z56lh362pb0w9b0fwg28fhsspm8qwm4llkm") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-cursor-1.0.0-beta.3 (c (n "toad-cursor") (v "1.0.0-beta.3") (h "1cn0zz9chh2d01arnwjyc01j9im87cf8fji04739s0kmfj1cnl3j") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

