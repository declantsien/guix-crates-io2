(define-module (crates-io to ad toad-len) #:use-module (crates-io))

(define-public crate-toad-len-0.1.0 (c (n "toad-len") (v "0.1.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0ik3zfzynpy062n6bjxpzl4aj5v5ifg3ypzmgb93d502wwq9jln2") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-len-0.1.1 (c (n "toad-len") (v "0.1.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1zwi4jj8kbgpbsm9dy5x37fjnnws7xsd9zd56r51cjnrar7pcbhx") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-len-0.1.2 (c (n "toad-len") (v "0.1.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "06q9brkm35p4a8shriz9wgr04srscgj4acr0c6sgh9cppzykws31") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-len-0.1.3 (c (n "toad-len") (v "0.1.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0dvbwg0bjwzpqly5sy238lvi0nyzn0q58523f0xc094k9c9ag6w1") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-len-1.0.0 (c (n "toad-len") (v "1.0.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1pw1ahvmvdrqir72fk8q3d3arivifri17g36xmg7pk5gflb1akqa") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-toad-len-1.0.0-beta.1 (c (n "toad-len") (v "1.0.0-beta.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "077j96p9svyymm5k8pghg8fk6b6pbbm2wps41fdpvzmhz4z6afy7") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-len-1.0.0-beta.2 (c (n "toad-len") (v "1.0.0-beta.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1531pgblib8nfx230qb76sgxgs93my13x974cpbfzg90psbk7idn") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-toad-len-1.0.0-beta.3 (c (n "toad-len") (v "1.0.0-beta.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "14arrv6vfndljb23rxzqwxdw9l6qv9gxgsiqlifi5f1kfbwjndkn") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

