(define-module (crates-io to rt tortoise) #:use-module (crates-io))

(define-public crate-tortoise-0.1.0 (c (n "tortoise") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w5f7xnx09pkw2wm2mn2hzk8573h5ixq6dicv74qq6p6hiba0l3c")))

