(define-module (crates-io to rt tortuga-guest) #:use-module (crates-io))

(define-public crate-tortuga-guest-0.1.0 (c (n "tortuga-guest") (v "0.1.0") (h "117c737bzqfwyy2pk40l4jl66fr0h2w5jfpl63447vwcci5qcpmn")))

(define-public crate-tortuga-guest-0.1.1 (c (n "tortuga-guest") (v "0.1.1") (h "0f3bihw38mqas1rrjf1apkmg8103g5g6q6y1hip114cnjmksnyqg")))

