(define-module (crates-io to rt tortank) #:use-module (crates-io))

(define-public crate-tortank-0.1.0 (c (n "tortank") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1crcjn2mfhj3hwsgxfqfk45haic612s1vgskj4ja1j4iya5vrj3s")))

(define-public crate-tortank-0.1.1 (c (n "tortank") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qi6bgb3ndkwz07002khk8f020sg09a26djrmhn7a5yk7bxrzgc7")))

(define-public crate-tortank-0.1.2 (c (n "tortank") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0x510mq7p5960lyvdraa7hy2s038fxqp1qkl2k2z0psyxrvbc0x1")))

(define-public crate-tortank-0.1.3 (c (n "tortank") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rlgz78wswqjrvipva73b7bw1asca0dzxcl4i8s1m4q58znx09fy")))

(define-public crate-tortank-0.1.4 (c (n "tortank") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "02nz18by9f4dcbinnw4h016ffvwpv3n828zvnsbfxqx72gzbas40")))

(define-public crate-tortank-0.1.5 (c (n "tortank") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "09987l0dykwxv0gm04alywg59wggda61gbv4mz9i89d1j8k20a2f")))

(define-public crate-tortank-0.1.6 (c (n "tortank") (v "0.1.6") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1as8cw801i61mjkg7x27fa4wxl32mzqc7paidii9hn73qvjrxpwi")))

(define-public crate-tortank-0.1.7 (c (n "tortank") (v "0.1.7") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1h3jf9xv03ykkkaqs556vr66i6d52gvy20h562iajkqcdh9hs9cb")))

(define-public crate-tortank-0.1.8 (c (n "tortank") (v "0.1.8") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1n8k58wy2wwkh7w2vqxq0m2gzxdp82iz4dvy33lqj47cq50yq5kb")))

(define-public crate-tortank-0.1.9 (c (n "tortank") (v "0.1.9") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bxh3p1p3mzp4a82hnr5d73gfbyavy2lqf6qx7aww7w2g42brm9r")))

(define-public crate-tortank-0.1.10 (c (n "tortank") (v "0.1.10") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "07j0lp06b2jp0bsf2wyhh0n7wkdk7ir4mha1cmv8yqz49w91mjvl")))

(define-public crate-tortank-0.1.11 (c (n "tortank") (v "0.1.11") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "15czbj7q4zxv35shq29wswpc936n1lvsy95kx5lw5jp24jnxqn7g")))

(define-public crate-tortank-0.1.12 (c (n "tortank") (v "0.1.12") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "128kj8ns6grnfs201d2v0la23yz1fw1xxdikz3zinlnpwsbp893r")))

(define-public crate-tortank-0.1.13 (c (n "tortank") (v "0.1.13") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0v1hv3rp9shpik2cl84z3zg24vpczdw87vwy4fwm3963ag0hrdhq")))

(define-public crate-tortank-0.1.14 (c (n "tortank") (v "0.1.14") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1857293siy5g673q8yccv3wsga0cms26cz206lwspkd5rf7rpnzr")))

(define-public crate-tortank-0.1.15 (c (n "tortank") (v "0.1.15") (d (list (d (n "chrono") (r "^0.4.27") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jqq4xghvvr5dgnk216m93668g1x60vd7zxx1q4hy6xr91w1ngaa")))

(define-public crate-tortank-0.1.16 (c (n "tortank") (v "0.1.16") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1axkxwj6lami70m46dwk3wyzv972vc47q3a69ilgrnsx5p5z8k0z")))

(define-public crate-tortank-0.1.17 (c (n "tortank") (v "0.1.17") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nckv5yrfh98rxa7lq5z495wi37akdbq37vd1a24qjks699bw9ix")))

(define-public crate-tortank-0.1.18 (c (n "tortank") (v "0.1.18") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0d0pfzr14g3am68f7ijwbck567hhx0qna125v5zzi7vy09c2g40j")))

(define-public crate-tortank-0.1.19 (c (n "tortank") (v "0.1.19") (d (list (d (n "chrono") (r "^0.4.28") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bqm5l3zxa9gw4fm8qx751alx0fhlaq2l8692ay8zl03s5dyad7j")))

(define-public crate-tortank-0.20.0 (c (n "tortank") (v "0.20.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0najhcvcw0lffz3xmlx5qaf4j23sij0bz8rmbfh5r4643xwqvh5w")))

