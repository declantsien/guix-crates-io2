(define-module (crates-io to pf topfew) #:use-module (crates-io))

(define-public crate-topfew-0.1.0 (c (n "topfew") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1p3c3dl2c7znfz4bjydzpdgvb7ly6ban5ixsmxn1z5cczif0sdz2")))

(define-public crate-topfew-0.1.1 (c (n "topfew") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "162hx6waw3bzxzsr5ilxgwzqlbjxi2mr9n04iknpxc62p5l2m0c7")))

(define-public crate-topfew-0.1.3 (c (n "topfew") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0hh3nr7wvk4br86rd3jifqlwmal65rfs4dpf5hf6bmrfvd01351s")))

(define-public crate-topfew-0.2.0 (c (n "topfew") (v "0.2.0") (d (list (d (n "ahash") (r "^0.5.1") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0hm15746w2ydv8v52n85qw15s8kzrlmx5cg419ggi9z105ppywqh")))

(define-public crate-topfew-0.2.1 (c (n "topfew") (v "0.2.1") (d (list (d (n "ahash") (r ">=0.6.1, <0.7.0") (d #t) (k 0)) (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "num_cpus") (r ">=1.13.0, <2.0.0") (d #t) (k 0)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 2)) (d (n "rayon") (r ">=1.3.0, <2.0.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "smartstring") (r ">=0.2.5, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "1spgipvp00vca3q9agg2a39fqq9szrnnakbm520y7lmbdm7lnniw")))

(define-public crate-topfew-0.2.2 (c (n "topfew") (v "0.2.2") (d (list (d (n "ahash") (r "^0.7.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dc98yk3acjgchq96dy108pnpd7cg1fq1xp3s4kna8y9hbv0fryw")))

(define-public crate-topfew-0.2.3 (c (n "topfew") (v "0.2.3") (d (list (d (n "ahash") (r "^0.7.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "06n658dykzs87sjpi6r22ach993a47hncv95m87ip0k2s0wi893x")))

