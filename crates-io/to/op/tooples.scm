(define-module (crates-io to op tooples) #:use-module (crates-io))

(define-public crate-tooples-0.1.0 (c (n "tooples") (v "0.1.0") (h "0mr0hflb4bvydknigk7b2v700baiabmki1fkiir9pj8mcj8vs4lr")))

(define-public crate-tooples-0.2.0 (c (n "tooples") (v "0.2.0") (h "0avdr37zycs2sran52sk5qhgi1lbg18ci8r4pn9l9dbl5rglhyrp")))

