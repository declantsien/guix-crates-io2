(define-module (crates-io to rr torrust-tracker-located-error) #:use-module (crates-io))

(define-public crate-torrust-tracker-located-error-3.0.0-alpha.1 (c (n "torrust-tracker-located-error") (v "3.0.0-alpha.1") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sy4pywcmm4qq59w03i8rln623llk9pa3chgiqlr23hf3hj55f65") (y #t)))

(define-public crate-torrust-tracker-located-error-3.0.0-alpha.2 (c (n "torrust-tracker-located-error") (v "3.0.0-alpha.2") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1kby6ffn9pidrkb8nxng1g7r5rcqk2hrdwwpjchy3zgaxsbw4hmg")))

(define-public crate-torrust-tracker-located-error-3.0.0-alpha.3 (c (n "torrust-tracker-located-error") (v "3.0.0-alpha.3") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0phyvb2sdlr7x3xwrn6sfnqjfiv7hji0g7m0vnm0gyhbcavav92k")))

(define-public crate-torrust-tracker-located-error-3.0.0-alpha.9 (c (n "torrust-tracker-located-error") (v "3.0.0-alpha.9") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1hqrcmvhhj6nw9aj7q7k68c97pjkpr4qdv5kwa7p72iism7z0fif") (r "1.72")))

(define-public crate-torrust-tracker-located-error-3.0.0-alpha.10 (c (n "torrust-tracker-located-error") (v "3.0.0-alpha.10") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0j052xi58mn9v94ff92dc18wyrs1rll0h7nml7a7r6lgx4w4nrpy") (r "1.72")))

(define-public crate-torrust-tracker-located-error-3.0.0-alpha.11 (c (n "torrust-tracker-located-error") (v "3.0.0-alpha.11") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1yba7lx5ji7h1mg6ds4mycnm1ib9bs8hgnjcxwfrqjyfl9xx1xjc") (r "1.72")))

