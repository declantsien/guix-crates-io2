(define-module (crates-io to rr torrust-tracker-contrib-bencode) #:use-module (crates-io))

(define-public crate-torrust-tracker-contrib-bencode-3.0.0-alpha.8 (c (n "torrust-tracker-contrib-bencode") (v "3.0.0-alpha.8") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)))) (h "16nkrr9v7qvw2n5hmc8xcq1738xkfzsrwsbqh4aflz3j8ql1h44k") (r "1.72")))

(define-public crate-torrust-tracker-contrib-bencode-3.0.0-alpha.9 (c (n "torrust-tracker-contrib-bencode") (v "3.0.0-alpha.9") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)))) (h "1pbrlg9akijx7x7cmbz0xjzgp0lllmd6kbncw9yfdszqwh0yqgjr") (r "1.72")))

(define-public crate-torrust-tracker-contrib-bencode-3.0.0-alpha.10 (c (n "torrust-tracker-contrib-bencode") (v "3.0.0-alpha.10") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)))) (h "0p2vlyswwkgxsba8q745g3l147glldq654lbz14jiy2nhcn21675") (r "1.72")))

(define-public crate-torrust-tracker-contrib-bencode-3.0.0-alpha.11 (c (n "torrust-tracker-contrib-bencode") (v "3.0.0-alpha.11") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)))) (h "1ydhz78plvdqd5h1ax24rmlq6d6d6v7927pjwqp1h8lip6yijdaw") (r "1.72")))

