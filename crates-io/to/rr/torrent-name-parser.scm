(define-module (crates-io to rr torrent-name-parser) #:use-module (crates-io))

(define-public crate-torrent-name-parser-0.1.0 (c (n "torrent-name-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "11xwg8a5kwjcz4ksch19py5mcwjz134l9p90yalb841bvmc0c4vf")))

(define-public crate-torrent-name-parser-0.1.1 (c (n "torrent-name-parser") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "054i6ppirsmiww51kjgi01p7xxf9gww7s3srbnja9xg2k8770mxh")))

(define-public crate-torrent-name-parser-0.1.2 (c (n "torrent-name-parser") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "11sk80gmp4rnj6g010d26qm6g05gmj0maf24fjz4ccvjxbc68h6c")))

(define-public crate-torrent-name-parser-0.1.3 (c (n "torrent-name-parser") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0z65y5gb6qh1xhdiyvys5ldf4xgskb62rhl6vrziqv7nv245432c")))

(define-public crate-torrent-name-parser-0.1.4 (c (n "torrent-name-parser") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0gb1g1r8811kl1g5a1z71f4y3hy1yx8f5mp9i0bjvrb6apdp0k6q")))

(define-public crate-torrent-name-parser-0.2.0 (c (n "torrent-name-parser") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1fgnjxwpq0p22yp3bj53n5dyh0ybgrvl8sr0bmgjs7d2v3zchwn6")))

(define-public crate-torrent-name-parser-0.2.1 (c (n "torrent-name-parser") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1f6s49rrxmglbxi9p2jy9p67px42q996a68rszw9vis8cy0g0q0z")))

(define-public crate-torrent-name-parser-0.3.0 (c (n "torrent-name-parser") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "07lw2q78lnrk658g7p013ln3dhcx6g37shyd6g81jdpxkhhx4dd9")))

(define-public crate-torrent-name-parser-0.3.1 (c (n "torrent-name-parser") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "06j84sq837g9dykiwbbzqj4wah2fjri2j3axc3bkg1f3zs5ys84f")))

(define-public crate-torrent-name-parser-0.3.2 (c (n "torrent-name-parser") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "15i2vfwy1pcv83pzzgjf0pvvlxyl53s4xafzr4lk4l73bri9xcs9")))

(define-public crate-torrent-name-parser-0.5.0 (c (n "torrent-name-parser") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "078rqlzyyka4nbrbsq9pzhn442whv8aars26ibcavki01kjybl73")))

(define-public crate-torrent-name-parser-0.6.0 (c (n "torrent-name-parser") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1n140ai0gnzm3hwqgjq6hjy6qldkb7lcv0lv0xmqa7pgnb6zxck3")))

(define-public crate-torrent-name-parser-0.6.1 (c (n "torrent-name-parser") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0wzagaq5ifzzjhb92l40r09fll5b96llvyhwr1zvfih1h62glppx")))

(define-public crate-torrent-name-parser-0.6.2 (c (n "torrent-name-parser") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1fcxkjkxrjr97s1h77qnnrxwnmkvyazdzhs9pi4r0r22ayr7qkcw")))

(define-public crate-torrent-name-parser-0.6.3 (c (n "torrent-name-parser") (v "0.6.3") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0vr3shs10zv3s2syhlhmfyzlqc1z7zs0j8db1i3adwrmfxls6ab9")))

(define-public crate-torrent-name-parser-0.7.0 (c (n "torrent-name-parser") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "10qyl3m9iqbg9j695dfs0r1k9gzlrj2dzhjfxkk17d7s0lczia3p")))

(define-public crate-torrent-name-parser-0.8.0 (c (n "torrent-name-parser") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1zk5fimpr97n9zszy92rjck344dl87wlinq515vkqnmgdjmkcqrh")))

(define-public crate-torrent-name-parser-0.9.0 (c (n "torrent-name-parser") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1lzn4mpahj2ppirpaa0chjqz85ai3km46vkb68kr2p7r1xwkrfd7")))

(define-public crate-torrent-name-parser-0.10.0 (c (n "torrent-name-parser") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0xzdyrvsv0s73srd72fxcwjwcfycj323b4fwf19yv6arn0awbzml")))

(define-public crate-torrent-name-parser-0.10.1 (c (n "torrent-name-parser") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0y4f3n5v55wzbfzz3ljfg9hy4l6izysjn1dkz0ni9iwkqha18h4i")))

(define-public crate-torrent-name-parser-0.10.2 (c (n "torrent-name-parser") (v "0.10.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "05zn7n1zjp12yalkcz53l35pmz6dafb8kqr2a9spdb3iii0rz3jv")))

(define-public crate-torrent-name-parser-0.11.0 (c (n "torrent-name-parser") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0aj89qzir36xc0nqrbyx062ls8bylfnbjzj8q6kx40wicbl5ywkn")))

(define-public crate-torrent-name-parser-0.11.1 (c (n "torrent-name-parser") (v "0.11.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.4.0") (d #t) (k 2)))) (h "1j1mk14v9nxjjvnp63i43pizmywyxbn5x2xv82zmii2s55dlgla2")))

(define-public crate-torrent-name-parser-0.11.2 (c (n "torrent-name-parser") (v "0.11.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.4.0") (d #t) (k 2)))) (h "13wrhx9lc1fj8nzw62v9qws7pf8sl9zhxdadrxi5zikc7ihkihyy")))

(define-public crate-torrent-name-parser-0.12.0 (c (n "torrent-name-parser") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.4.0") (d #t) (k 2)))) (h "11wpfz5yrcwh58bd61ckhw94fi1wnhqbq6dqgcq5sr2bc2nnsvra")))

(define-public crate-torrent-name-parser-0.12.1 (c (n "torrent-name-parser") (v "0.12.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion-macro") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0lgfypjnjcaandm29wrsqgbanl0dyn1bqy467835qz4zl8n87nx2")))

