(define-module (crates-io to rr torrent_search) #:use-module (crates-io))

(define-public crate-torrent_search-0.1.0 (c (n "torrent_search") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https-rustls"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0cp493vvn593hlkd7n56r6yawpcl8n492qqwayx6haci3cnvb4h7") (y #t)))

(define-public crate-torrent_search-0.2.0 (c (n "torrent_search") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https-rustls"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "148243am82h27x2vh00i48808fqxv0790iannchz034fcgwmivzm") (y #t)))

(define-public crate-torrent_search-0.2.1 (c (n "torrent_search") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https-rustls"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1syr6cn9ns8s2jfvii6kw8mphm84rzqd36nz31z0vb76d2h6dvjn") (y #t)))

(define-public crate-torrent_search-0.2.2 (c (n "torrent_search") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https-rustls"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0k4jw79mqzvmxyz28faqh9dgd633jw220wvslyh07pz9c5mbyaml")))

(define-public crate-torrent_search-0.3.0 (c (n "torrent_search") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "07l7vlav1vgwqhy3rncj6b8lzhsgcbc8c8866rmx6khkpjscs9km")))

(define-public crate-torrent_search-0.3.1 (c (n "torrent_search") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "105vjlpdscm7j2dqsxqj82yvvczr5n3n8p2ck59x8k651kkqf2y6")))

(define-public crate-torrent_search-0.3.2 (c (n "torrent_search") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0qw57ykkd7zbk35fra227b8dhbl9jbp402n3w3qhm0zapgddif7x")))

(define-public crate-torrent_search-0.3.3 (c (n "torrent_search") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1gk84qcakpcvfnizhfv1h229a07573mgvq4d6p4xs8qr51yr7x0z")))

