(define-module (crates-io to rr torrust-tracker-primitives) #:use-module (crates-io))

(define-public crate-torrust-tracker-primitives-3.0.0-alpha.1 (c (n "torrust-tracker-primitives") (v "3.0.0-alpha.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "053pli1py67v9zrd79q8irivmphzjzvipa0ypy4m3d4n6idqwcrw") (y #t)))

(define-public crate-torrust-tracker-primitives-3.0.0-alpha.2 (c (n "torrust-tracker-primitives") (v "3.0.0-alpha.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lf53lakpxpcdwi3787f7hr56xpbkbf2baligimq0gqqfknj7spc")))

(define-public crate-torrust-tracker-primitives-3.0.0-alpha.3 (c (n "torrust-tracker-primitives") (v "3.0.0-alpha.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dvglphlrslqx3zjqyhxgks30jgzj5prwkmxc9ggpxx23dw0q8ad")))

(define-public crate-torrust-tracker-primitives-3.0.0-alpha.9 (c (n "torrust-tracker-primitives") (v "3.0.0-alpha.9") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kppyqz6bx3ph01z2jgpysr23mlr4in9s5vldgrz7nn200znh4nc") (r "1.72")))

(define-public crate-torrust-tracker-primitives-3.0.0-alpha.10 (c (n "torrust-tracker-primitives") (v "3.0.0-alpha.10") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "092piwznshifrhq1jfplq0acg7rvlf25srkj5kxhd44bgp00p3gb") (r "1.72")))

(define-public crate-torrust-tracker-primitives-3.0.0-alpha.11 (c (n "torrust-tracker-primitives") (v "3.0.0-alpha.11") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bh4rfwgzffg5bsjf34qmyfrcy5ckr8zvnmqq508awzg6z3ija6s") (r "1.72")))

