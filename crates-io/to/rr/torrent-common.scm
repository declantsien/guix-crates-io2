(define-module (crates-io to rr torrent-common) #:use-module (crates-io))

(define-public crate-torrent-common-0.1.0 (c (n "torrent-common") (v "0.1.0") (d (list (d (n "torrent-name-parser") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1wyzz3n04mpqd2mbfbjj70jhy4kbaqml4faqlgam1fxyajmczkwh") (f (quote (("require-parse-names" "torrent-name-parser") ("parse-names" "torrent-name-parser"))))))

(define-public crate-torrent-common-0.1.1 (c (n "torrent-common") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "torrent-name-parser") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0rw6q5rg9d20gfzka3vsbnq5di0bmsfamsh5n694xlx1xbv9xhcf") (f (quote (("require-parse-names" "torrent-name-parser") ("parse-names" "torrent-name-parser") ("deserialize" "serde")))) (y #t)))

(define-public crate-torrent-common-0.1.2 (c (n "torrent-common") (v "0.1.2") (d (list (d (n "torrent-name-parser") (r "^0.9") (o #t) (d #t) (k 0)))) (h "04l2zyqp9956skw32474wys2ajwsi96xkpav8ck46j487q23133w") (f (quote (("require-parse-names" "torrent-name-parser") ("parse-names" "torrent-name-parser"))))))

