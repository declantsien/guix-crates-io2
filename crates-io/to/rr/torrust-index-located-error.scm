(define-module (crates-io to rr torrust-index-located-error) #:use-module (crates-io))

(define-public crate-torrust-index-located-error-3.0.0-alpha.2 (c (n "torrust-index-located-error") (v "3.0.0-alpha.2") (d (list (d (n "log") (r "^0") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1gq5m85qbbsd4cq84fm37yhyylrx526c47pp7ksnbcmbiwpjclmc") (r "1.72")))

