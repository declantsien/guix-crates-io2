(define-module (crates-io to rr torrentfind) #:use-module (crates-io))

(define-public crate-torrentfind-0.1.0 (c (n "torrentfind") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "term-table") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "1lcb6b2831l37jmxa28bgjn05pi687z0z1r7r8s4avcqxyh5rdn1")))

