(define-module (crates-io to rr torrust-tracker-test-helpers) #:use-module (crates-io))

(define-public crate-torrust-tracker-test-helpers-3.0.0-alpha.1 (c (n "torrust-tracker-test-helpers") (v "3.0.0-alpha.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "net" "sync" "macros" "signal"))) (d #t) (k 0)) (d (n "torrust-tracker-configuration") (r "^3.0.0-alpha.1") (d #t) (k 0)) (d (n "torrust-tracker-primitives") (r "^3.0.0-alpha.1") (d #t) (k 0)))) (h "1kahq088f1467q9yhsjhrk98dsqljc2vcv982k5zrip5mav0mjiw") (y #t)))

(define-public crate-torrust-tracker-test-helpers-3.0.0-alpha.2 (c (n "torrust-tracker-test-helpers") (v "3.0.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "torrust-tracker-configuration") (r "^3.0.0-alpha.2") (d #t) (k 0)) (d (n "torrust-tracker-primitives") (r "^3.0.0-alpha.2") (d #t) (k 0)))) (h "1vah3bmp3v0iyw2crsa2dw8c4fny89xi58gfwbxl0d7g6h96zli7")))

(define-public crate-torrust-tracker-test-helpers-3.0.0-alpha.3 (c (n "torrust-tracker-test-helpers") (v "3.0.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "torrust-tracker-configuration") (r "^3.0.0-alpha.3") (d #t) (k 0)) (d (n "torrust-tracker-primitives") (r "^3.0.0-alpha.3") (d #t) (k 0)))) (h "18f6b1asz1blyvx7q27i90v0bp9axx41kacsxzmj008y8k8d567l")))

(define-public crate-torrust-tracker-test-helpers-3.0.0-alpha.9 (c (n "torrust-tracker-test-helpers") (v "3.0.0-alpha.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "torrust-tracker-configuration") (r "^3.0.0-alpha.9") (d #t) (k 0)) (d (n "torrust-tracker-primitives") (r "^3.0.0-alpha.9") (d #t) (k 0)))) (h "1yqsxqnvk753qdlyd28wjrbj2gnjw9szrbiivykzvhnch0n2z5l5") (r "1.72")))

(define-public crate-torrust-tracker-test-helpers-3.0.0-alpha.10 (c (n "torrust-tracker-test-helpers") (v "3.0.0-alpha.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "torrust-tracker-configuration") (r "^3.0.0-alpha.10") (d #t) (k 0)) (d (n "torrust-tracker-primitives") (r "^3.0.0-alpha.10") (d #t) (k 0)))) (h "1iba1q3h5sfi2h9nsyx35qg617qbi9ph4cqi33bxxn69dma8n4i9") (r "1.72")))

(define-public crate-torrust-tracker-test-helpers-3.0.0-alpha.11 (c (n "torrust-tracker-test-helpers") (v "3.0.0-alpha.11") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "torrust-tracker-configuration") (r "^3.0.0-alpha.11") (d #t) (k 0)) (d (n "torrust-tracker-primitives") (r "^3.0.0-alpha.11") (d #t) (k 0)))) (h "0phspjq4apwzrq953f2f2h7phwvgpjql3ll4hrh0axi8n9fkim0i") (r "1.72")))

