(define-module (crates-io to a- toa-ping) #:use-module (crates-io))

(define-public crate-toa-ping-0.0.1 (c (n "toa-ping") (v "0.0.1") (d (list (d (n "lazy-socket") (r "^0.1") (d #t) (k 0)))) (h "1fkdc84fs6am5pi2w92qlzyg5sgga42xnbiac81hyigcb0ijdc6g")))

(define-public crate-toa-ping-0.0.2 (c (n "toa-ping") (v "0.0.2") (d (list (d (n "lazy-socket") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "03vbj7i5sxr477i1chfanqp1p9nk5w03n2zs4wqhsjm82giizgm1")))

(define-public crate-toa-ping-0.1.0 (c (n "toa-ping") (v "0.1.0") (d (list (d (n "lazy-socket") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "17r2gjb48r155diirdxvbb56w79y4z5b5v9h5myj6z2jqk51d0g3")))

(define-public crate-toa-ping-0.1.1 (c (n "toa-ping") (v "0.1.1") (d (list (d (n "lazy-socket") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "13wjli83341kky9ri3x3pm3fi9837bs53mvg2h93g527cl0rc6cb")))

