(define-module (crates-io to a- toa-find) #:use-module (crates-io))

(define-public crate-toa-find-0.1.0 (c (n "toa-find") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1v1zd8p7a45yddsryaxfcwmg6cqcs658b5hf0kf6mmsha4nm65g7")))

(define-public crate-toa-find-0.1.1 (c (n "toa-find") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0p05j79a9i8j3ncizpgds92wvd818rx8205spmr341d7xf1clpwp")))

