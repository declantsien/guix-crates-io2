(define-module (crates-io to ni tonic_catch) #:use-module (crates-io))

(define-public crate-tonic_catch-0.1.2 (c (n "tonic_catch") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "tonic") (r "^0.6.2") (d #t) (k 0)) (d (n "tonic_catch_proc") (r "^0.1.1") (d #t) (k 0)))) (h "0h0kpaj850ms1ivnm5b61mzk5knsvdp01a2rmxqm7fpz6lfxlyk7")))

(define-public crate-tonic_catch-0.1.3 (c (n "tonic_catch") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic_catch_proc") (r "^0.1.2") (d #t) (k 0)))) (h "02fzxck31fhwjq0qirgr0qnbbl2wi5zkdda82mdi77isl269h80h")))

