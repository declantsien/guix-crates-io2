(define-module (crates-io to ni tonic-thiserror) #:use-module (crates-io))

(define-public crate-tonic-thiserror-0.1.0 (c (n "tonic-thiserror") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tonic-thiserror-impl") (r "^0.1.0") (d #t) (k 0)))) (h "0k4k545ml0xiw7c4cbqik8v1shqyj0w4r32blcfgxdm32rd6wl2k")))

