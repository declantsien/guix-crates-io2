(define-module (crates-io to ni tonic-types) #:use-module (crates-io))

(define-public crate-tonic-types-0.1.0 (c (n "tonic-types") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "1pns7haxj7dgsh1ic3q17mcyjhpd40gwa13sj2j3z6q2gqz6n1lb")))

(define-public crate-tonic-types-0.2.0 (c (n "tonic-types") (v "0.2.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)))) (h "1k7ddyvldm7xvaknijgzjdjyx1wyp4yl5jrcijjf4qlyircyfr7r")))

(define-public crate-tonic-types-0.3.0 (c (n "tonic-types") (v "0.3.0") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-build") (r "^0.8") (d #t) (k 1)) (d (n "prost-types") (r "^0.8") (d #t) (k 0)))) (h "12hy7g046nx07gmjm6hyxgxrg5w1ngl7k3a1m5r0fj64w1fz92lq")))

(define-public crate-tonic-types-0.4.0 (c (n "tonic-types") (v "0.4.0") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)))) (h "0ny9qfriryr73mxbxvymbmzqcwysb70pgf4ac5rvpphwcpsmx0a0")))

(define-public crate-tonic-types-0.5.0 (c (n "tonic-types") (v "0.5.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-build") (r "^0.10") (d #t) (k 1)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)))) (h "0k7vjaj8fq2v2qwnnzbyrc0iisqql2al1c7adzjhx8nkywzkqq3d")))

(define-public crate-tonic-types-0.6.0 (c (n "tonic-types") (v "0.6.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0fv3vfzsnj0ajd5fmwdvif2gnxgw4nn1f7i44ccrd581z22q7i8f")))

(define-public crate-tonic-types-0.6.1 (c (n "tonic-types") (v "0.6.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (f (quote ("prost"))) (d #t) (k 2)))) (h "1v8wvjj2q5r6056ls8i4y9xrqnq3ysh6lfx1wm150x2mljp7qfmk")))

(define-public crate-tonic-types-0.9.0 (c (n "tonic-types") (v "0.9.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (k 0)) (d (n "tonic-build") (r "^0.9") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "0vw2250ly7dzj2wrxi912gx8qmf553cwdz4f8jknzj26132is5dj")))

(define-public crate-tonic-types-0.9.1 (c (n "tonic-types") (v "0.9.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (k 0)) (d (n "tonic-build") (r "^0.9") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "10azyrynyhqiz1yxa61q6zp9rl1xwzsm2pj59h59j97dgb3rgf8h")))

(define-public crate-tonic-types-0.9.2 (c (n "tonic-types") (v "0.9.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (k 0)) (d (n "tonic-build") (r "^0.9") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "0k2dkmccmj98zw70hf3vz2q7c4xzw22g2jbld8p0gp43ap3n9mal")))

(define-public crate-tonic-types-0.10.0 (c (n "tonic-types") (v "0.10.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (k 0)))) (h "00sjas1a3x8jmymxd0nkc4w07lqyyf23lb5kyqchmz8nljmgb61g")))

(define-public crate-tonic-types-0.10.1 (c (n "tonic-types") (v "0.10.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (k 0)))) (h "1qciqkpv7blfv93yr1083m0pbvqdh0id9ivbkrzzivbs9jkwygd2")))

(define-public crate-tonic-types-0.10.2 (c (n "tonic-types") (v "0.10.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (k 0)))) (h "0494n2748ny5jmbqq1nv701x7brcaqcl097xnd393yab1s2vsf8b")))

(define-public crate-tonic-types-0.11.0 (c (n "tonic-types") (v "0.11.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (k 0)))) (h "1x3420h5bg3k03m4r79h1dgnk90kjxrlgw5fqc7cdm6qf6a0iapl")))

