(define-module (crates-io to ni tonik) #:use-module (crates-io))

(define-public crate-tonik-0.1.0 (c (n "tonik") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.3") (f (quote ("gzip" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros"))) (d #t) (k 2)))) (h "1c0p2mfp8fl1mi5im58afjnrlzzd4hi26fxf1jy3vyqb0f80nnv4")))

