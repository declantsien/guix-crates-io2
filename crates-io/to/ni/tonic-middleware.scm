(define-module (crates-io to ni tonic-middleware) #:use-module (crates-io))

(define-public crate-tonic-middleware-0.1.0 (c (n "tonic-middleware") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.78") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0mzc0xgb6mbbsaww36d2kvdfshzxzbb0nj3jdn1pzfhnsayj8c52") (y #t)))

(define-public crate-tonic-middleware-0.1.1 (c (n "tonic-middleware") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.78") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0wdkzzmd5kn3a5rvn8m2q5vidvpag17paqynndcazy0q9529n8jj") (y #t)))

(define-public crate-tonic-middleware-0.1.2 (c (n "tonic-middleware") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.78") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0ny2ksll7fpx59b4a7vg3qqyid4nk47d6rsjb1sdqqvcbnxr1pgg") (y #t)))

(define-public crate-tonic-middleware-0.1.3 (c (n "tonic-middleware") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.78") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "02w83dg3f6jzbwgk6qljgpi1nzab37x921yvs3z4gmdr1bq4gnhs")))

(define-public crate-tonic-middleware-0.1.4 (c (n "tonic-middleware") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.78") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)))) (h "0153sy2172r0psianli70hszyxzpihymls34j7dls68q1ymlvlwj")))

