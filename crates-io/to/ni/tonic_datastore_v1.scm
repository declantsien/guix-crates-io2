(define-module (crates-io to ni tonic_datastore_v1) #:use-module (crates-io))

(define-public crate-tonic_datastore_v1-0.1.0+ee9e8e4e6 (c (n "tonic_datastore_v1") (v "0.1.0+ee9e8e4e6") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.2.0") (d #t) (k 0)))) (h "0g2zmyijq06dav67li1gr7rs0dv8mk2kayhwz9blm46q45nn63z5")))

(define-public crate-tonic_datastore_v1-0.1.0+3562b6cb3 (c (n "tonic_datastore_v1") (v "0.1.0+3562b6cb3") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.2.0") (d #t) (k 0)))) (h "07ml41xcc58pynwl7p805hmnvh4w39bxvb8mxa8vgmajyak7jmdj")))

(define-public crate-tonic_datastore_v1-0.2.0+3562b6cb3 (c (n "tonic_datastore_v1") (v "0.2.0+3562b6cb3") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.8") (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)))) (h "1mfqbmk82s3j8r1ckgf7cmiwi4bsw48mjvxkdjxx6yyr7hg83w7z")))

