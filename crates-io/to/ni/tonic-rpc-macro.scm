(define-module (crates-io to ni tonic-rpc-macro) #:use-module (crates-io))

(define-public crate-tonic-rpc-macro-0.1.0 (c (n "tonic-rpc-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 0)))) (h "10q9hgxc7qd7w9gpnkvybfmd5q3mdjai3pvsv0a18whhq6sz5a7z")))

(define-public crate-tonic-rpc-macro-0.2.0 (c (n "tonic-rpc-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 0)))) (h "0fsvkdc9c3a2psp8wsa7qhlzfj6x27lkfdmkggib634a78k0xvx8")))

(define-public crate-tonic-rpc-macro-0.2.1 (c (n "tonic-rpc-macro") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 0)))) (h "11v6i99dvashn767n7p9p2brbfypamy37qjs7dai0rmmxh5xss8n")))

