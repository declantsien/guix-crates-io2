(define-module (crates-io to ni tonic_include_protos) #:use-module (crates-io))

(define-public crate-tonic_include_protos-0.1.0 (c (n "tonic_include_protos") (v "0.1.0") (h "1qy0yjpkf3lngpfv4l3i93dlzgyxzlmm8h9j2c96bazcm1jcfkyr")))

(define-public crate-tonic_include_protos-0.1.1 (c (n "tonic_include_protos") (v "0.1.1") (h "1lbf6i2k5rh81z8jlfjkgpij7szcjam7r56y17nnq1shilivmih5")))

(define-public crate-tonic_include_protos-0.1.2 (c (n "tonic_include_protos") (v "0.1.2") (h "09nf094nqar96rxglivqfmxksqqiqyf1kr32isrmfwp07cml0dix")))

