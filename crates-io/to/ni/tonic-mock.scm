(define-module (crates-io to ni tonic-mock) #:use-module (crates-io))

(define-public crate-tonic-mock-0.1.0 (c (n "tonic-mock") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.4") (d #t) (k 0)))) (h "0v323zfbxf1h6w142zpxcasn2vhp021ykqg232c8d8p61x4am79l")))

(define-public crate-tonic-mock-0.3.0 (c (n "tonic-mock") (v "0.3.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.11") (d #t) (k 0)))) (h "1sjmwxk58dvy18hfiw1599zfcnjg6fzqj5xdwlqby8ay0v7n74ga")))

