(define-module (crates-io to ni toniefile) #:use-module (crates-io))

(define-public crate-toniefile-0.1.1 (c (n "toniefile") (v "0.1.1") (d (list (d (n "audiopus") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "hound") (r "^3.0") (d #t) (k 2)) (d (n "libogg") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wav") (r "^1.0") (d #t) (k 2)))) (h "1k4zis0dq733b9mrkfxskl2287yay3g0kig2dbbzln7g8rfn264w")))

