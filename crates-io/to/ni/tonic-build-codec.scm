(define-module (crates-io to ni tonic-build-codec) #:use-module (crates-io))

(define-public crate-tonic-build-codec-0.6.1 (c (n "tonic-build-codec") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d9djcgxagicjv5c52bzmzr9r99x7rj4zhacsvkcmhs08l4z04ai") (f (quote (("transport") ("rustfmt") ("prost" "prost-build") ("default" "transport" "rustfmt" "prost") ("compression")))) (y #t)))

(define-public crate-tonic-build-codec-0.6.2 (c (n "tonic-build-codec") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18jbzn6bv4dk72kw5izjwa3afvdrv2abc8l36py8vq98ic8imsnk") (f (quote (("transport") ("rustfmt") ("prost" "prost-build") ("default" "transport" "rustfmt" "prost") ("compression"))))))

