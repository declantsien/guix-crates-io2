(define-module (crates-io to ni tonic-interceptor) #:use-module (crates-io))

(define-public crate-tonic-interceptor-0.1.0 (c (n "tonic-interceptor") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "tonic") (r "^0.11") (k 0)) (d (n "tower-layer") (r "^0.3") (d #t) (k 0)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "1qm2mxrxdbpl3d469jnv8klxm225jmq34dbxyc4y3nrjmqy77qjq")))

