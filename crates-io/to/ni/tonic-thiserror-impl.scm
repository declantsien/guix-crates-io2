(define-module (crates-io to ni tonic-thiserror-impl) #:use-module (crates-io))

(define-public crate-tonic-thiserror-impl-0.1.0 (c (n "tonic-thiserror-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10qd6k21wkmwfm4yskfi6dqsc04gpp5zsig0bmvvg73j1x8hqrpa")))

