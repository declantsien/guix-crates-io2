(define-module (crates-io to ni tonic-compiler) #:use-module (crates-io))

(define-public crate-tonic-compiler-0.3.0 (c (n "tonic-compiler") (v "0.3.0") (d (list (d (n "tonic-js-builder") (r "^0.3.0") (d #t) (k 0)) (d (n "tonic-parser") (r "^0.3.0") (d #t) (k 0)))) (h "1vwk8zpj70dsi2m4v34djn42z3kqfnjffsnmasai0w3zr2apfaa4")))

