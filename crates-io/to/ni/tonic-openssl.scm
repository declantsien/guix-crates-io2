(define-module (crates-io to ni tonic-openssl) #:use-module (crates-io))

(define-public crate-tonic-openssl-0.1.0 (c (n "tonic-openssl") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "tokio-openssl") (r "^0.4") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (d #t) (k 0)))) (h "0nmcw1nj6inm7n2hz6xnh2ss7yih5yjyfqz20vn8fq70gbdzpapf")))

(define-public crate-tonic-openssl-0.2.0 (c (n "tonic-openssl") (v "0.2.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio-openssl") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0vch7gd715s8bf0x7bdnim1m35928mbyc9g4bl2i2hrgh6rbyr6c")))

