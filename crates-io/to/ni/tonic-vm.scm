(define-module (crates-io to ni tonic-vm) #:use-module (crates-io))

(define-public crate-tonic-vm-0.1.0 (c (n "tonic-vm") (v "0.1.0") (d (list (d (n "tonic-parser") (r "^0.1") (d #t) (k 0)))) (h "00yvcyhq6jp71nsqw4zm5xk3rhzhvfjss00fyhmfaxkyb6p6s0ms")))

(define-public crate-tonic-vm-0.1.1 (c (n "tonic-vm") (v "0.1.1") (d (list (d (n "tonic-parser") (r "^0.1") (d #t) (k 0)))) (h "0xhxnm4pbny75cza7ywarv1cggyflscd3ba8rhxgk7xw1k15x9kc")))

(define-public crate-tonic-vm-0.2.0 (c (n "tonic-vm") (v "0.2.0") (d (list (d (n "tonic-parser") (r "^0.1") (d #t) (k 0)))) (h "00bdq88126s9zgdhvjykymjdpxsxcgmkrvbjdl4lml46sliwmhil")))

(define-public crate-tonic-vm-0.2.1 (c (n "tonic-vm") (v "0.2.1") (d (list (d (n "tonic-parser") (r "^0.1") (d #t) (k 0)))) (h "06gg138ryvqp049484fa007r2lwalyjz8ijmc8xgs28kqwms8294")))

(define-public crate-tonic-vm-0.2.2 (c (n "tonic-vm") (v "0.2.2") (d (list (d (n "tonic-parser") (r "^0.1") (d #t) (k 0)))) (h "02kd24yclnjz5pzxc6p82bk443phhhw4xpfymsbx02najl2z6dkk")))

