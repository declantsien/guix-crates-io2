(define-module (crates-io to ni tonic-include-proto) #:use-module (crates-io))

(define-public crate-tonic-include-proto-0.1.0 (c (n "tonic-include-proto") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 2)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "05k356kqxpmwr1ym6xvifia9iz8cmv2a4d7jh6y4ywp3r8dmcg82")))

(define-public crate-tonic-include-proto-0.1.1 (c (n "tonic-include-proto") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 2)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "1lgw9p4yz59ifkl4f33znnbv4hwlpdck2gjvzmfgihfs68mvsqkn")))

