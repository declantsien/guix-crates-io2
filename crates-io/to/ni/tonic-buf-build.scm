(define-module (crates-io to ni tonic-buf-build) #:use-module (crates-io))

(define-public crate-tonic-buf-build-0.1.0 (c (n "tonic-buf-build") (v "0.1.0") (d (list (d (n "prost-build") (r "^0.11.9") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1y70vvsvscrhmgd5idnfqsm8j2gri3jfwhvr14j14yvhbxxzl597")))

(define-public crate-tonic-buf-build-0.1.1 (c (n "tonic-buf-build") (v "0.1.1") (d (list (d (n "prost-build") (r "^0.11.9") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "19idy4a4gx3xacrf13mpfcr1k847jfvvwncwnk1ija5al9dj5wyj")))

(define-public crate-tonic-buf-build-0.1.2 (c (n "tonic-buf-build") (v "0.1.2") (d (list (d (n "prost-build") (r "^0.12.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.0") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1268r3w0v4vkcjh4ajar0s9vwv54yykcsqr9xcqjr1f5a4gawlxp")))

(define-public crate-tonic-buf-build-0.2.0 (c (n "tonic-buf-build") (v "0.2.0") (d (list (d (n "prost-build") (r "^0.12") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0pd2bc0z9gqx2fw05d44dfbgyqg0bfybzhiq03w2d59pvl7jvyw9")))

