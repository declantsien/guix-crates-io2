(define-module (crates-io to ni tonic-disable-doctest) #:use-module (crates-io))

(define-public crate-tonic-disable-doctest-0.1.0 (c (n "tonic-disable-doctest") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 0)))) (h "01p854d8vxlwzhrfqvda1r5xqilivl26vcnmx3m5y0xqkl5yjarj")))

