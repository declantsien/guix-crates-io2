(define-module (crates-io to ni tonic-cli) #:use-module (crates-io))

(define-public crate-tonic-cli-0.3.0 (c (n "tonic-cli") (v "0.3.0") (d (list (d (n "rquickjs") (r "^0.1.3") (f (quote ("macro" "classes" "doc-cfg" "loader"))) (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.9") (d #t) (k 0)) (d (n "tonic-compiler") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sc30xj21w7lrb3f0nw1axp37h1c5d7dzp9m8s77f0295p7fsc9c")))

