(define-module (crates-io to ni tonic-error-impl) #:use-module (crates-io))

(define-public crate-tonic-error-impl-0.1.0 (c (n "tonic-error-impl") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0nvvhlbsxrqclpvlcxyq6a816snq2pp5xm3ah2z2mbfr5b9j3652")))

(define-public crate-tonic-error-impl-0.1.1 (c (n "tonic-error-impl") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "100qv4krdh3gj6ba4mx5h4xk75czgwag6bwprg5il3h822cis0d5")))

(define-public crate-tonic-error-impl-0.2.0 (c (n "tonic-error-impl") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1zhg7n3rhfq231rqfsprj1s9g0c77yncz0d6lfh9564gryqa365p")))

(define-public crate-tonic-error-impl-0.2.1 (c (n "tonic-error-impl") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1850ky29w4f4hby882aw9ghgs6pwp522093ag7ax6b8rvwz3swgl")))

(define-public crate-tonic-error-impl-0.3.0 (c (n "tonic-error-impl") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0cv5wmxk0gxh47ijib7y7x3l9pv414i9w325p887rb7hkdjlqnzp")))

