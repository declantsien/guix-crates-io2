(define-module (crates-io to ni tonic-error) #:use-module (crates-io))

(define-public crate-tonic-error-0.1.0 (c (n "tonic-error") (v "0.1.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-error-impl") (r "^0.1.0") (d #t) (k 0)))) (h "0bksd4jibsrragz0p3wcfs1nx0vv3rm2mnlc0x53cgwic6r3hlnk")))

(define-public crate-tonic-error-0.1.1 (c (n "tonic-error") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-error-impl") (r "^0.1.1") (d #t) (k 0)))) (h "0lrby8y0x6n1c2i5yzyzwg3zr4q9kzzcmlxy8f32npybz1q0iwwx")))

(define-public crate-tonic-error-0.1.2 (c (n "tonic-error") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-error-impl") (r "^0.1.1") (d #t) (k 0)))) (h "1z6v0q2kdkwxsp13srma3ch4jj2cjby25l3b1gs90bf1x13djszn")))

(define-public crate-tonic-error-0.2.0 (c (n "tonic-error") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-error-impl") (r "^0.2.0") (d #t) (k 0)))) (h "0aqwfzwl0dniw0an7agn4x8ls8jf5k5a5qkfpql2sg1wl3c9rli2")))

(define-public crate-tonic-error-0.2.1 (c (n "tonic-error") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-error-impl") (r "^0.2.0") (d #t) (k 0)))) (h "067rl7li71v7asx07b9ihw1hi6pyrs162pgy4qw8ksqlawkrgqiz")))

(define-public crate-tonic-error-0.2.2 (c (n "tonic-error") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-error-impl") (r "^0.2.0") (d #t) (k 0)))) (h "0ybjd7f1j053fi41n0r3bkz3vrxvkamvd9nq3mykb9gqm51dbqyq")))

(define-public crate-tonic-error-0.3.0 (c (n "tonic-error") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-error-impl") (r "^0.3.0") (d #t) (k 0)))) (h "0aaja8h6ayq34z1hcc1gf8jvv5xzkf2cyaw2p3w58bi8bcwvr3sd")))

