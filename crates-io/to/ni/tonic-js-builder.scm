(define-module (crates-io to ni tonic-js-builder) #:use-module (crates-io))

(define-public crate-tonic-js-builder-0.1.0 (c (n "tonic-js-builder") (v "0.1.0") (h "18jydx0psi3f2hdqjxwng95wg33sglrh6i46kgr6k72hdm02511h")))

(define-public crate-tonic-js-builder-0.3.0 (c (n "tonic-js-builder") (v "0.3.0") (h "0xsdqrwdw6n3zy51r6zravi8jb1fvk7sbjkzjnbh42wxf5fw2na1")))

