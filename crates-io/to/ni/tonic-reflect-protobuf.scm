(define-module (crates-io to ni tonic-reflect-protobuf) #:use-module (crates-io))

(define-public crate-tonic-reflect-protobuf-0.9.0 (c (n "tonic-reflect-protobuf") (v "0.9.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("codegen" "prost"))) (k 0)))) (h "10xn6lifihnrqhhihphg5gh0gzf1wkaq67z618rrzha73mzvgx4c")))

