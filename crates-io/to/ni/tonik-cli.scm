(define-module (crates-io to ni tonik-cli) #:use-module (crates-io))

(define-public crate-tonik-cli-0.1.0 (c (n "tonik-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonik") (r "^0.1.0") (d #t) (k 0)))) (h "1rhv80gbk6a9pdwqsdc20xbharxh0vbabdwqkiaz1p9j0zzaadpb")))

(define-public crate-tonik-cli-0.2.0 (c (n "tonik-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonik") (r "^0.1.0") (d #t) (k 0)))) (h "18dis24k848mhj4gpdlbgzlknfwp4r56whr5k8mn65x1vhilzskp")))

