(define-module (crates-io to ni tonic-richer-error) #:use-module (crates-io))

(define-public crate-tonic-richer-error-0.2.0 (c (n "tonic-richer-error") (v "0.2.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-build") (r "^0.10") (d #t) (k 1)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("compression"))) (d #t) (k 2)) (d (n "tonic-build") (r "^0.7") (d #t) (k 2)))) (h "0wrr2h7sd0dz47sfv3s37bi4vz1vg7jxkmjvmz7c7yfnlx8qp5nl")))

(define-public crate-tonic-richer-error-0.2.1 (c (n "tonic-richer-error") (v "0.2.1") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-build") (r "^0.10") (d #t) (k 1)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("compression"))) (d #t) (k 2)) (d (n "tonic-build") (r "^0.7") (d #t) (k 2)))) (h "0886i3ps5n5sdq4sqihmxkqhfiglabw43n4lcv88v4wc30maps9f")))

(define-public crate-tonic-richer-error-0.3.0 (c (n "tonic-richer-error") (v "0.3.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "1478czqwwncq93a3yxnr6yzwn6010lh3xcy6lwj4irzpqysy14w0")))

(define-public crate-tonic-richer-error-0.3.1 (c (n "tonic-richer-error") (v "0.3.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)))) (h "18gzyxz2ijswakq9j558ydfaq3ax84g202aa0cwry7fdvys4khxg")))

(define-public crate-tonic-richer-error-0.3.2 (c (n "tonic-richer-error") (v "0.3.2") (d (list (d (n "prost") (r "^0.11") (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("codegen" "prost"))) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)))) (h "02kp7vcj7jaryff3p3mjl565kl4lyyzny2i03rq4hdggi14734id")))

