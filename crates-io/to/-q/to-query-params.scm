(define-module (crates-io to -q to-query-params) #:use-module (crates-io))

(define-public crate-to-query-params-0.0.2 (c (n "to-query-params") (v "0.0.2") (d (list (d (n "query-params-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1c3xdnb0g99hxqgd4r2zv8hppq96whhmqlrmjq3svnk87apgazk7") (y #t)))

(define-public crate-to-query-params-0.0.3 (c (n "to-query-params") (v "0.0.3") (d (list (d (n "query-params-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0nyhxzmcprdvgq6mrixhabhhb9w05a3ksfyw730sd6ji7nh26ilv") (y #t)))

(define-public crate-to-query-params-0.0.4 (c (n "to-query-params") (v "0.0.4") (d (list (d (n "query-params-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0a2dz4pihn9g6vnr808wcjqiv93cww67xr2g6hn1lk5fmkfivw47")))

