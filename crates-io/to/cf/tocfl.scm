(define-module (crates-io to cf tocfl) #:use-module (crates-io))

(define-public crate-tocfl-0.1.0 (c (n "tocfl") (v "0.1.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "052r91a0lpvhvh1sc34rc1fi4k9lcf0qvrhqzmh4xkrm3nh0102i")))

(define-public crate-tocfl-0.2.0 (c (n "tocfl") (v "0.2.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "124lvx3mbgw9sjq8dwlf6kj6wkr5vsqcw337pg2551qjn7nj9jwm")))

(define-public crate-tocfl-0.3.0 (c (n "tocfl") (v "0.3.0") (d (list (d (n "calamine") (r "^0.19.1") (d #t) (k 0)) (d (n "prettify_pinyin") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0dv7na59dsclr59mahb918025nrikvjyf4xnp40738pxccr5h5x4")))

