(define-module (crates-io to ur tournaments) #:use-module (crates-io))

(define-public crate-tournaments-0.1.0-alpha0 (c (n "tournaments") (v "0.1.0-alpha0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "04ficg4i7342pzzircfr35c70yr984hl5l5kr6zq83wn0mjd8pb2")))

(define-public crate-tournaments-0.1.0-alpha1 (c (n "tournaments") (v "0.1.0-alpha1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1p5slkycs3fygjy10iq06w9pc8wyhw9nzgdi1nx2n5m3ypmfzwkl")))

(define-public crate-tournaments-0.1.0-alpha2 (c (n "tournaments") (v "0.1.0-alpha2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1zrq4v1a9zy35gmik220vwrqncm3dzk8paaaw68qjcaaxi77i08a")))

(define-public crate-tournaments-0.1.0-alpha3 (c (n "tournaments") (v "0.1.0-alpha3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "16crfwyx3jqxbzndl72nbzjzpz17mlagybp8rnssiqbvv8cvjxpd")))

(define-public crate-tournaments-0.1.0-alpha4 (c (n "tournaments") (v "0.1.0-alpha4") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1vbwhcy7m31k71r00njnl6j38fdfwy701hpv2l51kf5gh93y7200")))

(define-public crate-tournaments-0.1.0 (c (n "tournaments") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1yhjqsn312gdbvabha8bsq1axi4rl5dwp0rlfls8ig3252abzg88")))

