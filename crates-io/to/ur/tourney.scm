(define-module (crates-io to ur tourney) #:use-module (crates-io))

(define-public crate-tourney-0.1.0 (c (n "tourney") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0064qljdbjyh453incsalzqjnyx6fgkab5p0v6n6n00f8kbn696d")))

(define-public crate-tourney-0.2.0 (c (n "tourney") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wcf0ahcr3dwfrfa1cr29sn9z2602rd11ij2bymv8dmm92klk2q5")))

(define-public crate-tourney-0.2.1 (c (n "tourney") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c96155ljqll1vxg9nbi5233p693bv76a0rgahj4mjzcdim1qrgn")))

(define-public crate-tourney-0.2.2 (c (n "tourney") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0krw5x1nz1wbpsrj4g462a5zf3c0nnvkxrflm6pbpkvgjw0alpgs")))

(define-public crate-tourney-0.2.3 (c (n "tourney") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hfpad650gzdhyqrl4xc1ygsjgh2rr0yj9manpb9c96005kw5vwg")))

(define-public crate-tourney-0.2.4 (c (n "tourney") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07fbhdfyd4s3l7bfsic23f2zgvwax23wdz2d204hyv2pxcr4c9g3")))

