(define-module (crates-io to wa towa-core) #:use-module (crates-io))

(define-public crate-towa-core-0.1.0 (c (n "towa-core") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "threefish") (r "^0.5.2") (d #t) (k 0)))) (h "1zxqd3mz1h2vcw2q1iqh14xgy4fcnnfnkg7v00xwswawd9l5if9j")))

