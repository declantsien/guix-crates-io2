(define-module (crates-io to -k to-kana) #:use-module (crates-io))

(define-public crate-to-kana-0.1.0 (c (n "to-kana") (v "0.1.0") (h "0shxwcgir6mnfzai5kl5wvq3inkfwmh0bkgbwj4880vf3l38s5da")))

(define-public crate-to-kana-0.2.0 (c (n "to-kana") (v "0.2.0") (h "0jw6afvwh9xyhdgiqchp84dlj7zw5bz7m1nqwjklg6q8399yjlwj")))

(define-public crate-to-kana-0.3.0 (c (n "to-kana") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1cdnr1fymxg3pd5cvajgbyibfsyw44466gx8nwckk45h556k8cbk")))

(define-public crate-to-kana-0.4.0 (c (n "to-kana") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1hl3qcy5mbaf044wqji28ph761x9z5ja8nj57s7gyz00dxgddk93")))

(define-public crate-to-kana-0.5.0 (c (n "to-kana") (v "0.5.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0wgjbbmz0p2kkra82iqb138cka9cdyh071qsvm0gg2r5js4l9lwd") (y #t)))

(define-public crate-to-kana-0.5.1 (c (n "to-kana") (v "0.5.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0kawinx6n58qf1x5dic66rr0wp2z5f3gmyxcfq8yh8x35fmk3p5z")))

(define-public crate-to-kana-0.6.0 (c (n "to-kana") (v "0.6.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0law264ig4cwhyc451igb88gns2mli8dlbyy3h4gy8gw070fhn5p")))

