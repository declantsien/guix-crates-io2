(define-module (crates-io to mt tomt_async) #:use-module (crates-io))

(define-public crate-tomt_async-0.1.0 (c (n "tomt_async") (v "0.1.0") (h "11mv64mnhv0icpk9xr9lyj16ax7pfssjl0brmj8pagl19wmb9jmw") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-tomt_async-0.1.1 (c (n "tomt_async") (v "0.1.1") (h "05802jqay71nfz2rfk8bzybqi9hr2k64l496x67jsh1crh6r3qmp") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-tomt_async-0.1.2 (c (n "tomt_async") (v "0.1.2") (h "1s7d4vajcglym4gml4ri4l3vjzdz1ydxq3d74m15pxcip8vsbymn") (f (quote (("sync") ("default" "sync"))))))

(define-public crate-tomt_async-0.1.3 (c (n "tomt_async") (v "0.1.3") (h "0kix23vaafy9x29s4qzvf7vnm3r5zi7pj0pd7dxgkqz5b09m42hc") (f (quote (("sync") ("default" "collections" "sync") ("collections"))))))

