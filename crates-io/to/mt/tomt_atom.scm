(define-module (crates-io to mt tomt_atom) #:use-module (crates-io))

(define-public crate-tomt_atom-0.1.1 (c (n "tomt_atom") (v "0.1.1") (h "0b40ih5p9fg9sj9wi6flbscvw85lgr0ppn3zj2j8j824zwbbnxax")))

(define-public crate-tomt_atom-0.1.2 (c (n "tomt_atom") (v "0.1.2") (h "1q892i7q1fn9v7xyg8077fgr9f5ik3bvbdq1r5xmg4nfr9yswg63")))

(define-public crate-tomt_atom-0.1.3 (c (n "tomt_atom") (v "0.1.3") (h "0g9yzxvqbh9n0f6hb6ng9d9z527lrk1v2w3ijc74063s44wmfcci")))

(define-public crate-tomt_atom-0.1.4 (c (n "tomt_atom") (v "0.1.4") (h "0ni3pmdg3mj8s215670gmmkc2r1n5cfkvizjxh7zcyp73vd11qzi")))

(define-public crate-tomt_atom-0.1.5 (c (n "tomt_atom") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.195") (o #t) (d #t) (k 0)))) (h "1jvizk2ym2y0mrbl9rfsx4sanvdjhvvpkjc8qnhy3rr7rmaqjvbv") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tomt_atom-0.1.6 (c (n "tomt_atom") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.195") (o #t) (d #t) (k 0)))) (h "0kcl0p27wjvfvpgfph3885fw1icrlh3f45qqspcxbcnkp3w6w6vr") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tomt_atom-0.1.7 (c (n "tomt_atom") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.195") (o #t) (d #t) (k 0)))) (h "13r9f4am8anhv0k21s9z2jkwkr1qfp8isqmch93yzdb9g9102f8j") (f (quote (("from_str") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

