(define-module (crates-io to pi topiary-web-tree-sitter-sys) #:use-module (crates-io))

(define-public crate-topiary-web-tree-sitter-sys-0.4.0 (c (n "topiary-web-tree-sitter-sys") (v "0.4.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.91") (f (quote ("strict-macro"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window"))) (o #t) (d #t) (k 0)))) (h "0ildq3rkzjlf66pc5lchqiy6wlc99hhfmrqynybpxw5pxv53b7nc") (f (quote (("node") ("default" "web-sys"))))))

