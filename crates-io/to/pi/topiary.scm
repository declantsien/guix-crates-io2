(define-module (crates-io to pi topiary) #:use-module (crates-io))

(define-public crate-topiary-0.0.1 (c (n "topiary") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1lxsq2dfc3bpbcz3dgh6a10ms0ifg47c2xkkx5v8m8a7kpgf925w")))

