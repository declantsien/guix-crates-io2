(define-module (crates-io to pi topiary-queries) #:use-module (crates-io))

(define-public crate-topiary-queries-0.4.0 (c (n "topiary-queries") (v "0.4.0") (h "10681yjj1p5wkdi43g281w3m2hvav6n49xgcpzrhkqm59hpsfscb") (f (quote (("tree_sitter_query") ("toml") ("rust") ("ocamllex") ("ocaml_interface") ("ocaml") ("nickel") ("json") ("default" "bash" "css" "json" "nickel" "ocaml" "ocaml_interface" "ocamllex" "rust" "toml" "tree_sitter_query") ("css") ("bash"))))))

