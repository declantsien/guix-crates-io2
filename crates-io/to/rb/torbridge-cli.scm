(define-module (crates-io to rb torbridge-cli) #:use-module (crates-io))

(define-public crate-torbridge-cli-0.1.0 (c (n "torbridge-cli") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("rustls" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "viuer") (r "^0.6.2") (d #t) (k 0)))) (h "0vbdlnrz89gk4nz1y1lid1czyf4lhi3ypcx8hc8v3sfyk150lcji")))

(define-public crate-torbridge-cli-0.1.1 (c (n "torbridge-cli") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("rustls" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "viuer") (r "^0.6.2") (d #t) (k 0)))) (h "1ybsa0rknk759h7kfcsaf5iijxidvvpddx4p89ih8a42q4js6d0z")))

