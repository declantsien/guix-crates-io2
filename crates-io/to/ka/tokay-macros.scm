(define-module (crates-io to ka tokay-macros) #:use-module (crates-io))

(define-public crate-tokay-macros-0.1.0 (c (n "tokay-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "tokay") (r "^0.4") (d #t) (k 0)))) (h "17lwrw89m4q5np2hw24mb6p932hfd4ka8yy78dkwsi2icz7p0d5p")))

(define-public crate-tokay-macros-0.3.0 (c (n "tokay-macros") (v "0.3.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "tokay") (r "^0.4") (d #t) (k 0)))) (h "048jslmg35qnpk6wjpcvkxxsl07z5pyr7d1a1vhqbq0y5w9d0r9i")))

(define-public crate-tokay-macros-0.4.0 (c (n "tokay-macros") (v "0.4.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "tokay") (r "^0.4") (d #t) (k 0)))) (h "1q7lpc0s43wmy64y5qk542nysmdlbzlb4drrvn2aa9jyiwv9lrl0")))

