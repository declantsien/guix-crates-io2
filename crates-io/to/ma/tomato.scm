(define-module (crates-io to ma tomato) #:use-module (crates-io))

(define-public crate-tomato-0.1.0 (c (n "tomato") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "14n3dv0nadwdc3g9dagdhyrh530dr7rv6psi1jflz12mwznc91q5")))

(define-public crate-tomato-0.1.1 (c (n "tomato") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "1048475cr3i1w8y5p282sw9982f85agdcr7fdiqbjwjxx3w8nrnb")))

(define-public crate-tomato-0.2.0 (c (n "tomato") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.4") (d #t) (k 0)))) (h "1064q6wm91h1msb7ipc9jhprji0666hrr7if04bfyw4zywgpc159")))

