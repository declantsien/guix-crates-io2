(define-module (crates-io to ff toffee) #:use-module (crates-io))

(define-public crate-toffee-0.1.0 (c (n "toffee") (v "0.1.0") (h "1l4szy32fk6q6w0gh4m578636x86iy15ff2inyh5kcb40acdixmb")))

(define-public crate-toffee-0.2.0 (c (n "toffee") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0i68g7vlh6zcmmq5n9qpnf1asiy5wk1vgbmb38ji6cl1h8jpm9s0")))

(define-public crate-toffee-0.3.0 (c (n "toffee") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1h502yb4z9fp351c13mal7s4g0z5ny9v4wi0w2fa354lq0cwk5kc")))

(define-public crate-toffee-0.4.0 (c (n "toffee") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1sykaw3bqpgf0zij4m9d5jnxk9szkyqrww988083gx5ixx1xsmwl")))

(define-public crate-toffee-0.4.2 (c (n "toffee") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0blsa03crd8bkxhgac97fqig2kbl44vb73dsyrs5f89www74sq0d")))

(define-public crate-toffee-0.5.1 (c (n "toffee") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0h0lf0b4ms8xdqxpnvrfrcvnjljx82vrikjdsi816c6szmcdfphj")))

(define-public crate-toffee-0.5.2 (c (n "toffee") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "06ajihbs1w13603pf2q5jwnkrid0q3mvvhba8c8c7p8dysdsrvkf")))

