(define-module (crates-io to rc torch-cmd) #:use-module (crates-io))

(define-public crate-torch-cmd-0.1.0 (c (n "torch-cmd") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)))) (h "1nsy4r0gyjn1wbz0mk8ka78hg3rdclgv8f0d9y8dsd7gw7gi1j9w")))

(define-public crate-torch-cmd-0.1.1 (c (n "torch-cmd") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)))) (h "1dv6l0vnrdrxqvkymkdhpbpizim356plp8fgsqfxc4ac7ngi6brf")))

