(define-module (crates-io to rc torcurve-rs) #:use-module (crates-io))

(define-public crate-torcurve-rs-0.1.0 (c (n "torcurve-rs") (v "0.1.0") (h "1hj1hj0ryprr8hd2dm9vwjkskv023ag0rlpgzg0vs6c0jwdykfmj")))

(define-public crate-torcurve-rs-0.1.1 (c (n "torcurve-rs") (v "0.1.1") (h "0lkw8f5f7hk2vawwsc4fnns80ww2s2dlsgwlni9c2l62alyypqrn")))

(define-public crate-torcurve-rs-0.1.2 (c (n "torcurve-rs") (v "0.1.2") (h "1hswnmwxxsnfz2n80vzpr2hfnb42aqianhhm51dl2wbi5v4bwd1f")))

(define-public crate-torcurve-rs-0.1.3 (c (n "torcurve-rs") (v "0.1.3") (h "19v3irf9djmp9haz3dxq56s4mrb907c01kn2q3z4wwzqs8kh0ksf")))

