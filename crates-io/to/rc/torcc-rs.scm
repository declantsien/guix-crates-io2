(define-module (crates-io to rc torcc-rs) #:use-module (crates-io))

(define-public crate-torcc-rs-0.1.0 (c (n "torcc-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08zl70il48izqmcr88kg1cwy7sfs013w1rhx93c9dgxvgrp3iyp6")))

(define-public crate-torcc-rs-0.1.1 (c (n "torcc-rs") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03i2myijs6i970i1gkvng2kgfbvb8b82vx99ldwna89xsss9hcpr")))

