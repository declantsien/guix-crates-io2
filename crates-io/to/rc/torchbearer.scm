(define-module (crates-io to rc torchbearer) #:use-module (crates-io))

(define-public crate-torchbearer-0.3.0 (c (n "torchbearer") (v "0.3.0") (d (list (d (n "bracket-pathfinding") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tcod") (r "^0.15.0") (d #t) (k 2)))) (h "0sxzhj2iyg0y7mr1dwfq23ja8hk03sfkzj7b21cy6hqx3x82fyyq")))

(define-public crate-torchbearer-0.3.1 (c (n "torchbearer") (v "0.3.1") (d (list (d (n "bracket-pathfinding") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tcod") (r "^0.15.0") (d #t) (k 2)))) (h "19z4kp794yiwa3nr15dqkmijc24cc3nr7mqh3n76wvabhd9j9rz3")))

(define-public crate-torchbearer-0.3.2 (c (n "torchbearer") (v "0.3.2") (d (list (d (n "bracket-pathfinding") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tcod") (r "^0.15.0") (d #t) (k 2)))) (h "1c1fhc64d66w30sc6xy9v23yiq8h5fv8cnlx7ysajd1als03li1j")))

(define-public crate-torchbearer-0.3.3 (c (n "torchbearer") (v "0.3.3") (d (list (d (n "bracket-pathfinding") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tcod") (r "^0.15.0") (d #t) (k 2)))) (h "0sg2kw851n7hr4k2qz4h7a3vddk2927l16qmzbnag6xlsj0m5s5k")))

(define-public crate-torchbearer-0.3.4 (c (n "torchbearer") (v "0.3.4") (d (list (d (n "bracket-pathfinding") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tcod") (r "^0.15.0") (d #t) (k 2)))) (h "07z8xsfyg033b681mhczch4kbvncrr2bqw2kcqc7rnjvjgqq172d")))

(define-public crate-torchbearer-0.3.5 (c (n "torchbearer") (v "0.3.5") (d (list (d (n "bracket-pathfinding") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tcod") (r "^0.15.0") (d #t) (k 2)))) (h "13hf52ws8b02p8zkp0094rcf8cssh3wkcwcpyns4a2vbq17ykaqc")))

(define-public crate-torchbearer-0.3.6 (c (n "torchbearer") (v "0.3.6") (d (list (d (n "bracket-pathfinding") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tcod") (r "^0.15.0") (d #t) (k 2)))) (h "0xlnvlpp9yfdqzk4rrlcm73yxrb8dph2wcqsng19q5j52wp7972y")))

(define-public crate-torchbearer-0.4.0 (c (n "torchbearer") (v "0.4.0") (d (list (d (n "bracket-pathfinding") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tcod") (r "^0.15") (d #t) (k 2)))) (h "1csa1sj1s1z404janzkm7k6vr2k6jxpxklyyarw4zh89wjx9zcim")))

(define-public crate-torchbearer-0.5.0 (c (n "torchbearer") (v "0.5.0") (d (list (d (n "bracket-pathfinding") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tcod") (r "^0.15") (d #t) (k 2)))) (h "1pcbxig4lkwhzxkhgsgmk604hxkmrvkjr7l0h81l01v102hrjdyb")))

(define-public crate-torchbearer-0.5.1 (c (n "torchbearer") (v "0.5.1") (d (list (d (n "bracket-pathfinding") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tcod") (r "^0.15") (d #t) (k 2)))) (h "10z74y4y27w1yd3b93jd0c9x3fwyibcp3p9b62h3lddrvjdf51jl")))

(define-public crate-torchbearer-0.5.2 (c (n "torchbearer") (v "0.5.2") (d (list (d (n "bracket-pathfinding") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tcod") (r "^0.15") (d #t) (k 2)))) (h "04vb2b42l0ap98qj1m4b6nb1l0rj5bpgljmpgcinzsiz67fkyqb5")))

(define-public crate-torchbearer-0.5.3 (c (n "torchbearer") (v "0.5.3") (d (list (d (n "bracket-pathfinding") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tcod") (r "^0.15") (d #t) (k 2)))) (h "08bap3jy0b0xlhiglhlyylq42nqlsqpivv6qcmb1z0q73fshmkvr")))

(define-public crate-torchbearer-0.6.0 (c (n "torchbearer") (v "0.6.0") (d (list (d (n "bracket-pathfinding") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tcod") (r "^0.15") (d #t) (k 2)))) (h "04307shlpgfpxcx2a7qs6rgclsnhbcvad358c3cb9snwhx8r06rm")))

(define-public crate-torchbearer-0.6.1 (c (n "torchbearer") (v "0.6.1") (d (list (d (n "bracket-pathfinding") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tcod") (r "^0.15") (d #t) (k 2)))) (h "0mmhi702p5yajz10p2cqk3d687c59az97aibzji8pzdrwfih8j5g")))

