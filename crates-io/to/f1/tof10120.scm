(define-module (crates-io to f1 tof10120) #:use-module (crates-io))

(define-public crate-tof10120-0.1.0 (c (n "tof10120") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.8") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.6") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("stm32f103" "rt" "medium"))) (d #t) (k 2)))) (h "0lbywxw0gl5jvpbal5imfhf47bgzs53v9pi5awd0msg1zja13amq")))

