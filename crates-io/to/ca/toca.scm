(define-module (crates-io to ca toca) #:use-module (crates-io))

(define-public crate-toca-0.1.0 (c (n "toca") (v "0.1.0") (d (list (d (n "device_query") (r "^1.1.1") (d #t) (k 0)) (d (n "enigo") (r "^0.0.14") (d #t) (k 0)))) (h "1c3pz8360z3msb1qzq26vyrihy2zx25y3ycnynv6amdnr3yb9zwg")))

(define-public crate-toca-0.1.1 (c (n "toca") (v "0.1.1") (d (list (d (n "device_query") (r "^1.1.1") (d #t) (k 0)) (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07p25s39qh1crbj0zha51blx3ifwa3h2rk05jv3fgxqjc3z8fyl0")))

