(define-module (crates-io to da todate) #:use-module (crates-io))

(define-public crate-todate-0.1.0 (c (n "todate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "17f4j51bi6hkz7k89qqy2qiy29f405y0srzhxkybfxpabvx802ss")))

(define-public crate-todate-0.1.1 (c (n "todate") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0g54s7f2kawjs641kyvrwdk6rr6nfzm2hzc0x6wcrlf5b52c8v8m")))

