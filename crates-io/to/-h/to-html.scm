(define-module (crates-io to -h to-html) #:use-module (crates-io))

(define-public crate-to-html-0.1.0 (c (n "to-html") (v "0.1.0") (d (list (d (n "ansi-to-html") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "fake-tty") (r "^0.1") (d #t) (k 0)) (d (n "logos") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jjy2ynip5wfrs1mq41zh89giibdgyv9m20q59bbpxy4dc67dyvx")))

(define-public crate-to-html-0.1.1 (c (n "to-html") (v "0.1.1") (d (list (d (n "ansi-to-html") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "fake-tty") (r "^0.2") (d #t) (k 0)) (d (n "logos") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z6db4mzcy8lq191c41v96v2mnq840abn0l1qafavxdk5a3rsskm")))

(define-public crate-to-html-0.1.3 (c (n "to-html") (v "0.1.3") (d (list (d (n "ansi-to-html") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "fake-tty") (r "^0.3.1") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1ggvjl38d6lv4asxk2a197b8jc2n0rqmhjzbdaccfh7jbyi2r4bf")))

(define-public crate-to-html-0.1.4 (c (n "to-html") (v "0.1.4") (d (list (d (n "ansi-to-html") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.10") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "fake-tty") (r "^0.3.1") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "03aghv44wjk2yf20v99f4zrm6y6ixpgy0vr2aw9gq47pdaj9crlm")))

(define-public crate-to-html-0.1.5 (c (n "to-html") (v "0.1.5") (d (list (d (n "ansi-to-html") (r "^0.2.1") (d #t) (k 0)) (d (n "basic-toml") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.10") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.10") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.3.0") (d #t) (k 1)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "fake-tty") (r "^0.3.1") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1nhg9nj0fw4xq8r1syv5jdkay79059ldj7vvb21vj8r935i913ah")))

