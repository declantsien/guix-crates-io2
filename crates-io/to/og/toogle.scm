(define-module (crates-io to og toogle) #:use-module (crates-io))

(define-public crate-toogle-0.1.0 (c (n "toogle") (v "0.1.0") (h "13l00y63fy68d3gwgf8xqm7s74xl2ajc76rdiapplxilmjc720vv")))

(define-public crate-toogle-0.1.1 (c (n "toogle") (v "0.1.1") (h "0gvpjggszsdg5m24mqmwrnkhr3ypn1n2nhacdx4r8p0h8dfdf08i")))

