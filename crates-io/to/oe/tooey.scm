(define-module (crates-io to oe tooey) #:use-module (crates-io))

(define-public crate-tooey-0.1.0 (c (n "tooey") (v "0.1.0") (d (list (d (n "keyboard-types") (r "^0.6.2") (d #t) (k 0)))) (h "1vx1i6qbkh1gi833cgiicf237pxans87v313q1cig9nzkbfvsi5g") (f (quote (("std") ("no_std")))) (y #t)))

(define-public crate-tooey-0.1.1 (c (n "tooey") (v "0.1.1") (d (list (d (n "keyboard-types") (r "^0.6.2") (d #t) (k 0)))) (h "1dj7h087jdhimjqinnb1l4h63qz50facfnq3jza94kwxp4icy9rj") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.1.2 (c (n "tooey") (v "0.1.2") (d (list (d (n "keyboard-types") (r "^0.6.2") (d #t) (k 0)))) (h "0kc9ahd47k42zqqxgc1pzhr7673d29bi3fm7mvzyxvxbv68dkr2w") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.1.3 (c (n "tooey") (v "0.1.3") (d (list (d (n "keyboard-types") (r "^0.6.2") (k 0)))) (h "15n6yikaf85pp5csyz3yy34v1r1gizxclh2w5b39152q9hq948ib") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.1.4 (c (n "tooey") (v "0.1.4") (h "0haxqza360bq12rgxibw3mb544y04b0v044ignpwbran722gr6x8") (f (quote (("std") ("no_std")))) (y #t)))

(define-public crate-tooey-0.2.0 (c (n "tooey") (v "0.2.0") (h "0xa92gckv2zjdna7x61nw79p9yq4h47329lx4jc8v2dlykh17msq") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.3.0 (c (n "tooey") (v "0.3.0") (h "15lq9y304i2s6yy0z23s1bisha9l8knw1q0sdvc46xg0ijl6n54f") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.4.0 (c (n "tooey") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0mihbcyn1zmg1105ba1d1w8sx15k24yzp1ijvdbf2x9yjwmxmlx2") (f (quote (("std") ("no_std")))) (y #t)))

(define-public crate-tooey-0.4.1 (c (n "tooey") (v "0.4.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "13p7vgkfaxrxzdrwm0lc73ax5rnj1yl0jnirhz60683pczhzyzpl") (f (quote (("std") ("no_std")))) (y #t)))

(define-public crate-tooey-0.4.2 (c (n "tooey") (v "0.4.2") (d (list (d (n "ndarray") (r "^0.15.6") (k 0)))) (h "0mr8gnwvk5jixbngr7vdrglbyl53a6vj5wjyix54wis7xp0xxb33") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.5.0 (c (n "tooey") (v "0.5.0") (d (list (d (n "ndarray") (r "^0.15.6") (k 0)))) (h "00a4iv1hz3j16gzgw0is4saq2h830p8vpdcapqi9i52ai82m6gbz") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.6.0 (c (n "tooey") (v "0.6.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (k 0)))) (h "19q9hzjc388bb7wxzfsjgq8h5z6pz42x9inidmn82z6kgy8mfxdr") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.7.0 (c (n "tooey") (v "0.7.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (k 0)))) (h "02mw65qlylza9n8vwmfxswggxvf4n8nakkama263ia0bfn235hsz") (f (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.7.1 (c (n "tooey") (v "0.7.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (k 0)))) (h "0z0cwqk9ddzhblvlasg2hhjc6wfmzny1dp54q1cdrcaqbn4w32qa") (f (quote (("std") ("objects") ("no_std") ("default" "objects"))))))

