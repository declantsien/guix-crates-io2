(define-module (crates-io to mi tomiko-auth) #:use-module (crates-io))

(define-public crate-tomiko-auth-0.1.0 (c (n "tomiko-auth") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.3") (f (quote ("runtime-tokio" "sqlite" "macros"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1hgknvgslr0jrj3d76942dp5plbgqwpn81zcpa7r4nb0qcfvscgn")))

