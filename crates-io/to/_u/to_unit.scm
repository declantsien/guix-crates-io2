(define-module (crates-io to _u to_unit) #:use-module (crates-io))

(define-public crate-to_unit-1.0.0 (c (n "to_unit") (v "1.0.0") (h "19pmacx5wiv0r6i38cl20jxf5cxpykky5z0fsh243i000i4p02z4")))

(define-public crate-to_unit-1.0.1 (c (n "to_unit") (v "1.0.1") (h "0wsjk0rch7ma24344bsyqjmkmykgw5w9k8r2bgpr7vl3v3pij1mg")))

(define-public crate-to_unit-1.0.2 (c (n "to_unit") (v "1.0.2") (h "1vrlsgindhk8yq7vcz9na6d0d63ka4ff3fg0b9hw5233x1m9ydxm")))

