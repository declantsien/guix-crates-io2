(define-module (crates-io to _u to_url_derive) #:use-module (crates-io))

(define-public crate-to_url_derive-0.1.0 (c (n "to_url_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "05fkkfmyfjhsp67q0i6n1y1m6ngv3m76kj2v195gp5r567hrgli4")))

