(define-module (crates-io to _u to_url) #:use-module (crates-io))

(define-public crate-to_url-0.1.0 (c (n "to_url") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "to_url_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0plmprw38pd218v4bczkqjc5vcf1w8qscry2waa6l2cp41s4gkwa") (f (quote (("derive") ("default" "derive"))))))

