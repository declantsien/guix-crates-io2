(define-module (crates-io to ck tockloader-proto) #:use-module (crates-io))

(define-public crate-tockloader-proto-0.1.0 (c (n "tockloader-proto") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1yb2w6gcsfj92wm7j73cmcbvkhr32gym5ra34nl4yac9204abkbk")))

(define-public crate-tockloader-proto-0.1.1 (c (n "tockloader-proto") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "11hrm2sgkb87qbl67qhdq4sf7yza3100973126r1cwxpl6bxzm76")))

(define-public crate-tockloader-proto-0.2.1 (c (n "tockloader-proto") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1n02q4y0radrb93pmwcq650z4ysyg1r5x7xv4a1mz8s1w2fwm5w9")))

