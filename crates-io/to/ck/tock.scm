(define-module (crates-io to ck tock) #:use-module (crates-io))

(define-public crate-tock-0.1.0 (c (n "tock") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0y01g0n2kifylf9v06jf2cp4m4xa7c24b7w6impghg3js99bcpw4") (f (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1.1 (c (n "tock") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0q9zppignr40c30620kcwylkmslpycw0hj9l7gz4jxwagj5dmga9") (f (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1.2 (c (n "tock") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0jp050a2zxhdvsxllq1acigd7cdn60wlr4ap98v0h8fi653h34na") (f (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1.3 (c (n "tock") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "1ibh4fy7s3hw2niyvhxxcdfz11yfx3nbcjbymrdffiavm0lcqzbl") (f (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1.4 (c (n "tock") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)))) (h "0lki1h64ssgwllgdi210f6zrhwxkn3mqaadlk9l0v7d30p4yw6nd") (f (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-1.0.0 (c (n "tock") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("color" "derive" "help" "std" "usage"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hlpbl0nwzhssh47s3p3m54vhvx37ay7giriiadp1dcjpirs94rr") (f (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-1.0.1 (c (n "tock") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std"))) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("color" "derive" "help" "std" "usage"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1phlibd786afz9p3grjh1yxijczr6mig6hv2alcdh78n5ml9dgk1") (f (quote (("interactive") ("default" "interactive"))))))

