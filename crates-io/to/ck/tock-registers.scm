(define-module (crates-io to ck tock-registers) #:use-module (crates-io))

(define-public crate-tock-registers-0.1.0 (c (n "tock-registers") (v "0.1.0") (h "1adpl6gqjyw5z8941c1sw5aymhgv6w32v8rls8cfwg72h3wk7k1a")))

(define-public crate-tock-registers-0.2.0 (c (n "tock-registers") (v "0.2.0") (h "05p3i27wk3l41x60slrl48ksl7v2v7w9znqab9260bpnyfa5sf1s")))

(define-public crate-tock-registers-0.3.0 (c (n "tock-registers") (v "0.3.0") (h "1qn3lmhjfzn8jplv2kgn9kvnyxmj0rjm0vygzvczj39fb8czan67")))

(define-public crate-tock-registers-0.4.0 (c (n "tock-registers") (v "0.4.0") (h "0g6qdd0nmkz1gpvps0ncnb03fb11v9ns8pkwzzi665ybs0gpalpf")))

(define-public crate-tock-registers-0.4.1 (c (n "tock-registers") (v "0.4.1") (h "0w3ac01kaa3ih7dl39myw75qil757gdgmhbqvlvl9yswqm4rrbsh")))

(define-public crate-tock-registers-0.5.0 (c (n "tock-registers") (v "0.5.0") (h "03y2v839i3g9jw2w08f9h7b07lafdq7y383dk308c888p3yklckh") (f (quote (("no_std_unit_tests"))))))

(define-public crate-tock-registers-0.6.0 (c (n "tock-registers") (v "0.6.0") (h "1shcxvzvpdg9wbvk46fs4rhn4sq510hy4z67r4bw8s6frjdaf8gm") (f (quote (("no_std_unit_tests"))))))

(define-public crate-tock-registers-0.7.0 (c (n "tock-registers") (v "0.7.0") (h "0bi8wrhq3955qn5vqf0wqq6qzcmn1d9m86pndqwhnk8zdjhgps2f") (f (quote (("std_unit_tests") ("register_types") ("default" "register_types" "std_unit_tests"))))))

(define-public crate-tock-registers-0.8.0 (c (n "tock-registers") (v "0.8.0") (h "1dc3fmvdrhic6mikhij2nshdcss4z1zpwdgnqgxz3d91pxn9hnm1") (f (quote (("register_types") ("default" "register_types"))))))

(define-public crate-tock-registers-0.8.1 (c (n "tock-registers") (v "0.8.1") (h "077jq2lhq1qkg0cxlsrxbk2j4pgx31wv6y59cnhpdqp7msh42sb9") (f (quote (("register_types") ("default" "register_types"))))))

(define-public crate-tock-registers-0.9.0 (c (n "tock-registers") (v "0.9.0") (h "1i4kil3lg6z0xzxwp2ksjkqiv0chag97x2vnc432r1hy7bdjz7ib") (f (quote (("register_types") ("default" "register_types"))))))

