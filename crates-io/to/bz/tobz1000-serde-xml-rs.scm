(define-module (crates-io to bz tobz1000-serde-xml-rs) #:use-module (crates-io))

(define-public crate-tobz1000-serde-xml-rs-0.4.1-tobz1000 (c (n "tobz1000-serde-xml-rs") (v "0.4.1-tobz1000") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0mnbbwhrldyd5h7xjmd96li5dbbbylyll4c715kfa47z131gbpwl")))

(define-public crate-tobz1000-serde-xml-rs-0.4.1-tobz1000-1 (c (n "tobz1000-serde-xml-rs") (v "0.4.1-tobz1000-1") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0p9qlyp3vw3aiz5amnvwvh626irrq29m925zd9xi1h3j4ikw731s")))

