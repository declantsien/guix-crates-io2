(define-module (crates-io to yv toyvm) #:use-module (crates-io))

(define-public crate-toyvm-0.1.0 (c (n "toyvm") (v "0.1.0") (h "19574nmdv5b487l1hqhd5dlc4bvdiqz3nzjyyw7i2gq2wvd9cbg3")))

(define-public crate-toyvm-0.1.1 (c (n "toyvm") (v "0.1.1") (h "12yidw7x38fr7xs8p229y80f0xc6qd9g8wwbswh8wxbca38x09qq")))

(define-public crate-toyvm-0.1.2 (c (n "toyvm") (v "0.1.2") (h "1nckq58i1hv14di1jyi9p9sldcl3k39nwjys6phqscnspah2yyd4")))

(define-public crate-toyvm-0.1.3 (c (n "toyvm") (v "0.1.3") (d (list (d (n "toy_runtime") (r "0.1.*") (d #t) (k 0)))) (h "00dfha0djqyzrbx9bi9r68mjrhsvma8graax509j339mhnlg5wqm")))

(define-public crate-toyvm-0.1.4 (c (n "toyvm") (v "0.1.4") (d (list (d (n "toy_runtime") (r "0.1.*") (d #t) (k 0)))) (h "0rpwgbcffhm88g88vs4i2xi92ffrmw3grnwyckjc3v25nkdjslzk")))

