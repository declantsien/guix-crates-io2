(define-module (crates-io to sh tosho-mplus) #:use-module (crates-io))

(define-public crate-tosho-mplus-0.1.0 (c (n "tosho-mplus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("rustls-tls" "socks" "stream" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tosho-macros") (r "^0.3") (d #t) (k 0)))) (h "1iaxb81fsqmd4dc5m1c2zwpa264idjzj8y22bzl4qjwq6ckbqn3v")))

