(define-module (crates-io to sh tosho-macros) #:use-module (crates-io))

(define-public crate-tosho-macros-0.3.0 (c (n "tosho-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14s731bjqshnjp21biwr9rcf38ks1z6wrz7im53zfa79qiin92xj")))

(define-public crate-tosho-macros-0.3.1 (c (n "tosho-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "11x1gnipi04dwjpzszinz1xh4lxpf6x8si69h0sf7v04h5brqh06")))

(define-public crate-tosho-macros-0.3.2 (c (n "tosho-macros") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vbjswaf1yx4a6dazyxg8mjamplnh6ymawank9z2spzih9m23mrh")))

