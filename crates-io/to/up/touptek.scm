(define-module (crates-io to up touptek) #:use-module (crates-io))

(define-public crate-touptek-1.0.0 (c (n "touptek") (v "1.0.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0a0qy4dnjfmj76w05hf00x4hyzkcy885908gw5fjfzjjr7b0rjw1")))

(define-public crate-touptek-1.1.0 (c (n "touptek") (v "1.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ig4pgd0w6b2l65nfcd5bfqp30kqk1svfzsx2i3zxq8mfg2b0wv4")))

