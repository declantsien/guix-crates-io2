(define-module (crates-io to fa tofas_extras) #:use-module (crates-io))

(define-public crate-tofas_extras-0.2.4 (c (n "tofas_extras") (v "0.2.4") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "tofas") (r "^0.2.4") (d #t) (k 0)))) (h "0rhq90w5czw4xwjzf9c3a4jdcmcbxzvpflhiidbs6xiksg2s2is6")))

(define-public crate-tofas_extras-0.2.5 (c (n "tofas_extras") (v "0.2.5") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "tofas") (r "^0.2.5") (d #t) (k 0)))) (h "079d1xp564xz49risg2z9jx3yqnhwpgyqk4kbqd29ya2fw3lfnnj")))

(define-public crate-tofas_extras-0.2.6 (c (n "tofas_extras") (v "0.2.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (d #t) (k 0)) (d (n "tofas") (r "^0.2.6") (d #t) (k 0)))) (h "0131r0w1r8dwjyfjh6m08ijf0nxiha4ipj6xj250kg16aka0sf1c")))

(define-public crate-tofas_extras-0.2.7 (c (n "tofas_extras") (v "0.2.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (d #t) (k 0)) (d (n "tofas") (r "^0.2.7") (d #t) (k 0)))) (h "0paq1cbdvd5sycngx3inycdkxavnn1ny9f5y6mhcpa3wgcb8z1my")))

