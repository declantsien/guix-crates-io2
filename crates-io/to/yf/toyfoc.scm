(define-module (crates-io to yf toyfoc) #:use-module (crates-io))

(define-public crate-toyfoc-0.0.1 (c (n "toyfoc") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "fugit") (r "^0.3") (d #t) (k 0)) (d (n "mt6701") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.1") (d #t) (k 0)))) (h "08ylnr8mp3ac7kl64fzqrhayz9mwvxk26dz524vx0zllhxd4ky5l")))

