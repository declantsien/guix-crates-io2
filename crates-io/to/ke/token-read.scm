(define-module (crates-io to ke token-read) #:use-module (crates-io))

(define-public crate-token-read-0.1.0 (c (n "token-read") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0a7b00ly8z2avsrnaj2jmgkamw1pcayig4xnj15db3vibqyma14k")))

(define-public crate-token-read-0.1.1 (c (n "token-read") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1g6gwnnqfqkxclk4x9n8la1fbmzs67gc5yz6pc8iay9hnaf6qd22")))

(define-public crate-token-read-0.1.2 (c (n "token-read") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "00n317k0aa0pl1criikycdzqzx0xh4nrfhrzlk3id8np9d1vzfhy")))

(define-public crate-token-read-0.2.0 (c (n "token-read") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "109ck5hv2z10whg2s6y725yhyz0bi31l7x0r01lcwk9qj8apr0la")))

