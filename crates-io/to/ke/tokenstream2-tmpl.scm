(define-module (crates-io to ke tokenstream2-tmpl) #:use-module (crates-io))

(define-public crate-tokenstream2-tmpl-0.1.0 (c (n "tokenstream2-tmpl") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m0sfpqxdkn3kx9ys1vany9ca68xgy2lw2vbyq4l55hy88nxhl07")))

(define-public crate-tokenstream2-tmpl-0.1.1 (c (n "tokenstream2-tmpl") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vhasjma5lrwgmq6rqqi3xn3fl1da2sh88ikqb95zhsncna8mx0c")))

