(define-module (crates-io to ke token-bindings) #:use-module (crates-io))

(define-public crate-token-bindings-0.10.0 (c (n "token-bindings") (v "0.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0fr0h4xaax0gbhjvh4kjd3yy3hx6x6n0g4avpab28kg3wsvqvn2d")))

(define-public crate-token-bindings-0.10.1 (c (n "token-bindings") (v "0.10.1") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0g9j4m0xlm88vkcwf1qnacdh9pd9g9c4m9pai3h9qqvnn9q6fk6m")))

(define-public crate-token-bindings-0.10.2 (c (n "token-bindings") (v "0.10.2") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0645gg6ijmrq5bbq5faaybf4hxiy3lx9pa2zzvzbmpk72ym762ib")))

(define-public crate-token-bindings-0.10.3 (c (n "token-bindings") (v "0.10.3") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "0dpy90dv5hrkc7m8nbjmiqk6abkk97j62qj3v3riq6daxfbpjakm")))

(define-public crate-token-bindings-0.11.0 (c (n "token-bindings") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "00j5vxqrxnys5sk16qv1cks60m3hrrp528lp1lr96a8dr69wiqcv")))

