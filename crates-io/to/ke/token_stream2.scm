(define-module (crates-io to ke token_stream2) #:use-module (crates-io))

(define-public crate-token_stream2-0.1.0 (c (n "token_stream2") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)))) (h "1p8y0x3cgq58fjh52i01inhfc920b78rx6k2bkgalkrb81q8cjp5") (r "1.56")))

(define-public crate-token_stream2-1.0.0 (c (n "token_stream2") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)))) (h "0gq2i9p6x4g9jzkg41idk680jw4qpa1w5lwrcv96kjs1j8kg07il") (r "1.56")))

(define-public crate-token_stream2-1.0.1 (c (n "token_stream2") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)))) (h "021jr020ayngq7d5sjrllbvwfw28zz99vhx771mfrd9pvjr6kzly") (r "1.56")))

(define-public crate-token_stream2-1.0.2 (c (n "token_stream2") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)))) (h "0ycfd07fipqfs8941vpyywdcg7x1j76snbzx5mf6mnhbggf46nk4") (r "1.56")))

