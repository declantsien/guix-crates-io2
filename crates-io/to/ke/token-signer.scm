(define-module (crates-io to ke token-signer) #:use-module (crates-io))

(define-public crate-token-signer-0.1.2 (c (n "token-signer") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.2.1") (d #t) (k 0)))) (h "03swksja283j37xqayfnzn8njrl6h0mwwn0sdnjkk3s6xi7m049f") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.2.0 (c (n "token-signer") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.2.1") (d #t) (k 0)))) (h "1shi0idd753caj013nfnib59yl827nswabx2n7459hlh03v3vn9c") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.2.1 (c (n "token-signer") (v "0.2.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.4.0") (d #t) (k 0)))) (h "18wnsc7zmp057dl5za1z8mpa5agmdbkl11gka97magpskqizz9j2") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.2.2 (c (n "token-signer") (v "0.2.2") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.4.0") (d #t) (k 0)))) (h "11jv1fsrfbh0srg70nvk3ndvj1q50v5f17bgjwi947n73zrrihmj") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.3.0 (c (n "token-signer") (v "0.3.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.3") (d #t) (k 0)))) (h "0q0ndbl124148yfwzac2fiah4cxza0k2ssikvgd3lczzhlkc5z1x") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.3.1 (c (n "token-signer") (v "0.3.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.3") (d #t) (k 0)))) (h "0619x1rdd8hmxcppnby937k5xhvgs42hb35nr8ngnm6v4yn132dk") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.5.0 (c (n "token-signer") (v "0.5.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1ig6n14m6p01vawsk9mhrjzsh3v5nam1429wybv73jjh9wjl9n5h") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.5.2 (c (n "token-signer") (v "0.5.2") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1x52az4nflfb68z4wmflk5xg5kaglmz0ri8fjwhyd89qp1q45s6k") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.5.3 (c (n "token-signer") (v "0.5.3") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0xcl0nw97psqhpv7x4lqmx29cg9b78al4m9hbnfy05jnxf6i911k") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.5.5 (c (n "token-signer") (v "0.5.5") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1xj6pfhmdkpyv1jd1y5llz6nl55p3rc4rbz265mkdmd3gsmas73d") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.6.0 (c (n "token-signer") (v "0.6.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1y455mfvdmivnbpglvgr3rjyl9d2ayrcvdhhjpl460ypbcngilhj") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.6.1 (c (n "token-signer") (v "0.6.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "0sf7f0dckg48rkfdw9i3fbgy495h9n5mwas2jr4lxj7jal1ji6ny") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.7.0 (c (n "token-signer") (v "0.7.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0p0l1k2xxd31xnlz2zri75ns22pvvnbjq3digzdrrlkmk2irqydf") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.8.0 (c (n "token-signer") (v "0.8.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1vzhkq64z4fykpdrx8nrx8g4diw7irilqhhb7k8d6vk7pkfp53dz") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.9.0 (c (n "token-signer") (v "0.9.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1rx34q33c5j1snja0hp6z11gc5v5z0kjrr6vz70filriv4xrzrc8") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.10.1 (c (n "token-signer") (v "0.10.1") (d (list (d (n "anchor-lang") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1dvk735h1hnwr5bm4iizly3j6yx6awijcwgss1qlrp13kid7ncip") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.10.2 (c (n "token-signer") (v "0.10.2") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1fxnx9i3gaasx3q9n70jpk2kd6hklxxl8azxq80pcnjmnjmgq3am") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.10.3 (c (n "token-signer") (v "0.10.3") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1x20z9fjac01l332mii9zlxglp58jan1vl24g2ns3fsd8c6rngk2") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.10.4 (c (n "token-signer") (v "0.10.4") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "0vajbzjcrdf8wxgxzcmrlpr16vyh8imxz51z61va9z4vymv8pn9c") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-token-signer-0.11.1 (c (n "token-signer") (v "0.11.1") (d (list (d (n "anchor-lang") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22, <=0.24") (d #t) (k 0)) (d (n "vipers") (r "^2.0") (d #t) (k 0)))) (h "1wk9j1bw2w0dwywcblksqndvkyc1gcwm1s8yhps1a90igwr84840") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

