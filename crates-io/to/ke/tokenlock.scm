(define-module (crates-io to ke tokenlock) #:use-module (crates-io))

(define-public crate-tokenlock-0.1.0 (c (n "tokenlock") (v "0.1.0") (h "0dl8z3ivxv0d2d6nl9f2kgm5wjhaw0wnvggpan3pr0cl5xgs0bkn")))

(define-public crate-tokenlock-0.1.1 (c (n "tokenlock") (v "0.1.1") (h "1dylx5a954m10jmx0cnqnpxrgbmp4nl9rrqik189dsvgz54whm9z")))

(define-public crate-tokenlock-0.1.2 (c (n "tokenlock") (v "0.1.2") (h "1qqanh7z22ibfz6awgdk17515k82p1g688d8znjxh8lhj8mngxh9")))

(define-public crate-tokenlock-0.1.3 (c (n "tokenlock") (v "0.1.3") (h "0nx2cs3h9x741fzs0i3mjgs2cvqb8z5dqr8l879a14dy4yvfdm0g")))

(define-public crate-tokenlock-0.1.4 (c (n "tokenlock") (v "0.1.4") (h "1sazyqqkcvpix8d3v6gdcm5q931956mlj0s2d4wv5657x97ydasr")))

(define-public crate-tokenlock-0.1.5 (c (n "tokenlock") (v "0.1.5") (h "0byziz40zaq7p9p9x093yi93y9p6bacyrq8lvdq5n3cavcqk6rc8")))

(define-public crate-tokenlock-0.1.6 (c (n "tokenlock") (v "0.1.6") (h "13xbjxz1g0knlx4xl4787dfmymw3z70ayz5v5dvp3bfy6w29xjnj")))

(define-public crate-tokenlock-0.2.0 (c (n "tokenlock") (v "0.2.0") (h "1dna389aky1afcbr852l357fc5yldmymci4ma2fj6fgpvsrl7889") (y #t)))

(define-public crate-tokenlock-0.2.1 (c (n "tokenlock") (v "0.2.1") (h "1ghvss1i35h1czvllxm730k12hs9lzl52816slq7hlb0mnzks19c") (y #t)))

(define-public crate-tokenlock-0.2.2 (c (n "tokenlock") (v "0.2.2") (h "0sgn44i2fk6kfz6frm9b63sbsgrg62zn3mnja3s1vx10qp56dgx3")))

(define-public crate-tokenlock-0.2.3 (c (n "tokenlock") (v "0.2.3") (h "1mas57r36yjp11fc2m229mkgbh8jskcbdrdk8k1parv6wfyai9yn") (f (quote (("std") ("default" "std"))))))

(define-public crate-tokenlock-0.3.0 (c (n "tokenlock") (v "0.3.0") (h "06bv21cc1kcs0g46lqzcbrp3rlwdw9l9bw88dqj61rcwlhbbsj45") (f (quote (("std") ("default" "std"))))))

(define-public crate-tokenlock-0.3.1 (c (n "tokenlock") (v "0.3.1") (h "1gp9a9rjlsbj7g48k6g5j9bw30n3a7ildhkih2pm8m3l7s7lydb1") (f (quote (("std") ("default" "std"))))))

(define-public crate-tokenlock-0.3.2 (c (n "tokenlock") (v "0.3.2") (h "04207awp8xrsf9vv534gpq7imf7b7by8lvi03q8f5d8s2x4wbibv") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-tokenlock-0.3.3 (c (n "tokenlock") (v "0.3.3") (h "0332gbgih8q6mhvmqdhcir61h5s8vb3sp8ghvpxg513y9dq107kl") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-tokenlock-0.3.4 (c (n "tokenlock") (v "0.3.4") (h "129b91r4q0g65c3cj73dk9c2wvprb1z26gshsyg53c4qji4jgx5n") (f (quote (("std") ("default" "std"))))))

(define-public crate-tokenlock-0.3.5 (c (n "tokenlock") (v "0.3.5") (d (list (d (n "async-scoped") (r "^0.7.0") (f (quote ("use-tokio"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1178nc6h85y7rfjxsz1366cr6827dcs057n3jzyp9689nyhzim57") (f (quote (("unstable") ("std" "spin" "alloc") ("doc_cfg") ("default" "std") ("alloc"))))))

(define-public crate-tokenlock-0.3.6 (c (n "tokenlock") (v "0.3.6") (d (list (d (n "async-scoped") (r "^0.7.0") (f (quote ("use-tokio"))) (d #t) (k 2)) (d (n "const-default_1") (r "^1") (o #t) (k 0) (p "const-default")) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "175j98mhmyxqnsq2mlcdp9965b58xqfhir8b20mlqhiaqpn668ls") (f (quote (("unstable") ("std" "spin" "alloc") ("doc_cfg") ("default" "std") ("alloc"))))))

(define-public crate-tokenlock-0.3.7 (c (n "tokenlock") (v "0.3.7") (d (list (d (n "async-scoped") (r "^0.7.0") (f (quote ("use-tokio"))) (d #t) (k 2)) (d (n "const-default_1") (r "^1") (o #t) (k 0) (p "const-default")) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "0z9z9j9y3v89rwyxda7ck0qgcv0z3s2kndi6wlbgm4v8xlnnvvj5") (f (quote (("unstable") ("std" "spin" "alloc") ("doc_cfg") ("default" "std") ("alloc"))))))

(define-public crate-tokenlock-0.3.8 (c (n "tokenlock") (v "0.3.8") (d (list (d (n "async-scoped") (r "^0.7.0") (f (quote ("use-tokio"))) (d #t) (k 2)) (d (n "const-default_1") (r "^1") (o #t) (k 0) (p "const-default")) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "spin") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "14va7ckjhbs4d9i1rymi1cczrviix9pkqxfl4dlhdpx080dda3hg") (f (quote (("unstable") ("std" "spin" "alloc") ("doc_cfg") ("default" "std") ("alloc"))))))

