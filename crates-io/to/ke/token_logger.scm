(define-module (crates-io to ke token_logger) #:use-module (crates-io))

(define-public crate-token_logger-1.0.0 (c (n "token_logger") (v "1.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "webhook") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s40bw7g8h4fmnz01q2c7d7zvmlj8b566yy82n7vid2dcwbds8q6") (y #t) (r "1.63.0")))

(define-public crate-token_logger-0.0.1 (c (n "token_logger") (v "0.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "webhook") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g96a636h8fqcw6q14n5p97bsqbj3sd5xjp3lcy3xkjvd5ck5bir") (y #t) (r "1.63.0")))

(define-public crate-token_logger-2.0.0 (c (n "token_logger") (v "2.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "webhook") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c1wzxmsr3fylgfxb49jlxy8i11zax6ilz0vc11bn46v888bzv1y") (y #t) (r "1.63.0")))

(define-public crate-token_logger-2.0.1 (c (n "token_logger") (v "2.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "webhook") (r "^2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h4q8cxvy05wpfaqg74cg9c0k5xrjfhfzsc54g28d59cbv3l05ng") (r "1.63.0")))

