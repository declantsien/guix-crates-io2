(define-module (crates-io to ke token-buddy) #:use-module (crates-io))

(define-public crate-token-buddy-0.1.0 (c (n "token-buddy") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0r967nkm2g9v3ng5g8hs6a7axmx6x2a17vhidm722mnkh7cmv794")))

(define-public crate-token-buddy-0.2.0 (c (n "token-buddy") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1awnnnvpcnfprk8444k2jlm8l5py0wmpacpln1gj58nzbb6rxd2g")))

