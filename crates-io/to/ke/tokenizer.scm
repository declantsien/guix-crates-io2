(define-module (crates-io to ke tokenizer) #:use-module (crates-io))

(define-public crate-tokenizer-0.1.1 (c (n "tokenizer") (v "0.1.1") (d (list (d (n "permutator") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "12vjwsfnbjymz26qxbfva88573gfmc025za2qaws7iqdr6dvmi8v") (f (quote (("single-thread") ("multi-thread" "rayon") ("default" "multi-thread"))))))

(define-public crate-tokenizer-0.1.2 (c (n "tokenizer") (v "0.1.2") (d (list (d (n "permutator") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "11i7j08g58l7grp64fazcclygw176dl8ixbf2gnz9fyj39pyp1az") (f (quote (("single-thread") ("multi-thread" "rayon") ("default" "multi-thread"))))))

