(define-module (crates-io to ke token-cell) #:use-module (crates-io))

(define-public crate-token-cell-1.0.0 (c (n "token-cell") (v "1.0.0") (h "1ph7nki0zg6i8gw3417wa10qzc1hjn0paynfzi75kkp97pmb1nap")))

(define-public crate-token-cell-1.1.0 (c (n "token-cell") (v "1.1.0") (h "1iypmlgijafvvisjz43zy023klns86knipi2g92z4y8yhm16hfm3") (f (quote (("no_std"))))))

(define-public crate-token-cell-1.1.1 (c (n "token-cell") (v "1.1.1") (d (list (d (n "ghost-cell") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1vd5fc7x3kiaqi0lrj43zvsfjxk5pydxbajmx0a8j5nanqv4wfvx") (f (quote (("no_std") ("default" "debug") ("debug"))))))

(define-public crate-token-cell-1.2.0 (c (n "token-cell") (v "1.2.0") (d (list (d (n "ghost-cell") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1pqyn0rx5jvz0vn1fa9gc2p5544pvq0mmi1666gnj7mx1amflz9p") (f (quote (("no_std") ("default" "debug") ("debug"))))))

(define-public crate-token-cell-1.3.0 (c (n "token-cell") (v "1.3.0") (d (list (d (n "ghost-cell") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "15fgp9qvl0k8p8kxjnsc3smq2k98khjk0xp113rjhi2k0f0b0m24") (f (quote (("no_std") ("default" "debug") ("debug"))))))

(define-public crate-token-cell-1.4.0 (c (n "token-cell") (v "1.4.0") (d (list (d (n "ghost-cell") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1izdsji1ci081qa2zb8pn7rk6fms2z5pzldrcy2g5d47m6x48r05") (f (quote (("no_std") ("default" "debug") ("debug"))))))

(define-public crate-token-cell-1.4.1 (c (n "token-cell") (v "1.4.1") (d (list (d (n "ghost-cell") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1r5a6nq1xdapp0fvwqig2ajkv1gang0if39cjay4a377ba8hjl9y") (f (quote (("no_std") ("default" "debug") ("debug"))))))

(define-public crate-token-cell-1.4.2 (c (n "token-cell") (v "1.4.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "ghost-cell") (r "^0.2") (d #t) (k 2)))) (h "06zq6m6z8bjcjlvivd5sxpssy7dkbfg7fah3wj7qzcy7zj0bz8sr") (f (quote (("no_std") ("default") ("debug"))))))

(define-public crate-token-cell-1.5.0 (c (n "token-cell") (v "1.5.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "ghost-cell") (r "^0.2") (d #t) (k 2)))) (h "11zbqr0i0bw0536lqczfiqzs7g6jpb0xf15b9s5b00xkzmjbk8pl") (f (quote (("std") ("default" "std") ("debug"))))))

