(define-module (crates-io to ke tokengen-core) #:use-module (crates-io))

(define-public crate-tokengen-core-0.1.0 (c (n "tokengen-core") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wx1n9d3dzy7fxib95b1w0fja7hpcfzc02vsxy0lij82hxdxhll3")))

(define-public crate-tokengen-core-0.1.1 (c (n "tokengen-core") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h774cf3ix008wg29ghh7skkhywqgk0ljp9anmbynbzkyl2hr0nm")))

