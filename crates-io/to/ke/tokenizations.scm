(define-module (crates-io to ke tokenizations) #:use-module (crates-io))

(define-public crate-tokenizations-0.1.0 (c (n "tokenizations") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "0sir5h3imab0qq7df7daw76cz0f1hyqr66wvr8d6dydjqfz8y797")))

(define-public crate-tokenizations-0.1.1 (c (n "tokenizations") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "0nfz2q1cdllfnj21xp7j18jlbq1vzrznj6zlv2n8pdjb6r0wmyk4")))

(define-public crate-tokenizations-0.1.2 (c (n "tokenizations") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "1mwbqn9wilc411rkhnk4w27n4jbgnv9sl5msaiaaz62f7dwyl39v")))

(define-public crate-tokenizations-0.1.3 (c (n "tokenizations") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "0qgaaxacy40fkin2q61yafhshgphzivbg5cjqsn73wvxp78l817z")))

(define-public crate-tokenizations-0.1.4 (c (n "tokenizations") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "0117q7qlb09p9psh7brglgkcjn3y3qj0lv1lqjph6mk1s6fc3891")))

(define-public crate-tokenizations-0.2.2 (c (n "tokenizations") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "1nxs5d1fbpmcygwibd29chiqvg0h0acnrsp5n39bjk96vhxycf7z")))

(define-public crate-tokenizations-0.2.4 (c (n "tokenizations") (v "0.2.4") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "1fwyv1689qf4asg9rxdq315n38823pbsfrrdigkzan1mqhm8m9az")))

(define-public crate-tokenizations-0.3.0 (c (n "tokenizations") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "11pnxcjgixlvg06rccp6qarm2zh4lgqvc2f7dka8q2as2wi87p2x")))

(define-public crate-tokenizations-0.4.0 (c (n "tokenizations") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "seqdiff") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "0p54x8qvm41mdrqppp20l0jp1866pbaihwdll6bljzg1xl2h3jr3")))

(define-public crate-tokenizations-0.4.1 (c (n "tokenizations") (v "0.4.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "seqdiff") (r "^0.2") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "16wh71hxzijzccqzf6hns3n2fgg15y1d5wzfhz87pd5jjax3956y")))

(define-public crate-tokenizations-0.4.2 (c (n "tokenizations") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "seqdiff") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "0h3fn2g3kl6y8ncadm5d4jvcr6aif6gx54l879i09588id5inf42")))

