(define-module (crates-io to ke tokeit) #:use-module (crates-io))

(define-public crate-tokeit-0.1.0 (c (n "tokeit") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokei") (r "^12") (d #t) (k 0)))) (h "1vs25abgmblh532zfnkyddiz5sq7gnqfsrx0l5frzsprjq7rdgi3")))

(define-public crate-tokeit-0.1.1 (c (n "tokeit") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokei") (r "^12") (d #t) (k 0)))) (h "0h1l4k1nrzb1nngsair7zcr1f2dapbhn8p4kr758m970qs3imbs8")))

