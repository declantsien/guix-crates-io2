(define-module (crates-io to ke tokengen-cli) #:use-module (crates-io))

(define-public crate-tokengen-cli-0.1.0 (c (n "tokengen-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokengen-core") (r "^0.1.0") (d #t) (k 0)))) (h "0dnwkicwlkmjy4iimb553020canis12i3a91s8jg5glq3bxb845x")))

(define-public crate-tokengen-cli-0.1.1 (c (n "tokengen-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokengen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1cj18vzah5xx93s29vv6k1a2saskk3rj79nnm4av3q9fx13kynwl")))

