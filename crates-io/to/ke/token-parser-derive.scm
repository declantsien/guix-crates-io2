(define-module (crates-io to ke token-parser-derive) #:use-module (crates-io))

(define-public crate-token-parser-derive-0.1.0 (c (n "token-parser-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gbfrbf6grkzcdss0ppfqblsfd6hp9bqhyj65ilxcvsdw84hjvh3")))

(define-public crate-token-parser-derive-0.2.0 (c (n "token-parser-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r7s9snlk0wvw8p1jvjpghh3hizl9nkk7rab365ipcvssldldmbi")))

