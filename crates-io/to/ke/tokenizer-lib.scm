(define-module (crates-io to ke tokenizer-lib) #:use-module (crates-io))

(define-public crate-tokenizer-lib-0.1.0 (c (n "tokenizer-lib") (v "0.1.0") (h "1pmsn86nyy15jm5h813b76b1m9jpxlkhgcz9jg0p48wywjvckpgm")))

(define-public crate-tokenizer-lib-0.3.0 (c (n "tokenizer-lib") (v "0.3.0") (h "1k67idj63rayvgl3j7h5zmsvnfrx8jlcznhb2ajjqxxfy37v5wmw")))

(define-public crate-tokenizer-lib-0.4.0 (c (n "tokenizer-lib") (v "0.4.0") (h "0f8z1x9nd4ipxfzq7nykhnid59bymdbw2ydbb8h2p5iryrm1sxqp")))

(define-public crate-tokenizer-lib-0.4.1 (c (n "tokenizer-lib") (v "0.4.1") (h "1kfi0c62afyvy0yj4g43xldpi8vv0iicz99v955n11l3x81b31dl")))

(define-public crate-tokenizer-lib-0.4.2 (c (n "tokenizer-lib") (v "0.4.2") (h "18f4yn2sbr0344b1y8m1qk0cy7dsw5h2c6x51rq6nkzy9sqjq93x")))

(define-public crate-tokenizer-lib-1.0.0 (c (n "tokenizer-lib") (v "1.0.0") (h "19d9vh6wxyxpvcaz22fiqjmfp581hf75j1lfrrprsc91zvdvliy4")))

(define-public crate-tokenizer-lib-1.1.0 (c (n "tokenizer-lib") (v "1.1.0") (h "1s3pcvyvm35khppj4i3z4kfcdjgbhjbm15gbvmm4pzjfkfmld6i6")))

(define-public crate-tokenizer-lib-1.1.1 (c (n "tokenizer-lib") (v "1.1.1") (h "1qdb1lyvdjmlrycrf500a1bk2h7dwxlw0k4c2882i9ciyw742n8f")))

(define-public crate-tokenizer-lib-1.2.0 (c (n "tokenizer-lib") (v "1.2.0") (h "0j50wnibvbs0ji6g561881aqmd8b47fdbjvid8klv8s7zcq92hlh")))

(define-public crate-tokenizer-lib-1.2.1 (c (n "tokenizer-lib") (v "1.2.1") (h "0cpr560qy4jjvzzl6dw6f52q5cgdx7x7br0ycr1dw5krbzg8brag")))

(define-public crate-tokenizer-lib-1.3.0 (c (n "tokenizer-lib") (v "1.3.0") (h "1zjgyk4d1pdlp3vynyh83hr5j36bznsyijvb779n1r33ii591ybz")))

(define-public crate-tokenizer-lib-1.4.0 (c (n "tokenizer-lib") (v "1.4.0") (h "0h2qz1gk92ssmf0na5imh4w98877n5a45jihm1avblmass8lldcm")))

(define-public crate-tokenizer-lib-1.5.0 (c (n "tokenizer-lib") (v "1.5.0") (h "0snkaj1rc6gg421nkjc987yc75616wnx1j88qxdsmxyv9j5yh6lj") (f (quote (("parallel") ("generator") ("default" "parallel" "buffered" "generator") ("buffered"))))))

(define-public crate-tokenizer-lib-1.5.1 (c (n "tokenizer-lib") (v "1.5.1") (d (list (d (n "source-map") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "1n6552ibwx4sq53jbih8xxcxmy4qkhhrwx0ws0g6wrv08wxxrp5f") (f (quote (("sized-tokens" "source-map") ("parallel") ("generator") ("default" "parallel" "buffered" "generator") ("buffered"))))))

(define-public crate-tokenizer-lib-1.6.0 (c (n "tokenizer-lib") (v "1.6.0") (d (list (d (n "source-map") (r "^0") (o #t) (d #t) (k 0)))) (h "0zs6lfp72z8lq0kcajfvwp7y2hp1n324x05yil1jdljwazvd9lsl") (f (quote (("sized-tokens" "source-map") ("parallel") ("generator") ("default" "parallel" "buffered" "generator") ("buffered"))))))

