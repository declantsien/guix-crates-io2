(define-module (crates-io to ke tokengen-web) #:use-module (crates-io))

(define-public crate-tokengen-web-0.1.0 (c (n "tokengen-web") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "tokengen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1d9gik4r0vapwvmk8b0rgn78jk9gd65hpca0gpjlqb70bx848wy8")))

(define-public crate-tokengen-web-0.1.1 (c (n "tokengen-web") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "tokengen-core") (r "^0.1.0") (d #t) (k 0)))) (h "17z1wj0b87si40v91mdvdg4i8q1k73qn7dl0n74hxbfn3gbbpd51")))

