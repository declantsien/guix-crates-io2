(define-module (crates-io to ke tokenlist) #:use-module (crates-io))

(define-public crate-tokenlist-0.1.0 (c (n "tokenlist") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pubkey") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vk2yk6g1afnqvh4z6v10fdk1x2gcmdsxavq5dqp9knn4s3v06ml") (y #t)))

(define-public crate-tokenlist-0.1.1 (c (n "tokenlist") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pubkey") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "13i5z8ppnip7c6bfl5q9bhfnwykfszlaqlh5asr8l8nw7c229wjl") (y #t)))

