(define-module (crates-io to ke tokenbucket) #:use-module (crates-io))

(define-public crate-tokenbucket-0.1.0 (c (n "tokenbucket") (v "0.1.0") (h "0ysvmw6mwq0623lx1a402gvwb9p5g7wng5hvfrvbxkwkqsfs9x8n")))

(define-public crate-tokenbucket-0.1.1 (c (n "tokenbucket") (v "0.1.1") (h "1h6xklxa893q7plxvmjivji7xv1nvnlfpxwp5fcgg3b73g1nz25b")))

(define-public crate-tokenbucket-0.1.2 (c (n "tokenbucket") (v "0.1.2") (h "0k4g2j8bpjh7msc8lplh730jqcy8hs760416p36q8f86lw0yz7a2")))

(define-public crate-tokenbucket-0.1.3 (c (n "tokenbucket") (v "0.1.3") (h "0rdvmvv6x50ah8k1n957ifrzbl07i52vazgl0i9ch12m529k1yih")))

(define-public crate-tokenbucket-0.1.4 (c (n "tokenbucket") (v "0.1.4") (h "1hlbm7x58bm2w06m2vpznyb988gm0arflkkl90xxyn8n170j6m6f")))

(define-public crate-tokenbucket-0.1.5 (c (n "tokenbucket") (v "0.1.5") (h "0bixxvaxph2bc2fcyif17cdsz6qigqx8s56ffhmpdwa3cfz2vwj0")))

