(define-module (crates-io to ke token-metadata-descriptor) #:use-module (crates-io))

(define-public crate-token-metadata-descriptor-0.0.1 (c (n "token-metadata-descriptor") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.5.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.10.29") (d #t) (k 0)) (d (n "solana-security-txt") (r "^1.0.2") (d #t) (k 0)))) (h "1iz32r7nxp1b2i4yrgqrx42njmr7yrk1xazvcaicc1dwn72wf3ph") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

