(define-module (crates-io to ke token-tree) #:use-module (crates-io))

(define-public crate-token-tree-0.0.73015087 (c (n "token-tree") (v "0.0.73015087") (d (list (d (n "expect-test") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)))) (h "09phn7qa3d3xld9vr9nphb91l8v6rmw4lfpdx8cv0b6xy7ib1297") (f (quote (("dev-dependencies" "serde/derive" "expect-test")))) (y #t)))

(define-public crate-token-tree-0.0.0-- (c (n "token-tree") (v "0.0.0--") (h "0jpabr858mzrjik6pc5rm5vw747y3sg448l5jk5by226yy0ilm1s") (y #t)))

(define-public crate-token-tree-0.0.0-r0 (c (n "token-tree") (v "0.0.0-r0") (h "073v0w0qg57cl8f3b84iwca6jrvgjyd8adlw2zgd91v1q4qb87g8")))

