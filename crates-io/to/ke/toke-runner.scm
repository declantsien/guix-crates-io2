(define-module (crates-io to ke toke-runner) #:use-module (crates-io))

(define-public crate-toke-runner-0.1.0 (c (n "toke-runner") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0fq4rpghayr134916jm8v35hv52ml9n1fcppq79yb0i0rspzgici")))

(define-public crate-toke-runner-0.1.1 (c (n "toke-runner") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "051x4wvz0nppzs0dbgz9dzsq69i6jmn14qn6mrwf88gmi4m3fqgk")))

(define-public crate-toke-runner-0.1.2 (c (n "toke-runner") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0l5ify4a9xbp8n66mn7wncbccznx3fh9zdzarh6njxk4a3s133vw")))

(define-public crate-toke-runner-0.1.3 (c (n "toke-runner") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0mn9bm6w2f5wrgdlq1ckr30f2b5j64i04xkjdkav70s8bag6is2n")))

(define-public crate-toke-runner-0.1.4 (c (n "toke-runner") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1bxmz2c61wl9cx8qb8n7v300i7hrl3h50m132d2xh9pf2gzm2wq8")))

