(define-module (crates-io to ke token_store) #:use-module (crates-io))

(define-public crate-token_store-0.1.0 (c (n "token_store") (v "0.1.0") (h "0v3vgih8znad55kv6da4fvsh7pwbmb6z23nqlan5bixy4ap2g3nn")))

(define-public crate-token_store-0.1.1 (c (n "token_store") (v "0.1.1") (h "1fllv531pjkr2sahfzi4swnjqzzy7cm0fj0s2d6k7salywa05zlp")))

(define-public crate-token_store-0.1.2 (c (n "token_store") (v "0.1.2") (h "0j8qws13w6x53j1n5k814igbvdr0hd8cca8mkhxi04gwfn1q71m6")))

