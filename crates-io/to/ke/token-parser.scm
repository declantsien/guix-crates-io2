(define-module (crates-io to ke token-parser) #:use-module (crates-io))

(define-public crate-token-parser-0.1.0 (c (n "token-parser") (v "0.1.0") (d (list (d (n "token-parser-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0hnamb4l35xppm8nd1bswxjw9xkbs4ds855hp8ak79763wawyfhx")))

(define-public crate-token-parser-0.2.0 (c (n "token-parser") (v "0.2.0") (d (list (d (n "token-parser-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0mrfk6wj1dijwqx0ad4x21hhvi49j330ww3xw96p53cz6b961ld2")))

(define-public crate-token-parser-0.3.0 (c (n "token-parser") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1cs6bcv2dfsdwyj26w9rh2ds6lnvhfap90imdcrsz84nl263hwf5") (f (quote (("radix-parsing" "num-traits"))))))

