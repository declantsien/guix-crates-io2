(define-module (crates-io to de todel_codegen) #:use-module (crates-io))

(define-public crate-todel_codegen-0.3.3 (c (n "todel_codegen") (v "0.3.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "12sz5vg8bv7ya00d3fcvpb5qv287px4vjjprlmd2cmkc56rqs1xy")))

