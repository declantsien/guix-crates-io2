(define-module (crates-io to bi tobii-sys) #:use-module (crates-io))

(define-public crate-tobii-sys-0.1.0 (c (n "tobii-sys") (v "0.1.0") (h "07kr2j3pdsfygv7d5zxmiy8qjfn16sphbyfif1rilgdj8mmyvf6c")))

(define-public crate-tobii-sys-0.2.0 (c (n "tobii-sys") (v "0.2.0") (h "0y8imaj917qqr90av79wg0nyiij4iy9598g81g9qwjqyn617a830")))

