(define-module (crates-io to n_ ton_smart_contract_address) #:use-module (crates-io))

(define-public crate-ton_smart_contract_address-0.0.0 (c (n "ton_smart_contract_address") (v "0.0.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "1va32fddp0cr7m0xhnm45gqy9yrzdqdy08mdk6s0j5lnrp5i2hzp")))

(define-public crate-ton_smart_contract_address-0.0.1 (c (n "ton_smart_contract_address") (v "0.0.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1rl05vyf7n0m7qcpq4172cl15k3svsxp6mm6l8hrnfm2l2q7ijh7")))

(define-public crate-ton_smart_contract_address-0.0.2 (c (n "ton_smart_contract_address") (v "0.0.2") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0vninlk5i6v2bwf2a38q19llkbr2n4m3d80ijkb2dphc29j48n3j")))

(define-public crate-ton_smart_contract_address-0.0.3 (c (n "ton_smart_contract_address") (v "0.0.3") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1l4h7j2kfgnixa3hw7b9i4lfxcg63zamccy3fn81zw2z548r6vrp")))

(define-public crate-ton_smart_contract_address-0.0.4 (c (n "ton_smart_contract_address") (v "0.0.4") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "19vllrgdvmnzy6x1wa3hr712vq0ynkqsif6kaxr70fxw9vscmy61")))

