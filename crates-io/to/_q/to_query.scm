(define-module (crates-io to _q to_query) #:use-module (crates-io))

(define-public crate-to_query-0.5.0 (c (n "to_query") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bdv62wb8h29zv02sslc984qk2ivgd59f4vvb9n3gi6ncw2cmw18") (y #t)))

(define-public crate-to_query-0.5.1 (c (n "to_query") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yp0dhhl6r1g700jsvmb46javiily3wbmdqnjf6y5jyj1i1j6rcl") (y #t)))

(define-public crate-to_query-0.5.2 (c (n "to_query") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0c1y5qdl09q71v8f1yxgmycr5fh0amnjf3kccpyf3ckmwvnq0qd3") (y #t)))

(define-public crate-to_query-0.5.3 (c (n "to_query") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bzanq4d24brd82q3w0xac9g6hbzqj8amj3cf2p14jwvd5g1w2gi") (y #t)))

(define-public crate-to_query-0.5.4 (c (n "to_query") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08r28xv447r1jkgv6ari679rlxmnjmlhx490ammx0q4nzp4lnz8d")))

