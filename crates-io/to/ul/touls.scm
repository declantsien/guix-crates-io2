(define-module (crates-io to ul touls) #:use-module (crates-io))

(define-public crate-touls-0.1.0 (c (n "touls") (v "0.1.0") (d (list (d (n "borgflux") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "wakey-wakey") (r "^0.1.0") (d #t) (k 0)))) (h "0jpan2fmf08w185il0ggbch79j9bwx7rspqz18kyvvc70zi2p5rv")))

