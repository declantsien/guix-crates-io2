(define-module (crates-io to ip toipe) #:use-module (crates-io))

(define-public crate-toipe-0.1.0 (c (n "toipe") (v "0.1.0") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1jw9zvbi665zg89v1fcr4ks0894m3r6wf7b62glw5plr5yamsz1f")))

(define-public crate-toipe-0.1.1 (c (n "toipe") (v "0.1.1") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "136lpjhyanfq06nfmvvxw9lqv94b7bq7dhhm33gmda9vi4v6yz1z")))

(define-public crate-toipe-0.2.0 (c (n "toipe") (v "0.2.0") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0pz4yi64wx3vpizv2s99vjbswwm3jbjrqkqizjavkv313nc7baf0")))

(define-public crate-toipe-0.3.1 (c (n "toipe") (v "0.3.1") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1ymr46fsjfq6cx6f7xs82mil44xy7svl2wr3934xl63csg9iipf7")))

(define-public crate-toipe-0.4.0 (c (n "toipe") (v "0.4.0") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0cbcpyz4r6d2yxyd4yk49h668lk4lcx8s4mm0njjrdi0gcvz27c0")))

(define-public crate-toipe-0.4.1 (c (n "toipe") (v "0.4.1") (d (list (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "053fj1bwlq0w7x8ipf8prk5nnl0marfd9d4200klb08560f66ls7")))

(define-public crate-toipe-0.5.0 (c (n "toipe") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "bisection") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.5") (f (quote ("derive" "color" "suggestions"))) (d #t) (k 0)) (d (n "include-flate") (r "^0.1.4") (f (quote ("stable"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0a5269sry6vhx6y655ylbd8br31yvh7qhmh3y8f1dwb2wxwr8l9k")))

