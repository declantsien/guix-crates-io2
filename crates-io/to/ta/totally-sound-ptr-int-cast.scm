(define-module (crates-io to ta totally-sound-ptr-int-cast) #:use-module (crates-io))

(define-public crate-totally-sound-ptr-int-cast-0.1.0 (c (n "totally-sound-ptr-int-cast") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "0q0l6xyih02n1l0xkm7p8jq0nlxiwziaxa335xl4l7hy7wc6sd6v")))

(define-public crate-totally-sound-ptr-int-cast-0.1.1 (c (n "totally-sound-ptr-int-cast") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "1wc7v0pkwk4jbsql51d8m4bpkbhyq7g6gb2i1g5vd6xawfgniw6s")))

