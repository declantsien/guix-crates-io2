(define-module (crates-io to ta total_float_wrap) #:use-module (crates-io))

(define-public crate-total_float_wrap-0.1.0 (c (n "total_float_wrap") (v "0.1.0") (h "13y2lcqr6wagqzc3bz4zqnj7amz3ikibyl7z9qaav0g4g1dfp6ky")))

(define-public crate-total_float_wrap-0.1.1 (c (n "total_float_wrap") (v "0.1.1") (h "1cajsgi5pn6w1fwk67hs7nbqvpf1dfw9q9ckyddpp8vcra4vwnr4")))

