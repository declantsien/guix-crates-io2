(define-module (crates-io to ta totally-safe-transmute) #:use-module (crates-io))

(define-public crate-totally-safe-transmute-0.0.1 (c (n "totally-safe-transmute") (v "0.0.1") (h "1n8fim6myha23gcq0vxwp4nxhakcak340ki5ynlyjm7l0niqy37a")))

(define-public crate-totally-safe-transmute-0.0.2 (c (n "totally-safe-transmute") (v "0.0.2") (h "0yyinvf4r6zc8pv6k57sjjlw2070av8v9vi9pv0yxxqcnxfg7v3s")))

(define-public crate-totally-safe-transmute-0.0.3 (c (n "totally-safe-transmute") (v "0.0.3") (h "0m479s5gwqcxb0dfvbsryx6wqpmfgavgjd50dmrp5w6a72nyic1c")))

(define-public crate-totally-safe-transmute-0.0.4 (c (n "totally-safe-transmute") (v "0.0.4") (h "1a33fi2kv0baqdvihm7a1j0j2n34ncr5yx9ws9n3xsym5pzhpfpr")))

