(define-module (crates-io to ta totally-sync-variable) #:use-module (crates-io))

(define-public crate-totally-sync-variable-0.1.0 (c (n "totally-sync-variable") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k5jaanb09dmc9rw8hdzl6ws8cn2llhk8cnzd39jgd01v9vfpxsa")))

(define-public crate-totally-sync-variable-0.1.1 (c (n "totally-sync-variable") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mld7vc0wldk0a2fd5rksjilixy50hpxzwdax9j79111cq7b1848")))

