(define-module (crates-io to ta total-recall) #:use-module (crates-io))

(define-public crate-total-recall-0.3.0 (c (n "total-recall") (v "0.3.0") (d (list (d (n "eframe") (r "^0.16.0") (d #t) (k 0)) (d (n "egui") (r "^0.16.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.2") (d #t) (k 0)))) (h "10bxffgmq66qq8qfm83k5g636jq6kwjn52fcbf8690rg1n2g9a4p")))

