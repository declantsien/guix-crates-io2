(define-module (crates-io to ta totally-ordered) #:use-module (crates-io))

(define-public crate-totally-ordered-0.1.0 (c (n "totally-ordered") (v "0.1.0") (h "1lhd893ql8ivgq8ksgw6y8zwypcb6d8bhkx1a59mqarffva8391r")))

(define-public crate-totally-ordered-0.1.1 (c (n "totally-ordered") (v "0.1.1") (h "0cvzvdqaf578k9iawidpkf02mq21dkpbhcmhwdx7n7sb5v7fsnkb")))

(define-public crate-totally-ordered-0.2.0 (c (n "totally-ordered") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "08dfg3mz0kv0ws3dl3apvijg5xd5k2ik4p9czlgmlymv93dp3bkg") (f (quote (("std") ("default" "std"))))))

