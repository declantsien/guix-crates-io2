(define-module (crates-io to mb tomba) #:use-module (crates-io))

(define-public crate-tomba-0.1.0 (c (n "tomba") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0s7s0va6jpbjfigqf32lw3fbp3lxsqzjm6pza5lcmg8fhgbd9i0i")))

