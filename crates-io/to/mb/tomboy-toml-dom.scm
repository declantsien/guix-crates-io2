(define-module (crates-io to mb tomboy-toml-dom) #:use-module (crates-io))

(define-public crate-tomboy-toml-dom-0.1.0 (c (n "tomboy-toml-dom") (v "0.1.0") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r3f3h2aw09i5d8n79vs6paqcx8czwj4wyl29mlgjs4dj2mapw81")))

(define-public crate-tomboy-toml-dom-0.1.1 (c (n "tomboy-toml-dom") (v "0.1.1") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fgyijpc2nk5vlhbviy7zixqxyg1bb5ppmh9gvmp7gphdvy70k0h")))

(define-public crate-tomboy-toml-dom-0.1.2 (c (n "tomboy-toml-dom") (v "0.1.2") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mz64vxh63h2n2kq7in1cakpv83ajcd0y5lqz3pvv8537g2nnssm")))

(define-public crate-tomboy-toml-dom-0.1.3 (c (n "tomboy-toml-dom") (v "0.1.3") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vr5b3ln1c2z4b77hrvpzkh1mchsx7gw13ddca6b5ghfy186f61h")))

(define-public crate-tomboy-toml-dom-0.1.4 (c (n "tomboy-toml-dom") (v "0.1.4") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13zggpbm6vclxyvj2zc7fqjnpjlrkbmlc4a8qvkz761kab0jw7fn")))

(define-public crate-tomboy-toml-dom-0.1.5 (c (n "tomboy-toml-dom") (v "0.1.5") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02498zh9bhpi9jrr9sqy32ly8ixg510gx5j747g7saj4d7l5jqmc")))

(define-public crate-tomboy-toml-dom-0.1.6 (c (n "tomboy-toml-dom") (v "0.1.6") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1s3j9qiwdza9mjn689mvcjfngr43d1j040kdcrnvg7pz83nlc7xl")))

(define-public crate-tomboy-toml-dom-0.1.7 (c (n "tomboy-toml-dom") (v "0.1.7") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15g71h5v8c4hlm0y5j87b9g6c2ldf9jarby49dnidg004f3h3z6c")))

(define-public crate-tomboy-toml-dom-0.1.8 (c (n "tomboy-toml-dom") (v "0.1.8") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ainyl45l9fbmrx91lzgz9gbzaq3d4pcr9vd22bbssiji20h9n82")))

(define-public crate-tomboy-toml-dom-0.1.9 (c (n "tomboy-toml-dom") (v "0.1.9") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1d6b5h6qknx5vzdfcd9q1gmamlqymlkj8pll8111z6pwg77bgmp7")))

(define-public crate-tomboy-toml-dom-0.1.10 (c (n "tomboy-toml-dom") (v "0.1.10") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06zh5imnwn8r5ws841qa8krak1n6dql2jyz7f85gsg3xp7xs227s")))

(define-public crate-tomboy-toml-dom-0.1.11 (c (n "tomboy-toml-dom") (v "0.1.11") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gy5fql26lzxcnc2x5hnpkf6832867bxbyxn5fg21p4zvjd2209z")))

(define-public crate-tomboy-toml-dom-0.1.12 (c (n "tomboy-toml-dom") (v "0.1.12") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1vs8skdfiy9mnjr3660gzfp3613w2hqw9kh5hd2042gr3n68kws4")))

(define-public crate-tomboy-toml-dom-0.1.13 (c (n "tomboy-toml-dom") (v "0.1.13") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ws8qxfbfa9g42h0w0cybbbccsqmli0q02z355hsxa7yml95qdnx")))

(define-public crate-tomboy-toml-dom-0.1.14 (c (n "tomboy-toml-dom") (v "0.1.14") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1c9rlsz0hh9v03zily5hh0c6m143x7j4xhhdv3994n9b0g4fw3q9")))

(define-public crate-tomboy-toml-dom-0.1.15 (c (n "tomboy-toml-dom") (v "0.1.15") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gj2dx5a6jb10xnizw0zs5yqnw41c617lh41fjdbgx8ln7224zkr")))

(define-public crate-tomboy-toml-dom-0.1.16 (c (n "tomboy-toml-dom") (v "0.1.16") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "190sc3ih8305a5mhq18w77m88haqy6ij27ly3g2n2h34jk3xvc2b")))

(define-public crate-tomboy-toml-dom-0.1.17 (c (n "tomboy-toml-dom") (v "0.1.17") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qmwfkmxa98v45j0yvpz6xhm4y3zr492b496mgn7h27l9hims8sa")))

(define-public crate-tomboy-toml-dom-0.1.18 (c (n "tomboy-toml-dom") (v "0.1.18") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1xj88j6gm4iz06p3rggd2w62g4vjfffc2m3wba3q115mavwr1r8g")))

(define-public crate-tomboy-toml-dom-0.1.19 (c (n "tomboy-toml-dom") (v "0.1.19") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "05jfmva7cqydmicarv3gi9nlmkiyak91pi8mqfdyryh04admrsdm")))

(define-public crate-tomboy-toml-dom-0.1.20 (c (n "tomboy-toml-dom") (v "0.1.20") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0013azigrywnsv777zrr0vf6zlz351gi8r9pwwr8yvv4bywjmwyn")))

(define-public crate-tomboy-toml-dom-0.1.21 (c (n "tomboy-toml-dom") (v "0.1.21") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1k96h8yljfr1ksl2njbdbm026dssl8i8v07rrc3ah2m3g4s7qxm4")))

