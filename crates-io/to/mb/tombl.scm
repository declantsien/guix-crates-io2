(define-module (crates-io to mb tombl) #:use-module (crates-io))

(define-public crate-tombl-0.2.2 (c (n "tombl") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (f (quote ("indexmap"))) (d #t) (k 0)))) (h "1hja0aw782ld8j117dl2p00cgr762cfrq9m4s83indrrr936fqq1")))

