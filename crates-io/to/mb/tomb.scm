(define-module (crates-io to mb tomb) #:use-module (crates-io))

(define-public crate-tomb-0.1.0 (c (n "tomb") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "13200rjazqbd81sg1bnf2n4k0h9d912k19klchpdyhhv88id954l") (f (quote (("default" "fastrand")))) (s 2) (e (quote (("fastrand" "dep:fastrand"))))))

(define-public crate-tomb-0.2.0 (c (n "tomb") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1v8z1m0jqf1qvv1v0azds8vqk2w2nq2n6hvaschh36qixxvk3xl8") (f (quote (("default" "fastrand")))) (s 2) (e (quote (("fastrand" "dep:fastrand"))))))

