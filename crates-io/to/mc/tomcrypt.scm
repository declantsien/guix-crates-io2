(define-module (crates-io to mc tomcrypt) #:use-module (crates-io))

(define-public crate-tomcrypt-0.1.0 (c (n "tomcrypt") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "tomcrypt-sys") (r "^0.1") (d #t) (k 0)))) (h "041yjmilgqsxpiic0glqfk1l222akzj49qcd1v4p8mnpkppf2ra8")))

