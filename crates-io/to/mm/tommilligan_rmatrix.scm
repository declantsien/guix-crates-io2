(define-module (crates-io to mm tommilligan_rmatrix) #:use-module (crates-io))

(define-public crate-tommilligan_rmatrix-0.0.1 (c (n "tommilligan_rmatrix") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.3.3") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "16v4w7bwi1i7lalzhkyyni2jak20d7b6s7knr5g5149hs4lixvm7")))

