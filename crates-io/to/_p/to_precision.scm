(define-module (crates-io to _p to_precision) #:use-module (crates-io))

(define-public crate-to_precision-0.1.0 (c (n "to_precision") (v "0.1.0") (h "00rzykh5yb847cv0y27k03lc8a2g2sw89mbr570vpbf8khmz708i")))

(define-public crate-to_precision-0.1.1 (c (n "to_precision") (v "0.1.1") (h "0nxil6fjyjbanvnjikvdy42x77gdcgrlglad4kr2r0hc67h7rj4g")))

(define-public crate-to_precision-0.1.2 (c (n "to_precision") (v "0.1.2") (h "19jsmks4bm079v2ijmaf4kyv6680kxl12pn9ai4vlvxm960igv26")))

