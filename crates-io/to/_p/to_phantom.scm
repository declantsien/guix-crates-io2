(define-module (crates-io to _p to_phantom) #:use-module (crates-io))

(define-public crate-to_phantom-0.1.0 (c (n "to_phantom") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1pp54wckhng96vvrs3fgbv8jr3la6pfy8ffvbidmj6qxmcsmy6jd")))

