(define-module (crates-io to on toonify-cli) #:use-module (crates-io))

(define-public crate-toonify-cli-1.0.0 (c (n "toonify-cli") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toonify") (r "^1.0.0") (d #t) (k 0)))) (h "1n22mlrfsqx4dd48snbvil8gz0fghnn7m3d1h2zyh3i4qnbw4vfp")))

