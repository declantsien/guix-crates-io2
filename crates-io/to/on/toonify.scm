(define-module (crates-io to on toonify) #:use-module (crates-io))

(define-public crate-toonify-1.0.0 (c (n "toonify") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "multipart"))) (d #t) (k 0)))) (h "1k9ijr3qj8q596jmqwb0ff310pn3nq51238mrshi3hrhazgphn9r")))

