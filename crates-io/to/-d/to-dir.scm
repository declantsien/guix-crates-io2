(define-module (crates-io to -d to-dir) #:use-module (crates-io))

(define-public crate-to-dir-0.2.0 (c (n "to-dir") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0ndrd50jyzi2b48shpjy574lnya7g2hdm379gkm4fz7lfvmx04b9")))

(define-public crate-to-dir-0.2.1 (c (n "to-dir") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "06apc63i21x73zh2jdp1cr9i11b7ng6n30rfnbxx3wn194vrdmck")))

