(define-module (crates-io to -d to-directory) #:use-module (crates-io))

(define-public crate-to-directory-0.1.0 (c (n "to-directory") (v "0.1.0") (d (list (d (n "bincode") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "143r4bkx5662bbyr10h5ak395w3rn1z76nj6ihm1dq3ifyszfflp")))

