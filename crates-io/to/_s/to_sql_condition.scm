(define-module (crates-io to _s to_sql_condition) #:use-module (crates-io))

(define-public crate-to_sql_condition-0.1.0 (c (n "to_sql_condition") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18hmpd2i5sqfzqvbinl1ps3h1y98miayv9f19nix65pvh4y94pi7") (y #t)))

(define-public crate-to_sql_condition-0.1.1 (c (n "to_sql_condition") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zbwnd75nqnxy0l20l5awk82z64mlsxqdwvj2dzy95cl28sk91xs")))

(define-public crate-to_sql_condition-0.1.2 (c (n "to_sql_condition") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h3w4l9cas3k9bsw5hvl5kd0rkwhrzgbz6906sn02ppqgj9maakf")))

(define-public crate-to_sql_condition-0.1.3 (c (n "to_sql_condition") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12picgi7g315s03pjnfm40rl8gyh2gbxpyl7sd895d19r15j25r3")))

(define-public crate-to_sql_condition-0.1.4 (c (n "to_sql_condition") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mwv5bs9rg4z3l9rh1cbng74y1sxjf4as5pl3hbbvzz4n5xlq7d7")))

(define-public crate-to_sql_condition-0.2.1 (c (n "to_sql_condition") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m4mq07vflxi3dn6r9231ral5p4fgq3sfccvj3jsl3v20myiiqdr")))

