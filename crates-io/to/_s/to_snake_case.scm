(define-module (crates-io to _s to_snake_case) #:use-module (crates-io))

(define-public crate-to_snake_case-0.1.0 (c (n "to_snake_case") (v "0.1.0") (h "1wdqf2n567x13hn1hdv6gs3scv6i5br67a3345lw8vbv2rah29ij")))

(define-public crate-to_snake_case-0.1.1 (c (n "to_snake_case") (v "0.1.1") (h "1bc7x87v8v7sd2c8rbaqnj3g2hpdk0236wpjq0g6ywb7ybc360l9")))

