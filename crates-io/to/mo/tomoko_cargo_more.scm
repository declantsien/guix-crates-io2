(define-module (crates-io to mo tomoko_cargo_more) #:use-module (crates-io))

(define-public crate-tomoko_cargo_more-0.1.0 (c (n "tomoko_cargo_more") (v "0.1.0") (h "02q08yzk3z11s8kwk0svfndwd5gldb1i398p5hss6f9idf631a4m")))

(define-public crate-tomoko_cargo_more-0.3.0 (c (n "tomoko_cargo_more") (v "0.3.0") (h "1wrawyy7dzsz60qklzby5rlyschjvns7d4navh15944axjqwh6ap")))

