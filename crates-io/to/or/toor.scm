(define-module (crates-io to or toor) #:use-module (crates-io))

(define-public crate-toor-0.1.0 (c (n "toor") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0432rwlws2ljkhhl20b0z83aiaysa63wg9siq7jcfdgmr23pb72p") (r "1.73.0")))

(define-public crate-toor-0.2.0 (c (n "toor") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14i9m9nc20d5117yjs7mpq3x38j4hp04sqfrnh3klm459hhj8rig") (r "1.73.0")))

(define-public crate-toor-0.3.0 (c (n "toor") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "miette") (r "^5.10") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k37sia47429m35l52743mp4lj21fk2rn649yqjk0wpqshwln9w7") (r "1.73.0")))

