(define-module (crates-io to ws tows) #:use-module (crates-io))

(define-public crate-tows-1.0.0 (c (n "tows") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0rs0zfn1y12mjbrmjcb0rry7rxn5d5fanl8n4l4zys3szy7jx5gr")))

(define-public crate-tows-1.0.1 (c (n "tows") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0427mhbw8si0znz2nnbwfapvh7q8bslxivpj4z1mmnycinxixz69")))

(define-public crate-tows-1.0.2 (c (n "tows") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "097d1vcwnvlc5a0mi5yixcdj4hpw73sar1dxib17sfdm59pi8lv0")))

