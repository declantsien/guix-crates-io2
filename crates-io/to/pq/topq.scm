(define-module (crates-io to pq topq) #:use-module (crates-io))

(define-public crate-topq-0.1.0 (c (n "topq") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "013vilnxkf3f9rnlx1ffib6nmi05avjknz1kjy5spw7b6zzjghjr")))

(define-public crate-topq-0.2.0 (c (n "topq") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)))) (h "1z739m2dwbj8nzfkin5wniwwz1nwmha4llgra5vvx1ha38w8b1dz")))

