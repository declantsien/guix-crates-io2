(define-module (crates-io to r_ tor_proxy) #:use-module (crates-io))

(define-public crate-tor_proxy-0.1.0 (c (n "tor_proxy") (v "0.1.0") (h "1658fsqhkq829h6f78nz3344lvx68c17hn75vzq2jwxwnqjl47s4")))

(define-public crate-tor_proxy-0.1.2 (c (n "tor_proxy") (v "0.1.2") (h "0yqvpbv2w3sj8mf28sbsqz7a2gcmnjba47ncb2j113ikvl6vap8d")))

