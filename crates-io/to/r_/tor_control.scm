(define-module (crates-io to r_ tor_control) #:use-module (crates-io))

(define-public crate-tor_control-0.2.0 (c (n "tor_control") (v "0.2.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)))) (h "0avl9lyw5hwbk223b7jmfs5ip8fiyh7cccjc2xfg480n86sn9xgp")))

