(define-module (crates-io to r_ tor_client) #:use-module (crates-io))

(define-public crate-tor_client-0.0.1 (c (n "tor_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qh50gdi32ln70zapbcgk46kn0284w7gwzwa735cyzm44fzyskx5")))

(define-public crate-tor_client-0.0.2 (c (n "tor_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "1fjvyd6z2dpn5ynv1pb3gv6l8s4qd92gll2jdqx5bbhj0ly1f5k3")))

(define-public crate-tor_client-0.0.3 (c (n "tor_client") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0xdapdrl28vv2xysyv5v3h3zi1g86s4dx8jyw2wg98x58d4f39nr")))

(define-public crate-tor_client-0.0.4 (c (n "tor_client") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1vbkhb7vdglnwdc11fbfb0n1a2122pnfm6jrajwb3yvj5mhlx0y5")))

(define-public crate-tor_client-0.0.5 (c (n "tor_client") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1y7j7qwxgfpz8s9pzwd43zr703qgab3fckki90bgj7fiw94yfgmw")))

(define-public crate-tor_client-0.0.6 (c (n "tor_client") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0didi4dgy01m0hh368mmhi646vr8i48gx5761xmbl8xmsb25n24g")))

(define-public crate-tor_client-0.0.7 (c (n "tor_client") (v "0.0.7") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.41") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0fy6v2igz73xijpq51zm3ldxkmxhp6gsxx70ws34ahb1rd9gxikl")))

(define-public crate-tor_client-0.0.8 (c (n "tor_client") (v "0.0.8") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "02cgawdm2xbapcgzfw9gzv6gjkl1z2iclf7bnwcf50m5sn0vadv3")))

(define-public crate-tor_client-0.0.10 (c (n "tor_client") (v "0.0.10") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0dmqn5drqdl6hljlrc4fdy66l0yprdw4h3zxyfig6ffxyifwkp08")))

(define-public crate-tor_client-0.0.11 (c (n "tor_client") (v "0.0.11") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)) (d (n "socks") (r "^0.2.3") (d #t) (k 0)))) (h "09941j2crrc3p5qxd1f3s9pcydasm2mqsnr8vlysk98s7jmnsfk2")))

