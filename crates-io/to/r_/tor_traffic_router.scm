(define-module (crates-io to r_ tor_traffic_router) #:use-module (crates-io))

(define-public crate-Tor_Traffic_Router-0.1.0 (c (n "Tor_Traffic_Router") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("socks"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (k 0)))) (h "18c6v443b14g4y60i1axqzz85disn8vakpxr9idn9xhx9jcybyfd")))

(define-public crate-Tor_Traffic_Router-0.1.1 (c (n "Tor_Traffic_Router") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("socks"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (k 0)))) (h "0gb0ldig862a2cz3xj93vcr4zh4jac1w1svpp4hl01c7hjc8m14l")))

(define-public crate-Tor_Traffic_Router-0.1.2 (c (n "Tor_Traffic_Router") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("socks"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (k 0)))) (h "0v5ghz9yj8fji5f6h4lfw5dc3m7x4yz40wfwxcw402d4pakh6bf4")))

(define-public crate-Tor_Traffic_Router-0.1.4 (c (n "Tor_Traffic_Router") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("socks"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (k 0)))) (h "1pm6rhclfhhwm5p18wmhy4hkym87wxajk09ca90i3qqhflqri3y2")))

(define-public crate-Tor_Traffic_Router-0.2.0 (c (n "Tor_Traffic_Router") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("socks"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (k 0)))) (h "1p6w9b7hnjl5jpaplxybd1mwg2awjccdapnqdsirmgh8427b6c1m")))

(define-public crate-Tor_Traffic_Router-0.3.0 (c (n "Tor_Traffic_Router") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("socks"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (k 0)))) (h "14mz37mp8haxkfc021zvr7jzb5293hiskhi3b1irz0iqp9bbykxq")))

