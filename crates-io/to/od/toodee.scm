(define-module (crates-io to od toodee) #:use-module (crates-io))

(define-public crate-toodee-0.1.0 (c (n "toodee") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1riyv0d26v9cazaf2hi4ksy34bzi6nkazlvbnrgwbvdwhp41n982") (f (quote (("translate") ("sort") ("default" "translate" "sort"))))))

(define-public crate-toodee-0.1.1 (c (n "toodee") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0mfr6syh1g56brkcc94fpzkvjpi7inrxl83jkicsgb1xm2pd980q") (f (quote (("translate") ("sort") ("default" "translate" "sort"))))))

(define-public crate-toodee-0.1.2 (c (n "toodee") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0y8n9r9mjk1hjxcrkbcijzlzcgp2spksjd5b4nwgm75zfmmh00kl") (f (quote (("translate") ("sort") ("default" "translate" "sort"))))))

(define-public crate-toodee-0.1.3 (c (n "toodee") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1jq40jxvwclcw07jgdxgh3b3g5vcfh94686mm1mxx82595yik8cg") (f (quote (("translate") ("sort") ("default" "translate" "sort"))))))

(define-public crate-toodee-0.1.4 (c (n "toodee") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "07wv6g3v4yi7ciqh0zs8ba0hnxf20kwkj1r4h9gc2d4584cy4nmg") (f (quote (("translate") ("sort") ("default" "translate" "sort"))))))

(define-public crate-toodee-0.2.0 (c (n "toodee") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0px59f4j05cx7k1dbhl3kwxilhh83g751i8sm79yd46n3zk6l63p") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.2.1 (c (n "toodee") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1gkr882rh02iqnn8015gk4pvwfjaxjfr346c3yh0x3i47x5i5azk") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.2.2 (c (n "toodee") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0l2pg10h6j0kcwafaqc8vmrz8y52jc4rgdiiwlpg3ps9xw7h6wn1") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.2.3 (c (n "toodee") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "04idjws8w8y2s8v06119kf7mcwr6z13lxh07kz1rd30ndvz920i1") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.2.4 (c (n "toodee") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "02dczfwhic3jjkfwiz6j0dw7pw9cib2cyxir8dsdzv56w5zfhkz2") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.3.0 (c (n "toodee") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "19c0p0yqzwncsw1g5gbsd1qnnigh0shgvxh9z3mi5vg6wi9pymzj") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.4.0 (c (n "toodee") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0q273d8ah2syzd75521xim5gx3imglq3z878jph29y3zn3hqw7bv") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.4.1 (c (n "toodee") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19b5kzzrzf9lhm9w3p89l1ad1p6v1d553rxblgdw97bag62b2s64") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy") ("copy"))))))

(define-public crate-toodee-0.4.2 (c (n "toodee") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.181") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "1ja6g15r3d79g219x9mn1lfk1m6s4ydwjzp39zpr5xl83cw2j2y5") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy" "serde") ("copy")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-toodee-0.5.0 (c (n "toodee") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "grid") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.181") (f (quote ("derive" "alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "14pyl6llr6pw1zs2b5a3avfbm3ipih7b0nbx0hsb7dbw7f0xgjv7") (f (quote (("translate") ("sort") ("default" "translate" "sort" "copy" "serde") ("copy")))) (s 2) (e (quote (("serde" "dep:serde"))))))

