(define-module (crates-io to me tome) #:use-module (crates-io))

(define-public crate-tome-0.1.0 (c (n "tome") (v "0.1.0") (h "0qayvan5cgb2g3pllhk4ifpqimw57w96vhadqd4cmlsbf7nng71p") (y #t)))

(define-public crate-tome-0.0.1 (c (n "tome") (v "0.0.1") (h "0k0v2c7rr8cz68rcbns2g4rrq5a3khvnizf91g6lhbsnb7v8j4f6") (y #t)))

