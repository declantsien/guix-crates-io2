(define-module (crates-io to me tometrics) #:use-module (crates-io))

(define-public crate-tometrics-0.1.0 (c (n "tometrics") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xh6pzglvb1anwc5qyr7k6fhhbn2kyl66js664m4x63h1l1vq4k6")))

(define-public crate-tometrics-0.1.1 (c (n "tometrics") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dwgnpz05ccl0dp0r85xl7yy9a9wq4m2c9cr2dhi94650xx9ng8q")))

(define-public crate-tometrics-0.1.2 (c (n "tometrics") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wn6fdfxgb3cwmci2bracy70cq1l70v9mdjlg8qpnhn91n1ixhax")))

