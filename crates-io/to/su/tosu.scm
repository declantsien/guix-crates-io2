(define-module (crates-io to su tosu) #:use-module (crates-io))

(define-public crate-tosu-0.1.0 (c (n "tosu") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)))) (h "0wjw4hrbqakrhwsi0k3jh5d5yrdp1408gfb4zn6am2nppi60wjbf")))

