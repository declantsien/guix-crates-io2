(define-module (crates-io to dc todc-mem) #:use-module (crates-io))

(define-public crate-todc-mem-0.1.0 (c (n "todc-mem") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "shuttle") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "shuttle") (r "^0.6") (d #t) (k 2)))) (h "1z3lc27d0xsyblnk5ya1x570al9wj17nqav0pzck7m1vcqbzcx81") (s 2) (e (quote (("shuttle" "dep:shuttle"))))))

