(define-module (crates-io to _b to_bopomofo) #:use-module (crates-io))

(define-public crate-to_bopomofo-0.1.0 (c (n "to_bopomofo") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ya8ds4lx7y96kp3gyn6ry6b8jn2mqcgcx2ly8bwhn7i7nlm50m7")))

(define-public crate-to_bopomofo-0.1.1 (c (n "to_bopomofo") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0hfp5fzwb7dxckxl50ljzh8m82g53wy9nkm8x05iim461nd169nn")))

(define-public crate-to_bopomofo-0.1.2 (c (n "to_bopomofo") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0q5j0wsn3q210zvs76ww6594kk4glg9bw6ppgbg5czlx0kj6x87v")))

