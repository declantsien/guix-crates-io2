(define-module (crates-io to _b to_bytes_derive) #:use-module (crates-io))

(define-public crate-to_bytes_derive-0.1.0 (c (n "to_bytes_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nrm8vgi79d9i2v664f5jf4vyqh73ngn9vjaidh9va5xmpcgcv9x")))

