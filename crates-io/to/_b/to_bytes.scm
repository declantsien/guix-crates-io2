(define-module (crates-io to _b to_bytes) #:use-module (crates-io))

(define-public crate-to_bytes-0.1.0 (c (n "to_bytes") (v "0.1.0") (d (list (d (n "to_bytes_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "09p85jng20agi38z0mmlfxjlll542bxy314d8qv6skzm94il4g88") (f (quote (("derive" "to_bytes_derive") ("default" "derive"))))))

