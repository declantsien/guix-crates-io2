(define-module (crates-io to lk tolk) #:use-module (crates-io))

(define-public crate-tolk-0.1.0 (c (n "tolk") (v "0.1.0") (d (list (d (n "tolk-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1ynyqg9105rmg68jna1jys238dxfib9vgi4xs0ddg70xj17xdfpi")))

(define-public crate-tolk-0.1.1 (c (n "tolk") (v "0.1.1") (d (list (d (n "tolk-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1k4b9lbvmclliqwn0y22xb9rpb52wip2ln3dc353dskicmxmvapq")))

(define-public crate-tolk-0.2.0 (c (n "tolk") (v "0.2.0") (d (list (d (n "tolk-sys") (r "^0.2") (d #t) (k 0)))) (h "17nilalx4h1hdrc4zlnqfr0jsxjpjwkrxs37j8z8fp7piqycdf1p")))

(define-public crate-tolk-0.2.1 (c (n "tolk") (v "0.2.1") (d (list (d (n "tolk-sys") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1fzbdf0cyhii7lvnm88ny72kx7h6shg4hjk9h9rx1p90agbi87ci")))

(define-public crate-tolk-0.3.0 (c (n "tolk") (v "0.3.0") (d (list (d (n "tolk-sys") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0fbyp1w5cjzscbrzpj7sa63g6si9wpr7vj0y2762k3bacclgx45n")))

(define-public crate-tolk-0.4.0 (c (n "tolk") (v "0.4.0") (d (list (d (n "tolk-sys") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1jxp568a3y8qrr9l8z3mjfqr9f8skyz5g73z67z0rbzapfz66qgg")))

(define-public crate-tolk-0.5.0 (c (n "tolk") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tolk-sys") (r "^0.2") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1ih09v2jc15k6vlikz20b4sc6nzfs1d56fg7rs87irim8x632ndk")))

