(define-module (crates-io to lk tolk-sys) #:use-module (crates-io))

(define-public crate-tolk-sys-0.1.0 (c (n "tolk-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0skvhs8haiswxj8s46fcdvpa1qwlcri09qfsmq6qa37122bksvis")))

(define-public crate-tolk-sys-0.2.0 (c (n "tolk-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "12j7afivl73kn573aphf6rvyjw4dmc7sw8xw2b3apz3fm5sajxkm")))

(define-public crate-tolk-sys-0.2.1 (c (n "tolk-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1nj0cp267zik5xb3afx7zn7r4677ll3p2736790rzmp6rsrn5lig")))

(define-public crate-tolk-sys-0.2.2 (c (n "tolk-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "0xjc9r0588jbjl1raxp8f4mp18b5898f7yrgv621zfibwxdd6285")))

