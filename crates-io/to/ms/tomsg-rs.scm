(define-module (crates-io to ms tomsg-rs) #:use-module (crates-io))

(define-public crate-tomsg-rs-0.1.0 (c (n "tomsg-rs") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("io-util" "sync" "net"))) (d #t) (k 0)))) (h "1v9k5x9ww2r4z1m8g6v1qwjaq8kcmz1qsi3xhlqnin9m3lfd3r6w")))

(define-public crate-tomsg-rs-0.2.0 (c (n "tomsg-rs") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("io-util" "sync" "net"))) (d #t) (k 0)))) (h "16w7wfbx3n91g1w6rh3bh19dsgawfc2gyrhq94xxj2gb0w5kn4gq")))

(define-public crate-tomsg-rs-0.3.0 (c (n "tomsg-rs") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("io-util" "sync" "net" "rt"))) (d #t) (k 0)))) (h "17q7ci94xjmnxzj2bpa9bl18fhi3hd0lgssjli8liw3n78qmiysl")))

(define-public crate-tomsg-rs-0.4.0 (c (n "tomsg-rs") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("io-util" "sync" "net" "rt"))) (d #t) (k 0)))) (h "1jkc8sfz2xnrrj0brpzri4xvx9g1raqbpmnk82xj9fdlydhmpyfy")))

(define-public crate-tomsg-rs-0.5.0 (c (n "tomsg-rs") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("io-util" "sync" "net" "rt"))) (d #t) (k 0)))) (h "19z6irvzzccyh3fndf4w73vgcfhvzazqaf1hmw3hq648czj12x6b")))

(define-public crate-tomsg-rs-0.6.0 (c (n "tomsg-rs") (v "0.6.0") (d (list (d (n "tokio") (r "^1.5") (f (quote ("io-util" "sync" "net" "rt"))) (d #t) (k 0)))) (h "0ry9whphhlfzzk09mmwyd8qa0mxr9fmh5nqhbyg4d1bxxfb5xq6s")))

