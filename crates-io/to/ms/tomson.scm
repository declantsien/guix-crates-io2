(define-module (crates-io to ms tomson) #:use-module (crates-io))

(define-public crate-tomson-0.1.0 (c (n "tomson") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.6") (d #t) (k 0)) (d (n "toml") (r "^0.1.20") (d #t) (k 0)))) (h "0zyvh0456msrv0ig1j6prj3hhb9xf0dfdqipm1g41yf7vmg5hk34")))

(define-public crate-tomson-0.1.1 (c (n "tomson") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.1.20") (d #t) (k 0)))) (h "0m0aplw825pwqvg3lv5qj7lnrwc0bi3sviysv19fdlbr48wywn30")))

