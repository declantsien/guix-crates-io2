(define-module (crates-io to -s to-socket-addrs) #:use-module (crates-io))

(define-public crate-to-socket-addrs-0.1.0 (c (n "to-socket-addrs") (v "0.1.0") (h "1ib6msj6dp0dvy1ymkgajj3yh2xdf08xpzp52jjfgdwy2fbgf7da")))

(define-public crate-to-socket-addrs-0.2.0 (c (n "to-socket-addrs") (v "0.2.0") (d (list (d (n "async-attributes") (r "^1.1.2") (d #t) (k 2)) (d (n "async-std") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2") (f (quote ("no-debug"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("net" "rt" "macros"))) (o #t) (d #t) (k 0)))) (h "1d3g95bpkcjs2i2dsz5s6jb6h1zg50r5mm0x4l0qx3gjb8lca8y1") (f (quote (("test_dns_ipv6") ("sync") ("default" "sync")))) (y #t) (s 2) (e (quote (("tokio" "dep:tokio") ("async" "dep:async-std"))))))

(define-public crate-to-socket-addrs-0.2.1 (c (n "to-socket-addrs") (v "0.2.1") (d (list (d (n "async-attributes") (r "^1.1.2") (d #t) (k 2)) (d (n "async-std") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2") (f (quote ("no-debug"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("net" "rt" "macros"))) (o #t) (d #t) (k 0)))) (h "0rj3mirm8l6ak9m5m0dvnpbljl4a1s4bv9irdwhpzm6hf3fb9pg2") (f (quote (("test_dns_ipv6") ("sync") ("default" "sync")))) (s 2) (e (quote (("tokio" "dep:tokio") ("async" "dep:async-std"))))))

