(define-module (crates-io to -s to-syn-value) #:use-module (crates-io))

(define-public crate-to-syn-value-0.1.0 (c (n "to-syn-value") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.88") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "to-syn-value_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0iw4wqhpc3axcal3zxzdccvg9sn6jr1738sasyyr6iwa22sbgp25")))

(define-public crate-to-syn-value-0.1.1 (c (n "to-syn-value") (v "0.1.1") (d (list (d (n "syn") (r "^2.0.32") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "to-syn-value_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1vc2bk044kccfnyy0qs5wgsppc2416g7qrc9hr73pv7a5i7nik6z")))

