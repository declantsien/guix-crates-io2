(define-module (crates-io to -s to-syn-error) #:use-module (crates-io))

(define-public crate-to-syn-error-1.0.0 (c (n "to-syn-error") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "to-syn-error-derive") (r "^1.0") (d #t) (k 0)))) (h "0754g08hnn9a1jqcv35pdjxcs3d638kk9p9a1z71vifiklzz55s2")))

(define-public crate-to-syn-error-1.0.1 (c (n "to-syn-error") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "to-syn-error-derive") (r "^1.0") (d #t) (k 0)))) (h "0kw5rvgpzhs64iznvr0q5pb291caap2x3sz1lr07x296gxqagx9g")))

