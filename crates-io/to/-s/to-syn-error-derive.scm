(define-module (crates-io to -s to-syn-error-derive) #:use-module (crates-io))

(define-public crate-to-syn-error-derive-1.0.0 (c (n "to-syn-error-derive") (v "1.0.0") (d (list (d (n "assert-parse") (r "^1.0") (d #t) (k 2)) (d (n "assert-parse-register-assert-macro") (r "^1.0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h71rwd6rhwy58z7a1b0japl5cypqs0wpqsyzc99wslqfph07rxm")))

