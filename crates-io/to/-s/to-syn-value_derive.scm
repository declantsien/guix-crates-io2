(define-module (crates-io to -s to-syn-value_derive) #:use-module (crates-io))

(define-public crate-to-syn-value_derive-0.1.0 (c (n "to-syn-value_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.88") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1g6crhgkvdpgrxd8lnnmdi9kqqjjld39s73jsg0nid81vv3dwkyd")))

(define-public crate-to-syn-value_derive-0.1.1 (c (n "to-syn-value_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "04ljvhsja99xgj9zxn8gbx2cb2sfy7fl5c7k9wa4716yg2kzvzrx")))

