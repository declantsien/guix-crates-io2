(define-module (crates-io to xe toxearley) #:use-module (crates-io))

(define-public crate-toxearley-0.0.3 (c (n "toxearley") (v "0.0.3") (d (list (d (n "lexers") (r "^0.0.4") (d #t) (k 0)) (d (n "linenoise-rust") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "0ipa3nbx79nkyx7c0q3whc9rpmkyrqnpaxx4przgvij2qpky0qs5")))

