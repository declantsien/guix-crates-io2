(define-module (crates-io to ru torus) #:use-module (crates-io))

(define-public crate-torus-0.1.0 (c (n "torus") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "cmdline-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "linefeed") (r "~0.2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "rusqlite") (r "^0.11.0") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0jpnvmab2bhzbrj6wy5pdgq8vgfhvk4k7jlhnp324cjzy1z2sclx")))

(define-public crate-torus-0.1.1 (c (n "torus") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "cmdline-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "linefeed") (r "~0.2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "rusqlite") (r "^0.11.0") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1ahvw1jyv6s34hah7yzx4s6m8bv42l641gzadvcqy8na535s533m")))

