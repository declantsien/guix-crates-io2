(define-module (crates-io to ss tosserror-derive) #:use-module (crates-io))

(define-public crate-tosserror-derive-0.1.1 (c (n "tosserror-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "091bcsc2xwrkq5mnnprbfi7lfqizdmf896k2nqjl6fjbv0sp67c2") (f (quote (("thiserror") ("default")))) (r "1.56")))

(define-public crate-tosserror-derive-0.1.2 (c (n "tosserror-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1clm38z6p6cd16c9wf56sphm2l5gcq7x0plpqc6w0yrp37nzfm6s") (f (quote (("thiserror") ("default")))) (r "1.56")))

