(define-module (crates-io to ss tosspay) #:use-module (crates-io))

(define-public crate-tosspay-0.1.0 (c (n "tosspay") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1b0j9fy3sdkq3z5r9x7rdnzmzccsz1bcnyn8wr6hqp2mcw3dahh3")))

