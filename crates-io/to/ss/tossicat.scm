(define-module (crates-io to ss tossicat) #:use-module (crates-io))

(define-public crate-tossicat-0.1.0 (c (n "tossicat") (v "0.1.0") (h "0f55bjnrs8s61bkprpzcsdvlg1v977rd8hfavb9cajj2657kfv7g")))

(define-public crate-tossicat-0.2.0 (c (n "tossicat") (v "0.2.0") (h "1s2ffqf09airdvc3yh7p4z1xzy79x7ysm1ki0n5r9di0rclvqfxc")))

(define-public crate-tossicat-0.3.0 (c (n "tossicat") (v "0.3.0") (h "0mqpj3nswx0614l7in7jzwcbhjswcbxpyvpna5zvw1wgfxdw7408")))

(define-public crate-tossicat-0.3.1 (c (n "tossicat") (v "0.3.1") (h "0xg4caabd5axav04q309zhhdvjdxzdi478mn2nlbrkih7qhlyjiz")))

(define-public crate-tossicat-0.3.2 (c (n "tossicat") (v "0.3.2") (h "1xpk8fl376j9igkhmn441bwm5s022pjvl3r2nzmalrw4spwc06di")))

(define-public crate-tossicat-0.4.1 (c (n "tossicat") (v "0.4.1") (h "1afw7r48bzxq0fddn6zmhqmj7p0y0ihns8hp75jnx0v40lfwn9f7")))

(define-public crate-tossicat-0.4.2 (c (n "tossicat") (v "0.4.2") (h "05jgl9apx14b8a5p11swxnyhyysc284az72xqjiqi18vks5v6pj9")))

(define-public crate-tossicat-0.4.3 (c (n "tossicat") (v "0.4.3") (h "083m0kwzyfxkxakf9krgsrqkpsnvljhvdbg2zjzg1yqb11ma5xiz")))

(define-public crate-tossicat-0.4.4 (c (n "tossicat") (v "0.4.4") (h "0bb6mq4wzj7s7bz027s0ybw6qrhls2nax7myghv5rayvgkn5yg1w")))

(define-public crate-tossicat-0.4.5 (c (n "tossicat") (v "0.4.5") (h "13rdsgcjcdapr8pjf9h2drax2arlskf85s10h0lywk6gpd3m9920")))

(define-public crate-tossicat-0.4.6 (c (n "tossicat") (v "0.4.6") (h "0mjar9d5b46pdq0r21gfz54vg4fkry54yp6lrji110hcf3j5kixa")))

(define-public crate-tossicat-0.4.7 (c (n "tossicat") (v "0.4.7") (h "12yqj2373cbs5qzbvx3ffdk4khwr60ybq4dd7d3yhg7yqkv85r7z")))

(define-public crate-tossicat-0.5.0 (c (n "tossicat") (v "0.5.0") (h "0zhixg896c7nzh1wsmfm4sss1g7q0h2dpwz4rzr17na4ygsmx7if")))

(define-public crate-tossicat-0.5.1 (c (n "tossicat") (v "0.5.1") (h "0msic98ly7rsgbgl2xy09pq26y5fpphyd0iaimhal4mg6vn1l3h6")))

(define-public crate-tossicat-0.6.0 (c (n "tossicat") (v "0.6.0") (h "14hi5gar0sldw51gjac2j68603m6pdabdvz5khv769m0yi0clbwh")))

