(define-module (crates-io to ss tosserror) #:use-module (crates-io))

(define-public crate-tosserror-0.1.0 (c (n "tosserror") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "0542z9prqk2vs9b385dzbvh7znb24y48ws308m6cahqp07qsbvdi") (r "1.56")))

(define-public crate-tosserror-0.1.1 (c (n "tosserror") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tosserror-derive") (r "=0.1.1") (d #t) (k 0)))) (h "1cba134h1c4fa933mwvy7nfd2pb43m5i7lbfms80gmbb91xjqbkz") (f (quote (("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "tosserror-derive/thiserror")))) (r "1.56")))

(define-public crate-tosserror-0.1.2 (c (n "tosserror") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tosserror-derive") (r "=0.1.2") (d #t) (k 0)))) (h "00lbaammsqzz7zr82fayqz2q29i4c1inw8ib3lf05siw5mwnbrwj") (f (quote (("default")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "tosserror-derive/thiserror")))) (r "1.56")))

