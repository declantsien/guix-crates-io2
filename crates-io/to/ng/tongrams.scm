(define-module (crates-io to ng tongrams) #:use-module (crates-io))

(define-public crate-tongrams-0.1.0 (c (n "tongrams") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sucds") (r "^0.1.8") (f (quote ("intrinsics"))) (d #t) (k 0)) (d (n "yada") (r "^0.5.0") (d #t) (k 0)))) (h "1qmr0l8g6hymg3a77h8xdqb2hf1hiibp07ajyjrzpbffz0zw6z2a")))

(define-public crate-tongrams-0.1.1 (c (n "tongrams") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sucds") (r "^0.1.8") (f (quote ("intrinsics"))) (d #t) (k 0)) (d (n "yada") (r "^0.5.0") (d #t) (k 0)))) (h "00ls13dws7sbabml8db7svvhwshrymfskxzf34gkac486yzrh3d9")))

(define-public crate-tongrams-0.1.2 (c (n "tongrams") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sucds") (r "^0.1.8") (f (quote ("intrinsics"))) (d #t) (k 0)) (d (n "yada") (r "^0.5.0") (d #t) (k 0)))) (h "0am3xvvsgx6115h58krbbpwjppg0i8yg2iiypix307ffls7mhh2w")))

(define-public crate-tongrams-0.1.3 (c (n "tongrams") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sucds") (r "^0.2.3") (f (quote ("intrinsics"))) (d #t) (k 0)) (d (n "yada") (r "^0.5.0") (d #t) (k 0)))) (h "15d9zgpcgjqab8wylrp77x7rjyzp5f0jhify89l295qhk3sm7r5v")))

(define-public crate-tongrams-0.1.4 (c (n "tongrams") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sucds") (r "^0.2.4") (f (quote ("intrinsics"))) (d #t) (k 0)) (d (n "yada") (r "^0.5.0") (d #t) (k 0)))) (h "1qalf9wgrw13m5cimmm7zsfxcvcq01fvqg9zghghy976bbx3idnb")))

