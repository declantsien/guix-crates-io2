(define-module (crates-io to ng tongue) #:use-module (crates-io))

(define-public crate-tongue-0.0.1 (c (n "tongue") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nszwcpds75p3dm8avbwfpds5d1pmp1q1bmk14c5mjf69x5hp5xq")))

(define-public crate-tongue-0.0.2 (c (n "tongue") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dprh49n66xdxacznm9nmp4a7mrsn2bj4vbjp0yivlcwc3sq6nhk")))

(define-public crate-tongue-0.0.3 (c (n "tongue") (v "0.0.3") (h "00dc01w5ff78smi6fjf2x937w2p3yb4lhxhkjbcf6v2hss12x9vi")))

(define-public crate-tongue-0.0.4 (c (n "tongue") (v "0.0.4") (h "0217mkb1720p5xx8sm1ky7rqvc0ajnry49z8fx98nvx0ifm2y1f3")))

(define-public crate-tongue-0.0.5 (c (n "tongue") (v "0.0.5") (h "0zgqzhf6r7zlb0pjvyfxsm6cjx5fmz520jwg2l5rqkxpxrn1mihx")))

(define-public crate-tongue-0.0.6 (c (n "tongue") (v "0.0.6") (h "18inavkmaw2w6f5lsrrsb30flaw0103lg9b782nwd75nc59hqwrg")))

(define-public crate-tongue-0.0.7 (c (n "tongue") (v "0.0.7") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1k3q201lz6f2s3444bpnf024x1rq8mzsarjjlj71agy2dhx759v5")))

(define-public crate-tongue-0.0.8 (c (n "tongue") (v "0.0.8") (h "1pcimmgrhz2sb01w9fs14w2z39z7g8sbrhrbiha87h47wmzdwqdj")))

(define-public crate-tongue-0.0.9 (c (n "tongue") (v "0.0.9") (h "08phxg0gkwgml78gzby7m8ka85dlblmqwyvw6cc6rzb89vazhxb3")))

(define-public crate-tongue-0.1.0 (c (n "tongue") (v "0.1.0") (h "0mw6k0m5jfjbmybmk4g57rghsyihvx7vymln5055qb2xl9k0pmd1")))

(define-public crate-tongue-0.1.1 (c (n "tongue") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18g0j0fidbqq0jq1cm8vy34dhpnxf58fa7ys6cfagvr6g8kxid96")))

(define-public crate-tongue-0.1.2 (c (n "tongue") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1j15x22w3zwmhjwb0bi935pnvqcp0jrz0xd2ikk934l6bbw2l4gb")))

