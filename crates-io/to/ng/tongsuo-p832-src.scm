(define-module (crates-io to ng tongsuo-p832-src) #:use-module (crates-io))

(define-public crate-tongsuo-p832-src-832.0.1+8.3.2 (c (n "tongsuo-p832-src") (v "832.0.1+8.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0w5i287797ni8hhdhir0z2j9v72qlqpp7amjfyi19hs8z1jjvyc8") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-tongsuo-p832-src-832.0.2+8.3.2 (c (n "tongsuo-p832-src") (v "832.0.2+8.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1d7a8xyzi51mcydl37xfqa8bkxn6bk8499mj9ppbxf4mr39ywl8a") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-tongsuo-p832-src-832.0.3+8.3.2 (c (n "tongsuo-p832-src") (v "832.0.3+8.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1nq86xm6a968n1x748ik87lj2sdh31lj9wqxqqpwcjnfjkdlxrwd") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-tongsuo-p832-src-832.0.4+8.3.2 (c (n "tongsuo-p832-src") (v "832.0.4+8.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0sj6sfy370rb826ql8b8jrzzaaq78ml00058vxpcdnwla2gx05n9") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-tongsuo-p832-src-832.0.5+8.3.2 (c (n "tongsuo-p832-src") (v "832.0.5+8.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1d0a40ds2hk1xn2f01hlag7c6iga5ib3n2wzia262j4qfmncwgnz") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

(define-public crate-tongsuo-p832-src-832.0.6+8.3.2 (c (n "tongsuo-p832-src") (v "832.0.6+8.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "1zbj3qxmj9g854g4m81a7xl861b1k2yvwbjh2ngl074msr25i8k6") (f (quote (("weak-crypto") ("seed") ("idea") ("force-engine") ("default") ("camellia"))))))

