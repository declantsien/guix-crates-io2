(define-module (crates-io to gg toggl-reports) #:use-module (crates-io))

(define-public crate-toggl-reports-0.1.0 (c (n "toggl-reports") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "enum-display-derive") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-aux") (r "^2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0mg2wcpygjcj818mpxyypfhwfgrfcmygyyih9p87a3vpyrr2bmx0")))

