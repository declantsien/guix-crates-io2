(define-module (crates-io to gg togglempc) #:use-module (crates-io))

(define-public crate-togglempc-0.1.0 (c (n "togglempc") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1g4xqpdh4rb2k1cqzl45chbih1xlk3i6bh4mssy83vlcgq52shxc")))

