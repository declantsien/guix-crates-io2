(define-module (crates-io to gg toggle_florp) #:use-module (crates-io))

(define-public crate-toggle_florp-1.0.0 (c (n "toggle_florp") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0zmlvvv17agzal2khm65vw8225ydpa4m7ssdqmdfwydd8rqdlljd")))

(define-public crate-toggle_florp-1.0.1 (c (n "toggle_florp") (v "1.0.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "14hkd41vkm5lv6j7fp8q3h36gr8lfkk54cfqbp47w0nnk7lhz5w3")))

(define-public crate-toggle_florp-1.1.0 (c (n "toggle_florp") (v "1.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1s5dzhna0j9ykivsbpn0g4b8drdsb3j27zri09qixk8i5zpvrx98")))

(define-public crate-toggle_florp-2.0.0 (c (n "toggle_florp") (v "2.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1q30764l9d94q20c2m27qrl6jj7c0x49g29rrjxfc2qfzygyx0da")))

