(define-module (crates-io to gg toggle-comment) #:use-module (crates-io))

(define-public crate-toggle-comment-0.1.0 (c (n "toggle-comment") (v "0.1.0") (h "0avxgi0a9f3wznj1ki9aqzhv5qfg8iwzjfz8bq19q2kcylb9mmc9")))

(define-public crate-toggle-comment-0.2.0 (c (n "toggle-comment") (v "0.2.0") (d (list (d (n "clap") (r "~2.33") (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b2p2lqqhl884rpndg8r20vg97f0cdy35k8h7v0k7pp0bvqn850y")))

(define-public crate-toggle-comment-0.3.0 (c (n "toggle-comment") (v "0.3.0") (d (list (d (n "clap") (r "~2.33") (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gv01sqqf17fxn015i7iq918jw98mxc2d0cdz367j50wq0dr6qc0")))

(define-public crate-toggle-comment-0.4.0 (c (n "toggle-comment") (v "0.4.0") (d (list (d (n "clap") (r "~2.33") (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vr2wbkq53v0gc3bskjgchyc0x5j62ggwgzx4fa61n00kz40cgzg")))

(define-public crate-toggle-comment-0.5.0 (c (n "toggle-comment") (v "0.5.0") (d (list (d (n "clap") (r "~2.33") (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b9qqmpkpk9d3vaxaaxiiyag103gpn5wk420nhq7nhzjh1wj6ynh")))

