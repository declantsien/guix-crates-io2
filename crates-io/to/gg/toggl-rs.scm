(define-module (crates-io to gg toggl-rs) #:use-module (crates-io))

(define-public crate-toggl-rs-0.0.2 (c (n "toggl-rs") (v "0.0.2") (h "0ihqgkk40dw0jgp9w5gn5dsn9fp629adh0ca80kpiwm8q7db73a5")))

(define-public crate-toggl-rs-0.0.3 (c (n "toggl-rs") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mockito") (r "^0.31") (d #t) (k 2)) (d (n "owo-colors") (r "^3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0rw4k4fyqg49n0qil5v96rj4ac2cj7wgpafijazc9q4s2pib3fsk") (f (quote (("default" "client") ("client"))))))

