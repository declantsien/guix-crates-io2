(define-module (crates-io to gg toggle) #:use-module (crates-io))

(define-public crate-toggle-0.1.0 (c (n "toggle") (v "0.1.0") (h "1f6i43jv458gwl6n1cmd4959ricrfqgdpzy6jz43vqhp6kliybda")))

(define-public crate-toggle-0.2.0 (c (n "toggle") (v "0.2.0") (h "1m5y965a08ra37npdg8ramiaa2k7n1pxrfg0zar648n9r64rxc5w")))

