(define-module (crates-io to ne toner) #:use-module (crates-io))

(define-public crate-toner-0.2.7 (c (n "toner") (v "0.2.7") (h "1pl80jr7sy36n3van7bjfigmrwwbq9fif8nsyjqs9p0p0ib1pyb3")))

(define-public crate-toner-0.2.8 (c (n "toner") (v "0.2.8") (h "0wqlgay1ay6xa3id1qifa1d06n3qhi08n37xrdzc9n5mxrvk0pxf")))

(define-public crate-toner-0.2.9 (c (n "toner") (v "0.2.9") (h "0bjg5q9fj81x5p0naypm2hql0x3gfhmdygnz99bm2q2rb22rl1f0")))

(define-public crate-toner-0.2.10 (c (n "toner") (v "0.2.10") (h "19xd9mgykiiv7syg14j1jxbdd0jq4k2pzwcjbsx4dgdil310qsqa")))

(define-public crate-toner-0.2.11 (c (n "toner") (v "0.2.11") (h "1vfi8lcljkhmlhqdpc92jh2gc7p6aircr3sf4my3qh0lchwmk4ji")))

(define-public crate-toner-0.2.14 (c (n "toner") (v "0.2.14") (h "05ddldkyjc4h0i38z1gkfymih9vgs8h8f2zd48fqsz0fssgyzdch")))

(define-public crate-toner-0.2.15 (c (n "toner") (v "0.2.15") (h "0hcm0yshm040bhly89wzm9rwvhnjjnjkl21cwb2a5xh9bir7xjbk")))

(define-public crate-toner-0.2.16 (c (n "toner") (v "0.2.16") (h "111jpz1l7kb88s2j6lydyk5vx56z16q1wvb0x8zq0m4hz2grah89")))

(define-public crate-toner-0.2.17 (c (n "toner") (v "0.2.17") (d (list (d (n "tlb") (r "^0.2.17") (d #t) (k 0)) (d (n "tlb-ton") (r "^0.2.17") (d #t) (k 0)) (d (n "ton-contracts") (r "^0.2.17") (d #t) (k 0)))) (h "064zfnhaldqqxxdg5yqcl3plvw422jzp620plyi0fj4nhd9ra09s")))

(define-public crate-toner-0.2.18 (c (n "toner") (v "0.2.18") (d (list (d (n "tlb") (r "^0.2.18") (d #t) (k 0)) (d (n "tlb-ton") (r "^0.2.18") (d #t) (k 0)) (d (n "ton-contracts") (r "^0.2.18") (d #t) (k 0)))) (h "02a47fqn2b6zbijrr8d0qsmx34rbpj4mvr6l9ks8bpzqld80my7s")))

(define-public crate-toner-0.2.19 (c (n "toner") (v "0.2.19") (d (list (d (n "tlb") (r "^0.2.19") (d #t) (k 0)) (d (n "tlb-ton") (r "^0.2.19") (d #t) (k 0)) (d (n "ton-contracts") (r "^0.2.19") (d #t) (k 0)))) (h "0yphz528dqnv4zdq4g0sbvrrrdqk79gz8ajb6pcq4mns46mb284j")))

