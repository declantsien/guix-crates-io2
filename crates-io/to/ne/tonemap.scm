(define-module (crates-io to ne tonemap) #:use-module (crates-io))

(define-public crate-tonemap-0.0.1 (c (n "tonemap") (v "0.0.1") (h "0v8chjimjg5dkj67z66hk3ma37gyzyiyra207pqii7kfqdp0d5kh")))

(define-public crate-tonemap-0.0.2 (c (n "tonemap") (v "0.0.2") (d (list (d (n "glam") (r "^0.9.5") (o #t) (d #t) (k 0)))) (h "0jzic9n67ssyjph45yajrcw7x85bvpr5mxgxpg02m81k3c4dfkhl") (f (quote (("glam_support" "glam") ("default" "glam_support"))))))

(define-public crate-tonemap-0.0.3 (c (n "tonemap") (v "0.0.3") (d (list (d (n "glam") (r "^0.9.5") (o #t) (d #t) (k 0)))) (h "164lnvp7ajvswcbpms2ywhpqmvhima09j3s76n9x9qw9h4hbkmvi") (f (quote (("glam_support" "glam") ("default" "glam_support"))))))

(define-public crate-tonemap-0.0.4 (c (n "tonemap") (v "0.0.4") (d (list (d (n "glam") (r "^0.9.5") (o #t) (d #t) (k 0)) (d (n "spirv-std") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0pglb3jblpca3bk25cl2k27azq749fldcydwsxz0f8y8b244g5rd") (f (quote (("spirv-std-support" "spirv-std") ("glam-support" "glam") ("default"))))))

