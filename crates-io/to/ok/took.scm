(define-module (crates-io to ok took) #:use-module (crates-io))

(define-public crate-took-0.1.0 (c (n "took") (v "0.1.0") (h "0cfw5p7c4n2km67bcav1kz5y8a3zk5p2h7883jbcbadf917n8y67")))

(define-public crate-took-0.1.1 (c (n "took") (v "0.1.1") (h "1y5mr8gsaa7dp86hfqc40cx7xh5lg77v5ins4rda1aj7dwh7xiis")))

(define-public crate-took-0.1.2 (c (n "took") (v "0.1.2") (h "17ilplzmjpaj1yxqqccf35akwmpbqpp2d6ms2c5nzgl3n1fac7mg")))

