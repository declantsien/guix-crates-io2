(define-module (crates-io to pd topdio) #:use-module (crates-io))

(define-public crate-topdio-0.1.0 (c (n "topdio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.5") (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (d #t) (k 0)))) (h "00421jhs015bfgjpzbv87gf85g9wg1ghmx9rrx0hncmagpdjpykw")))

(define-public crate-topdio-0.1.1 (c (n "topdio") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.5") (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (d #t) (k 0)))) (h "1hvcxvjdz01ihal1ffqngbipp49wnpadbkq5lgg8giys53pbh0g3")))

(define-public crate-topdio-0.1.2 (c (n "topdio") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.5") (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (d #t) (k 0)))) (h "18rffqs2wpb9pd576l4jj20hcr1ak29nyksmqcprwjidpam1cd8s")))

