(define-module (crates-io to pd topdown-rs) #:use-module (crates-io))

(define-public crate-topdown-rs-0.0.1 (c (n "topdown-rs") (v "0.0.1") (h "1x7q9xr2y5mc7xjwmazvn7cji26yswnjvl8b6siqlyrbk5764zmv")))

(define-public crate-topdown-rs-0.1.0 (c (n "topdown-rs") (v "0.1.0") (h "0vrdrppfchbaipaw2132zpy9b2xibg93gc11sgsqd289c1a60blm")))

(define-public crate-topdown-rs-0.2.0 (c (n "topdown-rs") (v "0.2.0") (h "0lcsgrp045ssgy33ccjvh460vpj3glnikm4s5s0v6yl8x5lvv2cx")))

(define-public crate-topdown-rs-0.3.0 (c (n "topdown-rs") (v "0.3.0") (h "07skqvhwifnx2j561g18876vgrd1jrvdj5chk1zk1kpafcm6jnd1")))

(define-public crate-topdown-rs-0.3.1 (c (n "topdown-rs") (v "0.3.1") (d (list (d (n "regex") (r "^0.1.10") (d #t) (k 0)))) (h "1k23bwdwq14c3dk72i2gksfdsjdbrjyp3av67jpxzn8312m2drh6")))

(define-public crate-topdown-rs-0.3.2 (c (n "topdown-rs") (v "0.3.2") (d (list (d (n "regex") (r "^0.1.14") (d #t) (k 0)))) (h "0ynv290x1k6d6pxjb0nlikhp3087mhjmr81ncii4zhlymj04mr6d")))

(define-public crate-topdown-rs-0.3.3 (c (n "topdown-rs") (v "0.3.3") (d (list (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "13z4lql5591m6ry2ihxs861d09kj0921786l7jydf70wgx3ykp99")))

