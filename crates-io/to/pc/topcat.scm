(define-module (crates-io to pc topcat) #:use-module (crates-io))

(define-public crate-topcat-0.1.0 (c (n "topcat") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "08ns69hizicwip4g9bfy768rdlh21fx3zp0silzqfbbbms0z9rx4") (r "1.71.0")))

