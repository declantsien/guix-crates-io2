(define-module (crates-io to fu tofuri-sync) #:use-module (crates-io))

(define-public crate-tofuri-sync-0.1.0 (c (n "tofuri-sync") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)))) (h "1cygqk4sjzqx0vzp2gmymjv4hbmzfgzcxmyabbh26l58w6k5x4bi")))

