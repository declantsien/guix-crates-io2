(define-module (crates-io to fu tofuri-blockchain-sync) #:use-module (crates-io))

(define-public crate-tofuri-blockchain-sync-0.1.0 (c (n "tofuri-blockchain-sync") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)))) (h "1xwzdbil0ldwfcqzzid25s3wrgsvmjvqclm8bp5xd9g8jhz5ng2b")))

