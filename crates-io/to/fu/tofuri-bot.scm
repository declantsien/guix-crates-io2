(define-module (crates-io to fu tofuri-bot) #:use-module (crates-io))

(define-public crate-tofuri-bot-0.1.0 (c (n "tofuri-bot") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serenity") (r "^0.11") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tofuri-api-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0qn0q4zqzxjjiw943zk2g3s5xvv6w6a7xnhm2pb6x4g7csc9s34b")))

