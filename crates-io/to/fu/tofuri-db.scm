(define-module (crates-io to fu tofuri-db) #:use-module (crates-io))

(define-public crate-tofuri-db-0.1.0 (c (n "tofuri-db") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19") (d #t) (k 0)) (d (n "tofuri-block") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-int") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-stake") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-transaction") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-tree") (r "^0.1") (d #t) (k 0)))) (h "1p58j43wa81m9syifl5whsjap7innx7dx69as7xy6clj1hy3rbb8")))

