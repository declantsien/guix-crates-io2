(define-module (crates-io to fu tofuri-address) #:use-module (crates-io))

(define-public crate-tofuri-address-0.1.0 (c (n "tofuri-address") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)))) (h "13sk7f3lihsixfyccwcrd9h3irdn6vnk34pkfwyv2001xh8fkh5x")))

(define-public crate-tofuri-address-0.1.1 (c (n "tofuri-address") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)))) (h "1cv87lnp1affyyvb6clzyc385m5z4qb0n1ilqhb1h7lih1hf1xa7")))

