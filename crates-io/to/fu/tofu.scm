(define-module (crates-io to fu tofu) #:use-module (crates-io))

(define-public crate-tofu-0.1.0 (c (n "tofu") (v "0.1.0") (d (list (d (n "openssl") (r "*") (d #t) (k 0)))) (h "0lmi7yscr5pgs2v0c376b2v5202mz8kz3x5qbj156gpd0zaay85n")))

(define-public crate-tofu-0.1.1 (c (n "tofu") (v "0.1.1") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "1ff912nxcs8nh78iv73ygvi4h32akjrg0xf9n4fj5kp15v57h3zr")))

(define-public crate-tofu-0.1.3 (c (n "tofu") (v "0.1.3") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "0dd8j26xx1djiwmsng3scbqvyxhv1vy0kbw8w29g1jvxffffjw58")))

(define-public crate-tofu-0.1.4 (c (n "tofu") (v "0.1.4") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "119fiz3y8bz9r5jk05d1jm0qcwhg72vjijhdy1g31mrv04c4i848")))

(define-public crate-tofu-0.1.5 (c (n "tofu") (v "0.1.5") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "1sx88axyz57rcmki42kl93vf4z3dwaha7pmzwqd23sy7jcq8l324")))

(define-public crate-tofu-0.1.6 (c (n "tofu") (v "0.1.6") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "1adkgd4l2xjj7q23r6j0x93hflns2pwx4wby0gl60nakd30273iv")))

(define-public crate-tofu-0.1.7 (c (n "tofu") (v "0.1.7") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "0wh5a3vrz33frw69j3hc3f9ns8ky1dglz8vwfzijx3qwr43lis9x")))

(define-public crate-tofu-0.1.8 (c (n "tofu") (v "0.1.8") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "1k8pkla8nxrnaqbanlk36gkvzfhgkascnf8957rhx8jlw2ydkmbi")))

(define-public crate-tofu-0.1.9 (c (n "tofu") (v "0.1.9") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "006i6n9ycvpw8lki9zh151svdd3fsdas2cp0x1lsxz00vm6f6b47")))

(define-public crate-tofu-0.1.10 (c (n "tofu") (v "0.1.10") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "0zl371riafslb3iil0abxnl41j5vp9h5zgiy4qksfq89gc45rm0p")))

(define-public crate-tofu-0.1.11 (c (n "tofu") (v "0.1.11") (d (list (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "odds") (r "0.*") (d #t) (k 2)) (d (n "openssl") (r "0.*") (d #t) (k 0)) (d (n "tempdir") (r "0.*") (d #t) (k 0)))) (h "116ssihnh4yl8hlmm2znxc7y83s4cv1sqbnilirn1y2qq4sigvm0")))

