(define-module (crates-io to fu tofuri-transaction) #:use-module (crates-io))

(define-public crate-tofuri-transaction-0.1.0 (c (n "tofuri-transaction") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-int") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-key") (r "^0.1") (d #t) (k 0)))) (h "1sd6vp26cl0zv34ky1ka1d9lx3vlij09h6jsj9k4d158ma7018wy")))

