(define-module (crates-io to fu tofuri-pay-core) #:use-module (crates-io))

(define-public crate-tofuri-pay-core-0.1.0 (c (n "tofuri-pay-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tofuri-address") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-key") (r "^0.1") (d #t) (k 0)))) (h "0cbjcnlfzjxgycvssdqkbhkizysf5yzwmx0zycjcilcnyzsyz6by")))

