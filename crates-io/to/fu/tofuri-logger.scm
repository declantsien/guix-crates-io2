(define-module (crates-io to fu tofuri-logger) #:use-module (crates-io))

(define-public crate-tofuri-logger-0.1.0 (c (n "tofuri-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wqki1901jkjzszjwzhl7j1d6rj69nk1slrzarlgdrg9xp4gbgl4")))

