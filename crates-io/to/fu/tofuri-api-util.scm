(define-module (crates-io to fu tofuri-api-util) #:use-module (crates-io))

(define-public crate-tofuri-api-util-0.1.0 (c (n "tofuri-api-util") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "tofuri-address") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-api-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-block") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-int") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-stake") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-transaction") (r "^0.1") (d #t) (k 0)))) (h "0jiblhpzz9pibqmil1br5w0cgh0zf0fa3q894ysg45ppm6cyw7rz")))

