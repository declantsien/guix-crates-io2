(define-module (crates-io to fu tofuri-fork) #:use-module (crates-io))

(define-public crate-tofuri-fork-0.1.0 (c (n "tofuri-fork") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19") (d #t) (k 0)) (d (n "tofuri-address") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-block") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-db") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-int") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-stake") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-transaction") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-tree") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-util") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "02yjglllg9acsys41nzsk9wnj076cbhfj8ifqpjrk0bwp854gdpk")))

