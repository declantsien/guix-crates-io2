(define-module (crates-io to fu tofuri-p2p) #:use-module (crates-io))

(define-public crate-tofuri-p2p-0.1.0 (c (n "tofuri-p2p") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libp2p") (r "^0.50") (f (quote ("mplex" "noise" "tcp" "request-response" "autonat" "macros" "identify" "mdns" "gossipsub" "tokio"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-util") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (d #t) (k 0)))) (h "0wyy4l359dpvy5akdny307k34yj3fqsrnc4fa1bx1jn371zi0p8p")))

