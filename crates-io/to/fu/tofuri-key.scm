(define-module (crates-io to fu tofuri-key) #:use-module (crates-io))

(define-public crate-tofuri-key-0.1.0 (c (n "tofuri-key") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25") (f (quote ("rand-std" "recovery" "global-context"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "vrf") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0yniabmvinjpy50iiygl48baykr9d1zr7rx7h5g5ip24k8253wnz") (s 2) (e (quote (("vrf" "dep:vrf"))))))

