(define-module (crates-io to fu tofuri-rpc) #:use-module (crates-io))

(define-public crate-tofuri-rpc-0.1.0 (c (n "tofuri-rpc") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "multiaddr") (r "^0.17") (d #t) (k 0)) (d (n "tofuri-block") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-rpc-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-stake") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-sync") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-transaction") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)))) (h "1g1vj3xia1ny24y42vjkp8gdyvf0vp5rd3p0sq79dclllh4rggrg")))

