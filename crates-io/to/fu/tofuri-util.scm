(define-module (crates-io to fu tofuri-util) #:use-module (crates-io))

(define-public crate-tofuri-util-0.1.0 (c (n "tofuri-util") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tofuri-block") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-stake") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-transaction") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0h5fp7ngcq33dzhzpz0kwry5x36jbjxayf643ishimr36cjq02wv")))

