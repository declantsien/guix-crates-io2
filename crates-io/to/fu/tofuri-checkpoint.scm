(define-module (crates-io to fu tofuri-checkpoint) #:use-module (crates-io))

(define-public crate-tofuri-checkpoint-0.1.0 (c (n "tofuri-checkpoint") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tofuri-block") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)))) (h "07jl08v4dpa9939l7wq0gpffk2sca5145qlig5f4777kkr6s5kg3")))

