(define-module (crates-io to fu tofu-img) #:use-module (crates-io))

(define-public crate-tofu-img-1.0.0 (c (n "tofu-img") (v "1.0.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1mvj4m1dyi9pvwp03j8hgd0wi0y4yn73lzkr23b7wpl98khj2drg")))

