(define-module (crates-io to fu tofuri-api-internal) #:use-module (crates-io))

(define-public crate-tofuri-api-internal-0.1.0 (c (n "tofuri-api-internal") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "multiaddr") (r "^0.17") (d #t) (k 0)) (d (n "tofuri-api-internal-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-block") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-blockchain") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-core") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-stake") (r "^0.1") (d #t) (k 0)) (d (n "tofuri-transaction") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("full"))) (d #t) (k 0)))) (h "1x695s6yvjizqh6qamykdn6z4prk3516amrj1z0zb5f4zh6rqwpq")))

