(define-module (crates-io to fu tofuri-core) #:use-module (crates-io))

(define-public crate-tofuri-core-0.1.0 (c (n "tofuri-core") (v "0.1.0") (h "0y97jiagn20lr8hlwxjwn6a3pnmvx5ylga7xxvcf6gs796h18r5k")))

(define-public crate-tofuri-core-0.1.1 (c (n "tofuri-core") (v "0.1.1") (h "03q5wwfj1c9vfykm78znf29y8ndyhjb3paklxkrjrp10ymyfp862")))

(define-public crate-tofuri-core-0.1.2 (c (n "tofuri-core") (v "0.1.2") (h "0cq86qdin14awbbikv8drsvw77xyz7lbpjgmk4yj7v5b8yx7rzch")))

