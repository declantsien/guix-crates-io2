(define-module (crates-io to ph toph) #:use-module (crates-io))

(define-public crate-toph-0.1.0 (c (n "toph") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0f5xxzcz3dc6jcq7cnjyqj737nwmf7s0mncw4d4ak0f7m3nla28i")))

(define-public crate-toph-0.2.0 (c (n "toph") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "12damz19v5yzjdry8gvc1z7c03wh076irm3fjgd1v9v16qwlzp8h")))

(define-public crate-toph-0.3.0 (c (n "toph") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0pjspgrdsr99i11knawlxjrv233jkfbpavxdpngbpc4glrnwdrrp")))

(define-public crate-toph-0.4.0 (c (n "toph") (v "0.4.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1ampf0ir6vqsngab9zsfclqhzi9pj7syi6w2h947iqpa8r7fc1id")))

(define-public crate-toph-0.5.0 (c (n "toph") (v "0.5.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1pzgjwirv75srqfgfvp9d2c923165njb30v685gdhwsih6x1hwhx")))

(define-public crate-toph-0.6.0 (c (n "toph") (v "0.6.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "199dcqp1l3c56aa817q8klj9gwspjwl4ifxar27wmz5ly31whiqw")))

(define-public crate-toph-0.7.0 (c (n "toph") (v "0.7.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "01687ns2fxx9g267rbkzh5cw0m2yjf26j5irsvya339slylxks5x")))

(define-public crate-toph-0.8.0 (c (n "toph") (v "0.8.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "15g9r8ymw0rr0c97gdlfzfr7aj8y6pqkr20j0yj58vs09k2h26n9")))

