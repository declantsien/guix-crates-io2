(define-module (crates-io to p- top-english-words) #:use-module (crates-io))

(define-public crate-top-english-words-1.0.0 (c (n "top-english-words") (v "1.0.0") (h "1aafsqx29qggcx8vq6ncn0pkwxrqdv6jg93yanp3h8h88f28bxz5")))

(define-public crate-top-english-words-1.0.1 (c (n "top-english-words") (v "1.0.1") (h "1drhqvv8if89dsr5lgss7h366k77xkb658w4j99jqf3a3iyv9bsc")))

(define-public crate-top-english-words-1.0.2 (c (n "top-english-words") (v "1.0.2") (d (list (d (n "include-lines") (r "^1.0.0") (d #t) (k 0)))) (h "0r6qkfvvj040b9lv2g8kyifkpwpb8gawsib6j853ggpppvj16xxa")))

(define-public crate-top-english-words-1.1.0 (c (n "top-english-words") (v "1.1.0") (d (list (d (n "include-lines") (r "^1.0.0") (d #t) (k 0)))) (h "0dcfrvigz5svbr96hv1833lfmqnrj9dcra12jw12hq37n9qsxayi")))

(define-public crate-top-english-words-1.1.1 (c (n "top-english-words") (v "1.1.1") (d (list (d (n "include-lines") (r "^1.1.2") (d #t) (k 0)))) (h "0b57spkjdy38l108fls1gi0jla6r9a7lz76hj2l8ywf2ng9zhvbq")))

