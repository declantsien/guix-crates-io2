(define-module (crates-io to p- top-type-sizes) #:use-module (crates-io))

(define-public crate-top-type-sizes-0.1.0 (c (n "top-type-sizes") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "1zmj1y3bzspiqidq2djqy7ld8phhva6g02q6snz1x52nywrpy4md")))

(define-public crate-top-type-sizes-0.1.1 (c (n "top-type-sizes") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "0wcbl16w3ird91jzmxqfnjpcfaa0mhn6dp89s3ckhxy05n336rb6")))

(define-public crate-top-type-sizes-0.1.2 (c (n "top-type-sizes") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "14dc6jcdc0d9i7bx42fr7bi7xcw9pxhgvg79av9ijpfdcy3hva1j")))

(define-public crate-top-type-sizes-0.1.3 (c (n "top-type-sizes") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "0cmw2k9f3ckwl0ycw336gl7s875y25j96wkxl4hbsisypdm9yc5z")))

(define-public crate-top-type-sizes-0.1.4 (c (n "top-type-sizes") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "1djnca71gy1md20wnlr50y9zwh68swbw29j7s1hdcmnwhrksb3x0")))

(define-public crate-top-type-sizes-0.1.5 (c (n "top-type-sizes") (v "0.1.5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)))) (h "17ijc7p34inz2w53yvgzjz9zamwnjjsi1a9wxxfqyk9199ffqxbv")))

