(define-module (crates-io to p- top-gg) #:use-module (crates-io))

(define-public crate-top-gg-0.0.0 (c (n "top-gg") (v "0.0.0") (h "14fzysfv8ykjkwi0zc5pnz24kxi30fhca9apjkrqiya65d1niwsk") (y #t)))

(define-public crate-top-gg-0.1.0-alpha.0 (c (n "top-gg") (v "0.1.0-alpha.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.1") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0fyjvgkh8zkzzhh7jvkwy7c5xwr51mlpgvmgs1wrpq7fciyxs1ml")))

