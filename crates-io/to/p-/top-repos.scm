(define-module (crates-io to p- top-repos) #:use-module (crates-io))

(define-public crate-top-repos-0.1.0 (c (n "top-repos") (v "0.1.0") (h "06vavl8aprcgrq4smkgnyv1qw9byavlvlv6m8m52ncily42wrva5")))

(define-public crate-top-repos-0.1.1 (c (n "top-repos") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1wz7jw7jdmvqdn22dwim38sjjkyb0vjd2975zldhd0r3mpfqmh22")))

