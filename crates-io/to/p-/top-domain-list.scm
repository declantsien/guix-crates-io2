(define-module (crates-io to p- top-domain-list) #:use-module (crates-io))

(define-public crate-top-domain-list-0.1.0 (c (n "top-domain-list") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1mrra6rzkz3f6mrki6pvzi9gw1b5bykfrjr9289xiclcihdrpadm")))

(define-public crate-top-domain-list-0.2.0 (c (n "top-domain-list") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "189z6rpjq78azik11a0k6yrja8isaj9dbs5mkvy21g56rlf3vlkd")))

