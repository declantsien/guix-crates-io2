(define-module (crates-io to rp torpid) #:use-module (crates-io))

(define-public crate-torpid-0.1.0 (c (n "torpid") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1fsr7y1ym61asxivnxnlf5nm633lc335yqxycml2a0gdsgb4v93n")))

(define-public crate-torpid-0.1.1 (c (n "torpid") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1bw13l4viff9d2vxjzhs2fkxlj6z47cfmhmfx7lcaa15hgvm2vv1")))

(define-public crate-torpid-0.1.2 (c (n "torpid") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1qdz4klj5bmlgvj9yic0nzimikyhyclpypbw65cpp3zj61qj6sfa")))

(define-public crate-torpid-0.1.3 (c (n "torpid") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1x8yiilz7g89zklw1j2disi05f7bpmahy25lyfih7vmbjlb9gdmq")))

