(define-module (crates-io to il toiletdb) #:use-module (crates-io))

(define-public crate-toiletdb-0.1.0 (c (n "toiletdb") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1rnfzzzw30r6picd578ffr2k2wqa2zivl6hqpy6r3gwvnc2axq04")))

(define-public crate-toiletdb-0.1.1 (c (n "toiletdb") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1dr8f46czhhxnfjp5hx86pxppwvx91p5j3ngyflb7p90avg8m7jw")))

(define-public crate-toiletdb-0.1.2 (c (n "toiletdb") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1d0qfrwam3zbcw6cf8wkiiyhkznjf0dgx0gyl32d1czcd07mr2gq")))

