(define-module (crates-io to il toiletcli) #:use-module (crates-io))

(define-public crate-toiletcli-0.0.1 (c (n "toiletcli") (v "0.0.1") (h "0c03jvfhm86kdabl0gqkw26qg9y62f108lw05472wc1cd2sg7hv9") (y #t)))

(define-public crate-toiletcli-0.0.2 (c (n "toiletcli") (v "0.0.2") (h "1prcmaqfyb0sxp38mzm1bss34dj4lh449rsmqgj6yrd2yqxan32s") (y #t)))

(define-public crate-toiletcli-0.0.3 (c (n "toiletcli") (v "0.0.3") (h "1az1rayacsq13mrzqp04ljrsc8c8xv774q1ylnxmi6zzp1dy7kv1") (y #t)))

(define-public crate-toiletcli-0.0.4 (c (n "toiletcli") (v "0.0.4") (h "1f70a387z0ksyswvrb62npjmrqxhqxzgdfzvimp4s1701mhk9nkq") (y #t)))

(define-public crate-toiletcli-0.0.5 (c (n "toiletcli") (v "0.0.5") (h "1sk72mxx1k1lqpl9i531f7g55z4qsrxkfigrlccw6q10wwj31g9x")))

(define-public crate-toiletcli-0.0.6 (c (n "toiletcli") (v "0.0.6") (h "1r0g55h98snqbxgna6xi6za62lhqf39ihbm2i2khm8fqr1a8121g") (y #t)))

(define-public crate-toiletcli-0.0.7 (c (n "toiletcli") (v "0.0.7") (h "0k8hv2llnifzm26vncfiahny1mxklh0zcxf3bzj2hfp96rgnjzrs") (y #t)))

(define-public crate-toiletcli-0.1.0 (c (n "toiletcli") (v "0.1.0") (h "1dvf48lg17w6fd83xnnsfxqj7s86w5sb6bsp0sca29k2rkbx10g2")))

(define-public crate-toiletcli-0.2.0 (c (n "toiletcli") (v "0.2.0") (h "1yh3n3cckf9is2y05gb016y8fjzyfd17v6yf2330v3py27b80g9j") (f (quote (("mock_colors")))) (y #t)))

(define-public crate-toiletcli-0.2.1 (c (n "toiletcli") (v "0.2.1") (h "0gk5ipr6dijjw3n0aazw06yk7csizvk1w0xcblr7s0hnv5cql32k") (f (quote (("mock_colors")))) (y #t)))

(define-public crate-toiletcli-0.2.3 (c (n "toiletcli") (v "0.2.3") (h "0xparvsj160a6c8l7r3hqi2yh8zn7k66c9zk8vqracgv87iihlbq") (f (quote (("mock_colors") ("flags") ("default" "colors" "flags") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.4.0 (c (n "toiletcli") (v "0.4.0") (h "1p2c29f00szqk9wvysynrscvl6jmch6kib85cvpaq9b3fhp6k118") (f (quote (("default")))) (y #t)))

(define-public crate-toiletcli-0.4.1 (c (n "toiletcli") (v "0.4.1") (h "13mar4spmmylh1abnq2zm23rbds194x56hlqbrhn1qmi1aa07wqx") (f (quote (("default")))) (y #t)))

(define-public crate-toiletcli-0.5.0 (c (n "toiletcli") (v "0.5.0") (h "1ya1ybgcrvmgz5xx29621m5dhlgv6cwx117hxqy5mmssar4b2ci0") (f (quote (("default")))) (y #t)))

(define-public crate-toiletcli-0.5.1 (c (n "toiletcli") (v "0.5.1") (h "0fkc6jw99nbq50caaldki456rzjalywqai4i5fh29jpfyqhfmwin") (f (quote (("default"))))))

(define-public crate-toiletcli-0.6.0 (c (n "toiletcli") (v "0.6.0") (h "0khid51rr1ghr2fv2683ix9zz1irwrc6r1v6zcvdfh7y91hbb7vm") (f (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.6.1 (c (n "toiletcli") (v "0.6.1") (h "00k02pnd5314cybq5n2fbkdfly9iml9nms3jcrplcqw198ny20wg") (f (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.7.0 (c (n "toiletcli") (v "0.7.0") (h "0kr8vih2dkw22iar7mp8gk41whncfz42h6vfgxipvm7dcb0l6j6w") (f (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.7.1 (c (n "toiletcli") (v "0.7.1") (h "1b600z2cq816yhqgrxp53xlqcpnkc2ap7airqjdba600s4i71x5v") (f (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.7.3 (c (n "toiletcli") (v "0.7.3") (h "0v31fai3i460zmkvqqrbm0bmz1ppn6wxqdhqg7gpw8wkisfwr9xi") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.8.1 (c (n "toiletcli") (v "0.8.1") (h "03ck29p6lv197sdmn4xf2mws5fm2jc19qar1rlzgmzkpidkq0rkx") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.9.0 (c (n "toiletcli") (v "0.9.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1596pj333f7i88s4lbfdvr0857hc9fzq7fnjwpdzqvabd5zy1krz") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.9.1 (c (n "toiletcli") (v "0.9.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "12063xr1y2g35625f0arah5l6lc1lqdf185rchnicqmc3fzfx1wi") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.9.2 (c (n "toiletcli") (v "0.9.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1vrxly27732sy6825xsiqfpri4lbxhic39sbvvcjd4vhg50kqrmj") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.9.3 (c (n "toiletcli") (v "0.9.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "17k3kzmfpbn2ksmkjjl5mqpkann6yk0261pdzynri2g1pf5f10wf") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.9.4 (c (n "toiletcli") (v "0.9.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "12nljzw1mrmaxqy2rxhxkslr4kdf4diwchw7sxcz9h3csv1lbv8j") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.10.0 (c (n "toiletcli") (v "0.10.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1m4nyhgs9bb7w78k3vnkj1n2kyjrqrb17h993g49yb8gvmjgk9aw") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.11.0 (c (n "toiletcli") (v "0.11.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "07v6la6a4rvqdgn328ifazmwnim5vrkf3ws0b4xzpz4cpz6p3l1b") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (y #t)))

(define-public crate-toiletcli-0.12.0 (c (n "toiletcli") (v "0.12.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0vww7cfx8hbqcz7ywjz0q52w1hlpvnqsgcy0czy09syrmqk10psz") (f (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

