(define-module (crates-io to us tousize) #:use-module (crates-io))

(define-public crate-tousize-0.1.0 (c (n "tousize") (v "0.1.0") (h "02g41xqrml390x4gzg0ymwm2q0d0wqal24bchhr8pq0j0hrl1fg7")))

(define-public crate-tousize-1.0.0 (c (n "tousize") (v "1.0.0") (h "04qf70hadh6y9c8dvskvxmd1dvn69dj3i3gxyzwwhpzc54ybh73h")))

