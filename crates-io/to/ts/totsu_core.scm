(define-module (crates-io to ts totsu_core) #:use-module (crates-io))

(define-public crate-totsu_core-0.1.0 (c (n "totsu_core") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0p59ind129pih94mbrd370r5z3qqs6136sc7dxix23d9g6m167v5") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-totsu_core-0.1.1 (c (n "totsu_core") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "144ghglhg6s9k5idbxpcy959jym062j55vga8gjn13bq6gk2cs5d") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

