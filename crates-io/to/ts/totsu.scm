(define-module (crates-io to ts totsu) #:use-module (crates-io))

(define-public crate-totsu-0.1.0 (c (n "totsu") (v "0.1.0") (h "1r8niqxfnrd0423vh53iv7m3w9bxr2cymlp1p68yk0rjpidhnvka")))

(define-public crate-totsu-0.2.0 (c (n "totsu") (v "0.2.0") (h "01jypgv383m04h5wafyfpy0l1fswzjgih1qw3l741n8y9apvq6lp")))

(define-public crate-totsu-0.3.0 (c (n "totsu") (v "0.3.0") (h "1zw7cp2pcnx9rbmnrsnliakqvcml12fmik2yi2zsnx47pj8v3kaj")))

(define-public crate-totsu-0.4.0 (c (n "totsu") (v "0.4.0") (h "0cws3r7s73nsivwd1688x3cqjl06viz77d76fqml0pbqd3f97lvi")))

(define-public crate-totsu-0.4.1 (c (n "totsu") (v "0.4.1") (h "0kj6mbh0k3wgqvl8c15xnijkw7mqvjhbsaa89338026h0cwqj57q")))

(define-public crate-totsu-0.4.2 (c (n "totsu") (v "0.4.2") (h "0blic1hp7s0qgnh55mbdsw7hwm86k15ml8qiab59mgfb20a1vbnk")))

(define-public crate-totsu-0.5.0 (c (n "totsu") (v "0.5.0") (h "079hp9fdgfz566rijd7jpg64761jkxai3knaj8abiz22yj798b7g")))

(define-public crate-totsu-0.6.0 (c (n "totsu") (v "0.6.0") (d (list (d (n "cblas") (r "^0.2.0") (d #t) (k 0)) (d (n "float_eq") (r "^0.5.0") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.6.0") (f (quote ("mkl-static-lp64-seq"))) (k 2)) (d (n "lapacke") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)))) (h "0iidkc395sp7p5pm4qv7yc76garvsfk1479g4kf9ihfxb1lwm6ym")))

(define-public crate-totsu-0.7.0 (c (n "totsu") (v "0.7.0") (d (list (d (n "cblas") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "float_eq") (r "^0.5.0") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.6.0") (f (quote ("mkl-static-lp64-seq"))) (k 2)) (d (n "lapacke") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1nlgcg1bc40w23r9679f9c6dqdsf1mvxip1903jrls1xpbbbv07c") (f (quote (("nostd" "num-traits/libm") ("default" "num-traits/std" "lapacke" "cblas"))))))

(define-public crate-totsu-0.8.0 (c (n "totsu") (v "0.8.0") (d (list (d (n "cblas") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "float_eq") (r "^0.6.0") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.6.0") (f (quote ("mkl-static-lp64-seq"))) (k 2)) (d (n "lapacke") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1sr7zx79z414j8hs82f4qzlrm5ksyjzc5jsin8qp0ybz5mrdd760") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("f64lapack" "lapacke" "cblas") ("default" "std"))))))

(define-public crate-totsu-0.8.1 (c (n "totsu") (v "0.8.1") (d (list (d (n "cblas") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "float_eq") (r "^0.6.1") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.6.0") (f (quote ("mkl-static-lp64-seq"))) (k 2)) (d (n "lapacke") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "00g4b89g4c9pr3qpkipv376bdkric3ysra9wwsgjyviaykjkyazj") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("f64lapack" "lapacke" "cblas") ("default" "std"))))))

(define-public crate-totsu-0.9.0 (c (n "totsu") (v "0.9.0") (d (list (d (n "cblas") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "float_eq") (r "^0.7.0") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.6.0") (f (quote ("mkl-static-lp64-seq"))) (k 2)) (d (n "lapacke") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "12w6fmm2vagxjydklpz9fn1wrbl4z3iflqp8yr12q2330i8hk45b") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("f64lapack" "lapacke" "cblas") ("default" "std"))))))

(define-public crate-totsu-0.9.1 (c (n "totsu") (v "0.9.1") (d (list (d (n "cblas") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.7.0") (f (quote ("mkl-static-lp64-seq"))) (k 2)) (d (n "lapacke") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1gaxpn3vkxpjmpvjv3wdszy0hyy161gnmkg446a5ycgqi7gwrnas") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("f64lapack" "lapacke" "cblas") ("default" "std"))))))

(define-public crate-totsu-0.10.0 (c (n "totsu") (v "0.10.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "totsu_core") (r "^0.1.0") (d #t) (k 0)))) (h "1qg9kqqwz9jnp7zpm5ga51canrs1kas18ky00dyrfv97p6abrnah")))

(define-public crate-totsu-0.10.1 (c (n "totsu") (v "0.10.1") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "totsu_core") (r "^0.1.0") (d #t) (k 0)))) (h "07dj95isanl6lpam02bvg2m2gbqa0wl11jqiisqzgh1h6nzi31mi")))

(define-public crate-totsu-0.10.2 (c (n "totsu") (v "0.10.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "totsu_core") (r "^0.1.1") (d #t) (k 0)))) (h "1m0y16ri3izcs4y6nz8mc7idywi6y2fl9m1qa3pyavx109bns7jr")))

