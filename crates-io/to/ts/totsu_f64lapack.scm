(define-module (crates-io to ts totsu_f64lapack) #:use-module (crates-io))

(define-public crate-totsu_f64lapack-0.1.0 (c (n "totsu_f64lapack") (v "0.1.0") (d (list (d (n "cblas") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.8.1") (f (quote ("mkl-static-lp64-seq"))) (d #t) (k 2)) (d (n "lapacke") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "totsu") (r "^0.10.0") (d #t) (k 2)) (d (n "totsu_core") (r "^0.1.0") (d #t) (k 0)))) (h "00bw6gqdsd8q937asj670xp92xghvskf0dxdxx9svh9jna3maha7")))

(define-public crate-totsu_f64lapack-0.1.1 (c (n "totsu_f64lapack") (v "0.1.1") (d (list (d (n "cblas") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "intel-mkl-src") (r "^0.8.1") (f (quote ("mkl-static-lp64-seq"))) (d #t) (k 2)) (d (n "lapacke") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "totsu") (r "^0.10.2") (d #t) (k 2)) (d (n "totsu_core") (r "^0.1.1") (d #t) (k 0)))) (h "08c9cg2vvck7jwiz9yivcflqywxqy5rf4yxgbbbq74pbnr59kkk9")))

