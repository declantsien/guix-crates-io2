(define-module (crates-io to ts totsu_f32cuda) #:use-module (crates-io))

(define-public crate-totsu_f32cuda-0.1.0 (c (n "totsu_f32cuda") (v "0.1.0") (d (list (d (n "cublas-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 0)) (d (n "totsu") (r "^0.10.0") (d #t) (k 2)) (d (n "totsu_core") (r "^0.1.0") (d #t) (k 0)))) (h "18khafvqj055injz4ifk3lsaq4rm234l2g19wsmzvlgrlp60dni5")))

(define-public crate-totsu_f32cuda-0.1.1 (c (n "totsu_f32cuda") (v "0.1.1") (d (list (d (n "cublas-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rustacuda") (r "^0.1.3") (d #t) (k 0)) (d (n "totsu") (r "^0.10.2") (d #t) (k 2)) (d (n "totsu_core") (r "^0.1.1") (d #t) (k 0)))) (h "1z3fifr8g1g47iz7v99m66mmagmd9x0dqkvhqrrqkk3drlzq9837")))

