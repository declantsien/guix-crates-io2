(define-module (crates-io to xi toxiproxy_rust) #:use-module (crates-io))

(define-public crate-toxiproxy_rust-0.1.0 (c (n "toxiproxy_rust") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kcmgdp5b8nr8syczaak6jjvk4ryghdb6ca9jinhjky2gb3ak9aw")))

(define-public crate-toxiproxy_rust-0.1.1 (c (n "toxiproxy_rust") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zsg8blvfvrkrzgd50yk2v0zd45nbclszfcrfvlkm3h4d4ibvahi")))

(define-public crate-toxiproxy_rust-0.1.2 (c (n "toxiproxy_rust") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14c3pic1i5q23qn50hqk6p8v66h28zrjix6mfjv088skv03yvzlv")))

(define-public crate-toxiproxy_rust-0.1.3 (c (n "toxiproxy_rust") (v "0.1.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p29snrq7yyj4x2ql88wy269j7js013gxafw0hgrxg9wh6gnjf93")))

(define-public crate-toxiproxy_rust-0.1.4 (c (n "toxiproxy_rust") (v "0.1.4") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dqf87mipnchcdgfb5dfs6hswj5r6n81xvncqdpq02d4pvck8cmm")))

(define-public crate-toxiproxy_rust-0.1.5 (c (n "toxiproxy_rust") (v "0.1.5") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q4dlwspijc4lzgyv5rffy0j86k2gzhqcjix5knhbfj8w9rkyg08")))

(define-public crate-toxiproxy_rust-0.1.6 (c (n "toxiproxy_rust") (v "0.1.6") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "157nxxmcv8kaahpmch096mj6v64bgyral6qx63mg8pdzfbq0c6m2")))

