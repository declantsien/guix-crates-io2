(define-module (crates-io cr se crseo-sys) #:use-module (crates-io))

(define-public crate-crseo-sys-0.1.0 (c (n "crseo-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "01mhyfk6yjsw5zf70bw2hcbfyhy2ppwp50xsias7c7a3p06fkx6w")))

(define-public crate-crseo-sys-0.2.0 (c (n "crseo-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0938ji7zn7lhdg1xzg8wylkcn3n7mpkkf4m57c3z2sllmamyvd92")))

(define-public crate-crseo-sys-1.0.0 (c (n "crseo-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "184gj0yjcv9x6rh0isy5x4gky9jbh3fc4z1r8i04lk5mfqpjjq5w") (y #t)))

(define-public crate-crseo-sys-1.0.1 (c (n "crseo-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1vqdn30dq10sbwi3cd5qia0vlbjx71n6df7cxvz9vaa1dizqw2mf") (y #t)))

(define-public crate-crseo-sys-1.0.2 (c (n "crseo-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1bkpjjjj9nxyaxx7gq7slkbhnwrblpaw100fr5r6pm699pc30rfq")))

(define-public crate-crseo-sys-1.0.3 (c (n "crseo-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "08xgy45hf9fnn8yb480vcrzkansgq5cmb0dkfqylwhj391yw5ii7")))

(define-public crate-crseo-sys-1.1.0 (c (n "crseo-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "057fh5bcxlj6vkfwnbhnfz14kk286vyb7wcqnysi46hkkg95iayq")))

(define-public crate-crseo-sys-1.1.1 (c (n "crseo-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1k8b8lkyy3wnizlagq5bvbh807krd5499hqmb7bpqwhlc273l08s")))

