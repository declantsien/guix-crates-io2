(define-module (crates-io cr uc crucio) #:use-module (crates-io))

(define-public crate-crucio-0.1.0 (c (n "crucio") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("async-await" "nightly" "compat"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1a785lmqf5w3560s89fb0mvs558g47sy8mdnnfslwn2ryn0azbbd")))

