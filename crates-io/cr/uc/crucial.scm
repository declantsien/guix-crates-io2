(define-module (crates-io cr uc crucial) #:use-module (crates-io))

(define-public crate-crucial-0.0.0 (c (n "crucial") (v "0.0.0") (h "1sslr594s4lisbnfc13imv22r154r5ra28qifhz87rn8d8pka293") (r "1.77")))

(define-public crate-crucial-0.0.1 (c (n "crucial") (v "0.0.1") (h "1ibfbixs729bvrfl7xsy5lb8zxy1wz3qp0pxxa57qlnj2hjbvnkg") (r "1.77")))

