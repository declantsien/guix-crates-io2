(define-module (crates-io cr uc crucible) #:use-module (crates-io))

(define-public crate-crucible-0.0.0 (c (n "crucible") (v "0.0.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tic") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0fbb8vjr56fbccm3kb8xxjlgfc5lm2gc0bp957ck9s00d2vcl4jn")))

