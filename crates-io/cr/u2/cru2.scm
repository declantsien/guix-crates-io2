(define-module (crates-io cr u2 cru2) #:use-module (crates-io))

(define-public crate-cru2-0.1.0 (c (n "cru2") (v "0.1.0") (h "09vrh8jb32k11x7ai3n5cd8x04f63xkkxmll8gfkb7blq0dgbfrw")))

(define-public crate-cru2-0.1.1 (c (n "cru2") (v "0.1.1") (h "1ap75xfni5xr29wxcys887a86i0nrw6i3bidi98bj1wn02gmrkyq")))

(define-public crate-cru2-0.1.2 (c (n "cru2") (v "0.1.2") (h "10wjny9334377988lx94dn6c9slfp0y1gkmyaigyy8f68rcfz3wz")))

