(define-module (crates-io cr c6 crc64-rs) #:use-module (crates-io))

(define-public crate-crc64-rs-0.1.0 (c (n "crc64-rs") (v "0.1.0") (h "129dfq0az0mgdzd8f7xkxw4dbvixpiax6x0k664il8ydx1nycvq7") (y #t)))

(define-public crate-crc64-rs-0.2.0 (c (n "crc64-rs") (v "0.2.0") (h "1pv0a3w65zckv7ywm464wz87d3kdxklrbinwgc9axp722f50b4za") (y #t)))

