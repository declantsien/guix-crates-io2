(define-module (crates-io cr c6 crc64) #:use-module (crates-io))

(define-public crate-crc64-0.2.0 (c (n "crc64") (v "0.2.0") (h "0jgn9awzx7df3pidpf2nkc30vn55yan8prvj559hnwlz4cqb3mmj")))

(define-public crate-crc64-0.2.1 (c (n "crc64") (v "0.2.1") (h "0x2nb7v4jchhrbgq1r72p05pdqfxcx7agw1vs8nbaa2cq1mh7f1i")))

(define-public crate-crc64-0.2.2 (c (n "crc64") (v "0.2.2") (h "008yx18v4fnv22x8m7p8sayyhpjqnc0a6sdmq3sdcswv9xrnsf0j")))

(define-public crate-crc64-1.0.0 (c (n "crc64") (v "1.0.0") (h "0469vp0q9431pqx1236g60if5q3xyxpv4h14smkd45dfzsa6aqjm")))

(define-public crate-crc64-2.0.0 (c (n "crc64") (v "2.0.0") (h "1wwqdss36dmhz4fd0wynlaig463l4dwvr21db1fvf6aypapy61r7")))

