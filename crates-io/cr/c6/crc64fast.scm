(define-module (crates-io cr c6 crc64fast) #:use-module (crates-io))

(define-public crate-crc64fast-0.1.0 (c (n "crc64fast") (v "0.1.0") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0633av1fqz0mkc7j60praw0wi85ff3d1gzv8vi8xvjm7w06m30ha")))

(define-public crate-crc64fast-1.0.0 (c (n "crc64fast") (v "1.0.0") (d (list (d (n "crc") (r "^1") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1gc2ibmx2rryk1c6hpa140di7n5065g1d0z5c6d72k4jaz2i0gdx") (f (quote (("pmull") ("fake-simd"))))))

(define-public crate-crc64fast-1.1.0 (c (n "crc64fast") (v "1.1.0") (d (list (d (n "crc") (r "^3") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qi9ah3j83r58l2pikmqxmyk4zjbsq9jg7h0y3y1wa90xbn95fr6") (f (quote (("pmull") ("fake-simd")))) (r "1.70.0")))

