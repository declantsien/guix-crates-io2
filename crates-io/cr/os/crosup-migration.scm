(define-module (crates-io cr os crosup-migration) #:use-module (crates-io))

(define-public crate-crosup-migration-0.1.0 (c (n "crosup-migration") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.0") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "1z11zd9g74wxhcf9g17yd980712b13hmjfvjjyafvj77jsi5pqx5")))

