(define-module (crates-io cr os crosis) #:use-module (crates-io))

(define-public crate-crosis-0.0.1 (c (n "crosis") (v "0.0.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)))) (h "10rhv4v382gs85blk0a14n7fcm3ga7bv0b8h6767qkiz4pys5bzj") (y #t)))

(define-public crate-crosis-0.1.0 (c (n "crosis") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "replit-protocol") (r "^0.2") (d #t) (k 0)))) (h "1maw367yhvbgvfb0wyy18r16chfww4881brzidrwmqcjkag30mpg")))

