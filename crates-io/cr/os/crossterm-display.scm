(define-module (crates-io cr os crossterm-display) #:use-module (crates-io))

(define-public crate-crossterm-display-0.1.0 (c (n "crossterm-display") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0y9ny68rxpwmh8bpgarsinqi882ifpqhxkkzmhfs3zf7nhhxfvsd")))

(define-public crate-crossterm-display-0.1.1 (c (n "crossterm-display") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1fn8wr622674p3rs3rawfz8a9v69bsf8srdm2l66mfs3vb3hjd8m")))

(define-public crate-crossterm-display-0.1.2 (c (n "crossterm-display") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0rpjybl1k0spn1zjqbjn3l2wagl70x5vfjygmyyyp8z9x04vd8pi")))

