(define-module (crates-io cr os crossyword) #:use-module (crates-io))

(define-public crate-crossyword-0.1.0 (c (n "crossyword") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fgv8c5zcb7fbj1akzlx6yvzzc8sm0nrcqz2d588r0147nnydikm")))

(define-public crate-crossyword-0.1.1 (c (n "crossyword") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16mm7h4wi5qp2s2wj83436hqwbmygrmlv72pc9jm04sb6j5vda2a")))

(define-public crate-crossyword-0.2.0 (c (n "crossyword") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "03l95zpb9m88l7dlhqpc85bh9py7ngicsnc5q38vkr5px39gp54a")))

