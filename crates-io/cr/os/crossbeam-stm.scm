(define-module (crates-io cr os crossbeam-stm) #:use-module (crates-io))

(define-public crate-crossbeam-stm-0.3.0 (c (n "crossbeam-stm") (v "0.3.0") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)))) (h "03zs6sx4n4n4xsk6wlrhz1ardvwjjp6y18ip34n72d0ywzpjwhpn")))

(define-public crate-crossbeam-stm-0.3.1 (c (n "crossbeam-stm") (v "0.3.1") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "1z6rwwg5swi57v7q6dcywcin2kimcbl3r6a1chdk2vqv5a7c5i0l") (y #t)))

(define-public crate-crossbeam-stm-0.4.0 (c (n "crossbeam-stm") (v "0.4.0") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "0gvskp66d38aqp5cgkq6sna65h7x5n839w4h030g9bynbmhcm2qm") (y #t)))

(define-public crate-crossbeam-stm-0.5.0 (c (n "crossbeam-stm") (v "0.5.0") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "092k1gz926hgxrny5fcrrh44z8gpqq7vwaqf3w8bbh82qf5cn3gd") (y #t)))

(define-public crate-crossbeam-stm-0.6.0 (c (n "crossbeam-stm") (v "0.6.0") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "1i730g8gdc6d7wmr598chhn3gnnyqfg2v7sim7jkisxxllpnyvp1") (y #t)))

(define-public crate-crossbeam-stm-0.6.1 (c (n "crossbeam-stm") (v "0.6.1") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "1bnbxb723zh1409z7ji57k761ccnp7ajg98ydvsi9sb6i4l4xmwz") (y #t)))

