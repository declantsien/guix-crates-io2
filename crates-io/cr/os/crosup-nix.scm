(define-module (crates-io cr os crosup-nix) #:use-module (crates-io))

(define-public crate-crosup-nix-0.1.0 (c (n "crosup-nix") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "nix-editor") (r "^0.3.0") (d #t) (k 0)))) (h "15qmzja9ki1kqyd88sgnwi9xp5hwdrj703nz4whlmagasgd1q5z7")))

(define-public crate-crosup-nix-0.1.1 (c (n "crosup-nix") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "nix-editor") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("h1-client-rustls"))) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1fsrzzh5phwydf0mzrm319dr64njxs3qn2m05zzwni6gyb91s9yi")))

