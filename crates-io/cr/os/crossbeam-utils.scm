(define-module (crates-io cr os crossbeam-utils) #:use-module (crates-io))

(define-public crate-crossbeam-utils-0.1.0 (c (n "crossbeam-utils") (v "0.1.0") (h "1cg6shp2q89v0afy0y34p7dlm3mwm9vp6zmv30q4i37c0fvpm5s8")))

(define-public crate-crossbeam-utils-0.2.0 (c (n "crossbeam-utils") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "12xvq1mpzpp9abs6np7yibaz3f5k99jip3bd0l6068f7v2ap696q") (f (quote (("nightly"))))))

(define-public crate-crossbeam-utils-0.2.1 (c (n "crossbeam-utils") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "19vv6wd8cvjbpip1jk28226rf2bgn6x1cz7s5al4ixk92q0dp34w") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.2.2 (c (n "crossbeam-utils") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1n8qr52sw9y6yxzyfxi1phh55rsxms7ry4iipdd8vmd16ag8jq17") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.3.0 (c (n "crossbeam-utils") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1sx9fq90kzf7cilnqpmmij0q0lp5qp3hwqyvl6s509p04illclki") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.3.1 (c (n "crossbeam-utils") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "08h6jd92hf86wdf4gyaic772rs0d328v6v8v154spf24gvpafabh") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.3.2 (c (n "crossbeam-utils") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0yrw4l3mqc64wv2x1nvac84l1jqxybwcxynkzzbhkd61pjrshdnn") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.4.0 (c (n "crossbeam-utils") (v "0.4.0") (h "10wp0k5fdayhi1pdk7r8zsqqr4f3gvfx8hxm16cp857d882247xp") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.4.1 (c (n "crossbeam-utils") (v "0.4.1") (h "0jk8iy9gbaisja1sp8nvylvmj4hnjxfaf34xyggnrncrdargllpa") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.5.0 (c (n "crossbeam-utils") (v "0.5.0") (h "05b00dr1srl94d7jkalhwxdvpkaby2f8p3m37y8jpgg82wx4azb7") (f (quote (("use_std") ("nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-utils-0.6.0 (c (n "crossbeam-utils") (v "0.6.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0bc5vavfjvqjz2hqb80n7q9zjs6m341iwaj45n32mkscxw7a5zbf") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crossbeam-utils-0.6.1 (c (n "crossbeam-utils") (v "0.6.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "05hql2s6k2b37fzr20l20ihc3s30lg01hy9h1rail5wr4z616nf5") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crossbeam-utils-0.6.2 (c (n "crossbeam-utils") (v "0.6.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0wjwapavynv9yvrxsl09r57njpllgvjzmg7h4d94f9r845aw2zz0") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crossbeam-utils-0.6.3 (c (n "crossbeam-utils") (v "0.6.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "06p21qcbf93428dh4q1lnhgwwabl21ynyds443jn0w3ryij4ivj1") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crossbeam-utils-0.6.4 (c (n "crossbeam-utils") (v "0.6.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0ng8bc4z4a0ds81mnlkspa2mixfl9crbnysk53lrqvvn49hci3dk") (f (quote (("std" "lazy_static") ("nightly") ("default" "std"))))))

(define-public crate-crossbeam-utils-0.6.5 (c (n "crossbeam-utils") (v "0.6.5") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0p5aa8k3wpsn17md4rx038ac2azm9354knbxdfvn7dd7yk76yc7q") (f (quote (("std" "lazy_static") ("nightly") ("default" "std"))))))

(define-public crate-crossbeam-utils-0.6.6 (c (n "crossbeam-utils") (v "0.6.6") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1rk0r9n04bmq4a3g2q5qhvvlmrmx780gc6h9lmc94mwndslkz5q4") (f (quote (("std" "lazy_static") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-crossbeam-utils-0.7.0 (c (n "crossbeam-utils") (v "0.7.0") (d (list (d (n "autocfg") (r "^0.1.6") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1x1rn35q2v05qif14ijfg7800d3rf3ji2cg79awnacfw5jq6si6f") (f (quote (("std" "lazy_static") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-crossbeam-utils-0.7.1 (c (n "crossbeam-utils") (v "0.7.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0ng136f4s5wzvh1kahp5knyidg25476g2vkna7iyjj8z3ml8ppdx") (f (quote (("std" "lazy_static") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-crossbeam-utils-0.7.2 (c (n "crossbeam-utils") (v "0.7.2") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1a31wbrda1320gj2a6az1lin2d34xfc3xf88da4c17qy5lxcgiy3") (f (quote (("std" "lazy_static") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-crossbeam-utils-0.8.0 (c (n "crossbeam-utils") (v "0.8.0") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "const_fn") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "199ywwmkg60kqavhw8rhy9wybsfjr9p5czinhq56jprmk06m94gc") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-crossbeam-utils-0.8.1 (c (n "crossbeam-utils") (v "0.8.1") (d (list (d (n "autocfg") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "cfg-if") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 2)))) (h "13fvrqlap7bgvlnpqr5gjcxdhx1jv99pkfg5xdlq5xcy30g6vn82") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-crossbeam-utils-0.8.2 (c (n "crossbeam-utils") (v "0.8.2") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.4") (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qqzffa93m9mf8s8kj5mmn93dh6hyakl4y3axdn5m3szhclg7s5s") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-crossbeam-utils-0.8.3 (c (n "crossbeam-utils") (v "0.8.3") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.4") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0j9cjldgd1x6l8lc8kvqryw35fny9iix9in5k7zfya0lm6gxksg7") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-crossbeam-utils-0.8.4 (c (n "crossbeam-utils") (v "0.8.4") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0y52m693x5s65z4f54r35ak6m0gmxic8x4hmxldghsjd1lgj7ssg") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-crossbeam-utils-0.8.5 (c (n "crossbeam-utils") (v "0.8.5") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ny481cx8a5pdknypb41jqym03dl8x26i2ldyyp3yb3zrq8zqb6q") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-crossbeam-utils-0.8.6 (c (n "crossbeam-utils") (v "0.8.6") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0841rd6wgj7nrrhwp7kyclh4x0k9q4zc7cfd9bk4gy9lvczf1jng") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (y #t) (r "1.36")))

(define-public crate-crossbeam-utils-0.8.7 (c (n "crossbeam-utils") (v "0.8.7") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "19hn7av0w65y894ab8aw42d20i9m5d4sb80nm0zm6sf2y78vxrdm") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (r "1.36")))

(define-public crate-crossbeam-utils-0.8.8 (c (n "crossbeam-utils") (v "0.8.8") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0f6b3xrbyc3yx0qa1digmy48mxmh58359kv34qy6ws5p433j9w8b") (f (quote (("std" "lazy_static") ("nightly") ("default" "std")))) (r "1.36")))

(define-public crate-crossbeam-utils-0.8.9 (c (n "crossbeam-utils") (v "0.8.9") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0y3rr0rl308abzagc29fqffdk67fg8yd0lj0amm2i1vpjn0gkwcg") (f (quote (("std" "once_cell") ("nightly") ("default" "std")))) (r "1.36")))

(define-public crate-crossbeam-utils-0.8.10 (c (n "crossbeam-utils") (v "0.8.10") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "10vz3b5p5ksfmy2l3mndgqs4371sb7kra90jq89brmrlrq8fx0kx") (f (quote (("std" "once_cell") ("nightly") ("default" "std")))) (r "1.36")))

(define-public crate-crossbeam-utils-0.8.11 (c (n "crossbeam-utils") (v "0.8.11") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "1g426qw2j7czkbg0vw6mzifhgy1ng4qgpp2sn4vlamkvvi57v22i") (f (quote (("std" "once_cell") ("nightly") ("default" "std")))) (r "1.38")))

(define-public crate-crossbeam-utils-0.8.12 (c (n "crossbeam-utils") (v "0.8.12") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "1b0zs5ahnwkgky7svwah9fhmqx645qnb3h97cnk6q68zzb2zxfpd") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.38")))

(define-public crate-crossbeam-utils-0.8.13 (c (n "crossbeam-utils") (v "0.8.13") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "1al6zpi4f33b4nb6lb7y5v75jjvsv0sfi8cy8vn404mg4kkj6bs2") (f (quote (("std") ("nightly") ("default" "std")))) (y #t) (r "1.38")))

(define-public crate-crossbeam-utils-0.8.14 (c (n "crossbeam-utils") (v "0.8.14") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "17wjbnlj4whbdvc1syk2gfy8maqx01sg2hmqpdnjh9l7g7x6ddsg") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.38")))

(define-public crate-crossbeam-utils-0.8.15 (c (n "crossbeam-utils") (v "0.8.15") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0jwq8srmjcwvq9q883k9zyb26qqznaj4jjqdxmvw7xcmrkc3q1iw") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.38")))

(define-public crate-crossbeam-utils-0.8.16 (c (n "crossbeam-utils") (v "0.8.16") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "153j0gikblz7n7qdvdi8pslhi008s1yp9cmny6vw07ad7pbb48js") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.38")))

(define-public crate-crossbeam-utils-0.8.17 (c (n "crossbeam-utils") (v "0.8.17") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.7.1") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "13y7wh993i7q71kg6wcfj65w3rlmizzrz7cqgz1l9whlgw9rcvf0") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.61")))

(define-public crate-crossbeam-utils-0.8.18 (c (n "crossbeam-utils") (v "0.8.18") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "loom") (r "^0.7.1") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "176kcvw428zj024i60kd5jsqvli0y3khxac4ylk4gn7bf2kk1963") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.60")))

(define-public crate-crossbeam-utils-0.8.19 (c (n "crossbeam-utils") (v "0.8.19") (d (list (d (n "loom") (r "^0.7.1") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0iakrb1b8fjqrag7wphl94d10irhbh2fw1g444xslsywqyn3p3i4") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.60")))

(define-public crate-crossbeam-utils-0.8.20 (c (n "crossbeam-utils") (v "0.8.20") (d (list (d (n "loom") (r "^0.7.1") (o #t) (d #t) (t "cfg(crossbeam_loom)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "100fksq5mm1n7zj242cclkw6yf7a4a8ix3lvpfkhxvdhbda9kv12") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.60")))

