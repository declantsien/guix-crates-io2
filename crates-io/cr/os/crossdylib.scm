(define-module (crates-io cr os crossdylib) #:use-module (crates-io))

(define-public crate-crossdylib-1.0.0 (c (n "crossdylib") (v "1.0.0") (d (list (d (n "atomic_refcell") (r "^0") (d #t) (k 0)) (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "findshlibs") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 0)))) (h "16qv6ibn7jrw2f0777dag9z6cpknm1zh10k1j99yn4s02xv48072")))

(define-public crate-crossdylib-1.0.1 (c (n "crossdylib") (v "1.0.1") (d (list (d (n "atomic_refcell") (r "^0") (d #t) (k 0)) (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "findshlibs") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 0)))) (h "1i6rybl6bxh3223ny465aljfwsy03fx9bp34v8bw52pknw719k0f")))

(define-public crate-crossdylib-1.0.2 (c (n "crossdylib") (v "1.0.2") (d (list (d (n "atomic_refcell") (r "^0") (d #t) (k 0)) (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "findshlibs") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 0)))) (h "02mqikd18xjp5abnavw2i8fxbgnbsrky75dvc9awjilfqn8mmxkm")))

(define-public crate-crossdylib-1.1.0 (c (n "crossdylib") (v "1.1.0") (d (list (d (n "atomic_refcell") (r "^0") (d #t) (k 0)) (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "findshlibs") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 0)))) (h "19gm57q8ifrkhzimczzdcpwidqh1xknkfzcairjq5as0hwkbail0")))

(define-public crate-crossdylib-2.0.0 (c (n "crossdylib") (v "2.0.0") (d (list (d (n "atomic_refcell") (r "^0") (d #t) (k 0)) (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "findshlibs") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 0)))) (h "17yic7bnyp69w72j33f2fjbppr7clswpv3z91bbkbhd81xsia2yc")))

(define-public crate-crossdylib-2.1.0 (c (n "crossdylib") (v "2.1.0") (d (list (d (n "atomic_refcell") (r "^0") (d #t) (k 0)) (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "findshlibs") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 0)))) (h "1h6qzl1myi1g4zh10lvic38d1z5a5l47ncb78dcwx6xr3w4l2vjx")))

(define-public crate-crossdylib-3.0.0 (c (n "crossdylib") (v "3.0.0") (d (list (d (n "atomic_refcell") (r "^0") (d #t) (k 0)) (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "findshlibs") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 0)))) (h "03affjnwaaam7d1cxqs8w8pmwbnpm7a93sfv5vk53kigfhrlwd71")))

