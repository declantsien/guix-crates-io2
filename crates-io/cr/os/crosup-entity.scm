(define-module (crates-io cr os crosup-entity) #:use-module (crates-io))

(define-public crate-crosup-entity-0.1.0 (c (n "crosup-entity") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "sea-orm") (r "^0.11.3") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "04zx8wfh6r6ymc58hckbj25i0ridr2896hm4l8ar8qw62nj2wj94")))

