(define-module (crates-io cr os crostab) #:use-module (crates-io))

(define-public crate-crostab-0.0.1 (c (n "crostab") (v "0.0.1") (d (list (d (n "num") (r ">=0.4.0") (d #t) (k 2)) (d (n "serde") (r ">=1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0") (d #t) (k 2)) (d (n "spare") (r ">=0.0.1") (d #t) (k 2)) (d (n "veho") (r ">=0.0.16") (d #t) (k 0)))) (h "1ks17zsj0ic0pb68s5hhdhkb4c28b9wp9b6bypfacxx14lbvdr0a")))

(define-public crate-crostab-0.0.2 (c (n "crostab") (v "0.0.2") (d (list (d (n "num") (r ">=0.4.0") (d #t) (k 2)) (d (n "serde") (r ">=1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0") (d #t) (k 2)) (d (n "spare") (r ">=0.0.1") (d #t) (k 2)) (d (n "veho") (r ">=0.0.17") (d #t) (k 0)))) (h "1d4m5mj7vjmm614m6l6jmd31501279qwpsxs6fc87qfaymg73hlv")))

