(define-module (crates-io cr os crossclip) #:use-module (crates-io))

(define-public crate-crossclip-0.7.0 (c (n "crossclip") (v "0.7.0") (d (list (d (n "clipboard-win") (r "^3.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "x11-clipboard") (r "^0.5.1") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"openbsd\"))") (k 0)))) (h "04bl7pf8aab0ndhscji8z7nvjq66kkhv9v674q6g47z8xlpsl00m")))

(define-public crate-crossclip-0.7.1 (c (n "crossclip") (v "0.7.1") (d (list (d (n "clipboard-win") (r "^4.4.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "x11-clipboard") (r "^0.6.1") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"openbsd\"))") (k 0)))) (h "02zwdmf9ppjwyg411cvwq1hf3ip2i04kdphsnm0wbgy5i36bih8k")))

