(define-module (crates-io cr os crossbow-admob) #:use-module (crates-io))

(define-public crate-crossbow-admob-0.1.5 (c (n "crossbow-admob") (v "0.1.5") (d (list (d (n "crossbow-android") (r "^0.1.5") (d #t) (k 0)))) (h "0hr05qrgj6ygmssaa9bbl4vn1qjh3sqhfkq0d9vs0difricxw3p4")))

(define-public crate-crossbow-admob-0.1.6 (c (n "crossbow-admob") (v "0.1.6") (d (list (d (n "crossbow-android") (r "^0.1.6") (d #t) (k 0)))) (h "02ir9m1x4rdkk6jwr31igq8g3y7rwz8ssis5cb6bizbkv57872ff")))

(define-public crate-crossbow-admob-0.1.7 (c (n "crossbow-admob") (v "0.1.7") (d (list (d (n "crossbow-android") (r "^0.1.7") (d #t) (k 0)))) (h "06v8ax2412vr9ds2axk1ximd8sp4zbg2151hnxn1dq37ah1b7w6a")))

