(define-module (crates-io cr os crosstermine) #:use-module (crates-io))

(define-public crate-crosstermine-0.1.0 (c (n "crosstermine") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prayterm") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19cmz6r7sx1cp8z4pvfid22x0qi064j4aqmhh47qpij41fqw4nm5") (y #t)))

(define-public crate-crosstermine-0.1.1 (c (n "crosstermine") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prayterm") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a897h26yyi79h3klnvipbn6b9r1wqprxzfgn56dnr8rr666rir8") (y #t)))

(define-public crate-crosstermine-0.1.2 (c (n "crosstermine") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04h7f3kjd4n3ydwwr09cfbjfmmqkx2ia30lpgnfr9z0hc1pcl26q") (y #t)))

(define-public crate-crosstermine-0.2.0 (c (n "crosstermine") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "minefield") (r "^0.1") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h1dm66c9az123g2sc7bshvbcx08pxcwrb4cf56xc4dkp7naqipq") (y #t)))

(define-public crate-crosstermine-0.3.0 (c (n "crosstermine") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "minefield") (r "^0.1") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12bynr8iaky4wrzzly2rj50wa62f9gnxzzlcdgm3qndib71dclii") (y #t)))

(define-public crate-crosstermine-0.3.1 (c (n "crosstermine") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "minefield") (r "^0.1") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08r4wk1lwi5x0z9k64afxdd1g564qgd75cfjz089fgghc075iig6") (y #t)))

(define-public crate-crosstermine-3.1.0 (c (n "crosstermine") (v "3.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "minefield") (r "^3.1") (d #t) (k 0)) (d (n "mvc-rs") (r "^0.1") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08a2ff362v8qk9174dikybsxaz67s7ai3dqlcnpzmpjbwgk39bk3") (y #t)))

(define-public crate-crosstermine-3.2.0 (c (n "crosstermine") (v "3.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "minefield") (r "^3.2") (d #t) (k 0)) (d (n "mvc-rs") (r "^0.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hicb1664cimk651wi7hhi2xya65lipvnyyldyz95x8pzjwdrjpl") (y #t)))

(define-public crate-crosstermine-3.3.0 (c (n "crosstermine") (v "3.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "minefield") (r "^3.3") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lgx7yf52xgc3vdaymcz5wkh3p4zzf7z875mzfgfmvb5wyvvyal6")))

