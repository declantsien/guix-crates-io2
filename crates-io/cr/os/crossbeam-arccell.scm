(define-module (crates-io cr os crossbeam-arccell) #:use-module (crates-io))

(define-public crate-crossbeam-arccell-0.6.2 (c (n "crossbeam-arccell") (v "0.6.2") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "1b3bf153629p3cm9280ns1qzl3asa6aqzhk5gbjjngjz9a30z11a") (y #t)))

(define-public crate-crossbeam-arccell-0.6.3 (c (n "crossbeam-arccell") (v "0.6.3") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "0zb6vd5c3haamb0kdn4j81l34q4r6vxyc37m3sr0cn3vfrs11a16") (y #t)))

(define-public crate-crossbeam-arccell-0.6.4 (c (n "crossbeam-arccell") (v "0.6.4") (d (list (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 2)))) (h "1n6823nxa0c3p07g9aiakrlfjz0pr56pldnx8afhd7cy6kk89mii") (y #t)))

