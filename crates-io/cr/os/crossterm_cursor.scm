(define-module (crates-io cr os crossterm_cursor) #:use-module (crates-io))

(define-public crate-crossterm_cursor-0.1.0 (c (n "crossterm_cursor") (v "0.1.0") (d (list (d (n "crossterm_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10lffbbpkz2qxqngz6kfncx7k0hm9n2lmhxxpmgxi2vh47jja779")))

(define-public crate-crossterm_cursor-0.2.0 (c (n "crossterm_cursor") (v "0.2.0") (d (list (d (n "crossterm_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "051jdibzk9cdphyp23iqlvryda3dqjxgfsqmvwdnn14sl9pmzc1m")))

(define-public crate-crossterm_cursor-0.2.1 (c (n "crossterm_cursor") (v "0.2.1") (d (list (d (n "crossterm_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14m09i18cpr2r54yjbzak5m9l643nsirc42rjpsljc2hjwj34prf")))

(define-public crate-crossterm_cursor-0.2.2 (c (n "crossterm_cursor") (v "0.2.2") (d (list (d (n "crossterm_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qyqglwr1mjdi6kndig3zhr2l6hq96d0b2irv4f4ir2aj5iamf3f")))

(define-public crate-crossterm_cursor-0.2.3 (c (n "crossterm_cursor") (v "0.2.3") (d (list (d (n "crossterm_utils") (r "^0.2.3") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xj7bhgf6y40mbirp9d084kyns7ha6w1x7m4vqd140k4yfvq70iz")))

(define-public crate-crossterm_cursor-0.2.4 (c (n "crossterm_cursor") (v "0.2.4") (d (list (d (n "crossterm_utils") (r "^0.2.3") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gcdw8nny1ybzim12wp8is0srkjlfx4yk7ilgphgpykvjd1xp3ab")))

(define-public crate-crossterm_cursor-0.2.5 (c (n "crossterm_cursor") (v "0.2.5") (d (list (d (n "crossterm_utils") (r "^0.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qsfyw8dyg5kparjmh0hcbnz82xkld6p2ilax6rnr7aaw3z5w8qh")))

(define-public crate-crossterm_cursor-0.2.6 (c (n "crossterm_cursor") (v "0.2.6") (d (list (d (n "crossterm_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p33mwpijrhr1mss3nz8bm31wcxlsd802gwls9n3xn0pbw4gsjzv")))

(define-public crate-crossterm_cursor-0.3.0 (c (n "crossterm_cursor") (v "0.3.0") (d (list (d (n "crossterm_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zik377dcnl82vb37h8wwgfp1nnrmn4bsm0x0prc9fdzjzwhzviq")))

(define-public crate-crossterm_cursor-0.3.1 (c (n "crossterm_cursor") (v "0.3.1") (d (list (d (n "crossterm_utils") (r "^0.3.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0isz2dajm6j5dyrxaqf52sd89j8p7bf8f44bxign16zfvqx762rw")))

(define-public crate-crossterm_cursor-0.4.0 (c (n "crossterm_cursor") (v "0.4.0") (d (list (d (n "crossterm_input") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon" "winnt" "minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ys2m2gpr15j9bsn9s77ibfm0izazdlwqnzr8c70i8scr3a713n4")))

