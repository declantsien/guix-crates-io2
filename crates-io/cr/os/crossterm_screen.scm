(define-module (crates-io cr os crossterm_screen) #:use-module (crates-io))

(define-public crate-crossterm_screen-0.1.0 (c (n "crossterm_screen") (v "0.1.0") (d (list (d (n "crossterm_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sczrn8796wb0jm8zk9qisrdz3wcifqpqrn82md9psjzghz1vss8")))

(define-public crate-crossterm_screen-0.2.0 (c (n "crossterm_screen") (v "0.2.0") (d (list (d (n "crossterm_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02hd610f4s0hkj9nfwgcd77z4fvbh2i64dkxqwl3ww0bw7xi0p6h")))

(define-public crate-crossterm_screen-0.2.1 (c (n "crossterm_screen") (v "0.2.1") (d (list (d (n "crossterm_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0irrqprc353ads9n0z2989zpq38sz00m0wy89faqhxh9z46l794m")))

(define-public crate-crossterm_screen-0.2.2 (c (n "crossterm_screen") (v "0.2.2") (d (list (d (n "crossterm_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sjvdrkai9n4kvii1fmqq5y848fmw22aaj0h5sa0qwbbwzndagyx")))

(define-public crate-crossterm_screen-0.2.3 (c (n "crossterm_screen") (v "0.2.3") (d (list (d (n "crossterm_utils") (r "^0.2.3") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rbdm92f35bqr8hdjw7bx5wh789j3smxwd6yvs1sarvq3ngrp24h")))

(define-public crate-crossterm_screen-0.2.4 (c (n "crossterm_screen") (v "0.2.4") (d (list (d (n "crossterm_utils") (r "^0.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dw33lcgv85jijp7ciksfj987dgrvri9lmai6isbsr593li14wcm")))

(define-public crate-crossterm_screen-0.2.5 (c (n "crossterm_screen") (v "0.2.5") (d (list (d (n "crossterm_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xkv1ynf3l8693zbavjdll7f2x6fjw5zq6n42kaj4k639x499whb")))

(define-public crate-crossterm_screen-0.3.0 (c (n "crossterm_screen") (v "0.3.0") (d (list (d (n "crossterm_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12cblzm09z42yqnv5m6zcmmz6ws57jga1wvb9zk9v4a3dar2rkzf")))

(define-public crate-crossterm_screen-0.3.1 (c (n "crossterm_screen") (v "0.3.1") (d (list (d (n "crossterm_utils") (r "^0.3.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jqp6vw45cqn4yx8w30yqrsaqv0abalrhshs2rn70qrdh5ldcd5l")))

(define-public crate-crossterm_screen-0.3.2 (c (n "crossterm_screen") (v "0.3.2") (d (list (d (n "crossterm_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("minwindef" "wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dwxs60qx5089gy7h034zagzs5cg50g4bmlaqk1acqnyzakzp3d0")))

