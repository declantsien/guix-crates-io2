(define-module (crates-io cr os cross-platform-service) #:use-module (crates-io))

(define-public crate-cross-platform-service-0.1.0 (c (n "cross-platform-service") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "signal-hook") (r "^0.3.9") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (t "cfg(windows)") (k 1)))) (h "02711zn4csi1wmf7adgnkpbikfx4bxifrp3bgvfsgsnkblsyc6w5")))

