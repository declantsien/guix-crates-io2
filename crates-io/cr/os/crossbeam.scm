(define-module (crates-io cr os crossbeam) #:use-module (crates-io))

(define-public crate-crossbeam-0.1.0 (c (n "crossbeam") (v "0.1.0") (h "0s0ylyyad6j798fp4fsls246q8dxnyxkh5j7jswr50dwvi1wf078")))

(define-public crate-crossbeam-0.1.1 (c (n "crossbeam") (v "0.1.1") (h "0icy9xl82gr4j34nwvqv94fm4gk3isa8l7i7sm7hvzwvw8pljal6")))

(define-public crate-crossbeam-0.1.2 (c (n "crossbeam") (v "0.1.2") (h "060n8iy8ghf71cayz02w664v9sspwb22xrf5936cskp44ixlgzal")))

(define-public crate-crossbeam-0.1.3 (c (n "crossbeam") (v "0.1.3") (h "1ivxg0yqs9yvz9cajrwm12k0djirps80nkp3ynzhsfvj7y4yy6ly")))

(define-public crate-crossbeam-0.1.4 (c (n "crossbeam") (v "0.1.4") (h "02d98x8a438qx53nlm8zlls0jbpx55zp1nm89g8j3g0q6n1kbl50")))

(define-public crate-crossbeam-0.1.5 (c (n "crossbeam") (v "0.1.5") (h "1xg06rmsxis75jq72cshlf3k0hya7impf4afy0r4anvs8y1lwvpv") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.1.6 (c (n "crossbeam") (v "0.1.6") (h "0rb4g1sm350drsc2na6p6x7g5kj3jdxnn23zwdxbpajwpkzi15hi") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.0 (c (n "crossbeam") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1x60nb9fp0nwb603q9kxwldpsdyz6140kvnqw41yqlgzyh9scdi9") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.1 (c (n "crossbeam") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1qbvffwis07ns5z7shpc00rsd07f4fw3544zh4cwc2irxn02m01q") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.2 (c (n "crossbeam") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0yhd0l6ls4372vd6zhcm708vw5c0gcglmmh54vndxnx5qs9ad7kq") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.3 (c (n "crossbeam") (v "0.2.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0a5rx98fhsxlhmq2d28b9qg8crvf8s460z4lh6vcyna6haycv39i") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.4 (c (n "crossbeam") (v "0.2.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "10f4v5mdxri88azsvadlliasz3zgxc7shxa7k7j3vq343d729717") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.5 (c (n "crossbeam") (v "0.2.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0sk1fh7i32l6dai0hfr497im8piyamxa4lxf9fac8gwsqg3mi9a3") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.6 (c (n "crossbeam") (v "0.2.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0071vjcb4rff232hbw3knkrkm601z48818qsz25p10h1nrg5kxqj") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.7 (c (n "crossbeam") (v "0.2.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1ym3ail5q7wjxh7n15a0j3n4h1p34ifcb5ggn3mfpg43fh5jw6n6") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.8 (c (n "crossbeam") (v "0.2.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "184m2mvs9ss1bp30cj06pz4bjhgsh9gmg3n163y0zllkkz72i0il") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.9 (c (n "crossbeam") (v "0.2.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1k0i96pl7d3vpigyi92ykwlighbdn02hzh7skmghqfchbs1lz5zv") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.10 (c (n "crossbeam") (v "0.2.10") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "15wga0kvk3iqf3l077957j931brf1pl3p74xibd698jccqas4phc") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.3.0 (c (n "crossbeam") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "02msr29qa2dvg36fhmlg3iy9brw385zvqi7d22vca0ijafbandw8") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.11 (c (n "crossbeam") (v "0.2.11") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0j5vfhhcsxrv6xcjwii0smkvwrhz1kzrrzx8yab2dzhii0kv4c7d") (f (quote (("nightly")))) (y #t)))

(define-public crate-crossbeam-0.3.1 (c (n "crossbeam") (v "0.3.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "00hmrs77fwfg0szm8wndsfsnm32whnnqwn2wskqq0v0ik4vd52s5") (f (quote (("nightly")))) (y #t)))

(define-public crate-crossbeam-0.3.2 (c (n "crossbeam") (v "0.3.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "06ga3l3lavn1vya3r6v4rg08y6m2ccc4qskacis3difmsj19gki4") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.2.12 (c (n "crossbeam") (v "0.2.12") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1gmrayc93lygb8v62bj0c5zwyflvj5bli7ari650k259nlyncrmx") (f (quote (("nightly"))))))

(define-public crate-crossbeam-0.4.0 (c (n "crossbeam") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0mqdpwppm6wif7s0sw3im9k9h145s0drg7gh999zxiqhna55dxmb") (f (quote (("use_std" "crossbeam-epoch/use_std" "crossbeam-utils/use_std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-0.4.1 (c (n "crossbeam") (v "0.4.1") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0l1nsd3gs3fwfkz36d43fi18m6ldbw66g2zjj044hzxqn53q4h6p") (f (quote (("use_std" "crossbeam-epoch/use_std" "crossbeam-utils/use_std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "use_std"))))))

(define-public crate-crossbeam-0.5.0 (c (n "crossbeam") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.6") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0pqqgski5gw3ncf1qy0p728mqj91yifdf4m4yn9da0m2szr2zjfi") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "std"))))))

(define-public crate-crossbeam-0.6.0 (c (n "crossbeam") (v "0.6.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.4") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.6.3") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.7.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "156vywxq1ca6hkil89qf79an0235f0xpxcaw7ki0kyyr96kpwk5d") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "std"))))))

(define-public crate-crossbeam-0.7.0 (c (n "crossbeam") (v "0.7.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.7") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.0") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.7.1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1xfrpx72ban9v2ahkx71vkqzkbc4z7pq4gajzwn8y10pjyv8xvf0") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "std"))))))

(define-public crate-crossbeam-0.7.1 (c (n "crossbeam") (v "0.7.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.7") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.0") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.7.1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "03gy8gzjb42kr4pfbd4kzmmxdm866j1y745z42d9j4513h3r4i5i") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "std"))))))

(define-public crate-crossbeam-0.7.2 (c (n "crossbeam") (v "0.7.2") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.9") (o #t) (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.7.2") (k 0)) (d (n "crossbeam-queue") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.6") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0g5jysq5x4gndc1v5sq9n3f1m97k7qihwdpigw6ar6knj14qm09d") (f (quote (("std" "crossbeam-channel" "crossbeam-deque" "crossbeam-epoch/std" "crossbeam-queue" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc" "crossbeam-epoch/alloc" "crossbeam-utils/alloc"))))))

(define-public crate-crossbeam-0.7.3 (c (n "crossbeam") (v "0.7.3") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.8") (k 0)) (d (n "crossbeam-queue") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "13kzn2d49n2qn5q42y2dj48kyv6aln2d9smq8x9n675l3zzknck9") (f (quote (("std" "crossbeam-channel" "crossbeam-deque" "crossbeam-epoch/std" "crossbeam-queue" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc" "crossbeam-epoch/alloc" "crossbeam-utils/alloc"))))))

(define-public crate-crossbeam-0.8.0 (c (n "crossbeam") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (o #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8") (o #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9") (o #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "103xnwzkk1zd9kiy6f0f131ap433qfkc757wyrha5bxa7pmsc0gx") (f (quote (("std" "alloc" "crossbeam-channel/std" "crossbeam-deque/std" "crossbeam-epoch/std" "crossbeam-queue/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly" "crossbeam-queue/nightly") ("default" "std") ("alloc" "crossbeam-epoch/alloc" "crossbeam-queue/alloc"))))))

(define-public crate-crossbeam-0.8.1 (c (n "crossbeam") (v "0.8.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (o #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8") (o #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.5") (o #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.2") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ibq943sxbli0d03aihys3n4l9apy88d22z9759b0g1wdf7miraa") (f (quote (("std" "alloc" "crossbeam-channel/std" "crossbeam-deque/std" "crossbeam-epoch/std" "crossbeam-queue/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly" "crossbeam-queue/nightly") ("default" "std") ("alloc" "crossbeam-epoch/alloc" "crossbeam-queue/alloc"))))))

(define-public crate-crossbeam-0.8.2 (c (n "crossbeam") (v "0.8.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (o #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8") (o #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.5") (o #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.2") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0b0s0ans81ja6gm7awlaw3k2rqywzmhq4mm9ra8yaak16q6sy098") (f (quote (("std" "alloc" "crossbeam-channel/std" "crossbeam-deque/std" "crossbeam-epoch/std" "crossbeam-queue/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly" "crossbeam-queue/nightly") ("default" "std") ("alloc" "crossbeam-epoch/alloc" "crossbeam-queue/alloc")))) (r "1.38")))

(define-public crate-crossbeam-0.8.3 (c (n "crossbeam") (v "0.8.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.10") (o #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.4") (o #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.17") (o #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.10") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.18") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0849rji35blaljaygbr8ppqidnbm367vp77w6za413na35ci1fbf") (f (quote (("std" "alloc" "crossbeam-channel/std" "crossbeam-deque/std" "crossbeam-epoch/std" "crossbeam-queue/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly" "crossbeam-queue/nightly") ("default" "std") ("alloc" "crossbeam-epoch/alloc" "crossbeam-queue/alloc")))) (r "1.61")))

(define-public crate-crossbeam-0.8.4 (c (n "crossbeam") (v "0.8.4") (d (list (d (n "crossbeam-channel") (r "^0.5.10") (o #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.4") (o #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.17") (o #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.10") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.18") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1a5c7yacnk723x0hfycdbl91ks2nxhwbwy46b8y5vyy0gxzcsdqi") (f (quote (("std" "alloc" "crossbeam-channel/std" "crossbeam-deque/std" "crossbeam-epoch/std" "crossbeam-queue/std" "crossbeam-utils/std") ("nightly" "crossbeam-epoch/nightly" "crossbeam-utils/nightly" "crossbeam-queue/nightly") ("default" "std") ("alloc" "crossbeam-epoch/alloc" "crossbeam-queue/alloc")))) (r "1.61")))

