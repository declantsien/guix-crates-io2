(define-module (crates-io cr os crossbundle-derive) #:use-module (crates-io))

(define-public crate-crossbundle-derive-0.1.0 (c (n "crossbundle-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kl9vzrdbyrw1ik86zjlvrsad5cl2ljq3sv3mdlbdq44s1xgrhhc")))

(define-public crate-crossbundle-derive-0.1.1 (c (n "crossbundle-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05digsc9mr2hhxg5nx59xa2xzz4kbnqiv2h4ygmpqrwzx7khkwd3")))

(define-public crate-crossbundle-derive-0.1.2 (c (n "crossbundle-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ny8s0zn1s4sdhp4fp4vv2jyfpkrz5xas3zwvhcxnmmw2irq4ps7")))

(define-public crate-crossbundle-derive-0.1.3 (c (n "crossbundle-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mz49a2a79az6ybdcxz5fbc3s2mbx4gwnqhfq5rysfpnncq1y24x")))

