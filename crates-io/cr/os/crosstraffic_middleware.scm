(define-module (crates-io cr os crosstraffic_middleware) #:use-module (crates-io))

(define-public crate-crosstraffic_middleware-0.1.0 (c (n "crosstraffic_middleware") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.67") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (d #t) (k 0)) (d (n "transportations_library") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)))) (h "13wkcawn9r738jsxq41gz095dl5ij1rzmkgckwp5grw5dv9z52x5")))

