(define-module (crates-io cr os crosstown_bus) #:use-module (crates-io))

(define-public crate-crosstown_bus-0.1.0 (c (n "crosstown_bus") (v "0.1.0") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "153pgbnvdb9kbf80xb16qnjsa27yi45s4hn26ivp3jiks5i665jq") (y #t)))

(define-public crate-crosstown_bus-0.1.1 (c (n "crosstown_bus") (v "0.1.1") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "106kng81wdmb86sjkna7p8f9brbvj9vl1ph4dsafjz1slrhhhcfl") (y #t)))

(define-public crate-crosstown_bus-0.1.2 (c (n "crosstown_bus") (v "0.1.2") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0fgsg6d81xcnfk93r4lcsfqqyjfl7wc2sx1fsydn0q6j5bs7y2wv") (y #t)))

(define-public crate-crosstown_bus-0.1.3 (c (n "crosstown_bus") (v "0.1.3") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "09ny4hzlqjlbc6gzzp8s6rypyvqbylnhgdndlmjqg8r6g1djdqfq")))

(define-public crate-crosstown_bus-0.1.4 (c (n "crosstown_bus") (v "0.1.4") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0i8s7bl3zd0v9nzg694d5hlglg1g67jr3b60z9xcysdzqm9v4p63")))

(define-public crate-crosstown_bus-0.2.0 (c (n "crosstown_bus") (v "0.2.0") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "015xnwp0h3lahafpd6j35q8pjarwsp3ipmi61wvb353cpdw8cyrc")))

(define-public crate-crosstown_bus-0.2.1 (c (n "crosstown_bus") (v "0.2.1") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1aiijmlapax13h7qgi0a80r1l829gpnr4bh3pgc0v88wy4pmmgr3")))

(define-public crate-crosstown_bus-0.2.2 (c (n "crosstown_bus") (v "0.2.2") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0fhfigr84lz6vf4ic5saxhydym090ld8kp7xvdq4ycb7j397zg3x")))

(define-public crate-crosstown_bus-0.2.3 (c (n "crosstown_bus") (v "0.2.3") (d (list (d (n "amiquip") (r "^0.4") (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0v1p1hddj4c75z5ivxyz01cwqlrqqg263v97i2p73s2hhrnly65y")))

(define-public crate-crosstown_bus-0.3.0 (c (n "crosstown_bus") (v "0.3.0") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "091fj91fxgg9447p9xlgp2nhfcc20kwk0hwic02y2rq33jk3fkq7")))

(define-public crate-crosstown_bus-0.4.0 (c (n "crosstown_bus") (v "0.4.0") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rja6g95w86b9mpdbc63ynpxrnn2pqysnsm1h78c9kxcll9kwngg")))

(define-public crate-crosstown_bus-0.4.1 (c (n "crosstown_bus") (v "0.4.1") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01rkrgrx35xwdikjwl1xyhk5vwdzn7sk4rk66wh6l53zca24p2jc")))

(define-public crate-crosstown_bus-0.4.2 (c (n "crosstown_bus") (v "0.4.2") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0dczn67qz1m19v9vwgw4yq38rk065fww6b3ws62i6wl7dv005n0z")))

(define-public crate-crosstown_bus-0.4.3 (c (n "crosstown_bus") (v "0.4.3") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0y3ljff9q38czw8bhl5mkd2gpbqy66ylrcl77gxxl1czjhbkqlis")))

(define-public crate-crosstown_bus-0.4.5 (c (n "crosstown_bus") (v "0.4.5") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hkx0af584ar4hljxj1jf3hfm6vfm282c2m623g0nyy4djzhd88i")))

(define-public crate-crosstown_bus-0.4.6 (c (n "crosstown_bus") (v "0.4.6") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0lmmdbjsqmqx9kvzfddl8l1pgawkhyjidc26l0skr90mfaacdd4v")))

(define-public crate-crosstown_bus-0.5.0 (c (n "crosstown_bus") (v "0.5.0") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1c58c73why3lvanwf9blr6wfhiml5y1si5a76wc02ly8gdp5jbm5")))

(define-public crate-crosstown_bus-0.5.2 (c (n "crosstown_bus") (v "0.5.2") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bds8m1k5h156hyl2jvsha8srr57j3qzzb21jdy52pra9d4cdkqx")))

(define-public crate-crosstown_bus-0.5.3 (c (n "crosstown_bus") (v "0.5.3") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "18rbrs4z6pvnwzbv6bjz5q0ksbrfwndvxmsd0rj900higr9pnzy5")))

(define-public crate-crosstown_bus-0.5.4 (c (n "crosstown_bus") (v "0.5.4") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "13zl9jcv885d69r0xa9hwy7y3c55zh80x9ix7jbrkjx6ikznf181")))

(define-public crate-crosstown_bus-0.6.0 (c (n "crosstown_bus") (v "0.6.0") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qvf1aniqskh0pv7gd3wb96207bz0gjpgqsbqkm4m71214kp8hzg")))

(define-public crate-crosstown_bus-0.7.0 (c (n "crosstown_bus") (v "0.7.0") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08l7642irw7jrsgsjpx3q31n7zc4z97milrib83d3wlib77n0ghv")))

(define-public crate-crosstown_bus-0.7.1 (c (n "crosstown_bus") (v "0.7.1") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0y4gp0w2lyv1jmbnspfng6iqw6hkxgz1rh7nxxpvzp6dzjvipp1j")))

(define-public crate-crosstown_bus-0.8.0 (c (n "crosstown_bus") (v "0.8.0") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1nz4nx94fpc93q4zryxdbqkrc9a2vm0m3xpc2j49pg7flghi7gd1")))

(define-public crate-crosstown_bus-0.8.1 (c (n "crosstown_bus") (v "0.8.1") (d (list (d (n "amiquip") (r "^0.4.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0398lsllfrwm5vm7i7k9bq31y1c6zz3zg4kjsxnhfj7jva7bg3bz")))

(define-public crate-crosstown_bus-1.0.0 (c (n "crosstown_bus") (v "1.0.0") (d (list (d (n "amiquip") (r "^0.4.2") (d #t) (k 0)) (d (n "borsh") (r "^1.4.0") (d #t) (k 0)) (d (n "borsh-derive") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "028c8vlrgxkf08wj936wwnqga6hzdz28hgyg1m9hwgvwhlv1fylf")))

(define-public crate-crosstown_bus-1.0.1 (c (n "crosstown_bus") (v "1.0.1") (d (list (d (n "amiquip") (r "^0.4.2") (d #t) (k 0)) (d (n "borsh") (r "^1.4.0") (d #t) (k 0)) (d (n "borsh-derive") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02bycf6chhm4wp7sqa16z89ggfxz07q1j21mag5l4fn1kp4psyz0")))

(define-public crate-crosstown_bus-1.1.0 (c (n "crosstown_bus") (v "1.1.0") (d (list (d (n "amiquip") (r "^0.4.2") (d #t) (k 0)) (d (n "borsh") (r "^1.4.0") (d #t) (k 0)) (d (n "borsh-derive") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0msdnyxnrfnvr2zkjgkis0v0ghid5c87d6fmwrrj2h1vzqfrwsfb")))

