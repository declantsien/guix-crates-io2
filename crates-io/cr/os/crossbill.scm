(define-module (crates-io cr os crossbill) #:use-module (crates-io))

(define-public crate-crossbill-0.1.0 (c (n "crossbill") (v "0.1.0") (h "1xy1wfqm6lghnfwn5pradxsmyy7w8xaa41jvvmgmn95j5drjh6cl")))

(define-public crate-crossbill-0.1.1 (c (n "crossbill") (v "0.1.1") (h "1pjmz1frwl9ynairl30vcs0qi2npzqjyz0vhbi0qsnnz8xg18nby")))

