(define-module (crates-io cr os crossterm_winapi) #:use-module (crates-io))

(define-public crate-crossterm_winapi-0.1.0 (c (n "crossterm_winapi") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (k 0)))) (h "0p90gaj3v94ba5vczxv7fs3jznd90sqxm2pzhb70gai8xigch6wb")))

(define-public crate-crossterm_winapi-0.1.1 (c (n "crossterm_winapi") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (k 0)))) (h "02pwbrk13v0yic1xdp3hx88dyxf2n2fb8lbjd1j20i8h8ws04fzn")))

(define-public crate-crossterm_winapi-0.1.2 (c (n "crossterm_winapi") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.5") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (k 0)))) (h "044n13vmwqdz6cx7497108avg4yfch6kq68fbmny1ifkij255p3i")))

(define-public crate-crossterm_winapi-0.1.3 (c (n "crossterm_winapi") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (k 0)))) (h "1fgm0izv18xxdqvi1q1jdwdgvzpcxgjhccxhk9mahxqdi3a14fjx")))

(define-public crate-crossterm_winapi-0.1.4 (c (n "crossterm_winapi") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (k 0)))) (h "0chrlzf7ziwwkjdsksw28jvpmkb11b09c8qgp993k9a73i51w1lc")))

(define-public crate-crossterm_winapi-0.1.5 (c (n "crossterm_winapi") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (k 0)))) (h "0i8x72ffypprf2lnqi6zfgzz1didibs24w4pkdm2wibwcb6ffmdh")))

(define-public crate-crossterm_winapi-0.2.0 (c (n "crossterm_winapi") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vm5kijkmxml0c72j6a9243vwy8051kw7nxa0k4v6j94abv1qnnf")))

(define-public crate-crossterm_winapi-0.2.1 (c (n "crossterm_winapi") (v "0.2.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f7i4xxy929g0wncn6svb26c0k43dl9z66r2b32jxmm2dm3z888m")))

(define-public crate-crossterm_winapi-0.3.0 (c (n "crossterm_winapi") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0c35dbqijxq4fys9i7qm4qq9ji9982clwpi1w2rxk2146lmfj9fz")))

(define-public crate-crossterm_winapi-0.4.0 (c (n "crossterm_winapi") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1j3av8bba3f5y4n4w1vgn0iz28vdajxrli6lqxnvpddbphskmph2")))

(define-public crate-crossterm_winapi-0.5.0 (c (n "crossterm_winapi") (v "0.5.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00pb700nyn648hil30hnxas30syhxsa79paff956ky8wgywk8sin")))

(define-public crate-crossterm_winapi-0.5.1 (c (n "crossterm_winapi") (v "0.5.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xk2msw45g7qfkdls43ybij477lgdgnkdxq6qi85nb8yj00cfxw7")))

(define-public crate-crossterm_winapi-0.6.0 (c (n "crossterm_winapi") (v "0.6.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p5a4y3nnkkinqkifg7hzd1pac49hjbygbq09gahmip9xxhmvlk0")))

(define-public crate-crossterm_winapi-0.6.1 (c (n "crossterm_winapi") (v "0.6.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0p4gnrkxykdfx173px1i53836zq9yrc53gkxzmsh3d9gs1372yq5")))

(define-public crate-crossterm_winapi-0.6.2 (c (n "crossterm_winapi") (v "0.6.2") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nqhjp979c6iv092dbxl6hpncwgwjciafyj1nvcpa008iqzmq9n2")))

(define-public crate-crossterm_winapi-0.7.0 (c (n "crossterm_winapi") (v "0.7.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fa047zz228qyqbcjdss64j0n01p4adr29yh9w24lgjdrr59da0d")))

(define-public crate-crossterm_winapi-0.8.0 (c (n "crossterm_winapi") (v "0.8.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01q57jwnlicqxa2igg5nig3916kf5w6sqgbxk40q6hr2frh6cs9s")))

(define-public crate-crossterm_winapi-0.8.1 (c (n "crossterm_winapi") (v "0.8.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1crqha8wp2nl1xsg6p409bi7k19cdc5l1l1h0w1farnzcgiifgvp") (y #t)))

(define-public crate-crossterm_winapi-0.9.0 (c (n "crossterm_winapi") (v "0.9.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "075z15gxm4rn5yywq46khbg29bf504ix0f06zq3hx8aa91db7q9a")))

(define-public crate-crossterm_winapi-0.9.1 (c (n "crossterm_winapi") (v "0.9.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winbase" "consoleapi" "processenv" "handleapi" "synchapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0axbfb2ykbwbpf1hmxwpawwfs8wvmkcka5m561l7yp36ldi7rpdc")))

