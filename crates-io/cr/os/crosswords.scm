(define-module (crates-io cr os crosswords) #:use-module (crates-io))

(define-public crate-crosswords-0.1.0 (c (n "crosswords") (v "0.1.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "levenshtein") (r "^1.0.4") (d #t) (k 0)))) (h "04649g3vnk7xcdg41h5hj12wbsmcrf5ih5waydkalzvmxn4sr0kq")))

