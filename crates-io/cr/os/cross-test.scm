(define-module (crates-io cr os cross-test) #:use-module (crates-io))

(define-public crate-cross-test-0.1.0 (c (n "cross-test") (v "0.1.0") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0s59qj1q58rpz9jvcl0cwq751gylvhr2y0gils0lw3q2ibvh98b8")))

(define-public crate-cross-test-0.1.1 (c (n "cross-test") (v "0.1.1") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.19") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0lbaj42wp9rwpbczanmkzgdwnwkmmy5zmwxdiwvggvgmwhl0zrr2")))

(define-public crate-cross-test-0.1.2 (c (n "cross-test") (v "0.1.2") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0aziv04gj3jb8hrjb839bhjpw3i43nzc0mwqql75j4jg8ngp202z")))

(define-public crate-cross-test-0.1.3 (c (n "cross-test") (v "0.1.3") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0cxfjx8v74pc4r77fmccrzc8rd9y4iwhwzqf0nqk4g21x4v3mw7q")))

(define-public crate-cross-test-0.1.4 (c (n "cross-test") (v "0.1.4") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0d3xc8h74pdhgwkb9qch1phmmmpysvd1rkz8br0wbvncqi9yycf4")))

(define-public crate-cross-test-0.1.5 (c (n "cross-test") (v "0.1.5") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1k1hw2b0iglwibxkcr3cqsmfk15h9bx3n4ypalp9lgvbpf6kwy1j")))

(define-public crate-cross-test-0.1.6 (c (n "cross-test") (v "0.1.6") (d (list (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0yzfsya7x4yk9yjy04hak8ffvcrpfjjidnblc3nb41b78c3n1gnp")))

