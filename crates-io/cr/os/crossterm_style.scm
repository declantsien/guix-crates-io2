(define-module (crates-io cr os crossterm_style) #:use-module (crates-io))

(define-public crate-crossterm_style-0.1.0 (c (n "crossterm_style") (v "0.1.0") (d (list (d (n "crossterm_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06sbf6i8pz5v856r1nncg7h6d0gbkjk18yqn2q69rzck62hv9r52")))

(define-public crate-crossterm_style-0.2.0 (c (n "crossterm_style") (v "0.2.0") (d (list (d (n "crossterm_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v9hzxrwld7zl91fdsfbx5swhbqiwb6s9ana2izlc68gsm3n0d80")))

(define-public crate-crossterm_style-0.3.0 (c (n "crossterm_style") (v "0.3.0") (d (list (d (n "crossterm_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03vrb2c313blh4q579nl872l6xb8b5a8y1jf44r5bhdjffi8vy8v")))

(define-public crate-crossterm_style-0.3.1 (c (n "crossterm_style") (v "0.3.1") (d (list (d (n "crossterm_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lqvvbpjw9bcg9dk8fxk767hh54kyw81jvkm2disxa7dvrv7ix0k")))

(define-public crate-crossterm_style-0.3.2 (c (n "crossterm_style") (v "0.3.2") (d (list (d (n "crossterm_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ny62d851bkwg6sipis2ka7xkvzc5b4yakimgxbj7vimrgkpz01x")))

(define-public crate-crossterm_style-0.3.3 (c (n "crossterm_style") (v "0.3.3") (d (list (d (n "crossterm_utils") (r "^0.2.3") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xv13gj6x4anwavpk1h8pylf1riz0xkbcmjnck4slf77bx09cdcq")))

(define-public crate-crossterm_style-0.3.4 (c (n "crossterm_style") (v "0.3.4") (d (list (d (n "crossterm_utils") (r "^0.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00jj682cywd7sivyhg6k8z7k9pcxn3qzq6c0yfnmy2hmvx1jhfax") (y #t)))

(define-public crate-crossterm_style-0.4.0 (c (n "crossterm_style") (v "0.4.0") (d (list (d (n "crossterm_utils") (r "^0.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nnxyzp9fbky7vkk5sakc9ikzcly5zd164liq7iwawlmm9bcnqjl")))

(define-public crate-crossterm_style-0.4.1 (c (n "crossterm_style") (v "0.4.1") (d (list (d (n "crossterm_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02cc1mzkglk1g6rry3v2r7fng43sns804vlpi9m496p2ca10z5cb")))

(define-public crate-crossterm_style-0.5.0 (c (n "crossterm_style") (v "0.5.0") (d (list (d (n "crossterm_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kjkwicysnzjii2747020k8qgmif6dpzmgrs22sh4ngxr5p7ygkh")))

(define-public crate-crossterm_style-0.5.1 (c (n "crossterm_style") (v "0.5.1") (d (list (d (n "crossterm_utils") (r "^0.3.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yqnv32q80a8v1v2k5c63hxbvgs0dd983n9z1y9df0643cjx3dx2")))

(define-public crate-crossterm_style-0.5.2 (c (n "crossterm_style") (v "0.5.2") (d (list (d (n "crossterm_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11hxr0gjzr8mn1cb84lkhg9r50jgwcvdypn8cpvvjx5y0cml0mml")))

