(define-module (crates-io cr os cross-test-action-test) #:use-module (crates-io))

(define-public crate-cross-test-action-test-0.1.3 (c (n "cross-test-action-test") (v "0.1.3") (d (list (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0v08qh8b8455d95i4kz03zwy48lbqjf58bgzvj3r2mg294h58qp2")))

(define-public crate-cross-test-action-test-0.1.4 (c (n "cross-test-action-test") (v "0.1.4") (d (list (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0r85zw8h8ravff1h3a7ha6cpnawcky8h5c3dmj7r5h00j8a6c3yr")))

(define-public crate-cross-test-action-test-0.1.5 (c (n "cross-test-action-test") (v "0.1.5") (d (list (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0xx3wgcxc25pqhblr2wjhndzhm8ygs8g6hgrhch2wirjibw76f3f")))

(define-public crate-cross-test-action-test-0.1.6 (c (n "cross-test-action-test") (v "0.1.6") (d (list (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1zilhza4f6gnqzmbgv6ywjrxiwhbn6w1332qbcsij7wfljk6h7cd")))

(define-public crate-cross-test-action-test-0.1.7 (c (n "cross-test-action-test") (v "0.1.7") (d (list (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0gc52qfxbxgyx3jm86dy160l191vz0a6zig5fsb34xyi7acd0s4q")))

(define-public crate-cross-test-action-test-0.1.9 (c (n "cross-test-action-test") (v "0.1.9") (d (list (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1a9yg0kc27jc3f47g7lr485c2hdgb4d24vdzxazi1s9ry9d6w76i")))

(define-public crate-cross-test-action-test-0.1.10 (c (n "cross-test-action-test") (v "0.1.10") (d (list (d (n "cross-test") (r "^0.1.6") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "06bqiy6rlfnfg4wr91ym0g4i385hmw1x8lr8wxxy95vkygv6y9wa")))

