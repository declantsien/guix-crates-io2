(define-module (crates-io cr os crossbeam-queue) #:use-module (crates-io))

(define-public crate-crossbeam-queue-0.0.0 (c (n "crossbeam-queue") (v "0.0.0") (h "10rdia517dzpfwj0qhr5wwc7xa179p7m97if7h2h64gafz538ma8")))

(define-public crate-crossbeam-queue-0.1.0 (c (n "crossbeam-queue") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0gjqgm2l2mkn9i4x275hhah6hlbndvmn00ipy1r9x5qk74pm15ra")))

(define-public crate-crossbeam-queue-0.1.1 (c (n "crossbeam-queue") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.6.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0p5h4gp6b7ynpzb9lbvc1s8cm1i4b0y9bris81l6dypbxf34py98")))

(define-public crate-crossbeam-queue-0.1.2 (c (n "crossbeam-queue") (v "0.1.2") (d (list (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0jsa9dbxnwqcxfws09vaschf92d4imlbbikmcn4ka8z7rzb9r5vw")))

(define-public crate-crossbeam-queue-0.2.0 (c (n "crossbeam-queue") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "00177hj1anmb3fijm6lc95cncb19ad2kv0a2gf3jybd8cic53mnz")))

(define-public crate-crossbeam-queue-0.2.1 (c (n "crossbeam-queue") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1nwkjh185bdwjrv1zj2g7an9lglv8sp4459268m4fwvi3v5fx5f6") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc"))))))

(define-public crate-crossbeam-queue-0.2.2 (c (n "crossbeam-queue") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1llq7z955gcfcfg3yyffb8szvzji9z1m44r05zj0gfxn2kkzysxb") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-crossbeam-queue-0.2.3 (c (n "crossbeam-queue") (v "0.2.3") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "maybe-uninit") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0w15z68nz3ac4f2s4djhwha8vmlwsh9dlfrmsl4x84y2ah5acjvp") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc"))))))

(define-public crate-crossbeam-queue-0.3.0 (c (n "crossbeam-queue") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0x6p2x9sa2zll60g4af6c48r1dcky0h43g3jw6xa76h47xb5hakb") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc"))))))

(define-public crate-crossbeam-queue-0.3.1 (c (n "crossbeam-queue") (v "0.3.1") (d (list (d (n "cfg-if") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r ">=0.8.0, <0.9.0") (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 2)))) (h "0mk790w2y7cn9zqj6cn1kl8vvgaamlkj6fmpxg1iprdqyp3v6v0g") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc"))))))

(define-public crate-crossbeam-queue-0.3.2 (c (n "crossbeam-queue") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1japlqpj30c3b3qwk5wnvcw6sk1zsl7ip30lmp18hp224k0ds44v") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc"))))))

(define-public crate-crossbeam-queue-0.3.3 (c (n "crossbeam-queue") (v "0.3.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0451lw9nvds4rds4cjbkaz4y90qgva874gx7h3ydz16bkxndfydr") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-crossbeam-queue-0.3.4 (c (n "crossbeam-queue") (v "0.3.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1kmw36dcn2zsnhsnh58ark0nn28wja7n41zxksjjv1540nr3bm2d") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-crossbeam-queue-0.3.5 (c (n "crossbeam-queue") (v "0.3.5") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "18kpa83m1ivz3230f4ax95f9pgcclj227rg4y1w5fyja1x0dh98z") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.36")))

(define-public crate-crossbeam-queue-0.3.6 (c (n "crossbeam-queue") (v "0.3.6") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "19ram1wp87i57w834hwl95mzz9g53qlzkraq6lvab629n21jbm0w") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.38")))

(define-public crate-crossbeam-queue-0.3.7 (c (n "crossbeam-queue") (v "0.3.7") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0327njm6bcj8y1vkyajj5ivc08y21vq18csk2mmbxs8j6ild3czb") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (y #t) (r "1.38")))

(define-public crate-crossbeam-queue-0.3.8 (c (n "crossbeam-queue") (v "0.3.8") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1p9s6n4ckwdgxkb7a8ay9zjzmgc8ppfbxix2vr07rwskibmb7kyi") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.38")))

(define-public crate-crossbeam-queue-0.3.9 (c (n "crossbeam-queue") (v "0.3.9") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.17") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0lz17pgydh29w8brld8dysi1m4n5bxfpnj8w9bxk0q6xpyyzbg5r") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.61")))

(define-public crate-crossbeam-queue-0.3.10 (c (n "crossbeam-queue") (v "0.3.10") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.18") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "18kq9nd3c81adk697jx35jfh35kjvd8zxhg8j7a86nmv462mkimd") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-crossbeam-queue-0.3.11 (c (n "crossbeam-queue") (v "0.3.11") (d (list (d (n "crossbeam-utils") (r "^0.8.18") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0d8y8y3z48r9javzj67v3p2yfswd278myz1j9vzc4sp7snslc0yz") (f (quote (("std" "alloc" "crossbeam-utils/std") ("nightly" "crossbeam-utils/nightly") ("default" "std") ("alloc")))) (r "1.60")))

