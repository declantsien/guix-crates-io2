(define-module (crates-io cr os crossbow-permissions) #:use-module (crates-io))

(define-public crate-crossbow-permissions-0.1.0 (c (n "crossbow-permissions") (v "0.1.0") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "02x4a79ii3c9dj918npd69j96cdam3yd3l5ssd43ypnlnm4binvv")))

(define-public crate-crossbow-permissions-0.1.1 (c (n "crossbow-permissions") (v "0.1.1") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "12kxcrh8npb9vy0pldly4h82j33vfp64dp6w9rj9gfkmy7ihbcrn")))

(define-public crate-crossbow-permissions-0.1.2 (c (n "crossbow-permissions") (v "0.1.2") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0yxcdc0s2nynb75qwzr6vsishsiyfpsw1v4dcs3wj5sn5lmcpyf6")))

(define-public crate-crossbow-permissions-0.1.3 (c (n "crossbow-permissions") (v "0.1.3") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "087dxgrfng9931bj7zmkfy39qlai7g0scqz0lcigi0lhx151vwgh")))

