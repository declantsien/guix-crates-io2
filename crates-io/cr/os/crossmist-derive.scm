(define-module (crates-io cr os crossmist-derive) #:use-module (crates-io))

(define-public crate-crossmist-derive-0.1.0 (c (n "crossmist-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "10nvp6z64vsi9g95j71w5zfamq6s93xm7zqvdz65gfyz9sipcypb")))

(define-public crate-crossmist-derive-0.1.3 (c (n "crossmist-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "13m05j9f3ldia7jy3072vfg2a0syji0bkqdajz1m74ax15giap3n")))

(define-public crate-crossmist-derive-0.1.4 (c (n "crossmist-derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "1821rv78mz7kzsybk8n9irkc7k6bayd0kapn3jxpdgijrljf7i5r") (y #t)))

(define-public crate-crossmist-derive-0.1.8 (c (n "crossmist-derive") (v "0.1.8") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "149dlgrxhjhvcxcc0inc83d934c6qwhhwpkz28lrl06b2s01lxv4")))

(define-public crate-crossmist-derive-0.2.0 (c (n "crossmist-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "0v35cgmmwmq73fj031chfc7z1mf2qy9ywrpn2w60rv8ry49bysqp")))

(define-public crate-crossmist-derive-0.2.3 (c (n "crossmist-derive") (v "0.2.3") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "0n8si7kgsm2qpbq5lss81hfs1wv4b8b6wxxpvxn20igxwzjvnqbi")))

(define-public crate-crossmist-derive-0.2.4 (c (n "crossmist-derive") (v "0.2.4") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "14pmmhli9ca2q28z2i2gsdcm07hdydng0h2q1mhrvssbz3xlhdhm")))

(define-public crate-crossmist-derive-1.0.0 (c (n "crossmist-derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "0bkwcdnvs8rsxsfszm16rvpk18cv5y36y5x20a3ab9a78wbhhay3")))

(define-public crate-crossmist-derive-1.0.2 (c (n "crossmist-derive") (v "1.0.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (d #t) (k 0)))) (h "137s7wn1sjkscxmkms1sgp3lmdqwvyd0zqbs99b30k4ijz74x7sb")))

