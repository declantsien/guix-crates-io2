(define-module (crates-io cr os crossroads) #:use-module (crates-io))

(define-public crate-crossroads-0.1.0 (c (n "crossroads") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06fgf00dqrwi4d4pi2phqmpcs6305qkfr668ad17qh9w7kl8syjz")))

(define-public crate-crossroads-0.1.1 (c (n "crossroads") (v "0.1.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1m1l142nn9s5jv82wgcchvhh59k7hhfw48yipf2zssx9fv7fc25j")))

