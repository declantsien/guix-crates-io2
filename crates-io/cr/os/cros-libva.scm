(define-module (crates-io cr os cros-libva) #:use-module (crates-io))

(define-public crate-cros-libva-0.0.1 (c (n "cros-libva") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 2)) (d (n "log") (r "^0") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "17gbnxqb6055kmrkl2adbbwzv4v9c8qcxaiia96j8w22zmbx0b0n")))

(define-public crate-cros-libva-0.0.2 (c (n "cros-libva") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 2)) (d (n "log") (r "^0") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0148m3plyr7a1694lmk4c2injl9qwygh4vdy7qklg9v9cmw15zcb")))

(define-public crate-cros-libva-0.0.3 (c (n "cros-libva") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 2)) (d (n "log") (r "^0") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1ig6564fls8dx7axzw3m5cxs2kcrq6wd5n6rdvkjzgg5nn9jsdgv")))

(define-public crate-cros-libva-0.0.4 (c (n "cros-libva") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 2)) (d (n "log") (r "^0") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gpggn1h20p2v1zvffds2fh0hb6iay436a7g4v8p49fpaacywy6w")))

(define-public crate-cros-libva-0.0.5 (c (n "cros-libva") (v "0.0.5") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 2)) (d (n "log") (r "^0") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ayxgxlwkh6ynqx0269pv3iflxpclrsd6b42dbif1rkch3awkf5b")))

(define-public crate-cros-libva-0.0.6 (c (n "cros-libva") (v "0.0.6") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 2)) (d (n "log") (r "^0") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jk1nbc8vp8g9arl9hwqs28irk4ywr3z1f5yyfsgichfwcz98x8g")))

