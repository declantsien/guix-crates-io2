(define-module (crates-io cr os crossword_generator) #:use-module (crates-io))

(define-public crate-crossword_generator-0.0.1 (c (n "crossword_generator") (v "0.0.1") (d (list (d (n "async-recursion") (r "^1.1.0") (d #t) (k 0)) (d (n "async_fn_traits") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.15") (d #t) (k 0)) (d (n "trait-set") (r "^0.3.0") (d #t) (k 0)))) (h "0sw3kxdxnc7g23q87psbjsrs4qap8v614akcgd0zgl1yxgz2zraq")))

