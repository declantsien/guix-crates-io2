(define-module (crates-io cr os crossbeam-deque) #:use-module (crates-io))

(define-public crate-crossbeam-deque-0.1.0 (c (n "crossbeam-deque") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1x1n9rfxcw1r89xi3f9g74cfz9zhkqxs1z87idc2yxlx6mv77mmz")))

(define-public crate-crossbeam-deque-0.1.1 (c (n "crossbeam-deque") (v "0.1.1") (d (list (d (n "crossbeam-epoch") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0jlzikmra6djwxgkrszhbi19mpbw0yhhlshc4vs13r75k5c41x8m")))

(define-public crate-crossbeam-deque-0.2.0 (c (n "crossbeam-deque") (v "0.2.0") (d (list (d (n "crossbeam-epoch") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1wwwbnvxh0rza38xiws8qc46klzhv19zgvarn37pijis6v2zhfgp")))

(define-public crate-crossbeam-deque-0.3.0 (c (n "crossbeam-deque") (v "0.3.0") (d (list (d (n "crossbeam-epoch") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "18jyxh92fxnavpchcfnhawvx7q576fxiva6dxcspyvy388vwggf1")))

(define-public crate-crossbeam-deque-0.3.1 (c (n "crossbeam-deque") (v "0.3.1") (d (list (d (n "crossbeam-epoch") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1rya0zdv8fqy7qmx8j7xccpg5snx8snzy9xl0pnlsnd70kpm70gy")))

(define-public crate-crossbeam-deque-0.4.0 (c (n "crossbeam-deque") (v "0.4.0") (d (list (d (n "crossbeam-epoch") (r "^0.4.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0z2d69pcgr5snxm3zx2w6pcr88plr6rrqd2bpxk15jl4aj3f2sid")))

(define-public crate-crossbeam-deque-0.4.1 (c (n "crossbeam-deque") (v "0.4.1") (d (list (d (n "crossbeam-epoch") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1qdizmnrlibwavrzvwh421x8w7bmvnp8zfhr0kian7n7g268ldpz")))

(define-public crate-crossbeam-deque-0.5.0 (c (n "crossbeam-deque") (v "0.5.0") (d (list (d (n "crossbeam-epoch") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.3") (d #t) (k 2)))) (h "1b9g7la0jlpi04h0nyv7gmfyhw4qcl31x99jcyg9ldxs2ambpawc")))

(define-public crate-crossbeam-deque-0.5.1 (c (n "crossbeam-deque") (v "0.5.1") (d (list (d (n "crossbeam-epoch") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.3") (d #t) (k 2)))) (h "00d86n77m4fpdbhymhxjic9j5illbcdn9s0f16gzysddlakmf4yf")))

(define-public crate-crossbeam-deque-0.5.2 (c (n "crossbeam-deque") (v "0.5.2") (d (list (d (n "crossbeam-epoch") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.3") (d #t) (k 2)))) (h "16nb554hxn35s9p0j6is5i6w5b2sjkfs6a1p9rjjy8m4nnlw94kp")))

(define-public crate-crossbeam-deque-0.6.0 (c (n "crossbeam-deque") (v "0.6.0") (d (list (d (n "crossbeam-epoch") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.3") (d #t) (k 2)))) (h "0w7aq2z7yqdk3xvm9k3ryinswcflsbqmjw5l6499grmqq8vgwlrg")))

(define-public crate-crossbeam-deque-0.6.1 (c (n "crossbeam-deque") (v "0.6.1") (d (list (d (n "crossbeam-epoch") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.3") (d #t) (k 2)))) (h "18gd5hphvxrvf2cyxwaaa54w5f281bgrfb1pab5vk1q49kyax1il")))

(define-public crate-crossbeam-deque-0.6.2 (c (n "crossbeam-deque") (v "0.6.2") (d (list (d (n "crossbeam-epoch") (r "^0.6") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1lv929p05lawvgc9k3izab6c056p4lpgci7ybaicf97q8pwvdqag")))

(define-public crate-crossbeam-deque-0.6.3 (c (n "crossbeam-deque") (v "0.6.3") (d (list (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "04rcpgjs6ns57vag8a3dzx26190dhbvy2l0p9n22b9p1yf64pr05")))

(define-public crate-crossbeam-deque-0.7.0 (c (n "crossbeam-deque") (v "0.7.0") (d (list (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1vps794wrhkp0q1jxsx6m5j120vhxgh8n4r075nn6nbq67ig4j2h") (y #t)))

(define-public crate-crossbeam-deque-0.7.1 (c (n "crossbeam-deque") (v "0.7.1") (d (list (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0was9x71cz5g1y3670cyy6jdmsdfg6k9mbf0ddz2k1mdd7hx535i") (y #t)))

(define-public crate-crossbeam-deque-0.7.2 (c (n "crossbeam-deque") (v "0.7.2") (d (list (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1jm3rqb3qfpfywrakyy81f61xnl4jsim7lam9digw6w6cdfr9an3") (y #t)))

(define-public crate-crossbeam-deque-0.7.3 (c (n "crossbeam-deque") (v "0.7.3") (d (list (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "maybe-uninit") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "11c2c0x5grdba3ah3g94yn6b8s47xi8qwm85h8hq5vmf9nbsy0lz") (y #t)))

(define-public crate-crossbeam-deque-0.8.0 (c (n "crossbeam-deque") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1ad995vzq74k7jd1pgn9zxbacyzj9ii6l0svhlb2dxzy8vxnxbwl") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("default" "std")))) (y #t)))

(define-public crate-crossbeam-deque-0.8.1 (c (n "crossbeam-deque") (v "0.8.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "07nypn86id2lf912ahiww1jvqp0zbk2xa25ra7vzplph375c0mb4") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("default" "std"))))))

(define-public crate-crossbeam-deque-0.7.4 (c (n "crossbeam-deque") (v "0.7.4") (d (list (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "maybe-uninit") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1v99xcdjk4zixvxnq7pssip670mlyhw1ma3qc88ca11jxnfz43y2")))

(define-public crate-crossbeam-deque-0.8.2 (c (n "crossbeam-deque") (v "0.8.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1z6ifz35lyk0mw818xcl3brgss2k8islhgdmfk9s5fwjnr982pki") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("default" "std")))) (r "1.38")))

(define-public crate-crossbeam-deque-0.8.3 (c (n "crossbeam-deque") (v "0.8.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1vqczbcild7nczh5z116w8w46z991kpjyw7qxkf24c14apwdcvyf") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("default" "std")))) (r "1.38")))

(define-public crate-crossbeam-deque-0.8.4 (c (n "crossbeam-deque") (v "0.8.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.9.16") (o #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.17") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0la7fx9n1vbx3h23va0xmcy36hziql1pkik08s3j3asv4479ma7w") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("default" "std")))) (r "1.61")))

(define-public crate-crossbeam-deque-0.8.5 (c (n "crossbeam-deque") (v "0.8.5") (d (list (d (n "crossbeam-epoch") (r "^0.9.17") (k 0)) (d (n "crossbeam-utils") (r "^0.8.18") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "03bp38ljx4wj6vvy4fbhx41q8f585zyqix6pncz1mkz93z08qgv1") (f (quote (("std" "crossbeam-epoch/std" "crossbeam-utils/std") ("default" "std")))) (r "1.61")))

