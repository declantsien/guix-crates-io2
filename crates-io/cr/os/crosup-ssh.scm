(define-module (crates-io cr os crosup-ssh) #:use-module (crates-io))

(define-public crate-crosup-ssh-0.1.0 (c (n "crosup-ssh") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (f (quote ("vendored-openssl"))) (d #t) (k 0)))) (h "056w6il3ssz4fy0i51y43npcmp7f76fpy0x5j7r542znbqwpvch5")))

