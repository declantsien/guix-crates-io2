(define-module (crates-io cr os crossterm_utils) #:use-module (crates-io))

(define-public crate-crossterm_utils-0.1.0 (c (n "crossterm_utils") (v "0.1.0") (d (list (d (n "crossterm_winapi") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hf78wxclnk651wmcr4yfa14qgk7wczdscamm1arq7x9dqw28vyj")))

(define-public crate-crossterm_utils-0.2.0 (c (n "crossterm_utils") (v "0.2.0") (d (list (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wkwmggjxxkn4bj7v3v0qpcbw0q36drl8apn9xagx8bnk8ldjcpz")))

(define-public crate-crossterm_utils-0.2.1 (c (n "crossterm_utils") (v "0.2.1") (d (list (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xh2r29mkaxdy2yqv360i6w4xq82r70fcbmy79dh9fv5wqw5zdn3")))

(define-public crate-crossterm_utils-0.2.2 (c (n "crossterm_utils") (v "0.2.2") (d (list (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10bkzgmfw3ljilkpxdza8l6fmpc34cw1j88nvf38mp96d8290iwy")))

(define-public crate-crossterm_utils-0.2.3 (c (n "crossterm_utils") (v "0.2.3") (d (list (d (n "crossterm_winapi") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p3pn2iiq5jslggvk09g2ljp4krbmqg5jprk575pgvnh104x88c3")))

(define-public crate-crossterm_utils-0.2.4 (c (n "crossterm_utils") (v "0.2.4") (d (list (d (n "crossterm_winapi") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05q6iazwqacdmsramxnpx5njy6iapk4hby6zkmk31is040dsfx7q")))

(define-public crate-crossterm_utils-0.3.0 (c (n "crossterm_utils") (v "0.3.0") (d (list (d (n "crossterm_winapi") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mpn4dqb32k3fa0ggh6gvx3n0js0by97mhnzd9nwys76x3xa6nnr")))

(define-public crate-crossterm_utils-0.3.1 (c (n "crossterm_utils") (v "0.3.1") (d (list (d (n "crossterm_winapi") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xql40hbh551vc5smwgi0p10l18yjhcjyyz8mwcg8ngz6vr6vd7i")))

(define-public crate-crossterm_utils-0.4.0 (c (n "crossterm_utils") (v "0.4.0") (d (list (d (n "crossterm_winapi") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18v4smjpm7r9as2x8lnrc87j4myfkfxis8al7n2zwrbjbxz084sw")))

