(define-module (crates-io cr os crossterm_terminal) #:use-module (crates-io))

(define-public crate-crossterm_terminal-0.1.0 (c (n "crossterm_terminal") (v "0.1.0") (d (list (d (n "crossterm_cursor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)))) (h "0i3d9b6i0a4sipiyngp66qlkqj3agg47mbz8ji4n5lryxzs1mdpi")))

(define-public crate-crossterm_terminal-0.2.0 (c (n "crossterm_terminal") (v "0.2.0") (d (list (d (n "crossterm_cursor") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)))) (h "0m14qc0hh6qfvlwg80q7594si3rv3vspcxcpf22nkv10825s49jg")))

(define-public crate-crossterm_terminal-0.2.1 (c (n "crossterm_terminal") (v "0.2.1") (d (list (d (n "crossterm_cursor") (r "^0.2.1") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)))) (h "0ph2ic149rhksi760rlhphh42imayflghww9fz97b7d6n143zs04")))

(define-public crate-crossterm_terminal-0.2.2 (c (n "crossterm_terminal") (v "0.2.2") (d (list (d (n "crossterm_cursor") (r "^0.2.1") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)))) (h "0q5vkqls00wb1qdwm4bg77jyyp832xq9y9zllw6lqf8rkkyygj80")))

(define-public crate-crossterm_terminal-0.2.3 (c (n "crossterm_terminal") (v "0.2.3") (d (list (d (n "crossterm_cursor") (r "^0.2.2") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)))) (h "0vlpyphlfs09fmrkk6bpbg38ppxgsxy012fwcaisqimp43mbma6a")))

(define-public crate-crossterm_terminal-0.2.4 (c (n "crossterm_terminal") (v "0.2.4") (d (list (d (n "crossterm_cursor") (r "^0.2.3") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.2.3") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)))) (h "0mmk0gh1fz9c8k45ygd5yh52kgwkly483x9db39mzk6dqnbjqy8q")))

(define-public crate-crossterm_terminal-0.2.5 (c (n "crossterm_terminal") (v "0.2.5") (d (list (d (n "crossterm_cursor") (r "^0.2") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.2") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)))) (h "00n144im47g4412qgcwz8lz0mb2p1gy7y75jv8fzh49hlw6102hs")))

(define-public crate-crossterm_terminal-0.2.6 (c (n "crossterm_terminal") (v "0.2.6") (d (list (d (n "crossterm_cursor") (r "^0.2.6") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.2.4") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)))) (h "0di6468avyc82zm579xrfk5flhfbbjj4wjisyjhnmhp036sld1fv")))

(define-public crate-crossterm_terminal-0.3.0 (c (n "crossterm_terminal") (v "0.3.0") (d (list (d (n "crossterm_cursor") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ajv9y9iyw6kfg0naxv3zygl3s7b8ay7ysc26blv53v5rir66wl4")))

(define-public crate-crossterm_terminal-0.3.1 (c (n "crossterm_terminal") (v "0.3.1") (d (list (d (n "crossterm_cursor") (r "^0.3.1") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.3.1") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n201hyw0xfaz7jnd6lj5jk8gi0y5y68z63dqdkfz8kqz37bz0hq")))

(define-public crate-crossterm_terminal-0.3.2 (c (n "crossterm_terminal") (v "0.3.2") (d (list (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "crossterm_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "crossterm_winapi") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "125wsjg1nbv1yiw3h2x8c3bdp6dcwhah5mrzz8ir259pdja9pw5k")))

