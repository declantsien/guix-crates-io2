(define-module (crates-io cr os crosspub) #:use-module (crates-io))

(define-public crate-crosspub-0.5.4 (c (n "crosspub") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "138x3vh3slldkblz8z9bfwgisw3a0vyj27y5pqyjcb7vban0g06x")))

