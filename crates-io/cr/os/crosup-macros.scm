(define-module (crates-io cr os crosup-macros) #:use-module (crates-io))

(define-public crate-crosup-macros-0.1.0 (c (n "crosup-macros") (v "0.1.0") (h "1ggybqybc42fsjh4ayl891r02vqnyshi2nbsv594r3symsnvsjbj")))

(define-public crate-crosup-macros-0.1.1 (c (n "crosup-macros") (v "0.1.1") (h "00pz3bikja1qrkl0rb2i4b5qka37qdrqxcbhnbrc7xn5ax9j7n7s")))

(define-public crate-crosup-macros-0.1.2 (c (n "crosup-macros") (v "0.1.2") (h "0zq9n4f544av9jip53sl2fhpa4h1a6wr6c8md1wihq7qncwfbx4m")))

(define-public crate-crosup-macros-0.1.3 (c (n "crosup-macros") (v "0.1.3") (h "16xz8lrc4d3x83r2qdgymy4m4s39x0y7i7mny4ggqz249cx74mip")))

(define-public crate-crosup-macros-0.1.4 (c (n "crosup-macros") (v "0.1.4") (h "191pyq38d3ilzvznz7z7dy1px3pzy4ygvsj9d4dnzj2drlz74j3q")))

(define-public crate-crosup-macros-0.1.5 (c (n "crosup-macros") (v "0.1.5") (h "1142rbngsaa13b6lm3wv6kbk5w78hj35xn5if62rx2lz2b4p6xfm")))

(define-public crate-crosup-macros-0.2.0 (c (n "crosup-macros") (v "0.2.0") (h "1wn5gl6j8ky1gmxygybz75nrdfhk3vdkd6ahn9rag14i56basj4g")))

