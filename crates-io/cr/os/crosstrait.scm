(define-module (crates-io cr os crosstrait) #:use-module (crates-io))

(define-public crate-crosstrait-0.1.0 (c (n "crosstrait") (v "0.1.0") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "linkme") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (f (quote ("critical-section"))) (k 0)))) (h "0ym47lckhfk1jinws283c38x4c3a2jhag13951nwqh8amcj542im") (f (quote (("used_linker" "linkme/used_linker") ("std" "alloc" "once_cell/std") ("default" "std") ("alloc"))))))

