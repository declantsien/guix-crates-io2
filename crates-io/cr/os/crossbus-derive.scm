(define-module (crates-io cr os crossbus-derive) #:use-module (crates-io))

(define-public crate-crossbus-derive-0.0.1-a (c (n "crossbus-derive") (v "0.0.1-a") (h "1ivm60cxm4z02k46m6sgb9h0l530mdahdgd7aqz38z07wc1didr0")))

(define-public crate-crossbus-derive-0.0.2-a (c (n "crossbus-derive") (v "0.0.2-a") (h "0bgj0zx03bfl34l1hhfa6g8vpdnl907igxaqhp3sbfizygfd3z7g")))

