(define-module (crates-io cr os crossbeam_requests) #:use-module (crates-io))

(define-public crate-crossbeam_requests-0.2.0 (c (n "crossbeam_requests") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)))) (h "1yni6gcfrq03ysphzaqir5frvwkqhwamdsc6pljhs6lb4gljaw3p")))

(define-public crate-crossbeam_requests-0.3.0 (c (n "crossbeam_requests") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "167460yh7pyawvsihmwq0c6j4619zjfb14dg577v9sgwz6yhynzf")))

