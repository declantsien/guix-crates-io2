(define-module (crates-io cr os crosvm_data_model) #:use-module (crates-io))

(define-public crate-crosvm_data_model-0.1.0-gamma.0 (c (n "crosvm_data_model") (v "0.1.0-gamma.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (k 0)) (d (n "libc") (r "^0.2.116") (d #t) (t "cfg(unix)") (k 0)) (d (n "remain") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything" "std" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13jp0xmz2rja2slbqgf236g28rkilxdrn1y0i581iji9qasmcsg7")))

