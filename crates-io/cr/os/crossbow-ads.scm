(define-module (crates-io cr os crossbow-ads) #:use-module (crates-io))

(define-public crate-crossbow-ads-0.1.0 (c (n "crossbow-ads") (v "0.1.0") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1n76wmkjyawx1yd7qy72k4aplw42hz3rbqpc1hvcqz9iy7xii0yv")))

(define-public crate-crossbow-ads-0.1.1 (c (n "crossbow-ads") (v "0.1.1") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0n34rwanpihrfnzj1qafkxw02njacbi0ckmi9v1rvjgpq7hzlkp5")))

(define-public crate-crossbow-ads-0.1.2 (c (n "crossbow-ads") (v "0.1.2") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "039a2gff7z9z5z1188hbjxyikdkhdqx58qi0nz60zym03az5zri5")))

(define-public crate-crossbow-ads-0.1.3 (c (n "crossbow-ads") (v "0.1.3") (d (list (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "09bpx464q0p1z6afqdb0pa6a7qh6c2h1lpivisz5bwjgmizs35wn")))

