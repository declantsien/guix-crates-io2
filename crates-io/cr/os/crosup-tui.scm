(define-module (crates-io cr os crosup-tui) #:use-module (crates-io))

(define-public crate-crosup-tui-0.1.0 (c (n "crosup-tui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bat") (r "^0.23.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0bp2wcr7kzxlrsq06kipfnr150j9a4hk29rqg7nk5ix0gyl9mvyc")))

