(define-module (crates-io cr os crosup-repo) #:use-module (crates-io))

(define-public crate-crosup-repo-0.1.0 (c (n "crosup-repo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crosup-entity") (r "^0.1.0") (d #t) (k 0)) (d (n "sea-orm") (r "^0.11.3") (f (quote ("runtime-tokio-rustls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "1gck2yl3fxicwsx6wswwd1sf7kapds98bfscixhz3vs0m2b7r9ix")))

