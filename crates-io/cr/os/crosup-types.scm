(define-module (crates-io cr os crosup-types) #:use-module (crates-io))

(define-public crate-crosup-types-0.1.0 (c (n "crosup-types") (v "0.1.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "18mzw5jgm7d0s2020scfls9kmz54gc6vahb2xldy9myikn77dcw8")))

(define-public crate-crosup-types-0.1.1 (c (n "crosup-types") (v "0.1.1") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "008d6wg5iy997qayifs2pcql9jrs1ydybqp68lrh18mdchvxfyry")))

(define-public crate-crosup-types-0.1.2 (c (n "crosup-types") (v "0.1.2") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1f0k05bmwz2m5l8aw5h2iymy6i1x27371wssm3jvla59hkbkvibk")))

(define-public crate-crosup-types-0.1.3 (c (n "crosup-types") (v "0.1.3") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1c1qs7mdw3vk2w2n884apcwpvygf4fz06mqvhwcraxn63i81j2d7")))

(define-public crate-crosup-types-0.1.4 (c (n "crosup-types") (v "0.1.4") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "15maxdlpvp9p48r6mshafjfl6zix3hq1n025cqrk4gr7i9vzydmy")))

(define-public crate-crosup-types-0.1.5 (c (n "crosup-types") (v "0.1.5") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0mbw8779gn60xq7v5mkah1d9r3z35s9p2kp45hpsibl28ckvbc2l")))

(define-public crate-crosup-types-0.1.6 (c (n "crosup-types") (v "0.1.6") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1i49myfhibshybdk9pdhgbhxw1ydjhg816k7q5q8wz053jvscf01")))

(define-public crate-crosup-types-0.1.7 (c (n "crosup-types") (v "0.1.7") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "13wrlrb6sfgl2rrdssg1aynn18wp36ya0in75nz6jhxa54rj632g")))

(define-public crate-crosup-types-0.1.8 (c (n "crosup-types") (v "0.1.8") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0g8jycz5yra5v4czhx0ja0iz1vyc63ad2sibwrlv66dcfi1wvwxb")))

(define-public crate-crosup-types-0.2.0 (c (n "crosup-types") (v "0.2.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "11knqaamzr0al64lvvf67ibh3z9jx78pa8w16bbk1abzzwvli93q")))

