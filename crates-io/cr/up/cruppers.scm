(define-module (crates-io cr up cruppers) #:use-module (crates-io))

(define-public crate-cruppers-0.4.0 (c (n "cruppers") (v "0.4.0") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)))) (h "0a1ad29qz2i207ipbaj3wlj4yznavwg0s9xqf87hkwlassdlj9ij") (f (quote (("memory") ("iostream") ("exception") ("default" "memory" "exception"))))))

(define-public crate-cruppers-0.4.1 (c (n "cruppers") (v "0.4.1") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)))) (h "1n1kxsxgi7zszbcrac3wsxcsi97rviawwb9v0z4d0rprqqknrrij") (f (quote (("memory") ("iostream") ("exception") ("default" "memory" "exception"))))))

(define-public crate-cruppers-0.4.2 (c (n "cruppers") (v "0.4.2") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)))) (h "1map0z8zgazwsaaadrl34g65f24wvq3rs33nf5i3gzdnar01yyfk") (f (quote (("memory") ("iostream") ("exception") ("default" "memory" "exception"))))))

(define-public crate-cruppers-0.4.3 (c (n "cruppers") (v "0.4.3") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)))) (h "1wksmd3ssyrfxrj3g7nhrar085lhx2bv3140kqg4cii49v2zshi8") (f (quote (("memory") ("iostream") ("exception") ("default" "memory" "exception"))))))

(define-public crate-cruppers-0.5.0 (c (n "cruppers") (v "0.5.0") (d (list (d (n "cc") (r ">=1.0.0") (d #t) (k 1)))) (h "1lzw5fm94950wi4ygnwmz3g4in8sfna4cpq0danzn007rp6zf3gi") (f (quote (("memory") ("iostream") ("exception") ("default" "memory" "exception" "iostream"))))))

