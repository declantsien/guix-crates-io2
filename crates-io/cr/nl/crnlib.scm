(define-module (crates-io cr nl crnlib) #:use-module (crates-io))

(define-public crate-crnlib-0.1.0 (c (n "crnlib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "image") (r "0.*") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1c5dv5ba0nn51y6ab2jsg1kwc2im3llfx316ixw7h0k890ra0c4r")))

(define-public crate-crnlib-0.1.1 (c (n "crnlib") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bitvec") (r "^0.17") (d #t) (k 0)) (d (n "image") (r ">=0.23") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1p17xbwxa8lgknql38d1xd8pqsdwnz0qga1zmp17cqhilpxb8r2z")))

