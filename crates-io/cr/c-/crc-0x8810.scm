(define-module (crates-io cr c- crc-0x8810) #:use-module (crates-io))

(define-public crate-crc-0x8810-0.1.0 (c (n "crc-0x8810") (v "0.1.0") (d (list (d (n "assert_hex") (r "^0.2") (d #t) (k 2)) (d (n "crc") (r "^2") (d #t) (k 2)) (d (n "crc-any") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "0nfqg0ari44kkicf5fgiarphcnwfy9kkqkizf5zv7plbdw33y3n4")))

