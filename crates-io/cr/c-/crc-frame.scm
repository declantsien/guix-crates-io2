(define-module (crates-io cr c- crc-frame) #:use-module (crates-io))

(define-public crate-crc-frame-1.0.0 (c (n "crc-frame") (v "1.0.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "04whhq6pz1dzv5y35xkwqmipzmgkpljrpfjzyjy46776arrk2cij")))

(define-public crate-crc-frame-1.0.1 (c (n "crc-frame") (v "1.0.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "0kncvwh0r4vgsp1g1y35i678h8zry8bxsn2k0cc13ck0w4x9kd9l")))

(define-public crate-crc-frame-1.0.2 (c (n "crc-frame") (v "1.0.2") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "1bbjhxkwca22n6jag4jh8d250cj0plj0iyiprgyjzy7pbilpxy3l")))

(define-public crate-crc-frame-1.0.3 (c (n "crc-frame") (v "1.0.3") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "1wmvps1r4p5csw6vn3qqc8a22rhpw1ir142kkj20ixf8pnwn73nk")))

(define-public crate-crc-frame-1.0.4 (c (n "crc-frame") (v "1.0.4") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "0s9czipnvzmz0vnm5db71szza3ifji6ib1x10h1qfkjsllym7p7k")))

(define-public crate-crc-frame-1.0.5 (c (n "crc-frame") (v "1.0.5") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "1ir13ggdwr2a7pp9b83lrnzrqymdsxvhrcymz9imrd1zx231ris7")))

(define-public crate-crc-frame-1.0.6 (c (n "crc-frame") (v "1.0.6") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "1r79cf3wdw6k985j9ggykfqyvcrm6z176bnpvdgmq78yial7ycxn")))

(define-public crate-crc-frame-1.0.7 (c (n "crc-frame") (v "1.0.7") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "0m5gblmz89sc82130adzsci2wid89lxcpxa51gyzv7yfnx24k1rf")))

(define-public crate-crc-frame-1.0.8 (c (n "crc-frame") (v "1.0.8") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.7") (d #t) (k 0)))) (h "1chxardplc32adq4vyx72k8gnzbcymyyg4p5zfhjf56kjpg4q6xg")))

(define-public crate-crc-frame-1.0.9 (c (n "crc-frame") (v "1.0.9") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "fault-injection") (r "^1.0.9") (d #t) (k 0)))) (h "1x0yg4xwzln9zsij0x5817wqp91257hwhayvy4q5j6h87dpgc9bz")))

