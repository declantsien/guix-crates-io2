(define-module (crates-io cr c- crc-core) #:use-module (crates-io))

(define-public crate-crc-core-0.1.0 (c (n "crc-core") (v "0.1.0") (h "177nk83frd779l72dqcvjs8rmds19f3gaw1l49w56h4xfxq12g80")))

(define-public crate-crc-core-0.1.1 (c (n "crc-core") (v "0.1.1") (h "1p5b4kgbmv21qdhn1002vaxsscv383hyafdlmml2x1fkq0npyk0f") (f (quote (("std") ("default" "std"))))))

(define-public crate-crc-core-0.1.2 (c (n "crc-core") (v "0.1.2") (h "1y7nlhffqxzgzapffnqvx77ra2cri13h1c9psl9ny6czd32j17nk") (f (quote (("std") ("default" "std"))))))

