(define-module (crates-io cr c- crc-catalog) #:use-module (crates-io))

(define-public crate-crc-catalog-0.1.0 (c (n "crc-catalog") (v "0.1.0") (h "0l81n7l6ln66fqw8kwwvcdx1xy0fshyyp0yznzfskci9vpx6px22")))

(define-public crate-crc-catalog-1.0.0 (c (n "crc-catalog") (v "1.0.0") (h "0a6kgpym6bqdccn7jhzvgbvzplapg18fzsa23ly61pjm9b0772y6")))

(define-public crate-crc-catalog-1.1.0 (c (n "crc-catalog") (v "1.1.0") (h "007kxrpjwr9xjb4310smbddanvl3cqbzd2wqdyfdyq2cd5ablv4b")))

(define-public crate-crc-catalog-1.1.1 (c (n "crc-catalog") (v "1.1.1") (h "00qlxgzg15fnyx6nwviibz94rjw803l2avi2k3shjfx0dnsyvbnc")))

(define-public crate-crc-catalog-2.0.0 (c (n "crc-catalog") (v "2.0.0") (h "0gks5h0pxc6ghviyhmq9cqh3f9j32xk2p4w1rrn0jbkx1mm4g4fh")))

(define-public crate-crc-catalog-2.0.1 (c (n "crc-catalog") (v "2.0.1") (h "14gc4wdrf669m10rn0cvfvy8gd4wd116l67js48di5f06fqqaa79")))

(define-public crate-crc-catalog-2.1.0 (c (n "crc-catalog") (v "2.1.0") (h "1zzkk9fjm262z5hrg4xsx77grvmmld6vq2z86s77grhaj396a09d")))

(define-public crate-crc-catalog-2.2.0 (c (n "crc-catalog") (v "2.2.0") (h "115l7pzskv5xzp9i7146rp1qrbfdi7gikig1p80p6zpham7fib4w")))

(define-public crate-crc-catalog-2.3.0 (c (n "crc-catalog") (v "2.3.0") (h "1y9jax8hjq8nmafqrvw4zqs7hvps2800kwrpdn4qrga42knzjfa9") (y #t)))

(define-public crate-crc-catalog-2.4.0 (c (n "crc-catalog") (v "2.4.0") (h "1xg7sz82w3nxp1jfn425fvn1clvbzb3zgblmxsyqpys0dckp9lqr")))

