(define-module (crates-io cr c- crc-32c) #:use-module (crates-io))

(define-public crate-crc-32c-0.1.0 (c (n "crc-32c") (v "0.1.0") (d (list (d (n "crc") (r "^3.0.1") (d #t) (k 2)) (d (n "crc32c") (r "^0.6.4") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0b1d9az1j7gsmixk51kaabs4ls4a61fbl15fil6cycwpy5fzbcck")))

