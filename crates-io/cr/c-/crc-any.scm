(define-module (crates-io cr c- crc-any) #:use-module (crates-io))

(define-public crate-crc-any-1.0.0 (c (n "crc-any") (v "1.0.0") (h "0nl320j2ayja25689a9w8m29x57masa3mailh6cw3kfj5kcghbg0") (f (quote (("develop"))))))

(define-public crate-crc-any-1.1.0 (c (n "crc-any") (v "1.1.0") (h "1hwr5r0f9j6mcif82577gyh54gsd7r09ipn5x6sff9ngbhvy0cvr") (f (quote (("develop"))))))

(define-public crate-crc-any-1.1.1 (c (n "crc-any") (v "1.1.1") (h "01rhgd36ff3n8k7iwfjg9g3b0jgkz23c9lg3rcllqkq8n8gdc9mk") (f (quote (("develop"))))))

(define-public crate-crc-any-1.1.2 (c (n "crc-any") (v "1.1.2") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)))) (h "05g7jyn4lqs90l61f3nsrhaqm4xbb4b9pabqrby19y2agh3xcw09") (f (quote (("develop"))))))

(define-public crate-crc-any-1.1.3 (c (n "crc-any") (v "1.1.3") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)))) (h "1906nlcsmr4nzg93af8pk3dnnw86k47631pdfib12807524hvzqz") (f (quote (("develop"))))))

(define-public crate-crc-any-2.0.0 (c (n "crc-any") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0dnc1qapr64g6ig5hwqbbdq9gd5szzdnf9kw62sa3gzx8xfgzlqn") (f (quote (("no_std") ("development"))))))

(define-public crate-crc-any-2.1.0 (c (n "crc-any") (v "2.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "19jcgccj8k8zn4dvi15nvfl234849gx1dd0pni0z115q0pb5s3y8") (f (quote (("no_std") ("development"))))))

(define-public crate-crc-any-2.1.1 (c (n "crc-any") (v "2.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.1") (d #t) (k 0)))) (h "1y2ajjv6k3j5yz1n9n48pg3wc2vx4idhw1drkq4b3b9hx157vx6v") (f (quote (("development"))))))

(define-public crate-crc-any-2.1.2 (c (n "crc-any") (v "2.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.2") (d #t) (k 0)))) (h "0cpgdrp39clizvq2zb7242dg2pfjj89dnagbm0m91nb2hv6g87d9") (f (quote (("development"))))))

(define-public crate-crc-any-2.1.3 (c (n "crc-any") (v "2.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (d #t) (k 0)))) (h "03dp4ini5aaq87bk4gfq542jsd0hwqh7rfh917dfqqrcplprm61k") (f (quote (("development"))))))

(define-public crate-crc-any-2.2.0 (c (n "crc-any") (v "2.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0fi6vwfbiy1x8wgiv32r7v3py61i0z4xslqrl427lsy4v8f0lbm3") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.2.1 (c (n "crc-any") (v "2.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "008pkmlld3vlh3vcisxir3bkjw7dsqr3c1dvksinbqpqhydvq0g5") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.2.2 (c (n "crc-any") (v "2.2.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0dqdy6v7n120w7azdc2v42ns6x8a7bagzhbdn9z05wc2xncgrr6z") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.2.3 (c (n "crc-any") (v "2.2.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0kc3ww4cd5cjs2xr836djmd517xvdhygw7pb3gypdas2q29r1jjy") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.0 (c (n "crc-any") (v "2.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "034kx5ss5g5ablrv85yxprmswndy83kxlg42448zvxg0nii79kwa") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.1 (c (n "crc-any") (v "2.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1w8j5vp546v7djlrypm1ijrm191vvn1c20wpikfy8zhzxh80z2ml") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.2 (c (n "crc-any") (v "2.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1r9869f9qvp3vqwvhlqbcsmcba2ai98mh9w8qbba0myc4k99rxcq") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.3 (c (n "crc-any") (v "2.3.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0rczsw72pn10cjn9f9wrirv643mr2lzawasnzvhyd91vfklmbd0g") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.4 (c (n "crc-any") (v "2.3.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1izd56xcxmdmvpzx779b1j46lq9s1vwcqqp3pj7mmp4n3j0m2lqr") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.5 (c (n "crc-any") (v "2.3.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "01wh7pb0svpi8vvqalk5rimxx6ba9xvhpvv93kaz8sclvzplny63") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.6 (c (n "crc-any") (v "2.3.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "055qmzpwm4a47d122sqgxpczv1pknh7plx4xqwpwjgd54ii7k620") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.7 (c (n "crc-any") (v "2.3.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0qhd7i09xa30q68mnjiv1gmz2k3qpbb86kca4yq8ddlb428l5xrn") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.8 (c (n "crc-any") (v "2.3.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1wzc346jf1s87lij3fpkizbxj64s3bak5qhh2l95vg223n3drsc3") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.9 (c (n "crc-any") (v "2.3.9") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1zr8fph3vls47j5455sjsqkb4jfadnlmaax8l92bwcw6100vx60d") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.11 (c (n "crc-any") (v "2.3.11") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1q9lzw1pi8ngphvqx0nf40cy08y5fvn0l6szkxrb0i64qn8hx5dr") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.10 (c (n "crc-any") (v "2.3.10") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "04vzdp9crm2al86i79dsnmdnkbyq4h0h72f4d94xdn7sbrw57fak") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.3.12 (c (n "crc-any") (v "2.3.12") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0fq85y5akcadahnj5nqbs47qhgp5cpfn2z19zc3gp4wpxr3989kr") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.4.0 (c (n "crc-any") (v "2.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1mjn9al1gf01hn7yr1vrbd0fmdmg73hvbzwlcy6f8vxg9ppnihj4") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.4.1 (c (n "crc-any") (v "2.4.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)))) (h "08d9g8mh1ihnhf9i8f5d320nkwr7l1wrip7vk8bcxpjq99l7acq7") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.4.2 (c (n "crc-any") (v "2.4.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1yvq3jfglgjy22hzr72irbz7wn43rk989z97lyh1pradqcr9zz53") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.4.3 (c (n "crc-any") (v "2.4.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1pw89ncs7gbqvxnkp5spks61qn6bcg13vwab1zml6dpnhyv4cikp") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2.4.4 (c (n "crc-any") (v "2.4.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1l5n1szzwx05i5j659gg3x7zs6b7jjg99pvvk84vcvqzi0gmw6n0") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper")))) (r "1.60")))

(define-public crate-crc-any-2.5.0 (c (n "crc-any") (v "2.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "debug-helper") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0wzs26q5cf29fhfnrkrjsr8dpai0rlm4im8b53by8rbrbzzwjbm6") (f (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper")))) (r "1.60")))

