(define-module (crates-io cr t- crt-sh) #:use-module (crates-io))

(define-public crate-crt-sh-0.1.0 (c (n "crt-sh") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pem") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "table-extract") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "18g0jnqwp7f3vdxg9dxkqi05vjnwm9k31f1dmlv1bky9pvxyn9p6")))

(define-public crate-crt-sh-0.1.1 (c (n "crt-sh") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pem") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "table-extract") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "0zh62ky4waslw0x9hcmq9vl8l4cp49makcpr0zgxyavx2k07knby")))

(define-public crate-crt-sh-0.1.2 (c (n "crt-sh") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pem") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "table-extract") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "0ynrf171k9dl7y1lm6ki3g5jmvss27zz9qmlsngc79a2rx21k8lx")))

(define-public crate-crt-sh-0.1.3 (c (n "crt-sh") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pem") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "scraper") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1i1qwm2nz5mb32yk153923g6b7jm2lg4z3k2c7q28nxd4pd9xk4q")))

