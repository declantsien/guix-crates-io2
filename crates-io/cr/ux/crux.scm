(define-module (crates-io cr ux crux) #:use-module (crates-io))

(define-public crate-crux-0.1.0 (c (n "crux") (v "0.1.0") (h "1pfrkyzslwif94y9a6qzzd95j465mjv8kwskikb2d7jfqkk6hf4r")))

(define-public crate-crux-0.2.0 (c (n "crux") (v "0.2.0") (h "0pmqwgl4h4rqnpq9yvf1cfwvngx8gddnjhswv270z0rsrsz65ws2")))

(define-public crate-crux-0.3.0 (c (n "crux") (v "0.3.0") (h "0djcrhhrq30536f8283xp1ymp8g5jp3vfxcbfbfj4jb0qyda0gcx")))

