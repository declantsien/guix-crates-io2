(define-module (crates-io cr ux crux_platform) #:use-module (crates-io))

(define-public crate-crux_platform-0.1.0 (c (n "crux_platform") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bcs") (r "^0.1.4") (d #t) (k 0)) (d (n "crux_core") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xw4xmm3wxxrm4s2w4bm0n0h58a9yf6f249dlpkydmxv2ryq8l9")))

(define-public crate-crux_platform-0.1.1 (c (n "crux_platform") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bcs") (r "^0.1.4") (d #t) (k 0)) (d (n "crux_core") (r "^0.3") (d #t) (k 0)) (d (n "crux_macros") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "18k61djph2ysqc77j8yyxa47vglrzaydhh91j9hjr4h2j9lq8ibs") (r "1.66")))

(define-public crate-crux_platform-0.1.2 (c (n "crux_platform") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bcs") (r "^0.1.5") (d #t) (k 0)) (d (n "crux_core") (r "^0.5") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q0gkgjz4r6j6s5s5vq41m92c338p9v44jd2j3a5vpha8macb772") (r "1.66")))

(define-public crate-crux_platform-0.1.3 (c (n "crux_platform") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crux_core") (r ">=0.4") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yksxzgws59ddjl1vmyizr3y7nldhvzmbf5kh4s24wm68plv5kda") (r "1.66")))

(define-public crate-crux_platform-0.1.4 (c (n "crux_platform") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.6") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "18id8hvh0v51c8nc964b9czbbas4yl5gkg3izsnvzpbvb8s2niki") (r "1.66")))

(define-public crate-crux_platform-0.1.5 (c (n "crux_platform") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.6") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dzfphw6l99iwvx1z2mhn6ylp88w5d4xyhhvn4bcnv82mgf32xhg") (r "1.66")))

(define-public crate-crux_platform-0.1.6 (c (n "crux_platform") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bcsxa93piql31wil2qx2myss4ss73id8clv7h6f0jy03zks68c7") (r "1.66")))

(define-public crate-crux_platform-0.1.7 (c (n "crux_platform") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pbzhspi8d8rswigc9d4m3djwbf4q6mj8hjrwxk68awj6chcck68") (r "1.66")))

(define-public crate-crux_platform-0.1.8 (c (n "crux_platform") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "09b6h2zkji5mwa2x8r0zdpbly6di2wz86vc7ajnsjxdwq83cjlvb") (r "1.66")))

(define-public crate-crux_platform-0.1.9 (c (n "crux_platform") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vn32jb5r31bvzbr9l5bnd372pcdzy35r9407s9c1hkch7ccfzb9") (r "1.66")))

(define-public crate-crux_platform-0.1.10 (c (n "crux_platform") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vr445div46rz87x7n069x5c4ra5274r8fjkrvflwb0zjgqfh9qw") (r "1.66")))

(define-public crate-crux_platform-0.1.11 (c (n "crux_platform") (v "0.1.11") (d (list (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)))) (h "04b5490zhzf8lif5s2c8nqwr397930qck1gf2a1405bayih1kcsp") (r "1.66")))

(define-public crate-crux_platform-0.1.12-rc.1 (c (n "crux_platform") (v "0.1.12-rc.1") (d (list (d (n "crux_core") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)))) (h "13kvwjgfmwcrq7jgmljhqxcyl9bafb5x3fmsj42pm6cyxgb7iczs") (r "1.66")))

(define-public crate-crux_platform-0.1.12-rc.2 (c (n "crux_platform") (v "0.1.12-rc.2") (d (list (d (n "crux_core") (r "^0.8.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yqnf11yypv89lhcqlx6k20fkg64djhypby6axs2vkmps6mkjr9y") (r "1.66")))

(define-public crate-crux_platform-0.1.12-rc.3 (c (n "crux_platform") (v "0.1.12-rc.3") (d (list (d (n "crux_core") (r "^0.8.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "03qp9rrxq0yzlswhj31ak64w0c5z19qg41k4f3h7508w542qscqx") (r "1.66")))

(define-public crate-crux_platform-0.1.12 (c (n "crux_platform") (v "0.1.12") (d (list (d (n "crux_core") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "0spp4z8vnwpp16n39krzzsix1jfryg0r2vnfifvzhwzd8l1l6zyc") (r "1.66")))

