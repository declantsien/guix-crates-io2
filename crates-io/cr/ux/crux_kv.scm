(define-module (crates-io cr ux crux_kv) #:use-module (crates-io))

(define-public crate-crux_kv-0.1.0 (c (n "crux_kv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bcs") (r "^0.1.4") (d #t) (k 0)) (d (n "crux_core") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kx1s1i8ldp2xrxpa1lc2y4607kkiykbm2jmc2prv5z25kivfv8q")))

(define-public crate-crux_kv-0.1.1 (c (n "crux_kv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bcs") (r "^0.1.4") (d #t) (k 0)) (d (n "crux_core") (r "^0.3") (d #t) (k 0)) (d (n "crux_macros") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "138b5kk0gmnlcfjhh73qh19jkxwgz6cbhqjwz6l1fgfqdnldlbg5") (r "1.66")))

(define-public crate-crux_kv-0.1.2 (c (n "crux_kv") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bcs") (r "^0.1.5") (d #t) (k 0)) (d (n "crux_core") (r "^0.5") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vrikiwq2i9fgyairiisdmzyzgr5dc9zfwdzs4kianarwgwj2hv4") (r "1.66")))

(define-public crate-crux_kv-0.1.3 (c (n "crux_kv") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crux_core") (r ">=0.4") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "09067qxgzznw1amwbpcwkvrv5v7inx703vmfys6m9jr1fmq6frl3") (r "1.66")))

(define-public crate-crux_kv-0.1.4 (c (n "crux_kv") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.6") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hykq2yirlzaqpv4q72k54ypi9rlaqbvs5nbx9qwr9p7139h92pv") (r "1.66")))

(define-public crate-crux_kv-0.1.5 (c (n "crux_kv") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.6") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "00irqbrlb1h2g4axqg8739dclz9hcbxi7z98vvn9wybnklh5c24a") (r "1.66")))

(define-public crate-crux_kv-0.1.6 (c (n "crux_kv") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "06kqlxvifp6jgi23wvc6mhpdpjp1p76ba9bkyy9wg82qz6qx1qk5") (r "1.66")))

(define-public crate-crux_kv-0.1.7 (c (n "crux_kv") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1igsyb0bffgcdyy2qzspckh99jwy45pgvv7byrc64zfnjgpjcvdm") (r "1.66")))

(define-public crate-crux_kv-0.1.8 (c (n "crux_kv") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v3cj6a3v1inn9n42y9l3nmqw32c6pzwf39ihl0rwklw2i401a1c") (r "1.66")))

(define-public crate-crux_kv-0.1.9 (c (n "crux_kv") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "190zcfnx7z2yclj3bqhclwxbip6ircxmqc4ly2d8qch4pwlz8z8p") (r "1.66")))

(define-public crate-crux_kv-0.1.10 (c (n "crux_kv") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0faq587xhfsiqza7s09q4gwbc4vdqzyklw4qaz0ik49qgncvk6vz") (r "1.66")))

(define-public crate-crux_kv-0.2.0 (c (n "crux_kv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "06mp2vwz9sdil8j5q10ngwsm2s5gbcdnqsvdc5h2hwz5ai0arrhz") (r "1.66")))

(define-public crate-crux_kv-0.3.0 (c (n "crux_kv") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "041ddl57nwlc9z4kczyyga1ypzvvwsn103awplkj5gw8q0qqi1kq") (r "1.66")))

(define-public crate-crux_kv-0.4.0 (c (n "crux_kv") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1j4lxv13h8ch884ncgk2rfc54ax5p4184y227r7s8277gwdgpp2v") (r "1.66")))

(define-public crate-crux_kv-0.4.1-rc.1 (c (n "crux_kv") (v "0.4.1-rc.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "15xljs424jhxzpq8ysni6q00g6g9nvwlcgcnrcplzwjk6h84jafb") (r "1.66")))

(define-public crate-crux_kv-0.4.1-rc.2 (c (n "crux_kv") (v "0.4.1-rc.2") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "05khkmbsl9zsfs73y6n57a4v3hn7jfr0l8cnp3q8qfnj4mcj14hj") (r "1.66")))

(define-public crate-crux_kv-0.4.1-rc.3 (c (n "crux_kv") (v "0.4.1-rc.3") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1rlsmjizzvn5sxsjwj0b6203wiw7jsh1svxj6dlqp8znpx60l46w") (r "1.66")))

(define-public crate-crux_kv-0.4.1 (c (n "crux_kv") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0asg9ik6089mjxwfvkv8r8ryqlk6dhq4vr8394k9xgkf23giyh31") (r "1.66")))

