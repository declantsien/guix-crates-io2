(define-module (crates-io cr ux crux_time) #:use-module (crates-io))

(define-public crate-crux_time-0.1.0 (c (n "crux_time") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bcs") (r "^0.1.4") (d #t) (k 0)) (d (n "crux_core") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)))) (h "1482mba4v4h30g10syd58i7bjsnmww6il2ak9l023j0nx0iyh7ja")))

(define-public crate-crux_time-0.1.1 (c (n "crux_time") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bcs") (r "^0.1.4") (d #t) (k 0)) (d (n "crux_core") (r "^0.3") (d #t) (k 0)) (d (n "crux_macros") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1djy5a41jssc02rb99y1dpw0ww8ss5iqpwby1hylcgjx9flh724k") (r "1.66")))

(define-public crate-crux_time-0.1.2 (c (n "crux_time") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bcs") (r "^0.1.5") (d #t) (k 0)) (d (n "crux_core") (r "^0.5") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1riqb96a2kkafc8da02gkj9i3harhyih5v4j8vmr0b31alvd2ddv") (r "1.66")))

(define-public crate-crux_time-0.1.3 (c (n "crux_time") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crux_core") (r ">=0.4") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y3mj1bk4x7qlg5gckrj9i5wwj3b4ml9jkysihypd3mpz6g7450q") (r "1.66")))

(define-public crate-crux_time-0.1.4 (c (n "crux_time") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crux_core") (r ">=0.4") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "1254p0n6czrg6a6xn0xlcmh3462jia2cc43rnmr2gq7611zrhnnh") (r "1.66")))

(define-public crate-crux_time-0.1.5 (c (n "crux_time") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.6") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "13l2l0sppafkdzsws5yxi2va9vr2d53ph4r8dypvsv844g3rvsr9") (r "1.66")))

(define-public crate-crux_time-0.1.6 (c (n "crux_time") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.6") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dmb6zsxcih9l57ip0x29qpj0s01snspqapvp7qm3qar04xhywji") (r "1.66")))

(define-public crate-crux_time-0.1.7 (c (n "crux_time") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "04nxzd9nk8vdlvrn5acc813sqfip0y3620q88klq5m90ap1yq577") (r "1.66")))

(define-public crate-crux_time-0.1.8 (c (n "crux_time") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m831z6g5qmmik0yqm49aibk75942cfwkxwx9ydaiif3zr31crqn") (r "1.66")))

(define-public crate-crux_time-0.2.0 (c (n "crux_time") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "crux_macros") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r46z6m09y91dpkw7dx8jpbw1982p8s7dlvhj39m8gyhd0psffr1") (r "1.66")))

(define-public crate-crux_time-0.3.0 (c (n "crux_time") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v5z4cbpjnfvapmh5fkj3x7cvmw43zw4z6dl5pa3hdv27ab7xl04") (r "1.66")))

(define-public crate-crux_time-0.3.1 (c (n "crux_time") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n4zkji7kbdkgsbm22izm382nl0gkws9qs51npsjppx0mskkx7p9") (r "1.66")))

(define-public crate-crux_time-0.4.0 (c (n "crux_time") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1gi92kn89h5vsj6r7m5mjcswhhv8igi83iyh3zv44jmwqcv4ki1b") (r "1.66")))

(define-public crate-crux_time-0.4.1 (c (n "crux_time") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "097lb34rlc7njgqr8vk5zhdi5gyd4v6n62lig6h4gaq47961iszf") (r "1.66")))

(define-public crate-crux_time-0.4.2 (c (n "crux_time") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "crux_core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1d978s5kwwkq96x6ncwi3gkql1ad41xhp50j5l1ncgxkadzajds9") (r "1.66")))

(define-public crate-crux_time-0.4.3-rc.1 (c (n "crux_time") (v "0.4.3-rc.1") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0h4h5ns1dgks3h3vx7525j8jkg9qfxnsjvjgr9kryz1qvq64q8n4") (r "1.66")))

(define-public crate-crux_time-0.4.3-rc.2 (c (n "crux_time") (v "0.4.3-rc.2") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1zkdswdjivhvj55d4ncdpnb9nrzm98yywwcixcg1g2dpldhbnjim") (r "1.66")))

(define-public crate-crux_time-0.4.3-rc.3 (c (n "crux_time") (v "0.4.3-rc.3") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "08xwwxq29ksfm12q0ib13pplp12d8qvn4vdil5i0sqxdp7zs69my") (r "1.66")))

(define-public crate-crux_time-0.4.3 (c (n "crux_time") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "crux_core") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0hjh2rqsz1fxml63pgvmagpvxn3ysvcd492kdwnrdpv62hjhfakq") (r "1.66")))

