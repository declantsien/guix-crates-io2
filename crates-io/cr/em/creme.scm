(define-module (crates-io cr em creme) #:use-module (crates-io))

(define-public crate-creme-0.1.0 (c (n "creme") (v "0.1.0") (h "1kmaxhg42dqm6gw38pqdzwg8sfc7ira2kysys4lyd59z8hqkz5js") (y #t)))

(define-public crate-creme-0.0.1 (c (n "creme") (v "0.0.1") (h "0pw06fwnv6yy0lmmyhzjla3f367hhblrchjnm9k8xrnq8sh443gd")))

