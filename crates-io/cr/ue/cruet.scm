(define-module (crates-io cr ue cruet) #:use-module (crates-io))

(define-public crate-cruet-0.11.4 (c (n "cruet") (v "0.11.4") (d (list (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1aqnh9grq7qcggkndacdx6d0xby1kgdyxi1kk5frfxknw4166c34") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-cruet-0.12.0 (c (n "cruet") (v "0.12.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07s3fff5bjm6hlvjymsr29bgq5fi6gj8vbpbsxzly1im899vjcc1")))

(define-public crate-cruet-0.13.0 (c (n "cruet") (v "0.13.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bzq841nx7zy3y32f607m3yzy41ncy4bf8hr2g4j7m3j27pxdakx")))

(define-public crate-cruet-0.13.1 (c (n "cruet") (v "0.13.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0w3jsf5cnxffjznh5w2sh50gyk45rkgps1qibsyha34dmpwml042")))

(define-public crate-cruet-0.13.2 (c (n "cruet") (v "0.13.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qzwf3xln46mqqry5lgbw5dcmhmahcsj393hdyqixlf7cm3if3af")))

(define-public crate-cruet-0.13.3 (c (n "cruet") (v "0.13.3") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1774i91y3azksfii0dspxa2nxsnhz5dz5wcdvrvbw57nv21rwfhi")))

(define-public crate-cruet-0.14.0 (c (n "cruet") (v "0.14.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bdfbidy1x456gmsnk7b1a3nhrsqwqf1zq5ijyy9c94p8fan0ck1")))

