(define-module (crates-io cr oa croaring-mw) #:use-module (crates-io))

(define-public crate-croaring-mw-0.4.5 (c (n "croaring-mw") (v "0.4.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.5") (d #t) (k 0) (p "croaring-sys-mw")) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "08b2fz15vk4xkdscfn0k3s3zmf9kplw9rqid72ff9wsbrrqybpmw") (f (quote (("compat" "croaring-sys/compat"))))))

