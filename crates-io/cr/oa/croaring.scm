(define-module (crates-io cr oa croaring) #:use-module (crates-io))

(define-public crate-croaring-0.1.0 (c (n "croaring") (v "0.1.0") (d (list (d (n "croaring-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0kz40kxj5dxram0dg15z1wnhdk9b0kv6cndmc0l9sab076gksgqq")))

(define-public crate-croaring-0.1.1 (c (n "croaring") (v "0.1.1") (d (list (d (n "croaring-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "196s8qnaka67fzznb7d7mx1a49a58x66jpag6d7nr1m5l0jafzml")))

(define-public crate-croaring-0.1.2 (c (n "croaring") (v "0.1.2") (d (list (d (n "croaring-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0x4ymbzkkags4g8fn3dh39ih3cq868mjpviyj53m87ldn93hxizz")))

(define-public crate-croaring-0.1.3 (c (n "croaring") (v "0.1.3") (d (list (d (n "croaring-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.4") (d #t) (k 2)))) (h "0wqfzz01pdi62npmrrjrl5j486ya6jfd4bsbl4x55kg7jr624i58")))

(define-public crate-croaring-0.2.0 (c (n "croaring") (v "0.2.0") (d (list (d (n "croaring-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.4") (d #t) (k 2)))) (h "15bmziyvy4qw7v3lgdiqr8l0j48m0wgxy43606ffqldlhd5y13v5")))

(define-public crate-croaring-0.2.1 (c (n "croaring") (v "0.2.1") (d (list (d (n "croaring-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.4") (d #t) (k 2)))) (h "0kqdq238g3cpnp2q8sg6kql4ipvbddbzyikrlvzfw2mpw24zz3m2")))

(define-public crate-croaring-0.3.0 (c (n "croaring") (v "0.3.0") (d (list (d (n "croaring-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0h9fp8dn124bnp31hww2n0jmkhj67svpjqlfl7rjb7gycxy3mln7")))

(define-public crate-croaring-0.3.1 (c (n "croaring") (v "0.3.1") (d (list (d (n "croaring-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1ss3ck6z6n9vhwc6ki0fjzm4qky90wm843d3aaazw7q0597vljiz")))

(define-public crate-croaring-0.3.2 (c (n "croaring") (v "0.3.2") (d (list (d (n "croaring-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1nasgjyhamr5kraxihrpngvyg5i0ws5saffialifi6r2f3dsd4k4")))

(define-public crate-croaring-0.3.3 (c (n "croaring") (v "0.3.3") (d (list (d (n "croaring-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0yzgm4ixgpyy2vkigxs1q2q4gkrn63f9y6zvfkkn9gb5fjj9ji4a")))

(define-public crate-croaring-0.3.4 (c (n "croaring") (v "0.3.4") (d (list (d (n "croaring-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0bi9hnmnb8b33qkqzg620n1bd3gf78c9q6nfni9wl881brjc1jjh")))

(define-public crate-croaring-0.3.5 (c (n "croaring") (v "0.3.5") (d (list (d (n "croaring-sys") (r "^0.3.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "08ivv7kb835d4liygp902wywgvixybfl4k1z00gnrym16js6w55g")))

(define-public crate-croaring-0.3.6 (c (n "croaring") (v "0.3.6") (d (list (d (n "croaring-sys") (r "^0.3.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "03nydjr8zs05kby3g1fy9mijxvgm06ckbchy6z68lh7hxl01d5iq")))

(define-public crate-croaring-0.3.7 (c (n "croaring") (v "0.3.7") (d (list (d (n "croaring-sys") (r "^0.3.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1kz9nsf679s9hjs3i9a4iw8jb0ycrdnf5v7rbv17y8shy4qj9j52")))

(define-public crate-croaring-0.3.8 (c (n "croaring") (v "0.3.8") (d (list (d (n "croaring-sys") (r "^0.3.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0f1whjf1wn544wb4xgfnq6jhqhnwhs5ajs60p7p73fm9x3n51cyj")))

(define-public crate-croaring-0.3.9 (c (n "croaring") (v "0.3.9") (d (list (d (n "croaring-sys") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0fry237b4pnvhglh1kc05cvjfnfrkbxcqfvrkmfxrpy9rrh2s5bi")))

(define-public crate-croaring-0.4.0 (c (n "croaring") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1vpkxiw46p4qp3dp23yp7vnyvd64xaz0lgmyb97klcllr485jx1q")))

(define-public crate-croaring-0.4.1 (c (n "croaring") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1w7fp1avq9sj2r54w78r7agkw4rg5klqkj3ccrxim574icwsw0s2")))

(define-public crate-croaring-0.4.2 (c (n "croaring") (v "0.4.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0kr4lk2j80r33c84i9788js1sq1060js01adqdkp20435l9mfj41")))

(define-public crate-croaring-0.4.3 (c (n "croaring") (v "0.4.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1kri9zsrg0frgmp8812sab1ps2id55f64jmwhbbjdanq681cc2sk")))

(define-public crate-croaring-0.4.4 (c (n "croaring") (v "0.4.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0r2lj00rcg3gyh1ga92xz33z6a20y8ps2f7rsvmrv3mg3iy0bsaj")))

(define-public crate-croaring-0.4.5 (c (n "croaring") (v "0.4.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "1sx2q3d4scg6907hay4dz9d4jb75lir56gcylqnll3gdmkkf79cf")))

(define-public crate-croaring-0.4.6 (c (n "croaring") (v "0.4.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.4.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0g6n9vci3nrhanaxnq9333vw7zhvg5iqagn9lpbngh4cgnni83d0")))

(define-public crate-croaring-0.5.0 (c (n "croaring") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "roaring") (r "^0.5.2") (d #t) (k 2)))) (h "0f9g0nb3w14lpysibnxwzx33ndsh87kn3f2yj2xp3x4b3hygmzym")))

(define-public crate-croaring-0.5.1 (c (n "croaring") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "croaring-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.7") (d #t) (k 2)))) (h "0ffigq8l9b98483n98nyssi3g9mk9325wj4s9xdffpvcqbvyvqc6")))

(define-public crate-croaring-0.5.2 (c (n "croaring") (v "0.5.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.5.2") (d #t) (k 0) (p "croaring-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.8") (d #t) (k 2)))) (h "0rhq5k1ihan3qxh726a2g2dnji9xhp1xa8hk7is2mylzlylfxwdg")))

(define-public crate-croaring-0.6.0 (c (n "croaring") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.6.0") (d #t) (k 0) (p "croaring-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.8") (d #t) (k 2)))) (h "1lxnfkfc5964ycncv9ib1dxhxrca7qn39zvxckgadq77b0npl6jp")))

(define-public crate-croaring-0.6.1 (c (n "croaring") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.6.1") (d #t) (k 0) (p "croaring-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.8") (d #t) (k 2)))) (h "1gr4j8qxpp0qr58my7gjx73a4qna5d0jmd1dglhwnchm7pdsapk6")))

(define-public crate-croaring-0.6.2 (c (n "croaring") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.6.2") (d #t) (k 0) (p "croaring-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "1kmbq3l5c444cr83rv5in1q7dvy8pw2fyq39gsldqbq409syflvw")))

(define-public crate-croaring-0.6.3 (c (n "croaring") (v "0.6.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.6.3") (d #t) (k 0) (p "croaring-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "0ad8jsmr9zs549d9hnhycs8xx4q1bbwgv5xv5j5m1h1bygxz4170")))

(define-public crate-croaring-0.7.0 (c (n "croaring") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.7.0") (d #t) (k 0) (p "croaring-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "1ma5lmqr73v2pif4mw4axhk62q5xsffk4cmn12bfbgz8p99gldxn") (f (quote (("default") ("buildtime_bindgen" "ffi/buildtime_bindgen"))))))

(define-public crate-croaring-0.8.0 (c (n "croaring") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.8.0") (d #t) (k 0) (p "croaring-sys")) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "1kysryjzysdk9sqhy1px9vwaa1n734qkg00dj6virz4nljyixy9k") (f (quote (("default") ("buildtime_bindgen" "ffi/buildtime_bindgen"))))))

(define-public crate-croaring-0.8.1 (c (n "croaring") (v "0.8.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.8.1") (d #t) (k 0) (p "croaring-sys")) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "0jkp2hpk2h26h5n40xrx0pg27ww5qycbmd6bqlz5kihcnjm690pb") (f (quote (("default") ("buildtime_bindgen" "ffi/buildtime_bindgen"))))))

(define-public crate-croaring-0.9.0 (c (n "croaring") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.9.0") (d #t) (k 0) (p "croaring-sys")) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "1r9rrp3ly5mxy9yq21lj1339h0nvm4892s2qya5i3nw17x0yjfbm") (f (quote (("default") ("buildtime_bindgen" "ffi/buildtime_bindgen"))))))

(define-public crate-croaring-1.0.0 (c (n "croaring") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ffi") (r "^1.0.0") (d #t) (k 0) (p "croaring-sys")) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "13vvlnn54jf0nk1iv66xp8cq1ixa59xv8gnayqfj2g4ncbq3kk5g") (f (quote (("default") ("buildtime_bindgen" "ffi/buildtime_bindgen")))) (y #t)))

(define-public crate-croaring-1.0.1 (c (n "croaring") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ffi") (r "^1.0.0") (d #t) (k 0) (p "croaring-sys")) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "040hvnqxlbbfl21ww7hh50wfkvzx3b1yhls79x6cw02v4ykz0rkj") (f (quote (("default") ("buildtime_bindgen"))))))

(define-public crate-croaring-1.1.0 (c (n "croaring") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ffi") (r "^2.0.0") (d #t) (k 0) (p "croaring-sys")) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "roaring") (r "^0.10") (d #t) (k 2)))) (h "11l03jz0yaw5ld2lyf28a7i9jnp0yr4bipw2mlqy94scm3yaw7k1") (f (quote (("default") ("buildtime_bindgen"))))))

