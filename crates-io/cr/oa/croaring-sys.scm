(define-module (crates-io cr oa croaring-sys) #:use-module (crates-io))

(define-public crate-croaring-sys-0.1.0 (c (n "croaring-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01dq0whs3pl52vvsaxs4629ys2ma94dwgd54zhas6gi1wpyayh6k")))

(define-public crate-croaring-sys-0.1.1 (c (n "croaring-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03xiir9pi9khbr1yzx62mxpbcdpqfplwbh6fslnhbfh118wn5iz6")))

(define-public crate-croaring-sys-0.1.2 (c (n "croaring-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kg4ba9w4dbfzh29cgn957b6wzbnav8yw42b9gndah7av30dnw14")))

(define-public crate-croaring-sys-0.1.3 (c (n "croaring-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qc1k7jsmhlz69vafndnm66rdn6n68xpkyknl2drc9prk75znslj")))

(define-public crate-croaring-sys-0.2.0 (c (n "croaring-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pbr7r84z5v15z5i4nv6pm1paa0g6f3wn7rf24pcsxan2w8na416")))

(define-public crate-croaring-sys-0.2.1 (c (n "croaring-sys") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zp64jslna4l0ijyh1f48z57n9sbvwxwfqdh8pxh10hwjbbnkbav")))

(define-public crate-croaring-sys-0.3.0 (c (n "croaring-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.23.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "154ffvmm56ry3myvhl2fg19api5gn06ch5spdxh66h35m9kyhk8i")))

(define-public crate-croaring-sys-0.3.1 (c (n "croaring-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.23.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y60al53w4h790yzcs024rbcf4p7kz5ari5wv3mhwpbpsl24s3gx")))

(define-public crate-croaring-sys-0.3.3 (c (n "croaring-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "160wnvz24zmj0yvzsyk1k7s3pbjw5g20gw92wh8ym6w9yb5mk2qn")))

(define-public crate-croaring-sys-0.3.4 (c (n "croaring-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1ahpim938qn621wl7w0idpxxz6ngl4x1qzfq0n1r14ra244jizdw")))

(define-public crate-croaring-sys-0.3.5 (c (n "croaring-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1gd7azsyiv1g3gkf3nd753f05gl9dm3zzk0d3qhv02gld70x0ax3")))

(define-public crate-croaring-sys-0.3.6 (c (n "croaring-sys") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "18gxgih5czj3lx4mlsfysawv1qhnzkz0hg08sq3326wk07pl98s8")))

(define-public crate-croaring-sys-0.3.7 (c (n "croaring-sys") (v "0.3.7") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1icj6yy5zclwz09kymfz533y5lzd6vd7cdfhwn5hjr466v548vnk")))

(define-public crate-croaring-sys-0.3.8 (c (n "croaring-sys") (v "0.3.8") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "19s37m4qral5s2892659snqjdla7a2kdra0hck71nnfz7grh0ssl")))

(define-public crate-croaring-sys-0.3.9 (c (n "croaring-sys") (v "0.3.9") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0rrd3r13js3xv99599vwf6lyhkm7kb5bf49rpqcc8zz6fpws915c")))

(define-public crate-croaring-sys-0.4.0 (c (n "croaring-sys") (v "0.4.0") (d (list (d (n "bindgen") (r ">= 0.37, <= 0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1rqawbpmbr5cgrc235x68w3lm3bkq7w39zw25la8phx0171x20b7")))

(define-public crate-croaring-sys-0.4.1 (c (n "croaring-sys") (v "0.4.1") (d (list (d (n "bindgen") (r ">= 0.37, <= 0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1fvs1h6g6v4wzkhmqm6wx5fik50fy637wpbrq62q5hjv0m9ixxsi")))

(define-public crate-croaring-sys-0.4.2 (c (n "croaring-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1x6vvd0ka1zs099xva8sj1lcbnxfa542hlkamd2iq5xbnbrbp0rw")))

(define-public crate-croaring-sys-0.4.3 (c (n "croaring-sys") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0jhqkand7kv3g4wdrgvzjp8dia2lg1nrsadb5q518khdply73h9g")))

(define-public crate-croaring-sys-0.4.4 (c (n "croaring-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1j6dv9ki3y098kzym0wsswgsbqdlgr8y0kb08z2nwinwfmnvdlxp")))

(define-public crate-croaring-sys-0.4.5 (c (n "croaring-sys") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "0rpqw6ffwbc2as90s5y62v1xhyk48k12c2zvpiy1zkjsr3iz13dz")))

(define-public crate-croaring-sys-0.4.6 (c (n "croaring-sys") (v "0.4.6") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "11jszcdcd71502dr80ypxxa9qkvvjwcwvirvwihklh5v05js9mn5")))

(define-public crate-croaring-sys-0.5.0 (c (n "croaring-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "09wfz7bvjr925z0v69cxinx1b2asidhhb58v1fl646z4yrrwadls")))

(define-public crate-croaring-sys-0.5.1 (c (n "croaring-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cs9chvrgw4sr5dfpak4npzqr80vpwnhh4pf32qp4abwnkc3sgm2")))

(define-public crate-croaring-sys-0.5.2 (c (n "croaring-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ns76ini8py44gq90awlvl7jxq7jf8p6jilqyxrlbq6ml4rpxmr1")))

(define-public crate-croaring-sys-0.6.0 (c (n "croaring-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09f5zdq83ihdgrwlw7kqs8sb910hwcs7vqjs0ygj5jm4lwsiqlyf")))

(define-public crate-croaring-sys-0.6.1 (c (n "croaring-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ikipk02qbivvba7vywbszzmwdpkm9mwyv78ylwj6xyfkvascx16")))

(define-public crate-croaring-sys-0.6.2 (c (n "croaring-sys") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1575lx8dvwym7bgqdkzi49kns57mq8v3r7k338jmvvpa549bhs8g")))

(define-public crate-croaring-sys-0.6.3 (c (n "croaring-sys") (v "0.6.3") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qvdi9clghlm87fvd4ac8c5a46hba19pjphs336z40cw5id90had")))

(define-public crate-croaring-sys-0.7.0 (c (n "croaring-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.62") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pq9g60gwwza6jzh85bc2k0h7qz12vw83x8yi81hn6sfm4d1h28z") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-croaring-sys-0.8.0 (c (n "croaring-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.62") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0zbqq0qx85hi5by1sbn8dvqmmvylizn6r0aj9925ii3dbp3s8hhd") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-croaring-sys-0.8.1 (c (n "croaring-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.62") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0fyg8x0h21bz405g2rvpr9lw7dd2yvp62fix60rb42vl4pxkmkyz") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-croaring-sys-0.9.0 (c (n "croaring-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.62") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "15d4hhbzj255pzvijqfrg6kip6gn84ij5chfdnk3vb5vrqixq81y") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-croaring-sys-1.0.0 (c (n "croaring-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.62") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "13fsyk2ldpc61k066xn46n8gi9zn8vxd1qqkxqf35pabzbxh7sll") (f (quote (("default") ("buildtime_bindgen" "bindgen"))))))

(define-public crate-croaring-sys-1.1.0 (c (n "croaring-sys") (v "1.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1yq7sfy4fsl4fpvgq3ng2fj88wwv8bphgg4yjikp0jirii4i4wg4") (f (quote (("default") ("buildtime_bindgen"))))))

(define-public crate-croaring-sys-2.0.0 (c (n "croaring-sys") (v "2.0.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0zxiqgk3d62c17rnbc9g0vz93sg14sf9sn05yikkvhq4gh160lmb") (f (quote (("default") ("buildtime_bindgen"))))))

