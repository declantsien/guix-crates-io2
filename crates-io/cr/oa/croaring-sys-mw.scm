(define-module (crates-io cr oa croaring-sys-mw) #:use-module (crates-io))

(define-public crate-croaring-sys-mw-0.4.5 (c (n "croaring-sys-mw") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "1vkiqbb27h7zhkmd5gry2hcdw89pca7aqk85dd94r9cz4rvw2lpa") (f (quote (("compat"))))))

