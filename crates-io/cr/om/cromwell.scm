(define-module (crates-io cr om cromwell) #:use-module (crates-io))

(define-public crate-cromwell-0.1.0 (c (n "cromwell") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "1b4kfha8zplpivmravw7s3axxv4ihbwvgg788va8kb7c4x7amz2r")))

(define-public crate-cromwell-0.1.1 (c (n "cromwell") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "1lmkcl2015ma5bvnpxcmql29yz3ihls5ikz4kpw3rh86ay56i1y4")))

(define-public crate-cromwell-0.1.2 (c (n "cromwell") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "1cfqc3ihz9jm0wpjy3w32c98gzcwxdxbv2s1ardnmwfm98bf373h")))

(define-public crate-cromwell-0.1.3 (c (n "cromwell") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0a5dp4il8vr7ks0bqmaardvj37rh0r08lwxxzkgr1armsrr7b00s")))

(define-public crate-cromwell-0.1.4 (c (n "cromwell") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "094df6rwigdi6v79wlvwzipn4xjwar8rvc2jc7pzmrbbnl7xmkg0")))

(define-public crate-cromwell-0.1.5 (c (n "cromwell") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0irpzdjcqf4khd9v59l4zl29vs5sc8li4dq5x9r6286x9gagmyc1")))

