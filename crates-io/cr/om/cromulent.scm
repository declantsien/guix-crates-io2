(define-module (crates-io cr om cromulent) #:use-module (crates-io))

(define-public crate-cromulent-0.1.0 (c (n "cromulent") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wordexp-sys") (r "^1") (d #t) (k 0)))) (h "1v2h4pl42rfv21jhzgiq3mdskaarb9wfx23hr3dnfmf1mpyg0a85")))

(define-public crate-cromulent-0.1.1 (c (n "cromulent") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wordexp-sys") (r "^1") (d #t) (k 0)))) (h "0m1ykl1bxhy66dxmpyn96ynhrbrmrnl6sm01yn5a09c5r4mhsgss")))

