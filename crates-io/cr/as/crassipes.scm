(define-module (crates-io cr as crassipes) #:use-module (crates-io))

(define-public crate-crassipes-0.0.1 (c (n "crassipes") (v "0.0.1") (d (list (d (n "crs-bind") (r "^0.1.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "0r41xhj4cnplj4fbi8lx11y601a6fkqg5z6vq2g5fwzlipf5kf4p")))

(define-public crate-crassipes-0.0.2 (c (n "crassipes") (v "0.0.2") (d (list (d (n "crs-bind") (r "^0.1.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "03vr6ih16k5qvgiwxxzb4pjcbrr2divvrq61gm8ww8hwxpsmgq9j")))

(define-public crate-crassipes-0.1.1 (c (n "crassipes") (v "0.1.1") (d (list (d (n "crs-bind") (r "^0.1.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "0ladfcnqqx90y9f69hcbscac5khawfl5mq8l6b0144m7b71lh1br")))

(define-public crate-crassipes-0.1.2 (c (n "crassipes") (v "0.1.2") (d (list (d (n "crs-bind") (r "^0.1.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "1849s0wrl5bqmp2a2s9jalydsbg5xh8a0yfgwm6xq8n896w2z2gr")))

(define-public crate-crassipes-0.2.0 (c (n "crassipes") (v "0.2.0") (d (list (d (n "crs-bind") (r "^0.1.3") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "1ybpx5lrc5ma47dkig2jqw8v70hp4675z302y7wgdj677yxa57l5")))

(define-public crate-crassipes-0.2.1 (c (n "crassipes") (v "0.2.1") (d (list (d (n "crs-bind") (r "^0.1.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)))) (h "0y8l20sgpn10w2bikzcflf4syp0x0fp4py8s7mib30w01p66jw1l")))

