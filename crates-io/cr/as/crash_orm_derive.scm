(define-module (crates-io cr as crash_orm_derive) #:use-module (crates-io))

(define-public crate-crash_orm_derive-0.1.0 (c (n "crash_orm_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yx6129i02sjbnk560xdaq4yn35n0l0rsp29yr6vqmx7nv20l5q9")))

(define-public crate-crash_orm_derive-0.1.1 (c (n "crash_orm_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02zawjn7ifd387gwscb0l1y5awy9vgxib4ymqxj2qfgb4030d3m7")))

(define-public crate-crash_orm_derive-0.1.2 (c (n "crash_orm_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "140ac24br47mvnxa8cwbygy5kqxqx96f6svh23bwyizw5d5n8648")))

(define-public crate-crash_orm_derive-0.1.3 (c (n "crash_orm_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b2ialnjc4qcz61x4x8faarfrnrp2d59vr2546wg91vwc5ivs9hx")))

(define-public crate-crash_orm_derive-0.1.4 (c (n "crash_orm_derive") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nzzsic956gaxb0vd3q0klhjivr4x8wh5qq837x0v62ccj9zrwn7")))

(define-public crate-crash_orm_derive-0.1.5 (c (n "crash_orm_derive") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s79kwajx3g4pxbm5ay10yy06i3q5aci3l1gx3k22pqnbjwqv9d1")))

(define-public crate-crash_orm_derive-0.2.0 (c (n "crash_orm_derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nxy3q281rhr9mjl0qxjlfv1zv85rknks0zyzl6dilq0ywx9sx9i")))

(define-public crate-crash_orm_derive-0.2.1 (c (n "crash_orm_derive") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1njak8mirm1if1wgyznbgn13sbzfhvx1792bm9a31bc1387d3pc8")))

(define-public crate-crash_orm_derive-0.3.0 (c (n "crash_orm_derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mkdlaskv3sq3x0hk3rh1ygn61q54ml4mpwhax4ikqjqc9km2ffa")))

