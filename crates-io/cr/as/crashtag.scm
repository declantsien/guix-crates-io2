(define-module (crates-io cr as crashtag) #:use-module (crates-io))

(define-public crate-crashtag-0.1.0 (c (n "crashtag") (v "0.1.0") (h "1y789cxwfhmfidi0rnvyiflvl4qq3xjjcs7d8w8xkdpj8ja7r9ib") (y #t)))

(define-public crate-crashtag-0.1.1 (c (n "crashtag") (v "0.1.1") (h "0x4a4dl0cpji5p3wk3kpb0wz5ch4z3ar4l2vk4425mci8zmpfscq")))

