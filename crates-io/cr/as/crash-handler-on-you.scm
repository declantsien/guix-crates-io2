(define-module (crates-io cr as crash-handler-on-you) #:use-module (crates-io))

(define-public crate-crash-handler-on-you-0.1.0 (c (n "crash-handler-on-you") (v "0.1.0") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0apdarg4r5zadf7n1pj684hsnp7z0ag7vg8rkpf68ikmr69cg3xg") (y #t)))

(define-public crate-crash-handler-on-you-0.1.1 (c (n "crash-handler-on-you") (v "0.1.1") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "17rmayzrdxdgvr8i9qaa019jn4yd080kwsps9r1badgqjj4smb9b") (y #t)))

(define-public crate-crash-handler-on-you-0.1.2 (c (n "crash-handler-on-you") (v "0.1.2") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "15z3klixqzlrqshs1bkk7127msq0kk8d0980dbli321j9gzw3bzk") (y #t)))

(define-public crate-crash-handler-on-you-0.1.3 (c (n "crash-handler-on-you") (v "0.1.3") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "aptos-workspace-hack") (r "^0.1.0") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1dn19pbb7n96wq89cv01yj3si14v3hay57hg9hqfp1pfsvqncxr8") (y #t)))

(define-public crate-crash-handler-on-you-0.1.4 (c (n "crash-handler-on-you") (v "0.1.4") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1zawp16cd1iz800nf3f4wnl5151bgb7x856yn0d2rplirbb7311y") (y #t)))

(define-public crate-crash-handler-on-you-0.1.6 (c (n "crash-handler-on-you") (v "0.1.6") (d (list (d (n "aptos-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "04lbin452z1lbiqnn6ipcrma55zf97azhb9mlb8pp4azfkplha3j") (y #t)))

(define-public crate-crash-handler-on-you-0.1.7 (c (n "crash-handler-on-you") (v "0.1.7") (d (list (d (n "aptos-logger") (r "^0.1.7") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0xaw78ms4nlw3yyflp4gmbf1wpnrmcwzxrg4p93424hfirrhmy41") (y #t)))

(define-public crate-crash-handler-on-you-0.2.1 (c (n "crash-handler-on-you") (v "0.2.1") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "08p1vgrczhjfnpfsxlwgb74phhgv64m4hmx560666mcrmwwx9iiv") (y #t)))

(define-public crate-crash-handler-on-you-0.2.2 (c (n "crash-handler-on-you") (v "0.2.2") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "15fm40iiiarflxm7c906q5y1acydrfvshsq877j4qpwid3649j5g") (y #t)))

(define-public crate-crash-handler-on-you-0.2.6 (c (n "crash-handler-on-you") (v "0.2.6") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "116vlvnz6wmvxzli7d28ja6dsjpdzrq28sinvxwdfq78jl1bi9jw") (y #t)))

(define-public crate-crash-handler-on-you-0.2.7 (c (n "crash-handler-on-you") (v "0.2.7") (d (list (d (n "aptos-logger") (r "^0.2.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0m8l9x9b7gkfx50m0aw49i1d0pcy3j6yka8f3rdzjjqc933q65kb") (y #t)))

