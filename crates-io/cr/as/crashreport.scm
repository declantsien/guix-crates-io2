(define-module (crates-io cr as crashreport) #:use-module (crates-io))

(define-public crate-crashreport-1.0.0 (c (n "crashreport") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "supports-hyperlinks") (r "^1.2.0") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "13r0ryq9jja1jq4m4lp4q3ini71lz75h5c597sc7lhqalbxrmj5p") (f (quote (("default" "color") ("color") ("assume_gitlab") ("assume_github"))))))

(define-public crate-crashreport-1.0.1 (c (n "crashreport") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "supports-hyperlinks") (r "^1.2.0") (d #t) (k 0)) (d (n "terminal-link") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0gp068kp15yg49k67yfapda74ia10xi5qd5fai9ixi6yc9w1xhv1") (f (quote (("default" "color") ("color") ("assume_gitlab") ("assume_github"))))))

