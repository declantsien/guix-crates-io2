(define-module (crates-io cr as crashpad-sys) #:use-module (crates-io))

(define-public crate-crashpad-sys-0.1.0 (c (n "crashpad-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "0mwbs4p89j3256xk4blpi574n3xpaykcsbyh742kk25b1c6n6mg5") (f (quote (("with-precompiled"))))))

(define-public crate-crashpad-sys-0.1.1 (c (n "crashpad-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "0rjf3i03nb9vjvzaxbxiizhyji71ddi8skzx46m48fkzmw91ngfr") (f (quote (("with-precompiled"))))))

(define-public crate-crashpad-sys-0.1.2 (c (n "crashpad-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "0fc9ldrq9bhpixgw381856bwmys9la9bs95fhazis47bb61lhl2c") (f (quote (("with-precompiled"))))))

