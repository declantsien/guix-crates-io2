(define-module (crates-io cr as crash_orm_migration) #:use-module (crates-io))

(define-public crate-crash_orm_migration-0.1.0 (c (n "crash_orm_migration") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crash_orm") (r "^0.1.5") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "07b06isnrj7vcbykxkxbjp2r1xr90iaani0a7f2l4gracc5p8s18")))

(define-public crate-crash_orm_migration-0.1.1 (c (n "crash_orm_migration") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crash_orm") (r "^0.1.5") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "1124krlapxvfnkw79nrw999irq59irr56swi137kyyi39hgghy7h")))

(define-public crate-crash_orm_migration-0.1.2 (c (n "crash_orm_migration") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crash_orm") (r "^0.1.5") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "028qb40b1jsf80g0g888yyqlqjsqzvfk3qpj8mywbc774191da08")))

