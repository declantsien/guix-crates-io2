(define-module (crates-io cr as crash-context) #:use-module (crates-io))

(define-public crate-crash-context-0.0.1 (c (n "crash-context") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1s4mbzxqmawigaypc090k8x8k0qp65lm4bgn7nk7f9h77ry4ixg0")))

(define-public crate-crash-context-0.0.2 (c (n "crash-context") (v "0.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "06hba2j4a68m7r5lpw8cqarfff241k1w6sl94qk7lxlkflbrrcl2") (r "1.59.0")))

(define-public crate-crash-context-0.0.3 (c (n "crash-context") (v "0.0.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.34") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Kernel"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "13w8jvfwd1ngx0azs4q8629pgmxqsl0q7q0y6yx2i5848021kqs8") (r "1.59.0")))

(define-public crate-crash-context-0.1.0 (c (n "crash-context") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0zwpngzimzl2zkg4h2ls6fzn0b4wr33lg3as3xi5gx72fz534rqb") (r "1.59.0")))

(define-public crate-crash-context-0.2.0 (c (n "crash-context") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "066y99ah0qc05534ls6x43z57s7xy01rnmcwg08lxanvq8blc8by") (r "1.59.0")))

(define-public crate-crash-context-0.3.0 (c (n "crash-context") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0w1dambykma3wv51fmmqnbj1l3spvj3mvdlib27m9b4cc4wyazjb") (r "1.59.0")))

(define-public crate-crash-context-0.3.1 (c (n "crash-context") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0pahqlaxvqydgyw666b1md5xplc76sqmjkcz565cjqkq7ajqgq16") (r "1.59.0")))

(define-public crate-crash-context-0.4.0 (c (n "crash-context") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1rp2b5sacg8hwrbbciyhivq9gc935l44asqjq8iixnsna557azng") (r "1.59.0")))

(define-public crate-crash-context-0.5.0 (c (n "crash-context") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "04b28y2v48p48pjx1wljra8hq7bbv6nbzpydflqhz45zqd26qgi5") (r "1.59.0")))

(define-public crate-crash-context-0.5.1 (c (n "crash-context") (v "0.5.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1c53qh1vhsy760qs28g8k21lm2h92k73bk9wa4957clwlf1p5fxq") (r "1.59.0")))

(define-public crate-crash-context-0.6.0 (c (n "crash-context") (v "0.6.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0917nmynw7i9rs29m8xnd1b3hgxx8gz54ai9ag1c0c48p223nhvd") (r "1.59.0")))

(define-public crate-crash-context-0.6.1 (c (n "crash-context") (v "0.6.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "mach2") (r "^0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "120j9izrbz3yzlbfzp9k1axhwlicjwv0jc8na5kwd87c3rkfyp5q") (r "1.59.0")))

