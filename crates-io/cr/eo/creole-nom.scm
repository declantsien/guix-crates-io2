(define-module (crates-io cr eo creole-nom) #:use-module (crates-io))

(define-public crate-creole-nom-0.1.0 (c (n "creole-nom") (v "0.1.0") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "0m5drziiwshs1a2msaad3jmc51as2d3bp4a26rfzmbp79s66d60j")))

(define-public crate-creole-nom-0.1.1 (c (n "creole-nom") (v "0.1.1") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "15dchxc9gkwhnj3dqvn44d80nx2r1434gr4i874xsfhvn8xxsa94")))

(define-public crate-creole-nom-0.1.2 (c (n "creole-nom") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yk3mv70fysbqvpgp363m9x424m0k5i1m3a7j2zh0yc2jbmr1skf")))

(define-public crate-creole-nom-0.1.3 (c (n "creole-nom") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vvs48v9i46f1545mnmqqgj34kik8x8k3czr1yfg1na6916xyqd2")))

(define-public crate-creole-nom-0.1.4 (c (n "creole-nom") (v "0.1.4") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z8vddl8f28a3ps8lh9ldzyh9bf3rn2l8mjf2vwdndpfk20y3lld")))

(define-public crate-creole-nom-1.0.0 (c (n "creole-nom") (v "1.0.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ls6jxansskq055gvpxmzwwpbjn8lbim3hz0jsk74c08m3j5zlca") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.1 (c (n "creole-nom") (v "1.0.1") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1732dxdyfjbn49agnzw02xjcmxqsfy8854xwna7ghm66279jxprb") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold")))) (y #t)))

(define-public crate-creole-nom-1.0.2 (c (n "creole-nom") (v "1.0.2") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jiabkqpk1yysc417ivsd0wwdg4l9jp207k25s488zkaxhcb1hb9") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.3 (c (n "creole-nom") (v "1.0.3") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "189aqivry3wzaa78hm2rvj57igfrv2xgxaigf2w2hy1k3z45ndk0") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.4 (c (n "creole-nom") (v "1.0.4") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vv8zm452xa21c2b2p6zhshyivig8k6blgk3dxhvf1mv4xh2i5sy") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.5 (c (n "creole-nom") (v "1.0.5") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qh151jmmdfc2hsd2d7yq2ilbipgmmldm7dyy8hrbxx5cv48jqg3") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.6 (c (n "creole-nom") (v "1.0.6") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kiwiyhrqbrwmd5khbc3nmrpazq7pwnyf01nm1hfs6mf9sk57ixn") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.7 (c (n "creole-nom") (v "1.0.7") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n3ng59yxd83ps4wv6m1amfayhsv2qw80xk7zv53d5a9sslvwzl3") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.8 (c (n "creole-nom") (v "1.0.8") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jliixwkqb6zjbmapcrawlxivfqnq4yc3c08jy69q9bi144c0d97") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

(define-public crate-creole-nom-1.0.9 (c (n "creole-nom") (v "1.0.9") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19dvxr1ni81l7dcmcyr1shcsjsbv8smswiww1x7zqd3s62g39gjy") (f (quote (("link-button") ("font-color") ("fold") ("extended" "link-button" "font-color" "fold"))))))

