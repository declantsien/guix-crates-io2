(define-module (crates-io cr eo creo) #:use-module (crates-io))

(define-public crate-creo-0.1.0 (c (n "creo") (v "0.1.0") (d (list (d (n "creo-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "18ya9dsi280zal3kagsypg66yxrx5dgzi8kfwrfsc7ni53k18pad")))

