(define-module (crates-io cr sf crsf) #:use-module (crates-io))

(define-public crate-crsf-0.1.0 (c (n "crsf") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)))) (h "1nqvcad21k50sg9sj6akp9yqgdwxl03fvmh4a55arvz8h941mlrd")))

(define-public crate-crsf-0.2.0 (c (n "crsf") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)))) (h "18hsgs1qwz3na9gdhf20vlmvrll9fr4gg3ah282rpg3jbmbjj7yh")))

(define-public crate-crsf-1.0.0 (c (n "crsf") (v "1.0.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)))) (h "0w9nvc39l4q7dkcm8hvmgdkbkbca9p1pr4plsgdb6ax4mah1qmi5") (y #t)))

(define-public crate-crsf-1.0.1 (c (n "crsf") (v "1.0.1") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)))) (h "0k3ja140s4pagax2zl4xpcf67fi4233n8b5v24pih3jnhk6z4qrp")))

(define-public crate-crsf-2.0.0 (c (n "crsf") (v "2.0.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)) (d (n "snafu") (r "^0.8.0") (k 0)))) (h "03k9mvcdgv69brwvyq9wl26zx84awgiiw42bzyrwgiwalhmlrhlk") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-crsf-2.0.1 (c (n "crsf") (v "2.0.1") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^4.2.2") (d #t) (k 2)) (d (n "snafu") (r "^0.8.0") (k 0)))) (h "006fnz1n489wzzbbfb2sgj0l5pgd4gqwrq145269cwq58dp0q5if") (s 2) (e (quote (("defmt" "dep:defmt"))))))

