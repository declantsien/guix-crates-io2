(define-module (crates-io cr aw craweb) #:use-module (crates-io))

(define-public crate-craweb-0.1.0 (c (n "craweb") (v "0.1.0") (d (list (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kfc61ghj3mj7d3igw72rys67b217kv6b66h5wcm4ac1s67wik0x")))

(define-public crate-craweb-0.1.1 (c (n "craweb") (v "0.1.1") (d (list (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g8r5w11xiaybjld5wm1q94kblnq7l49zjjbiw3wng21d140asag")))

(define-public crate-craweb-0.1.2 (c (n "craweb") (v "0.1.2") (d (list (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16mrcvyk32dn8zdn0sa50ahxnri92zyq86xan7war5fnv5ykih9n")))

(define-public crate-craweb-0.1.3 (c (n "craweb") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6cv5bkhw6dkad8mj6b13zl26k2y5h4hig9n5kblyypa93pddr9")))

(define-public crate-craweb-0.2.0 (c (n "craweb") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nxn7p9idv52y8q48b5585927ag42z24k6q0jmxvggdgy1anspp3")))

(define-public crate-craweb-0.2.1 (c (n "craweb") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jajh6nvdk4kwibm3vwiz1xkz1dph5hj5c3nplcn5y89110825dk")))

(define-public crate-craweb-0.2.2 (c (n "craweb") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "044bxparal4r3jvf15mgwdbsgkff55aj4ni78b3hhjcihj74l0qc") (y #t)))

(define-public crate-craweb-0.2.3 (c (n "craweb") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zqjlxqs10zk3byl1b7jar8zpljgvwif88zc1ai691qkq1x9y8p3")))

(define-public crate-craweb-0.3.0 (c (n "craweb") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1766iq9w211bnqsr9f2l7h64dmlip7m5ibiw8ri40dgd51hwyfbz")))

