(define-module (crates-io cr aw crawdad) #:use-module (crates-io))

(define-public crate-crawdad-0.1.0 (c (n "crawdad") (v "0.1.0") (h "05k3hah8vm9mavj49msn0xc348qzd04jb3fm0w0pgzcb2wan6hjz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-crawdad-0.1.1 (c (n "crawdad") (v "0.1.1") (h "15y4vwawqvlqwv2bsrwn23f4j1zrfzrq47szlzrd76vd6vibwmvl") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-crawdad-0.2.0 (c (n "crawdad") (v "0.2.0") (h "1p9s93d1qirqh69cqfqdwlqhrpmhi2spybkycnwcndfaz08rlz0c") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-crawdad-0.2.1 (c (n "crawdad") (v "0.2.1") (h "14dng3bdnxl6qnbk1hr5i28danjzwkgq2ml8sqz3ih7a3npshznr") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-crawdad-0.3.0 (c (n "crawdad") (v "0.3.0") (h "1if7brjfq9ld47cdy6wdh4278s77ngwk9q7vr08hwygdsbnd3yw7") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-crawdad-0.4.0 (c (n "crawdad") (v "0.4.0") (h "1zmpqd2wai4x8r592dgq1gvgrj46ljkqc8z6bb045qx528bb2hlp") (f (quote (("default" "alloc") ("alloc"))))))

