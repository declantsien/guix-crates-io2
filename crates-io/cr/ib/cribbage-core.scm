(define-module (crates-io cr ib cribbage-core) #:use-module (crates-io))

(define-public crate-cribbage-core-0.1.0 (c (n "cribbage-core") (v "0.1.0") (h "0gb8g2dpqcv71s4qmri9bc7d955zphpj1w60jhz38hc4iyjrmg84") (y #t)))

(define-public crate-cribbage-core-0.1.1 (c (n "cribbage-core") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.3") (d #t) (k 0)))) (h "0pagzx75cg5jdy7a4zrihy60d038qip2fhv88pg56g4f4731358j") (f (quote (("extensive-tests")))) (y #t)))

(define-public crate-cribbage-core-0.1.2 (c (n "cribbage-core") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0dnhmrqadxiw4yhxvcawksi32j551yfsimq288vwn0076r7wsivc") (f (quote (("extensive-tests")))) (y #t)))

(define-public crate-cribbage-core-0.1.5 (c (n "cribbage-core") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1vi302yf3hzsgrmswkas1gc4fhlc1i3wkpc67p1s31c9ybs2sg56") (f (quote (("extensive-tests"))))))

