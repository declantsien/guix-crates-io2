(define-module (crates-io cr ib cribbage) #:use-module (crates-io))

(define-public crate-cribbage-0.1.0 (c (n "cribbage") (v "0.1.0") (h "1zjaa1r8l4h5arsw9j9damqyh1b7sr8wslg54vmxymc18xf7r8bl") (y #t)))

(define-public crate-cribbage-0.1.1 (c (n "cribbage") (v "0.1.1") (d (list (d (n "cribbage-core") (r "^0.1.0") (d #t) (k 0)))) (h "0n1866g9alxd4ain1m1wq1x0lhfwcfsxisfkb1vmj9bnl8xbl4bi") (y #t)))

(define-public crate-cribbage-0.1.2 (c (n "cribbage") (v "0.1.2") (d (list (d (n "cribbage-core") (r "^0.1.1") (d #t) (k 0)))) (h "0d1rgr44278yzih4ajsmf3xjwaxla0183bvzzc198pbianmahacr") (y #t)))

(define-public crate-cribbage-0.1.3 (c (n "cribbage") (v "0.1.3") (d (list (d (n "cribbage-core") (r "^0.1.1") (d #t) (k 0)))) (h "1ahiznqlhzvwywka4h6qlyi2blhnjxmm7in9sfprnh3w689xv3n7") (y #t)))

(define-public crate-cribbage-0.1.4 (c (n "cribbage") (v "0.1.4") (d (list (d (n "cribbage-core") (r "^0.1.2") (d #t) (k 0)))) (h "1m7ymfhrzbj30sjm2l6pvbdxizcxjf50y10fg61ixswapnqaw4vg") (y #t)))

(define-public crate-cribbage-0.1.5 (c (n "cribbage") (v "0.1.5") (d (list (d (n "cribbage-core") (r "^0.1.5") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "00c99dignyqfbh79gpfyhpgb4sv8zcl1ca93y8mzqr9jg7dymg84")))

