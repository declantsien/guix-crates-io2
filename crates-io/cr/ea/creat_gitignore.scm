(define-module (crates-io cr ea creat_gitignore) #:use-module (crates-io))

(define-public crate-creat_gitignore-0.1.0 (c (n "creat_gitignore") (v "0.1.0") (h "0m6b6lnk5s1db22mdlzy7mcjh35x9173rd8nyl1ginfx33bmssn6")))

(define-public crate-creat_gitignore-0.1.1 (c (n "creat_gitignore") (v "0.1.1") (h "0y8f969511axs92jpzqjv4vna6hylqmlaii3nay7yplx65chd95s")))

(define-public crate-creat_gitignore-0.1.2 (c (n "creat_gitignore") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0xa49szc14i5aixa358z7vqznzds3xyml460rz1grq5zvp7jwzxp") (y #t)))

(define-public crate-creat_gitignore-0.1.3 (c (n "creat_gitignore") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1x4xyccx4f3cl9fkx2x3iyf16v7pk1hnpn735qwkqqvbg7dgaj0d")))

