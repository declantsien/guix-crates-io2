(define-module (crates-io cr ea create-bloom-filter) #:use-module (crates-io))

(define-public crate-create-bloom-filter-0.1.0 (c (n "create-bloom-filter") (v "0.1.0") (d (list (d (n "bloomfilter") (r "^1.0") (d #t) (k 0)) (d (n "linecount") (r "^0.1.0") (d #t) (k 0)))) (h "0b01rcwqifg16v8hqz1c5zgjy4cz7ig9la2nvlm668hd46jqbmfq")))

(define-public crate-create-bloom-filter-0.1.1 (c (n "create-bloom-filter") (v "0.1.1") (d (list (d (n "bloomfilter") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "linecount") (r "^0.1.0") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h4f71m1qd2y2aiqy52nz7g353728bg2468psxjcgrmzpyhb6ldp")))

