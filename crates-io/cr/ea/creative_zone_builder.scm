(define-module (crates-io cr ea creative_zone_builder) #:use-module (crates-io))

(define-public crate-creative_zone_builder-1.0.0 (c (n "creative_zone_builder") (v "1.0.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "0q3jhi86hwfvhzypfcvhr2pqdq4a0yskg12m47gr4qp7d921il4n")))

(define-public crate-creative_zone_builder-1.0.1 (c (n "creative_zone_builder") (v "1.0.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "0ddxr2fclixdagjfxx2w0a89n9gyv3npcq3zjqb870maqzfzp67g")))

(define-public crate-creative_zone_builder-1.1.0 (c (n "creative_zone_builder") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "0vq0hc62p4psm2jxvin1x1pzsxpl5myqadb5jz7hmiam9d036kzp")))

(define-public crate-creative_zone_builder-1.1.2 (c (n "creative_zone_builder") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "06hzi3zfyasvc6xvjnfplid2q68q49fj0sgjxjzwhv6j539falq2")))

