(define-module (crates-io cr ea create_html) #:use-module (crates-io))

(define-public crate-create_html-0.1.0 (c (n "create_html") (v "0.1.0") (h "03blki8hj403982v3zrh92lpdgawr426lmjsyxd5257fww3vafb7")))

(define-public crate-create_html-0.1.1 (c (n "create_html") (v "0.1.1") (h "1h9m6yrwy8ra7bmmxhjy85ns5jqrxnh9vi0083qv10cf58lbn2cq")))

(define-public crate-create_html-0.1.2 (c (n "create_html") (v "0.1.2") (h "0d4bh9vkxmdq5b7p44wlfc48s4b8llmdqp0dlb3kzm6n3vinv7xa")))

(define-public crate-create_html-0.1.3 (c (n "create_html") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iychra4yll52ympkd5qhy8xzmr9kbvk7hynrsg33v1v4jxpbgs7")))

