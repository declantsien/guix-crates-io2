(define-module (crates-io cr ea create_chatgpt_app) #:use-module (crates-io))

(define-public crate-create_chatgpt_app-0.1.0 (c (n "create_chatgpt_app") (v "0.1.0") (d (list (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "0fjgrmbn8p7g5crjg2jdpgs5gr8y5h68kf476fliabkfsk8ia6mg")))

