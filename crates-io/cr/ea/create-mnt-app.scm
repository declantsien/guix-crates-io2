(define-module (crates-io cr ea create-mnt-app) #:use-module (crates-io))

(define-public crate-create-mnt-app-0.1.0 (c (n "create-mnt-app") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "names") (r "^0.14.0") (d #t) (k 0)) (d (n "rust-embed") (r "^6.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spinach") (r "^2.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "1x5h2k6s5f3i1wyzvan61gfxldblmy3mrrpvyc8vdxqlmfsn904i")))

