(define-module (crates-io cr ea creator-derive) #:use-module (crates-io))

(define-public crate-creator-derive-0.3.3 (c (n "creator-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fp3x3zd4pgh5rwgw1xdzfnh8v3xbddh6ndgvgdmf0g2qqzxvg4v")))

(define-public crate-creator-derive-0.3.4 (c (n "creator-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "140qxxgqwrk629achx697bv5n68a10da4zdwm2hl1bgxn21bs9cr")))

(define-public crate-creator-derive-0.3.5 (c (n "creator-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "0gpql5qqr71ylv3adyw0m8pr1w5vfk4jhhp3mjqqpyx2j8cv5as1")))

(define-public crate-creator-derive-0.3.7 (c (n "creator-derive") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "0dac7cn4r0g71insvkcki63cb1sdc1p1l0c0hrn1rywanws4y2ky")))

(define-public crate-creator-derive-0.3.8 (c (n "creator-derive") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "1jc1cg7qh5z3sw5h37ik8qamcs0y6vkjwwfc521gf181nqk0kc8h")))

(define-public crate-creator-derive-0.4.0 (c (n "creator-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vgs55ky28h2ys8xck7nlkkwv5bny5hn3vbapnryqcnj7xh05xbd")))

(define-public crate-creator-derive-0.4.1 (c (n "creator-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12aql5s17g7lcv381yc39kh9a3jj9sm6s01icyp9xi1pz2yar392")))

