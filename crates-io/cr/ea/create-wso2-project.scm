(define-module (crates-io cr ea create-wso2-project) #:use-module (crates-io))

(define-public crate-create-wso2-project-0.0.1 (c (n "create-wso2-project") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "rust-embed") (r "^6.8") (f (quote ("compression" "interpolate-folder-path"))) (d #t) (k 0)))) (h "0x8qq34z2k6191d527gf1xigbswr3mv3gjhw9gwirabjlibn100n")))

(define-public crate-create-wso2-project-0.0.2 (c (n "create-wso2-project") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "rust-embed") (r "^6.8") (f (quote ("compression" "interpolate-folder-path"))) (d #t) (k 0)))) (h "1qv88ssmf45dyzc53bm7pyfqa121lgdarsaxjlqd75ihanl0ilmm")))

