(define-module (crates-io cr ea create-farm) #:use-module (crates-io))

(define-public crate-create-farm-0.1.0 (c (n "create-farm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "napi") (r "^2.15.2") (f (quote ("napi4" "serde-json"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.15.2") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3") (f (quote ("compression" "interpolate-folder-path"))) (d #t) (k 0)))) (h "057ikarakr0pza3cljw0zqh7n27bgdiy88gp1z8bps3vngdd3kg0")))

