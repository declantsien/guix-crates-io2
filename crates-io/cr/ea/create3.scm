(define-module (crates-io cr ea create3) #:use-module (crates-io))

(define-public crate-create3-0.1.0 (c (n "create3") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1jb6szs2qap9s89klwq3aa5l2gbwwm1fhhf004pww8imkv0lg3d0")))

