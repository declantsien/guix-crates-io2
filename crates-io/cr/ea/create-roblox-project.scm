(define-module (crates-io cr ea create-roblox-project) #:use-module (crates-io))

(define-public crate-create-roblox-project-0.1.0 (c (n "create-roblox-project") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "git2") (r "^0.13.17") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("fs" "macros" "process" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "17kml6fln1z0nik91qssdn9iq8vydrjxd7q25ldk2adid24jz8gs")))

