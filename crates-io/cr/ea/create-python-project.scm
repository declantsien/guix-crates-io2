(define-module (crates-io cr ea create-python-project) #:use-module (crates-io))

(define-public crate-create-python-project-0.1.0 (c (n "create-python-project") (v "0.1.0") (h "17c56c66wn9v2c7w76k748bjg2pcrwivk2msbgdw409kxbqfqm5n")))

(define-public crate-create-python-project-0.1.1 (c (n "create-python-project") (v "0.1.1") (h "0jarqrf0qnsc2i2b5fiilgjid53cr2sd5yss1c32034k4923igsd")))

(define-public crate-create-python-project-0.2.1 (c (n "create-python-project") (v "0.2.1") (h "1vzmlb3kah8z15zpr6wbx8d9x5nqyq6sya4aymjnmhacv7y7c6jl")))

(define-public crate-create-python-project-0.2.2 (c (n "create-python-project") (v "0.2.2") (h "12b3i6d68njbyxzzqdwhwzi5f9frgp2kyb5dprlrjj05hl547cpz")))

(define-public crate-create-python-project-0.2.3 (c (n "create-python-project") (v "0.2.3") (h "1nczxk2x1bfxjrpvjshh7bga6wxnxfc3mag62d4jm3zpz0fz2pmv")))

