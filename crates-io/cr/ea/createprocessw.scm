(define-module (crates-io cr ea createprocessw) #:use-module (crates-io))

(define-public crate-CreateProcessW-0.1.0 (c (n "CreateProcessW") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.24.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "0qqgyxpppdgrgr74mhwi7p9fkq9171hczvvdlz8i2bd5j96wa7gi") (y #t)))

(define-public crate-CreateProcessW-0.1.1 (c (n "CreateProcessW") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("std" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "0dfs22k48j5gk9srqqgdfw94c58iiiq3m2v9z32xqqhx85233z6p") (y #t)))

(define-public crate-CreateProcessW-0.1.2 (c (n "CreateProcessW") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "windows") (r "^0.28.0") (f (quote ("std" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "0wxmz0g9lbhhahnihql10qa06s4k4pql21r3dysls39dn8x6aa7h") (y #t)))

(define-public crate-CreateProcessW-0.1.3 (c (n "CreateProcessW") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "1x9xglvwl1bcxz6g032z4d3rvi3kgikkq720zgl5n6h0ajfm5z5g") (y #t)))

(define-public crate-CreateProcessW-0.1.4 (c (n "CreateProcessW") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "0gcxd07kppb2lcwlc0gsc7imj1qn3cnnmv151d661084mcp5x053")))

