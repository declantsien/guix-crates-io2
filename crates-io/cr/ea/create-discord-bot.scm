(define-module (crates-io cr ea create-discord-bot) #:use-module (crates-io))

(define-public crate-create-discord-bot-0.1.0 (c (n "create-discord-bot") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "08lqmgdx5jk7yy59zrsjpvsxd1yzn6zh6g4gyv8c81974cqy3055")))

(define-public crate-create-discord-bot-0.1.1 (c (n "create-discord-bot") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0j9m20jggqk5j7xa4cl02kh3k4fsj0vp6ja854a2sv2pyafr9ap3")))

