(define-module (crates-io cr ea create-farm-rs) #:use-module (crates-io))

(define-public crate-create-farm-rs-0.1.0 (c (n "create-farm-rs") (v "0.1.0") (d (list (d (n "create-farm") (r "^0.1.0") (d #t) (k 0)) (d (n "napi") (r "^2.15.2") (f (quote ("napi4" "serde-json"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.15.2") (d #t) (k 0)))) (h "16lbfn9dgf2zq6wnm2lq91nb53y0g4mpjd17q3k7myl270mchi2m")))

