(define-module (crates-io cr ea creature_feature) #:use-module (crates-io))

(define-public crate-creature_feature-0.1.0 (c (n "creature_feature") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1igkrp89b3zaibhfysx4fhbaca48kq5fx1bzflwl9x7n0slrsrmp") (f (quote (("serde1" "serde"))))))

(define-public crate-creature_feature-0.1.1 (c (n "creature_feature") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nrccpmwkz0h76z4xzlsnk7cfdi93jbxawx70v57d0mn20chfm5i") (f (quote (("serde1" "serde"))))))

(define-public crate-creature_feature-0.1.2 (c (n "creature_feature") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16czabsgivsxb1rnvapr6s5428pf4jakxrszq4fs30y6l0fw9x7v") (f (quote (("serde1" "serde"))))))

(define-public crate-creature_feature-0.1.3 (c (n "creature_feature") (v "0.1.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bl19yzp85w8cc5zi43wl74m0z4kwsnvzadqdnks6cfyxizmafg6") (f (quote (("serde1" "serde"))))))

(define-public crate-creature_feature-0.1.4 (c (n "creature_feature") (v "0.1.4") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zcsjx548rnalqdfdck2vg0db7jcnm5k87hjv6dli8vjvpdssgi2") (f (quote (("serde1" "serde"))))))

(define-public crate-creature_feature-0.1.6 (c (n "creature_feature") (v "0.1.6") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15h1gxnl6bzzdhvpf3zcd4c4njlnp3nz1fpwv408n73cgvs10qyr") (f (quote (("serde1" "serde") ("default")))) (s 2) (e (quote (("heapless" "dep:heapless"))))))

(define-public crate-creature_feature-0.1.7 (c (n "creature_feature") (v "0.1.7") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (o #t) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03shq4z8pn1iq8ri385zg86gl8dlbrv434qg6yxqap6hjjgkva7r") (f (quote (("serde1" "serde") ("default")))) (s 2) (e (quote (("heapless" "dep:heapless"))))))

