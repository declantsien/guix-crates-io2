(define-module (crates-io cr ea create-output-dir) #:use-module (crates-io))

(define-public crate-create-output-dir-0.1.0 (c (n "create-output-dir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cargo-util") (r "^0.2.2") (d #t) (k 0)))) (h "0s46lfhv1bqxfriy1bj2sb4bf8710zbcml1vvcxbkgyjgx2m3vp1") (r "1.66")))

(define-public crate-create-output-dir-1.0.0 (c (n "create-output-dir") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.3") (f (quote ("mac_os_10_7_support"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ir3cb56258jsqrwm4nzs1vnwsm3iai3g8vignf6ziwdlbqbaqzi") (r "1.66")))

