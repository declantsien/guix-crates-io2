(define-module (crates-io cr ea creator-permissions) #:use-module (crates-io))

(define-public crate-creator-permissions-0.1.1 (c (n "creator-permissions") (v "0.1.1") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0a7bqr6irj3v07sncl0bynvirmxv18kmzi1ix6g2sz6m5zjh5da6")))

(define-public crate-creator-permissions-0.1.2 (c (n "creator-permissions") (v "0.1.2") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "09yj8cqiqa124yy2h1yl5ka89g0v59f3ja3v27scdwbvd1nkqcrb")))

(define-public crate-creator-permissions-0.1.3 (c (n "creator-permissions") (v "0.1.3") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1dhlingiamy71r66pzjx1y31w1n88qpan2pnw24p74m38kaa0nb3")))

(define-public crate-creator-permissions-0.1.4 (c (n "creator-permissions") (v "0.1.4") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "04xcqgv2v9hjxhf8nw3fj1k41m0zkwcxcwzrgr0cbbpb4a4mzwqc")))

(define-public crate-creator-permissions-0.1.5 (c (n "creator-permissions") (v "0.1.5") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0xb20aagxp5rn7fl323a89ld60vw87j0xpm8sjc5vhmrl8ifs9si")))

(define-public crate-creator-permissions-0.1.6 (c (n "creator-permissions") (v "0.1.6") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1xr3l04fh07sjfqibzb3a482vfkqkm8fzl237ns8vzp6mxk7f3gk")))

(define-public crate-creator-permissions-0.1.7 (c (n "creator-permissions") (v "0.1.7") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1s6b6zgla0vxp1z5b0sfpjkgkkc2na0g78zg4flck9bsifax0cs7")))

(define-public crate-creator-permissions-0.1.8 (c (n "creator-permissions") (v "0.1.8") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0dbbw8capzak4whlrzf2i2knci752gz6yqhkayyqp7iq2pjc8p58")))

(define-public crate-creator-permissions-0.2.0 (c (n "creator-permissions") (v "0.2.0") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0x2snkmiymmk5q9g7y5v7y3bxxkgypd4y6g8552jy9z3s2gbymwf")))

(define-public crate-creator-permissions-0.2.1 (c (n "creator-permissions") (v "0.2.1") (d (list (d (n "jni") (r "^0.17") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "02j4drl58khmgrrp65729jsflwflflyaiz4p77vq6vckllrarh3a")))

(define-public crate-creator-permissions-0.3.2 (c (n "creator-permissions") (v "0.3.2") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "19i4mnxysbfaf4a07r7b4kccb2wyxi6kzwy5sxw0bfhkii2n24f4")))

(define-public crate-creator-permissions-0.3.3 (c (n "creator-permissions") (v "0.3.3") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1zlwf42i17s3aljv7hb5rygdql72mm007287941bwz5rmsgm9zbl")))

(define-public crate-creator-permissions-0.3.4 (c (n "creator-permissions") (v "0.3.4") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1ld4a9bk0nm5dfkidlyyksbspx3f116akgi6a47vrzgyld2kk105")))

(define-public crate-creator-permissions-0.3.5 (c (n "creator-permissions") (v "0.3.5") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0nlcsg2d8rl06gm0cxnz9d3lwhbm7rqxvz3id7g87056pag2vv64")))

(define-public crate-creator-permissions-0.3.7 (c (n "creator-permissions") (v "0.3.7") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1ml4svn5gw73khn2jblif8qhffy8z8m03qxyncsg08n373nx4398")))

(define-public crate-creator-permissions-0.3.8 (c (n "creator-permissions") (v "0.3.8") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1hx8y6m63h2yh4z1ccz3n1gdda652x0z453jlq5lh75np69jcd53")))

(define-public crate-creator-permissions-0.4.0 (c (n "creator-permissions") (v "0.4.0") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "120mwbidzri5yslx8ihvcbn6ax87330ys5picc57v27bj21wf77s")))

(define-public crate-creator-permissions-0.4.1 (c (n "creator-permissions") (v "0.4.1") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "07lys9c6y5xc7iwl8q1srdbdvvvk9969ik0dbbf691axqpi4hq17")))

