(define-module (crates-io cr ea creators_crate) #:use-module (crates-io))

(define-public crate-creators_crate-0.1.0 (c (n "creators_crate") (v "0.1.0") (h "0m5v7sly12hkk32rl31fi80blq8svlfvvmqw7mlvwzdiyss3mssc")))

(define-public crate-creators_crate-0.1.1 (c (n "creators_crate") (v "0.1.1") (h "1da9hgs9gpiqvmanmpvkkv86v3d9sr473y6a1wh3pczjhhp0443n") (f (quote (("web3" "eth" "sui") ("sui") ("eth"))))))

