(define-module (crates-io cr ea creator-ads) #:use-module (crates-io))

(define-public crate-creator-ads-0.2.1 (c (n "creator-ads") (v "0.2.1") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "06fwqrsp70qg5v06c4b465i9vil1ipn309zgqxps1wz21a4b21ww")))

(define-public crate-creator-ads-0.3.2 (c (n "creator-ads") (v "0.3.2") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "17gm5w184kvrhs67wkz1745n7i4lq4siywynrp2d0s0xw9q0zrp2")))

(define-public crate-creator-ads-0.3.3 (c (n "creator-ads") (v "0.3.3") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1i76hnzlaz39rddavh8mhbp9faw9q2jqgpr0biqdrq06l7y7xpjz")))

(define-public crate-creator-ads-0.3.4 (c (n "creator-ads") (v "0.3.4") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0dkryrpm617w69mg5z294h6a35rgg83pnmb01ir1qgfvk9w8b410")))

(define-public crate-creator-ads-0.3.5 (c (n "creator-ads") (v "0.3.5") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1i7dw0k322s5bgscxd4aydfx3cdm5yh4g7zq6f6l98cm3as6hhxx")))

(define-public crate-creator-ads-0.3.7 (c (n "creator-ads") (v "0.3.7") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "036xv8lr9cw1p9llb0cv03y4z1dgizfkk9hkd963dpsr4fya57b6")))

(define-public crate-creator-ads-0.3.8 (c (n "creator-ads") (v "0.3.8") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0gfiic49sr8097fsxmgf6qgmhr03jqn3g5gf98vaw4r8ky3mcj4v")))

(define-public crate-creator-ads-0.4.0 (c (n "creator-ads") (v "0.4.0") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "09kqdh7sl1lh16n6awjg6xidhacvjbc69cr4xlz547bm8dlbjqzy")))

(define-public crate-creator-ads-0.4.1 (c (n "creator-ads") (v "0.4.1") (d (list (d (n "jni") (r "^0.18") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "02q8jqqwkz99ya7p5zc067y9f9jf598izcsynsjmzan33z12q6sw")))

