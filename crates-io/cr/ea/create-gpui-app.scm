(define-module (crates-io cr ea create-gpui-app) #:use-module (crates-io))

(define-public crate-create-gpui-app-0.1.0 (c (n "create-gpui-app") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0jhyf652jqgrr43sazapmf923va5ljpxi9f908mhlqbpxp8m9fjp")))

(define-public crate-create-gpui-app-0.1.1 (c (n "create-gpui-app") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "1lp3zzibl44xg26f7bnz8wg5x4jwhqfd1yycqs2m5cyigdl6q4kk")))

