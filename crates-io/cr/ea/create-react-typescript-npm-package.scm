(define-module (crates-io cr ea create-react-typescript-npm-package) #:use-module (crates-io))

(define-public crate-create-react-typescript-npm-package-0.1.0 (c (n "create-react-typescript-npm-package") (v "0.1.0") (h "1j2689py38lchxx620hj6kb3xpkcvsz3kq745j7k8rjxllkw9cyv")))

(define-public crate-create-react-typescript-npm-package-0.1.1 (c (n "create-react-typescript-npm-package") (v "0.1.1") (h "0h4np4ad24aa8d73g70d9lp9jsii05qql6nlq3xlnpkhqbvazbqi")))

