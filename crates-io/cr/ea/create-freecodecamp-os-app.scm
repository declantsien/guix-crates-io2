(define-module (crates-io cr ea create-freecodecamp-os-app) #:use-module (crates-io))

(define-public crate-create-freecodecamp-os-app-1.0.0 (c (n "create-freecodecamp-os-app") (v "1.0.0") (d (list (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1jd9dvnbb9qznskqplbvbnjf8lil8932cm8919amn1k8yqma3yss")))

(define-public crate-create-freecodecamp-os-app-3.0.0 (c (n "create-freecodecamp-os-app") (v "3.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "11ml7l5yahrhkjfiimppvrcq9ri9fj9a5swnj8rcig34inpnggj3")))

(define-public crate-create-freecodecamp-os-app-3.0.1 (c (n "create-freecodecamp-os-app") (v "3.0.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0s805xr2rs5na2zzhk7mqsgry3l5l7ncfw8d4b2wp4gvxvbryg0v")))

(define-public crate-create-freecodecamp-os-app-3.0.2 (c (n "create-freecodecamp-os-app") (v "3.0.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1ng4x7lgg9zd8vazby4jxg8knqacg4jr8w1q3w8bda2xiw5m9795")))

