(define-module (crates-io cr ea creator) #:use-module (crates-io))

(define-public crate-creator-0.1.0 (c (n "creator") (v "0.1.0") (h "02icmj0pzj7rvgx6zdcwpvl7pishlbd90yhlhbwf3b6dd16k1943")))

(define-public crate-creator-0.1.1 (c (n "creator") (v "0.1.1") (d (list (d (n "creator-permissions") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0ismpy0fld017ziya8xrlbkhd71lr3imxlg14qxb0zkj6liysgqd") (f (quote (("permissions" "creator-permissions"))))))

(define-public crate-creator-0.1.2 (c (n "creator") (v "0.1.2") (d (list (d (n "cargo-creator") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1bg7s1nrflmdrjn2avj9qc3hsmr8q898lxqsyxwy0klmg20ygwx7") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.1.3 (c (n "creator") (v "0.1.3") (d (list (d (n "cargo-creator") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1ikwjy937vvjn3dpyr62adcqf03c4vnzwwrhvnk60adzi8x3569a") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.1.4 (c (n "creator") (v "0.1.4") (d (list (d (n "cargo-creator") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0fq2jszw6bg42f9vy70wzq91ayy57b5q88klzrg96svd3xw163km") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.1.6 (c (n "creator") (v "0.1.6") (d (list (d (n "cargo-creator") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "1shysz8d4h18pxcvracxb09fm3x86qfhfprlg2vq1il5l2lrsl4g") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.1.7 (c (n "creator") (v "0.1.7") (d (list (d (n "cargo-creator") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0mw1vpdx7q1gw9v3ri7hindrcwbs6nx0qpxj0v9a8yhz12kdbda4") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.1.8 (c (n "creator") (v "0.1.8") (d (list (d (n "cargo-creator") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1q09d5r74yrbx5b2lj1fwk9bgimspajga9a7fjwbmqxamw61qzq8") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.2.0 (c (n "creator") (v "0.2.0") (d (list (d (n "cargo-creator") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "10l5y9lms00435swxsidbqf7i8z0agvzfgqgva4kdppl7i8a216l") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.2.1 (c (n "creator") (v "0.2.1") (d (list (d (n "cargo-creator") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "creator-permissions") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1zkxrxzj54963v5nw1ikadq2kzs79kjpkaydkrcn9lzszn1xbhd7") (f (quote (("tool" "cargo-creator") ("permissions" "creator-permissions"))))))

(define-public crate-creator-0.3.3 (c (n "creator") (v "0.3.3") (d (list (d (n "creator-ads") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "creator-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "creator-permissions") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "creator-tools") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.2") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1crgrzp804v2z5q4zm71nq21wm6i7zys00maz34ynqr7if8y1g8l") (f (quote (("default"))))))

(define-public crate-creator-0.3.5 (c (n "creator") (v "0.3.5") (d (list (d (n "creator-ads") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "creator-derive") (r "^0.3.5") (d #t) (k 0)) (d (n "creator-permissions") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "creator-tools") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.2") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "03j5k2y6c516hic2syphn872p1jbj62n2dlcyvj71bbzq597jgnq") (f (quote (("default"))))))

(define-public crate-creator-0.3.7 (c (n "creator") (v "0.3.7") (d (list (d (n "creator-ads") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "creator-derive") (r "^0.3.7") (d #t) (k 0)) (d (n "creator-permissions") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "creator-tools") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.2") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "0305s08cccqd8x1b7amcvwz1jlnhqmpd2pcv2wrxrgav3saxfwl0") (f (quote (("default"))))))

(define-public crate-creator-0.3.8 (c (n "creator") (v "0.3.8") (d (list (d (n "creator-ads") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "creator-derive") (r "^0.3.8") (d #t) (k 0)) (d (n "creator-permissions") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "creator-tools") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.2") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "165mkk1xxl5w85p5islm0xxcmh047xc2salmrq44rk6acq2gd3mh") (f (quote (("default"))))))

(define-public crate-creator-0.4.1 (c (n "creator") (v "0.4.1") (d (list (d (n "creator-ads") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "creator-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "creator-permissions") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "creator-tools") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "ndk-glue") (r "^0.3") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "13s7iw5aq4narvcnj214rigknimkv1nm8d2p4p7mha385n70nyvx") (f (quote (("default"))))))

