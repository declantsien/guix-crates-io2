(define-module (crates-io cr ea create-idea-module) #:use-module (crates-io))

(define-public crate-create-idea-module-0.1.0 (c (n "create-idea-module") (v "0.1.0") (d (list (d (n "askama") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "0hm9qw90qhiq35gsm4xv0aw6xpqrryfnlrx2advkf09jvjkr4b42")))

(define-public crate-create-idea-module-0.1.1 (c (n "create-idea-module") (v "0.1.1") (d (list (d (n "askama") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "17jxvlwnh2267bl0pfkb14l0z2cvzgwnxx8g1i5mfg5lbcvy3csq")))

(define-public crate-create-idea-module-0.1.2 (c (n "create-idea-module") (v "0.1.2") (d (list (d (n "askama") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "1f8fm8f4gzqa5s3aszaqkxjbvcs75swznlwai1lmahgc0l5kndb2")))

(define-public crate-create-idea-module-0.1.3 (c (n "create-idea-module") (v "0.1.3") (d (list (d (n "askama") (r "^0.11") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)))) (h "0macgbqj7vm74n951jw818sgy334mjn49vdn90vs9xg0pdb7nik1")))

