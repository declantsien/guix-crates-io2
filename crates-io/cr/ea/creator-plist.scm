(define-module (crates-io cr ea creator-plist) #:use-module (crates-io))

(define-public crate-creator-plist-1.0.1 (c (n "creator-plist") (v "1.0.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "line-wrap") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "04q82rrfqpwvxfpvwff9kgi0mvyyizbdxa10g8g0k6jbh1xzp5p1") (f (quote (("enable_unstable_features_that_may_break_with_minor_version_bumps") ("default" "serde"))))))

