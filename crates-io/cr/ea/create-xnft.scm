(define-module (crates-io cr ea create-xnft) #:use-module (crates-io))

(define-public crate-create-xnft-0.1.0 (c (n "create-xnft") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sqz45ajsi47lz0szrmd5zqq22r0vr4wmbgn0hyyqynsyn21ns7k")))

(define-public crate-create-xnft-0.1.1 (c (n "create-xnft") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nvzz2cf46vy5j26mmk8q4qyqiiyzmvr2ws3dpgskx7jn1pmx2sp")))

