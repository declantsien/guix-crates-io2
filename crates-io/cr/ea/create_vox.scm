(define-module (crates-io cr ea create_vox) #:use-module (crates-io))

(define-public crate-create_vox-0.1.0 (c (n "create_vox") (v "0.1.0") (h "023lwidr96kvkvpj68zh9pa1hslqmnfnmx5kwkd4ijx9v7gay0si")))

(define-public crate-create_vox-0.1.1 (c (n "create_vox") (v "0.1.1") (h "0l1k6nqg85qq5ih9xr0lv4177klk9hhpnlldfq3sniiz1xg2jv3z")))

(define-public crate-create_vox-0.2.1 (c (n "create_vox") (v "0.2.1") (h "0727fbnnl6ah4mv96xm6xmlq5cfbvxa95jfw5hb2gz993275qsal")))

(define-public crate-create_vox-0.3.0 (c (n "create_vox") (v "0.3.0") (h "1sq9nj7w8p4a8p5lw0ka8r9027nhhs7sjf2gpiv6aa5bghpc2a6s")))

(define-public crate-create_vox-0.4.0 (c (n "create_vox") (v "0.4.0") (h "1wpryhg9kdabp5diijxshngbam0q8rnxfny0xrx620hqsnjy7cgc")))

(define-public crate-create_vox-0.5.0 (c (n "create_vox") (v "0.5.0") (h "0jd23p8irymdi1lfv1sq4z1rnzvca8j0vqh8zyk90k58v5668iwp")))

(define-public crate-create_vox-0.6.0 (c (n "create_vox") (v "0.6.0") (h "0wx8s04hj1g6ahgknsbfnkz5x04grajrp4swblffr30318lc4l4q")))

(define-public crate-create_vox-1.0.0 (c (n "create_vox") (v "1.0.0") (d (list (d (n "easybench") (r "^1.1.0") (d #t) (k 2)))) (h "045amfk7px3rcwrz5isni4j3amp46px9011702070va81p3xw6rr")))

(define-public crate-create_vox-1.0.1 (c (n "create_vox") (v "1.0.1") (d (list (d (n "easybench") (r "^1.1.0") (d #t) (k 2)))) (h "0ishnvqgrmvx4i77qlka2a1cj3qimp1d4zqv23nq0f9ns0mm4fx2")))

