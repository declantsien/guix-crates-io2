(define-module (crates-io cr ea create_farm_node) #:use-module (crates-io))

(define-public crate-create_farm_node-0.1.0 (c (n "create_farm_node") (v "0.1.0") (d (list (d (n "create-farm") (r "^0.1.0") (d #t) (k 0)) (d (n "napi") (r "^2.15.2") (f (quote ("napi4" "serde-json"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.15.2") (d #t) (k 0)))) (h "1c6g8w6ljclrvdasgp62y33z34v6aqlxgfb812brb1qr05fszxi1")))

