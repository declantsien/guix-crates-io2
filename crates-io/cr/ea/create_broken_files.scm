(define-module (crates-io cr ea create_broken_files) #:use-module (crates-io))

(define-public crate-create_broken_files-1.0.0 (c (n "create_broken_files") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "17p20l3f3hb5b7jm8sjbx359b1z87hmwv1sbma18v5fxv73n5m9s")))

(define-public crate-create_broken_files-2.0.0 (c (n "create_broken_files") (v "2.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0nq0sa3v8wzvpn9qa23q76nf5f7j1qxzfgqj84axbfk79y2anyv2")))

(define-public crate-create_broken_files-3.0.0 (c (n "create_broken_files") (v "3.0.0") (d (list (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0vwjbwzfg5a2pvp6b6vbi0f3fz9sig1axl96rjj0hh82944zxh48")))

(define-public crate-create_broken_files-3.0.1 (c (n "create_broken_files") (v "3.0.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "01ks4caaqx8aab14i8dimliaxck13d3xd25f5p9a9hrgilazqf76")))

