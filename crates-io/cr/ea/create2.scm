(define-module (crates-io cr ea create2) #:use-module (crates-io))

(define-public crate-create2-0.0.0 (c (n "create2") (v "0.0.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0y1rmk5fvx5wxxs04irg2sqsq4qmi66dzdf0jgi9hwrfxkh0hwsb")))

(define-public crate-create2-0.0.1 (c (n "create2") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0yf5y9hc8jzvjpdswk956n7qqnq3rzgzs8cikbv9ad5v2yp4d7mn")))

(define-public crate-create2-0.0.2 (c (n "create2") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "08f89jd4y8s7q1ffwlc6fc5g6wq5c12wk5ilhsc7lmw3frwh0x77")))

