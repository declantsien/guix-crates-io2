(define-module (crates-io cr ea create-leptos-csr) #:use-module (crates-io))

(define-public crate-create-leptos-csr-0.2.0 (c (n "create-leptos-csr") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0rj49m7sl6iz9i1qgkxvnmlphm26zcplrhi9cxj6p52jplplgjrx") (y #t)))

(define-public crate-create-leptos-csr-0.2.1 (c (n "create-leptos-csr") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0sa05xdgw2cvaq9dahc7rb015msvy8w8s0cfmfikg6gsbzajwvby") (y #t)))

(define-public crate-create-leptos-csr-0.2.2 (c (n "create-leptos-csr") (v "0.2.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "04ng3pafz55xjix5s0c7br2krlzjvndc52dq45f675jpmyw7vabk")))

