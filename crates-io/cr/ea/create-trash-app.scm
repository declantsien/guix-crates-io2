(define-module (crates-io cr ea create-trash-app) #:use-module (crates-io))

(define-public crate-create-trash-app-0.1.0 (c (n "create-trash-app") (v "0.1.0") (h "1ki625a6gxjwbvxgj7sn39l1d7kj9dj78hcdbwj67yf48h67sm79") (y #t)))

(define-public crate-create-trash-app-0.1.1 (c (n "create-trash-app") (v "0.1.1") (h "0pgna18pprvbvlslqq4kv2iam94lq04bx9gw8vxlyrpanqxahjfx") (y #t)))

(define-public crate-create-trash-app-0.1.2 (c (n "create-trash-app") (v "0.1.2") (h "0aq9wyan84zf989vb06v4kd2hbbz4gz1sv3p2vwhrfss0x04mdm6") (y #t)))

(define-public crate-create-trash-app-0.1.3 (c (n "create-trash-app") (v "0.1.3") (h "1h2igy98y5pzmrn808vdfjz1p6qxzjimwxmyapca07vi8a2x0h7h") (y #t)))

(define-public crate-create-trash-app-0.0.4 (c (n "create-trash-app") (v "0.0.4") (h "1k39s07f9n7irv3vpd5r0l2q5v6rc9z655k5gnx75i7d53ddj8y5") (y #t)))

(define-public crate-create-trash-app-0.0.5 (c (n "create-trash-app") (v "0.0.5") (h "0lfzdmx8ar696xvnr9yf3zjk7bk8jpp1cbaihx8qdj46q1hyi19w") (y #t)))

(define-public crate-create-trash-app-0.0.6 (c (n "create-trash-app") (v "0.0.6") (h "135b37x1rfnhjcnvn4nqkfljipxn9zhyqlyqd2zpvnfdazngawq5") (y #t)))

