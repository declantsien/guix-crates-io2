(define-module (crates-io cr ea creak) #:use-module (crates-io))

(define-public crate-creak-0.0.0 (c (n "creak") (v "0.0.0") (h "0zk0v9f97qkkfvk7hmicqpkxbr5g26a7nd3nfvf0nik93gzd40rn") (y #t)))

(define-public crate-creak-0.1.0 (c (n "creak") (v "0.1.0") (d (list (d (n "claxon") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (o #t) (d #t) (k 0)) (d (n "lewton") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "minimp3") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "06w8nryglz3dz0s9d5803qg917xklja7lzslf7hzb70m0vc4phkg") (f (quote (("wav" "hound") ("vorbis" "lewton") ("mp3" "minimp3") ("flac" "claxon") ("default" "wav" "mp3" "vorbis" "flac"))))))

(define-public crate-creak-0.2.0 (c (n "creak") (v "0.2.0") (d (list (d (n "claxon") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (o #t) (d #t) (k 0)) (d (n "lewton") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "minimp3") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0wx89czvjwqwic7yrynhlmaqb1c2lrm4k6bia00k1n0lwwi3hxqx") (f (quote (("wav" "hound") ("vorbis" "lewton") ("mp3" "minimp3") ("flac" "claxon") ("default" "wav" "mp3" "vorbis" "flac"))))))

(define-public crate-creak-0.3.0 (c (n "creak") (v "0.3.0") (d (list (d (n "claxon") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (o #t) (d #t) (k 0)) (d (n "lewton") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "minimp3") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "135wp11815a3xcqyvsl1zi43k65fzynj5y4xlmx2ygarihnh7lm9") (f (quote (("wav" "hound") ("vorbis" "lewton") ("mp3" "minimp3") ("flac" "claxon") ("default" "wav" "mp3" "vorbis" "flac"))))))

