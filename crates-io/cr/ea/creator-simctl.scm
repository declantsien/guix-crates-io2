(define-module (crates-io cr ea creator-simctl) #:use-module (crates-io))

(define-public crate-creator-simctl-0.1.1 (c (n "creator-simctl") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)))) (h "1a6fash1g05n0yf1dp2bqnyi9s6vm4hfqc4cbxrh8aghlwprs020")))

