(define-module (crates-io cr ea create-bitcoin-transaction) #:use-module (crates-io))

(define-public crate-create-bitcoin-transaction-0.1.0 (c (n "create-bitcoin-transaction") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.29.1") (d #t) (k 0)) (d (n "bitcoin-address") (r "=0.1.4") (d #t) (k 0)) (d (n "bitcoin-hd-keys") (r "=0.1.16") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.3") (d #t) (k 0)) (d (n "sha2") (r "=0.10.5") (d #t) (k 0)))) (h "01dnjyn79iwfpkw4xwgd55cnmn7smyq7c7sj0bpxi6gsi1g0j3ph")))

(define-public crate-create-bitcoin-transaction-0.1.1 (c (n "create-bitcoin-transaction") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.29.1") (d #t) (k 0)) (d (n "bitcoin-address") (r "=0.1.4") (d #t) (k 0)) (d (n "bitcoin-hd-keys") (r "=0.1.17") (d #t) (k 0)) (d (n "bitcoin-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.3") (d #t) (k 0)) (d (n "sha2") (r "=0.10.5") (d #t) (k 0)))) (h "19y410dm98f2cyh67jx9sgmibygqs6rrmlifcw2iqihhv7kh9cn6")))

(define-public crate-create-bitcoin-transaction-0.1.2 (c (n "create-bitcoin-transaction") (v "0.1.2") (d (list (d (n "bitcoin") (r "^0.29.1") (d #t) (k 0)) (d (n "bitcoin-address") (r "=0.1.4") (d #t) (k 0)) (d (n "bitcoin-hd-keys") (r "=0.1.18") (d #t) (k 0)) (d (n "bitcoin-utils") (r "=0.1.1") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.3") (d #t) (k 0)) (d (n "sha2") (r "=0.10.5") (d #t) (k 0)))) (h "0wrsr55nq5l3bxiiwl64k6b2qcbjc3gn52dblzczcp1yqrhz7s6h")))

(define-public crate-create-bitcoin-transaction-0.1.3 (c (n "create-bitcoin-transaction") (v "0.1.3") (d (list (d (n "bitcoin") (r "^0.29.1") (d #t) (k 0)) (d (n "bitcoin-address") (r "=0.1.4") (d #t) (k 0)) (d (n "bitcoin-hd-keys") (r "=0.1.18") (d #t) (k 0)) (d (n "bitcoin-script-opcodes") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoin-utils") (r "=0.1.1") (d #t) (k 0)) (d (n "hex-utilities") (r "^0.1.3") (d #t) (k 0)) (d (n "sha2") (r "=0.10.5") (d #t) (k 0)))) (h "0qjcc2vf1bzpzkm32x6bxa6yndv6nfkvxj4zdl5l9fr98brj8nkr")))

