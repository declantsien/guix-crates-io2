(define-module (crates-io cr ae crae) #:use-module (crates-io))

(define-public crate-crae-0.1.0 (c (n "crae") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.10") (d #t) (k 0)))) (h "0qa8snl2vn595qp4phnriihqs79sfsix0cll8c0w8kjfprh79yrf")))

