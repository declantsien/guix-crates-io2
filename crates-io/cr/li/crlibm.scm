(define-module (crates-io cr li crlibm) #:use-module (crates-io))

(define-public crate-crlibm-0.1.0 (c (n "crlibm") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "ctor") (r "^0.1.21") (d #t) (k 0)))) (h "0nwjyz186jyp4w3rcaq1crz6x0vp5cbzqby9dcixwar1156dngbx")))

(define-public crate-crlibm-0.1.1 (c (n "crlibm") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "ctor") (r "^0.1.21") (d #t) (k 0)))) (h "1lx38y40i37ri0g9mdnq3bslii1iw04pxbymxwbdy3ybg7v7zy6c")))

