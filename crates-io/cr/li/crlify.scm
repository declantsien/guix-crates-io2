(define-module (crates-io cr li crlify) #:use-module (crates-io))

(define-public crate-crlify-1.0.0 (c (n "crlify") (v "1.0.0") (h "1ra6h95mw1cd745w6n2xjjmvw8jnhqnxa7900ijgpv3mzp1lbfyk")))

(define-public crate-crlify-1.0.1 (c (n "crlify") (v "1.0.1") (h "0a8bnzsh7k6zwzxyk47phpsyn38ppxlvqijsmbv6fc6lr1kk9l4b")))

(define-public crate-crlify-1.0.2 (c (n "crlify") (v "1.0.2") (h "15i6c3m16qrmjgb3x31yc8nzcv0pan4519r8307q3nr5csy4knb1")))

(define-public crate-crlify-1.0.3 (c (n "crlify") (v "1.0.3") (h "1h05anipl41b7a2j71cp394hjkbdw9dx5qx56mqc35i6gc7wizyp") (r "1.66")))

(define-public crate-crlify-1.0.4 (c (n "crlify") (v "1.0.4") (h "1gyg1frk270r1ld8wa6kxx5avcv19w1nalh4wix3cl34m6fxj714") (r "1.67")))

