(define-module (crates-io cr l5 crl5-module-fn) #:use-module (crates-io))

(define-public crate-crl5-module-fn-0.1.8 (c (n "crl5-module-fn") (v "0.1.8") (d (list (d (n "actix-web") (r "^4.1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "error-stack") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.20") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (d #t) (k 0)))) (h "02ybab91hyfjjygdd5izp9m627l9ilnbdjp20y15hkvxfz13a7nd")))

