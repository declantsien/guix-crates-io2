(define-module (crates-io cr c2 crc24) #:use-module (crates-io))

(define-public crate-crc24-0.0.1 (c (n "crc24") (v "0.0.1") (h "14qk14cppj6rpmnilx4kdas5wgdm33ll08y2mmqr0wz9niafp24x")))

(define-public crate-crc24-0.0.2 (c (n "crc24") (v "0.0.2") (h "1klswd5lhddbq6l83n89cwrzi9xf2qvav3dnnxz2xja2jb7ryk9d")))

(define-public crate-crc24-0.0.3 (c (n "crc24") (v "0.0.3") (h "1awdn441ji2d8117i4l7ad8n5n6sfcmr95bj8yfx6rar3wvb5zyp")))

(define-public crate-crc24-0.1.0 (c (n "crc24") (v "0.1.0") (h "05g1vs13kyvqx7vgh5fz8afwydggzlxg99znppg061msvsp4nf1y")))

(define-public crate-crc24-0.1.1 (c (n "crc24") (v "0.1.1") (h "0321icx95mlaicyd7pq0n194gr18nvynw031qdibmirbsyv12nhw")))

(define-public crate-crc24-0.1.2 (c (n "crc24") (v "0.1.2") (h "0xxjsawxqdcpwsr69j8v4ah9pby9pv3z43a0w2r4ppkizjns16w3")))

(define-public crate-crc24-0.1.3 (c (n "crc24") (v "0.1.3") (h "15fqycvz565ywl5f0xdjb8g1wjda22m7x70yg7zdml0xk1vighgx")))

(define-public crate-crc24-0.1.4 (c (n "crc24") (v "0.1.4") (h "1cc5xgg10k0cqmh128pnv9znqm4jpx659vr6bzlcw1ls35xgwpd2")))

(define-public crate-crc24-0.1.5 (c (n "crc24") (v "0.1.5") (h "0sdg5lplps1nijfp7fkhwzs6gcqfv5i7isf53smyqbwdz61qnnhw")))

(define-public crate-crc24-0.1.6 (c (n "crc24") (v "0.1.6") (h "1876c92swdpq1iv8j3y21vvfar96pxayn8rhvl42rf1yrx0if4px")))

