(define-module (crates-io cr c2 crc24-openpgp-fast) #:use-module (crates-io))

(define-public crate-crc24-openpgp-fast-0.1.1 (c (n "crc24-openpgp-fast") (v "0.1.1") (d (list (d (n "crc24") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mgv0836wg1pai21xmfj55ny51qbczvqasrwqsnhlagg99z980hr")))

(define-public crate-crc24-openpgp-fast-0.1.2 (c (n "crc24-openpgp-fast") (v "0.1.2") (d (list (d (n "crc24") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0sjq8w2dsbsgsvq6x3jfzsk29jq241kncajibgzzclc628k6zxn6")))

(define-public crate-crc24-openpgp-fast-0.1.3 (c (n "crc24-openpgp-fast") (v "0.1.3") (d (list (d (n "crc24") (r "^0.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1hqbpwzgpfhcaq0rwaci82g9wwrbpjrh0bqilqr4r2cdj0xp7hrj")))

