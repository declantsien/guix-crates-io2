(define-module (crates-io cr ts crtsh) #:use-module (crates-io))

(define-public crate-crtsh-0.4.2 (c (n "crtsh") (v "0.4.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "13kaf74aaqqz9gvjk0b4dqmndqzlfp9yx196zz018ndinw4kzpgj")))

