(define-module (crates-io cr #{4t}# cr4t3) #:use-module (crates-io))

(define-public crate-cr4t3-3.0.0 (c (n "cr4t3") (v "3.0.0") (h "1plvjpqfhb0n866lnn1ikgg0481k36c3ns9qnxdyibqn7rq0qg3k")))

(define-public crate-cr4t3-3.1.0 (c (n "cr4t3") (v "3.1.0") (h "12wry3sz7bncmbr4br7jp7560xsdsqd81zw7vfynwf3izl6l1syz") (y #t)))

(define-public crate-cr4t3-3.1.1 (c (n "cr4t3") (v "3.1.1") (h "0d13rgxiivlrjkk9rajk916qbxqjb95l5pg8r20ya2vygbk3w9r7")))

(define-public crate-cr4t3-3.1.2 (c (n "cr4t3") (v "3.1.2") (h "11gmcbq07qyzg78hq0fj99v9z6js7s7n7jdnrpkrfva4dgyr3rck")))

(define-public crate-cr4t3-3.1.5 (c (n "cr4t3") (v "3.1.5") (h "08y8irm4941rr59148mxidplz8nzifangdvql1k2fqxd9ssfxh0q")))

(define-public crate-cr4t3-4.0.0 (c (n "cr4t3") (v "4.0.0") (h "06czcyzb0iz3qzplx5p8csa9i1n5w385ajrkqfic4s5padyzv5a4")))

(define-public crate-cr4t3-4.1.0 (c (n "cr4t3") (v "4.1.0") (h "1hv39q9p519c7bkg560iq988mssfnlj9z73cpg08lr0949h8j37l")))

(define-public crate-cr4t3-4.1.5 (c (n "cr4t3") (v "4.1.5") (h "1kd3hh8dk5pkxls55zmqifwz05pfy5rsgf6w71jh6lnlnbxn869g")))

