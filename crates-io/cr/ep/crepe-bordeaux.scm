(define-module (crates-io cr ep crepe-bordeaux) #:use-module (crates-io))

(define-public crate-crepe-bordeaux-0.1.0 (c (n "crepe-bordeaux") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "arboard") (r "^3.3.2") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g2246jh98cchy2hqg48vn44dv5bdhigfskw7yhphm0x5y5hp2b1")))

(define-public crate-crepe-bordeaux-0.1.1 (c (n "crepe-bordeaux") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "arboard") (r "^3.3.2") (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "0kph3v0byaagaf8kjmg6wil1xmn4dnq8p1gvhlnzyw1nwyb8lyk5")))

