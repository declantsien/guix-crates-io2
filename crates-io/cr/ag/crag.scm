(define-module (crates-io cr ag crag) #:use-module (crates-io))

(define-public crate-crag-0.1.0 (c (n "crag") (v "0.1.0") (h "1kji5bvmaibv1j3knd21xw0d79n8vrhq2bsnk0l9h2sskz02p3l6") (y #t)))

(define-public crate-crag-0.2.0 (c (n "crag") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "unicode" "wrap_help" "env"))) (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (d #t) (k 0)))) (h "1p2r0ivhajhdcjql8m3j649xr9xqv0dm41wf2c20hcf4dbx0icl3")))

(define-public crate-crag-0.3.0 (c (n "crag") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "unicode" "wrap_help" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0qsxn6rddxcslz6yc0dg83z01s6j000acpnj7f7m2dgixayj8a47")))

(define-public crate-crag-0.4.0 (c (n "crag") (v "0.4.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "unicode" "wrap_help" "env"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "00v1kfhhs20i3a39h1z65l51w2y0hv6chf7w949xiz4cf6ajby9f") (f (quote (("cli" "clap" "colored"))))))

(define-public crate-crag-0.4.1 (c (n "crag") (v "0.4.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "unicode" "wrap_help" "env"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1il1fkqp698hjqbj9iwfcqrjd93kdqkfgbcjn9jxg8gzkl4p76q9") (f (quote (("cli" "clap" "colored"))))))

