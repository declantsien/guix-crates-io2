(define-module (crates-io cr c8 crc8-rs) #:use-module (crates-io))

(define-public crate-crc8-rs-1.0.0 (c (n "crc8-rs") (v "1.0.0") (h "1ajm167764xynws3l5xnz2xd1r6flsijsi6qfwkbhd1jm55bw42x")))

(define-public crate-crc8-rs-1.0.1 (c (n "crc8-rs") (v "1.0.1") (h "184jrwxscgk5w87sdh6k8wh32yg1r7cbvkvfx9clc1dd28wj3n7m")))

(define-public crate-crc8-rs-1.1.0 (c (n "crc8-rs") (v "1.1.0") (h "04p844sa3pza8xpa3c9vqxrq5ga97ry8caa9zj5i15wsr0aa5sk3")))

(define-public crate-crc8-rs-1.1.1 (c (n "crc8-rs") (v "1.1.1") (h "0saz44aiyan3xqm1hbq4vmkzvm1mhf3jidlpslc52a0gw6j0n1l9")))

