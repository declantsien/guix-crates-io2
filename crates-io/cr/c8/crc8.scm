(define-module (crates-io cr c8 crc8) #:use-module (crates-io))

(define-public crate-crc8-0.1.0 (c (n "crc8") (v "0.1.0") (h "1w96143w80dv7fy9p4fvsx61srivfkkdbx2rln6py01h05d44bzf") (y #t)))

(define-public crate-crc8-0.1.1 (c (n "crc8") (v "0.1.1") (h "17jmjwg36saj95gq0bwqariy6b9536hzihhkwkbmyf8ghds227dk")))

