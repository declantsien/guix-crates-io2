(define-module (crates-io cr cn crcnt_ddd) #:use-module (crates-io))

(define-public crate-crcnt_ddd-0.1.0 (c (n "crcnt_ddd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.1") (d #t) (k 0)) (d (n "mysql_async") (r "^0.31") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2nsg3vr0ac359zkg5qrqqnlhqlw063djx7kilvb3zprfs5pl16")))

(define-public crate-crcnt_ddd-0.1.1 (c (n "crcnt_ddd") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.1") (d #t) (k 0)) (d (n "mysql_async") (r "^0.31") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xblq6ajrd04ilhnxvzgw2criwmbhjiz3i1r2ddbhgy0k66l5j4l")))

(define-public crate-crcnt_ddd-0.2.0 (c (n "crcnt_ddd") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_async") (r "^0.31") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12jh1ag3gp55jv2f2nwbv8gmlmdw48f4sb5nm1zqmvhhwq6fxjaa")))

(define-public crate-crcnt_ddd-0.2.1 (c (n "crcnt_ddd") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rsf0ak2bn3wvr8hd971sk0pm16qkwd6ndh2b5d96q9w1vq44ysf")))

(define-public crate-crcnt_ddd-0.2.2 (c (n "crcnt_ddd") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "1fj1dwydkm2gd6r5w8lnrk8jdpl1jbhah4xvqj7chjlsy05hyqmy")))

(define-public crate-crcnt_ddd-0.2.3 (c (n "crcnt_ddd") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "1ffxb7r503ly0np7c449ikgayypp8yl8mlrkf348wa5f3q75xwrn")))

(define-public crate-crcnt_ddd-0.2.4 (c (n "crcnt_ddd") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "1pibqydil8hw005g7i5cjxk8m7q0h0nznacfam5hng5li9c72j00")))

(define-public crate-crcnt_ddd-0.2.5 (c (n "crcnt_ddd") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "0nrzkrbhz658j2i60235dv3qk2bykk78qhcrg3xl524p794ivahs")))

(define-public crate-crcnt_ddd-0.2.6 (c (n "crcnt_ddd") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "1pmqahy4wrlli59xhp4dlakjawa2l7czqfi381zqpldfg3mq0nnb")))

(define-public crate-crcnt_ddd-0.2.7 (c (n "crcnt_ddd") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "02kg8wzwg3fy73sp30bpkvli0pph302jaxxaz7c5cz8di83dhnvs")))

(define-public crate-crcnt_ddd-0.2.8 (c (n "crcnt_ddd") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "116pppwa2fjms6yb49glzq1hvghw07g2ij67y29g1blmhgs9ig1s")))

(define-public crate-crcnt_ddd-0.2.9 (c (n "crcnt_ddd") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "1c8gr960lf16bm2n0aybj3kwdg3z06yvsv46c8p09jh4f7qyrwyh")))

(define-public crate-crcnt_ddd-0.2.10 (c (n "crcnt_ddd") (v "0.2.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "018z1zr5nracldgksnnx27gwdxs08zqpza61dzajdn029wnww3qr")))

(define-public crate-crcnt_ddd-0.2.11 (c (n "crcnt_ddd") (v "0.2.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "0702kd3pr60rzw9a9s3dc6cal70wvpz8zrnil8mm4vpxrkj2ik2c")))

(define-public crate-crcnt_ddd-0.2.12 (c (n "crcnt_ddd") (v "0.2.12") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "0v370afbx94k4wqb0dr6s0jgs986cn5xn9l1dbyrydw8f2bmh2w2")))

(define-public crate-crcnt_ddd-0.2.13 (c (n "crcnt_ddd") (v "0.2.13") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "0mys1ffcyzpbha652jawwv3c6xf8xjls4s80948c8n3gs2llyif9")))

(define-public crate-crcnt_ddd-0.2.14 (c (n "crcnt_ddd") (v "0.2.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crcnt_ddd_macros") (r "^0.2") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "1xmgrbfapsmaawlf05wgj7zx26fcl5sdypzal3chpwsb08mgmp7c")))

