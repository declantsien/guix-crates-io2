(define-module (crates-io cr uz cruzbit-leveldb-sys) #:use-module (crates-io))

(define-public crate-cruzbit-leveldb-sys-1.0.0 (c (n "cruzbit-leveldb-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "ffi-opaque") (r "^2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 1)))) (h "1760i98awjxn5rwprnl6vdpc8mmscfxy6781zzz6wl8knxqpzjka") (f (quote (("snappy")))) (l "leveldb") (r "1.57.0")))

