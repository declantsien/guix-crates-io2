(define-module (crates-io cr uz cruzbit-leveldb) #:use-module (crates-io))

(define-public crate-cruzbit-leveldb-1.0.0 (c (n "cruzbit-leveldb") (v "1.0.0") (d (list (d (n "cruzbit-leveldb-sys") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1vglfywzkzh3msxf24kzacmcfiwjr5kk67ad1xlw2xx6q7m4k2ch") (f (quote (("default" "cruzbit-leveldb-sys/snappy")))) (r "1.57.0")))

