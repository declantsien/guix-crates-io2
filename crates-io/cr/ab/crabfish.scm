(define-module (crates-io cr ab crabfish) #:use-module (crates-io))

(define-public crate-crabfish-0.1.0 (c (n "crabfish") (v "0.1.0") (d (list (d (n "chess") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "11f3v8j8f8kzq5qlcgvhjbdx52px0fzvjx9g2yqafgh6vwqwc3mc")))

(define-public crate-crabfish-0.1.1 (c (n "crabfish") (v "0.1.1") (d (list (d (n "chess") (r "^3") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1pk8k69jnp5jhs4mqmh6p2z102rzp38d49zilyyhqf11d0wyrfcc")))

(define-public crate-crabfish-0.1.2 (c (n "crabfish") (v "0.1.2") (d (list (d (n "chess") (r "^3") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0y9jdlp57kz825xl49xsrz8vvlp4qjbn7pk4b67rszfgskhn3h1s")))

