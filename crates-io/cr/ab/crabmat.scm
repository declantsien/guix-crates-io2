(define-module (crates-io cr ab crabmat) #:use-module (crates-io))

(define-public crate-crabmat-0.1.0 (c (n "crabmat") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "0wqgmmhbc5lzcda4spj1vb9q7048kxbsj7iiq6i95f5fbv6lf7fs")))

