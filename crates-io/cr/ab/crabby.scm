(define-module (crates-io cr ab crabby) #:use-module (crates-io))

(define-public crate-crabby-0.1.0 (c (n "crabby") (v "0.1.0") (d (list (d (n "yew") (r "^0.21.0") (f (quote ("csr"))) (d #t) (k 0)) (d (n "yew-router") (r "^0.18.0") (d #t) (k 0)))) (h "0rqc56nilclp7al38qrcmspj5v3rdzylp8kyv3qv5rs1jhjjkawb")))

