(define-module (crates-io cr ab crab_nbt) #:use-module (crates-io))

(define-public crate-crab_nbt-0.0.1 (c (n "crab_nbt") (v "0.0.1") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("into" "from"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0618ffmzp5varp3yfxdnjgvzahsshx3nvk8y8zlfhaz66nw8cw2b") (f (quote (("macro") ("full" "macro") ("default" "macro"))))))

(define-public crate-crab_nbt-0.0.2 (c (n "crab_nbt") (v "0.0.2") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("into" "from"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "05y0ybbpr8q32w0jb7qk0wwnxgv351lwm99xlx26wm6aycjw5j04") (f (quote (("macro") ("full" "macro") ("default" "macro"))))))

(define-public crate-crab_nbt-0.0.3 (c (n "crab_nbt") (v "0.0.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("into" "from"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1j4mabi57n64imqniaavzl0ffwzl7qvx1jvs6mfknim3bm4lffii") (f (quote (("macro") ("full" "macro") ("default" "macro"))))))

(define-public crate-crab_nbt-0.1.0 (c (n "crab_nbt") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("into" "from"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1ch899pndkji9vs2nx6xyrpfpzyf3cpgzzz3z72p6qz9nk87a9f2") (f (quote (("macro") ("full" "macro") ("default" "macro"))))))

