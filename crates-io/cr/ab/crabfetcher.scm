(define-module (crates-io cr ab crabfetcher) #:use-module (crates-io))

(define-public crate-crabfetcher-0.1.0 (c (n "crabfetcher") (v "0.1.0") (d (list (d (n "columns") (r "^0.1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)))) (h "1k45dn7njv6h9lf5zq6wil89isfm9hw3sdynn9snzfplk4sh2kw0")))

(define-public crate-crabfetcher-0.1.1 (c (n "crabfetcher") (v "0.1.1") (d (list (d (n "columns") (r "^0.1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)))) (h "19hsxyz84b6dnv89y7v3da6cfw1aqnzx5yxx8306z5vg3xzj6r8n")))

(define-public crate-crabfetcher-0.1.2 (c (n "crabfetcher") (v "0.1.2") (d (list (d (n "columns") (r "^0.1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)))) (h "0ijhx8wjnjf8bdb0yqc7j3arq3riy3hrl0745qbk5swwxlzxcn7l")))

(define-public crate-crabfetcher-0.1.3 (c (n "crabfetcher") (v "0.1.3") (d (list (d (n "columns") (r "^0.1.0") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)))) (h "0lw2d8965a53c409jwf4hijdi8w2wjxlpagc5ai1wixadv5pv3bs")))

