(define-module (crates-io cr ab crab_env) #:use-module (crates-io))

(define-public crate-crab_env-0.1.0 (c (n "crab_env") (v "0.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1hbcckql1pvyhcq7i53ahmd27m9s0rp3lkwpnic4fdgfdxpb0kwa")))

(define-public crate-crab_env-0.1.1 (c (n "crab_env") (v "0.1.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0b1fsbhq6c3sa3djs5cd8ps5psr2v8am67wzqmpfxbzyv2a6lnkp")))

(define-public crate-crab_env-0.1.2 (c (n "crab_env") (v "0.1.2") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "10882hwdymd2w75m4yip9hphnnl647wdy9kkryyi44d1gigqfm4z")))

(define-public crate-crab_env-0.1.3 (c (n "crab_env") (v "0.1.3") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0hffkb4xs129f83a37v2dj6drickzjc3hlihpn5dzn2vpiwv8snv")))

(define-public crate-crab_env-0.1.4 (c (n "crab_env") (v "0.1.4") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0r82igj87a7kwlw2v4akj7v03q6pgjsmnz730l0h4jwk0jr1mig8")))

(define-public crate-crab_env-0.1.5 (c (n "crab_env") (v "0.1.5") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0pzps44y49z7bkfxi3ifk3aqj85fi5p94xw1vcdwc6105i59rjyn")))

(define-public crate-crab_env-0.1.6 (c (n "crab_env") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1i2z3zp7h1s1mgjbvv46i6z9cn5035a037a03pl7xzc0rs83i4bc")))

(define-public crate-crab_env-0.1.7 (c (n "crab_env") (v "0.1.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "01br4magjghppsqn7pvfi7k65kwwhhnlqydh89gs3limwkbdw6ij")))

(define-public crate-crab_env-0.1.8 (c (n "crab_env") (v "0.1.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1mxfhnwy6vsfnmw5ggnd2w1riin9w4g856nrcqha7s64hpx3zr2w")))

(define-public crate-crab_env-0.1.9 (c (n "crab_env") (v "0.1.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0a2nx7a9msix3wkvq7mmkkcmmzkzaw9661lszmqwjshkif556302")))

(define-public crate-crab_env-0.1.10 (c (n "crab_env") (v "0.1.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1m27j0bfswsv02ays3awb3333ps3i0pk78sl87niyry34n1f5yw1")))

(define-public crate-crab_env-0.1.11 (c (n "crab_env") (v "0.1.11") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1g199pshasd3wlrlpz3381972j6664wqdhb815scqm92i4pmps1q")))

