(define-module (crates-io cr ab crab) #:use-module (crates-io))

(define-public crate-crab-0.2.0 (c (n "crab") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "01pn7njxxqk1yzy3v9k0c1hsc4xc2l2sl46n5ii3vach6w8nf2kw")))

(define-public crate-crab-0.2.1 (c (n "crab") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "1j5naf8pbh83mdqrk53q1xszlzcpshiqygqv4g32kx3h624sf4kf")))

(define-public crate-crab-0.2.2 (c (n "crab") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1wxriby2b54nfa9d1l6qc1wrbfnwwdpwpa8288cib68ickjsb8bh")))

