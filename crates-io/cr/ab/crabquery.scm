(define-module (crates-io cr ab crabquery) #:use-module (crates-io))

(define-public crate-crabquery-0.1.0 (c (n "crabquery") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)))) (h "11z847633cg2rxg8asrhlp6sm0x0sjw7kcjfd5jklisqjg6gzpwg")))

(define-public crate-crabquery-0.1.1 (c (n "crabquery") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)))) (h "154d716j5l76j5xvh0kx0a8k4p2zzw9y5njnyxh0gjwsjphs6xl8")))

(define-public crate-crabquery-0.1.2 (c (n "crabquery") (v "0.1.2") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)))) (h "1cfg4jddbivkpxcxcszha69akk63qp4nsgckj1w7gnpsp5q22yzk")))

(define-public crate-crabquery-0.1.3 (c (n "crabquery") (v "0.1.3") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)))) (h "11z2sv62xrandpjij4cvphy55dckj29x4w5p1c70dwjlfxisabkg")))

(define-public crate-crabquery-0.1.4 (c (n "crabquery") (v "0.1.4") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_arcdom") (r "^0.1.1") (d #t) (k 0)))) (h "1xhx2474qvw3j3y31s0a3zdm5nagwmkyd1wh5hwwnbi97165vgdq")))

(define-public crate-crabquery-0.1.5 (c (n "crabquery") (v "0.1.5") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_arcdom") (r "^0.1.1") (d #t) (k 0)))) (h "0lf8hs9br5xzigggl3klac0q8na1f75mw82gc5dszfjpmwm3hlcm")))

(define-public crate-crabquery-0.1.6 (c (n "crabquery") (v "0.1.6") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_arcdom") (r "^0.1.1") (d #t) (k 0)))) (h "046rabdifgy6wfcnk6bjrf71ki3grpfz6wnbcg59hfqzxg936k40")))

(define-public crate-crabquery-0.1.7 (c (n "crabquery") (v "0.1.7") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_arcdom") (r "^0.1.1") (d #t) (k 0)))) (h "15v9a4jjq3f0lhr89lg97sn3b5yyvdsvf01g2nxy2i2v5l0sc081")))

(define-public crate-crabquery-0.1.8 (c (n "crabquery") (v "0.1.8") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10.0") (d #t) (k 0)) (d (n "markup5ever_arcdom") (r "^0.1.1") (d #t) (k 0)))) (h "12jc38nbqb9bfrxmn4jq6s2g91v04h4aw4mjz5gifx4vwphid2lh")))

(define-public crate-crabquery-0.1.9 (c (n "crabquery") (v "0.1.9") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11") (d #t) (k 0)) (d (n "markup5ever_arcdom") (r "^0.1") (d #t) (k 0)))) (h "1gw4l4ah01d4kd820vxfm2rjwxfj619s0900prh2g541h6fq2p7z")))

