(define-module (crates-io cr ab crabsformer) #:use-module (crates-io))

(define-public crate-crabsformer-2019.3.8 (c (n "crabsformer") (v "2019.3.8") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "00l1a79ipb9ldmk5l5bk9fmrk7afqq4l7k2bc7rx9069qrk5z01i")))

(define-public crate-crabsformer-2019.3.9 (c (n "crabsformer") (v "2019.3.9") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "06jkhf43grdz2xc7vzv87jr1mhik5s1vi46534kw3d079zyzh8h2")))

(define-public crate-crabsformer-2019.3.10 (c (n "crabsformer") (v "2019.3.10") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "08d846fmxyakmkb9i2bgscl8pvdjglmyx8azxz68q90rgk44msmb")))

(define-public crate-crabsformer-2019.3.11 (c (n "crabsformer") (v "2019.3.11") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0mvmpsw9rcb6bbx1xsnhdb9if23sk43p0k0kkf9g7hkz14wh1k44")))

(define-public crate-crabsformer-2019.3.12 (c (n "crabsformer") (v "2019.3.12") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0j4fcswr18xw60pxwpb2j8kdbxzz70m4j4ck96v2r2cqmaw7fc8d")))

(define-public crate-crabsformer-2019.3.13 (c (n "crabsformer") (v "2019.3.13") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "12629z68crjw8khlf71srz63sm6a4rl5n2f330dvfi6kadvjpdmg")))

(define-public crate-crabsformer-2019.3.14 (c (n "crabsformer") (v "2019.3.14") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1wf36f085aidszh4j2894anqm6xcc7hqrya4b2mrzzkzcdvivjmx")))

(define-public crate-crabsformer-2019.3.15 (c (n "crabsformer") (v "2019.3.15") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1h45cin063pf48wrq7yvsdn3qyr628cnav16br1g98gv8knka0vk")))

(define-public crate-crabsformer-2019.3.16 (c (n "crabsformer") (v "2019.3.16") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0sxaafdmw6wnkld9agazb76ckjhcs6f8vlk936r5a1isy20kj11i")))

(define-public crate-crabsformer-2019.3.17 (c (n "crabsformer") (v "2019.3.17") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "110928ym0vdlba662wnn8aqjv6prv6irkv21sflkn4ylh46v8rbf")))

