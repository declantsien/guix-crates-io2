(define-module (crates-io cr ab crablit) #:use-module (crates-io))

(define-public crate-crablit-0.1.0 (c (n "crablit") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "124izw0fb85xl397f2r7wmrpm0qg3gy6zmssdjdpl27hvr0fhga8")))

(define-public crate-crablit-0.1.1 (c (n "crablit") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16v9x796fm9ldvrpcmvajw2wgwdhyxcshsnh66ikwfcbwm7lbjy3")))

(define-public crate-crablit-0.1.2 (c (n "crablit") (v "0.1.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "19m3n2qgqdsy3hkdfx06d1aw23anzmlnw85bih27598wyvxkq0j6")))

(define-public crate-crablit-0.1.3 (c (n "crablit") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1b2a4d9iyr2v781bbzzh61d3b6xhhqbjp3msfgzlw4jbdppj88q9")))

(define-public crate-crablit-0.1.4 (c (n "crablit") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1zlz9y4fzhpqgwa0v0cwk9ga1sbriv16592rbz4f15rc99raf8gj")))

(define-public crate-crablit-0.1.5 (c (n "crablit") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1pilaxisbnfpxkavx12i6kxzhmagwsg2wsqvjlmrh5a0j9sj60x8")))

(define-public crate-crablit-0.1.6 (c (n "crablit") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0ig2gawq8bdvl9j55a91fl5jhmzd8gk61r70ddvgpqjnyppgwisb")))

