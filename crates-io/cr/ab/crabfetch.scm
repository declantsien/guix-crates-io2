(define-module (crates-io cr ab crabfetch) #:use-module (crates-io))

(define-public crate-crabfetch-0.1.0 (c (n "crabfetch") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.20.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1q9swp8rniah63js5zr09fz85wfyhyzmshmass6g48irx2c1hv0i")))

