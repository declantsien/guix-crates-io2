(define-module (crates-io cr ab crab-errors-rs) #:use-module (crates-io))

(define-public crate-crab-errors-rs-0.1.6 (c (n "crab-errors-rs") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "async-graphql") (r "^7.0.0") (o #t) (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "libsql") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "098fxlc0i5drn0j2abzzxxbfbphdlfc90bc6dxfjw7q2kcv41xz0") (f (quote (("libsql_error" "libsql") ("axum_error" "axum") ("async_graphql" "async-graphql"))))))

