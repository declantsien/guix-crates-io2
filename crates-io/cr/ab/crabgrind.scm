(define-module (crates-io cr ab crabgrind) #:use-module (crates-io))

(define-public crate-crabgrind-0.1.0 (c (n "crabgrind") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "09d2jlbpd3l7bsq0b7xkfhx5zl89mjpllnli8wp96vlv5nssznkr") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.1 (c (n "crabgrind") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0pzbzccfqc3xv7bzdbvp0px4a2abbfr2ajnh0vk72315yv07i3ni") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.2 (c (n "crabgrind") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1i54npfdlb5kk2dbfmg3sy2srp4kiv4jf46ybq2v71r3c6xi1m4j") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.3 (c (n "crabgrind") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1aikq15g0p95hcchgkli192yk4lgn35mqq0xq65k0diybsm2n426") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.4 (c (n "crabgrind") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "07ln2scvlj5f5av7qvry6awqpzzj8rbr65gvkg4qy5hghlm7q6jc") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.5 (c (n "crabgrind") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1wk08jvh2d06abbq9zfwwj3nqbzfgavrrkwnd2vx1dw17bzad70x") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.6 (c (n "crabgrind") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "18kyp5kqjva29yr070jrh6nghdjpsx6blpqb6vyzlgk1pjk5lwcf") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.7 (c (n "crabgrind") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0yab6c4c5hwkqsvav3wi67w36a77q5w2n4df7wa1d4x8cbfafr8z") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.8 (c (n "crabgrind") (v "0.1.8") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0cxgchx9qkzhwgwm6n3m09fjwqxvp46d6npf02y2pxjzvafw83kb") (y #t) (r "1.1")))

(define-public crate-crabgrind-0.1.9 (c (n "crabgrind") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0205c2mh31gr39b5ayg9y8dpngvxxbab4zap0jjq35iaygj47gdx") (r "1.1")))

(define-public crate-crabgrind-0.1.10 (c (n "crabgrind") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0j6ha2wagilqclwx5mv9bswica8pnkmr9i7yl6lqnnd6l9wl45qf") (r "1.1")))

