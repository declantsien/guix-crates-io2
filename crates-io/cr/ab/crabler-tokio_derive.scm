(define-module (crates-io cr ab crabler-tokio_derive) #:use-module (crates-io))

(define-public crate-crabler-tokio_derive-0.1.8 (c (n "crabler-tokio_derive") (v "0.1.8") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "148wdnc1zv8j7fwqnisxrn7mz7gw5fqiq4p5zni128wzb0r39vmn")))

