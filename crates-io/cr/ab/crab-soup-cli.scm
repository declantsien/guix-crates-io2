(define-module (crates-io cr ab crab-soup-cli) #:use-module (crates-io))

(define-public crate-crab-soup-cli-0.0.1 (c (n "crab-soup-cli") (v "0.0.1") (d (list (d (n "cook-markdown") (r "^0.0.1") (d #t) (k 0)) (d (n "cook-with-rust-parser") (r "^0.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0pb023cp3rzz75bxhw57vhkhrlb7ag8x6vvnsiaba1rwgbdjq7kj")))

