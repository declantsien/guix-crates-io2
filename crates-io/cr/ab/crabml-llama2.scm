(define-module (crates-io cr ab crabml-llama2) #:use-module (crates-io))

(define-public crate-crabml-llama2-0.1.0 (c (n "crabml-llama2") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "crabml") (r "^0.1.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "14gh30ix1q4dnh7g311bqffbxs2sgck4mj5gvxg54b1x9pqanhxf")))

