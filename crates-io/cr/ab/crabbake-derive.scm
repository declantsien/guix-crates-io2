(define-module (crates-io cr ab crabbake-derive) #:use-module (crates-io))

(define-public crate-crabbake-derive-0.4.0 (c (n "crabbake-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0z1jsy8wr3cbz1aih9dqk1h3j579hg93vx2yzp8y4rlzbr8l8rab") (y #t)))

