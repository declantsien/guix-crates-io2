(define-module (crates-io cr ab crabzilla) #:use-module (crates-io))

(define-public crate-crabzilla-0.1.0 (c (n "crabzilla") (v "0.1.0") (d (list (d (n "deno_core") (r "^0.79") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "import_fn") (r "^0.1") (d #t) (k 0)))) (h "1jvwf1b7mny6grnn25wvycgp7srb2x0zlw70c1n445zl4328vdqh")))

(define-public crate-crabzilla-0.1.1 (c (n "crabzilla") (v "0.1.1") (d (list (d (n "deno_core") (r "^0.79") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "import_fn") (r "^0.1") (d #t) (k 0)))) (h "15wbv70bmnshbwx6kr4cr6y4j0nvpg2z284k3yjn0rp31pjffib7")))

(define-public crate-crabzilla-0.1.2 (c (n "crabzilla") (v "0.1.2") (d (list (d (n "deno_core") (r "^0.112") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "import_fn") (r "^0.1") (d #t) (k 0)))) (h "0dj3wzgkc0abrkf6jfskhd5bx4qk886vkypihypakz610cyc283s")))

