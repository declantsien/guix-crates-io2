(define-module (crates-io cr ab crabkeeper) #:use-module (crates-io))

(define-public crate-crabkeeper-0.1.0 (c (n "crabkeeper") (v "0.1.0") (h "0bsjy6qnh7lfkgnnv4bi07ajs8fms15slsa25kiz9lq9z6zyjzyg")))

(define-public crate-crabkeeper-0.1.1 (c (n "crabkeeper") (v "0.1.1") (h "1aa0rpkmrwc74pd5jldhwakxqwk8da52krx0s4l60rvzckvxwniz")))

