(define-module (crates-io cr ab crabml-cli) #:use-module (crates-io))

(define-public crate-crabml-cli-0.1.0 (c (n "crabml-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crabml") (r "^0.1.0") (d #t) (k 0)) (d (n "crabml-llama2") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1ghnmjqr2n3813sxdymzpii4ax81gmrk393krljc1hz1r9wa48g7")))

