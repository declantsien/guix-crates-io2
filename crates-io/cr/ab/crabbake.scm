(define-module (crates-io cr ab crabbake) #:use-module (crates-io))

(define-public crate-crabbake-0.1.0 (c (n "crabbake") (v "0.1.0") (h "05q8gi6sznwzr9lh3qdjgqjnjb732hiy4sqpjh6wgvgp6s20d87p") (y #t)))

(define-public crate-crabbake-0.4.0 (c (n "crabbake") (v "0.4.0") (d (list (d (n "crabbake-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "14rbcskadcl1pw32pywganhygapiy2s163l3qk2d56wgr24aiwbv") (f (quote (("derive" "crabbake-derive")))) (y #t)))

