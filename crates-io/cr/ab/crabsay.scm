(define-module (crates-io cr ab crabsay) #:use-module (crates-io))

(define-public crate-crabsay-0.1.0 (c (n "crabsay") (v "0.1.0") (d (list (d (n "ferris-says") (r "^0.2.1") (d #t) (k 0)))) (h "0agrypykb7n6h69873vbnfvw5r3aln5svrihn52p0s0m563ckldy")))

(define-public crate-crabsay-0.1.1 (c (n "crabsay") (v "0.1.1") (d (list (d (n "ferris-says") (r "^0.2.1") (d #t) (k 0)))) (h "1sxsg1lpkdc7q60lk0x4dnxlwrb14nxc4hhwg9gjrbvnnx6cclg6")))

