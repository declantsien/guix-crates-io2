(define-module (crates-io cr ab crabber) #:use-module (crates-io))

(define-public crate-crabber-0.1.0 (c (n "crabber") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ai13arp48iwhg41ysp05hvqpqv6zlfc8rdqdaxsdqznldbn1807")))

