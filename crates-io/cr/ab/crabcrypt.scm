(define-module (crates-io cr ab crabcrypt) #:use-module (crates-io))

(define-public crate-crabcrypt-0.1.0 (c (n "crabcrypt") (v "0.1.0") (h "09nc295nz38qzqg12waflgricspzmqrwiiki37zkd9xjw33j07dd")))

(define-public crate-crabcrypt-0.1.1 (c (n "crabcrypt") (v "0.1.1") (h "0amasyadja9q01syirijl6l3sg0kmlqwkyn9w4d6a53rv38y8byl")))

