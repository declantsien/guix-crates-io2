(define-module (crates-io cr ab crabml) #:use-module (crates-io))

(define-public crate-crabml-0.1.0 (c (n "crabml") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "half") (r "^2.3.1") (d #t) (k 0)) (d (n "int-enum") (r "^0.5.0") (d #t) (k 0)) (d (n "matrixmultiply") (r "^0.3") (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "pollster") (r "^0.2.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)))) (h "14rmcfp89ghsnv7y84rl7jwvkw0i0ayj4ibw65pq7snz3i9913pd")))

