(define-module (crates-io cr ab crabtype) #:use-module (crates-io))

(define-public crate-crabtype-1.0.0 (c (n "crabtype") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1sadh05i6s8fx2cl688gksmymzkvzv8swhm1sxapghslqymy321i")))

