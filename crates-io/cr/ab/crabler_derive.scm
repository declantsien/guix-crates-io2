(define-module (crates-io cr ab crabler_derive) #:use-module (crates-io))

(define-public crate-crabler_derive-0.1.0 (c (n "crabler_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "137s7dmrq929gx24k4sryf1h8l5wn4c9hzcvgsyl9qdcqjfnkx3h")))

(define-public crate-crabler_derive-0.1.1 (c (n "crabler_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "0hphxf3m74wp5y4cwh296fjfgibd3znx4rpr4yhahircmgf8lzs0")))

(define-public crate-crabler_derive-0.1.2 (c (n "crabler_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "0yvyqs355rgmlih54kr7l9jxi410vccx54zbr5pmv4rvrx7hvlr0")))

(define-public crate-crabler_derive-0.1.3 (c (n "crabler_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "0pm1a5n7mzkn2jk0vbqzs6005ik0s2kginwdznvvp3m7pil8snc5")))

(define-public crate-crabler_derive-0.1.4 (c (n "crabler_derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "04nfl1glm7fc9mfy3s8jfmrdmawxrvrs4m4908l2bnlgbcwqvxh9")))

(define-public crate-crabler_derive-0.1.5 (c (n "crabler_derive") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "058n0i8sd6wiyzdqp0inhxrrakaaawlgqa8ydkyd3y21lljikzrj")))

(define-public crate-crabler_derive-0.1.6 (c (n "crabler_derive") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "0y9d2ym4mb03lgiz2sfg62j8i6kckmvwkrjbbzvd3pa4mc3cl5c3")))

(define-public crate-crabler_derive-0.1.7 (c (n "crabler_derive") (v "0.1.7") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "1hi5ygfgm2ah9d2nr85dja86vjm5zqd90za1x2q6f8s6n5hgcyyh")))

(define-public crate-crabler_derive-0.1.8 (c (n "crabler_derive") (v "0.1.8") (d (list (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.15") (d #t) (k 0)))) (h "10nhh91l109jhnziyn9zp6vq3iv0mcn0gfm3pg3x1add21w4hbc9")))

