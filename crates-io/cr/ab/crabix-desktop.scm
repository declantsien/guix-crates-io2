(define-module (crates-io cr ab crabix-desktop) #:use-module (crates-io))

(define-public crate-crabix-desktop-0.1.0 (c (n "crabix-desktop") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 0)) (d (n "markdown-it") (r "^0.4.0") (f (quote ("linkify" "syntect"))) (d #t) (k 0)))) (h "0x40gaj805869y5m273r4rhv5pmdzn7yky9h88rqvqkla33azkdd")))

(define-public crate-crabix-desktop-0.2.0 (c (n "crabix-desktop") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 0)) (d (n "fermi") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "markdown-it") (r "^0.4.0") (f (quote ("linkify" "syntect"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("net"))) (d #t) (k 0)))) (h "1sjf5f74ly33503x264bzjy96yrdav7mhjx1x3sa26mrxhk0mvg9")))

(define-public crate-crabix-desktop-0.2.1 (c (n "crabix-desktop") (v "0.2.1") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 0)) (d (n "fermi") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "markdown-it") (r "^0.4.0") (f (quote ("linkify" "syntect"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("net"))) (d #t) (k 0)))) (h "1c7lmw3ppnik3if88fralp1hdw98ylvv6i81vnfsvhakm6adb0fy")))

