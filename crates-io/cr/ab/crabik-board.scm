(define-module (crates-io cr ab crabik-board) #:use-module (crates-io))

(define-public crate-crabik-board-0.1.0 (c (n "crabik-board") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nrf52810-hal") (r "^0.12.0") (d #t) (k 0)) (d (n "nrf52810-pac") (r "^0.9") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "1zm0dr83mi5d3jbdhcksfkjdbz1r2049zhqfdbzgjx3mrkr06vm7") (y #t)))

(define-public crate-crabik-board-0.1.1 (c (n "crabik-board") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "nrf52810-hal") (r "^0.12.0") (d #t) (k 0)) (d (n "nrf52810-pac") (r "^0.9") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "0g3mv6s31an67p927hnjp8q8rwax4hrm13qlyjrvcgwjx42wqblg")))

