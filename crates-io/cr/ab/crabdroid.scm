(define-module (crates-io cr ab crabdroid) #:use-module (crates-io))

(define-public crate-crabdroid-0.1.0 (c (n "crabdroid") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "jni") (r "^0.20.0") (d #t) (k 0)) (d (n "jni_fn") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1fpm6q75jq3zx5lyrzjx58cg38na11lm1fzv17q8cl676hsriy6x")))

(define-public crate-crabdroid-0.2.0 (c (n "crabdroid") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "jni") (r "^0.20") (d #t) (k 0)) (d (n "jni_fn") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xg5h43c2p5d15z8l54vwjbpja4v4k1mis4n8mf5lhygbgv8cq80") (f (quote (("thread-pool" "futures/thread-pool") ("default"))))))

(define-public crate-crabdroid-0.3.0 (c (n "crabdroid") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "jni") (r "^0.20") (d #t) (k 0)) (d (n "jni_fn") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vhbxjnwdz198mzab6sjd21xhgnd2pv88162d9vj0i5cpms7z3hi") (f (quote (("thread-pool" "futures/thread-pool") ("default"))))))

