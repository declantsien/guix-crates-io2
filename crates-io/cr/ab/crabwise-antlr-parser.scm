(define-module (crates-io cr ab crabwise-antlr-parser) #:use-module (crates-io))

(define-public crate-crabwise-antlr-parser-0.1.0 (c (n "crabwise-antlr-parser") (v "0.1.0") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)) (d (n "combine-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "either") (r "^1.5.2") (d #t) (k 0)))) (h "1sjsrfcwn239jqw17bg5b5zy49k6q48pwcbrkyfyjdxk8pwzzcsd")))

(define-public crate-crabwise-antlr-parser-0.1.1 (c (n "crabwise-antlr-parser") (v "0.1.1") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)) (d (n "combine-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "either") (r "^1.5.2") (d #t) (k 0)))) (h "0hhsq8jcvgbnhwm8p9z2l7xlgrz3virxax6by0i5hjmgcgg7rrc4")))

(define-public crate-crabwise-antlr-parser-0.1.2 (c (n "crabwise-antlr-parser") (v "0.1.2") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)) (d (n "combine-proc-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "either") (r "^1.5.2") (d #t) (k 0)))) (h "1r3jkbb4igzrzf48j0xdhawj5gyjixwg2x1lxnw0nr94nib6bpw8")))

