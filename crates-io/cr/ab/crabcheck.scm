(define-module (crates-io cr ab crabcheck) #:use-module (crates-io))

(define-public crate-crabcheck-0.1.0 (c (n "crabcheck") (v "0.1.0") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0f6hxd2z3nib0gabalw6g4n3s50akfqyzgqwvhys3qxdzyw0wncx")))

