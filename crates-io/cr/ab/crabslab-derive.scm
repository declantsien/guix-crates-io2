(define-module (crates-io cr ab crabslab-derive) #:use-module (crates-io))

(define-public crate-crabslab-derive-0.1.0 (c (n "crabslab-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "163h7rw7zvmwyx19nnxax438ilgwq8h145kyn1dh7yqwxlpaaa66")))

(define-public crate-crabslab-derive-0.1.1 (c (n "crabslab-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "02bx5r6k8ibgnjh409zb5n3wpzbq17wfwq8bpaxaw290wf7jnwxh")))

(define-public crate-crabslab-derive-0.1.2 (c (n "crabslab-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1yi3d5jqygdsvgxqi2szqr9jypsdr8rwn8mxkp9kikjv22cyk4gq")))

(define-public crate-crabslab-derive-0.1.3 (c (n "crabslab-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1r4ssk8drc19r9qz0wzgmkxw66kzsd4w0nfkyb71p39l6qmj6bl7")))

(define-public crate-crabslab-derive-0.2.0 (c (n "crabslab-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0jw3s73a0ybqmp4if3vb3jxfgyg2bx3v874sspgg0prdxvmsaxys")))

(define-public crate-crabslab-derive-0.2.1 (c (n "crabslab-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0jyvzqp1fgn4ya9109kfal08svihahqmczg6jkps2sxqq479rmhv")))

(define-public crate-crabslab-derive-0.3.0 (c (n "crabslab-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "10nldaxd786ab5sd57fipkj4qpmavmqzxdmid9zrbbnlknc96rlh")))

(define-public crate-crabslab-derive-0.4.0 (c (n "crabslab-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1i1n9x023xkdhmjbrjhrqjfdk3s9v6cckq4vyj7cys6m3wjfw4bc")))

(define-public crate-crabslab-derive-0.4.1 (c (n "crabslab-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1fkf40n6jidyx36r67bpgbpcv3hzcwxim71m2m8mgnkdim4xximj")))

(define-public crate-crabslab-derive-0.4.2 (c (n "crabslab-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1png8khh5ic5zxh77w6659k9yqsmp1q3nx2jnfcphnqp7zflp0r3")))

