(define-module (crates-io cr on cronback-proto) #:use-module (crates-io))

(define-public crate-cronback-proto-0.1.0 (c (n "cronback-proto") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "clock" "std"))) (k 0)) (d (n "dto") (r "^0.1.0") (d #t) (k 0) (p "cronback-dto")) (d (n "pbjson") (r "^0.5.1") (d #t) (k 0)) (d (n "pbjson-build") (r "^0.5.1") (d #t) (k 1)) (d (n "pbjson-types") (r "^0.5.1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9") (d #t) (k 1)) (d (n "ulid") (r "^1.0") (d #t) (k 0)))) (h "1zmynqw6spppwq33vs56gg3h03gqwvckl7splka3gkr7ryxfawdw") (r "1.68.0")))

