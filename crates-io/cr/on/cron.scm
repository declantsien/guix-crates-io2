(define-module (crates-io cr on cron) #:use-module (crates-io))

(define-public crate-cron-0.0.1 (c (n "cron") (v "0.0.1") (h "0xf0kgajzjwh46h4cdlszs2la00iig6kjdcsk3h3yb69vgkx1y5f")))

(define-public crate-cron-0.0.2 (c (n "cron") (v "0.0.2") (d (list (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "19hs6c2l9q9xnhfj3650ca79ghyj0sg7jj4rr50ccvgc4i17n1yl")))

(define-public crate-cron-0.1.0 (c (n "cron") (v "0.1.0") (d (list (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)))) (h "0mfs5xhq708y07szccizahn62mq53zjkclsq2412q98gg5kaxz8c")))

(define-public crate-cron-0.2.0 (c (n "cron") (v "0.2.0") (d (list (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0pnq2s6c487ampcx1lqcr1fikz9hywaaxhp9bydgnqlqyw434bg4")))

(define-public crate-cron-0.3.0 (c (n "cron") (v "0.3.0") (d (list (d (n "chrono") (r "~0.3") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0sdn8nk99sxhj3vd63p5g2mrk2jaaqi286as8p35sil81n815lc3")))

(define-public crate-cron-0.4.0 (c (n "cron") (v "0.4.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0vynrqqb08rc3yxl2a7cs85b74xa1lb1dl29md68255i9rysnd2s")))

(define-public crate-cron-0.4.1 (c (n "cron") (v "0.4.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "1447xnfy3zq26c2n98j5bfcdmqzwdmv0yvcfz3w13i4bf6g5wyv4")))

(define-public crate-cron-0.5.0 (c (n "cron") (v "0.5.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "0fisp2v5yzdg70rsymr8pvdzbjdha42a91z3mlpmkv63hyz7l7ry")))

(define-public crate-cron-0.6.0 (c (n "cron") (v "0.6.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "1f6s95l4hry2mkyzg9mz1ncy87jp249dn9p1ljs970i0i8nicb5b")))

(define-public crate-cron-0.6.1 (c (n "cron") (v "0.6.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~4.1") (d #t) (k 0)))) (h "1addgq9awh8bck9q9fkv6bjld6pwk587mb4jvpcaazkz4wvac05b")))

(define-public crate-cron-0.7.0 (c (n "cron") (v "0.7.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~4.1") (d #t) (k 0)))) (h "18vs9qmzy61l4pnk0v2w6h22qkj0282l7q1hg5rczdisylldvfq5")))

(define-public crate-cron-0.8.0 (c (n "cron") (v "0.8.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "~4.1") (d #t) (k 0)))) (h "0gxy8v46d402flxmarm1vynxw9mrpa9w5s49mxslxvjwadj392k2")))

(define-public crate-cron-0.9.0 (c (n "cron") (v "0.9.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "~4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "13wgcv8ksb23h5qd6099y4lgilv7b5ywdzadldkskxrcfq5ys2g0")))

(define-public crate-cron-0.10.0 (c (n "cron") (v "0.10.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1vylh968g4wriwm7m3haaw73zvfkhzrpqihn0vb4lxw43ml8vvw2")))

(define-public crate-cron-0.10.1 (c (n "cron") (v "0.10.1") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "~0.6") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "03yi4yys1gp8bfjlwsr84vcyypi2hnhx64ilxyf9awvb7k6fbrsf")))

(define-public crate-cron-0.11.0 (c (n "cron") (v "0.11.0") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "~0.6") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "0kbkbii015i92dv29g7yvn57yabrhgq0aq376xd0s41y4klijqnp")))

(define-public crate-cron-0.12.0 (c (n "cron") (v "0.12.0") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "~0.6") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "1dza4ng021s3zl4xljpjx1x5cz7fgdbf2ri8zlmwas60wi8npxqz")))

(define-public crate-cron-0.12.1 (c (n "cron") (v "0.12.1") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "~0.6") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "01qc1cnhibxh55pwv3mwaxvfgbjpgk1lfl7an5m4ljvv0xrkx33g")))

