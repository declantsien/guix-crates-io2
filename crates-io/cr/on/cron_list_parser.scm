(define-module (crates-io cr on cron_list_parser) #:use-module (crates-io))

(define-public crate-cron_list_parser-0.0.1 (c (n "cron_list_parser") (v "0.0.1") (h "0y75ml3a5a46n6rgira4y8wk0qh2da00y8hi07m2xd5j1vp7p0p0")))

(define-public crate-cron_list_parser-0.0.2 (c (n "cron_list_parser") (v "0.0.2") (h "1rv56rqjbj8msjwy2rc6dp3gha0s8wdskf0qjx41pbv19vm8nv8c")))

(define-public crate-cron_list_parser-0.0.3 (c (n "cron_list_parser") (v "0.0.3") (h "14f1qjhb5094qrv5d8wfa4bpkdgrvlrq2ifs1ci1ykax8vcafnx0")))

(define-public crate-cron_list_parser-0.0.4 (c (n "cron_list_parser") (v "0.0.4") (h "00pn03i66yfi46h0vvwwcvza0hnbkpxmhdgc0k4dgwn9323h1bs2")))

