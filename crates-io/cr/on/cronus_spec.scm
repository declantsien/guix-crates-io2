(define-module (crates-io cr on cronus_spec) #:use-module (crates-io))

(define-public crate-cronus_spec-0.1.0 (c (n "cronus_spec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03kxf2sx24xq303nxg136b046qfhlpfsabldn2pgfpdvv8bhpvvk")))

(define-public crate-cronus_spec-0.1.1 (c (n "cronus_spec") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0mfl4ddc9q3pps0k3q62llxzr4s8s43mc1n342dggcc30r6mca48")))

(define-public crate-cronus_spec-0.2.0 (c (n "cronus_spec") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0hfb37mq7b70qx2cvj95ng84l6ga7xnr01mgfmv2611hy689l5i7")))

(define-public crate-cronus_spec-0.2.1 (c (n "cronus_spec") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1aslbg3g4qn588pwpq91r65z2kj649bj5i958csr3rrc14cajg2w")))

(define-public crate-cronus_spec-0.2.2 (c (n "cronus_spec") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1bb8zyds0adshc8rwcm2akbbym9krlavs87nww6b6bd3nkbxyq6z")))

(define-public crate-cronus_spec-0.2.3 (c (n "cronus_spec") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1kprwwydd6zdc4wfhrabz5pxr9r8qpvh1fvpjmyqxgc2w086ynms")))

(define-public crate-cronus_spec-0.2.4 (c (n "cronus_spec") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "09659v7dg0q87dyrfdqaj18d6x1jhi79jn0y23vcn1s43q5n922g")))

(define-public crate-cronus_spec-0.2.5 (c (n "cronus_spec") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10kzq2bqqq85dk4sw1q4z8kjlhiligac4nn27bldjzlbim4cdiwn")))

(define-public crate-cronus_spec-0.2.6 (c (n "cronus_spec") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1s7fi1pv5p7yhba7abw22n82xmkj4l7xllyvnilrcwwb2xjsdpia")))

(define-public crate-cronus_spec-0.2.7 (c (n "cronus_spec") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0vrp85hicgmkq0pl701vkypz93bgas83xi3kn92pnn1y5c77fkvb")))

(define-public crate-cronus_spec-0.2.8 (c (n "cronus_spec") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1r1nl6zvylph7zf3ixb8v97k8prkbq004kq4lklgladxbz92s24q")))

