(define-module (crates-io cr on cronos-program) #:use-module (crates-io))

(define-public crate-cronos-program-0.0.1 (c (n "cronos-program") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "cronos-indexer") (r "^0.0.1") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1fxan6va83gw2hcz4zhgsxjla50xkwkv328416hf5bkd9jdw6xdk") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.2 (c (n "cronos-program") (v "0.0.2") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "cronos-indexer") (r "^0.0.1") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "13njfxfdavnhscrjd8nga80bg6hxi1f1ngdvyjyqs1zls9r6gamk") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.3 (c (n "cronos-program") (v "0.0.3") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1mfqaxxhndka2mkafsgrpaww2j1wzh710hdlqffw17xjy4ic1r45") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.4 (c (n "cronos-program") (v "0.0.4") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1px1rksnsbbsf2cn52aibhf0nhbqv7hnhp1994416jlxqhxn2s2g") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.5 (c (n "cronos-program") (v "0.0.5") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "00mmp9sfg40n35ajgky91vnrh3nkfw736790hx076wx7g3njknlq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.6 (c (n "cronos-program") (v "0.0.6") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "12p1xfqlndhnj6v24bwaqh51336n1a4d55s4ailr1s1acf5d16vi") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.7 (c (n "cronos-program") (v "0.0.7") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0pdbwgarl3g6kx9ipg0786phgb0nk3nbcnxq10rhv4m9wkk6zf4c") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.8 (c (n "cronos-program") (v "0.0.8") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1a7p2y7869nw6446zj72knkjjiw3cs3xkyrrma0d5ymxbn5diqn4") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.9 (c (n "cronos-program") (v "0.0.9") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1ns2p8qh95sm1d2sq0frh7r4h1kll4frbnryr9qarm1mzxsdzbn0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.10 (c (n "cronos-program") (v "0.0.10") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0r287dpmmyvavyplhbjslzsx26ishyv88v3x1i01wmaw72yjcay6") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.11 (c (n "cronos-program") (v "0.0.11") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "02nqqry6w31nmr981m11zbwj3fv6284rmh57wfd2fmww7cfwyz1w") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.12 (c (n "cronos-program") (v "0.0.12") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0gjggpy9dz2mmd0afn0bywg37ry53mdbfj6yims60wkzk3n6x20v") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.13 (c (n "cronos-program") (v "0.0.13") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1s2vmcic8kzzxh4mq4z2fycv7gfq37a6wf972d95sfl3nb9rd3x2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.14 (c (n "cronos-program") (v "0.0.14") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1c7nnf0x3qdcgaa7am0ipz3izi99pmmrr8990n0bhr9kjyv2k61x") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.15 (c (n "cronos-program") (v "0.0.15") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0hg2wgwy4fnnvpmpcciwrbzclqsaa7dfdfqldh45ib9lgndlhdza") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.17 (c (n "cronos-program") (v "0.0.17") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1hp0awngaddvjf5dxfn8a8clbarc6kpja8dr6yp8h28mz3svlayb") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.18 (c (n "cronos-program") (v "0.0.18") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0rhyq6c3bl2hzwpxqgyvbhwc5njvpdn2gvk4abqq9d6x09rff46y") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.19 (c (n "cronos-program") (v "0.0.19") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "10v1palj8l7dirjk0vg7q3lnp3ih2ckzi5sw3xih0rzm80xdpf93") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.20 (c (n "cronos-program") (v "0.0.20") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "10fvdi0kpw6cm02wzdqcfk89scfh1j76fl5kxnza7ld1lavvgx53") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.21 (c (n "cronos-program") (v "0.0.21") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1s332wps376z2j4gdgyknz5nwqhs133yjsmzvad2adpw939crhi0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.22 (c (n "cronos-program") (v "0.0.22") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0i5x3z4bqcnl0d35wm59sam281n57zb31wmwj7cg0x5wzaqljp3h") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.23 (c (n "cronos-program") (v "0.0.23") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "01yky38srdpxswdds8q78q6bn36n1f8mafggf7kr8r6szv447y1v") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.24 (c (n "cronos-program") (v "0.0.24") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1b4gjh3phh1w65k8n271gayyna8m4l7sha2r54q3fp768lyjxv0r") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.25 (c (n "cronos-program") (v "0.0.25") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "13g6dls9rapq5d14x64gn7s1n144639g4pzw7izpci1g635ym8rq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.26 (c (n "cronos-program") (v "0.0.26") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1407a1znpsrgalfi89p3mj1k32dgd4ya0bb0ddjr8q6zffycf7aq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.27 (c (n "cronos-program") (v "0.0.27") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "12afi9nbq6gs37vycdbl6zqrfalj7arawbn7l2nwx8yk9awkrljq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.28 (c (n "cronos-program") (v "0.0.28") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "1n555m78jl4fk12sg5qx6w0vh9bwn54rg02b0y8000sv5cdpssml") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.0.29 (c (n "cronos-program") (v "0.0.29") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "10pcv585gk0yf4czb6p81qxqnzml80adrzrrxv4l613a8v4n371b") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.0 (c (n "cronos-program") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "000zxr1cb5ikxqdym7dldv680l9llsvyw1mlqqw7hl6prkmw9abk") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.1 (c (n "cronos-program") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "07gac1zny2mhf0jh79cg3d0jvhxhnik181825xlp76fgfxwcaqsd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.2 (c (n "cronos-program") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.12") (d #t) (k 0)))) (h "0ka5806fax7qmhzhg3lxn8789g39qlhbbf9h7bnfngi9g2dihc01") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.3 (c (n "cronos-program") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.9") (d #t) (k 0)))) (h "1slzrs926bcf1ww9984vdcrz4lbvy6f9a0l0kpxxald8wrq2csm0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.4 (c (n "cronos-program") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.9") (d #t) (k 0)))) (h "0p0m2flizlzmhpgf3yxsp5lacxwr4qglpnzpjnaq584z9i45a3n2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.5 (c (n "cronos-program") (v "0.1.5") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.9") (d #t) (k 0)))) (h "14gdj2qqssyr8r3cqjnhz9g3qc9sfw4qrj8r3y3bil57g182ljl0") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.6 (c (n "cronos-program") (v "0.1.6") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.9") (d #t) (k 0)))) (h "14lbmj33p9z7gppsj02yq695b08zn5qk8q688x514g7zih0808xl") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.7 (c (n "cronos-program") (v "0.1.7") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.9") (d #t) (k 0)))) (h "0rik2ryghbn7dh16jcxx4j2gqhyzahy6nmlz6ww2gp4jkkzkxvxh") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-program-0.1.8 (c (n "cronos-program") (v "0.1.8") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.9") (d #t) (k 0)))) (h "0c52shljc9wnqansdx1351f7vajp93vc6hva7y9xpdqcqy3iy8l6") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

