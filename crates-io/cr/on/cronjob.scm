(define-module (crates-io cr on cronjob) #:use-module (crates-io))

(define-public crate-cronjob-0.1.0 (c (n "cronjob") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "0ahwq7r6l10f4n29fmn5rch4fg5n91w88h902zv34d84gh7m3j22") (y #t)))

(define-public crate-cronjob-0.1.1 (c (n "cronjob") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "03bn9k6l0mk6blx4xzlvnqnz2ks7wx615ldfyvz638g9ma2f88fz") (y #t)))

(define-public crate-cronjob-0.1.11 (c (n "cronjob") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "10zabi52bmcx7s3rnv7d2cam5kjndzpnmjzxhfn8b7n0janlcwia") (y #t)))

(define-public crate-cronjob-0.1.12 (c (n "cronjob") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "1ri1b0bj0rim43z1wj33awakzpqm5h1xw63waww0lxc4ipb11xx3") (y #t)))

(define-public crate-cronjob-0.2.12 (c (n "cronjob") (v "0.2.12") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "08f7zl80nkzhy6h605v3fdcx97qxyvfhmbj6cq46n8hrq23n2g60") (y #t)))

(define-public crate-cronjob-0.2.13 (c (n "cronjob") (v "0.2.13") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "1429p3dx7b51pzv2nhaklv9kk5pnsrcdpd4sa23pzrgsp4rs4mnk") (y #t)))

(define-public crate-cronjob-0.2.14 (c (n "cronjob") (v "0.2.14") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "02b5ny8l50ywwayf21k01nh1y4f2hzbqdca66xa5gjfzmc564blx") (y #t)))

(define-public crate-cronjob-0.2.15 (c (n "cronjob") (v "0.2.15") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "cron") (r "^0.4.0") (d #t) (k 0)))) (h "00m90jllyksriywck68ij7453ipmhs74znqa5ha98h69andwlw37") (y #t)))

(define-public crate-cronjob-0.2.16 (c (n "cronjob") (v "0.2.16") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "cron") (r "^0.6.1") (d #t) (k 0)))) (h "1ssfcsi33ynsqx6yngjnjg347hwzd1q3hhb7lmrji9xpr9alfvsl") (y #t)))

(define-public crate-cronjob-0.2.17 (c (n "cronjob") (v "0.2.17") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "cron") (r "^0.6.1") (d #t) (k 0)))) (h "0jhwy4qqdba15czwp8bdn8wkhbms20yazmchh1yi0y71v895wbl9")))

(define-public crate-cronjob-0.3.17 (c (n "cronjob") (v "0.3.17") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "cron") (r "^0.6.1") (d #t) (k 0)))) (h "0419j2yd597ax67qgq7d6ci33jdpha57w06anzf137b7xwbbfbjb")))

(define-public crate-cronjob-0.4.17 (c (n "cronjob") (v "0.4.17") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.11.0") (d #t) (k 0)))) (h "1p7n4ymqzz4s0mkb4ppi3yk1nz7rr4wn9y4s2g5z6xmisl9zb5p5")))

