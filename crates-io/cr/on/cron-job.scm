(define-module (crates-io cr on cron-job) #:use-module (crates-io))

(define-public crate-cron-job-0.1.0 (c (n "cron-job") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "0zcia9q8ay897h7s2z5hijv0g489hidnyscywjnz2ksyzi70y9ja")))

(define-public crate-cron-job-0.1.1 (c (n "cron-job") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "0wj9zd1p4vmng7f2aig5kxpsjm8pfmlppakggidbyr72h9c7r240")))

(define-public crate-cron-job-0.1.2 (c (n "cron-job") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "1305bcwp88ycajp4mg9chl1yd97hvrb0spfgwd71w9dbcsbgz7iv")))

(define-public crate-cron-job-0.1.4 (c (n "cron-job") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)))) (h "0jx5ylas7n35whxqlznmkaivpj4jqk0mv14d4mckmys70jxrdkrh")))

