(define-module (crates-io cr on cron_rs) #:use-module (crates-io))

(define-public crate-cron_rs-0.1.0 (c (n "cron_rs") (v "0.1.0") (d (list (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "14kr26jn3d5bv99iyijjlcmi9rimxxm0w490llhdgxzldql06yfr")))

(define-public crate-cron_rs-0.1.1 (c (n "cron_rs") (v "0.1.1") (d (list (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "0g4j56wj3i0p4qkg1jspvl1z7xip493fsp88iqkzy0i3x01sgmql")))

(define-public crate-cron_rs-0.1.2 (c (n "cron_rs") (v "0.1.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "10cl2na7rqb9yqc1fig730dlrv9wb3p4mqgfzb09rr6f4xvakid3")))

(define-public crate-cron_rs-0.1.3 (c (n "cron_rs") (v "0.1.3") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "1dvcy9sy7s6dl0m451027qbska0ahfgm8zdv4pbpm8r3zz4c6scr")))

(define-public crate-cron_rs-0.1.4 (c (n "cron_rs") (v "0.1.4") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "1q1m0x87734rswvb433805cqqnkgzmx3rv766mzf989kbl990ms1")))

(define-public crate-cron_rs-0.1.5 (c (n "cron_rs") (v "0.1.5") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "0wb6zjsai1wn6dqfjdbs7p78l4yi1wbx1w4if0mdv5r3bgpw4fpb")))

(define-public crate-cron_rs-0.1.6 (c (n "cron_rs") (v "0.1.6") (d (list (d (n "hyper") (r "~0.10") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.9") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "1ba8cqk7klmcfi4d7hib5phc5mr0hjxnv0mk4w7cd879qrbccgk1")))

