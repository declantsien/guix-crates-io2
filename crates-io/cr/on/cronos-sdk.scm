(define-module (crates-io cr on cronos-sdk) #:use-module (crates-io))

(define-public crate-cronos-sdk-0.0.1 (c (n "cronos-sdk") (v "0.0.1") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.7") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-account-decoder") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-client") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.9.5") (d #t) (k 0)))) (h "04pqhd9zr51rk0gzh8z01q6w9v18795zx7f670pcp9mrq2bnmygk")))

(define-public crate-cronos-sdk-0.0.2 (c (n "cronos-sdk") (v "0.0.2") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.7") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "1aswjk36hsykl84ya758x229pvs9vl7ljxkiidc94fkwymcwq7lj")))

(define-public crate-cronos-sdk-0.0.3 (c (n "cronos-sdk") (v "0.0.3") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.8") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "05svcwlb463yfm2qrbd34hfm2l4wzbawfbhh9nlf0i5x1i9di8gy")))

(define-public crate-cronos-sdk-0.0.4 (c (n "cronos-sdk") (v "0.0.4") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.9") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "0c0f9nqqcfyazvisj039mxc88lnpfx8l25kq64lnff0xjwbz0b1v")))

(define-public crate-cronos-sdk-0.0.5 (c (n "cronos-sdk") (v "0.0.5") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.11") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "0p26gpy0rmwcqf10xlcslind2a344q5pndsyviv6pydvfmnd07rx")))

(define-public crate-cronos-sdk-0.0.6 (c (n "cronos-sdk") (v "0.0.6") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.12") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "1sbifdlyhkm1r1v485bq7qwz2blc1mi337bnscvcd6p02hqzsj4w")))

(define-public crate-cronos-sdk-0.0.7 (c (n "cronos-sdk") (v "0.0.7") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.14") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "10clw87r7fjhjg1vrmyphci1bl0xliikq8si0mxcwvbs9a2dsdsl")))

(define-public crate-cronos-sdk-0.0.8 (c (n "cronos-sdk") (v "0.0.8") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.15") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "110nxk7qjb0f4yaclpzir8i0j3mlcgicmxdndq0gf0p90vp9f9gz")))

(define-public crate-cronos-sdk-0.0.9 (c (n "cronos-sdk") (v "0.0.9") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.17") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "1z4n17ay6rdvfncgwwyzh4273al43yi3bll921ry89s84hz7v1a2")))

(define-public crate-cronos-sdk-0.0.10 (c (n "cronos-sdk") (v "0.0.10") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.17") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "15akizahi346yr0hxhispsdrg2yncdjp7issqqm4iq3jm9hi24rq")))

(define-public crate-cronos-sdk-0.0.11 (c (n "cronos-sdk") (v "0.0.11") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.19") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "1nlckv4mlhmx3sldimgf3pswwlvb574n1197f9qzp3rr4bjq1njy")))

(define-public crate-cronos-sdk-0.0.12 (c (n "cronos-sdk") (v "0.0.12") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.20") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "0njmbxryk7varq4q7ln2hvr01xsr5zyg1sqnlcss2gqchiw1bi1l")))

(define-public crate-cronos-sdk-0.0.13 (c (n "cronos-sdk") (v "0.0.13") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.21") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "08b2jg6hjb1m0vfdm2g1vlrb8afimxql6nfrrcki6b2q2k4m3wfi")))

(define-public crate-cronos-sdk-0.0.14 (c (n "cronos-sdk") (v "0.0.14") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.22") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "10jdb6061jg4anp8l63zx9gb1rys4vrw9sjav8igw5w8c2iy8mwy")))

(define-public crate-cronos-sdk-0.0.15 (c (n "cronos-sdk") (v "0.0.15") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.24") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "03cdg5vbn2mc7ph23rr9kwarlvjgv4bpnq9fwsbir19mnfjbxvpf")))

(define-public crate-cronos-sdk-0.0.16 (c (n "cronos-sdk") (v "0.0.16") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.25") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "1sa24dp88vlh5cjnax2qcrbl1hm4h350ysipbidkcbmhaml9yf7h")))

(define-public crate-cronos-sdk-0.0.17 (c (n "cronos-sdk") (v "0.0.17") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.26") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "1w9i13ji9frksjlalidixxwdxi97d6m2hcs2xchr03spnwc6ngac")))

(define-public crate-cronos-sdk-0.0.18 (c (n "cronos-sdk") (v "0.0.18") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.27") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "0p1l06fnbajmrifgsvhkzf38gljr6d2mg5vm8lnz3lvz1jp0nqmw")))

(define-public crate-cronos-sdk-0.0.19 (c (n "cronos-sdk") (v "0.0.19") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.0.28") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "186qyfiljhsp6w05p8kn8skpck46pcmy9m83hp7i44cwzc1hrbn7")))

(define-public crate-cronos-sdk-0.1.0 (c (n "cronos-sdk") (v "0.1.0") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "0xxsyn7i8qk58dgqpb3l79pds2mzi7mgwqwqhg49li6i3jbj662i")))

(define-public crate-cronos-sdk-0.1.1 (c (n "cronos-sdk") (v "0.1.1") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)))) (h "1kygpnvv5z29zv01mn77dxfln5wbgyd83m6wkn4q9zacv073a3s7")))

(define-public crate-cronos-sdk-0.1.2 (c (n "cronos-sdk") (v "0.1.2") (d (list (d (n "anchor-client") (r "^0.20.1") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.9.8") (d #t) (k 0)))) (h "09d8v4vf3rdzm0537c0i0jy0fnagjga5vbq9j53n2x8k1qhqzfab")))

(define-public crate-cronos-sdk-0.1.3 (c (n "cronos-sdk") (v "0.1.3") (d (list (d (n "anchor-client") (r "^0.22.0") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.9.8") (d #t) (k 0)))) (h "0sb2wac5ajrdbh6s7l23y089qa97d3jvcm07aplrlpas4m88qa26")))

(define-public crate-cronos-sdk-0.1.4 (c (n "cronos-sdk") (v "0.1.4") (d (list (d (n "anchor-client") (r "^0.22.0") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.4") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.9.8") (d #t) (k 0)))) (h "1d41q0zkaa3a3jx046dmqbyccjj20xm3365914xgnn7zzmkvp5rq")))

(define-public crate-cronos-sdk-0.1.5 (c (n "cronos-sdk") (v "0.1.5") (d (list (d (n "anchor-client") (r "^0.22.0") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.5") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.9.8") (d #t) (k 0)))) (h "1ydrdva0bq3idwy532ycyir5j3xjlg8jm5nyjl83dzlvh17x844h")))

(define-public crate-cronos-sdk-0.1.6 (c (n "cronos-sdk") (v "0.1.6") (d (list (d (n "anchor-client") (r "^0.22.0") (f (quote ("debug"))) (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.6") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.9.8") (d #t) (k 0)))) (h "1bawn5jnvpv8xn8d82hypl5fv2vm3j7scf0rav6krq4yi9fbvwq3")))

(define-public crate-cronos-sdk-0.1.8 (c (n "cronos-sdk") (v "0.1.8") (d (list (d (n "anchor-client") (r "=0.22.0") (d #t) (k 0)) (d (n "cronos-program") (r "^0.1.8") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "solana-account-decoder") (r "=1.9.9") (d #t) (k 0)) (d (n "solana-client") (r "=1.9.9") (d #t) (k 0)) (d (n "solana-client-helpers") (r "^1.1.0") (d #t) (k 0)) (d (n "solana-program") (r "=1.9.9") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.9") (d #t) (k 0)))) (h "0yj2f2gawhcp2wxa4qh3lrg36r4yw38gn7vfh3irffa5dwg5f2ya")))

