(define-module (crates-io cr on cronback-migration) #:use-module (crates-io))

(define-public crate-cronback-migration-0.1.0 (c (n "cronback-migration") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.0") (f (quote ("runtime-tokio-native-tls" "sqlx-sqlite"))) (d #t) (k 0)))) (h "1kqsa3jcb3823jzs42ryx3nna9njppldmj0cg5kwp4iq4b6xckl0") (r "1.68.0")))

