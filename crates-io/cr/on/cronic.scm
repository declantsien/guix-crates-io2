(define-module (crates-io cr on cronic) #:use-module (crates-io))

(define-public crate-cronic-0.1.0 (c (n "cronic") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cron") (r "^0.7.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1nq80aa15hl17ndsjv093h5vaii4wmiihnzmmqk7c5fhzn2v22wa")))

