(define-module (crates-io cr on crond) #:use-module (crates-io))

(define-public crate-crond-0.1.0 (c (n "crond") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cron-parser") (r "^0.7.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "0hnw2g16a7bg7k57f4j1z5pf3zzg95370hlcjzgykm267fq9ymm6")))

(define-public crate-crond-0.1.1 (c (n "crond") (v "0.1.1") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "cron-parser") (r "^0.7.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "1rmbpy568k6k2avnyafw3ma3wb1c4vkgnsrrqgh46rg0bdcys9b2")))

