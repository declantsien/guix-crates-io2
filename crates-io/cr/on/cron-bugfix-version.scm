(define-module (crates-io cr on cron-bugfix-version) #:use-module (crates-io))

(define-public crate-cron-bugfix-version-0.4.0 (c (n "cron-bugfix-version") (v "0.4.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "nom") (r "~2.1") (d #t) (k 0)))) (h "1d5skgy9b7lfidwac8q2kfdnwf0cb2409mwmdmibz097y6navsjp")))

