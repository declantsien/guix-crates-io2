(define-module (crates-io cr on crony) #:use-module (crates-io))

(define-public crate-crony-0.1.0 (c (n "crony") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.7.0") (d #t) (k 0)))) (h "1v7ycd9fplgkacr5vz5l4a3y9sm4xy9bj8jh6h0b4ah5gmj39d3y")))

(define-public crate-crony-0.2.0 (c (n "crony") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "066nc4vx9svxf5796j0f6y28a1lwz6yq2hq07z7hg36j8xrvh9yl")))

(define-public crate-crony-0.2.1 (c (n "crony") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "11zv1c7mkbwmv2k1z9wd9b951mbspmhw4l8qxzf3f8kszarcc0ip")))

(define-public crate-crony-0.2.2 (c (n "crony") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1c0z5jzldcqjai7iap4ckfd72hajf15fpa65rmjr6q87v4y6pp8p")))

(define-public crate-crony-0.3.0 (c (n "crony") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1z4bs6b9l1hfrz5wij89mwdg3f7vyp7pkwxwjccaqkrp8j6ygdx8")))

(define-public crate-crony-0.3.1 (c (n "crony") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "08hzypah1zqkbmygr4l4vvncb9q21b0rbwzgpjfak63fpp4fila5")))

