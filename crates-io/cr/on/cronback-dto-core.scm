(define-module (crates-io cr on cronback-dto-core) #:use-module (crates-io))

(define-public crate-cronback-dto-core-0.1.0 (c (n "cronback-dto-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde" "clock" "std"))) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "printing"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 2)))) (h "17my48lsc6vhwnz08k3rfybf5271g4xsg7q965z9gky5cayyam79") (r "1.68.0")))

