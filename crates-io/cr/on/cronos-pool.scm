(define-module (crates-io cr on cronos-pool) #:use-module (crates-io))

(define-public crate-cronos-pool-0.2.0-alpha1 (c (n "cronos-pool") (v "0.2.0-alpha1") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "1sphbwrgdr2y08r99nc9ks7hlqwsgyifpql7js4mjb7nfj74j63x") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-pool-0.2.0-alpha3 (c (n "cronos-pool") (v "0.2.0-alpha3") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "1g6q4gpdyv9cgscg3mi568cvfc6gfmlc4sh49bj1mxy1xggk3i2r") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-pool-0.2.0-alpha4 (c (n "cronos-pool") (v "0.2.0-alpha4") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "0f8331j2hymm0xpr819ra5g86gyfb6khs1whzkjwxbjw2d1ml3i9") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-pool-0.2.0-alpha5 (c (n "cronos-pool") (v "0.2.0-alpha5") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "0dqmgwmqz0bkrm088i7rx6w51aicr8d5ng5sdb3d6g14yw9snx6x") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-pool-0.2.0 (c (n "cronos-pool") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "1q57f5pcql5cbrjvs9w90wdhq3ihxa3qp649lmq6qs8g6qmm2ibj") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

