(define-module (crates-io cr on cron-lingo) #:use-module (crates-io))

(define-public crate-cron-lingo-0.1.0 (c (n "cron-lingo") (v "0.1.0") (d (list (d (n "time") (r "=0.2.22") (d #t) (k 0)))) (h "0gp20hcams96dmv20flrlm2k1qnvf8dbd7p7s8m7666w1kqkddj1")))

(define-public crate-cron-lingo-0.1.1 (c (n "cron-lingo") (v "0.1.1") (d (list (d (n "time") (r "=0.2.22") (d #t) (k 0)))) (h "0m0p27yz9wl1h65vlq372d4xyp9wizl9zc9qlglz64i7297y6g5a")))

(define-public crate-cron-lingo-0.2.0 (c (n "cron-lingo") (v "0.2.0") (d (list (d (n "time") (r "=0.2.22") (d #t) (k 0)))) (h "1k5bjwnppsbnr67niic1ila57s7230mxm7ga118aqmrxbzhpw37b")))

(define-public crate-cron-lingo-0.2.1 (c (n "cron-lingo") (v "0.2.1") (d (list (d (n "time") (r "=0.2.22") (d #t) (k 0)))) (h "12jiah0ysbnh59h63myyf9q3lqvdvb3f1kjgzqga8ggk428xm03f")))

(define-public crate-cron-lingo-0.2.2 (c (n "cron-lingo") (v "0.2.2") (d (list (d (n "time") (r "=0.2.22") (d #t) (k 0)))) (h "1jrdwmb1nf95lakf940rlqd2sd91xlbkiqsbj4is98is3v6a79x1")))

(define-public crate-cron-lingo-0.3.0 (c (n "cron-lingo") (v "0.3.0") (d (list (d (n "time") (r "=0.2.22") (d #t) (k 0)))) (h "0jmsll03s8blsff3jv687gk99r18jmq0cbsx0c5lg7p1nn9dyikn")))

(define-public crate-cron-lingo-0.3.1 (c (n "cron-lingo") (v "0.3.1") (d (list (d (n "time") (r "=0.2.22") (d #t) (k 0)))) (h "01fvg2ls2559gp8n6pf65zi8zjl5b96b867n1k0r57ha8pvfa0ca")))

(define-public crate-cron-lingo-0.4.0 (c (n "cron-lingo") (v "0.4.0") (d (list (d (n "time") (r "^0.3") (f (quote ("local-offset" "parsing" "macros"))) (d #t) (k 0)))) (h "1j8i1xi00va5i82s0fw42m20x54qg59ia4izsqqskx8987shbavk")))

(define-public crate-cron-lingo-0.4.1 (c (n "cron-lingo") (v "0.4.1") (d (list (d (n "time") (r "^0.3") (f (quote ("local-offset" "parsing" "macros"))) (d #t) (k 0)))) (h "1lgm3jxwk0kvgzyh0w0hiv1h1ndxlzf50m1pf20kqpfyd00w7gvn")))

(define-public crate-cron-lingo-0.4.2 (c (n "cron-lingo") (v "0.4.2") (d (list (d (n "time") (r "^0.3") (f (quote ("local-offset" "parsing" "macros"))) (d #t) (k 0)))) (h "0lm2f2f4y5yph3qfgpcjdvw1xw147jamc3pjzm81m36cnc3dlam8")))

