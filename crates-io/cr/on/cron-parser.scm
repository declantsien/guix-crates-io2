(define-module (crates-io cr on cron-parser) #:use-module (crates-io))

(define-public crate-cron-parser-0.1.0 (c (n "cron-parser") (v "0.1.0") (h "0b3dq0dk6s4xz50sihlz00w00xdciimw2awmb4y0g9b4g81gzmh8")))

(define-public crate-cron-parser-0.2.0 (c (n "cron-parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)))) (h "18ar4j43gcmhwraskyw8sdx3a33d9iqp9hr4w07whkwyarx9gwvy")))

(define-public crate-cron-parser-0.3.0 (c (n "cron-parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)))) (h "0dmr6538k63y998bc85yi5381xl5hawnxclmdkg086c7zl63z1bz")))

(define-public crate-cron-parser-0.3.1 (c (n "cron-parser") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)))) (h "0ijfff0l5hadw3ccjy141bs8kdb3mrh62f5f0m2za8zw3kg4xa7q")))

(define-public crate-cron-parser-0.4.0 (c (n "cron-parser") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)))) (h "1kjbcgbq4zvw7dp3qjjmqix0bidpai6n4b4abg190kw4fgffirq2")))

(define-public crate-cron-parser-0.4.1 (c (n "cron-parser") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)))) (h "0bhl7d5af1s5zp2j89m0l6k6flj5k1b6d36rgwp0jdri8mx0qml9")))

(define-public crate-cron-parser-0.4.2 (c (n "cron-parser") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)))) (h "1n96n2l4a4cpmfgf424nm4dhp4kwdvhfg39b1ab53f5daawhj841")))

(define-public crate-cron-parser-0.5.0 (c (n "cron-parser") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)))) (h "16jzkzbvn25yiz10r5q1m0197iazsrysq506nwyrzzyj1brhv3br")))

(define-public crate-cron-parser-0.5.1 (c (n "cron-parser") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "09s7h9rmln4zzv1bn27s8hfms8nhp2j6392jwql86bmy9ydspkqm")))

(define-public crate-cron-parser-0.6.0 (c (n "cron-parser") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "09iyb2v67sax5asakzds8f3wgyjhcwd93mic58l1i2pasdzh9fx7")))

(define-public crate-cron-parser-0.7.0 (c (n "cron-parser") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1aipfd2qgbbvhmj4fiz2g2j2m2bx0y8n7drr3fiaa3y71vkly3bn")))

(define-public crate-cron-parser-0.7.1 (c (n "cron-parser") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1q4zmysvfc54wbv9q1l8k617p0ihrzc5swgrf2ijj2vk73fysdlw")))

(define-public crate-cron-parser-0.7.2 (c (n "cron-parser") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "00k1izckpisnicbh0ckbv6xfwynq75x0nhv3a652dgjckrky9fq5")))

(define-public crate-cron-parser-0.7.3 (c (n "cron-parser") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0gq3p2vfdn5fsk9jg1ma814ysmdfbamj43bvh8k4g91mbfbhbah2")))

(define-public crate-cron-parser-0.7.4 (c (n "cron-parser") (v "0.7.4") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0j9n4kdvhwyzr5lk29h3mdq0pgpf1jgg9dbzhi67lkgrmwy35pvx")))

(define-public crate-cron-parser-0.7.5 (c (n "cron-parser") (v "0.7.5") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "0w5iv4h33n85a2950f3ab8xkz4h8309cy0czgk2ak3il5j9h9lq7")))

(define-public crate-cron-parser-0.7.6 (c (n "cron-parser") (v "0.7.6") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1kqvj79y9zsg5ii26f9lii0j3zc9bfb2xryfwh9qcxi3nb474hp9")))

(define-public crate-cron-parser-0.7.7 (c (n "cron-parser") (v "0.7.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1dii30xdxhbv0lfkf56blszjqxgwvzswccp7c0igv5k0x0f6si78")))

(define-public crate-cron-parser-0.7.8 (c (n "cron-parser") (v "0.7.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "1jgpr9a46660n09x4qsvq8khz7ndxyshr1pj1pwyncnpnp6drmcl")))

(define-public crate-cron-parser-0.7.9 (c (n "cron-parser") (v "0.7.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "0xwmb8ap5qs1fqfg4vqnv1zk4ld7b4khh51j8rg2dpm7nkagm9cl")))

(define-public crate-cron-parser-0.7.10 (c (n "cron-parser") (v "0.7.10") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "1y7d7k3d5q8jy8isywcdilcdgirh3wsjwxkffbvg9jn7rg92368y")))

(define-public crate-cron-parser-0.7.11 (c (n "cron-parser") (v "0.7.11") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0wvss8xf24mb4bv7ns646lv6wqjp2pamshhjfaml6vj4cw8xml5k")))

(define-public crate-cron-parser-0.8.0 (c (n "cron-parser") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.24") (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("clock"))) (k 2)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "10rar0bxpw0g1p2qm1ziiy1yc6ngv83hz894kpa2a0cg7l1gd52q")))

(define-public crate-cron-parser-0.8.1 (c (n "cron-parser") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4.31") (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 2)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1nvs222bhvcddphcbj00p7sk9blxwyhbwxf6pracq2cyqp6412zs")))

(define-public crate-cron-parser-0.9.0 (c (n "cron-parser") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 2)) (d (n "chrono-tz") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1zj2iadr2dnb4vw9nhcbfxghz09y2ax4l28hn0j3ndqnrl7gphyy")))

