(define-module (crates-io cr on cronenberg) #:use-module (crates-io))

(define-public crate-cronenberg-0.1.0 (c (n "cronenberg") (v "0.1.0") (d (list (d (n "nom") (r "^4.0.0-alpha2") (d #t) (k 0)))) (h "17ph3bnyh0347c2n6xirdsnbcqky6kx9zv5zpjpn2dg78f4v8i4b") (y #t)))

(define-public crate-cronenberg-0.2.0 (c (n "cronenberg") (v "0.2.0") (d (list (d (n "nom") (r "^4.0.0-alpha2") (d #t) (k 0)))) (h "1nr36ni87qvnq9cp9if3dib8djn56n38nfzkqjsp9y31hyws1gj8")))

(define-public crate-cronenberg-0.3.0 (c (n "cronenberg") (v "0.3.0") (d (list (d (n "nom") (r "^4.0.0-alpha2") (d #t) (k 0)))) (h "1bczp5frn6gdk366bzfwbsmqg9dygz0qhbslnqjirl90yp5x202m")))

