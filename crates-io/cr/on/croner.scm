(define-module (crates-io cr on croner) #:use-module (crates-io))

(define-public crate-croner-0.0.1 (c (n "croner") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1j9hv4nmyinnbhwc7smwykj7daf4ml4i5qwcl3mn9hd7qranfz71")))

(define-public crate-croner-0.0.2 (c (n "croner") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0zfhqglki8v2c81xkr61mc2yaa1c1120qpgldbaf3f9p0kf63w1m")))

(define-public crate-croner-0.0.3 (c (n "croner") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)))) (h "111rm5hq7wcxgzhlhci1n9211mgbjw5v1mg891qp9x05r6fbsb64")))

(define-public crate-croner-0.0.4 (c (n "croner") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)))) (h "0hv3pmp3vmgn85a3lf36rhgziimvapd634ai212af3anv8rl6his")))

(define-public crate-croner-0.0.5 (c (n "croner") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)))) (h "16q6hifwpbb73v9chf6czliybga9z159b5j2nhrnpy2m1z8y65zd")))

(define-public crate-croner-0.0.6 (c (n "croner") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1bhvdwkghjdkkg2pm8k1grr8gb59nlgp3f698wspjz320ky3hbk3")))

(define-public crate-croner-0.0.7 (c (n "croner") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1sljncimbzznaz6jgfzaqpafzar9gwswcrasyh8dmp3xf5a9vxvs")))

(define-public crate-croner-0.0.8 (c (n "croner") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "03bfyfsk5g4bn8zc646lwr5j7j20ghbaxsn8jwp12n6b0in8330x")))

(define-public crate-croner-0.0.9 (c (n "croner") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0lbx8a2jw32j7f76jbk93dp6n8y9f7wngahlx34qn4ssa2n20jn0")))

(define-public crate-croner-0.0.10 (c (n "croner") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1dkq8cr5n9yqcv5wcbd2pb835hgaq79h6a955x4xl10v5rxvvyjy")))

(define-public crate-croner-1.0.0 (c (n "croner") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1kb1ifybzj98wx6pvbzz4awkglbl2kw2ybf1b4r01xfsbhsa7ifi")))

(define-public crate-croner-1.0.1 (c (n "croner") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0q5abxr0awlxszqmvfniafki58amgi3slx8zdg2489qaksqz9372") (y #t)))

(define-public crate-croner-1.0.2 (c (n "croner") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "06l601c3z02arpyss179n4xzwkj602h14w4fpdmh9vi04qk492qp") (y #t)))

(define-public crate-croner-1.0.3 (c (n "croner") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "18y2awh83ffhcwmrjr093sxl64n6yvxpqfaf1ac4n4jpd136h18m")))

(define-public crate-croner-1.0.4 (c (n "croner") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1gh7976i9da4dn44d1rhmbvv5yy2arf70rfd42zdgfi5cp2rks70")))

(define-public crate-croner-1.0.5 (c (n "croner") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1fxy3rgar9y98p2zfqpk98z5sx7cfm6qyvw64q6vg77kb94j0yky")))

(define-public crate-croner-2.0.0 (c (n "croner") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cs77sxv0pjwbx98kcp6rf701v1dlbl1a8ch0j5pjgdx30dzny8j")))

(define-public crate-croner-2.0.1 (c (n "croner") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "04njdqlnn7vfp8m7c3vpnf9r63xvl2ykxhy4ch383n27rbc5jwh4")))

(define-public crate-croner-2.0.2 (c (n "croner") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1x1f2avxxvg98yv46c1wx281mxx178igpi7b5r8vbarxd0lplsbc")))

(define-public crate-croner-2.0.3 (c (n "croner") (v "2.0.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0p20fkkw3kfcn4rs0ix7r5xhhfr8fzc8vj6mr3f48ikgvixjj62q")))

(define-public crate-croner-2.0.4 (c (n "croner") (v "2.0.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1h8nrpqp9zf8xfw1r5zrxxg6rm3s9vxi4i8g1xdaf3pafi9sssji")))

