(define-module (crates-io cr on cronback-dto) #:use-module (crates-io))

(define-public crate-cronback-dto-0.1.0 (c (n "cronback-dto") (v "0.1.0") (d (list (d (n "dto-core") (r "^0.1.0") (d #t) (k 0) (p "cronback-dto-core")) (d (n "dto-derive") (r "^0.1.0") (d #t) (k 0) (p "cronback-dto-derive")) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0g23qpkqa0fs4952v82wpvcyq4m897afah5438cfxn6gfdpra9lw") (r "1.68.0")))

