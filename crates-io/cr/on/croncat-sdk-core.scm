(define-module (crates-io cr on croncat-sdk-core) #:use-module (crates-io))

(define-public crate-croncat-sdk-core-0.1.0 (c (n "croncat-sdk-core") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)))) (h "0b56nqkz7vzq7v6n1pzb3c88r6cm1n13c6k5d0jirka1iz11dc12")))

(define-public crate-croncat-sdk-core-0.1.1 (c (n "croncat-sdk-core") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)))) (h "0sldakz37vi1h03w5bq4wx4b0p8x8bpm6b47z38xvgx13rsc831p")))

(define-public crate-croncat-sdk-core-0.1.2 (c (n "croncat-sdk-core") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "153sfwygvs5d043gnplj93fhkfilxq8bfl12qzgxmlz0rb8af5v2")))

(define-public crate-croncat-sdk-core-0.1.3 (c (n "croncat-sdk-core") (v "0.1.3") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.3") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k8v04lcpyr381ngfqrsy7aiy3914faxzdl0wv4chnjjmcbjvdhr")))

(define-public crate-croncat-sdk-core-0.1.4 (c (n "croncat-sdk-core") (v "0.1.4") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xmk6vsm4xaw973bh8k87pfgvr5ccd0c9gmr5ldvqbyikraqr999")))

(define-public crate-croncat-sdk-core-0.1.5 (c (n "croncat-sdk-core") (v "0.1.5") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zjcyw5arqf4y7a9lryr6yd1f9zp4mign8dv39w1l70si7xm3fd8")))

(define-public crate-croncat-sdk-core-1.0.0 (c (n "croncat-sdk-core") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yxj15bklzq9z5hsjwjq4br6mz9smmfyw4bkbcch3fvc7c3cz1p4")))

(define-public crate-croncat-sdk-core-1.0.1 (c (n "croncat-sdk-core") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i2dhd73xprb9x7zjaf6n7bd9rjj1r786c3zky6lw3wnwggbkf65")))

(define-public crate-croncat-sdk-core-1.0.2 (c (n "croncat-sdk-core") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0341qh81mjsb3yax48nmz7g4hkmqpwxa7gd37fjg9j2v6b4g2gm2")))

(define-public crate-croncat-sdk-core-1.0.3 (c (n "croncat-sdk-core") (v "1.0.3") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17n846lv2z2kqnxq16cb6s6vwv49xjblix6nil6qi31h2vqy4hx3")))

(define-public crate-croncat-sdk-core-1.0.4 (c (n "croncat-sdk-core") (v "1.0.4") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw20") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ykwbrayhkcqh771mlzzw2p49q9ll99g55v3jf4b44x2r1n9fb7n")))

