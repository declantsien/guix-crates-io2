(define-module (crates-io cr on cronos-health) #:use-module (crates-io))

(define-public crate-cronos-health-0.2.0-alpha1 (c (n "cronos-health") (v "0.2.0-alpha1") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "0p6qkx9jmzf60z8gmf7w232bjdnjc15kbwzh5q3fmdzay7cfisfk") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-health-0.2.0-alpha3 (c (n "cronos-health") (v "0.2.0-alpha3") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "19ymrf5438s4gjcjg2fhl6v1120h82jkflw1pgk7qdyxax7dkmlh") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-health-0.2.0-alpha4 (c (n "cronos-health") (v "0.2.0-alpha4") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "0qxmvvydlf15vmnq8xg4wbh97hpw2scvc08ysb78vxrz7snhgvb3") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-health-0.2.0-alpha5 (c (n "cronos-health") (v "0.2.0-alpha5") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "1zwh19gks61g9jjanws8pfv4nl0w97c14imkyml7cd3zmhgmapgd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-cronos-health-0.2.0 (c (n "cronos-health") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)))) (h "1v686vkhgn6ns4y9yd02c1b7swrm5b8rn4hk4vh5qr2r9ip44px1") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

