(define-module (crates-io cr on cron-descriptor) #:use-module (crates-io))

(define-public crate-cron-descriptor-0.1.0 (c (n "cron-descriptor") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rust-i18n") (r "^1.1.2") (d #t) (k 0)) (d (n "strfmt") (r "^0.2.1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "ctor") (r "^0.1.26") (d #t) (k 2)))) (h "07lns7zminyw69bqk30j1xj8g8ddm8zws5nbk55m8zrix3shkhd8")))

(define-public crate-cron-descriptor-0.1.1 (c (n "cron-descriptor") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rust-i18n") (r "^1.1.2") (d #t) (k 0)) (d (n "strfmt") (r "^0.2.1") (d #t) (k 0)) (d (n "string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "ctor") (r "^0.1.26") (d #t) (k 2)))) (h "061ngkzc6y92w01xjxjvxxdfxxshrzby7347v5rrnjmd7wmmwxks")))

