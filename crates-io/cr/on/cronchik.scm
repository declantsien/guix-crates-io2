(define-module (crates-io cr on cronchik) #:use-module (crates-io))

(define-public crate-cronchik-1.0.0 (c (n "cronchik") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.4.3") (d #t) (k 0)) (d (n "time") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1ad2a86yqf1dlini8jxrx2n5zfjfggd14qdg1qxzid69w8113frj")))

(define-public crate-cronchik-1.0.1 (c (n "cronchik") (v "1.0.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.4.3") (d #t) (k 0)) (d (n "time") (r "^0.2") (o #t) (d #t) (k 0)))) (h "18wv7r952hmx7yipw3llwlnwbm75p4ns1fd4f5ms76jzg0h1sv6d")))

(define-public crate-cronchik-1.0.2 (c (n "cronchik") (v "1.0.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.4.3") (d #t) (k 0)) (d (n "time") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0f5ssi7cpl8gcdd2cw7m1l39jzb9p8lbzrwxwjdqar4w9gpjvnjs")))

(define-public crate-cronchik-2.0.0 (c (n "cronchik") (v "2.0.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.4.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "01g5ggzcl82j83h2mqcqlbdg2g7xy57sp7mqwvf6x4lwr0bpf3yv")))

(define-public crate-cronchik-2.0.1 (c (n "cronchik") (v "2.0.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.4.3") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "0mss4zg39zyrph95f2jnix92ibhx6hm20j9js27l44gcb8im1l74")))

(define-public crate-cronchik-2.0.2 (c (n "cronchik") (v "2.0.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "0i2qj9aah1x3s6lpywga6hb0j8crlbw3xny61s5q1lvy4fkjm3d3")))

(define-public crate-cronchik-2.0.3 (c (n "cronchik") (v "2.0.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "1jz9padnfw5j5bgncr73ki9wvnxya8w4wqn7rfg6n9rl15wc1p25") (f (quote (("std"))))))

(define-public crate-cronchik-2.0.4 (c (n "cronchik") (v "2.0.4") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statiki") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("macros"))) (d #t) (k 2)))) (h "120k47yamhsdayzwaqiqdf4splvkb48ys9nycnlwkk70gnynxvaz") (f (quote (("std"))))))

