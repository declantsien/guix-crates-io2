(define-module (crates-io cr on cronista) #:use-module (crates-io))

(define-public crate-cronista-0.1.0 (c (n "cronista") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)))) (h "0i084c5yxwpc84bhjj3db3g124wwc73wkqdccpfcamqqnjm9hz8k")))

