(define-module (crates-io cr on cronwhen) #:use-module (crates-io))

(define-public crate-cronwhen-0.1.1 (c (n "cronwhen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron-parser") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15vgviw41v0rwla19j0cpzs2rcrmhmkh91yy56zzahkcyz6i7vgd")))

(define-public crate-cronwhen-0.1.2 (c (n "cronwhen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron-parser") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1gnmia0k0cpk61211nzbsa7pg0c1fr67bijznmphg11r23yalvp0")))

(define-public crate-cronwhen-0.1.3 (c (n "cronwhen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron-parser") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0km44wxarn9plgws2fsknxm4gj2rlabk75ym1vavyjknwhzr88m4")))

