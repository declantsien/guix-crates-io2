(define-module (crates-io cr on crontab) #:use-module (crates-io))

(define-public crate-crontab-0.0.0 (c (n "crontab") (v "0.0.0") (d (list (d (n "expectest") (r "^0.9.1") (d #t) (k 2)))) (h "0r0yf30pgcixv0dmwsskrbmq343ap23m8sg6lfj14x6238ddsb66")))

(define-public crate-crontab-0.1.0 (c (n "crontab") (v "0.1.0") (d (list (d (n "expectest") (r "^0.9.1") (d #t) (k 2)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "0qypp4zzydxkrj916v919d57ddlxf7fg99zpfq0wancd2ha6209f")))

(define-public crate-crontab-0.2.0 (c (n "crontab") (v "0.2.0") (d (list (d (n "expectest") (r "^0.9.1") (d #t) (k 2)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "0zwjhb2r11wgzp8658mxjf761ky6bc9i8d118yh8bw7p0n0iyikg")))

