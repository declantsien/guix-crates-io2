(define-module (crates-io cr on croner-scheduler) #:use-module (crates-io))

(define-public crate-croner-scheduler-0.0.10 (c (n "croner-scheduler") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "croner") (r "^1.0.4") (d #t) (k 0)))) (h "0vxm16zrfq7bdid9xn4nbm75wpx77d3gy5zcxns4hlrjm03lj2pf")))

(define-public crate-croner-scheduler-0.0.11 (c (n "croner-scheduler") (v "0.0.11") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "croner") (r "^1.0.4") (d #t) (k 0)))) (h "0lfsglbkaysxlr5fb85qn1gpfyvgvd0wqki44swpg5r1b4bilb3k")))

(define-public crate-croner-scheduler-0.0.12 (c (n "croner-scheduler") (v "0.0.12") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "croner") (r "^2.0.3") (d #t) (k 0)))) (h "1dbzgasyzcca4laq0glsjfjxsyfag9533wcwcigp65qngg1vg8lj")))

