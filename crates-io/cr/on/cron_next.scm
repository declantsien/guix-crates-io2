(define-module (crates-io cr on cron_next) #:use-module (crates-io))

(define-public crate-cron_next-0.1.0 (c (n "cron_next") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron_clock") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nd4gdcp9y41x11zpbxjjaxq1j6ph0x4ywdzjfp4015g6myqpfhf")))

