(define-module (crates-io cr on cron_cal) #:use-module (crates-io))

(define-public crate-cron_cal-0.1.0 (c (n "cron_cal") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "cron") (r "^0.10") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07zh1n3xfips7cw3n3nw1d7jy6f0hnihrkbz58qxhnzf31dk9xsc")))

