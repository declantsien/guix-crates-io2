(define-module (crates-io cr on croncat-errors-macro) #:use-module (crates-io))

(define-public crate-croncat-errors-macro-0.1.5 (c (n "croncat-errors-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0c7vv7js1zy8gqw58ky7nl3slzj0jkls5jc016akzfr5kfb382yr")))

(define-public crate-croncat-errors-macro-0.1.6-rc.1 (c (n "croncat-errors-macro") (v "0.1.6-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "125px2spyhppyn4wpw4np78jgnnd7yk5jr6zckw0j9b8c4zh5p1l")))

(define-public crate-croncat-errors-macro-1.0.0 (c (n "croncat-errors-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0dlqgvs8yhqcyhsvfyk5sa6lfc5nz2lg3x0a40ldsf561wch03ap")))

(define-public crate-croncat-errors-macro-1.0.1 (c (n "croncat-errors-macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "01na93q0za6rzmcxffrn73sg4vmkbfypa55hf5y24827g940fd91")))

(define-public crate-croncat-errors-macro-1.0.2 (c (n "croncat-errors-macro") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "15mnjvwh3g8mc6ih0jxj4aa2ima8c4lh4y1qcjls5ifjbap6z1rp")))

(define-public crate-croncat-errors-macro-1.0.3 (c (n "croncat-errors-macro") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1s83b3m4y2yrkn4vckmsmxra7ga5rmcng744zgbkm2nb068akybh")))

(define-public crate-croncat-errors-macro-1.0.4 (c (n "croncat-errors-macro") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "11ss5imldwbswh6gnk4yj1nx1w9sq002hpkxmxfbk4367x3q1w2j")))

