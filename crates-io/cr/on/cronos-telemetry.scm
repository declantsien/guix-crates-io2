(define-module (crates-io cr on cronos-telemetry) #:use-module (crates-io))

(define-public crate-cronos-telemetry-0.1.8 (c (n "cronos-telemetry") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cronos-sdk") (r "^0.1.8") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "elasticsearch") (r "^7.14.0-alpha.1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "solana-account-decoder") (r "=1.9.9") (d #t) (k 0)) (d (n "solana-client") (r "=1.9.9") (d #t) (k 0)) (d (n "solana-client-helpers") (r "=1.1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.9.9") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03rv6g12nv76zbmfrfbdmy4w8zw64dqzyp0j8lrb1kag56v8vnsx")))

