(define-module (crates-io cr on croncat-sdk-factory) #:use-module (crates-io))

(define-public crate-croncat-sdk-factory-0.1.0 (c (n "croncat-sdk-factory") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "1ggr7nqyldy7y96qvp5rfgvyxx00zsav761fd8c9cwr4nzy93kq1")))

(define-public crate-croncat-sdk-factory-0.1.1 (c (n "croncat-sdk-factory") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "06cgkzkgzigprywc4rccgaqs8jrscis8w251knwnv43rf7zr325l")))

(define-public crate-croncat-sdk-factory-0.1.2 (c (n "croncat-sdk-factory") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0k7rq16dw02pkrdmlriiavjwd8n50l5rfmc681fm6m6l8d9j0qsq")))

(define-public crate-croncat-sdk-factory-0.1.3 (c (n "croncat-sdk-factory") (v "0.1.3") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.3") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0l2h90nzaq8nm89ag2x8m93hz5yn25gqid0z2l0gzn2n5i20gi1h")))

(define-public crate-croncat-sdk-factory-0.1.4 (c (n "croncat-sdk-factory") (v "0.1.4") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "00lzpzznxb2vzp69z0gz0d7nb1609ih8jkhd5ds2bx77c6wwkah7")))

(define-public crate-croncat-sdk-factory-0.1.5 (c (n "croncat-sdk-factory") (v "0.1.5") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0ac20izlmxzj6hh812wr6q5ds2ik11qpdkd7zw5jbw5m5bgxzsrd")))

(define-public crate-croncat-sdk-factory-1.0.0 (c (n "croncat-sdk-factory") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0fcy6fwicj0ydx8dlrzm8ligm6xy9zx0d9j5db94ds0hwqjqjna9")))

(define-public crate-croncat-sdk-factory-1.0.1 (c (n "croncat-sdk-factory") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "17lr58lbkph6hsmfc8c39ar92vd8jzgkv16pjarb17jq8s65k0fc")))

(define-public crate-croncat-sdk-factory-1.0.2 (c (n "croncat-sdk-factory") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "0qv2clbs2bd85y451c800vw47f4325mk45p6xxi7p8mfrgx2nhbj")))

(define-public crate-croncat-sdk-factory-1.0.3 (c (n "croncat-sdk-factory") (v "1.0.3") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "1dzrpckcyf9ij13ywvh41lbx07d1y0n1i13j232fvwpnaq2awd38")))

(define-public crate-croncat-sdk-factory-1.0.4 (c (n "croncat-sdk-factory") (v "1.0.4") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)))) (h "16c5g2lyydx5c47rmv9cgll8dfr7h5m0zh3hb9zj2q2rzzn6193s")))

