(define-module (crates-io cr on cronback-dto-derive) #:use-module (crates-io))

(define-public crate-cronback-dto-derive-0.1.0 (c (n "cronback-dto-derive") (v "0.1.0") (d (list (d (n "dto-core") (r "^0.1.0") (d #t) (k 0) (p "cronback-dto-core")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0gbf6sfvaxp0dq193q4zccfwkdpw93m19lqjjqljifcnw3xd9na3") (r "1.68.0")))

