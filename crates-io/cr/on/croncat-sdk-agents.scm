(define-module (crates-io cr on croncat-sdk-agents) #:use-module (crates-io))

(define-public crate-croncat-sdk-agents-0.1.0 (c (n "croncat-sdk-agents") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wfbnszs7pd1s5snx052q08kmy5kg3jfkbk6n17qml1817pwcy0w")))

(define-public crate-croncat-sdk-agents-0.1.1 (c (n "croncat-sdk-agents") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xk0j2qjwlz2nqj7qjiiw0z6m6zhvj3c6x2ml5d9rw1qvashyfjv")))

(define-public crate-croncat-sdk-agents-0.1.2 (c (n "croncat-sdk-agents") (v "0.1.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dw1y469gvp9nysqnyhmz6by39cmcmaj9b0n2hxc8506pk2lwh3s")))

(define-public crate-croncat-sdk-agents-0.1.3 (c (n "croncat-sdk-agents") (v "0.1.3") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.3") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jnfkqpyzmxfm2r80vm9b8gnij1jac3sdmsgprg7kd6lx94imm33")))

(define-public crate-croncat-sdk-agents-0.1.4 (c (n "croncat-sdk-agents") (v "0.1.4") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d7bl05mxzfvs2fp1g96pzr3hwxqyjjkkzhs3kf78xnpj7fxzpna")))

(define-public crate-croncat-sdk-agents-0.1.5 (c (n "croncat-sdk-agents") (v "0.1.5") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jgxp0rdpm9m82dgrfzsb733i4zqif698qzz7dhmfjyppak9bagl")))

(define-public crate-croncat-sdk-agents-1.0.0 (c (n "croncat-sdk-agents") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1125jh2d5d0909y2d4cd4ln9535v4p55g0r5i6sr898szpa081nq")))

(define-public crate-croncat-sdk-agents-1.0.1 (c (n "croncat-sdk-agents") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11wz5ajmrawvgkx5gacpiglz3pxlci5c9872spkn67ridcryy545")))

(define-public crate-croncat-sdk-agents-1.0.2 (c (n "croncat-sdk-agents") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y8yqkl59hsv0ycsiz2iq0z0fdbf9r8isxk74a0zk05zcqn5nkn8")))

(define-public crate-croncat-sdk-agents-1.0.3 (c (n "croncat-sdk-agents") (v "1.0.3") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jjcxnim5542gnad763bccrvyc07508jz7miii8m694955yd24r6")))

(define-public crate-croncat-sdk-agents-1.0.4 (c (n "croncat-sdk-agents") (v "1.0.4") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "croncat-sdk-core") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lpvcarxsvwr14ym9k5h656s35fpvwvjpzfl3iah6221wjhz93fb")))

