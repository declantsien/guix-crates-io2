(define-module (crates-io cr on cronparse) #:use-module (crates-io))

(define-public crate-cronparse-0.1.0 (c (n "cronparse") (v "0.1.0") (h "1dsj8id62nb71wdgqn50i5vnbp44wilmy8g8x3lmmw39qddakwwv")))

(define-public crate-cronparse-0.1.1 (c (n "cronparse") (v "0.1.1") (h "1bmh26bfadfmmdx2bngr1apsh7696jxxik8sk1y74rmlmcclakw6")))

(define-public crate-cronparse-0.1.2 (c (n "cronparse") (v "0.1.2") (h "0l60xgwxjvkl53bxg3hmwr27qjvcyhha1lc68ggpwiz5dbprfp3r")))

(define-public crate-cronparse-0.2.0 (c (n "cronparse") (v "0.2.0") (h "19s2d9fa03104hicnhy5n3hvscd9jigjp2s7vmy4f0369bbx3wgp")))

(define-public crate-cronparse-0.3.0 (c (n "cronparse") (v "0.3.0") (h "012rgadklr14774qc4wjqpm2gn73nyc27hsj59h8cd0g88qmy150")))

(define-public crate-cronparse-0.4.0 (c (n "cronparse") (v "0.4.0") (h "1yrk38pm1gkggkbl52y4w3r4shfsiyh6pcg9bpwvmi3qjw4q7ghv")))

(define-public crate-cronparse-0.4.1 (c (n "cronparse") (v "0.4.1") (h "1zbkab0zx079jcxq2vs4xd8wwsidgdk30fvf9gayv100p16yn2hc")))

(define-public crate-cronparse-0.5.0 (c (n "cronparse") (v "0.5.0") (h "1lx82qck7n5bmk3ic1jixn5ldzacl81svfws7hwgzmy04ybkkazz")))

(define-public crate-cronparse-0.5.1 (c (n "cronparse") (v "0.5.1") (d (list (d (n "skeptic") (r "^0.5.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.5.0") (d #t) (k 2)))) (h "0c5xsjgvawawlhi627m0rmw2dd00sx0k66m8zmgz43j5nzdz4wn9")))

