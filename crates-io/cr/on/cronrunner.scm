(define-module (crates-io cr on cronrunner) #:use-module (crates-io))

(define-public crate-cronrunner-2.2.0 (c (n "cronrunner") (v "2.2.0") (h "1gjq7h62n17g32r3fy58dq4z1iqq5szcrnfjw10i1fpysmfqgijb")))

(define-public crate-cronrunner-2.2.1 (c (n "cronrunner") (v "2.2.1") (h "1z973b4v27a3b4nqqys5gm51kjkyjsx4i1m0v7fcajfpg1xn8zp5")))

(define-public crate-cronrunner-2.3.0 (c (n "cronrunner") (v "2.3.0") (h "0yj83v53wndqs4myr70572hb5gcb4af4sf13wdd09460ff6bc6ya")))

