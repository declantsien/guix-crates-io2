(define-module (crates-io cr on cronos-heartbeat) #:use-module (crates-io))

(define-public crate-cronos-heartbeat-0.1.8 (c (n "cronos-heartbeat") (v "0.1.8") (d (list (d (n "anchor-lang") (r "^0.23.0") (d #t) (k 0)))) (h "0c7k06szf3lvj089iapqdxf2ggkg0nbwfryy17128g4hp5bc54a1") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

