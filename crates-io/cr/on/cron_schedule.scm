(define-module (crates-io cr on cron_schedule) #:use-module (crates-io))

(define-public crate-cron_schedule-0.1.0 (c (n "cron_schedule") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "nom") (r "~4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0kkypmwrrq83znhbz54wdy1818rqb3i27kh66m0c4xc0q0056gac")))

(define-public crate-cron_schedule-0.2.0 (c (n "cron_schedule") (v "0.2.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "nom") (r "~4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1iw5xj5s8z3f0yajly50q0p3yx0ill43l71rhdssr436hy4zkrsk")))

(define-public crate-cron_schedule-0.2.1 (c (n "cron_schedule") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "nom") (r "~4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1qxpi2k1j9gbhmj26c1brb7yvd5ql5dj594hhcbwzncdivgsf79c")))

(define-public crate-cron_schedule-0.2.2 (c (n "cron_schedule") (v "0.2.2") (d (list (d (n "chrono") (r "=0.4.19") (d #t) (k 0)) (d (n "nom") (r "~4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "08qxan3g4xp3sk47hc9vglcsjx1nnpl8ba1v9hb0rah013d46kxz")))

(define-public crate-cron_schedule-0.2.3 (c (n "cron_schedule") (v "0.2.3") (d (list (d (n "chrono") (r "=0.4.23") (f (quote ("clock"))) (k 0)) (d (n "nom") (r "~4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "11f8prcblb17yp9ixl3yvi2sh009bsc12mp4w8smr013z9990bjh")))

(define-public crate-cron_schedule-0.3.0 (c (n "cron_schedule") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "nom") (r "~7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "07maxn9jss39nl4lyhfg0bmpi5157yzq2s7rflgbdcyhylc11y45")))

(define-public crate-cron_schedule-0.3.1 (c (n "cron_schedule") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "nom") (r "~7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1s9a0y1dadhxvmjy3lh453klxr9k3hs7c2vda647ngjgm5s52gbh")))

