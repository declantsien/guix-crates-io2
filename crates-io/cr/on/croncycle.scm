(define-module (crates-io cr on croncycle) #:use-module (crates-io))

(define-public crate-croncycle-0.1.0 (c (n "croncycle") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "cron") (r "^0.12.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1bkx3wqssbqfqf2ykvsj3wfs0nh1gwmjm30i9bqgqawz6dvclyh8")))

(define-public crate-croncycle-0.1.1 (c (n "croncycle") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "cron") (r "^0.12.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "08zp7814gqbn8yxwc7x9fnkxl7bv68r1wbccs7f9i3k1g5ai0kiw")))

