(define-module (crates-io cr on cron-exp) #:use-module (crates-io))

(define-public crate-cron-exp-0.1.0 (c (n "cron-exp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1qzl18bswwj515cizp13jymj15zflm8j98v7i0ls176gryff85pp")))

