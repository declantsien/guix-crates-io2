(define-module (crates-io cr on cron-plus) #:use-module (crates-io))

(define-public crate-cron-plus-0.0.1 (c (n "cron-plus") (v "0.0.1") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "~0.6") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "113f2dfg3kj5m8f7jbsj2fvkkdpidc952q4a2as6c28fv376b7bi")))

(define-public crate-cron-plus-0.0.2 (c (n "cron-plus") (v "0.0.2") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "~0.6") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "0zqncracsljrcc08pla65qhqbil6brgbw1dz36a4iw32vsqq1inz")))

(define-public crate-cron-plus-0.0.3 (c (n "cron-plus") (v "0.0.3") (d (list (d (n "chrono") (r "~0.4") (f (quote ("clock"))) (k 0)) (d (n "chrono-tz") (r "~0.6") (d #t) (k 2)) (d (n "nom") (r "~7") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "17qazhcp9ff7g188k685rz5j6k0l2dbgxa125hvp6qw8jp1pjvkq")))

