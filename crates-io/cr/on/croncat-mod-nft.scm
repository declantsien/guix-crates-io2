(define-module (crates-io cr on croncat-mod-nft) #:use-module (crates-io))

(define-public crate-croncat-mod-nft-1.0.0 (c (n "croncat-mod-nft") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "=1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.0") (d #t) (k 2)) (d (n "cw2") (r "^1.0.1") (d #t) (k 0)) (d (n "cw721") (r "^0.16.0") (d #t) (k 0)) (d (n "cw721-base") (r "^0.16.0") (d #t) (k 2)) (d (n "mod-sdk") (r "^1.0.0") (d #t) (k 0)))) (h "158g2h4yhcdysigdr19w1160x1akb8ygwr4q7k0sc8ppcnm14s1v") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

