(define-module (crates-io cr on cron-expr) #:use-module (crates-io))

(define-public crate-cron-expr-0.1.0 (c (n "cron-expr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (o #t) (d #t) (k 0)))) (h "0820wyg18fv2zbq7dbyvsz0s51slhq7gpp5abzpslxsnpvzr89gs") (f (quote (("serialize" "serde"))))))

