(define-module (crates-io cr yp cryptix-field) #:use-module (crates-io))

(define-public crate-cryptix-field-0.1.0 (c (n "cryptix-field") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "cryptix-bigint") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (k 2)) (d (n "rand_chacha") (r "^0.3.1") (k 2)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)))) (h "13b89g8rdfimri1nw0kpmfyvalziyanxm0x2swgvwbss2xa1lxbl") (f (quote (("std")))) (s 2) (e (quote (("rand" "cryptix-bigint/rand" "dep:rand_core"))))))

