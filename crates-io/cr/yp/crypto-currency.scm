(define-module (crates-io cr yp crypto-currency) #:use-module (crates-io))

(define-public crate-crypto-currency-0.0.1 (c (n "crypto-currency") (v "0.0.1") (d (list (d (n "bincode") (r "0.9.*") (d #t) (k 0)) (d (n "crypto-util") (r "0.0.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)))) (h "1ysj6ccl3lsgc8lakvacp3j8r8lbwa66f4b4zw3rvixavp99g9yc") (y #t)))

(define-public crate-crypto-currency-0.0.2 (c (n "crypto-currency") (v "0.0.2") (d (list (d (n "bincode") (r "0.9.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)))) (h "1sq7w3hnrdmz6yy7vw2wjc9kg3fz5fbcvaf26ivp5civ7zmasci3") (y #t)))

