(define-module (crates-io cr yp cryptolens) #:use-module (crates-io))

(define-public crate-cryptolens-0.0.1 (c (n "cryptolens") (v "0.0.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cdz87adk9v9yckg4gxlga8x4yg1rlw31iiwa8lx5ac46dzji61h")))

