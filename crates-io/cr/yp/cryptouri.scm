(define-module (crates-io cr yp cryptouri) #:use-module (crates-io))

(define-public crate-cryptouri-0.0.1 (c (n "cryptouri") (v "0.0.1") (d (list (d (n "clear_on_drop") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iq-bech32") (r "^0") (d #t) (k 0)))) (h "0iih0z9ssbd318bh5h39b7hmbjqhwr5fllag7l30i7vfyysnbfzs") (f (quote (("nightly" "clear_on_drop/nightly"))))))

(define-public crate-cryptouri-0.1.0 (c (n "cryptouri") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.3") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "zeroize") (r "^0.5") (d #t) (k 0)))) (h "1qm04a1il92snpdlqfnwc38gjbn8k2mqzwpg71xcy8vw7hw49wk6")))

(define-public crate-cryptouri-0.1.1 (c (n "cryptouri") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.3") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "zeroize") (r "^0.9.1") (d #t) (k 0)))) (h "0ydfdmddgy68syhkwdwckdmxmaw59cppghq0hcc7zfsqn5nvj67q")))

(define-public crate-cryptouri-0.2.0 (c (n "cryptouri") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.4") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0263jwri9mb792rsksl66yh2i72czx20dbwlvdnyd7dzc8hzb663")))

(define-public crate-cryptouri-0.3.0 (c (n "cryptouri") (v "0.3.0") (d (list (d (n "anomaly") (r "^0.1") (d #t) (k 0)) (d (n "secrecy") (r "^0.6") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0njv8gj6dabh0kc10i777fmkfr7syiqwx6hp57r6qx6drcqqb2v2")))

(define-public crate-cryptouri-0.4.0 (c (n "cryptouri") (v "0.4.0") (d (list (d (n "anomaly") (r "^0.2") (d #t) (k 0)) (d (n "secrecy") (r "^0.6") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ax87xlq7v0fk3yc0w8x65fxrl75zx90r1nivj78f1k254fqa1sl")))

