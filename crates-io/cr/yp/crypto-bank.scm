(define-module (crates-io cr yp crypto-bank) #:use-module (crates-io))

(define-public crate-crypto-bank-0.0.1 (c (n "crypto-bank") (v "0.0.1") (d (list (d (n "crypto-currency") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-market") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-market-event") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-market-stream") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-market-stream-ws") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-util") (r "0.0.*") (d #t) (k 0)))) (h "03agsa1vs0b614j93vmdfl38ly8ssnz08hfza15r2m6pq9c48n9z")))

