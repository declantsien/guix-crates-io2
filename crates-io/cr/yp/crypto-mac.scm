(define-module (crates-io cr yp crypto-mac) #:use-module (crates-io))

(define-public crate-crypto-mac-0.1.0 (c (n "crypto-mac") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-ops") (r "^0.1") (d #t) (k 0)) (d (n "digest") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1dfr1z7q3h2b34lr633p07fqvdli9a6ihxzzfqsh7cwgb5s2mywp") (y #t)))

(define-public crate-crypto-mac-0.2.0 (c (n "crypto-mac") (v "0.2.0") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1j0ppnqaa7a54sh04dz0zsbvnqrqdf77d8fsbzzhjfs9b78zy36p") (y #t)))

(define-public crate-crypto-mac-0.3.0 (c (n "crypto-mac") (v "0.3.0") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "17laip4yvq54vyh6h9jac3z8c925qz0amz4a4wya3jvdqa32r9nv") (y #t)))

(define-public crate-crypto-mac-0.4.0 (c (n "crypto-mac") (v "0.4.0") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "160ixpghhz5kz16f38kzcyv6lx8wmi4cgbhlhq4nazf678iib43p") (y #t)))

(define-public crate-crypto-mac-0.5.0 (c (n "crypto-mac") (v "0.5.0") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "054ylshsc9y8jpdimzf8czgqhjsz6hsp163yidgl2jx31madm0fm") (y #t)))

(define-public crate-crypto-mac-0.5.1 (c (n "crypto-mac") (v "0.5.1") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "0b7s3kkjiajf5r4279g21pzcgnnsrhhbhkjl4yk62qcj7nl7fg2c") (f (quote (("dev")))) (y #t)))

(define-public crate-crypto-mac-0.5.2 (c (n "crypto-mac") (v "0.5.2") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "0n6r10zlnfv9gbjj0380sxfffxhq1khfjqwsn7fx8iil9pzv9689") (f (quote (("dev")))) (y #t)))

(define-public crate-crypto-mac-0.6.0 (c (n "crypto-mac") (v "0.6.0") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "105lsny6j51pqg5z691vvnygisxxycaknvmaa988ajaqlms6adwr") (f (quote (("dev")))) (y #t)))

(define-public crate-crypto-mac-0.6.1 (c (n "crypto-mac") (v "0.6.1") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "0fwlsbnmd1xvalkm392aqcy7n79jz0yg5iyyg3hbrjr4r3a5bzxq") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-crypto-mac-0.6.2 (c (n "crypto-mac") (v "0.6.2") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "0wfrh1qjpwpzgdgyix17r444cl03qdz90jd8qd3plv04bb80dyks") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-crypto-mac-0.7.0 (c (n "crypto-mac") (v "0.7.0") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "subtle") (r "^1") (k 0)))) (h "1rbrq6qy9dl0pj4ym2zy33miaaa8vpzdss60p9bdb58xy46l0d24") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.8.0 (c (n "crypto-mac") (v "0.8.0") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "1axfs4zmy74rn9666p92j7nmcv11zdp2d51yrppc2dv26cqa715m") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.9.0 (c (n "crypto-mac") (v "0.9.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "0ry5w76z014f4hxcf24031gk2z0j3dzwpvbk7kp00ghngpinxfvz") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.9.1 (c (n "crypto-mac") (v "0.9.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "1jki2vi6ci0b5xyw3vaz0pqba2jvvszgc5pbyvi5rjn7aixdkg2q") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.10.0 (c (n "crypto-mac") (v "0.10.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cipher") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "19iyh7h9qaqrv29dhbd31rm6pq023ry78nw7jwr3qjy3l22zsms8") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.11.0-pre (c (n "crypto-mac") (v "0.11.0-pre") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cipher") (r "=0.3.0-pre.4") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "11r8ls42l5j5n4ra3np994h7d344s9pck29cd6w6bbpxanbqfkz8") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.11.0-pre.1 (c (n "crypto-mac") (v "0.11.0-pre.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cipher") (r "=0.3.0-pre.5") (d #t) (k 0)) (d (n "crypto-common") (r "=0.1.0-pre") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "07cv0sc0rvkjfq2m3c80l6wj57zkazkkhwnxw08ybgvqxf38zmz5") (f (quote (("std" "crypto-common/std") ("dev" "blobby") ("core-api" "crypto-common/core-api"))))))

(define-public crate-crypto-mac-0.11.0 (c (n "crypto-mac") (v "0.11.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cipher") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "0ghh3qmjf7hv580zqdk4yrbg99v57jx773zb7lzi7j4hj24bdyi5") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.10.1 (c (n "crypto-mac") (v "0.10.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cipher") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "06h84hcaksgjzzzc9g9dpmifwx221qzzif6fw8l807khxh471w5z") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-crypto-mac-0.11.1 (c (n "crypto-mac") (v "0.11.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "cipher") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "subtle") (r "=2.4") (k 0)))) (h "05672ncc54h66vph42s0a42ljl69bwnqjh0x4xgj2v1395psildi") (f (quote (("std") ("dev" "blobby"))))))

