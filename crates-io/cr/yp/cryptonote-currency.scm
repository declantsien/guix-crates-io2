(define-module (crates-io cr yp cryptonote-currency) #:use-module (crates-io))

(define-public crate-cryptonote-currency-0.1.0 (c (n "cryptonote-currency") (v "0.1.0") (d (list (d (n "cryptonote-basic") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptonote-config") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "19b54lavcdwjawc5gysp1jd6j9b74n5f42p83sjw6id9akpshl14")))

