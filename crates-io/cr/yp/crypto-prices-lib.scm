(define-module (crates-io cr yp crypto-prices-lib) #:use-module (crates-io))

(define-public crate-crypto-prices-lib-0.1.0 (c (n "crypto-prices-lib") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kaqvp53myf839afklrd70hxwwqkd0rp05z8ayk4rm0rbgbx3mpa") (y #t)))

(define-public crate-crypto-prices-lib-0.2.0 (c (n "crypto-prices-lib") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hxlzdab3nyhiph4x44hzlhi597c6gm4yj66r11h83acpx8wnfr5") (y #t)))

(define-public crate-crypto-prices-lib-0.3.0 (c (n "crypto-prices-lib") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lna6jlh5mg9jn8dr85pnh4sjzjbp8h701izhbcmpdn7xgcmi0cq") (y #t)))

(define-public crate-crypto-prices-lib-0.4.0 (c (n "crypto-prices-lib") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "098xsmvvwavzsyijfmxfhlbqdjd8zizpmwasr8ybfl119wmasxqg") (y #t)))

(define-public crate-crypto-prices-lib-0.4.1 (c (n "crypto-prices-lib") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rvk54wl0nr17lhsqaai25ahqb88a2ng0rkw6g8a2mlmqia92a2m") (y #t)))

(define-public crate-crypto-prices-lib-0.4.2 (c (n "crypto-prices-lib") (v "0.4.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bjhajx753bbh8wcl2qfg8p05d9drgijvgnq2b7sy64ph6yjbpp1") (y #t)))

(define-public crate-crypto-prices-lib-0.5.0 (c (n "crypto-prices-lib") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06b6ic9ril27vhvwdprm92disihw8n7gca4r3sfz1x46zlx6m209") (y #t)))

