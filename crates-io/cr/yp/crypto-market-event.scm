(define-module (crates-io cr yp crypto-market-event) #:use-module (crates-io))

(define-public crate-crypto-market-event-0.0.1 (c (n "crypto-market-event") (v "0.0.1") (d (list (d (n "bincode") (r "0.9.*") (d #t) (k 0)) (d (n "crypto-market") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)))) (h "14bn5k1143m8phxlhj36a1gpcwj1k4a0kwzp710rw7kgf6204pak") (y #t)))

