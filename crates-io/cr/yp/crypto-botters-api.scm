(define-module (crates-io cr yp crypto-botters-api) #:use-module (crates-io))

(define-public crate-crypto-botters-api-0.1.0 (c (n "crypto-botters-api") (v "0.1.0") (h "1bwrfnkrza2gfx9v4r6k7n658wdvc1ap45g7qz2v0ffsjw28zsq4")))

(define-public crate-crypto-botters-api-0.1.1 (c (n "crypto-botters-api") (v "0.1.1") (h "12vw5p77rwrwy3hsbi6xczx9ndmnclp8dmagll9i8r535fnfm365")))

