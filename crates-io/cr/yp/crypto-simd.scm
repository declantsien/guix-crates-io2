(define-module (crates-io cr yp crypto-simd) #:use-module (crates-io))

(define-public crate-crypto-simd-0.1.0 (c (n "crypto-simd") (v "0.1.0") (d (list (d (n "packed_simd") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0ml46anxznvhpvnhhjgqbmxspi8rwgilqi1ihg49zwsb94hg4z78") (f (quote (("std") ("simd") ("default" "simd" "std"))))))

(define-public crate-crypto-simd-0.1.1 (c (n "crypto-simd") (v "0.1.1") (d (list (d (n "packed_simd_crate") (r "^0.3") (o #t) (d #t) (k 0) (p "packed_simd")))) (h "1cllr7a8qpnrd963nwmrljk95vxib6gwj8qq8k29myas9glyx818") (f (quote (("std") ("simd") ("packed_simd" "packed_simd_crate") ("default" "simd" "std"))))))

