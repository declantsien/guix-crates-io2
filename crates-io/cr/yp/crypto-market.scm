(define-module (crates-io cr yp crypto-market) #:use-module (crates-io))

(define-public crate-crypto-market-0.0.1 (c (n "crypto-market") (v "0.0.1") (d (list (d (n "bincode") (r "0.9.*") (d #t) (k 0)) (d (n "crypto-currency") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-util") (r "0.0.*") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)))) (h "0nh6zwh94hl6wpalb1yygqkk802i3hcb0xwaa78rin6179h779rb") (y #t)))

