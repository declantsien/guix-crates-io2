(define-module (crates-io cr yp crypto-client) #:use-module (crates-io))

(define-public crate-crypto-client-0.1.0 (c (n "crypto-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "05d60d5vmy5p74nqrx0rsi42gxjj6aim466n7zaxf7wmhnxiycsf")))

