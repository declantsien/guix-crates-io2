(define-module (crates-io cr yp cryptonote-account) #:use-module (crates-io))

(define-public crate-cryptonote-account-0.1.0 (c (n "cryptonote-account") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "0fvh8zrz0c6ic2sq48kr1qfk74xkbvkmxgdcx7apv7zxb3ihqdb4")))

(define-public crate-cryptonote-account-0.1.1 (c (n "cryptonote-account") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "0gnyjhyz70gg3196rcvg714gqcpcffj5yg1dgj6yrrr332wjv3hs")))

(define-public crate-cryptonote-account-0.1.2 (c (n "cryptonote-account") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cryptonote-base58") (r "^0.1.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "08h504rjbdi9kyd51d0pqb17x74nzy1ji29hvc662cl7h0aqim6l")))

(define-public crate-cryptonote-account-0.1.3 (c (n "cryptonote-account") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cryptonote-base58") (r "^0.1.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "1ff29k5cgkhschrfjxqpf7pikpzi8wby0a99vpspz4dafyhr0zjy")))

(define-public crate-cryptonote-account-0.1.4 (c (n "cryptonote-account") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cryptonote-base58") (r "^0.1.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "038cyff5dmj0n8yadjksx8h14brd3cvlwkqrif9c9qf7aw59ziy3")))

