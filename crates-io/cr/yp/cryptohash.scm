(define-module (crates-io cr yp cryptohash) #:use-module (crates-io))

(define-public crate-cryptohash-0.1.0 (c (n "cryptohash") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "0r1fp4almz6j40cym0q76d2k9gvy7pkl3px8gf5w1kiqa7rmaaff")))

(define-public crate-cryptohash-0.1.1 (c (n "cryptohash") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "0sskqv58pl23vfs1pckvsbfkr66v7lmam538j6zskppzzc9bn3h5")))

(define-public crate-cryptohash-0.2.0 (c (n "cryptohash") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "1n7ly6ssajrcbf11aqdg5pdxj3yyk8q7g9c7k0zij8ibd7r4xmdf")))

(define-public crate-cryptohash-0.3.0 (c (n "cryptohash") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "10v6irggmv5yj66viz61f0a7dig5pg2xpdjpr77k2mbshpmbf70l")))

(define-public crate-cryptohash-0.4.0 (c (n "cryptohash") (v "0.4.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "10w7msfpfklkpd6bk7kq1yxvn3k6s8lbvz11ws3r03mcard3g8al")))

(define-public crate-cryptohash-0.5.0 (c (n "cryptohash") (v "0.5.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "0x4ji10idpbib7xny9dyi6c0cb4391k38145fdcjkrzpkx6c5pz3")))

(define-public crate-cryptohash-0.6.0 (c (n "cryptohash") (v "0.6.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "0kypj9n5ygiv7f95m150hacy67g5dicmala7ffgasxlnm5nnrcy9")))

(define-public crate-cryptohash-0.7.0 (c (n "cryptohash") (v "0.7.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)))) (h "12asai34617gz0b7x8l2hjs2a5z8gwijfb9c2dn7whhrijigyvxs")))

