(define-module (crates-io cr yp cryptostream) #:use-module (crates-io))

(define-public crate-cryptostream-0.1.0 (c (n "cryptostream") (v "0.1.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0zl0qa3j32zqz8lsdaigc3cx0jppq562ww194qdnh4ssjs9a66n8")))

(define-public crate-cryptostream-0.2.0 (c (n "cryptostream") (v "0.2.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "09d88ajv1n52k30nv1llzhq9012zzrhmqba6hq2w9fxnnp87ygiv") (f (quote (("openssl-vendored" "openssl/vendored") ("default" "openssl-vendored"))))))

(define-public crate-cryptostream-0.3.0 (c (n "cryptostream") (v "0.3.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "192brsvpr49asbhxaizcg1w3ppp5frfn15sy9lwqkr367l9wggsz") (f (quote (("openssl-vendored" "openssl/vendored") ("default" "openssl-vendored"))))))

(define-public crate-cryptostream-0.3.1 (c (n "cryptostream") (v "0.3.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1w1ipxfmy7x1lgw04wmw6ijz172lpkl4svim1andxvby0sv1z3bg") (f (quote (("openssl-vendored" "openssl/vendored") ("default" "openssl-vendored"))))))

(define-public crate-cryptostream-0.3.2 (c (n "cryptostream") (v "0.3.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("min_const_gen"))) (d #t) (k 2)) (d (n "size") (r "^0.4.0") (d #t) (k 2)))) (h "1499acfsbc68iday0nyixkpx7fi4wfpnmv7br3a0ljw27wx2vb6a") (f (quote (("openssl-vendored" "openssl/vendored"))))))

