(define-module (crates-io cr yp crypto-morse) #:use-module (crates-io))

(define-public crate-crypto-morse-0.1.0 (c (n "crypto-morse") (v "0.1.0") (h "1qbli7wbkgbvf793qkwj2iwcw40fap74hm83v6yazj074yyqnvr4")))

(define-public crate-crypto-morse-0.2.0 (c (n "crypto-morse") (v "0.2.0") (h "07c23bllbayr5v17fdrj5lq1z4hiqh23lxcx75zk3qgvygv0ddcb")))

