(define-module (crates-io cr yp crypto-tests) #:use-module (crates-io))

(define-public crate-crypto-tests-0.1.0 (c (n "crypto-tests") (v "0.1.0") (d (list (d (n "digest") (r "^0.1") (d #t) (k 0)))) (h "013xc4aa0rsvigxi4yr053q25n3zkslmb1ifdxfy9dx7j53dxsyi")))

(define-public crate-crypto-tests-0.1.1 (c (n "crypto-tests") (v "0.1.1") (d (list (d (n "digest") (r "^0.2") (d #t) (k 0)))) (h "00q305p5rvaicpiz2fj278sphhdxm5g75kllcr50fm1bq3mqqyvd")))

(define-public crate-crypto-tests-0.2.0 (c (n "crypto-tests") (v "0.2.0") (d (list (d (n "digest") (r "^0.3") (d #t) (k 0)))) (h "15dwjj4dxzqwilrrsqvfqxp3swah9whz59d60x90vm2nxw851p2l")))

(define-public crate-crypto-tests-0.2.1 (c (n "crypto-tests") (v "0.2.1") (d (list (d (n "digest") (r "^0.3") (d #t) (k 0)))) (h "0fdk6wym3w2bv1m2iganhdlzyfxbgmx6sz8iqjw4513i8w66qx8g")))

(define-public crate-crypto-tests-0.2.2 (c (n "crypto-tests") (v "0.2.2") (d (list (d (n "block-cipher-trait") (r "^0.1") (d #t) (k 0)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1qgjlbhqvyq5hh58bw7aw83wh2xv1j2552y80zf07phvb027m77d") (y #t)))

(define-public crate-crypto-tests-0.2.3 (c (n "crypto-tests") (v "0.2.3") (d (list (d (n "block-cipher-trait") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "088z291ik8bz32qxfmspy61grzmgmq3fgs1si6j8cv9xjj2pshpp") (y #t)))

(define-public crate-crypto-tests-0.2.4 (c (n "crypto-tests") (v "0.2.4") (d (list (d (n "block-cipher-trait") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0v6x27rhyvc6ba0lvmzfp9lxbh0q3z9qz962v6v9r4ap0w08c0mm")))

(define-public crate-crypto-tests-0.3.0 (c (n "crypto-tests") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "0f12s89nnwl1jcjw8fp3il31b4wq3gg0399fqj53zmhb749rzawj")))

(define-public crate-crypto-tests-0.3.1 (c (n "crypto-tests") (v "0.3.1") (d (list (d (n "block-cipher-trait") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "06h1vrf673qhxw60i94vlwasfkvflmbqh57kxn10yygl9fyjj50b")))

(define-public crate-crypto-tests-0.3.2 (c (n "crypto-tests") (v "0.3.2") (d (list (d (n "block-cipher-trait") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "0bizg07h57drs9rz08jldz9icwaylgzf7b394li5cy108hbfj4xk")))

(define-public crate-crypto-tests-0.4.0-alpha (c (n "crypto-tests") (v "0.4.0-alpha") (d (list (d (n "block-cipher-trait") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "digest") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1bzqag707gsfkr0ifv3bzwk44jas2impaacr459d8i3xs8psryk8")))

(define-public crate-crypto-tests-0.4.0-beta (c (n "crypto-tests") (v "0.4.0-beta") (d (list (d (n "block-cipher-trait") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "digest") (r "^0.5.0-delta") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1fvqp6x1gjhm0cbwvfixbr7bpkhhz6m70cy8cqv2rn1v1560hhmj")))

(define-public crate-crypto-tests-0.4.0-rc1 (c (n "crypto-tests") (v "0.4.0-rc1") (d (list (d (n "block-cipher-trait") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "digest") (r "^0.5.0-rc1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1dg659pqppgx621svif46cywjpli7718gi5bihjzvs5zmiyqn6ay")))

(define-public crate-crypto-tests-0.4.0-rc2 (c (n "crypto-tests") (v "0.4.0-rc2") (d (list (d (n "block-cipher-trait") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "digest") (r "^0.5.0-rc1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1wir3pil83yjzbhlcncahkqps44i63vqfd8gz85fjl44bmd6s0gi")))

(define-public crate-crypto-tests-0.4.0 (c (n "crypto-tests") (v "0.4.0") (d (list (d (n "block-cipher-trait") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "070picx7wds2yqhbb994ghsrya8k0k0ck177ksq3fmgkb1w2gi3x")))

(define-public crate-crypto-tests-0.4.1 (c (n "crypto-tests") (v "0.4.1") (d (list (d (n "block-cipher-trait") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1vj2q5yzdfsbdf2mpdp40g8vb6jfvmvhqb88dr7vibx6v5my9cxw")))

(define-public crate-crypto-tests-0.5.0 (c (n "crypto-tests") (v "0.5.0") (d (list (d (n "block-cipher-trait") (r "^0.4.0-beta") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "digest") (r "^0.6.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "065vqdxm86r0h9823q91akiqfpr94jvdqhrj3gcbv2k5dy0hpcpg")))

(define-public crate-crypto-tests-0.5.2 (c (n "crypto-tests") (v "0.5.2") (d (list (d (n "block-cipher-trait") (r "^0.4.0-gamma") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "1c5idd02sgflga8ibb9mnyzch2df4w54fb8s69bn491v1w8npnzg")))

(define-public crate-crypto-tests-0.5.3 (c (n "crypto-tests") (v "0.5.3") (d (list (d (n "block-cipher-trait") (r "^0.4.0-gamma") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "02ybx4kxldbxmn244flh4ax03pljgc6kxm36n86zsc6irw2bqbha")))

(define-public crate-crypto-tests-0.5.4 (c (n "crypto-tests") (v "0.5.4") (d (list (d (n "block-cipher-trait") (r "^0.4.0-gamma") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "0r7l5j7zlsm1125fqqlv7rlgqrg3ypja1mjrbyrb899m72ab0c4q")))

(define-public crate-crypto-tests-0.5.5 (c (n "crypto-tests") (v "0.5.5") (d (list (d (n "block-cipher-trait") (r "^0.4") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.4") (d #t) (k 0)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "08yrh40a9ll4k29ppizg2yjf96i6s3i9pbkhxp60y8arar93134v")))

