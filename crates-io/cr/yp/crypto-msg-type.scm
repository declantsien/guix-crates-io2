(define-module (crates-io cr yp crypto-msg-type) #:use-module (crates-io))

(define-public crate-crypto-msg-type-1.0.0 (c (n "crypto-msg-type") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "10cb3nc59x3rqffzwa9541gb976agz40wc4xk6vqs6ihw9h58w29")))

(define-public crate-crypto-msg-type-1.0.1 (c (n "crypto-msg-type") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "0ddgxpbn97fg6h428alvd891mg7mg0il5hiazh6990b8bmikk3cq")))

(define-public crate-crypto-msg-type-1.0.2 (c (n "crypto-msg-type") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1h1cxyj43wd6c074q33sv395vr07p0wm0jzxbwlxm66rv6hhvgx4")))

(define-public crate-crypto-msg-type-1.0.3 (c (n "crypto-msg-type") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "08fc1pm0zccrzjjij7xgc667xilns7ba7qfp52mmbah41cr8v29l")))

(define-public crate-crypto-msg-type-1.0.4 (c (n "crypto-msg-type") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1dlml7z51w82hpgr3bd7vs495r58zxiic0sglvpz2w0ljnsdlr2d")))

(define-public crate-crypto-msg-type-1.0.5 (c (n "crypto-msg-type") (v "1.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "053czg1i1w736i5dzvprs3rpqcmfh9zwqb378z9p4s4wsnlg1d9n")))

(define-public crate-crypto-msg-type-1.0.6 (c (n "crypto-msg-type") (v "1.0.6") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1bnxh41gzdhmr1wskahv13am33ki3amgqx9xxn5mm6s3k6p2b8kj")))

(define-public crate-crypto-msg-type-1.0.7 (c (n "crypto-msg-type") (v "1.0.7") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "07y2y5fdv1gm9dg1v1z51s1lcn5nbqhibnzq9aqlli5z7gyf199p")))

(define-public crate-crypto-msg-type-1.0.8 (c (n "crypto-msg-type") (v "1.0.8") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1xciakq2yqb3x21909q0d4i7dckrx1vq5vpasydwldahy9r7jhf2")))

(define-public crate-crypto-msg-type-1.0.9 (c (n "crypto-msg-type") (v "1.0.9") (d (list (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0y24sbpm4hcjhncpa1qfry9jc41myflp6p54gmwzvsk86ga6si6w")))

(define-public crate-crypto-msg-type-1.0.10 (c (n "crypto-msg-type") (v "1.0.10") (d (list (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "054cf978xvm02b7574vwzhq8d27m4jgqa4s0a8yfr98v3qf2w6hd")))

(define-public crate-crypto-msg-type-1.0.11 (c (n "crypto-msg-type") (v "1.0.11") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1nxjza38nik3d225ndnppl986fszv9fbrq2w7q5d7rl7ws8ghlx7")))

(define-public crate-crypto-msg-type-1.0.12 (c (n "crypto-msg-type") (v "1.0.12") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "1njwnr6bk9zf7rbxsvxjsp1xxyk2p9czx2a30hal5j6j459wv98l")))

