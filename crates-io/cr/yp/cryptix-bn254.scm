(define-module (crates-io cr yp cryptix-bn254) #:use-module (crates-io))

(define-public crate-cryptix-bn254-0.1.0 (c (n "cryptix-bn254") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.4.0") (d #t) (k 2)) (d (n "cryptix-bigint") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptix-ecc") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptix-field") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptix-pairing") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (k 2)) (d (n "rand_chacha") (r "^0.3.1") (k 2)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)))) (h "1a45mh01q2qz35g12vf8f2vwyg1w6jx3x3dzmvy3hm8zdqb68p2i") (f (quote (("std")))) (s 2) (e (quote (("rand" "cryptix-field/rand" "dep:rand_core"))))))

