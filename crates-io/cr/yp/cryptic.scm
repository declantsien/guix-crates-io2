(define-module (crates-io cr yp cryptic) #:use-module (crates-io))

(define-public crate-cryptic-0.1.0 (c (n "cryptic") (v "0.1.0") (d (list (d (n "clap") (r "^2.25.0") (f (quote ("yaml" "suggestions" "color"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "197akrr40xasq6bknn0pay02zv3cl0vybl4xr3dqpp4azrqd9jxp") (y #t)))

(define-public crate-cryptic-0.1.1 (c (n "cryptic") (v "0.1.1") (d (list (d (n "clap") (r "^2.25.0") (f (quote ("yaml" "suggestions" "color"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)) (d (n "rpassword") (r "^0.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "1zj3b6xbnxjfyzvd254ad2jv8yfzl6nd5f9ps3csfwar0xsv2xhr")))

