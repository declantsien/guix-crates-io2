(define-module (crates-io cr yp crypto_vault) #:use-module (crates-io))

(define-public crate-crypto_vault-0.0.1 (c (n "crypto_vault") (v "0.0.1") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0d2nqspa1ngp2mw64l4kg0ydk3r1gd9wx73x8k424yczk0nlznqz")))

(define-public crate-crypto_vault-0.0.2 (c (n "crypto_vault") (v "0.0.2") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1lw26a10ghwirgmcxrhqh8wgb43pz3vnd0fn917j0a8hravjs54y")))

(define-public crate-crypto_vault-0.0.3 (c (n "crypto_vault") (v "0.0.3") (d (list (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1g9ighd7kyl10ls419vwxznn20rall006y28r0hhnk553pssb268")))

