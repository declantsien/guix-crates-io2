(define-module (crates-io cr yp crypto-util) #:use-module (crates-io))

(define-public crate-crypto-util-0.0.1 (c (n "crypto-util") (v "0.0.1") (d (list (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "futures") (r "0.1.*") (d #t) (k 0)))) (h "0v50f9wyx4awsi0y2pl1vdynz01y53bscmx0yixqnw9scvxc4is1")))

(define-public crate-crypto-util-0.0.2 (c (n "crypto-util") (v "0.0.2") (d (list (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)))) (h "11wpvn0wcvwi19yn3fxr989zij6710b6v3d60n85q4jwajl0j1z9")))

(define-public crate-crypto-util-0.1.0 (c (n "crypto-util") (v "0.1.0") (d (list (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*.*") (d #t) (k 0)))) (h "12h29y45x7pdfrglq82x5xrif443ngwb1b7shvmwz1ff2zhpp5jc")))

