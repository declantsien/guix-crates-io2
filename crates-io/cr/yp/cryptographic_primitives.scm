(define-module (crates-io cr yp cryptographic_primitives) #:use-module (crates-io))

(define-public crate-cryptographic_primitives-0.1.0 (c (n "cryptographic_primitives") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1l19136l2j43hrs1g61larcdl18bl8az06ryayina5rlzzaab2db")))

