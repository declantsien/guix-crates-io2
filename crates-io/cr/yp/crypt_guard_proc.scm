(define-module (crates-io cr yp crypt_guard_proc) #:use-module (crates-io))

(define-public crate-crypt_guard_proc-0.1.0 (c (n "crypt_guard_proc") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "0yilf0s5slzs6vxfvwr7xyyqsvgnrcnywd4k1d8f3v2gy12wfn8g")))

