(define-module (crates-io cr yp cryptify) #:use-module (crates-io))

(define-public crate-cryptify-1.0.0 (c (n "cryptify") (v "1.0.0") (d (list (d (n "labyrinth_macros") (r "^2.0.0") (d #t) (k 0)))) (h "1vpq8c2bsaiss1hiv0f9klw29q9k6fip6vr4jmmap2nr6xygfxwi") (y #t)))

(define-public crate-cryptify-2.0.0 (c (n "cryptify") (v "2.0.0") (d (list (d (n "labyrinth_macros") (r "^2.0.0") (d #t) (k 0)))) (h "0pgq2v28cqgafa44nk15bb7rf7x256p81w2jqdx7bqzj0g4yyqng") (y #t)))

(define-public crate-cryptify-3.0.0 (c (n "cryptify") (v "3.0.0") (d (list (d (n "labyrinth_macros") (r "^3.0.0") (d #t) (k 0)))) (h "1ffn4hqc8rqh5nrqdcxq4y3jfw35wfcly4bsidxi9071bxxw49x0")))

(define-public crate-cryptify-3.0.4 (c (n "cryptify") (v "3.0.4") (d (list (d (n "labyrinth_macros") (r "^2.0.0") (d #t) (k 0)))) (h "0mkc6kdz30djv5f1anbqpi5hjx69l9amg43898jvbdbwbsm2ar3v")))

(define-public crate-cryptify-3.0.6 (c (n "cryptify") (v "3.0.6") (d (list (d (n "labyrinth_macros") (r "^2.0.0") (d #t) (k 0)))) (h "0brz5pvnximhx1zkfnmfbhmj2brlvn2z0nqfwpkd4nkq62wll7jm")))

(define-public crate-cryptify-3.0.8 (c (n "cryptify") (v "3.0.8") (d (list (d (n "labyrinth_macros") (r "^2.0.0") (d #t) (k 0)))) (h "0szjyq4mi8wnps6bilhfpv10fxgxwna06b2lsld4bdzcrjjpay2x")))

(define-public crate-cryptify-3.1.0 (c (n "cryptify") (v "3.1.0") (d (list (d (n "labyrinth_macros") (r "^2.0.0") (d #t) (k 0)))) (h "16ngh0ilk1r864vi16lz4n0b8jgyjlrj7fmkhfqdzr693rdzjgkn")))

(define-public crate-cryptify-3.1.1 (c (n "cryptify") (v "3.1.1") (d (list (d (n "labyrinth_macros") (r "^2.0.0") (d #t) (k 0)))) (h "163kkf5p8p87zfxdcq7x18gj0shrm0jggsbfxkxvrw06nlxnbxyh")))

