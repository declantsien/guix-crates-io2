(define-module (crates-io cr yp crypti) #:use-module (crates-io))

(define-public crate-crypti-0.1.0 (c (n "crypti") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h17v862ni1cpdj24r302l7r8h32nvy0qxcld0knyl3rscic8kn4")))

(define-public crate-crypti-0.2.0 (c (n "crypti") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bq93rjjfx2v0z7df895xmsxv7f5n40ccyi3c75jq9hv9j45mv2d")))

(define-public crate-crypti-0.2.1 (c (n "crypti") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "1si221l3z3x4vn8n7yj3zl456gva4zivl49isfvcfnx2q8f37ykj")))

(define-public crate-crypti-0.2.2 (c (n "crypti") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "0zjq8yk2hbihfl2191kjlr2y9qgq19c5m2lklaffrhb0pxj0az7i")))

