(define-module (crates-io cr yp cryptonote-raw-crypto) #:use-module (crates-io))

(define-public crate-cryptonote-raw-crypto-0.1.0 (c (n "cryptonote-raw-crypto") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0xi35xwkdi7fh7432472dj8q2xyssg58162n9nic60r0nasvrf1q")))

(define-public crate-cryptonote-raw-crypto-0.1.1 (c (n "cryptonote-raw-crypto") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "152imfjxwskj29qj2ysxk1rwhdqbclj4nnr21vrg86951wy25pv0")))

(define-public crate-cryptonote-raw-crypto-0.1.2 (c (n "cryptonote-raw-crypto") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1c5mzkjzalphykyfcy7qbl66h16ykswqnvhhq6b2ydysqw3bfp4i")))

(define-public crate-cryptonote-raw-crypto-0.1.3 (c (n "cryptonote-raw-crypto") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0bzz5ccd7h02sn7dphmqg37cgbjrfsp0kvhsqx1mw4g3q0svqw93")))

(define-public crate-cryptonote-raw-crypto-0.1.4 (c (n "cryptonote-raw-crypto") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "00d9kh9kq74z99xv01kkdyn2j5040izi0sj9plcw5mhzkr0pq3m8")))

(define-public crate-cryptonote-raw-crypto-0.1.5 (c (n "cryptonote-raw-crypto") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0rk3qwqrbsdyahn5k4fba37a6n8r2mlnarjhiy9s7vc3f3pq6r71")))

(define-public crate-cryptonote-raw-crypto-0.2.5 (c (n "cryptonote-raw-crypto") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "07lczb6ffzwi8v8b450q7gc72x61c4pbk7bldq93gnskf7vhh4pf")))

(define-public crate-cryptonote-raw-crypto-0.4.0 (c (n "cryptonote-raw-crypto") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0n39hmiwdqmj74mlnb56g27b44brqqcgmfqd8mb9wzp26q7hxxai")))

(define-public crate-cryptonote-raw-crypto-0.5.0 (c (n "cryptonote-raw-crypto") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "18db7ww515a4b39prcgbk1zvk9wfnbyqmbk1qwlibk9bfljnnfpw")))

(define-public crate-cryptonote-raw-crypto-0.5.1 (c (n "cryptonote-raw-crypto") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0ik5zwsh9z2cswip86l61wpkhsprag635w511nyljp57zn9h8gwz")))

(define-public crate-cryptonote-raw-crypto-0.5.2 (c (n "cryptonote-raw-crypto") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0km42jpma48wb3gxyama07wqjqw78h28f5sgmx6fchf7sdvhrmhs")))

(define-public crate-cryptonote-raw-crypto-0.5.3 (c (n "cryptonote-raw-crypto") (v "0.5.3") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0m5nw3q4i9vwyvl5hnqzx6k8fdaksxyd02i15s5h8k8mzbw7947i")))

(define-public crate-cryptonote-raw-crypto-0.5.4 (c (n "cryptonote-raw-crypto") (v "0.5.4") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0w500q1xh1igmcmnzh0r43946qmdg8jq5194wp4pv8p4hd6rnk6s")))

(define-public crate-cryptonote-raw-crypto-0.5.5 (c (n "cryptonote-raw-crypto") (v "0.5.5") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "03acbkv4qzig2q07zpcd0nigxz58ww85dr79lq7bzq8jrjvw4b4p")))

(define-public crate-cryptonote-raw-crypto-0.5.6 (c (n "cryptonote-raw-crypto") (v "0.5.6") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0323fcpl684hcvy850012b40a59am40ary94spx5yblqp17jz7h0")))

(define-public crate-cryptonote-raw-crypto-0.5.7 (c (n "cryptonote-raw-crypto") (v "0.5.7") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.26") (d #t) (k 1)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1fnh8h26jlcrzg33rmn1ffygnxzs1n38cdylj2m9n0i14gngmg3h")))

