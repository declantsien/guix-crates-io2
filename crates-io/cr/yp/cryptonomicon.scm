(define-module (crates-io cr yp cryptonomicon) #:use-module (crates-io))

(define-public crate-cryptonomicon-0.1.0 (c (n "cryptonomicon") (v "0.1.0") (d (list (d (n "ed25519-dalek") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)))) (h "17mrnz9fhi02lb3jrby79mz6wgacjzkr1g6qhhhclkn65s6azpkh") (f (quote (("x509_v3" "x509") ("x509" "pki") ("pki" "asymmetric") ("elliptic_curve_25519" "elliptic_curve" "ed25519-dalek") ("elliptic_curve" "asymmetric") ("default" "elliptic_curve_25519" "x509_v3") ("asymmetric"))))))

