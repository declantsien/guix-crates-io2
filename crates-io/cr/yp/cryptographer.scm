(define-module (crates-io cr yp cryptographer) #:use-module (crates-io))

(define-public crate-cryptographer-0.1.0 (c (n "cryptographer") (v "0.1.0") (h "0vs6mr0xwy6bvh5i5xjp9k9mqy73rz3r5gzrv2wqyn1349qibjg6")))

(define-public crate-cryptographer-0.1.1 (c (n "cryptographer") (v "0.1.1") (h "1x1m97151i51kvvjacpsm7df0i8ynfp92prr6zhhzg6jv0jv6vdw")))

