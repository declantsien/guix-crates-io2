(define-module (crates-io cr yp crypto-ext) #:use-module (crates-io))

(define-public crate-crypto-ext-0.0.1 (c (n "crypto-ext") (v "0.0.1") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "p256") (r "^0.11.1") (f (quote ("ecdsa"))) (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)))) (h "1gpvdqw4n5h7292yjh7xlf8gfpf8sazmkzhzdb4z860rixf9hi3g") (r "1.64")))

(define-public crate-crypto-ext-0.0.2 (c (n "crypto-ext") (v "0.0.2") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "p256") (r "^0.11.1") (f (quote ("ecdsa"))) (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)))) (h "08rls6dnwb3ah65wnlx55ibl9i2dcm38lqn7vm5s950y0mmbryar") (r "1.65")))

(define-public crate-crypto-ext-9.0.0 (c (n "crypto-ext") (v "9.0.0") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "p256") (r "^0.11.1") (f (quote ("ecdsa"))) (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)))) (h "0sdy6glgv1ff7mr2cd3f1v48hgadjcpkrbbjbd584prgzn13s7z0") (r "1.66")))

