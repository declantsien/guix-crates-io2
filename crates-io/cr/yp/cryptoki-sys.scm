(define-module (crates-io cr yp cryptoki-sys) #:use-module (crates-io))

(define-public crate-cryptoki-sys-0.1.0 (c (n "cryptoki-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "05p9ss0zkhg6ny8ggly2hwbr6zqhvlk10hb9mzrpwk3yr1kca4p7") (f (quote (("generate-bindings" "bindgen"))))))

(define-public crate-cryptoki-sys-0.1.1 (c (n "cryptoki-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "1319nm7cw5v7lhj1lnlh59hhsq6xr4anrkgd11filk57avk7ps7s") (f (quote (("generate-bindings" "bindgen"))))))

(define-public crate-cryptoki-sys-0.1.2 (c (n "cryptoki-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "0ljzl7hc86jlpkzncd6qjg2nk4qr073farwp42hcx64fpxnb89m4") (f (quote (("generate-bindings" "bindgen"))))))

(define-public crate-cryptoki-sys-0.1.3 (c (n "cryptoki-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "1nhxnyxsqhi1bxpbj98fpz1yaa1qhxcy8myf9fsha5rvklb21hmf") (f (quote (("generate-bindings" "bindgen"))))))

(define-public crate-cryptoki-sys-0.1.4 (c (n "cryptoki-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "1ljnd7vk9wadgkrkx8yajfd9wxn2km4wd4i69yhzk7960jxraj0y") (f (quote (("generate-bindings" "bindgen"))))))

(define-public crate-cryptoki-sys-0.1.5 (c (n "cryptoki-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "1bhf2qd13wlg3k35s4jv1k3amiwba7cn783554fy3xybi4c264hx") (f (quote (("generate-bindings" "bindgen")))) (y #t)))

(define-public crate-cryptoki-sys-0.1.6 (c (n "cryptoki-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.63.0") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.0") (d #t) (k 1)))) (h "158h480g6nh767gb11r4bglphxlvd6hsxnvndvnq92lj14z99jab") (f (quote (("generate-bindings" "bindgen"))))))

(define-public crate-cryptoki-sys-0.1.7 (c (n "cryptoki-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)) (d (n "libloading") (r "^0.7.0") (d #t) (k 0)))) (h "1m8pv589z3r9h2zj27ny446hy4s7d5wr4kzdxmlaqik449g8x5vs") (f (quote (("generate-bindings" "bindgen")))) (r "1.66.0")))

