(define-module (crates-io cr yp crypto2) #:use-module (crates-io))

(define-public crate-crypto2-0.1.0 (c (n "crypto2") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "04169ij08km7yi91lf9svsm1hglxiwkyw023cnp7ay16fc9wpyq1") (f (quote (("std") ("openssh") ("nightly") ("default" "std" "openssh")))) (y #t)))

(define-public crate-crypto2-0.1.1 (c (n "crypto2") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1cay7zkw9wbwihbzy8h8r2a9lkwbgrx8lv9n28kwqbmyf5d1k2jn") (f (quote (("std") ("openssh") ("nightly") ("default" "std" "openssh")))) (y #t)))

(define-public crate-crypto2-0.1.2 (c (n "crypto2") (v "0.1.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1977cbxpcl11gdrfnmxyx0q6h2kx5byixz0lcwb6p9vpkqyp2qd1") (f (quote (("std") ("openssh") ("nightly") ("default" "std" "openssh"))))))

