(define-module (crates-io cr yp crypto_pimitives) #:use-module (crates-io))

(define-public crate-crypto_pimitives-0.1.0 (c (n "crypto_pimitives") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "0spancsqmdck5kn6gzpwzzvz3ppqz6qazgcqc9mcfffzkwk31rdh")))

(define-public crate-crypto_pimitives-0.1.1 (c (n "crypto_pimitives") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "0qz2aygp0gp939nzjpydxxrlr32pp8q47wmzkjrbww488xkjiias")))

