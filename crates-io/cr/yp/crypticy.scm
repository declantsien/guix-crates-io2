(define-module (crates-io cr yp crypticy) #:use-module (crates-io))

(define-public crate-crypticy-0.1.0 (c (n "crypticy") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.7") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "13pz4j77zkpqv84f8wkcy2dj5iqxf1ka5nn5bxrpyvhbfz63bdqm")))

(define-public crate-crypticy-0.1.1 (c (n "crypticy") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.7") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0xx7wrlmg8yhhbyams8q683468wi5d69nwskk28m3sf089kfc040")))

(define-public crate-crypticy-0.2.0 (c (n "crypticy") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.7") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1kf7nhip7h38phzxgcgngss8g9d7bxnrlzbd43wba1jmzxfjdrxh")))

(define-public crate-crypticy-0.2.2 (c (n "crypticy") (v "0.2.2") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "argon2") (r "^0.4.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ib6h59kyqxb0b840582g9b02r33xxcqjc08z1q42vnhd05q5d1r")))

