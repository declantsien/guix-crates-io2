(define-module (crates-io cr yp cryptonote-crypto) #:use-module (crates-io))

(define-public crate-cryptonote-crypto-0.1.0 (c (n "cryptonote-crypto") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "cryptonight") (r "^0.1.0") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0hakqfc8fwi2h1wzscw7m2n6hr6a3ib7ya5yfp626r09pjb3q5w9")))

(define-public crate-cryptonote-crypto-0.2.0 (c (n "cryptonote-crypto") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptonight") (r "^0.1.0") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "12bwdxfkym51h88s629810mj6pn62wlvmjac3q7hbdhgjn4h93n9")))

(define-public crate-cryptonote-crypto-0.2.1 (c (n "cryptonote-crypto") (v "0.2.1") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptonight") (r "^0.1.0") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "0sfmfssy204ldd0ywr2kg0qfyfb9g2dlzb2bjmvr9wlhk9rgjsb4")))

(define-public crate-cryptonote-crypto-0.2.2 (c (n "cryptonote-crypto") (v "0.2.2") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "chacha") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptonight") (r "^0.1.0") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1f1k57xyrjsyqk9h55hg30hbf3i153d3ch2giwpa9px72wlcpddl")))

