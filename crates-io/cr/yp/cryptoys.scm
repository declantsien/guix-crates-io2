(define-module (crates-io cr yp cryptoys) #:use-module (crates-io))

(define-public crate-cryptoys-0.1.0 (c (n "cryptoys") (v "0.1.0") (d (list (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)))) (h "16qb0gy2yirnb9sl0q2n2lp2vc9wxg7mjs5jw83j0vkjq982rcnv")))

(define-public crate-cryptoys-0.1.1 (c (n "cryptoys") (v "0.1.1") (d (list (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)))) (h "167rnxbp5lgw7jkk8mm6xpmy56a3cwdvn6drd5dgd8n8b8p0p8qc")))

(define-public crate-cryptoys-0.2.1 (c (n "cryptoys") (v "0.2.1") (d (list (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)))) (h "1w23lnsa2i0qcla4bxlrzvlds0mlp65h0szmlxaa017ljv75dggi")))

(define-public crate-cryptoys-1.2.1 (c (n "cryptoys") (v "1.2.1") (d (list (d (n "modinverse") (r "^0.1.1") (d #t) (k 0)))) (h "14nz3fwc7n4lmzmdfjk3h3bxrahdnkyxnrgjsxsdf9jcmjc9jsd5")))

