(define-module (crates-io cr yp crypto-rsl) #:use-module (crates-io))

(define-public crate-crypto-rsl-0.1.0 (c (n "crypto-rsl") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0qn4rfj1gl0gv7byha62n55fgc67lzqibgfngqs6mn8pgc1h4mis") (f (quote (("with-bench"))))))

