(define-module (crates-io cr yp cryptol_client) #:use-module (crates-io))

(define-public crate-cryptol_client-0.1.0 (c (n "cryptol_client") (v "0.1.0") (d (list (d (n "jsonrpsee") (r "^0.16.2") (f (quote ("jsonrpsee-core" "jsonrpsee-http-client"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nvfqqbcyr7a1i255v42g5ycri1bqcpslv44sm02qhqq4bfg7kn0")))

(define-public crate-cryptol_client-0.1.1 (c (n "cryptol_client") (v "0.1.1") (d (list (d (n "jsonrpsee") (r "^0.16.2") (f (quote ("jsonrpsee-core" "jsonrpsee-http-client"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z1w7sml3m33128g7k239nvlqvmd2mc4lh1zm00x1s4qxnb9sl5h")))

