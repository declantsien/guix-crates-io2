(define-module (crates-io cr yp cryptix-bigint) #:use-module (crates-io))

(define-public crate-cryptix-bigint-0.1.0 (c (n "cryptix-bigint") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (k 0)))) (h "0mbf7vik1zz1kb263s6zykllqgjrsh67nf2l0pllv4b3pvrj6z96") (s 2) (e (quote (("rand" "dep:rand_core"))))))

