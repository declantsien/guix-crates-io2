(define-module (crates-io cr yp crypt32-sys) #:use-module (crates-io))

(define-public crate-crypt32-sys-0.0.1 (c (n "crypt32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0zi2ag5krlfb1mnr199cp0m3xxgy9mf6kqwbvsm0xrnfsr0nbmkb")))

(define-public crate-crypt32-sys-0.2.0 (c (n "crypt32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1v5l03ghqdllzd444bjaw3ab52g4c58jjr60pzrv5q39w3vqhjg3")))

