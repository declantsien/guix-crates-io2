(define-module (crates-io cr yp crypto-random-map) #:use-module (crates-io))

(define-public crate-crypto-random-map-0.1.0 (c (n "crypto-random-map") (v "0.1.0") (d (list (d (n "convert-base") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1waw291zq1lpmfkkhspg77vvp4maqk3n1wrd8gg2ks82bcqwrscj")))

(define-public crate-crypto-random-map-0.2.0 (c (n "crypto-random-map") (v "0.2.0") (d (list (d (n "convert-base") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0cmg19idbiav5w3nfn5njxkwqsn3kps16jq84ng381ijy0x7j7q6")))

