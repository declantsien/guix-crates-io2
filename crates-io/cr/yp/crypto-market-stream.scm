(define-module (crates-io cr yp crypto-market-stream) #:use-module (crates-io))

(define-public crate-crypto-market-stream-0.0.1 (c (n "crypto-market-stream") (v "0.0.1") (d (list (d (n "bincode") (r "0.9.*") (d #t) (k 0)) (d (n "crypto-currency") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-market") (r "0.0.*") (d #t) (k 0)) (d (n "crypto-market-event") (r "0.0.*") (d #t) (k 0)) (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "multiqueue") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 0)))) (h "048k4wc4hfcmsbxgw11nc8qnn8j7s1x7jcxmhrka5q7glkkyj4rr") (y #t)))

