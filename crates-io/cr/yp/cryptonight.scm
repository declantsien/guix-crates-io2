(define-module (crates-io cr yp cryptonight) #:use-module (crates-io))

(define-public crate-cryptonight-0.1.0 (c (n "cryptonight") (v "0.1.0") (d (list (d (n "blake") (r "^2.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "groestl") (r "^0.8.0") (d #t) (k 0)) (d (n "jh-ffi") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "skein-ffi") (r "^0.5.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "04ajqzy2nmfy08xcn8dniz3mqai18sv4jniwdh7ki94mm3ik30lk")))

