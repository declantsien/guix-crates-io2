(define-module (crates-io cr yp crypto-brainfuck) #:use-module (crates-io))

(define-public crate-crypto-brainfuck-0.1.0 (c (n "crypto-brainfuck") (v "0.1.0") (h "0bsmcccj4mrlq8v934hqwvq1amni8accdvrc8nyh7238kc6iqzwy")))

(define-public crate-crypto-brainfuck-0.2.0 (c (n "crypto-brainfuck") (v "0.2.0") (d (list (d (n "shortest-brainfuck") (r "^0.2") (d #t) (k 0)))) (h "1czhlsnfhp6vc06v9kr92m63z8g66q3xyxkks7g7zn8ff05d5gcq")))

