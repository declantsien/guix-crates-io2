(define-module (crates-io cr yp cryptoy) #:use-module (crates-io))

(define-public crate-cryptoy-0.0.1 (c (n "cryptoy") (v "0.0.1") (h "1av8012k6ip7409krr0ia8pwhx1svbbx013m17q83q7v20frmpzb")))

(define-public crate-cryptoy-0.1.0 (c (n "cryptoy") (v "0.1.0") (d (list (d (n "crypto-bigint") (r "^0.5.5") (f (quote ("serde" "zeroize" "rand"))) (d #t) (k 0)) (d (n "crypto-primes") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "04486yr7gdzq8r2vwrbmn184120wfyz9zqp9lkv4y8l1cif5dsj8") (f (quote (("i-swear-not-to-deploy-this"))))))

(define-public crate-cryptoy-0.1.1 (c (n "cryptoy") (v "0.1.1") (d (list (d (n "crypto-bigint") (r "^0.5.5") (f (quote ("serde" "zeroize" "rand"))) (d #t) (k 0)) (d (n "crypto-primes") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lq86f7sbbmbnnymabwfw32kwi7iqp8m9x8qf5c38kc5lwr6varw")))

(define-public crate-cryptoy-0.1.2 (c (n "cryptoy") (v "0.1.2") (d (list (d (n "crypto-bigint") (r "^0.5.5") (f (quote ("serde" "zeroize" "rand"))) (d #t) (k 0)) (d (n "crypto-primes") (r "^0.5.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "10jyyq0hps9fv0gmvzc22pf1csv001cy0vvqpi0dfwkzg8gk59zq")))

(define-public crate-cryptoy-0.2.0 (c (n "cryptoy") (v "0.2.0") (d (list (d (n "crypto-bigint") (r "^0.5.5") (f (quote ("serde" "zeroize" "rand" "generic-array"))) (d #t) (k 0)) (d (n "crypto-primes") (r "^0.5.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0yvywand1i3dmr88razn112mlhh9iiw1d4qpapdqvvl308n1gipa")))

(define-public crate-cryptoy-0.3.0 (c (n "cryptoy") (v "0.3.0") (d (list (d (n "crypto-bigint") (r "^0.5.5") (f (quote ("serde" "zeroize" "rand" "generic-array"))) (d #t) (k 0)) (d (n "crypto-primes") (r "^0.5.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "096f0is8y681p2c5s19812zzj66xg80hpkw5ivh13qk3ripn5zp0")))

(define-public crate-cryptoy-0.4.0 (c (n "cryptoy") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "num_bigint") (r "^0.8.4") (f (quote ("rand" "prime"))) (d #t) (k 0) (p "num-bigint-dig")) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0m3gqdmb3av3lf3rl9gqlv04f6h1nhk4x54hpdf9parqrgwk7azm") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:clap"))))))

