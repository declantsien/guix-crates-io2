(define-module (crates-io cr yp crypt) #:use-module (crates-io))

(define-public crate-crypt-0.0.2 (c (n "crypt") (v "0.0.2") (h "161njbb0zbbgdy6rwn1ng8w45idbnl9650wpsdxakkrs1xgylcrp")))

(define-public crate-crypt-0.1.0 (c (n "crypt") (v "0.1.0") (h "1yxf3rpiixz36yasz2fwdf3y9h1yxgxh2c7fsxzhv9zn1q56md7v")))

(define-public crate-crypt-0.1.1 (c (n "crypt") (v "0.1.1") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1f4kvb5sxbpbibbpw7q74vpz01w25qb82iqdgcnnxdfzxvf4669h") (y #t)))

(define-public crate-crypt-0.1.2 (c (n "crypt") (v "0.1.2") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r ">=0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j4rrxqr950pj74azixqdhx8zmvxhsa61khairzqb32qz64d5k0y") (y #t)))

(define-public crate-crypt-0.1.3 (c (n "crypt") (v "0.1.3") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r ">=0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1s7z903drfg9h7213jqq45candfhljln9w34yq5fjlbylpdb360l") (y #t)))

(define-public crate-crypt-0.1.4 (c (n "crypt") (v "0.1.4") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r ">=0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "08msdb3488xi3qrnl19qhradys4jhagp2nnljn66fvx8rhiv82g4") (y #t)))

(define-public crate-crypt-0.1.5 (c (n "crypt") (v "0.1.5") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r ">=0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "05v4wk04r0sjw2648dqfmpi4mysvxl5q1ld8za7ry7cvx9cibihq") (y #t)))

(define-public crate-crypt-0.1.6 (c (n "crypt") (v "0.1.6") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r ">=0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z36mrdkz91h8aap9l18sv2zfpj7v6rdfbhrlbqg8hvqbvpjk6ii") (y #t)))

(define-public crate-crypt-0.1.7 (c (n "crypt") (v "0.1.7") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r ">=0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sik1jpszj9fzg9870667z1ij8xn000pmrnf4sq2jmj04b7wbrnq") (y #t)))

