(define-module (crates-io cr yp crypto_api_blake2) #:use-module (crates-io))

(define-public crate-crypto_api_blake2-0.1.2 (c (n "crypto_api_blake2") (v "0.1.2") (d (list (d (n "crypto_api") (r "^0.2.2") (d #t) (k 0)))) (h "03kvpgmlxhcz8jm44bw0bvz6s6ibr9yl06a224sbw2hh5y011jk3") (y #t)))

(define-public crate-crypto_api_blake2-0.1.3 (c (n "crypto_api_blake2") (v "0.1.3") (d (list (d (n "crypto_api") (r "^0.2.2") (d #t) (k 0)))) (h "1i36kgdk8vnn72ijx5kmd9vcvcifm67ypjrzrw4qxr1c5dp8j4c9")))

(define-public crate-crypto_api_blake2-0.2.0 (c (n "crypto_api_blake2") (v "0.2.0") (d (list (d (n "crypto_api") (r "^0.2.2") (d #t) (k 0)))) (h "1lywxr8b8si1ch0kf9lkbwjl4dgx6m3x2d6nqgnabwbcw2xkvmm1")))

