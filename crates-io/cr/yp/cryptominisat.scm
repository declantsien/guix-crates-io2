(define-module (crates-io cr yp cryptominisat) #:use-module (crates-io))

(define-public crate-cryptominisat-0.1.0 (c (n "cryptominisat") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.17") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1bxb4z4wnf558mkjl9sb4abbr45ga9d9ijcy5aq8sasgvq525vfd")))

(define-public crate-cryptominisat-5.0.1 (c (n "cryptominisat") (v "5.0.1") (d (list (d (n "cmake") (r "^0.1.17") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0i9hwy8rvlv0bsbhx4nq9nvqz29lycj0n6ssz13zzbwqkdknvx9k")))

(define-public crate-cryptominisat-5.6.3 (c (n "cryptominisat") (v "5.6.3") (d (list (d (n "cmake") (r "^0.1.17") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0qrw7bxgabbrxzmqr468yq0n3v6biqsgck99gf7ggaxdpmgqh7m9")))

(define-public crate-cryptominisat-5.8.0 (c (n "cryptominisat") (v "5.8.0") (d (list (d (n "cmake") (r "^0.1.17") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "15xrp9k3h6x0404hinrga096gq8nv0aw9whd106cjgnyhja5hjbs") (l "cryptominisat5")))

(define-public crate-cryptominisat-5.8.0-rust1 (c (n "cryptominisat") (v "5.8.0-rust1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0pwsz0xlbii8zxgiz4impyz9psqansjdvk7hpj6x3s9a9x5g212j") (l "cryptominisat5")))

