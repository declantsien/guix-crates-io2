(define-module (crates-io cr yp crypto_vectors) #:use-module (crates-io))

(define-public crate-crypto_vectors-0.1.0 (c (n "crypto_vectors") (v "0.1.0") (h "173kd2nly01vi8nf815z9a1cymcic68g3rvps9641fyfs75ak6h4")))

(define-public crate-crypto_vectors-0.1.1 (c (n "crypto_vectors") (v "0.1.1") (h "0lm7h7zmak8h5fw05fsg8zg8ywsik1k68v9lp3icrd4lkjkhdssi")))

(define-public crate-crypto_vectors-0.1.2 (c (n "crypto_vectors") (v "0.1.2") (h "1f1hcwdizjni80sh5wbdq7i6p4fj75rnybaiq86bskmjspl2bd97")))

(define-public crate-crypto_vectors-0.1.3 (c (n "crypto_vectors") (v "0.1.3") (h "0xvinjfgxdqavl098iilb38xybg017v64w4m6r52r1cl2si96vna")))

(define-public crate-crypto_vectors-0.1.4 (c (n "crypto_vectors") (v "0.1.4") (h "1kdwgr3mciqfr31yyjxhrflsl1cdwpf0idzlca75pd2j948ixa8k")))

