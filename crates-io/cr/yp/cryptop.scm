(define-module (crates-io cr yp cryptop) #:use-module (crates-io))

(define-public crate-cryptop-0.1.0 (c (n "cryptop") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1df5jaldmvs824r107lyq1mv1p1h07pzhwd2ifbgx42yzns988q1")))

(define-public crate-cryptop-0.1.1 (c (n "cryptop") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cbqmi3rkl7k0xiaiwbdcsr6akmah0bmb1125bsw5i1dxawaybdc")))

(define-public crate-cryptop-0.1.2 (c (n "cryptop") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03llwlnskjy5gqzcdafy1k3bbhxrmwsc45mrhd0ql7qq7d7r7pdx")))

(define-public crate-cryptop-0.1.3 (c (n "cryptop") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fk8m6lckikp5hjrc7w9js84j4pjxmkg9cziy28mjdr7jf4ra469")))

(define-public crate-cryptop-0.1.4 (c (n "cryptop") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvndkiksjcyqbjy9c2rp7shsbkchpb3qjhh0752fak4k9cwm1pr")))

(define-public crate-cryptop-0.1.5 (c (n "cryptop") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfi1a3ifpi6kb7jmkq2wyy0p7ilfay6jkq28q70wp2p42p9zih8")))

(define-public crate-cryptop-0.1.6 (c (n "cryptop") (v "0.1.6") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n5qvyfcqycnhkhz405yls9y59kifk7n8j2bdbr36kj4kadd2c5r")))

(define-public crate-cryptop-0.1.7 (c (n "cryptop") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0cxjb48w3asv9pzll22z97228y2lxicf4hr6v2qsvdsrq1d1kggy")))

(define-public crate-cryptop-0.1.8 (c (n "cryptop") (v "0.1.8") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "088k3gfk10q1s5m4ag4mjfp2dfvslvzbyg5h89dw9lgprnwlwdda")))

(define-public crate-cryptop-0.1.9 (c (n "cryptop") (v "0.1.9") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "112lp2qjfvx3472mriir1hq4c78iixsgz6y4r038mgwjk36kdqs8")))

(define-public crate-cryptop-0.1.10 (c (n "cryptop") (v "0.1.10") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0v3jsnigghsf2i9wginq0bm4b45py2xkss3vwljm64ylnncvs37l")))

(define-public crate-cryptop-0.1.11 (c (n "cryptop") (v "0.1.11") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01k3ann4hs16sam4d028bzkwi9y8q5623h6cfpzi11dgpwgdfbyf")))

