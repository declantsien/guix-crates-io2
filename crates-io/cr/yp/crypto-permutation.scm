(define-module (crates-io cr yp crypto-permutation) #:use-module (crates-io))

(define-public crate-crypto-permutation-0.1.0 (c (n "crypto-permutation") (v "0.1.0") (h "1v8r7wmnjkvs3990ml4k2y66rx8p6bcvdaq1g9v9z023xx10g1fq") (f (quote (("std" "alloc") ("io_uint_u64") ("io_uint_u32") ("io_uint_u16") ("io_uint_u128") ("io_le_uint_slice") ("default") ("alloc")))) (r "1.65")))

