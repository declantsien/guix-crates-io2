(define-module (crates-io cr yp cryptonote-config) #:use-module (crates-io))

(define-public crate-cryptonote-config-0.1.0 (c (n "cryptonote-config") (v "0.1.0") (d (list (d (n "cryptonote-basic") (r "^0.1.0") (d #t) (k 0)))) (h "06xcfxkf5fflz4swj9p7c88qizz8kp21cai8gkjghf0ln4mpag0s")))

(define-public crate-cryptonote-config-0.1.1 (c (n "cryptonote-config") (v "0.1.1") (d (list (d (n "cryptonote-basic") (r "^0.1.0") (d #t) (k 0)))) (h "0yijhhs926176syay32rrpglcrb5f546z8g1n771s2mz8ln44k2v")))

