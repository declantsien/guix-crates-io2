(define-module (crates-io cr yp crypto-common) #:use-module (crates-io))

(define-public crate-crypto-common-0.0.0 (c (n "crypto-common") (v "0.0.0") (h "09ygdp4nqpxggxbkpfa9mfgnbzm9xwy0hjv0qgdwhk62akpl7ing")))

(define-public crate-crypto-common-0.1.0-pre (c (n "crypto-common") (v "0.1.0-pre") (d (list (d (n "block-buffer") (r "^0.10.0-pre.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0gd511vrinpkhj4ghjpsxfpa2jm8z2sa6pc3dn6pp1bs4bz609rs") (f (quote (("std") ("core-api" "block-buffer") ("block-padding" "block-buffer/block-padding"))))))

(define-public crate-crypto-common-0.1.0 (c (n "crypto-common") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0pxwzl1krkib2smmi30z8zjqpmvw1xh20k1dzwwxnnkkb7k6jxan") (f (quote (("std"))))))

(define-public crate-crypto-common-0.1.1 (c (n "crypto-common") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1l4q4ync13i056vjc775v0za8qh987da7yvrjj25q909cd9nngb8") (f (quote (("std"))))))

(define-public crate-crypto-common-0.1.2 (c (n "crypto-common") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.14.4") (f (quote ("more_lengths"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "01jflgmzc2ydgrvk62pj2amw6b7bvhd2jvjfshfcxxmkbrlhsq54") (f (quote (("std"))))))

(define-public crate-crypto-common-0.1.3 (c (n "crypto-common") (v "0.1.3") (d (list (d (n "generic-array") (r "^0.14.4") (f (quote ("more_lengths"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "1s1wpm88qlrp079mzh3dlxm9vbqs4ch016yp9pzhcdjygfi2r5ap") (f (quote (("std"))))))

(define-public crate-crypto-common-0.1.4 (c (n "crypto-common") (v "0.1.4") (d (list (d (n "generic-array") (r "^0.14.4") (f (quote ("more_lengths"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "1h3lpgrrj1w4kra9ww641q19aj0l10j3jrmy594qvi5r68nm16ar") (f (quote (("std") ("getrandom" "rand_core/getrandom"))))))

(define-public crate-crypto-common-0.1.5 (c (n "crypto-common") (v "0.1.5") (d (list (d (n "generic-array") (r "^0.14.4") (f (quote ("more_lengths"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "0bx1lpmv05s6pyqhqsma28lwc34ywvazkmizbgj13kjcxv0dikrc") (f (quote (("std") ("getrandom" "rand_core/getrandom"))))))

(define-public crate-crypto-common-0.1.6 (c (n "crypto-common") (v "0.1.6") (d (list (d (n "generic-array") (r "^0.14.4") (f (quote ("more_lengths"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "1cvby95a6xg7kxdz5ln3rl9xh66nz66w46mm3g56ri1z5x815yqv") (f (quote (("std") ("getrandom" "rand_core/getrandom"))))))

(define-public crate-crypto-common-0.2.0-pre (c (n "crypto-common") (v "0.2.0-pre") (d (list (d (n "generic-array") (r "^0.14.6") (f (quote ("more_lengths"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.14") (d #t) (k 0)))) (h "1f7gbfdpmz17p7x196vxj0k0a9snqga58j7qpcn87q00fwzaiakg") (f (quote (("std") ("getrandom" "rand_core/getrandom")))) (r "1.56")))

(define-public crate-crypto-common-0.2.0-pre.1 (c (n "crypto-common") (v "0.2.0-pre.1") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "=0.2.0-pre.6") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "1qbgdwqsrdcpjkfx9j9ndk1v8ns5pl01v4x6glpr8ks02c1h3gs7") (s 2) (e (quote (("std" "getrandom?/std" "rand_core?/std") ("getrandom" "dep:getrandom" "rand_core?/getrandom")))) (r "1.65")))

(define-public crate-crypto-common-0.2.0-pre.2 (c (n "crypto-common") (v "0.2.0-pre.2") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "=0.2.0-pre.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "125fr7ay2zh1hdr13r2jb6mnghkp54m7cmv8aswf6m2xg407jbqh") (s 2) (e (quote (("std" "getrandom?/std" "rand_core?/std") ("getrandom" "dep:getrandom" "rand_core?/getrandom")))) (r "1.65")))

(define-public crate-crypto-common-0.2.0-pre.3 (c (n "crypto-common") (v "0.2.0-pre.3") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "=0.2.0-pre.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "0qm13kyd1i9k8pbnira120pks5bvdfz5wrwjxib85cb4fdlyn5yc") (s 2) (e (quote (("std" "getrandom?/std" "rand_core?/std") ("getrandom" "dep:getrandom" "rand_core?/getrandom")))) (r "1.65")))

(define-public crate-crypto-common-0.2.0-pre.4 (c (n "crypto-common") (v "0.2.0-pre.4") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "^0.2.0-rc.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "1fd6lfw6s9j61izydnfa0p0mc82cnhji4mb9n1016kyl64vlwvl0") (s 2) (e (quote (("std" "getrandom?/std" "rand_core?/std") ("getrandom" "dep:getrandom" "rand_core?/getrandom")))) (r "1.65")))

(define-public crate-crypto-common-0.2.0-pre.5 (c (n "crypto-common") (v "0.2.0-pre.5") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "^0.2.0-rc.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "13n3jgmxa40i1ydsbkfj82hxv94bkn6iwj1a4wqbh82i9z02xamp") (s 2) (e (quote (("std" "getrandom?/std" "rand_core?/std") ("getrandom" "dep:getrandom" "rand_core?/getrandom")))) (r "1.65")))

