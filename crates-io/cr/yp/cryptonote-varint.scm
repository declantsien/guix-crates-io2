(define-module (crates-io cr yp cryptonote-varint) #:use-module (crates-io))

(define-public crate-cryptonote-varint-0.1.0 (c (n "cryptonote-varint") (v "0.1.0") (h "13lsfwaa0s3py3wm658w861hss7p04r8f2janf2xgia6lfqni144")))

(define-public crate-cryptonote-varint-0.1.1 (c (n "cryptonote-varint") (v "0.1.1") (h "1sf9j9krkifydzd4qalqng2060pkxg2pk7fx3sxbpmr98vw88vkr")))

(define-public crate-cryptonote-varint-0.1.2 (c (n "cryptonote-varint") (v "0.1.2") (h "1sjg56blkfawwprhldaklbiym0f8i4k3cx601i0sn22bljvq3g88")))

(define-public crate-cryptonote-varint-0.1.3 (c (n "cryptonote-varint") (v "0.1.3") (h "1m8636dzblawjbzapa9mc587gc6caccm9a05za2jfirznr3hw6hj")))

