(define-module (crates-io cr yp crypto-splash) #:use-module (crates-io))

(define-public crate-crypto-splash-0.1.0 (c (n "crypto-splash") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zxdapa69vjx6xajscffrfkk49r4z8kw8a1blgsipxg9ygqldbs5")))

