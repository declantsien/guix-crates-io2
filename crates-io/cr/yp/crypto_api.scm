(define-module (crates-io cr yp crypto_api) #:use-module (crates-io))

(define-public crate-crypto_api-0.1.12 (c (n "crypto_api") (v "0.1.12") (h "0i5znqilcvy43nicrx0gik7ba36c6f1168xlvg5l9mljwg8gf215")))

(define-public crate-crypto_api-0.1.13 (c (n "crypto_api") (v "0.1.13") (h "0iz3z0wnhx0drzynh2ikk9pnw59m7b5yg8fs41c5fsri11m7m5m3")))

(define-public crate-crypto_api-0.1.14 (c (n "crypto_api") (v "0.1.14") (h "1frcn2akqv2pziv4wlbs6hx47kxb0ml7s6rixkd8q696jv7i8sxl")))

(define-public crate-crypto_api-0.2.0 (c (n "crypto_api") (v "0.2.0") (h "11s9f8cz2xkf7p79kxj40yd8av5v8a5b706ipxmvsfi1kyi86w5i")))

(define-public crate-crypto_api-0.2.1 (c (n "crypto_api") (v "0.2.1") (h "06ghfvbsnd11620ildghv1fjq17zs1c7l0lmr5lhhx5z04ipi53s")))

(define-public crate-crypto_api-0.2.2 (c (n "crypto_api") (v "0.2.2") (h "00miidrgyiz7454gyj8lgkwx8vyyvj51fac5ighrjisswy3mx19g")))

