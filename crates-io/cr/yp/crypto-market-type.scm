(define-module (crates-io cr yp crypto-market-type) #:use-module (crates-io))

(define-public crate-crypto-market-type-0.1.0 (c (n "crypto-market-type") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0b4s594qhbk95h1n2xfpbnglhbmmgfimaq1nh0bpvv0is464cy06")))

(define-public crate-crypto-market-type-0.1.1 (c (n "crypto-market-type") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0jzvfmh595rz1nnsgm323dis777r64l1ndrbmcl7yh8mjdccgpzd")))

(define-public crate-crypto-market-type-0.1.2 (c (n "crypto-market-type") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "1dvycdnni5hbdkwyzqp4lg58kws7xbhpynr7nan1id4g35dhg64m")))

(define-public crate-crypto-market-type-0.1.3 (c (n "crypto-market-type") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "10ax6z3646a7j7iav6jqpmfh96abh8nyi2k2c865a79gnsvb5xxa")))

(define-public crate-crypto-market-type-0.1.4 (c (n "crypto-market-type") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0cb7c8h9pz7102x25yvzvh7cd551vgaillfh0y312wa3wyzfzyyy")))

(define-public crate-crypto-market-type-0.1.5 (c (n "crypto-market-type") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "128z2ajvhg4ianmgwaidqykpf0mdik0kwzr2a3qvpjpww1lgcsmc")))

(define-public crate-crypto-market-type-0.1.6 (c (n "crypto-market-type") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "194qwbhdxdq2f3dnwcp4kzlk8pqpidxxzand87j4jnabhvzafz41")))

(define-public crate-crypto-market-type-0.1.7 (c (n "crypto-market-type") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "00f6as1f2k45xbw4i7kq22pg914csr2af1i5h90wh05c178b0l6y")))

(define-public crate-crypto-market-type-0.1.8 (c (n "crypto-market-type") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "008fdnqm7p44nqw0whd0zwyk42riqg66b5x9a5vpkzbgf6drl0gd")))

(define-public crate-crypto-market-type-1.0.0 (c (n "crypto-market-type") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0v9simq39163wj9zw1zycj4ggp5br2xlxax8g9r54bqfqqdql3f3")))

(define-public crate-crypto-market-type-1.0.1 (c (n "crypto-market-type") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0c5zb5wdavhkbibd9597z8av8x0iifdscinjgilmmvn2xx9vki46")))

(define-public crate-crypto-market-type-1.0.2 (c (n "crypto-market-type") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0im0p4cagv07z9p83q0ac1fl7njrbf7w08lmygm8y6h1kdmlsyd5")))

(define-public crate-crypto-market-type-1.0.3 (c (n "crypto-market-type") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0658qpx62nzj8hlq079axrd1c3fpjl7p33habq95wma8ym2krb0z")))

(define-public crate-crypto-market-type-1.0.4 (c (n "crypto-market-type") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "02y3713570xzy0z7rjgq1f5z6w4c5p9gkhmc17wq3k939lisj382")))

(define-public crate-crypto-market-type-1.0.5 (c (n "crypto-market-type") (v "1.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "03yp8590k28ykfyr99qg5v79c7qa9ghpfalwc26k8rl55y28h77x")))

(define-public crate-crypto-market-type-1.0.6 (c (n "crypto-market-type") (v "1.0.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1lf7h8p5p3c37yi3z3zzq1fsxcv3s9dqppqyx0d73yn07yp75krq")))

(define-public crate-crypto-market-type-1.0.7 (c (n "crypto-market-type") (v "1.0.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1sqgs56hpwzdny0pxlbalr2pnf8c3l5m5kydpqvfdsfwxprwigw8")))

(define-public crate-crypto-market-type-1.0.8 (c (n "crypto-market-type") (v "1.0.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1cm08ixa2rrq8sdp8abvdrm7kj0i1c4hsf5lyq27m1zb2jk8yy2l")))

(define-public crate-crypto-market-type-1.0.9 (c (n "crypto-market-type") (v "1.0.9") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1v4rgwxgh7ym80dyani3ww7dqx08pvsx6dkxk9kih7is4sa02z65")))

(define-public crate-crypto-market-type-1.1.0 (c (n "crypto-market-type") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "021v34gxd1srryj7ssk5wsahcdhkhqnyn8jkkawgblxyxwvbb79c")))

(define-public crate-crypto-market-type-1.1.1 (c (n "crypto-market-type") (v "1.1.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "1zy68zycmsjxgl3gafpj055m8lw9c7lx8mw2683mca0fn5bzafwz")))

(define-public crate-crypto-market-type-1.1.2 (c (n "crypto-market-type") (v "1.1.2") (d (list (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.2") (d #t) (k 0)))) (h "1xa684s5z1d8miwklxsd6z3xxm435ic3b1p6d5gy35nc3n9hbar9")))

(define-public crate-crypto-market-type-1.1.3 (c (n "crypto-market-type") (v "1.1.3") (d (list (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1vmhdf41y93z86j7xsnkwjn5g51a6niajl7d6jh0lxdmvbvq10dm")))

(define-public crate-crypto-market-type-1.1.4 (c (n "crypto-market-type") (v "1.1.4") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "05qrwhbw63aqrfw2w45518nsnl9aqdl3vr0jx8b7g62xi8h5n3nq")))

(define-public crate-crypto-market-type-1.1.5 (c (n "crypto-market-type") (v "1.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1hggv3hy7xz6akq388iqma8rjyldp0zi4ki2hxmk3bxrcz71nwds")))

(define-public crate-crypto-market-type-1.1.6 (c (n "crypto-market-type") (v "1.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0z5j5ai9aaiv1nasaw8xsyvijl858cs931nnrb20wnrai081ih4f")))

