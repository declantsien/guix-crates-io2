(define-module (crates-io cr yp crypto-rust) #:use-module (crates-io))

(define-public crate-crypto-rust-0.1.0 (c (n "crypto-rust") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.135") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09dcg2n3csn9dgj02nsyf8gpsc0rq1jmrrfgk5xr5rarazyc8frp")))

