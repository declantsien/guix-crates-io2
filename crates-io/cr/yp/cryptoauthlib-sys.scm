(define-module (crates-io cr yp cryptoauthlib-sys) #:use-module (crates-io))

(define-public crate-cryptoauthlib-sys-0.1.0 (c (n "cryptoauthlib-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)))) (h "0bhm9sm7k8qp0zxmi88viiimvjs5z9wy9fkaf0z2bqvrsh20lscm")))

(define-public crate-cryptoauthlib-sys-0.2.0 (c (n "cryptoauthlib-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)))) (h "0yr0vf1jahgazw677z88pp5xcvcqd260nsm0fcdvl4hrs14y5w90")))

(define-public crate-cryptoauthlib-sys-0.2.1 (c (n "cryptoauthlib-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)))) (h "0ic4jndwifzzzsncs792yarp5c7c1r13vnq7z6j61jxg4k4rk77f")))

(define-public crate-cryptoauthlib-sys-0.2.2 (c (n "cryptoauthlib-sys") (v "0.2.2") (d (list (d (n "cmake") (r "= 0.1.45") (d #t) (k 1)))) (h "1r5ndj56cwrsdm4n1zg79sjw7hn6zhbvnmd46c5n1r3fy3a2s8ys")))

