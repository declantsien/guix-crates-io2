(define-module (crates-io cr yp crypto-wasi) #:use-module (crates-io))

(define-public crate-crypto-wasi-0.1.0 (c (n "crypto-wasi") (v "0.1.0") (h "06hc4h1hl8fid1xs1k1xfmvgkx2q8wkrw3hqzzjf876nmfkmf19f")))

(define-public crate-crypto-wasi-0.1.1 (c (n "crypto-wasi") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "der") (r "^0.7.8") (f (quote ("alloc" "oid"))) (d #t) (k 0)) (d (n "pem") (r "^1.1.1") (d #t) (k 0)))) (h "00x3pbs647wy18rij8cpahk9ya82wczbar2mbiq1kcvh3d4ss3xc")))

