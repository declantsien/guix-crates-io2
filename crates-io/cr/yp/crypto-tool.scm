(define-module (crates-io cr yp crypto-tool) #:use-module (crates-io))

(define-public crate-crypto-tool-0.0.1 (c (n "crypto-tool") (v "0.0.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rust_util") (r "^0.2.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "15jr69qwp9r9xixi2d1kbw7br39brmq33z5g6p4isli4kg5pjqy8")))

