(define-module (crates-io cr yp cryptovec) #:use-module (crates-io))

(define-public crate-cryptovec-0.1.0 (c (n "cryptovec") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11zm0q6mxj17r17v8bmj7arhr5fqs8fywfzr4hn9pkwpg1rxcpgm")))

(define-public crate-cryptovec-0.2.0 (c (n "cryptovec") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h9mhz67j37g095wbylrhlxjysizq9rj2rr4b85l4kw8fd3mgdqp")))

(define-public crate-cryptovec-0.3.0 (c (n "cryptovec") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09h1kar68iwf5kypp24zhd00hkcb13m907k2l732smx2yz107ps2")))

(define-public crate-cryptovec-0.3.1 (c (n "cryptovec") (v "0.3.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1mj87n7wrlqkq8gk4z3gnzx3rrlw64av7rfrj3np8j467bhh9j99")))

(define-public crate-cryptovec-0.3.2 (c (n "cryptovec") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02df1zs471xwi01v695r8a38vvg0g9qgwc6rk8471wyxkjam681j")))

(define-public crate-cryptovec-0.3.3 (c (n "cryptovec") (v "0.3.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1qmiraqg2rcmwbdm0073ix1chbdrvj8j70463lzb7hm29y0cr210")))

(define-public crate-cryptovec-0.3.4 (c (n "cryptovec") (v "0.3.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0s07s8fsg43426kywxir2khih6kiyz697sm9ajlj17bvslxi9k1r")))

(define-public crate-cryptovec-0.4.0 (c (n "cryptovec") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0mdq3f0kd879v9dxch2hd1ymn13hg0q5ijspqm283jl5s5sl6xwa")))

(define-public crate-cryptovec-0.4.1 (c (n "cryptovec") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "16qd4ls24pcwbw86sb56y25rzhbiv289f3cm78zizsc7bjjgbxc3")))

(define-public crate-cryptovec-0.4.2 (c (n "cryptovec") (v "0.4.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1cwp7milqqnaanc6njikw9hb0ph295b7p89gy9izscz1jccsbsxn")))

(define-public crate-cryptovec-0.4.3 (c (n "cryptovec") (v "0.4.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0hq3dsc8arxip5wh35z2jkakb81v469f3bdh830qyq6qa2l3knv1")))

(define-public crate-cryptovec-0.4.4 (c (n "cryptovec") (v "0.4.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1rsgrbdrlvqmr6ins5b3l7yz8yqk08ax2543qlxjw85306pad170")))

(define-public crate-cryptovec-0.4.5 (c (n "cryptovec") (v "0.4.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1wczbm4wb2di3a923gpiyxlr5ld5y8qwgphn04agljrcvac3sc3x")))

(define-public crate-cryptovec-0.4.6 (c (n "cryptovec") (v "0.4.6") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1n88dmhfb2dxs48zllq1g1dya76zx4fajw482qy8jj4hgg1da4p4")))

(define-public crate-cryptovec-0.5.0 (c (n "cryptovec") (v "0.5.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)))) (h "1gdl3sjnaakbc03cdz3k3khggh8jshv1iv4nchbimcxpykg98f6j")))

(define-public crate-cryptovec-0.5.1 (c (n "cryptovec") (v "0.5.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)))) (h "19xv2j6dvl0dbkw0sa20nb1p2d8n4irpfzrnxi6np7r05dc6knhi")))

(define-public crate-cryptovec-0.5.2 (c (n "cryptovec") (v "0.5.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)))) (h "1ghvg7rnznkfb841qa7k60xsxvb9x5crl14ypyd01gz330gqnb7r")))

(define-public crate-cryptovec-0.5.3 (c (n "cryptovec") (v "0.5.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)))) (h "0izpjz5ymv7m32w3551fwwja59smjyx3qf1w15ig46gjf6ay5z54")))

(define-public crate-cryptovec-0.5.4 (c (n "cryptovec") (v "0.5.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)))) (h "1krix8kxskrg5l1kwlq7kigzvbw8bw79cvjzzbsqc36fij62f4gn")))

(define-public crate-cryptovec-0.5.5 (c (n "cryptovec") (v "0.5.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)))) (h "1lhixmdqky6sqm19i7jv863m014z384s7s7bwg4q5z8yvf4jm9j1")))

(define-public crate-cryptovec-0.6.0 (c (n "cryptovec") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (k 0)))) (h "1mbsxcm91ndkcb2xcvhq34a1wmnf2ggz1ff1b9dc4kgmrf7srgz4")))

(define-public crate-cryptovec-0.6.1 (c (n "cryptovec") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (k 0)))) (h "1pqb2g1n3sx0d2cjiy06amcr2wlf9izwb4jj68nk5cmvlq9zmiyc")))

