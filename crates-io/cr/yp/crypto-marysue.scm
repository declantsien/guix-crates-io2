(define-module (crates-io cr yp crypto-marysue) #:use-module (crates-io))

(define-public crate-crypto-marysue-0.1.0 (c (n "crypto-marysue") (v "0.1.0") (d (list (d (n "convert-base") (r "^1.0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0qcbyzcvifw20m19vqx7gifwhi4z5z9kq0jv4llz58mzh8sarsqg")))

(define-public crate-crypto-marysue-0.2.0 (c (n "crypto-marysue") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crypto-random-map") (r "^0.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1mn82kfii2ccwk4ax5d4wk4fdyj5x4yga4x25qp652lbnjf40l0z")))

