(define-module (crates-io cr yp cryptsetup) #:use-module (crates-io))

(define-public crate-cryptsetup-0.1.0 (c (n "cryptsetup") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0w7xz1ybrvqx61sdkgwddwcsk84cqpyhcwznbqx5df2h58nn2j5v")))

(define-public crate-cryptsetup-0.2.0 (c (n "cryptsetup") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1m3xsan54c63ify8a4bl1x65d9lw2j2g6h1rfnl51skcc3mmva14")))

