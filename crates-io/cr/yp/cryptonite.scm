(define-module (crates-io cr yp cryptonite) #:use-module (crates-io))

(define-public crate-cryptonite-0.1.0 (c (n "cryptonite") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9.20") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "15j6a5g7ykd1f2g6b8051nnavaq9azfqmdpsdgwkr00sfia75qrg")))

(define-public crate-cryptonite-0.1.1 (c (n "cryptonite") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.7") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9.20") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "1b314ivlhhr1srnm6j0hs45ikckjxvvdzm5cxxjm0dlzfqmh1vba")))

(define-public crate-cryptonite-0.1.2 (c (n "cryptonite") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.7") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9.20") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "0j2csr53z4dsniwdmxfc6zq0qx39xxsmi6kcp1ch5dzqnsjx22ik")))

(define-public crate-cryptonite-0.1.3 (c (n "cryptonite") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.7") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "openssl") (r "^0.9.20") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "073pbpf3ccgyap4w5sz67vpz68y4lsibcbmfcnil2j15h0njixnw")))

