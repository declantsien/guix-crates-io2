(define-module (crates-io cr yp crypt3) #:use-module (crates-io))

(define-public crate-crypt3-0.1.0 (c (n "crypt3") (v "0.1.0") (d (list (d (n "md5") (r "^0.6") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)))) (h "0y9b4ymbxqscxzfq51bg8zdpmv4cyg9kv1i7hk897fp3gwb68vpr")))

