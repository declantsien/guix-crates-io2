(define-module (crates-io cr yp cryptix-blmq) #:use-module (crates-io))

(define-public crate-cryptix-blmq-0.1.0 (c (n "cryptix-blmq") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "cryptix-bigint") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptix-bn254") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptix-ecc") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptix-field") (r "^0.1.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "cryptix-pairing") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (k 2)) (d (n "rand_chacha") (r "^0.3.1") (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1bwl5y3zc2n4nacfdcd66k2svd5xh9gx9grvbgmqcnij71kp2mnl") (f (quote (("std" "cryptix-bn254/std") ("pk"))))))

