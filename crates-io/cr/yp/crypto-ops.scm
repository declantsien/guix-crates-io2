(define-module (crates-io cr yp crypto-ops) #:use-module (crates-io))

(define-public crate-crypto-ops-0.1.0 (c (n "crypto-ops") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1322x1cbd646n9v8ccnmyrk6j5caf696hiviahx29fnnrs0y49i8")))

(define-public crate-crypto-ops-0.1.1 (c (n "crypto-ops") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0vw133dgxyi0h76pn0d7dkl7i5swyq6vlc449d6f9cawgq3anqx8")))

