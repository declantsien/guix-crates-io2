(define-module (crates-io cr yp cryptobox) #:use-module (crates-io))

(define-public crate-cryptobox-0.1.0 (c (n "cryptobox") (v "0.1.0") (d (list (d (n "ma_proper") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1zg4yz89rr95kdzb3h06hblgfbhgcvcwnq3cw413inyvhsdzs5yy") (f (quote (("use-maproper-volatile" "ma_proper/volatile_fallback") ("use-maproper" "ma_proper") ("default"))))))

