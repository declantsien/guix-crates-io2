(define-module (crates-io cr yp cryptoapis) #:use-module (crates-io))

(define-public crate-cryptoapis-1.0.0 (c (n "cryptoapis") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0rm301cnsrhzf35sh7g8vr8mydamv2wix5ww7nz7a2bpq977z869")))

(define-public crate-cryptoapis-1.1.0 (c (n "cryptoapis") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0zqc4bhi82bvznlfljbfimbiam8b9babdn5png05qd1lx4zf0jsf")))

(define-public crate-cryptoapis-1.2.0 (c (n "cryptoapis") (v "1.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1kb46ls3ldjxl2zv9z7na133n76k9lcn9zy0dki6cc9qkvr3fz15")))

(define-public crate-cryptoapis-1.3.0 (c (n "cryptoapis") (v "1.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "10nmvsbl6yr992ci9xsa6d97agiimyr037n0dacirkwff0k2m73v")))

(define-public crate-cryptoapis-1.4.0 (c (n "cryptoapis") (v "1.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "11g9iarwkcmwpb2i5jnfdm66nb00nxz8zfm28xmwgy8rzsvxslbl")))

(define-public crate-cryptoapis-1.5.0 (c (n "cryptoapis") (v "1.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1sgiswbdz5xhz34w9f46q69g19a0gsqk45xs6vqi58dk0idn71r9")))

(define-public crate-cryptoapis-1.6.0 (c (n "cryptoapis") (v "1.6.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qwisg6q8l9588s2njlnqk0nsl3wxfkrl5a1jm3f67afip1h4d3j")))

(define-public crate-cryptoapis-1.7.0 (c (n "cryptoapis") (v "1.7.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "090767lp0zg9965iyj1bbiwcd0wr6jkzhr3r0wd7sx1j06y1626f")))

