(define-module (crates-io cr yp crypto-price-cli) #:use-module (crates-io))

(define-public crate-crypto-price-cli-0.1.0 (c (n "crypto-price-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bivwl6mhw7sxbqg38xg0i7n3p15b36v6jh70rjjv1ifmcilg4jn")))

(define-public crate-crypto-price-cli-0.1.1 (c (n "crypto-price-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02x84znjq7lbnsrxm990f86djcbxp4aq3ik48r6y1r3j9d4pg1bi")))

(define-public crate-crypto-price-cli-0.1.2 (c (n "crypto-price-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16pmz7myk2092k1yarzjnwlw53bi9sz47bjksmd5fqyz53pqdmn4")))

