(define-module (crates-io cr yp cryptolens-yc) #:use-module (crates-io))

(define-public crate-cryptolens-yc-0.1.0 (c (n "cryptolens-yc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.64") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0y0f7dvm7vhk5bhk36gcgg9gqw9bdwpvfnfdb9bjsiy5i7zbp69w")))

(define-public crate-cryptolens-yc-0.2.0 (c (n "cryptolens-yc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.64") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "00awxkriiql4h64fq4cfabqy6qqds7i4212zd8flj2650p8hzby6")))

