(define-module (crates-io cr yp cryptape-sm) #:use-module (crates-io))

(define-public crate-cryptape-sm-0.3.0 (c (n "cryptape-sm") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.1.3") (d #t) (k 0)))) (h "1l3sbdpp8yif8bk7z0f9jrjl8pa6dy8w17rs4z0153nikyxs94xk")))

