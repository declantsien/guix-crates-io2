(define-module (crates-io cr yp crypto-literal-algorithm) #:use-module (crates-io))

(define-public crate-crypto-literal-algorithm-0.1.0 (c (n "crypto-literal-algorithm") (v "0.1.0") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "ofb") (r "^0.1.1") (d #t) (k 0)))) (h "0hz7kjpwhyga7927nc8qv1fz62vs0p9hgi67n8li1qfb93fx2qmr")))

(define-public crate-crypto-literal-algorithm-0.1.1 (c (n "crypto-literal-algorithm") (v "0.1.1") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "ofb") (r "^0.1.1") (d #t) (k 0)))) (h "0q287pvvy88girmspnv10sywyrzr205ps818rx5qfck0mg42pqqj")))

