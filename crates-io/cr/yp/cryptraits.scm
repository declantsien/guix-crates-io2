(define-module (crates-io cr yp cryptraits) #:use-module (crates-io))

(define-public crate-cryptraits-0.1.0 (c (n "cryptraits") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "06rnj7nzhzl1x9gbgrb4adpbq5ksh5qyqk6d371mysahcrway7bl") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.2.0 (c (n "cryptraits") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "11n9hci09gndqd8jip52zpl7lchgjladphf7xf4bppqagy5l46jq") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.3.0 (c (n "cryptraits") (v "0.3.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "07ajakcl2av1dc5i7nj2az60bxyq5s6b8kli5znyr1a55fly599a") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.4.0 (c (n "cryptraits") (v "0.4.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1ll9qrcma5j8pliv36vbnpjq36jn9ij65jv0wsn2x7vmg9yavzyh") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.5.0 (c (n "cryptraits") (v "0.5.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "0k6bjjcxyxkgvzj1nqglpmh04salvh7jnzz4z6miiaj1drn8jlmb") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.6.0 (c (n "cryptraits") (v "0.6.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1bb1jc5ihgnx0dp3s8hf57x9bqc6asg4c6nijh4x6ziqi668y91d") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.6.1 (c (n "cryptraits") (v "0.6.1") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "0fkq7scgyn9ymw8xigs8pjf5wm9vjylyy8bx192x6g2wf581ds0n") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.6.2 (c (n "cryptraits") (v "0.6.2") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "14y3q5g1wbbqkkmqij2hd1x0cjpj7fcm6276l335p2ckrzva07iz") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.7.0 (c (n "cryptraits") (v "0.7.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "11isgwxx0mb6g1am2r3l0wbd1iky8s7jxi3plb85sryw8rx1qhkw") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.7.1 (c (n "cryptraits") (v "0.7.1") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "0kms7ijpnjzl8h1im4rq2zq7qz94rm6apn2y531inj9ss7xzy33i") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.8.0 (c (n "cryptraits") (v "0.8.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "0299976zwlgzj3vjxn9p2wp6nlj86q7ahaaxxbc91chxh9djlphx") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.8.1 (c (n "cryptraits") (v "0.8.1") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "0ca6iwz7bsvcn9vhay4iqpy6zad8spf7ldd68y1vrqg8kl0yv33i") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.9.0 (c (n "cryptraits") (v "0.9.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1ln4ah0pgjsy4p3v898vq69qf1v7kd8893644qqh05mk2hmhf7wn") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.9.1 (c (n "cryptraits") (v "0.9.1") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "088jswvr3a7l9330flkmald8cwrx0w26gr8qav07jc7rnc755x7r") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.10.0 (c (n "cryptraits") (v "0.10.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "12xagbs0l2vl58zh4ha6mxcsnn8mh58ji1l651ixhx66wkbd514n") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.10.1 (c (n "cryptraits") (v "0.10.1") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1i0r5xi9slva0rb3cd1ql3kid3j41p9g9csv0fal6rk2w09rkaap") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.11.0 (c (n "cryptraits") (v "0.11.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1bbmj6gsbb9gh1s3lhxvza8md8nbnwdxq595ar6h8qhdwipf0wlc") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.12.0 (c (n "cryptraits") (v "0.12.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "05gwb7a97pb088ina3ci92b8j1rdhnd2sgizpphsa2z2fgrm0zpx") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.12.1 (c (n "cryptraits") (v "0.12.1") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1fd4k5wsngv3rfy2apijnf8xmn60z8fi0p80qab77qsil5zlsxmn") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.12.2 (c (n "cryptraits") (v "0.12.2") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1dv6agc8grndqlqpiprpcqhjq9c2sy783k6b6f3z075kd2s2ps58") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.12.3 (c (n "cryptraits") (v "0.12.3") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1b2r0dvza2lr48nk3c8ydlm28prd19ilvbfhszs0v9fni5pkjy2v") (f (quote (("std" "rand_core/std") ("default" "std")))) (y #t)))

(define-public crate-cryptraits-0.12.4 (c (n "cryptraits") (v "0.12.4") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "0irii1gjjppvxd1y87zzzxzgypmsz0v6ycr29isia044i2wj5459") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.13.0 (c (n "cryptraits") (v "0.13.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.3") (d #t) (k 0)))) (h "1il9a8jq5asgynzm54hjkq34nwi504lm7pj5vrf67yy6q9qhg93c") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.14.0 (c (n "cryptraits") (v "0.14.0") (d (list (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "1xwcfa2vdr97j5f47jhyw6p5yyx9rcrkbch1liai1sm6qsjnbpcq") (f (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-cryptraits-0.14.1 (c (n "cryptraits") (v "0.14.1") (d (list (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "1386qw4sii6p9gwcdcifd9zkflq5gkh6h024nzz1g4pglj64rk2j") (f (quote (("std" "rand_core/std") ("default" "std"))))))

