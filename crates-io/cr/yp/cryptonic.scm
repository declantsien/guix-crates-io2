(define-module (crates-io cr yp cryptonic) #:use-module (crates-io))

(define-public crate-Cryptonic-0.1.0 (c (n "Cryptonic") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "tfhe") (r "^0.1.7") (f (quote ("boolean" "shortint" "x86_64-unix"))) (d #t) (k 0)))) (h "0aimf7ssq470rwccjvml33rwiw9hzmclj4mj9c6a3lmma9j15s09")))

(define-public crate-Cryptonic-0.1.1 (c (n "Cryptonic") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "tfhe") (r "^0.1.7") (f (quote ("boolean" "shortint" "x86_64-unix"))) (d #t) (k 0)))) (h "05diiq847nndxl845p4xcxh8cgdkzc7hjhjqfqmwwlqj936jkvjy")))

(define-public crate-Cryptonic-0.1.2 (c (n "Cryptonic") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.0") (d #t) (k 0)) (d (n "tfhe") (r "^0.2.1") (f (quote ("boolean" "shortint" "integer" "x86_64-unix"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tfhe") (r "^0.2.1") (f (quote ("boolean" "shortint" "integer" "x86_64"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01h4464b6ky7wigy9lbmdspc7l7nbidm05jf68p3i1j7ki1ad963")))

