(define-module (crates-io cr yp crypto-invert) #:use-module (crates-io))

(define-public crate-crypto-invert-1.0.0 (c (n "crypto-invert") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "14z2c6cwapfp37pgl4phqzjii33fcvlq1xn7f3iiv0x2ww1h3097")))

(define-public crate-crypto-invert-1.0.1 (c (n "crypto-invert") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1h6nr2nh5lamrwlvip90lhkshwdldhwx84v3xn6bli0dgnjim85s")))

