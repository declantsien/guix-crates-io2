(define-module (crates-io cr yp cryptonight-rs) #:use-module (crates-io))

(define-public crate-cryptonight-rs-0.1.0 (c (n "cryptonight-rs") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1yal7x7wvi867l126x72kwncml7b7n48rnm04vmr5vyxhwl1aglj")))

(define-public crate-cryptonight-rs-0.2.0 (c (n "cryptonight-rs") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1hm2njblzp872s4m7rwlqf8ybmfz9l3dmqsq2cglzk48sd9plkr2")))

(define-public crate-cryptonight-rs-0.2.1 (c (n "cryptonight-rs") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0qyw2rvkmfaj5ipphn1baqvrw94cc4acwipb137nxjpipwfn36zd")))

