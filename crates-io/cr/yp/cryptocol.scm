(define-module (crates-io cr yp cryptocol) #:use-module (crates-io))

(define-public crate-cryptocol-0.0.0 (c (n "cryptocol") (v "0.0.0") (h "167d7hf975xf54b29rlacg4w39fn64b8fhbazi5dz1ag2iiyqky1")))

(define-public crate-cryptocol-0.5.0 (c (n "cryptocol") (v "0.5.0") (h "1xs397hwcmc4f39wp1k1g80xhqcscrpmb0isgc7fyb5cjpy5vx0c")))

(define-public crate-cryptocol-0.6.0 (c (n "cryptocol") (v "0.6.0") (h "08m3gmncg71x0cswxgmiprsklhc0snxjn4yawv9ca9b2f4p8kqi0")))

(define-public crate-cryptocol-0.6.2 (c (n "cryptocol") (v "0.6.2") (h "0h4f8894h7dxwn16llq8fdzwm0qxw5b5an8g1y5pmng4ks676n08")))

(define-public crate-cryptocol-0.6.3 (c (n "cryptocol") (v "0.6.3") (h "12hrkscyfq83n2izzd6j1bsxx56ddb2lmwhzi95cy5c7l2nx26q1")))

(define-public crate-cryptocol-0.7.0 (c (n "cryptocol") (v "0.7.0") (h "15mkis6q8vybi3v3vsyx9ln5gfwcyqj3f9k02bzpik5ysjj5dir3")))

(define-public crate-cryptocol-0.7.1 (c (n "cryptocol") (v "0.7.1") (h "1l7gqzx09nqwbkqwgg5f7n4i1zxydl0q9q1273c7qnn1191q0apx")))

(define-public crate-cryptocol-0.7.2 (c (n "cryptocol") (v "0.7.2") (h "0snykpg4md43qzmw7sn9kxxp8sz698bljfkl30sil76i2bpini8l")))

(define-public crate-cryptocol-0.7.3 (c (n "cryptocol") (v "0.7.3") (h "037jkp921nvs4wddj9539dfzfkbyrrm5avymmfh59bh171sm1h7c")))

(define-public crate-cryptocol-0.7.4 (c (n "cryptocol") (v "0.7.4") (h "040r6gbkk0kf6gjpq5hhvhi4f8xf8lsxlwbwc43jb4jwgnsbmcqn")))

(define-public crate-cryptocol-0.7.5 (c (n "cryptocol") (v "0.7.5") (h "148ax08lrsd3lqih9gyasq7536qm41vcdmmnk7j20lh2hhj5lzm1")))

(define-public crate-cryptocol-0.7.6 (c (n "cryptocol") (v "0.7.6") (h "0zjihz8iab58y1x4yagfjar29p6gra6sz2nh4c5w2clbgll88p2q")))

(define-public crate-cryptocol-0.8.0 (c (n "cryptocol") (v "0.8.0") (h "0qr3n4iylgy9f7dvn5p1v09qxi077i7d9c8m93kqnv4s6wxjkzq7")))

(define-public crate-cryptocol-0.8.1 (c (n "cryptocol") (v "0.8.1") (h "0m6234x2s8dcjpj6xgj9hxfqyjk2ijxyvcdlxccr7l8zp4l3rrl4")))

