(define-module (crates-io cr yp cryptocompare) #:use-module (crates-io))

(define-public crate-cryptocompare-0.1.0 (c (n "cryptocompare") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1b9a7ila7rzrvbx4wczp4dahiwi4nvsaavw61vns27fmwvphrd4l")))

