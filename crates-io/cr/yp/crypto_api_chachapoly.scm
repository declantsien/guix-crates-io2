(define-module (crates-io cr yp crypto_api_chachapoly) #:use-module (crates-io))

(define-public crate-crypto_api_chachapoly-0.1.6 (c (n "crypto_api_chachapoly") (v "0.1.6") (d (list (d (n "crypto_api") (r "^0.1.13") (d #t) (k 0)))) (h "0h5xl5zpm3hwr6cwjaqw3xhmbdcpj4ia9wbj2z6fsp5kxzswl6fg")))

(define-public crate-crypto_api_chachapoly-0.1.7 (c (n "crypto_api_chachapoly") (v "0.1.7") (d (list (d (n "crypto_api") (r "^0.1.14") (d #t) (k 0)))) (h "1h9bac35yp12vib0qaxlrpj097wlb9krjyr3sc89fh8sfri6p3p5")))

(define-public crate-crypto_api_chachapoly-0.1.8 (c (n "crypto_api_chachapoly") (v "0.1.8") (d (list (d (n "crypto_api") (r "^0.2.2") (d #t) (k 0)))) (h "1av3m8mqlqnl4m812jjr1yhi9ai9n0zb9fbwzssk226fp9fy77ig")))

(define-public crate-crypto_api_chachapoly-0.2.0 (c (n "crypto_api_chachapoly") (v "0.2.0") (d (list (d (n "crypto_api") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "sodiumoxide") (r "^0.2.2") (d #t) (k 2)))) (h "0k3lyc8hlidqqjzly7f9hs30iyvv1jd4xxysnldpn8cnsawq6jci")))

(define-public crate-crypto_api_chachapoly-0.2.1 (c (n "crypto_api_chachapoly") (v "0.2.1") (d (list (d (n "crypto_api") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0fjr63h84pj9jli5cpgfwfm9252lqx18ibqmlqhs9vabbc7kzc6l") (f (quote (("run_examples" "rand" "sodiumoxide") ("default"))))))

(define-public crate-crypto_api_chachapoly-0.2.2 (c (n "crypto_api_chachapoly") (v "0.2.2") (d (list (d (n "crypto_api") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1wmaqpmcvy8r2n513cyzzy721ps9gh3za7d8vfnp3z88mdyavclm") (f (quote (("run_examples" "rand" "sodiumoxide") ("default"))))))

(define-public crate-crypto_api_chachapoly-0.3.0 (c (n "crypto_api_chachapoly") (v "0.3.0") (d (list (d (n "crypto_api") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)))) (h "1jnj9rsqlcm0zkb4fdq4l8jyb8pqwpw4aaa67wr5vqan45ayqjx9")))

(define-public crate-crypto_api_chachapoly-0.4.0 (c (n "crypto_api_chachapoly") (v "0.4.0") (d (list (d (n "crypto_api") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)))) (h "0zwgkhs6afvhbrv9sy1kdcqqkl7vdzqdalcrn6wzn3zgxi4vz0zm") (y #t)))

(define-public crate-crypto_api_chachapoly-0.4.1 (c (n "crypto_api_chachapoly") (v "0.4.1") (d (list (d (n "crypto_api") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)))) (h "05y9dipndzhy3k0jb3dvp9c428iqi4ckw2p4677qnx0vnhdg718n")))

(define-public crate-crypto_api_chachapoly-0.4.2 (c (n "crypto_api_chachapoly") (v "0.4.2") (d (list (d (n "crypto_api") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)))) (h "0xbpp9h42n1a3p35cwrhm1gdh563wrjyp24dhr4pv3pnrymizz0r")))

(define-public crate-crypto_api_chachapoly-0.4.3 (c (n "crypto_api_chachapoly") (v "0.4.3") (d (list (d (n "crypto_api") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)))) (h "1f7zzva6b4xinvmvcdkzhjxi82yramn09jgr2y53b7ff4shbcc6r")))

(define-public crate-crypto_api_chachapoly-0.5.0 (c (n "crypto_api_chachapoly") (v "0.5.0") (d (list (d (n "crypto_api") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "json") (r "^0.12") (d #t) (k 2)))) (h "029q3ifk5k9hncah2wm3m80rpx49f6113kxxf0dzk0b9b81f2fmn")))

