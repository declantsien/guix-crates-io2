(define-module (crates-io cr yp cryptonote-base58) #:use-module (crates-io))

(define-public crate-cryptonote-base58-0.1.0 (c (n "cryptonote-base58") (v "0.1.0") (d (list (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)))) (h "0ixpxicijihyzshrzwwxv2ijsnm3vq9b3cgxlflf13dh9liwjzcc")))

(define-public crate-cryptonote-base58-0.1.1 (c (n "cryptonote-base58") (v "0.1.1") (d (list (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)))) (h "17h01dx5k4ks8sb4wj06m4h3kbgvgg0qizlxlmscmxzxfhw3pmcz")))

(define-public crate-cryptonote-base58-0.1.2 (c (n "cryptonote-base58") (v "0.1.2") (d (list (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)))) (h "0h8nkfh0028cvaksqg6nm82f40w3hyx4hmzapmdia7vagxzczw5p")))

