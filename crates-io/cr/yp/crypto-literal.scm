(define-module (crates-io cr yp crypto-literal) #:use-module (crates-io))

(define-public crate-crypto-literal-0.1.1 (c (n "crypto-literal") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "crypto-literal-algorithm") (r "^0.1.1") (d #t) (k 0)) (d (n "crypto-literal-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)))) (h "0pyqinklwb91jqqqsk0jz07qqhsnh67shs6jj9gjbl40xr5lwqmb")))

