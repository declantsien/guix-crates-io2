(define-module (crates-io cr ac cracker) #:use-module (crates-io))

(define-public crate-cracker-0.1.0 (c (n "cracker") (v "0.1.0") (d (list (d (n "blake2") (r "^0.5") (d #t) (k 0)) (d (n "jobsteal") (r "^0.5.1") (d #t) (k 0)) (d (n "pwhash") (r "^0.1.2") (d #t) (k 0)))) (h "1gva993gwljfdh9ns73bmpn55xnyds12vkq86k4q6rqyhfhhzxiz")))

