(define-module (crates-io cr an cranelift-preopt) #:use-module (crates-io))

(define-public crate-cranelift-preopt-0.23.0 (c (n "cranelift-preopt") (v "0.23.0") (d (list (d (n "cranelift-codegen") (r "^0.23.0") (k 0)) (d (n "cranelift-entity") (r "^0.23.0") (k 0)))) (h "0ibz3vfgbd6w98q7v9rk588wiagscn4mascqcry0i8lnljq3sk14") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.24.0 (c (n "cranelift-preopt") (v "0.24.0") (d (list (d (n "cranelift-codegen") (r "^0.24.0") (k 0)) (d (n "cranelift-entity") (r "^0.24.0") (k 0)))) (h "0947xmy9jac1ydm9xl9qyayrk0mw1ysqg5ms15yy5sdkag47jy3p") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.25.0 (c (n "cranelift-preopt") (v "0.25.0") (d (list (d (n "cranelift-codegen") (r "^0.25.0") (k 0)) (d (n "cranelift-entity") (r "^0.25.0") (k 0)))) (h "1wzg8333l3vylziv3jrpibyfix627qdy4gm4vsymzrw5q7w011sp") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.26.0 (c (n "cranelift-preopt") (v "0.26.0") (d (list (d (n "cranelift-codegen") (r "^0.26.0") (k 0)) (d (n "cranelift-entity") (r "^0.26.0") (k 0)))) (h "0rf973pcb3x8dxr8h8c63fak3qc5441g36pm3kvsiwr5038sci3a") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.27.0 (c (n "cranelift-preopt") (v "0.27.0") (d (list (d (n "cranelift-codegen") (r "^0.27.0") (k 0)) (d (n "cranelift-entity") (r "^0.27.0") (k 0)))) (h "1c1l7vvi81hib458279zk57c89l30pvkaw02ms6j9fffi79kfya5") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.28.0 (c (n "cranelift-preopt") (v "0.28.0") (d (list (d (n "cranelift-codegen") (r "^0.28.0") (k 0)) (d (n "cranelift-entity") (r "^0.28.0") (k 0)))) (h "129am04s56921w901jbph809r6n4x0y8w7zlxfdllakxj9j86d51") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.29.0 (c (n "cranelift-preopt") (v "0.29.0") (d (list (d (n "cranelift-codegen") (r "^0.29.0") (k 0)) (d (n "cranelift-entity") (r "^0.29.0") (k 0)))) (h "128r5h225fksw8a5x8s6f6qsi76fr0fjqjxql0glyibdbphbgqni") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.30.0 (c (n "cranelift-preopt") (v "0.30.0") (d (list (d (n "cranelift-codegen") (r "^0.30.0") (k 0)) (d (n "cranelift-entity") (r "^0.30.0") (k 0)))) (h "05p8zk9c4hgwqgwqfg1g3f5gj76xy66zlpsc7fck6a07a7asy84s") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.31.0 (c (n "cranelift-preopt") (v "0.31.0") (d (list (d (n "cranelift-codegen") (r "^0.31.0") (k 0)) (d (n "cranelift-entity") (r "^0.31.0") (k 0)))) (h "1bz94sy50vymcqq4c443437gsxp2a783zzjy90kk0x1mhj2lv33f") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.32.0 (c (n "cranelift-preopt") (v "0.32.0") (d (list (d (n "cranelift-codegen") (r "^0.32.0") (k 0)) (d (n "cranelift-entity") (r "^0.32.0") (k 0)))) (h "0iaq6r3qj59rvfawh7c97fxg42av9hswgq6rvvcb5mfv5agp2ycj") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.33.0 (c (n "cranelift-preopt") (v "0.33.0") (d (list (d (n "cranelift-codegen") (r "^0.33.0") (k 0)) (d (n "cranelift-entity") (r "^0.33.0") (k 0)))) (h "1mq8civ8pn7pwdmvzbkzv2b31z634zljh2pp0dxrj2jfnl90rdav") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.34.0 (c (n "cranelift-preopt") (v "0.34.0") (d (list (d (n "cranelift-codegen") (r "^0.34.0") (k 0)) (d (n "cranelift-entity") (r "^0.34.0") (k 0)))) (h "13h8by9sp2370ig2bynz0a8mkh4jwqiwvnm0bawlhayxmr4qwf53") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.35.0 (c (n "cranelift-preopt") (v "0.35.0") (d (list (d (n "cranelift-codegen") (r "^0.35.0") (k 0)) (d (n "cranelift-entity") (r "^0.35.0") (k 0)))) (h "073h8j0vzb4rh50wac583qbic1h637wflxaiq0cdpaxsg02yb0qp") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.36.0 (c (n "cranelift-preopt") (v "0.36.0") (d (list (d (n "cranelift-codegen") (r "^0.36.0") (k 0)) (d (n "cranelift-entity") (r "^0.36.0") (k 0)))) (h "1awzb04rp50wnd898pr0s0ndniq7vis4akayfwxxy3n10ssg2dp5") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.37.0 (c (n "cranelift-preopt") (v "0.37.0") (d (list (d (n "cranelift-codegen") (r "^0.37.0") (k 0)) (d (n "cranelift-entity") (r "^0.37.0") (k 0)))) (h "0ggplyd3z817xxsavbj20asl0i88ixbbx29vki1gr400l84zk6pn") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.38.0 (c (n "cranelift-preopt") (v "0.38.0") (d (list (d (n "cranelift-codegen") (r "^0.38.0") (k 0)) (d (n "cranelift-entity") (r "^0.38.0") (k 0)))) (h "1rfxlnfndpjcxp2yw08ar30rag7gfxdb3xkqd6d5lybmisv6k49h") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.39.0 (c (n "cranelift-preopt") (v "0.39.0") (d (list (d (n "cranelift-codegen") (r "^0.39.0") (k 0)) (d (n "cranelift-entity") (r "^0.39.0") (k 0)))) (h "0x1qi3w8rmy6xw4zngrgjnxvh16k2pm5hw4y4zqvdyxwwhmjjkqh") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.40.0 (c (n "cranelift-preopt") (v "0.40.0") (d (list (d (n "cranelift-codegen") (r "^0.40.0") (k 0)) (d (n "cranelift-entity") (r "^0.40.0") (k 0)))) (h "16l69lmx0is8s3h1h30kjys7s340z3yziwz99mgfayixvlilr21f") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.41.0 (c (n "cranelift-preopt") (v "0.41.0") (d (list (d (n "cranelift-codegen") (r "^0.41.0") (k 0)) (d (n "cranelift-entity") (r "^0.41.0") (k 0)))) (h "1fqzb1jilbnllijh6a4brf7y15aa7gq185kjnw5xrygqqzwslc22") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.42.0 (c (n "cranelift-preopt") (v "0.42.0") (d (list (d (n "cranelift-codegen") (r "^0.42.0") (k 0)) (d (n "cranelift-entity") (r "^0.42.0") (k 0)))) (h "1qh599mqmd9rminny30bi0m8ag3mighb7wjika5mygyc2ky7038n") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.43.0 (c (n "cranelift-preopt") (v "0.43.0") (d (list (d (n "cranelift-codegen") (r "^0.43.0") (k 0)) (d (n "cranelift-entity") (r "^0.43.0") (k 0)))) (h "1kmzl8k1dr1kbi3k3702lxsxj9a9kv0rjkmwrv8cx6xwh86kwmjb") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.43.1 (c (n "cranelift-preopt") (v "0.43.1") (d (list (d (n "cranelift-codegen") (r "^0.43.1") (k 0)) (d (n "cranelift-entity") (r "^0.43.1") (k 0)))) (h "02iv9k4y41fa2b08nhppjaagiqkazpkmfmz45dq574z0s5idspkn") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.44.0 (c (n "cranelift-preopt") (v "0.44.0") (d (list (d (n "cranelift-codegen") (r "^0.44.0") (k 0)) (d (n "cranelift-entity") (r "^0.44.0") (k 0)))) (h "144y914yfigdkn4vz06z4z1pbl8x7wrxgvc1pdikba3lvrmpbkyp") (f (quote (("std" "cranelift-codegen/std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.45.0 (c (n "cranelift-preopt") (v "0.45.0") (d (list (d (n "cranelift-codegen") (r "^0.45.0") (k 0)) (d (n "cranelift-entity") (r "^0.45.0") (d #t) (k 0)))) (h "07ij92zkfhp4c5ns0cwszh2p2i9d1fa000lmgk4z3z1wg3y8hnl3") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.46.0 (c (n "cranelift-preopt") (v "0.46.0") (d (list (d (n "cranelift-codegen") (r "^0.46.0") (k 0)) (d (n "cranelift-entity") (r "^0.46.0") (d #t) (k 0)))) (h "0ag10qsm6i2ba4a4hspb3zis7kvk017a8ky3hg0ak35izv373w4k") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.46.1 (c (n "cranelift-preopt") (v "0.46.1") (d (list (d (n "cranelift-codegen") (r "^0.46.1") (k 0)) (d (n "cranelift-entity") (r "^0.46.1") (d #t) (k 0)))) (h "024j7q8mznmn6fyi2dhm3b93hkp2rszb8h4920cj76279mc0y1pn") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.47.0 (c (n "cranelift-preopt") (v "0.47.0") (d (list (d (n "cranelift-codegen") (r "^0.47.0") (k 0)) (d (n "cranelift-entity") (r "^0.47.0") (d #t) (k 0)))) (h "0kqaxl1cpp7jffx1ck46w78xfz6nnkw4h0kj2aj6y1q08jrj8dvl") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.48.0 (c (n "cranelift-preopt") (v "0.48.0") (d (list (d (n "cranelift-codegen") (r "^0.48.0") (k 0)) (d (n "cranelift-entity") (r "^0.48.0") (d #t) (k 0)))) (h "0jxi6xa4pzp4b8j3c5gfkzx22s4gsfrkli2zndaq6yzkymnl1gpy") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.49.0 (c (n "cranelift-preopt") (v "0.49.0") (d (list (d (n "cranelift-codegen") (r "^0.49.0") (k 0)) (d (n "cranelift-entity") (r "^0.49.0") (d #t) (k 0)))) (h "0qv1qxbqkq31b3l1n579w47hpqp1pjkdsw2xl7f3567ampw5ah79") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.50.0 (c (n "cranelift-preopt") (v "0.50.0") (d (list (d (n "cranelift-codegen") (r "^0.50.0") (k 0)) (d (n "cranelift-entity") (r "^0.50.0") (d #t) (k 0)))) (h "1r0kz981zpckbx6hn7d1v4ga6ijd2g11c40606zzagp3phljj341") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.51.0 (c (n "cranelift-preopt") (v "0.51.0") (d (list (d (n "cranelift-codegen") (r "^0.51.0") (k 0)) (d (n "cranelift-entity") (r "^0.51.0") (d #t) (k 0)))) (h "0cc7yfhfcixiy8cbd31dfb68ac1msalcxa26h8ply5ldsd1ajarl") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.52.0 (c (n "cranelift-preopt") (v "0.52.0") (d (list (d (n "cranelift-codegen") (r "^0.52.0") (k 0)) (d (n "cranelift-entity") (r "^0.52.0") (d #t) (k 0)))) (h "1xrj1ph93syn2wp7ifsi7h9wqyfz3mz0fjp5f6vba31pb44j12vr") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.53.0 (c (n "cranelift-preopt") (v "0.53.0") (d (list (d (n "cranelift-codegen") (r "^0.53.0") (k 0)) (d (n "cranelift-entity") (r "^0.53.0") (d #t) (k 0)))) (h "190vkrgcl8kqbrp4cfznr7ljzndmb6mxcrghln3jxzda8x8k4qrj") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.54.0 (c (n "cranelift-preopt") (v "0.54.0") (d (list (d (n "cranelift-codegen") (r "^0.54.0") (k 0)) (d (n "cranelift-entity") (r "^0.54.0") (d #t) (k 0)))) (h "18zmq7j8b71j612l6m7g8h5mmccvn3mrzdrv617byr9nsbv954xy") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.55.0 (c (n "cranelift-preopt") (v "0.55.0") (d (list (d (n "cranelift-codegen") (r "^0.55.0") (k 0)) (d (n "cranelift-entity") (r "^0.55.0") (d #t) (k 0)))) (h "1dgsz1fvp5wbw2awsxxhl0qlfb3rk04snrnhfm85w4kmi0ialyac") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.56.0 (c (n "cranelift-preopt") (v "0.56.0") (d (list (d (n "cranelift-codegen") (r "^0.56.0") (k 0)) (d (n "cranelift-entity") (r "^0.56.0") (d #t) (k 0)))) (h "0xg8d1082fvv7v36hrw9n3za95cr5j9v10gjdlnj1vj22h78j1b2") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.58.0 (c (n "cranelift-preopt") (v "0.58.0") (d (list (d (n "cranelift-codegen") (r "^0.58.0") (k 0)) (d (n "cranelift-entity") (r "^0.58.0") (d #t) (k 0)))) (h "0ic704qkv7hd2xg64lwg9cl087wpbihs27y882d57fyl93hxjvzy") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.59.0 (c (n "cranelift-preopt") (v "0.59.0") (d (list (d (n "cranelift-codegen") (r "^0.59.0") (k 0)) (d (n "cranelift-entity") (r "^0.59.0") (d #t) (k 0)))) (h "1x5ha7lj9967fr778hqfi35gjj0qklgpsr4z55azbyylfwnwq777") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.60.0 (c (n "cranelift-preopt") (v "0.60.0") (d (list (d (n "cranelift-codegen") (r "^0.60.0") (k 0)) (d (n "cranelift-entity") (r "^0.60.0") (d #t) (k 0)))) (h "0p6jyjvpl8nrj6av7d9gbp1d2a50ski4a03s34inpf8y6l55wgvd") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.61.0 (c (n "cranelift-preopt") (v "0.61.0") (d (list (d (n "cranelift-codegen") (r "^0.61.0") (k 0)) (d (n "cranelift-entity") (r "^0.61.0") (d #t) (k 0)))) (h "15h2vyshbn5s3hix3kp3nfc6f4sm8ni0hs51aziymw3qw0bcb6kr") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.62.0 (c (n "cranelift-preopt") (v "0.62.0") (d (list (d (n "cranelift-codegen") (r "^0.62.0") (k 0)) (d (n "cranelift-entity") (r "^0.62.0") (d #t) (k 0)))) (h "08cnfhs31nn14hdhbggc9b2xknadz2pn7hbifrj12ffbxzz4gn68") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.63.0 (c (n "cranelift-preopt") (v "0.63.0") (d (list (d (n "cranelift-codegen") (r "^0.63.0") (k 0)) (d (n "cranelift-entity") (r "^0.63.0") (d #t) (k 0)))) (h "16da6myjvfyzzk87j37h97jxqswslxq25iv68zn51x408nmf2gmw") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.64.0 (c (n "cranelift-preopt") (v "0.64.0") (d (list (d (n "cranelift-codegen") (r "^0.64.0") (k 0)) (d (n "cranelift-entity") (r "^0.64.0") (d #t) (k 0)))) (h "0nzyqnsgiv5vyig7j169ikwpy1i6wz5hsv1a6nqs71plw04m82j6") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.65.0 (c (n "cranelift-preopt") (v "0.65.0") (d (list (d (n "cranelift-codegen") (r "^0.65.0") (k 0)) (d (n "cranelift-entity") (r "^0.65.0") (d #t) (k 0)))) (h "1mg9jwhbg23m6wqjbb94aicqjm1vs0kgnaj7mj22bw4jq6fszd3r") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.66.0 (c (n "cranelift-preopt") (v "0.66.0") (d (list (d (n "cranelift-codegen") (r "^0.66.0") (k 0)) (d (n "cranelift-entity") (r "^0.66.0") (d #t) (k 0)))) (h "0kpfzx7f2whird60hiqp0d07p8mafc65jv65k77lcqnzm7xjmgdv") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.67.0 (c (n "cranelift-preopt") (v "0.67.0") (d (list (d (n "cranelift-codegen") (r "^0.67.0") (k 0)) (d (n "cranelift-entity") (r "^0.67.0") (d #t) (k 0)))) (h "128isl3wd86xblw4kib77wzzrk9g6j3s3zn8kfvb94pjs9p0gfzr") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.68.0 (c (n "cranelift-preopt") (v "0.68.0") (d (list (d (n "cranelift-codegen") (r "^0.68.0") (k 0)) (d (n "cranelift-entity") (r "^0.68.0") (d #t) (k 0)))) (h "0sy8fcbfl172ppc9xl685kzgdqjknzmmqgwy32w4bsdrj1qd6317") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.69.0 (c (n "cranelift-preopt") (v "0.69.0") (d (list (d (n "cranelift-codegen") (r "^0.69.0") (k 0)) (d (n "cranelift-entity") (r "^0.69.0") (d #t) (k 0)))) (h "0dvm3g0y7zd0kxvp11h9pqy0fw0kqp7945kp8shn699x7ssr5m3i") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.70.0 (c (n "cranelift-preopt") (v "0.70.0") (d (list (d (n "cranelift-codegen") (r "^0.70.0") (k 0)) (d (n "cranelift-entity") (r "^0.70.0") (d #t) (k 0)))) (h "0myfmycnfad3lrzj7iniymxrraf4ygfn79vvqkahsnrbgwpzgc81") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.71.0 (c (n "cranelift-preopt") (v "0.71.0") (d (list (d (n "cranelift-codegen") (r "^0.71.0") (k 0)) (d (n "cranelift-entity") (r "^0.71.0") (d #t) (k 0)))) (h "05xin95dwg6q61aslz42ldw01zr2dbzk2y5p476d879qnickrh0f") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.72.0 (c (n "cranelift-preopt") (v "0.72.0") (d (list (d (n "cranelift-codegen") (r "^0.72.0") (k 0)) (d (n "cranelift-entity") (r "^0.72.0") (d #t) (k 0)))) (h "0cbv5dr6x28hw48csyqa23545vngrpnadkx4y0fz5bqljwp6jkhv") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.73.0 (c (n "cranelift-preopt") (v "0.73.0") (d (list (d (n "cranelift-codegen") (r "^0.73.0") (k 0)) (d (n "cranelift-entity") (r "^0.73.0") (d #t) (k 0)))) (h "0d7ml2fy229azdj7jfnr2c1nxhcby93sz8vsm5435bihxnbk8k6i") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.74.0 (c (n "cranelift-preopt") (v "0.74.0") (d (list (d (n "cranelift-codegen") (r "^0.74.0") (k 0)) (d (n "cranelift-entity") (r "^0.74.0") (d #t) (k 0)))) (h "1siq5f29j0wb7pm0fq8gxc72jdmdanhbip5k4hnpjjbxj3c3rqdc") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.73.1 (c (n "cranelift-preopt") (v "0.73.1") (d (list (d (n "cranelift-codegen") (r "^0.73.1") (k 0)) (d (n "cranelift-entity") (r "^0.73.1") (d #t) (k 0)))) (h "1xvabb2pd96bfppc8rs3dy9xg9yq0vsw9b55a4kjapxgc7455h53") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.75.0 (c (n "cranelift-preopt") (v "0.75.0") (d (list (d (n "cranelift-codegen") (r "^0.75.0") (k 0)) (d (n "cranelift-entity") (r "^0.75.0") (d #t) (k 0)))) (h "01z6wzz90fnsz7bm0s14g3f2d0pazg2f7bl5cg89hb1acjwqq57s") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.76.0 (c (n "cranelift-preopt") (v "0.76.0") (d (list (d (n "cranelift-codegen") (r "^0.76.0") (k 0)) (d (n "cranelift-entity") (r "^0.76.0") (d #t) (k 0)))) (h "0aph5al4sb61bqjpliqjrip4v65qcijwyigvr3k8a78pk28wrf4w") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.77.0 (c (n "cranelift-preopt") (v "0.77.0") (d (list (d (n "cranelift-codegen") (r "^0.77.0") (k 0)) (d (n "cranelift-entity") (r "^0.77.0") (d #t) (k 0)))) (h "09m9gkjcqmja6bbp09gj1ypalkz2q8hwmqzvnfxbl95zwnkcicdx") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.78.0 (c (n "cranelift-preopt") (v "0.78.0") (d (list (d (n "cranelift-codegen") (r "^0.78.0") (k 0)) (d (n "cranelift-entity") (r "^0.78.0") (d #t) (k 0)))) (h "0h165ybfqp9f3d4fhxc3gl5gddvmr9wzx0qllaf0v69gxl9k89pk") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.79.0 (c (n "cranelift-preopt") (v "0.79.0") (d (list (d (n "cranelift-codegen") (r "^0.79.0") (k 0)))) (h "0xcii99zb7xdsqsnj0zd7xy0vvw1lkksvmcd7aq9dzi3p42rn3ns") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.79.1 (c (n "cranelift-preopt") (v "0.79.1") (d (list (d (n "cranelift-codegen") (r "^0.79.1") (k 0)))) (h "0ql62dk8shchbbcaq20x7xhydf1yvvkrwxdc0p97s6symx02gvn3") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.80.0 (c (n "cranelift-preopt") (v "0.80.0") (d (list (d (n "cranelift-codegen") (r "^0.80.0") (k 0)))) (h "102rjzaqjwcmm1x41x4y9nd2kqz60a3fgacic0dnnflzhc9mcy2g") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.81.0 (c (n "cranelift-preopt") (v "0.81.0") (d (list (d (n "cranelift-codegen") (r "^0.81.0") (k 0)))) (h "07mscay9hznf2a43wmyavqmyms78dwkx6fd7d8wz5cark0na4460") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.81.1 (c (n "cranelift-preopt") (v "0.81.1") (d (list (d (n "cranelift-codegen") (r "^0.81.1") (k 0)))) (h "00krrcqgr3sinppbjqcq15ylrmib3wlckyjccmdmjvgbq1m494da") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.80.1 (c (n "cranelift-preopt") (v "0.80.1") (d (list (d (n "cranelift-codegen") (r "^0.80.1") (k 0)))) (h "1898pgafb37v2f9nwrqm4mm8bpba2jh2rxy7czd01x2wi5gi93ix") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.82.0 (c (n "cranelift-preopt") (v "0.82.0") (d (list (d (n "cranelift-codegen") (r "^0.82.0") (k 0)))) (h "0aaglavawakfimh17kvxsrgj9vnqa729bqj4ja6gmpdkvw3cqbhs") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.82.1 (c (n "cranelift-preopt") (v "0.82.1") (d (list (d (n "cranelift-codegen") (r "^0.82.1") (k 0)))) (h "1dsr2cm072xsp360sv4p4qmvll7hhp2a061cphv302s57dixwinz") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.82.2 (c (n "cranelift-preopt") (v "0.82.2") (d (list (d (n "cranelift-codegen") (r "^0.82.2") (k 0)))) (h "0cmizy4w74lslz9vykljh345711g10ifxlac09bsa2gam507djb8") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.81.2 (c (n "cranelift-preopt") (v "0.81.2") (d (list (d (n "cranelift-codegen") (r "^0.81.2") (k 0)))) (h "06m8vl8l3j7k2s9qklyck54ff2756cv53nclbia185w799dg18y2") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.82.3 (c (n "cranelift-preopt") (v "0.82.3") (d (list (d (n "cranelift-codegen") (r "^0.82.3") (k 0)))) (h "0vdfzfdhyqfx091n17bba00m18im2idpcy3wlql45cvsiy2mf82v") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.83.0 (c (n "cranelift-preopt") (v "0.83.0") (d (list (d (n "cranelift-codegen") (r "^0.83.0") (k 0)))) (h "1lh2664ng561k7mlnbziag5wkqw8b63820wdxxin0l26phqx7qfw") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.84.0 (c (n "cranelift-preopt") (v "0.84.0") (d (list (d (n "cranelift-codegen") (r "^0.84.0") (k 0)))) (h "00bjx2x74391njz7d582ys85hsfgfvlq03s92ly1ss4m6a11l9vz") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.85.0 (c (n "cranelift-preopt") (v "0.85.0") (d (list (d (n "cranelift-codegen") (r "^0.85.0") (k 0)))) (h "13mhj3vl2v3ha5d9i89isdaj3lala8hn188gkhg38kwva66kfmh6") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.85.1 (c (n "cranelift-preopt") (v "0.85.1") (d (list (d (n "cranelift-codegen") (r "^0.85.1") (k 0)))) (h "1j8b5zazsgrwnn6czvcblrjh7952955b9rdjvr61mkf6bl3bp2ha") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.86.0 (c (n "cranelift-preopt") (v "0.86.0") (d (list (d (n "cranelift-codegen") (r "^0.86.0") (k 0)))) (h "1c4zvgzql3bvnikk4d5k2nrgapfm8gag07snsjp8wq6y6vk166zn") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.85.2 (c (n "cranelift-preopt") (v "0.85.2") (d (list (d (n "cranelift-codegen") (r "^0.85.2") (k 0)))) (h "1b8jcjjkyls8jv0pl36ji9662hgqz293798piybqaxxbdf4y4w41") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.85.3 (c (n "cranelift-preopt") (v "0.85.3") (d (list (d (n "cranelift-codegen") (r "^0.85.3") (k 0)))) (h "1zm7bhq2rx2pvmjfhs88rv6l88p1vwfp6fhcjxlchqnkp3qdkj2d") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.86.1 (c (n "cranelift-preopt") (v "0.86.1") (d (list (d (n "cranelift-codegen") (r "^0.86.1") (k 0)))) (h "1yn16fvxa3mrgbpc6g09jn0650k6vphgm292r3d3bg5wrwhamnkk") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.87.0 (c (n "cranelift-preopt") (v "0.87.0") (d (list (d (n "cranelift-codegen") (r "^0.87.0") (k 0)))) (h "1g9fk20aw8mvww5gbgr1zmyvsvslz28a4swkzqjan9b9721j24ia") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.87.1 (c (n "cranelift-preopt") (v "0.87.1") (d (list (d (n "cranelift-codegen") (r "^0.87.1") (k 0)))) (h "088v4bx681915y7xfl5yy9s86pyiddfl2nyaaayiwmrawg5v3siy") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.88.0 (c (n "cranelift-preopt") (v "0.88.0") (d (list (d (n "cranelift-codegen") (r "^0.88.0") (k 0)))) (h "1plmlg3qnmii4p1601dmrb23c36rx1j4hcrrnhf6kvmib0jn809y") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.88.1 (c (n "cranelift-preopt") (v "0.88.1") (d (list (d (n "cranelift-codegen") (r "^0.88.1") (k 0)))) (h "0xr4q72c47bqkhah7h00ipsv4qnbbbrw9czg124qj5y6zkqpil6w") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.89.0 (c (n "cranelift-preopt") (v "0.89.0") (d (list (d (n "cranelift-codegen") (r "^0.89.0") (d #t) (k 0)))) (h "1fyrdmqjqa3csvzamkf8vljcnchdyr0xrabi5qfnq650h9ypvamy") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.89.1 (c (n "cranelift-preopt") (v "0.89.1") (d (list (d (n "cranelift-codegen") (r "^0.89.1") (d #t) (k 0)))) (h "0rs15b5fcs2bqwnhbbvikphaa8rqpzj19hhjwc024gcsfsxx006v") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.89.2 (c (n "cranelift-preopt") (v "0.89.2") (d (list (d (n "cranelift-codegen") (r "^0.89.2") (d #t) (k 0)))) (h "03qkhfry7x1c001gdgvdnw07pzgf47g52xqi8bcqmpxc9rfmi3wl") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.88.2 (c (n "cranelift-preopt") (v "0.88.2") (d (list (d (n "cranelift-codegen") (r "^0.88.2") (k 0)))) (h "037yq5as7cx2wr69a8kkvhvnql34mcrnb732a19v5ibzsp7rlamh") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.90.0 (c (n "cranelift-preopt") (v "0.90.0") (d (list (d (n "cranelift-codegen") (r "^0.90.0") (d #t) (k 0)))) (h "0lb7f4l3skhph8b0j3wqhs6amfkzdz65vb6ivw4j8zl2gqwxv7p9") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.90.1 (c (n "cranelift-preopt") (v "0.90.1") (d (list (d (n "cranelift-codegen") (r "^0.90.1") (d #t) (k 0)))) (h "1plblgknl0hfrxan8idwadsf0l6lnzi47xsmdc6w1ns0vfyql7nv") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.91.0 (c (n "cranelift-preopt") (v "0.91.0") (d (list (d (n "cranelift-codegen") (r "^0.91.0") (d #t) (k 0)))) (h "078h45zd0zd805gj54xjig9ryppjgw1sa6y4xfk1s6nvshd53x5w") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.92.0 (c (n "cranelift-preopt") (v "0.92.0") (d (list (d (n "cranelift-codegen") (r "^0.92.0") (d #t) (k 0)))) (h "1md4djflf1sl62m8rgcsmlpbbryqiyfq9lhsqnwidk5782j1az72") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.91.1 (c (n "cranelift-preopt") (v "0.91.1") (d (list (d (n "cranelift-codegen") (r "^0.91.1") (d #t) (k 0)))) (h "0pbhkyjwxf974dg0v9zl6ypfzpzbav42aq19dyv6znq7wwxd8cx3") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

(define-public crate-cranelift-preopt-0.92.1 (c (n "cranelift-preopt") (v "0.92.1") (d (list (d (n "cranelift-codegen") (r "^0.92.1") (d #t) (k 0)))) (h "1ml3drdxxcd2sjpcp79da4g12nliy4j945aiw45z29ymyj5hhw1v") (f (quote (("std" "cranelift-codegen/std") ("default" "std") ("core" "cranelift-codegen/core"))))))

