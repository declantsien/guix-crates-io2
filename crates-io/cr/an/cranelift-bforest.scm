(define-module (crates-io cr an cranelift-bforest) #:use-module (crates-io))

(define-public crate-cranelift-bforest-0.19.0 (c (n "cranelift-bforest") (v "0.19.0") (d (list (d (n "cranelift-entity") (r "^0.19.0") (k 0)))) (h "0b3c969brk9rkkyi9j3a1mzw3ssbppzzcskpb4zdwhd4j8jfdi77") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.20.0 (c (n "cranelift-bforest") (v "0.20.0") (d (list (d (n "cranelift-entity") (r "^0.20.0") (k 0)))) (h "1lnawhlsjnjldmw2pwg495dzd0wxyydz5far08ids8aha8dqb5hy") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.21.0 (c (n "cranelift-bforest") (v "0.21.0") (d (list (d (n "cranelift-entity") (r "^0.21.0") (k 0)))) (h "1nqrs6clyb07qcycp8hp34pk4cvwj0y2clpibx2mxlxrcgpvl7br") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.21.1 (c (n "cranelift-bforest") (v "0.21.1") (d (list (d (n "cranelift-entity") (r "^0.21.1") (k 0)))) (h "1yqg8l38asg1bv2pqf38p34sy4g37mzdshrwgz524qm7lv15pa4g") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.22.0 (c (n "cranelift-bforest") (v "0.22.0") (d (list (d (n "cranelift-entity") (r "^0.22.0") (k 0)))) (h "1z39fmzc1qy7k5hkx1chzwdzxlqf50s00wi4lb82h5i5iv3d9ngd") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.23.0 (c (n "cranelift-bforest") (v "0.23.0") (d (list (d (n "cranelift-entity") (r "^0.23.0") (k 0)))) (h "0yvqj9l6zh6r06rlr61n9vdpxjnkiqyh3a1ihnlmjfzpnhd8wpwc") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.24.0 (c (n "cranelift-bforest") (v "0.24.0") (d (list (d (n "cranelift-entity") (r "^0.24.0") (k 0)))) (h "0w3gyc8p3zm47v0gm111pqfms1bzklxcx1dij0djmnlhszslbzjv") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.25.0 (c (n "cranelift-bforest") (v "0.25.0") (d (list (d (n "cranelift-entity") (r "^0.25.0") (k 0)))) (h "187s9l7knkjkq5p2qd4prxq9m4i3nfqg4wkx07zqiwqs4ixm4bbv") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.26.0 (c (n "cranelift-bforest") (v "0.26.0") (d (list (d (n "cranelift-entity") (r "^0.26.0") (k 0)))) (h "1grq1vp48ivbxn0mrnvvx9yc3jnj6jddy53bhj59pj56x4jgzy20") (f (quote (("std" "cranelift-entity/std") ("default" "std"))))))

(define-public crate-cranelift-bforest-0.27.0 (c (n "cranelift-bforest") (v "0.27.0") (d (list (d (n "cranelift-entity") (r "^0.27.0") (k 0)))) (h "0spd57bzydh20f4w57sx3pvz8x6y6rq6wik1l05bchv0g8glcfmm") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.28.0 (c (n "cranelift-bforest") (v "0.28.0") (d (list (d (n "cranelift-entity") (r "^0.28.0") (k 0)))) (h "1n20w9kaz1gl8dbh1j372kilq3pqggff4kmrw7gcxciqzk1b13f8") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.29.0 (c (n "cranelift-bforest") (v "0.29.0") (d (list (d (n "cranelift-entity") (r "^0.29.0") (k 0)))) (h "0qzp9q1hphkkrfxf8jsxhfn500x88rijybsr7pv9si2l9bz5v8m3") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.30.0 (c (n "cranelift-bforest") (v "0.30.0") (d (list (d (n "cranelift-entity") (r "^0.30.0") (k 0)))) (h "01bm1zym88k1qmk1xys4v1nadg4xnpqijsk25n64mgv60v95g8z5") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.31.0 (c (n "cranelift-bforest") (v "0.31.0") (d (list (d (n "cranelift-entity") (r "^0.31.0") (k 0)))) (h "0m4cid7ps5gk0ifms7dqamgkqgk10bps6wr8nw9f2xnj7npr57s0") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.32.0 (c (n "cranelift-bforest") (v "0.32.0") (d (list (d (n "cranelift-entity") (r "^0.32.0") (k 0)))) (h "01m222rp8kdfp90wkfn5hvp3nyp0fvzf8zvdgfvhs46mzsai152w") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.33.0 (c (n "cranelift-bforest") (v "0.33.0") (d (list (d (n "cranelift-entity") (r "^0.33.0") (k 0)))) (h "0zmdj25p3hviyz84crb9pka02g7dg3xcmgdq9fxl7ni4m8dlkwg2") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.34.0 (c (n "cranelift-bforest") (v "0.34.0") (d (list (d (n "cranelift-entity") (r "^0.34.0") (k 0)))) (h "02jzjlsjw0ys8c9v94n247ycfvsz1b82v6gm0mz3frm5gnzq26hf") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.35.0 (c (n "cranelift-bforest") (v "0.35.0") (d (list (d (n "cranelift-entity") (r "^0.35.0") (k 0)))) (h "148n48ax0p2gcwhbn8fybbxmmydqrmc5wf87fd6qwjk1rjcm120y") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.36.0 (c (n "cranelift-bforest") (v "0.36.0") (d (list (d (n "cranelift-entity") (r "^0.36.0") (k 0)))) (h "17r9a01pm3spj17b22l5m65p358ibrf2sa91pmb5syk1pxjwvvsq") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.37.0 (c (n "cranelift-bforest") (v "0.37.0") (d (list (d (n "cranelift-entity") (r "^0.37.0") (k 0)))) (h "17jz6pbnj0rvb33d6qkp4v395arv1lhxya6f2wv6266llzksbd30") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.38.0 (c (n "cranelift-bforest") (v "0.38.0") (d (list (d (n "cranelift-entity") (r "^0.38.0") (k 0)))) (h "08q9zqjgpvgjbppx07xh91xih0cykhj0h316dmhs6m42jqvmfxxl") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.39.0 (c (n "cranelift-bforest") (v "0.39.0") (d (list (d (n "cranelift-entity") (r "^0.39.0") (k 0)))) (h "0iws0vpsyjipyg3c09v0a51vglx9rlaq9hwdad73gagr8rkja2k9") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.40.0 (c (n "cranelift-bforest") (v "0.40.0") (d (list (d (n "cranelift-entity") (r "^0.40.0") (k 0)))) (h "05f33751zfxl0vhabii92xh7dz98xfs8r91b9k4jfdjcy7d30z49") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.41.0 (c (n "cranelift-bforest") (v "0.41.0") (d (list (d (n "cranelift-entity") (r "^0.41.0") (k 0)))) (h "13wlxsbp5iyhcc0mzpq41a59cfy1lsb5acj51j9dyy1dafr62rg1") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.42.0 (c (n "cranelift-bforest") (v "0.42.0") (d (list (d (n "cranelift-entity") (r "^0.42.0") (k 0)))) (h "1mdmwcf4iiq7b60kjpbd87v0gvkb6mq2l9fk4cv64qgy1h3y31sa") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.43.0 (c (n "cranelift-bforest") (v "0.43.0") (d (list (d (n "cranelift-entity") (r "^0.43.0") (k 0)))) (h "1s5c111jk1sx5x80jiw5jnc9qjr20qrqxp5dd1dwx40k2vajm1bj") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.43.1 (c (n "cranelift-bforest") (v "0.43.1") (d (list (d (n "cranelift-entity") (r "^0.43.1") (k 0)))) (h "1hqwl7yq1iw7r436s4998wwaki3gqfjidbnzfw4c2zgv29vb4y7d") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.44.0 (c (n "cranelift-bforest") (v "0.44.0") (d (list (d (n "cranelift-entity") (r "^0.44.0") (k 0)))) (h "0l8l85jwd8dvxxy86jq9dzbsv2baril64g3m5si095rcv154zw7z") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core"))))))

(define-public crate-cranelift-bforest-0.45.0 (c (n "cranelift-bforest") (v "0.45.0") (d (list (d (n "cranelift-entity") (r "^0.45.0") (k 0)))) (h "168sds2dp1yf41srfadnm9bpqnf59j33dndpqz8i7aa4lj16pmy4")))

(define-public crate-cranelift-bforest-0.46.0 (c (n "cranelift-bforest") (v "0.46.0") (d (list (d (n "cranelift-entity") (r "^0.46.0") (k 0)))) (h "0929j337c3m2alb9nhrnjr0yclycb7rwz2k8v8jpadwz69i16cg3")))

(define-public crate-cranelift-bforest-0.46.1 (c (n "cranelift-bforest") (v "0.46.1") (d (list (d (n "cranelift-entity") (r "^0.46.1") (k 0)))) (h "03zhb2079clq1kla5zzka3d3zzb5rkwfnd4f3zhmygkdjj47bj8q")))

(define-public crate-cranelift-bforest-0.47.0 (c (n "cranelift-bforest") (v "0.47.0") (d (list (d (n "cranelift-entity") (r "^0.47.0") (k 0)))) (h "03a475p07g4dip7bwr485zz7pfsb2g5mnhp7p0sxwa8kpx4mndb3")))

(define-public crate-cranelift-bforest-0.48.0 (c (n "cranelift-bforest") (v "0.48.0") (d (list (d (n "cranelift-entity") (r "^0.48.0") (k 0)))) (h "0rfz3v0rhb7w72ql409wkrj5m5fwxx5v0r7s2fyypavapcpckf2q")))

(define-public crate-cranelift-bforest-0.49.0 (c (n "cranelift-bforest") (v "0.49.0") (d (list (d (n "cranelift-entity") (r "^0.49.0") (k 0)))) (h "0w0n696zwmxd5nx2jz0qv4yh0cbzfsflfy9gvjgwhx0624jy8ads")))

(define-public crate-cranelift-bforest-0.50.0 (c (n "cranelift-bforest") (v "0.50.0") (d (list (d (n "cranelift-entity") (r "^0.50.0") (k 0)))) (h "1p6485mjn324sbkm1dhxd6n1ixdidkrzi38p4v759ppwrv4al1dx")))

(define-public crate-cranelift-bforest-0.51.0 (c (n "cranelift-bforest") (v "0.51.0") (d (list (d (n "cranelift-entity") (r "^0.51.0") (k 0)))) (h "0xlkplkvzxd0bk60l52wc6h0h70hx21ag3wgaprdjzyl8xy2xfa0")))

(define-public crate-cranelift-bforest-0.52.0 (c (n "cranelift-bforest") (v "0.52.0") (d (list (d (n "cranelift-entity") (r "^0.52.0") (k 0)))) (h "1w8llnq89jlpdcckzqgshp8w25p18wnfz14y5x7n6pac23pp5ajn")))

(define-public crate-cranelift-bforest-0.53.0 (c (n "cranelift-bforest") (v "0.53.0") (d (list (d (n "cranelift-entity") (r "^0.53.0") (k 0)))) (h "0wsg421gh5cjpv9gyfcpnh8i6ah6lyrx4vf500r908paj9cms1iv")))

(define-public crate-cranelift-bforest-0.54.0 (c (n "cranelift-bforest") (v "0.54.0") (d (list (d (n "cranelift-entity") (r "^0.54.0") (k 0)))) (h "0av3fadvjkrwf5j763mcix38cgkz3zxz2rpvqm0qj4dyy7zjacmx")))

(define-public crate-cranelift-bforest-0.55.0 (c (n "cranelift-bforest") (v "0.55.0") (d (list (d (n "cranelift-entity") (r "^0.55.0") (k 0)))) (h "0xi7zkhfp1bqi09alxyssxjbw5ij9l4bid7ps030j48rfipnxbs0")))

(define-public crate-cranelift-bforest-0.56.0 (c (n "cranelift-bforest") (v "0.56.0") (d (list (d (n "cranelift-entity") (r "^0.56.0") (k 0)))) (h "1p3y0bqy1iz6lp1cncnjff18yg5laiiwzh41llwmqp87nj84iyqx")))

(define-public crate-cranelift-bforest-0.58.0 (c (n "cranelift-bforest") (v "0.58.0") (d (list (d (n "cranelift-entity") (r "^0.58.0") (k 0)))) (h "1nax9sf5g5b879db058bmc57laxm0s21v6ccns51ray9kpam63zx")))

(define-public crate-cranelift-bforest-0.59.0 (c (n "cranelift-bforest") (v "0.59.0") (d (list (d (n "cranelift-entity") (r "^0.59.0") (k 0)))) (h "062qvkik8ji15zax78w7048297wz1f8w3xlkv9xqbfa2h0gw5aa5")))

(define-public crate-cranelift-bforest-0.60.0 (c (n "cranelift-bforest") (v "0.60.0") (d (list (d (n "cranelift-entity") (r "^0.60.0") (k 0)))) (h "1szpfx4mbadvm2hkby96x0x901rqba8dlya6fs0z6d1jiymz0639")))

(define-public crate-cranelift-bforest-0.61.0 (c (n "cranelift-bforest") (v "0.61.0") (d (list (d (n "cranelift-entity") (r "^0.61.0") (k 0)))) (h "17wc509nj5l9gg6rw1q186c0cp1rq8kvw08cbvnv69ysadgyv52g")))

(define-public crate-cranelift-bforest-0.62.0 (c (n "cranelift-bforest") (v "0.62.0") (d (list (d (n "cranelift-entity") (r "^0.62.0") (k 0)))) (h "142a55y6mrhpd6xncivjpgilds6haxr08j7z6sbmxpcw56r70qih")))

(define-public crate-cranelift-bforest-0.63.0 (c (n "cranelift-bforest") (v "0.63.0") (d (list (d (n "cranelift-entity") (r "^0.63.0") (k 0)))) (h "1nlylyzc6d4xxadzb70lj1a9fshrsgys3wahqs0zblpkqfv5nhnl")))

(define-public crate-cranelift-bforest-0.64.0 (c (n "cranelift-bforest") (v "0.64.0") (d (list (d (n "cranelift-entity") (r "^0.64.0") (k 0)))) (h "19y529jsdxvvgkv76529rlrjlfi28d44mc5h1yl9pcy4ab771s59")))

(define-public crate-cranelift-bforest-0.65.0 (c (n "cranelift-bforest") (v "0.65.0") (d (list (d (n "cranelift-entity") (r "^0.65.0") (k 0)))) (h "086i2v460kvgwps56m013m4ympa3ffyj2x46rj5an0fvdcn3lhfr")))

(define-public crate-cranelift-bforest-0.66.0 (c (n "cranelift-bforest") (v "0.66.0") (d (list (d (n "cranelift-entity") (r "^0.66.0") (k 0)))) (h "1pygxn7plwxkp44qf90gbg9hl0g64l8pqbi2b8g4mqif0mmjik4d")))

(define-public crate-cranelift-bforest-0.67.0 (c (n "cranelift-bforest") (v "0.67.0") (d (list (d (n "cranelift-entity") (r "^0.67.0") (k 0)))) (h "1wjzywa86113mrlzpwiafslrkfhi306j4c52c5zq33vmi5l5y1hg")))

(define-public crate-cranelift-bforest-0.68.0 (c (n "cranelift-bforest") (v "0.68.0") (d (list (d (n "cranelift-entity") (r "^0.68.0") (k 0)))) (h "1l7vf2q713bb94xq3lbl185mib0pivz5z2rd5fk0ip070mf588cj")))

(define-public crate-cranelift-bforest-0.69.0 (c (n "cranelift-bforest") (v "0.69.0") (d (list (d (n "cranelift-entity") (r "^0.69.0") (k 0)))) (h "1yz7ih6p73isx2g18sqgb36haawnqywwlszsqnw3xmq2nmizsrj0")))

(define-public crate-cranelift-bforest-0.70.0 (c (n "cranelift-bforest") (v "0.70.0") (d (list (d (n "cranelift-entity") (r "^0.70.0") (k 0)))) (h "0lp0j2ykbzbwq8fvpc09hvxbrhrjakc68r2cpdf0kn3jn7zq5xri")))

(define-public crate-cranelift-bforest-0.71.0 (c (n "cranelift-bforest") (v "0.71.0") (d (list (d (n "cranelift-entity") (r "^0.71.0") (k 0)))) (h "1kqb8x1djkv8gbm46vj7byvps2s61xzz7zc95dvq85070x8pmvmw")))

(define-public crate-cranelift-bforest-0.72.0 (c (n "cranelift-bforest") (v "0.72.0") (d (list (d (n "cranelift-entity") (r "^0.72.0") (k 0)))) (h "1zmmv57shpkf29czvlishi0q3mk9d4n4mdk2a5mi6c1mdnmpc544")))

(define-public crate-cranelift-bforest-0.73.0 (c (n "cranelift-bforest") (v "0.73.0") (d (list (d (n "cranelift-entity") (r "^0.73.0") (k 0)))) (h "168yf2iyay7vs5s39wk1kd5a8v6n0wh87kbqim4xgds6j7n43xh7")))

(define-public crate-cranelift-bforest-0.74.0 (c (n "cranelift-bforest") (v "0.74.0") (d (list (d (n "cranelift-entity") (r "^0.74.0") (k 0)))) (h "0glflbp8v3fwkamarcc1fxfvl414kx37gzp0sxz9qz3fd1h3bjn8")))

(define-public crate-cranelift-bforest-0.73.1 (c (n "cranelift-bforest") (v "0.73.1") (d (list (d (n "cranelift-entity") (r "^0.73.1") (k 0)))) (h "1kbfjbyaajh3aiqp6d91azizsbzw8k2gnzcp1rn0d0lpgg2a74lb")))

(define-public crate-cranelift-bforest-0.75.0 (c (n "cranelift-bforest") (v "0.75.0") (d (list (d (n "cranelift-entity") (r "^0.75.0") (k 0)))) (h "0grh5ls2khzpknp6c05nzmjvhckrrj8nmyskdk4j5910z7a8sgyz")))

(define-public crate-cranelift-bforest-0.76.0 (c (n "cranelift-bforest") (v "0.76.0") (d (list (d (n "cranelift-entity") (r "^0.76.0") (k 0)))) (h "0cvk0qwy6n5fwiq9nb39wp2323kf9z7qab7saizj81bmjrkylsvy")))

(define-public crate-cranelift-bforest-0.77.0 (c (n "cranelift-bforest") (v "0.77.0") (d (list (d (n "cranelift-entity") (r "^0.77.0") (k 0)))) (h "0fai7ij05v2rl15a82zr2irqpz93582v4r8kysyfwi6svm13c08m")))

(define-public crate-cranelift-bforest-0.78.0 (c (n "cranelift-bforest") (v "0.78.0") (d (list (d (n "cranelift-entity") (r "^0.78.0") (k 0)))) (h "04r54knzq554vhkv1j8c2sf3cw9ajfh99lwdd8p8zky8hbgvf36c")))

(define-public crate-cranelift-bforest-0.79.0 (c (n "cranelift-bforest") (v "0.79.0") (d (list (d (n "cranelift-entity") (r "^0.79.0") (k 0)))) (h "0sxsvdwcwwpl9fanq4jzf8bx15im77f52hzzpyf5pbs1a415xyzh")))

(define-public crate-cranelift-bforest-0.79.1 (c (n "cranelift-bforest") (v "0.79.1") (d (list (d (n "cranelift-entity") (r "^0.79.1") (k 0)))) (h "1ah0p61p5q07g1iylkm8kaxd94am5kqsyc69qw5rnafb29fsmpgb")))

(define-public crate-cranelift-bforest-0.80.0 (c (n "cranelift-bforest") (v "0.80.0") (d (list (d (n "cranelift-entity") (r "^0.80.0") (k 0)))) (h "1cqsjgz6wvn73qwchkj6w2ha1brg8ddzf4xpcfzlqyx45dmvl5lm")))

(define-public crate-cranelift-bforest-0.81.0 (c (n "cranelift-bforest") (v "0.81.0") (d (list (d (n "cranelift-entity") (r "^0.81.0") (k 0)))) (h "06gxwjj8l15dwyrb8wms9k1waz9jq4pmfza05k2pbj66miapai3i")))

(define-public crate-cranelift-bforest-0.81.1 (c (n "cranelift-bforest") (v "0.81.1") (d (list (d (n "cranelift-entity") (r "^0.81.1") (k 0)))) (h "17gks9qivppwcxg9a6fzm95vqi1pyns2w49wp0mpa0yfkbr2gw1j")))

(define-public crate-cranelift-bforest-0.80.1 (c (n "cranelift-bforest") (v "0.80.1") (d (list (d (n "cranelift-entity") (r "^0.80.1") (k 0)))) (h "1vzl0k11vg62lahni5fhl90znv9p27mnbk9kbxxx5dv7p36niz32")))

(define-public crate-cranelift-bforest-0.82.0 (c (n "cranelift-bforest") (v "0.82.0") (d (list (d (n "cranelift-entity") (r "^0.82.0") (k 0)))) (h "0ljyjckm7amm2pw0nw9695qay633k9h6i4787bwblszv54idp238")))

(define-public crate-cranelift-bforest-0.82.1 (c (n "cranelift-bforest") (v "0.82.1") (d (list (d (n "cranelift-entity") (r "^0.82.1") (k 0)))) (h "1d0x4bdkiddbcvg01vqxzi105aicha3khdwsa16i1pfpgcqj4sfi")))

(define-public crate-cranelift-bforest-0.82.2 (c (n "cranelift-bforest") (v "0.82.2") (d (list (d (n "cranelift-entity") (r "^0.82.2") (k 0)))) (h "1cg0x5aplb5r0v8hnl1v92v0q21h2w4s298bdrcn5c7y7snmd6av")))

(define-public crate-cranelift-bforest-0.81.2 (c (n "cranelift-bforest") (v "0.81.2") (d (list (d (n "cranelift-entity") (r "^0.81.2") (k 0)))) (h "14q038a7lkwjl3j0j7dfms46ln6w9shis5nm7dfmza8dmdrhzfhf")))

(define-public crate-cranelift-bforest-0.82.3 (c (n "cranelift-bforest") (v "0.82.3") (d (list (d (n "cranelift-entity") (r "^0.82.3") (k 0)))) (h "0x8gi908lmnzkniwizgjjbhjvzcbp4k4fyyk325fgj0ncshs5yiq")))

(define-public crate-cranelift-bforest-0.83.0 (c (n "cranelift-bforest") (v "0.83.0") (d (list (d (n "cranelift-entity") (r "^0.83.0") (k 0)))) (h "12y611l4y3qy80p8jaz91v0nk2qqmdb6jgnp1q6jdqrggqz42i7d")))

(define-public crate-cranelift-bforest-0.84.0 (c (n "cranelift-bforest") (v "0.84.0") (d (list (d (n "cranelift-entity") (r "^0.84.0") (k 0)))) (h "0zkr18df2n4pww1v36csf2i748vl6phk210y6qdd3hhki4cc79rg")))

(define-public crate-cranelift-bforest-0.85.0 (c (n "cranelift-bforest") (v "0.85.0") (d (list (d (n "cranelift-entity") (r "^0.85.0") (k 0)))) (h "1478l6kmba10dvnhsb3z1km1vb6kq2haz2zvhzwffwbp5z9ci7c9")))

(define-public crate-cranelift-bforest-0.85.1 (c (n "cranelift-bforest") (v "0.85.1") (d (list (d (n "cranelift-entity") (r "^0.85.1") (k 0)))) (h "16p259lsi232g1aq5jh2frxvwlyzrbqv6z5hh1q57k6y0nxgn0br")))

(define-public crate-cranelift-bforest-0.86.0 (c (n "cranelift-bforest") (v "0.86.0") (d (list (d (n "cranelift-entity") (r "^0.86.0") (k 0)))) (h "0lr2byhmch6z8czlg0dr77v9ggy4brg4pzql5xiqc450xa3v4mq4")))

(define-public crate-cranelift-bforest-0.85.2 (c (n "cranelift-bforest") (v "0.85.2") (d (list (d (n "cranelift-entity") (r "^0.85.2") (k 0)))) (h "1rk0l23wnz3b4na483qrkf86vfv225gh2qzraz5dqlw45nq3s0cg")))

(define-public crate-cranelift-bforest-0.85.3 (c (n "cranelift-bforest") (v "0.85.3") (d (list (d (n "cranelift-entity") (r "^0.85.3") (k 0)))) (h "1s6wgi8r8pc19vqv0axz6mrr6dfl70mdxgc0yk68s0y949h0v7bl")))

(define-public crate-cranelift-bforest-0.86.1 (c (n "cranelift-bforest") (v "0.86.1") (d (list (d (n "cranelift-entity") (r "^0.86.1") (k 0)))) (h "0m8gdr2hgl1w13cd05dlb1jxkwzdvxr1d552vc7cd6i4wb6gm7sj")))

(define-public crate-cranelift-bforest-0.87.0 (c (n "cranelift-bforest") (v "0.87.0") (d (list (d (n "cranelift-entity") (r "^0.87.0") (k 0)))) (h "0n5307x3509l40c421g2r6bp8cg8a5582f1h7m833my8rkdmm54k")))

(define-public crate-cranelift-bforest-0.87.1 (c (n "cranelift-bforest") (v "0.87.1") (d (list (d (n "cranelift-entity") (r "^0.87.1") (k 0)))) (h "1lgx7r57g2indwzxj3r41qgy59s49437g13qdgbwcnjsx9dl54cz")))

(define-public crate-cranelift-bforest-0.88.0 (c (n "cranelift-bforest") (v "0.88.0") (d (list (d (n "cranelift-entity") (r "^0.88.0") (k 0)))) (h "1c14p04ya27s5w9y0fvs97rsk76d3vawsyq45clgcb22dhzbsyxj")))

(define-public crate-cranelift-bforest-0.88.1 (c (n "cranelift-bforest") (v "0.88.1") (d (list (d (n "cranelift-entity") (r "^0.88.1") (k 0)))) (h "0yn1sqmgij3nxygr4vnc5fd2wsydkyvx4qxmr8h3jrhg5p7rqh24")))

(define-public crate-cranelift-bforest-0.89.0 (c (n "cranelift-bforest") (v "0.89.0") (d (list (d (n "cranelift-entity") (r "^0.89.0") (d #t) (k 0)))) (h "0q7k18z3nyrr7myh723cwiwdkhaqgl2afvhrjm5d4w98qbj1wpmy")))

(define-public crate-cranelift-bforest-0.89.1 (c (n "cranelift-bforest") (v "0.89.1") (d (list (d (n "cranelift-entity") (r "^0.89.1") (d #t) (k 0)))) (h "03bxsprr2ilii5xwb0p6bqsicv7ab8ds3f5vvvxi5hwcyxhvpmp4")))

(define-public crate-cranelift-bforest-0.89.2 (c (n "cranelift-bforest") (v "0.89.2") (d (list (d (n "cranelift-entity") (r "^0.89.2") (d #t) (k 0)))) (h "0rb236cnwdq6snd98qg3hkm98y189rcfinn3m7iy5cf5s26kjfsr")))

(define-public crate-cranelift-bforest-0.88.2 (c (n "cranelift-bforest") (v "0.88.2") (d (list (d (n "cranelift-entity") (r "^0.88.2") (k 0)))) (h "1p9dkcb88c4ydm15i9xnv203fpxizk0na6kcz9blnj440mnny1aj")))

(define-public crate-cranelift-bforest-0.90.0 (c (n "cranelift-bforest") (v "0.90.0") (d (list (d (n "cranelift-entity") (r "^0.90.0") (d #t) (k 0)))) (h "1p0bif8w6rqp9prfykfp4bqljf66h5ksbxk77b5viliwjiyxy062")))

(define-public crate-cranelift-bforest-0.90.1 (c (n "cranelift-bforest") (v "0.90.1") (d (list (d (n "cranelift-entity") (r "^0.90.1") (d #t) (k 0)))) (h "1bdb31003z14c9c1qp0063a5zcvgrf4ddjw48kx14qa1fqlpfb5n")))

(define-public crate-cranelift-bforest-0.91.0 (c (n "cranelift-bforest") (v "0.91.0") (d (list (d (n "cranelift-entity") (r "^0.91.0") (d #t) (k 0)))) (h "1q9vdz7vblhikcx4rj7fwclp6zpxzbiwpfdq9b0lyi141cqjp5gw")))

(define-public crate-cranelift-bforest-0.92.0 (c (n "cranelift-bforest") (v "0.92.0") (d (list (d (n "cranelift-entity") (r "^0.92.0") (d #t) (k 0)))) (h "1hvc6mhkzgkkn6xdk2drfvxrqfizr87dcvxjwdd81x98n3m58g9g")))

(define-public crate-cranelift-bforest-0.93.0 (c (n "cranelift-bforest") (v "0.93.0") (d (list (d (n "cranelift-entity") (r "^0.93.0") (d #t) (k 0)))) (h "18p3pkai7m1avcc3pw6yibx4w5dnpv24pbz3rn2sx9k953wqrcci")))

(define-public crate-cranelift-bforest-0.91.1 (c (n "cranelift-bforest") (v "0.91.1") (d (list (d (n "cranelift-entity") (r "^0.91.1") (d #t) (k 0)))) (h "05811vi3rp6j3678jv4yky51ms3nbwcklh44w55nyfpx5m8v8aia")))

(define-public crate-cranelift-bforest-0.92.1 (c (n "cranelift-bforest") (v "0.92.1") (d (list (d (n "cranelift-entity") (r "^0.92.1") (d #t) (k 0)))) (h "10ixc77r5mq6rxafzac7hw5wcp8bfwyjwpy6hr0fl1rs0kpmidjw")))

(define-public crate-cranelift-bforest-0.93.1 (c (n "cranelift-bforest") (v "0.93.1") (d (list (d (n "cranelift-entity") (r "^0.93.1") (d #t) (k 0)))) (h "0nl1618bgg1pccc9s5jja9w6a14g2437cji0yfmi83zfmjx9ldx7")))

(define-public crate-cranelift-bforest-0.94.0 (c (n "cranelift-bforest") (v "0.94.0") (d (list (d (n "cranelift-entity") (r "^0.94.0") (d #t) (k 0)))) (h "1haczmb2camn8g5mg8lyl3gslxzyjhacafy74zdr3y91zi9v0bl6")))

(define-public crate-cranelift-bforest-0.95.0 (c (n "cranelift-bforest") (v "0.95.0") (d (list (d (n "cranelift-entity") (r "^0.95.0") (d #t) (k 0)))) (h "0l5g4bxyn43pz0ilwibszs662018aqgi1995sj2ldjzq8jpvb7rf")))

(define-public crate-cranelift-bforest-0.93.2 (c (n "cranelift-bforest") (v "0.93.2") (d (list (d (n "cranelift-entity") (r "^0.93.2") (d #t) (k 0)))) (h "0yz5sa1cp9g672i3jzlsrsnpscsk5n0rlafwyw7v5r9jwai2pi1b")))

(define-public crate-cranelift-bforest-0.95.1 (c (n "cranelift-bforest") (v "0.95.1") (d (list (d (n "cranelift-entity") (r "^0.95.1") (d #t) (k 0)))) (h "0w2frrg17kdccqnapvn7ygvsb76l77kd7ppj9bnci0mwjkxgnxqj")))

(define-public crate-cranelift-bforest-0.94.1 (c (n "cranelift-bforest") (v "0.94.1") (d (list (d (n "cranelift-entity") (r "^0.94.1") (d #t) (k 0)))) (h "1y8k5778j8qabnsfsa4qkihpq2phslmi35sa7cgwqmwm5mrz8lq8")))

(define-public crate-cranelift-bforest-0.96.0 (c (n "cranelift-bforest") (v "0.96.0") (d (list (d (n "cranelift-entity") (r "^0.96.0") (d #t) (k 0)))) (h "1abps4cfhkndkxbv6kmk93rkzdd2n6rndr7kha8x6hyqrw9gmysx")))

(define-public crate-cranelift-bforest-0.96.1 (c (n "cranelift-bforest") (v "0.96.1") (d (list (d (n "cranelift-entity") (r "^0.96.1") (d #t) (k 0)))) (h "1g3yk4a821ljizghs4k6yvn1aiaaaf1hjzpvg4xrjlv2m7060qcv")))

(define-public crate-cranelift-bforest-0.96.2 (c (n "cranelift-bforest") (v "0.96.2") (d (list (d (n "cranelift-entity") (r "^0.96.2") (d #t) (k 0)))) (h "0rf104vslbir5fzs2rrdbcxhrs29k5n2li3hnfp6srf4yj088ip4")))

(define-public crate-cranelift-bforest-0.96.3 (c (n "cranelift-bforest") (v "0.96.3") (d (list (d (n "cranelift-entity") (r "^0.96.3") (d #t) (k 0)))) (h "0niy7acnkwsk83vgmv3sc98asv9q38r2b18rkmqbckli999ll1lw")))

(define-public crate-cranelift-bforest-0.96.4 (c (n "cranelift-bforest") (v "0.96.4") (d (list (d (n "cranelift-entity") (r "^0.96.4") (d #t) (k 0)))) (h "13kig59xbmfd5z79b9k5cqi59xvf6l6qf6asxqx4vxa9h3vq4aqq")))

(define-public crate-cranelift-bforest-0.97.0 (c (n "cranelift-bforest") (v "0.97.0") (d (list (d (n "cranelift-entity") (r "^0.97.0") (d #t) (k 0)))) (h "0p5f3lx1dac1af34isgzmwwfx8fhzrr0z18f9ilh75bqb8yyxnz5")))

(define-public crate-cranelift-bforest-0.97.1 (c (n "cranelift-bforest") (v "0.97.1") (d (list (d (n "cranelift-entity") (r "^0.97.1") (d #t) (k 0)))) (h "1g5d19vf1n8jgwl3f6lycb51d14asrgkp5afaad355rsmj79na2w")))

(define-public crate-cranelift-bforest-0.98.0 (c (n "cranelift-bforest") (v "0.98.0") (d (list (d (n "cranelift-entity") (r "^0.98.0") (d #t) (k 0)))) (h "1z872s1azbavb4lihfk8zg0mlz5bf17dxwjvdcrfndb2wmray9zc")))

(define-public crate-cranelift-bforest-0.98.1 (c (n "cranelift-bforest") (v "0.98.1") (d (list (d (n "cranelift-entity") (r "^0.98.1") (d #t) (k 0)))) (h "1z56n1fnminmms23cbgjmr95jkxcib4hivbq5z9l48lhaqjig00k")))

(define-public crate-cranelift-bforest-0.99.0 (c (n "cranelift-bforest") (v "0.99.0") (d (list (d (n "cranelift-entity") (r "^0.99.0") (d #t) (k 0)))) (h "0imc48mp1jhiyzc4qw9xxl5lpih592mbrzr7ccr1h3ajdnv3z3mg")))

(define-public crate-cranelift-bforest-0.99.1 (c (n "cranelift-bforest") (v "0.99.1") (d (list (d (n "cranelift-entity") (r "^0.99.1") (d #t) (k 0)))) (h "1vpm1d4qhyq4zn96xaqk1yjl15bzn09jz1azbqld08ra4h880d6p")))

(define-public crate-cranelift-bforest-0.97.2 (c (n "cranelift-bforest") (v "0.97.2") (d (list (d (n "cranelift-entity") (r "^0.97.2") (d #t) (k 0)))) (h "1ksn6qzr0k5iwsxnqwz1k3cqg8w5r5xvfncm1frwy32c5ianzbks")))

(define-public crate-cranelift-bforest-0.98.2 (c (n "cranelift-bforest") (v "0.98.2") (d (list (d (n "cranelift-entity") (r "^0.98.2") (d #t) (k 0)))) (h "1yzd41if59zizq5n5hq0vv86mswsdgjvl80blz6k2fmw18n2zgqf")))

(define-public crate-cranelift-bforest-0.99.2 (c (n "cranelift-bforest") (v "0.99.2") (d (list (d (n "cranelift-entity") (r "^0.99.2") (d #t) (k 0)))) (h "13xg3kv2ziw0l2gf7jsrqlg5pwkr44ggbcidfh42hxzvyv6a34as")))

(define-public crate-cranelift-bforest-0.100.0 (c (n "cranelift-bforest") (v "0.100.0") (d (list (d (n "cranelift-entity") (r "^0.100.0") (d #t) (k 0)))) (h "14l1b4mszx6qvgcfx6dhsz1fh7sxg20a74kpbpapmhknwylx3f83")))

(define-public crate-cranelift-bforest-0.101.0 (c (n "cranelift-bforest") (v "0.101.0") (d (list (d (n "cranelift-entity") (r "^0.101.0") (d #t) (k 0)))) (h "07k65yahfpcy5a9nbi1chrhlsxabm15i7z2anc1xv244vbq1splf")))

(define-public crate-cranelift-bforest-0.101.1 (c (n "cranelift-bforest") (v "0.101.1") (d (list (d (n "cranelift-entity") (r "^0.101.1") (d #t) (k 0)))) (h "1gpiyv3x0n3m6dfq4xrsmyg34yy89fbarhwz23kihc5inqxjqlf1")))

(define-public crate-cranelift-bforest-0.100.1 (c (n "cranelift-bforest") (v "0.100.1") (d (list (d (n "cranelift-entity") (r "^0.100.1") (d #t) (k 0)))) (h "1a6vi6y2cpn43lyj1jlmvm8m1nigqxfzkdqyck087whkwn4vy73m")))

(define-public crate-cranelift-bforest-0.101.2 (c (n "cranelift-bforest") (v "0.101.2") (d (list (d (n "cranelift-entity") (r "^0.101.2") (d #t) (k 0)))) (h "1zpkfmcqpv2vlv4n0s4c4hbhy6nj1ifrm6xz4ksc02lq0xrl6wzp")))

(define-public crate-cranelift-bforest-0.101.3 (c (n "cranelift-bforest") (v "0.101.3") (d (list (d (n "cranelift-entity") (r "^0.101.3") (d #t) (k 0)))) (h "18vq1hx77w7hh8p9wn1jqyy8zlxmrz7qkp19xfz42nc5g7xqlbpz")))

(define-public crate-cranelift-bforest-0.101.4 (c (n "cranelift-bforest") (v "0.101.4") (d (list (d (n "cranelift-entity") (r "^0.101.4") (d #t) (k 0)))) (h "18rrhm3w7mdlyi2k2rwx3hq9v5ph666m63hi0d6w1p67bqjbjnrb")))

(define-public crate-cranelift-bforest-0.102.0 (c (n "cranelift-bforest") (v "0.102.0") (d (list (d (n "cranelift-entity") (r "^0.102.0") (d #t) (k 0)))) (h "0h9d76jgkf9s8lc0b7lkrbrvznkpdlmpia6r2525l2v9mzr3isvn")))

(define-public crate-cranelift-bforest-0.102.1 (c (n "cranelift-bforest") (v "0.102.1") (d (list (d (n "cranelift-entity") (r "^0.102.1") (d #t) (k 0)))) (h "0a9kkiazl8j390hf87wlhh1wm1mizjif9f8wd4mzjqr2imk5czlf")))

(define-public crate-cranelift-bforest-0.103.0 (c (n "cranelift-bforest") (v "0.103.0") (d (list (d (n "cranelift-entity") (r "^0.103.0") (d #t) (k 0)))) (h "14lbvigsdkzmk5z5xzf2p8j267snkj33js7dyw137gcm1cn588kw")))

(define-public crate-cranelift-bforest-0.104.0 (c (n "cranelift-bforest") (v "0.104.0") (d (list (d (n "cranelift-entity") (r "^0.104.0") (d #t) (k 0)))) (h "1w5w2pg41zidx7g4v968ywmv4xqirsh6q8r83s7s2864lknzw6fq")))

(define-public crate-cranelift-bforest-0.104.1 (c (n "cranelift-bforest") (v "0.104.1") (d (list (d (n "cranelift-entity") (r "^0.104.1") (d #t) (k 0)))) (h "0cm76mxb0j58qkb67ylgsjzafblwnq7slyhyvn8kb1jv418hsz3y")))

(define-public crate-cranelift-bforest-0.105.0 (c (n "cranelift-bforest") (v "0.105.0") (d (list (d (n "cranelift-entity") (r "^0.105.0") (d #t) (k 0)))) (h "05n8l0fpvyfps59wxmdnh45fknvs2s0mwqgvq5fhz5x3pc6pkv3p")))

(define-public crate-cranelift-bforest-0.105.1 (c (n "cranelift-bforest") (v "0.105.1") (d (list (d (n "cranelift-entity") (r "^0.105.1") (d #t) (k 0)))) (h "0d7fr2qlaqrgy0imzy4wr036wy6adcf5dykh6d0kzabjj4d3k9i9")))

(define-public crate-cranelift-bforest-0.104.2 (c (n "cranelift-bforest") (v "0.104.2") (d (list (d (n "cranelift-entity") (r "^0.104.2") (d #t) (k 0)))) (h "0b0xv37pxhjgnzwy6lviw3i5mn6lwmyysh1jjf3vklg6h4msdzwm")))

(define-public crate-cranelift-bforest-0.105.2 (c (n "cranelift-bforest") (v "0.105.2") (d (list (d (n "cranelift-entity") (r "^0.105.2") (d #t) (k 0)))) (h "1hlbvkp6gkf57dfi8ga72b7x10lph5pilk5qfrzi7dbc5g2gq5cm")))

(define-public crate-cranelift-bforest-0.105.3 (c (n "cranelift-bforest") (v "0.105.3") (d (list (d (n "cranelift-entity") (r "^0.105.3") (d #t) (k 0)))) (h "092wi61n3j7q76dmgq9a6mh93iv5d5pvpv6y3nxnp9mw58g55m8n")))

(define-public crate-cranelift-bforest-0.106.0 (c (n "cranelift-bforest") (v "0.106.0") (d (list (d (n "cranelift-entity") (r "^0.106.0") (d #t) (k 0)))) (h "10bkjlh7y1mzxr456pggjwpjvjy180634sf5glch6q2sryqmwlva")))

(define-public crate-cranelift-bforest-0.106.1 (c (n "cranelift-bforest") (v "0.106.1") (d (list (d (n "cranelift-entity") (r "^0.106.1") (d #t) (k 0)))) (h "1l93fd6h1i2rg2mmnhgm4f71dc5jxpz5bspbkv990360dk67adsv")))

(define-public crate-cranelift-bforest-0.105.4 (c (n "cranelift-bforest") (v "0.105.4") (d (list (d (n "cranelift-entity") (r "^0.105.4") (d #t) (k 0)))) (h "1bzvm7xivnb1md8c1k7wi7pqk7xszphv7f0qqnsfkyxxc8xrjv29")))

(define-public crate-cranelift-bforest-0.106.2 (c (n "cranelift-bforest") (v "0.106.2") (d (list (d (n "cranelift-entity") (r "^0.106.2") (d #t) (k 0)))) (h "175jgvcdjdfi4zhfz1vlz4r74w9606r50yyay5pbv2y2zzrx8mrv")))

(define-public crate-cranelift-bforest-0.104.3 (c (n "cranelift-bforest") (v "0.104.3") (d (list (d (n "cranelift-entity") (r "^0.104.3") (d #t) (k 0)))) (h "1dqxdd58fs7ia3dlm8kgabwf6dnr3h15m9bkpb0giqpgs3zk8l5q")))

(define-public crate-cranelift-bforest-0.107.0 (c (n "cranelift-bforest") (v "0.107.0") (d (list (d (n "cranelift-entity") (r "^0.107.0") (d #t) (k 0)))) (h "1m5yd0xs2bckrgfhlm4qk88byh8r1g5q986hc59mp6w7lqi7kckr")))

(define-public crate-cranelift-bforest-0.107.1 (c (n "cranelift-bforest") (v "0.107.1") (d (list (d (n "cranelift-entity") (r "^0.107.1") (d #t) (k 0)))) (h "0284qdj5s9njdm06lshmqqpifjbhnb35g7ky3k439s72qc5p0yiw")))

(define-public crate-cranelift-bforest-0.107.2 (c (n "cranelift-bforest") (v "0.107.2") (d (list (d (n "cranelift-entity") (r "^0.107.2") (d #t) (k 0)))) (h "0nrjzbdlcxpqh2nrmq3z2s4p7vyq2j179xhc9lcl2zcgygm2rxzb")))

(define-public crate-cranelift-bforest-0.108.0 (c (n "cranelift-bforest") (v "0.108.0") (d (list (d (n "cranelift-entity") (r "^0.108.0") (d #t) (k 0)))) (h "05s9z35s01y0a53sz920097nlcy5kfyw12rfybdya1z3ym30jpzp")))

(define-public crate-cranelift-bforest-0.108.1 (c (n "cranelift-bforest") (v "0.108.1") (d (list (d (n "cranelift-entity") (r "^0.108.1") (d #t) (k 0)))) (h "1xiyhrg19xk2ir85j9xzf39b2x722552rbkfmdmxl5fwmlvz3ni9")))

