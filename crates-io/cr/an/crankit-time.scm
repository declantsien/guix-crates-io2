(define-module (crates-io cr an crankit-time) #:use-module (crates-io))

(define-public crate-crankit-time-0.1.0 (c (n "crankit-time") (v "0.1.0") (d (list (d (n "playdate-sys-v02") (r "^0.2.11") (o #t) (k 0) (p "playdate-sys")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "0fccr5ll2m1w9582xp9bi34nlxpdk4l7cbifgsq6lggac8n735xl") (f (quote (("default" "playdate-sys-v02")))) (s 2) (e (quote (("playdate-sys-v02" "dep:playdate-sys-v02"))))))

(define-public crate-crankit-time-0.1.1 (c (n "crankit-time") (v "0.1.1") (d (list (d (n "playdate-sys-v02") (r "^0.2.11") (o #t) (k 0) (p "playdate-sys")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "1k693i5qkw15hv15nmwk3c6qpkzaapqa65lzx0qk5pps3h43i613") (f (quote (("default" "playdate-sys-v02")))) (s 2) (e (quote (("playdate-sys-v02" "dep:playdate-sys-v02"))))))

