(define-module (crates-io cr an cranelift-entity) #:use-module (crates-io))

(define-public crate-cranelift-entity-0.14.0 (c (n "cranelift-entity") (v "0.14.0") (h "1m6nxzvmniwzjb07sr94p48ychqfka3jrv78r3fqd1v1m6b5fkxf") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.15.0 (c (n "cranelift-entity") (v "0.15.0") (h "18bnjshq9fk23dwhp638l6rd33ipz6riwlhwrwwa6xsmlpwb5038") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.16.0 (c (n "cranelift-entity") (v "0.16.0") (h "1gyywyhr6c3dwcyipndn08f2vycy69pr6yqw8423xybp62fsqfsh") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.16.1 (c (n "cranelift-entity") (v "0.16.1") (h "09v0k2kybma1vfpaqcy5inwf8ybddgnvqw84hy7qccxslh54zk70") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.17.0-alpha (c (n "cranelift-entity") (v "0.17.0-alpha") (h "19rmql3lyp6r641y3bwqmw2axc1wxg41rxakxyziic008yf71rjr") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.17.0 (c (n "cranelift-entity") (v "0.17.0") (h "17dasbvp500yqmqfpnmkdyxrgcbskfv7i22cbhq6qj0fpdpnbp8f") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.18.0 (c (n "cranelift-entity") (v "0.18.0") (h "0vjf7hh15qcdzpv9qsi3ikdj5ajgc65q84yb5pg9zx68fd21jqza") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.18.1 (c (n "cranelift-entity") (v "0.18.1") (h "0sx4zb6sz0dqygn0qi2jnavzc9wc4fvzy3paxspamxaprsm667bm") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.19.0 (c (n "cranelift-entity") (v "0.19.0") (h "005d9gfxr4sbdbfhpi5fy1zp8qfbw7fnna8zb3kn0l6n13vwdld1") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.20.0 (c (n "cranelift-entity") (v "0.20.0") (h "0jypmrkblb5gj6ygmgrcbqci8zzrpbax260v3yiw8gpbl9sisi5j") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.20.1 (c (n "cranelift-entity") (v "0.20.1") (h "0lj766wzlr0f89a4sf247n9a77lshpl7w959vqjp2l9b7wajyh97") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.21.0 (c (n "cranelift-entity") (v "0.21.0") (h "0ga9bjpv12j24pyz1vv34rhx5hxk5rblv91s34z8cgxk17dpkmdj") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.21.1 (c (n "cranelift-entity") (v "0.21.1") (h "0icsdwvzcr1srxbmlc4595kwascdp522q7w10s62p9qdzi1d8b4b") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.22.0 (c (n "cranelift-entity") (v "0.22.0") (h "1bk0f6g9jsf9zvsxx45cp4fjmfg25m0fhmrrw7qifmajjjflxhvm") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.23.0 (c (n "cranelift-entity") (v "0.23.0") (h "15nab0hgyp7jk4hnp48wwmcm9dbf5r921xygw3102qild4iziqwv") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.24.0 (c (n "cranelift-entity") (v "0.24.0") (h "0p95gk1cpp9b5npxm29nmz5yxl0kfnvzw4253l0kxcd4638y4p0a") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.25.0 (c (n "cranelift-entity") (v "0.25.0") (h "1k4diswr982p46p99dgfvfzlqk2xk18csr3f9myhsplf40lh76pw") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.26.0 (c (n "cranelift-entity") (v "0.26.0") (h "12vx02vk0gwh7b4g7r0qrh0k1vhjnidrkjqrd7cb9xqcdki41pyl") (f (quote (("std") ("default" "std"))))))

(define-public crate-cranelift-entity-0.27.0 (c (n "cranelift-entity") (v "0.27.0") (h "0nixa206k150pky4kj3f3ah4j0gs2yfmr3mxcjg8i35nz1xssyni") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.28.0 (c (n "cranelift-entity") (v "0.28.0") (h "16mn9q2nhg7jfvdj33xwqd9sabn17mq8a9mnb0zbbjs871sc7k64") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.29.0 (c (n "cranelift-entity") (v "0.29.0") (h "09l2xjzca5klhvkip40hja7kp7lfpvmhlhk246z5zj3xrc4lw4jf") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.30.0 (c (n "cranelift-entity") (v "0.30.0") (h "1zf4g9bb40yqmmbcv0gn7np6i3yvnl5ph3r564mksnxpfy4f4rjv") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.31.0 (c (n "cranelift-entity") (v "0.31.0") (h "0026z2cvvmj4z7af3w59rfpi2v4vpb59bgbm0dpqsjhffk1j54kk") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.32.0 (c (n "cranelift-entity") (v "0.32.0") (h "04cz5fy6y3vpad033y6qgh4bxkxim1wczd3zyb29afw5vlqi1la2") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.33.0 (c (n "cranelift-entity") (v "0.33.0") (h "17zx599ajavr9k7frji1h5xx9bfad1qdj5qgiga92mbym3q1h80q") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.34.0 (c (n "cranelift-entity") (v "0.34.0") (h "1pbbx5d4f0bjnmly2rrvln0nywmiaaiyf2k4hgcwxb1ng00bp0qc") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.35.0 (c (n "cranelift-entity") (v "0.35.0") (h "1chrifxgzfinsi4a809xa56gcgcm434rxfpwggxp7s70zigbx52x") (f (quote (("std") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.36.0 (c (n "cranelift-entity") (v "0.36.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16kkvg9l73p68q2lxxs0lg8iy8azxggj8miz5dsdkqmc2fsry7hb") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.37.0 (c (n "cranelift-entity") (v "0.37.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13mq5zpd4qzm3g84605w0yqhcqvkakjfvfc2i41pl20qx95jbnpx") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.38.0 (c (n "cranelift-entity") (v "0.38.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02556n639mxrp8lqh621nziq23g5d7kaiyrccwgbf6aqdxk57nvi") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.39.0 (c (n "cranelift-entity") (v "0.39.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09aq5zpdrnw71j8y4ivdycvvdlmfll0sqx7h2j5znjkqvhby2cmm") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.40.0 (c (n "cranelift-entity") (v "0.40.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ca1ayjbnk5pcrhkaa6496hvzylvscz8274lrnwnm4l4c3r4xlyb") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.41.0 (c (n "cranelift-entity") (v "0.41.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nz6mcgaqq404v0q1k5ric8jd39mdxrn0a3l4f6dsl2m32hkmqw2") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.42.0 (c (n "cranelift-entity") (v "0.42.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fkdr6j58y460kbigjffjgjasfzzd12dvl4j950wrcx3vs7fbj34") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.43.0 (c (n "cranelift-entity") (v "0.43.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l14298fv2dv9pw17acm9gr1wlf6biglj7vvh12m4d64zs7wi6fw") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.43.1 (c (n "cranelift-entity") (v "0.43.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1f058q8if2jd40hbpnhk7hnly7z1vnkfikkc46xdyqh5myv0k64w") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.44.0 (c (n "cranelift-entity") (v "0.44.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lihsii24d6p826qp5i70pdb9r7nnqvx81a7hf44kq5xv4akyxg8") (f (quote (("std") ("enable-serde" "serde") ("default" "std") ("core"))))))

(define-public crate-cranelift-entity-0.45.0 (c (n "cranelift-entity") (v "0.45.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b47cbj8a0cdi5imwgcd4980y3627cqjs2hydwla4rmh4gdgd46c") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.46.0 (c (n "cranelift-entity") (v "0.46.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18d27sf1w1w4bd97d2lsvg3jg7xxbj0flpzjg9qj328i5vrf231h") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.46.1 (c (n "cranelift-entity") (v "0.46.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0y2acncyrsxks8mcp76d7xf9329851h4fzlbfr5kqpm7s9cgl5g9") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.47.0 (c (n "cranelift-entity") (v "0.47.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qkfywarvg6g3ykszx0qli0jh1nx179cwqvss6jbzpg51mdkw13w") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.48.0 (c (n "cranelift-entity") (v "0.48.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kbl0vw0d5gffiwq5dncwbvfxjafvjq2i8kxijxlfh87bp91yljn") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.49.0 (c (n "cranelift-entity") (v "0.49.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j9xmlfbg9y5sw0k2ay36agw93k8kb8dwhzqih7f2dmk4ab5ybck") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.50.0 (c (n "cranelift-entity") (v "0.50.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vg9mbvs8y9gnh32g556mh3yilck0dwshr020r9z964bvwrlf1pz") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.51.0 (c (n "cranelift-entity") (v "0.51.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15fbalmqvjksynmznrzv0fkgzx8qgccwwg3jy8pixx52vgrphzh5") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.52.0 (c (n "cranelift-entity") (v "0.52.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ncb9qxack3jgix1dkm4z3j8wghgxpg7c2dzawqpmnb4a3h5fabj") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.53.0 (c (n "cranelift-entity") (v "0.53.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12crqn4p1vjmrfif66vc7a8vqycs67wfrc4laibw8290dfj8ymq3") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.54.0 (c (n "cranelift-entity") (v "0.54.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mmnfavx61sb7r4qh3gv2fci96l2swqjnbn5l4b5fw91l2dsv61l") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.55.0 (c (n "cranelift-entity") (v "0.55.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d2mry90h8in76c82x5vgm2072hs94gq513xd3fdwki7sk1qvg1p") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.56.0 (c (n "cranelift-entity") (v "0.56.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1327659jf6bay4684m59z2ldbmkhzsv0gmi1rnyphdh10n3mvz9m") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.58.0 (c (n "cranelift-entity") (v "0.58.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1f79af4nlnxji3pfgmbkh6s9mz1rgg3kkyp1kng4q68j13apx30c") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.59.0 (c (n "cranelift-entity") (v "0.59.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0f1zwz4i0kb036fsjccjpqx0ib8di2xaaaxphw0vrsz150mpk3mq") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.60.0 (c (n "cranelift-entity") (v "0.60.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0f37dp7s03w7vsb70dp9wyd9fpfr4q5j96j918d6vl7n6izjfrlf") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.61.0 (c (n "cranelift-entity") (v "0.61.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nabfl0cx0ixrn75nljqpmhvzjnp2lsi5lw7iqvk24hailiqrrrm") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.62.0 (c (n "cranelift-entity") (v "0.62.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wh4p80919xsblggh0r575x8w84ch8di1sib5ci8vr2xpp2x0v6k") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.63.0 (c (n "cranelift-entity") (v "0.63.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wkl0ww926dsl2hg0wfjdkbjr6bxbsvm1zvi264rrfp56b276slj") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.64.0 (c (n "cranelift-entity") (v "0.64.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09h314y3md1qidq0dvdrgxfggk6kff2pq9fxb4r9iacd2casr9nz") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.65.0 (c (n "cranelift-entity") (v "0.65.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07fwy7lcc0kvikddaz1mai1xz8wnqi930kly6db4paax1ambhzph") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.66.0 (c (n "cranelift-entity") (v "0.66.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0w9zs6snb6jizym8bbx8g3g2mjglg68gnb470z8nb81fjxcdqz6r") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.67.0 (c (n "cranelift-entity") (v "0.67.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lq2vkcswa1y3savyrnxwggf0svkxiiqv60jqn05zxn9w1rxnmk1") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.68.0 (c (n "cranelift-entity") (v "0.68.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19hz6rmx8pdmii4bg3vzvmllj83vwjmkhsv6bfj54pz12k7dpfl6") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.69.0 (c (n "cranelift-entity") (v "0.69.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qsxgy1xx26k38x15j04xjb4cx66vlq7cj4bl24iylrjm8s4411v") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.70.0 (c (n "cranelift-entity") (v "0.70.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1iqicg7d8zs06wppv57va8iqprsha40q0wiibxvyq39z811r0pp5") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.71.0 (c (n "cranelift-entity") (v "0.71.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15pzl2z4vaapkypvpn1bm7w74hisc2l3w6c0m6djmr33lf26hbhs") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.72.0 (c (n "cranelift-entity") (v "0.72.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05f43c2l0nsny6c40244mh98i7maj2mbar3wkw7sz2bc75jg8331") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.73.0 (c (n "cranelift-entity") (v "0.73.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vd8mxddxxs9c5n34x59r2df15ibwz9nh5nj7iv1glx8fzfnzykr") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.74.0 (c (n "cranelift-entity") (v "0.74.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k668jmfygrjj7vsnbyzjyad0hf66gvlc9kqjr12xy04xy5llsvx") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.73.1 (c (n "cranelift-entity") (v "0.73.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qr5vdqyiasrm4csf2mfqqgpp0bvifvs7d34q7wrgv0sgskwgm4a") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.75.0 (c (n "cranelift-entity") (v "0.75.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03ip8ndksa75rsw6x83yxzky8yshcfrb9smj8k7zl9z6qrz3ws7y") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.76.0 (c (n "cranelift-entity") (v "0.76.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16a7979nnd07swbdvqv407r6dgw0xvxa9jpg174933jb9lz76a9k") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.77.0 (c (n "cranelift-entity") (v "0.77.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0imm4707hshgy47q0zj6z4zla3iwh543cxx1qj6r9kya0ym8z1j8") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.78.0 (c (n "cranelift-entity") (v "0.78.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fmvd66xpxhky5gqia38q171ba5jxac94k8m332qzgqqcf6mv953") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.79.0 (c (n "cranelift-entity") (v "0.79.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00rjm305lm2wjh97y0bb6nw6ik13gibci0cx09xs6ql4z6bfbpb4") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.79.1 (c (n "cranelift-entity") (v "0.79.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02nq6ia4jk3ri9fcx5fmf1agl56n30n2al67kwbjalzy7w7dlym2") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.80.0 (c (n "cranelift-entity") (v "0.80.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qg4x9cz01mljbddm6mxwj5ccjankgyz0gg5bkwvrlrx0m06g452") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.81.0 (c (n "cranelift-entity") (v "0.81.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xf9j5ds4ii24akr8l8qjdf9ars0w79b891nml0cb273fmw889dx") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.81.1 (c (n "cranelift-entity") (v "0.81.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pknfrpyhvf19cfyvrd9byqnb6fxrqqw0zs19m8yizmvgh5sas0j") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.80.1 (c (n "cranelift-entity") (v "0.80.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gvlr32m2w8yi5skfl15rclhvbnvkrq7l0v9dyrjrflpvdr5sz67") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.82.0 (c (n "cranelift-entity") (v "0.82.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0py33i1hxbffnp62pk00xg57vywzbn9k60r4hf97alsimbk0jw3g") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.82.1 (c (n "cranelift-entity") (v "0.82.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15zqhg5slz84d870p5nq952ywhr6qnvl0kbxmwxn87rcabpq5g36") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.82.2 (c (n "cranelift-entity") (v "0.82.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i9riw8x254rppza956r4lz0jb3m6rkcm6yigsy8q1vsklbf08p8") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.81.2 (c (n "cranelift-entity") (v "0.81.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kdk052a7cgd4xz7w2r49fdiynwnvb9wc04s8xzxcwc08258wkyq") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.82.3 (c (n "cranelift-entity") (v "0.82.3") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kw5j62fcyhs9nf7md8m6jlx1q2k0k39vbqzc027c5l35cbzwfwy") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.83.0 (c (n "cranelift-entity") (v "0.83.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vg7fpjx6rg30y8agxm2hhwwgqsyr0v8walgcq9r4hnms6wb4ir3") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.84.0 (c (n "cranelift-entity") (v "0.84.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0248wav877d20xgsfmpsi2vri8qp7wr3c6wj1b4h5agg3xxbsd3y") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.85.0 (c (n "cranelift-entity") (v "0.85.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jsnrd366lmxc8xj740r4vlxnkwhd9cqvdqdhsg4rqf4b5cbkf2c") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.85.1 (c (n "cranelift-entity") (v "0.85.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dk3ia8sby7yv76hgjnawbai1pkmkhadp06snzdhc7yyw2jmjv48") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.86.0 (c (n "cranelift-entity") (v "0.86.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m5x203wz1h6pp7aaw2sq3sg8bfdnp2zrvr2ly53ysnzs86ldjxz") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.85.2 (c (n "cranelift-entity") (v "0.85.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wl1yf7kxxqr4p39zjvd2nm87z4ydsyzvx9r2akyl56a80bvhwvm") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.85.3 (c (n "cranelift-entity") (v "0.85.3") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bgp0b4qwqjdw46safz5mk1impczspwyi5djcq7gwmi3rp7ymsh9") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.86.1 (c (n "cranelift-entity") (v "0.86.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0viygzb8hfjra5pn9lqlbc5jcj0jlsgp7l539v4w2b674jk8mahi") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.87.0 (c (n "cranelift-entity") (v "0.87.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19d7ksh7j8nxfrdkkfr7w7k6xsqc8srami0iqg9ijhbg0pas0b9l") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.87.1 (c (n "cranelift-entity") (v "0.87.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p7zq524fs11slm1rbasqb3k2yfnqhkfdidcbkc2bz4h923xgkmf") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.88.0 (c (n "cranelift-entity") (v "0.88.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p4qippfsr1qfynmas150zcmvxj7hj5ycmc64ydnr6c7psbrajim") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.88.1 (c (n "cranelift-entity") (v "0.88.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00v1smbdrqglf9f3y5lgsbk2475x5np615mg26zk8pfpg4r6fr8b") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.89.0 (c (n "cranelift-entity") (v "0.89.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09aq7pld7j08j3kc78ki74xlfg24d7962nqpz8zj6nb8bpn4zbnx") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.89.1 (c (n "cranelift-entity") (v "0.89.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pf2w96zzpzbzl3dzqicwhm9k856v0f7283573xrsl2l9w8a32pd") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.89.2 (c (n "cranelift-entity") (v "0.89.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1841xxizb2vprfcas2wgnxywdmyvklm289hbchs83zci6xyf397f") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.88.2 (c (n "cranelift-entity") (v "0.88.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lpkc44m2hi1qp0q1i2haf2kzvcx02dqvkvhcfapd1y1znrg3847") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.90.0 (c (n "cranelift-entity") (v "0.90.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17mabv5qmfpszwv32g0lsrz9cm4wxdiw2akx723vlv9cbn9n5c51") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.90.1 (c (n "cranelift-entity") (v "0.90.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ipkfs9fvl5dk09a5njcqzk0jrab6vx4qiadpicxgd9wjvggydq6") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.91.0 (c (n "cranelift-entity") (v "0.91.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1z6nmig7ddzz1f8hxkavvmc7jg59p55cqyxa1hpmdcq5wjsmwf3l") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.92.0 (c (n "cranelift-entity") (v "0.92.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04xsbk302q03g5gsnh7pmjqxjx6cpn2fk4mf1w4pdy4k8p4q9ffs") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.93.0 (c (n "cranelift-entity") (v "0.93.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0camfy35fng5qlyndsb1ybbbjl9a69rjc46788cpnhh1gdzlys5g") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.91.1 (c (n "cranelift-entity") (v "0.91.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k4x3bi45cawab3j7nyscczby32ybsxcpcrsp5q1ngwwm2ybqncs") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.92.1 (c (n "cranelift-entity") (v "0.92.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m1vmyabb9ygmm0yzz9xxx5ga4gcdfxh6gk2m8wybrspr37kk7js") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.93.1 (c (n "cranelift-entity") (v "0.93.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "018p8q6kds5j2vh9rqq9g56fxxip5hz35cczbw093wlkn3vq7xbw") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.94.0 (c (n "cranelift-entity") (v "0.94.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jk982rrm8a7zjqazvkqwr0idjgmlr30lpv4k4wagkv639n0kiv8") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.95.0 (c (n "cranelift-entity") (v "0.95.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06lpcwzc5g2glmlm77w9rpx67fwlad5s85qgz2p5zaccj6qijqqa") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.93.2 (c (n "cranelift-entity") (v "0.93.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qfa4bdb5nvqsb1g231qqjgc0laxa1hrd269p0casl5lqy9acbpl") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.95.1 (c (n "cranelift-entity") (v "0.95.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1c30hqkgg7q8v5l4ib9ip5r7l0wr46svm29zwq2yadqv0qw9s2a0") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.94.1 (c (n "cranelift-entity") (v "0.94.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19f7m52vficgx3mp6h5rs7sy17nq6v4d35jz1rv1kca9zwagr17i") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.96.0 (c (n "cranelift-entity") (v "0.96.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15hp4n4n61ljyn2vvk4pg4vnn9c3c934rqm3bz91v55ihkzly8rf") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.96.1 (c (n "cranelift-entity") (v "0.96.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rg0bmnk9lgrrbnhki8yrahcb1lpq3rk74yd6w9rsirrxqrrmwxs") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.96.2 (c (n "cranelift-entity") (v "0.96.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1awgqwhz02937qb61g0sgslk0ax8vr8wcf3x3fb525aw18n6l6fd") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.96.3 (c (n "cranelift-entity") (v "0.96.3") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0k2dd3h7h78m02n3q5i0xw8xh6prc95k2s816ahh292zq3mhprf2") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.96.4 (c (n "cranelift-entity") (v "0.96.4") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19iiqikvdys950xv3x68wd9hqlvmk0hdkx2qllgn4rmv5m19gn4v") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.97.0 (c (n "cranelift-entity") (v "0.97.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jfg1xd2k3d1mp1j4ypz6cw08yyrvb5gdj01465dm439nyag928a") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.97.1 (c (n "cranelift-entity") (v "0.97.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ysxyqajxz0jmlpmkqvmbsb3dsr1lzn6r51b3qvnfhv8nnc52mnn") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.98.0 (c (n "cranelift-entity") (v "0.98.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p993kwxjmp3xlgi7d6wy48pn7mflkb54alcrxri0rs2prbr3wbs") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.98.1 (c (n "cranelift-entity") (v "0.98.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02gal8xpqn8h47azb6r8hxi0x3yajg7j4iwnwh03rwja8g0p6cay") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.99.0 (c (n "cranelift-entity") (v "0.99.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d8qwjghl0lvv4mkpbj0gapkl098fdbmfkm03g0q53hxykd8l8mb") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.99.1 (c (n "cranelift-entity") (v "0.99.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lh5bbnmj13wfcv67i8yjlfvmjpxb73lrnn8lx8lbrjyjcw6l7cx") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.97.2 (c (n "cranelift-entity") (v "0.97.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "067rl8zyqbkwh71la1vn2j1vbrp87nyj9d3sinb6cn8y58sv72lx") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.98.2 (c (n "cranelift-entity") (v "0.98.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "133h1dyg8samzz6gbxy5nx9kblnq488l68mxn4d52si4935mcmjd") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.99.2 (c (n "cranelift-entity") (v "0.99.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nf6qdkm57355ykgshm3dmaysblf2yl6mz2vcsc2fqj0c804813j") (f (quote (("enable-serde" "serde"))))))

(define-public crate-cranelift-entity-0.100.0 (c (n "cranelift-entity") (v "0.100.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0204rff48hky3md80sbrmfzw461cjz5psxh7n7qbzlls39jglczk") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.101.0 (c (n "cranelift-entity") (v "0.101.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0g8s21gjv00xsrc4g30x0r96z7by4lv7yjga39lr624295z6pzn2") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.101.1 (c (n "cranelift-entity") (v "0.101.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1pbr6h7y5myn7v8jhpm5abv68yfqfqn620lp4dgy99xgycy3vwp3") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.100.1 (c (n "cranelift-entity") (v "0.100.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0895yznnx7pxv4i0k575js1ckh2fl1kzvj8pqhzz8wiwzs39nqfc") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.101.2 (c (n "cranelift-entity") (v "0.101.2") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1cjjrlrjq7ry3zfswkq3k88qs0a4ssk5qfc8xp88gzmm7gny45s9") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.101.3 (c (n "cranelift-entity") (v "0.101.3") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1vbi7fp62784b39izwz8swpplb71mm1ndr7nq87r5fp18h81fnxs") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.101.4 (c (n "cranelift-entity") (v "0.101.4") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "154z1k5mnpfmwax09y8yxdcq73fxr30c4p8k6nfpz0pwjqr9lkhb") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.102.0 (c (n "cranelift-entity") (v "0.102.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0p6awqq87jk9v4gwblafmyx5c9rd5rm3mi6zhj6ian4bh3d1m1rg") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.102.1 (c (n "cranelift-entity") (v "0.102.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "14nqjkyfl49yj9n3jjvn03gja89l0pa0k0pqpki4ya016xbmzcpx") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.103.0 (c (n "cranelift-entity") (v "0.103.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1xx3paak3pnsbi99myknazrv7jrjjqpydxzdpilnqxy45x9yxqs4") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.104.0 (c (n "cranelift-entity") (v "0.104.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0zgfkfihwmh9h7nvg35cqc7w9qxrkb3pgyavnj739rldmsa6yyqp") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.104.1 (c (n "cranelift-entity") (v "0.104.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0r8s680z0gy2w81710bahksdcl7r68473d50srsm8s0skcam8rr8") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.105.0 (c (n "cranelift-entity") (v "0.105.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1hp1kx42qm780nv0df7q99616vdk6ch7dh2ssxfriml6d8bqinfz") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.105.1 (c (n "cranelift-entity") (v "0.105.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "06qadwgqav5jv1v47igl73jkagc0hzfr1fsr0k31glqa32axyh3z") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.104.2 (c (n "cranelift-entity") (v "0.104.2") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "07zp2fk8sf4spl5b7m2j1kxricwqyj4qs8g9q1rzsdb21kcbi9ln") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.105.2 (c (n "cranelift-entity") (v "0.105.2") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0hp9y86ymm2ryaqpnsp9mmcqpw48d817rady1561icbm383127m7") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.105.3 (c (n "cranelift-entity") (v "0.105.3") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1jsq40byd4akxhj7b8sh20m89lywhzfyz1wb4idlrp155yj8383f") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.106.0 (c (n "cranelift-entity") (v "0.106.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0djdmqix2nmlaf1rrk00n8ggwjwva49iizmppzfy2g0mkbkpamgq") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.106.1 (c (n "cranelift-entity") (v "0.106.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0pg7f84g8pdwqh09csmh6ljvh3lbvzmvmpgyyp64ygqaf1kh87mg") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.105.4 (c (n "cranelift-entity") (v "0.105.4") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0nykddvd4yw9m0688y7s3kxl95ryc31lv1qkhmzvw30wr9x8i5za") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.106.2 (c (n "cranelift-entity") (v "0.106.2") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1m6462qrf2saq8l4cifyw043j7rkl38mqql7v8agb9kdhd30k4zi") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.104.3 (c (n "cranelift-entity") (v "0.104.3") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1m9d6ng6bf2awri3rf0idmggffa6c9mjgpzvr127lq82c12jbrhq") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.107.0 (c (n "cranelift-entity") (v "0.107.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0fkfdcbm6ha5wpmz1li6dzr1f6274x82jjl7jjbdng5p67wh9gi9") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.107.1 (c (n "cranelift-entity") (v "0.107.1") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0cxn03jfvk8sdlkvqrxnasccyh6c4m0xk0jv02zq2fgmi1jrss3q") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.107.2 (c (n "cranelift-entity") (v "0.107.2") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0m60hnfpm5r0v4rvv7wawn88vfwly2gayr2zwxk2ap565dix1b1j") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.108.0 (c (n "cranelift-entity") (v "0.108.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "0y05hp8xlhidjcx6z2fabvhbmrqw6vn97wl3c9jivl76hg07r3wy") (f (quote (("enable-serde" "serde" "serde_derive"))))))

(define-public crate-cranelift-entity-0.108.1 (c (n "cranelift-entity") (v "0.108.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (o #t) (d #t) (k 0)))) (h "1429004wrpgzn2kny9kbgqnsq73l2sjqxiwkximr1a0cdg9viavy") (f (quote (("enable-serde" "serde" "serde_derive"))))))

