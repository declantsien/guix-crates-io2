(define-module (crates-io cr an cranelift_entity_set) #:use-module (crates-io))

(define-public crate-cranelift_entity_set-0.1.0 (c (n "cranelift_entity_set") (v "0.1.0") (d (list (d (n "cranelift-entity") (r "^0.58") (d #t) (k 0)))) (h "1218qbf461qb9fl3r60s8zp3fj6krdym489h7n41vjgv2yrijf5y")))

(define-public crate-cranelift_entity_set-0.2.0 (c (n "cranelift_entity_set") (v "0.2.0") (d (list (d (n "cranelift-entity") (r "^0.58") (d #t) (k 0)))) (h "15nsagw4pkva4wc09b69cssbb7a13ymgifga8qq549zff8ar06qa")))

(define-public crate-cranelift_entity_set-0.3.0 (c (n "cranelift_entity_set") (v "0.3.0") (d (list (d (n "cranelift-entity") (r "^0.58") (d #t) (k 0)))) (h "10p97jjli44hgf1kbf41ppfcpbra46gqnz6fv4mlnsa70sccpfh1")))

