(define-module (crates-io cr an cranc) #:use-module (crates-io))

(define-public crate-cranc-0.1.0 (c (n "cranc") (v "0.1.0") (h "1mws81fqwz34igzg605ypglph7kl9f4ax74fzdj3xbb02vf230xr")))

(define-public crate-cranc-0.1.1 (c (n "cranc") (v "0.1.1") (h "1mrjcy8z7na5sp47dx46wd935daqfmj8pfi6gg7b8xpsdjx5mwwv")))

