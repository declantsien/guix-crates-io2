(define-module (crates-io cr an crankit-input) #:use-module (crates-io))

(define-public crate-crankit-input-0.1.0 (c (n "crankit-input") (v "0.1.0") (d (list (d (n "playdate-sys") (r "^0.2.11") (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "0k4zg6sr7pzxn1hzil1r1mbg142kx4mq74pmxiwp2mcsc3lllan8")))

(define-public crate-crankit-input-0.2.0 (c (n "crankit-input") (v "0.2.0") (d (list (d (n "playdate-sys") (r "^0.2.11") (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "0wa1rswbsgq7nqgplka5myg254iq4nqdrih9wz95qmpmnnxi0xxv")))

(define-public crate-crankit-input-0.3.0 (c (n "crankit-input") (v "0.3.0") (d (list (d (n "playdate-sys-v02") (r "^0.2.11") (o #t) (k 0) (p "playdate-sys")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "1xsbxi02zg6prnya5a6z4lw4cqx5hyydwf0p226b0832xb8kyw8w") (f (quote (("default" "playdate-sys-v02")))) (s 2) (e (quote (("playdate-sys-v02" "dep:playdate-sys-v02"))))))

(define-public crate-crankit-input-0.3.1 (c (n "crankit-input") (v "0.3.1") (d (list (d (n "playdate-sys-v02") (r "^0.2.11") (o #t) (k 0) (p "playdate-sys")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "0ldg11bnsk7q83km2pd7y19jhbsx87r1a4z3jr8343lml1927c08") (f (quote (("default" "playdate-sys-v02")))) (s 2) (e (quote (("playdate-sys-v02" "dep:playdate-sys-v02"))))))

(define-public crate-crankit-input-0.3.2 (c (n "crankit-input") (v "0.3.2") (d (list (d (n "playdate-sys-v02") (r "^0.2.11") (o #t) (k 0) (p "playdate-sys")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "1nv2s1kr2ghjbrvpxcn4hybz1kafgpvziz8gbfk1rsi0a13c4zyg") (f (quote (("default" "playdate-sys-v02")))) (s 2) (e (quote (("playdate-sys-v02" "dep:playdate-sys-v02"))))))

(define-public crate-crankit-input-0.4.0 (c (n "crankit-input") (v "0.4.0") (d (list (d (n "playdate-sys-v02") (r "^0.2.11") (o #t) (k 0) (p "playdate-sys")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "00p0bmshg3s3h5fds332n6wcv71yyj8f326ayi3jqdcrp9a7nfrv") (f (quote (("default")))) (s 2) (e (quote (("playdate-sys-v02" "dep:playdate-sys-v02"))))))

