(define-module (crates-io cr an craneliftc) #:use-module (crates-io))

(define-public crate-craneliftc-0.1.0 (c (n "craneliftc") (v "0.1.0") (d (list (d (n "cranelift") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0mjshhqj9j9naqgblxy5nx8vbby03dafd1pmi8ygaxiyhfgp0gz9")))

(define-public crate-craneliftc-0.2.0 (c (n "craneliftc") (v "0.2.0") (d (list (d (n "cranelift") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "18l4b6js4n5zb4adnk7dx775laflfpdnx6fq7l0ax085bya4i5ca")))

(define-public crate-craneliftc-0.3.0 (c (n "craneliftc") (v "0.3.0") (d (list (d (n "cranelift") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0d2sgwrr0yr9zkkhfhp8z9d90m76xz0bnh7qvairyp1lyn7pr51f")))

(define-public crate-craneliftc-0.4.0 (c (n "craneliftc") (v "0.4.0") (d (list (d (n "cranelift") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "17gzzrxz7p5jg5nwgfylk117n12yzhkn5f94gipywwds77fwx3pv")))

(define-public crate-craneliftc-0.5.0 (c (n "craneliftc") (v "0.5.0") (d (list (d (n "cranelift") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0ppqkz3bgjrlkd7dfnq52453n66zpdbzc3ia4z5fk4hwb6348dzb")))

(define-public crate-craneliftc-0.5.1 (c (n "craneliftc") (v "0.5.1") (d (list (d (n "cranelift") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "10mjqmcmpjcfpi6nrnk4lmwp0gd6dm6fghkwb8v90xm670fhx48w")))

