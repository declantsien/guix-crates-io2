(define-module (crates-io cr an cranenum) #:use-module (crates-io))

(define-public crate-cranenum-0.0.1 (c (n "cranenum") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "11xs9q89z25lr83ipwnsrcq7hxdvb6b5wkbcmhjp8aif6x2kaqkr")))

