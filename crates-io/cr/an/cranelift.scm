(define-module (crates-io cr an cranelift) #:use-module (crates-io))

(define-public crate-cranelift-0.14.0 (c (n "cranelift") (v "0.14.0") (d (list (d (n "cranelift-codegen") (r "^0.14.0") (k 0)) (d (n "cranelift-frontend") (r "^0.14.0") (k 0)))) (h "02gix4bjyk2xcrdwb7f2yyjfvs2pkn8v07vy6w1mp6k0w9aqvgf8") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.15.0 (c (n "cranelift") (v "0.15.0") (d (list (d (n "cranelift-codegen") (r "^0.15.0") (k 0)) (d (n "cranelift-frontend") (r "^0.15.0") (k 0)))) (h "0n8rfwiv9xy309h9xz57mhi3y5r7dy0yivckcaghkw39nnmm2dws") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.16.0 (c (n "cranelift") (v "0.16.0") (d (list (d (n "cranelift-codegen") (r "^0.16.0") (k 0)) (d (n "cranelift-frontend") (r "^0.16.0") (k 0)))) (h "02xk4gqam4qlrlc6k72wyj90ck3xlywnggh1j9fwp3n54fgwqv9j") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.16.1 (c (n "cranelift") (v "0.16.1") (d (list (d (n "cranelift-codegen") (r "^0.16.1") (k 0)) (d (n "cranelift-frontend") (r "^0.16.1") (k 0)))) (h "195572pk2lwx38m7xx5s534bnnd5ffs0gszln34z14183g230gy5") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.17.0 (c (n "cranelift") (v "0.17.0") (d (list (d (n "cranelift-codegen") (r "^0.17.0") (k 0)) (d (n "cranelift-frontend") (r "^0.17.0") (k 0)))) (h "11q8lfhfp7c99bm1v0vl5pkskx961il09silw763ajxcwrk2l56s") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.18.1 (c (n "cranelift") (v "0.18.1") (d (list (d (n "cranelift-codegen") (r "^0.18.1") (k 0)) (d (n "cranelift-frontend") (r "^0.18.1") (k 0)))) (h "0hx0b543qp04wiz15rihl53jq9mbi9y1cy5rsa5s18rippwcs1cv") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.19.0 (c (n "cranelift") (v "0.19.0") (d (list (d (n "cranelift-codegen") (r "^0.19.0") (k 0)) (d (n "cranelift-frontend") (r "^0.19.0") (k 0)))) (h "02vch9203ig3dyws13biyr3fdpg6ylw1xc1wdaf7wlnkrrlz7yds") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.20.0 (c (n "cranelift") (v "0.20.0") (d (list (d (n "cranelift-codegen") (r "^0.20.0") (k 0)) (d (n "cranelift-frontend") (r "^0.20.0") (k 0)))) (h "0djp195gawvzfmz87n057r3hpm7m7jm7yxl56vmmv2939lypzwqf") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.21.0 (c (n "cranelift") (v "0.21.0") (d (list (d (n "cranelift-codegen") (r "^0.21.0") (k 0)) (d (n "cranelift-frontend") (r "^0.21.0") (k 0)))) (h "027kg5z1nngca4drq96r7q85nsdw1ix60ayiyvq4nx58pflqhk26") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.21.1 (c (n "cranelift") (v "0.21.1") (d (list (d (n "cranelift-codegen") (r "^0.21.1") (k 0)) (d (n "cranelift-frontend") (r "^0.21.1") (k 0)))) (h "12254w2avhfs54mxfa2psc7k66py8hvafwgckkifdcw1zm0p56ag") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.22.0 (c (n "cranelift") (v "0.22.0") (d (list (d (n "cranelift-codegen") (r "^0.22.0") (k 0)) (d (n "cranelift-frontend") (r "^0.22.0") (k 0)))) (h "0zli01sb6nzsnjlbyryd3zwv3010g7yhfzp6xcdrnx60iq7h5y8y") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.23.0 (c (n "cranelift") (v "0.23.0") (d (list (d (n "cranelift-codegen") (r "^0.23.0") (k 0)) (d (n "cranelift-frontend") (r "^0.23.0") (k 0)))) (h "02p43aiyzcz6s98znnk3hfgmrfrym521272vrlxqvgqmsirrp2jy") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.24.0 (c (n "cranelift") (v "0.24.0") (d (list (d (n "cranelift-codegen") (r "^0.24.0") (k 0)) (d (n "cranelift-frontend") (r "^0.24.0") (k 0)))) (h "0dq1vnjw65jyc3c5k4bljr3z5sivdaxqcpqgz94v7nn0glkx4x04") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.25.0 (c (n "cranelift") (v "0.25.0") (d (list (d (n "cranelift-codegen") (r "^0.25.0") (k 0)) (d (n "cranelift-frontend") (r "^0.25.0") (k 0)))) (h "1rxiljw3d0n5zhiapwhlb1a14z2a07mh5hi3hgvmrgc2k5mvhggk") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.26.0 (c (n "cranelift") (v "0.26.0") (d (list (d (n "cranelift-codegen") (r "^0.26.0") (k 0)) (d (n "cranelift-frontend") (r "^0.26.0") (k 0)))) (h "08px1xq3s2x3jb10l7sg4nnz8p9pvxj9dh82niysyljp2zlfn9wr") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.27.0 (c (n "cranelift") (v "0.27.0") (d (list (d (n "cranelift-codegen") (r "^0.27.0") (k 0)) (d (n "cranelift-frontend") (r "^0.27.0") (k 0)))) (h "1nnai7cdf2a0p0jzs7j9scx2pbl3ipn5d0cysvgflf0shm7zdkq0") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.28.0 (c (n "cranelift") (v "0.28.0") (d (list (d (n "cranelift-codegen") (r "^0.28.0") (k 0)) (d (n "cranelift-frontend") (r "^0.28.0") (k 0)))) (h "0n53a49c2gihss4r5cx1pif4h7qajdf72yrc72870grirwzl4fhb") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.29.0 (c (n "cranelift") (v "0.29.0") (d (list (d (n "cranelift-codegen") (r "^0.29.0") (k 0)) (d (n "cranelift-frontend") (r "^0.29.0") (k 0)))) (h "09yzkc8wwn4vqpi2xsa71zlair7jjy9vhm09v2mrlqak12ng9xpl") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.30.0 (c (n "cranelift") (v "0.30.0") (d (list (d (n "cranelift-codegen") (r "^0.30.0") (k 0)) (d (n "cranelift-frontend") (r "^0.30.0") (k 0)))) (h "08mzqnngdf1qhrzc80r6bmhmav2rvzj0dmcd9dfr4y55sgp3cpqk") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.31.0 (c (n "cranelift") (v "0.31.0") (d (list (d (n "cranelift-codegen") (r "^0.31.0") (k 0)) (d (n "cranelift-frontend") (r "^0.31.0") (k 0)))) (h "1jg7h11pl4frwz4394lhyf6rcl7xap5mx50im5w88wbfyf5bf9c1") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.32.0 (c (n "cranelift") (v "0.32.0") (d (list (d (n "cranelift-codegen") (r "^0.32.0") (k 0)) (d (n "cranelift-frontend") (r "^0.32.0") (k 0)))) (h "09wlb60wfkrqaxzcw90wddpil6r538w50gzsl90di0ml2mirqm26") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.33.0 (c (n "cranelift") (v "0.33.0") (d (list (d (n "cranelift-codegen") (r "^0.33.0") (k 0)) (d (n "cranelift-frontend") (r "^0.33.0") (k 0)))) (h "100lywy9nkxp9zq6js78kdz2lfswcydqs119vmjix82sab1bxly9") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.34.0 (c (n "cranelift") (v "0.34.0") (d (list (d (n "cranelift-codegen") (r "^0.34.0") (k 0)) (d (n "cranelift-frontend") (r "^0.34.0") (k 0)))) (h "0xbqmvg3zsms2hdlpmzi74ccdh7w74qa06zqgpx0i6xshyy7l8mk") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.35.0 (c (n "cranelift") (v "0.35.0") (d (list (d (n "cranelift-codegen") (r "^0.35.0") (k 0)) (d (n "cranelift-frontend") (r "^0.35.0") (k 0)))) (h "0w4y51pf4xxjzjbbkqaiwh1k51v5mzrgzi5q0xrifn012qinmx6z") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.36.0 (c (n "cranelift") (v "0.36.0") (d (list (d (n "cranelift-codegen") (r "^0.36.0") (k 0)) (d (n "cranelift-frontend") (r "^0.36.0") (k 0)))) (h "1bfbidwd1k2692f3j2k2bjbhg633bgrcjh6mbb1ikym64ch9ki29") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.37.0 (c (n "cranelift") (v "0.37.0") (d (list (d (n "cranelift-codegen") (r "^0.37.0") (k 0)) (d (n "cranelift-frontend") (r "^0.37.0") (k 0)))) (h "02p9s2r7qadxc860zdd5x666wqpkqb2b92fal4vg51yps1wjd1p1") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.38.0 (c (n "cranelift") (v "0.38.0") (d (list (d (n "cranelift-codegen") (r "^0.38.0") (k 0)) (d (n "cranelift-frontend") (r "^0.38.0") (k 0)))) (h "18rvi1rircaixdzzxcnn3c8dv0xhvng17ymwl3wspn2xcn1klxy0") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.39.0 (c (n "cranelift") (v "0.39.0") (d (list (d (n "cranelift-codegen") (r "^0.39.0") (k 0)) (d (n "cranelift-frontend") (r "^0.39.0") (k 0)))) (h "0svvi3sgq8850sgwzh3fkglqw76y6wx7lzmy4jzrn1bply37vwx7") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.40.0 (c (n "cranelift") (v "0.40.0") (d (list (d (n "cranelift-codegen") (r "^0.40.0") (k 0)) (d (n "cranelift-frontend") (r "^0.40.0") (k 0)))) (h "0ss664218j370b1fwqmxjavdi9x9z6ks03hhy4s3z4fg49jcb5nb") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.41.0 (c (n "cranelift") (v "0.41.0") (d (list (d (n "cranelift-codegen") (r "^0.41.0") (k 0)) (d (n "cranelift-frontend") (r "^0.41.0") (k 0)))) (h "0jfg61g21kqyy1hppank33zvwsg6hqns4c5453ga99kj6awra32y") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.42.0 (c (n "cranelift") (v "0.42.0") (d (list (d (n "cranelift-codegen") (r "^0.42.0") (k 0)) (d (n "cranelift-frontend") (r "^0.42.0") (k 0)))) (h "1j42bvgbazjhiyi5i5nkv3fg5568nzsgy4skkryvnj9g0lc4b192") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.43.0 (c (n "cranelift") (v "0.43.0") (d (list (d (n "cranelift-codegen") (r "^0.43.0") (k 0)) (d (n "cranelift-frontend") (r "^0.43.0") (k 0)))) (h "0phrrpd20v6hxdx2hm3qa60bg4zfi102c65cn3g51npw7cxwcqlq") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.43.1 (c (n "cranelift") (v "0.43.1") (d (list (d (n "cranelift-codegen") (r "^0.43.1") (k 0)) (d (n "cranelift-frontend") (r "^0.43.1") (k 0)))) (h "1hnqhaglmj9bypsfdl09j7rqj6gyy4f51yk4k2b3pm8gb9ymwvbb") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.44.0 (c (n "cranelift") (v "0.44.0") (d (list (d (n "cranelift-codegen") (r "^0.44.0") (k 0)) (d (n "cranelift-frontend") (r "^0.44.0") (k 0)))) (h "1q1k13k3had2ckgd080p64aym2abx6465yvczpfsk37a1nzic52s") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.45.0 (c (n "cranelift") (v "0.45.0") (d (list (d (n "cranelift-codegen") (r "^0.45.0") (k 0)) (d (n "cranelift-frontend") (r "^0.45.0") (k 0)))) (h "14jj0i3q7qd1c2nj5k3dry5naly80vizsx0fr1m2ghkcr7fhyvcc") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.46.0 (c (n "cranelift") (v "0.46.0") (d (list (d (n "cranelift-codegen") (r "^0.46.0") (k 0)) (d (n "cranelift-frontend") (r "^0.46.0") (k 0)))) (h "181cdmx3jn2qzzm63i3bxkgyx4xmjq4vddfrrz5zgkdc9wl9jgy4") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.46.1 (c (n "cranelift") (v "0.46.1") (d (list (d (n "cranelift-codegen") (r "^0.46.1") (k 0)) (d (n "cranelift-frontend") (r "^0.46.1") (k 0)))) (h "17l9g4isvv0b3ryspmix87xhz3881jzc7k2ngfl74ghirn34ailv") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.47.0 (c (n "cranelift") (v "0.47.0") (d (list (d (n "cranelift-codegen") (r "^0.47.0") (k 0)) (d (n "cranelift-frontend") (r "^0.47.0") (k 0)))) (h "0a60gnxy57lq4aam0ilgi2xrirgfyk0ilg28iarjm76dbc1y4lbn") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.48.0 (c (n "cranelift") (v "0.48.0") (d (list (d (n "cranelift-codegen") (r "^0.48.0") (k 0)) (d (n "cranelift-frontend") (r "^0.48.0") (k 0)))) (h "00v4y2cdn45hw777zr45k8pa4fi3ka3h29jr8myvifcsjf60rm2w") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.49.0 (c (n "cranelift") (v "0.49.0") (d (list (d (n "cranelift-codegen") (r "^0.49.0") (k 0)) (d (n "cranelift-frontend") (r "^0.49.0") (k 0)))) (h "1bfb8z02rymf3k3x9v2lifk002cbwbpmpwvimshv11j0mqi2l8b7") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.50.0 (c (n "cranelift") (v "0.50.0") (d (list (d (n "cranelift-codegen") (r "^0.50.0") (k 0)) (d (n "cranelift-frontend") (r "^0.50.0") (k 0)))) (h "0vhjzaidh3py8jgia9n4bw1ri2z0y8zzyvlfqypjh18r09flfdz1") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.51.0 (c (n "cranelift") (v "0.51.0") (d (list (d (n "cranelift-codegen") (r "^0.51.0") (k 0)) (d (n "cranelift-frontend") (r "^0.51.0") (k 0)))) (h "11l77hagzbc0m47xqais0igssh8y702ar957z3bbyyqmpncgdwfi") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.52.0 (c (n "cranelift") (v "0.52.0") (d (list (d (n "cranelift-codegen") (r "^0.52.0") (k 0)) (d (n "cranelift-frontend") (r "^0.52.0") (k 0)))) (h "00cp820ikxcbfh1dh6n8r28ggya3z8vcv7n35pmffqzzdr5bdwzh") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.53.0 (c (n "cranelift") (v "0.53.0") (d (list (d (n "cranelift-codegen") (r "^0.53.0") (k 0)) (d (n "cranelift-frontend") (r "^0.53.0") (k 0)))) (h "1c2xrkcijj1k3gxbzr0va75ycbknf1ralvcpzdvazbycqqg5vqkr") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.54.0 (c (n "cranelift") (v "0.54.0") (d (list (d (n "cranelift-codegen") (r "^0.54.0") (k 0)) (d (n "cranelift-frontend") (r "^0.54.0") (k 0)))) (h "0rik7llhgvlrwwkk3lvbphm7qs52i0k2ipfn4ab2xf1802gwx2aw") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.55.0 (c (n "cranelift") (v "0.55.0") (d (list (d (n "cranelift-codegen") (r "^0.55.0") (k 0)) (d (n "cranelift-frontend") (r "^0.55.0") (k 0)))) (h "1lrpafa0bxdmpfi4190p3lk5skhhhw8vjb11jsjk478hm79hnydn") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.56.0 (c (n "cranelift") (v "0.56.0") (d (list (d (n "cranelift-codegen") (r "^0.56.0") (k 0)) (d (n "cranelift-frontend") (r "^0.56.0") (k 0)))) (h "0m6r69vspamd5n5awqflbxw75kzav17s0cs8jvwgipcdmzj7xb89") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.58.0 (c (n "cranelift") (v "0.58.0") (d (list (d (n "cranelift-codegen") (r "^0.58.0") (k 0)) (d (n "cranelift-frontend") (r "^0.58.0") (k 0)))) (h "1ls5xddp46ywqibg0h0853ssfsns09dm63rimq4qca8gd83arw4m") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.59.0 (c (n "cranelift") (v "0.59.0") (d (list (d (n "cranelift-codegen") (r "^0.59.0") (k 0)) (d (n "cranelift-frontend") (r "^0.59.0") (k 0)))) (h "1fk4c18ryjhymhmpkrvirs6v3q2fccc5ifcdjcssyiwjxrq8ayqp") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.60.0 (c (n "cranelift") (v "0.60.0") (d (list (d (n "cranelift-codegen") (r "^0.60.0") (k 0)) (d (n "cranelift-frontend") (r "^0.60.0") (k 0)))) (h "10idqysxbf0pbixb8sc5p03iavixw694r0622wp5hz0311ppnv7k") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.61.0 (c (n "cranelift") (v "0.61.0") (d (list (d (n "cranelift-codegen") (r "^0.61.0") (k 0)) (d (n "cranelift-frontend") (r "^0.61.0") (k 0)))) (h "0a98b6r4sk19h5ixnklvfs5b5dpwwygpc2h9r2qg2awm5ilrisym") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.62.0 (c (n "cranelift") (v "0.62.0") (d (list (d (n "cranelift-codegen") (r "^0.62.0") (k 0)) (d (n "cranelift-frontend") (r "^0.62.0") (k 0)))) (h "08yi97dpisady6bvvxjjk73mf99iaz454ygbjp89pq25a532r6xf") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.63.0 (c (n "cranelift") (v "0.63.0") (d (list (d (n "cranelift-codegen") (r "^0.63.0") (k 0)) (d (n "cranelift-frontend") (r "^0.63.0") (k 0)))) (h "0k4a79mq3770sjhpp20k8i9ndp4mrn8n4qxmz8qwkxl7cmz51kz1") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.64.0 (c (n "cranelift") (v "0.64.0") (d (list (d (n "cranelift-codegen") (r "^0.64.0") (k 0)) (d (n "cranelift-frontend") (r "^0.64.0") (k 0)))) (h "0fjvhjpw9d3bi257fj1fkknb1lb89jpwfvsm4smxcjrmrhpwzl4d") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.65.0 (c (n "cranelift") (v "0.65.0") (d (list (d (n "cranelift-codegen") (r "^0.65.0") (k 0)) (d (n "cranelift-frontend") (r "^0.65.0") (k 0)))) (h "1xb80vgf0wc2wiz462xwrk8j1q1qfw828x4yhxdadxs7lc4mb6sb") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.66.0 (c (n "cranelift") (v "0.66.0") (d (list (d (n "cranelift-codegen") (r "^0.66.0") (k 0)) (d (n "cranelift-frontend") (r "^0.66.0") (k 0)))) (h "0n2ms4wpz58vl2cghc5dc2v6yhi6jysj2rf8jx2vdyx7bn91rhjg") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.67.0 (c (n "cranelift") (v "0.67.0") (d (list (d (n "cranelift-codegen") (r "^0.67.0") (k 0)) (d (n "cranelift-frontend") (r "^0.67.0") (k 0)))) (h "0yyzv713gdp77nzi51yzw462912daix4m9bfsz5n8y6h7pwjlp1w") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.68.0 (c (n "cranelift") (v "0.68.0") (d (list (d (n "cranelift-codegen") (r "^0.68.0") (k 0)) (d (n "cranelift-frontend") (r "^0.68.0") (k 0)))) (h "0wq2k7r5519zbl57m5y5wjyjk95sahwvifysc6lwjnqlqn4nys30") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.69.0 (c (n "cranelift") (v "0.69.0") (d (list (d (n "cranelift-codegen") (r "^0.69.0") (k 0)) (d (n "cranelift-frontend") (r "^0.69.0") (k 0)))) (h "1dv2vry4nppqkxqhvn0jfaxbhdcgja5a8nbnji8296wbfjyj40wi") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.70.0 (c (n "cranelift") (v "0.70.0") (d (list (d (n "cranelift-codegen") (r "^0.70.0") (k 0)) (d (n "cranelift-frontend") (r "^0.70.0") (k 0)))) (h "00ng8lrfnj9h8wh4r9vzy97i7sksw2qb8cngp5gsf5pd2qmh75wb") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.71.0 (c (n "cranelift") (v "0.71.0") (d (list (d (n "cranelift-codegen") (r "^0.71.0") (k 0)) (d (n "cranelift-frontend") (r "^0.71.0") (k 0)))) (h "13qf0lc5jly5a4haal9qwzhyc3drwmq1542b1jbqvr4hivsaja0x") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.72.0 (c (n "cranelift") (v "0.72.0") (d (list (d (n "cranelift-codegen") (r "^0.72.0") (k 0)) (d (n "cranelift-frontend") (r "^0.72.0") (k 0)))) (h "1zfmsldviisa61m5kh3jaf6y959vpjn33am1dnn70619srppvby7") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.73.0 (c (n "cranelift") (v "0.73.0") (d (list (d (n "cranelift-codegen") (r "^0.73.0") (k 0)) (d (n "cranelift-frontend") (r "^0.73.0") (k 0)))) (h "04zn235bk2y9vgi4sqdq04qa15y3w3678sl1bmm7xb46fcbr2mlm") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.74.0 (c (n "cranelift") (v "0.74.0") (d (list (d (n "cranelift-codegen") (r "^0.74.0") (k 0)) (d (n "cranelift-frontend") (r "^0.74.0") (k 0)))) (h "1v21vxy3ibqwgy55qpgm1yd9xj4qssnqgjcmx634rfwc8gh55x7i") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.73.1 (c (n "cranelift") (v "0.73.1") (d (list (d (n "cranelift-codegen") (r "^0.73.1") (k 0)) (d (n "cranelift-frontend") (r "^0.73.1") (k 0)))) (h "0kwyds740jwc42h89yr6rd7n4lynd48qjllfs3jyz35f6pbs065s") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.75.0 (c (n "cranelift") (v "0.75.0") (d (list (d (n "cranelift-codegen") (r "^0.75.0") (k 0)) (d (n "cranelift-frontend") (r "^0.75.0") (k 0)))) (h "1jal5mpn44d5qac4h9a31ac7n5fly8mp20wn98qjbdxp91mz0hn7") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.76.0 (c (n "cranelift") (v "0.76.0") (d (list (d (n "cranelift-codegen") (r "^0.76.0") (k 0)) (d (n "cranelift-frontend") (r "^0.76.0") (k 0)))) (h "1z1x40v993k9768j29f66dx5irb3j1rv0dcs69m3c3ql7nd676gl") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.77.0 (c (n "cranelift") (v "0.77.0") (d (list (d (n "cranelift-codegen") (r "^0.77.0") (k 0)) (d (n "cranelift-frontend") (r "^0.77.0") (k 0)))) (h "0irhhjg8y5021j31hy8mlcv1i1dzaf75gzjnnx7knakr3qnm6lqp") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.78.0 (c (n "cranelift") (v "0.78.0") (d (list (d (n "cranelift-codegen") (r "^0.78.0") (k 0)) (d (n "cranelift-frontend") (r "^0.78.0") (k 0)))) (h "08pgg2x0bcnsaadynjs3cfc4h73qkcdp3vwbrnn1xmjmbkbbmwrk") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.79.0 (c (n "cranelift") (v "0.79.0") (d (list (d (n "cranelift-codegen") (r "^0.79.0") (k 0)) (d (n "cranelift-frontend") (r "^0.79.0") (k 0)))) (h "0f66xws1gca0jr6nfxf7ri7gdjlnii9r0744hsbny32mrlnva6dk") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.79.1 (c (n "cranelift") (v "0.79.1") (d (list (d (n "cranelift-codegen") (r "^0.79.1") (k 0)) (d (n "cranelift-frontend") (r "^0.79.1") (k 0)))) (h "0j53d196kmknx4gdgnrhjdjrjv72z631bs7j697gbz692xgbjpri") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.80.0 (c (n "cranelift") (v "0.80.0") (d (list (d (n "cranelift-codegen") (r "^0.80.0") (k 0)) (d (n "cranelift-frontend") (r "^0.80.0") (k 0)))) (h "18ri19pld8h32yg7hp4qjhglcqyp9ic0xrp99x63r0ln700f9n9s") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.81.0 (c (n "cranelift") (v "0.81.0") (d (list (d (n "cranelift-codegen") (r "^0.81.0") (k 0)) (d (n "cranelift-frontend") (r "^0.81.0") (k 0)))) (h "1i8nay47ns5p2314y99pvc3dxvq54c3b7vvf6j4jiij0n785ia30") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.81.1 (c (n "cranelift") (v "0.81.1") (d (list (d (n "cranelift-codegen") (r "^0.81.1") (k 0)) (d (n "cranelift-frontend") (r "^0.81.1") (k 0)))) (h "16lpwhsrh3ih0kqq4z4s3s99iaw322brll6sipmv36ygx1mqg9a1") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.80.1 (c (n "cranelift") (v "0.80.1") (d (list (d (n "cranelift-codegen") (r "^0.80.1") (k 0)) (d (n "cranelift-frontend") (r "^0.80.1") (k 0)))) (h "1fj3cmyxrpy9lflrp4n02wrcdm2r63w0q0xka2j34b23n3996f4q") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.82.0 (c (n "cranelift") (v "0.82.0") (d (list (d (n "cranelift-codegen") (r "^0.82.0") (k 0)) (d (n "cranelift-frontend") (r "^0.82.0") (k 0)))) (h "0lif8zza1a8c33d6rjdgpdf8qqjivqrpprfjbpps1985idlsqvd0") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.82.1 (c (n "cranelift") (v "0.82.1") (d (list (d (n "cranelift-codegen") (r "^0.82.1") (k 0)) (d (n "cranelift-frontend") (r "^0.82.1") (k 0)))) (h "0gqkdkmjnkm7zl4g32qrf8cdk2g2zdjb873sdmsdbq8q47f4lhlh") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.82.2 (c (n "cranelift") (v "0.82.2") (d (list (d (n "cranelift-codegen") (r "^0.82.2") (k 0)) (d (n "cranelift-frontend") (r "^0.82.2") (k 0)))) (h "1jnhsqkyqhr58mnkm4rbxpzhxpvmkx144y54ssimsn7v06hlp61x") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.81.2 (c (n "cranelift") (v "0.81.2") (d (list (d (n "cranelift-codegen") (r "^0.81.2") (k 0)) (d (n "cranelift-frontend") (r "^0.81.2") (k 0)))) (h "0ihbkgzwp17y6zyc2n86l8c40r9zqcbchc4d2xlmr88klga3vnxc") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.82.3 (c (n "cranelift") (v "0.82.3") (d (list (d (n "cranelift-codegen") (r "^0.82.3") (k 0)) (d (n "cranelift-frontend") (r "^0.82.3") (k 0)))) (h "1pjpqjmq3p9lwxrqhsj888d802mzg6bsvl1csrzxz7y6g6k3z9rf") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.83.0 (c (n "cranelift") (v "0.83.0") (d (list (d (n "cranelift-codegen") (r "^0.83.0") (k 0)) (d (n "cranelift-frontend") (r "^0.83.0") (k 0)))) (h "17a9839g457lcjxy8581if44il41cqhmwhgpcrxn3jd9zxs1zd7i") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.84.0 (c (n "cranelift") (v "0.84.0") (d (list (d (n "cranelift-codegen") (r "^0.84.0") (k 0)) (d (n "cranelift-frontend") (r "^0.84.0") (k 0)))) (h "1w55whdvij6hk52jy7ms6xi5vgsf6dylkz43kf9lwz2nmpbhncgh") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.85.0 (c (n "cranelift") (v "0.85.0") (d (list (d (n "cranelift-codegen") (r "^0.85.0") (k 0)) (d (n "cranelift-frontend") (r "^0.85.0") (k 0)))) (h "0i94k3mwhrz9y8wpvvadjg31n476mki8y6p8c3l3y7nfdv8wqkj0") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.85.1 (c (n "cranelift") (v "0.85.1") (d (list (d (n "cranelift-codegen") (r "^0.85.1") (k 0)) (d (n "cranelift-frontend") (r "^0.85.1") (k 0)))) (h "07ci6cdq3ycbnsiji5zlas852xfz9jiv5zbypd8fn7fll2vcqqpc") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.86.0 (c (n "cranelift") (v "0.86.0") (d (list (d (n "cranelift-codegen") (r "^0.86.0") (k 0)) (d (n "cranelift-frontend") (r "^0.86.0") (k 0)))) (h "1417a3s1xz4szynl4sw6ik8wjr5hxw4vza6f3ny8f7ng2xcmn06r") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.85.2 (c (n "cranelift") (v "0.85.2") (d (list (d (n "cranelift-codegen") (r "^0.85.2") (k 0)) (d (n "cranelift-frontend") (r "^0.85.2") (k 0)))) (h "1hm6q1akbjd8r2zmjn59822rr22cgfwfnzb7n49m5c9b1wb2xvv1") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.85.3 (c (n "cranelift") (v "0.85.3") (d (list (d (n "cranelift-codegen") (r "^0.85.3") (k 0)) (d (n "cranelift-frontend") (r "^0.85.3") (k 0)))) (h "1m4mwi0m9jcdy7fjn5hrp61qsinfl6jih74xh4lz4vf68gx17rd3") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.86.1 (c (n "cranelift") (v "0.86.1") (d (list (d (n "cranelift-codegen") (r "^0.86.1") (k 0)) (d (n "cranelift-frontend") (r "^0.86.1") (k 0)))) (h "0j3lhgiphjgg1n9r1vrz29hr600q3f4xdxkx11rv6kj572kvp5mx") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.87.0 (c (n "cranelift") (v "0.87.0") (d (list (d (n "cranelift-codegen") (r "^0.87.0") (k 0)) (d (n "cranelift-frontend") (r "^0.87.0") (k 0)))) (h "0w8q59llbig0dw40f0z98pa3fc4bzrpjlr6kn1jmyrllfxihhbwv") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.87.1 (c (n "cranelift") (v "0.87.1") (d (list (d (n "cranelift-codegen") (r "^0.87.1") (k 0)) (d (n "cranelift-frontend") (r "^0.87.1") (k 0)))) (h "1hcmxhl1bzhd4q050s6gwwzs4m2wrskq244l9h432wkdh1vg6gap") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.88.0 (c (n "cranelift") (v "0.88.0") (d (list (d (n "cranelift-codegen") (r "^0.88.0") (k 0)) (d (n "cranelift-frontend") (r "^0.88.0") (k 0)))) (h "1zj8jwmk04xsh96h4w0acdmczfl6q1fpigp1y2izra259m596aym") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.88.1 (c (n "cranelift") (v "0.88.1") (d (list (d (n "cranelift-codegen") (r "^0.88.1") (k 0)) (d (n "cranelift-frontend") (r "^0.88.1") (k 0)))) (h "17kiqqh7n7n9x3ni2zgq0p80vdpv5zw8vs3sdw8y0ldgqpyscis5") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.89.0 (c (n "cranelift") (v "0.89.0") (d (list (d (n "cranelift-codegen") (r "^0.89.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.89.0") (d #t) (k 0)))) (h "02aavyr2g9j5vlrvhqznmpn6db5cc0q03wg1bbmjy18lfdlvcp1d") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.89.1 (c (n "cranelift") (v "0.89.1") (d (list (d (n "cranelift-codegen") (r "^0.89.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.89.1") (d #t) (k 0)))) (h "0315rmsza0nr1qj6nc093sh68c0l9qiyl2k7kr5zag8a7bc9l5s2") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.89.2 (c (n "cranelift") (v "0.89.2") (d (list (d (n "cranelift-codegen") (r "^0.89.2") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.89.2") (d #t) (k 0)))) (h "180dw6gl4lxxd047wzl0n9mp6hdvc5aaxwlbwdpb2khwa6657mx3") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.88.2 (c (n "cranelift") (v "0.88.2") (d (list (d (n "cranelift-codegen") (r "^0.88.2") (k 0)) (d (n "cranelift-frontend") (r "^0.88.2") (k 0)))) (h "1a4vnyzxgxgcdvnzbc0b5ain48mc17vfw4v8rs76mha380b0q6za") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.90.0 (c (n "cranelift") (v "0.90.0") (d (list (d (n "cranelift-codegen") (r "^0.90.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.90.0") (d #t) (k 0)))) (h "1y38b49f843n0ipn085ahsc3mkjzaxd3crczxvkxfgnizk8hiiwy") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.90.1 (c (n "cranelift") (v "0.90.1") (d (list (d (n "cranelift-codegen") (r "^0.90.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.90.1") (d #t) (k 0)))) (h "14ikjdm8yka5hh7wazsakhrb8w9pi4ccbqb3djr6m4bvq2k43qvx") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.91.0 (c (n "cranelift") (v "0.91.0") (d (list (d (n "cranelift-codegen") (r "^0.91.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.91.0") (d #t) (k 0)))) (h "1v07vv1i5xl654k7cry9ycwyq1crfi3hnlc9xvxmapcxkkgq481d") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.92.0 (c (n "cranelift") (v "0.92.0") (d (list (d (n "cranelift-codegen") (r "^0.92.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.92.0") (d #t) (k 0)))) (h "0d567xd4x6q45jc7v9yra14lc941xdv8xzfxiin9pi8q3cp71whd") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.93.0 (c (n "cranelift") (v "0.93.0") (d (list (d (n "cranelift-codegen") (r "^0.93.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.93.0") (d #t) (k 0)))) (h "1xbbd0k44v8zg6hjd71a63rac19bkpclzzwh7m6qa5mq034nfarc") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.92.1 (c (n "cranelift") (v "0.92.1") (d (list (d (n "cranelift-codegen") (r "^0.92.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.92.1") (d #t) (k 0)))) (h "1vmrmlsg2hy9xi28sy8mlqnnmk9hyjvmg2wdvw35y0djbh35ly73") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.93.1 (c (n "cranelift") (v "0.93.1") (d (list (d (n "cranelift-codegen") (r "^0.93.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.93.1") (d #t) (k 0)))) (h "1n552b725rrl2ykardgmkg3hhmk2ixhphvlm3jp2zkiq68fhal6v") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.91.1 (c (n "cranelift") (v "0.91.1") (d (list (d (n "cranelift-codegen") (r "^0.91.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.91.1") (d #t) (k 0)))) (h "004gk1m7nswqk01fjdw7l8wcay8kwdcv4xan8g9gqmwawrkp9q4l") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.94.0 (c (n "cranelift") (v "0.94.0") (d (list (d (n "cranelift-codegen") (r "^0.94.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.94.0") (d #t) (k 0)))) (h "0z97r329djimgd4cy3hxw02frl6lfxxbv0i8867fdcykrzfkqd83") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.95.0 (c (n "cranelift") (v "0.95.0") (d (list (d (n "cranelift-codegen") (r "^0.95.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.95.0") (d #t) (k 0)))) (h "1f2sqdlzgi0whb64bx4pas61mqrz51zjmsaa48xjxx8lmpvz79q6") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.93.2 (c (n "cranelift") (v "0.93.2") (d (list (d (n "cranelift-codegen") (r "^0.93.2") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.93.2") (d #t) (k 0)))) (h "1ddlgj9lkgfvjifrl01nlq1d1p0yrx5gl97048f8nl9bf16qxg43") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.95.1 (c (n "cranelift") (v "0.95.1") (d (list (d (n "cranelift-codegen") (r "^0.95.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.95.1") (d #t) (k 0)))) (h "0ppzm2pyzs8wrvkpn2ssy4ska53608q60dwi3fg39z9hvbsbmfjh") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.94.1 (c (n "cranelift") (v "0.94.1") (d (list (d (n "cranelift-codegen") (r "^0.94.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.94.1") (d #t) (k 0)))) (h "1skb53nazxi13rng70h86biarzym0kfivky3f0bxyclh0wqnndnp") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.96.0 (c (n "cranelift") (v "0.96.0") (d (list (d (n "cranelift-codegen") (r "^0.96.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.96.0") (d #t) (k 0)))) (h "12929x8z6g4jiqc78xlvgx3jygdhkyjj22q039442ykvjk4hg988") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.96.1 (c (n "cranelift") (v "0.96.1") (d (list (d (n "cranelift-codegen") (r "^0.96.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.96.1") (d #t) (k 0)))) (h "09gbniallp471jwacsxybwrvm0dpfx5hk3pfvp3wh9l4mpq3fc4i") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.96.2 (c (n "cranelift") (v "0.96.2") (d (list (d (n "cranelift-codegen") (r "^0.96.2") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.96.2") (d #t) (k 0)))) (h "08s6186ldxzvnja5flq5bv5yxs7l5h1hfjd2v6xpcvz2zgjhdgza") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.96.3 (c (n "cranelift") (v "0.96.3") (d (list (d (n "cranelift-codegen") (r "^0.96.3") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.96.3") (d #t) (k 0)))) (h "1jgi42hxy023yapqhybpambw7yyl7ddln755w7j85rvlbsqqk00j") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.96.4 (c (n "cranelift") (v "0.96.4") (d (list (d (n "cranelift-codegen") (r "^0.96.4") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.96.4") (d #t) (k 0)))) (h "1nzmi1ixgdq9qcfa8b1rx35ad9296jnjspcm06i0yvhkcqk2ihyw") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.97.0 (c (n "cranelift") (v "0.97.0") (d (list (d (n "cranelift-codegen") (r "^0.97.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.97.0") (d #t) (k 0)))) (h "1m6kmpaa8k1yzdm5yqvj88snsigdywbz4g3ccs07q53lxwqidwbx") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.97.1 (c (n "cranelift") (v "0.97.1") (d (list (d (n "cranelift-codegen") (r "^0.97.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.97.1") (d #t) (k 0)))) (h "0l4fnpar5bgip68hazc6mzzn7phr7jfxma7qcnigfn4y0gxhnfm6") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.98.0 (c (n "cranelift") (v "0.98.0") (d (list (d (n "cranelift-codegen") (r "^0.98.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.98.0") (d #t) (k 0)))) (h "092v5hyxw4mlnxd1g1xscnrm2mqh5ilgq4h0dfjpm68cb28fyqdj") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.98.1 (c (n "cranelift") (v "0.98.1") (d (list (d (n "cranelift-codegen") (r "^0.98.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.98.1") (d #t) (k 0)))) (h "1z3l8s0whg7qg6qxf5x580cwf4dn1s7z3h4575w0pa6ysryxvjp2") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.99.0 (c (n "cranelift") (v "0.99.0") (d (list (d (n "cranelift-codegen") (r "^0.99.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.99.0") (d #t) (k 0)))) (h "146cdng6jp256lsvxwqbppggvlqqc0ca1lbmbragdfx1z1rdfqxf") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.99.1 (c (n "cranelift") (v "0.99.1") (d (list (d (n "cranelift-codegen") (r "^0.99.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.99.1") (d #t) (k 0)))) (h "0z1jwi7p3mcji9iivxgkyhrmicajrq0ddxix8xaqjhckmp5lii0g") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.97.2 (c (n "cranelift") (v "0.97.2") (d (list (d (n "cranelift-codegen") (r "^0.97.2") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.97.2") (d #t) (k 0)))) (h "176vgc49rlcm1gasmhnxv79dkh021h4cfxm8hgcyppj6r2sy0izi") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.98.2 (c (n "cranelift") (v "0.98.2") (d (list (d (n "cranelift-codegen") (r "^0.98.2") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.98.2") (d #t) (k 0)))) (h "1lg9pd326qmlvh59pj1z1llixniz9pm0zpzy0mx8n0wwz7vrz439") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.99.2 (c (n "cranelift") (v "0.99.2") (d (list (d (n "cranelift-codegen") (r "^0.99.2") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.99.2") (d #t) (k 0)))) (h "115cdfpj96a4x459kh7qrhk3175xqfadh42qssi83zgc7ac4d4zi") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.100.0 (c (n "cranelift") (v "0.100.0") (d (list (d (n "cranelift-codegen") (r "^0.100.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.100.0") (d #t) (k 0)))) (h "06vscilmfiairy11d62373n99xkwbmx2s3ry5mazm9q5kcmd5rkc") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.101.0 (c (n "cranelift") (v "0.101.0") (d (list (d (n "cranelift-codegen") (r "^0.101.0") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.101.0") (d #t) (k 0)))) (h "1z7raq61nafpipgnan0ydbi0nq0imynfm55h19p0m2l6jwx6aml9") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.101.1 (c (n "cranelift") (v "0.101.1") (d (list (d (n "cranelift-codegen") (r "^0.101.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.101.1") (d #t) (k 0)))) (h "0dfixmiwpqvqqf0i6gxrbsxp71iwhshj3yqmk08xc9lbl6ykg66n") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.100.1 (c (n "cranelift") (v "0.100.1") (d (list (d (n "cranelift-codegen") (r "^0.100.1") (d #t) (k 0)) (d (n "cranelift-frontend") (r "^0.100.1") (d #t) (k 0)))) (h "006dshvjk2h97hsp4qvc4xr8s4zy7z7rdas2w3nj0vphnsl93qcx") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.101.2 (c (n "cranelift") (v "0.101.2") (d (list (d (n "cranelift-codegen") (r "^0.101.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.101.2") (d #t) (k 0)))) (h "0c3jyi5fsh7kjky8k4l2yvhbfb0zhlaqszzlac2i8pxgqf9x9b1s") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.101.3 (c (n "cranelift") (v "0.101.3") (d (list (d (n "cranelift-codegen") (r "^0.101.3") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.101.3") (d #t) (k 0)))) (h "19vwgyqr41v4gfw93h86r2dws661vcrwakrrkpjqilln6bra4ba7") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.101.4 (c (n "cranelift") (v "0.101.4") (d (list (d (n "cranelift-codegen") (r "^0.101.4") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.101.4") (d #t) (k 0)))) (h "012ccqp1m3nnhl40jpgmc3ngc54q61fk61is7psdnkjnn88z7zg4") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.102.0 (c (n "cranelift") (v "0.102.0") (d (list (d (n "cranelift-codegen") (r "^0.102.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.102.0") (d #t) (k 0)))) (h "0v8l60fch4gjahysv5nlb1qbx0yi9kicbs90k3m3q7yp5iypgahw") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.102.1 (c (n "cranelift") (v "0.102.1") (d (list (d (n "cranelift-codegen") (r "^0.102.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.102.1") (d #t) (k 0)))) (h "1c93ka2qrzphsr83d3dq11iqvq876j4kzbbrmhpv7p48ps5x1kyw") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.103.0 (c (n "cranelift") (v "0.103.0") (d (list (d (n "cranelift-codegen") (r "^0.103.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.103.0") (d #t) (k 0)))) (h "03irki4188zrjhscyf37hy3dvzls0djiwkb76rxpbg5zvbl11qn2") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.104.0 (c (n "cranelift") (v "0.104.0") (d (list (d (n "cranelift-codegen") (r "^0.104.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.104.0") (d #t) (k 0)))) (h "18nm5gp8d0hzmmjhr4kybw88ax4r1fmdxvy790k3bki5qqdcq2pl") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.104.1 (c (n "cranelift") (v "0.104.1") (d (list (d (n "cranelift-codegen") (r "^0.104.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.104.1") (d #t) (k 0)))) (h "1cp9mqara7g7yjayli5jhk45cwmnrdxnkcj3qcnhdkiv1jl59kz4") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.105.0 (c (n "cranelift") (v "0.105.0") (d (list (d (n "cranelift-codegen") (r "^0.105.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.105.0") (d #t) (k 0)))) (h "0ql2h11ds2786rryw4x2a5whr07j4nk4qlk5mzvycrdr7sgirbbi") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.105.1 (c (n "cranelift") (v "0.105.1") (d (list (d (n "cranelift-codegen") (r "^0.105.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.105.1") (d #t) (k 0)))) (h "18r1ibxl1qx4wcd66gj193k2idkx87zqgqcd9bwncb30r2grf9ys") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.104.2 (c (n "cranelift") (v "0.104.2") (d (list (d (n "cranelift-codegen") (r "^0.104.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.104.2") (d #t) (k 0)))) (h "1amjcxwc3hg2gxg8bghg917qdas9yfc11dsgj6l6pmnj90w17cbc") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.105.2 (c (n "cranelift") (v "0.105.2") (d (list (d (n "cranelift-codegen") (r "^0.105.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.105.2") (d #t) (k 0)))) (h "1vpmgv7dscfndfih7vqsqmhy8qikzjsn2vcs4franzigs70gwcmn") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.105.3 (c (n "cranelift") (v "0.105.3") (d (list (d (n "cranelift-codegen") (r "^0.105.3") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.105.3") (d #t) (k 0)))) (h "1y7zj4v9n45nch4mys4hspn4sgdpmi206rdpppnna0csdxkn92bd") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.106.0 (c (n "cranelift") (v "0.106.0") (d (list (d (n "cranelift-codegen") (r "^0.106.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.106.0") (d #t) (k 0)))) (h "01z81v3q8yw3yd4rh1k3yh6zrc2spqgzx2d2rrh153crjkylwvxz") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.106.1 (c (n "cranelift") (v "0.106.1") (d (list (d (n "cranelift-codegen") (r "^0.106.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.106.1") (d #t) (k 0)))) (h "1bgvhlhgrw9k33ifmk5vr6n5xq6mzknp2g5l0ir1692w37jrsjc9") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.105.4 (c (n "cranelift") (v "0.105.4") (d (list (d (n "cranelift-codegen") (r "^0.105.4") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.105.4") (d #t) (k 0)))) (h "1r80w0j26vprgaa98p6kvdwflnkywcazwk4fs60agzs10ra5mjdj") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.106.2 (c (n "cranelift") (v "0.106.2") (d (list (d (n "cranelift-codegen") (r "^0.106.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.106.2") (d #t) (k 0)))) (h "0c0g284bq5vz5m7vgzq2387afz09c38p4x9g9ragkd6n8hdjcm9k") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.104.3 (c (n "cranelift") (v "0.104.3") (d (list (d (n "cranelift-codegen") (r "^0.104.3") (f (quote ("std" "unwind"))) (k 0)) (d (n "cranelift-frontend") (r "^0.104.3") (d #t) (k 0)))) (h "0zlcg5d4znlz80mpdvcr8m1n69a93k6kz2lhbj5inq9l70zk9qx8") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.107.0 (c (n "cranelift") (v "0.107.0") (d (list (d (n "cranelift-codegen") (r "^0.107.0") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "cranelift-frontend") (r "^0.107.0") (d #t) (k 0)))) (h "0hs4hpaw2fplhicihw3qqrc4151f12ivvkzfrsrxbm235ajv7mfp") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.107.1 (c (n "cranelift") (v "0.107.1") (d (list (d (n "cranelift-codegen") (r "^0.107.1") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "cranelift-frontend") (r "^0.107.1") (d #t) (k 0)))) (h "0mlln3i8342szds5n3qxvbpzb5x31yrnr1hzf8hyqcshmb3pr76d") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.107.2 (c (n "cranelift") (v "0.107.2") (d (list (d (n "cranelift-codegen") (r "^0.107.2") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "cranelift-frontend") (r "^0.107.2") (d #t) (k 0)))) (h "1d6zcgavvis6n1fd25400y006cjsxljxc6i8qfabvh50xqxwq6y3") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.108.0 (c (n "cranelift") (v "0.108.0") (d (list (d (n "cranelift-codegen") (r "^0.108.0") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "cranelift-frontend") (r "^0.108.0") (d #t) (k 0)))) (h "1zva6v0brlnzr5svglsfbrzhlssjxv1v7kfazrf30p4d1r12wq5r") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

(define-public crate-cranelift-0.108.1 (c (n "cranelift") (v "0.108.1") (d (list (d (n "cranelift-codegen") (r "^0.108.1") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "cranelift-frontend") (r "^0.108.1") (d #t) (k 0)))) (h "0qb03ri5kw95g74k6jb0za6fgwx2cs0lwcxcaps0chn7jj0968ww") (f (quote (("std" "cranelift-codegen/std" "cranelift-frontend/std") ("default" "std") ("core" "cranelift-codegen/core" "cranelift-frontend/core"))))))

