(define-module (crates-io cr an crane-webserver) #:use-module (crates-io))

(define-public crate-crane-webserver-0.1.0 (c (n "crane-webserver") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1fs7pcvy1qjk1pn9pywm666w8p7i2pjv7arbv5jg2kswyxi33qdi")))

(define-public crate-crane-webserver-0.1.1 (c (n "crane-webserver") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "01l17xyiks6ar3rfwi50866x7n2i2rgx7q9wvri27k7s8q1bfspf")))

(define-public crate-crane-webserver-0.1.2 (c (n "crane-webserver") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0qnkwipcw0jnngd353090lcg5drpbxc2nz5jm2b3gx534q3p3xaq")))

(define-public crate-crane-webserver-0.1.3 (c (n "crane-webserver") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "010i8im06iz8m75yrskfrcpn80ab3yaip2pwxqhn6zlmnb3yil5g")))

(define-public crate-crane-webserver-0.1.4 (c (n "crane-webserver") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1x4yy68dfr56wkwv2714m962xx3mcdzncjv02ishanj6ki8nkxib")))

(define-public crate-crane-webserver-0.1.5 (c (n "crane-webserver") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "084i7yv0310dy8h5zswsjq4ca1vg3k4nlv4w8ak3ixfa1cdh47xj")))

(define-public crate-crane-webserver-0.1.6 (c (n "crane-webserver") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "06xzn2jkd851m1pqpijg062ixqz6ddc70klsg5iczjjin4pspqr4")))

