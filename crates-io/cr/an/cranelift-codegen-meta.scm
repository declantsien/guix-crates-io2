(define-module (crates-io cr an cranelift-codegen-meta) #:use-module (crates-io))

(define-public crate-cranelift-codegen-meta-0.18.0 (c (n "cranelift-codegen-meta") (v "0.18.0") (h "1wavxcn2ih9qz9rj8s331hmmrqk7h4sphkcgh63xscfk37nm178a")))

(define-public crate-cranelift-codegen-meta-0.18.1 (c (n "cranelift-codegen-meta") (v "0.18.1") (h "07mss76p4374a12b8vc1zgf0fp8r8jr8h80yvzib2dr482nxy9ks")))

(define-public crate-cranelift-codegen-meta-0.19.0 (c (n "cranelift-codegen-meta") (v "0.19.0") (h "11a6nhmpzd2nvn885p8if76svabkz5k875ikw1f0zw6wpj9z8rlw")))

(define-public crate-cranelift-codegen-meta-0.20.0 (c (n "cranelift-codegen-meta") (v "0.20.0") (h "1wx5vjnxxlnmncikvs3ha46nmyr3q9n6dsic69xhnyly22hdm8qx")))

(define-public crate-cranelift-codegen-meta-0.21.0 (c (n "cranelift-codegen-meta") (v "0.21.0") (h "1w8wjbxr59icinmmmizk27ds8r604akh2ki9bxqlyrif9gkgnfps")))

(define-public crate-cranelift-codegen-meta-0.21.1 (c (n "cranelift-codegen-meta") (v "0.21.1") (h "0klcxw23hlwd7r454a2x6z4c7h0x7dcd89rsdj5x2id61hk1qnbc")))

(define-public crate-cranelift-codegen-meta-0.22.0 (c (n "cranelift-codegen-meta") (v "0.22.0") (h "1k9d3v5ia4sf8plqr07kkk0ri0hk05ggaccazsac6h902q2gbfmp")))

(define-public crate-cranelift-codegen-meta-0.23.0 (c (n "cranelift-codegen-meta") (v "0.23.0") (d (list (d (n "cranelift-entity") (r "^0.23.0") (d #t) (k 0)))) (h "1szrmrqpdjnsl6y0af5w1xsk31b5s7r5ambkvw4zdscngc4idb2f")))

(define-public crate-cranelift-codegen-meta-0.24.0 (c (n "cranelift-codegen-meta") (v "0.24.0") (d (list (d (n "cranelift-entity") (r "^0.24.0") (d #t) (k 0)))) (h "1jnwnv2110xy3cfbpyb4qbf6h2avapkva41ix6znfz15b6vl1mxr")))

(define-public crate-cranelift-codegen-meta-0.25.0 (c (n "cranelift-codegen-meta") (v "0.25.0") (d (list (d (n "cranelift-entity") (r "^0.25.0") (d #t) (k 0)))) (h "11pnrqzi9xf3akb113h999gm5r23f856gx20r9ismn85g9ln4cln")))

(define-public crate-cranelift-codegen-meta-0.26.0 (c (n "cranelift-codegen-meta") (v "0.26.0") (d (list (d (n "cranelift-entity") (r "^0.26.0") (d #t) (k 0)))) (h "1v9lax36gj6cvx1g07a2kwbds1sj4hwgc2jl85ykgj5x7vnj6k01")))

(define-public crate-cranelift-codegen-meta-0.27.0 (c (n "cranelift-codegen-meta") (v "0.27.0") (d (list (d (n "cranelift-entity") (r "^0.27.0") (d #t) (k 0)))) (h "1ifgkys7hcjvgqwfvjrqy9nzay6m8j823lv0jkqb1vqa3pnlymaa")))

(define-public crate-cranelift-codegen-meta-0.28.0 (c (n "cranelift-codegen-meta") (v "0.28.0") (d (list (d (n "cranelift-entity") (r "^0.28.0") (d #t) (k 0)))) (h "1rp1wvlqadhydixgfjbqnnj6had6jk0gpv98zwy29q575mk1w4s3")))

(define-public crate-cranelift-codegen-meta-0.29.0 (c (n "cranelift-codegen-meta") (v "0.29.0") (d (list (d (n "cranelift-entity") (r "^0.29.0") (d #t) (k 0)))) (h "1l8pwfpnfx2jsxyw61kkycf5s78ij3xrnvdwdyb2q5z8a4swwq1y")))

(define-public crate-cranelift-codegen-meta-0.30.0 (c (n "cranelift-codegen-meta") (v "0.30.0") (d (list (d (n "cranelift-entity") (r "^0.30.0") (d #t) (k 0)))) (h "0z9jq3lpfh4yrdmwm8z4mg73n3fx1h6l9pc3w2bi5iqa8lpplyg3")))

(define-public crate-cranelift-codegen-meta-0.31.0 (c (n "cranelift-codegen-meta") (v "0.31.0") (d (list (d (n "cranelift-entity") (r "^0.31.0") (k 0)))) (h "170jmbnjcmg3whqsx5w0kd835xxy81sn4vbw9g8sd4h08gnibvhg") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.32.0 (c (n "cranelift-codegen-meta") (v "0.32.0") (d (list (d (n "cranelift-entity") (r "^0.32.0") (k 0)))) (h "0gmygp1n0fxq9hqwywg9f9w0kb5p13brpn4xk1hji3ra3k43dxf1") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.33.0 (c (n "cranelift-codegen-meta") (v "0.33.0") (d (list (d (n "cranelift-entity") (r "^0.33.0") (k 0)))) (h "0f30710vhmnfmmfdjbd8q9gxm96lmw9qpi7832w1qj0ahcwhnqn2") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.34.0 (c (n "cranelift-codegen-meta") (v "0.34.0") (d (list (d (n "cranelift-entity") (r "^0.34.0") (k 0)))) (h "16yvwlvqly9gr8ayfqqswh469p384vbs14jwdf7hwpv6qxc60077") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.35.0 (c (n "cranelift-codegen-meta") (v "0.35.0") (d (list (d (n "cranelift-entity") (r "^0.35.0") (k 0)))) (h "1g8c1ygppkx6p1jc5xchq947myf3j52xqq0l1l96qsr0ihfb6b82") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.36.0 (c (n "cranelift-codegen-meta") (v "0.36.0") (d (list (d (n "cranelift-entity") (r "^0.36.0") (k 0)))) (h "0mxr0bbni8rzawr18ybpm9j8rm9b13cj7whsk8y0lj6xpk3b1bnp") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.37.0 (c (n "cranelift-codegen-meta") (v "0.37.0") (d (list (d (n "cranelift-entity") (r "^0.37.0") (k 0)))) (h "1a58ddgharhcjmfs717dk9ba8nqylwc9bsdpc75i13ha56nfd7dx") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.38.0 (c (n "cranelift-codegen-meta") (v "0.38.0") (d (list (d (n "cranelift-entity") (r "^0.38.0") (k 0)))) (h "1nwszdj49m5qcnzzviqpym091adsarw9rnr6zzbydav3vdvya479") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.39.0 (c (n "cranelift-codegen-meta") (v "0.39.0") (d (list (d (n "cranelift-entity") (r "^0.39.0") (k 0)))) (h "129jxy1arr88v583rpl6b7hq18lcv6yvzyqnwsk9wkwli9j6122s") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.40.0 (c (n "cranelift-codegen-meta") (v "0.40.0") (d (list (d (n "cranelift-entity") (r "^0.40.0") (k 0)))) (h "0j34pzhahrabw0ivnx93d7k6pdl769lhk4y6d9iy5f54cf3n11hb") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.41.0 (c (n "cranelift-codegen-meta") (v "0.41.0") (d (list (d (n "cranelift-entity") (r "^0.41.0") (k 0)))) (h "0dqkxdriajvv78d1ls3ndg3fbj1rrb9i2ww5s349b3b91ncbzydg") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.42.0 (c (n "cranelift-codegen-meta") (v "0.42.0") (d (list (d (n "cranelift-entity") (r "^0.42.0") (k 0)))) (h "0yg0l5wmw3rwp2vl1w6z0i9xnic48nlgv47gmhvgj58nkli63iwh") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.43.0 (c (n "cranelift-codegen-meta") (v "0.43.0") (d (list (d (n "cranelift-entity") (r "^0.43.0") (k 0)))) (h "0ry99clircmxr7wi9q2hkvxydhk503k4y0ysx9sxm71kg2va7jw7") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.43.1 (c (n "cranelift-codegen-meta") (v "0.43.1") (d (list (d (n "cranelift-entity") (r "^0.43.1") (k 0)))) (h "1h98mpz5n2vrnpj82y2wh3hjjijzxak8a7ly8vi66m643bqsqr9a") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.44.0 (c (n "cranelift-codegen-meta") (v "0.44.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.44.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.44.0") (k 0)))) (h "17zyh6is9yibjj96vcmbjfvx6m0gswmwxxfijxxig3zfw9jzciyx") (f (quote (("std" "cranelift-entity/std") ("default" "std") ("core" "cranelift-entity/core"))))))

(define-public crate-cranelift-codegen-meta-0.45.0 (c (n "cranelift-codegen-meta") (v "0.45.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.45.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.45.0") (d #t) (k 0)))) (h "0rq3q37bisd66ykkb2lr8m2923ajy6n7rz840y2n05x405h63z8v")))

(define-public crate-cranelift-codegen-meta-0.46.0 (c (n "cranelift-codegen-meta") (v "0.46.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.46.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.46.0") (d #t) (k 0)))) (h "0l61cjpihcbjpav4mmgla8afybay6n3qf7sy2iigm9nw5xhlf0d9")))

(define-public crate-cranelift-codegen-meta-0.46.1 (c (n "cranelift-codegen-meta") (v "0.46.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.46.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.46.1") (d #t) (k 0)))) (h "1jp981d1x6yhvd04aqlibsqv2v1fiknn2qlb6wy2bzz6ymg2cmzx")))

(define-public crate-cranelift-codegen-meta-0.47.0 (c (n "cranelift-codegen-meta") (v "0.47.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.47.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.47.0") (d #t) (k 0)))) (h "0ccjb0vy3h36cscxrmdgpz6skdqq4jmqr4dbjz5r5010rwyx9g29")))

(define-public crate-cranelift-codegen-meta-0.48.0 (c (n "cranelift-codegen-meta") (v "0.48.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.48.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.48.0") (d #t) (k 0)))) (h "1kaj2n3n4wp2p47pjahcds1b1q6ihmwi1490xh0i5nd73373ypfz")))

(define-public crate-cranelift-codegen-meta-0.49.0 (c (n "cranelift-codegen-meta") (v "0.49.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.49.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.49.0") (d #t) (k 0)))) (h "1rd4rjxvn5x3w366zivj5bimhr581a504wh1si3m0399c3yk99ja")))

(define-public crate-cranelift-codegen-meta-0.50.0 (c (n "cranelift-codegen-meta") (v "0.50.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.50.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.50.0") (d #t) (k 0)))) (h "191krhiqflp6cb52jnwd63p853dxrb3gm6w60afp2zf0q98xzcvw")))

(define-public crate-cranelift-codegen-meta-0.51.0 (c (n "cranelift-codegen-meta") (v "0.51.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.51.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.51.0") (d #t) (k 0)))) (h "151wyfr8j34hj4bpqdjpa2vcir3ypxabv6w1j79zwpln3mi62g44")))

(define-public crate-cranelift-codegen-meta-0.52.0 (c (n "cranelift-codegen-meta") (v "0.52.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.52.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.52.0") (d #t) (k 0)))) (h "0l1g04z4f8548nv4pvyv1zzggsf9azwdwisij97kmmgbrklf8w6c")))

(define-public crate-cranelift-codegen-meta-0.53.0 (c (n "cranelift-codegen-meta") (v "0.53.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.53.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.53.0") (d #t) (k 0)))) (h "1m5rsynfpf3c39bfdfab1ha4all1vxzmb1hrdp8k8h5385k66fl8")))

(define-public crate-cranelift-codegen-meta-0.54.0 (c (n "cranelift-codegen-meta") (v "0.54.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.54.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.54.0") (d #t) (k 0)))) (h "0xbj0kcvfhw4nnw3vvv4kdicbi7ffvzg466jxf1cla4faj8ydarw")))

(define-public crate-cranelift-codegen-meta-0.55.0 (c (n "cranelift-codegen-meta") (v "0.55.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.55.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.55.0") (d #t) (k 0)))) (h "0wyn43lvww9pb5kfdnwj947xjawk2afyndf8sws92fwlhk4p4f1m")))

(define-public crate-cranelift-codegen-meta-0.56.0 (c (n "cranelift-codegen-meta") (v "0.56.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.56.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.56.0") (d #t) (k 0)))) (h "01vjjkhy37wz2npixzzp1qxmkl347mjvrks7ndjyqdzxbxfhxrb5")))

(define-public crate-cranelift-codegen-meta-0.58.0 (c (n "cranelift-codegen-meta") (v "0.58.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.58.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.58.0") (d #t) (k 0)))) (h "1amb64rm9kwqpfvlc4lg710jzj9vzin0q0n56jjk5vxzha6kr310")))

(define-public crate-cranelift-codegen-meta-0.59.0 (c (n "cranelift-codegen-meta") (v "0.59.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.59.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.59.0") (d #t) (k 0)))) (h "1fd3f915i68nggq3p3zc0g3sq2gv3d3rkhbjrn5a478dzgaxck08")))

(define-public crate-cranelift-codegen-meta-0.60.0 (c (n "cranelift-codegen-meta") (v "0.60.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.60.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.60.0") (d #t) (k 0)))) (h "07qpky8vyks5d02pb9515bkw8mf7sg8hlsgy208g5b8cw8mdxzs5")))

(define-public crate-cranelift-codegen-meta-0.61.0 (c (n "cranelift-codegen-meta") (v "0.61.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.61.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.61.0") (d #t) (k 0)))) (h "0pnvyspgmbqcyq5j0ss48j28888c42q1j3dzjz2l6ww7sb7d1b63")))

(define-public crate-cranelift-codegen-meta-0.62.0 (c (n "cranelift-codegen-meta") (v "0.62.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.62.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.62.0") (d #t) (k 0)))) (h "1qd7w6h448hzmg6jxyhbsij5x4jqw0ll5vkcm11k6d4gbkbxf114")))

(define-public crate-cranelift-codegen-meta-0.63.0 (c (n "cranelift-codegen-meta") (v "0.63.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.63.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.63.0") (d #t) (k 0)))) (h "0jqlqls1qiq3lnm6xaa3bhc5ghf61d0g6jydzb2l2vd30qizpj82")))

(define-public crate-cranelift-codegen-meta-0.64.0 (c (n "cranelift-codegen-meta") (v "0.64.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.64.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.64.0") (d #t) (k 0)))) (h "1ivqxzlbvbqa3yc26sb8nxj4998x7nbvrnb3bysp61g4krygzrvh")))

(define-public crate-cranelift-codegen-meta-0.65.0 (c (n "cranelift-codegen-meta") (v "0.65.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.65.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.65.0") (d #t) (k 0)))) (h "10qawkkjacy6pvy775i82zzygbl187n93awa5d6dzh87y7v3rpbl")))

(define-public crate-cranelift-codegen-meta-0.66.0 (c (n "cranelift-codegen-meta") (v "0.66.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.66.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.66.0") (d #t) (k 0)))) (h "164mmgz71af0z4v8lnbbi36adfshmsr65ghhsm54y7l66404cgrw")))

(define-public crate-cranelift-codegen-meta-0.67.0 (c (n "cranelift-codegen-meta") (v "0.67.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.67.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.67.0") (d #t) (k 0)))) (h "1869hg5b5ydji2myy9yigmamwjpfg5ac2w4nbyrn63f6hxz0rjxl")))

(define-public crate-cranelift-codegen-meta-0.68.0 (c (n "cranelift-codegen-meta") (v "0.68.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.68.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.68.0") (d #t) (k 0)))) (h "1my8z3bh5ry1542l87ja9k9k8m8hx763mny8yi06ac38hmvb5wjf")))

(define-public crate-cranelift-codegen-meta-0.69.0 (c (n "cranelift-codegen-meta") (v "0.69.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.69.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.69.0") (d #t) (k 0)))) (h "041qjfy46y49j446r51c43x169l28lsnmwqgiy6nahxlmiyaqk65")))

(define-public crate-cranelift-codegen-meta-0.70.0 (c (n "cranelift-codegen-meta") (v "0.70.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.70.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.70.0") (d #t) (k 0)))) (h "0a91prrsg9qfds57560sdvri8fld5i57kcj96l8rwrdcnmffkbvw")))

(define-public crate-cranelift-codegen-meta-0.71.0 (c (n "cranelift-codegen-meta") (v "0.71.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.71.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.71.0") (d #t) (k 0)))) (h "054gzws6917zgcl2j4y9g6xh8355vzfvqc7wql8gvmg9bgy4r6b5")))

(define-public crate-cranelift-codegen-meta-0.72.0 (c (n "cranelift-codegen-meta") (v "0.72.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.72.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.72.0") (d #t) (k 0)))) (h "03dvcqy5ywyl0gvwcfnfbvv5lnra7yl4w494b4akjy96p04rfc9a")))

(define-public crate-cranelift-codegen-meta-0.73.0 (c (n "cranelift-codegen-meta") (v "0.73.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.73.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.73.0") (d #t) (k 0)))) (h "0njylyzvasw01lv9imgk6rgafag5ij9yq1fxh0gwhypzy2r12phh")))

(define-public crate-cranelift-codegen-meta-0.74.0 (c (n "cranelift-codegen-meta") (v "0.74.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.74.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.74.0") (d #t) (k 0)))) (h "08dqanzi63551jbv5x3lq02h7xgnq7jq7zlzhhipxl56llv1khjc")))

(define-public crate-cranelift-codegen-meta-0.73.1 (c (n "cranelift-codegen-meta") (v "0.73.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.73.1") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.73.1") (d #t) (k 0)))) (h "1zvrl7xxv1xn8s56snx9vaqrqn3dc63vqx8pfhx7ipij1ag7i54w")))

(define-public crate-cranelift-codegen-meta-0.75.0 (c (n "cranelift-codegen-meta") (v "0.75.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.75.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.75.0") (d #t) (k 0)))) (h "0ws0d3a4qz4h5xv4f2wzr8bar7ivxc945smyz5msa552ipk4pg0j")))

(define-public crate-cranelift-codegen-meta-0.76.0 (c (n "cranelift-codegen-meta") (v "0.76.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.76.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.76.0") (d #t) (k 0)))) (h "0psm7iih6knh500m68qsbj4lv98n8rv176afppb2qkxbzhignvlp")))

(define-public crate-cranelift-codegen-meta-0.77.0 (c (n "cranelift-codegen-meta") (v "0.77.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.77.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.77.0") (d #t) (k 0)))) (h "1b0bbyy8af04fl929ha7m6mq2pl8zn41vx5kzzsglbf638k09xaw")))

(define-public crate-cranelift-codegen-meta-0.78.0 (c (n "cranelift-codegen-meta") (v "0.78.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.78.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.78.0") (d #t) (k 0)))) (h "1gsj12axwg71cggv9mlc5dkh30d808171q8nx82599akl2a6lgvr")))

(define-public crate-cranelift-codegen-meta-0.79.0 (c (n "cranelift-codegen-meta") (v "0.79.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.79.0") (d #t) (k 0)))) (h "11dy5kllr5lr3jn192199mbsmjns5nflgicl7mhcacf92qclq9r8") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.79.1 (c (n "cranelift-codegen-meta") (v "0.79.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.79.1") (d #t) (k 0)))) (h "1ypfdzla2qg9kfljvs573v14c8cwz4rffiz5jqawhh09ajg1w700") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.80.0 (c (n "cranelift-codegen-meta") (v "0.80.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.80.0") (d #t) (k 0)))) (h "0j5d7rv3i95hx3g4hqdqyyns8mq180q4nwqf21lhzfvi0caf2vnk") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.81.0 (c (n "cranelift-codegen-meta") (v "0.81.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.81.0") (d #t) (k 0)))) (h "0b39i0i1mm0cwsbhgkhjrcbzjxdpiar4b1c8mcbnxkybdzbmamw1") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.81.1 (c (n "cranelift-codegen-meta") (v "0.81.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.81.1") (d #t) (k 0)))) (h "00ly6rj20njygg2nm6zs00pf7j8n8arw3m9snpqwnhzg5i6w22i9") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.80.1 (c (n "cranelift-codegen-meta") (v "0.80.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.80.1") (d #t) (k 0)))) (h "1zkaa00zjfwy32k3yfsd2sw1dli64y49k8xqn4pp02yk4x3an83s") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.82.0 (c (n "cranelift-codegen-meta") (v "0.82.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.82.0") (d #t) (k 0)))) (h "1pb4i63b4lj41a6h0fb04ibg631dbrnh5gzkncz3kgj3vrng5cgf") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.82.1 (c (n "cranelift-codegen-meta") (v "0.82.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.82.1") (d #t) (k 0)))) (h "0cp4srflnvk8ij2jfhaqldkk3sglbcbmha519j0plv5wsgbhwgbh") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.82.2 (c (n "cranelift-codegen-meta") (v "0.82.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.82.2") (d #t) (k 0)))) (h "1q171lgczliji3hm1iz101dhdqh0vzhdrlw1ymjcr7sk4lnigw5h") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.81.2 (c (n "cranelift-codegen-meta") (v "0.81.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.81.2") (d #t) (k 0)))) (h "10ccmn6rg8wrnlsyzhrvpck2bn26867pslyjqhbxc3a7kppjzj5z") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.82.3 (c (n "cranelift-codegen-meta") (v "0.82.3") (d (list (d (n "cranelift-codegen-shared") (r "^0.82.3") (d #t) (k 0)))) (h "090s1g28wsw28sx9bb3nb8irh716ldz7snvangcx72cvkswdschg") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.83.0 (c (n "cranelift-codegen-meta") (v "0.83.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.83.0") (d #t) (k 0)))) (h "078rp0k4ix7vc669r9d2646zm8cbxl3hcs1jbb66xpvpc4wdq038") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.84.0 (c (n "cranelift-codegen-meta") (v "0.84.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.84.0") (d #t) (k 0)))) (h "0hd12sqmxp19hp1cdcqmlkv8bh0aas45wjcdzpmmmj9f40psqzh5") (f (quote (("rebuild-isle"))))))

(define-public crate-cranelift-codegen-meta-0.85.0 (c (n "cranelift-codegen-meta") (v "0.85.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.85.0") (d #t) (k 0)))) (h "187zshjrgbvx2vkrf0vqjz7l1gc1sfk3ja7lr4zcnzzblfxnr9ly")))

(define-public crate-cranelift-codegen-meta-0.85.1 (c (n "cranelift-codegen-meta") (v "0.85.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.85.1") (d #t) (k 0)))) (h "0g3lxl77vryvwvdxcnrpnlf25q8x79yc169bw8627gvi441k1k2l")))

(define-public crate-cranelift-codegen-meta-0.86.0 (c (n "cranelift-codegen-meta") (v "0.86.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.86.0") (d #t) (k 0)))) (h "1wdcms4ga2akzrmk0xd9jx0aq6kb6nfsdfrn0c029jxgqb89jrxw")))

(define-public crate-cranelift-codegen-meta-0.85.2 (c (n "cranelift-codegen-meta") (v "0.85.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.85.2") (d #t) (k 0)))) (h "00awxkzrirxmsgz3fh7yxmh5g2v6r9d90kyq4hj28n1g0n0khhki")))

(define-public crate-cranelift-codegen-meta-0.85.3 (c (n "cranelift-codegen-meta") (v "0.85.3") (d (list (d (n "cranelift-codegen-shared") (r "^0.85.3") (d #t) (k 0)))) (h "1dmx5133aai2a1qk6m63b8kfxn30skgs3ff5310f945bznlcx8z0")))

(define-public crate-cranelift-codegen-meta-0.86.1 (c (n "cranelift-codegen-meta") (v "0.86.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.86.1") (d #t) (k 0)))) (h "1ldprm0lp9dr0i11h26wf067k9ps1x7snglf6m2qv0svv2z785jm")))

(define-public crate-cranelift-codegen-meta-0.87.0 (c (n "cranelift-codegen-meta") (v "0.87.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.87.0") (d #t) (k 0)))) (h "1f6vdr4nvnkprjgzqbll16v86wha857213q4z4p1jqz6f66iiv7r")))

(define-public crate-cranelift-codegen-meta-0.87.1 (c (n "cranelift-codegen-meta") (v "0.87.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.87.1") (d #t) (k 0)))) (h "01pswf9lqvf8v7kwib6pnrprfhr3xd6yzpx32frq3hs8y3zyh0ns")))

(define-public crate-cranelift-codegen-meta-0.88.0 (c (n "cranelift-codegen-meta") (v "0.88.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.88.0") (d #t) (k 0)))) (h "0i52fdgjfipmvrv9hjp2rl7577cyc2yyw7nsqhgjrii9pgg9zd11")))

(define-public crate-cranelift-codegen-meta-0.88.1 (c (n "cranelift-codegen-meta") (v "0.88.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.88.1") (d #t) (k 0)))) (h "1rbp93dki99f2ihingas6zra95xdjy31mgcqyax8yxf4wqvcx1sj")))

(define-public crate-cranelift-codegen-meta-0.89.0 (c (n "cranelift-codegen-meta") (v "0.89.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.89.0") (d #t) (k 0)))) (h "0qq587j2m1xm60ajb249y6jfyib1qpz9z7bfp7lp6gcms0c84hg8")))

(define-public crate-cranelift-codegen-meta-0.89.1 (c (n "cranelift-codegen-meta") (v "0.89.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.89.1") (d #t) (k 0)))) (h "1dnnp7r52nmaw7zk1hs73xjr4r6mxqzjycrim7ysjjcjyj2g5whm")))

(define-public crate-cranelift-codegen-meta-0.89.2 (c (n "cranelift-codegen-meta") (v "0.89.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.89.2") (d #t) (k 0)))) (h "10ba0r145qzq325vi5rc523cdyi5p2j6n7ap07q7d52p8ag6dh8s")))

(define-public crate-cranelift-codegen-meta-0.88.2 (c (n "cranelift-codegen-meta") (v "0.88.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.88.2") (d #t) (k 0)))) (h "13rmfha29dqg9ixzwp1cfq913imsnvp6acaggqfd2dzjza0ini8w")))

(define-public crate-cranelift-codegen-meta-0.90.0 (c (n "cranelift-codegen-meta") (v "0.90.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.90.0") (d #t) (k 0)))) (h "1526kdkn9h96h11h9ckdgwhilsf1gix4210aqinxhfz6iagzgfiw")))

(define-public crate-cranelift-codegen-meta-0.90.1 (c (n "cranelift-codegen-meta") (v "0.90.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.90.1") (d #t) (k 0)))) (h "1dglg4igwcxb1hgm3i2wbm0308wdabx0yswvfpilxhr73j7pm4mv")))

(define-public crate-cranelift-codegen-meta-0.91.0 (c (n "cranelift-codegen-meta") (v "0.91.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.91.0") (d #t) (k 0)))) (h "13x328pdsji6ch901fg0q8dxzfv1kq3jnp4nr4da22218rr1h6li")))

(define-public crate-cranelift-codegen-meta-0.92.0 (c (n "cranelift-codegen-meta") (v "0.92.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.92.0") (d #t) (k 0)))) (h "1lp0sa5bqk0nyz09k6bpr7qjr2mh1gh7xbwkb00f833mi481p6rz")))

(define-public crate-cranelift-codegen-meta-0.93.0 (c (n "cranelift-codegen-meta") (v "0.93.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.93.0") (d #t) (k 0)))) (h "1fwjildnjj2z7b1afj27vf3a3l3kfcc50f783ybv45w9xkav4why")))

(define-public crate-cranelift-codegen-meta-0.91.1 (c (n "cranelift-codegen-meta") (v "0.91.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.91.1") (d #t) (k 0)))) (h "0x3ffp0d86ggz7ggqzknrppwp1dh187hqc7qk0m13b9lajs0g4v3")))

(define-public crate-cranelift-codegen-meta-0.92.1 (c (n "cranelift-codegen-meta") (v "0.92.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.92.1") (d #t) (k 0)))) (h "0nmgax0pcdcy3w7wgddxxg17yp8ldpiiwnd3agya4ddcilbanz7b")))

(define-public crate-cranelift-codegen-meta-0.93.1 (c (n "cranelift-codegen-meta") (v "0.93.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.93.1") (d #t) (k 0)))) (h "0f9j7vkj4430z0zgv7yykg6y2a3pxz2zgwnfaznj3iqym5yvdfq5")))

(define-public crate-cranelift-codegen-meta-0.94.0 (c (n "cranelift-codegen-meta") (v "0.94.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.94.0") (d #t) (k 0)))) (h "0f77sa770zikyigb73idy7k6542qa9lqkwsq5k4prksmg4575yy7")))

(define-public crate-cranelift-codegen-meta-0.95.0 (c (n "cranelift-codegen-meta") (v "0.95.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.95.0") (d #t) (k 0)))) (h "0fz69v53sb3iwib61w4sdr79114v67iza3qsvj86dzvdy4c8mp1w")))

(define-public crate-cranelift-codegen-meta-0.93.2 (c (n "cranelift-codegen-meta") (v "0.93.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.93.2") (d #t) (k 0)))) (h "1j571jnkp766sm06xyi7iq6zbpqzb60p2yjkp2qz3gz2cm1ibwkj")))

(define-public crate-cranelift-codegen-meta-0.95.1 (c (n "cranelift-codegen-meta") (v "0.95.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.95.1") (d #t) (k 0)))) (h "1nn9qmvs4q3m71mxw209zln9rzdxdmh6n7v529pxdck21p1mmb68")))

(define-public crate-cranelift-codegen-meta-0.94.1 (c (n "cranelift-codegen-meta") (v "0.94.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.94.1") (d #t) (k 0)))) (h "1s3wlsl0zvcvs9ygscblcv3z28ad81s17cw94s6fg6v86h70wvs1")))

(define-public crate-cranelift-codegen-meta-0.96.0 (c (n "cranelift-codegen-meta") (v "0.96.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.96.0") (d #t) (k 0)))) (h "1lqirb3jv3jzknbby02ylwdzfrcsb605mnmf6ha8rjsrg4pcsg70")))

(define-public crate-cranelift-codegen-meta-0.96.1 (c (n "cranelift-codegen-meta") (v "0.96.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.96.1") (d #t) (k 0)))) (h "1nr6y7pzq7z6hddxrwy29p8jr9cz5lzlnh78v5fvaqz12gi724q1")))

(define-public crate-cranelift-codegen-meta-0.96.2 (c (n "cranelift-codegen-meta") (v "0.96.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.96.2") (d #t) (k 0)))) (h "01cgkybdcyihgi42mx4mwf3qvaa2gqds2jsmzd66z62y5kcmd3f9")))

(define-public crate-cranelift-codegen-meta-0.96.3 (c (n "cranelift-codegen-meta") (v "0.96.3") (d (list (d (n "cranelift-codegen-shared") (r "^0.96.3") (d #t) (k 0)))) (h "1fqyn8c5l872yfxc1gqkj820k8bwrph32bqbk6gqdqcacwicwxy7")))

(define-public crate-cranelift-codegen-meta-0.96.4 (c (n "cranelift-codegen-meta") (v "0.96.4") (d (list (d (n "cranelift-codegen-shared") (r "^0.96.4") (d #t) (k 0)))) (h "014rzlxb0mcbp5d44ckjaqghiqc3mvjxg02np8fwir1mh81p17v4")))

(define-public crate-cranelift-codegen-meta-0.97.0 (c (n "cranelift-codegen-meta") (v "0.97.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.97.0") (d #t) (k 0)))) (h "1v230hxcmdlm89k199iysm251dy9bdj1zrjgxpwf35xdjafqc7q7")))

(define-public crate-cranelift-codegen-meta-0.97.1 (c (n "cranelift-codegen-meta") (v "0.97.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.97.1") (d #t) (k 0)))) (h "1m3d1ic540mpdr7jpwrpyvrznhg4wdb4g51y0df0q4vkh44cm9s0")))

(define-public crate-cranelift-codegen-meta-0.98.0 (c (n "cranelift-codegen-meta") (v "0.98.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.98.0") (d #t) (k 0)))) (h "0b3fk60cwkv3q07afr7i0fsm9250giz6fd55fibbiamkvyw00fs1")))

(define-public crate-cranelift-codegen-meta-0.98.1 (c (n "cranelift-codegen-meta") (v "0.98.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.98.1") (d #t) (k 0)))) (h "062gnylfn9h89xd2cwa501gfbf0l8i15ikx8ksch98xrm2pycxfk")))

(define-public crate-cranelift-codegen-meta-0.99.0 (c (n "cranelift-codegen-meta") (v "0.99.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.99.0") (d #t) (k 0)))) (h "0j1gyq1g55cx8q2dy8azxvh4v6rwlai33i1nnllnx0cbs42q3hb3")))

(define-public crate-cranelift-codegen-meta-0.99.1 (c (n "cranelift-codegen-meta") (v "0.99.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.99.1") (d #t) (k 0)))) (h "1pzsyjkxjag9il1n0590lihsr310a0y43ds759qwnr9fazd53pm3")))

(define-public crate-cranelift-codegen-meta-0.97.2 (c (n "cranelift-codegen-meta") (v "0.97.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.97.2") (d #t) (k 0)))) (h "1c5abgd7mqxsydpcnc2ipnz380nq575012r57wwf6gdh5dxsv8rn")))

(define-public crate-cranelift-codegen-meta-0.98.2 (c (n "cranelift-codegen-meta") (v "0.98.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.98.2") (d #t) (k 0)))) (h "164596lfqd6jdpbqc2358a2mhpkj0dhdraj8jzlzmhf0a190dc6i")))

(define-public crate-cranelift-codegen-meta-0.99.2 (c (n "cranelift-codegen-meta") (v "0.99.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.99.2") (d #t) (k 0)))) (h "069lpbc7nbvzfk99x4d13nh2nggdhc72qvgnxvbfc6gga4vvk1il")))

(define-public crate-cranelift-codegen-meta-0.100.0 (c (n "cranelift-codegen-meta") (v "0.100.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.100.0") (d #t) (k 0)))) (h "0w0galc15z2yakns04nfm0nnln0v7jlhnwxq6lp6ixv3h4qshihg")))

(define-public crate-cranelift-codegen-meta-0.101.0 (c (n "cranelift-codegen-meta") (v "0.101.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.101.0") (d #t) (k 0)))) (h "01iw9ia0kci6igycmipa6gsk5y5370lw8nkm3xxd7qddrn9jfbq2")))

(define-public crate-cranelift-codegen-meta-0.101.1 (c (n "cranelift-codegen-meta") (v "0.101.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.101.1") (d #t) (k 0)))) (h "0jb6nlr2b3kjc53w16p6sqimr7yp2cmpk82yva1v2ia71lxqsfmb")))

(define-public crate-cranelift-codegen-meta-0.100.1 (c (n "cranelift-codegen-meta") (v "0.100.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.100.1") (d #t) (k 0)))) (h "0lqc7lkk6s93y30f0b4s7p6f1zlym2rlkbrygw2dnlqc47gpzp5m")))

(define-public crate-cranelift-codegen-meta-0.101.2 (c (n "cranelift-codegen-meta") (v "0.101.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.101.2") (d #t) (k 0)))) (h "1nxj1i43parwll1x30b391zc5gjiz9151ick0z77j32827279cf5")))

(define-public crate-cranelift-codegen-meta-0.101.3 (c (n "cranelift-codegen-meta") (v "0.101.3") (d (list (d (n "cranelift-codegen-shared") (r "^0.101.3") (d #t) (k 0)))) (h "0q6lk01s7qvgz7bl402n7bblbyy95s87car36xg7xlh0wsqskz86")))

(define-public crate-cranelift-codegen-meta-0.101.4 (c (n "cranelift-codegen-meta") (v "0.101.4") (d (list (d (n "cranelift-codegen-shared") (r "^0.101.4") (d #t) (k 0)))) (h "1mn3cx41lszhpqc3aw0jpd3kk8dip9c44r8jn2vbkhc2579fcg3s")))

(define-public crate-cranelift-codegen-meta-0.102.0 (c (n "cranelift-codegen-meta") (v "0.102.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.102.0") (d #t) (k 0)))) (h "16np9h1a1dkapylxq16hn11l3r3306frqg2s7asjvkzwr7nxxc7x")))

(define-public crate-cranelift-codegen-meta-0.102.1 (c (n "cranelift-codegen-meta") (v "0.102.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.102.1") (d #t) (k 0)))) (h "1xl3g7rr92a0vz9y1sw26vx4r4lynnw3by2g80xiajixkvqnnrah")))

(define-public crate-cranelift-codegen-meta-0.103.0 (c (n "cranelift-codegen-meta") (v "0.103.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.103.0") (d #t) (k 0)))) (h "0qs4m5hrxxl0v1jk4zzhw4acm7znvm6sr9gn9fbqyhynl6szx435")))

(define-public crate-cranelift-codegen-meta-0.104.0 (c (n "cranelift-codegen-meta") (v "0.104.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.104.0") (d #t) (k 0)))) (h "1yrd4z3ap7jxnwzh3k8db4pzwwqjavfwyc6w2ydn8lcfwdj3dzd3")))

(define-public crate-cranelift-codegen-meta-0.104.1 (c (n "cranelift-codegen-meta") (v "0.104.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.104.1") (d #t) (k 0)))) (h "1zvpdcy2pf3bmbi961gp3yzd6b5xmw33archs0w1xff2snp78zmw")))

(define-public crate-cranelift-codegen-meta-0.105.0 (c (n "cranelift-codegen-meta") (v "0.105.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.105.0") (d #t) (k 0)))) (h "02lsj739k4j6ln36gznj0lzhx9qnlb9p20cghyqs5ni6spqr46qf")))

(define-public crate-cranelift-codegen-meta-0.105.1 (c (n "cranelift-codegen-meta") (v "0.105.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.105.1") (d #t) (k 0)))) (h "1csgk5b2cad0lywdxp3128d3rnnm9si06sxcv6d5p69h0485xxgz")))

(define-public crate-cranelift-codegen-meta-0.104.2 (c (n "cranelift-codegen-meta") (v "0.104.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.104.2") (d #t) (k 0)))) (h "180f5mpxsaamary2rmh76phy48936zrsrdldpcygbjjwz91scbds")))

(define-public crate-cranelift-codegen-meta-0.105.2 (c (n "cranelift-codegen-meta") (v "0.105.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.105.2") (d #t) (k 0)))) (h "1wdycx9mvy1f84w22h172gx0l24a84fp9isczgi2rjm96xib7rhh")))

(define-public crate-cranelift-codegen-meta-0.105.3 (c (n "cranelift-codegen-meta") (v "0.105.3") (d (list (d (n "cranelift-codegen-shared") (r "^0.105.3") (d #t) (k 0)))) (h "0q9va4rvych99fj5n44h2lnkd1lhs0hpb87w7pvp1h45b7cdak6j")))

(define-public crate-cranelift-codegen-meta-0.106.0 (c (n "cranelift-codegen-meta") (v "0.106.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.106.0") (d #t) (k 0)))) (h "1nxx8wqrc9zcw481jqr40g4i4m0hagr2bpflp9gq73ihfibjx534")))

(define-public crate-cranelift-codegen-meta-0.106.1 (c (n "cranelift-codegen-meta") (v "0.106.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.106.1") (d #t) (k 0)))) (h "04kjifs7vmg3rap630khi0wnh16pr83viaii9pcl28s68215pcz4")))

(define-public crate-cranelift-codegen-meta-0.105.4 (c (n "cranelift-codegen-meta") (v "0.105.4") (d (list (d (n "cranelift-codegen-shared") (r "^0.105.4") (d #t) (k 0)))) (h "0ry18vip2vjsjcnxzx06ygr53dg42xwjwgjsad1cl14axzcjwk33")))

(define-public crate-cranelift-codegen-meta-0.106.2 (c (n "cranelift-codegen-meta") (v "0.106.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.106.2") (d #t) (k 0)))) (h "1nm104c3c5bj7w3ai25rzf3nb320lw0djzybas4if056c0ag42xl")))

(define-public crate-cranelift-codegen-meta-0.104.3 (c (n "cranelift-codegen-meta") (v "0.104.3") (d (list (d (n "cranelift-codegen-shared") (r "^0.104.3") (d #t) (k 0)))) (h "0bsyihl8ig8v285y70wrkgv0428q2ybq5phdbiapjbad30hck4za")))

(define-public crate-cranelift-codegen-meta-0.107.0 (c (n "cranelift-codegen-meta") (v "0.107.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.107.0") (d #t) (k 0)))) (h "04zncvb8gmiqz2zqcwdrykrqm548b3rgk9swgkv0f7ark5mwalqn")))

(define-public crate-cranelift-codegen-meta-0.107.1 (c (n "cranelift-codegen-meta") (v "0.107.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.107.1") (d #t) (k 0)))) (h "12raa48v9jzrk5prsr54xbmxc098r3lsv648jrc02n57w5r0cndz")))

(define-public crate-cranelift-codegen-meta-0.107.2 (c (n "cranelift-codegen-meta") (v "0.107.2") (d (list (d (n "cranelift-codegen-shared") (r "^0.107.2") (d #t) (k 0)))) (h "0g21p3gqli1724q3gjrb394p5rsqnnn1dp00fl3d8y3ya5mfr6xl")))

(define-public crate-cranelift-codegen-meta-0.108.0 (c (n "cranelift-codegen-meta") (v "0.108.0") (d (list (d (n "cranelift-codegen-shared") (r "^0.108.0") (d #t) (k 0)))) (h "14i5x3c8pq3a36hvp9rbh6bz3wfmp203vkqkrp60s6862vqfznic")))

(define-public crate-cranelift-codegen-meta-0.108.1 (c (n "cranelift-codegen-meta") (v "0.108.1") (d (list (d (n "cranelift-codegen-shared") (r "^0.108.1") (d #t) (k 0)))) (h "043wkljd965ismg4m5jamrq4c4z81rgd64wmms6ps0whfhkwzx99")))

