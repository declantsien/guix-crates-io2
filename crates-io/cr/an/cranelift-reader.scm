(define-module (crates-io cr an cranelift-reader) #:use-module (crates-io))

(define-public crate-cranelift-reader-0.14.0 (c (n "cranelift-reader") (v "0.14.0") (d (list (d (n "cranelift-codegen") (r "^0.14.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "09nzcalxcx14kzzbwfcdi44rzskj1wdr11qz9irizcjrgm8zwjyv")))

(define-public crate-cranelift-reader-0.15.0 (c (n "cranelift-reader") (v "0.15.0") (d (list (d (n "cranelift-codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "1ij7ag59074903igrw788l1gp5y7xzds2bwyywxx1r4i15x1f8rv")))

(define-public crate-cranelift-reader-0.16.0 (c (n "cranelift-reader") (v "0.16.0") (d (list (d (n "cranelift-codegen") (r "^0.16.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "0k763clvf297izhna3yyrqvq5yk9f0dc7kk4qhad7hdgsnwnifv5")))

(define-public crate-cranelift-reader-0.16.1 (c (n "cranelift-reader") (v "0.16.1") (d (list (d (n "cranelift-codegen") (r "^0.16.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "1cqzx104mqglzh25q7mginr3682rqvy4sh7nazysi38463lwjrki")))

(define-public crate-cranelift-reader-0.17.0 (c (n "cranelift-reader") (v "0.17.0") (d (list (d (n "cranelift-codegen") (r "^0.17.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "0l9md1svhw9m6fwp68qk3xrfjnclc55f4wzf2qiy9rdj9czy54jr")))

(define-public crate-cranelift-reader-0.18.1 (c (n "cranelift-reader") (v "0.18.1") (d (list (d (n "cranelift-codegen") (r "^0.18.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "1r7mp366wmx6rra8v2c4z107q2am1816kk5ni0zlv6f909797hak")))

(define-public crate-cranelift-reader-0.19.0 (c (n "cranelift-reader") (v "0.19.0") (d (list (d (n "cranelift-codegen") (r "^0.19.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "1pgqywdqzc8ljy00lmc0a1q4slya1sw8wibdrp890yin3wz50vbx")))

(define-public crate-cranelift-reader-0.20.0 (c (n "cranelift-reader") (v "0.20.0") (d (list (d (n "cranelift-codegen") (r "^0.20.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "1rmlrs98jrnk8g4x1zdc1db2nrx3ah8ams4ff9nz0rgbxfmalmdx")))

(define-public crate-cranelift-reader-0.21.0 (c (n "cranelift-reader") (v "0.21.0") (d (list (d (n "cranelift-codegen") (r "^0.21.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "0ql35krp6i5pcilv18bgmi16mx85pfjiqdq0dlmcyp0g716cpz6m")))

(define-public crate-cranelift-reader-0.21.1 (c (n "cranelift-reader") (v "0.21.1") (d (list (d (n "cranelift-codegen") (r "^0.21.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "1v7i1v4vnsvah9vs330fmvcncpljr83ydx5r5ybk2m8f0hq7lm8g")))

(define-public crate-cranelift-reader-0.22.0 (c (n "cranelift-reader") (v "0.22.0") (d (list (d (n "cranelift-codegen") (r "^0.22.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "1gw06hm7zkjc36ygmz96vv8ynjrq2cnf7sbjb7h7sjzj25fkw33w")))

(define-public crate-cranelift-reader-0.23.0 (c (n "cranelift-reader") (v "0.23.0") (d (list (d (n "cranelift-codegen") (r "^0.23.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.2.0") (d #t) (k 0)))) (h "0d2rg9zwxvsqs2v5mygm2drjmk855gqzq9p5xz6zvin5gf562arf")))

(define-public crate-cranelift-reader-0.24.0 (c (n "cranelift-reader") (v "0.24.0") (d (list (d (n "cranelift-codegen") (r "^0.24.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.2.0") (d #t) (k 0)))) (h "10i3asb8lfhzsbfbbsjmh74jclzxbpaqziff4rbri8bc6x3p82xr")))

(define-public crate-cranelift-reader-0.25.0 (c (n "cranelift-reader") (v "0.25.0") (d (list (d (n "cranelift-codegen") (r "^0.25.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.2.0") (d #t) (k 0)))) (h "0wvcx2kkhf3qqp2fn6m2bgfvnjxg67bchhjk45j2ngm9bl9zlqvq")))

(define-public crate-cranelift-reader-0.26.0 (c (n "cranelift-reader") (v "0.26.0") (d (list (d (n "cranelift-codegen") (r "^0.26.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.2.0") (d #t) (k 0)))) (h "0na1ydgqg81s6735hrllaf89fqsw7l6nrc2mj38q3hg08kfzndfq")))

(define-public crate-cranelift-reader-0.27.0 (c (n "cranelift-reader") (v "0.27.0") (d (list (d (n "cranelift-codegen") (r "^0.27.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.2.0") (d #t) (k 0)))) (h "0qy5vys3vfgcy2zraarhz9sw0l56m3psk3qbp5znap07bb9b5k08")))

(define-public crate-cranelift-reader-0.28.0 (c (n "cranelift-reader") (v "0.28.0") (d (list (d (n "cranelift-codegen") (r "^0.28.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.2.0") (d #t) (k 0)))) (h "05dfphx384spyczkqps5m3b6psv0hrm7jqvmamh57q34xjv03h47")))

(define-public crate-cranelift-reader-0.29.0 (c (n "cranelift-reader") (v "0.29.0") (d (list (d (n "cranelift-codegen") (r "^0.29.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.2.0") (d #t) (k 0)))) (h "1ps8qnvz0pnvaykicsbvdqcbcq4ppqih5dgw7psrn429xmy03l70")))

(define-public crate-cranelift-reader-0.30.0 (c (n "cranelift-reader") (v "0.30.0") (d (list (d (n "cranelift-codegen") (r "^0.30.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.3.0") (d #t) (k 0)))) (h "1gxrr37fck5z47b1g4v34wabk3q6r6k2b840j7fsvm8q9yrgjh05")))

(define-public crate-cranelift-reader-0.31.0 (c (n "cranelift-reader") (v "0.31.0") (d (list (d (n "cranelift-codegen") (r "^0.31.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "04567b2r2vnbs796hyjx19xnx5g2yz0jaib86lhkyjpy5vzkgj6z")))

(define-public crate-cranelift-reader-0.32.0 (c (n "cranelift-reader") (v "0.32.0") (d (list (d (n "cranelift-codegen") (r "^0.32.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "1w9fcrf04mm96hn5q5ndr61q0r96pdkgdf8bmjc1i9bhh9x2n6lb")))

(define-public crate-cranelift-reader-0.33.0 (c (n "cranelift-reader") (v "0.33.0") (d (list (d (n "cranelift-codegen") (r "^0.33.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "0f9gfyrhzv0pwbx2apv6y3imsm8g7aq2g3bsxi7arq6cizf62nd3")))

(define-public crate-cranelift-reader-0.34.0 (c (n "cranelift-reader") (v "0.34.0") (d (list (d (n "cranelift-codegen") (r "^0.34.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "0li11pzcalsi37i3a6d07cl38w86ax0pdbncngj5nsifr34qan4r")))

(define-public crate-cranelift-reader-0.35.0 (c (n "cranelift-reader") (v "0.35.0") (d (list (d (n "cranelift-codegen") (r "^0.35.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "0zhvx7cfva1gxzzz2drakb7sycz3gh1qggks7l583hw4p0n75w58")))

(define-public crate-cranelift-reader-0.36.0 (c (n "cranelift-reader") (v "0.36.0") (d (list (d (n "cranelift-codegen") (r "^0.36.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "1mfpzhsb9w1ppv8fwib9ckrgn4p0z48ifd9z39pszw1ima108ngw")))

(define-public crate-cranelift-reader-0.37.0 (c (n "cranelift-reader") (v "0.37.0") (d (list (d (n "cranelift-codegen") (r "^0.37.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "01sl85xnjc3g8p2xay09wjfkgmaq5a9yka46yzbv0a0d0yqns3b5")))

(define-public crate-cranelift-reader-0.38.0 (c (n "cranelift-reader") (v "0.38.0") (d (list (d (n "cranelift-codegen") (r "^0.38.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "0hy9znqar1lw3bi72my4zcds0w8k0zv14y6yacvhdr5aahf6f6wa")))

(define-public crate-cranelift-reader-0.39.0 (c (n "cranelift-reader") (v "0.39.0") (d (list (d (n "cranelift-codegen") (r "^0.39.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "1ci0509vmdspcjhx6jrpzs60n8ddkrl76ix68707j4nanj0z9lq1")))

(define-public crate-cranelift-reader-0.40.0 (c (n "cranelift-reader") (v "0.40.0") (d (list (d (n "cranelift-codegen") (r "^0.40.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "1cddfmwcp81mxkag0ac0snp4bdwh7120xwgnwprij45ja879vzlx")))

(define-public crate-cranelift-reader-0.41.0 (c (n "cranelift-reader") (v "0.41.0") (d (list (d (n "cranelift-codegen") (r "^0.41.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)))) (h "11v7dnk51jxg85ldzz4rclw5cjk900rckkrnhs3j5krqr0c32xky")))

(define-public crate-cranelift-reader-0.42.0 (c (n "cranelift-reader") (v "0.42.0") (d (list (d (n "cranelift-codegen") (r "^0.42.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.0") (d #t) (k 0)))) (h "1ggyscs1k8ssfijshaa61x2xx8k4rlsdamkgmwrjaxy189xb2nf7")))

(define-public crate-cranelift-reader-0.43.0 (c (n "cranelift-reader") (v "0.43.0") (d (list (d (n "cranelift-codegen") (r "^0.43.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "0bjzb91lx3614c4d31yqzjdqfhh0inqi576n6nw71azygnhw09gr")))

(define-public crate-cranelift-reader-0.43.1 (c (n "cranelift-reader") (v "0.43.1") (d (list (d (n "cranelift-codegen") (r "^0.43.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "1vsvma5d7gj3wfx7dlb0l58znz3b3b6gsnp07b9x38chbhpjjp6g")))

(define-public crate-cranelift-reader-0.44.0 (c (n "cranelift-reader") (v "0.44.0") (d (list (d (n "cranelift-codegen") (r "^0.44.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "0jvhqv87p45jwlgzkgdnckyx9lfk01bll34vb74k9d852fc21rsa")))

(define-public crate-cranelift-reader-0.45.0 (c (n "cranelift-reader") (v "0.45.0") (d (list (d (n "cranelift-codegen") (r "^0.45.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "06c785k0anyvgfa9n05d9i5psyxifwdbg2z6fa6waly7zxvvqnh1")))

(define-public crate-cranelift-reader-0.46.0 (c (n "cranelift-reader") (v "0.46.0") (d (list (d (n "cranelift-codegen") (r "^0.46.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "08cd31mnywic564l17mq4fhlk7q91hc7s71b389x9f99y1bf65jr")))

(define-public crate-cranelift-reader-0.46.1 (c (n "cranelift-reader") (v "0.46.1") (d (list (d (n "cranelift-codegen") (r "^0.46.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "0143yvggwcw5y0dwmz1nmd9a4c42z2d2jswayhyybnlpgwk2722i")))

(define-public crate-cranelift-reader-0.47.0 (c (n "cranelift-reader") (v "0.47.0") (d (list (d (n "cranelift-codegen") (r "^0.47.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "13wa6cf43q512rdv2hbyyjjzh5v1hbxqfwnyym90hl3q3r01bd58")))

(define-public crate-cranelift-reader-0.48.0 (c (n "cranelift-reader") (v "0.48.0") (d (list (d (n "cranelift-codegen") (r "^0.48.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.8.1") (d #t) (k 0)))) (h "10kjcc4nvxgzx5m44bxgv85yhib2z4jfalwz6y4a8a4yvfnldfsp")))

(define-public crate-cranelift-reader-0.49.0 (c (n "cranelift-reader") (v "0.49.0") (d (list (d (n "cranelift-codegen") (r "^0.49.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 0)))) (h "1i5222kgvbwagwh46r7x2l7bhpsmv9hv72gbjanf4v7kx9cai9f7")))

(define-public crate-cranelift-reader-0.50.0 (c (n "cranelift-reader") (v "0.50.0") (d (list (d (n "cranelift-codegen") (r "^0.50.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 0)))) (h "191wfx0p9c73gfg9nykjfcidqb2arq0s9wih89613ilqrv5ss3x3")))

(define-public crate-cranelift-reader-0.51.0 (c (n "cranelift-reader") (v "0.51.0") (d (list (d (n "cranelift-codegen") (r "^0.51.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 0)))) (h "1v2y9rrd8k564v2k5gvfx8iqbi7qf1n99zb5ziz184lg8r6qdgm5")))

(define-public crate-cranelift-reader-0.52.0 (c (n "cranelift-reader") (v "0.52.0") (d (list (d (n "cranelift-codegen") (r "^0.52.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9") (d #t) (k 0)))) (h "0vv243n0n4iymgf1fhf70zdd85y7mqnrpxcycdsjkd8jk7dl220s")))

(define-public crate-cranelift-reader-0.53.0 (c (n "cranelift-reader") (v "0.53.0") (d (list (d (n "cranelift-codegen") (r "^0.53.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "0bka6893vz6j0kgyzs03b4nbrnhnpf1r4f0jgwgsqs8kipwm8lq7")))

(define-public crate-cranelift-reader-0.54.0 (c (n "cranelift-reader") (v "0.54.0") (d (list (d (n "cranelift-codegen") (r "^0.54.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "1hh1zmnl461hfn4lsri3yzhsxz7f227cp336rl711w153r8ix6dz")))

(define-public crate-cranelift-reader-0.55.0 (c (n "cranelift-reader") (v "0.55.0") (d (list (d (n "cranelift-codegen") (r "^0.55.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "19133qhwfrfgqiif5ixhpsmmfmhmvz342hh8x6cczdzb9va2zify")))

(define-public crate-cranelift-reader-0.56.0 (c (n "cranelift-reader") (v "0.56.0") (d (list (d (n "cranelift-codegen") (r "^0.56.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "03dqzpqs5lnz4g14hxanq4f68al9rcj1layls947v607jb91f1gq")))

(define-public crate-cranelift-reader-0.58.0 (c (n "cranelift-reader") (v "0.58.0") (d (list (d (n "cranelift-codegen") (r "^0.58.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "16lsy61navp929mmrz17xmam3m6x8bfc2xgyaz44xdammxf6pmbk")))

(define-public crate-cranelift-reader-0.59.0 (c (n "cranelift-reader") (v "0.59.0") (d (list (d (n "cranelift-codegen") (r "^0.59.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "16m764302y7alq5l43zdkplzc063xjhvxj27ck1hl8f52v0afgq7")))

(define-public crate-cranelift-reader-0.60.0 (c (n "cranelift-reader") (v "0.60.0") (d (list (d (n "cranelift-codegen") (r "^0.60.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "0bbhi86g81nw0inxx340prdg07wykjnn0mrlgzj4c1hfzxaqys25")))

(define-public crate-cranelift-reader-0.61.0 (c (n "cranelift-reader") (v "0.61.0") (d (list (d (n "cranelift-codegen") (r "^0.61.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "16rm1xjzcrk4yqhkzzzdh4vq2k09hi47shm3f7sjjqfcfirabb9w")))

(define-public crate-cranelift-reader-0.62.0 (c (n "cranelift-reader") (v "0.62.0") (d (list (d (n "cranelift-codegen") (r "^0.62.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "041gafrwnp5xq848p3w7v9kbb75yw4p5wqfm2nk0c6m92zl2vw6w")))

(define-public crate-cranelift-reader-0.63.0 (c (n "cranelift-reader") (v "0.63.0") (d (list (d (n "cranelift-codegen") (r "^0.63.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)))) (h "0drv4zmkvrn267jqdya2v4wxm1wfh841zk53axglp1jgi719v9qp")))

(define-public crate-cranelift-reader-0.64.0 (c (n "cranelift-reader") (v "0.64.0") (d (list (d (n "cranelift-codegen") (r "^0.64.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1qh3y9sajwikgqk0nqpya4qp9p5sz683vvpslvd0381cs4422w8z")))

(define-public crate-cranelift-reader-0.65.0 (c (n "cranelift-reader") (v "0.65.0") (d (list (d (n "cranelift-codegen") (r "^0.65.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "059d3iinpizsisl29kk1bama52llr2833xh8n8szmm3scgrp5hbd")))

(define-public crate-cranelift-reader-0.66.0 (c (n "cranelift-reader") (v "0.66.0") (d (list (d (n "cranelift-codegen") (r "^0.66.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0irfw562pvpcxwq4lam1nzn2iaz17lbra4s2czlx28ini6fd5la9")))

(define-public crate-cranelift-reader-0.67.0 (c (n "cranelift-reader") (v "0.67.0") (d (list (d (n "cranelift-codegen") (r "^0.67.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0mc09vic0cg6457zjgjpqafzk6k61c2v8lxy8yw8gjap1jrg0i0x")))

(define-public crate-cranelift-reader-0.68.0 (c (n "cranelift-reader") (v "0.68.0") (d (list (d (n "cranelift-codegen") (r "^0.68.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1qbiipms02zmb8kf7hdvsa6i873ra89r56f08p115vxakjcn72fp") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.69.0 (c (n "cranelift-reader") (v "0.69.0") (d (list (d (n "cranelift-codegen") (r "^0.69.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1728ygj8ylzvi14jfrzsy5vd2vc73qw3n3x149hfhpf3wdi2h0pf") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.70.0 (c (n "cranelift-reader") (v "0.70.0") (d (list (d (n "cranelift-codegen") (r "^0.70.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0xycdgd8zyqfjx79ar9krf9r89rr5c0mc4nq3qwr9bz2ccrizqni") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.71.0 (c (n "cranelift-reader") (v "0.71.0") (d (list (d (n "cranelift-codegen") (r "^0.71.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "075wgrizk1d1wm8ld8jj222820nz6kjz6jlbjn4cb0pqm3q7caw4") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.72.0 (c (n "cranelift-reader") (v "0.72.0") (d (list (d (n "cranelift-codegen") (r "^0.72.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "19v3zfwlvl9n7ffimyxryq5m6j1l93c1n6irc0qmlj0w4xsacy1q") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.73.0 (c (n "cranelift-reader") (v "0.73.0") (d (list (d (n "cranelift-codegen") (r "^0.73.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0jgyp2vkx62ys2d8hfg4b2lx92476m4qhc6wfyfv6qj95lmc5560") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.74.0 (c (n "cranelift-reader") (v "0.74.0") (d (list (d (n "cranelift-codegen") (r "^0.74.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0sv4qy2z3hpmwx7v3wc3ljpxh5yds49c5wl14ynn4s60qfs2xv3v") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.73.1 (c (n "cranelift-reader") (v "0.73.1") (d (list (d (n "cranelift-codegen") (r "^0.73.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0jvmkj5sdv6n8bkp82dg5dpa7jn7jw17rq5k0iiyllg0cjkf8gb9") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.75.0 (c (n "cranelift-reader") (v "0.75.0") (d (list (d (n "cranelift-codegen") (r "^0.75.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "10rx0nll9mcrc7r925ds9x4b7rr5ax624qdlhcjl9idp7afjaflr") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.76.0 (c (n "cranelift-reader") (v "0.76.0") (d (list (d (n "cranelift-codegen") (r "^0.76.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "10rc3i54k4j46jgrmkrj534254q5djlzyj6hjw5zfv5v2x0ivkj8") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.77.0 (c (n "cranelift-reader") (v "0.77.0") (d (list (d (n "cranelift-codegen") (r "^0.77.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1240fky9hjgv148367ji08hnpk26307pw853z2iw6lwfs7i5isc1") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.78.0 (c (n "cranelift-reader") (v "0.78.0") (d (list (d (n "cranelift-codegen") (r "^0.78.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1bsz98cr6r7zcn66w8022wgc0bcc021qmkkkq0hv6cixs4s325kp") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.79.0 (c (n "cranelift-reader") (v "0.79.0") (d (list (d (n "cranelift-codegen") (r "^0.79.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0sjh6z20qv35v2z26gkiq75bw9z8bskr2b9wzaq5g7bbixnh7dg3") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.79.1 (c (n "cranelift-reader") (v "0.79.1") (d (list (d (n "cranelift-codegen") (r "^0.79.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0vjp8a531kwkn4c3041jkqbfkriirx2bny0b31pzc2md4ciph3dl") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.80.0 (c (n "cranelift-reader") (v "0.80.0") (d (list (d (n "cranelift-codegen") (r "^0.80.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0xm6578zlqyzjcw7zpsrmx9aw1hc36j551mp46s2j93jdgf21mdq") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.81.0 (c (n "cranelift-reader") (v "0.81.0") (d (list (d (n "cranelift-codegen") (r "^0.81.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1zl98gsz8j7fmnv9g41wlq82aairkbvywp5qgj5wrqi4idcribjd") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.81.1 (c (n "cranelift-reader") (v "0.81.1") (d (list (d (n "cranelift-codegen") (r "^0.81.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "19i69ikklhhj8rbmqnglywk2wzkmms4mgj7fx16ni4nl8hdalmkq") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.80.1 (c (n "cranelift-reader") (v "0.80.1") (d (list (d (n "cranelift-codegen") (r "^0.80.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0kp0v68yydm28rm76bv6540b9h0ficzvjvp9wyh2ljv66yh2xfh4") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.82.0 (c (n "cranelift-reader") (v "0.82.0") (d (list (d (n "cranelift-codegen") (r "^0.82.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "16hjvsffdy22xzcpfysj9a4swvbg38wp2njv2fx5frh8pjhbxvm2") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.82.1 (c (n "cranelift-reader") (v "0.82.1") (d (list (d (n "cranelift-codegen") (r "^0.82.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0s9mcsazgzdh9sbfsfngifjv4v5sfspfxrsakvidxh9qb8c3cch2") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.82.2 (c (n "cranelift-reader") (v "0.82.2") (d (list (d (n "cranelift-codegen") (r "^0.82.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1s6hk62m1gknbf1x7jx0g3m9898sdv4bgwma73m5f59j3fmlq32d") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.81.2 (c (n "cranelift-reader") (v "0.81.2") (d (list (d (n "cranelift-codegen") (r "^0.81.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "07k0f4wz1bxkqbcms6z6iwi6z93ym1nai423fmmkc91j07sjyr0f") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.82.3 (c (n "cranelift-reader") (v "0.82.3") (d (list (d (n "cranelift-codegen") (r "^0.82.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1py5s0kmffnsvmkplydnqdgf8kvaskk9phldsbjix8d1azj6gl1g") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.83.0 (c (n "cranelift-reader") (v "0.83.0") (d (list (d (n "cranelift-codegen") (r "^0.83.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1iygr164yrl4jpdlsyszs8qc11ia3kjngia4lfjmspj574175bia") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.84.0 (c (n "cranelift-reader") (v "0.84.0") (d (list (d (n "cranelift-codegen") (r "^0.84.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "171kjln5bxv9kmpgi8zk3g5vqf0wxg727r9wlw3cikl922kpwjma") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.85.0 (c (n "cranelift-reader") (v "0.85.0") (d (list (d (n "cranelift-codegen") (r "^0.85.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "01ml4y7pfq3hwj6c0bbliq3c90axni90dz5nq2r238wksjd4mbny") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.85.1 (c (n "cranelift-reader") (v "0.85.1") (d (list (d (n "cranelift-codegen") (r "^0.85.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1s9y7w45b7wpwxx8ny4dx3kyy9520ly4mykbiz8fi7ilvpis6i5a") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.86.0 (c (n "cranelift-reader") (v "0.86.0") (d (list (d (n "cranelift-codegen") (r "^0.86.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1s7zywgw5nizqhz9z1r72cyvihk9q7j26piva6fpsjzbw7v6n3kr") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.85.2 (c (n "cranelift-reader") (v "0.85.2") (d (list (d (n "cranelift-codegen") (r "^0.85.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0azy1xqd4gb3fmw2w4mm2ak2a1ynxzhir1rp7gjxddfhlg75rnmx") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.85.3 (c (n "cranelift-reader") (v "0.85.3") (d (list (d (n "cranelift-codegen") (r "^0.85.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "099fa255j4p2998956czg0r6gr338m1vqn6v0gfvizvb6zs64l5z") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.86.1 (c (n "cranelift-reader") (v "0.86.1") (d (list (d (n "cranelift-codegen") (r "^0.86.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1s9j6j4sg3l0736260kbgv7n0qpaqc70vvkm8csl6zql8xw90fql") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.87.0 (c (n "cranelift-reader") (v "0.87.0") (d (list (d (n "cranelift-codegen") (r "^0.87.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1d5hz1ng0n158x4gdi08md8ijjdmzrg6mskf40999d70bld2lcp0") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.87.1 (c (n "cranelift-reader") (v "0.87.1") (d (list (d (n "cranelift-codegen") (r "^0.87.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0659v41g279n69g6ghh92xaxc9j891q8gbzvf71i6jv8i8z6hpq1") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.88.0 (c (n "cranelift-reader") (v "0.88.0") (d (list (d (n "cranelift-codegen") (r "^0.88.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "17k51m988xh77vv7c31gbx0xji2pgbdpks98y5d62085s063bphc") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.88.1 (c (n "cranelift-reader") (v "0.88.1") (d (list (d (n "cranelift-codegen") (r "^0.88.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "0k2953q5i5nr0p8mvjcc3b8c75p0qklkpp1cdh9bi3rnkipbk43b") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.89.0 (c (n "cranelift-reader") (v "0.89.0") (d (list (d (n "cranelift-codegen") (r "^0.89.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (k 0)))) (h "03yjk65k8lczp6a13smam2wzyvhpgjwxb3f39gjrkfyvw62p9qpb") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.89.1 (c (n "cranelift-reader") (v "0.89.1") (d (list (d (n "cranelift-codegen") (r "^0.89.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (k 0)))) (h "0fhpmpfdkl95icfqi6kwbshdjxh118swv0kls031p5b6bawdgggw") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.89.2 (c (n "cranelift-reader") (v "0.89.2") (d (list (d (n "cranelift-codegen") (r "^0.89.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (k 0)))) (h "0l489kmmz2rixav0aj4ap2r3nd8ky4s2amcn9kq5ss84pnrpb5h8") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.88.2 (c (n "cranelift-reader") (v "0.88.2") (d (list (d (n "cranelift-codegen") (r "^0.88.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)))) (h "1vl761dqwdmm120xp1j89b7ylhcpyrcfw2phicsw9hf9b1bfjyv8") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.90.0 (c (n "cranelift-reader") (v "0.90.0") (d (list (d (n "cranelift-codegen") (r "^0.90.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (k 0)))) (h "02isi88l287ch1499q63bmw7rld21ckvg6b15clkzxxqf3igclc9") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.90.1 (c (n "cranelift-reader") (v "0.90.1") (d (list (d (n "cranelift-codegen") (r "^0.90.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (k 0)))) (h "19hw1mdnkpg7mdg8xvvwwvzfqvgvxx8xgn299z7ifchbsqpm76di") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.91.0 (c (n "cranelift-reader") (v "0.91.0") (d (list (d (n "cranelift-codegen") (r "^0.91.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (k 0)))) (h "0yvl5cbrng1bjr037wpnngmiwh20rf9gf3k8djqvxk78g5yrbls9") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.92.0 (c (n "cranelift-reader") (v "0.92.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.92.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1zn8hc9w326m56xc4zwhg1sh7s0jkla7s8r4ri241hv9c2hlzagr") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.93.0 (c (n "cranelift-reader") (v "0.93.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.93.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "091r7qv580h7lyc55sjijmlvws80c73yj36ksz5dfh7rs40ssdik") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.91.1 (c (n "cranelift-reader") (v "0.91.1") (d (list (d (n "cranelift-codegen") (r "^0.91.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (k 0)))) (h "04di54cizngw0ibws0pmj1z8zvn30d7jx5117dm06wgqjz0ba1c0") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.92.1 (c (n "cranelift-reader") (v "0.92.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.92.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1abh5lycqz3vads674bax31j2ykcw116piqgbfqc4h80w7wapsy3") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.93.1 (c (n "cranelift-reader") (v "0.93.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.93.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1cjczw8casfs2hk5n6dq0j6j8dnkiqnz2kg5zbr32yljpw05bbfr") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.94.0 (c (n "cranelift-reader") (v "0.94.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.94.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1srps5yls7pj6bmpc33dv52fk83khq051zv9rg3399vdxz7wkhnh") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.95.0 (c (n "cranelift-reader") (v "0.95.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.95.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0b7ppm00x7zmmrh3fyj6qga92xa7rlas7cbgnfhcmx5rg8yqyl5j") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.93.2 (c (n "cranelift-reader") (v "0.93.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.93.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0b5phl8ib910paw3pkvxnkpq29q9imadivip277hbys71fy9nn2q") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.95.1 (c (n "cranelift-reader") (v "0.95.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.95.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0a6dzpd0ibnqf0qfd37jzw5bcyd4g0a0vx5xcdhyd34gm8h3qslj") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.94.1 (c (n "cranelift-reader") (v "0.94.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.94.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "18fgcwp8g6gz1bqvgpaylh4swn0ww4avbnhwhkwbb978x5lxvgfl") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.96.0 (c (n "cranelift-reader") (v "0.96.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.96.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0xy87b2jwsjk02jdvbrgh4kfmpg5xn733ybr4zi741933jyfywlx") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.96.1 (c (n "cranelift-reader") (v "0.96.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.96.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1v28rkxbpk19kk6cq0iissd7im3c2prbh9rvik6w5swr5ncyld5h") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.96.2 (c (n "cranelift-reader") (v "0.96.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.96.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0yia4vqazhk8rxk6dsvgnms4i4d41klfdrqk534bf6hf7dngj0x0") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.96.3 (c (n "cranelift-reader") (v "0.96.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.96.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1i5fc1x1abniq54jd2pfaj7wxqdgprzfbg1gd6r7n2bckzl5cak1") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.96.4 (c (n "cranelift-reader") (v "0.96.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.96.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1sjj2zv6wc6pwghi30zw40pjppgf4988z2irlrrijnpv8x6jjyy8") (f (quote (("experimental_x64") ("default"))))))

(define-public crate-cranelift-reader-0.97.0 (c (n "cranelift-reader") (v "0.97.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.97.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1h5b0ycpsrwvwxz5a29ljpqavl8xpbx8qlsz7l6sgsxwlnwsk850")))

(define-public crate-cranelift-reader-0.97.1 (c (n "cranelift-reader") (v "0.97.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.97.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0br7qfl7n55z8b3qkpxw9cny2ssz76vc44y0cp28iny955iwrhjy")))

(define-public crate-cranelift-reader-0.98.0 (c (n "cranelift-reader") (v "0.98.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.98.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0lixbiav2lq5dbsa22bi3ambiwcj637wdwgnbwhx7lg8gwi5a4xc")))

(define-public crate-cranelift-reader-0.98.1 (c (n "cranelift-reader") (v "0.98.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.98.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0g4igq68c500rg6qyaamiq7a7sqfmdy89c9cnqlwy4364pv3la0k")))

(define-public crate-cranelift-reader-0.99.0 (c (n "cranelift-reader") (v "0.99.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.99.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1xral0c5fzn0c6828k8bwwwrgpa77b6b7bp4zb2sdg1561qjwrj8")))

(define-public crate-cranelift-reader-0.99.1 (c (n "cranelift-reader") (v "0.99.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.99.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1fpw2mjhzq7kdhr377qpy66kv4m1p0ibaxs7wkq3kzhwdf1y6hrn")))

(define-public crate-cranelift-reader-0.97.2 (c (n "cranelift-reader") (v "0.97.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.97.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0ncscdilhgyw8xrliz4xhmkb2nh26fycb6rn6vv965ninnm8lcaf")))

(define-public crate-cranelift-reader-0.98.2 (c (n "cranelift-reader") (v "0.98.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.98.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "02bvnj1mankaf1kym6vwyv538rvi9k0hqpc4m52x6cjv0dzizmp0")))

(define-public crate-cranelift-reader-0.99.2 (c (n "cranelift-reader") (v "0.99.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.99.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "11g6d6zby2h0hw5wphm97gs89gh8v0dj2l89wciz6633wd8gzr1m")))

(define-public crate-cranelift-reader-0.100.0 (c (n "cranelift-reader") (v "0.100.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.100.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1y9bc67dd6yvy108069xs03jn778n2gzg0p2a3mizbgxvklcnp7c")))

(define-public crate-cranelift-reader-0.101.0 (c (n "cranelift-reader") (v "0.101.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.101.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "16ilyw958vb5x0wwmk0gvday78wy98agv72wrwhcafv7l4hdg8y5")))

(define-public crate-cranelift-reader-0.101.1 (c (n "cranelift-reader") (v "0.101.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.101.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0hlcvizq2b9z5p73q1dpxcmgqrhr5dm3qm21ilj7gyspkh938zia")))

(define-public crate-cranelift-reader-0.100.1 (c (n "cranelift-reader") (v "0.100.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.100.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "0ibfzw9qbpya442pm4gr0vqgh9v14gsgk9gzg2vjwn0icfyrb7d1")))

(define-public crate-cranelift-reader-0.101.2 (c (n "cranelift-reader") (v "0.101.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.101.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1c46hgxissz4ndcxfifdablchdgwxi2psmkiib82jspk67qsq9vy")))

(define-public crate-cranelift-reader-0.101.3 (c (n "cranelift-reader") (v "0.101.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.101.3") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "01cgapynnn6z6ngy6h5y9zylnbnp75d45qiww5nmq8x1fpcfyanr")))

(define-public crate-cranelift-reader-0.101.4 (c (n "cranelift-reader") (v "0.101.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.101.4") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (f (quote ("std"))) (k 0)))) (h "1bv1z7m0gm27p6m3p99dnvi9yn66lvbpiph36wyhzzxh2pix1ff2")))

(define-public crate-cranelift-reader-0.102.0 (c (n "cranelift-reader") (v "0.102.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.102.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "0zrwb2qbhmfb0rjbq6fgyigxsrdvafccy2c9nzx87c8rksgl8hb0")))

(define-public crate-cranelift-reader-0.102.1 (c (n "cranelift-reader") (v "0.102.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.102.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "165br1bk1y7d65gbgn4917qxr47y77biwmhm8ffp0cga7ric88z9")))

(define-public crate-cranelift-reader-0.103.0 (c (n "cranelift-reader") (v "0.103.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.103.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "1m1jim4qnfg82vd80sh8km041ffsmv30hzw7yyccih441lzhjsfv")))

(define-public crate-cranelift-reader-0.104.0 (c (n "cranelift-reader") (v "0.104.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.104.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "00pkim6dylwclvzzjq6nilx552k49hkln7kbykw8xy1j61nj5ij4")))

(define-public crate-cranelift-reader-0.104.1 (c (n "cranelift-reader") (v "0.104.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.104.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "1165zhhqqz7m6zfyrbc7qj7zp2p7skdc0awv9xrq39f7wc4pg7yk")))

(define-public crate-cranelift-reader-0.105.0 (c (n "cranelift-reader") (v "0.105.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.105.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "0zlqbkzaq74qzg45vv9cmdiw9halsawb6w10vpamp0ixx2v4v2sb")))

(define-public crate-cranelift-reader-0.105.1 (c (n "cranelift-reader") (v "0.105.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.105.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "1h6dchxdpbdis7dyxix0kysly3ar8s8xs5qklsjfh0qd7gf8k84j")))

(define-public crate-cranelift-reader-0.104.2 (c (n "cranelift-reader") (v "0.104.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.104.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "0rq25l2r5haqfn5c6r68gfr8ny974b39znjlrm9c5sffic7zf2w1")))

(define-public crate-cranelift-reader-0.105.2 (c (n "cranelift-reader") (v "0.105.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.105.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "1dk0hj72q1bm95hwrjjpw5wbzkdbrrbv5ydn3wxnxpxn8n9mnc4s")))

(define-public crate-cranelift-reader-0.105.3 (c (n "cranelift-reader") (v "0.105.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.105.3") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "1kiyd3sf7qjsw0arrcvbjk36lb5c268agi3s42v7bqww7qwfpvx9")))

(define-public crate-cranelift-reader-0.106.0 (c (n "cranelift-reader") (v "0.106.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.106.0") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (k 0)))) (h "195zrki4andqivfc1922djlphzysml1bkf35cv3lp10d1jiqpl05")))

(define-public crate-cranelift-reader-0.106.1 (c (n "cranelift-reader") (v "0.106.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.106.1") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (k 0)))) (h "0n0c9mk9vsk812abqn9cs1zj13lmnqk3vrlml1d8zqpnrhwiwzl5")))

(define-public crate-cranelift-reader-0.105.4 (c (n "cranelift-reader") (v "0.105.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.105.4") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "0zkfwgp3szc08xlma9iyqqfalyf7l38kj74hqydm38kq14jrmx5c")))

(define-public crate-cranelift-reader-0.106.2 (c (n "cranelift-reader") (v "0.106.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.106.2") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (k 0)))) (h "1s5f6yzj3ci56svlqmxma6jirbd7ya3c1nxvy7ksbpdd6dq7b9px")))

(define-public crate-cranelift-reader-0.104.3 (c (n "cranelift-reader") (v "0.104.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.104.3") (f (quote ("std" "unwind"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.12") (f (quote ("std"))) (k 0)))) (h "1jxmpyfza95g2z30s2ggi9lw7v89x5ncyjkashaxgffckf0fiq2v")))

(define-public crate-cranelift-reader-0.107.0 (c (n "cranelift-reader") (v "0.107.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.107.0") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (k 0)))) (h "062cqxxdvdiwg3hvq1xgri8rbv49h58z1898a879k7wjicscq88p")))

(define-public crate-cranelift-reader-0.107.1 (c (n "cranelift-reader") (v "0.107.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.107.1") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (k 0)))) (h "12jxqf8y4aszj7brkqv9cjx3nd1w010vssmpd124abqkb3jcjrpy")))

(define-public crate-cranelift-reader-0.107.2 (c (n "cranelift-reader") (v "0.107.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.107.2") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (k 0)))) (h "1wvl5h5k6rgs7bjc8xjv7xykhxw85xzb9m046ynig6r14wq30fff")))

(define-public crate-cranelift-reader-0.108.0 (c (n "cranelift-reader") (v "0.108.0") (d (list (d (n "anyhow") (r "^1.0.22") (f (quote ("std"))) (k 0)) (d (n "cranelift-codegen") (r "^0.108.0") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (d #t) (k 0)))) (h "1vymjnyp1d551c5rg33p54g0c8qiwwy4hx7g5i6yrmc1ridv14ns")))

(define-public crate-cranelift-reader-0.108.1 (c (n "cranelift-reader") (v "0.108.1") (d (list (d (n "anyhow") (r "^1.0.22") (f (quote ("std"))) (k 0)) (d (n "cranelift-codegen") (r "^0.108.1") (f (quote ("std" "unwind" "trace-log"))) (k 0)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.13") (f (quote ("std"))) (d #t) (k 0)))) (h "07r8q85c44hn4k3wgk8qvibb1b8jkji28ngj6xl34rz3p5mpbpkb")))

