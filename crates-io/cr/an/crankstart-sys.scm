(define-module (crates-io cr an crankstart-sys) #:use-module (crates-io))

(define-public crate-crankstart-sys-0.1.1 (c (n "crankstart-sys") (v "0.1.1") (d (list (d (n "euclid") (r "^0.20.13") (f (quote ("libm"))) (k 0)))) (h "0k77y5drsv7wkwvffalw6w1bzxswhar15d79y19sshsaiav21nyx")))

(define-public crate-crankstart-sys-0.1.2 (c (n "crankstart-sys") (v "0.1.2") (d (list (d (n "euclid") (r "^0.20.13") (f (quote ("libm"))) (k 0)))) (h "1az57ncgj0wwgib3j7fljqlib0i2z721nx7lrq2sx4i580kybnhb")))

