(define-module (crates-io cr w- crw-types) #:use-module (crates-io))

(define-public crate-crw-types-0.1.0 (c (n "crw-types") (v "0.1.0") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1k18c00q1b5ccwff45j5pbpxqgabndzsz5xy0vnxn8cc1spviwfi") (y #t)))

