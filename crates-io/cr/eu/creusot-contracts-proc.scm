(define-module (crates-io cr eu creusot-contracts-proc) #:use-module (crates-io))

(define-public crate-creusot-contracts-proc-0.1.0 (c (n "creusot-contracts-proc") (v "0.1.0") (d (list (d (n "pearlite-syn") (r "^0.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xrm4xr9hlj6c4jg276lkbg4ldyvbfj4zl73qsnp9sjqv86b3irb")))

