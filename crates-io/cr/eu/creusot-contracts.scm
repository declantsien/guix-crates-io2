(define-module (crates-io cr eu creusot-contracts) #:use-module (crates-io))

(define-public crate-creusot-contracts-0.1.0 (c (n "creusot-contracts") (v "0.1.0") (d (list (d (n "creusot-contracts-dummy") (r "^0.1.0") (d #t) (k 0)) (d (n "creusot-contracts-proc") (r "^0.1.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.3.2") (d #t) (k 0)))) (h "0szhhn7drx8fvpc489j3lrdp4b6lq8xgqgdn445dy5nhs61abn4r") (f (quote (("typechecker") ("default") ("contracts"))))))

