(define-module (crates-io cr ow crows-macros) #:use-module (crates-io))

(define-public crate-crows-macros-0.1.0 (c (n "crows-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00g2jm6xcksl3pjbssj2kjv9qwgidk9mv39qhs6f4wx9n60qdqnd")))

(define-public crate-crows-macros-0.2.0 (c (n "crows-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mzmdsjd08px9frf3jgzk5pw0hb71gr2gxx55mkbyby5596xz5jy")))

(define-public crate-crows-macros-0.3.0 (c (n "crows-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0as8bszfjh85s8ydawcr08fav36f67zgi93iwm2nqgigz3k58l9r")))

