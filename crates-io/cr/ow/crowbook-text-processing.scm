(define-module (crates-io cr ow crowbook-text-processing) #:use-module (crates-io))

(define-public crate-crowbook-text-processing-0.1.0 (c (n "crowbook-text-processing") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1s22l4qic4p0a52rswpc8j4hp0rvxkxwl1vvaw254rvklf1bcbx2")))

(define-public crate-crowbook-text-processing-0.1.1 (c (n "crowbook-text-processing") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1jxii890amayfjq6jfwmvscvfayqbzzbgja74jv8d3jh4fwadfq3")))

(define-public crate-crowbook-text-processing-0.1.2 (c (n "crowbook-text-processing") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "033rvq5pjpigp1z6pil8m065ajr0z5bx8gzf82kl82zqzpkm1nnh")))

(define-public crate-crowbook-text-processing-0.1.3 (c (n "crowbook-text-processing") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1qdrbwggagxx639grbgkzbhcknaxva3z35jdb6pj9w502cxw9zs1")))

(define-public crate-crowbook-text-processing-0.1.4 (c (n "crowbook-text-processing") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0c92yf7g31j2y4iz04jig2v0mxx7g93xhzkzh0zlqd1w0ir3xyvn")))

(define-public crate-crowbook-text-processing-0.1.5 (c (n "crowbook-text-processing") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1xkbsmvh68ldpkm8fp6328vz3grjij9jzf8pnjcsc996g8pdg2r8")))

(define-public crate-crowbook-text-processing-0.1.6 (c (n "crowbook-text-processing") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "19ixm61xsc7nj5q2kddyb6rw80fqr14n2p4b1xsgp2zcg5w1mcvr")))

(define-public crate-crowbook-text-processing-0.2.0 (c (n "crowbook-text-processing") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "01mxllyaqmk81nynm1a9i1dzna5lrzr007x4i7nzx9w1w0skma1y")))

(define-public crate-crowbook-text-processing-0.2.1 (c (n "crowbook-text-processing") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "06cpy1p70c932zaahvvcdm40596zrvqs25c1c4pjf9m12yc5q8ba")))

(define-public crate-crowbook-text-processing-0.2.2 (c (n "crowbook-text-processing") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "001cdh6z2qr577qhq0sy28w8r3k45y9q65rw093871gms5lj0jwh")))

(define-public crate-crowbook-text-processing-0.2.3 (c (n "crowbook-text-processing") (v "0.2.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1xv0w1vrys2145m5l9w3zd4q0mnyqbb6p42xqfz8ff7pyr5rqxgr") (y #t)))

(define-public crate-crowbook-text-processing-0.2.4 (c (n "crowbook-text-processing") (v "0.2.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "13krah101drbgav6f2nc7c4gn2rv0ci841q9f7ka5yvv789nrcqr")))

(define-public crate-crowbook-text-processing-0.2.5 (c (n "crowbook-text-processing") (v "0.2.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "12qxr96laja1ypgyji9gp8cz34w40in2rhfn2q5znfvyq8jpqs1q")))

(define-public crate-crowbook-text-processing-0.2.6 (c (n "crowbook-text-processing") (v "0.2.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0y45zfg3s0xaq7yj8xl5pidlk2ibfzylb5gxbjv6b27kvwi29lrq")))

(define-public crate-crowbook-text-processing-0.2.7 (c (n "crowbook-text-processing") (v "0.2.7") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1d63g3vpc18kqqvm4ciwdnp6pgmy2ddzb0z247bv67qfwqfgz2k5")))

(define-public crate-crowbook-text-processing-0.2.8 (c (n "crowbook-text-processing") (v "0.2.8") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "09m7338g3xf4rq7ygi531arnd5ydkrqr4xs90w16iw29kcy06abx")))

(define-public crate-crowbook-text-processing-1.0.0 (c (n "crowbook-text-processing") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k5c16wxjayvl3wgbwfgqi8y765q9db7rlrbmf63jwpf0ffnpzp0")))

(define-public crate-crowbook-text-processing-1.1.0 (c (n "crowbook-text-processing") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rdscqz3npgihwyvbsp83mk6wp9rv0mgi2dy4903rrlq1jsa7d3r")))

(define-public crate-crowbook-text-processing-1.1.1 (c (n "crowbook-text-processing") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09wa14bzmwilg10myg1xq9p1w01ggs7l6rqkqcksf2xkw0gl8jd7")))

