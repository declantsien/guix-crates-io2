(define-module (crates-io cr ow crows-service) #:use-module (crates-io))

(define-public crate-crows-service-0.1.0 (c (n "crows-service") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09liajkjivxkkw68a43wrikk7irlz0sjfaxrhfn87b47vsggjj2n") (f (quote (("async"))))))

(define-public crate-crows-service-0.2.0 (c (n "crows-service") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wgvx7ldzwxs5a3p3923qvdcdmp988ab1yxgj1ih9l8nghjr14pk") (f (quote (("async"))))))

(define-public crate-crows-service-0.3.0 (c (n "crows-service") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13kwgbz8g5j7cwkinbxqnvhn2qcvg8p7sjb7dbn5p8wwn008n1v7") (f (quote (("async"))))))

