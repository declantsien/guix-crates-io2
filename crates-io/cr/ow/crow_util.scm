(define-module (crates-io cr ow crow_util) #:use-module (crates-io))

(define-public crate-crow_util-0.1.0 (c (n "crow_util") (v "0.1.0") (h "1zp1a8ghgflbx36wyjhzp15yl49q5aqmvqaqixlshfvglgx8hxkn") (y #t)))

(define-public crate-crow_util-0.1.1 (c (n "crow_util") (v "0.1.1") (h "10wp714qrjsrjmmn89lgdy73akrsc3264v9bdasf6j464nvi3z66") (y #t)))

(define-public crate-crow_util-0.1.2 (c (n "crow_util") (v "0.1.2") (h "1ykbyb8bd921f4icj7xd8ylgkfvywpx3i4a4bfch1cqzz1zgvp30") (y #t)))

(define-public crate-crow_util-0.1.3 (c (n "crow_util") (v "0.1.3") (h "1a7b4ml0ija3h12qfl4ayqpb3w61ncf0y3473f6w3iwqn7bzi68w") (y #t)))

(define-public crate-crow_util-0.1.4 (c (n "crow_util") (v "0.1.4") (h "0maj3665y31r411zgvgx4ljjzcyy1xa06bxs51g5r835l2lc0jls") (y #t)))

(define-public crate-crow_util-0.2.0 (c (n "crow_util") (v "0.2.0") (h "0b7nyg2c7yb44241g25cawifmpfk85sb83dwqmviw2s2v7xafbxx") (y #t)))

(define-public crate-crow_util-0.2.1 (c (n "crow_util") (v "0.2.1") (h "123251nnjkr7czl12i4hbk7qixay3h7yg38jwbvmhpjrxma5r2ak") (y #t)))

(define-public crate-crow_util-0.2.2 (c (n "crow_util") (v "0.2.2") (h "1a8808ylj83ihph0mhz5qq14j9azxsk1nyvwc50lz7sycyspzkp7") (y #t)))

(define-public crate-crow_util-0.2.3 (c (n "crow_util") (v "0.2.3") (h "052jmaqsydmqsrs666k2hakbh680bj1rx9i2n58ijb0rw2g5cxqp") (y #t)))

(define-public crate-crow_util-0.2.4 (c (n "crow_util") (v "0.2.4") (h "1nqb8nnhz0y4673aq3ankbhd5z5h847gnr3ckm6s9nfi22wrwqqc") (y #t)))

(define-public crate-crow_util-0.2.5 (c (n "crow_util") (v "0.2.5") (h "1ycljb66pfzm9n4iy3pzp2inz3j4a48kp7i03n6ripcllz99jzad") (y #t)))

(define-public crate-crow_util-0.2.5-1 (c (n "crow_util") (v "0.2.5-1") (h "1kxrkqdzn6rx5kdl4ninciis93yqn39085n65kaidwx86gjf3zkq") (y #t)))

(define-public crate-crow_util-0.3.0 (c (n "crow_util") (v "0.3.0") (h "1jykgj9xdadchvr72c3gg7cy8s0wp3i7d2xvh892aacl7yf2ivvq") (y #t)))

(define-public crate-crow_util-0.4.0 (c (n "crow_util") (v "0.4.0") (h "1x12dj7rhiavizhq3vjfbd543fx4qhdrra2zicnancs2zhmnyysh") (y #t)))

(define-public crate-crow_util-0.5.0 (c (n "crow_util") (v "0.5.0") (h "13kwbi4ffpfwwmly1p4kg0v6gfz8lvahy58xqvdllg2gn7779j9h") (y #t)))

