(define-module (crates-io cr ow crows-shared) #:use-module (crates-io))

(define-public crate-crows-shared-0.1.0 (c (n "crows-shared") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0724cixp0143bk7l7kjra10rg7drn8ikxbqwsbgwv51vd2inl3k4")))

(define-public crate-crows-shared-0.2.0 (c (n "crows-shared") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15kd59jgr4ivy16a9djach20gjnkkhg09zhg7vbmzjzn36n9kqm3")))

(define-public crate-crows-shared-0.3.0 (c (n "crows-shared") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0za0mvr9yvzszclxpjzb8f6djzb491nnm52455x7ljwd1bzjj6i0")))

