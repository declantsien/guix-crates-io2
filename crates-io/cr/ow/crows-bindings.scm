(define-module (crates-io cr ow crows-bindings) #:use-module (crates-io))

(define-public crate-crows-bindings-0.1.0 (c (n "crows-bindings") (v "0.1.0") (d (list (d (n "crows-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "crows-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.7") (d #t) (k 0)))) (h "0hkm3gziy390bsfydndf54ggpm3yyr3hh4ay8c32nxgby8n52ny7")))

(define-public crate-crows-bindings-0.2.0 (c (n "crows-bindings") (v "0.2.0") (d (list (d (n "crows-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "crows-shared") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.7") (d #t) (k 0)))) (h "1l7grqmrx6sfn02fkngp4sg4jmj4j9ib6zzz0zrd1wq45j35b561")))

(define-public crate-crows-bindings-0.3.0 (c (n "crows-bindings") (v "0.3.0") (d (list (d (n "crows-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "crows-shared") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.7") (d #t) (k 0)))) (h "107vkl9vgi3kwzrlzbys6f3467bkxm6kkxgjhhg4y0giy91p8hrx")))

