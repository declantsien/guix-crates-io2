(define-module (crates-io cr ow crowbook-localize) #:use-module (crates-io))

(define-public crate-crowbook-localize-0.0.1 (c (n "crowbook-localize") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1h0z0jnfvlg6v7dx6lj8y2dvfp5vgsgvkr837lmhgmk6r5yphlhr")))

(define-public crate-crowbook-localize-0.0.2 (c (n "crowbook-localize") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "14p0j8h4cblqk2cmprwjfkbcz7w5115srzn5lm1gf7drx74xz20p")))

(define-public crate-crowbook-localize-0.0.3 (c (n "crowbook-localize") (v "0.0.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1z9hbbzwf4prdrl9f7bg5hyk0124djm9nh3c7vv9i9whjr3hgdrk")))

(define-public crate-crowbook-localize-0.0.4 (c (n "crowbook-localize") (v "0.0.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "06n23p4zirdj7w2ksfwnj7r9f965xzq9x0j2mg4x9vidmp90s8wc")))

(define-public crate-crowbook-localize-0.0.5 (c (n "crowbook-localize") (v "0.0.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0kaddpn9gyzyzcx4apb3pb88kp1sib6arxw74x06nyyrrng1kizw")))

(define-public crate-crowbook-localize-0.0.6 (c (n "crowbook-localize") (v "0.0.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0c7lrvzhp62qa0x876c8aqd6hpdw4mnw9n0xfg6xhhb846lqns2j") (y #t)))

(define-public crate-crowbook-localize-0.0.7 (c (n "crowbook-localize") (v "0.0.7") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0i3v2d3xxvdykrlyb6wnmy147n1fbnpg0czrm0ll9iiqm5q9r87g")))

(define-public crate-crowbook-localize-0.0.8 (c (n "crowbook-localize") (v "0.0.8") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1zjy7j6fhjbnq64ac6095wslx8xsvkg6hcy21zk2rn7hx55njbmk")))

(define-public crate-crowbook-localize-0.0.9 (c (n "crowbook-localize") (v "0.0.9") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "03mqy55jz9km7s6dvbcavsiad6swwa7npb3q31sqf9yi91vxw7ds")))

(define-public crate-crowbook-localize-0.1.0 (c (n "crowbook-localize") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1b49bjcm69gns2g1lh4d5qddjhrndc397fza01l4bc2hffzp5jg1")))

