(define-module (crates-io cr ow crow_engine) #:use-module (crates-io))

(define-public crate-crow_engine-0.1.0 (c (n "crow_engine") (v "0.1.0") (d (list (d (n "crow_util") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (f (quote ("image"))) (k 0)))) (h "1mmxqqcsqbfajk0cdy8w4zrqnxxxyz1djlvszpp990wfyr34va6p") (y #t)))

(define-public crate-crow_engine-0.2.0-1 (c (n "crow_engine") (v "0.2.0-1") (d (list (d (n "crow_util") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (f (quote ("image"))) (k 0)))) (h "1jizv3bvpghacmznrxrajmqk0a2yfcycr8yb9nyzwwwckmvsbrzw") (y #t)))

