(define-module (crates-io cr mf crmf) #:use-module (crates-io))

(define-public crate-crmf-0.0.0 (c (n "crmf") (v "0.0.0") (h "18rjgsp7l1ly9vv2j0ymddpg2r275cmridbv9zx9ddbd0bxsp54p")))

(define-public crate-crmf-0.2.0-pre.0 (c (n "crmf") (v "0.2.0-pre.0") (d (list (d (n "cms") (r "=0.2.0-pre") (d #t) (k 0)) (d (n "const-oid") (r "^0.9") (d #t) (k 2)) (d (n "der") (r "^0.7") (f (quote ("alloc" "derive"))) (d #t) (k 0)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "=0.2.0-pre.0") (k 0)))) (h "05k19x5wzbkwgv1bq5rxcxh33ddh88sh85irdsb501jl44ccdgk0") (f (quote (("std" "der/std" "spki/std") ("pem" "alloc" "der/pem") ("alloc" "der/alloc")))) (r "1.65")))

(define-public crate-crmf-0.2.0 (c (n "crmf") (v "0.2.0") (d (list (d (n "cms") (r "^0.2") (d #t) (k 0)) (d (n "const-oid") (r "^0.9") (d #t) (k 2)) (d (n "der") (r "^0.7") (f (quote ("alloc" "derive"))) (d #t) (k 0)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "^0.2") (k 0)))) (h "14pdc9czxcjzwy7p5rkwh6n100f087n04wjv9gggb1svdnwj3zin") (f (quote (("std" "der/std" "spki/std") ("pem" "alloc" "der/pem") ("alloc" "der/alloc")))) (r "1.65")))

