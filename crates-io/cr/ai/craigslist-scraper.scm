(define-module (crates-io cr ai craigslist-scraper) #:use-module (crates-io))

(define-public crate-craigslist-scraper-0.1.0 (c (n "craigslist-scraper") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "html5ever") (r "^0.5.3") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "slack") (r "^0.15.0") (d #t) (k 0)) (d (n "tendril") (r "^0.2.2") (d #t) (k 0)))) (h "0y61hjsf24fgvv0z8ag8mdsx9cf2bywm6l1vk7zqq5cll3vhgp5j")))

