(define-module (crates-io cr dt crdt_rs) #:use-module (crates-io))

(define-public crate-crdt_rs-0.1.0 (c (n "crdt_rs") (v "0.1.0") (h "1hai771dq17qsvwgxwgn22f1b1x07s9993513ibwpncmd7gq5crh")))

(define-public crate-crdt_rs-0.1.1 (c (n "crdt_rs") (v "0.1.1") (h "1lmiwcs0w6gq12yf28fkibjk2c8l1r6rkvyxvaa8dafky0qji48r")))

(define-public crate-crdt_rs-0.1.2 (c (n "crdt_rs") (v "0.1.2") (h "051wrqlbswfmdw5gv5wzaa5q2zzc0q93v2r7b40mrlw6ja48vrlf")))

