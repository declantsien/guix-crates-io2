(define-module (crates-io cr dt crdt_tree) #:use-module (crates-io))

(define-public crate-crdt_tree-0.0.1 (c (n "crdt_tree") (v "0.0.1") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "187qzjdpflvh6g7n456wsdbfhhi5rbm1mxiykb2qcgp2zdl8jrhh")))

(define-public crate-crdt_tree-0.0.2 (c (n "crdt_tree") (v "0.0.2") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "0k3f7kyf29swvcdrmmnqk55vkwzhi9km6ijspifs213j01s7xqqd")))

(define-public crate-crdt_tree-0.0.3 (c (n "crdt_tree") (v "0.0.3") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "1fcxj78y33wnawm51fjb6pwxp30snbdj5xvm0fvx1522x52q1vyl")))

(define-public crate-crdt_tree-0.0.4 (c (n "crdt_tree") (v "0.0.4") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "0vgwd2vmw0dqi3ljz74lsqn6sj0yk06c3701gf75hvfdr0x20vai")))

(define-public crate-crdt_tree-0.0.5 (c (n "crdt_tree") (v "0.0.5") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "104gchdzkx6sj757js6s2093pa20y8mqz5s65b5xnz3ddh2xlgd8")))

(define-public crate-crdt_tree-0.0.6 (c (n "crdt_tree") (v "0.0.6") (d (list (d (n "crdts") (r ">=4.2.0, <5.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (k 0)) (d (n "serde") (r ">=1.0.113, <2.0.0") (f (quote ("derive"))) (k 0)))) (h "0md54nsbmm9skikwhgxwqqbyz2pwg34bhxd07rb501r38scf7ys4")))

(define-public crate-crdt_tree-0.0.7 (c (n "crdt_tree") (v "0.0.7") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "03b8jx9b0na5chjhkxd1yl25mfcdyccl7vw4q1yj74y7f0784jkl")))

(define-public crate-crdt_tree-0.0.8 (c (n "crdt_tree") (v "0.0.8") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "018pdvl2xxpnqmx0s20crdkknwnxsiikh2wvs7cn867c53j5s842")))

(define-public crate-crdt_tree-0.0.9 (c (n "crdt_tree") (v "0.0.9") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "1zm81inniqpdzbip0kx2m9rg2c5bygcdyrdhccg3jrngl868dvf3")))

(define-public crate-crdt_tree-0.0.10 (c (n "crdt_tree") (v "0.0.10") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "10571vgqli9lzkd3m9k0hzf9cqmpv8dsfk7r90am4cq4aqhvqlfi")))

(define-public crate-crdt_tree-0.0.11 (c (n "crdt_tree") (v "0.0.11") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "01v5yjn4brwaidm1yzsx1g4gfa2l8hvkdylj9sx0yxshh5znqz85")))

(define-public crate-crdt_tree-0.0.12 (c (n "crdt_tree") (v "0.0.12") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "1y6bjnm3004n408dwx6dj6z3vqyqzwdpai8bhwz8sn769dhchqyq")))

(define-public crate-crdt_tree-0.0.13 (c (n "crdt_tree") (v "0.0.13") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "1hzv7vjjp2j0rhlr4s2j8q09mv0zkq368wnnasc72mlal8gs9hrj")))

(define-public crate-crdt_tree-0.0.14 (c (n "crdt_tree") (v "0.0.14") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "0mc3r5p9kwprf8swsxkkdvfaf8z2a35082mgprq0i8576apjl9sh")))

(define-public crate-crdt_tree-0.0.15 (c (n "crdt_tree") (v "0.0.15") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "13qwas8b9371prk395304v1j2n9ggrz4lxwiyf0ghq0658zzckad")))

(define-public crate-crdt_tree-0.0.16 (c (n "crdt_tree") (v "0.0.16") (d (list (d (n "crdts") (r "^4.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (k 0)))) (h "1jnyvaszcpk8yn7mscdacnl04vjj1i4xh2026crzqi8gjzrwgrm7")))

