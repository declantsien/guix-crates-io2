(define-module (crates-io cr dt crdt_fileset) #:use-module (crates-io))

(define-public crate-crdt_fileset-0.1.0 (c (n "crdt_fileset") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "07lijl3gqrvf9q6l1dyq465j779i34bap222jx39lqxl18h5p04n")))

(define-public crate-crdt_fileset-0.1.1 (c (n "crdt_fileset") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1cj1qip89nak2bcmi7261ybir1qf8njdmhz5rkkyp032dnnwddmr")))

