(define-module (crates-io cr dt crdt-woot) #:use-module (crates-io))

(define-public crate-crdt-woot-0.1.0 (c (n "crdt-woot") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "arref") (r "^0.1.0") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.5.1") (d #t) (k 2)) (d (n "ctor") (r "^0.1.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v0sdfjcqyi6h2wd033lmpzgcy5hizfsysl8fq73qrhl8yzyz0p7") (f (quote (("arbitrary"))))))

