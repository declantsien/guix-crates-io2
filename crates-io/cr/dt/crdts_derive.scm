(define-module (crates-io cr dt crdts_derive) #:use-module (crates-io))

(define-public crate-crdts_derive-7.3.0 (c (n "crdts_derive") (v "7.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ydy4d1hr58a121dn27qpyflyvx5n13z5y8nisbmb5cx1w8ycv4y")))

(define-public crate-crdts_derive-7.3.1 (c (n "crdts_derive") (v "7.3.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mkz4gp6bf469p9d0w56cd274sl4wwkxlb6yqssdvr6ajb0h2qnb")))

(define-public crate-crdts_derive-7.3.2 (c (n "crdts_derive") (v "7.3.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1m2sbv0qnjj3cs9x513vcqwym65bdaywwzg0hx8rnbif6kfvrkaf")))

(define-public crate-crdts_derive-7.3.3 (c (n "crdts_derive") (v "7.3.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xvfbs9kvfiln02y425s03f9g8hb4yf5rqlyzq58js02fz3wl3rw")))

(define-public crate-crdts_derive-7.3.4 (c (n "crdts_derive") (v "7.3.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "037c7q1k0j2x9j4as1znvygxpl1q5ak65qdjmj74hggd7cdvk5dg")))

(define-public crate-crdts_derive-7.3.5 (c (n "crdts_derive") (v "7.3.5") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1v54a092h045hhyzphvhjwvwgjxkm7l6nlaaajk74x3dcdnm7y8c")))

