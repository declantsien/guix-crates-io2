(define-module (crates-io cr dt crdts_macro_derive) #:use-module (crates-io))

(define-public crate-crdts_macro_derive-7.3.0 (c (n "crdts_macro_derive") (v "7.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bfip1msz5lk3cvp09k4hnpar3ynvv1xqs478mpkslpyiz4y94w4")))

