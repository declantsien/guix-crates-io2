(define-module (crates-io cr dt crdt) #:use-module (crates-io))

(define-public crate-crdt-0.2.0 (c (n "crdt") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (d #t) (k 0)))) (h "09fb11y3c031ha866j5m8jxqdgpk0iws08rmmphccmcpicinxjd0")))

(define-public crate-crdt-0.3.0 (c (n "crdt") (v "0.3.0") (d (list (d (n "capnpc") (r "*") (d #t) (k 1)) (d (n "quickcheck") (r "*") (d #t) (k 0)) (d (n "quickcheck_macros") (r "*") (d #t) (k 0)))) (h "0gcivficc3gq83k852wj20mhvrhccyxhnwhzgmkg58gqm5hw51zf")))

(define-public crate-crdt-0.4.0 (c (n "crdt") (v "0.4.0") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "1ri83hi0pqpf6l5lri3cdn00fsy241ap9y2ll3rsxyqnhvg0pc12") (f (quote (("quickcheck_generators" "quickcheck") ("default"))))))

(define-public crate-crdt-0.4.1 (c (n "crdt") (v "0.4.1") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "0qpz1wjv0nhnrpn0f25raf9lnj80bsrxdx9b56nr08d2yh52w76v") (f (quote (("quickcheck_generators" "quickcheck") ("default"))))))

(define-public crate-crdt-0.5.0 (c (n "crdt") (v "0.5.0") (d (list (d (n "quickcheck") (r "*") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "*") (d #t) (k 2)))) (h "099yq7ci6bysp146ywgxjzxxkn92dmv4nanlqaifpim90i82h9c5") (f (quote (("quickcheck_generators" "quickcheck") ("default"))))))

