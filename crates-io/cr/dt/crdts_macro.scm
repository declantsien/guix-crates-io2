(define-module (crates-io cr dt crdts_macro) #:use-module (crates-io))

(define-public crate-crdts_macro-7.3.0 (c (n "crdts_macro") (v "7.3.0") (d (list (d (n "crdts_macro_derive") (r "^7.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lw64mhysmclng7p2p94ji3s7af3y632hf7r8614y5pk8kkzyi7n")))

