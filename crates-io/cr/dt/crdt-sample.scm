(define-module (crates-io cr dt crdt-sample) #:use-module (crates-io))

(define-public crate-crdt-sample-0.1.0 (c (n "crdt-sample") (v "0.1.0") (h "1ic7zsb89aavsxswrnkhxqyl8257y2srnc3pg3nilkm7lwb7mj8n")))

(define-public crate-crdt-sample-0.2.0 (c (n "crdt-sample") (v "0.2.0") (h "0sbqrnx50xrqjs2njl1c11j94wqgi2ppwvnxg3y8wr9l7x7qm70x")))

