(define-module (crates-io cr sp crsp) #:use-module (crates-io))

(define-public crate-crsp-0.1.0 (c (n "crsp") (v "0.1.0") (d (list (d (n "crsp-base") (r "^0.1.0") (d #t) (k 0)))) (h "1kcasrkdahm0z5j0pvc8af2spn52gxqnq1g315krkjr5i39wpk2d")))

(define-public crate-crsp-0.2.0 (c (n "crsp") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "crsp-base") (r "^0.2.0") (d #t) (k 0)) (d (n "pixels") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "1rcx95p5w6a1izis6pd3dws3i53rcf9r2wr63bylryidfyk520j4")))

