(define-module (crates-io cr sp crsp-base) #:use-module (crates-io))

(define-public crate-crsp-base-0.1.0 (c (n "crsp-base") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0ddsh34jz5y00v993p0616w39qn0gvp4wwa9a7j897aa5shc4f1p")))

(define-public crate-crsp-base-0.2.0 (c (n "crsp-base") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.14") (d #t) (k 0)) (d (n "flume") (r "^0.10.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0i0kby6yjamgbjam6997hnxqvg2wpa61qbh31wkxx26ppjq935r9")))

