(define-module (crates-io cr cd crcdir) #:use-module (crates-io))

(define-public crate-crcdir-0.1.0 (c (n "crcdir") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "00k4p7j1m98h3giyazxydbxq0isyllyq1gd2mil9vcr9wnz1wf0y")))

(define-public crate-crcdir-0.1.1 (c (n "crcdir") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "0fkmyn3bw6pjv3z7q4d31xr5mddqvrhqhi0akqsss2790pzgsdkk")))

(define-public crate-crcdir-0.2.0 (c (n "crcdir") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "015z1fw06ypbpi0gy1iv1c7mvqqbbbzgkk57l81fmp7rvxkdfmzy")))

(define-public crate-crcdir-0.2.1 (c (n "crcdir") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1p911qi7nwfmxbnvg27p9gmxh19srx36zwajlmzm573jbr0zbhwa")))

(define-public crate-crcdir-0.2.2 (c (n "crcdir") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1sgnvwljkbbr88bf0xii6fqvsbggyjdmd4zqs56qbcppa6ig6r8c")))

(define-public crate-crcdir-0.3.0 (c (n "crcdir") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "filebuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "0n85g9ybmghww3kxbj7apkgl7xln5dbrxxanm7bimjnkjqnlsm9f") (f (quote (("progress" "indicatif") ("default" "progress"))))))

