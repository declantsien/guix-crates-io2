(define-module (crates-io cr fs crfsuite-sys) #:use-module (crates-io))

(define-public crate-crfsuite-sys-0.1.0 (c (n "crfsuite-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)))) (h "0h9g9w9nidln259r415iqc110zbs9q9lzni2b6hjqksjy3cr2ljr")))

(define-public crate-crfsuite-sys-0.1.1 (c (n "crfsuite-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)))) (h "1wl7z1gl8w3kra20bfa689q7fjqdkzp4iiyf9qgn5p4f7vgpiwyq")))

(define-public crate-crfsuite-sys-0.1.2 (c (n "crfsuite-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)))) (h "0i866l89iml73w54wv5a8dg9qfky78ci4l1gmr1ygxzqq8w5990f")))

(define-public crate-crfsuite-sys-0.1.3 (c (n "crfsuite-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)))) (h "054639343x90ani1cpzydizpkzajgi110liaixkzggizms8hc9jn")))

(define-public crate-crfsuite-sys-0.1.4 (c (n "crfsuite-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0raws1bdkq6zq48q9hc2yh1rwa88rkgh0vbfln72y3zkjv6qnjki")))

(define-public crate-crfsuite-sys-0.1.5 (c (n "crfsuite-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03gxxgar9b2hsxss11wk4qk2jixhsavc3z5vmwzira38f3fwjpag") (l "crfsuite")))

(define-public crate-crfsuite-sys-0.2.0 (c (n "crfsuite-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a3zb7qmqzrmjh658kmzckbc2w1fpc05mw19wk86j5w4jsdhz4ic") (l "crfsuite")))

(define-public crate-crfsuite-sys-0.3.0 (c (n "crfsuite-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblbfgs-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1bn55aq8ll5h0qdbfnr3q42730yzwl403bjv47590pc9lwili3wd") (l "crfsuite")))

(define-public crate-crfsuite-sys-0.3.1 (c (n "crfsuite-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "liblbfgs-sys") (r "^0.1.0") (d #t) (k 0)))) (h "15h9kihv08pcs5d0490xv01hwbihnyaswy6fh2606mcyc81sk9fy") (l "crfsuite")))

