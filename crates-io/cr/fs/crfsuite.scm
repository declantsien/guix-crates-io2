(define-module (crates-io cr fs crfsuite) #:use-module (crates-io))

(define-public crate-crfsuite-0.1.0 (c (n "crfsuite") (v "0.1.0") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)))) (h "03kcqlz3x4lcs3y5ljv41zy75xxifqq9q171asbx9az4hg9in5cj")))

(define-public crate-crfsuite-0.1.1 (c (n "crfsuite") (v "0.1.1") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p5nr0mxdnnq9xx4p9wky90vwff8s57h2dvgd3vjppvgsfrq1r9q")))

(define-public crate-crfsuite-0.2.0 (c (n "crfsuite") (v "0.2.0") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sah136w3zh3ir9l2n093mfw5jnr6wll2g1g1a656sy0w0ccvx45")))

(define-public crate-crfsuite-0.2.1 (c (n "crfsuite") (v "0.2.1") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c1awn2mgag95qimybwk438f5fwbkl6bw2mv12ky8izbxwi4ifag")))

(define-public crate-crfsuite-0.2.2 (c (n "crfsuite") (v "0.2.2") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11ihd9g1pcx1kycbqdmqxcccghxb1zf4srszq9qp8hyb6yx14y05")))

(define-public crate-crfsuite-0.2.3 (c (n "crfsuite") (v "0.2.3") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wawv2y07a42mk37mvpy43q698l8s32g4v5fkkgzpqjqvby10cca")))

(define-public crate-crfsuite-0.2.4 (c (n "crfsuite") (v "0.2.4") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rcqvr4c6w4sp22vyxi40s57svp3f1ggz3qv059krbc5zfw1r6cl")))

(define-public crate-crfsuite-0.2.6 (c (n "crfsuite") (v "0.2.6") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q38z2rn49y1gvvs8r8m5nvmhig1flbn4hnk4h08drb8sdfv5h6l")))

(define-public crate-crfsuite-0.2.7 (c (n "crfsuite") (v "0.2.7") (d (list (d (n "crfsuite-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "123j0pvii1yahx642km2byan5s7ng4p89ikpj6dli0piglslzq7q")))

(define-public crate-crfsuite-0.2.8 (c (n "crfsuite") (v "0.2.8") (d (list (d (n "crfsuite-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07iadmk2d7lsgdj5lrhz62hzfy6sfmp3n9wl7hl6nwms6cr7jdgf")))

(define-public crate-crfsuite-0.3.0 (c (n "crfsuite") (v "0.3.0") (d (list (d (n "crfsuite-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00i5kwcc3kfcsawzpbw1ybv1yyk7dakbb89gszrl5ldzrghis5ch")))

(define-public crate-crfsuite-0.3.1 (c (n "crfsuite") (v "0.3.1") (d (list (d (n "crfsuite-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qhj9h9yfmm8g9c5hsknas7z9h456m86jqpkc7l05k2izcrgv9gi")))

