(define-module (crates-io cr fs crfs) #:use-module (crates-io))

(define-public crate-crfs-0.1.0 (c (n "crfs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cqdb") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)))) (h "1gnvg3gfaai6kmlcm0ia8iqw6v76n81fa6fqdhdisr7jjkwf8sg0")))

(define-public crate-crfs-0.1.1 (c (n "crfs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cqdb") (r "^0.4") (d #t) (k 0)) (d (n "crfsuite") (r "^0.3.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)))) (h "178vyqqzqsvff09n82l71hy70k4gmaccfzawlk81zj0csszsjzz8")))

(define-public crate-crfs-0.1.2 (c (n "crfs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cqdb") (r "^0.4") (d #t) (k 0)) (d (n "crfsuite") (r "^0.3.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "0hqj8gjlg48374vbjfpfic12f5vvgf4kd4apn41vsc9adc5x76p7")))

(define-public crate-crfs-0.1.3 (c (n "crfs") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb") (r "^0.5") (d #t) (k 0)) (d (n "crfsuite") (r "^0.3.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "0qr1pbpw6c6pkpv68pxq8n5vssjq3n2s6wdw7sf063mqxp2n183i")))

(define-public crate-crfs-0.2.0 (c (n "crfs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb") (r "^0.5") (d #t) (k 0)) (d (n "crfsuite") (r "^0.3.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "199z1k3k8br9c5axnzw6bpxz1j71njn3j509vhgkqrc3jbanq8w3")))

