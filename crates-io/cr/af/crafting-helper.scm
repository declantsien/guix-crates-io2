(define-module (crates-io cr af crafting-helper) #:use-module (crates-io))

(define-public crate-crafting-helper-1.0.0 (c (n "crafting-helper") (v "1.0.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "0m8x6f089zryl1lgckgqhm69j8whcp22gcf6cmkwcpvlbzc628l6")))

