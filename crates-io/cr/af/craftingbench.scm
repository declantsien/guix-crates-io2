(define-module (crates-io cr af craftingbench) #:use-module (crates-io))

(define-public crate-craftingbench-0.1.0 (c (n "craftingbench") (v "0.1.0") (h "0whnw4c91289cahv8wlzf7qm0rqa1q2g0p3p91ssd0cm3vgkyc12")))

(define-public crate-craftingbench-0.1.1 (c (n "craftingbench") (v "0.1.1") (h "0703n57f7ry0652cwvr17yrs2hg53h15575sr0hsz5ynms140j3i")))

(define-public crate-craftingbench-0.1.2 (c (n "craftingbench") (v "0.1.2") (h "0pycb8qdkj95sf86kiq2m0ry41km8sns40k32z4wcfhvqx1plawg")))

(define-public crate-craftingbench-0.1.3 (c (n "craftingbench") (v "0.1.3") (h "0wsv8kyix6960dx79ilz6142amnssq04606n04krn277xy96vssf")))

(define-public crate-craftingbench-0.1.4 (c (n "craftingbench") (v "0.1.4") (h "0c4pv47h0538d7k3ci9hjh0qkl18saax8v8mykf0bvrp2k0kz16h")))

