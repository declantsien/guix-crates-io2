(define-module (crates-io cr af craft) #:use-module (crates-io))

(define-public crate-craft-0.1.0 (c (n "craft") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (f (quote ("io-compat"))) (d #t) (k 0)) (d (n "futures01") (r "^0.1.28") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "patricia_router") (r "^0.1.0") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.6") (d #t) (k 0)))) (h "137l8k6vvgqi5zzj3lk6cc02fq387x9zy94a2lrhhlxjsq16qsx0")))

