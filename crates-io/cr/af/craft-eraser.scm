(define-module (crates-io cr af craft-eraser) #:use-module (crates-io))

(define-public crate-craft-eraser-0.1.0 (c (n "craft-eraser") (v "0.1.0") (h "0acr7l8i6n821aqlva9589810bbgy8i5519k7qb6y6jmgqq508gj")))

(define-public crate-craft-eraser-0.2.0 (c (n "craft-eraser") (v "0.2.0") (h "0r7djqgxb0jjrqym5m6y43iyy23zlgwfy4lkdi7hzpnclkp1p2zf")))

(define-public crate-craft-eraser-0.2.1 (c (n "craft-eraser") (v "0.2.1") (h "0qhqym4jm4xkjcyc0ljiqmz2432i061c5hxfsb0k22amywydcvcs")))

(define-public crate-craft-eraser-0.2.2 (c (n "craft-eraser") (v "0.2.2") (h "072fdyb6gr0j6z49ismi0zdgkdwwvkqyscs76j7qnvk82kdky4aa")))

(define-public crate-craft-eraser-0.3.0 (c (n "craft-eraser") (v "0.3.0") (h "0w1ba95fb54xxy07wfwhmci6118ncrm4v2fjgvxmng815anfx389")))

(define-public crate-craft-eraser-0.3.1 (c (n "craft-eraser") (v "0.3.1") (h "04jl5sdv1rvvi0jrchkq4rfp1dk5gicl45gm95q7kqxh92291913")))

(define-public crate-craft-eraser-0.3.2 (c (n "craft-eraser") (v "0.3.2") (h "1vy9c2yxibj37aqcnbr9n9cqc65xpazvx2ai7k429dj6kjig7qal")))

