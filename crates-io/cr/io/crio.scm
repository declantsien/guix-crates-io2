(define-module (crates-io cr io crio) #:use-module (crates-io))

(define-public crate-crio-0.1.0 (c (n "crio") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vkn03nq8fcdn9v91a9gbvx3jgyyz2ai58djsxlzz44fzpbmm363") (y #t)))

(define-public crate-crio-0.1.1 (c (n "crio") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ksvxyb91mxq6s7ln1m8r3nl55g9p0rdsipw4fy68hz341riiny7") (y #t)))

(define-public crate-crio-0.1.2 (c (n "crio") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1mgym0mcrzjxy3jlkklcr7c6iac6i7k7p6xhf9rqb2jzi29izhs5") (y #t)))

(define-public crate-crio-0.1.3 (c (n "crio") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1npmpggwwpaxxjj1fyhxd4q2mj5r47r5n85vx6jncv1kfz26h51s") (y #t)))

(define-public crate-crio-0.1.4 (c (n "crio") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0di7raj89lg0m6iq1c1xqprip0k26cj100p3jjfqpjmkpagjql4v") (y #t)))

(define-public crate-crio-0.1.5 (c (n "crio") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1wkylcf9fjbn7wahqyn9g1y2mccfllgrn1gg41xmlrpw0wgl7ijf") (y #t)))

(define-public crate-crio-0.1.6 (c (n "crio") (v "0.1.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "182razmwq6rk4bfm7lrqkiib4qrkvz293y95b412zxihmh11cs75") (y #t)))

(define-public crate-crio-0.2.0 (c (n "crio") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ch1gbsc8slina2cfwg8z45fmav390iqpwnq13s2z25b863mr04f")))

(define-public crate-crio-0.2.1 (c (n "crio") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1y730xv0xx0d20cfg34aaj1vig5i0j2djnpp64gbf2i9wnxp8rdl")))

(define-public crate-crio-0.2.2 (c (n "crio") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06crsa3xzjfpvxj5sqlhrvma2j4890dn7zm5v0xggrinmrs7jahm")))

(define-public crate-crio-0.2.3 (c (n "crio") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "19m73g30nksq7ivca30zdrika1in7gqx9n7k584ss609wckhz2h8")))

(define-public crate-crio-0.2.4 (c (n "crio") (v "0.2.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "17mvsqn186770nwq13rmmi47xx6zgc154ks5awgplwxm6j65pm0k")))

(define-public crate-crio-0.2.5 (c (n "crio") (v "0.2.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0998nbz671fk90j91xr7ha3mrizazdixlfpjdskiys057ldhig96")))

(define-public crate-crio-0.2.6 (c (n "crio") (v "0.2.6") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05mhq8v56n2nx13da2h16j5gb8mqks503bj5mw2cnd8cy04wkfhd")))

(define-public crate-crio-0.2.7 (c (n "crio") (v "0.2.7") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09kq4022y8ix13ig6lflkyhzwcxyrxlicniaybaldwb73w5y1nl0")))

(define-public crate-crio-0.2.8 (c (n "crio") (v "0.2.8") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gp1mzgz9b3n8x0v8h1k9ajd9d4pplxvv0yzpfqkl06r7nznpz8w")))

(define-public crate-crio-0.2.9 (c (n "crio") (v "0.2.9") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09iha68vvq3x7yn04dqdvgzg78wjkhxbwdkq42gv9d2wpqcrm48m")))

(define-public crate-crio-0.2.10 (c (n "crio") (v "0.2.10") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gsx37rvkk34rd061vq2mb99qzaqxyg4rwzia31r942abdzfkfxm")))

(define-public crate-crio-0.3.0 (c (n "crio") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04fdchhxn4a9sjwi09dix2vj6r7pf8z6zg5gjxrgwd1dm6xjb667")))

(define-public crate-crio-0.3.1 (c (n "crio") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rh5vv8ymplcxf2y9mvady9f2xrkw0rhmkpz127rczllfaj1w390")))

(define-public crate-crio-0.4.0 (c (n "crio") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "154dfbvw07a60ikpiyc3h1jxd7mgr9ba6z18sc26ar05p66j0czx")))

(define-public crate-crio-0.4.1 (c (n "crio") (v "0.4.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kcy1rgmq0x9wzxmnxwnm02pavrpalf381hcblhq0qd2hyp21qc7")))

(define-public crate-crio-0.4.2 (c (n "crio") (v "0.4.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08701s93y4bqkgjqsjm40x2l0qxs776mjrkzcq7vyxqcbll88gka")))

(define-public crate-crio-0.4.3 (c (n "crio") (v "0.4.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07a740q92p66fkxgicdbhc3m10gyrri2wwx2vg3jfgnf7q2azbwp")))

(define-public crate-crio-0.4.4 (c (n "crio") (v "0.4.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fc18bqnqrnpmngcmw8qvqwi9n34ml7m0frd47wbf6bs05pzfiqf")))

(define-public crate-crio-0.4.5 (c (n "crio") (v "0.4.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kimhx914rrnn9kk78vzf1vn55dd6r2q98b1cw9bbd5akk8xcmgf")))

(define-public crate-crio-1.0.0 (c (n "crio") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14ylysx3nbqxs7hg4y3ph8xx10sm52cg9y7cc6d2dn02j4wqis8l")))

(define-public crate-crio-1.0.1 (c (n "crio") (v "1.0.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z65cdhnappj0j9gb8z3nsjg4zafiphx6b1k91jzz5kaii6kgljb")))

(define-public crate-crio-2.0.0 (c (n "crio") (v "2.0.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wanpl7xiwdg3sdi78cyadggijavnm911a2mrpz5mji77lw590bg") (y #t)))

(define-public crate-crio-2.0.1 (c (n "crio") (v "2.0.1") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00963bh3ic94cjwi724hyakzglmsnbq95b0pj7wj1bcas3chcvr4") (y #t)))

(define-public crate-crio-2.0.2 (c (n "crio") (v "2.0.2") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "198q1hb353zp6h5lpfwzl524vcxlcig8k3s4064awmxszqq21j9i") (y #t)))

(define-public crate-crio-2.0.3 (c (n "crio") (v "2.0.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18wgq1a24dsci1rfs4ikq32qi5r7zgmz1gz8c35bg678n088cvsk")))

(define-public crate-crio-2.0.4 (c (n "crio") (v "2.0.4") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02z8rwvfcn433dbh064dl160y7zwf8c062f55v82n3svhcgfx9kw")))

(define-public crate-crio-2.0.5 (c (n "crio") (v "2.0.5") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vji6qnn6psl2vv9p9a86yv8mbka0qryx31q267g84674k8svkk0")))

