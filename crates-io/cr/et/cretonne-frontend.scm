(define-module (crates-io cr et cretonne-frontend) #:use-module (crates-io))

(define-public crate-cretonne-frontend-0.1.0 (c (n "cretonne-frontend") (v "0.1.0") (d (list (d (n "cretonne") (r "^0.1.0") (d #t) (k 0)))) (h "0v3rillvj5hf0wixqzaxjf32zbrj4vm9qjdpvkfmmhydfxg59aly") (y #t)))

(define-public crate-cretonne-frontend-0.2.0 (c (n "cretonne-frontend") (v "0.2.0") (d (list (d (n "cretonne") (r "^0.2.0") (d #t) (k 0)))) (h "0389x5jbzkgi3q722q90lmnrlb5l5dqy670lwvwx0z0x3v7hgqpq") (y #t)))

(define-public crate-cretonne-frontend-0.3.0 (c (n "cretonne-frontend") (v "0.3.0") (d (list (d (n "cretonne") (r "^0.3.0") (d #t) (k 0)))) (h "1kmg385zip0r3m5hwiiaxjyrjfjm9n7xl7xm8m9kzzq6grsab0c0") (y #t)))

(define-public crate-cretonne-frontend-0.3.1 (c (n "cretonne-frontend") (v "0.3.1") (d (list (d (n "cretonne") (r "^0.3.1") (d #t) (k 0)))) (h "13cbivdys86xziaqxh9k0mxds0spyy60vavrvrkxhqklhhwz9yj4") (y #t)))

(define-public crate-cretonne-frontend-0.3.2 (c (n "cretonne-frontend") (v "0.3.2") (d (list (d (n "cretonne") (r "^0.3.2") (d #t) (k 0)))) (h "1ivm93d5pkj16wq6sdxg6ac4d82idd82gkycphmyr1iz3cyz2nqs") (y #t)))

(define-public crate-cretonne-frontend-0.3.3 (c (n "cretonne-frontend") (v "0.3.3") (d (list (d (n "cretonne") (r "^0.3.3") (d #t) (k 0)))) (h "1k9p8j0w5f8pa2k8k1mw2157c4q4ijq0kbqjw3j2zh08iibvg61q") (y #t)))

(define-public crate-cretonne-frontend-0.3.4 (c (n "cretonne-frontend") (v "0.3.4") (d (list (d (n "cretonne") (r "^0.3.4") (d #t) (k 0)))) (h "00ysznz3a0lbl1imby6vs1833jcgxd5chaws9j3iyp1py9hrp1yr") (y #t)))

(define-public crate-cretonne-frontend-0.4.0 (c (n "cretonne-frontend") (v "0.4.0") (d (list (d (n "cretonne") (r "^0.4.0") (d #t) (k 0)))) (h "1p2c1qp9c35v9nnr4dfzl2lpa87wm0r1h1mwdk4m2rg2b51jpawy") (y #t)))

(define-public crate-cretonne-frontend-0.4.1 (c (n "cretonne-frontend") (v "0.4.1") (d (list (d (n "cretonne") (r "^0.4.1") (d #t) (k 0)))) (h "0g3iw4z6nx7004bfkbv9akda5xnf8i13rsrff4xcsbwdb1kplkzv") (y #t)))

(define-public crate-cretonne-frontend-0.4.2 (c (n "cretonne-frontend") (v "0.4.2") (d (list (d (n "cretonne") (r "^0.4.2") (d #t) (k 0)))) (h "1kf3iv9h59ag3ii1rwc0dnjf9mizbzfghpngn2r8570pvj5m1s17") (y #t)))

(define-public crate-cretonne-frontend-0.4.3 (c (n "cretonne-frontend") (v "0.4.3") (d (list (d (n "cretonne") (r "^0.4.3") (d #t) (k 0)))) (h "147ar2w4k2aa3avm3xqlm008dmpyykb54yp123pwypnjrk5v4is5") (y #t)))

(define-public crate-cretonne-frontend-0.4.4 (c (n "cretonne-frontend") (v "0.4.4") (d (list (d (n "cretonne") (r "^0.4.4") (d #t) (k 0)))) (h "177g84liv1y2v6hvm4y44babkbadhwbcffbxwqwvda927y9lavyf") (y #t)))

(define-public crate-cretonne-frontend-0.5.0 (c (n "cretonne-frontend") (v "0.5.0") (d (list (d (n "cretonne-codegen") (r "^0.5.0") (d #t) (k 0)))) (h "1d4bygn36lgz3zz9v64qgy18lyrh1cj46xqh80cc34h372q9xgxs") (y #t)))

(define-public crate-cretonne-frontend-0.5.1 (c (n "cretonne-frontend") (v "0.5.1") (d (list (d (n "cretonne-codegen") (r "^0.5.1") (d #t) (k 0)))) (h "1b5v59m80iw8nmir13zhl3nr5pwyz4fyhfx7vcpfvp3w4z10jmcd") (y #t)))

(define-public crate-cretonne-frontend-0.6.0 (c (n "cretonne-frontend") (v "0.6.0") (d (list (d (n "cretonne-codegen") (r "^0.6.0") (k 0)))) (h "0hs6yjp03r5fn67b4v025mbxs1719ky54k9yry17l2arilm61rbs") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.7.0 (c (n "cretonne-frontend") (v "0.7.0") (d (list (d (n "cretonne-codegen") (r "^0.7.0") (k 0)))) (h "1d6dvxb9kfd0rqghmigix8gfgnssxdymxqvp4ccbl1zb6fw0gw7i") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.8.0 (c (n "cretonne-frontend") (v "0.8.0") (d (list (d (n "cretonne-codegen") (r "^0.8.0") (k 0)))) (h "0vk4497sl5rg4nvy69y2w0xj3my7jaafaacrgiqzbxvmw2pcvz4b") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.9.0 (c (n "cretonne-frontend") (v "0.9.0") (d (list (d (n "cretonne-codegen") (r "^0.9.0") (k 0)))) (h "1ybyjm3fg7h8kgvxivaxxipy5327hlanx1nynpssm6vxv4xfwngg") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.10.0 (c (n "cretonne-frontend") (v "0.10.0") (d (list (d (n "cretonne-codegen") (r "^0.10.0") (k 0)))) (h "172zmyzxv8c9bsjvsbx0wzx08z8bkachabfzkmcifv5h3c78rkvz") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.11.0 (c (n "cretonne-frontend") (v "0.11.0") (d (list (d (n "cretonne-codegen") (r "^0.11.0") (k 0)))) (h "0x88ckk7v4wb9p459dg64qcp68fscc87cq745kgp2nxwna4gxc0l") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.12.0 (c (n "cretonne-frontend") (v "0.12.0") (d (list (d (n "cretonne-codegen") (r "^0.12.0") (k 0)))) (h "0mbl346igkifi3gkqa8c0z7mzs6zv83ajhl5b5rkyf9psmjqh8z0") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.13.0 (c (n "cretonne-frontend") (v "0.13.0") (d (list (d (n "cretonne-codegen") (r "^0.13.0") (k 0)))) (h "1kh70cr619vxa57qbg92wlgxng97dc541f2iqr2lpywnnz9c1lmv") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.13.1 (c (n "cretonne-frontend") (v "0.13.1") (d (list (d (n "cretonne-codegen") (r "^0.13.1") (k 0)))) (h "16gx3xjjdy48r773bx84g64mmwrf3b318va2l6w89nnpkmxr38jj") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-frontend-0.13.2 (c (n "cretonne-frontend") (v "0.13.2") (d (list (d (n "cretonne-codegen") (r "^0.13.2") (k 0)))) (h "03qbbfzsqm3ynmqci1gpbjawn8f8j9w58jqa0xxn0hcfnag7357z") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

