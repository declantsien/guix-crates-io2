(define-module (crates-io cr et cretonne-faerie) #:use-module (crates-io))

(define-public crate-cretonne-faerie-0.5.0 (c (n "cretonne-faerie") (v "0.5.0") (d (list (d (n "cretonne-codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.5.0") (d #t) (k 0)) (d (n "faerie") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)))) (h "0xi9gd5idnrxng6blbqhlj2gcyz50y114j2pyj675cwna8d4w5id") (y #t)))

(define-public crate-cretonne-faerie-0.5.1 (c (n "cretonne-faerie") (v "0.5.1") (d (list (d (n "cretonne-codegen") (r "^0.5.1") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.5.1") (d #t) (k 0)) (d (n "faerie") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)))) (h "01yx93x9x8mq540asp3kh5q30kl8axs7pmcs3j5s11pvrxw9zh8h") (y #t)))

(define-public crate-cretonne-faerie-0.6.0 (c (n "cretonne-faerie") (v "0.6.0") (d (list (d (n "cretonne-codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.6.0") (d #t) (k 0)) (d (n "faerie") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)))) (h "1bp696hqhfkjwaz86h7icmjkjlsir1aayx67bscx8gfp0nyc6jgy") (y #t)))

(define-public crate-cretonne-faerie-0.7.0 (c (n "cretonne-faerie") (v "0.7.0") (d (list (d (n "cretonne-codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.7.0") (d #t) (k 0)) (d (n "faerie") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)))) (h "0l13266xivp89m68l4dvh0v9dkzamwrzzh5hxg107s1ncylij1dm") (y #t)))

(define-public crate-cretonne-faerie-0.8.0 (c (n "cretonne-faerie") (v "0.8.0") (d (list (d (n "cretonne-codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.8.0") (d #t) (k 0)) (d (n "faerie") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)))) (h "0krs497jsnbmpc1bq71ca5hhlchpl6h5z6bgwf5rg0ajf1l6wsms") (y #t)))

(define-public crate-cretonne-faerie-0.9.0 (c (n "cretonne-faerie") (v "0.9.0") (d (list (d (n "cretonne-codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.9.0") (d #t) (k 0)) (d (n "faerie") (r "^0.4.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.15") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.1") (d #t) (k 0)))) (h "1y3svi1jzkd8sanss9kz33c1znx7pq1zkvxjvj3myk1r3d5f9my5") (y #t)))

(define-public crate-cretonne-faerie-0.10.0 (c (n "cretonne-faerie") (v "0.10.0") (d (list (d (n "cretonne-codegen") (r "^0.10.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.10.0") (d #t) (k 0)) (d (n "faerie") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.15") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "05nn8fkj62id21k7h8f0pc34zz251qsyni2y7j9wb2vpmakwg2aa") (y #t)))

(define-public crate-cretonne-faerie-0.11.0 (c (n "cretonne-faerie") (v "0.11.0") (d (list (d (n "cretonne-codegen") (r "^0.11.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.11.0") (d #t) (k 0)) (d (n "faerie") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.15") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "1knda9p1r2q4hsik2jg8ycijbf3kppi8m6dn1sy2r53p0pg0v19y") (y #t)))

(define-public crate-cretonne-faerie-0.12.0 (c (n "cretonne-faerie") (v "0.12.0") (d (list (d (n "cretonne-codegen") (r "^0.12.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.12.0") (d #t) (k 0)) (d (n "faerie") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.15") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "1c4wi9b50qmj0v6ya0w6ihy175qjbbdrc5l6kvvm9hlkmjs6bmp8") (y #t)))

(define-public crate-cretonne-faerie-0.13.0 (c (n "cretonne-faerie") (v "0.13.0") (d (list (d (n "cretonne-codegen") (r "^0.13.0") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.13.0") (d #t) (k 0)) (d (n "faerie") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.15") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "0br2fbfh36kcm1gbpk89dlm15mysv5kz4yjxzmmgg0zz9nm2ykx9") (y #t)))

(define-public crate-cretonne-faerie-0.13.2 (c (n "cretonne-faerie") (v "0.13.2") (d (list (d (n "cretonne-codegen") (r "^0.13.2") (d #t) (k 0)) (d (n "cretonne-module") (r "^0.13.2") (d #t) (k 0)) (d (n "faerie") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.15") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "19p4wwk24z95hn1vlbc18qzb93s7v3w9f4zinjqlalpy0h0ji009") (y #t)))

