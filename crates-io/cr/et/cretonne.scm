(define-module (crates-io cr et cretonne) #:use-module (crates-io))

(define-public crate-cretonne-0.1.0 (c (n "cretonne") (v "0.1.0") (h "02l2841nfmpabl5x9hyfjsfivzq5g1l7rkxaygc1vx6mahas0a9z") (y #t)))

(define-public crate-cretonne-0.2.0 (c (n "cretonne") (v "0.2.0") (h "0igi3dbyw3clic8i8i2qdsad90vk41f40kihcyv6w9jm9gsjj2zd") (y #t)))

(define-public crate-cretonne-0.3.0 (c (n "cretonne") (v "0.3.0") (h "159jnplx3r504irx2d1n66d1xcq64cx7c5wxsjc9fd8lzcqpbqli") (y #t)))

(define-public crate-cretonne-0.3.1 (c (n "cretonne") (v "0.3.1") (h "01rrz7ak3hfkszdm6ph5lxz0z517bvbg76difkfm2zlh4x4g21fg") (y #t)))

(define-public crate-cretonne-0.3.2 (c (n "cretonne") (v "0.3.2") (h "18bfpn4wjynp2fvcr1qcr0rv6x0visak912br1a562drr2v20gp6") (y #t)))

(define-public crate-cretonne-0.3.3 (c (n "cretonne") (v "0.3.3") (h "0kbsy0m97f99mgqf09553y61lcv92xdmwshqj7yica0v89sm4bk3") (y #t)))

(define-public crate-cretonne-0.3.4 (c (n "cretonne") (v "0.3.4") (h "1c135a4ggv8h8iivah82pl42ypghvh6w7d5fmb8rnc25szvbpyrk") (y #t)))

(define-public crate-cretonne-0.4.0 (c (n "cretonne") (v "0.4.0") (h "1fvr8sajmnq5n4a259hc34c8djwnpyfggh6jrgkflf29g8sg9bkl") (y #t)))

(define-public crate-cretonne-0.4.1 (c (n "cretonne") (v "0.4.1") (h "0ncp6fs0d2z48s9dmy715d7fahg98xciqpp3xpq0z9913b9q2qfn") (y #t)))

(define-public crate-cretonne-0.4.2 (c (n "cretonne") (v "0.4.2") (h "0q8b1vcsbp7lkqycjma7cvainr1wykv304hm3213892a15740gqz") (y #t)))

(define-public crate-cretonne-0.4.3 (c (n "cretonne") (v "0.4.3") (d (list (d (n "cretonne-entity") (r "^0.4.3") (d #t) (k 0)))) (h "01v8cghdxdzwgxm9c9fbblczb96498vb9v5rgiqnrmx0m78qclql") (y #t)))

(define-public crate-cretonne-0.4.4 (c (n "cretonne") (v "0.4.4") (d (list (d (n "cretonne-entity") (r "^0.4.4") (d #t) (k 0)))) (h "074zwb5sys1wa6xcivd6gapcsl2n27gy5l46cm4ssnw7fgjxszg7") (y #t)))

(define-public crate-cretonne-0.5.0 (c (n "cretonne") (v "0.5.0") (d (list (d (n "cretonne-codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "cretonne-frontend") (r "^0.5.0") (d #t) (k 0)))) (h "1zv70grpx5l75kbgwahqrf7cl13glh2vhgswfbhxgb8vir1a89y1") (y #t)))

(define-public crate-cretonne-0.5.1 (c (n "cretonne") (v "0.5.1") (d (list (d (n "cretonne-codegen") (r "^0.5.1") (d #t) (k 0)) (d (n "cretonne-frontend") (r "^0.5.1") (d #t) (k 0)))) (h "1r7xjdx1ks8g41k03d4q1vpdwjf70nx50mixhb857kv6qdi9rqwb") (y #t)))

(define-public crate-cretonne-0.6.0 (c (n "cretonne") (v "0.6.0") (d (list (d (n "cretonne-codegen") (r "^0.6.0") (k 0)) (d (n "cretonne-frontend") (r "^0.6.0") (k 0)))) (h "1p5bimjgvlpbgq6ha9bh1m904g1n505176mdd9kpb5lb6m4i7gkd") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.7.0 (c (n "cretonne") (v "0.7.0") (d (list (d (n "cretonne-codegen") (r "^0.7.0") (k 0)) (d (n "cretonne-frontend") (r "^0.7.0") (k 0)))) (h "1nfb8d0xv7xnp2kwnwy3cafvsz2ljl1268j7v7vb7vxys2maj0y2") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.8.0 (c (n "cretonne") (v "0.8.0") (d (list (d (n "cretonne-codegen") (r "^0.8.0") (k 0)) (d (n "cretonne-frontend") (r "^0.8.0") (k 0)))) (h "0kyg0l53s3qj1c9d0n6853qcj9bbs6jzxx5k495cwvrhzb4myksv") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.9.0 (c (n "cretonne") (v "0.9.0") (d (list (d (n "cretonne-codegen") (r "^0.9.0") (k 0)) (d (n "cretonne-frontend") (r "^0.9.0") (k 0)))) (h "13x2j5mvg8zi3p5kgkzrgv0jlhqrg4bvwmy93qqgg5b6z8fcyn9r") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.10.0 (c (n "cretonne") (v "0.10.0") (d (list (d (n "cretonne-codegen") (r "^0.10.0") (k 0)) (d (n "cretonne-frontend") (r "^0.10.0") (k 0)))) (h "1d7vkj8yfg0hlg3s2zhc7rmn41jy3wdljm5l2yh5j2p9n628231f") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.11.0 (c (n "cretonne") (v "0.11.0") (d (list (d (n "cretonne-codegen") (r "^0.11.0") (k 0)) (d (n "cretonne-frontend") (r "^0.11.0") (k 0)))) (h "1ka7ia5drsa4c1fnxi03dbrlf2r09xazk7d485np5jql17agiqc3") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.12.0 (c (n "cretonne") (v "0.12.0") (d (list (d (n "cretonne-codegen") (r "^0.12.0") (k 0)) (d (n "cretonne-frontend") (r "^0.12.0") (k 0)))) (h "1k1fyhl3f44qx9k3zn1cxwjv00ybb7dggqqbgls138l6qxlhsq83") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.13.0 (c (n "cretonne") (v "0.13.0") (d (list (d (n "cretonne-codegen") (r "^0.13.0") (k 0)) (d (n "cretonne-frontend") (r "^0.13.0") (k 0)))) (h "139jzsnz0b44rwj0gq7gzlpxk1qbalj8m54sdsdc5vbsc847zdc7") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.13.1 (c (n "cretonne") (v "0.13.1") (d (list (d (n "cretonne-codegen") (r "^0.13.1") (k 0)) (d (n "cretonne-frontend") (r "^0.13.1") (k 0)))) (h "1i2xicv5w5ikgrsfkzfvz729f58cb5dr9pnlyd3f2l5dwvqs0i80") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

(define-public crate-cretonne-0.13.2 (c (n "cretonne") (v "0.13.2") (d (list (d (n "cretonne-codegen") (r "^0.13.2") (k 0)) (d (n "cretonne-frontend") (r "^0.13.2") (k 0)))) (h "1dcplbxyq2fqkg7bcrjvj5sry659ibvvmrqaxfcpld0g2y374ihs") (f (quote (("std" "cretonne-codegen/std" "cretonne-frontend/std") ("default" "std") ("core" "cretonne-codegen/core" "cretonne-frontend/core")))) (y #t)))

