(define-module (crates-io cr et cretonne-entity) #:use-module (crates-io))

(define-public crate-cretonne-entity-0.4.3 (c (n "cretonne-entity") (v "0.4.3") (h "09qdsrk75hfmn9ls8zgy4s84qizic6shh7xvdnr6ff52gw21zwxy") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.4.4 (c (n "cretonne-entity") (v "0.4.4") (h "09hhrq7hnc2admh7ish4c5q3yjyqnfhy2p8j0i8qwkh00rh3ppna") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.5.0 (c (n "cretonne-entity") (v "0.5.0") (h "1w8jkgykqbyzx6vx0a1xzlvwgsmx386qbkqfkkkpykam1knpvad5") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.5.1 (c (n "cretonne-entity") (v "0.5.1") (h "00478ba5lffy0iyw3k8997rskyawyqa49vd8wf7lm8ywmsdb684w") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.6.0 (c (n "cretonne-entity") (v "0.6.0") (h "1qrxffp8wiw2m0nw88cya4mxd529gjnhkghkh8hp7iq9x3z8j0xm") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.7.0 (c (n "cretonne-entity") (v "0.7.0") (h "1ykhz2szq9dl8r19nv0bm6c6f9lbdw4ww6xr72w2gqj2755qzq1z") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.8.0 (c (n "cretonne-entity") (v "0.8.0") (h "02xsszkpwnjyqw59dqpwdx1arwfjpssq4sl244dqp417n7da4295") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.9.0 (c (n "cretonne-entity") (v "0.9.0") (h "1h2llcfb88z39iklb7nkqika0mk355gy7z64f8srz4k8yghpc3fq") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.10.0 (c (n "cretonne-entity") (v "0.10.0") (h "0d2r8347lxlrj8kfipn1n6784hp2hg4qwifznbzdi94rvglnmqc3") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.11.0 (c (n "cretonne-entity") (v "0.11.0") (h "192vg5ivy5vk2x47nz07z9xiw4p0yn0pkqsi95q95nxn1xwmggmh") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.12.0 (c (n "cretonne-entity") (v "0.12.0") (h "0fnshsshsbbdarssl6110612fpa3vzsx60m7jani33kv1kydq2a1") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.13.0 (c (n "cretonne-entity") (v "0.13.0") (h "02mz8hd4x1rws1jz8vcly6jafk88wjvg5f476g6p8bga1s2pdc5c") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.13.1 (c (n "cretonne-entity") (v "0.13.1") (h "1rlfdifj4z5h9jyw38h3iy781lggl98p6kdgpnr3jz3568kbw7cq") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cretonne-entity-0.13.2 (c (n "cretonne-entity") (v "0.13.2") (h "0srwv03aak6z1vp3gqfq0v7yckmd5axlnynq4fxnpai2bgpx5yrz") (f (quote (("std") ("default" "std")))) (y #t)))

