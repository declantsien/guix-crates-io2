(define-module (crates-io cr et cretonne-reader) #:use-module (crates-io))

(define-public crate-cretonne-reader-0.1.0 (c (n "cretonne-reader") (v "0.1.0") (d (list (d (n "cretonne") (r "^0.1.0") (d #t) (k 0)))) (h "1r4vii87m7s2w5rd6qfqmj95x65h60l9wd5gv1dam3pl2amam3qr") (y #t)))

(define-public crate-cretonne-reader-0.2.0 (c (n "cretonne-reader") (v "0.2.0") (d (list (d (n "cretonne") (r "^0.2.0") (d #t) (k 0)))) (h "0sd4fjr8b1gs1jckhqkqnc11ljphd72qasid1r0iip96wqx8l0x5") (y #t)))

(define-public crate-cretonne-reader-0.3.0 (c (n "cretonne-reader") (v "0.3.0") (d (list (d (n "cretonne") (r "^0.3.0") (d #t) (k 0)))) (h "1q39yv7xkql2r3xgn79jrzafl71xb6vqx56cwcs1957389m8ym66") (y #t)))

(define-public crate-cretonne-reader-0.3.1 (c (n "cretonne-reader") (v "0.3.1") (d (list (d (n "cretonne") (r "^0.3.1") (d #t) (k 0)))) (h "1c23gbsd4gi0viqvhhj4q5fyfs6v0bvb617qg9d2wv4lfyl9j0h1") (y #t)))

(define-public crate-cretonne-reader-0.3.2 (c (n "cretonne-reader") (v "0.3.2") (d (list (d (n "cretonne") (r "^0.3.2") (d #t) (k 0)))) (h "1cvf32fdnbmdwj25krwk2kkz0yrvf1kxkl43y51chqx654ihjff5") (y #t)))

(define-public crate-cretonne-reader-0.3.3 (c (n "cretonne-reader") (v "0.3.3") (d (list (d (n "cretonne") (r "^0.3.3") (d #t) (k 0)))) (h "0r8k8dij97l5f73wsnd52n3mawjpz9i27lwiwpnmz5cxz3vzqb5q") (y #t)))

(define-public crate-cretonne-reader-0.3.4 (c (n "cretonne-reader") (v "0.3.4") (d (list (d (n "cretonne") (r "^0.3.4") (d #t) (k 0)))) (h "0lgzx4rn44qdl5c6amm939zx9i06i9bkdafh154xi14vpcp9fadz") (y #t)))

(define-public crate-cretonne-reader-0.4.0 (c (n "cretonne-reader") (v "0.4.0") (d (list (d (n "cretonne") (r "^0.4.0") (d #t) (k 0)))) (h "1x9ngr2zkajig9hr037wyvn4bv34yqrxlrjirhzxx1gy00bk0yax") (y #t)))

(define-public crate-cretonne-reader-0.4.1 (c (n "cretonne-reader") (v "0.4.1") (d (list (d (n "cretonne") (r "^0.4.1") (d #t) (k 0)))) (h "12kqnb0j71f8z72hyalh506fjz56gn10c02p41aj7jy993p3dkby") (y #t)))

(define-public crate-cretonne-reader-0.4.2 (c (n "cretonne-reader") (v "0.4.2") (d (list (d (n "cretonne") (r "^0.4.2") (d #t) (k 0)))) (h "07fisxzxnz3ri30vsc816x66rlfkh41b3zalh4s6069282klmsyd") (y #t)))

(define-public crate-cretonne-reader-0.4.3 (c (n "cretonne-reader") (v "0.4.3") (d (list (d (n "cretonne") (r "^0.4.3") (d #t) (k 0)))) (h "094mk5r6cjgdb045w4mbdsx7z2y18ici6ph3c09rjdhg97i2kv31") (y #t)))

(define-public crate-cretonne-reader-0.4.4 (c (n "cretonne-reader") (v "0.4.4") (d (list (d (n "cretonne") (r "^0.4.4") (d #t) (k 0)))) (h "0650qzqp429dpzwlzcmv13cy6fvvisiv8m1ph8aw40v08f5xigp6") (y #t)))

(define-public crate-cretonne-reader-0.5.0 (c (n "cretonne-reader") (v "0.5.0") (d (list (d (n "cretonne-codegen") (r "^0.5.0") (d #t) (k 0)))) (h "18gzgg52nxl8jhicdmymricx17qrgw0rpiwhgyjfzc5k0qgfaf66") (y #t)))

(define-public crate-cretonne-reader-0.5.1 (c (n "cretonne-reader") (v "0.5.1") (d (list (d (n "cretonne-codegen") (r "^0.5.1") (d #t) (k 0)))) (h "0fhv35q2023kajb46jribpnl93wz1jkr7d2gxjnh1mdlw3f72sy7") (y #t)))

(define-public crate-cretonne-reader-0.6.0 (c (n "cretonne-reader") (v "0.6.0") (d (list (d (n "cretonne-codegen") (r "^0.6.0") (d #t) (k 0)))) (h "1gv3n7f4iba49d0j81hkbyl0mmcmdq61flyl426ga92apjbn5hkq") (y #t)))

(define-public crate-cretonne-reader-0.7.0 (c (n "cretonne-reader") (v "0.7.0") (d (list (d (n "cretonne-codegen") (r "^0.7.0") (d #t) (k 0)))) (h "0bn8bhwwb9fawhq28vys29pn3sff6mjwfyw9qp7baqdc80ss8f8z") (y #t)))

(define-public crate-cretonne-reader-0.8.0 (c (n "cretonne-reader") (v "0.8.0") (d (list (d (n "cretonne-codegen") (r "^0.8.0") (d #t) (k 0)))) (h "17majgnahhk8igcjybzgsqdq4cj5g2yrdx9mnpfisqfmfn4wqpzw") (y #t)))

(define-public crate-cretonne-reader-0.9.0 (c (n "cretonne-reader") (v "0.9.0") (d (list (d (n "cretonne-codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.1") (d #t) (k 0)))) (h "1b8df5vh0vj47rvkvnygljipfvcd9i7yxqj7xbj7piwggaryxynn") (y #t)))

(define-public crate-cretonne-reader-0.10.0 (c (n "cretonne-reader") (v "0.10.0") (d (list (d (n "cretonne-codegen") (r "^0.10.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "0nvizjfjqj6j7p98mjfy0sjyqdqj4x5dl3h7z3d0wb43bbvnm39l") (y #t)))

(define-public crate-cretonne-reader-0.11.0 (c (n "cretonne-reader") (v "0.11.0") (d (list (d (n "cretonne-codegen") (r "^0.11.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "0ir020nlrdyvlfyk7605syfywshkmq6y9gf3kfrkq4sa841lsk2s") (y #t)))

(define-public crate-cretonne-reader-0.12.0 (c (n "cretonne-reader") (v "0.12.0") (d (list (d (n "cretonne-codegen") (r "^0.12.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "1jbpf1ixnkmkg15kn8ks7ycp7n7m6s64min223a41szjwqwl66hi") (y #t)))

(define-public crate-cretonne-reader-0.13.0 (c (n "cretonne-reader") (v "0.13.0") (d (list (d (n "cretonne-codegen") (r "^0.13.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "0261mav1kiw9n5llikjjqlk9hyafk2bissxl4p9cyay3ya1rngr0") (y #t)))

(define-public crate-cretonne-reader-0.13.1 (c (n "cretonne-reader") (v "0.13.1") (d (list (d (n "cretonne-codegen") (r "^0.13.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.2") (d #t) (k 0)))) (h "1rqs4wq9rbkh7fw1byc3fc3rcrfqmy1avjw5drim0pwxqm29f0yl") (y #t)))

(define-public crate-cretonne-reader-0.13.2 (c (n "cretonne-reader") (v "0.13.2") (d (list (d (n "cretonne-codegen") (r "^0.13.2") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.0.3") (d #t) (k 0)))) (h "17w4knjzhjdxjs1p7vl2w2ywh1x26p14ys89621db8pbcgkpx951") (y #t)))

