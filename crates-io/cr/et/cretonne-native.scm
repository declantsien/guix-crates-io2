(define-module (crates-io cr et cretonne-native) #:use-module (crates-io))

(define-public crate-cretonne-native-0.1.0 (c (n "cretonne-native") (v "0.1.0") (d (list (d (n "cretonne") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0r84nq1d6bpr724xb3v6znc788d1xmsb1pr71s4w2mi0x84nvani") (y #t)))

(define-public crate-cretonne-native-0.2.0 (c (n "cretonne-native") (v "0.2.0") (d (list (d (n "cretonne") (r "^0.2.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1b4hnr14nz34gqbs0x92sz416pf556g856xpsgybv3rsc7a65zzj") (y #t)))

(define-public crate-cretonne-native-0.3.0 (c (n "cretonne-native") (v "0.3.0") (d (list (d (n "cretonne") (r "^0.3.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0kvrz6amj0i3kjxqrghcz7vws0nybqn3w2y9srwq0p97hkq1h4sb") (y #t)))

(define-public crate-cretonne-native-0.3.1 (c (n "cretonne-native") (v "0.3.1") (d (list (d (n "cretonne") (r "^0.3.1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "04fzs3a3z7z6qigs8g4kv42vc20j0wmf5qcvpv25ldfrzcn6k8wk") (y #t)))

(define-public crate-cretonne-native-0.3.2 (c (n "cretonne-native") (v "0.3.2") (d (list (d (n "cretonne") (r "^0.3.2") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0by72qdnjimh4h0cjx61l6dg5yljyawx5yxxhg0hbx0a5fjcdzsn") (y #t)))

(define-public crate-cretonne-native-0.3.3 (c (n "cretonne-native") (v "0.3.3") (d (list (d (n "cretonne") (r "^0.3.3") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1rj0ia35hb3l0dv6p38ycqcpxdvyxa4z9zpfclrgyzkhsdnzwgfc") (y #t)))

(define-public crate-cretonne-native-0.3.4 (c (n "cretonne-native") (v "0.3.4") (d (list (d (n "cretonne") (r "^0.3.4") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1s9g6szw3k752q5hg9x5qbai545vh5ldlyvbqk6wdskrppls2s42") (y #t)))

(define-public crate-cretonne-native-0.4.0 (c (n "cretonne-native") (v "0.4.0") (d (list (d (n "cretonne") (r "^0.4.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0266rnamlr13h6bdysmjq9zvynj0yckz9m0n5fixjjwkqkcvaxlb") (y #t)))

(define-public crate-cretonne-native-0.4.1 (c (n "cretonne-native") (v "0.4.1") (d (list (d (n "cretonne") (r "^0.4.1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1mswi5fpc3wrc1zmkgfb27s3r1bbi4wkqhswmhkgagd9qcjmjsac") (y #t)))

(define-public crate-cretonne-native-0.4.2 (c (n "cretonne-native") (v "0.4.2") (d (list (d (n "cretonne") (r "^0.4.2") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1ljycalxpfqsb4hccbijbk3bbc6m5idgp043w9ck352bw8s57hgl") (y #t)))

(define-public crate-cretonne-native-0.4.3 (c (n "cretonne-native") (v "0.4.3") (d (list (d (n "cretonne") (r "^0.4.3") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0k4z5d2xg09krb5ag4f08k1a66m6bn7llah2h1bawhj8x8y5hd3j") (y #t)))

(define-public crate-cretonne-native-0.4.4 (c (n "cretonne-native") (v "0.4.4") (d (list (d (n "cretonne") (r "^0.4.4") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1s4f67ib12ml00jycgmksifndk9pxhnf076kmpsz85m68wna35g6") (y #t)))

(define-public crate-cretonne-native-0.5.0 (c (n "cretonne-native") (v "0.5.0") (d (list (d (n "cretonne-codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1526lkk5w1558mhs6ihdfy7kzyv5cwhyyvk5cpjbxqr81a00nxgb") (y #t)))

(define-public crate-cretonne-native-0.5.1 (c (n "cretonne-native") (v "0.5.1") (d (list (d (n "cretonne-codegen") (r "^0.5.1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0vd2pgx2jldh0kwlicwk4y3ybvlizk84z81akdk7qbq221gi922k") (y #t)))

(define-public crate-cretonne-native-0.6.0 (c (n "cretonne-native") (v "0.6.0") (d (list (d (n "cretonne-codegen") (r "^0.6.0") (k 0)) (d (n "raw-cpuid") (r "^3.1.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1mc3nk6694yjwv3384bqsyppy49482m82sx94vpbxz8n57cl21yz") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-native-0.7.0 (c (n "cretonne-native") (v "0.7.0") (d (list (d (n "cretonne-codegen") (r "^0.7.0") (k 0)) (d (n "raw-cpuid") (r "^3.1.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "18wc3q09clglk8j3bbbnzkd3q3j09ix1hrqkqv4mydjx49mk35fv") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-native-0.8.0 (c (n "cretonne-native") (v "0.8.0") (d (list (d (n "cretonne-codegen") (r "^0.8.0") (k 0)) (d (n "raw-cpuid") (r "^3.1.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "041n0vngni6qsm6ql33nl5f0z2yndabai13ka0zj7brd79ynxv0k") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core")))) (y #t)))

(define-public crate-cretonne-native-0.9.0 (c (n "cretonne-native") (v "0.9.0") (d (list (d (n "cretonne-codegen") (r "^0.9.0") (k 0)) (d (n "raw-cpuid") (r "^3.1.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.0.1") (k 0)))) (h "0n0bk4qhymcbirg26v0qc5fxibnxdb8balqm5dyh59aszvpsyjw8") (f (quote (("std" "cretonne-codegen/std") ("default" "std") ("core" "cretonne-codegen/core" "raw-cpuid/nightly")))) (y #t)))

(define-public crate-cretonne-native-0.10.0 (c (n "cretonne-native") (v "0.10.0") (d (list (d (n "cretonne-codegen") (r "^0.10.0") (k 0)) (d (n "raw-cpuid") (r "^3.1.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.0.2") (k 0)))) (h "126qd0kdy4mzjm5lvf1kcn1l5mq5nah9hayzgmsx27lk1gvj1qvi") (f (quote (("std" "cretonne-codegen/std" "target-lexicon/std") ("default" "std") ("core" "cretonne-codegen/core" "raw-cpuid/nightly")))) (y #t)))

(define-public crate-cretonne-native-0.11.0 (c (n "cretonne-native") (v "0.11.0") (d (list (d (n "cretonne-codegen") (r "^0.11.0") (k 0)) (d (n "raw-cpuid") (r "^3.1.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.0.2") (k 0)))) (h "1ds0ygm92an9s03js5wigiw8wv6xhnqrqvqh5ncwvjj9hbbw3g3w") (f (quote (("std" "cretonne-codegen/std" "target-lexicon/std") ("default" "std") ("core" "cretonne-codegen/core" "raw-cpuid/nightly")))) (y #t)))

(define-public crate-cretonne-native-0.12.0 (c (n "cretonne-native") (v "0.12.0") (d (list (d (n "cretonne-codegen") (r "^0.12.0") (k 0)) (d (n "raw-cpuid") (r "^3.1.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.0.2") (k 0)))) (h "0v0i2q100a5gp6hqq1p9prin5ik9hcbfvvm9rnrbna8ghpg6imv6") (f (quote (("std" "cretonne-codegen/std" "target-lexicon/std") ("default" "std") ("core" "cretonne-codegen/core" "raw-cpuid/nightly")))) (y #t)))

(define-public crate-cretonne-native-0.13.0 (c (n "cretonne-native") (v "0.13.0") (d (list (d (n "cretonne-codegen") (r "^0.13.0") (k 0)) (d (n "raw-cpuid") (r "^4") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.0.2") (k 0)))) (h "1igh686hcvvxvi0gl88n8g0i96prhpzp3qhwcav27f1b565s0p55") (f (quote (("std" "cretonne-codegen/std" "target-lexicon/std") ("default" "std") ("core" "cretonne-codegen/core" "raw-cpuid/nightly")))) (y #t)))

(define-public crate-cretonne-native-0.13.1 (c (n "cretonne-native") (v "0.13.1") (d (list (d (n "cretonne-codegen") (r "^0.13.1") (k 0)) (d (n "raw-cpuid") (r "^4") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.0.2") (k 0)))) (h "1cyxfgl5sp2g158ldhyyvqys3iqcggbkf4ynqymm702xwrd3qh8s") (f (quote (("std" "cretonne-codegen/std" "target-lexicon/std") ("default" "std") ("core" "cretonne-codegen/core" "raw-cpuid/nightly")))) (y #t)))

(define-public crate-cretonne-native-0.13.2 (c (n "cretonne-native") (v "0.13.2") (d (list (d (n "cretonne-codegen") (r "^0.13.2") (k 0)) (d (n "raw-cpuid") (r "^4") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)) (d (n "target-lexicon") (r "^0.0.3") (k 0)))) (h "0q11fln85zl7wjiypjssawdmbcbm95knb161zjs25j5ycpy67z2g") (f (quote (("std" "cretonne-codegen/std" "target-lexicon/std") ("default" "std") ("core" "cretonne-codegen/core" "raw-cpuid/nightly")))) (y #t)))

