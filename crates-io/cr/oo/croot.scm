(define-module (crates-io cr oo croot) #:use-module (crates-io))

(define-public crate-croot-0.1.0 (c (n "croot") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0010aivw8b232psmv0zq1yk16yiwm40kldia4ygvfyhhspyi4cvn") (y #t)))

(define-public crate-croot-0.1.1 (c (n "croot") (v "0.1.1") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "14hsx1fwhxgbwdwd2xd59q2vnlz7z5y63hw404wwqhlnsjybzw4f") (y #t)))

(define-public crate-croot-0.2.0 (c (n "croot") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0zdkhsvikrnawds62assk9fw5qz62sp2dkn13hszk68aj4vv59b2") (y #t)))

(define-public crate-croot-0.2.1 (c (n "croot") (v "0.2.1") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0hjx5i34w92j91mds0k67pq8hr6x1y6qmj1yqkdasji707mv3z54") (y #t)))

(define-public crate-croot-0.3.0 (c (n "croot") (v "0.3.0") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0fbcis40gvrfsl216m4hfjgfg3sdb3hp4bkwiksig146qz7qymc7")))

(define-public crate-croot-0.3.1 (c (n "croot") (v "0.3.1") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0z4kbdvqqajsnlxl7iqjakhdxcv1cjwg2rpn13rm9fbv3bdj0qvq")))

(define-public crate-croot-0.3.2 (c (n "croot") (v "0.3.2") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "0a6wrhswp80yc1dv5112vzxqmijhyl4zngjhywk1zrq3750gg8ln")))

(define-public crate-croot-0.3.3 (c (n "croot") (v "0.3.3") (d (list (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "04zgv3hnw8r7qll35dgkr5zabwbsll08qrdrf03pz1b4601qg073")))

