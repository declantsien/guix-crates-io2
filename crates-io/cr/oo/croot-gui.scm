(define-module (crates-io cr oo croot-gui) #:use-module (crates-io))

(define-public crate-croot-gui-0.1.0 (c (n "croot-gui") (v "0.1.0") (d (list (d (n "croot") (r "^0.3.2") (d #t) (k 0)) (d (n "eframe") (r "^0.21.3") (o #t) (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (f (quote ("image"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg" "png"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)))) (h "1rq7n1as67yqwhw771x381px4rfsn6hyd3nmwnggsw23s3i4427z") (s 2) (e (quote (("gui" "dep:eframe" "dep:egui_extras"))))))

