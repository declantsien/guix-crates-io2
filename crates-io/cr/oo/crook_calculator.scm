(define-module (crates-io cr oo crook_calculator) #:use-module (crates-io))

(define-public crate-crook_calculator-0.1.0 (c (n "crook_calculator") (v "0.1.0") (h "12pcqwhvpnmmrjqc36v3ghx3b6mvvf7zhwxxcak4v0yiy965qb4g")))

(define-public crate-crook_calculator-0.1.1 (c (n "crook_calculator") (v "0.1.1") (h "1gzcizw34g4hjbyvlk5bd9np9g66zwmwp72z96nqyc8jskrnr595")))

