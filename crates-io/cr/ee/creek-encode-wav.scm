(define-module (crates-io cr ee creek-encode-wav) #:use-module (crates-io))

(define-public crate-creek-encode-wav-0.1.0 (c (n "creek-encode-wav") (v "0.1.0") (d (list (d (n "byte-slice-cast") (r "^1.0.0") (d #t) (k 0)) (d (n "creek-core") (r "^0.1") (d #t) (k 0)))) (h "0jvs0chai7a61mzq2cnrdwdw0gdd46l7waj5rxxqvzz6ny1sm1jx")))

(define-public crate-creek-encode-wav-0.1.1 (c (n "creek-encode-wav") (v "0.1.1") (d (list (d (n "byte-slice-cast") (r "^1.0.0") (d #t) (k 0)) (d (n "creek-core") (r "^0.1.1") (d #t) (k 0)))) (h "05pkjlanwqsl8njkyfizzp328zk93rxpic04kn3lifsmshi28gh6")))

(define-public crate-creek-encode-wav-0.2.0 (c (n "creek-encode-wav") (v "0.2.0") (d (list (d (n "byte-slice-cast") (r "^1.0.0") (d #t) (k 0)) (d (n "creek-core") (r "^0.2.0") (d #t) (k 0)))) (h "1rn0z0jxmcvzp68bmwjkdbrzdxyscgm5kvhx93wfydmf364rnx9g")))

