(define-module (crates-io cr ee creepy) #:use-module (crates-io))

(define-public crate-creepy-0.1.0 (c (n "creepy") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "1d3apkp4l81qv9dridpws1924j4s5dqznb5lph5mrlyqr2zx3562")))

(define-public crate-creepy-0.2.0 (c (n "creepy") (v "0.2.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "12szrn1npl2kxi0w53akx34x0n5q7v6a0xrnahlljidi9vz8925d")))

(define-public crate-creepy-0.2.1 (c (n "creepy") (v "0.2.1") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)))) (h "1c02847zwzn7gprgy6a5i1f7p7ny6nywgfla8n7i964d93ja1sxd")))

(define-public crate-creepy-0.2.2 (c (n "creepy") (v "0.2.2") (d (list (d (n "arboard") (r "^3.2.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)))) (h "1whzz11iy67amn4sqa7y14f87mfn7q8ndlnrgrqbiiaiqb5dwyd0")))

