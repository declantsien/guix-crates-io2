(define-module (crates-io cr ee creep) #:use-module (crates-io))

(define-public crate-creep-0.1.0 (c (n "creep") (v "0.1.0") (h "0ssm0b1cw6rxfkkr956h9pfwbw76hivapibv09zr9vbgyw6g2wyi")))

(define-public crate-creep-0.1.1 (c (n "creep") (v "0.1.1") (h "12xwxsanwjnzq60l09jbmnjksarh8n92j38mn7r8c7b3fxd203xl")))

(define-public crate-creep-0.2.0 (c (n "creep") (v "0.2.0") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)))) (h "1i4ywghjcla9xmdqk31035zrj5qq4dzsk6x5smrinswh94bm8y6l")))

(define-public crate-creep-0.2.1 (c (n "creep") (v "0.2.1") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)))) (h "1zsx3lkcg6gdj6mph0w8rs77525l89pcllb2v1f198vmz55c3pb2")))

(define-public crate-creep-0.2.2 (c (n "creep") (v "0.2.2") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)))) (h "0p77hnfhndf67b8qwc40myk1hvyg08hp25vb243s51hl66f9ics4")))

(define-public crate-creep-0.3.0 (c (n "creep") (v "0.3.0") (d (list (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)))) (h "09n33l7lg0fx7i8w934lyljyiinv2mf00pr75jsgb628dz61jkmx")))

