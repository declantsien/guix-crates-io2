(define-module (crates-io cr ee creek-core) #:use-module (crates-io))

(define-public crate-creek-core-0.1.0 (c (n "creek-core") (v "0.1.0") (d (list (d (n "rtrb") (r "^0.2") (d #t) (k 0)))) (h "0l9madxczjj1n6byv8mj9fls1kv743n74iq9pnvfg45ls1399sby")))

(define-public crate-creek-core-0.1.1 (c (n "creek-core") (v "0.1.1") (d (list (d (n "rtrb") (r "^0.2") (d #t) (k 0)))) (h "1r6ni6hhp4yj6c9rq7b5vfai9m6alhjga9nmca5wd0076jzxwkrj")))

(define-public crate-creek-core-0.2.0 (c (n "creek-core") (v "0.2.0") (d (list (d (n "rtrb") (r "^0.3.0") (d #t) (k 0)))) (h "0h0ykl3jg0x5p9r8vqmz7g4nj9407g6a539j0s7jnhgsxb9cxinm")))

(define-public crate-creek-core-0.2.1 (c (n "creek-core") (v "0.2.1") (d (list (d (n "rtrb") (r "^0.3.0") (d #t) (k 0)))) (h "17qq7jyysq587j8jpvr35rpncljd188fnyn8na19rr0pmvqsbjws")))

(define-public crate-creek-core-0.2.2 (c (n "creek-core") (v "0.2.2") (d (list (d (n "rtrb") (r "^0.3.0") (d #t) (k 0)))) (h "0761pvvnwy0rx2a76r6ghfbpq1jw68jg0dnjh0qibxvrfs781652")))

