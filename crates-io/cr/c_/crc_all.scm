(define-module (crates-io cr c_ crc_all) #:use-module (crates-io))

(define-public crate-crc_all-0.1.0 (c (n "crc_all") (v "0.1.0") (h "11lz5qn9hcaswdx7qn6nhfz0ic7zfsas7vv7dka3w9gzmg4zn0s5")))

(define-public crate-crc_all-0.2.0 (c (n "crc_all") (v "0.2.0") (h "1m4724kjkw6q5cncfldclvn2rs7rdgkqazrvcsf95nya4nphx1x8")))

(define-public crate-crc_all-0.2.1 (c (n "crc_all") (v "0.2.1") (d (list (d (n "crc_all_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0r3bxlgpi4n88462lwhvy3ak96rbcqgs3p3bajizdn73y4s5ffz4") (y #t)))

(define-public crate-crc_all-0.2.2 (c (n "crc_all") (v "0.2.2") (h "06vgiai5kkwbdh6053kxjzlzg111plbrmbmk9mqigygfxcbilv64")))

