(define-module (crates-io cr es crest) #:use-module (crates-io))

(define-public crate-crest-0.1.0 (c (n "crest") (v "0.1.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "14vniqsiqbi9p09f5mvgrx2i7ra6jrivpwzm77lbr74baq55wv80")))

(define-public crate-crest-0.1.1 (c (n "crest") (v "0.1.1") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "01j91ghvrckmr7ihczjmh9scw6yfwl3lk4rlxb1py4ljrgpdcz8k")))

(define-public crate-crest-0.2.0 (c (n "crest") (v "0.2.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0i7j9j16lsh04y6qb3mpaqn1sarxrii5p16cjsx1qnkv7f6p7kbc")))

(define-public crate-crest-0.3.0 (c (n "crest") (v "0.3.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.26") (d #t) (k 1)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0nkd4v1cxhi2mdm8p89al7l31r4rpyx29bwgwzphc4x27ix5hv5r") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-crest-0.3.1 (c (n "crest") (v "0.3.1") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.26") (d #t) (k 1)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1n36gh5zaqffvg8a2q12qdskm5x86c8k9wskbai2vfyxqg2hkvjx") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-crest-0.3.2 (c (n "crest") (v "0.3.2") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.26") (d #t) (k 1)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0n53nxibb3kcjdcy5m46vhgjmwynqyllixam613ipp4qblhb12d3") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-crest-0.3.3 (c (n "crest") (v "0.3.3") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.26") (d #t) (k 1)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "19yiflyrqwhqdad9n6zi0hkyrk137r58hdl3k697lg6wf6fb7yj3") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-crest-0.3.4 (c (n "crest") (v "0.3.4") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1v9ir7n9q2c7q6f4rji79gjzsnillg4mlmsdsrhrljhy5xf1mhsn")))

