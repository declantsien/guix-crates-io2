(define-module (crates-io cr ys crystalsoft-utils) #:use-module (crates-io))

(define-public crate-crystalsoft-utils-0.1.0 (c (n "crystalsoft-utils") (v "0.1.0") (h "1wpc0dkg6aq1vfj80symk5h0yyy7b6lldqhws34aq0cdxwj6ngwb")))

(define-public crate-crystalsoft-utils-0.1.1 (c (n "crystalsoft-utils") (v "0.1.1") (h "1mvsf79inhk76xsi10056i4lnjp3wjmmr3w29rx2k1klz9kfra45")))

