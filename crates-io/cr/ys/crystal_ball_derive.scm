(define-module (crates-io cr ys crystal_ball_derive) #:use-module (crates-io))

(define-public crate-crystal_ball_derive-0.3.0 (c (n "crystal_ball_derive") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zali8wk90601pvhx859giq21k7ksgr6xfvh6ixhzlxi185lb1rf")))

