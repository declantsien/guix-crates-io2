(define-module (crates-io cr ys crystalorb-mock-network) #:use-module (crates-io))

(define-public crate-crystalorb-mock-network-0.2.0 (c (n "crystalorb-mock-network") (v "0.2.0") (d (list (d (n "crystalorb") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xx2np3c8sjmx4pk70b915c9rn7gz7j8zrzjagk6bd6b60gq6bhz")))

(define-public crate-crystalorb-mock-network-0.2.1 (c (n "crystalorb-mock-network") (v "0.2.1") (d (list (d (n "crystalorb") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "04ylxvyj5zda1r8jgv08kqpp2gryi7q8lcsa1sj639pxra9s123c")))

(define-public crate-crystalorb-mock-network-0.3.0 (c (n "crystalorb-mock-network") (v "0.3.0") (d (list (d (n "crystalorb") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "14gbl603s574r6kdk5fnyvahfbx9ws5bb61irg3yrs956fmrq4jd")))

