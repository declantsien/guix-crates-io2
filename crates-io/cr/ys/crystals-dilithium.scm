(define-module (crates-io cr ys crystals-dilithium) #:use-module (crates-io))

(define-public crate-crystals-dilithium-1.0.0 (c (n "crystals-dilithium") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1819q22asws3zbd6w7w8647bdna2b42s77d2aiyljgsfnic2zgzv")))

