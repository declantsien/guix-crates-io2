(define-module (crates-io cr c3 crc32fix) #:use-module (crates-io))

(define-public crate-crc32fix-1.0.0 (c (n "crc32fix") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "png") (r "^0.13.2") (d #t) (k 0)))) (h "03m9klb9pmrsz50bz060kxnyywxgn0hd3dxxfmf7y7cmvnxxay9d")))

