(define-module (crates-io cr c3 crc32-v2) #:use-module (crates-io))

(define-public crate-crc32-v2-0.0.0 (c (n "crc32-v2") (v "0.0.0") (h "1fx1zjkfmad03r3qrzpk44rjfva8yllva54jipynh72775kkgr4h")))

(define-public crate-crc32-v2-0.0.1 (c (n "crc32-v2") (v "0.0.1") (h "0rgcln257w67sr3a3s1f1s7sfnzbbx6xfqbhdpz1lk2b4mcz2ivd")))

(define-public crate-crc32-v2-0.0.2 (c (n "crc32-v2") (v "0.0.2") (h "1va0yfxkhqi09sxwdhd1h7yh61ag9qh2xmz0brf2nba5z3gng5pa")))

(define-public crate-crc32-v2-0.0.3 (c (n "crc32-v2") (v "0.0.3") (h "07c0lagi12drbxdrp0scb3n0wvmqa5q9xlmq0qgif5nfz3mh0viv")))

(define-public crate-crc32-v2-0.0.4 (c (n "crc32-v2") (v "0.0.4") (h "0lfs9ic0z8bkhmrsg990qaxpk0i0964hs1za7dn6k41lrk76ym3z")))

