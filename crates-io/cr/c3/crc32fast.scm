(define-module (crates-io cr c3 crc32fast) #:use-module (crates-io))

(define-public crate-crc32fast-1.0.0 (c (n "crc32fast") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0ygi0x9sjyr8wzzz7zn6bpjbfzsqqw5vd1xqy4px3xscc9y4y5n2") (y #t)))

(define-public crate-crc32fast-1.0.1 (c (n "crc32fast") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "09dj9q0plfxaafsjmqciqn0j2baw8x8gg1mgl4ycpd0fk918krnr")))

(define-public crate-crc32fast-1.0.2 (c (n "crc32fast") (v "1.0.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "19bch6ih96jxh520qfaww6mj2dm1yb5nksz0dh9558iihsq3mg5r")))

(define-public crate-crc32fast-1.0.3 (c (n "crc32fast") (v "1.0.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1x34qcsfgrm90rpw901aglvz68f9jsp0g1p29xh34kn1hd45wyd8")))

(define-public crate-crc32fast-1.0.4 (c (n "crc32fast") (v "1.0.4") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1afz1mxgmg7mhvqs24rlc84dxl8qpjp5vq16cfn0297hm6qqrld6")))

(define-public crate-crc32fast-1.1.0 (c (n "crc32fast") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "196q5w7hb0ak0cs900bgcv9slxaf2a8nvqjxf1a60k310mqqaq5x")))

(define-public crate-crc32fast-1.1.1 (c (n "crc32fast") (v "1.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1svk1y49xk0878mhq7fp6v2qfpgf8xh8f3zxdajczg58kxaqbrp0")))

(define-public crate-crc32fast-1.1.2 (c (n "crc32fast") (v "1.1.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "14piiddv0y0r8az5myd5x2lfs9aw4wsqy52zxcxg6plpqr0547g9")))

(define-public crate-crc32fast-1.2.0 (c (n "crc32fast") (v "1.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1c9dhkvf3brrzzplcijaywxi2w8wv5578i0ryhcm7x8dmzi5s4ms") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.2.1 (c (n "crc32fast") (v "1.2.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "06ivjlkzcxxxk7nyshc44aql4zjpmvirq46vmzrakdjax3n6y5c1") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.2.2 (c (n "crc32fast") (v "1.2.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "17riq124dh6rfanysm68vmgp9sdll4sbd326qiyr3508b3lb299q") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.3.0 (c (n "crc32fast") (v "1.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0di8ip18srxbva7dvz8y9rqmmc43s1lc3nasl4fgr17az86jk33k") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.3.1 (c (n "crc32fast") (v "1.3.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qv0krk4ggxzz68x199xm9wg3bw4dgiff8971dznz1r91qqrq852") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.3.2 (c (n "crc32fast") (v "1.3.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "03c8f29yx293yf43xar946xbls1g60c207m9drf8ilqhr25vsh5m") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.4.0 (c (n "crc32fast") (v "1.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ahy259ypc955l5ak24hdlgllb6vm6y2pvwr6qrlyisbg255m1dk") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.4.1 (c (n "crc32fast") (v "1.4.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1bgb0bj6mhqxs8v7ahmi6xk97f4yv5i3jg1cbmicg19ijvbgissq") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1.4.2 (c (n "crc32fast") (v "1.4.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1czp7vif73b8xslr3c9yxysmh9ws2r8824qda7j47ffs9pcnjxx9") (f (quote (("std") ("nightly") ("default" "std"))))))

