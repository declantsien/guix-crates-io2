(define-module (crates-io cr c3 crc32csum) #:use-module (crates-io))

(define-public crate-crc32csum-0.1.0 (c (n "crc32csum") (v "0.1.0") (d (list (d (n "crc32c") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "055dsixqhjggjnrh34nvjpqbza1mws791limjxz1rsn9rn5wwfdx")))

