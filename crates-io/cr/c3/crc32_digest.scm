(define-module (crates-io cr c3 crc32_digest) #:use-module (crates-io))

(define-public crate-crc32_digest-0.8.0 (c (n "crc32_digest") (v "0.8.0") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1fjygz5bwf9bjqqxv408xcrkzak24jbcsn5c7sbz34cma0nx4vpr")))

(define-public crate-crc32_digest-0.8.1 (c (n "crc32_digest") (v "0.8.1") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "10qrnj4r64wrdllqbpvvggxh5klwivqj5vhsd70sc15s2syfzma5")))

