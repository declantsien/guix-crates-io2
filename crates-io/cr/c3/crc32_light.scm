(define-module (crates-io cr c3 crc32_light) #:use-module (crates-io))

(define-public crate-crc32_light-0.1.0 (c (n "crc32_light") (v "0.1.0") (h "1jh60bgamp6nkjp0vqy01a822r2zr454b57byn93hlgy3qn1pwdp")))

(define-public crate-crc32_light-0.1.1 (c (n "crc32_light") (v "0.1.1") (h "08q311qq1zd4mcr3p9viyyljdhdd3qrmahw98lwnyk80m4hk54yn")))

