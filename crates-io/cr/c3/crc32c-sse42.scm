(define-module (crates-io cr c3 crc32c-sse42) #:use-module (crates-io))

(define-public crate-crc32c-sse42-0.0.0 (c (n "crc32c-sse42") (v "0.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i8zcac9zl0mh7iy8prlxgazzc8vb5nmlh9y74ddg847kypm217y") (f (quote (("sse42") ("default" "sse42"))))))

