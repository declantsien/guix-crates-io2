(define-module (crates-io cr c3 crc32) #:use-module (crates-io))

(define-public crate-crc32-0.0.1 (c (n "crc32") (v "0.0.1") (h "15y37pahd06a02clvzx3xbkpvxchpvl8gpv97i5w3kinld84b4m4")))

(define-public crate-crc32-0.0.2 (c (n "crc32") (v "0.0.2") (h "1zy351clxb3gbxv0y3zjl181zxgybaxsbhvgjnkgxwiqbz2vl32b")))

