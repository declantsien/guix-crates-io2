(define-module (crates-io cr c3 crc32c) #:use-module (crates-io))

(define-public crate-crc32c-0.1.0 (c (n "crc32c") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1ba19jdvdljzpqrvjqwadv4x77dl6i5pzy2xqr5gy8yh87kkq0d2")))

(define-public crate-crc32c-0.1.1 (c (n "crc32c") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.53") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0i1z3rk4cb9275c5zdx9kbkqfz4irnk9brz46ja7s9jv3nr8xbw3")))

(define-public crate-crc32c-0.2.0 (c (n "crc32c") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "stdsimd") (r "^0.0.3") (d #t) (k 0)))) (h "07c6ca7hlq959zjr7cpn9k7mv0s1xgkzp2mip4rirq49ykrj8zq1")))

(define-public crate-crc32c-0.3.0 (c (n "crc32c") (v "0.3.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0x3yh3abp2mwvyn5gbm23by390f61airjrkrsaq4dik7p62h1aw9")))

(define-public crate-crc32c-0.3.1 (c (n "crc32c") (v "0.3.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1g1m1qfazn4qh3gsqsrksb1laz1rijx8yfiq3gpamqriab5sypc7")))

(define-public crate-crc32c-0.4.0 (c (n "crc32c") (v "0.4.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1ic4xmkjgscfp1jld1n3v0m5s7aysqidb0p8rv0qhaf14vpkgfkp")))

(define-public crate-crc32c-0.5.0 (c (n "crc32c") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)))) (h "1ajig62ibbkr8ypb7500iwvkb3g0329sn6677rsms1ap3ps9lhgn")))

(define-public crate-crc32c-0.6.0 (c (n "crc32c") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0j4l9ghdhfk78yap0ssz5s9bawglca3wv45zz8mj30ba7s9xy311")))

(define-public crate-crc32c-0.6.1 (c (n "crc32c") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1d948nrdp4aj7nldd4hqv5xgidw9cw43c25xi2lwnjsqi69rqszf")))

(define-public crate-crc32c-0.6.2 (c (n "crc32c") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0awgj8p771wkr4nnnlkd196f91kbgv9sd5i5zpal8bmvr2055xwq")))

(define-public crate-crc32c-0.6.3 (c (n "crc32c") (v "0.6.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0gingi2i3r057y3pfpq62vaddbiglw88l9pv8lw7m4p98bds5zix")))

(define-public crate-crc32c-0.6.4 (c (n "crc32c") (v "0.6.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0x2af5jknzh4inj0i6shwpjhm9s9r1c8l79bbhywblmlwmh8vx6q")))

(define-public crate-crc32c-0.6.5 (c (n "crc32c") (v "0.6.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1qhcylgz7y7ifw0wa92yfmnz1w0hr1aaxcs4vq4ad7wvmac4a9c9")))

(define-public crate-crc32c-0.6.6 (c (n "crc32c") (v "0.6.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0zawyjr3pmj28p2cpxpxhd7jfw2v34k5wb5bc080gsa9dsxkhsvi")))

(define-public crate-crc32c-0.6.7 (c (n "crc32c") (v "0.6.7") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("alloc" "getrandom"))) (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0iv9kd71hjw7s3mlr1lhxn2ww91l4ikc0579gjy4jpak7vwvj9q2")))

