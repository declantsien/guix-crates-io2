(define-module (crates-io cr c3 crc32c-hw) #:use-module (crates-io))

(define-public crate-crc32c-hw-0.0.0 (c (n "crc32c-hw") (v "0.0.0") (h "067cqjlj3vz9k2virprs83k65dpgkc7fdhrww0p1s92ag9wpnwds")))

(define-public crate-crc32c-hw-0.1.0 (c (n "crc32c-hw") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1rjc1wm0jaijz4p8gay075h9s9mv8jbxsfjndlzvyd9kgh3c35mh")))

(define-public crate-crc32c-hw-0.1.1 (c (n "crc32c-hw") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1mf9vh0fbkzjrjriwb87rgzf81rjd3jafhvav4615iic94ajc3lm") (f (quote (("no-stdlib"))))))

(define-public crate-crc32c-hw-0.1.2 (c (n "crc32c-hw") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "stdsimd") (r "^0.0.3") (d #t) (k 0)))) (h "029bhv64hj5q4iv3axmxs3v8clf0y7jzaikk1bpv85b3v5ih6p4v") (f (quote (("no-stdlib"))))))

(define-public crate-crc32c-hw-0.1.3 (c (n "crc32c-hw") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1y4p9l7lv52m1d39i99dsj12x1lk5mah1fv1kxd3k90v1b5p4hwr") (f (quote (("no-stdlib"))))))

