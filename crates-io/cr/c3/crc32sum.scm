(define-module (crates-io cr c3 crc32sum) #:use-module (crates-io))

(define-public crate-crc32sum-0.1.0 (c (n "crc32sum") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)))) (h "0xp5ybxmc2msjdyasz8rlq11y4r4y6hn5fsvpgpnya152v90azaz")))

