(define-module (crates-io cr c3 crc32mnemo) #:use-module (crates-io))

(define-public crate-crc32mnemo-0.1.0 (c (n "crc32mnemo") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "0yk94jnl0hfkv06wn4ffk4d91093ma4d9yv0vis38xcdiw426qcq") (r "1.59.0")))

(define-public crate-crc32mnemo-0.2.0 (c (n "crc32mnemo") (v "0.2.0") (d (list (d (n "bech32") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "0v2dng6jvdcgm0c9z21rpvf7ihf1m3kcx95090jzd02hb2jrdwva") (r "1.59.0")))

(define-public crate-crc32mnemo-0.3.0 (c (n "crc32mnemo") (v "0.3.0") (d (list (d (n "amplify") (r "^4.0.0-alpha.1") (d #t) (k 0)) (d (n "bech32") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "mnemonic") (r "^1.0.1") (d #t) (k 0)))) (h "0y1g4iv88r43zz07ay8sqjah598lvdy9ll8wczi6wcy58ldjgnd0") (r "1.59.0")))

