(define-module (crates-io cr oi croissant) #:use-module (crates-io))

(define-public crate-croissant-0.0.0-alpha (c (n "croissant") (v "0.0.0-alpha") (d (list (d (n "chrono") (r "^0.4.33") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1i1abz3hkabr544gjg6r9943cazy525m4y7jfghp68yvgry23dpy") (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-croissant-0.0.0-beta (c (n "croissant") (v "0.0.0-beta") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "1ml9x9ajmn3cl6kp8cm5ajm4lm5sywxfp35i6k3lv7yvambndrz4") (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-croissant-0.0.0-gamma (c (n "croissant") (v "0.0.0-gamma") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "croissant-macro") (r "^0.0.0-alpha") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "1msgra185hksxga34yg1wxvn2ris6vnb6v5s69njvhd1xlgrhv9p") (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-croissant-0.0.0-delta (c (n "croissant") (v "0.0.0-delta") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "croissant-macro") (r "^0.0.0-beta") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1yrjy4nn101fks6hrpziw1xx6rm1a7g68v4bm55vkdymfi3icib6") (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-croissant-0.0.1 (c (n "croissant") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "croissant-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1v0ak4d3i0gq34qv3jqf5lbi5fakrphf9r4il35cc5hahnx18ky4") (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-croissant-0.0.2 (c (n "croissant") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "croissant-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1dib5w97hn3jw6papbk1cghjpxwbghipcfq889kfs2xvgvl76qwq") (y #t) (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-croissant-0.0.3 (c (n "croissant") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "croissant-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "01a6fm6wksk081s93ywm7405q4598m3gjwvvh4mvklbwmpwib6rf") (s 2) (e (quote (("async" "dep:tokio"))))))

