(define-module (crates-io cr oi croissant-macro) #:use-module (crates-io))

(define-public crate-croissant-macro-0.0.0-alpha (c (n "croissant-macro") (v "0.0.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1hqigzjg4s7d0a66imcx2xric0nf1gsii9n2brik6nkljkbrf506")))

(define-public crate-croissant-macro-0.0.0-beta (c (n "croissant-macro") (v "0.0.0-beta") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0lzdszrqk3gr15pbymdpgi94k8znmcvvs2lway6939mwbwvz1m8l")))

(define-public crate-croissant-macro-0.0.1 (c (n "croissant-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "17f8rmlqh40x94ymxi17wd2fr13qnl2im7rfilv6anyp8vca1qa1")))

(define-public crate-croissant-macro-0.0.2 (c (n "croissant-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1bkrr3czchl7rjk8q64g63sc6ansvvdh8lzrqxycsplwnqmqks21")))

