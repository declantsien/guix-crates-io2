(define-module (crates-io cr um crumb) #:use-module (crates-io))

(define-public crate-crumb-0.1.0 (c (n "crumb") (v "0.1.0") (h "1861vgp5xbh93ah2z8n8qmp9yq12vbpsnqmqn8wh6sipfydhg3zc")))

(define-public crate-crumb-0.2.0 (c (n "crumb") (v "0.2.0") (h "162wc3bdnflkhp4v4ysy4mnis3a698x1zxgimw3kyy85c2dab6k6")))

(define-public crate-crumb-0.2.1 (c (n "crumb") (v "0.2.1") (h "040vvkmf3rx052mh5yq9wbwvjpzygyhjsx177j5pgysp3dbyg61h")))

