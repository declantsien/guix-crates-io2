(define-module (crates-io cr um crumbles) #:use-module (crates-io))

(define-public crate-crumbles-0.1.0 (c (n "crumbles") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rangemap") (r "^1") (d #t) (k 0)))) (h "0xbl5nsqvcicymnz2d72crb4n6w45lvkz21k6l4kmcj1p2zzm7a7")))

(define-public crate-crumbles-0.1.1 (c (n "crumbles") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rangemap") (r "^1") (d #t) (k 0)))) (h "1aya741dwnnzwf5prlqjg84mygnfgqkxmngjy8yv83p1c7bs7gcz")))

(define-public crate-crumbles-0.1.2 (c (n "crumbles") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rangemap") (r "^1") (d #t) (k 0)))) (h "01p7ba02ayknnnp6jw15zhljv5djvp485cxifb1arfgmbv2xrxb3")))

(define-public crate-crumbles-0.1.3 (c (n "crumbles") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rangemap") (r "^1") (d #t) (k 0)))) (h "1lmmnb7lrbk8rp926wp97jz8rk2v9zpx6bi8ha2m1ajx5c6ym7xy")))

(define-public crate-crumbles-0.2.0 (c (n "crumbles") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rangemap") (r "^1") (d #t) (k 0)))) (h "13bjr1g4pvg91da48zf47klg1vszbqy0axkqj00x0qmv2v6l1svr")))

(define-public crate-crumbles-0.3.0 (c (n "crumbles") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rangemap") (r "^1") (d #t) (k 0)))) (h "0dln0f4smlnmzdnm528gkmf3rdqx19ayx3s6nb5qpy5psrh5zrx8")))

