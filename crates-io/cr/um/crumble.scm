(define-module (crates-io cr um crumble) #:use-module (crates-io))

(define-public crate-crumble-0.9.0 (c (n "crumble") (v "0.9.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14j09ynjal43ny08x936wa774i5qzyxjaibbklsa7pq5gky6jkzy")))

(define-public crate-crumble-0.10.0 (c (n "crumble") (v "0.10.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0314py4xag0cikvv005n96xjmi8yk9rkzll7qjlsr08ppbqka2a8")))

(define-public crate-crumble-0.10.1 (c (n "crumble") (v "0.10.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05mf4ndxnll4qfysrrqqarjb6qrlyi6kwax73c9n9p7z58wf1dqy")))

(define-public crate-crumble-0.10.2 (c (n "crumble") (v "0.10.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0z6rn1g7rsqr47k7rfmqpgzw077nm74c2dk8q5bby6mxdiv6sij2")))

