(define-module (crates-io cr um crumble_ci) #:use-module (crates-io))

(define-public crate-crumble_ci-0.1.0 (c (n "crumble_ci") (v "0.1.0") (d (list (d (n "colored") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "04yy9ya0vvqan0rw7xba0f37253prr3niabc1w85x3ppd8ldg6p3")))

