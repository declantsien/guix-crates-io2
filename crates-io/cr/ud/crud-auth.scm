(define-module (crates-io cr ud crud-auth) #:use-module (crates-io))

(define-public crate-crud-auth-0.1.0 (c (n "crud-auth") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("cargo" "env" "unicode"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "miette") (r "^5.1.1") (f (quote ("fancy"))) (d #t) (k 0)))) (h "1ymy89kcrzvq4901pdkal236ihamfq3kbbas4ml6rfyibjy7bggr")))

(define-public crate-crud-auth-0.1.1 (c (n "crud-auth") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("cargo" "env" "unicode"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "miette") (r "^5.7") (f (quote ("fancy"))) (d #t) (k 0)))) (h "0dnvfsn9ylhz9lpg502h1cacay64arjzkldfsq8n793yn6nqbc7m")))

(define-public crate-crud-auth-0.1.2 (c (n "crud-auth") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("cargo" "env" "unicode"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "miette") (r "^5.9") (f (quote ("fancy"))) (d #t) (k 0)))) (h "1kilf7ymsjmk5hrvakfi5z35nl2cidqcab082gcn5qhx595vc9y5")))

(define-public crate-crud-auth-0.1.4 (c (n "crud-auth") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("cargo" "env" "unicode"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "miette") (r "^5.9") (f (quote ("fancy"))) (d #t) (k 0)))) (h "1aqa4dxkkfqrzz6mnhpjgy8n6y3q295g9zkmv855lb565km8gil7")))

(define-public crate-crud-auth-0.1.5 (c (n "crud-auth") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("cargo" "env" "unicode"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "miette") (r "^5.9") (f (quote ("fancy"))) (d #t) (k 0)))) (h "19ligjs6sn519g5xwpw1f55v8p8c7kh3cpp6kl890397d0hl78zw")))

(define-public crate-crud-auth-0.1.6 (c (n "crud-auth") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("color" "suggestions" "env" "unicode" "wrap_help" "cargo"))) (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)))) (h "1a4f5s8k01zv6slay1vrb7si9p39il7dzxf54lh33sxcmn17aplk")))

