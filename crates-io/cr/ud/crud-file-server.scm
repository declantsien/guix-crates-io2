(define-module (crates-io cr ud crud-file-server) #:use-module (crates-io))

(define-public crate-crud-file-server-0.1.0 (c (n "crud-file-server") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 2)) (d (n "tide") (r "^0.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0aksgl2mf9nhsp3rgqfc35nckn8dqwk7hnmh4bkznq5zf16p6qmz")))

(define-public crate-crud-file-server-0.2.0 (c (n "crud-file-server") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 2)) (d (n "tide") (r "^0.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "11bgs31r02dlzx03g4xq3va1d32kbx7syy31gwg7c1csygh893zi")))

(define-public crate-crud-file-server-0.3.0 (c (n "crud-file-server") (v "0.3.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r "^2") (d #t) (k 2)) (d (n "tide") (r "^0.16") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ayqvdfb6aj2sbkbnddcjv8648hzr2nrs461zcd4pcm31y5h51ln")))

