(define-module (crates-io cr ud crude-profiler) #:use-module (crates-io))

(define-public crate-crude-profiler-0.1.0 (c (n "crude-profiler") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "08112xp1f88qcpz4bfgdz3z9gybhprm0cqx06k2b2p4mm9mmbdc2")))

(define-public crate-crude-profiler-0.1.1 (c (n "crude-profiler") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "14ih6m00aax32sl0kl711di42a3d1f7gdja7z5gw65a9jwhg26x8")))

(define-public crate-crude-profiler-0.1.2 (c (n "crude-profiler") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "029sydvkfq8w49376kixjbn6s5dy0336xyy39h2fl62d67cyk24x")))

(define-public crate-crude-profiler-0.1.3 (c (n "crude-profiler") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "008q7mpxinngsj1cvvbas9qnq3aid00j57fr5l44qpavr2651xfx")))

(define-public crate-crude-profiler-0.1.4 (c (n "crude-profiler") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "03k5kraq4pla5sacffrxbljzg958pbw2bk8vv8c667h1cb0irj21")))

(define-public crate-crude-profiler-0.1.5 (c (n "crude-profiler") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "13ziqxkcq5glfj3ifk2lxvcmrdnvf024lmcf2z87pdf1wr9wln3j")))

(define-public crate-crude-profiler-0.1.6 (c (n "crude-profiler") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0vwrdml3nzkik1zdlsndn0hfp1aw5x06ds339mpzhr0vyi5cif58")))

(define-public crate-crude-profiler-0.1.7 (c (n "crude-profiler") (v "0.1.7") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1kcy2hpi8fb6y3z3p4qyns0jrq93y0644ddnqklardhnqw5532ck")))

