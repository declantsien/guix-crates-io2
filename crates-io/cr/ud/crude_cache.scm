(define-module (crates-io cr ud crude_cache) #:use-module (crates-io))

(define-public crate-crude_cache-0.1.0 (c (n "crude_cache") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "timeit") (r "^0.1.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "0wjcxc2i6knj9p5p87zwj9gwnlhv8392p6nxg13bsh1a3xccn20z")))

(define-public crate-crude_cache-0.1.1 (c (n "crude_cache") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "timeit") (r "^0.1.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "rt" "macros"))) (d #t) (k 0)))) (h "03adzgpb6mms8kj8lj9nv7r22kbfh7l8kkjfc3b1yxmjk0pxkpl6")))

