(define-module (crates-io cr ud crudite) #:use-module (crates-io))

(define-public crate-crudite-0.1.0 (c (n "crudite") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "im") (r "^13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (d #t) (k 0)))) (h "0ll4glx3f6h11205znqw45i1xmnyqyn4kj43bfd9kqbdwy7acin3")))

