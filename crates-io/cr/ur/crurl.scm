(define-module (crates-io cr ur crurl) #:use-module (crates-io))

(define-public crate-crurl-0.1.0 (c (n "crurl") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.21") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1kpsq201qjmha63vwq0fq7hj4p5d2avshx45ysyfww0lmylcvzpw") (y #t)))

(define-public crate-crurl-0.2.0 (c (n "crurl") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08awqxy7mrf10diaf1z3jyrd05xnavpg4if0q0pv25y906d4ybjz") (y #t)))

