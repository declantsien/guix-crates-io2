(define-module (crates-io cr at crate-version-experiment) #:use-module (crates-io))

(define-public crate-crate-version-experiment-0.1.0 (c (n "crate-version-experiment") (v "0.1.0") (h "16xmp1441id4a7md1fnh5s53d7x3klhhai80i2969jsraxlkxfqj")))

(define-public crate-crate-version-experiment-0.1.0-experiment-version (c (n "crate-version-experiment") (v "0.1.0-experiment-version") (h "0vf44w1a29c4fn5gs8xkss6v2fd45w5nra3f24s4ajf4vygyrykg")))

(define-public crate-crate-version-experiment-0.3.5 (c (n "crate-version-experiment") (v "0.3.5") (h "0qmg0mn8q4akhbvxfwyk1yl51mv649i1v3287zwwzv786wzv7spf")))

