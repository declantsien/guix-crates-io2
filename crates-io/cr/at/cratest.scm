(define-module (crates-io cr at cratest) #:use-module (crates-io))

(define-public crate-cratest-0.0.0 (c (n "cratest") (v "0.0.0") (d (list (d (n "crates_io_api") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rustwide") (r "^0.11") (d #t) (k 0)))) (h "1crd9hyjfy675gm50wmkd8dcrr2h27iaagyaa1rpyvfjkmd4il6x")))

