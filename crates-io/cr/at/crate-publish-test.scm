(define-module (crates-io cr at crate-publish-test) #:use-module (crates-io))

(define-public crate-crate-publish-test-0.1.0 (c (n "crate-publish-test") (v "0.1.0") (h "16m6lnajhaim8g5k5lf45hjxz4y6jbjcdq7fisd4v9738wzmkzlv") (y #t)))

(define-public crate-crate-publish-test-0.1.1 (c (n "crate-publish-test") (v "0.1.1") (h "141qw4838hk0x9j8wbyhfsbq4hzcddxk28gmfxn1zvaz2mrcn22c")))

