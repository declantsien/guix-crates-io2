(define-module (crates-io cr at crates_search) #:use-module (crates-io))

(define-public crate-crates_search-0.1.0 (c (n "crates_search") (v "0.1.0") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.3") (d #t) (k 0)))) (h "0wqik110dfdn6r2jf91c7c452h8pxs31s41kvp9kync3anqjh8ya")))

(define-public crate-crates_search-0.1.1 (c (n "crates_search") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0559v2lmaz6cc9x4kw30zp6b4x96qxaks0s2f9azdh207gack7mh")))

(define-public crate-crates_search-0.1.2 (c (n "crates_search") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1yhap85mk3d6is1swaja9bc4rpda5dzmc7nflma1n8nwzhxf57wr")))

