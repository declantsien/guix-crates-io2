(define-module (crates-io cr at crates-io-test) #:use-module (crates-io))

(define-public crate-crates-io-test-0.1.0 (c (n "crates-io-test") (v "0.1.0") (h "0skwsazr6pm9qay8w9kgmlv6nvgcm2cv11jzcpy5n19rs9q8sb65") (y #t)))

(define-public crate-crates-io-test-0.1.1 (c (n "crates-io-test") (v "0.1.1") (h "0w1cl7x9avkkd0xm6f7r2d9mjl8vlcx25b8l8l3c6xxizvfqci7a") (y #t)))

(define-public crate-crates-io-test-0.1.2 (c (n "crates-io-test") (v "0.1.2") (h "0vh590rxx83sbw1yr3bsq0g2h4xxdrgihc7zx67yravhyh8g701x") (y #t)))

(define-public crate-crates-io-test-0.1.3 (c (n "crates-io-test") (v "0.1.3") (h "1ni284c13b9cdk2dmmhjcqvqzh5biddwrl5105h4p0vpr91y8d0z") (y #t)))

(define-public crate-crates-io-test-0.1.4 (c (n "crates-io-test") (v "0.1.4") (h "1bjxha7r4p8ifd07z7g4ajvyxxv9l4a08w1w0wcwlz1yzk8408nm") (y #t)))

