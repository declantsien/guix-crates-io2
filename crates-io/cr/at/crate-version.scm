(define-module (crates-io cr at crate-version) #:use-module (crates-io))

(define-public crate-crate-version-0.1.0 (c (n "crate-version") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0hhgys7gg1mshg8kdim5npkylacfy3f37gbhaqznamsj3qxlkdsa")))

