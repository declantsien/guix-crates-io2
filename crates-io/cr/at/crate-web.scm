(define-module (crates-io cr at crate-web) #:use-module (crates-io))

(define-public crate-crate-web-0.1.0 (c (n "crate-web") (v "0.1.0") (d (list (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0yda74ml031ldndiyqi4328c325ia4zqrfa1rfnpjphjgd54h89m")))

(define-public crate-crate-web-0.1.1 (c (n "crate-web") (v "0.1.1") (d (list (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0v3vlc2yb509xlz42mpdghwdxi43d40ld28ywiq6h7bnxmf7dvvp")))

