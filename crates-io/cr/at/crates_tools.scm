(define-module (crates-io cr at crates_tools) #:use-module (crates-io))

(define-public crate-crates_tools-0.1.0 (c (n "crates_tools") (v "0.1.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)) (d (n "ureq") (r "~2.4") (o #t) (d #t) (k 0)))) (h "0ajna3lmrrgjwwsvbcpvcqjli1h43i34pyxgmwk2wpgicr4g9s2i") (f (quote (("use_alloc") ("no_std") ("network" "ureq") ("full" "enabled") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.2.0 (c (n "crates_tools") (v "0.2.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)) (d (n "ureq") (r "~2.4") (o #t) (d #t) (k 0)))) (h "18b70lpc3c2bw8ckijh5m925jig2frjyp4lbzcj727bg3ldwr67q") (f (quote (("use_alloc") ("no_std") ("network" "ureq") ("full" "enabled") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.3.0 (c (n "crates_tools") (v "0.3.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)) (d (n "ureq") (r "~2.9") (o #t) (d #t) (k 0)))) (h "1jdv5ic7ad4wa0f4fbl9m26qdchckayka5wjj2z1s1as6s264mv2") (f (quote (("network" "ureq") ("full" "enabled" "network") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.4.0 (c (n "crates_tools") (v "0.4.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)) (d (n "ureq") (r "~2.9") (o #t) (d #t) (k 0)))) (h "1ifwdc131cs2b5x8fg5109q5lvfmm8rkbbc5jrhdq7rlawyih8gy") (f (quote (("network" "ureq") ("full" "enabled" "network") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.5.0 (c (n "crates_tools") (v "0.5.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)) (d (n "ureq") (r "~2.9") (o #t) (d #t) (k 0)))) (h "0ax7gr5byds8gljr6gb6hky9d65b24vzp9q0siz4kc31d0ivqacm") (f (quote (("network" "ureq") ("full" "enabled" "network") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.6.0 (c (n "crates_tools") (v "0.6.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)) (d (n "ureq") (r "~2.9") (o #t) (d #t) (k 0)))) (h "017m0bxl1zbqniv02lw18l9x4gvwxxjwhfykpxisdnnmcxw9mfck") (f (quote (("network" "ureq") ("full" "enabled" "network") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.7.0 (c (n "crates_tools") (v "0.7.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)) (d (n "ureq") (r "~2.9") (o #t) (d #t) (k 0)))) (h "00qd0rrbif6v0kqxmvc0z8w7qvqmrj7qzmgx0k4mg8yj05n4fqz9") (f (quote (("network" "ureq") ("full" "enabled" "network") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.8.0 (c (n "crates_tools") (v "0.8.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)) (d (n "ureq") (r "~2.9") (o #t) (d #t) (k 0)))) (h "0s4wrbixhg5y395h3irhyg7bz6ml3w9bd77hj1rqwji1jci69qnk") (f (quote (("network" "ureq") ("full" "enabled" "network") ("enabled" "network") ("default" "enabled"))))))

(define-public crate-crates_tools-0.9.0 (c (n "crates_tools") (v "0.9.0") (d (list (d (n "flate2") (r "~1.0") (d #t) (k 0)) (d (n "tar") (r "~0.4") (d #t) (k 0)) (d (n "test_tools") (r "~0.9.0") (d #t) (k 2)) (d (n "ureq") (r "~2.9") (o #t) (d #t) (k 0)))) (h "1hspjm7bimq5cb7z7m5h5hg5ff34zcbc931drh9qsqrab469i9wf") (f (quote (("network" "ureq") ("full" "enabled" "network") ("enabled" "network") ("default" "enabled"))))))

