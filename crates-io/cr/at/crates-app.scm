(define-module (crates-io cr at crates-app) #:use-module (crates-io))

(define-public crate-crates-app-0.1.0 (c (n "crates-app") (v "0.1.0") (h "1lag9yq4p9b69543af5lnlk4h5mjbj48igcjqx9dal0l5glrsi15")))

(define-public crate-crates-app-0.1.1 (c (n "crates-app") (v "0.1.1") (h "0nlwgmmygdp4q2wvn630x3vv05zmxay96xb3fqmnbnkrwd53pf96")))

