(define-module (crates-io cr at crates_jameszow) #:use-module (crates-io))

(define-public crate-crates_jameszow-0.1.0 (c (n "crates_jameszow") (v "0.1.0") (h "1gnhdzi71wm1c74zp5lr92b3xmnbgz8iwmdc8rpry187s4wzjk62") (y #t)))

(define-public crate-crates_jameszow-0.2.0 (c (n "crates_jameszow") (v "0.2.0") (h "1li5pc66a8y3nw5g725rwy386wbmagl61c3lsn99xda6m98wdp9j") (y #t)))

