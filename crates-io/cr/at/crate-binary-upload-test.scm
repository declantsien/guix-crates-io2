(define-module (crates-io cr at crate-binary-upload-test) #:use-module (crates-io))

(define-public crate-crate-binary-upload-test-0.1.0 (c (n "crate-binary-upload-test") (v "0.1.0") (h "0lp215naly6a07yn7b1dvsnmjn2cf9604s33wd6zj853sb54dnrd")))

(define-public crate-crate-binary-upload-test-0.1.1 (c (n "crate-binary-upload-test") (v "0.1.1") (h "02xbsp1w1pppnngp8gk2xddxsqraj5mx5r8b9v7ms552nlzi33ss")))

