(define-module (crates-io cr at crate-token) #:use-module (crates-io))

(define-public crate-crate-token-0.1.1 (c (n "crate-token") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.2.1") (d #t) (k 0)))) (h "1069kycjbbjbbyh98gwyp5bvljr7xmwvlxya2c6hajd9gk4wy3mh") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-crate-token-0.2.0 (c (n "crate-token") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.2.1") (d #t) (k 0)))) (h "1l6zimhn3k6mb0qgb0zk2f41khgalz4sf8zm23dr1fsmzagrzi58") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-crate-token-0.3.0 (c (n "crate-token") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.2.1") (d #t) (k 0)))) (h "1awngfacf4b5akaaqx1ilgpxia7nxihc8h91mab3l1rmr43l07nw") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-crate-token-0.3.1 (c (n "crate-token") (v "0.3.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.2.1") (d #t) (k 0)))) (h "1k4lfa8v5ygxk7847gc3gsqyz19q0vimcib39imjgxy6l55hpp2d") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-crate-token-0.4.0 (c (n "crate-token") (v "0.4.0") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.5.5") (d #t) (k 0)))) (h "1819b39mh94saa3zn374qzn9n0iric255kg6178p5mgjzqmxj6lz") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-crate-token-0.4.1 (c (n "crate-token") (v "0.4.1") (d (list (d (n "anchor-lang") (r ">=0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 0)) (d (n "vipers") (r "^1.5.7") (d #t) (k 0)))) (h "1l8k8hrp4mrbaj8hg8wyi15ix98mg9khspqag4a46kxjb7jd8q3p") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-crate-token-0.5.0 (c (n "crate-token") (v "0.5.0") (d (list (d (n "anchor-lang") (r "^0.22") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "1ww10gnnfaj5spcgvq5rk8z4qyrpk5f6ijv3pynzkr1jaj8pizds") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-crate-token-0.6.0 (c (n "crate-token") (v "0.6.0") (d (list (d (n "anchor-lang") (r "^0.24") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.2") (d #t) (k 0)) (d (n "vipers") (r "^2") (d #t) (k 0)))) (h "0x73m72pyn7x55axsfsrd3zb9a40grwfljk0c671mz66603j4l7l") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

