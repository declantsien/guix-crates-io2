(define-module (crates-io cr at craterfinder) #:use-module (crates-io))

(define-public crate-craterfinder-0.1.0 (c (n "craterfinder") (v "0.1.0") (d (list (d (n "ferris-says") (r "^0.2") (d #t) (k 0)))) (h "0c6qvnx93yvspxpflgg1mlrj639g309f5rjysg08lfc3b3f9lc2v")))

(define-public crate-craterfinder-0.1.1 (c (n "craterfinder") (v "0.1.1") (d (list (d (n "ferris-says") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17h6w4pbrhvhm2kzfic53k7i8spligfafghxq9kp9jazjdl57xpy")))

