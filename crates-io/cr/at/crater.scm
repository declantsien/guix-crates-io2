(define-module (crates-io cr at crater) #:use-module (crates-io))

(define-public crate-crater-0.1.0 (c (n "crater") (v "0.1.0") (h "1fqr00r8gilc1amr6fxbckd43sdld01qf1qy0yf0v66zdrz84a2f")))

(define-public crate-crater-0.1.1 (c (n "crater") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1sssnhsz209axcay327l3bfpmwlfwg1dx6acyyrs8v83q87jxca1") (f (quote (("stress_tests"))))))

