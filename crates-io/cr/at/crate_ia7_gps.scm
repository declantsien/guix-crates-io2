(define-module (crates-io cr at crate_ia7_gps) #:use-module (crates-io))

(define-public crate-crate_IA7_GPS-0.1.0 (c (n "crate_IA7_GPS") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "044g3abbwk7clva80j40bx1sg1qccfgvk9dafrf83wag0isdv7lm")))

(define-public crate-crate_IA7_GPS-0.2.0 (c (n "crate_IA7_GPS") (v "0.2.0") (d (list (d (n "rpi_embedded") (r "^0.0.5") (d #t) (k 0)))) (h "10lkd8i1cs9glyfgkj5qf3wpp88nxi26a8lf0ldnfrwpbd258znc")))

