(define-module (crates-io cr at cratic-prelude) #:use-module (crates-io))

(define-public crate-cratic-prelude-0.1.0 (c (n "cratic-prelude") (v "0.1.0") (d (list (d (n "macro_rules_attribute") (r "^0.1.2") (d #t) (k 0)))) (h "1r1rn693mx7bjyw2j2ahcz4isz6v5y4c5r2ji3bp6x070aqnprg9") (y #t)))

(define-public crate-cratic-prelude-0.1.1 (c (n "cratic-prelude") (v "0.1.1") (d (list (d (n "macro_rules_attribute") (r "^0.1.2") (d #t) (k 0)))) (h "1wmsaqb776lpgzfwjw300acycwxkrm7capfrm3igdjarvy65v18n") (y #t)))

