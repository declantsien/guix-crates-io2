(define-module (crates-io cr at crate_interface) #:use-module (crates-io))

(define-public crate-crate_interface-0.1.0 (c (n "crate_interface") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxwk07jdm4dzafj8rrq2dq6419khrhliwznyl169isjm7drbi1s")))

(define-public crate-crate_interface-0.1.1 (c (n "crate_interface") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a2gdb5ks64fvc95yy9l5q9475w6zp35vsx1a8dqhw9qg7dpjjym")))

