(define-module (crates-io cr at crate_crypto_internal_peerdas_polynomial) #:use-module (crates-io))

(define-public crate-crate_crypto_internal_peerdas_polynomial-0.1.0 (c (n "crate_crypto_internal_peerdas_polynomial") (v "0.1.0") (d (list (d (n "bls12_381") (r "^0.1.0") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0bk889zk2rv45bbl057hddn5rz0rc5w289z9z151pfab5dq3fla1") (r "1.66")))

(define-public crate-crate_crypto_internal_peerdas_polynomial-0.2.1 (c (n "crate_crypto_internal_peerdas_polynomial") (v "0.2.1") (d (list (d (n "bls12_381") (r "^0.2.0") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "05dfh7jidsf30xxd2vjjkiva0d4mzvd1ykvq028zzd4cxqvsgkvv") (r "1.66")))

(define-public crate-crate_crypto_internal_peerdas_polynomial-0.2.5 (c (n "crate_crypto_internal_peerdas_polynomial") (v "0.2.5") (d (list (d (n "bls12_381") (r "^0.2.5") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0qradhijc38a4igiqfzdwvdg51wlykfrsvl3w88zfka9i8dkjfpk") (r "1.66")))

(define-public crate-crate_crypto_internal_peerdas_polynomial-0.2.6 (c (n "crate_crypto_internal_peerdas_polynomial") (v "0.2.6") (d (list (d (n "bls12_381") (r "^0.2.6") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "13cwlyf6xq6pxr1q43z5vn214qmk51jc6aw67kg57fxgnqyyf9s5") (r "1.66")))

(define-public crate-crate_crypto_internal_peerdas_polynomial-0.3.0 (c (n "crate_crypto_internal_peerdas_polynomial") (v "0.3.0") (d (list (d (n "bls12_381") (r "^0.3.0") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "00ad0hmy29m0xg7dk3qira2a86mvmg1z0y7psqy8j14vqvcmwnga") (r "1.66")))

