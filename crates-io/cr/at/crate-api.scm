(define-module (crates-io cr at crate-api) #:use-module (crates-io))

(define-public crate-crate-api-0.0.1 (c (n "crate-api") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "fs_snapshot") (r "^0.1.2") (d #t) (k 2)) (d (n "rustdoc-json-types-fork") (r "^0.0.1") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "toml_edit") (r "^0.13") (d #t) (k 0)))) (h "0firnyl0s89smp5p9rmfv628f1c6v7mx7p81skb0nqh9083vpr7v") (r "1.57.0")))

