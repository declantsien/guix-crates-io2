(define-module (crates-io cr at crate-test-20220118) #:use-module (crates-io))

(define-public crate-crate-test-20220118-0.1.0 (c (n "crate-test-20220118") (v "0.1.0") (h "0hqwd0nmwj3b67qz6v57klwxhhpfn3xws8v01p1ahq5a9vqyp97w")))

(define-public crate-crate-test-20220118-0.2.0 (c (n "crate-test-20220118") (v "0.2.0") (h "1v1qjgf8yri0wfkaj2ximk76vbvq50agr8nab06af6x6zp79qgsp")))

