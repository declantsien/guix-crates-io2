(define-module (crates-io cr at crates-io-sketchbook) #:use-module (crates-io))

(define-public crate-crates-io-sketchbook-0.1.0 (c (n "crates-io-sketchbook") (v "0.1.0") (h "1p1wncdgzkcj8vbb4gcnwgr8qyzmfns9bqkn484mkqq40jj7ikxf")))

(define-public crate-crates-io-sketchbook-0.2.0 (c (n "crates-io-sketchbook") (v "0.2.0") (h "1hycxam8w74zbi8ynkpd2y5rkhpxv25xdw4ggldhwpbnibq43zma")))

(define-public crate-crates-io-sketchbook-0.2.1 (c (n "crates-io-sketchbook") (v "0.2.1") (h "035bw1xx9hfjik3gxbxnssas2cw4jx6670wff2shyxx3dw0539jr")))

