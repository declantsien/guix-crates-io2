(define-module (crates-io cr at crates-api) #:use-module (crates-io))

(define-public crate-crates-api-0.1.0 (c (n "crates-api") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.14") (d #t) (k 0)) (d (n "serde") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8.21") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.4") (d #t) (k 0)))) (h "0ra99hd0n2xh93qh18gkdgdqaaba55g4z6s388zn714hk5l775g8")))

