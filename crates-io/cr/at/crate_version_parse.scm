(define-module (crates-io cr at crate_version_parse) #:use-module (crates-io))

(define-public crate-crate_version_parse-0.1.0 (c (n "crate_version_parse") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "197fqs9hh7vrfwifh4d5vrlydpvi5215s0l2dcq5yqcq6azrn6ph")))

(define-public crate-crate_version_parse-0.1.1 (c (n "crate_version_parse") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1am1q2vdbj8dsw5bimykrgna2sasqygp5rkrd7lh8763lpcy0202")))

(define-public crate-crate_version_parse-0.2.0 (c (n "crate_version_parse") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "040s4ki0plbxiwy52hzrylxqnpyxdm5ly8a82lhz21fr9fcam5r5") (f (quote (("std") ("default" "std"))))))

