(define-module (crates-io cr at cratesio-dbdump-csvtab) #:use-module (crates-io))

(define-public crate-cratesio-dbdump-csvtab-0.1.0 (c (n "cratesio-dbdump-csvtab") (v "0.1.0") (d (list (d (n "cached-path") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.1") (f (quote ("bundled" "csvtab"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0vkbdyg7z6bxdgj99kpm1zf3y5adznn9aq5rcjmb4vky40z8whzj")))

(define-public crate-cratesio-dbdump-csvtab-0.2.0 (c (n "cratesio-dbdump-csvtab") (v "0.2.0") (d (list (d (n "cached-path") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.1") (f (quote ("bundled" "csvtab"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1l0ajcs2sb4a3d0mmlgjlj262aaqfdj31ig3afhg89p2c6yyrsf8")))

(define-public crate-cratesio-dbdump-csvtab-0.2.1 (c (n "cratesio-dbdump-csvtab") (v "0.2.1") (d (list (d (n "cached-path") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.1") (f (quote ("bundled" "csvtab"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0m9xdrh7mbvd6a7fg93il51rcin22lhmx29pxqxl14kkcif85gxj")))

(define-public crate-cratesio-dbdump-csvtab-0.2.2 (c (n "cratesio-dbdump-csvtab") (v "0.2.2") (d (list (d (n "cached-path") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.1") (f (quote ("bundled" "csvtab"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "18ky37j4x3jwz4caifk03kgf7gl1khn9w942aaqm44604qvfxwzx")))

