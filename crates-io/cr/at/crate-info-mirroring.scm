(define-module (crates-io cr at crate-info-mirroring) #:use-module (crates-io))

(define-public crate-crate-info-mirroring-0.1.0 (c (n "crate-info-mirroring") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2") (d #t) (k 0)) (d (n "g-k-crates-io-client") (r "^0.27") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "17njcasz3182l362bkbyblqn7rygm5gk3b2h5cl80ygz3favynqb")))

