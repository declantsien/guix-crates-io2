(define-module (crates-io cr at crates-query) #:use-module (crates-io))

(define-public crate-crates-query-0.1.0 (c (n "crates-query") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crates-index") (r "^2.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0c9nh1mnzzg8m2dsw6m2rhkyal75d3j69ggr0ijii4qik31fn1bk")))

(define-public crate-crates-query-0.1.1 (c (n "crates-query") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crates-index") (r "^2.3.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "09a0xn48dfwf8hand2cbci0rbcqhyvbln32jp4i307mjn0912mmq")))

(define-public crate-crates-query-0.1.2 (c (n "crates-query") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crates-index") (r "^2.3.0") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)))) (h "0hk0p751w6372g62qw0f4l567b6ddn9x6v9kk4q2q156wkss23k2")))

