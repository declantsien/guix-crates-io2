(define-module (crates-io cr at crate_that_i_break_sometimes_for_testing) #:use-module (crates-io))

(define-public crate-crate_that_i_break_sometimes_for_testing-0.0.0 (c (n "crate_that_i_break_sometimes_for_testing") (v "0.0.0") (h "07fmq1ibjjb8lwzwkrq2bn5yvmsgshvkwyjj936s3sa991589qci")))

(define-public crate-crate_that_i_break_sometimes_for_testing-0.0.1 (c (n "crate_that_i_break_sometimes_for_testing") (v "0.0.1") (h "1adaqmibywa6xwbvb42vkpngjr1pn9wrla2bkn8xmlyvn9bs27jk")))

(define-public crate-crate_that_i_break_sometimes_for_testing-0.1.0 (c (n "crate_that_i_break_sometimes_for_testing") (v "0.1.0") (h "0f865qaj5lb2z11dv5hxvck29snhy6v78d3qa1l3bi5vlz1wpdzn")))

(define-public crate-crate_that_i_break_sometimes_for_testing-0.1.1 (c (n "crate_that_i_break_sometimes_for_testing") (v "0.1.1") (h "02mm32w4hvqaa2zhp2a2cxs6szxs2cxcllk323k6cqi5gpanvfpx")))

(define-public crate-crate_that_i_break_sometimes_for_testing-0.1.2 (c (n "crate_that_i_break_sometimes_for_testing") (v "0.1.2") (h "1z1j6mdyn4lk5jd6gr6n4ynf97j58zj86hxffgbnirg2jalf1w67")))

