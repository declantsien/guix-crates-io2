(define-module (crates-io cr at crates_and_cargo) #:use-module (crates-io))

(define-public crate-crates_and_cargo-0.1.0 (c (n "crates_and_cargo") (v "0.1.0") (h "0x3gp9hig3x92r5kqsi4ln4sc1pcv32kwfvf5gn380dckcrz9r3p")))

(define-public crate-crates_and_cargo-0.1.1 (c (n "crates_and_cargo") (v "0.1.1") (h "0gy072jch343znwx1ysv093w2rz6s41qplqfn1pmq3iwsmkswigj")))

