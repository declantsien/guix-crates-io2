(define-module (crates-io cr at crates-io-semverver) #:use-module (crates-io))

(define-public crate-crates-io-semverver-0.22.0 (c (n "crates-io-semverver") (v "0.22.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1j6kg6jmlf1yy29qzn7c9mkxjyl4cy4dqgad5ciqqdyfskz6nymq")))

