(define-module (crates-io cr at crates-publish-minimal) #:use-module (crates-io))

(define-public crate-crates-publish-minimal-0.1.0 (c (n "crates-publish-minimal") (v "0.1.0") (h "00lzg3cinxmn9x9s4nw81k2vn20d4s96c18xrk0agyibhpcf5k0r")))

(define-public crate-crates-publish-minimal-0.1.1 (c (n "crates-publish-minimal") (v "0.1.1") (h "1mk1hpvda9w60hggiw0229zq90hq0lgysa90cdcjsgf7vgi0r9ch")))

