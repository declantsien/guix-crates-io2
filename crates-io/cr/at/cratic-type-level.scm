(define-module (crates-io cr at cratic-type-level) #:use-module (crates-io))

(define-public crate-cratic-type-level-0.1.0 (c (n "cratic-type-level") (v "0.1.0") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 0)) (d (n "cratic-debug-tools") (r "^0.1.1") (d #t) (k 0)) (d (n "cratic-prelude") (r "^0.1.1") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.1.2") (d #t) (k 0)))) (h "0q6vg4l76b7fbpl72xllnsinimnb5sravc0870lrgjn339v3xali") (f (quote (("asm_tests")))) (y #t)))

(define-public crate-cratic-type-level-0.1.1 (c (n "cratic-type-level") (v "0.1.1") (d (list (d (n "assert-type-eq") (r "^0.1.0") (d #t) (k 0)) (d (n "cratic-debug-tools") (r "^0.1.1") (d #t) (k 0)) (d (n "cratic-prelude") (r "^0.1.1") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.1.2") (d #t) (k 0)))) (h "1yrx0lgivmwqdrsd2pf8vrf3kcph65myz7vmlswbj2ggdxv4mf6z") (f (quote (("asm_tests")))) (y #t)))

