(define-module (crates-io cr at crate-git-revision) #:use-module (crates-io))

(define-public crate-crate-git-revision-0.0.1 (c (n "crate-git-revision") (v "0.0.1") (h "1p6bnb2rlxamypbbsd8scfcl4dgiqrvaa62ml73p6w6lkhzgv4p9") (y #t)))

(define-public crate-crate-git-revision-0.0.2 (c (n "crate-git-revision") (v "0.0.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0c5lf46kx6dnhqdks21rbr1bli7r92kdpbmgspz2qggy47fjdgq7") (r "1.63")))

(define-public crate-crate-git-revision-0.0.3 (c (n "crate-git-revision") (v "0.0.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0r3mcm88nllfc2nzrzjrd67rdd751ys1s8wjcam0h1glqslaikkq") (r "1.65")))

(define-public crate-crate-git-revision-0.0.4 (c (n "crate-git-revision") (v "0.0.4") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1j38qlngp15d4kszn3wfn9003i2rka9w03sg1qxq7rx46vqsx67r") (r "1.65")))

(define-public crate-crate-git-revision-0.0.5 (c (n "crate-git-revision") (v "0.0.5") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0067dsm71n276qxgpxsay5xjcgykxpa10vqqjflw36dhj7m45qp5") (r "1.66")))

(define-public crate-crate-git-revision-0.0.6 (c (n "crate-git-revision") (v "0.0.6") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "165grcpn3hd1715y8l9lrc0pkmrm379mwxs16kvx47nk8cgvy8f5") (r "1.66")))

