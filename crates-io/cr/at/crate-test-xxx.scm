(define-module (crates-io cr at crate-test-xxx) #:use-module (crates-io))

(define-public crate-crate-test-xxx-0.1.0 (c (n "crate-test-xxx") (v "0.1.0") (h "05g9p70ddzq5kvcd0gl0wgxgsiv6g6nnxzhav1imm7nwlfjnhhjx") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.1 (c (n "crate-test-xxx") (v "0.1.1") (h "0xv6dwl4ikis8y0yq3m130rd9h737mfq176i2v10wr16wryhql2y") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.2 (c (n "crate-test-xxx") (v "0.1.2") (h "0lki4vqz1rv6cs874sz93yya2icvxk8rqrpwprickg57dh67xv04") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.3 (c (n "crate-test-xxx") (v "0.1.3") (h "1jg98dfzqsvhd1n5b0dji1annzjz9hqib9ld2d29wmjn0ffjvqb9") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.4 (c (n "crate-test-xxx") (v "0.1.4") (h "0z6z34n88cmbdlbj0a7kph6zhws7y8sg7r2h6v27s76kxyikq9dr") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.5 (c (n "crate-test-xxx") (v "0.1.5") (h "12sphr536abwp6pfx3q6hawbiifrns87fjd7ax138czqcvmsq695") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.6 (c (n "crate-test-xxx") (v "0.1.6") (h "05svvmd4i018943jamx3hgvnxhhirwx9pznlkdfcbhl53p07gbca") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.7 (c (n "crate-test-xxx") (v "0.1.7") (h "09absh1vdkhi68bvyjc8y61mahp3da4kbvg7ba6xvjnfhq3sib29") (f (quote (("mod2") ("mod1"))))))

(define-public crate-crate-test-xxx-0.1.8 (c (n "crate-test-xxx") (v "0.1.8") (h "1g7f7cy87z0i03h13wirmhx0536zr33b76nzcjmd512aijqvh3q4") (f (quote (("mod2") ("mod1") ("full" "mod1" "mod2"))))))

(define-public crate-crate-test-xxx-0.1.9 (c (n "crate-test-xxx") (v "0.1.9") (h "1z26v631yln6ncn062n05ma2aa5fq43hjiy1vkqnn1vijl4cvkyr") (f (quote (("mod2") ("mod1") ("full" "mod1" "mod2"))))))

(define-public crate-crate-test-xxx-0.1.10 (c (n "crate-test-xxx") (v "0.1.10") (h "1vmmn079yf7a3cap2xqjlw74w1s408izh2nii85jxqarlxigyqc6") (f (quote (("mod2") ("mod1") ("full" "mod1" "mod2"))))))

(define-public crate-crate-test-xxx-0.1.11 (c (n "crate-test-xxx") (v "0.1.11") (h "16jpazi788bwc3z70dq1zz8kydj1d8dvag1097zjyas62rf72l83") (f (quote (("mod2") ("mod1") ("full" "mod1" "mod2"))))))

(define-public crate-crate-test-xxx-0.1.12 (c (n "crate-test-xxx") (v "0.1.12") (h "1z8v32h2clns0xmdqn9xqc6q3j872idsx9855iv4rdizg3c1c88g") (f (quote (("mod2") ("mod1") ("full" "mod1" "mod2"))))))

(define-public crate-crate-test-xxx-0.1.13 (c (n "crate-test-xxx") (v "0.1.13") (h "0g7vhqrkxk5sxzf2b83n8scjvri3c9aq81kc6jcnq16ahsvkzn54") (f (quote (("mod2") ("mod1") ("full" "mod1" "mod2"))))))

