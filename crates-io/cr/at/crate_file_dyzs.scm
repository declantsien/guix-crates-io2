(define-module (crates-io cr at crate_file_dyzs) #:use-module (crates-io))

(define-public crate-crate_file_dyzs-0.1.0 (c (n "crate_file_dyzs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crate_usb_dyzs") (r "^0.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00cj1p3vxhd74s08haa41wsia1v3n1akyxbpxmpy2439slvb8iy5")))

(define-public crate-crate_file_dyzs-0.1.1 (c (n "crate_file_dyzs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crate_usb_dyzs") (r "^0.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0slnyydry2wv4qjaxmij8mm8lbf19n0flqn41hv2gss7a9ala9vk")))

(define-public crate-crate_file_dyzs-0.1.2 (c (n "crate_file_dyzs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crate_usb_dyzs") (r "^0.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zlr33giz2xcbvzxpklz2mq21i8b6409nm6w3d0qhnszmd2frm3v")))

(define-public crate-crate_file_dyzs-0.1.3 (c (n "crate_file_dyzs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crate_usb_dyzs") (r "^0.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "106lwzqd0nxl3l895lxjzrjzlyiyd4dnhbjiqsgkb3iapbp0jvic")))

(define-public crate-crate_file_dyzs-0.1.4 (c (n "crate_file_dyzs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crate_usb_dyzs") (r "^0.1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i3sh704c9kymv916p7jrawr2pjdbqzs2lg461xbvfihzi0ybm68")))

