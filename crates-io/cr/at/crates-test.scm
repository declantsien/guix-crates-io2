(define-module (crates-io cr at crates-test) #:use-module (crates-io))

(define-public crate-crates-test-0.0.0 (c (n "crates-test") (v "0.0.0") (h "0i30dkb55ql59zzrrvcy12dfk7n1dhwkrbqv2kw4xwi8fm56q5bx")))

(define-public crate-crates-test-0.0.1 (c (n "crates-test") (v "0.0.1") (h "1hgmniss5445bq6iv20642v62vlwgaix3cfyr3gqvyd2afdg0ir4")))

(define-public crate-crates-test-0.0.3 (c (n "crates-test") (v "0.0.3") (h "06daxqlvnzc23znpzsgx7dksdhdjr2sbv3c639j0qvfj9g7lis40")))

(define-public crate-crates-test-0.0.2 (c (n "crates-test") (v "0.0.2") (h "02axivjkn2i2gpzn00gn06kyvmmg80p74mwz71dp344kz94rfnm8")))

(define-public crate-crates-test-0.0.2-alpha.0 (c (n "crates-test") (v "0.0.2-alpha.0") (h "09l2r949ndl0j64yikyd11gyl1sh5fy4170zhdg0r3bdr9xzdvjj")))

(define-public crate-crates-test-0.0.4 (c (n "crates-test") (v "0.0.4") (h "1v6s6il3d4f41b3w926mmfl1pjm6wg7yhx37m1lb5znmnpskkwa3")))

