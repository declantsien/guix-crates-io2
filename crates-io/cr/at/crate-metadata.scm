(define-module (crates-io cr at crate-metadata) #:use-module (crates-io))

(define-public crate-crate-metadata-0.1.0 (c (n "crate-metadata") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "147i8qrbxiq75s3lqpa5bp8ra3c568rpvyl2kyjwnhg37mwykxkb")))

(define-public crate-crate-metadata-0.1.1 (c (n "crate-metadata") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09j1n62jz89595887j04wnr4zyl3xgkyrff8whwvmlax9mxmg95n")))

