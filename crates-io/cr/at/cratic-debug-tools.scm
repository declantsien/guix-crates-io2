(define-module (crates-io cr at cratic-debug-tools) #:use-module (crates-io))

(define-public crate-cratic-debug-tools-0.1.0 (c (n "cratic-debug-tools") (v "0.1.0") (d (list (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (k 0)))) (h "0amg28dsxf1ma12vkyn364wxz29lydq7cisi2kg78d8pfsajswhj") (y #t)))

(define-public crate-cratic-debug-tools-0.1.1 (c (n "cratic-debug-tools") (v "0.1.1") (d (list (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (k 0)))) (h "0dkr22hpakxwf1knvv5dl7fkgcggrc1pw14cm81avvbh1hraadf1") (y #t)))

