(define-module (crates-io cr at cratesiover) #:use-module (crates-io))

(define-public crate-cratesiover-1.0.0 (c (n "cratesiover") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0knxzry08kxjnm63ml16305i6w9igqxq79njj2w3j9jac7l06sh9")))

(define-public crate-cratesiover-2.0.0 (c (n "cratesiover") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "13flky5gb5fag80a8z3ik1zhz2m17hnrq17f5pi91fdaqn01vr26")))

(define-public crate-cratesiover-2.1.0 (c (n "cratesiover") (v "2.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0906cvyl2k8782h4l82q2k1ssgw49rlwcq1ifhdylz567k98y7vz")))

(define-public crate-cratesiover-2.2.0 (c (n "cratesiover") (v "2.2.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "06zjmdqylhmanask36r6b0k8ql1wk8v5azxlq6m9ddcca47k1l26")))

(define-public crate-cratesiover-2.2.1 (c (n "cratesiover") (v "2.2.1") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "linefeed") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "14g7c8fyhry3b2kgjw8b5cg3zcqrfa208ak07bwxqpj8dmjnzvvb")))

