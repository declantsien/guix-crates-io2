(define-module (crates-io cr at crates-io) #:use-module (crates-io))

(define-public crate-crates-io-0.1.0 (c (n "crates-io") (v "0.1.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ll3z800p9wfhgcxvnf7gq36539804sb6ribnvf2xi1zxaqn67zm")))

(define-public crate-crates-io-0.2.0 (c (n "crates-io") (v "0.2.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "071qy5j9bqx7w9ld7ivdi671jd23nz1ygzjg847701w5q51snx3g")))

(define-public crate-crates-io-0.3.0 (c (n "crates-io") (v "0.3.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "1ipzh9hjzgspsifacz2lx8vv5sa72w2kj9m171k85b1az0sq74rk")))

(define-public crate-crates-io-0.4.0 (c (n "crates-io") (v "0.4.0") (d (list (d (n "curl") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0kk6abp1qbpv44hkq1yjp7xgpzjzafs83i1l26ycr0aph1gbwig9")))

(define-public crate-crates-io-0.5.0 (c (n "crates-io") (v "0.5.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0b4n18mjagrwkrkc89yvy8nvx3hq1ygp62a08lkab225az5d4f8p")))

(define-public crate-crates-io-0.6.0 (c (n "crates-io") (v "0.6.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0hqrk8wqzq177b2bjc6bzlz4fl6s0alvhrsvawmx924ffc3jz60l")))

(define-public crate-crates-io-0.7.0 (c (n "crates-io") (v "0.7.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1jfid2xc68nzj6hsrnlxjaf3g2siy934b1hcd9kw9dkvfnmdbfxd")))

(define-public crate-crates-io-0.8.0 (c (n "crates-io") (v "0.8.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0jj96hm065vigg23wjw9iqq05p3c2cl4qqsfy76w0162p0d28y4y")))

(define-public crate-crates-io-0.9.0 (c (n "crates-io") (v "0.9.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0pwa0hda8zi5051xqrgzy4zflpdmkg9p4x4ych2ff3gm2ay0h7il")))

(define-public crate-crates-io-0.10.0 (c (n "crates-io") (v "0.10.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1hn1xab3p57cqvgg6gnqr9jmfyjcnsr0rrdv7gbv4lc4v5qfzf44")))

(define-public crate-crates-io-0.11.0 (c (n "crates-io") (v "0.11.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0l57383mi9cwbkx8ymx6k46xjbdrxhn9mbm58fphyd9r0ab1zzqh")))

(define-public crate-crates-io-0.12.0 (c (n "crates-io") (v "0.12.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0wqmd4yhpb6373dj5v45czrgn5jsqyvij02flwhbsa4162jm98yd")))

(define-public crate-crates-io-0.13.0 (c (n "crates-io") (v "0.13.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0lsxm8sz30pwlaw2n1x98ln9v4sqh3hxn9ijs28gaksk89a4qrvg")))

(define-public crate-crates-io-0.14.0 (c (n "crates-io") (v "0.14.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0pgrxzik93pqwwv4ys5m1c4afvl6ifki34hk10zfkvlykg4lxwgi")))

(define-public crate-crates-io-0.15.0 (c (n "crates-io") (v "0.15.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "02dx93gr0c2sl6y5bkxh5ry5mjj0qaqk99a38fsf0n52cznl55xn")))

(define-public crate-crates-io-0.16.0 (c (n "crates-io") (v "0.16.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "073fhpdpvmrb1wkijlsafns5z5j0x2bb9djg4kiv339lylz70y2z")))

(define-public crate-crates-io-0.17.0 (c (n "crates-io") (v "0.17.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1463krnyradja6h1wx6768blwb68cilm2s4265a71z6k105yd93c")))

(define-public crate-crates-io-0.18.0 (c (n "crates-io") (v "0.18.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0h5d26ys7dnvqmp442g41k0dxww9qsinhd8bdq8rhqdvbshx2r3s")))

(define-public crate-crates-io-0.19.0 (c (n "crates-io") (v "0.19.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1xg2qb41yp9cjnrcy010h8n7y10nwp368qgd4f2xk7svpxssj6p5")))

(define-public crate-crates-io-0.20.0 (c (n "crates-io") (v "0.20.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "127as4778r4vigr1vmg3vk1dizmz8g96982h2rs0s76n7g16lypy")))

(define-public crate-crates-io-0.21.0 (c (n "crates-io") (v "0.21.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0k36p0k1yzk1dbzz9fbanvjxl00h6868k9p71ibwr7fd7rf0i0mg")))

(define-public crate-crates-io-0.22.0 (c (n "crates-io") (v "0.22.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0sa5w8bj3wvn5jr2zni6av8hi8yly1aqar2bv619s478yp1ih409")))

(define-public crate-crates-io-0.23.0 (c (n "crates-io") (v "0.23.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0d344j0rjc70lp6ahha7da761pglvjmff80rqgxj63sz32dhk5kf")))

(define-public crate-crates-io-0.24.0 (c (n "crates-io") (v "0.24.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1zi6zl2mxlqm5lrg82xap31bqff4p5ql2gaxl04mhr2pw7k5y2a9")))

(define-public crate-crates-io-0.25.0 (c (n "crates-io") (v "0.25.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0skxmcrkvg0gz4a66wgbzhc91qy2m8917k8gnps3bxijxd7wfl6m")))

(define-public crate-crates-io-0.26.0 (c (n "crates-io") (v "0.26.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1yzgv1iia09r39jwbg8zv21c5d61wxn351pimza2n7fsb5kgiy9h")))

(define-public crate-crates-io-0.27.0 (c (n "crates-io") (v "0.27.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0jyn16wmry40wi2k496i86skhkqj8sgcz92lr5n6w45vijdcj19n")))

(define-public crate-crates-io-0.28.0 (c (n "crates-io") (v "0.28.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1qa81j41w6dffyxyc313gjbk4jk23d4j34nq2jmcljy86mq7picx")))

(define-public crate-crates-io-0.29.0 (c (n "crates-io") (v "0.29.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1m9s1xj2v18ncxayr5bbjz3fnd90f80mnxx996nknbbf095xp9al")))

(define-public crate-crates-io-0.30.0 (c (n "crates-io") (v "0.30.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0xygr09qgq1xalzln8k96nrlh8zagr622hczffs7f0liq0lrmbm3")))

(define-public crate-crates-io-0.31.0 (c (n "crates-io") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0b11d95jhddzlympzc4i9mwk143nmkrxaic6q03z02bsd1j90755")))

(define-public crate-crates-io-0.32.0 (c (n "crates-io") (v "0.32.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1jh7fsk3k85mmksvn4ahgi2g56dmmxcfwh07v3vivg8a2369x535")))

(define-public crate-crates-io-0.31.1 (c (n "crates-io") (v "0.31.1") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0jhx0pgprlzypq037rkdsc2k3knlimx2vp5k7vwyvsa6iaa7gy89")))

(define-public crate-crates-io-0.33.0 (c (n "crates-io") (v "0.33.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "147mggf85fz77vsrzsvsxxwid4f4fg30zwfyirx7sl1k7y33hw91")))

(define-public crate-crates-io-0.33.1 (c (n "crates-io") (v "0.33.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0nmpzr697a6v12ljwpmjrhqpmkf784nsm8m1g6jwadmkq96p3mxj")))

(define-public crate-crates-io-0.34.0 (c (n "crates-io") (v "0.34.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "139jnq0bp507sj5na86iw0qbqfahwm5v7ajy344fgcikj52qfjkb")))

(define-public crate-crates-io-0.35.0 (c (n "crates-io") (v "0.35.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0zlp8zdkd0qr1ik64wik8bavdm75y54xxp6d7mqvxvv8ssq97144")))

(define-public crate-crates-io-0.35.1 (c (n "crates-io") (v "0.35.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0bkypzxwchlff8nb6iy3a060lbph6hvhxsxj98k0f0m6gl3vdpz2")))

(define-public crate-crates-io-0.36.0 (c (n "crates-io") (v "0.36.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "06wzr6sfn7szbafagbpyx1q84smvz6cssafz8xgg7lg68hvrgf8s")))

(define-public crate-crates-io-0.36.1 (c (n "crates-io") (v "0.36.1") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "187iw4m9x1mv1alsw79iy7fdsf9a5hws65k1j3gis34236zzhl0a") (y #t)))

(define-public crate-crates-io-0.37.0 (c (n "crates-io") (v "0.37.0") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0z65z08lz27s1yi7aw5nphlziyihb52a7ni3bvmz59gw9adscsl7")))

(define-public crate-crates-io-0.38.0 (c (n "crates-io") (v "0.38.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "11gc58zp7xm4kk13m7q04h22r18anzbkhm1hiqplx067k5w5mlsb")))

(define-public crate-crates-io-0.39.0 (c (n "crates-io") (v "0.39.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0whggjbgx74xhmmphami3acng61k13hgybbya9ccwdfn1c0gvb8s") (r "1.72.0")))

(define-public crate-crates-io-0.39.1 (c (n "crates-io") (v "0.39.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1a5i5l9v2khikp3b1cbpqjbx0nhggfq261lpfv3skh9w2210qlx4") (r "1.73")))

(define-public crate-crates-io-0.39.2 (c (n "crates-io") (v "0.39.2") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "07zgv2i6j15bs6nd0ddgmmmf6sl46kq920003znyqf63qc1gj8k6") (r "1.73")))

(define-public crate-crates-io-0.40.0 (c (n "crates-io") (v "0.40.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1y4f7hjs7wq84qcmfcc5mflp00by9vk2ypkfc1wcz2c8zi6qp58r") (r "1.75.0")))

(define-public crate-crates-io-0.40.1 (c (n "crates-io") (v "0.40.1") (d (list (d (n "curl") (r "^0.4.46") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1n46mskr3cffh14ji58jpv6k27x75cmnnqk050xaimasiw4gqs0p") (r "1.76.0")))

