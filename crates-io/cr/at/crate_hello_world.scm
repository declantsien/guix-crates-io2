(define-module (crates-io cr at crate_hello_world) #:use-module (crates-io))

(define-public crate-crate_hello_world-0.1.0 (c (n "crate_hello_world") (v "0.1.0") (h "14rcpap3bl0895dbnpnb6vp1cmg7dhdwixrz2i4i4vy2xcd6fs2g")))

(define-public crate-crate_hello_world-0.1.1 (c (n "crate_hello_world") (v "0.1.1") (h "0711b7l70qaq1xpnfq40b6c0h3mzmkwrvdraff3cmqsa0345irkm")))

