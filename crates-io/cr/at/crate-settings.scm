(define-module (crates-io cr at crate-settings) #:use-module (crates-io))

(define-public crate-crate-settings-0.0.1 (c (n "crate-settings") (v "0.0.1") (d (list (d (n "settings-macros") (r "^0.0.1") (d #t) (k 0)))) (h "01bmgszqwj63bcnspgzid3qfka3qrs3zyrlha5ydxzs857ivzc2c")))

(define-public crate-crate-settings-0.0.2 (c (n "crate-settings") (v "0.0.2") (d (list (d (n "settings-macros") (r "^0.0.2") (d #t) (k 0)))) (h "1xjwzxazbqd6qgc3vqcrf4m1dw99lwhggxw37dkk0crcz3pqrb4h")))

(define-public crate-crate-settings-0.0.3 (c (n "crate-settings") (v "0.0.3") (d (list (d (n "settings-macros") (r "^0.0.3") (d #t) (k 0)))) (h "0fgwnb0zdqpw2k8rhh3kbq8k1l8nk8pa8hy57mbh3y7zjrfgjnw3")))

(define-public crate-crate-settings-0.0.4 (c (n "crate-settings") (v "0.0.4") (d (list (d (n "settings-macros") (r "^0.0.4") (d #t) (k 0)))) (h "1kwnil7d6jq2wnsp1wpz4qagnhjx29ik07h86kbbsfi9qpg9a843")))

(define-public crate-crate-settings-0.0.5 (c (n "crate-settings") (v "0.0.5") (d (list (d (n "settings-macros") (r "^0.0.5") (d #t) (k 0)))) (h "180hmzp92r586fsxl31wz3xwmckqps4wbscmgmqg84jffaab2fmr")))

(define-public crate-crate-settings-0.0.6 (c (n "crate-settings") (v "0.0.6") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1bf2m0si3p0wv9i8kpbwzv9l5n83bvx5vpa9z5ng93qmrac5lwbj")))

(define-public crate-crate-settings-0.0.7 (c (n "crate-settings") (v "0.0.7") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0k2hz7w3fyyjdsfa8gkyfq651f8dghrygrxz3nkihqbzxcgkvx8f")))

