(define-module (crates-io cr at cratchit) #:use-module (crates-io))

(define-public crate-cratchit-0.0.2 (c (n "cratchit") (v "0.0.2") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.1.4") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0kq6g6ymckqgbpja57pwfxiy8wgsljw3m0ksnwxrqw0w2zhfyads") (y #t)))

(define-public crate-cratchit-0.0.3 (c (n "cratchit") (v "0.0.3") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.1.4") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "1a6p51jba109r2d833pjlgvgc39dy0gjs6mlk9xwiphnan117dd2")))

(define-public crate-cratchit-0.0.4 (c (n "cratchit") (v "0.0.4") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.1.4") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0f3c4n4asfnm61fyrhxarpkihjkj85hr9bn89rjmzramx1isvlgf")))

