(define-module (crates-io cr at crate_usb) #:use-module (crates-io))

(define-public crate-crate_usb-0.1.0 (c (n "crate_usb") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00qigm4sym4g40f2kisfdk2fgpwx1ls6yn84hrkjhhywygviqr3m")))

