(define-module (crates-io cr at crate-inspector) #:use-module (crates-io))

(define-public crate-crate-inspector-0.1.0 (c (n "crate-inspector") (v "0.1.0") (d (list (d (n "rustdoc-json") (r "^0.8") (d #t) (k 0)) (d (n "rustdoc-types") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13p1qwp1483f8d5x48p6n7v1yyn4j1bd17nfjvyvhc7dzbcjlica")))

(define-public crate-crate-inspector-0.1.1 (c (n "crate-inspector") (v "0.1.1") (d (list (d (n "rustdoc-json") (r "^0.8") (d #t) (k 0)) (d (n "rustdoc-types") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fdg8d72s314hsdjmwyy60lcrh70dqhz3clknd9j4a4pplk08w1f")))

(define-public crate-crate-inspector-0.1.2 (c (n "crate-inspector") (v "0.1.2") (d (list (d (n "rustdoc-json") (r "^0.8") (d #t) (k 0)) (d (n "rustdoc-types") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kvqdg1p4drpfrhvsjs8ba113mikrnb1gwjx1jfrdki0q97yhl01")))

(define-public crate-crate-inspector-0.1.3 (c (n "crate-inspector") (v "0.1.3") (d (list (d (n "rustdoc-json") (r "^0.8") (d #t) (k 0)) (d (n "rustdoc-types") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kb3waifd1swidfq3zgmx877xxn40ffgp0rgmsnnq1i763r29mwg")))

(define-public crate-crate-inspector-0.1.4 (c (n "crate-inspector") (v "0.1.4") (d (list (d (n "rustdoc-json") (r "^0.8") (d #t) (k 0)) (d (n "rustdoc-types") (r "^0.23") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "110vna57myv02vh34z4pk0pji725sb4fkg4bwn57ik2gsc6gy3n2")))

