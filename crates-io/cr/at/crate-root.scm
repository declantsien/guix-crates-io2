(define-module (crates-io cr at crate-root) #:use-module (crates-io))

(define-public crate-crate-root-0.1.0 (c (n "crate-root") (v "0.1.0") (h "1qd44f8glrb28z9j3v3msh6sdwh65raw29wmf95vcxng7rq1sirf")))

(define-public crate-crate-root-0.1.1 (c (n "crate-root") (v "0.1.1") (h "1aijkif5682lbi31hb8m5mgn9rmp662s74q01z4gc020pj1j55jg")))

(define-public crate-crate-root-0.1.2 (c (n "crate-root") (v "0.1.2") (h "0bcmgyl7b9p292cr5ppnp9pcl19h8shjmvycccxn0fzhg741mx0w")))

(define-public crate-crate-root-0.1.3 (c (n "crate-root") (v "0.1.3") (h "1lzhhvnznwp1rm7ig1nj6684173sss9aah2i5hnh6sdj493gxijr")))

