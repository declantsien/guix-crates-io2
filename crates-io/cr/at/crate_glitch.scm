(define-module (crates-io cr at crate_glitch) #:use-module (crates-io))

(define-public crate-crate_glitch-0.1.1 (c (n "crate_glitch") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "glitch-in-the-matrix") (r "^0.14.0") (d #t) (k 0)) (d (n "gm-types") (r "^0.4.0-pre") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "1yk71pcrhwhy7hqh9p6135hz61hxh0p0waxbkdp6b7p41im37b36")))

(define-public crate-crate_glitch-0.2.0 (c (n "crate_glitch") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "matrix-sdk") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "1s54ald6dbs3hm7004f96byp71635hk8kp55cqy2c9yr85nwksx0")))

