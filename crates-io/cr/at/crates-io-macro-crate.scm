(define-module (crates-io cr at crates-io-macro-crate) #:use-module (crates-io))

(define-public crate-crates-io-macro-crate-0.1.0 (c (n "crates-io-macro-crate") (v "0.1.0") (h "0m0d2y16v045s0a3zv1abzwijah4aswnsj37qsxs81wbrmcm9mnh")))

(define-public crate-crates-io-macro-crate-0.1.1 (c (n "crates-io-macro-crate") (v "0.1.1") (h "17639jch1mn99vpfwhwsxd6qvlvf0miwbj3c818haq4yfvwb637g")))

(define-public crate-crates-io-macro-crate-0.1.2 (c (n "crates-io-macro-crate") (v "0.1.2") (h "1qvgzbh22j4za2ay13w9kysprp0gmiqmbmvw2kmvcxnki990pfzp")))

