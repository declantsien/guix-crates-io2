(define-module (crates-io cr at crates-io-seo-test) #:use-module (crates-io))

(define-public crate-crates-io-seo-test-0.1.0 (c (n "crates-io-seo-test") (v "0.1.0") (h "0ajikhmyivmz4vdxa5df46xpqcsw58313bswspc4ymcmiivlvjgg")))

(define-public crate-crates-io-seo-test-0.2.0 (c (n "crates-io-seo-test") (v "0.2.0") (h "0ympm36079xnx9g3h1pydki5aapc68w4xiphr9ifda894gfq89hg")))

(define-public crate-crates-io-seo-test-0.3.0 (c (n "crates-io-seo-test") (v "0.3.0") (h "1znk4h64z9k6fdsmhxlx0b5546b4w2sxk0jdx91sv1n7rgyi8adv")))

(define-public crate-crates-io-seo-test-0.4.0 (c (n "crates-io-seo-test") (v "0.4.0") (h "0wzbprs5j5z0944z41wg19dj5z6fy20sxqif65z7cgvnp1kr68b5")))

(define-public crate-crates-io-seo-test-0.5.0 (c (n "crates-io-seo-test") (v "0.5.0") (h "0xdc4v7h9h9ziza9wsbrqyd4899xi52rariwaj3pwnajfy2242z5")))

(define-public crate-crates-io-seo-test-0.6.0 (c (n "crates-io-seo-test") (v "0.6.0") (h "0lc4nni03vpgsvkkfc7jwmm92phlyd3q15mwx5ljaqzjikw963jn")))

(define-public crate-crates-io-seo-test-0.7.0 (c (n "crates-io-seo-test") (v "0.7.0") (h "1wxgaswip7i0cm3kpryi5wqwv8dnf4mbxs7lvdq0gmhm8slyi821")))

