(define-module (crates-io cr ay crayfish-macros) #:use-module (crates-io))

(define-public crate-crayfish-macros-0.1.0 (c (n "crayfish-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0rm2hiknc7fxk22xllf0a53j84v51hrszj30cy038f6iabp4a63w")))

