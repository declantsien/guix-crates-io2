(define-module (crates-io cr ay craydate-build) #:use-module (crates-io))

(define-public crate-craydate-build-0.1.0 (c (n "craydate-build") (v "0.1.0") (h "1ql70pfyxia0p2y7g9hrnjsyd0mysj30g0i0bhw9pm0n36g5yaa5")))

(define-public crate-craydate-build-0.1.1 (c (n "craydate-build") (v "0.1.1") (h "1lqzqmhdgs2h5mjxf30xbi9dgd9a5jk509571zc61jmgppjpvmws")))

(define-public crate-craydate-build-0.1.2 (c (n "craydate-build") (v "0.1.2") (h "114yvp9bp2ha8h01lsqzy2w8mwwsk0s5iyvc86q5sc9hg8fwy1z0")))

