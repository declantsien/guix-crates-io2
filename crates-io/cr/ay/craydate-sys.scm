(define-module (crates-io cr ay craydate-sys) #:use-module (crates-io))

(define-public crate-craydate-sys-0.1.0 (c (n "craydate-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "dynpath") (r "^0.1") (d #t) (k 0)))) (h "10iwsin5dafbl8szfr5mpz76lrh7gmpcb6bqwvgkbc6819yna5gy")))

(define-public crate-craydate-sys-0.1.1 (c (n "craydate-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "dynpath") (r "^0.1") (d #t) (k 0)))) (h "1aq33slvgd5gvpwbmpnji7l4k8b5b6q5bmdb3s4hs6mjvqklz76m")))

(define-public crate-craydate-sys-0.1.2 (c (n "craydate-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "dynpath") (r "^0.1") (d #t) (k 0)))) (h "18bhv98dypsi67i0rlx8010bw8zhsrv7pamcczrbmrxbil57dvwg")))

(define-public crate-craydate-sys-0.1.3 (c (n "craydate-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 0)))) (h "12210iyc8s1c2bxhch3g2nq8yhm09dgkqs3nchg5h140clpiqd64") (f (quote (("generate" "bindgen"))))))

