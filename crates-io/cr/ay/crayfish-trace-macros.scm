(define-module (crates-io cr ay crayfish-trace-macros) #:use-module (crates-io))

(define-public crate-crayfish-trace-macros-0.1.0 (c (n "crayfish-trace-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0f68qdjc5xs4yix53b6lwlqlrmxlcrcraydx4j9vcn44wr0v8glm") (f (quote (("enabled"))))))

