(define-module (crates-io cr ay craydate) #:use-module (crates-io))

(define-public crate-craydate-0.1.0 (c (n "craydate") (v "0.1.0") (d (list (d (n "craydate-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1iicdgcbvxpldw1y3jzlcc6hnigva4rnb86vxfydxy4kh8m5xisc")))

(define-public crate-craydate-0.1.1 (c (n "craydate") (v "0.1.1") (d (list (d (n "craydate-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "18vl926wdzmz92lhwrzs6mq8gdk7b9fhpvw2mvv8nvm3iwk0bisc")))

(define-public crate-craydate-0.1.2 (c (n "craydate") (v "0.1.2") (d (list (d (n "craydate-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0h54rigbhlkabbl0ah12xcxa6xi0bq5jzy4w2n7n787cbkankji3")))

(define-public crate-craydate-0.1.3 (c (n "craydate") (v "0.1.3") (d (list (d (n "craydate-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0h1wxydm18g4vb7flqxwv56i7kyczr741r49xrr7cgwsq6dq9q9w")))

(define-public crate-craydate-0.1.4 (c (n "craydate") (v "0.1.4") (d (list (d (n "craydate-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "1scdkffl2g31s9jnr1zs741nsf0mg40a157dxx72dfxzznd7vy0p")))

(define-public crate-craydate-0.1.5 (c (n "craydate") (v "0.1.5") (d (list (d (n "craydate-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "11176dhqsgrxb7f3dbk2nhz4fdclzwznils39b6j0m2lv3ck3ix1")))

(define-public crate-craydate-0.1.6 (c (n "craydate") (v "0.1.6") (d (list (d (n "craydate-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0gmw8d902kibzl0mgc2pggn777ha6l9zidhwa664z2hk6n3nmyyk")))

(define-public crate-craydate-0.1.7 (c (n "craydate") (v "0.1.7") (d (list (d (n "craydate-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "craydate-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "07c13rhsvs0zv4825kyjfcq51asyk4q8rxcrzffw3yx7gnk5afir")))

