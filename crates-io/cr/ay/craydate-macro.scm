(define-module (crates-io cr ay craydate-macro) #:use-module (crates-io))

(define-public crate-craydate-macro-0.1.0 (c (n "craydate-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10yrnyxxf93mxf50yizk3qm37m1brrxknnljb24c2shhfsanndip")))

(define-public crate-craydate-macro-0.1.1 (c (n "craydate-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08y46faz309apmlchkfgfjp802hdgky6wh4fmlxk39x9rc5bns88")))

(define-public crate-craydate-macro-0.1.2 (c (n "craydate-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01693dy6g0fgdpg7fq7vi8dn9k04c011dr9k1hwl1cfxpzzl4s2l")))

