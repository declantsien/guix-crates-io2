(define-module (crates-io cr az crazyradio) #:use-module (crates-io))

(define-public crate-crazyradio-0.1.0 (c (n "crazyradio") (v "0.1.0") (d (list (d (n "rusb") (r "^0.5") (d #t) (k 0)))) (h "0fhzg1xq4xbz83yzxz92lf46z2k2c4ym1xqss5403qp2s60dajb8")))

(define-public crate-crazyradio-0.1.1 (c (n "crazyradio") (v "0.1.1") (d (list (d (n "rusb") (r "^0.5") (d #t) (k 0)))) (h "1c500n109fqpbmmzy9ybl0pm6w7lcwlyxvkyirbw8g99n9r8wjn5")))

(define-public crate-crazyradio-0.1.2 (c (n "crazyradio") (v "0.1.2") (d (list (d (n "rusb") (r "^0.5") (d #t) (k 0)))) (h "1qsg0vj8ci171l2fkg43696jl66vh1h7wj7a4xag4x0338fn0bbw")))

(define-public crate-crazyradio-0.1.3 (c (n "crazyradio") (v "0.1.3") (d (list (d (n "rusb") (r "^0.5") (d #t) (k 0)))) (h "0bb0vqsxnvbm88b3yaqdxlk2bzcr846vqa6x0vlwbsf5kj0y78sl")))

(define-public crate-crazyradio-0.1.4 (c (n "crazyradio") (v "0.1.4") (d (list (d (n "rusb") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1w855mr7mj2chdmi1l1xyynqmxakdd6gs0hax0bhmc24gfmcrrkb") (f (quote (("serde_support" "serde"))))))

(define-public crate-crazyradio-0.1.5 (c (n "crazyradio") (v "0.1.5") (d (list (d (n "rusb") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vk33ixf4mwz6asy9pfxf1ww743v8bz9xsyb32fa5yq1gbyggq5p") (f (quote (("serde_support" "serde"))))))

(define-public crate-crazyradio-0.1.6 (c (n "crazyradio") (v "0.1.6") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.50") (o #t) (d #t) (k 0)) (d (n "flume") (r "^0.10.4") (o #t) (d #t) (k 0)) (d (n "rusb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11zdlg7gk2sq1nbilwx4jddfdbkhbn1j0ma46xnd3h9wx3xmy3qy") (f (quote (("shared_radio" "flume" "async-trait") ("serde_support" "serde"))))))

(define-public crate-crazyradio-0.2.0 (c (n "crazyradio") (v "0.2.0") (d (list (d (n "flume") (r "^0.10.4") (o #t) (k 0)) (d (n "rusb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "023w4il96x3mjik91llbasjyvrv7qdlp5v918gmc2ibn98z8zdjj") (f (quote (("shared_radio" "flume") ("serde_support" "serde") ("async" "flume/async"))))))

(define-public crate-crazyradio-0.3.0 (c (n "crazyradio") (v "0.3.0") (d (list (d (n "flume") (r "^0.10.4") (o #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.6.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0x4j3lzrjz2p9kmbd4a7dqw4lx44q4iw1wpfz8lnmnmxdmjj48r0") (f (quote (("shared_radio" "flume") ("serde_support" "serde") ("async" "flume/async"))))))

