(define-module (crates-io cr ev crevice_notan-derive) #:use-module (crates-io))

(define-public crate-crevice_notan-derive-0.10.0 (c (n "crevice_notan-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0ifmqgw64f916i2d4bh7yn5f06z6xb9p7iams95kb2h8nnrp3n6l") (f (quote (("default") ("debug-methods"))))))

(define-public crate-crevice_notan-derive-0.13.0 (c (n "crevice_notan-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0c0znnrwywaqbjf9n5iaga980fpsskw46ryjy8l7h0gnjsmd368c") (f (quote (("default") ("debug-methods"))))))

