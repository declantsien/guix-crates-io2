(define-module (crates-io cr ev crevice-derive) #:use-module (crates-io))

(define-public crate-crevice-derive-0.1.0 (c (n "crevice-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0q1ngv561m7k97v16pljfnpf4p6sgd9b3s7c552fch8pg2jqdbh5")))

(define-public crate-crevice-derive-0.2.0 (c (n "crevice-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0s2ncwl78xgslb2k5rh2q46mrjsy8p87gn76xk5ss2vqhp57w8iy") (f (quote (("test_type_layout"))))))

(define-public crate-crevice-derive-0.3.0 (c (n "crevice-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0lp242h6p15jlw02pf60pbf506zh0bcam16svjahnw115bgxv2xp") (f (quote (("test_type_layout"))))))

(define-public crate-crevice-derive-0.4.0 (c (n "crevice-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "1za2pxjvlj5wi8lfmv2bs2ky3kkc8vym7s39fvs99gix1nimqlpd") (f (quote (("test_type_layout"))))))

(define-public crate-crevice-derive-0.5.0 (c (n "crevice-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0d4qy0jk60ljr3vcxavflpa1wgfzb6fb36wglb8hihahcls7snny") (f (quote (("test_type_layout"))))))

(define-public crate-crevice-derive-0.6.0 (c (n "crevice-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0z26nfs0v7q8f9x3kk0kpmcgvsgxvhagrcsw52x8wgb1253r9psr") (f (quote (("test_type_layout"))))))

(define-public crate-crevice-derive-0.7.1 (c (n "crevice-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "1l57pp4xzj7zzs2j4q3yxsaa6fdlimrkq6ayxff6sp5d2kjifx6p") (f (quote (("test_type_layout"))))))

(define-public crate-crevice-derive-0.8.0 (c (n "crevice-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "1r7x01g7v12248dxkrn9iszwp4kwai34fp7az0315p6skx2w723d") (f (quote (("default") ("debug-methods"))))))

(define-public crate-crevice-derive-0.9.0 (c (n "crevice-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "1j34xml831m830nqfsgwd1qxidbvzykb1ca5x434g7iq9xw3bmdm") (f (quote (("default") ("debug-methods"))))))

(define-public crate-crevice-derive-0.10.0 (c (n "crevice-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)))) (h "0vmjb14w7dawjh6fdggiafajv9193jlr1plss0miwj9yxdyc0813") (f (quote (("default") ("debug-methods"))))))

