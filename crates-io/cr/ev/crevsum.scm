(define-module (crates-io cr ev crevsum) #:use-module (crates-io))

(define-public crate-crevsum-0.2.0 (c (n "crevsum") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "common_failures") (r "^0.1") (d #t) (k 0)) (d (n "crev-common") (r "^0.2") (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1k5m7xh5zl47n424rxlzqvkczg46qs2za9knjyzzzdb0p2lv1c04")))

(define-public crate-crevsum-0.3.0 (c (n "crevsum") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "common_failures") (r "^0.1") (d #t) (k 0)) (d (n "crev-common") (r "^0.3") (d #t) (k 0)) (d (n "crev-recursive-digest") (r "^0.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "14ykhx4ns4k47kkcydmivqjqnwjfjrzybamrqayd8rrcd2vqijb8")))

