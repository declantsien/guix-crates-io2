(define-module (crates-io cr tq crtq) #:use-module (crates-io))

(define-public crate-crtq-0.0.1 (c (n "crtq") (v "0.0.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "hazard") (r "^0.3.0") (d #t) (k 0)) (d (n "queuecheck") (r "^0.1.0") (d #t) (k 2)))) (h "1pkld226nkvcsl9r97ycn95hpjz6s9sb1jc77nfw2xvx5iljal24") (f (quote (("valgrind")))) (y #t)))

(define-public crate-crtq-0.1.0 (c (n "crtq") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "hazard") (r "^0.3.0") (d #t) (k 0)) (d (n "queuecheck") (r "^0.1.0") (d #t) (k 2)))) (h "1957cf379qpx7spna9hz8wckvvkd43n5if0q1jkzsc300ni1pjbs") (f (quote (("valgrind"))))))

(define-public crate-crtq-0.1.1 (c (n "crtq") (v "0.1.1") (d (list (d (n "hazard") (r "^0.3.0") (d #t) (k 0)) (d (n "queuecheck") (r "^0.1.0") (d #t) (k 2)))) (h "01zpydas8rjhjhc1rcvzv6p1n6pibkxg4v3m07jsskzcvirq78ix") (f (quote (("valgrind"))))))

