(define-module (crates-io cr ca crcany-macro) #:use-module (crates-io))

(define-public crate-crcany-macro-0.0.1 (c (n "crcany-macro") (v "0.0.1") (d (list (d (n "crcany-core") (r "^0.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ji1wynbz99ynz3fzhxijn4h3b9syj2m8whqhbxj865cpqnqwga3")))

(define-public crate-crcany-macro-0.0.2 (c (n "crcany-macro") (v "0.0.2") (d (list (d (n "crcany") (r "^0") (d #t) (k 2)) (d (n "crcany-core") (r "^0.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0win7cfz33svfnhvnkddlryspl95miv0rwhkgzhab14wbniw9lkv")))

