(define-module (crates-io cr ca crcany) #:use-module (crates-io))

(define-public crate-crcany-0.0.1 (c (n "crcany") (v "0.0.1") (d (list (d (n "crcany-core") (r "^0.0.1") (d #t) (k 0)) (d (n "crcany-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1d3xxhjb4z36q7rxjwrv8m8snv9s29ygkdpxxw3r6zw7awvj0npq")))

(define-public crate-crcany-0.0.2 (c (n "crcany") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crcany-core") (r "^0.0.2") (d #t) (k 0)) (d (n "crcany-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1ppw06bbxjw8nfnfxzm839b2k5vyjjv4mga9s7y6m89m49jk8yd3")))

