(define-module (crates-io cr ca crcany-core) #:use-module (crates-io))

(define-public crate-crcany-core-0.0.1 (c (n "crcany-core") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1blbzakjhvbwdmxa4lj1l6v9z2p7z8q0n75338hvnbayidmhak0m")))

(define-public crate-crcany-core-0.0.2 (c (n "crcany-core") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0p8yzi7hdk904ggcqpqvaaznq310jixlsw5vcd4ajih3gpbi3dkc")))

