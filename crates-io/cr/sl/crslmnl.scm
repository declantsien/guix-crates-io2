(define-module (crates-io cr sl crslmnl) #:use-module (crates-io))

(define-public crate-crslmnl-0.1.0 (c (n "crslmnl") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "pnet") (r "^0.16") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "18fy0nid40xp9hn3a8y85hhgg5xszgd8a44715qwiqvs8bva1dkj") (f (quote (("ge-1_0_4") ("default" "ge-1_0_4"))))))

(define-public crate-crslmnl-0.2.0 (c (n "crslmnl") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "pnet") (r "^0.16") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0v34mvff3jaxw0sk97szlydr3bsl5i90g30n2p7kh1pshizzb1pa") (f (quote (("ge-1_0_4") ("default" "ge-1_0_4"))))))

(define-public crate-crslmnl-0.2.1 (c (n "crslmnl") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 2)) (d (n "pnet") (r "^0.20") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "02kbmgazga6xmv9mrq5czq61w7aamyc9521im7lqn7x3knxdab9v") (f (quote (("ge-1_0_4") ("default" "ge-1_0_4"))))))

