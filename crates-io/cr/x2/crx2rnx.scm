(define-module (crates-io cr x2 crx2rnx) #:use-module (crates-io))

(define-public crate-crx2rnx-1.0.0 (c (n "crx2rnx") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rinex") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pwgjwq6c7z970kwy540xckzy5f91l5x623q1wrx0nqfjcmdj6ig")))

(define-public crate-crx2rnx-1.0.1 (c (n "crx2rnx") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.2.22") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "rinex") (r "^0.6.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03csix23qw277w4b5y0gc6yk0n23mxdn8jhnycjdlb45763clkz1")))

(define-public crate-crx2rnx-2.1.0 (c (n "crx2rnx") (v "2.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "^0.10.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1hnr250q6kjvl5mylbbhbcha58mv5b2fpkg7h39rh2cfkxpzvvyw")))

(define-public crate-crx2rnx-2.1.1 (c (n "crx2rnx") (v "2.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0lwp8vhq7vmq6hph3wf58yy9rks83hnlr1nzpkikm1j4l76j4klg")))

(define-public crate-crx2rnx-2.2.0 (c (n "crx2rnx") (v "2.2.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0w5v4ml98d88bx1iiv9bfkw8i9cym3nbwaxn9hzgsvjpyy5wqkrl")))

(define-public crate-crx2rnx-2.2.1 (c (n "crx2rnx") (v "2.2.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0y1smw1x47zy4njr4v8k61mmr13p069xr0b8i5vraz5i6bn4lxk5")))

(define-public crate-crx2rnx-2.3.0 (c (n "crx2rnx") (v "2.3.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.15.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1xs2w8jylrmdzjpjcnb8grms2q4sbz8wfid11r5yyy7ppris25ii")))

(define-public crate-crx2rnx-2.3.3 (c (n "crx2rnx") (v "2.3.3") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.16.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ficljcxdgzxn3iw7sjngml564h1xn5mi60spjcgypv9qq1hk79i")))

(define-public crate-crx2rnx-2.3.4 (c (n "crx2rnx") (v "2.3.4") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "rinex") (r "=0.16.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0q7nyav00cr5jkj2nx9gnp6dgilrk42raahsr2w2vxkqajrh8mg6")))

