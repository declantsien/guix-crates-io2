(define-module (crates-io cr us crusadertest2) #:use-module (crates-io))

(define-public crate-crusadertest2-0.1.0 (c (n "crusadertest2") (v "0.1.0") (d (list (d (n "crusadertest1") (r "^0.1.0") (d #t) (k 0)))) (h "0ww3bgyjbhir0a4axj8whi1gmgp4q0g7z16hkz3rl7x1gjysb70f")))

(define-public crate-crusadertest2-0.2.0 (c (n "crusadertest2") (v "0.2.0") (d (list (d (n "crusadertest1") (r "^0.1.0") (d #t) (k 0)))) (h "1ymybmrxqhj4hmma66p4rz78miaxxjfwbnr7rahlxhsk08b6i02m")))

