(define-module (crates-io cr us crustacean) #:use-module (crates-io))

(define-public crate-crustacean-0.1.0 (c (n "crustacean") (v "0.1.0") (d (list (d (n "clang") (r "^0.10.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "07j6fni6zvwzawq3dph1sm1q6xvqydadbln1qknlnkfr25rd7syz") (f (quote (("static" "clang/static")))) (y #t)))

(define-public crate-crustacean-0.2.0 (c (n "crustacean") (v "0.2.0") (d (list (d (n "clang") (r "^0.10.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "0nz5avf72z8013362z66b4bp451dcl9414zk2pvfz7dkmcq11i1l") (f (quote (("static" "clang/static"))))))

(define-public crate-crustacean-0.2.1 (c (n "crustacean") (v "0.2.1") (d (list (d (n "clang") (r "^0.10.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "13r0r6f1rscp2dwqnqp1zkiblasnqns7dglqdmr98nv1ffawadzi") (f (quote (("static" "clang/static"))))))

(define-public crate-crustacean-0.2.2 (c (n "crustacean") (v "0.2.2") (d (list (d (n "clang") (r "^0.12.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1n3gx14bh2anjmdk8a1phl2nqa8bvmnh5pvlicb1hcl471w2zv2j") (f (quote (("static" "clang/static"))))))

(define-public crate-crustacean-0.2.3 (c (n "crustacean") (v "0.2.3") (d (list (d (n "clang") (r "^0.12.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1d06zsx1q083k2w0kqj2ikzsv0vlcihhvabvhvib51nxkrvcz7j0") (f (quote (("static" "clang/static"))))))

(define-public crate-crustacean-0.2.4 (c (n "crustacean") (v "0.2.4") (d (list (d (n "clang") (r "^0.13.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "03jxf0nnixwa7qz7iz1bbqhx2hjf62f5yn51ns0xdch27ajlhz4p") (f (quote (("static" "clang/static"))))))

(define-public crate-crustacean-0.3.0 (c (n "crustacean") (v "0.3.0") (d (list (d (n "clang") (r "^0.14.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "1rcsr0nqhni03sqzvpa6rahg633zqs5smccfrrh4fc7nz3w7xnqm") (f (quote (("static" "clang/static"))))))

(define-public crate-crustacean-0.3.1 (c (n "crustacean") (v "0.3.1") (d (list (d (n "clang") (r "^0.14.0") (d #t) (k 0)) (d (n "clang-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "13yylmg2rbkf05gxbpd2vbpsb7sv6x2nhp8cji25dzk2lavp21ig") (f (quote (("static" "clang/static"))))))

