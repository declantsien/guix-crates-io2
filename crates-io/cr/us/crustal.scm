(define-module (crates-io cr us crustal) #:use-module (crates-io))

(define-public crate-crustal-0.1.2 (c (n "crustal") (v "0.1.2") (h "09qqw7y2a0fx4gfkzlglwbv494z2h18nhds6fnb7v2xxv7hqpix6")))

(define-public crate-crustal-0.1.3 (c (n "crustal") (v "0.1.3") (h "047hnrkpgc6cbfxdapg1znch7w8794jcdc4yzawwknb04h2s2cdz")))

(define-public crate-crustal-0.1.4 (c (n "crustal") (v "0.1.4") (h "00n92v20a97sh3g77m7rigx7bdg0spl4cbry8hj71jciq0lnvlh1")))

(define-public crate-crustal-0.1.5 (c (n "crustal") (v "0.1.5") (h "18jilzrpysd95flikksirjgbsnpv805qrzbgyg441m99q8r174if")))

(define-public crate-crustal-0.1.6 (c (n "crustal") (v "0.1.6") (h "062mqncj520vymnnc35qsgqfphkiifwqljbg5jn58mcpw1mf0ld3")))

(define-public crate-crustal-0.1.7 (c (n "crustal") (v "0.1.7") (h "10ig1x298nxiiggx51axvapihz1bdlw50kll94yc2829179ix4nl")))

(define-public crate-crustal-0.2.0 (c (n "crustal") (v "0.2.0") (h "1rbriqbr2ggj64sg5qzspdc7gqdar9bnplifjn61fm9zgggvagc1")))

(define-public crate-crustal-0.3.0 (c (n "crustal") (v "0.3.0") (h "0rjyyv8kdlfnjyf13ckqvbcx4i396n95fax0v93rkl9myn44hyjl")))

(define-public crate-crustal-0.3.5 (c (n "crustal") (v "0.3.5") (h "0qz4br4b6a90gqdp2d3m0k9x5i50dciz8ky7ahp90k2acbf9lyga")))

