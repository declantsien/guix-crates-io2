(define-module (crates-io cr us crustacean-executable) #:use-module (crates-io))

(define-public crate-crustacean-executable-0.2.0 (c (n "crustacean-executable") (v "0.2.0") (d (list (d (n "clap") (r "^2.9.2") (d #t) (k 0)) (d (n "crustacean") (r "^0.2.0") (d #t) (k 0)))) (h "1dsqg7bmwqc8xmyrbqbg16fbjgywmkz4610inynb0lfcmdfxw3zh")))

(define-public crate-crustacean-executable-0.2.1 (c (n "crustacean-executable") (v "0.2.1") (d (list (d (n "clap") (r "^2.9.2") (d #t) (k 0)) (d (n "crustacean") (r "^0.2.1") (d #t) (k 0)))) (h "1vbm0hq91wdcc46anycp7668ldblwsq2nbsy89gzh65pxzvzfqgc")))

(define-public crate-crustacean-executable-0.2.2 (c (n "crustacean-executable") (v "0.2.2") (d (list (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.2.2") (d #t) (k 0)))) (h "1skxspc1h7cx0skqlx36n0wzr6r0zwfp3bxckyav703cd1x1bzyi")))

(define-public crate-crustacean-executable-0.2.3 (c (n "crustacean-executable") (v "0.2.3") (d (list (d (n "clap") (r "^2.13.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.2.3") (d #t) (k 0)))) (h "00yln7m2xcdka3iw01nck18gcc1xcdcihhx1myhi8fqbllkr0zg7")))

(define-public crate-crustacean-executable-0.2.4 (c (n "crustacean-executable") (v "0.2.4") (d (list (d (n "clap") (r "^2.15.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.2.4") (d #t) (k 0)))) (h "054arvnk09wdkyhhkqzxwic9i2zxwvn91lxrv8jnbw6g1jijdi5l")))

(define-public crate-crustacean-executable-0.3.0 (c (n "crustacean-executable") (v "0.3.0") (d (list (d (n "clap") (r "^2.17.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.3.0") (d #t) (k 0)))) (h "170hbq7ib7jpahfahg3jnjwi9zrj7p7rx5a0z2j8z4p6xqqvbpxw")))

(define-public crate-crustacean-executable-0.3.1 (c (n "crustacean-executable") (v "0.3.1") (d (list (d (n "clap") (r "^2.17.1") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.3.1") (d #t) (k 0)))) (h "198nikd91xfiq95nlb8spfw6plsbijz7k87jz5541nv8w581siv8")))

