(define-module (crates-io cr us crustacean-plugin) #:use-module (crates-io))

(define-public crate-crustacean-plugin-0.2.0 (c (n "crustacean-plugin") (v "0.2.0") (d (list (d (n "crustacean") (r "^0.2.0") (d #t) (k 0)) (d (n "easy-plugin") (r "^0.7.3") (d #t) (k 0)))) (h "10rmcdygaw8hr792kv7ljg0ak21kbbf8j069xzczk3idfmlxfbnf")))

(define-public crate-crustacean-plugin-0.2.1 (c (n "crustacean-plugin") (v "0.2.1") (d (list (d (n "crustacean") (r "^0.2.1") (d #t) (k 0)) (d (n "easy-plugin") (r "^0.7.3") (d #t) (k 0)))) (h "0793vvy6s3qdj01s96xqqh17iqvh5hcxi687z46kzhwl4z9f4rp7")))

(define-public crate-crustacean-plugin-0.2.2 (c (n "crustacean-plugin") (v "0.2.2") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.2.2") (d #t) (k 0)) (d (n "easy-plugin") (r "^0.11.0") (d #t) (k 0)))) (h "15vi1pns11ysx898g4ww71zss7qad4d9rv0m380897h07py9d0i4")))

(define-public crate-crustacean-plugin-0.2.3 (c (n "crustacean-plugin") (v "0.2.3") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.2.3") (d #t) (k 0)) (d (n "easy-plugin") (r "^0.11.0") (d #t) (k 0)))) (h "0xrq7npcfwxxs2apkk33w22mh7vaa1hljlx0l4yp9bmg3pn2sd3h")))

(define-public crate-crustacean-plugin-0.2.4 (c (n "crustacean-plugin") (v "0.2.4") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.2.4") (d #t) (k 0)) (d (n "easy-plugin") (r "^0.11.0") (d #t) (k 0)))) (h "0rcpb5bz1h5pyv4vqa6b7pvl2k0v2vanqgdl63bdy7sa4n0nvmi6")))

(define-public crate-crustacean-plugin-0.3.0 (c (n "crustacean-plugin") (v "0.3.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.3.0") (d #t) (k 0)) (d (n "easy-plugin") (r "^0.11.4") (d #t) (k 0)))) (h "1v0kpgsiswwmxv11bpjkq7sijiak5mlx3h11wzfm6jddb7pb01ya")))

(define-public crate-crustacean-plugin-0.3.1 (c (n "crustacean-plugin") (v "0.3.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "crustacean") (r "^0.3.1") (d #t) (k 0)) (d (n "easy-plugin") (r "^0.11.4") (d #t) (k 0)))) (h "157hskpga2cj7s9ndp9rm6wg3m4b15bh0nwxap5xvph6rxyp84zn")))

