(define-module (crates-io cr us crush) #:use-module (crates-io))

(define-public crate-crush-0.1.0 (c (n "crush") (v "0.1.0") (d (list (d (n "clap") (r "~2.2") (d #t) (k 0)) (d (n "crushtool") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)) (d (n "simple_logger") (r "~0.4") (d #t) (k 0)))) (h "1swnf4c20b5d2ycnnax7a329nmjx2r1b2jdh49zvvfqj700z4q2i")))

