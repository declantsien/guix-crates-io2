(define-module (crates-io cr us crusts) #:use-module (crates-io))

(define-public crate-crusts-0.0.1 (c (n "crusts") (v "0.0.1") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "insta") (r "^1.23.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "0xi0z0blr03z8f4m8y88m2j2194891lr06jwkx1xfdv88cqxgklc")))

(define-public crate-crusts-0.0.2 (c (n "crusts") (v "0.0.2") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "insta") (r "^1.23.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "02v9dvk2sf2sw84nxi1dpfszaxi60x5a6a9lxmsb5nghmdb0yx0a")))

