(define-module (crates-io cr us crusts-sg) #:use-module (crates-io))

(define-public crate-crusts-sg-0.0.2 (c (n "crusts-sg") (v "0.0.2") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "insta") (r "^1.23.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "txl-rs") (r "^0.0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1apb0qkvq1cc7qmzcq1kfbizxwkfqwwbrwa6p4fjlhsjzanc24ha")))

(define-public crate-crusts-sg-0.0.3 (c (n "crusts-sg") (v "0.0.3") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "insta") (r "^1.23.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "txl-rs") (r "^0.0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0dp1bcb33wc7dzzr8l0p6f53v3qlmmzmn875c222fxd5cj6y8gz0")))

