(define-module (crates-io cr us crust_ide) #:use-module (crates-io))

(define-public crate-crust_ide-0.1.0 (c (n "crust_ide") (v "0.1.0") (d (list (d (n "gtk") (r "^0.5") (d #t) (k 0) (p "gtk4")))) (h "1w769pvq61fra6vwgywxzg72q18rv3vfdk95319wkf1jb6q22x21")))

(define-public crate-crust_ide-0.1.5 (c (n "crust_ide") (v "0.1.5") (d (list (d (n "gtk") (r "^0.5") (d #t) (k 0) (p "gtk4")))) (h "0zk1awng78cq10w0risnpxqhxhqp74lbg33lsfmh1bbn87c4p139")))

(define-public crate-crust_ide-0.1.6 (c (n "crust_ide") (v "0.1.6") (d (list (d (n "gtk") (r "^0.5") (d #t) (k 0) (p "gtk4")))) (h "0cvpr8sqqmflsrsb58ns2lx1d021bnqjnmipa4xdgi77x086w3j7")))

(define-public crate-crust_ide-0.1.7 (c (n "crust_ide") (v "0.1.7") (d (list (d (n "gtk") (r "^0.5") (d #t) (k 0) (p "gtk4")))) (h "1jakf6wjd7z4csw3cw27vhmb3rpqffh3qbj2k8c7pa7mx38aw7nr")))

