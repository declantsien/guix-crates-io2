(define-module (crates-io cr us crus) #:use-module (crates-io))

(define-public crate-crus-0.1.0 (c (n "crus") (v "0.1.0") (h "10k32qd4ybmn82c5fvkppzxhmplr14amhagzdf1bq9g991ky596p")))

(define-public crate-crus-0.1.1 (c (n "crus") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "0p0g1qri3ngsz72f7m0gqq7119w6748d7xc0x8bmrdxc0pcw7rx9")))

