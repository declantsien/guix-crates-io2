(define-module (crates-io cr us crustd) #:use-module (crates-io))

(define-public crate-crustd-0.0.1 (c (n "crustd") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "validator") (r "^0.18.1") (d #t) (k 0)))) (h "0sjxh4nilldcdm5wif4c168nsjv5ml13qwbrg2kcnf30jbmn6cwp")))

