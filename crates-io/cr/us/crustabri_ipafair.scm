(define-module (crates-io cr us crustabri_ipafair) #:use-module (crates-io))

(define-public crate-crustabri_ipafair-1.0.0 (c (n "crustabri_ipafair") (v "1.0.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "crustabri") (r "^1.0.0") (d #t) (k 0)) (d (n "ipafair-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0nfnvascx736d7sp7zf1f8sx4rdplmfbk8lyy2gm3cgcrpsairb4")))

