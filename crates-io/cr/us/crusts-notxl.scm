(define-module (crates-io cr us crusts-notxl) #:use-module (crates-io))

(define-public crate-crusts-notxl-0.0.3 (c (n "crusts-notxl") (v "0.0.3") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "insta") (r "^1.23.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "txl-rs") (r "^0.0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "08d3sndyq0jk6p4h3gk2xh2cz4whvm459lylzp74f3wafpmarg6s")))

