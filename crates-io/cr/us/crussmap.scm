(define-module (crates-io cr us crussmap) #:use-module (crates-io))

(define-public crate-crussmap-1.0.0 (c (n "crussmap") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.160") (d #t) (k 0)))) (h "0da0pk1mp6kiisgnmah4awqc985r61r2r48p3z77g68ssvkgh936")))

(define-public crate-crussmap-1.0.1 (c (n "crussmap") (v "1.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.160") (d #t) (k 0)))) (h "15d8msi9i2viivmajs9l8l6c0l5chb36v2digxkq96hb75mmn4nz")))

