(define-module (crates-io cr us crustword) #:use-module (crates-io))

(define-public crate-crustword-0.1.0 (c (n "crustword") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0biws30vvh01fy2yv7msvc2cwza32xmg73kspilvglyib8v080cr")))

