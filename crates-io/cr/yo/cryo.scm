(define-module (crates-io cr yo cryo) #:use-module (crates-io))

(define-public crate-cryo-0.1.0 (c (n "cryo") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0ryd4svwq187bwp00brf964dpgd3jy5ca0gm2d2j8fbpzcrm1v3p") (f (quote (("use_parking_lot" "parking_lot"))))))

(define-public crate-cryo-0.1.1 (c (n "cryo") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "163hfb5c1fbkd7p5zixlyj45q7qrplhbxqgd0l0fpaldj9jzm2kr") (f (quote (("use_parking_lot" "parking_lot"))))))

(define-public crate-cryo-0.1.2 (c (n "cryo") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0cpsj1qbjbg66g95aywzvcpj6xhkgg507yf233pq2n534kc8y7si")))

(define-public crate-cryo-0.1.3 (c (n "cryo") (v "0.1.3") (d (list (d (n "parking_lot") (r ">= 0.6, < 0.10") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "192sqipa5m1nm6habw4vfardmjbgw9y1zr2mwcspsq6nz2q27rzc") (y #t)))

(define-public crate-cryo-0.1.4 (c (n "cryo") (v "0.1.4") (d (list (d (n "parking_lot") (r ">= 0.6, < 0.10") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1c4mhibipq5v8zwf6m5cylxv0drxqy27fr1lf3ziv9c7ijjrmr17")))

(define-public crate-cryo-0.1.5 (c (n "cryo") (v "0.1.5") (d (list (d (n "parking_lot") (r ">= 0.6, < 0.10") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1lnx98wl6pmwircrs4hvpbymxpj1k0sbpgdp8zlsdvrildz0l0dg")))

(define-public crate-cryo-0.1.6 (c (n "cryo") (v "0.1.6") (d (list (d (n "parking_lot") (r ">= 0.6, < 0.10") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "10gx2sjinbqnz0jp4kf50drwffbdmkzgayhh8i3dqb2g82rxq4si")))

(define-public crate-cryo-0.2.0 (c (n "cryo") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "0kw7fnx98nv06lin4wrbjqfc24rwfzbhq5z7gfvb1d6y4qqyn2qz") (f (quote (("std") ("default" "std"))))))

(define-public crate-cryo-0.2.1 (c (n "cryo") (v "0.2.1") (d (list (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "08d7s4mi5jvw2hca6gqm6rvzsvap2j1jziwhms8nhcid5saq8nii") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

(define-public crate-cryo-0.2.2 (c (n "cryo") (v "0.2.2") (d (list (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "0gnsy30p4c01khnqgyikc94k227vkfdgiy0i6z5qml4w0glg59p1") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

(define-public crate-cryo-0.2.3 (c (n "cryo") (v "0.2.3") (d (list (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "0wl8wks4xaj035v5bzqx1lpldf2dpp6cgdfg5h83hwcxzpkv4nm8") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

(define-public crate-cryo-0.2.4 (c (n "cryo") (v "0.2.4") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "0i0ry82z6bnix3zjfjw1bgwkzk6sa7pgmyx3dgi8cp6mjxs8ha02") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

(define-public crate-cryo-0.2.5 (c (n "cryo") (v "0.2.5") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "0xl49fila7i67yhsc1l6amjxabnrcqil0i7m5ajv5aj1ncyw2ggs") (f (quote (("std") ("default" "atomic" "std") ("atomic")))) (y #t)))

(define-public crate-cryo-0.2.6 (c (n "cryo") (v "0.2.6") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "1qqp1cc4pbv7x7c118a9f28025kdsh2r3620kc22kph1hgmm9axi") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

(define-public crate-cryo-0.2.7 (c (n "cryo") (v "0.2.7") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "1g24z3liaazl1d1knmsxw9k6vyb0g6i8dmamn5lmqrvz218ygsan") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

(define-public crate-cryo-0.1.7 (c (n "cryo") (v "0.1.7") (d (list (d (n "parking_lot") (r ">=0.6, <0.10") (o #t) (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (d #t) (k 0)))) (h "1z8h7n08f8n5ywbrr8fnm3g5hisadzwarrwlv6m9pq2syd75sanm")))

(define-public crate-cryo-0.3.0 (c (n "cryo") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "177lq5iwly1nrskjm4nnclhgmiry309fvabkbk9mlkm5bykqpiyx") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

(define-public crate-cryo-0.3.1 (c (n "cryo") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "lock_api") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "046xwigylc87v9ci7mwkwjar6qrzi51nhkcgf0g4hf1ncpag7ak1") (f (quote (("std") ("default" "atomic" "std") ("atomic"))))))

