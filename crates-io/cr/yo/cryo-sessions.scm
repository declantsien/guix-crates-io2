(define-module (crates-io cr yo cryo-sessions) #:use-module (crates-io))

(define-public crate-cryo-sessions-0.0.1 (c (n "cryo-sessions") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (f (quote ("r2d2" "ahash" "tokio-comp"))) (d #t) (k 0)))) (h "01y0w1kp815gg8ib3h7ll205m4hmf3pjp3v0ib00z4y8w11i9bmf")))

(define-public crate-cryo-sessions-0.0.2 (c (n "cryo-sessions") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (f (quote ("r2d2" "ahash" "tokio-comp"))) (d #t) (k 0)))) (h "1qq29nypjl8k0y9nchk01g4w6gzmh0js9nhb8jsrj5fy60xdcq3s")))

(define-public crate-cryo-sessions-0.0.3 (c (n "cryo-sessions") (v "0.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (f (quote ("r2d2" "ahash" "tokio-comp"))) (d #t) (k 0)))) (h "011zlg2fplwa8ya4m4vsvkl8rf1ykabim86x0b07srdyj6vzzqvn") (y #t)))

(define-public crate-cryo-sessions-0.0.4 (c (n "cryo-sessions") (v "0.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "redis") (r "^0.23.3") (f (quote ("r2d2" "ahash" "tokio-comp"))) (d #t) (k 0)))) (h "187snq42rn8w6mhghpbzli54541r1h76xs1bsymk7yl35vrzpipm")))

