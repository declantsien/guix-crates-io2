(define-module (crates-io cr yo cryogen_plugin_json) #:use-module (crates-io))

(define-public crate-cryogen_plugin_json-0.1.0 (c (n "cryogen_plugin_json") (v "0.1.0") (d (list (d (n "clap") (r "2.20.*") (d #t) (k 0)) (d (n "cryogen_prelude") (r "0.1.*") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1j3zlz2wnggmz34ppvr941kwvs21qklrc2i8s4yan8a3i8sbmzk9")))

