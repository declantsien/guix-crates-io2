(define-module (crates-io cr yo cryogen_plugin_markdown) #:use-module (crates-io))

(define-public crate-cryogen_plugin_markdown-0.1.0 (c (n "cryogen_plugin_markdown") (v "0.1.0") (d (list (d (n "clap") (r "2.20.*") (d #t) (k 0)) (d (n "cryogen_prelude") (r "0.1.*") (d #t) (k 0)) (d (n "hoedown") (r "6.0.*") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.6.*") (d #t) (k 0)))) (h "0q8j318nw1p0ky9rhfp7mvnljw0676w9b5lbw76m5cjqhaq74bz9")))

