(define-module (crates-io cr yo cryo_to_df) #:use-module (crates-io))

(define-public crate-cryo_to_df-0.3.0 (c (n "cryo_to_df") (v "0.3.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jnvh5xm1sd3fjashsw1wxwm8p3xf4dwf5bhyjsdwkq4crl2wng1")))

(define-public crate-cryo_to_df-0.3.2 (c (n "cryo_to_df") (v "0.3.2") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jciz4vm1b5x8klgyl91m5mr1psiip65gi9m1v7xz1ggi2047wlx")))

