(define-module (crates-io cr yo cryogen_prelude) #:use-module (crates-io))

(define-public crate-cryogen_prelude-0.1.0 (c (n "cryogen_prelude") (v "0.1.0") (d (list (d (n "clap") (r "2.20.*") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "tera") (r "0.8.*") (d #t) (k 0)))) (h "19x9pnji2rdxql3qrxrfb3i6f17kzfk7pijjqq0lzl1zs4qcn311")))

