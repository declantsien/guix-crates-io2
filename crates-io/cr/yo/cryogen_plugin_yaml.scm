(define-module (crates-io cr yo cryogen_plugin_yaml) #:use-module (crates-io))

(define-public crate-cryogen_plugin_yaml-0.1.0 (c (n "cryogen_plugin_yaml") (v "0.1.0") (d (list (d (n "clap") (r "2.20.*") (d #t) (k 0)) (d (n "cryogen_prelude") (r "0.1.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.6.*") (d #t) (k 0)))) (h "1y130n5lalyq4pg23n7mmzmva7cbjg4dh414jz5mz1v7lmxvhn38")))

