(define-module (crates-io cr ad cradle_system) #:use-module (crates-io))

(define-public crate-cradle_system-0.0.1-alpha1 (c (n "cradle_system") (v "0.0.1-alpha1") (h "17kby8wfdlpzpm972sv9cr9ljpf902cry3bdmycfj525bag1x711")))

(define-public crate-cradle_system-0.0.1-alpha2 (c (n "cradle_system") (v "0.0.1-alpha2") (h "1s22r6mng0wn0j6qfzp9586ihc9cmrmwd2m6l8bqw7fmp1q1i1qh")))

