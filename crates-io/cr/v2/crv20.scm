(define-module (crates-io cr v2 crv20) #:use-module (crates-io))

(define-public crate-crv20-0.1.0 (c (n "crv20") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "curve-casper-erc20") (r "^0.1.0") (d #t) (k 0)))) (h "1kzal8hjpfxkp4w4jx0p5bm225f2vac1xj4p3ql7i6xjmm6nqjg8")))

