(define-module (crates-io cr -s cr-sys) #:use-module (crates-io))

(define-public crate-cr-sys-0.1.0 (c (n "cr-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18sg7v56p54lz1gc9ciw545l17dfgkkxhkyzzazx3fnidbp4hkwp") (f (quote (("guest")))) (l "cr")))

(define-public crate-cr-sys-0.1.1 (c (n "cr-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ns3gymlabbj0hqjvbag6qbr1icp9r9fclld5fflg4z0988hadmd") (f (quote (("guest")))) (l "cr")))

