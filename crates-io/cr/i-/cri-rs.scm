(define-module (crates-io cr i- cri-rs) #:use-module (crates-io))

(define-public crate-cri-rs-0.1.0 (c (n "cri-rs") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "grpcio") (r "^0.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.16.2") (d #t) (k 0)))) (h "000lwaa4cx2gbgna1740njawbn93cz9dbs2k0ljyncpiiqdqn34l")))

