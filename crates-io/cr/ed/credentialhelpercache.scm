(define-module (crates-io cr ed credentialhelpercache) #:use-module (crates-io))

(define-public crate-credentialhelpercache-1.0.0 (c (n "credentialhelpercache") (v "1.0.0") (d (list (d (n "credentialhelper") (r "^1.0.0") (d #t) (k 0)) (d (n "moka") (r "^0.12") (f (quote ("sync"))) (d #t) (k 0)))) (h "1y9k0l3r4i0zx32c855ykswald1qrw1zi0nslx9rmdz6rrvd6x84") (r "1.73.0")))

