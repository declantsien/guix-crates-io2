(define-module (crates-io cr ed credentialhelper) #:use-module (crates-io))

(define-public crate-credentialhelper-1.0.0 (c (n "credentialhelper") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "149jzv98rnqhaiylaln935jyhk5dv57m2k204vas4zzsrmcqpri1") (r "1.73.0")))

