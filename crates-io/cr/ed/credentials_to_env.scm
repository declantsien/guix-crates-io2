(define-module (crates-io cr ed credentials_to_env) #:use-module (crates-io))

(define-public crate-credentials_to_env-0.2.1 (c (n "credentials_to_env") (v "0.2.1") (d (list (d (n "credentials") (r "^0.2.1") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "111yxaj7q6h8wlzxhks4ybg96dr0hak9g7jjbvd73qi9n8lgivp6")))

(define-public crate-credentials_to_env-0.3.1 (c (n "credentials_to_env") (v "0.3.1") (d (list (d (n "credentials") (r "^0.3.1") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)))) (h "1dhvq5hrdb1hamq0wnfcxw1yhlcpmmncqmqcgqf90h91mwylfqz5")))

(define-public crate-credentials_to_env-0.4.0 (c (n "credentials_to_env") (v "0.4.0") (d (list (d (n "credentials") (r "^0.4.0") (d #t) (k 0)) (d (n "exec") (r "^0.2.0") (d #t) (k 0)))) (h "05ix6gakvwfd3mqz731746ch8nx6f6rd6cxfm1k2j5x8v50xvx9x") (f (quote (("unstable"))))))

(define-public crate-credentials_to_env-0.4.3 (c (n "credentials_to_env") (v "0.4.3") (d (list (d (n "credentials") (r "^0.4.0") (d #t) (k 0)) (d (n "exec") (r "^0.2.0") (d #t) (k 0)))) (h "1i32ckjdw7n42jd9z04z4yrijgrma6azym62lv0k207w3szxf7gk") (f (quote (("unstable"))))))

(define-public crate-credentials_to_env-0.4.4 (c (n "credentials_to_env") (v "0.4.4") (d (list (d (n "credentials") (r "^0.5.0") (d #t) (k 0)) (d (n "exec") (r "^0.2.0") (d #t) (k 0)))) (h "0cdiwp5rqi5n4ag2ckrbrhxckv47mx43n1335wqchava80x0w8lp") (f (quote (("unstable"))))))

(define-public crate-credentials_to_env-0.4.5 (c (n "credentials_to_env") (v "0.4.5") (d (list (d (n "credentials") (r "^0.7") (d #t) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)))) (h "0d58v5sqgcjfiv5456kbf5xgxm98rz4xjffd5zr5bh0lnpmsmfcw") (f (quote (("unstable"))))))

(define-public crate-credentials_to_env-0.4.6 (c (n "credentials_to_env") (v "0.4.6") (d (list (d (n "credentials") (r "^0.8") (d #t) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)))) (h "0fw19iqrr14jzgbx72479028nfj0z4ffypk8fyp2v2mm2003817i") (f (quote (("unstable"))))))

(define-public crate-credentials_to_env-0.4.7 (c (n "credentials_to_env") (v "0.4.7") (d (list (d (n "common_failures") (r "^0.1") (d #t) (k 0)) (d (n "credentials") (r "^0.10") (d #t) (k 0)) (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "04l2bbgan6vszp0k1cr3ywg6z3m7w3bppvw7g12zxg8i89hh6z1i") (f (quote (("unstable"))))))

