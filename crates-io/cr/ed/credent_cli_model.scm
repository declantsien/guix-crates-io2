(define-module (crates-io cr ed credent_cli_model) #:use-module (crates-io))

(define-public crate-credent_cli_model-0.2.0 (c (n "credent_cli_model") (v "0.2.0") (d (list (d (n "smol") (r "^1.2.4") (d #t) (k 0)))) (h "1w9v7m6ghx7718i9nbnmi42hbwlq3hcm3gpg3khspq90qla8rhzd")))

(define-public crate-credent_cli_model-0.2.1 (c (n "credent_cli_model") (v "0.2.1") (d (list (d (n "smol") (r "^1.2.4") (d #t) (k 0)))) (h "1shwj3jzhl8d0gnyxz25p52kk668hhyx349y9987fwj3x9jy5avs")))

(define-public crate-credent_cli_model-0.3.0 (c (n "credent_cli_model") (v "0.3.0") (d (list (d (n "smol") (r "^1.2.4") (d #t) (k 0)))) (h "04kw2imy76cnm49qml5hrda93bl32kl4cwczr04pzag70ds54yb8")))

(define-public crate-credent_cli_model-0.4.0 (c (n "credent_cli_model") (v "0.4.0") (d (list (d (n "smol") (r "^1.2.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "07r8625qypiy6b1i8qb45n22wwbc3ihl8jgfm10qwz565hwbby7m") (f (quote (("backend-tokio" "tokio" "tokio/io-std" "tokio/rt") ("backend-smol" "smol"))))))

(define-public crate-credent_cli_model-0.4.1 (c (n "credent_cli_model") (v "0.4.1") (d (list (d (n "smol") (r "^1.2.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "1y3xzy2xc3b9jnyk2jv6lr9gk9w4sdl8bwdvbmslwc498hjkigg5") (f (quote (("backend-tokio" "tokio" "tokio/io-std" "tokio/rt") ("backend-smol" "smol"))))))

