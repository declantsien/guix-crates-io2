(define-module (crates-io cr ed credit_card_types) #:use-module (crates-io))

(define-public crate-credit_card_types-0.1.100 (c (n "credit_card_types") (v "0.1.100") (h "1dj6bwgkm72am8q3qp7dfyjki4bv2nbaxjc0njq241mwdqcncafj")))

(define-public crate-credit_card_types-0.1.120 (c (n "credit_card_types") (v "0.1.120") (h "1ffndk582mla72hbxdnfwdgg20ayxrjzm2p8lp8zsi2llc7rynbs")))

(define-public crate-credit_card_types-0.1.121 (c (n "credit_card_types") (v "0.1.121") (h "1p7v4h37yz96ir1073nmwzd5lgq0jngv37sxximf2gckp9084652")))

(define-public crate-credit_card_types-0.1.200 (c (n "credit_card_types") (v "0.1.200") (h "0j043frcp4xjhwngvll0kyfbyay9fzyml753cizn6qklirj73iql")))

