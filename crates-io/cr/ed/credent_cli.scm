(define-module (crates-io cr ed credent_cli) #:use-module (crates-io))

(define-public crate-credent_cli-0.1.0 (c (n "credent_cli") (v "0.1.0") (d (list (d (n "credent_model") (r "^0.1.0") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.4") (d #t) (k 0)))) (h "1szbb83wbzyixf45hzjcrazrc1flj6yyml8p96sygcvacy1ymwqp")))

(define-public crate-credent_cli-0.2.0 (c (n "credent_cli") (v "0.2.0") (d (list (d (n "credent_cli_model") (r "^0.2.0") (d #t) (k 0)) (d (n "credent_model") (r "^0.1.0") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.4") (d #t) (k 0)))) (h "1s6plinjdfv3si219g4qk2p9h1q7sv978rvzzsgw3ca1nwl1d1nf")))

(define-public crate-credent_cli-0.2.1 (c (n "credent_cli") (v "0.2.1") (d (list (d (n "credent_cli_model") (r "^0.2.0") (d #t) (k 0)) (d (n "credent_model") (r "^0.2.1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.4") (d #t) (k 0)))) (h "18d5crk410svg062sblmdpkazzb05619ky8iwybffg8kkj6rpayh")))

(define-public crate-credent_cli-0.3.0 (c (n "credent_cli") (v "0.3.0") (d (list (d (n "credent_cli_model") (r "^0.3.0") (d #t) (k 0)) (d (n "credent_model") (r "^0.3.0") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.4") (d #t) (k 0)))) (h "10r7ph8qw4gknfl1v23qmxd76w4y799mcyb89pxn0p9ymkkai5sh")))

(define-public crate-credent_cli-0.4.0 (c (n "credent_cli") (v "0.4.0") (d (list (d (n "credent_cli_model") (r "^0.4.0") (k 0)) (d (n "credent_model") (r "^0.4.0") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0fbd42hq0pxyvxqnl2n6vyk9cvi0csab54d1302sjdjjxwacm878") (f (quote (("backend-tokio" "credent_cli_model/backend-tokio" "tokio" "tokio/io-std" "tokio/io-util" "tokio/rt") ("backend-smol" "credent_cli_model/backend-smol" "smol"))))))

(define-public crate-credent_cli-0.4.1 (c (n "credent_cli") (v "0.4.1") (d (list (d (n "credent_cli_model") (r "^0.4.1") (k 0)) (d (n "credent_model") (r "^0.4.1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.0") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "06wzs5ds04vf4pn2pz5llqfvcw5swg1hkbd2wj6nl61j6ambf2b5") (f (quote (("backend-tokio" "credent_cli_model/backend-tokio" "tokio" "tokio/io-std" "tokio/io-util" "tokio/rt") ("backend-smol" "credent_cli_model/backend-smol" "smol"))))))

