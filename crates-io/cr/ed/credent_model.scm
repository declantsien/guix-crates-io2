(define-module (crates-io cr ed credent_model) #:use-module (crates-io))

(define-public crate-credent_model-0.1.0 (c (n "credent_model") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ik5llbxv3bcrn0rn9lqnj2w8f2c8nmg2dzzk1m9zp0bbddsr17l") (f (quote (("default" "serde"))))))

(define-public crate-credent_model-0.2.1 (c (n "credent_model") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jray75db5b667hgzrqdmwwlypv5mkgnvka65kx38nyyfxa2y9m5") (f (quote (("default" "serde"))))))

(define-public crate-credent_model-0.3.0 (c (n "credent_model") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xx0cijdivbk7n5bqxn9ksq8z7yfwrhqvvwsp66hpyqs57rb25i5") (f (quote (("default" "serde"))))))

(define-public crate-credent_model-0.4.0 (c (n "credent_model") (v "0.4.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vw3pp0sxpfz3gv1yrprg6s82833sz4yja28a3d9yj1z7ng2g5c1") (f (quote (("default" "serde"))))))

(define-public crate-credent_model-0.4.1 (c (n "credent_model") (v "0.4.1") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h3dm5x92gaxmwlm2rvgj0f9swnkncks7za30sm5nhmgzq3whdx4") (f (quote (("default" "serde"))))))

