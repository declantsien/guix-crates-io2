(define-module (crates-io cr ed credman) #:use-module (crates-io))

(define-public crate-credman-0.1.0 (c (n "credman") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "postcard") (r "^1.0.8") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "01g73v1gw6lvva9zvbysbglpbh58gmlyrx1hhvkc0sycpl5cx5qd")))

