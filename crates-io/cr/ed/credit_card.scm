(define-module (crates-io cr ed credit_card) #:use-module (crates-io))

(define-public crate-credit_card-0.1.0 (c (n "credit_card") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cin5862pzdd8ybzp9xlbw0yb1243hpw2i1a53yh4rb6a1ag6vrw")))

(define-public crate-credit_card-0.1.1 (c (n "credit_card") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1633c88phkvqbdcqp3dp2r15x3qhz7p7g72j6wfi5s2qm6sjr1xd")))

(define-public crate-credit_card-0.1.2 (c (n "credit_card") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14v52zmi6ypdasrs0q2nrgys2nw69bscmi06zdri0pnimjnngapg")))

(define-public crate-credit_card-0.1.3 (c (n "credit_card") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02raqvpkmz8x2i77i33729jjab0sqi0qbnq14pz5mi335igg4klz")))

(define-public crate-credit_card-0.1.4 (c (n "credit_card") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "07ahprli0agma0w30f7gvxw28x8vnyg1cvq30rggy2sm07wsy803")))

