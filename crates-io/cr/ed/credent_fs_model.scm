(define-module (crates-io cr ed credent_fs_model) #:use-module (crates-io))

(define-public crate-credent_fs_model-0.2.0 (c (n "credent_fs_model") (v "0.2.0") (d (list (d (n "credent_model") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1xyrr1xk68dsv45l1yscmsvh9xqgnvbx77pfvgprhg7dg2zxa22l")))

(define-public crate-credent_fs_model-0.2.1 (c (n "credent_fs_model") (v "0.2.1") (d (list (d (n "credent_model") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1rs7cslgb9bhl4b7bbvi370awnr73zk25faz9ncq2syq1r572xnc")))

(define-public crate-credent_fs_model-0.3.0 (c (n "credent_fs_model") (v "0.3.0") (d (list (d (n "credent_model") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1m1ajp5i3vf11mllc0v1qdb53vmcq3mvkn4kzdg9vscmgn56g0zw")))

(define-public crate-credent_fs_model-0.4.0 (c (n "credent_fs_model") (v "0.4.0") (d (list (d (n "credent_model") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0pacrvfxly1a8gr67p75149kyck15qmhxkk1rmvxzgvb9gnbr609")))

(define-public crate-credent_fs_model-0.4.1 (c (n "credent_fs_model") (v "0.4.1") (d (list (d (n "credent_model") (r "^0.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0z0v3x64avvssb48sj827q7mzs7yqbm156scqf50pc0lin214qwx")))

