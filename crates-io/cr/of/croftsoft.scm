(define-module (crates-io cr of croftsoft) #:use-module (crates-io))

(define-public crate-croftsoft-0.1.0 (c (n "croftsoft") (v "0.1.0") (h "13aklzqfpddhm1salh6368a7iznhphv5nz7bs8pzhf5j85dz9c35") (y #t)))

(define-public crate-croftsoft-0.1.1 (c (n "croftsoft") (v "0.1.1") (d (list (d (n "com-croftsoft-core") (r "^0.1.0") (d #t) (k 0)))) (h "1h5zb7mrgc4dfcvhd4p9lrl6lljb3ys45nfc8fh6sj9y99a4gm1f")))

(define-public crate-croftsoft-0.2.0 (c (n "croftsoft") (v "0.2.0") (d (list (d (n "com-croftsoft-core") (r "^0.2.0") (d #t) (k 0)))) (h "0scnwzf24jpwskyihy3a0nhwgpc0f42p62wajj0jqgghj3cx8sr5")))

(define-public crate-croftsoft-0.9.0 (c (n "croftsoft") (v "0.9.0") (d (list (d (n "com-croftsoft-core") (r "^0.9.0") (d #t) (k 0)))) (h "0kv8763yvzfv1nk2vxx533zlv66d2kpk6chq6c1r57qws8zswk2s")))

(define-public crate-croftsoft-0.10.0 (c (n "croftsoft") (v "0.10.0") (d (list (d (n "com-croftsoft-core") (r "^0.10.0") (d #t) (k 0)))) (h "0c4p3dmar1xv0bf4n73nvfk6v8hp6nf1x7nq19achmjrlmrx45mx")))

(define-public crate-croftsoft-0.10.1 (c (n "croftsoft") (v "0.10.1") (d (list (d (n "com-croftsoft-core") (r "^0.14.0") (d #t) (k 0)) (d (n "com-croftsoft-lib") (r "^0.1.1") (d #t) (k 0)))) (h "19l05zdiyqy6f5c2m9ijwp3qf3jcq9fjrglqcrl8mh7jpi8mavfv")))

