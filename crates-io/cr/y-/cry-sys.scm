(define-module (crates-io cr y- cry-sys) #:use-module (crates-io))

(define-public crate-cry-sys-0.1.0 (c (n "cry-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1w34qp9sjlwb9smcpgll0n5pjqck93ikydhcrk0mjswxyh4al9wi")))

