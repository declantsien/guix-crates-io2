(define-module (crates-io cr am cram) #:use-module (crates-io))

(define-public crate-cram-0.1.0 (c (n "cram") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "pb-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.6.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "0djznzz9ll0wffcrdl930b8j9m68fvim7wp3ga48z0h8qgz6h2jg")))

(define-public crate-cram-0.1.1 (c (n "cram") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "queue") (r "^0.3.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "058322qbynhkkm298wclrqi9d0np1glahpr6shjrl807m5d8z4f2")))

(define-public crate-cram-0.1.2 (c (n "cram") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "queue") (r "^0.3.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "0d9kdp3036qjam26r4b5y81b97cnry4l0n21414lhvlzk5mqmzcd")))

