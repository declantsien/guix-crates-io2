(define-module (crates-io cr c1 crc16) #:use-module (crates-io))

(define-public crate-crc16-0.1.0 (c (n "crc16") (v "0.1.0") (h "1xrf67v8sja2xl17692zffzlribaqchb7js7c6ik6mjpq4vxh7x5")))

(define-public crate-crc16-0.2.0 (c (n "crc16") (v "0.2.0") (h "1yhjfs56kh1fn1hn74lbv34mxfvswxrli4x2jryjls6rbc42jmd4")))

(define-public crate-crc16-0.3.0 (c (n "crc16") (v "0.3.0") (h "19qbizm7jypwqxafj5wj6gjryv41mkwlnc38y7k3f8pdvyhipy4n")))

(define-public crate-crc16-0.3.1 (c (n "crc16") (v "0.3.1") (h "03ancp64vnc4mkp5k9746inw1p3zxaaafikxg7vxyxb0hlp63wcm")))

(define-public crate-crc16-0.3.2 (c (n "crc16") (v "0.3.2") (h "0synjr0vvqpcnbxgdvgpbbgb30sg6knpdiarv9qp73mhs89mbrqx")))

(define-public crate-crc16-0.3.3 (c (n "crc16") (v "0.3.3") (h "1b79jdz5ai518mmjh33gc7y9zpfxhlfj0zxyr4is9n036hsr4an7")))

(define-public crate-crc16-0.3.4 (c (n "crc16") (v "0.3.4") (h "1pg28xdz27mck17jl247vhdrpixgfmw3gq25b4x3wbrkjx3mr9hi")))

(define-public crate-crc16-0.4.0 (c (n "crc16") (v "0.4.0") (h "1zzwb5iv51wnh96532cxkk4aa8ys47rhzrjy98wqcys25ks8k01k")))

