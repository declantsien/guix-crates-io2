(define-module (crates-io cr it criterion-perf-events) #:use-module (crates-io))

(define-public crate-criterion-perf-events-0.1.0 (c (n "criterion-perf-events") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "perfcnt") (r "^0.5") (d #t) (k 0)))) (h "1pngpicgrr83jiwyrz7vz7zq8gb3i6f3aimmnbfifwr5mpp1pyd0")))

(define-public crate-criterion-perf-events-0.1.1 (c (n "criterion-perf-events") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "perfcnt") (r "^0.5") (d #t) (k 0)))) (h "1m34lyd15y8h2r8s1ign0yylynmqshlg98f4kb6dc1h2lmnq7qkx")))

(define-public crate-criterion-perf-events-0.1.2 (c (n "criterion-perf-events") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 0)) (d (n "perfcnt") (r "^0.6.0") (d #t) (k 0)))) (h "1ccm7sblhlvc8a1gagpy4idr06qi795b49s7gch6m3b4h4zcgfzh")))

(define-public crate-criterion-perf-events-0.1.3 (c (n "criterion-perf-events") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 0)) (d (n "perfcnt") (r "^0.7.0") (d #t) (k 0)))) (h "1qfzimnn0n9qxni622f3klj64qz2hhcx10x9v43v3q2bnkpsi3g2")))

(define-public crate-criterion-perf-events-0.1.4 (c (n "criterion-perf-events") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 0)) (d (n "perfcnt") (r "^0.7.2") (d #t) (k 0)))) (h "1b0f1gc80a1kcra737siz7a3ifvn521qrqnvmszhifzs14g139gb")))

(define-public crate-criterion-perf-events-0.2.0 (c (n "criterion-perf-events") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 0)) (d (n "perfcnt") (r "^0.8.0") (d #t) (k 0)))) (h "1zspq45cd7v778xg0nwqb88v24np01sgaq2qd9bcvq5rrsf5752y")))

(define-public crate-criterion-perf-events-0.3.0 (c (n "criterion-perf-events") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 0)) (d (n "perfcnt") (r "^0.8.0") (d #t) (k 0)))) (h "0k8j8r3bri6glvf4qzvd2v4aq5x1dj5qar8pmq4qkf7a3jgkg99p")))

(define-public crate-criterion-perf-events-0.4.0 (c (n "criterion-perf-events") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.0") (d #t) (k 0)) (d (n "perfcnt") (r "^0.8.0") (d #t) (k 0)))) (h "05rggh4abyljys0wppyqbbcw6r7iazzrrpr4w9jphyhz3qc0nbwh") (r "1.64")))

