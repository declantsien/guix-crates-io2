(define-module (crates-io cr it criterion-decimal-throughput) #:use-module (crates-io))

(define-public crate-criterion-decimal-throughput-1.0.0 (c (n "criterion-decimal-throughput") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "18i3gchi78vzd7d3wy4cp1iydmz8shg7yyxibsmsnxd949x5i77i")))

(define-public crate-criterion-decimal-throughput-1.0.1 (c (n "criterion-decimal-throughput") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1pxi3alz2jy2g363f7is1abdkcdsscxhywgccpn30wmarxsmkwb3")))

(define-public crate-criterion-decimal-throughput-1.0.2 (c (n "criterion-decimal-throughput") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1hc7fkn9l8y6j6r6zkgg9qyarcij39xrcxkpss7im76a40w5i6ka")))

