(define-module (crates-io cr it criterion-linux-perf) #:use-module (crates-io))

(define-public crate-criterion-linux-perf-0.1.0 (c (n "criterion-linux-perf") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 0)) (d (n "perf-event") (r "^0.4.5") (d #t) (k 0)))) (h "0qv1bwhrl82qfmm3pkzc00dg8a59cp9m40g2xn942bg4yzxiqm1i")))

