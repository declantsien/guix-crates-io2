(define-module (crates-io cr it criterion-plot) #:use-module (crates-io))

(define-public crate-criterion-plot-0.1.0 (c (n "criterion-plot") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "cast") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.7") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 2)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "0k9wwm7snnfnvq2shaijzxv1fgkablhvddnnx8vwn5fa5kzwzq06")))

(define-public crate-criterion-plot-0.1.1 (c (n "criterion-plot") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.1.30") (d #t) (k 2)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "13sb21ly9jksg7maigf97d9dabq4xsvasj98p8g1rvc258gdf6ir")))

(define-public crate-criterion-plot-0.1.2 (c (n "criterion-plot") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.1.30") (d #t) (k 2)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "00xz29904jzjlqwch558hsqlnpzqn7bb9n7ds143m318r0rxl8nd")))

(define-public crate-criterion-plot-0.1.3 (c (n "criterion-plot") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cast") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.1.30") (d #t) (k 2)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "0mqra32mdhrhcvjmf8fljgqs0h7awms6c4k6nib4k7azhvvk2m48")))

(define-public crate-criterion-plot-0.2.0 (c (n "criterion-plot") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.1.30") (d #t) (k 2)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)))) (h "050qcf671zz6mds50x8843illv7m7js27x5c9v1ihhn84viy5yxv")))

(define-public crate-criterion-plot-0.2.1 (c (n "criterion-plot") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "15xlmrzydqc9ylvdhl7c0mllgm64gflw79c4rcxpjckjdh59mxpj")))

(define-public crate-criterion-plot-0.2.2 (c (n "criterion-plot") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "19kdw3spifxabi3qbfy4yncivgkrdh6dzy4z88hsbc3brnx19682")))

(define-public crate-criterion-plot-0.2.3 (c (n "criterion-plot") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1ripw22cgckk522r3d5gg7djhi525k5746p3v6gxshfkm247qzwz")))

(define-public crate-criterion-plot-0.2.4 (c (n "criterion-plot") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "05j75ifxmiyyli1mswy9plyybdvg24j5qrd6ygfkf0w2l9xbg53q")))

(define-public crate-criterion-plot-0.2.5 (c (n "criterion-plot") (v "0.2.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0ksg1kyg53fjmzrhim2ycp2nycvik8q1lmjrqr7bkgfvmim9sr3f")))

(define-public crate-criterion-plot-0.2.6 (c (n "criterion-plot") (v "0.2.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0icdq4vw9l2r12h5p1ibqpfb5cvm1j825cv733xaim7zwlql60pi") (y #t)))

(define-public crate-criterion-plot-0.2.7 (c (n "criterion-plot") (v "0.2.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "048la78qg7kna7s5zq4gwb2bbnivqrp2rdm6wzn67r1rjxl82pyx")))

(define-public crate-criterion-plot-0.3.0 (c (n "criterion-plot") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "05jbvkdiwghdzkdhz0h2q9yahw6w97zvh8lr2kh6fhmrmfjy81s1")))

(define-public crate-criterion-plot-0.3.1 (c (n "criterion-plot") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "13pv09z4ryp70qyzablkibwa2mh6c2852qq1sjr9wjigvwnj3ybn")))

(define-public crate-criterion-plot-0.4.0 (c (n "criterion-plot") (v "0.4.0") (d (list (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "18kjl0fh2n5ws6ssiqskikmz893dm9rfdgi5j2l2qddyig7cdkgc")))

(define-public crate-criterion-plot-0.4.1 (c (n "criterion-plot") (v "0.4.1") (d (list (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0id5sfww0hjxlzvkzacdlgbls3lxza8iysqljr7j7s2qxbh1a7m0")))

(define-public crate-criterion-plot-0.4.2 (c (n "criterion-plot") (v "0.4.2") (d (list (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0q4bc51caf597kq6azy1x8pn64yp7q7i2sm2f7cf3wh0kycggsnx")))

(define-public crate-criterion-plot-0.4.3 (c (n "criterion-plot") (v "0.4.3") (d (list (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "17c8v5fv064181yspagkdcfd6jhs7233ba6g94bbl7v0xjnzw8p0")))

(define-public crate-criterion-plot-0.4.4 (c (n "criterion-plot") (v "0.4.4") (d (list (d (n "cast") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0mys2zkizh5az6ax77m5aqifk0vz35rn0a6wykvmjx9gkzg9c2fh")))

(define-public crate-criterion-plot-0.4.5 (c (n "criterion-plot") (v "0.4.5") (d (list (d (n "cast") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xhq0jz1603585h7xvm3s4x9irmifjliklszbzs4cda00y1cqwr6")))

(define-public crate-criterion-plot-0.5.0 (c (n "criterion-plot") (v "0.5.0") (d (list (d (n "cast") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 2)) (d (n "num-complex") (r "^0.4") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1c866xkjqqhzg4cjvg01f8w6xc1j3j7s58rdksl52skq89iq4l3b")))

