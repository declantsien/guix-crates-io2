(define-module (crates-io cr it criterion-cycles-per-byte) #:use-module (crates-io))

(define-public crate-criterion-cycles-per-byte-0.1.0 (c (n "criterion-cycles-per-byte") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jkpn66d83j40252n2qsi3zl91s5a12v3zx8s78qr8hszb95xn7z") (y #t)))

(define-public crate-criterion-cycles-per-byte-0.1.1 (c (n "criterion-cycles-per-byte") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 0)))) (h "1pfl6wk8ysq8p4sp8k4jjvsdh7hpglfcbxr314cwga76kvd42p2m")))

(define-public crate-criterion-cycles-per-byte-0.1.2 (c (n "criterion-cycles-per-byte") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 0)))) (h "15iw8zvyilx6k3a7z79vpzmpm6kkyds4c1ng3jlwfc43axd4hd4d")))

(define-public crate-criterion-cycles-per-byte-0.1.3 (c (n "criterion-cycles-per-byte") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 0)))) (h "1p6b9fz60ga5k14gp6gfjvvk5xykvn29lpwm5244y1f9cammw5q7")))

(define-public crate-criterion-cycles-per-byte-0.4.0 (c (n "criterion-cycles-per-byte") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 0)))) (h "1i92ccjc3lz3bfihlb8if1ilcxf6baw76vl2zd4w44b65hjlmi9b")))

(define-public crate-criterion-cycles-per-byte-0.5.0 (c (n "criterion-cycles-per-byte") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 0)))) (h "0a4nzlcr21n42wqhmrf367vbwn59ki7gfr56cg6hpa1c4swrr4hk")))

(define-public crate-criterion-cycles-per-byte-0.6.0 (c (n "criterion-cycles-per-byte") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 0)))) (h "1zz2q5v8wgr66xfac8c16iq68i53xx2j0b4l2iz3kwmq8haid0aj")))

