(define-module (crates-io cr it critfail) #:use-module (crates-io))

(define-public crate-critfail-0.3.0 (c (n "critfail") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0q892p2qxpvzqiaiv3q0ngvvvgmsh9zqf55y7r4rcga7xd81qj54") (f (quote (("wasm-bindgen" "rand/wasm-bindgen"))))))

