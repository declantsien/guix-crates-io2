(define-module (crates-io cr it criterion-inverted-throughput) #:use-module (crates-io))

(define-public crate-criterion-inverted-throughput-0.1.0 (c (n "criterion-inverted-throughput") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "0c6prv8pc7fgwfxyc7r1dnlp7681rpriqdb38njqykch0zr7yzbf")))

