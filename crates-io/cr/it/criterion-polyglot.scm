(define-module (crates-io cr it criterion-polyglot) #:use-module (crates-io))

(define-public crate-criterion-polyglot-0.1.0 (c (n "criterion-polyglot") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.76") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "test-binary") (r "^2.0.0") (d #t) (k 2)))) (h "1m03llyy3vdnsbg8cp31qwabzw5kzjld53v6w2jzwwlnmcp33c09") (f (quote (("default" "c") ("c" "cc"))))))

