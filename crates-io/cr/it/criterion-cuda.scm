(define-module (crates-io cr it criterion-cuda) #:use-module (crates-io))

(define-public crate-criterion-cuda-0.2.0 (c (n "criterion-cuda") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "cust") (r "^0.3") (d #t) (k 0)) (d (n "cust_core") (r "^0.1") (d #t) (k 0)) (d (n "cust_derive") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)))) (h "1mkp8xin4qhfbrxvsd39wyss0k5k4ihfgvwqgrba02f0slxhhli5")))

(define-public crate-criterion-cuda-0.2.1 (c (n "criterion-cuda") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 0)) (d (n "cust") (r "^0.3") (d #t) (k 0)) (d (n "cust_core") (r "^0.1") (d #t) (k 0)) (d (n "cust_derive") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)))) (h "189ibcqbmkgk6vifjc18nq79yvlhzd4cxak7jklcnxjpyw7nyvp0")))

