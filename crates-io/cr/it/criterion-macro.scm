(define-module (crates-io cr it criterion-macro) #:use-module (crates-io))

(define-public crate-criterion-macro-0.3.0 (c (n "criterion-macro") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0n07qmdl3xw1r4jd114ff08w0vahpiwrx6j1jdxcr98q5xnn87ny")))

(define-public crate-criterion-macro-0.3.1 (c (n "criterion-macro") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.1") (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0wnmfp4m178aiy90df607bln3f0x9pdqd0zjvpq0r0qh50w4p1sg")))

(define-public crate-criterion-macro-0.3.2 (c (n "criterion-macro") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.2") (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "041x2b6a55pr1v4x8bmf5sjprn5wg9bdxr3kqxhmll211imzilky")))

(define-public crate-criterion-macro-0.3.3 (c (n "criterion-macro") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3.3") (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "02bfy6j6w89frsqhmzvy9pfnpdzz0fq8m2v29zqqkbpa4dk0g26a")))

(define-public crate-criterion-macro-0.3.4 (c (n "criterion-macro") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3.4") (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0r4158rpvahxlw07nxy5fkx49krgvlmj7dxz9sr0n1b05s6c08c4")))

(define-public crate-criterion-macro-0.4.0 (c (n "criterion-macro") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1z3zbrk6y711v96jaalipzrs3sx0q0361ivjspdxf6can8v8z2i8")))

