(define-module (crates-io cr it criterion_bencher_compat) #:use-module (crates-io))

(define-public crate-criterion_bencher_compat-0.2.5 (c (n "criterion_bencher_compat") (v "0.2.5") (d (list (d (n "criterion") (r "^0.2.5") (k 0)))) (h "1h7nlyvczkw039bz34qlqhlh7hxqrn2q540m7m9dng9vqnds25nv") (f (quote (("real_blackbox" "criterion/real_blackbox") ("html_reports" "criterion/html_reports") ("default" "html_reports"))))))

(define-public crate-criterion_bencher_compat-0.2.6 (c (n "criterion_bencher_compat") (v "0.2.6") (d (list (d (n "criterion") (r "^0.2.6") (k 0)))) (h "08xdms69s423s9hyzd064b952d5hfjynaw7mxs3pp2fjygbzkkws") (f (quote (("real_blackbox" "criterion/real_blackbox") ("html_reports" "criterion/html_reports") ("default" "html_reports")))) (y #t)))

(define-public crate-criterion_bencher_compat-0.3.0 (c (n "criterion_bencher_compat") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (k 0)))) (h "1r371jlfwh88lrkpd4r5i799bfyc4nlcjmm54skpr7zkm135qgf2") (f (quote (("real_blackbox" "criterion/real_blackbox") ("default"))))))

(define-public crate-criterion_bencher_compat-0.3.1 (c (n "criterion_bencher_compat") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.1") (k 0)))) (h "10y03jvwr5gpcnpdiprws6zfamc0sd3f036ckd4fqbvcfk2cvli5") (f (quote (("real_blackbox" "criterion/real_blackbox") ("default"))))))

(define-public crate-criterion_bencher_compat-0.3.2 (c (n "criterion_bencher_compat") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.2") (k 0)))) (h "1s1ykyhv90v2mmzqdmv3xwpyknbjm0vfpbv0y63qp7szaw8j4ll9") (f (quote (("real_blackbox" "criterion/real_blackbox") ("default"))))))

(define-public crate-criterion_bencher_compat-0.3.3 (c (n "criterion_bencher_compat") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3.3") (k 0)))) (h "0wgfjr4mpgz6jz5dz7pzbcydqqx84glirn3vxza1jassakxhbr27") (f (quote (("real_blackbox" "criterion/real_blackbox") ("default"))))))

(define-public crate-criterion_bencher_compat-0.3.4 (c (n "criterion_bencher_compat") (v "0.3.4") (d (list (d (n "criterion") (r "^0.3.4") (k 0)))) (h "1vfi4gn9y3f3r8535h1vwbx0n15116shp7221xz0krs6fk42mffd") (f (quote (("real_blackbox" "criterion/real_blackbox") ("default"))))))

(define-public crate-criterion_bencher_compat-0.4.0 (c (n "criterion_bencher_compat") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (k 0)))) (h "160fk1gkxk3z66ydsaiax44w597yz517n15lr6g1mshl0zw2bf1i") (f (quote (("real_blackbox" "criterion/real_blackbox") ("default"))))))

