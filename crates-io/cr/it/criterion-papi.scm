(define-module (crates-io cr it criterion-papi) #:use-module (crates-io))

(define-public crate-criterion-papi-0.1.0 (c (n "criterion-papi") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpapi_sys") (r "^0.1") (d #t) (k 0)))) (h "0cdrxjwfkwcf1h8wwpa5r40s6iqhmicakb7ga5kycr1aha02qh7x")))

