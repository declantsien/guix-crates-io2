(define-module (crates-io cr _p cr_program_settings) #:use-module (crates-io))

(define-public crate-cr_program_settings-0.1.2 (c (n "cr_program_settings") (v "0.1.2") (d (list (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0qlvxbzndkjd5knmvab28gr6z2g04a28paf09nj6a87w786lbag8")))

