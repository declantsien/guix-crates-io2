(define-module (crates-io cr nd crndm_derive) #:use-module (crates-io))

(define-public crate-crndm_derive-0.1.0 (c (n "crndm_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut" "parsing"))) (d #t) (k 0)))) (h "0s8dz6fklz407gr2kkhcip583hpn3vr410hsy2xivvd1bnmsxarc")))

(define-public crate-crndm_derive-0.1.1 (c (n "crndm_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut" "parsing"))) (d #t) (k 0)))) (h "1hc613i76psfzm123qdzmdkmdvihzmj7dk9admh7w8l36wk03vqn")))

