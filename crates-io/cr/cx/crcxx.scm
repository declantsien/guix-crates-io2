(define-module (crates-io cr cx crcxx) #:use-module (crates-io))

(define-public crate-crcxx-0.2.0 (c (n "crcxx") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ngad2dfh893ynq65pbkb9yyzfgimfrcrj3ly1bp541ij63vymwb") (f (quote (("slice-by-8") ("slice-by-4") ("slice-by-32") ("slice-by-16") ("default" "slice-by-16")))) (y #t)))

(define-public crate-crcxx-0.2.1 (c (n "crcxx") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "07amxzif77s9d96hi14m2vg04315407z7m71jz6x6ra2jhh6zni8") (f (quote (("slice-by-8") ("slice-by-4") ("slice-by-32") ("slice-by-16") ("default" "slice-by-16")))) (y #t)))

(define-public crate-crcxx-0.3.0 (c (n "crcxx") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1x74s5dw4bd66srbq7s64kh33m4gl045fack711gig1pyvmv2431") (r "1.59")))

(define-public crate-crcxx-0.3.1 (c (n "crcxx") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0pkdh02sy7sjaama5677kzhhq7hvzan7dshhim2fnm3f9w3zw4jv") (r "1.59")))

