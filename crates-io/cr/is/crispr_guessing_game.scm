(define-module (crates-io cr is crispr_guessing_game) #:use-module (crates-io))

(define-public crate-crispr_guessing_game-0.1.0 (c (n "crispr_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "19j9d407v6nv1n7d8jbckcs8k1mpp0avhhxj4mvx6r8jcwps45ry")))

(define-public crate-crispr_guessing_game-1.0.0 (c (n "crispr_guessing_game") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09qgq1n6l9wymz92zn71yrjl2cvr4y4p0l9hvlx9vsi1ncm72vjg")))

(define-public crate-crispr_guessing_game-1.1.0 (c (n "crispr_guessing_game") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1lkib720034gm1vqmil5brh9f4d4gwfwak6khyg87y2mvfxwx94w")))

(define-public crate-crispr_guessing_game-1.1.1 (c (n "crispr_guessing_game") (v "1.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "189iasn9x3k9vxhi7vpzrymqwbb6z9yr8sfb682wd85ming90p2a")))

(define-public crate-crispr_guessing_game-1.1.2 (c (n "crispr_guessing_game") (v "1.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hpppl68638k5vd2hz3w0xf13sm9d9zx3ww1g3vfmqycvzfidfm6")))

(define-public crate-crispr_guessing_game-1.2.1 (c (n "crispr_guessing_game") (v "1.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "147qs1sf0l53rrydmvm329qjygicgy52w4yyr16l33jm31b6lzmf")))

