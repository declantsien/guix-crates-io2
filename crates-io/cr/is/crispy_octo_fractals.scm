(define-module (crates-io cr is crispy_octo_fractals) #:use-module (crates-io))

(define-public crate-crispy_octo_fractals-0.5.0 (c (n "crispy_octo_fractals") (v "0.5.0") (d (list (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "kik_sync_service") (r "^0.7.3") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.117.0") (d #t) (k 0)))) (h "1llsxnwxsb166vhn77n0drq6rla7ifih9rpdx4ppp24jzkh0h2y8")))

(define-public crate-crispy_octo_fractals-0.5.1 (c (n "crispy_octo_fractals") (v "0.5.1") (d (list (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "kik_sync_service") (r "^0.7.3") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.117.0") (d #t) (k 0)))) (h "1b9w3r19rgky1pbayigfxyrlxgv83nms7qxnq2h5x8axwgfkffw9")))

