(define-module (crates-io cr is crispr_helloworld) #:use-module (crates-io))

(define-public crate-crispr_helloworld-0.1.0 (c (n "crispr_helloworld") (v "0.1.0") (h "0frm74zmqk7i96a0cnvj7a718jiy9p10hxq06q6lnfl73vxq5f2v")))

(define-public crate-crispr_helloworld-0.1.1 (c (n "crispr_helloworld") (v "0.1.1") (h "1sdjzh8v50wnvs0zajy7d7hbpx854hrx826kisqbqlj6a4fqgad6")))

