(define-module (crates-io cr in cringe) #:use-module (crates-io))

(define-public crate-cringe-0.0.0 (c (n "cringe") (v "0.0.0") (d (list (d (n "npm") (r "^0.0.0") (d #t) (k 0)))) (h "1fdfvgz2kw9d3akhvgf9iipf8cmjrr36pl2ppnifkgki6lv6fhg0")))

(define-public crate-cringe-0.1.0 (c (n "cringe") (v "0.1.0") (d (list (d (n "npm") (r "^0.0.0") (d #t) (k 0)))) (h "0cq7bhjjl0gi6w9pm21k3b6mgmaa4qwn2pznsrpdn14ia8nnwh3a")))

