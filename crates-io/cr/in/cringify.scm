(define-module (crates-io cr in cringify) #:use-module (crates-io))

(define-public crate-cringify-0.1.0 (c (n "cringify") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1b2974flwl1804djp59fkpflmbsa8csgmapin95p7rrzbx884lx3")))

(define-public crate-cringify-0.1.1 (c (n "cringify") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1532fv8hki56z09ms98vig2s9i79kl66l2134pbkl2gdim44vyav")))

(define-public crate-cringify-0.2.0 (c (n "cringify") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "00zih8b4pd8qa7h9yxjv8dz37p2synk4i83c7f4vzkvy2dig77y6")))

