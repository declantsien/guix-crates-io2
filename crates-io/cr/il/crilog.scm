(define-module (crates-io cr il crilog) #:use-module (crates-io))

(define-public crate-crilog-0.1.0 (c (n "crilog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ghj5jrk17v3cr0h2r42f1ij1w6kzpwaakzk00bwnbkhfbwzdylk")))

