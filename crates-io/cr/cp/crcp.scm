(define-module (crates-io cr cp crcp) #:use-module (crates-io))

(define-public crate-crcp-0.1.0 (c (n "crcp") (v "0.1.0") (h "1yy4wllch5hvrrs3sb4y8fz9ycz7g2xcf76mp1n6r02s85g6aqrl")))

(define-public crate-crcp-0.2.0 (c (n "crcp") (v "0.2.0") (h "0h64ibdmqjwrgqbc9wlk1yj4jgq7l5y9ks1cwpkllsv8f3x7nzf9")))

(define-public crate-crcp-0.3.0 (c (n "crcp") (v "0.3.0") (h "1i2z5l7hqpvkc01q1cygq9fys3knaffb4gx7wvlk0bvyvj99ipzh")))

(define-public crate-crcp-0.3.1 (c (n "crcp") (v "0.3.1") (h "1mfa8vqj00qjmai44y76dxkk114hb2srr96zd3kc5k274j20mbws")))

(define-public crate-crcp-0.3.2 (c (n "crcp") (v "0.3.2") (h "047k0mglhwxbz4xzaq8g0bd16ynfvs8hhf8ax74a5ly0c7khvnqn")))

(define-public crate-crcp-0.3.3 (c (n "crcp") (v "0.3.3") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1mr36583yv96ldg5n7wjgr68p81lr5dq5hfckjailq69vfiy0vg8")))

(define-public crate-crcp-0.3.4 (c (n "crcp") (v "0.3.4") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0q6p9vdy1fll70wc9rm42nv25cb1yvp2bjkavqai8hl52w9xzazk")))

(define-public crate-crcp-0.3.5 (c (n "crcp") (v "0.3.5") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "043x2wqa94q0pbnwjadql7n96kvvrsxjns7ihb44xwhlza4bhyg4")))

(define-public crate-crcp-0.3.6 (c (n "crcp") (v "0.3.6") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "065kgwwypgrabls3kyc74y70fn0knswklmsv9a68djz9xkm4gkda")))

(define-public crate-crcp-0.3.7 (c (n "crcp") (v "0.3.7") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0f01zp12rmssdd7gvc66f2k9xlxfgc2411w0hjaiq1ws5kykwhys")))

(define-public crate-crcp-0.3.8 (c (n "crcp") (v "0.3.8") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1gzpy5c8z2qs7dx8pnjm7zs2vsh5mn5li8zgcglzgwvj1hcwjlk8")))

(define-public crate-crcp-0.3.9 (c (n "crcp") (v "0.3.9") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1dxl3fdi07al5b6sqv4crav80d5x6204rwzs86ar1wyk73p911bx")))

(define-public crate-crcp-0.3.10 (c (n "crcp") (v "0.3.10") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1j2cnlrdfdwrqxdxcrdsixqzv75b3pfbg8vbmxq6h2zyb436fh1k")))

(define-public crate-crcp-0.3.11 (c (n "crcp") (v "0.3.11") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0mq5whdzvmrrybgncccbladfvzvxqzj98imxls6zcnndzd27syvw")))

(define-public crate-crcp-0.3.12 (c (n "crcp") (v "0.3.12") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1ya89v5f6ipc0i5yy1frr0yy6872bmr5v4dl41amamdny20d1q1g")))

