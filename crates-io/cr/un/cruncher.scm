(define-module (crates-io cr un cruncher) #:use-module (crates-io))

(define-public crate-cruncher-0.1.0 (c (n "cruncher") (v "0.1.0") (d (list (d (n "cranelift") (r "^0.38") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.38") (d #t) (k 0)) (d (n "cranelift-simplejit") (r "^0.38") (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "0mnk33rrdsbgnww4zmcnpj1rv69mcmmycp4qxpfvmdgv53gfd9xa")))

(define-public crate-cruncher-0.2.0 (c (n "cruncher") (v "0.2.0") (d (list (d (n "cranelift") (r "^0.56") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.56") (d #t) (k 0)) (d (n "cranelift-simplejit") (r "^0.56") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "1ldpqdi1xc9xczq9y71wp0slwhmxi29n130vpkrfhlph3sr8d0nd")))

(define-public crate-cruncher-0.2.1 (c (n "cruncher") (v "0.2.1") (d (list (d (n "cranelift") (r "^0.56") (d #t) (k 0)) (d (n "cranelift-module") (r "^0.56") (d #t) (k 0)) (d (n "cranelift-simplejit") (r "^0.56") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "0wk9bybiyrfr9g4cn8fa8ahhjmprf4xzdjq00qhrkgv4s6jzrwy3")))

