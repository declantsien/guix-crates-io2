(define-module (crates-io cr un crunch64-cli) #:use-module (crates-io))

(define-public crate-crunch64-cli-0.2.0 (c (n "crunch64-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crunch64") (r "^0.2.0") (d #t) (k 0)))) (h "1dv673iq625qjy6rq28aslalrd47d21s2z83cx7vsxfi1vyw1nv3")))

(define-public crate-crunch64-cli-0.3.0 (c (n "crunch64-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crunch64") (r "^0.3.0") (d #t) (k 0)))) (h "1bx9i9x9zg1yp9sq1kq6zjc73c4knfva5mvlm0im4pibmpwvn09s")))

(define-public crate-crunch64-cli-0.3.1 (c (n "crunch64-cli") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crunch64") (r "^0.3.0") (d #t) (k 0)))) (h "1ffq7rcz9qjm13k09rv52fv3nhpqmgi5jd81k8cy4ph6w259pqrh")))

