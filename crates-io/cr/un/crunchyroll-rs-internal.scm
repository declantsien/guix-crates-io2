(define-module (crates-io cr un crunchyroll-rs-internal) #:use-module (crates-io))

(define-public crate-crunchyroll-rs-internal-0.1.0 (c (n "crunchyroll-rs-internal") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0adgl1np0wp39dd4s1hbynn0h7rvm3hsyb14532xj5s0ippf49y9")))

(define-public crate-crunchyroll-rs-internal-0.2.0 (c (n "crunchyroll-rs-internal") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x09fpi1pvnklz43psc2qx56ab726nnbynj5551z8xpx14gsmr51")))

(define-public crate-crunchyroll-rs-internal-0.2.1 (c (n "crunchyroll-rs-internal") (v "0.2.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y2xqkdr4pslh4r3bj8lqacy5b5pig2ngbr4qd5qwjgp6ix9bgk6")))

(define-public crate-crunchyroll-rs-internal-0.2.2 (c (n "crunchyroll-rs-internal") (v "0.2.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17ahi174nprg325kk9qg7a5cryyv0gw1n9vh7ncy0p4dh5qbim0h")))

(define-public crate-crunchyroll-rs-internal-0.2.3 (c (n "crunchyroll-rs-internal") (v "0.2.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15qribyppksq4dpr24va04qbnjnajb5cdblmy218pk6dnpr6dnxa") (y #t)))

(define-public crate-crunchyroll-rs-internal-0.2.4 (c (n "crunchyroll-rs-internal") (v "0.2.4") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xl7807n2ywrjffkz6g28b7v20ghqk86c3f1q9zp4fb3fvhq4g2z")))

(define-public crate-crunchyroll-rs-internal-0.2.5 (c (n "crunchyroll-rs-internal") (v "0.2.5") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mrg5br9w7j80732gax05v6c3lv8x7asxjj30gihrfrkwxrhl9ja")))

(define-public crate-crunchyroll-rs-internal-0.3.0 (c (n "crunchyroll-rs-internal") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s26ga23wa66hqq197v8ggyx8sbyzazw4ic5g373jcfv6qdvf9iz")))

(define-public crate-crunchyroll-rs-internal-0.3.1 (c (n "crunchyroll-rs-internal") (v "0.3.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13sc92g8kryyd805gags9mp1f8rrrnadnfr4bbf0pk8y4dq2qphy")))

(define-public crate-crunchyroll-rs-internal-0.3.2 (c (n "crunchyroll-rs-internal") (v "0.3.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05xqavp5mjvbpj6yk6ld9qza7xy7p2h9qnsx7wdqk50z3divr44x")))

(define-public crate-crunchyroll-rs-internal-0.3.3 (c (n "crunchyroll-rs-internal") (v "0.3.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iwc2myyhlc2q8dfd8ypfyvac7pxarajy6skw2n65i1q70s1mmxq")))

(define-public crate-crunchyroll-rs-internal-0.3.4 (c (n "crunchyroll-rs-internal") (v "0.3.4") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kp5ah9w75ddy8wni20f64hhblkhb7yixkqzd9h0yx85lmw9j6nr")))

(define-public crate-crunchyroll-rs-internal-0.3.5 (c (n "crunchyroll-rs-internal") (v "0.3.5") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02zgzsjrbf41jvxjlialw8j6i2npyzpc539hqm6c969qla3wz817")))

(define-public crate-crunchyroll-rs-internal-0.3.6 (c (n "crunchyroll-rs-internal") (v "0.3.6") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14da78jw4fm3b71r5cjf9nv9ibk9rgzhi7233vw040c5a3yp27ia")))

(define-public crate-crunchyroll-rs-internal-0.3.7 (c (n "crunchyroll-rs-internal") (v "0.3.7") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a5kz3q3mdxmhir095r6i8y78ridv4lxgkmrq650krxpibk0nw6i")))

(define-public crate-crunchyroll-rs-internal-0.4.0 (c (n "crunchyroll-rs-internal") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pn7x5b0bw4d2kkgnv4zsfyknwpicqx5la814c733wi5506gf65p")))

(define-public crate-crunchyroll-rs-internal-0.5.0 (c (n "crunchyroll-rs-internal") (v "0.5.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01qag0kq0gxy2mw0snnw31hc829z7dnzm4davhkw7wbnfbf8359g")))

(define-public crate-crunchyroll-rs-internal-0.5.1 (c (n "crunchyroll-rs-internal") (v "0.5.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0snfk9fs26njb5jqnpinwpg3qjlwlaqm1fyh7pfa6rjhjypy9cya")))

(define-public crate-crunchyroll-rs-internal-0.6.0 (c (n "crunchyroll-rs-internal") (v "0.6.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lim0c63jxpw1ilk2arkryfi6ji2i9h8z4amcjpdaprgv34b5mib")))

(define-public crate-crunchyroll-rs-internal-0.6.1 (c (n "crunchyroll-rs-internal") (v "0.6.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wl1xd6dqvgsgm3ks81cfxgw4x4yjzvj656kq444y28hrl3qhfdi")))

(define-public crate-crunchyroll-rs-internal-0.6.2 (c (n "crunchyroll-rs-internal") (v "0.6.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zj2vbi0aiicqqsmm3i45l66np6y714m4wahwcqvmir524gijq5j")))

(define-public crate-crunchyroll-rs-internal-0.6.3 (c (n "crunchyroll-rs-internal") (v "0.6.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v4zlbaiq0n55h6swayg32ffwsjg8d63xbbxvbkfspfn9yqjz8gh")))

(define-public crate-crunchyroll-rs-internal-0.7.0 (c (n "crunchyroll-rs-internal") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q7vcrdailjybm8llrlgr0g194rh890qs9mfksdgqsnfya60z15y")))

(define-public crate-crunchyroll-rs-internal-0.8.0 (c (n "crunchyroll-rs-internal") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0da3b7iz34y0lz1xgsprbwqyrb2xdl5sax5njj140m03fl7a5l5x")))

(define-public crate-crunchyroll-rs-internal-0.8.1 (c (n "crunchyroll-rs-internal") (v "0.8.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d4kvxw1nsw3xbm6d58lpc43fhvbibqz5xa732wzpdhvrsnydfji")))

(define-public crate-crunchyroll-rs-internal-0.8.2 (c (n "crunchyroll-rs-internal") (v "0.8.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09qfzzvqilicwakpf6d0p07xp8w7jwbxmrsklw5fy6asw8wil1f7")))

(define-public crate-crunchyroll-rs-internal-0.8.3 (c (n "crunchyroll-rs-internal") (v "0.8.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f8b6xr7hrywi54v434y19ll9jdwxcamxflnma84r9brbdcwkwyi")))

(define-public crate-crunchyroll-rs-internal-0.8.4 (c (n "crunchyroll-rs-internal") (v "0.8.4") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sr4a0g8bfgy7j6phz9bjb8z8p02knv5wgd3f7prw3kh8kqrl5ii")))

(define-public crate-crunchyroll-rs-internal-0.8.5 (c (n "crunchyroll-rs-internal") (v "0.8.5") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fvsksj4iixf750aakb22vrrzwpz0s4ixvszghpjxp443xk45nv2")))

(define-public crate-crunchyroll-rs-internal-0.8.6 (c (n "crunchyroll-rs-internal") (v "0.8.6") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0359gmng4gizxg75xf6brw0lmlba1jayjr41kvnbnbpq5ndjdpfj")))

(define-public crate-crunchyroll-rs-internal-0.9.0 (c (n "crunchyroll-rs-internal") (v "0.9.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wdklylmjj55sd7p1q034jxc9vrj8l12l5wlhvsi37aar7kjxfb5")))

(define-public crate-crunchyroll-rs-internal-0.10.0 (c (n "crunchyroll-rs-internal") (v "0.10.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14dz9y5ql4alinjpqj4k9vrzyyqgdgciq401nrwv7slwr9za41nl")))

(define-public crate-crunchyroll-rs-internal-0.10.1 (c (n "crunchyroll-rs-internal") (v "0.10.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08syy2409zjvn630bp026h2n4qj15sd2jg58h64wqgd4vypjfxvz")))

(define-public crate-crunchyroll-rs-internal-0.10.2 (c (n "crunchyroll-rs-internal") (v "0.10.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zdn3bzcqa6ncli904x8q56mvfs538jf5igxdbfd3cqiawkjclky")))

(define-public crate-crunchyroll-rs-internal-0.10.3 (c (n "crunchyroll-rs-internal") (v "0.10.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04hc01iasgcnpfkl943mvzmyy4mb2qwgilvnm6wqv50cf09rfn1g")))

(define-public crate-crunchyroll-rs-internal-0.10.4 (c (n "crunchyroll-rs-internal") (v "0.10.4") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g283y7lmamqpkqr2nh6x1348sqxcih10125dyjh6qvazb8wvmr4")))

(define-public crate-crunchyroll-rs-internal-0.10.5 (c (n "crunchyroll-rs-internal") (v "0.10.5") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i5id397nacb79bjvqf67zdn9j3hld4in0ski8216gq65hj9w2zw")))

(define-public crate-crunchyroll-rs-internal-0.10.6 (c (n "crunchyroll-rs-internal") (v "0.10.6") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kf7d4p9qglni6y4vq3cs9lvls5x7fy2sv02r247pj4wih4yij3c")))

(define-public crate-crunchyroll-rs-internal-0.10.7 (c (n "crunchyroll-rs-internal") (v "0.10.7") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14jni84qf6fnl8gjjdas387qf8zfx3r5qm6n1i02nv19w1pvmnj8")))

(define-public crate-crunchyroll-rs-internal-0.10.8 (c (n "crunchyroll-rs-internal") (v "0.10.8") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0flwm9vs1hi2p4186d72jyaqdid3mq45nh15fjqmmhhyhx563hw5")))

(define-public crate-crunchyroll-rs-internal-0.11.0 (c (n "crunchyroll-rs-internal") (v "0.11.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n53ca6xvgl46lapgd6gmwx9zq1qdgfdmm7x8mb5cl942q4hx7is")))

(define-public crate-crunchyroll-rs-internal-0.11.1 (c (n "crunchyroll-rs-internal") (v "0.11.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09m3vr2qknq8w38bbf2p0vykhgawsvqmqfw5r7w90cwaxi6q8g6f")))

(define-public crate-crunchyroll-rs-internal-0.11.2 (c (n "crunchyroll-rs-internal") (v "0.11.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gymb4zvx7izbd3jp09g13hjg1rgb4mny04in197hr6agj1gl5fa")))

(define-public crate-crunchyroll-rs-internal-0.11.3 (c (n "crunchyroll-rs-internal") (v "0.11.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15yp8nv1r5vai2xflf2ywh5kq0nxcd90xmrvspvlap7jcm998lgs")))

