(define-module (crates-io cr un crunch64) #:use-module (crates-io))

(define-public crate-crunch64-0.1.0 (c (n "crunch64") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m4g1j5r29h3x7nq5dsqcs9yid2acx5q78bpk61ddr81gf6p405s") (f (quote (("c_bindings")))) (y #t) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

(define-public crate-crunch64-0.1.1 (c (n "crunch64") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0idcck7lnyr3ijk486jjb8jwxhs0dbbqkg995af8f7466y36idm6") (f (quote (("c_bindings")))) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

(define-public crate-crunch64-0.2.0 (c (n "crunch64") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01bzqbkx3y9vh3xdbzy033yi0ak1srdq43kcw9v913d3pgvpaqam") (f (quote (("c_bindings")))) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

(define-public crate-crunch64-0.3.0 (c (n "crunch64") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gv1jbzfsyal6pc47vyjg3hvac2pby5wy5184sfgdbvjjh5sxpmd") (f (quote (("c_bindings")))) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

(define-public crate-crunch64-0.3.1 (c (n "crunch64") (v "0.3.1") (d (list (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iky7caibfcs1fzzw748hk8yy3p3xw5w7cmhg14p9h3wcg00i28i") (f (quote (("c_bindings")))) (s 2) (e (quote (("python_bindings" "dep:pyo3"))))))

