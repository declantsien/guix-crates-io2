(define-module (crates-io cr un crunch_eval) #:use-module (crates-io))

(define-public crate-crunch_eval-0.1.0 (c (n "crunch_eval") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "evalexpr") (r "^8.1.0") (d #t) (k 2)))) (h "18f3pwys78vma1zzsvs3anyg1x9g9d7g8wjcrqfnxdwib0nd2cv6")))

(define-public crate-crunch_eval-0.1.1 (c (n "crunch_eval") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "evalexpr") (r "^8.1.0") (d #t) (k 2)))) (h "03k18af23n270ydr502p5n7kzvhb11bwrdvyj9z33qcmqfaqnh9s")))

