(define-module (crates-io cr un crunch) #:use-module (crates-io))

(define-public crate-crunch-0.1.0 (c (n "crunch") (v "0.1.0") (h "04j3764nr19fxh8g8qjm07b5pg3cd57zr56787miygm5hqbza3j5")))

(define-public crate-crunch-0.2.0 (c (n "crunch") (v "0.2.0") (h "09whdhiz4fdsb2f0ghf246sk4jvqb3zfw0s1dqxms5f1nhbl8w1s")))

(define-public crate-crunch-0.2.1 (c (n "crunch") (v "0.2.1") (h "1f3campcj5qivq8v5mdwwli1x9jgg9nfx124msb18xjvqwm7kg78")))

(define-public crate-crunch-0.2.2 (c (n "crunch") (v "0.2.2") (h "1s3j0dpnhlbw4ckbn9h6k15zsf77x0savk69r850ln4024inij02")))

(define-public crate-crunch-0.2.3 (c (n "crunch") (v "0.2.3") (h "08v5dk08i44m45li74l09xvrp9m5703z170mmamcj2a46b0x70d6")))

(define-public crate-crunch-0.2.4 (c (n "crunch") (v "0.2.4") (h "1lfarayzr96lzdkrvab22z2c5wzm7760vd07n9p6lv8342r15njw")))

(define-public crate-crunch-0.3.0 (c (n "crunch") (v "0.3.0") (h "1iw5fc1dgzw94yv5r7ynscf0a2d51g46qpqv8mgn5bvy9rivim3q")))

(define-public crate-crunch-0.3.1 (c (n "crunch") (v "0.3.1") (h "1vzn6lgqn2rgljnzprqikp3p1y6ds0r06qphsfabmwk48vn9favx")))

(define-public crate-crunch-0.3.2 (c (n "crunch") (v "0.3.2") (h "0w45bk4g4dgw5dnwv0hq5lygc5rgadgkbnf2x53gwyjv1vf1jxxk")))

(define-public crate-crunch-0.3.3 (c (n "crunch") (v "0.3.3") (h "0xpg1vdr3y1f80hx8c2rnl6fass1lc79j7n6hn8q7q3x5kmwxh8y")))

(define-public crate-crunch-0.5.0 (c (n "crunch") (v "0.5.0") (h "0ff8znxxaciw62s9nmvcms9afw0q20kqmzxvap6z1kc6p34y75n8")))

(define-public crate-crunch-0.5.1 (c (n "crunch") (v "0.5.1") (h "1mjqrqgn6sa07rmm3lsqdlp1sdmhif2hhcjaigijj7d4h68lh1m2")))

(define-public crate-crunch-0.5.2 (c (n "crunch") (v "0.5.2") (h "1lc1w4jhgxj4jrg6jmc2kmsyw0ldwlxfjh7m99asipm8f7arlpbh")))

(define-public crate-crunch-0.5.3 (c (n "crunch") (v "0.5.3") (h "09dlxjhj84swywmis98im0gdcryg9vrs29nyaasybgx31pki7h0d")))

