(define-module (crates-io cr un crunchy) #:use-module (crates-io))

(define-public crate-crunchy-0.1.0 (c (n "crunchy") (v "0.1.0") (h "0x81z7xdvjkmvlx0wsl2w13bqd6k3nqn8fflmra45dnl0z4xlsxm")))

(define-public crate-crunchy-0.1.1 (c (n "crunchy") (v "0.1.1") (h "0n2x1n5b9knmxw81hafvbdn1lz9f4s37b5rnfsv9vh1zdmkiacw0")))

(define-public crate-crunchy-0.1.2 (c (n "crunchy") (v "0.1.2") (h "1cax8yzml5ylsp4cv51jx0pla8dk07897y6bhbdpkv0snri5h95r")))

(define-public crate-crunchy-0.1.3 (c (n "crunchy") (v "0.1.3") (h "1gmb3s5rcky2illh4r9s1i7xqj858pz29ygsxi7grgypyasrrap6")))

(define-public crate-crunchy-0.1.4 (c (n "crunchy") (v "0.1.4") (h "1vfqfbw9qr5kslf24j34j8a41y3pbchdyfrn3vp0fkrz5s889wll")))

(define-public crate-crunchy-0.1.5 (c (n "crunchy") (v "0.1.5") (h "0qw2c48j93rn1qjy5jp7sf0kxl6jz2q8iyikd1z61agbzafkv2ic") (f (quote (("limit_64") ("limit_512") ("limit_256") ("limit_2048") ("limit_128") ("limit_1024") ("default" "limit_64"))))))

(define-public crate-crunchy-0.1.6 (c (n "crunchy") (v "0.1.6") (h "1nnbh2k3pv1diyd0i0p3a08kd4afqc17yz0b43hn5xn9qlqs9x52") (f (quote (("limit_64") ("limit_512") ("limit_256") ("limit_2048") ("limit_128") ("limit_1024") ("default" "limit_128"))))))

(define-public crate-crunchy-0.2.1 (c (n "crunchy") (v "0.2.1") (h "1wk145dr0k7ljjz6gfjikschsvpw5ci6l2l2sjk0iykqq93z4h62") (f (quote (("std") ("limit_64") ("limit_512") ("limit_256") ("limit_2048") ("limit_128") ("limit_1024") ("default" "limit_128"))))))

(define-public crate-crunchy-0.2.2 (c (n "crunchy") (v "0.2.2") (h "1dx9mypwd5mpfbbajm78xcrg5lirqk7934ik980mmaffg3hdm0bs") (f (quote (("std") ("limit_64") ("limit_512") ("limit_256") ("limit_2048") ("limit_128") ("limit_1024") ("default" "limit_128"))))))

