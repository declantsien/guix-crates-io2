(define-module (crates-io cr s- crs-definitions) #:use-module (crates-io))

(define-public crate-crs-definitions-0.1.0 (c (n "crs-definitions") (v "0.1.0") (h "1xg614s374vva51zbs57fwqaf223avz07jijp47yc7lyxvspfpr6")))

(define-public crate-crs-definitions-0.2.0 (c (n "crs-definitions") (v "0.2.0") (h "0lxyyvrr44rvira1x3kdp86qhckyjy7j1s6vwrgn4w6al4l9pylc") (f (quote (("wkt") ("proj4") ("default" "proj4" "wkt"))))))

(define-public crate-crs-definitions-0.3.0 (c (n "crs-definitions") (v "0.3.0") (h "1fqjkxy7w3l7dky2b2pl7ql2zyxxj2mvr54lakqxf5pfj6ack9aj") (f (quote (("wkt") ("proj4") ("default" "proj4" "wkt"))))))

