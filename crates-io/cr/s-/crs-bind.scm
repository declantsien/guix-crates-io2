(define-module (crates-io cr s- crs-bind) #:use-module (crates-io))

(define-public crate-crs-bind-0.1.0 (c (n "crs-bind") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc-print") (r "^0.1.14") (d #t) (k 0)) (d (n "rcstring") (r "^0.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 1)) (d (n "zip-extensions") (r "^0.6.0") (d #t) (k 1)))) (h "1cazg01ddih953w92c2km4z7l1r9galbs85kvx3zydhcy2jnyksc")))

(define-public crate-crs-bind-0.1.1 (c (n "crs-bind") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc-print") (r "^0.1.14") (d #t) (k 0)) (d (n "rcstring") (r "^0.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 1)) (d (n "zip-extensions") (r "^0.6.0") (d #t) (k 1)))) (h "0n12m0j7m32dniydym7v6k2vhpcc1pbqzgbzpm0qjdcy8g2hf1k5")))

(define-public crate-crs-bind-0.1.2 (c (n "crs-bind") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc-print") (r "^0.1.14") (d #t) (k 0)) (d (n "rcstring") (r "^0.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 1)) (d (n "zip-extensions") (r "^0.6.0") (d #t) (k 1)))) (h "1kikidfc1r3bh7zmilygdycwmiixy56rdx89245c3czbywqiis6d")))

(define-public crate-crs-bind-0.1.3 (c (n "crs-bind") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc-print") (r "^0.1.14") (d #t) (k 0)) (d (n "rcstring") (r "^0.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 1)) (d (n "zip-extensions") (r "^0.6.0") (d #t) (k 1)))) (h "14b718hn2110xqhfhvws31h1c93cmhmhi9wl76hk7nrcf1fpkdhv")))

(define-public crate-crs-bind-0.1.4 (c (n "crs-bind") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc-print") (r "^0.1.14") (d #t) (k 0)) (d (n "rcstring") (r "^0.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 1)) (d (n "zip-extensions") (r "^0.6.0") (d #t) (k 1)))) (h "1qvnxad1dcjl84wwcp0r725v729fc1lx0k3nr5f66j1iwcmc7nqv")))

(define-public crate-crs-bind-0.1.5 (c (n "crs-bind") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc-print") (r "^0.1.14") (d #t) (k 0)) (d (n "rcstring") (r "^0.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.7.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 1)) (d (n "zip-extensions") (r "^0.6.0") (d #t) (k 1)))) (h "0jl3gwi9h12yj5klyix9iaq2y2n4jpv9sfgvbbyl5k3dwhaadda4")))

