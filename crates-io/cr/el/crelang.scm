(define-module (crates-io cr el crelang) #:use-module (crates-io))

(define-public crate-crelang-0.100.0 (c (n "crelang") (v "0.100.0") (d (list (d (n "crelang-core") (r "^0.100.0") (d #t) (k 0)) (d (n "crelang-syn") (r "^0.100.0") (d #t) (k 0)) (d (n "libcre") (r "^0.100.0") (d #t) (k 0)))) (h "0vhbji0s1yk8szkg94yixrn776hlix6j8qb0av1aafh6sbx6k8lr")))

