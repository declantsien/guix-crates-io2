(define-module (crates-io cr oc crockford-uuid) #:use-module (crates-io))

(define-public crate-crockford-uuid-0.1.0 (c (n "crockford-uuid") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "15ia0pln4sg22gaavv1wsxml8mr9cln789aya79ixsmlgfc9svxy")))

(define-public crate-crockford-uuid-0.1.1 (c (n "crockford-uuid") (v "0.1.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "06i0nqvy8r20idqx7diqgnz57p831qvcs3iylfbcw1yf6r2i6p64")))

(define-public crate-crockford-uuid-0.1.2 (c (n "crockford-uuid") (v "0.1.2") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "16kvcx3m649mk8yyppcmm0s7g4xbzmssr9s65k76v21an8p9rcgb")))

