(define-module (crates-io cr oc crochet) #:use-module (crates-io))

(define-public crate-crochet-0.1.0 (c (n "crochet") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "detour") (r "^0.9") (k 0) (p "detour2")) (d (n "dlopen2") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef"))) (d #t) (t "cfg(windows)") (k 2)))) (h "06qsy2xpks14bchicvm0sh0wcwxy25dycjccbwckd89aqr8k6kma") (y #t)))

(define-public crate-crochet-0.1.1 (c (n "crochet") (v "0.1.1") (d (list (d (n "crochet_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "detour") (r "^0.9") (k 0) (p "detour2")) (d (n "dlopen2") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef"))) (d #t) (t "cfg(windows)") (k 2)))) (h "1dbrx756j5m2sf9zm16l34znyir4lq5ravjdnmaqs5c1vp27cxyf")))

(define-public crate-crochet-0.2.0 (c (n "crochet") (v "0.2.0") (d (list (d (n "crochet_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "detour") (r "^0.9") (k 0) (p "detour2")) (d (n "dlopen2") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef"))) (d #t) (t "cfg(windows)") (k 2)))) (h "06br4ydaq5a2f24ps9b5zv41f4y9jd4azjrv7yfaadkyhaxx8i8d")))

(define-public crate-crochet-0.2.1 (c (n "crochet") (v "0.2.1") (d (list (d (n "crochet_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "detour") (r "^0.9") (k 0) (p "detour2")) (d (n "dlopen2") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef"))) (d #t) (t "cfg(windows)") (k 2)))) (h "0v2w9xjfwxgbm5qk3k4831s4xbnk3sk7aryhip281hsx5hvf5jm4")))

(define-public crate-crochet-0.2.2 (c (n "crochet") (v "0.2.2") (d (list (d (n "crochet_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "detour") (r "^0.9") (k 0) (p "detour2")) (d (n "dlopen2") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef"))) (d #t) (t "cfg(windows)") (k 2)))) (h "09p141fbfw742lzlzvkyh1l4d8slknn04z89s89sq1zs6k53yjbi")))

(define-public crate-crochet-0.2.3 (c (n "crochet") (v "0.2.3") (d (list (d (n "crochet_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "detour") (r "^0.9") (k 0) (p "detour2")) (d (n "dlopen2") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef"))) (d #t) (t "cfg(windows)") (k 2)))) (h "1wgawbnn50lnhkh0j8l1x15habd01vfmnnvnix8sq594cav5fjzh")))

