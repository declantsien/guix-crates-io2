(define-module (crates-io cr oc crochet_macro) #:use-module (crates-io))

(define-public crate-crochet_macro-0.1.0 (c (n "crochet_macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "dlopen2") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hdxlgqllvwkgfynh63qj83mi9xskiw674cj0d34bchz86bzh9sf")))

(define-public crate-crochet_macro-0.2.0 (c (n "crochet_macro") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "dlopen2") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1acss31v9rjid4l9vzcg0fy2bjalmqg5wk371nfn59qpk76275ak")))

(define-public crate-crochet_macro-0.2.1 (c (n "crochet_macro") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "dlopen2") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m5baabfi0zmywbycc6pvm6g5g8ych36xz2xa8z2msgxz426brki")))

(define-public crate-crochet_macro-0.2.2 (c (n "crochet_macro") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "dlopen2") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m65lhfqkfrssil8r9s2dzgs8abgj2vnwixcl8g28bnxbsxbaxs8")))

(define-public crate-crochet_macro-0.2.3 (c (n "crochet_macro") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "dlopen2") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1854nw48jvyqv8fcypbxs5qdl79zqiymginqfb8rc193z4zahx0y")))

