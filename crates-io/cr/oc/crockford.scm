(define-module (crates-io cr oc crockford) #:use-module (crates-io))

(define-public crate-crockford-0.1.0 (c (n "crockford") (v "0.1.0") (h "1qswsj9mx8hbh0d4hifabv509gfzb48ipwadgnj6xcfxlimqlzyr")))

(define-public crate-crockford-1.0.0-beta.1 (c (n "crockford") (v "1.0.0-beta.1") (h "0arpya2ynjccbfzv7n3avh2mpzvggl9kijgz3wf7lafdnqyj1jn2")))

(define-public crate-crockford-1.0.0-beta.2 (c (n "crockford") (v "1.0.0-beta.2") (h "1zyhvj507sy84hmavrg61hzi4gm5q9wyrzkb16fdx4shh1air0dy")))

(define-public crate-crockford-1.0.0-beta.3 (c (n "crockford") (v "1.0.0-beta.3") (h "1kcjv4giknf45cpysyzyji1xknw0ng7ck1n8qy6ydi9p2wcnyq1a")))

(define-public crate-crockford-1.0.0 (c (n "crockford") (v "1.0.0") (h "0ih84zxv6v5jvcsmrzjgblfb6qjdc1qpj7bk1rp6yki78g6r5ixw")))

(define-public crate-crockford-1.0.1 (c (n "crockford") (v "1.0.1") (h "1v7x3i1fy63sk666mfzz3vyy7gjhhnc0hds76dznlh4a2fr974s7")))

(define-public crate-crockford-1.1.0 (c (n "crockford") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0mrg4325b5sv70l2a5qgv1sqskpjb46yw8cxnwwgkga05q0yv7ia")))

(define-public crate-crockford-1.1.1 (c (n "crockford") (v "1.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1z225agzdwxdiz6a3ay46a2gmm1nfm009yyjif33wsdql88f524j")))

(define-public crate-crockford-1.1.2 (c (n "crockford") (v "1.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "06cb950nzk3y0x9njxvw2d23jvxkdzd8ilcn3wjnb39279frx8x4")))

(define-public crate-crockford-1.2.0 (c (n "crockford") (v "1.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0ab31v5wd17msr6cc3hafxmrnzffbnfas3wjdvh99ybksb78rr65")))

(define-public crate-crockford-1.2.1 (c (n "crockford") (v "1.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0h7ijy3b6w799jbp2pbg0npx3xgcqhadz4kvnnl2wcbpg4p8qnqz")))

