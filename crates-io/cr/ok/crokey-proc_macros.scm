(define-module (crates-io cr ok crokey-proc_macros) #:use-module (crates-io))

(define-public crate-crokey-proc_macros-0.2.0 (c (n "crokey-proc_macros") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jik6fb2l6x2sqi9ldn06bnzqb35r58hcqfd3jh9w1hsz84dbmi2")))

(define-public crate-crokey-proc_macros-0.2.2 (c (n "crokey-proc_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1vw4kd2j64rwpyl3wy8zrigwdfkskxj0j7gfw3pzxqwgxx407rd7")))

(define-public crate-crokey-proc_macros-0.3.0 (c (n "crokey-proc_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1047lsql8n8aabn22sjma2fq9ar7i5bbbwz4p1y1c96azbvpr4w8")))

(define-public crate-crokey-proc_macros-0.3.1 (c (n "crokey-proc_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0y3pv4zzwzsnar7gj4p9ip3hf2s86j2a6d3m2dmydjdkygwhfh8i")))

(define-public crate-crokey-proc_macros-0.4.0 (c (n "crokey-proc_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0y5c874c2ah3lj6xmxqj0wa06ib13xpajc6gzgrwm291mbpk4j2d")))

(define-public crate-crokey-proc_macros-0.5.1 (c (n "crokey-proc_macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1ly3azdhv9h8v0gk42ns2jdc230ih3qcxhag5yv5rf9hnrijijbw")))

(define-public crate-crokey-proc_macros-0.6.0 (c (n "crokey-proc_macros") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strict") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0fc8pjgjb175r82zyfvvpaj0ll17fhh0c4pknxpdx4fyhd4sxjlr")))

(define-public crate-crokey-proc_macros-0.6.3 (c (n "crokey-proc_macros") (v "0.6.3") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strict") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "150qki26nnkc3fhl4h5vm1hvkpyi33gyk8dcd9a6y34xsc8g721m")))

(define-public crate-crokey-proc_macros-0.6.4 (c (n "crokey-proc_macros") (v "0.6.4") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strict") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1xx9x77f4dkci409pyann3z9hw7fh55a9nix0r5krycdkl03qz9r")))

