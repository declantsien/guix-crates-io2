(define-module (crates-io cr im crimson) #:use-module (crates-io))

(define-public crate-crimson-0.1.0 (c (n "crimson") (v "0.1.0") (d (list (d (n "crimson_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1icwy5qp3ka5nmn4rmbjh92qja66fq1jzxw45bv6q5sgnjvw7z5l") (y #t)))

(define-public crate-crimson-0.2.0 (c (n "crimson") (v "0.2.0") (d (list (d (n "crimson_utils") (r "^0.1.0") (d #t) (k 0)))) (h "02fy74yknxwz81y73aa8x5mrhnrf0lramjni63nws5hbg8m8n1dx") (y #t)))

(define-public crate-crimson-0.2.1 (c (n "crimson") (v "0.2.1") (d (list (d (n "crimson_utils") (r "^0.2.0") (d #t) (k 0)))) (h "0lvlsnny5fxwq9zp7k4xqa8awilixkwmq8zlbrqpw15grpvzhf8z") (y #t)))

