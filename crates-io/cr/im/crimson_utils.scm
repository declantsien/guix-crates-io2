(define-module (crates-io cr im crimson_utils) #:use-module (crates-io))

(define-public crate-crimson_utils-0.1.0 (c (n "crimson_utils") (v "0.1.0") (h "1p6w98kkljj6dyigi0kl4xvh9yhq652205dzfy09yix05pzp5hgz") (y #t)))

(define-public crate-crimson_utils-0.2.0 (c (n "crimson_utils") (v "0.2.0") (h "03jp13lrh3cr3id7fykp8q0qgbnh8p44jjv1mr6kkmi3r7fjzixa") (y #t)))

