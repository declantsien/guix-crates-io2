(define-module (crates-io cr im crimp) #:use-module (crates-io))

(define-public crate-crimp-0.1.0 (c (n "crimp") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ndi08qh6s87xbmvpsr614pr1k263plagh04pynmwyfidkpbwd7y") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-crimp-0.2.0 (c (n "crimp") (v "0.2.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "075sh2na65l7xfaycwnafjr6bjffhwqjf890f80wcza03119dk7z") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-crimp-0.2.2 (c (n "crimp") (v "0.2.2") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1527s14p0xwzsfgif40an5zjkgc72llamvdc7cp1m75d42izks5v") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-crimp-4087.0.0 (c (n "crimp") (v "4087.0.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0f9bw42mvdidxqgbnanp5k6rqxfis22mk8pplvjviyfiyy1jrb8f") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

