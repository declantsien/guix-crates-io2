(define-module (crates-io gr ot groth16-solana) #:use-module (crates-io))

(define-public crate-groth16-solana-0.0.1 (c (n "groth16-solana") (v "0.0.1") (d (list (d (n "ark-bn254") (r "^0.3.0") (d #t) (k 2)) (d (n "ark-ec") (r "^0.3.0") (d #t) (k 2)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 2)) (d (n "solana-program") (r "^1.15.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ljz4ysg671b1nw40a7ahiz50cm4flsaa9lzmgzmgcylnm9pjd82")))

(define-public crate-groth16-solana-0.0.2 (c (n "groth16-solana") (v "0.0.2") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 2)) (d (n "ark-ec") (r "^0.4.2") (d #t) (k 2)) (d (n "ark-ff") (r "^0.4.2") (d #t) (k 2)) (d (n "ark-serialize") (r "^0.4.2") (d #t) (k 2)) (d (n "solana-program") (r "^1.16.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k45wk75rsdc1y7fkz9a6lv5zd2lmvlyvnd0v9fv9y4c1igmjl2n")))

(define-public crate-groth16-solana-0.0.3 (c (n "groth16-solana") (v "0.0.3") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ec") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-serialize") (r "^0.4.2") (d #t) (k 0)) (d (n "ark-std") (r "^0.4.0") (d #t) (k 2)) (d (n "array-bytes") (r "^6.2.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "solana-program") (r "^1.17.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07c6f352zsgxzbpy926ll4cki7yh2ghi2indcwds8q81z6g5mimc")))

