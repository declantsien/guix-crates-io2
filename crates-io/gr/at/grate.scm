(define-module (crates-io gr at grate) #:use-module (crates-io))

(define-public crate-grate-0.1.0 (c (n "grate") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06p0sqv7wislkxjr26iiq3kdsyd4sc425bwlk9gzhlz1px0gag1d")))

(define-public crate-grate-0.1.1 (c (n "grate") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l4frxglj8gnmskfq3li0jmrmmb5m57h6xivww8rkrmw19w6v2l0")))

(define-public crate-grate-0.2.0 (c (n "grate") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v0p0y286zrbwns73dn7q0awsl3v2r5f4q9v344m5ls250pp29ac")))

(define-public crate-grate-1.0.0 (c (n "grate") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14ydm3kch3w92yi886yh5m1srka87yb9r9z0c7klmybda0a1q87a")))

