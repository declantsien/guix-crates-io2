(define-module (crates-io gr it grit-junk-drawer) #:use-module (crates-io))

(define-public crate-grit-junk-drawer-0.0.1 (c (n "grit-junk-drawer") (v "0.0.1") (h "1nab91dq68rkqpyaf7sp221jh99r2bwblv7kgc77a128psfch0w9") (y #t)))

(define-public crate-grit-junk-drawer-0.0.2 (c (n "grit-junk-drawer") (v "0.0.2") (h "1vasp8j8p71ar66lj67x4qkrz246fplnxlz87pmisa7w8xzbrmyd") (y #t)))

(define-public crate-grit-junk-drawer-0.0.3 (c (n "grit-junk-drawer") (v "0.0.3") (h "1dpcj6mx82w6xx76l9y80pmvkarvk8hzbd22yglzlglkmilfi370") (y #t)))

(define-public crate-grit-junk-drawer-0.0.4 (c (n "grit-junk-drawer") (v "0.0.4") (h "14pn8bj21swxs3hb9jyvxrncss1yj33ijadrmirbpvdnb8gkf0jb")))

(define-public crate-grit-junk-drawer-0.0.5 (c (n "grit-junk-drawer") (v "0.0.5") (h "0v7lhmj09jbs5z3invkljq75cdbvkd46rsh9bfg375sfqbiclyhi")))

