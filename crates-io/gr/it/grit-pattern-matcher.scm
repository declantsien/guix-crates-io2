(define-module (crates-io gr it grit-pattern-matcher) #:use-module (crates-io))

(define-public crate-grit-pattern-matcher-0.1.0 (c (n "grit-pattern-matcher") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "elsa") (r "^1.9.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "grit-util") (r "^0.1.0") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1f0i5xwh9whmpwrrasy4xcpfh03195fvh43cdkdcp2j14x6mgh3p")))

(define-public crate-grit-pattern-matcher-0.2.0 (c (n "grit-pattern-matcher") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "elsa") (r "^1.9.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (o #t) (d #t) (k 0)) (d (n "grit-util") (r "^0.2.0") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1hhqh2sbnlh5inymngc9gd8cdkdval1bkx6an3xrkbmi6c4pkjpp")))

