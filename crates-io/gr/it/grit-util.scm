(define-module (crates-io gr it grit-util) #:use-module (crates-io))

(define-public crate-grit-util-0.1.0 (c (n "grit-util") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vxlmlvnz1z1k2h33jfwh7i23l50xbxv39sg60jbbzw0wcjczg3z")))

(define-public crate-grit-util-0.2.0 (c (n "grit-util") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.13.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vsph3gc0hfzn9rrsgx2drc5ap4wykpk3zm30ri2n1crfki6dd7n")))

