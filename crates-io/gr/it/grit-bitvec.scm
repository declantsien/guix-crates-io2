(define-module (crates-io gr it grit-bitvec) #:use-module (crates-io))

(define-public crate-grit-bitvec-0.1.1 (c (n "grit-bitvec") (v "0.1.1") (h "1ilcsnf3x7fbxnr4knj3wibsax5vb1zd3x2pxr2ji8pcx5lsc1yp") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.2.0 (c (n "grit-bitvec") (v "0.2.0") (h "0h1g2cxxkhc9d0xplvqbja9chpbjr0ij8n8w4c3dr7z465m3wlj3") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.3.0 (c (n "grit-bitvec") (v "0.3.0") (h "0g9vpav61645qv3byk9y10md7v8pj8c0986vs1jvm84w5m61nddm") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.3.1 (c (n "grit-bitvec") (v "0.3.1") (h "0fjbspnsp008vqbqsdz0933cbbdm9pjzsynn5f4w9w2mdkxkip52") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.3.2 (c (n "grit-bitvec") (v "0.3.2") (h "1bqa8lyyzylhcd2bqc4n2cklav7p04xj2gc2q1ss0in711fzxag4") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.3.3 (c (n "grit-bitvec") (v "0.3.3") (h "0bsmcv2whrhh94zp2v6z5srqzgwzma039kdmn32hg8a3a1vki0pc") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.3.4 (c (n "grit-bitvec") (v "0.3.4") (h "1v7fv2rp82s26f2aii1smpxiq1px16k7r3503b1x0y6kyhh3sklc") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.3.5 (c (n "grit-bitvec") (v "0.3.5") (h "1bp9i756zn2zl08fgw7yv331q50hfv6kbx845l3kyv8x99hbiz7g") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.3.6 (c (n "grit-bitvec") (v "0.3.6") (h "04p02dcpxajqhsn9v259zcbykp5jg3i1x09qayxdsn70czah250v") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls")))) (y #t)))

(define-public crate-grit-bitvec-0.4.0 (c (n "grit-bitvec") (v "0.4.0") (h "03irnv41dxx0bnnpx2l1g35pi3d1vw82ivs4pm7bl9lr3rlfi0vj") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls"))))))

(define-public crate-grit-bitvec-0.4.1 (c (n "grit-bitvec") (v "0.4.1") (h "032c7jjkb0f225rjazdxxxnymjd7in6bgqmi5lri6s2h32h99dwq") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls"))))))

(define-public crate-grit-bitvec-0.4.2 (c (n "grit-bitvec") (v "0.4.2") (h "1grpfk426liblq4pbkkswli349fnpq0b247ax003x9vlg4w2p4f4") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls"))))))

(define-public crate-grit-bitvec-0.4.3 (c (n "grit-bitvec") (v "0.4.3") (h "0w54hmh11cgkq3iqms09cvz04pvfr1m7gg5fyj3gjhdjmc50xycb") (f (quote (("small_int_impls") ("large_int_impls") ("default" "small_int_impls"))))))

