(define-module (crates-io gr it grit-data-prison) #:use-module (crates-io))

(define-public crate-grit-data-prison-0.1.0 (c (n "grit-data-prison") (v "0.1.0") (h "1rv6c6mlaz2mzwlx0aq71mf8w8yn81r2jc3x29gybrspwbr47y1d") (f (quote (("no_std")))) (y #t)))

(define-public crate-grit-data-prison-0.1.1 (c (n "grit-data-prison") (v "0.1.1") (h "00ybaa7wcp56pkz9xn4grmy6mw70ks4yrn1hwfjby58i2hjx6gs9") (f (quote (("no_std")))) (y #t)))

(define-public crate-grit-data-prison-0.1.2 (c (n "grit-data-prison") (v "0.1.2") (h "0c661xryfayx2271ni23mdw22w6ngql8lzj53n7p2kybx2dfpbps") (f (quote (("no_std")))) (y #t)))

(define-public crate-grit-data-prison-0.2.0 (c (n "grit-data-prison") (v "0.2.0") (h "1gham64sxlvpkcpvzfsb5rb6r4h6sqqpxa19wr1pxq2ki5c6idrk") (f (quote (("no_std")))) (y #t)))

(define-public crate-grit-data-prison-0.2.1 (c (n "grit-data-prison") (v "0.2.1") (h "0rjm4qj7sw2lg5x7lbg62grlag49480jphq7rjfnqg6kslgajinw") (f (quote (("no_std")))) (y #t)))

(define-public crate-grit-data-prison-0.2.2 (c (n "grit-data-prison") (v "0.2.2") (h "1cz8zi3fjcpk2gr3jrk1bqk3l49402hw91mxpjwzd0b45wkmy46d") (f (quote (("no_std")))) (y #t)))

(define-public crate-grit-data-prison-0.2.3 (c (n "grit-data-prison") (v "0.2.3") (h "17c23gs9xz168divr68rdqxaixkys4wkhafs2ic41xkm6y8pzziz") (f (quote (("no_std")))) (y #t)))

(define-public crate-grit-data-prison-0.3.0 (c (n "grit-data-prison") (v "0.3.0") (h "03lhkxpaz2bcrvpaavm8a0ik1x31p6wbz24731187rmp0xzigbi5") (f (quote (("no_std") ("major_malf_is_undefined") ("major_malf_is_panic") ("major_malf_is_err"))))))

(define-public crate-grit-data-prison-0.3.1 (c (n "grit-data-prison") (v "0.3.1") (h "0wfp128s37rchxchfyvh2z7jn8ig292dh1ipqpc5if56ys70hcyd") (f (quote (("no_std") ("major_malf_is_undefined") ("major_malf_is_panic") ("major_malf_is_err"))))))

(define-public crate-grit-data-prison-0.4.0 (c (n "grit-data-prison") (v "0.4.0") (h "0w2qcn2p0hb7zpb6vmk5qnnfja35kj0ynr2qj30a9kg9l4x5wgy4") (f (quote (("no_std") ("major_malf_is_undefined") ("major_malf_is_panic") ("major_malf_is_err"))))))

