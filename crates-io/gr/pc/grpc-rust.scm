(define-module (crates-io gr pc grpc-rust) #:use-module (crates-io))

(define-public crate-grpc-rust-0.1.0 (c (n "grpc-rust") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "semver") (r "^0.4.0") (d #t) (k 0)))) (h "189k71w162h984vpbyyx64brh23d8llg4l9smgykx01k8djkyp3r")))

