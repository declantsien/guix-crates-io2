(define-module (crates-io gr pc grpc-build-derive) #:use-module (crates-io))

(define-public crate-grpc-build-derive-0.1.0 (c (n "grpc-build-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14frsrwpgpqkddwj377hq0rx7yphmqnllynq8rm3swwz8ayi6d3h")))

(define-public crate-grpc-build-derive-0.2.0 (c (n "grpc-build-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "185jkswkvplkgs5nrnrr70fjjnaxmpjjcwf5dayisf94wq9z20a0")))

(define-public crate-grpc-build-derive-0.3.0 (c (n "grpc-build-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "140swasx65xmnmdmsyblrr85cjkjq5bdzhg4z5bd4kcdwpla7ck4")))

