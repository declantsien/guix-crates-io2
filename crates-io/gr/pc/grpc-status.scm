(define-module (crates-io gr pc grpc-status) #:use-module (crates-io))

(define-public crate-grpc-status-0.1.0 (c (n "grpc-status") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)))) (h "1rnprl9qi0vzwjkmn8f8zxn14w69f13jmfpvpx6nnzww5jnaywzm")))

