(define-module (crates-io gr pc grpc-build-core) #:use-module (crates-io))

(define-public crate-grpc-build-core-0.1.0 (c (n "grpc-build-core") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "grpc-build-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)))) (h "08x6f1nkjlz5zhsrbla1nm8xpll6386fl92q4y17wdkrjqc95v4a")))

(define-public crate-grpc-build-core-0.2.0 (c (n "grpc-build-core") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "grpc-build-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "00hh4nf4hzklc8icwgdqj5by30lzydy1gwsp13n11a7lq03cn5wr")))

(define-public crate-grpc-build-core-0.3.0 (c (n "grpc-build-core") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "grpc-build-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)))) (h "0h1xlfslvha2vjw665bw7zsqhln6qg6zpcacwifi0qb2ngnf4cwl")))

