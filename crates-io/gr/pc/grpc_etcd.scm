(define-module (crates-io gr pc grpc_etcd) #:use-module (crates-io))

(define-public crate-grpc_etcd-0.1.0 (c (n "grpc_etcd") (v "0.1.0") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "futures-cpupool") (r "0.1.*") (d #t) (k 0)) (d (n "grpc") (r "^0.1.11") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.1") (d #t) (k 1)) (d (n "tls-api") (r "0.*") (d #t) (k 0)))) (h "0ahjkg1kgvk3bwz2qpsx3129dlg65qrfnmbmr18r2faaal246kwh") (y #t)))

(define-public crate-grpc_etcd-0.1.1 (c (n "grpc_etcd") (v "0.1.1") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "futures-cpupool") (r "0.1.*") (d #t) (k 0)) (d (n "grpc") (r "^0.1.11") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.1") (d #t) (k 1)) (d (n "tls-api") (r "0.*") (d #t) (k 0)))) (h "1s1c6v4673qpjij4j3vvnj475d4md76qmf1cqs6avixdrayiazgn") (y #t)))

(define-public crate-grpc_etcd-0.1.2 (c (n "grpc_etcd") (v "0.1.2") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "futures-cpupool") (r "0.1.*") (d #t) (k 0)) (d (n "grpc") (r "^0.1.11") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.1") (d #t) (k 1)) (d (n "tls-api") (r "0.*") (d #t) (k 0)))) (h "1piiak19fnxj7iqyrgrdg84jzip9k8jim0cjjm2rc5h63mz6jly9") (y #t)))

(define-public crate-grpc_etcd-0.1.3 (c (n "grpc_etcd") (v "0.1.3") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "futures-cpupool") (r "0.1.*") (d #t) (k 0)) (d (n "grpc") (r "^0.1.11") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.1") (d #t) (k 1)) (d (n "tls-api") (r "0.*") (d #t) (k 0)))) (h "00f82vpgfn6rp3rq5ib1yvyfwd98hyh24i58i5klw6g6xcnmgvay") (y #t)))

(define-public crate-grpc_etcd-0.1.4 (c (n "grpc_etcd") (v "0.1.4") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "futures-cpupool") (r "0.1.*") (d #t) (k 0)) (d (n "grpc") (r "^0.1.11") (d #t) (k 0)) (d (n "protobuf") (r "1.*") (d #t) (k 0)) (d (n "protoc-rust-grpc") (r "^0.1") (d #t) (k 1)) (d (n "tls-api") (r "0.*") (d #t) (k 0)))) (h "0car2rw286iqlf49dv5zzm6n2sjrr3gh9gypakaafx8m8202gacr")))

