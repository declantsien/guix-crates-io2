(define-module (crates-io gr pc grpc-protobuf) #:use-module (crates-io))

(define-public crate-grpc-protobuf-0.7.0 (c (n "grpc-protobuf") (v "0.7.0") (d (list (d (n "bytes") (r "0.5.*") (d #t) (k 0)) (d (n "grpc") (r "= 0.7.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "0sxh239l0cjz6bnxr27m37gvhy5dl95j6nk4ligi6rdn2d2sc8k4")))

(define-public crate-grpc-protobuf-0.7.1 (c (n "grpc-protobuf") (v "0.7.1") (d (list (d (n "bytes") (r "0.5.*") (d #t) (k 0)) (d (n "grpc") (r "= 0.7.1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "0fcavqkr54rw9d357ny2n3zpdmzkplmqif08c03v7rd6mq9jbkkp")))

(define-public crate-grpc-protobuf-0.8.0 (c (n "grpc-protobuf") (v "0.8.0") (d (list (d (n "bytes") (r "0.5.*") (d #t) (k 0)) (d (n "grpc") (r "= 0.8.0") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "0g7128msl6g5rx762qshhyj6hsp857xbxi0avdyfakngzkq9k496")))

(define-public crate-grpc-protobuf-0.8.1 (c (n "grpc-protobuf") (v "0.8.1") (d (list (d (n "bytes") (r "0.5.*") (d #t) (k 0)) (d (n "grpc") (r "=0.8.1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "13cd1407wsrkrmr2j6q90x0dln4zagnq774z7506pb4rd2q93sar")))

(define-public crate-grpc-protobuf-0.8.2 (c (n "grpc-protobuf") (v "0.8.2") (d (list (d (n "bytes") (r "0.5.*") (d #t) (k 0)) (d (n "grpc") (r "=0.8.2") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "14ii7vqw5gx0zg3dqa8rw3rm05q0cf4gvfwhqq4a71czspd4ly2w")))

(define-public crate-grpc-protobuf-0.8.3 (c (n "grpc-protobuf") (v "0.8.3") (d (list (d (n "bytes") (r "0.5.*") (d #t) (k 0)) (d (n "grpc") (r "=0.8.3") (d #t) (k 0)) (d (n "protobuf") (r "~2.18.2") (f (quote ("with-bytes"))) (d #t) (k 0)))) (h "18n4s31vp8cg01mcl9pcdh92ig3hl2yylwslsm287gd5p1rf8f9b")))

