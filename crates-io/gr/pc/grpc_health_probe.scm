(define-module (crates-io gr pc grpc_health_probe) #:use-module (crates-io))

(define-public crate-grpc_health_probe-1.0.0 (c (n "grpc_health_probe") (v "1.0.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "fs"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-health") (r "^0.6.0") (d #t) (k 0)))) (h "1qrqmqd5yz81jnmq6hj9m6wxzsqxw2b4zd3936km0k5nbkn7j99r")))

(define-public crate-grpc_health_probe-1.0.1 (c (n "grpc_health_probe") (v "1.0.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "fs"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-health") (r "^0.6.0") (d #t) (k 0)))) (h "1mixdixih60vhl5lwrzpmv8mmbb36gagzfqg4hxcndxxmkxl4jav")))

