(define-module (crates-io gr pc grpc-compiler) #:use-module (crates-io))

(define-public crate-grpc-compiler-0.1.1 (c (n "grpc-compiler") (v "0.1.1") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "1cnd9g9wz43gn633csp8a7nkpridhqw3ry8vfx2hnzp0vcg1z4q5")))

(define-public crate-grpc-compiler-0.1.3 (c (n "grpc-compiler") (v "0.1.3") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "1h5zmrmv8wz4b5x5b85f6866nas8mrngc6kf0xgjm1fcjbv81z2x")))

(define-public crate-grpc-compiler-0.1.4 (c (n "grpc-compiler") (v "0.1.4") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "0w7pph1rsi1cal2q1p6l05hdn5zpl8yr82nx4vx34v1d6mnl890n")))

(define-public crate-grpc-compiler-0.1.7 (c (n "grpc-compiler") (v "0.1.7") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "1z9fh1a5n5g3a6rlzi1ax3di9yzgig9fyidzsfld8yfv81w7i6cf")))

(define-public crate-grpc-compiler-0.1.8 (c (n "grpc-compiler") (v "0.1.8") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "1xf2zy78ywliswll4rvma54x92m34ygzgkdfz669xwjxm3m74nhk")))

(define-public crate-grpc-compiler-0.1.9 (c (n "grpc-compiler") (v "0.1.9") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "0j42z441jzlx0mdm4wl6p2jiz8fx8x23qz7gpqhzvrq7w17m8kpv")))

(define-public crate-grpc-compiler-0.1.10 (c (n "grpc-compiler") (v "0.1.10") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "1wwfp7gxlf3pb0jdr1g974j8fk0d1l0ickyjr83kmz92x59lw765")))

(define-public crate-grpc-compiler-0.1.11 (c (n "grpc-compiler") (v "0.1.11") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "03f08467rjkmjk8fr1fy661dn0mv6vxa669y4qlv4mrnviz8sd83")))

(define-public crate-grpc-compiler-0.2.0 (c (n "grpc-compiler") (v "0.2.0") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "0hqn2jrlxmav0r6yliv8b2sainzrpya1xww4ykh08yn5547wpvjf")))

(define-public crate-grpc-compiler-0.2.1 (c (n "grpc-compiler") (v "0.2.1") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "0g7wp7b2ggcaksr5iqldg4lf6l7w7bl5i0v8xcj4y5rym83fz00b")))

(define-public crate-grpc-compiler-0.3.0 (c (n "grpc-compiler") (v "0.3.0") (d (list (d (n "protobuf") (r "1.*") (d #t) (k 0)))) (h "0ncfg84rgi6vy6mjvwy3nm3w8i4jjs9pmb2wdyr2b0dh45z4zinm")))

(define-public crate-grpc-compiler-0.3.1 (c (n "grpc-compiler") (v "0.3.1") (d (list (d (n "protobuf") (r "~1.5") (d #t) (k 0)))) (h "0s4x2wynh0wgk1k7vlrnxay43xfg7is7jjh8sa6gnl3d18j77brd")))

(define-public crate-grpc-compiler-0.4.0 (c (n "grpc-compiler") (v "0.4.0") (d (list (d (n "protobuf") (r "~1.6") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~1.6") (d #t) (k 0)))) (h "060zb81dxc2bg27s7fblgnr2y6i1lhz5g47bqyj3bi6bdxlxf3mf")))

(define-public crate-grpc-compiler-0.5.0 (c (n "grpc-compiler") (v "0.5.0") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "0kjbzygk26i43vbyjibl71lsx8db0852bsa09jnmxjiq8lvinjz8")))

(define-public crate-grpc-compiler-0.6.0 (c (n "grpc-compiler") (v "0.6.0") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "0n70fvlmabqz401wsrvycqy3d2x508xcdyrlvy2rq3wicg046rab")))

(define-public crate-grpc-compiler-0.6.1 (c (n "grpc-compiler") (v "0.6.1") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "0354r307al2wmrcixs9jjhcih9h5j793yks61s4412cdk46i8zxh")))

(define-public crate-grpc-compiler-0.6.2 (c (n "grpc-compiler") (v "0.6.2") (d (list (d (n "protobuf") (r ">= 2.8.0, < 2.10.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r ">= 2.8.0, < 2.10.0") (d #t) (k 0)))) (h "1cgznh0h1iw3r798mz5irx3nm112x8i91c0d1c6hmd77iv778wlh")))

(define-public crate-grpc-compiler-0.7.0 (c (n "grpc-compiler") (v "0.7.0") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "1827axjyp6ap18z896kjlkk7mlv62hj8mdxhn5662f287rmfgifr")))

(define-public crate-grpc-compiler-0.7.1 (c (n "grpc-compiler") (v "0.7.1") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "05f9kzdxrqx6md96lkr2vbr15gsh51gi7xi12bhji0fdfpd9x5ax")))

(define-public crate-grpc-compiler-0.8.0 (c (n "grpc-compiler") (v "0.8.0") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "0cppww4sv3x5y1lp1qdfvam8zcrfnn6w4n0b38dblkyqp56dngag")))

(define-public crate-grpc-compiler-0.8.1 (c (n "grpc-compiler") (v "0.8.1") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "0gfdm4lfvm6g53kfx45h474l717d4h73n4ccj2xyv62byl5rqqr8")))

(define-public crate-grpc-compiler-0.8.2 (c (n "grpc-compiler") (v "0.8.2") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 0)))) (h "1vmi699jng5qbd97fdimf8zzgrr7b1wxm1n90iwcz1q7vjvn2a4f")))

(define-public crate-grpc-compiler-0.8.3 (c (n "grpc-compiler") (v "0.8.3") (d (list (d (n "protobuf") (r "~2.18.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~2.18.2") (d #t) (k 0)))) (h "0d2ai3h1n9x3lcfwd2cnhkr4dzqmq4a28v8hpzmrkrqnkr273ya5")))

