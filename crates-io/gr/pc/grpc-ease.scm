(define-module (crates-io gr pc grpc-ease) #:use-module (crates-io))

(define-public crate-grpc-ease-0.1.0 (c (n "grpc-ease") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.6") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.15") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tonic-reflection") (r "^0.11.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0lgpanpff68s5ibz4697hf44hai2zzrpirzlz15l075zlimcsz96")))

(define-public crate-grpc-ease-0.1.1 (c (n "grpc-ease") (v "0.1.1") (d (list (d (n "prost") (r "^0.12.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.6") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.15") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tonic-reflection") (r "^0.11.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "14ba5wypdyldxb5v27460zh9wlm5q19v14m4kjys4zxba9f0gilb")))

