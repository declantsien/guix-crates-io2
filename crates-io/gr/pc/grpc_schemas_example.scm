(define-module (crates-io gr pc grpc_schemas_example) #:use-module (crates-io))

(define-public crate-grpc_schemas_example-0.1.0 (c (n "grpc_schemas_example") (v "0.1.0") (d (list (d (n "codegen") (r "^0.1.3") (d #t) (k 1)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.1") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1ccyws3nm4g1ml8zqn3fwgdgvjzq0kg1ab5p4np9lr2ksp0sqdg9")))

