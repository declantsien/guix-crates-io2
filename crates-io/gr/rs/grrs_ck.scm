(define-module (crates-io gr rs grrs_ck) #:use-module (crates-io))

(define-public crate-grrs_ck-0.1.0 (c (n "grrs_ck") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1nr8j5nzn6v9h7lnkbd7wzyr1p46z2wgrxyp2j2xvnf4zb9na0k2")))

