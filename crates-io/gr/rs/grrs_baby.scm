(define-module (crates-io gr rs grrs_baby) #:use-module (crates-io))

(define-public crate-grrs_baby-0.1.0 (c (n "grrs_baby") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "083a9k23ch3mnlyk5m9m7fr776qnd2gmm23ham4zs61sgddhpxjk")))

(define-public crate-grrs_baby-0.1.1 (c (n "grrs_baby") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0fk45z5rghpbxmfg43d4ifmz2q0qw0ygn5grqwcj5isva1c7qdsn")))

