(define-module (crates-io gr rs grrs-panzerstadt) #:use-module (crates-io))

(define-public crate-grrs-panzerstadt-0.1.0 (c (n "grrs-panzerstadt") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)))) (h "1pd8xjah61s1zwpb1sd1s7vriyg501qp0d9jd6vm6vfqcqjhg4y5")))

