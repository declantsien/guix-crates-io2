(define-module (crates-io gr rs grrs) #:use-module (crates-io))

(define-public crate-grrs-0.1.0 (c (n "grrs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0wcv2nzqxp01v82spaf88sxwjz50jgj6kvkw570bw52b9ijdrpzn")))

