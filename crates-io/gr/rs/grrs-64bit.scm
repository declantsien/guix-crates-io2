(define-module (crates-io gr rs grrs-64bit) #:use-module (crates-io))

(define-public crate-grrs-64bit-0.1.0 (c (n "grrs-64bit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0x0nl13ryjnvspdy32w9d8wsc4akncfxhkvgrmkflxpkxrpglqx1") (y #t)))

