(define-module (crates-io gr rs grrs2219) #:use-module (crates-io))

(define-public crate-grrs2219-0.1.0 (c (n "grrs2219") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^2.1.4") (d #t) (k 0)))) (h "0nrn53lnylpkilc6275mbg4ww6nrnbr8byrdk9l2zm78bvff08wk")))

