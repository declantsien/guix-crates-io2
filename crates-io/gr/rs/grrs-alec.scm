(define-module (crates-io gr rs grrs-alec) #:use-module (crates-io))

(define-public crate-grrs-alec-0.1.0 (c (n "grrs-alec") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1fiinnq21fb9xlbri685hrwr513059bvsrjzad5wmkr24f8g5f8j")))

