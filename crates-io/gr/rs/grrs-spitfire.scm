(define-module (crates-io gr rs grrs-spitfire) #:use-module (crates-io))

(define-public crate-grrs-spitfire-0.1.0 (c (n "grrs-spitfire") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "1wpryf7hiwvbx1y356jqibrr028mkz90fna7vj9f6fdzdpgahsxi")))

