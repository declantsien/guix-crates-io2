(define-module (crates-io gr rs grrs-gn) #:use-module (crates-io))

(define-public crate-grrs-gn-0.1.0 (c (n "grrs-gn") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "03mmz58fq71bvh2p33llijg12qvd0mgcxc6ld0rn3il5i9hzg487")))

