(define-module (crates-io gr rs grrs033) #:use-module (crates-io))

(define-public crate-grrs033-0.1.0 (c (n "grrs033") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "06kss8drkzhh0i3rmc2gxb5xhzv2d4r491ahn3mg9z5biinpwj61")))

