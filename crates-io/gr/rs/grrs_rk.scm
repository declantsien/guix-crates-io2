(define-module (crates-io gr rs grrs_rk) #:use-module (crates-io))

(define-public crate-grrs_rk-0.1.0 (c (n "grrs_rk") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.2") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1q4kh3jdn1av564xcn373dhb5879qzrcscjr5yfadwy8vylmacxx")))

