(define-module (crates-io gr rs grrs-db4555) #:use-module (crates-io))

(define-public crate-grrs-db4555-0.1.0 (c (n "grrs-db4555") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "0jjgm8zzi5bq2ifsc1bwb5b7k1za05zpgcakwnffrh0z56kc3ql4") (y #t)))

