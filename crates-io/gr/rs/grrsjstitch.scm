(define-module (crates-io gr rs grrsjstitch) #:use-module (crates-io))

(define-public crate-grrsjstitch-0.1.0 (c (n "grrsjstitch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "indicatif") (r "^0.16.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "predicates") (r "^1.0.8") (d #t) (k 2)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "02yg4zq1c81qcymf3kjb6czsk9c0sp6zg6lqgs8fg4h0rvb25wzg")))

