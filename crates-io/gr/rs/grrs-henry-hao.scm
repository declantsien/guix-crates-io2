(define-module (crates-io gr rs grrs-henry-hao) #:use-module (crates-io))

(define-public crate-grrs-henry-hao-0.1.0 (c (n "grrs-henry-hao") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^0.10") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0kxcc0imm7k8r6fb55mddkm5qfmslc6b7dkfgcd9012i7cdrgbwy")))

