(define-module (crates-io gr rs grrs_cheng) #:use-module (crates-io))

(define-public crate-grrs_cheng-0.1.0 (c (n "grrs_cheng") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "13qywhlrpxwwl5d0pk94h79x5zhvy43hksmq3q0mdkvlg6a6hpx4")))

