(define-module (crates-io gr rs grrs_hj) #:use-module (crates-io))

(define-public crate-grrs_hj-0.1.0 (c (n "grrs_hj") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 0)))) (h "1a41m3zwjnkq71rqkmfjxfw33c0c44pi1y59065bni59ds9n2bk5")))

(define-public crate-grrs_hj-0.1.1 (c (n "grrs_hj") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 0)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 0)))) (h "1lvb0477l7rmcv0qssr5bxf29kzc8jdyfsa0mf8nn9cd9mgihwiz")))

