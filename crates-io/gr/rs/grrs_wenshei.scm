(define-module (crates-io gr rs grrs_wenshei) #:use-module (crates-io))

(define-public crate-grrs_wenshei-0.1.0 (c (n "grrs_wenshei") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "0nl20g1l96yph3innk6gcq54my98x6di47hbd7gxmkgccyjdb9sh")))

