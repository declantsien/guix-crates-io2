(define-module (crates-io gr rs grrs-lazy) #:use-module (crates-io))

(define-public crate-grrs-lazy-0.1.0 (c (n "grrs-lazy") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap-verbosity-flag") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0q6say92mv6hsg3vg2zxbm3i5pf9ibq89bcgncycpabi8ssxxvzx")))

(define-public crate-grrs-lazy-0.2.0 (c (n "grrs-lazy") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "121hz5gvqklkcgjgbmng9pq06k7fppx03yaj7zrl364q8g6pljl6")))

