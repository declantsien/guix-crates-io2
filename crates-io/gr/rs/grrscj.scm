(define-module (crates-io gr rs grrscj) #:use-module (crates-io))

(define-public crate-grrscj-0.1.0 (c (n "grrscj") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "107f74bn3jzgljqgm4rd2a2dpdijmv1k5bmsfxbdz98q50r2byr5")))

