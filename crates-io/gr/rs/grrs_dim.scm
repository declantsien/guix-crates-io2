(define-module (crates-io gr rs grrs_dim) #:use-module (crates-io))

(define-public crate-grrs_dim-0.1.0 (c (n "grrs_dim") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0mn5h85wq11j6wd1vbj8zsdj1dn7182qbz108xskvraxq4mz9qx2") (y #t)))

