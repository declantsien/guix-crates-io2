(define-module (crates-io gr rs grrs-dawksh) #:use-module (crates-io))

(define-public crate-grrs-dawksh-0.1.0 (c (n "grrs-dawksh") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "1c2yvfrzx415jn1fdff02dc2wwwv9qdkiwm9ihpb7p9klj72ngvp")))

