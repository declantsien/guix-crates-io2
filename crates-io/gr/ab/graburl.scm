(define-module (crates-io gr ab graburl) #:use-module (crates-io))

(define-public crate-graburl-0.1.0 (c (n "graburl") (v "0.1.0") (d (list (d (n "ahref") (r "^0.2.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1mqcs2kzlsn99w07505vhc8zg9ppkgyy147v26pklixnd97ivq5l") (r "1.72.0")))

(define-public crate-graburl-0.1.1 (c (n "graburl") (v "0.1.1") (d (list (d (n "ahref") (r "^0.2.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1838jf53rrnc0lk0qb42n6l9ccvyg5a2nx6sp94y46436xzmw565") (r "1.72.0")))

(define-public crate-graburl-0.1.2 (c (n "graburl") (v "0.1.2") (d (list (d (n "ahref") (r "^0.2.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1kf3fqp3h019scncljvqsdc30sa09jxw5fl0d1d2s1y29zbrp5v3") (r "1.72.0")))

(define-public crate-graburl-0.1.3 (c (n "graburl") (v "0.1.3") (d (list (d (n "ahref") (r "^0.2.7") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0zg6fsxwi5b0rv9pj610d8y1vr1wiskkyvv85hfa64fm0s28qndj") (r "1.72.0")))

(define-public crate-graburl-0.1.4 (c (n "graburl") (v "0.1.4") (d (list (d (n "ahref") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0c4sabdkb08vblfqc0zf0k0mvqddrmc0napn7zvkqz7nc438sxd1") (r "1.72.0")))

(define-public crate-graburl-0.1.5 (c (n "graburl") (v "0.1.5") (d (list (d (n "ahref") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1sjrhs18ddlq697g1qg0wvvv9zyi1vbwdrbdrwxrixclwk9wpgjy") (r "1.72.0")))

(define-public crate-graburl-0.1.6 (c (n "graburl") (v "0.1.6") (d (list (d (n "ahref") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "111d74fs00yhf85cdsrdq79cx6xbndrriixxxkq5kwqwqzn7ifiq") (r "1.72.0")))

(define-public crate-graburl-0.1.7 (c (n "graburl") (v "0.1.7") (d (list (d (n "ahref") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1vhvqqwzc8r14cn8ixaygc78hdiqsg8709ky9npwg4s6sq36dsg2") (r "1.72.0")))

(define-public crate-graburl-0.1.8 (c (n "graburl") (v "0.1.8") (d (list (d (n "ahref") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "037avp0hsm2ad83n328d6f12fm7d2rw9y79hnx59zg57q34svr33") (r "1.72.0")))

