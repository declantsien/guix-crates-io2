(define-module (crates-io gr ab graby) #:use-module (crates-io))

(define-public crate-graby-0.1.0 (c (n "graby") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fd46nvrq433mwysxfh0q83f0bdycqlds7g45x376ss2s2j944kr")))

(define-public crate-graby-0.1.1 (c (n "graby") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cvl3m1vd5bdq41drivrq1fng4mq57gpf1p60djfh0cgbkas1x8f")))

(define-public crate-graby-1.0.0 (c (n "graby") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1pgiik2xfidir2x2icfq7pz16y8lrak2xkifqszxy9z38bsh9q56")))

