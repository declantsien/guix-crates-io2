(define-module (crates-io gr ab grabinput) #:use-module (crates-io))

(define-public crate-grabinput-0.1.0 (c (n "grabinput") (v "0.1.0") (h "0cpjimlcdvgkdjhv3il69ighrby9masnlil7ddzz1rm5gf69p2ij")))

(define-public crate-grabinput-0.1.1 (c (n "grabinput") (v "0.1.1") (h "094xcn2581cf79m7n21zj2sffaw1k64rrx8vjfd2y5gibjpgain5")))

(define-public crate-grabinput-0.2.0 (c (n "grabinput") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.90") (o #t) (d #t) (k 0)))) (h "112839kqlv81ya6ykpbdgw2w2kb4wfd7hd2vp94r55qs0ld3kz98")))

(define-public crate-grabinput-0.2.1 (c (n "grabinput") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "08mmhsr2npln8rfnx9cq2hma2n78lfjrm0gy71vyx5vwr0lr4y92")))

