(define-module (crates-io gr ab grabbag_macros) #:use-module (crates-io))

(define-public crate-grabbag_macros-0.0.1 (c (n "grabbag_macros") (v "0.0.1") (h "0kpj9s56cflgm130hd5hxzhs191w9laxzv6dggc0wd08ww6swpzp")))

(define-public crate-grabbag_macros-0.0.2 (c (n "grabbag_macros") (v "0.0.2") (h "1wy7ip62n5nria43p97psj7sbba64x9cbk041nnzw14nvz5xfdcb")))

(define-public crate-grabbag_macros-0.0.3 (c (n "grabbag_macros") (v "0.0.3") (h "1k2jar9jpgj4zzk3wijgj1dkl8zyjxkags8rr9kqp9r7lwnvb8qj")))

(define-public crate-grabbag_macros-0.0.4 (c (n "grabbag_macros") (v "0.0.4") (h "1ibg0qp0nap5ibwc2pzcc5ycp20n76q29x04mmyi2s2y6j3m6fc3")))

(define-public crate-grabbag_macros-0.1.0 (c (n "grabbag_macros") (v "0.1.0") (h "1cmzcz7fmmp84kisrfkdcz58pc605m4fpi8m6gpbsp34pwwzl2rj")))

(define-public crate-grabbag_macros-0.1.2 (c (n "grabbag_macros") (v "0.1.2") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)))) (h "0sxcvv3ycs6kzp81s3n4iwxdr5h2g4kczbkxgg68q0vx25h526bx")))

