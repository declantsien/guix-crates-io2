(define-module (crates-io gr ab grab) #:use-module (crates-io))

(define-public crate-grab-0.3.0 (c (n "grab") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1bb7pwn2i21bsn42ydjv9qal34pvkl9v4x3zkgv1ncd62zvwaqqx")))

(define-public crate-grab-0.3.1 (c (n "grab") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0fxbasz47lbc2hlm5a30633sl837x5kvgk35jj0pdkfgdvidsp1z")))

