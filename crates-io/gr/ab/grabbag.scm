(define-module (crates-io gr ab grabbag) #:use-module (crates-io))

(define-public crate-grabbag-0.0.1 (c (n "grabbag") (v "0.0.1") (d (list (d (n "grabbag_macros") (r "^0.0.1") (d #t) (k 0)))) (h "1fpqjp2l4zn9fzhk0445l57p6r9jzgmwvwyb7zm8943wz52a39bs")))

(define-public crate-grabbag-0.0.2 (c (n "grabbag") (v "0.0.2") (d (list (d (n "grabbag_macros") (r "^0.0.2") (d #t) (k 0)))) (h "1x6m5lq0h5hpgaphy7i4nk5bbc79c23i352lr3yaxk6jhgy44x7b")))

(define-public crate-grabbag-0.0.3 (c (n "grabbag") (v "0.0.3") (d (list (d (n "grabbag_macros") (r "^0.0.3") (d #t) (k 0)))) (h "084xfd6ay90fx3d6p6qcymsy6inlsv2cf4gl3vh1fqmh6i85n65n")))

(define-public crate-grabbag-0.0.4 (c (n "grabbag") (v "0.0.4") (d (list (d (n "grabbag_macros") (r "^0.0.4") (d #t) (k 0)))) (h "0xls7dgp85xpr9k0m6kwdyzx5nmir71x7pz3wnish0ihi2cqnw3j")))

(define-public crate-grabbag-0.1.0 (c (n "grabbag") (v "0.1.0") (d (list (d (n "grabbag_macros") (r "^0.1.0") (d #t) (k 0)))) (h "02vkpd8idg47h2d6slwkayy8k35jq4mi4iwddd2cz794mjqms3z6")))

(define-public crate-grabbag-0.1.1 (c (n "grabbag") (v "0.1.1") (d (list (d (n "grabbag_macros") (r "^0.1.0") (d #t) (k 0)))) (h "12bjc0jsn63wgkf8dc26p8zps93yp5c1nz3b0b8zviwdphzydl8q")))

(define-public crate-grabbag-0.1.2 (c (n "grabbag") (v "0.1.2") (d (list (d (n "grabbag_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1cvsc67jq95w9fq4qcbmfy7nc3rppzrf830kwa5nwwwn37nkc8j8")))

