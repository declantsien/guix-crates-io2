(define-module (crates-io gr p_ grp_api) #:use-module (crates-io))

(define-public crate-grp_api-0.1.0 (c (n "grp_api") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hgiw8q4imkxvkasmgywxa068iwk80h3yry5f91bgjqrj3ylqw8k")))

(define-public crate-grp_api-0.1.1 (c (n "grp_api") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1wz354dqzxxbbghyrq0h3bq8c5fq2a7yl3d702v327l0jdc834a5")))

