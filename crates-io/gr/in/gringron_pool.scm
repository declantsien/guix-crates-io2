(define-module (crates-io gr in gringron_pool) #:use-module (crates-io))

(define-public crate-gringron_pool-5.2.0-beta.3 (c (n "gringron_pool") (v "5.2.0-beta.3") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "gringron_chain") (r "^5.2.0-beta.3") (d #t) (k 2)) (d (n "gringron_core") (r "^5.2.0-beta.3") (d #t) (k 0)) (d (n "gringron_keychain") (r "^5.2.0-beta.3") (d #t) (k 0)) (d (n "gringron_util") (r "^5.2.0-beta.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ab9icnb9h38qakv53v5pamfa4jrbizx821hww93vdrl3caqx6nh")))

