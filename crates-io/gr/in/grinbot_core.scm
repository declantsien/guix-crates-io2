(define-module (crates-io gr in grinbot_core) #:use-module (crates-io))

(define-public crate-grinbot_core-0.1.0-alpha.1 (c (n "grinbot_core") (v "0.1.0-alpha.1") (d (list (d (n "askama") (r "^0.8.0") (d #t) (k 0)) (d (n "grin_wallet_libwallet") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 0)) (d (n "redux-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "03fgbdfc5fpk2nrsn54pqda8bka0192xa1zyfgq1x8hmjv3zp1jk")))

