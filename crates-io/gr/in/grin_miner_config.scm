(define-module (crates-io gr in grin_miner_config) #:use-module (crates-io))

(define-public crate-grin_miner_config-0.4.2 (c (n "grin_miner_config") (v "0.4.2") (d (list (d (n "cuckoo_miner") (r "^0.4.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "grin_miner_util") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "slog") (r "~2.1") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1z1ldm6n8lvlism0nqzwj3yjdwqiqz1f9mbcrlhh7m1b7bjmx8r6")))

(define-public crate-grin_miner_config-0.5.2 (c (n "grin_miner_config") (v "0.5.2") (d (list (d (n "cuckoo_miner") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "grin_miner_util") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "slog") (r "~2.1") (f (quote ("max_level_trace" "release_max_level_trace"))) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0v4xwpcffd5cvngflr5v3vnjackg58kjrlwqdb9y9cci7dlsj990")))

