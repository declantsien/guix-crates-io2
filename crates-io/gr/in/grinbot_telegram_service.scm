(define-module (crates-io gr in grinbot_telegram_service) #:use-module (crates-io))

(define-public crate-grinbot_telegram_service-0.1.0-alpha.1 (c (n "grinbot_telegram_service") (v "0.1.0-alpha.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "grinbot_core") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 0)) (d (n "redux-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "telegram-bot") (r "^0.6.3") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1gdkdlczzgbk0l6k1rcj3ygcj7gcq2f4mj0h904cx928h775mqnv")))

