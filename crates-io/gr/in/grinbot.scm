(define-module (crates-io gr in grinbot) #:use-module (crates-io))

(define-public crate-grinbot-0.1.0-alpha.1 (c (n "grinbot") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^2.31") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "grinbot_core") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "grinbot_keybase_service") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "grinbot_telegram_service") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0r45zkxb79lc5nhsv21vy3spjws4lzhdw1pj16mdcipnrbq5sp9k")))

