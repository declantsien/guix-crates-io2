(define-module (crates-io gr in grin_miner_plugin) #:use-module (crates-io))

(define-public crate-grin_miner_plugin-0.4.2 (c (n "grin_miner_plugin") (v "0.4.2") (d (list (d (n "blake2-rfc") (r "~0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "serde") (r "~1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.2") (d #t) (k 0)))) (h "104294831mky0i1gl79flgmkzxlslrjiwsxh007lim8pq6dsdshq")))

(define-public crate-grin_miner_plugin-0.5.2 (c (n "grin_miner_plugin") (v "0.5.2") (d (list (d (n "blake2-rfc") (r "~0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "serde") (r "~1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.2") (d #t) (k 0)))) (h "1vfwhfa3b3zb8mbqm7clmx62i5fbv6qvlxcpi5gga01q4j0rlgin")))

