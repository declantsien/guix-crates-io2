(define-module (crates-io gr in grin-vanity) #:use-module (crates-io))

(define-public crate-grin-vanity-0.1.0 (c (n "grin-vanity") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "grin_core") (r "^5.1.2") (d #t) (k 0)) (d (n "grin_keychain") (r "^5.1.2") (d #t) (k 0)) (d (n "grin_util") (r "^5.1.2") (d #t) (k 0)) (d (n "grin_wallet") (r "^5.1.0") (d #t) (k 0)) (d (n "grin_wallet_libwallet") (r "^5.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1z25njw2cdydpqshqcvja7w9fdmlcbrnwi72194b6shjsil1w79c")))

