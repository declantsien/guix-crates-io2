(define-module (crates-io gr av gravity_proto) #:use-module (crates-io))

(define-public crate-gravity_proto-0.1.0 (c (n "gravity_proto") (v "0.1.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0qdwc96ad1n84fw0yha0dwd259axc2k13zavy62bnlizk1vpn94s")))

(define-public crate-gravity_proto-0.2.0 (c (n "gravity_proto") (v "0.2.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "05j5x6draila4hxysvj7mqy70xpb304vm64w45xzwd3q57c6i0xl")))

(define-public crate-gravity_proto-0.2.1 (c (n "gravity_proto") (v "0.2.1") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "0qk00rlj1zsgdm6vqppq8mf1syl5ysg3v3gm5yhzb8kvz7z7x0ca")))

(define-public crate-gravity_proto-0.2.2 (c (n "gravity_proto") (v "0.2.2") (d (list (d (n "cosmos-sdk-proto") (r "^0.14.2") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)))) (h "06787n7papa4nlwdcj6v6wg8qvbqhh1grjy4p669hv51czxfjcrv")))

(define-public crate-gravity_proto-0.3.0 (c (n "gravity_proto") (v "0.3.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.15.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "0afh70s9sn3dhf4mybdr0nm9ws71z1lf5c691r63hbbn31s2dkf8")))

(define-public crate-gravity_proto-0.4.0-alpha (c (n "gravity_proto") (v "0.4.0-alpha") (d (list (d (n "cosmos-sdk-proto") (r "^0.15.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1d3gbzwqxadjk3lhlr9ppgk29cfwi87k6n9kws6rny660yd4a102")))

(define-public crate-gravity_proto-0.4.0-alpha2 (c (n "gravity_proto") (v "0.4.0-alpha2") (d (list (d (n "cosmos-sdk-proto") (r "^0.15.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1l5bv4mk0pjgvr2683mzr5fhf4kbhll2h067g5a929qbh2pn7raw")))

(define-public crate-gravity_proto-0.4.0 (c (n "gravity_proto") (v "0.4.0") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1ja425gcb3v5ky1k7k59z39fdr0aih3da0lvwcz2sjhwrgx285xr")))

(define-public crate-gravity_proto-0.4.1 (c (n "gravity_proto") (v "0.4.1") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1mkgghf9l7kfbngd9hmpcjikz88q5gqf4cnfrbi1ai35jdn0nc32")))

(define-public crate-gravity_proto-0.4.2 (c (n "gravity_proto") (v "0.4.2") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "1v25dzdbl39qa724mhmlsmd1zl6nsxq1ni2sim77jj8ki46wzln5")))

(define-public crate-gravity_proto-0.4.3 (c (n "gravity_proto") (v "0.4.3") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "039h88iz0xil9kd62bqw8avh5wgqjki717ly4sr2nqfgvrg50maz")))

(define-public crate-gravity_proto-0.4.4 (c (n "gravity_proto") (v "0.4.4") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.0") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "0cargzxyck5k4c2rf8y1hj5miz0qh86znvz46zjv5y6ykkcbpcng")))

(define-public crate-gravity_proto-0.4.5 (c (n "gravity_proto") (v "0.4.5") (d (list (d (n "cosmos-sdk-proto") (r "^0.16.1") (f (quote ("ethermint" "bech32ibc"))) (d #t) (k 0) (p "cosmos-sdk-proto-althea")) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)))) (h "121y1l0gw3ksxcxpb3d4f8xz8dv2j6krfz67iq162p5h23dw8sgb")))

