(define-module (crates-io gr av gravatar) #:use-module (crates-io))

(define-public crate-gravatar-0.1.0 (c (n "gravatar") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.24") (d #t) (k 0)) (d (n "url") (r "^0.2.27") (d #t) (k 0)))) (h "1pd0vvdq6s9pm870c9qz75zb3d9c9p5jxkq2z3mmm028ic4qaa4n")))

(define-public crate-gravatar-0.1.1 (c (n "gravatar") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.25") (d #t) (k 0)) (d (n "url") (r "^0.2.27") (d #t) (k 0)))) (h "1i725bknbmfbcllfi2iay84vnaz1f6npb32v70b8knvx0jra9cq0")))

(define-public crate-gravatar-0.1.2 (c (n "gravatar") (v "0.1.2") (d (list (d (n "rust-crypto") (r "^0.2.26") (d #t) (k 0)) (d (n "url") (r "^0.2.28") (d #t) (k 0)))) (h "0n874a9b91byk6d7jqm5s0caakywyx41md886d4lx3qbvn7madpj")))

(define-public crate-gravatar-0.1.3 (c (n "gravatar") (v "0.1.3") (d (list (d (n "rust-crypto") (r "^0.2.31") (d #t) (k 0)) (d (n "url") (r "^0.2.29") (d #t) (k 0)))) (h "1l0fz3sh5s35k8f6sbaswiah4k5abq3f958rv2hgqplc0maflypv")))

(define-public crate-gravatar-0.1.4 (c (n "gravatar") (v "0.1.4") (d (list (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "url") (r "^0.5.5") (d #t) (k 0)))) (h "1i6h7p29k3mz8zvzdhjk9rl9z23c3cbq8yz96r4yx2s9fb8wpirk")))

(define-public crate-gravatar-0.1.5 (c (n "gravatar") (v "0.1.5") (d (list (d (n "rust-crypto") (r "^0.2.35") (d #t) (k 0)) (d (n "url") (r "^1.1.0") (d #t) (k 0)))) (h "1708qzxqpjxhqlr6j8dfz5q89nx77icdljnmar43i583wk7a1chx")))

(define-public crate-gravatar-0.2.0 (c (n "gravatar") (v "0.2.0") (d (list (d (n "md-5") (r "^0.8.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "09fd044kwb8r0p63m5xgksi5m3q503jcwy5bxkb4i94pvzdyhi5c")))

