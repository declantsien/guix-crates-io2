(define-module (crates-io gr av graviton_ast) #:use-module (crates-io))

(define-public crate-graviton_ast-0.1.0 (c (n "graviton_ast") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zwffjrpkkrnaxs4fq5f8157809pli77y82c97m2kwidkm2x9g51")))

(define-public crate-graviton_ast-0.2.0 (c (n "graviton_ast") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f0cz19jixqqkbr99plfc7530k27j9kwiad0pa0c6n0xs6657gwg") (f (quote (("node_code_pos"))))))

(define-public crate-graviton_ast-0.3.0 (c (n "graviton_ast") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z63xf9w5dl33ba5arj2lfl4sagyivz67wm808n995jmrdqi2va4") (f (quote (("node_code_pos"))))))

(define-public crate-graviton_ast-0.3.1 (c (n "graviton_ast") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p6v0pfbps9623pp2j0i0ch41fqc9ffi30860lq35nywwrj6dqr9") (f (quote (("node_code_pos"))))))

(define-public crate-graviton_ast-0.4.0 (c (n "graviton_ast") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "19p5ii5bz11xs7q0kf7mvpr4q3i7yfqghrsx8gn8x549zdchbfdj") (f (quote (("node_code_pos"))))))

(define-public crate-graviton_ast-0.4.1 (c (n "graviton_ast") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "0brvv621r2cf4z7wjqb7kakbzdkp4ipzqzmdzhxcnja8qmyqq39v") (f (quote (("node_code_pos"))))))

(define-public crate-graviton_ast-0.5.0 (c (n "graviton_ast") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "15nmssjk2j1wyxcvanp3ypnznpbwz0jyimv143xdcvc1nii19fbi")))

(define-public crate-graviton_ast-0.6.0 (c (n "graviton_ast") (v "0.6.0") (d (list (d (n "graviton_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0daz2iydcaksj41l7y05lzsjpz473cw4hs2aiqkyip9vyfa766sn")))

