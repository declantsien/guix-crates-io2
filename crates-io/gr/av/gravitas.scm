(define-module (crates-io gr av gravitas) #:use-module (crates-io))

(define-public crate-gravitas-0.1.0 (c (n "gravitas") (v "0.1.0") (h "0zpj568n2s87l53910avq3n9b2gjpnjl0j0a668j911mjgyjca5m")))

(define-public crate-gravitas-0.1.1 (c (n "gravitas") (v "0.1.1") (h "04hi0kfv10ishj04nlcn13kclsjg63cvrbqd7v1vidvsl6qhx1xy")))

