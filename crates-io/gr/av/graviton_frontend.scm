(define-module (crates-io gr av graviton_frontend) #:use-module (crates-io))

(define-public crate-graviton_frontend-0.1.0 (c (n "graviton_frontend") (v "0.1.0") (d (list (d (n "graviton_ast") (r "^0.1.0") (d #t) (k 0)))) (h "090f0r04ks7gy88b3iib75s8rszvb9a18132lv68kifaa5q14af2")))

(define-public crate-graviton_frontend-0.2.0 (c (n "graviton_frontend") (v "0.2.0") (d (list (d (n "graviton_ast") (r "^0.2.0") (d #t) (k 0)))) (h "0db9gzmzi5mmv8fdna3hgg827qjyh8i3gwmb6355xp2naasjl9ss") (f (quote (("node_code_pos" "graviton_ast/node_code_pos"))))))

(define-public crate-graviton_frontend-0.3.0 (c (n "graviton_frontend") (v "0.3.0") (d (list (d (n "graviton_ast") (r "^0.3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0xccw5c2yc98kk0qn5xv4k90rzgn13w2hhc3jb8j4gsyhq1927y3") (f (quote (("node_code_pos" "graviton_ast/node_code_pos"))))))

(define-public crate-graviton_frontend-0.3.1 (c (n "graviton_frontend") (v "0.3.1") (d (list (d (n "graviton_ast") (r "^0.3.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "09m1m05h5qa5gjbnw0mhj5p7dyn9qc0y5245456xl6nn3k013ian") (f (quote (("node_code_pos" "graviton_ast/node_code_pos"))))))

(define-public crate-graviton_frontend-0.4.0 (c (n "graviton_frontend") (v "0.4.0") (d (list (d (n "graviton_ast") (r "^0.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0gdd8pqph7dg7myw2qcan7lprbaii6092hp0iix5zh06j841g8f5") (f (quote (("node_code_pos" "graviton_ast/node_code_pos"))))))

(define-public crate-graviton_frontend-0.4.1 (c (n "graviton_frontend") (v "0.4.1") (d (list (d (n "graviton_ast") (r "^0.4.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1njrby8wwrrs5kzh0v5jzpsql44lhq6iiisjdpdilynvbhgrrqcp") (f (quote (("node_code_pos" "graviton_ast/node_code_pos"))))))

(define-public crate-graviton_frontend-0.5.0 (c (n "graviton_frontend") (v "0.5.0") (d (list (d (n "graviton_ast") (r "^0.5.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "03mlybaz9n0hx1qvqs1qvd9ps1r2b2y5cmvg6vc02j1ymw9di4z8")))

(define-public crate-graviton_frontend-0.6.0 (c (n "graviton_frontend") (v "0.6.0") (d (list (d (n "graviton_ast") (r "^0.6.0") (d #t) (k 0)) (d (n "graviton_core") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1y2ql1ayby2i9mpqr7kimzkvzry07y2hpj3dqznqif0i2s4vhhr6")))

