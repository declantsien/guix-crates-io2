(define-module (crates-io gr av gravatar-rs) #:use-module (crates-io))

(define-public crate-gravatar-rs-0.1.0 (c (n "gravatar-rs") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1jy4xk5l3ci7cg0gil1q6qmjay6mvxkhc6xhj6as6jkk0imp6dbj")))

(define-public crate-gravatar-rs-0.2.0 (c (n "gravatar-rs") (v "0.2.0") (d (list (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0pi0mzm9wrs32bjr7vg3ihhix8nnqgck6gys9a1iirahnwwf2yad")))

(define-public crate-gravatar-rs-0.2.1 (c (n "gravatar-rs") (v "0.2.1") (d (list (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "01nkisbsgysg36wri3zlw2ibjrs465v780jrqz0h84sxbvlillb0")))

(define-public crate-gravatar-rs-0.2.2 (c (n "gravatar-rs") (v "0.2.2") (d (list (d (n "insta") (r "^1.20.0") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "06d2clf8bnd0hn73k1kc9nm6zc8jh2arnpzr9jpapkms5bln35kg")))

(define-public crate-gravatar-rs-0.2.3 (c (n "gravatar-rs") (v "0.2.3") (d (list (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1cxxk9gm630jafb9av1dgszm6qiyj83svba8c8wyx5rwnhmii945")))

