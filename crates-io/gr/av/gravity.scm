(define-module (crates-io gr av gravity) #:use-module (crates-io))

(define-public crate-gravity-0.1.0 (c (n "gravity") (v "0.1.0") (h "0i5iv3rrpj0bvhz17ipb8823njaqy71zvx7xqzidpif4wl9wpcph")))

(define-public crate-gravity-0.1.1 (c (n "gravity") (v "0.1.1") (h "0zml3347y9n05kzjwvjmvzwfyzcynz1r565k5b0w51c3v4qyl4yr") (y #t)))

(define-public crate-gravity-0.1.2 (c (n "gravity") (v "0.1.2") (h "1sx1hglj9b35ssqzr455s32vdv6qhv96l03az9nv3h3qgpwxm57r")))

(define-public crate-gravity-0.1.3 (c (n "gravity") (v "0.1.3") (h "0q02dr9ix3fxwlkr6bfyfabk4z57g5n04ddy608ziv2zfab12irl")))

