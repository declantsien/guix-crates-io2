(define-module (crates-io gr av graviton_core) #:use-module (crates-io))

(define-public crate-graviton_core-0.6.0 (c (n "graviton_core") (v "0.6.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1963z8w5nxy2rmj13xrgipxr0y3ykxpfvhpswk9dixkbw01adqds")))

