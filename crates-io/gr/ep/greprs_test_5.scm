(define-module (crates-io gr ep greprs_test_5) #:use-module (crates-io))

(define-public crate-greprs_test_5-0.1.0 (c (n "greprs_test_5") (v "0.1.0") (h "0c5cgb15glswc8pswjpwhsq9ff5vjcm7kpzy98lih99m572b46wc")))

(define-public crate-greprs_test_5-0.1.1 (c (n "greprs_test_5") (v "0.1.1") (h "0lm9vg7x9p4i687y8gzl8ql39cwvffr2hrw06nvlq7pgizzhdiv3")))

(define-public crate-greprs_test_5-0.1.2 (c (n "greprs_test_5") (v "0.1.2") (h "0lblz054pjakax94v9r1lzczdny6hl4ca4a6wiz07lvdrq319xcr")))

(define-public crate-greprs_test_5-0.1.3 (c (n "greprs_test_5") (v "0.1.3") (h "0gj1xfzzw44vip1k24jvnyfn034wag98bq0wlcg6fgwdyvpq9fzz")))

