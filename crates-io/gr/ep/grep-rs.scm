(define-module (crates-io gr ep grep-rs) #:use-module (crates-io))

(define-public crate-grep-rs-0.1.0 (c (n "grep-rs") (v "0.1.0") (h "1cdb6x73jhxnm1lz8ig43h3k55cz9569cdinmc783k3dp0rsj70k")))

(define-public crate-grep-rs-0.1.1 (c (n "grep-rs") (v "0.1.1") (h "18qzhdzlf6i2ynfj76rr20629dsi67zmk8sa7mmqis396kf0agnl")))

(define-public crate-grep-rs-0.2.0 (c (n "grep-rs") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "13bqlkqw1dpm9474n6xglsnj093s1ngmf5b8hwrmawaixbn90rdm")))

(define-public crate-grep-rs-0.2.1 (c (n "grep-rs") (v "0.2.1") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1q9fhkf9mka7hcr3f5c10a08hxcnz3yk78i61wvrp6kp3rssy1ck")))

