(define-module (crates-io gr ep grep-table-converter) #:use-module (crates-io))

(define-public crate-grep-table-converter-0.0.2 (c (n "grep-table-converter") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0i3j7crx971mr9rbf43dzar6zld9p71f1ln918dm4vay4ifkhpa7")))

(define-public crate-grep-table-converter-0.0.3 (c (n "grep-table-converter") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "12j86zgnj8rz0fys6dffnq5y72kmrkc0hqrvq389disb1jijxfj5")))

