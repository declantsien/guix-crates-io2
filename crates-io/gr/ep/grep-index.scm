(define-module (crates-io gr ep grep-index) #:use-module (crates-io))

(define-public crate-grep-index-0.0.1 (c (n "grep-index") (v "0.0.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.14") (d #t) (k 0)))) (h "07m44fbnpaknvblaha58gyjz6c93q5m7rsjyhlnzv3rjx20lss9f")))

