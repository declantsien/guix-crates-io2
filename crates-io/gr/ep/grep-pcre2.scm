(define-module (crates-io gr ep grep-pcre2) #:use-module (crates-io))

(define-public crate-grep-pcre2-0.1.0 (c (n "grep-pcre2") (v "0.1.0") (d (list (d (n "grep-matcher") (r "^0.1.0") (d #t) (k 0)) (d (n "pcre2") (r "^0.1") (d #t) (k 0)))) (h "0xdyk2wy2dlj0plhm2z61wliaz3k3ihgf5j71qmvnprks9rxwmjs")))

(define-public crate-grep-pcre2-0.1.1 (c (n "grep-pcre2") (v "0.1.1") (d (list (d (n "grep-matcher") (r "^0.1.1") (d #t) (k 0)) (d (n "pcre2") (r "^0.1.0") (d #t) (k 0)))) (h "01qmdczxxnj5ypmyw4vs99qqc9pmxk8ip0h275nvd5qr9x00g2zb")))

(define-public crate-grep-pcre2-0.1.2 (c (n "grep-pcre2") (v "0.1.2") (d (list (d (n "grep-matcher") (r "^0.1.1") (d #t) (k 0)) (d (n "pcre2") (r "^0.1.1") (d #t) (k 0)))) (h "0ps9pcrlnw9z35w9ikq64n9fdf28jw9v6xvmcdfmwc5syq01s4ky")))

(define-public crate-grep-pcre2-0.1.3 (c (n "grep-pcre2") (v "0.1.3") (d (list (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.0") (d #t) (k 0)))) (h "1wjc3gsan20gapga8nji6jcrmwn9n85q5zf2yfq6g50c7abkc2ql")))

(define-public crate-grep-pcre2-0.1.4 (c (n "grep-pcre2") (v "0.1.4") (d (list (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.0") (d #t) (k 0)))) (h "0sk8b188j81zfrmmy7jsq0pckydz42qf7w0pd2lwyfsa2nw4yksb")))

(define-public crate-grep-pcre2-0.1.5 (c (n "grep-pcre2") (v "0.1.5") (d (list (d (n "grep-matcher") (r "^0.1.5") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)))) (h "0hfyxsavqzf4rb5vc2a4hhi8dqw75vw1h95hinp4km9b6yxyvv66")))

(define-public crate-grep-pcre2-0.1.6 (c (n "grep-pcre2") (v "0.1.6") (d (list (d (n "grep-matcher") (r "^0.1.6") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.3") (d #t) (k 0)))) (h "15cy4ryixn6slwdsb183bc72m6m2g3ww21j46jsj34ba37kbmgg7")))

(define-public crate-grep-pcre2-0.1.7 (c (n "grep-pcre2") (v "0.1.7") (d (list (d (n "grep-matcher") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pcre2") (r "^0.2.6") (d #t) (k 0)))) (h "1zwb5acalq7rvy70z4gsarqmrfaqmf9016yvxz5f573434idai02")))

