(define-module (crates-io gr ep grep_json_deserialize) #:use-module (crates-io))

(define-public crate-grep_json_deserialize-0.1.0 (c (n "grep_json_deserialize") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0xl0h6wbcan912mc1h5kr4s53shsvrj9mskn2z3gidpixpc4pbhh") (y #t)))

(define-public crate-grep_json_deserialize-0.1.1 (c (n "grep_json_deserialize") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1kh3rk6z5gnhwp3x7jmma9r4ax6sgksnkl1dn7nsybgyyzy5ka0a") (y #t)))

(define-public crate-grep_json_deserialize-0.1.2 (c (n "grep_json_deserialize") (v "0.1.2") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1lfhjsynz3mq2k1c68isgffxw92r2l8ldm24a866paw80cxgfqgl") (y #t)))

