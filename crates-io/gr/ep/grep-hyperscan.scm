(define-module (crates-io gr ep grep-hyperscan) #:use-module (crates-io))

(define-public crate-grep-hyperscan-0.0.1 (c (n "grep-hyperscan") (v "0.0.1") (d (list (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "hyperscan") (r "^0.1.8") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1yxz5fz87nsgd1knxlcdbpaagfdkrf7av8zjab43hwmf8l80mx2r")))

