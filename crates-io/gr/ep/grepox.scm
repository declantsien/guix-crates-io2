(define-module (crates-io gr ep grepox) #:use-module (crates-io))

(define-public crate-grepox-0.2.3 (c (n "grepox") (v "0.2.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1dwgqbzpd7b39hjbwrna92pj1ykzk216608y9i8b8yldrwni3nri")))

(define-public crate-grepox-0.2.4 (c (n "grepox") (v "0.2.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1as1jm5kqq6hlj0h4cqcj04nz765fi19ypnbk1whhdnciz6ma34r")))

(define-public crate-grepox-0.2.5 (c (n "grepox") (v "0.2.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0w6bhy7a0yj095mmxabmzyxa9yg4srlmhsvichj3wkpdjyasfazh")))

(define-public crate-grepox-0.2.6 (c (n "grepox") (v "0.2.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1n0d0p79w4l00350qllfndili97w4ry3rzqjsg0rvcbcz5ddf329")))

(define-public crate-grepox-0.2.7 (c (n "grepox") (v "0.2.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1w85m17mynk1n1lr9y60kn7nq0dl87baxvz74iywq43qp2mm0aid")))

(define-public crate-grepox-0.2.8 (c (n "grepox") (v "0.2.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0i3b8cjhykb6vj6mi1yrybip0zhmrgq7yx08n0y72p1dvj5yr8zh")))

(define-public crate-grepox-0.2.9 (c (n "grepox") (v "0.2.9") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "naive_opt") (r "^0.1.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0j9spc1hmnqz4lmhf4yn7sf0gq1h5vanvwyln5pyxjprckm3ifwg")))

(define-public crate-grepox-0.2.10 (c (n "grepox") (v "0.2.10") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "naive_opt") (r "^0.1.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0w8qpgl6y9gqncj70cyqda3m6y0lza0z6pkr27m5x4mh8jhql0ca")))

(define-public crate-grepox-0.2.12 (c (n "grepox") (v "0.2.12") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1b5azgsdpjdps6173czm1naqdwgnczvs4v1ywn0j03202h3q0cvp")))

(define-public crate-grepox-0.2.13 (c (n "grepox") (v "0.2.13") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1nv0qsn91awi82k32v03wid3whlr8mr7ybrs5hbqvls22ix44p4l")))

(define-public crate-grepox-0.2.14 (c (n "grepox") (v "0.2.14") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1yg44qh0crnyvj2nl6ip5b1gapyr6hcp9z6jfvqarfanr6lwq8vq")))

(define-public crate-grepox-0.2.15 (c (n "grepox") (v "0.2.15") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.30") (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1sa1p9mgsa0v13pqwm3cc9ixz7v6gbia08m6prlva49fr0xy93ma")))

(define-public crate-grepox-0.2.16 (c (n "grepox") (v "0.2.16") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.30") (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1gnjc6671x4sf8w8c7zyq0y05h75wdiz30hs2q73n0s2vdshb3bq")))

(define-public crate-grepox-0.2.17 (c (n "grepox") (v "0.2.17") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.30") (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "03avxj1ff43pq8mm7a9n9d4j9106rhf31ybpn3djfv0is924948p")))

(define-public crate-grepox-0.2.18 (c (n "grepox") (v "0.2.18") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.30") (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "13956jnnqp8jcwjxbp7hjsdv078flmq6fyy28vm69r9vsisjyn3d")))

(define-public crate-grepox-0.2.19 (c (n "grepox") (v "0.2.19") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.30") (k 0)) (d (n "naive_opt") (r "^0.1.18") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0msf67jbak5qplj944708a2d49n4p2l9l4x6anhp1givbm7f9ib2")))

