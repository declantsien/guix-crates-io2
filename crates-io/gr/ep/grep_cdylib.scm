(define-module (crates-io gr ep grep_cdylib) #:use-module (crates-io))

(define-public crate-grep_cdylib-0.1.0 (c (n "grep_cdylib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "libloading") (r "^0.7.1") (d #t) (k 2)))) (h "1dg1xa79dm04assc037lx48j5y22i163rarmm4cdjsbcxz2v4s0r") (r "1.58")))

(define-public crate-grep_cdylib-0.1.1 (c (n "grep_cdylib") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "libloading") (r "^0.7.1") (d #t) (k 2)))) (h "1diakdlfc6kdyidi3695a9kim0jl4vngah7212wwd2ll3csf33yl") (r "1.58")))

(define-public crate-grep_cdylib-0.1.2 (c (n "grep_cdylib") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "libloading") (r "^0.7.1") (d #t) (k 2)))) (h "1ak2awd58zd15w71q8l0djy9z1wp4a1lx6jgsjq6psf5lv2skb5y") (r "1.58")))

(define-public crate-grep_cdylib-0.1.3 (c (n "grep_cdylib") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "libloading") (r "^0.7.1") (d #t) (k 2)))) (h "1x0i5iirkmqi38ssa3bs00dwcdpw7fp9fmlkh90bh66fl0qlhw30") (r "1.58")))

