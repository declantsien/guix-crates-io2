(define-module (crates-io gr ep grep-reader) #:use-module (crates-io))

(define-public crate-grep-reader-0.1.0 (c (n "grep-reader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "grep") (r "^0.3") (d #t) (k 0)))) (h "0cwv0b9dqq6q27ap2aprzq9ly4xikwdrvnfmdsvk9hnrdanjhflb") (f (quote (("adhoc"))))))

(define-public crate-grep-reader-0.1.1 (c (n "grep-reader") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "grep") (r "^0.3") (d #t) (k 0)))) (h "1wvv4pkkmrmxfz551lvas579avw6y5kdjxpzwx6s0wbygzh9wx3b") (f (quote (("adhoc"))))))

(define-public crate-grep-reader-0.1.2 (c (n "grep-reader") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "grep") (r "^0.3") (d #t) (k 0)))) (h "0clidsl25lf7zzdfc6pcjqvna11587sdqv0dz07y45g5bhal0acm") (f (quote (("adhoc")))) (y #t)))

(define-public crate-grep-reader-0.1.3 (c (n "grep-reader") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "grep") (r "^0.3") (d #t) (k 0)))) (h "00hqfing671yjxrx8mjrpvgiyg9kdvjabq00rnxrhfbd2mjqdimw") (f (quote (("adhoc"))))))

