(define-module (crates-io gr ep greppy) #:use-module (crates-io))

(define-public crate-greppy-0.1.0 (c (n "greppy") (v "0.1.0") (h "1666ajg3s51rwa15f6ygnzyj2y5fwm2nxkxpvpl7bvyxv27dizzq") (y #t)))

(define-public crate-greppy-0.1.1 (c (n "greppy") (v "0.1.1") (h "1jz1hjk9wraibjnkn7ra8az8vq4x12lkik6by93wis12zj938wvp")))

