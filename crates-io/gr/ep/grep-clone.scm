(define-module (crates-io gr ep grep-clone) #:use-module (crates-io))

(define-public crate-grep-clone-0.1.0 (c (n "grep-clone") (v "0.1.0") (h "1ybyr7r58w8ighdafxdlv11aabx504lsdplyyn2a0d2p7s8b6qzy") (y #t)))

(define-public crate-grep-clone-0.1.1 (c (n "grep-clone") (v "0.1.1") (h "08dvgfddlyyc0ia6j97d4q1d74bd24vmrkaplymkwlczl2v2gslw") (y #t)))

(define-public crate-grep-clone-0.1.2 (c (n "grep-clone") (v "0.1.2") (h "0i71k2djinszw1hsnvicxyimfvsw16p1ikx8cb4hnzibr3znr6im") (y #t)))

(define-public crate-grep-clone-0.2.0 (c (n "grep-clone") (v "0.2.0") (h "1kmy3a5v3yq68d48yym7hsn24w70g8m4rj9fh14girdmdny7h4dc") (y #t)))

(define-public crate-grep-clone-0.2.1 (c (n "grep-clone") (v "0.2.1") (h "1v15m4whqn9l525q5v7sjd6swmm4dqc4zji1fkc0522v8afp0hqn")))

