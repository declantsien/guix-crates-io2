(define-module (crates-io gr ep greps-cli-vc) #:use-module (crates-io))

(define-public crate-greps-cli-vc-0.1.0 (c (n "greps-cli-vc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "0ym0xfqky2l78vis683xbvybsra7zg0pw1wm32laghfs908ln1hl")))

