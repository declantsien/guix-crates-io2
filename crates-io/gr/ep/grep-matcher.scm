(define-module (crates-io gr ep grep-matcher) #:use-module (crates-io))

(define-public crate-grep-matcher-0.0.1 (c (n "grep-matcher") (v "0.0.1") (h "0766v5ia50rph6j3dp038mz2q1zy8vqqqd8lj7ii549wfhk40dsc")))

(define-public crate-grep-matcher-0.1.0 (c (n "grep-matcher") (v "0.1.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1qm7bfxlhdyr781w63l597rdnrqksy7s4ln3c73amc0bz2sivml8")))

(define-public crate-grep-matcher-0.1.1 (c (n "grep-matcher") (v "0.1.1") (d (list (d (n "memchr") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 2)))) (h "01xn0686m3rsffcdpg6sjc4sxq206b85qv6qm1whfl8lqgh4c2c2")))

(define-public crate-grep-matcher-0.1.2 (c (n "grep-matcher") (v "0.1.2") (d (list (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 2)))) (h "03j26zygfgwyam66bl5g922gimrvp4yyzl8qvaykyklnf247bl3r")))

(define-public crate-grep-matcher-0.1.3 (c (n "grep-matcher") (v "0.1.3") (d (list (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 2)))) (h "113lafx3abrr96ahpz6yn905ian1w3qsr5hijbb909p2j0xgmhkm")))

(define-public crate-grep-matcher-0.1.4 (c (n "grep-matcher") (v "0.1.4") (d (list (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 2)))) (h "0l4k9c0iw17vqw02z0wbx1nfj9h2xiiqx1px32lhhw7ibbyy3w7x")))

(define-public crate-grep-matcher-0.1.5 (c (n "grep-matcher") (v "0.1.5") (d (list (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 2)))) (h "1k618qni7bgx9mvdp1kaznqjvn2gpgiasrmi0cqd6b066cy5c9vd")))

(define-public crate-grep-matcher-0.1.6 (c (n "grep-matcher") (v "0.1.6") (d (list (d (n "memchr") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 2)))) (h "06dka5pi4lgldv6jqf7fgxviv633y5vdfjfkr8szwib9y8lcl0ir")))

(define-public crate-grep-matcher-0.1.7 (c (n "grep-matcher") (v "0.1.7") (d (list (d (n "memchr") (r "^2.6.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 2)))) (h "00mcjar5b6y1pwf0gjdywzgh1fnp6jl612n9qznwyfm420d198s7")))

