(define-module (crates-io gr ep greprf) #:use-module (crates-io))

(define-public crate-greprf-0.1.0 (c (n "greprf") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "unicode" "env" "wrap_help" "string"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (f (quote ("use_std" "pattern" "logging"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("rc" "derive" "alloc" "serde_derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (f (quote ("reset" "asm" "oid"))) (d #t) (k 0)))) (h "1z43cgr1g968qamdnaxc1sp4w9npgwvamlz8b2j29as97hjgn1nm")))

