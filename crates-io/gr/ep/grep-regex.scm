(define-module (crates-io gr ep grep-regex) #:use-module (crates-io))

(define-public crate-grep-regex-0.0.1 (c (n "grep-regex") (v "0.0.1") (h "1cavyhlq8nlgmd0bjc16w5wqfq2cn0pygl9x56cm6jag1l9qq5fm")))

(define-public crate-grep-regex-0.1.0 (c (n "grep-regex") (v "0.1.0") (d (list (d (n "grep-matcher") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "0n9nf459gib8ljrd5ydzkjm4v7hh8m9rm0wvwx3ma2bd1rd4qb54")))

(define-public crate-grep-regex-0.1.1 (c (n "grep-regex") (v "0.1.1") (d (list (d (n "grep-matcher") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.2") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1.0.1") (d #t) (k 0)))) (h "16qcikvlxv2qx4r3iq6fcw9l3slxpz24nfh0cl4889124i0vwzld")))

(define-public crate-grep-regex-0.1.2 (c (n "grep-regex") (v "0.1.2") (d (list (d (n "grep-matcher") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1.0.1") (d #t) (k 0)))) (h "08zm06zvx53jx49g697p43d2nsrjsd7h93bn6qyi5kp4349kiv46")))

(define-public crate-grep-regex-0.1.3 (c (n "grep-regex") (v "0.1.3") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1.0.1") (d #t) (k 0)))) (h "1lbb8837gzy25n706mnidaps4jl63ym679zraj8nfy5g02zbz549")))

(define-public crate-grep-regex-0.1.4 (c (n "grep-regex") (v "0.1.4") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1.0.1") (d #t) (k 0)))) (h "090k1sbn4jq680dmgp1jyqs7f9dzn198k0806kc8f40jcjazd88n")))

(define-public crate-grep-regex-0.1.5 (c (n "grep-regex") (v "0.1.5") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "0afl67ikb42phn6fryxv2mmj97zb75llynr92fgzhin879c4vmm0")))

(define-public crate-grep-regex-0.1.6 (c (n "grep-regex") (v "0.1.6") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "bstr") (r "^0.2.10") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "12yzalhxb6xh8iyydvdpd7jwrafv3d0zbmkzhjy32nk1ifybbfld")))

(define-public crate-grep-regex-0.1.7 (c (n "grep-regex") (v "0.1.7") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "bstr") (r "^0.2.10") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "15811jh6cjh06yq0dfigaljmiki7gdfg75s1ccfhfadirq4dxvy1")))

(define-public crate-grep-regex-0.1.8 (c (n "grep-regex") (v "0.1.8") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "bstr") (r "^0.2.10") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^1") (d #t) (k 0)))) (h "1lm3mpp93m8qw6sgcqw64inadp0z061x3xb0pnn51684594mxfm7")))

(define-public crate-grep-regex-0.1.9 (c (n "grep-regex") (v "0.1.9") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "bstr") (r "^0.2.10") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.2") (d #t) (k 0)))) (h "01mx4xsrfp5hf8dpnvld1svs6i5dpg6xghigp4wkhdlcfv4m658j")))

(define-public crate-grep-regex-0.1.10 (c (n "grep-regex") (v "0.1.10") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "bstr") (r "^0.2.10") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.2") (d #t) (k 0)))) (h "13p54yqdxhlndjiadznjpv89zvxdfl8s9wpjh6qdbwl97k9zhi8k")))

(define-public crate-grep-regex-0.1.11 (c (n "grep-regex") (v "0.1.11") (d (list (d (n "aho-corasick") (r "^0.7.3") (d #t) (k 0)) (d (n "bstr") (r "^1.1.0") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.5") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.2") (d #t) (k 0)))) (h "0gs6g08gxf15a73aj8240n63c9fq25f0sc657wp7m8sk3ns9hxcr")))

(define-public crate-grep-regex-0.1.12 (c (n "grep-regex") (v "0.1.12") (d (list (d (n "bstr") (r "^1.6.2") (d #t) (k 0)) (d (n "grep-matcher") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex-automata") (r "^0.4.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.0") (d #t) (k 0)))) (h "0393d2ydvq8qdgss8k7pbnfdns7ramlhxjk7pifdldd8bh9vnj7p")))

