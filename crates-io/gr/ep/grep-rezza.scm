(define-module (crates-io gr ep grep-rezza) #:use-module (crates-io))

(define-public crate-grep-rezza-0.1.0 (c (n "grep-rezza") (v "0.1.0") (h "1carg3v15yi3yq4n8bs1qxi2yixdn476g1wpgn7kwrlspwjgsrzz")))

(define-public crate-grep-rezza-0.2.0 (c (n "grep-rezza") (v "0.2.0") (h "17g0yyflmjzlibsjz8371y0xabrfk2y13fv5z89g278vzxxa2qgz")))

