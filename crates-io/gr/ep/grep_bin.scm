(define-module (crates-io gr ep grep_bin) #:use-module (crates-io))

(define-public crate-grep_bin-0.1.0 (c (n "grep_bin") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1m9r62kfl6qafd8knmiymn3vj4yv830j32ia40w4y9a5y89pgjgi")))

(define-public crate-grep_bin-1.0.0 (c (n "grep_bin") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0089brr8j34v3bsgkxyivsf2nck579q6zbqp6fbbydzi5w3yq3yg")))

(define-public crate-grep_bin-1.0.1 (c (n "grep_bin") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0yh8hqkm2w15w5hhxby6knlb7iaiy0rrma6dazikrafck8na1v9s")))

(define-public crate-grep_bin-1.0.2 (c (n "grep_bin") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0hg9jnfyxl7yyi6l9rb847c9v58xcckzazl363rq9gvnzfbqjwdz")))

(define-public crate-grep_bin-1.1.0 (c (n "grep_bin") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "104khbp7p1m7mz9rd77iwprpfns7rwq1wipckxrq0ph4hb863fz2")))

(define-public crate-grep_bin-1.1.1 (c (n "grep_bin") (v "1.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1qhmxk7mi8ix8500vc6nn1q9ad21nhhi049ngy16dic1bwschvry")))

(define-public crate-grep_bin-1.2.0 (c (n "grep_bin") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "04qk1wjxhnqah85p4idlvsrddqk94nly5mrr69ds8wpmhl3s39d4")))

(define-public crate-grep_bin-1.2.1 (c (n "grep_bin") (v "1.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "06g9i47q60p72hvv9xfwb8k0sp0q6931g5k2fjbklvmgdln0ych6")))

(define-public crate-grep_bin-2.0.0 (c (n "grep_bin") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0wvpygyz46dxbykfzyzdq2gy3wr771bqijfhc8r2slxgnixrblxa")))

