(define-module (crates-io gr rr grrrrrs) #:use-module (crates-io))

(define-public crate-grrrrrs-0.1.0 (c (n "grrrrrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1ysz8b4whk726gp4v2lc435lwk6cbh630wafbyncr6a4xwh2c5ff")))

