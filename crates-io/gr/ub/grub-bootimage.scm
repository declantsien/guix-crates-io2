(define-module (crates-io gr ub grub-bootimage) #:use-module (crates-io))

(define-public crate-grub-bootimage-0.1.0 (c (n "grub-bootimage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1l03v6qd1va9dy21bvlk9z33p9qi14iwf2ljhlnd0m71s34j8y2z")))

(define-public crate-grub-bootimage-0.1.1 (c (n "grub-bootimage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0n92mh62cc7y60b41h7svrr8xpmrb2qrghigh7zjc3hs0c92iq51")))

(define-public crate-grub-bootimage-0.1.2 (c (n "grub-bootimage") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "159if28ms5xh195irxi0gcvncg0nnrf42xq3i0vpwpaf10grf1nb")))

(define-public crate-grub-bootimage-0.2.0 (c (n "grub-bootimage") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0nafi1z5n1z1wiv7n0ralnz40wwflgx1j41fq4ra21dizwbawibr")))

(define-public crate-grub-bootimage-0.3.0 (c (n "grub-bootimage") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0fqw7b47901zhxc64b2ml0acbshdf0m7k8ar42a39syl7l9pa7p0")))

(define-public crate-grub-bootimage-0.4.0 (c (n "grub-bootimage") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0ac1hl0zq9rd7n428b1q4c0w79yhgf23y94zks5g6pszrmg9k4g1")))

(define-public crate-grub-bootimage-0.5.0 (c (n "grub-bootimage") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1bs4fng7yrf3b87cinzig5rzznczqjpwma9njha2h5wcf5jjsyii")))

(define-public crate-grub-bootimage-0.5.1 (c (n "grub-bootimage") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1v311zgcdxnyvjg6qvdx4mzmamc2hwh15gc0wb8q8gf0yhhd8hh5") (y #t)))

(define-public crate-grub-bootimage-0.5.2 (c (n "grub-bootimage") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1wmkmpv31qv51hv1sh57x449834nwjb711gknklikg118pg8lh4p")))

