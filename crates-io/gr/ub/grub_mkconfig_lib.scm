(define-module (crates-io gr ub grub_mkconfig_lib) #:use-module (crates-io))

(define-public crate-grub_mkconfig_lib-0.0.1 (c (n "grub_mkconfig_lib") (v "0.0.1") (d (list (d (n "indent") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "shell-quote") (r "^0.5.0") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1dn0zc3l8bxws1zfgd86yr539p4vvm4ksw0nrqa2qhh1rrwms8gx")))

(define-public crate-grub_mkconfig_lib-0.0.2 (c (n "grub_mkconfig_lib") (v "0.0.2") (d (list (d (n "indent") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "shell-quote") (r "^0.5.0") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "038zyc9213kw76mpw2238dzfm2cnv3aa4wrhdn4d7kqj6p11lwlx")))

(define-public crate-grub_mkconfig_lib-0.0.3 (c (n "grub_mkconfig_lib") (v "0.0.3") (d (list (d (n "indent") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "shell-quote") (r "^0.5.0") (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "1qj4j4z338d3pfrxwmjjj6f18sib5vjycwfpws7xmg5d60w8c6pp") (y #t)))

(define-public crate-grub_mkconfig_lib-0.1.0 (c (n "grub_mkconfig_lib") (v "0.1.0") (d (list (d (n "indent") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "shell-quote") (r "^0.5.0") (k 0)) (d (n "whoami") (r "^1.5.0-pre.0") (d #t) (k 0)))) (h "1l5nyk5gr0ngl68sx906m21v2pv5ar6zxma0q2llhnwdqj4aqpy4")))

