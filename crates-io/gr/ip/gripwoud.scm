(define-module (crates-io gr ip gripwoud) #:use-module (crates-io))

(define-public crate-gripwoud-0.1.0 (c (n "gripwoud") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "assert_fs") (r "^1.1.1") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "1mg0s3k8kvg8xb8asxmiykw7n0hrd16hvxqfxipanixbrsp2qavf")))

