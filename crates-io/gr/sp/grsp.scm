(define-module (crates-io gr sp grsp) #:use-module (crates-io))

(define-public crate-grsp-0.1.0 (c (n "grsp") (v "0.1.0") (d (list (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "12m10lxm17p2m3i6hcv5s8v15ss1b7yzglz8ny5qb0w28rd0imgm")))

(define-public crate-grsp-0.1.1 (c (n "grsp") (v "0.1.1") (d (list (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1hi2ngikl58zxvqazk0d1vhl4h9z18cj4y6mllf70fwq7xaays9d")))

(define-public crate-grsp-0.1.2 (c (n "grsp") (v "0.1.2") (d (list (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "04dz9n57nhdwq1dj1yyrdp3570apbf64rz0k44s2cnmc6b39l34r")))

