(define-module (crates-io gr um grumpkin-msm) #:use-module (crates-io))

(define-public crate-grumpkin-msm-0.1.0 (c (n "grumpkin-msm") (v "0.1.0") (d (list (d (n "blst") (r "~0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "halo2curves") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "sppark") (r "~0.1.2") (d #t) (k 0)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "1q9d1vj3cad16akx001x2csxc9jf5fa18dj027fg40nacml0gi0m") (f (quote (("portable" "blst/portable") ("force-adx" "blst/force-adx") ("default") ("cuda-mobile"))))))

