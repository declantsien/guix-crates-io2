(define-module (crates-io gr ue grue) #:use-module (crates-io))

(define-public crate-grue-0.1.0 (c (n "grue") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 2)) (d (n "indextree") (r "^3.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "strsim") (r "^0.9.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0a9i1zb0qbzpzfqwqnn25zrnk5k2780s3ixh7isa44amkjnidp4d")))

