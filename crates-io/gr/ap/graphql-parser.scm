(define-module (crates-io gr ap graphql-parser) #:use-module (crates-io))

(define-public crate-graphql-parser-0.1.0 (c (n "graphql-parser") (v "0.1.0") (d (list (d (n "combine") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "0ivqd9cllvnrbd3jizck70j8gz859gysybi4094a3wn1bnbic7cx")))

(define-public crate-graphql-parser-0.2.0 (c (n "graphql-parser") (v "0.2.0") (d (list (d (n "combine") (r "= 3.0.0-beta.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "0njxkmvpk579q7zjr63rrfjgd4pnmb57hqq0rzy1ms9y83z7sfi6")))

(define-public crate-graphql-parser-0.2.1 (c (n "graphql-parser") (v "0.2.1") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "0fhfpgn7jarahbqhka7rq1knpkxhd0mspp1izgnvaasw74kgz0c6")))

(define-public crate-graphql-parser-0.2.2 (c (n "graphql-parser") (v "0.2.2") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "0dg29agcrgzra1q7q3c7p6y3x2vf9n1gbrc6xnhqk622j4zyy307")))

(define-public crate-graphql-parser-0.2.3 (c (n "graphql-parser") (v "0.2.3") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "1nkalsk4x5fahlxpn0gz0chvnwzk4hhj0ci725jg2xl6y4qkqqd5")))

(define-public crate-graphql-parser-0.3.0 (c (n "graphql-parser") (v "0.3.0") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1wp6vnrhgi6q3b942zkc6p4mi104gbw71pnc0d5c1ps7ab7d9ayi")))

(define-public crate-graphql-parser-0.4.0 (c (n "graphql-parser") (v "0.4.0") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0x74y1496bg5gm2y50ibmw2b9l2ym4cw8r233awda9j47c0wisyj")))

