(define-module (crates-io gr ap graphile_worker_crontab_types) #:use-module (crates-io))

(define-public crate-graphile_worker_crontab_types-0.5.0 (c (n "graphile_worker_crontab_types") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "chrono") (r "^0.4.33") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0r4n4j8dq4cc2dbmc6hk10vbnfw5kpxli2x4q7nfl4sj4l5sbjl4")))

(define-public crate-graphile_worker_crontab_types-0.5.1 (c (n "graphile_worker_crontab_types") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0ipa5ks8gpsnnqdsib98mr0sx5anlcf3i8aralqdygnz10ry8ih1")))

(define-public crate-graphile_worker_crontab_types-0.5.2 (c (n "graphile_worker_crontab_types") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1hi098jf6bm4yyn57k2jsj32dglz07j3d21ihp7lw3zi88528vf1")))

(define-public crate-graphile_worker_crontab_types-0.5.3 (c (n "graphile_worker_crontab_types") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 2)) (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "17hzdhxqip5d1rlhp0mdr478ay4znshmr55vsb5c0q9dg3r68azd")))

(define-public crate-graphile_worker_crontab_types-0.5.4 (c (n "graphile_worker_crontab_types") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 2)) (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1jmxisqakjvn900nbh7hhqkmzj1npbcc9y23vy3kabn857bbnpzy")))

