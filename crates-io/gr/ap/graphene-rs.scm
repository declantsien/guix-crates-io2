(define-module (crates-io gr ap graphene-rs) #:use-module (crates-io))

(define-public crate-graphene-rs-0.0.0 (c (n "graphene-rs") (v "0.0.0") (h "1867yqmy877z85mqimpf5pz4p8qxbh63xbqmiw7m9mx27x37p539")))

(define-public crate-graphene-rs-0.14.0 (c (n "graphene-rs") (v "0.14.0") (d (list (d (n "ffi") (r "^0.14.0") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "068z40gzsgbls56j48q76yx37j1m2kjp2zpj29hiwjbfy0whlipi") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-graphene-rs-0.14.8 (c (n "graphene-rs") (v "0.14.8") (d (list (d (n "ffi") (r "^0.14.8") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qx52x0rfal29wlj0bc995gam403wf0c3fm2w2wrxvrh4l9hyf73") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-graphene-rs-0.15.0 (c (n "graphene-rs") (v "0.15.0") (d (list (d (n "ffi") (r "^0.15.0") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fcl149bv9wsar9sflimnx57fkm3laiqf15m67j61dlgbph8l24b") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t) (r "1.56")))

(define-public crate-graphene-rs-0.15.1 (c (n "graphene-rs") (v "0.15.1") (d (list (d (n "ffi") (r "^0.15.1") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w2mz098dr8mlz18ssmlnln1x6c3byizqbc9kz4n5nzgpvxzjm3w") (f (quote (("dox" "ffi/dox" "glib/dox")))) (r "1.56")))

(define-public crate-graphene-rs-0.16.0 (c (n "graphene-rs") (v "0.16.0") (d (list (d (n "ffi") (r "^0.16") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n454vsccjhqx1v8kiy9iizi59x5vj7w5ygd1zcpcix60r2xxa4m") (f (quote (("dox" "ffi/dox" "glib/dox")))) (r "1.63")))

(define-public crate-graphene-rs-0.16.3 (c (n "graphene-rs") (v "0.16.3") (d (list (d (n "ffi") (r "^0.16") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ys40hfx6fmc78mbjsx0advw1vm8fjddiprvvwh9il768z9v9v4m") (f (quote (("dox" "ffi/dox" "glib/dox")))) (r "1.63")))

(define-public crate-graphene-rs-0.17.0 (c (n "graphene-rs") (v "0.17.0") (d (list (d (n "ffi") (r "^0.17") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m42p9h4jfwaa5aqi9ngg1sk659baayxc5m90q12wd3y3kr1899p") (f (quote (("dox" "ffi/dox" "glib/dox")))) (r "1.64")))

(define-public crate-graphene-rs-0.17.1 (c (n "graphene-rs") (v "0.17.1") (d (list (d (n "ffi") (r "^0.17") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mk2nkhs0cad45k0177z5h5d9w19qfv7aiwxz71dzr5hbdb13kr1") (f (quote (("dox" "ffi/dox" "glib/dox")))) (r "1.64")))

(define-public crate-graphene-rs-0.17.10 (c (n "graphene-rs") (v "0.17.10") (d (list (d (n "ffi") (r "^0.17") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1na0kg5q746kpg0x8hic06rqknbjs9050i85idafsnav4q0vpx6y") (f (quote (("dox" "ffi/dox" "glib/dox")))) (r "1.64")))

(define-public crate-graphene-rs-0.18.0 (c (n "graphene-rs") (v "0.18.0") (d (list (d (n "ffi") (r "^0.18") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mp6cb2jr3jx3rx87lyzk0cmlkf6512sxdgh664nsjq111a7zlsw") (r "1.70")))

(define-public crate-graphene-rs-0.18.1 (c (n "graphene-rs") (v "0.18.1") (d (list (d (n "ffi") (r "^0.18") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00f4q1ra4haap5i7lazwhkdgnb49fs8adk2nm6ki6mjhl76jh8iv") (f (quote (("v1_12" "ffi/v1_12")))) (r "1.70")))

(define-public crate-graphene-rs-0.19.0 (c (n "graphene-rs") (v "0.19.0") (d (list (d (n "ffi") (r "^0.19") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b0xw7mnwl403884wnqgqfmz7gam4b62hnzcqcx0gy06ypj2fy0l") (f (quote (("v1_12" "ffi/v1_12")))) (r "1.70")))

(define-public crate-graphene-rs-0.19.2 (c (n "graphene-rs") (v "0.19.2") (d (list (d (n "ffi") (r "^0.19") (d #t) (k 0) (p "graphene-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "149c49949cyk9vwq5lnnm382dy6x5794aw7nnbi2jpvcx64d7r4r") (f (quote (("v1_12" "ffi/v1_12")))) (r "1.70")))

