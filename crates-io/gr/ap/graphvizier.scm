(define-module (crates-io gr ap graphvizier) #:use-module (crates-io))

(define-public crate-graphvizier-0.0.0 (c (n "graphvizier") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ipivzf8fm2jaacgbxr03rvyfymfhrry2hpzbkf0wm49rpa9828r")))

(define-public crate-graphvizier-0.0.1 (c (n "graphvizier") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1n0c93mzkby8kshd1qbnsd0mm5n64n448lqlsvakssp8fszzyyas")))

(define-public crate-graphvizier-0.0.2 (c (n "graphvizier") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qbgk70azgpzl7yn2m3kc14gfz2qpyp24dbdr1938bar30qf5k0h")))

(define-public crate-graphvizier-0.0.3 (c (n "graphvizier") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "01w3mhkiblrqzz7vg0wwzdgmx7fsq4wzx8fmgvmv80l3g0rbkb3l")))

