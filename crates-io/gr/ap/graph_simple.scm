(define-module (crates-io gr ap graph_simple) #:use-module (crates-io))

(define-public crate-graph_simple-0.1.1 (c (n "graph_simple") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1mxk0gcrbwlz750m5m9j85gpjs4zkr0x6g0rzck4b9d1xfx1k6nj")))

(define-public crate-graph_simple-0.1.2 (c (n "graph_simple") (v "0.1.2") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0flbxfcp3pvi76698m8qf0zzxwa5ymli74s2yzbkmrkyrkk95qsp")))

(define-public crate-graph_simple-0.1.3 (c (n "graph_simple") (v "0.1.3") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1vzbkwki5ibbayxmxw6jjjwc3qbkmpy7xh8c79f93dx9m81cn9xz")))

(define-public crate-graph_simple-0.1.4 (c (n "graph_simple") (v "0.1.4") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1ml9shrdidlkjq2hs975snwp2krfhi708dmbs7gvh6y61731dm1a")))

(define-public crate-graph_simple-0.1.5 (c (n "graph_simple") (v "0.1.5") (d (list (d (n "petgraph") (r ">=0.5, <0.7") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1rh80g3v5vb4pgasl4039q3jrkhj3sj5nv7wjmv51p1vjh6igns1")))

