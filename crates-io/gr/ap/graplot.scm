(define-module (crates-io gr ap graplot) #:use-module (crates-io))

(define-public crate-graplot-0.1.0 (c (n "graplot") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "08giv0gp4f6dkqki9v8pnhwkrpgd2x1phnw217l7lq1zqjyvx08q") (y #t)))

(define-public crate-graplot-0.1.1 (c (n "graplot") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "1nmv70gfnm9iwlifmhh8lmxkkb0ac59x0nbykadgmn6p8daa537l") (y #t)))

(define-public crate-graplot-0.1.2 (c (n "graplot") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "0xzd8qcbdvy68jfmnc55n7ndmk0n75aax9fachdr5zxib26js2nf") (y #t)))

(define-public crate-graplot-0.1.3 (c (n "graplot") (v "0.1.3") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "046nkg4wbd4d51rhrq86pzm6fzlqqc60sx2599fg5avvrvhcf240")))

(define-public crate-graplot-0.1.4 (c (n "graplot") (v "0.1.4") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "17xvkim1i7f614hgxxhw1z0f469369vnqljwr775azv6d30ihmz4")))

(define-public crate-graplot-0.1.5 (c (n "graplot") (v "0.1.5") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 0)))) (h "1jq1xb2s882yb6xjqb5qxmpmrd7v9sm22l9ajw6hzc5z4c0jc1y2") (y #t)))

(define-public crate-graplot-0.1.6 (c (n "graplot") (v "0.1.6") (d (list (d (n "litequad") (r "^0.1.0") (d #t) (k 0)))) (h "0f5n16rwa2v0jj7bqr03404g803d0qksd8njj9nwswas0p5rfldn")))

(define-public crate-graplot-0.1.7 (c (n "graplot") (v "0.1.7") (d (list (d (n "litequad") (r "^0.1.0") (d #t) (k 0)))) (h "0dqcyw0qkq7myi1mdsdvvqabh6xjy9yz6qs9k9mwagr9h3yyrcyb")))

(define-public crate-graplot-0.1.8 (c (n "graplot") (v "0.1.8") (d (list (d (n "litequad") (r "^0.1.0") (d #t) (k 0)))) (h "0z9hdb47424v79177vhzdpkx9acw80d3p8i485xva1q4wq2k6nym") (y #t)))

(define-public crate-graplot-0.1.9 (c (n "graplot") (v "0.1.9") (d (list (d (n "litequad") (r "^0.1") (d #t) (k 0)))) (h "09kzx0dydn1p8xil4ab6f4d43a4cx290rhzvbxwzk49640mxs60q")))

(define-public crate-graplot-0.1.10 (c (n "graplot") (v "0.1.10") (d (list (d (n "litequad") (r "^0.1") (d #t) (k 0)))) (h "03rd54k3rarl90nc87ydsnijy52kxs15x6cjrfgk3gsfb8djxsfp")))

(define-public crate-graplot-0.1.11 (c (n "graplot") (v "0.1.11") (d (list (d (n "litequad") (r "^0.1") (d #t) (k 0)))) (h "07304nvdrij3xpa53zq5gismv1dgjrkiipwk5pa0bazy0kslg844") (y #t)))

(define-public crate-graplot-0.1.12 (c (n "graplot") (v "0.1.12") (d (list (d (n "litequad") (r "^0.1") (d #t) (k 0)))) (h "0632b9i6268mdprv5ix76mmfhlx27fg885gqqwlcfn3zad7hrgag")))

(define-public crate-graplot-0.1.13 (c (n "graplot") (v "0.1.13") (d (list (d (n "litequad") (r "^0.1") (d #t) (k 0)))) (h "0m52w5hp94x1wqp9hssqm84xwv8wng16xf8pb0057l8d232f5a8w") (y #t)))

(define-public crate-graplot-0.1.14 (c (n "graplot") (v "0.1.14") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "158w84z8xj1n2gcp26gv7lw8qiazbfzpss8zxd3b8x63imyv8w4p")))

(define-public crate-graplot-0.1.15 (c (n "graplot") (v "0.1.15") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "0zvwbnsr8ygzgxp8gwb0wyhgg7zxj102rmgxfbgff0hj9wks0dm0")))

(define-public crate-graplot-0.1.16 (c (n "graplot") (v "0.1.16") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "0s7khs4w2nqxnqsy235y752c6c6ln0w92drd200vfzh01bbk3jc6")))

(define-public crate-graplot-0.1.17 (c (n "graplot") (v "0.1.17") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "09x7scxlbhn87q13k87qhg7l7325z66m2m4iwjb04vm3i31b5sjs")))

(define-public crate-graplot-0.1.18 (c (n "graplot") (v "0.1.18") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "196y0918w1cvkifwfn9k9qhij7a1cqwhsni244hmf779gwv9yl9a")))

(define-public crate-graplot-0.1.19 (c (n "graplot") (v "0.1.19") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "1rwn8dp45g2mz1vah8swxa8ri58628cbqjdw56h8ngyvf3sp3nyb")))

(define-public crate-graplot-0.1.20 (c (n "graplot") (v "0.1.20") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "0pq5gdh77c422j164m5q0m7anyicgw3ydsscpfvmnw86kwnyi19c")))

(define-public crate-graplot-0.1.21 (c (n "graplot") (v "0.1.21") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "1z7qvdnibvcy371qnzvpq2gjkkpn3gipikrkn99xp7li6gk40929")))

(define-public crate-graplot-0.1.22 (c (n "graplot") (v "0.1.22") (d (list (d (n "litequad") (r "^0.1.2") (d #t) (k 0)))) (h "036pnmgn0rkxgy8qy5lyls5w8jhy3cy86f9iq8jyvx0ryw6jxr2y")))

