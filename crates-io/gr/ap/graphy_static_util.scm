(define-module (crates-io gr ap graphy_static_util) #:use-module (crates-io))

(define-public crate-graphy_static_util-0.2.0 (c (n "graphy_static_util") (v "0.2.0") (d (list (d (n "graphy_error") (r "^0.2") (d #t) (k 0)))) (h "0jq06hnlpc57ywz8gvajd9qp9y2jnwk8z1i5bd8zv2h2id9fjq57")))

(define-public crate-graphy_static_util-0.3.0 (c (n "graphy_static_util") (v "0.3.0") (h "1sssx7kal6651asl2frf36nwpz1n2yz28kzssym8jf480m218hjf")))

