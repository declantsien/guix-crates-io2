(define-module (crates-io gr ap graphql-tools) #:use-module (crates-io))

(define-public crate-graphql-tools-0.0.1 (c (n "graphql-tools") (v "0.0.1") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "05qgpgnkiismfyg2nqlnhzc2fnn9j5lva9mmvp4zrw36yddfky83")))

(define-public crate-graphql-tools-0.0.2 (c (n "graphql-tools") (v "0.0.2") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1mkxlgazmvs5lm5kzzgmz54k7wkkf44nr70hi2argzcvflancaaf")))

(define-public crate-graphql-tools-0.0.3 (c (n "graphql-tools") (v "0.0.3") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0k9snc8c0ij5d92g7q8bvc5ghkd7jx60gqahi3zxaigs73brw74w")))

(define-public crate-graphql-tools-0.0.4 (c (n "graphql-tools") (v "0.0.4") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0xcfmym6bk8p1dy6zgcvx2dlf1rv46rs3x08m3pgbh63qvapi84j")))

(define-public crate-graphql-tools-0.0.5 (c (n "graphql-tools") (v "0.0.5") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0kwyb0s73zwi222f7416sfqwxcq0c588g88k9h9pdijdgng401by")))

(define-public crate-graphql-tools-0.0.6 (c (n "graphql-tools") (v "0.0.6") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "1rzfhd5kb3jlxakrwfnnin5ni8ahvz4fs4lqf8rbim11bn3cdp99")))

(define-public crate-graphql-tools-0.0.7 (c (n "graphql-tools") (v "0.0.7") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "12i1slfz88anhpjjicdphwyq7n35agj2xj45gq4m6jirbgvqmc02")))

(define-public crate-graphql-tools-0.0.8 (c (n "graphql-tools") (v "0.0.8") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "17mbzcj4z7bbm5zfm53w97k3f626zrvr1y1z710qpgv7db763p28")))

(define-public crate-graphql-tools-0.0.9 (c (n "graphql-tools") (v "0.0.9") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "15579s5ardav70yiaahx9ls67p3jvqx0pb9avqial6czyhkbydp5")))

(define-public crate-graphql-tools-0.0.10 (c (n "graphql-tools") (v "0.0.10") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0kf0vh8hwiczbli4ryphmdqa72w1fi7m02fhwd1i26pdkdfpbw2s")))

(define-public crate-graphql-tools-0.0.11 (c (n "graphql-tools") (v "0.0.11") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "06yp7zv0pp02k1ds8v0c4wjsya1lj6smga6ln9kq5cz3kzvbifn1")))

(define-public crate-graphql-tools-0.0.12 (c (n "graphql-tools") (v "0.0.12") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "13lyf1yd4grxvhxkflbmznjn3x0sqspc96qshd0536ig6raq52hy")))

(define-public crate-graphql-tools-0.0.13 (c (n "graphql-tools") (v "0.0.13") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0byjqij33laydh85yfgnrnn2nmh2prpz44pia37ny1zn7f4zdbfj")))

(define-public crate-graphql-tools-0.0.14 (c (n "graphql-tools") (v "0.0.14") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "04h3q89xz79js8pxdybk190lf2nddqssgkmmqb7nmjlcc56mkfzx")))

(define-public crate-graphql-tools-0.0.15 (c (n "graphql-tools") (v "0.0.15") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0iq6l111jl94x7cqcjslxgb86l80h1c4rjnr2nlqczsk5bacsz7l")))

(define-public crate-graphql-tools-0.0.16 (c (n "graphql-tools") (v "0.0.16") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0apz59m7zxlp6w3gvx6wbpwp5930gzy44fijkfdb907ihbd26qkw")))

(define-public crate-graphql-tools-0.0.17 (c (n "graphql-tools") (v "0.0.17") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)))) (h "0l9sm58y1x1zpqq2nkrwnkxjx534b07b8jgcnpx5xmrsp4gk49yv")))

(define-public crate-graphql-tools-0.0.18 (c (n "graphql-tools") (v "0.0.18") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x0yyr7yj33ych5hjzd1i8pnvh5854f5fpc20w33mc7306dj59lp")))

(define-public crate-graphql-tools-0.0.19 (c (n "graphql-tools") (v "0.0.19") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "181ijyvjnh2ynhyphhnivfi0vnzmfa91877z27qwkrzgrmxll98d")))

(define-public crate-graphql-tools-0.0.20 (c (n "graphql-tools") (v "0.0.20") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12kbq51qxrfik6dlcjxnb1jxkxkanlyang9rq7861n1fkrdl54m7")))

(define-public crate-graphql-tools-0.1.0 (c (n "graphql-tools") (v "0.1.0") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fwdyz1gixin61i3b8ij409bpq1kmb6xns5s0yyymdz2pnpkgi0k")))

(define-public crate-graphql-tools-0.2.0 (c (n "graphql-tools") (v "0.2.0") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f1g9bpkkhxn88xa7yknb7hlcscwv52ppshlg59q70qdi2nc6w8s")))

(define-public crate-graphql-tools-0.2.1 (c (n "graphql-tools") (v "0.2.1") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02364fxpdrm8wjd8jdqzpxliyfi07q0z8wgz0gzrdmx9miwskhvv")))

(define-public crate-graphql-tools-0.2.2 (c (n "graphql-tools") (v "0.2.2") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "1wnvbcqnm866gcplk2fqp8rz76mx32kfdc9dsh95biyvmjkdzlkm")))

(define-public crate-graphql-tools-0.2.3 (c (n "graphql-tools") (v "0.2.3") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "13kw2lkaslylj331cplq4lwvs7bnkdsivmjw4ihl11cv9x8h1y6f")))

(define-public crate-graphql-tools-0.2.4 (c (n "graphql-tools") (v "0.2.4") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "0gg2za2lgagy5zjqndsw7amlj75zwnqqvxdlsh55ni4a2mkaxxxl")))

(define-public crate-graphql-tools-0.2.5 (c (n "graphql-tools") (v "0.2.5") (d (list (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "01xln0r1j2pcbzslvbjp1nbjmfg722bbd4ryb0h39qy9mx655g2s")))

