(define-module (crates-io gr ap grapple-m24c64) #:use-module (crates-io))

(define-public crate-grapple-m24c64-0.1.0 (c (n "grapple-m24c64") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0aqgsrqnypwzpcyw9xxpaqsy3ml53jl180k4bijn1lyslysk2r80")))

(define-public crate-grapple-m24c64-0.1.1 (c (n "grapple-m24c64") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "175ss0bcb46zlqza9xwy2c25csjn055k2gmpvkkbm853yvivvvz8")))

