(define-module (crates-io gr ap graph-derive) #:use-module (crates-io))

(define-public crate-graph-derive-0.0.0 (c (n "graph-derive") (v "0.0.0") (h "0r8229k488hmwjrivjlzylshijgx16kwbfkyndvh6262rdfdnk00") (f (quote (("serde") ("default"))))))

(define-public crate-graph-derive-0.0.1 (c (n "graph-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0x6wl13zkibsxwr3harqp39m4dsyza7imi3mqkx0870c13ggamnl") (f (quote (("serde") ("default"))))))

