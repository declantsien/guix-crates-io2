(define-module (crates-io gr ap graph_gen) #:use-module (crates-io))

(define-public crate-graph_gen-0.1.0 (c (n "graph_gen") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "1kglrpy32d7a9b7rsy17b7n3xdqcz1cnjhp1nav9m6h7c3w0jhrp") (y #t)))

(define-public crate-graph_gen-0.1.1 (c (n "graph_gen") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0waffma7dn779dqjbdamf14snk734irw9v13png6n59gzi3ln1sh")))

(define-public crate-graph_gen-0.1.2 (c (n "graph_gen") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "12xdr87drd34hikz4gqa3mindpn8qk7iqa6cps89y8nxvq1wwiay")))

