(define-module (crates-io gr ap graphic-walker-parser) #:use-module (crates-io))

(define-public crate-graphic-walker-parser-0.0.1 (c (n "graphic-walker-parser") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "01l315jd53sy7dy7g27s9bawwn43g58ky9iqa28np2b286q0gjfw")))

(define-public crate-graphic-walker-parser-0.0.2 (c (n "graphic-walker-parser") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "058wbssd5chjxpss9iqnfbmkqn6xsjy2rgsya7jxmndxilrpr49a")))

(define-public crate-graphic-walker-parser-0.0.3 (c (n "graphic-walker-parser") (v "0.0.3") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "1xqn61831q0d3sr4frcqvqrns5qvjs44882sbqzmi9zlmccg0chy")))

(define-public crate-graphic-walker-parser-0.0.4 (c (n "graphic-walker-parser") (v "0.0.4") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "1ngxkpm819im3xi71270hxyk79i64zpx56j1m60hm7vm1ikg5s7i")))

(define-public crate-graphic-walker-parser-0.0.5 (c (n "graphic-walker-parser") (v "0.0.5") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "0v8ym25jrdcghlwqy8mg7azgkilgprj91svsxxhc8vxj38maycfi")))

(define-public crate-graphic-walker-parser-0.0.6 (c (n "graphic-walker-parser") (v "0.0.6") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "123l2pg4yzcxzh9vn4f88kim2w8nsir8cn41qx7dhjiy9qg3gid1")))

(define-public crate-graphic-walker-parser-0.0.7 (c (n "graphic-walker-parser") (v "0.0.7") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "0g1y0dcg987mxgl3yk9yciijrm3kik9y4h4gnpcczl81d8vfvwva")))

(define-public crate-graphic-walker-parser-0.0.8 (c (n "graphic-walker-parser") (v "0.0.8") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "0sm534rzrvmbza40q9mzsrm9pwa0hjcgppp5n310sj4n2m4nh748")))

(define-public crate-graphic-walker-parser-0.0.9 (c (n "graphic-walker-parser") (v "0.0.9") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "1rcmhjlrd5mr50ippd5vc0nd3vlwckdvlj3sdnm1grg92gh6a3g7")))

(define-public crate-graphic-walker-parser-0.0.10 (c (n "graphic-walker-parser") (v "0.0.10") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "12xxk5k4l27zj7azqjwrj7ljfcm4v06k1dcz7rla9qyqcanzhqrb")))

