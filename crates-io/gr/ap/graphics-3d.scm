(define-module (crates-io gr ap graphics-3d) #:use-module (crates-io))

(define-public crate-graphics-3d-0.0.0 (c (n "graphics-3d") (v "0.0.0") (d (list (d (n "graphics-shape") (r "^0.0.1") (d #t) (k 0)) (d (n "graphics-style") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "130v0qbsp61azlhjh96wczwdjpgxdlg0d861rqx4jz5zlm2czq25") (f (quote (("default"))))))

