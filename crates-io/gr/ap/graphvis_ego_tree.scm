(define-module (crates-io gr ap graphvis_ego_tree) #:use-module (crates-io))

(define-public crate-graphvis_ego_tree-0.1.0 (c (n "graphvis_ego_tree") (v "0.1.0") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "1hghqvnqz0lnqdnw5bdf0070x57z1ids878dm56llr5vg7w2z2li")))

(define-public crate-graphvis_ego_tree-0.1.1 (c (n "graphvis_ego_tree") (v "0.1.1") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "11cjdg6lskj91b6fxf4iypkcdbn84fkjllkr56nxr02cki4vcasj")))

(define-public crate-graphvis_ego_tree-0.2.0 (c (n "graphvis_ego_tree") (v "0.2.0") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "1vg71lc3lrp3lgpa95l3nq948sb1zd0jfxgrghdflacgjbgqpy06")))

(define-public crate-graphvis_ego_tree-0.2.1 (c (n "graphvis_ego_tree") (v "0.2.1") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "0rlbq6xnyk3ivk0l27lpj1v890f14k1xykkd7kd8zlhh17cd8xq6")))

(define-public crate-graphvis_ego_tree-0.3.0 (c (n "graphvis_ego_tree") (v "0.3.0") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "1g41g2mc7mnsbczlnn58dfxgv7wf2wfsax9j7ymaqsw5fmqhh7c6")))

(define-public crate-graphvis_ego_tree-0.3.1 (c (n "graphvis_ego_tree") (v "0.3.1") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "1n0javyjyz04i2pi1zbxir965ilwnd368cv2g5n10grghxhhy0zp")))

(define-public crate-graphvis_ego_tree-0.3.2 (c (n "graphvis_ego_tree") (v "0.3.2") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "03ic41y06qb0a4rvb7884hd4rihlp8m65plih0a1ijrkjqhya0pb")))

(define-public crate-graphvis_ego_tree-0.3.3 (c (n "graphvis_ego_tree") (v "0.3.3") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "0fl1maizpvq9x1c1sxz4w0w04ndjsyz2lv43fbni672mqj9v1acf")))

(define-public crate-graphvis_ego_tree-0.3.4 (c (n "graphvis_ego_tree") (v "0.3.4") (d (list (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)))) (h "0lsr8mp8r0vha11i18yax2m8sc21ziwx28hanmx99lv4kxq2is6g")))

