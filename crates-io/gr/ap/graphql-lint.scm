(define-module (crates-io gr ap graphql-lint) #:use-module (crates-io))

(define-public crate-graphql-lint-0.1.0 (c (n "graphql-lint") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "cynic-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qqsynckvcxqa1yd9cgm7hci17ylpi16zs34qrra3gb4hc8648c6")))

(define-public crate-graphql-lint-0.1.1 (c (n "graphql-lint") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "cynic-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00ry9jg94d1yx3wjjfvadhikkjb46k7ifypsbpcazrmvcxm9fs8w")))

(define-public crate-graphql-lint-0.1.2 (c (n "graphql-lint") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "cynic-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05xgs5l41113ir6sj346kphy65sjgnvjmwl5ygc2bn0jmb9rbmna") (y #t)))

(define-public crate-graphql-lint-0.1.3 (c (n "graphql-lint") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "cynic-parser") (r "^0.2.3") (d #t) (k 0)) (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pnxqhmgd9gzv8c1fx6kwwfn26kvffkhxf85shrc8fsm0nhvfwgl")))

