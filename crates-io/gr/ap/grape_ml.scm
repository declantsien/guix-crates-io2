(define-module (crates-io gr ap grape_ml) #:use-module (crates-io))

(define-public crate-grape_ml-0.0.1 (c (n "grape_ml") (v "0.0.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "simdeez") (r "^0.6.4") (d #t) (k 0)))) (h "0980qx423wnvvv032rmgh9zpiq30yb5v3yds514vrxli62lqngz3")))

