(define-module (crates-io gr ap graphiti) #:use-module (crates-io))

(define-public crate-graphiti-0.1.0 (c (n "graphiti") (v "0.1.0") (d (list (d (n "legion") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "1jwnrd3clks41jbi5n71jlg9bd8z8p02jawn4pzklm9ybvpxjfsi")))

(define-public crate-graphiti-0.1.1 (c (n "graphiti") (v "0.1.1") (d (list (d (n "legion") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "0g88990fqcj3kb30cxp9kkkj30nw90q0fc7i2c9v3wrqaxspzrmm")))

(define-public crate-graphiti-0.1.2 (c (n "graphiti") (v "0.1.2") (d (list (d (n "legion") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "1a42d8hs1y3v9rfj6p7vl5wgbdnhkd3prx6149dg2qdldzj2i561")))

