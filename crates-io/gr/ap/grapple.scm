(define-module (crates-io gr ap grapple) #:use-module (crates-io))

(define-public crate-grapple-0.3.1 (c (n "grapple") (v "0.3.1") (d (list (d (n "base64") (r "~0.9.2") (d #t) (k 0)) (d (n "clap") (r "~2.32") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "~1.1") (d #t) (k 0)) (d (n "md5") (r "~0.3.8") (d #t) (k 0)) (d (n "pbr") (r "~1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "~0.8.0") (d #t) (k 0)) (d (n "url") (r "~1.7") (d #t) (k 0)) (d (n "uuid") (r "~0.6.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mb0rv3risxc3zbdsyksnkpz9nmhkzshggrs8l7rpags3zy1zfmz") (f (quote (("default"))))))

