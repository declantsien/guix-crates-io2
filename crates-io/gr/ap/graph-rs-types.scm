(define-module (crates-io gr ap graph-rs-types) #:use-module (crates-io))

(define-public crate-graph-rs-types-0.1.0 (c (n "graph-rs-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)))) (h "152in49s1bi89yrdpgysyiscf9w15gz4kpgn62w07rhhvq7nsmcm")))

