(define-module (crates-io gr ap graphql_rs_native) #:use-module (crates-io))

(define-public crate-graphql_rs_native-0.0.1 (c (n "graphql_rs_native") (v "0.0.1") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "0ixif2l2kqbz40zdl2nvrlf5sjyn1iva4463xibyacvcf84cdnrk")))

