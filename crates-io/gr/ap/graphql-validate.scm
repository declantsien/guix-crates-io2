(define-module (crates-io gr ap graphql-validate) #:use-module (crates-io))

(define-public crate-graphql-validate-0.1.0 (c (n "graphql-validate") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "05iiq55p5n2j960liis8cxjqgixzhqvrfw1m99a3v3ni0sf0g0jl")))

(define-public crate-graphql-validate-0.2.0 (c (n "graphql-validate") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "0lsb4p8vjim5bv5b1lqyqx1jp0w6h6ib4ccal45x19ydflrmp381")))

(define-public crate-graphql-validate-0.3.0 (c (n "graphql-validate") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "1rbjvfhdf3sfgfggi3gwx8nlr4rd99s8j2r326zmafk0flihxph3")))

(define-public crate-graphql-validate-0.3.1 (c (n "graphql-validate") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "0h465l2b35aqilhkzcf8ala4zyg5303wqnw0gcwnhfyypvybnmr5")))

