(define-module (crates-io gr ap graphql_query_derive) #:use-module (crates-io))

(define-public crate-graphql_query_derive-0.0.1 (c (n "graphql_query_derive") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0dpbf22wbhlgc8rh4k61ygy1602nlqww26qv1v1amcnpj6w08hbz")))

(define-public crate-graphql_query_derive-0.1.0 (c (n "graphql_query_derive") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "051snqyczqz3awqx44igy0qhglyp7f0smcwwxcsallascgrb82gb")))

(define-public crate-graphql_query_derive-0.2.0 (c (n "graphql_query_derive") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "06rrhzychmr011532gxs3lqcwcrwzdssixg7xhh6bh51i20jb26y")))

(define-public crate-graphql_query_derive-0.3.0 (c (n "graphql_query_derive") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1z5ydckdyzf1afv29p53y7ifk583q0gjill27rfch7wddjnp0q22")))

(define-public crate-graphql_query_derive-0.4.0 (c (n "graphql_query_derive") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1xd6f03sjw1c4pp5i05ir81mvxwiii4mp5d4wcnp71fjppn5mrpx")))

(define-public crate-graphql_query_derive-0.5.0 (c (n "graphql_query_derive") (v "0.5.0") (d (list (d (n "graphql_client_codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1qlxcij1p4awyp0fs80p2hmvpvbj6fgl0qhpna78sks1bcp57nsh")))

(define-public crate-graphql_query_derive-0.5.1 (c (n "graphql_query_derive") (v "0.5.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1fa0hh3k1vcfqpvypqxfhq10vy54423hqx2p2wk9sdlv7nrfi43y")))

(define-public crate-graphql_query_derive-0.6.0 (c (n "graphql_query_derive") (v "0.6.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xyy5la8b566ikglslcv1h5qqlrgn3815s9bbcj1ry2abmdc8gmz")))

(define-public crate-graphql_query_derive-0.6.1 (c (n "graphql_query_derive") (v "0.6.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fz5aiz9qq1nf1iszk9lcnzmg38ggz6df79b0vfakn3pwbi2ll7p")))

(define-public crate-graphql_query_derive-0.7.0 (c (n "graphql_query_derive") (v "0.7.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1a77nbh3fzjy850gjhrs4a9smkrcg2npcbgdqf5gpv66mcwn9dbn")))

(define-public crate-graphql_query_derive-0.8.0 (c (n "graphql_query_derive") (v "0.8.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1iyha4ywjs39l70grg3hmai6z0aq72d56f6y1xm2m2j2svzm3lmh")))

(define-public crate-graphql_query_derive-0.9.0 (c (n "graphql_query_derive") (v "0.9.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql_client_codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bywba0yy76pcw58wni7448bj26h6sfwsjg6m5x24jg5bi6v3xp1")))

(define-public crate-graphql_query_derive-0.10.0 (c (n "graphql_query_derive") (v "0.10.0") (d (list (d (n "graphql_client_codegen") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0la2pg9gp7qfd1snz9y824jzs4f89iph72vmk7df27d7zlxhjsz5")))

(define-public crate-graphql_query_derive-0.11.0 (c (n "graphql_query_derive") (v "0.11.0") (d (list (d (n "graphql_client_codegen") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kppq7k7mfx0dzds47swyllw6w84yiz9ykvv0fiiwr52rmcwqmd7")))

(define-public crate-graphql_query_derive-0.12.0 (c (n "graphql_query_derive") (v "0.12.0") (d (list (d (n "graphql_client_codegen") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "166cvb0lfvi85xqwwnhw8dppy1m3aqgb74h6xhalpx0ix36wjbym")))

(define-public crate-graphql_query_derive-0.13.0 (c (n "graphql_query_derive") (v "0.13.0") (d (list (d (n "graphql_client_codegen") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "137lfn228zmjazsrjmh26718nd1d14an2qlg544zj4ykydaa9g80")))

(define-public crate-graphql_query_derive-0.14.0 (c (n "graphql_query_derive") (v "0.14.0") (d (list (d (n "graphql_client_codegen") (r "^0.14.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15qsxdp2yqixzd6fjvn03810jd7zd7msi9zs7nkwz67q72lbzzl3")))

