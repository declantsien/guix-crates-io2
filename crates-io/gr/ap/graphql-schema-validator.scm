(define-module (crates-io gr ap graphql-schema-validator) #:use-module (crates-io))

(define-public crate-graphql-schema-validator-0.1.0 (c (n "graphql-schema-validator") (v "0.1.0") (d (list (d (n "async-graphql-parser") (r "^6.0.9") (d #t) (k 0)) (d (n "async-graphql-value") (r "^6.0.9") (d #t) (k 0)))) (h "1agpr7kia4va9g1gnh9npr4hgkx886z0njb575b9krfjn84bza94")))

