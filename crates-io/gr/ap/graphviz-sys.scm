(define-module (crates-io gr ap graphviz-sys) #:use-module (crates-io))

(define-public crate-graphviz-sys-0.1.0-dev (c (n "graphviz-sys") (v "0.1.0-dev") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "09l4xclyyhf3imkj6qxxr7wxagmdjlgg730s5l5w06jnz4kn6ixd")))

(define-public crate-graphviz-sys-0.1.0 (c (n "graphviz-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1d76mv4kmn12rdj214xhbc68y6km682glhsxbz5sd8rxjwqybv5q")))

(define-public crate-graphviz-sys-0.1.1 (c (n "graphviz-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1g23ja152if6yhpy6wcail89h4dyga7l6hmlq8gl6xq2a8azp9w0")))

(define-public crate-graphviz-sys-0.1.2 (c (n "graphviz-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1gln31rd27xy6mx6gan843kpaxia85zrh859d8sla957bpbpr6ah")))

(define-public crate-graphviz-sys-0.1.3 (c (n "graphviz-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "1g9pfvk3ypdgv0b33vry5ll2fisrw23x4xc2crjx1hl5hs11j246")))

