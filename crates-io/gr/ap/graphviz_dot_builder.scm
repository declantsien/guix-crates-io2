(define-module (crates-io gr ap graphviz_dot_builder) #:use-module (crates-io))

(define-public crate-graphviz_dot_builder-0.1.0 (c (n "graphviz_dot_builder") (v "0.1.0") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1cfsprhxphb4k4nbxccqh8kcmp2ibjzx3kngpyd47bgqppii0ldi")))

(define-public crate-graphviz_dot_builder-0.1.1 (c (n "graphviz_dot_builder") (v "0.1.1") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1753qc7idmsc55j90ph4xgr4q6q9a95h4kgn7bwqgv8dbvm2dg0a")))

(define-public crate-graphviz_dot_builder-0.1.2 (c (n "graphviz_dot_builder") (v "0.1.2") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0rvqybx3kx8l3l3nlf115kb7br6mbkl2d7jiimfryxn8gyns9anf")))

