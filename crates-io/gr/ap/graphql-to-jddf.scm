(define-module (crates-io gr ap graphql-to-jddf) #:use-module (crates-io))

(define-public crate-graphql-to-jddf-0.1.0 (c (n "graphql-to-jddf") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "graphql_client") (r "^0.8") (d #t) (k 0)) (d (n "jddf") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "16l2kwdaqj6b07bx90xjz735wf7s0r2pqjd1vy12zi67dnfpmll6")))

