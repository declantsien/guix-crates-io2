(define-module (crates-io gr ap graphviz_parser) #:use-module (crates-io))

(define-public crate-graphviz_parser-0.1.0 (c (n "graphviz_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "18v4k98r11vjvpxsfp3c5qvw0rpq50nayyhf8nskv235i41zq5dr")))

