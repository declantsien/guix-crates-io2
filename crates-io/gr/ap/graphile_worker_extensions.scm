(define-module (crates-io gr ap graphile_worker_extensions) #:use-module (crates-io))

(define-public crate-graphile_worker_extensions-0.1.2 (c (n "graphile_worker_extensions") (v "0.1.2") (h "1jh6jjax1wpl4cac6gf6blmh7wvcbjgffh2y5jq3l5jy1yb7x2dw")))

(define-public crate-graphile_worker_extensions-0.1.3 (c (n "graphile_worker_extensions") (v "0.1.3") (h "0r65hbk4q4jgl7k9h95vxs2mvf43ks4p3qnk83h0dd4fqd5hc9s5")))

