(define-module (crates-io gr ap graphplan) #:use-module (crates-io))

(define-public crate-graphplan-0.1.0 (c (n "graphplan") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "061gw4873cdab28gi36q7d4jz1pvkchmxzf3vsrr59zi7wzy9ma9")))

(define-public crate-graphplan-0.1.1 (c (n "graphplan") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01wx4w6cl95k2h7f791ml0j6azdas66pg3cl3kg4si1dvpfwg4ck")))

(define-public crate-graphplan-0.1.2 (c (n "graphplan") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "157f6h9pm3rl908gay0asnsyxrcdqvmlsm4w9icdi16xbr6nd66w")))

(define-public crate-graphplan-0.1.3 (c (n "graphplan") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0rkqrbqm8666z3lrwj4h2fbmmp8kfg36i80fbp18gw3jck81ljdy")))

(define-public crate-graphplan-0.2.0 (c (n "graphplan") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "0jqnr6z6y8iwl881zfnki21mpnr0hv2zbpl51zrs1nm54a42vk95") (f (quote (("wasm" "stdweb"))))))

(define-public crate-graphplan-0.3.0 (c (n "graphplan") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "1iiw3q6jxs6d9mfzr0cxxbhjv5awdwqvdn2klpdzcbhyjab8xksa") (f (quote (("wasm" "stdweb"))))))

(define-public crate-graphplan-0.4.0 (c (n "graphplan") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "stdweb") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "toml_crate") (r "^0.4.2") (o #t) (d #t) (k 0) (p "toml")))) (h "16f6mz39gbn6nzn1r68g7h9zxi8yzi18cdbrl6mzrnyw75vjrj37") (f (quote (("wasm" "stdweb" "toml_crate" "serde" "serde_derive") ("toml" "toml_crate"))))))

(define-public crate-graphplan-0.5.0 (c (n "graphplan") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "stdweb") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "toml_crate") (r "^0.4.2") (o #t) (d #t) (k 0) (p "toml")))) (h "1flx75bm5lj4lg3jgyb9rn46sbc4dzbq2x94zj9ba7fsril2hmha") (f (quote (("wasm" "stdweb" "toml_crate" "serde" "serde_derive") ("toml" "toml_crate" "serde" "serde_derive"))))))

(define-public crate-graphplan-0.6.0 (c (n "graphplan") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)))) (h "19mgji5clbi19rv354k5dr7ckgd80gxr5l52ky4w047zdl7ab178")))

(define-public crate-graphplan-0.6.1 (c (n "graphplan") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)))) (h "0hpwd9ns8gqrp0wpn9lnm5chg0l7pb0jbqwaisa2l1902d7yhziq")))

