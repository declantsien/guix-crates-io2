(define-module (crates-io gr ap graphite_command) #:use-module (crates-io))

(define-public crate-graphite_command-0.1.0 (c (n "graphite_command") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bytemuck") (r "^1.10.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "graphite_command_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_mc_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hsb9pggpgly39sif7cskwg83i8dbwfjvjk6kk0q2b86msn4djfg")))

