(define-module (crates-io gr ap graphite_magma) #:use-module (crates-io))

(define-public crate-graphite_magma-0.1.0 (c (n "graphite_magma") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "graphite_binary") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_server") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "073b8xcimxgjyixw96sj9b3ifzxrrjvhh597vndar1w1wmpwl2as")))

