(define-module (crates-io gr ap graph-cycles) #:use-module (crates-io))

(define-public crate-graph-cycles-0.1.0 (c (n "graph-cycles") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lz8ydyhl9rlhdm4n32kj7ayzgkvz114lxb6dgqzlg6xqqrdjsis")))

