(define-module (crates-io gr ap graphy_backend) #:use-module (crates-io))

(define-public crate-graphy_backend-0.3.0 (c (n "graphy_backend") (v "0.3.0") (d (list (d (n "graphy_dll") (r "^0.3") (d #t) (k 0)) (d (n "graphy_environment") (r "^0.3") (d #t) (k 0)) (d (n "graphy_error") (r "^0.2") (d #t) (k 0)) (d (n "graphy_module") (r "^0.3") (d #t) (k 0)))) (h "0ks2dgqx6fh6ni8kmfjy48pabczhfa9gy3wn8x2q4y5x0hdnb0iw")))

