(define-module (crates-io gr ap graphviz-dot-parser) #:use-module (crates-io))

(define-public crate-graphviz-dot-parser-0.0.1 (c (n "graphviz-dot-parser") (v "0.0.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "1h104kzfzvhhjj6crixjnkjp94zxvwrbm54miqgi22axdd9qw78x")))

(define-public crate-graphviz-dot-parser-0.0.2 (c (n "graphviz-dot-parser") (v "0.0.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "0077529jcb2pzbzzkgmjgqfplp75lavbqxim28sv1c3qdkiia0h7")))

(define-public crate-graphviz-dot-parser-0.0.3 (c (n "graphviz-dot-parser") (v "0.0.3") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "070zzb6nvm08dqkd35j9c4mkdk88hz1gj5l45i6diq92bgpi2k60")))

(define-public crate-graphviz-dot-parser-0.0.4 (c (n "graphviz-dot-parser") (v "0.0.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1hazw3ykblh6qwvgbcf5wjfdg96b9i7n4v6774cqmfxdjc5fh0lj")))

