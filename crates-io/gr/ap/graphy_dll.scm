(define-module (crates-io gr ap graphy_dll) #:use-module (crates-io))

(define-public crate-graphy_dll-0.2.0 (c (n "graphy_dll") (v "0.2.0") (d (list (d (n "graphy_error") (r "^0.2.0") (d #t) (k 0)) (d (n "graphy_plugin") (r "^0.2.0") (d #t) (k 0)) (d (n "libloading") (r "= 0.2.1") (d #t) (k 0)) (d (n "serde") (r "= 0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "= 0.7.2") (d #t) (k 1)) (d (n "serde_json") (r "= 0.7.0") (d #t) (k 0)) (d (n "syntex") (r "= 0.31.0") (d #t) (k 1)))) (h "0g3pa10bp41grb214rkyryb1zhcmd8i49l9ymmjwk1allgkgzm7w")))

(define-public crate-graphy_dll-0.3.0 (c (n "graphy_dll") (v "0.3.0") (d (list (d (n "graphy_error") (r "^0.2") (d #t) (k 0)) (d (n "graphy_module") (r "^0.3") (d #t) (k 0)) (d (n "libloading") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.32") (d #t) (k 1)))) (h "0fll4lbak5gspp21a0rn2w7lcrp44add753vvrncwh3wz635216y")))

