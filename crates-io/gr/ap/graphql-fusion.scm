(define-module (crates-io gr ap graphql-fusion) #:use-module (crates-io))

(define-public crate-graphql-fusion-0.1.0 (c (n "graphql-fusion") (v "0.1.0") (d (list (d (n "async-graphql-parser") (r "^6.0.9") (d #t) (k 0)) (d (n "async-graphql-value") (r "^6.0.9") (d #t) (k 0)))) (h "09jmzxwqnrdknaj4i7dgl5khx91ch0kyzxn88ayndy1kyx8f2ahw")))

