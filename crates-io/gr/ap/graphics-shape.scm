(define-module (crates-io gr ap graphics-shape) #:use-module (crates-io))

(define-public crate-graphics-shape-0.0.0 (c (n "graphics-shape") (v "0.0.0") (d (list (d (n "graphics-style") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0lzdlcpxd90851gdw9mji41c884j5a3iyir7nwxl2lackxl65kfy") (f (quote (("default"))))))

(define-public crate-graphics-shape-0.0.1 (c (n "graphics-shape") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "19syyr61vv6rb56d9hzxy2sbkb89bc8ly4gfjaddnv8kvs59hm37") (f (quote (("default"))))))

(define-public crate-graphics-shape-0.0.2 (c (n "graphics-shape") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "projective") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b6ic9r7r5bykfdfhg396rwgm060la5lbhnmpihzrh6n7pj9sjwp") (f (quote (("default"))))))

(define-public crate-graphics-shape-0.0.3 (c (n "graphics-shape") (v "0.0.3") (d (list (d (n "shape-core") (r "^0.0.3") (d #t) (k 0)))) (h "1n10wr93n7sj25s4yj80577pr92z2hh77ldds6zxmxspdjn697z4") (f (quote (("wolfram") ("default"))))))

(define-public crate-graphics-shape-0.0.5 (c (n "graphics-shape") (v "0.0.5") (d (list (d (n "shape-core") (r "^0.1.1") (d #t) (k 0)))) (h "055w6bxfsqprfg23igm0lpsi4x27ryfzz34cfsh91izlnrg6wjly") (f (quote (("wolfram-expr" "shape-core/wolfram-expr") ("default"))))))

(define-public crate-graphics-shape-0.1.0 (c (n "graphics-shape") (v "0.1.0") (d (list (d (n "shape-core") (r "^0.1.1") (d #t) (k 0)))) (h "13isbddis05jr8n6a1dhcxfka2lcx4p51hw514hrik1j9hihwysb") (f (quote (("wolfram-expr" "shape-core/wolfram-expr") ("default"))))))

(define-public crate-graphics-shape-0.1.1 (c (n "graphics-shape") (v "0.1.1") (d (list (d (n "shape-core") (r "^0.1.1") (d #t) (k 0)))) (h "17pls0i0qn8jzhl8iw9wqqd15jnvrnq9x7wd2cwkgg0n85kncd7z") (f (quote (("wolfram-expr" "shape-core/wolfram-expr") ("default"))))))

