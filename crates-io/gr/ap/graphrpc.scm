(define-module (crates-io gr ap graphrpc) #:use-module (crates-io))

(define-public crate-graphrpc-0.0.0 (c (n "graphrpc") (v "0.0.0") (h "1b2fs5f23ninsg0n991ghrlvvpcpbx0zn9sl12g0mz1dsvwp4b54") (y #t)))

(define-public crate-graphrpc-0.0.1 (c (n "graphrpc") (v "0.0.1") (h "1ia2dnkkrc68dqmv6nkgs2cqj4ihfwqlb70wgb55cqr5wnkkkknv")))

