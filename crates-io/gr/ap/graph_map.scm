(define-module (crates-io gr ap graph_map) #:use-module (crates-io))

(define-public crate-graph_map-0.1.0 (c (n "graph_map") (v "0.1.0") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0aj47nz39i2fwrb4krhmp7zawsbjxrnqgpmr7d0xpbyh0gqwqbzz")))

