(define-module (crates-io gr ap graphics-rs) #:use-module (crates-io))

(define-public crate-graphics-rs-0.0.1 (c (n "graphics-rs") (v "0.0.1") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "0w7im02h7jqkfpnb02rysyh3zmx0z0nmw9wl03sv73vx7i7yyjyb") (y #t)))

(define-public crate-graphics-rs-0.0.2 (c (n "graphics-rs") (v "0.0.2") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "12cvazxxsjlqkdx2z0wqd1i4jlppf7jf9qncbpifp939w494mf66") (y #t)))

(define-public crate-graphics-rs-0.0.3 (c (n "graphics-rs") (v "0.0.3") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "043jv9pnq23klv4ykn95jfz5s8881i6i5xgq566mc1g499vfw731") (y #t)))

(define-public crate-graphics-rs-0.0.4 (c (n "graphics-rs") (v "0.0.4") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "1c0wwqa3vp1sbs63bq0w5mcwp50qycb45s08qcqnv171qbh8ghrv") (y #t)))

(define-public crate-graphics-rs-0.0.5 (c (n "graphics-rs") (v "0.0.5") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "02bpg38d88gmlmyh7xdjnma33fyg4sd5xcwd1pf16qgff49rgwsn") (y #t)))

(define-public crate-graphics-rs-0.0.6 (c (n "graphics-rs") (v "0.0.6") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "0lzslyx0dhg3gx0f3w4nizr5bid6sfcqjpdl2m3n9vziva8hrrvn") (y #t)))

(define-public crate-graphics-rs-0.0.7 (c (n "graphics-rs") (v "0.0.7") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "1sq3bc3cyc6bm9y979dkj0ga5mvz5lcg67gx13nr3244z3ljdbbp") (y #t)))

(define-public crate-graphics-rs-0.0.8 (c (n "graphics-rs") (v "0.0.8") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "1ia2lf42b8s0f7d0nxf7kkj5ghy5n9r063xj462gvpwrc2p29nix") (y #t)))

(define-public crate-graphics-rs-0.0.9 (c (n "graphics-rs") (v "0.0.9") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "0w1270pcw2kvdff4frcgbwibyyc8siy62cylxqarj13rmdvc90yb") (y #t)))

(define-public crate-graphics-rs-0.0.10 (c (n "graphics-rs") (v "0.0.10") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "0yi25gqdnrc31lkajp25y7ppj9zn3ndmnkgglh1718zqyi9k9ap8")))

