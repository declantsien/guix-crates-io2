(define-module (crates-io gr ap graphile_worker_macros) #:use-module (crates-io))

(define-public crate-graphile_worker_macros-0.2.0 (c (n "graphile_worker_macros") (v "0.2.0") (d (list (d (n "graphile_worker_task_handler") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 2)))) (h "0cx19rxg44vki55zjgwlsaim4hf43yk44l7ysx3hcckib9ajndns")))

(define-public crate-graphile_worker_macros-0.2.1 (c (n "graphile_worker_macros") (v "0.2.1") (d (list (d (n "graphile_worker_task_handler") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 2)))) (h "0cj64skybwrxka29c0l7v6wilq5sqgqylr21lyqs2y390v8b7v30")))

(define-public crate-graphile_worker_macros-0.2.2 (c (n "graphile_worker_macros") (v "0.2.2") (d (list (d (n "graphile_worker_task_handler") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 2)))) (h "16m6bsjci3n72418v2gpkvg5mzlcfxrmm6lricm3v5y7jcw0pr5h")))

