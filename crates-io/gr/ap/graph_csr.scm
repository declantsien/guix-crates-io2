(define-module (crates-io gr ap graph_csr) #:use-module (crates-io))

(define-public crate-graph_csr-0.1.0 (c (n "graph_csr") (v "0.1.0") (d (list (d (n "easy_mmap") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g377kh3ncfgdwh33izm01mvlzg1bm2nl5mqfssky63hm5m1vd04")))

(define-public crate-graph_csr-0.1.1 (c (n "graph_csr") (v "0.1.1") (d (list (d (n "easy_mmap") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0slgzv3wi63di7x6h7rgg2kpky6271ijflyhfjmqsckggai6z3x0")))

(define-public crate-graph_csr-0.2.0 (c (n "graph_csr") (v "0.2.0") (d (list (d (n "easy_mmap") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qr3n8snypwba6fhbrwy8qbhkqzaa0jqml4gdi2pjkkvnqpmnifi")))

(define-public crate-graph_csr-0.2.1 (c (n "graph_csr") (v "0.2.1") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "easy_mmap") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1iz6m8v71l1zj9wh5f9nidnhyl17d8mizfkpy3pr6byfwb4xgx7n")))

(define-public crate-graph_csr-0.2.2 (c (n "graph_csr") (v "0.2.2") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "easy_mmap") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "02yznyngzvc630gf70prjm1k03298ps5lfslgb61qnpmzgr8v3cr")))

(define-public crate-graph_csr-0.3.0 (c (n "graph_csr") (v "0.3.0") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "easy_mmap") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "03f16sh2pqwqkz165p2n6v4smkzf6j8w7wp828cg0nsb3wxxjsyr")))

(define-public crate-graph_csr-0.3.1 (c (n "graph_csr") (v "0.3.1") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "easy_mmap") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "06p6mjgfqbi6fl99jmqrbz29rp9h07g3jg9b8lrilsyi7mzv2n7f")))

(define-public crate-graph_csr-1.0.0 (c (n "graph_csr") (v "1.0.0") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "easy_mmap") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1lnr2vkj72hpgq83i5fd7y0gprz09rqpvski0pdb9akn9lsjmsrm")))

(define-public crate-graph_csr-1.0.1 (c (n "graph_csr") (v "1.0.1") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "easy_mmap") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1cd3wpsk3aj7padqz7rk1pnidpdc66db0pnybxx4145vgb7mjii1")))

(define-public crate-graph_csr-1.0.2 (c (n "graph_csr") (v "1.0.2") (d (list (d (n "atomic") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "easy_mmap") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "053fj99zb9jkn4514zy1z48gx8qi256nd96vv9ahc44dg72p7lmc")))

