(define-module (crates-io gr ap grapl-os-user-generator-plugin) #:use-module (crates-io))

(define-public crate-grapl-os-user-generator-plugin-0.1.0 (c (n "grapl-os-user-generator-plugin") (v "0.1.0") (d (list (d (n "derive-dynamic-node") (r "^0.1.6") (d #t) (k 0)) (d (n "grapl-graph-descriptions") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1kp1vw93vn1ph4f2mpj7i1062zncaql9rml89q47q11jynxc2fmq")))

(define-public crate-grapl-os-user-generator-plugin-0.1.1 (c (n "grapl-os-user-generator-plugin") (v "0.1.1") (d (list (d (n "derive-dynamic-node") (r "^0.1.6") (d #t) (k 0)) (d (n "grapl-graph-descriptions") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "192pxm6s130j6v9s9w9w49v57g5rwl6kkyspz0z8j2fns2sgxhz5")))

(define-public crate-grapl-os-user-generator-plugin-0.1.2 (c (n "grapl-os-user-generator-plugin") (v "0.1.2") (d (list (d (n "derive-dynamic-node") (r "^0.1.6") (d #t) (k 0)) (d (n "grapl-graph-descriptions") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "02fcxg463bz6w0l46xw5ja16cj7yhg62jhfh91afgszcy7xv2bf7")))

