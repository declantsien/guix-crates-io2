(define-module (crates-io gr ap graphviz-ffi) #:use-module (crates-io))

(define-public crate-graphviz-ffi-0.1.0 (c (n "graphviz-ffi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "04mm1crjgsrzjzfjzp7qffhd6fnh7i007crdk8sc9iy9gr9h1cvf")))

(define-public crate-graphviz-ffi-0.1.1 (c (n "graphviz-ffi") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "1plphkihlp72a9jy6nhmaiy7skj42k1byahg1wlyxiqdxym11mjc")))

(define-public crate-graphviz-ffi-0.1.2 (c (n "graphviz-ffi") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0qzihhhlaarzpyziycficvwm59d1x8p41ay6yi9j0rwh66dq31xq")))

