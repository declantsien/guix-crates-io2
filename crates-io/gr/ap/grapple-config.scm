(define-module (crates-io gr ap grapple-config) #:use-module (crates-io))

(define-public crate-grapple-config-0.1.0 (c (n "grapple-config") (v "0.1.0") (d (list (d (n "deku") (r "^0.16.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0xbl0243c4b98ayya94y2dmvphla3vbsq0p3nkkmpkp7vq0kw220") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.1 (c (n "grapple-config") (v "0.1.1") (d (list (d (n "deku") (r "^0.16.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0h5lvmsa6d8yzyc97zdi0g5bwd8iskq5bwz16nkzp7mdmgkhl533") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.2 (c (n "grapple-config") (v "0.1.2") (d (list (d (n "binmarshal") (r "^0.1.1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1y7ch6wlzr8qxv5ivvg9c3v0wv418wm9daj68r8bxryv6v1ak5qs") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.3 (c (n "grapple-config") (v "0.1.3") (d (list (d (n "binmarshal") (r "^0.1.1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1rdha7vxk5hp33z13h3cikk29w70da783ljx92xvsrdcrp8mchxr") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.4 (c (n "grapple-config") (v "0.1.4") (d (list (d (n "binmarshal") (r "^0.1.1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1imxhzc616ld24wny820jhz7pvnrxsjv6zlwllqy7kczdby8f1vi") (f (quote (("default" "m24c64")))) (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.5 (c (n "grapple-config") (v "0.1.5") (d (list (d (n "binmarshal") (r "^0.1.1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1fns1mjq3bqz2cq0xckh7pszgdz6kslkpi097980k76wrykqf1xq") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.6 (c (n "grapple-config") (v "0.1.6") (d (list (d (n "binmarshal") (r "^0.1.1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1sh0lcj88ysc7wqzwb21hbchjirav60ni64c31ms4jdvzidm2hvk") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.7 (c (n "grapple-config") (v "0.1.7") (d (list (d (n "binmarshal") (r "^0.1.1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0bxqf48i1jibklyqsj0lqjq9klmv3yw05lqcfkvaghaj9crk6m9x") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.8 (c (n "grapple-config") (v "0.1.8") (d (list (d (n "binmarshal") (r "^0.1.1") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "14capbidm6nl3xc8vvz7qgb81s3caz1ryswwnyf2i2kg6sx9si1z") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.1.9 (c (n "grapple-config") (v "0.1.9") (d (list (d (n "binmarshal") (r "^0.2.0") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1a1x6381rsnh0cv43wnxfnif0gblw2yis7hxl568cq2bvmzs0x2x") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

(define-public crate-grapple-config-0.2.0 (c (n "grapple-config") (v "0.2.0") (d (list (d (n "binmarshal") (r "^1.0.0") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "grapple-m24c64") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "03nam33kblki2y1k9l41a53fhxv0sgvii5kbn70frjqmqd5v7pyw") (s 2) (e (quote (("m24c64" "dep:grapple-m24c64"))))))

