(define-module (crates-io gr ap grappes) #:use-module (crates-io))

(define-public crate-grappes-0.1.0 (c (n "grappes") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11.0") (d #t) (k 0)))) (h "1vazw127iy3f7a0i7zs29j33rjx0l3xi7d0awlq86fzdgj06jrgl")))

(define-public crate-grappes-0.2.0 (c (n "grappes") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0yxxwr66pdzs7azinzzrv3c5wqfp1nwh17h3x1hxfd0711cyr2j4")))

