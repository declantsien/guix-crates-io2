(define-module (crates-io gr ap graphite_binary_macros) #:use-module (crates-io))

(define-public crate-graphite_binary_macros-0.1.0 (c (n "graphite_binary_macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "08bm14yzw764k149519xv1hf6qj3x07djra1gyipz8v8sgw7nrih")))

