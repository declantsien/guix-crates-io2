(define-module (crates-io gr ap graphgate-validation) #:use-module (crates-io))

(define-public crate-graphgate-validation-0.3.2 (c (n "graphgate-validation") (v "0.3.2") (d (list (d (n "graphgate-schema") (r "^0.3.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "parser") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-parser")) (d (n "value") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-value")))) (h "03xilr9h496vpycdh202699gk9amr79im8ssbx5a0i25pxy96488")))

(define-public crate-graphgate-validation-0.4.0 (c (n "graphgate-validation") (v "0.4.0") (d (list (d (n "graphgate-schema") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "parser") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-parser")) (d (n "value") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-value")))) (h "0mc68kpdbvbj0ydx8v140gv1pvz7j7ycmby0y7s1nz4pfs30pw99")))

(define-public crate-graphgate-validation-0.5.0 (c (n "graphgate-validation") (v "0.5.0") (d (list (d (n "graphgate-schema") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "parser") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-parser")) (d (n "value") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-value")))) (h "1r0jbjmjbfcg2lkdv7xz70jnwnp6ibwh9al0yva316b9gk6v7v3w")))

