(define-module (crates-io gr ap graphgate-schema) #:use-module (crates-io))

(define-public crate-graphgate-schema-0.3.2 (c (n "graphgate-schema") (v "0.3.2") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "parser") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-parser")) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "value") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-value")))) (h "1p4ic9d7m01vhy0g6my4zjdqf64lws1lp4ci4r59qycgxs22ll66")))

(define-public crate-graphgate-schema-0.4.0 (c (n "graphgate-schema") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "parser") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-parser")) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "value") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-value")))) (h "0r7wr1gn1bphmsdx0in7hfbw3sqlryl0dj2nfl555msmw4pmj9x2")))

(define-public crate-graphgate-schema-0.5.0 (c (n "graphgate-schema") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "parser") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-parser")) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "value") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-value")))) (h "06y6mf3vm6vr4za2y0v13vf7b65w0bwg2drbpq31jvpakrqhnp75")))

(define-public crate-graphgate-schema-0.5.1 (c (n "graphgate-schema") (v "0.5.1") (d (list (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "parser") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-parser")) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "value") (r "^2.5.9") (d #t) (k 0) (p "async-graphql-value")))) (h "0mlwipyyrs71fslp5q67mgli89jzksfdrmbyxqa4miwxn0kiaqi1")))

