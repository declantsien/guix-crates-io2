(define-module (crates-io gr ap graphite_binary) #:use-module (crates-io))

(define-public crate-graphite_binary-0.1.0 (c (n "graphite_binary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "graphite_binary_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hematite-nbt") (r "^0.5.2") (d #t) (k 2)) (d (n "quartz_nbt") (r "^0.2.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0whmrpjbdlbn7g22q61s98jk4rfjr2w54rpcwsrs4l1wcx6b9j0x")))

