(define-module (crates-io gr ap graphdb) #:use-module (crates-io))

(define-public crate-graphdb-0.0.0 (c (n "graphdb") (v "0.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1km2zjld5alpn4dhwpvjxh3ckqxn7jihb8sgwhpnv9whdxfly11r")))

