(define-module (crates-io gr ap grapes) #:use-module (crates-io))

(define-public crate-grapes-0.1.0 (c (n "grapes") (v "0.1.0") (d (list (d (n "imbl") (r "^1.0.1") (d #t) (k 0)))) (h "0d21nnpz0dy9bij7dd4hfk9k797zngdw2jv3gsaqmgalgh3r0ani")))

(define-public crate-grapes-0.1.1 (c (n "grapes") (v "0.1.1") (d (list (d (n "imbl") (r "^1.0.1") (d #t) (k 0)))) (h "05znv2msr5fz9kw5cqww5vbhydxl76bf8vmnw9f6sd6immcm6w5v")))

(define-public crate-grapes-0.1.2 (c (n "grapes") (v "0.1.2") (d (list (d (n "imbl") (r "^1.0.1") (d #t) (k 0)))) (h "1xwq3f0kg447fjd088r9r02045r99q2dmlhy21ir3ymah24wh3rz")))

(define-public crate-grapes-0.2.0 (c (n "grapes") (v "0.2.0") (d (list (d (n "imbl") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)))) (h "0sb6z9mgvkkix2m3mwlvjwj8w7jcnkykgmkhjbldndr40l0izjbm") (s 2) (e (quote (("serde" "dep:serde" "imbl/serde"))))))

(define-public crate-grapes-0.3.0 (c (n "grapes") (v "0.3.0") (d (list (d (n "imbl") (r "^1.0.1") (d #t) (k 0)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)) (d (n "velcro") (r "^0.5.3") (d #t) (k 2)))) (h "1q889znnl1h0s5pr340bc205k67n4ficihckimysaam3sp9hmz3p") (s 2) (e (quote (("serde" "dep:serde" "imbl/serde"))))))

