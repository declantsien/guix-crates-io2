(define-module (crates-io gr ap graph-search) #:use-module (crates-io))

(define-public crate-graph-search-0.0.2 (c (n "graph-search") (v "0.0.2") (h "100in6shv5n36f4mjp82nax0gax6hdjn61xk36bwahan3pxxy7g9") (y #t)))

(define-public crate-graph-search-0.1.0 (c (n "graph-search") (v "0.1.0") (h "0xb0bw93hfip1yibrpdl885mjj3r3bfpw7qxv8q82iqmwgcbn7j5") (y #t)))

(define-public crate-graph-search-0.2.0 (c (n "graph-search") (v "0.2.0") (h "1q84alga8nnxglwnvcgja7srhj0r34xs3k615ydnrn8qw42lc2c8") (y #t)))

