(define-module (crates-io gr ap grapl-graph-descriptions-py) #:use-module (crates-io))

(define-public crate-grapl-graph-descriptions-py-0.1.0 (c (n "grapl-graph-descriptions-py") (v "0.1.0") (d (list (d (n "grapl-graph-descriptions") (r "^0.2.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0a4lpawdggfh7fsiywvgclhmv6j13c9cnwc2h3l2l22a33kspqqa")))

(define-public crate-grapl-graph-descriptions-py-0.1.1 (c (n "grapl-graph-descriptions-py") (v "0.1.1") (d (list (d (n "grapl-graph-descriptions") (r "^0.2.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "015gsxv7fkyc1gzifp8byvipbgc8s04bi1zy7qjappx4bz00iak2")))

(define-public crate-grapl-graph-descriptions-py-0.1.2 (c (n "grapl-graph-descriptions-py") (v "0.1.2") (d (list (d (n "grapl-graph-descriptions") (r "^0.2.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0ff70b0i53dhzmlr2gd3kyr1kqz1ynmy38r3wrcj48fczdqrslnd")))

(define-public crate-grapl-graph-descriptions-py-0.1.3 (c (n "grapl-graph-descriptions-py") (v "0.1.3") (d (list (d (n "grapl-graph-descriptions") (r "^0.2.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1cdmll2njb9fd8fcyy6sxy008lmwqjz7gs7a5rq9za29a1z661wc")))

