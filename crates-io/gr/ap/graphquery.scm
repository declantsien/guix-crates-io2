(define-module (crates-io gr ap graphquery) #:use-module (crates-io))

(define-public crate-graphquery-0.1.0 (c (n "graphquery") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1hh4b44jzasxhm044lgwi788dp351rajkq1zqq9d529axy6c5c9g")))

(define-public crate-graphquery-0.2.0 (c (n "graphquery") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "07m41hkvlc5ffpk6h5av4vk720mrj632y9fhca1ikbs88wkg9jim")))

(define-public crate-graphquery-0.3.0 (c (n "graphquery") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0x9xkdg9rcy5ar268kjwzpji52azdqmaqv9k1r0xmdr3544dp5n2")))

