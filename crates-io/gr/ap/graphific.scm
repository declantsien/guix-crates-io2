(define-module (crates-io gr ap graphific) #:use-module (crates-io))

(define-public crate-graphific-0.1.0 (c (n "graphific") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0f6g9jvya4w7lmiw8ckdy9v12dzlws8pi94va58pm23lwh9dj9j8")))

(define-public crate-graphific-0.1.1 (c (n "graphific") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0qshdxajrx4iyhfj14bhvn84z8zn0f3lhb9z8p9jncqylszry7xk")))

