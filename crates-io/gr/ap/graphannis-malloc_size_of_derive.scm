(define-module (crates-io gr ap graphannis-malloc_size_of_derive) #:use-module (crates-io))

(define-public crate-graphannis-malloc_size_of_derive-0.0.1 (c (n "graphannis-malloc_size_of_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "0d1di0yh2yhjaybjcrpbfhrbc0qwm6cv1b9byhwckq29rz65xdf0")))

(define-public crate-graphannis-malloc_size_of_derive-0.1.0 (c (n "graphannis-malloc_size_of_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "0mq6sqi97sic33xrr23vdamwy2addf3634mmkqbypjf5jif6xrnm")))

(define-public crate-graphannis-malloc_size_of_derive-1.0.0 (c (n "graphannis-malloc_size_of_derive") (v "1.0.0") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.8") (d #t) (k 0)))) (h "0qc88kgl5za2nikgr2qsq17k4v4bs6zklfd5jvvda0bj85flb1h3")))

(define-public crate-graphannis-malloc_size_of_derive-2.0.0 (c (n "graphannis-malloc_size_of_derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.11") (d #t) (k 0)))) (h "1ydgpv93dfl61wc5kmdkr142xh1md5cgqqag20gglahbjvd8sbc1")))

