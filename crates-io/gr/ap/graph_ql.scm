(define-module (crates-io gr ap graph_ql) #:use-module (crates-io))

(define-public crate-graph_ql-0.1.0 (c (n "graph_ql") (v "0.1.0") (h "12ys2f0ghqggdbr9zldbcgph1ylkvxin5y8l91qqk9dl3r67z76r")))

(define-public crate-graph_ql-0.1.1 (c (n "graph_ql") (v "0.1.1") (h "0597bfriwldr3df3kf16npry46jy9979ya4rs1kmskys4pvn88ra")))

