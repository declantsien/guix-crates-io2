(define-module (crates-io gr ap graph-generators) #:use-module (crates-io))

(define-public crate-graph-generators-0.0.1 (c (n "graph-generators") (v "0.0.1") (d (list (d (n "petgraph") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0fc5saamc42svhfzmzckanqkih18nxd2j1nl5v0w18waq33dm27m")))

(define-public crate-graph-generators-0.1.0 (c (n "graph-generators") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0w571m0v68fh844w79vlzhvvk6cxidgvw0453b8m9sr1b7yzb8q4")))

