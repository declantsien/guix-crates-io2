(define-module (crates-io gr ap graphflo) #:use-module (crates-io))

(define-public crate-graphflo-0.1.0 (c (n "graphflo") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libflo") (r "^0.1") (d #t) (k 0)) (d (n "libflo_error") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)) (d (n "mut_static") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0ncn0p7w8d6dfr28c2d0vzidy3mdjazqyaravs5dpmp9hfxky95k") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

