(define-module (crates-io gr ap graphs_tools) #:use-module (crates-io))

(define-public crate-graphs_tools-0.1.0 (c (n "graphs_tools") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1khrcm18k2h02afl7knlnfj3lr583vdks69wld3nvsma37ahd1zp")))

(define-public crate-graphs_tools-0.1.1 (c (n "graphs_tools") (v "0.1.1") (d (list (d (n "indexmap") (r "~1.8") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtools") (r "~0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1snijcglhm47hf0arx4dhi1flb8v3srgvw4qnmc7mwvhp3agrnzc") (f (quote (("use_std") ("use_alloc") ("full" "use_std") ("default" "use_std"))))))

(define-public crate-graphs_tools-0.1.2 (c (n "graphs_tools") (v "0.1.2") (d (list (d (n "indexmap") (r "~1.8") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtools") (r "~0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15dhsdzfqbi5dxbkh14ffrbmib00a87gqa0rld051jq9lbn2g7cv") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc" "cell_factory") ("default" "use_std" "cell_factory") ("cell_factory"))))))

