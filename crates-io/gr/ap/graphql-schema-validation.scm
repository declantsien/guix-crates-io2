(define-module (crates-io gr ap graphql-schema-validation) #:use-module (crates-io))

(define-public crate-graphql-schema-validation-0.1.0 (c (n "graphql-schema-validation") (v "0.1.0") (d (list (d (n "async-graphql-parser") (r "^6.0.9") (d #t) (k 0)) (d (n "async-graphql-value") (r "^6.0.9") (d #t) (k 0)))) (h "1fbl76y7hpq8lcp8q7jj2cz87lmn8rc7kr31hzhm6z703vyk8y55")))

(define-public crate-graphql-schema-validation-0.1.1 (c (n "graphql-schema-validation") (v "0.1.1") (d (list (d (n "async-graphql-parser") (r "^6") (d #t) (k 0)) (d (n "async-graphql-value") (r "^6") (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "datatest-stable") (r "^0.2.3") (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 2)) (d (n "similar") (r "^2") (d #t) (k 2)))) (h "09mb4jyz1mc2qfb5a0fliv68m64sz3p99n1yqfyw3i88466kkfkh")))

(define-public crate-graphql-schema-validation-0.1.2 (c (n "graphql-schema-validation") (v "0.1.2") (d (list (d (n "async-graphql-parser") (r "^6") (d #t) (k 0)) (d (n "async-graphql-value") (r "^6") (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "datatest-stable") (r "^0.2.3") (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 2)) (d (n "similar") (r "^2") (d #t) (k 2)))) (h "0gqhkm5xddfnf6phcy7zll1lx5vxfb625r725qjav8ifhq2fhymk")))

(define-public crate-graphql-schema-validation-0.1.3 (c (n "graphql-schema-validation") (v "0.1.3") (d (list (d (n "async-graphql-parser") (r "^7") (d #t) (k 0)) (d (n "async-graphql-value") (r "^7") (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "datatest-stable") (r "^0.2.3") (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 2)) (d (n "similar") (r "^2") (d #t) (k 2)))) (h "0ysypqmd33p6iv7kcac05hg37q1n4af3p74iqwn38z77j8b701mp")))

