(define-module (crates-io gr ap graphql-cli) #:use-module (crates-io))

(define-public crate-graphql-cli-0.1.0 (c (n "graphql-cli") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.3") (d #t) (k 0)))) (h "0whnx78k2h4a7k85bhfb2cvadhk8fgb858j6dj227qhazns86akr")))

