(define-module (crates-io gr ap graphity) #:use-module (crates-io))

(define-public crate-graphity-0.0.1 (c (n "graphity") (v "0.0.1") (h "0nr4jvz4r6k1hdg8p1ilggllxhx642ps77g8i5f6v7cma0bcljfz")))

(define-public crate-graphity-1.0.0 (c (n "graphity") (v "1.0.0") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "0f7f1zq355clqynmwwa1jbsssxj95zx913dl1s2n4y08fc9cjj1c")))

(define-public crate-graphity-2.0.0 (c (n "graphity") (v "2.0.0") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "120mzfdb3p9qk7720izrnh9mz9pjcbsxnda93ilx8p6c08pwc7in")))

