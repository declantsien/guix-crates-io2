(define-module (crates-io gr ap graphy_event) #:use-module (crates-io))

(define-public crate-graphy_event-0.2.0 (c (n "graphy_event") (v "0.2.0") (d (list (d (n "graphy_dll") (r "^0.2") (d #t) (k 0)) (d (n "graphy_error") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.31") (d #t) (k 1)))) (h "0rpypnyhjsrp58s73yahnrrmvba8kn48l2fpfxf0k0668594wv5s")))

(define-public crate-graphy_event-0.3.0 (c (n "graphy_event") (v "0.3.0") (d (list (d (n "graphy_dll") (r "^0.3") (d #t) (k 0)) (d (n "graphy_error") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.32") (d #t) (k 1)))) (h "15nlk3abqpsw0k38r11sliabsgkywgv7m48q1ij4hmk06ncr3vkl")))

