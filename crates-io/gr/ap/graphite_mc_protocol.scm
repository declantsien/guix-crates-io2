(define-module (crates-io gr ap graphite_mc_protocol) #:use-module (crates-io))

(define-public crate-graphite_mc_protocol-0.1.0 (c (n "graphite_mc_protocol") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "graphite_binary") (r "^0.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1s9rdjy7p0fp708y84ar8i8092hxgdhz8cgazyvj93l37xcp8qyh")))

