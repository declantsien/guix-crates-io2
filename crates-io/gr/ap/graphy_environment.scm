(define-module (crates-io gr ap graphy_environment) #:use-module (crates-io))

(define-public crate-graphy_environment-0.2.0 (c (n "graphy_environment") (v "0.2.0") (d (list (d (n "graphy_dll") (r "^0.2") (d #t) (k 0)) (d (n "graphy_module") (r "^0.2") (d #t) (k 0)) (d (n "syntex") (r "^0") (d #t) (k 1)))) (h "1y7vw0xz5nlm8y3k771zm0jyixpw6hxykfzdppagyj3v2vvk7rfb")))

(define-public crate-graphy_environment-0.3.0 (c (n "graphy_environment") (v "0.3.0") (d (list (d (n "graphy_dll") (r "^0.3") (d #t) (k 0)) (d (n "graphy_module") (r "^0.3") (d #t) (k 0)))) (h "0sicigw30759r57f7rvi9xg7pajb5dpqyp0cnzcj5drzk12i450s")))

