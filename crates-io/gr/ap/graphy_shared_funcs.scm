(define-module (crates-io gr ap graphy_shared_funcs) #:use-module (crates-io))

(define-public crate-graphy_shared_funcs-0.3.0 (c (n "graphy_shared_funcs") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.4.2") (d #t) (k 0)) (d (n "graphy_dll") (r "^0.3") (d #t) (k 0)) (d (n "graphy_dll_error") (r "^0.3") (d #t) (k 0)) (d (n "graphy_environment") (r "^0.3") (d #t) (k 0)) (d (n "graphy_event") (r "^0.3") (d #t) (k 0)) (d (n "graphy_module") (r "^0.3") (d #t) (k 0)) (d (n "graphy_static") (r "^0.2") (d #t) (k 0)))) (h "0yb2bxv95xvi822c9piyliy3h1pi0q4cgikvn9mwzvgd82b2xkhx")))

