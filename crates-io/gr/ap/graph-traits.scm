(define-module (crates-io gr ap graph-traits) #:use-module (crates-io))

(define-public crate-graph-traits-0.1.0 (c (n "graph-traits") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "07jsp8f032i9ysg55zp30cy7zz83v0w05fc9yp4wb6sq9hjl7fk5") (f (quote (("default" "petgraph"))))))

(define-public crate-graph-traits-0.1.1 (c (n "graph-traits") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "03ys7c68qlg90j7vbkpn9d580v8gq5h59zl4732bxf6bjigai10n") (f (quote (("default" "petgraph"))))))

