(define-module (crates-io gr ap graph-types) #:use-module (crates-io))

(define-public crate-graph-types-0.0.0 (c (n "graph-types") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)))) (h "13f2rr9l5bk81zfbk9j5ck4dsaqqvbmclln45hls8fnpak48382j") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.1 (c (n "graph-types") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)))) (h "1fapq57rnqlfq81d7g974amy8r96pyrs3kqcsqynbfhv3x8fvcfh") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.2 (c (n "graph-types") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)))) (h "064a6xy1pgff6fpvh0kd4fw7h71a4hzy6zjnfxpjl42smwvg86qq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.3 (c (n "graph-types") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "0lkqgrxvgdamkmnqcfwa66mq2smf8waysnkfa6c43ph25k38grkh") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.4 (c (n "graph-types") (v "0.0.4") (d (list (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "graph-theory") (r "0.0.*") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "0rfnyicc0lf7q9gk3jfvzhxmyvkwmd5z18y4j0jymwxh8q60xv3j") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.5 (c (n "graph-types") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "graph-theory") (r "0.0.*") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "1l1sapc8nrpmpffbscy2w8ii7yg61np4832aqk6bmpnx1r3r74jn") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.6 (c (n "graph-types") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "graph-theory") (r "0.0.*") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "0g4p8bdb2hrw0c2779p9w7bx2qwhprp0qf7hag1rd5n9x8pqjali") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.7 (c (n "graph-types") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "graph-theory") (r "0.0.*") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "103n9mlfyw8z3nx8j38zpky2kmpjspn2kyxf5arkc47inpm24xcj") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.8 (c (n "graph-types") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "graph-theory") (r "0.0.*") (d #t) (k 2)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "1glk40d3crldkhg4hbz5vg5inyx9xz2a7aw63jg1qkqaqpjfpvlq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.9 (c (n "graph-types") (v "0.0.9") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "graph-theory") (r "0.0.*") (d #t) (k 2)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "0577bxfn3idhjnyyqy31yij7vn7bwwy0snpis8166cvl2gwxzak7") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.10 (c (n "graph-types") (v "0.0.10") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "graph-theory") (r "0.0.*") (d #t) (k 2)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.3") (o #t) (d #t) (k 0)))) (h "1s8d3nm7jd1ims5jr127j6isyqnbsvlsyvjcqlgsd5x0dpgb4kia") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.12 (c (n "graph-types") (v "0.0.12") (d (list (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "1dr5f5rcwnpl6lpwcs8adh0v1j6lwxk87mdgh0aclk7qwnhyq8ca") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-graph-types-0.0.13 (c (n "graph-types") (v "0.0.13") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 2)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "1rj2pgijbr7d0kl33im7vq0bbvbp3mbjv9p6b1q1j80lppnas67j") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

