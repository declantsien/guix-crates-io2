(define-module (crates-io gr ap graphql_query_builder) #:use-module (crates-io))

(define-public crate-graphql_query_builder-0.1.0 (c (n "graphql_query_builder") (v "0.1.0") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "1ayb9svn3axs1fjkwp8r8qdrx90zhgj3vsrh0y3cs5778a7s21d5")))

(define-public crate-graphql_query_builder-0.1.1 (c (n "graphql_query_builder") (v "0.1.1") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "18dsyp6nsjnbw3r5i83dl1732z7q4wc5vss01d1pgypk06470pam")))

(define-public crate-graphql_query_builder-0.1.2 (c (n "graphql_query_builder") (v "0.1.2") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0a6lmy5303py9ijc20p5ladsxdnafrsi4q8p7p4wsb01cap9fwkj")))

(define-public crate-graphql_query_builder-0.1.3 (c (n "graphql_query_builder") (v "0.1.3") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "119q9qnq3n7amw6wwa396q1pv3dblnpba8hdjgnh3q4v9b5zp11z")))

(define-public crate-graphql_query_builder-0.2.0 (c (n "graphql_query_builder") (v "0.2.0") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "1m6568972ilzys86dn8zrk3fs3408b5zqjfq8ivj4fcqn10rf650")))

(define-public crate-graphql_query_builder-0.2.1 (c (n "graphql_query_builder") (v "0.2.1") (d (list (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0mkdg6knphycwwhyyi7n4d54h6bn9jd6mfsj0m85vrqlp8lp6g0v")))

