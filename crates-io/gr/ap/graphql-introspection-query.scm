(define-module (crates-io gr ap graphql-introspection-query) #:use-module (crates-io))

(define-public crate-graphql-introspection-query-0.1.0 (c (n "graphql-introspection-query") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gwpyd1qabfnq8wcn56jbw1kzbkxhai4mwyfsix4aamx3mjaq2k1")))

(define-public crate-graphql-introspection-query-0.2.0 (c (n "graphql-introspection-query") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vgd5y0zaw9n5hwd21xbcsswy6asg17ljd1411nbsh2irwr4fakz")))

