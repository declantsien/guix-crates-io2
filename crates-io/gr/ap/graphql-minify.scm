(define-module (crates-io gr ap graphql-minify) #:use-module (crates-io))

(define-public crate-graphql-minify-0.1.0 (c (n "graphql-minify") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.3") (d #t) (k 2)) (d (n "logos") (r "^0.13.0") (f (quote ("std"))) (d #t) (k 0)))) (h "1fl9894mwi5lkv2ki6c120ajgbl9fbp81ix7qimbi78psp4gx2db")))

