(define-module (crates-io gr ap graph_symmetry) #:use-module (crates-io))

(define-public crate-graph_symmetry-0.1.0 (c (n "graph_symmetry") (v "0.1.0") (d (list (d (n "chem") (r "^0.0.2") (d #t) (k 0)) (d (n "purr") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0zng7qjmsr7dcinznmsgbv9fqbsdmmxkimfiv6dvwmg11g6vmyvx")))

(define-public crate-graph_symmetry-0.1.1 (c (n "graph_symmetry") (v "0.1.1") (d (list (d (n "chem") (r "^0.0.2") (d #t) (k 0)) (d (n "purr") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1b52nk8mb5qf6sdhaijv64iz5sf4123vyfkql48pcjz0ss14s6kr")))

