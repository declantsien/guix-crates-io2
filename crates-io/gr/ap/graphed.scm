(define-module (crates-io gr ap graphed) #:use-module (crates-io))

(define-public crate-graphed-0.1.0 (c (n "graphed") (v "0.1.0") (h "0si9bnzzrgily5p93xzndsl4mgvw7yr28mg1akqpjhz7x5cfh350")))

(define-public crate-graphed-0.2.0 (c (n "graphed") (v "0.2.0") (h "1s62xrpx3rsb6s2p1z0i183aj668ghbqvyjcvlgm676mwm4ch31r")))

(define-public crate-graphed-0.3.0 (c (n "graphed") (v "0.3.0") (h "0lasf9b68gp4j8cd9sg0adm3h2c251l23dhys8q7lagk49p5vn1d")))

