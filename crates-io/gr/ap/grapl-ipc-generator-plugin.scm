(define-module (crates-io gr ap grapl-ipc-generator-plugin) #:use-module (crates-io))

(define-public crate-grapl-ipc-generator-plugin-0.1.0 (c (n "grapl-ipc-generator-plugin") (v "0.1.0") (d (list (d (n "derive-dynamic-node") (r "^0.1.4") (d #t) (k 0)) (d (n "grapl-graph-descriptions") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "17bz2rczmkxk1p02qraprqqslal2jpipp3lsn3zx7c2hra4a17vi")))

(define-public crate-grapl-ipc-generator-plugin-0.1.1 (c (n "grapl-ipc-generator-plugin") (v "0.1.1") (d (list (d (n "derive-dynamic-node") (r "^0.1.4") (d #t) (k 0)) (d (n "grapl-graph-descriptions") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0wv1mq8kmf58sg1d1hbrnxksbb62v6kjj4yfdznkyhq59bq9x9zz")))

