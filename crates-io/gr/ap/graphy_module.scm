(define-module (crates-io gr ap graphy_module) #:use-module (crates-io))

(define-public crate-graphy_module-0.2.0 (c (n "graphy_module") (v "0.2.0") (d (list (d (n "graphy_error") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0") (d #t) (k 1)))) (h "10z1ljc1lpdkk1w30bmg437077gaa7c769g620vgkh1f0myxnfs6")))

(define-public crate-graphy_module-0.3.0 (c (n "graphy_module") (v "0.3.0") (d (list (d (n "graphy_error") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "syntex") (r "^0.32") (d #t) (k 1)))) (h "1vspnhk7l91vb74zrlaa0p1014mdhf3vy6zar214p8fzlf3sshs2")))

