(define-module (crates-io gr ap graphy_static) #:use-module (crates-io))

(define-public crate-graphy_static-0.2.0 (c (n "graphy_static") (v "0.2.0") (d (list (d (n "graphy_dll") (r "^0.2") (d #t) (k 0)) (d (n "graphy_plugin") (r "^0.2") (d #t) (k 0)) (d (n "graphy_static_util") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1fk2saz38vgik67vn9c2d8j3ksh5icqzm5y4amwf3w38cf9vrpdi")))

(define-public crate-graphy_static-0.3.0 (c (n "graphy_static") (v "0.3.0") (d (list (d (n "graphy_environment") (r "^0.3") (d #t) (k 0)) (d (n "graphy_static_util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "100sid2kfnxdy1vy2w3xznc8wrsgig2p3v56wfj4kiw3159sk575")))

