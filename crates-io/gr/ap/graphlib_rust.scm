(define-module (crates-io gr ap graphlib_rust) #:use-module (crates-io))

(define-public crate-graphlib_rust-0.0.1 (c (n "graphlib_rust") (v "0.0.1") (d (list (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "04l16wzgxy7k1vk696s78azgdx7p9s0fnmbqjmgip3acgfjrq1yb")))

(define-public crate-graphlib_rust-0.0.2 (c (n "graphlib_rust") (v "0.0.2") (d (list (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "1sh5v8pa9ymvzry64mim5pcdgwsri7rpf2bbkmlb6nsvd32y4zdl")))

(define-public crate-graphlib_rust-0.0.3 (c (n "graphlib_rust") (v "0.0.3") (d (list (d (n "ordered_hashmap") (r "^0.0.3") (d #t) (k 0)))) (h "0xfz8qdkxv64381f0sjjd017s51fss19c1bcvkyn0ic9hlf7z8rz")))

(define-public crate-graphlib_rust-0.0.4 (c (n "graphlib_rust") (v "0.0.4") (d (list (d (n "ordered_hashmap") (r "^0.0.4") (d #t) (k 0)))) (h "0xvm2r08pzpy4a8cjcya0v7r858xb8wjgk6mz42h4dg9m25wv7lm")))

