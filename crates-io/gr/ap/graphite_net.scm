(define-module (crates-io gr ap graphite_net) #:use-module (crates-io))

(define-public crate-graphite_net-0.1.0 (c (n "graphite_net") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "graphite_binary") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_mc_constants") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_mc_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "io-uring") (r "^0.5.2") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0snikc4nx5fg28z1cjkgi55xwzmb36fwd90j6qly3s6vkm0jp5cr")))

