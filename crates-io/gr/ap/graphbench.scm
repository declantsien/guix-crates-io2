(define-module (crates-io gr ap graphbench) #:use-module (crates-io))

(define-public crate-graphbench-0.1.3 (c (n "graphbench") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "union-find-rs") (r "^0.2.1") (d #t) (k 0)))) (h "00r4dyc2a22bmp1gay49bhlfb1gvx3clcqv827q77fww0c5ya7vx")))

(define-public crate-graphbench-0.1.4 (c (n "graphbench") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "union-find-rs") (r "^0.2.1") (d #t) (k 0)))) (h "0xizqa0r13gdanix8zvc0mwmvpkkarr1s055r95z9dn6iyzk30dx")))

(define-public crate-graphbench-0.1.5 (c (n "graphbench") (v "0.1.5") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "union-find-rs") (r "^0.2.1") (d #t) (k 0)))) (h "0hkhvinq33qjm3msjhmw67a1zw3q4rb6r5h4dalvf4jlby048zmg")))

(define-public crate-graphbench-0.1.6 (c (n "graphbench") (v "0.1.6") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "union-find-rs") (r "^0.2.1") (d #t) (k 0)))) (h "0jp6qi4psi28rm5zz3s0j9mg1ykj5z2avchy9mjywq3qqhs83kd9")))

(define-public crate-graphbench-0.1.7 (c (n "graphbench") (v "0.1.7") (d (list (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "union-find-rs") (r "^0.2.1") (d #t) (k 0)))) (h "11s3nj1jkcyc3ivj3kcvn3qxya7saym4r7s7nrmz38pxsswh3zhz")))

