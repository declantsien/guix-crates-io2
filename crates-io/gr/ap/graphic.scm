(define-module (crates-io gr ap graphic) #:use-module (crates-io))

(define-public crate-graphic-0.0.1-alpha.1 (c (n "graphic") (v "0.0.1-alpha.1") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4") (d #t) (k 2)))) (h "1q3hixsss5y1hsmw6prasxw86d94s2vsp2mdydhay9z94gxwv6aq") (y #t)))

(define-public crate-graphic-0.0.1-alpha.2 (c (n "graphic") (v "0.0.1-alpha.2") (h "0pjqdb78prjjcrazcr05vr9ahhzk4r3ngcgca8ll30y63yncvhsr") (y #t)))

