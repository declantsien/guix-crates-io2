(define-module (crates-io gr ap graph-theory) #:use-module (crates-io))

(define-public crate-graph-theory-0.0.0 (c (n "graph-theory") (v "0.0.0") (d (list (d (n "adjacency-matrix") (r "0.0.*") (d #t) (k 0)) (d (n "graph-types") (r "0.0.*") (d #t) (k 0)))) (h "1v687abmhwpd0y7kv14gvp8wm6agsmj2d4bqg8f8bdf63v6mlsbq") (f (quote (("wolfram" "graph-types/wolfram_wxf" "adjacency-matrix/wolfram") ("default"))))))

(define-public crate-graph-theory-0.0.1 (c (n "graph-theory") (v "0.0.1") (d (list (d (n "adjacency-list") (r "0.0.*") (d #t) (k 0)) (d (n "adjacency-matrix") (r "0.0.*") (d #t) (k 0)) (d (n "graph-types") (r "0.0.*") (d #t) (k 0)))) (h "1hf1h76gy3gmcmlkfrh35zpsb9cb1f384wpvqhklira1dwn7z06s") (f (quote (("wolfram" "graph-types/wolfram_wxf" "adjacency-matrix/wolfram") ("default"))))))

