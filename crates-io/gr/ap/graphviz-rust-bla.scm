(define-module (crates-io gr ap graphviz-rust-bla) #:use-module (crates-io))

(define-public crate-graphviz-rust-bla-0.2.0 (c (n "graphviz-rust-bla") (v "0.2.0") (d (list (d (n "dot-generator") (r "^0.2.0") (d #t) (k 0)) (d (n "dot-structures") (r "^0.1.0") (d #t) (k 0)) (d (n "into-attr") (r "^0.1.0") (d #t) (k 0)) (d (n "into-attr-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0439q2z9czg6r640pvjk6xnmwb2lzj3dj2sgm9qzw6hslw9qakly")))

