(define-module (crates-io gr ap graphrs) #:use-module (crates-io))

(define-public crate-graphrs-0.1.0 (c (n "graphrs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fhwlr4pkd4qlnc2fnwp574x0mq8mh5pv4sw945dnf5x9yh6qfrp") (y #t)))

(define-public crate-graphrs-0.2.0 (c (n "graphrs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06c822ham5v62g7z54rwazbpvk6yzrg0g4kimamdq1b7xszccz4a")))

(define-public crate-graphrs-0.3.0 (c (n "graphrs") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l7shmsf9kkqyjddzh4dvdbbmc9ayd8z86s573ky6ihk4sxn0k54")))

(define-public crate-graphrs-0.4.0 (c (n "graphrs") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q3lppg8z94v7xsysdjnsf14169b190z8s77r6p5vb5917g4lacm")))

(define-public crate-graphrs-0.4.1 (c (n "graphrs") (v "0.4.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16ykh4gch83fq3wdrdb0ybra9svi900qn31zfa21n99n9l5yb5gk")))

(define-public crate-graphrs-0.5.0 (c (n "graphrs") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bzghvb6gp75d9i1ap2sh3d2b7isw3z4nw72xdbl1sirs33iyfs6")))

(define-public crate-graphrs-0.6.0 (c (n "graphrs") (v "0.6.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04hhcqbc1pjz2h9mwyjb4cmqq7xmhbg7ydj5y9im8rzzvcas1356")))

(define-public crate-graphrs-0.7.0 (c (n "graphrs") (v "0.7.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h9lgfzmy4nnm6zd12adm82jjn4incvf9z0fpjyc7q0pxrk7ap5w")))

(define-public crate-graphrs-0.8.0 (c (n "graphrs") (v "0.8.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08ii0pf9n84nzsr953fypvq7shd9n7ayqg1dhpa2z8mq8hpvdyl2")))

(define-public crate-graphrs-0.9.0 (c (n "graphrs") (v "0.9.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "vec_to_array") (r "^0.2.1") (d #t) (k 0)))) (h "0chpvp3rhb4plsyn9978c0hrllgi3s73620c528n6rsywkhwxkzm")))

