(define-module (crates-io gr ap graphile_worker_ctx) #:use-module (crates-io))

(define-public crate-graphile_worker_ctx-0.1.0 (c (n "graphile_worker_ctx") (v "0.1.0") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "graphile_worker_job") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "0r303k31g6b4pa4z53rcf4nhrjvn5aw8q0k2azdvksax3ab2y8s0")))

(define-public crate-graphile_worker_ctx-0.1.1 (c (n "graphile_worker_ctx") (v "0.1.1") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "graphile_worker_job") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "0v0kd2x6azxsamzg3ggmalvm17p6byl2924nw98n860n7v41vvhi")))

(define-public crate-graphile_worker_ctx-0.1.2 (c (n "graphile_worker_ctx") (v "0.1.2") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "graphile_worker_job") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "056agnxc99vdby4g682gk3z28lyyv3xh2lfs51fp77w35kmy649m")))

(define-public crate-graphile_worker_ctx-0.2.0 (c (n "graphile_worker_ctx") (v "0.2.0") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "graphile_worker_extensions") (r "^0.1.2") (d #t) (k 0)) (d (n "graphile_worker_job") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "0224s0zacpyc5xra3b50m5qysll44dpc56i98lgszi9il8nyyfij")))

(define-public crate-graphile_worker_ctx-0.2.1 (c (n "graphile_worker_ctx") (v "0.2.1") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "graphile_worker_extensions") (r "^0.1.3") (d #t) (k 0)) (d (n "graphile_worker_job") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "0xv0nx3l404i5fglq7dyfxlvnc18k21rzp4i0cvbf2bknypncra6")))

