(define-module (crates-io gr ap graphql-idl-parser-ffi) #:use-module (crates-io))

(define-public crate-graphql-idl-parser-ffi-0.1.0 (c (n "graphql-idl-parser-ffi") (v "0.1.0") (d (list (d (n "graphql-idl-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sc0drwng6wpkay6mfhql251g11sj27wspmi2w62yhfj92zjg942")))

(define-public crate-graphql-idl-parser-ffi-0.1.1 (c (n "graphql-idl-parser-ffi") (v "0.1.1") (d (list (d (n "graphql-idl-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10g84ykyk56l2md840mzana5k9xw87a6l4khv70xqzbqmxzm1ns7")))

(define-public crate-graphql-idl-parser-ffi-0.1.2 (c (n "graphql-idl-parser-ffi") (v "0.1.2") (d (list (d (n "graphql-idl-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fi9yp4xfybgm3q1p7rrbfvq16y371a7i1r3a7dkphgbl5bm6hbr")))

(define-public crate-graphql-idl-parser-ffi-0.1.3 (c (n "graphql-idl-parser-ffi") (v "0.1.3") (d (list (d (n "graphql-idl-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00xwvvbm88ia01wf82rpl5ng3lf2cfh8jfjkkcryig80xgkh2hpw")))

