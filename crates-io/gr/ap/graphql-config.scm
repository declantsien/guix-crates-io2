(define-module (crates-io gr ap graphql-config) #:use-module (crates-io))

(define-public crate-graphql-config-0.1.0 (c (n "graphql-config") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0917pzr0570wfh1wxmh5mqw6qs2xmjl9cwav7wblnn0yylgcrh0j")))

(define-public crate-graphql-config-0.2.0 (c (n "graphql-config") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13a2kr0bnpc64168lwsnhrpd9jp7nj8gpkqk3wy4lr6zxbvvpx6a")))

