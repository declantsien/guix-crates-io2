(define-module (crates-io gr ap graphblas_sparse_linear_algebra_proc_macros) #:use-module (crates-io))

(define-public crate-graphblas_sparse_linear_algebra_proc_macros-0.1.0 (c (n "graphblas_sparse_linear_algebra_proc_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z0s9xyav12p6bv9a7wpg78ba7a11wwb88prh1v6my3345gl3j9i")))

(define-public crate-graphblas_sparse_linear_algebra_proc_macros-0.2.0 (c (n "graphblas_sparse_linear_algebra_proc_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0si24pm736xwjcnlphv6w7f0cspn33bwqjkmsw3vkdwlbx32x1f7")))

(define-public crate-graphblas_sparse_linear_algebra_proc_macros-0.3.0 (c (n "graphblas_sparse_linear_algebra_proc_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r64q8jpl3wm142z2qm8vn84l15mmrgf3s36pjc715ssh9k4q1sz")))

(define-public crate-graphblas_sparse_linear_algebra_proc_macros-0.3.1 (c (n "graphblas_sparse_linear_algebra_proc_macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g6gap4z6zpwn838dx8mwxc3n1q369g7xp5b12vf4760v0yjxian")))

