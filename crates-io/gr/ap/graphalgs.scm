(define-module (crates-io gr ap graphalgs) #:use-module (crates-io))

(define-public crate-graphalgs-0.0.1 (c (n "graphalgs") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.23.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "1kc63yryj01nfqfk92lx0cvnxs33bmyxqpm4s8djyqkw7sjz9bnc")))

(define-public crate-graphalgs-0.0.2 (c (n "graphalgs") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.23.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "099pngm0bffgf1sg0bf75sr1cncma7y2dbkaw041qjwi36w7fmqc")))

(define-public crate-graphalgs-0.0.3 (c (n "graphalgs") (v "0.0.3") (d (list (d (n "nalgebra") (r "^0.24.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "12ngm8mxshbf2a1gy6n7a7qapyqm1rpzhqpjzdssbmvp2c3nilar")))

(define-public crate-graphalgs-0.0.4 (c (n "graphalgs") (v "0.0.4") (d (list (d (n "nalgebra") (r "^0.24.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "18plygcchydg77bl64fgv6n9d8mm3azyk9aw0fqlsbycsz8zp7b4")))

(define-public crate-graphalgs-0.0.5 (c (n "graphalgs") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0b8qp1krvq162vncryxbghha5xwsj72crqbg8aswxahapyys5hzm")))

(define-public crate-graphalgs-0.0.6 (c (n "graphalgs") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1wq3q7l1aij0dvkpwavjwwsq6yrfr10jv2ymd09a2li4l17lwxgr")))

(define-public crate-graphalgs-0.1.0 (c (n "graphalgs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0iyhi156ylcycmdacqb1qzmb12fbfb3a2y8dpygj0lwsj8jnz4iy")))

(define-public crate-graphalgs-0.1.1 (c (n "graphalgs") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.25.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1fhzp14xhiv752n8ikqxb7rwgk05ybxdc7jm27yijxlfsv43iiwa")))

