(define-module (crates-io gr ap graph-neighbor-matching) #:use-module (crates-io))

(define-public crate-graph-neighbor-matching-0.1.0 (c (n "graph-neighbor-matching") (v "0.1.0") (d (list (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "11i1f0ym8hvpd8cf7363c3s22xs1ydp4k98ix3snd4xyam2a9raf")))

(define-public crate-graph-neighbor-matching-0.2.0 (c (n "graph-neighbor-matching") (v "0.2.0") (d (list (d (n "closed01") (r "^0.0.1") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "1y0m4f76172d0w2m3xgkzgcyilcc4w2ay8mbxy4bwaq02i8hwraf")))

(define-public crate-graph-neighbor-matching-0.2.1 (c (n "graph-neighbor-matching") (v "0.2.1") (d (list (d (n "closed01") (r "^0.0.1") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "1ikzsf0khg6nwadrnx6v2wazrvbdcvqard6vn4rzznglhfkvnab4")))

(define-public crate-graph-neighbor-matching-0.2.2 (c (n "graph-neighbor-matching") (v "0.2.2") (d (list (d (n "closed01") (r "^0.1.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "0v5jwl2g47452zmyk2s645winq6w9kxckbfxd1jppn8as72cxn1f")))

(define-public crate-graph-neighbor-matching-0.3.0 (c (n "graph-neighbor-matching") (v "0.3.0") (d (list (d (n "closed01") (r "^0.1.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "1a144fnac2ya4nyl6139b32kch8r6bqx78qvb70hy3wpk334m9kr")))

(define-public crate-graph-neighbor-matching-0.3.1 (c (n "graph-neighbor-matching") (v "0.3.1") (d (list (d (n "closed01") (r "^0.1.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "097laacfnr846s8lkfmisjf8m20y8wgvj5x5bl5z8g1g953m240h")))

(define-public crate-graph-neighbor-matching-0.3.2 (c (n "graph-neighbor-matching") (v "0.3.2") (d (list (d (n "closed01") (r "^0.3.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "1y6h6fjhrinml0mpvcjrp1ikql8mi7bcki53x4vshh6ry67k3yjs")))

(define-public crate-graph-neighbor-matching-0.3.3 (c (n "graph-neighbor-matching") (v "0.3.3") (d (list (d (n "closed01") (r "^0.3.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "08dwf604b17hkhqpi2bz7gv612va72w6h2wnn3w0banrh4crg484")))

(define-public crate-graph-neighbor-matching-0.4.0 (c (n "graph-neighbor-matching") (v "0.4.0") (d (list (d (n "closed01") (r "^0.3.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "1x9cys4675k3xh44rr5i0nalwnxmizk5yqqigqxjgl37z8fzs6h0")))

(define-public crate-graph-neighbor-matching-0.5.0 (c (n "graph-neighbor-matching") (v "0.5.0") (d (list (d (n "closed01") (r "^0.3.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "1kxc1r7w5xm53ajrr16pjxydy6r2q7ijjsinssi5a8nk3qw03i72")))

(define-public crate-graph-neighbor-matching-0.6.0 (c (n "graph-neighbor-matching") (v "0.6.0") (d (list (d (n "closed01") (r "^0.4.0") (d #t) (k 0)) (d (n "munkres") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "0pd4whfgziz98p8i38fys2bwrgljrb6vj6ydpiw9rs35q5zk2c2s")))

(define-public crate-graph-neighbor-matching-0.6.1 (c (n "graph-neighbor-matching") (v "0.6.1") (d (list (d (n "asexp") (r "^0.3") (d #t) (k 2)) (d (n "closed01") (r "^0.4.0") (d #t) (k 0)) (d (n "graph-io-gml") (r "^0.1") (d #t) (k 2)) (d (n "munkres") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.3") (d #t) (k 0)))) (h "04jd8sbji4iv8ifh3m04afsissjdcgb40hmg2w3j3gr1r2wfq5h6")))

(define-public crate-graph-neighbor-matching-0.6.2 (c (n "graph-neighbor-matching") (v "0.6.2") (d (list (d (n "asexp") (r "^0.3") (d #t) (k 2)) (d (n "closed01") (r "^0.4.0") (d #t) (k 0)) (d (n "graph-io-gml") (r "^0.1") (d #t) (k 2)) (d (n "munkres") (r "^0.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.3") (d #t) (k 0)))) (h "1vxm9yjq0761f21nzfx8ng0s8yic4ivf4nw602rxpfddckf42vc2")))

(define-public crate-graph-neighbor-matching-0.7.0 (c (n "graph-neighbor-matching") (v "0.7.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "asexp") (r "^0.3") (d #t) (k 2)) (d (n "closed01") (r "^0.4") (d #t) (k 0)) (d (n "graph-io-gml") (r "^0.2") (d #t) (k 2)) (d (n "munkres") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1sm74qng7xy2mg8pxma81qnqdai2dayl6znl2zm37adjasqhgsla")))

(define-public crate-graph-neighbor-matching-0.7.1 (c (n "graph-neighbor-matching") (v "0.7.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 0)) (d (n "asexp") (r "^0.3") (d #t) (k 2)) (d (n "closed01") (r "^0.4") (d #t) (k 0)) (d (n "graph-io-gml") (r "^0.2") (d #t) (k 2)) (d (n "munkres") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1aa2jf8qgz4rlfyfgqqw8acxi489b9afn93qmh4rynq94l1ca76i")))

(define-public crate-graph-neighbor-matching-0.8.0 (c (n "graph-neighbor-matching") (v "0.8.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "asexp") (r "^0.3") (d #t) (k 2)) (d (n "closed01") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "graph-io-gml") (r "^0.2") (d #t) (k 2)) (d (n "munkres") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1pavwy9ws5dwhr8jcv72s4vs8xgd7nk1q336hp5hjnfgfm7qq6hs")))

(define-public crate-graph-neighbor-matching-0.9.0 (c (n "graph-neighbor-matching") (v "0.9.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "asexp") (r "^0.3") (d #t) (k 2)) (d (n "closed01") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "graph-io-gml") (r "^0.3") (d #t) (k 2)) (d (n "munkres") (r "^0.5") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "0ndv7csjclciks554lq5k48pwgk4fdvw7j51dijc44nw7v6wmljm")))

