(define-module (crates-io gr ap graphite_command_macros) #:use-module (crates-io))

(define-public crate-graphite_command_macros-0.1.0 (c (n "graphite_command_macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0y6qhwli4y6c6yg7b11qwyljvskvdq96pqyzf8mx08vsid7wxsz7")))

