(define-module (crates-io gr ap graph_executor) #:use-module (crates-io))

(define-public crate-graph_executor-0.0.1 (c (n "graph_executor") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "dashmap") (r "^5.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1wvwjvb78irgmjbb069zb3q257vh9skhkf37q8r5897izgyp3bm3")))

