(define-module (crates-io gr ap graphile_worker_job) #:use-module (crates-io))

(define-public crate-graphile_worker_job-0.1.0 (c (n "graphile_worker_job") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "0xhyqpvh8ddv925w2va22rjrd4f7xr1kbxalgbr7pjsis2981g45")))

(define-public crate-graphile_worker_job-0.1.1 (c (n "graphile_worker_job") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "19gyczyhkgzs2hrh7np54saagqsnycwmga6cqfg4g5f09f3b7cw2")))

(define-public crate-graphile_worker_job-0.1.2 (c (n "graphile_worker_job") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("chrono" "postgres" "json" "macros" "runtime-tokio"))) (k 0)))) (h "1r5b19anhcl1bi0nxv1bpabqd25khf391siw65gfg2878ad5an7w")))

