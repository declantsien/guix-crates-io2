(define-module (crates-io gr ap grapple-lasercan) #:use-module (crates-io))

(define-public crate-grapple-lasercan-2024.2.0 (c (n "grapple-lasercan") (v "2024.2.0") (d (list (d (n "grapple-config") (r "^0.2.0") (d #t) (k 0)) (d (n "grapple-frc-msgs") (r "^2024.2.2") (f (quote ("lasercan_nop_patch" "grapple_lasercan"))) (k 0)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "1ahwflhhrkn7zqmxhyv1ic9d48szmdqmi7a82p3gm8zkhrp9fydg")))

(define-public crate-grapple-lasercan-2024.2.1 (c (n "grapple-lasercan") (v "2024.2.1") (d (list (d (n "grapple-config") (r "^0.2.0") (d #t) (k 0)) (d (n "grapple-frc-msgs") (r "^2024.2.2") (f (quote ("lasercan_nop_patch" "grapple_lasercan"))) (k 0)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "1h26j7vw23h0m6460lxv0rz67h0vc9pcb4cvj59flmpxy62nvvi9")))

