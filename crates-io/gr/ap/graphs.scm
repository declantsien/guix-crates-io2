(define-module (crates-io gr ap graphs) #:use-module (crates-io))

(define-public crate-graphs-0.1.0 (c (n "graphs") (v "0.1.0") (d (list (d (n "graph-traits") (r "^0.1.0") (d #t) (k 0)))) (h "0qgi8sczilgfa124f4cp5vyx44g3dgp7hg2cspw9ayjg2ndxy1mc")))

(define-public crate-graphs-0.1.1 (c (n "graphs") (v "0.1.1") (d (list (d (n "graph-traits") (r "^0.1.0") (d #t) (k 0)))) (h "012r6vw17rvgrg8lqrmg9ggxbrxxqsmz1vxd1qz41vvs3jmvfia6")))

