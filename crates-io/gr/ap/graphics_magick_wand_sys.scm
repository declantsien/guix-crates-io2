(define-module (crates-io gr ap graphics_magick_wand_sys) #:use-module (crates-io))

(define-public crate-graphics_magick_wand_sys-0.1.0 (c (n "graphics_magick_wand_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11inph8hqk71vgj63wdlpqn0xgaj03ak19hkyfhjg5mdhzh1i3xh")))

