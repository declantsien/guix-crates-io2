(define-module (crates-io gr ap graphrox) #:use-module (crates-io))

(define-public crate-graphrox-1.0.0 (c (n "graphrox") (v "1.0.0") (h "1bshil829zijlhnbyd4ajyx6c2jvbhf4knx4x7jjqddw658m02gl")))

(define-public crate-graphrox-1.0.1 (c (n "graphrox") (v "1.0.1") (h "17qyzc92knj35n2gmr4wy4r0jfh2pj1i9mhn1v6v68sgnykixcjk")))

(define-public crate-graphrox-1.0.2 (c (n "graphrox") (v "1.0.2") (h "0d42vd51k56flb0hx71284qa8yn390ngij1p1ml2gb1grzfbaqqa")))

(define-public crate-graphrox-1.0.4 (c (n "graphrox") (v "1.0.4") (h "0vlkkxah6sqmk81sixlrbbaqd53sk084sr5qw3d9hvk9b0xzdsfi")))

(define-public crate-graphrox-1.1.0 (c (n "graphrox") (v "1.1.0") (h "0sk3qsikfb489p34csf8akpa98mv73hd979jd9asl12qsiwgz8gk")))

(define-public crate-graphrox-1.2.0 (c (n "graphrox") (v "1.2.0") (h "1dflib71g9sw7gd6cysl5v1n15y2yfc5wa9a2w6pbwvm58pfkmrv")))

