(define-module (crates-io gr ap graph-similarity-cmd) #:use-module (crates-io))

(define-public crate-graph-similarity-cmd-0.0.1 (c (n "graph-similarity-cmd") (v "0.0.1") (d (list (d (n "asexp") (r "^0.3") (d #t) (k 0)) (d (n "closed01") (r "^0.4") (d #t) (k 0)) (d (n "graph-io-gml") (r "^0.1") (d #t) (k 0)) (d (n "graph-neighbor-matching") (r "^0.6.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.2") (d #t) (k 0)))) (h "112hj170wr2ccivgbjagacrbirsibmgy97zx2mgyd63ablc59356")))

