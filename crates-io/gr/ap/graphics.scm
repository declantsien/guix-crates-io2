(define-module (crates-io gr ap graphics) #:use-module (crates-io))

(define-public crate-graphics-0.0.0 (c (n "graphics") (v "0.0.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "wolfram_wxf") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1mmfxxb6kcwjgdvppxjfz9qwrcq97abfsiwj3dj9qyz7j4fbdm1v") (f (quote (("f64") ("f32") ("default" "f32"))))))

