(define-module (crates-io gr ap graphy_plugin) #:use-module (crates-io))

(define-public crate-graphy_plugin-0.2.0 (c (n "graphy_plugin") (v "0.2.0") (d (list (d (n "graphy_plugin_test_0") (r "^0.1.0") (d #t) (k 2)) (d (n "libloading") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.2") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "syntex") (r "^0.31.0") (d #t) (k 1)))) (h "013b4k2mnb1sizswslbvyh6m61dwrmpjr3c6dw8dz3ljzvhwxar9")))

