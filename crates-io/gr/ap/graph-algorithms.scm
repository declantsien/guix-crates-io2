(define-module (crates-io gr ap graph-algorithms) #:use-module (crates-io))

(define-public crate-graph-algorithms-0.1.0 (c (n "graph-algorithms") (v "0.1.0") (h "1sy8m0xdy66ikwxbq1hym5hybb5mbyrq6hn18k60fq341ik3xsq5")))

(define-public crate-graph-algorithms-0.1.1 (c (n "graph-algorithms") (v "0.1.1") (h "0v3g442j09andfdp3wv8c7rk9xax54mi1jd8aqncfx6jizz5hibx")))

(define-public crate-graph-algorithms-0.1.2 (c (n "graph-algorithms") (v "0.1.2") (h "0sg49pf7xmim647q48c7i05vppxaaggv1qnngi2mb2mkwb6rhdiz")))

(define-public crate-graph-algorithms-0.1.3 (c (n "graph-algorithms") (v "0.1.3") (h "0sj3dcbh6q41gi0prb3ab4zi1m4j6qcs98rdj4w95p1glkvb3c8x")))

