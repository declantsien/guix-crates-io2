(define-module (crates-io gr ap grapheme_to_phoneme) #:use-module (crates-io))

(define-public crate-grapheme_to_phoneme-0.0.1 (c (n "grapheme_to_phoneme") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.5.0") (d #t) (k 0)))) (h "0dmgw8gv6z3hiy2jdyyf7kb7hsdsgxf3jsv6gjlx16fqf7lpcl7w")))

(define-public crate-grapheme_to_phoneme-0.0.2 (c (n "grapheme_to_phoneme") (v "0.0.2") (d (list (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.5.0") (d #t) (k 0)))) (h "1j504ia043fgyhk9d44iv759ijw0f9nwpqpfv5pksii9cf3alzmz")))

(define-public crate-grapheme_to_phoneme-0.0.3 (c (n "grapheme_to_phoneme") (v "0.0.3") (d (list (d (n "arpabet") (r "^1.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.5.0") (d #t) (k 0)))) (h "1lw39nylawh1zzj5alvh6mrprmjkbhzzd21gkyrf0v5bmjiiaafz")))

(define-public crate-grapheme_to_phoneme-0.1.0 (c (n "grapheme_to_phoneme") (v "0.1.0") (d (list (d (n "arpabet") (r "^2.0.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.5.0") (d #t) (k 0)))) (h "17q1vlh089f9kznqycdixylh2z01isxcmql3axcdb7y93q1k7bx1")))

