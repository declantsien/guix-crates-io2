(define-module (crates-io gr ap graph6-rs) #:use-module (crates-io))

(define-public crate-graph6-rs-0.1.0 (c (n "graph6-rs") (v "0.1.0") (h "05qg0v2pr2yz8mvrjn70d12k9fp1zg3n02d6pdd3x3fzl1irlv3v")))

(define-public crate-graph6-rs-0.1.1 (c (n "graph6-rs") (v "0.1.1") (h "1k6z0psli20a07xb5fwzn50x2ayndyp1g8s04yj6jzb1gb26yj6m")))

(define-public crate-graph6-rs-0.1.2 (c (n "graph6-rs") (v "0.1.2") (h "19ln0kaz3wmz2sq9q7wqr0v8nccldipxnlld71b1gld2rbmssbq7")))

(define-public crate-graph6-rs-0.1.3 (c (n "graph6-rs") (v "0.1.3") (h "10ldi8csx24b7c5yiz038k3504r0xs7x39bzf2q2hnl605bxhzz9")))

(define-public crate-graph6-rs-0.1.4 (c (n "graph6-rs") (v "0.1.4") (h "0aapy2l511z8g66bh8bzlwz89rmn83ac1313pp67vq54gf4s24s3")))

(define-public crate-graph6-rs-0.1.5 (c (n "graph6-rs") (v "0.1.5") (h "0mzx5vsflv01364pgmaz4pf53ba206d3d567vsjpw1qbzprk7b4i")))

(define-public crate-graph6-rs-0.1.6 (c (n "graph6-rs") (v "0.1.6") (h "09aib3ci97k7npnsb3vzb6iclzhqqmyqcbzp3n4zybpbr3jgzm15")))

(define-public crate-graph6-rs-0.1.7 (c (n "graph6-rs") (v "0.1.7") (h "1xpxnbc081h8mqyxfwjjssql9g22hkz4vc4kdka1q8vs8k14y4fi")))

(define-public crate-graph6-rs-0.1.8 (c (n "graph6-rs") (v "0.1.8") (h "0zbfajcczv46qr0wfmgsjdbrf3rf35bmylhf2rbg6894rvj5cv87")))

