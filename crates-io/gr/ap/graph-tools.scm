(define-module (crates-io gr ap graph-tools) #:use-module (crates-io))

(define-public crate-graph-tools-0.1.0 (c (n "graph-tools") (v "0.1.0") (h "15dzvr78dyijmz2divrihxkqzxn5hlrqi2p41r48izzww6l2l705")))

(define-public crate-graph-tools-0.1.1 (c (n "graph-tools") (v "0.1.1") (h "02a9cb56fsm1pl010mkrqmjs1gd24xnpr40iqal7ijgh87gj86fs")))

(define-public crate-graph-tools-0.1.2 (c (n "graph-tools") (v "0.1.2") (h "1a9sd1mk3b5aw04wp7m0jg0hrcwi0b5qvsyda1q64s989vlmm7zl")))

