(define-module (crates-io gr ap graph_match) #:use-module (crates-io))

(define-public crate-graph_match-0.1.0 (c (n "graph_match") (v "0.1.0") (h "1vf64cc36nw8x1p9b0mmd6jfjkasajz2ws370i9z68d9v10ip75z")))

(define-public crate-graph_match-0.2.0 (c (n "graph_match") (v "0.2.0") (h "084j0if6aq34ryg2faiw9q2qpn577dv2v0frv63l8gc13l5jpksr")))

(define-public crate-graph_match-0.2.1 (c (n "graph_match") (v "0.2.1") (h "0aq6aksvzach1f4rfjndlbl38nywc0aayndmk4y1rb00dh66cm09")))

(define-public crate-graph_match-0.2.2 (c (n "graph_match") (v "0.2.2") (h "0fl3l740b0mamgncsv5p475v6xkzddaz5952dyzbnkh6lwdhn914")))

(define-public crate-graph_match-0.2.3 (c (n "graph_match") (v "0.2.3") (h "0wp802bgbdn4c058d51inqlgyjwvvf7l2fvd8bsnwdb7v9cf66ji")))

(define-public crate-graph_match-0.3.0 (c (n "graph_match") (v "0.3.0") (h "0vzww8kdx5hnbiz2ird62rg5b8mgbdnxvw6a07fsc693b80jxdzp")))

(define-public crate-graph_match-0.4.0 (c (n "graph_match") (v "0.4.0") (h "0v8sisfj0zqa6hpw63m3gqnr15iyh7bd2glc1pyrjpxpqlf7wvc6")))

(define-public crate-graph_match-0.5.0 (c (n "graph_match") (v "0.5.0") (h "0gsjkpnzc18cn04f3z7qpflmxkfmcmszfrf578lnad65a2bm8577")))

