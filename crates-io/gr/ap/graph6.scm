(define-module (crates-io gr ap graph6) #:use-module (crates-io))

(define-public crate-graph6-0.1.0 (c (n "graph6") (v "0.1.0") (h "12b392iw3h0q7cnrcjxqddqpgx4qy6xmy6frsdi1185b6q6l59kj")))

(define-public crate-graph6-1.1.0 (c (n "graph6") (v "1.1.0") (h "1xqqrjn7wwqiidyqxr267in7jcgfbm5a5v79kzydyy3pd3rcjzpx")))

(define-public crate-graph6-1.1.1 (c (n "graph6") (v "1.1.1") (h "0ljpi46hnr2fcs86zzl6f2sk5h6vv5v0f6yhsigwjvr7j7h25fpf")))

(define-public crate-graph6-2.0.0 (c (n "graph6") (v "2.0.0") (h "09crrzipxvjb7xhzz376pcs35z2imsiz4sckz2ikkfzf95rlsscr")))

(define-public crate-graph6-2.0.1 (c (n "graph6") (v "2.0.1") (h "0vv1i6djnb82dbmx8ip6fzxijdjaialmhja43g0nibjnbmx7ixqr")))

