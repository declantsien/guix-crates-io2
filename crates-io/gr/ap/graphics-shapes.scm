(define-module (crates-io gr ap graphics-shapes) #:use-module (crates-io))

(define-public crate-graphics-shapes-0.1.0 (c (n "graphics-shapes") (v "0.1.0") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "131z9ijc2ykbkbdjqhi488c6ga7n9wi3gl66daqwdij0aa00f12f") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.1 (c (n "graphics-shapes") (v "0.1.1") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yfxal1ln138zzd5flj14cw96lk8a4xi7qgp7gd0dr5nc3vzyrb6") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.2 (c (n "graphics-shapes") (v "0.1.2") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j03gps8g3mwd5nxgd78gx960y8wc4gj5flhsrg6q49ks2hjqkmw") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.3 (c (n "graphics-shapes") (v "0.1.3") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qc432kn5haqq22n3lnp0g084g2yj02gzhsy4qg1rcxzhlrbqpfl") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.4 (c (n "graphics-shapes") (v "0.1.4") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sdzk9ijhsm2gmf9jbhd8k0yr9qqbs03hd6nq7ird7dj7asq0944") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.5 (c (n "graphics-shapes") (v "0.1.5") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00chq85z21gc061pbjg2s3fvfciij4k4cyrppk66mc7wvb5ixpss") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.6 (c (n "graphics-shapes") (v "0.1.6") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "161d81lw4xdikwjvqzwcxs14fi1rlac85d7hbz6r0qfb1m0a7vsk") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.7 (c (n "graphics-shapes") (v "0.1.7") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0606p45w004kmk2jqnmic0rps4hln2mlx24kmzjss23l0j9i9hpn") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.8 (c (n "graphics-shapes") (v "0.1.8") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03hr71sizvcykjd2ybwaf2zca7syqxkschbdh901h0g5phjlqsi5") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.9 (c (n "graphics-shapes") (v "0.1.9") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kvkgz7jn92s0znlw99gknqihxawbayl8vihsgggh9lx346ccmdf") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.10 (c (n "graphics-shapes") (v "0.1.10") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1csma1ibsrm6nmk0wyl5sdhkl1v0gy53cdv5nlanp5fv0vig5jks") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.11 (c (n "graphics-shapes") (v "0.1.11") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hvcak4wafa7w7ppr1wsf129j2k7cad8hn1w9qz9090i4j2hf494") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.12 (c (n "graphics-shapes") (v "0.1.12") (d (list (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jkg7a25kyg4wn4ak4587qd6v1na4x6s2hlazjnjvxl4il01xdns") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.1.14 (c (n "graphics-shapes") (v "0.1.14") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p76cfy3abnnv9p4hx6p8jiz1rbq25ql7ilvdibx9g1dc81ipiaj") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.0 (c (n "graphics-shapes") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "123mhqcf35l96im7wb1pd8bfn71awvva0mr5l5bq939ad7p5gipy") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.1 (c (n "graphics-shapes") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m152skyx9m3qxvnzpfjhk55czpz0v0r4q31r9g9p29jp2vn9lc6") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.2 (c (n "graphics-shapes") (v "0.2.2") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19pylpc5cr6yxa2lc3kb58sa5yrwdvrwrc2yk0ddsbkx810q2czc") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.3 (c (n "graphics-shapes") (v "0.2.3") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17mcqap95rrh94i55bmjiw9v78255n8p08l1xa5rxycj7lhaqj91") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.4 (c (n "graphics-shapes") (v "0.2.4") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rhqib6r61nbgcwjjjvsmdqh6dg9l6d2xn03v5fk8r430kmgmad9") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.5 (c (n "graphics-shapes") (v "0.2.5") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lg7kdkka5pqnw1cq9ygdprfzhvzf53f3i6zxar8zqdiswa5x1hb") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.6 (c (n "graphics-shapes") (v "0.2.6") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n0fbvly5drg02rzj69n32hx0pd4qd7hn0q5dwjmx6c84rj7p70z") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.2.7 (c (n "graphics-shapes") (v "0.2.7") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12zf0msw42qm0zmbwz3kfn6gf1irc3c661ycg8813pm72a599wfl") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.3.0 (c (n "graphics-shapes") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1brf8jpa2ai587ywzsh9iz5533327r7jj35y2qsay5a7l0rd8glk") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.4.0 (c (n "graphics-shapes") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "00d8grwgw933cg0800a1sxq7ivhky6891gvldhw5djbs97v7jvzl") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

(define-public crate-graphics-shapes-0.4.1 (c (n "graphics-shapes") (v "0.4.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "051c3yx6in7ka13bspj8bg790jiw699jbf0bqxv0366lmnmc9d49") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint"))))))

