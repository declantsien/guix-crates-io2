(define-module (crates-io gr ap graphql_depth_limit) #:use-module (crates-io))

(define-public crate-graphql_depth_limit-0.1.0 (c (n "graphql_depth_limit") (v "0.1.0") (d (list (d (n "graphql-parser") (r "^0.2.3") (d #t) (k 0)))) (h "1qb4pm8265fk25cr1bgbi4yxfxajhd5rn9zkby6bx5f1qy3a3wqf")))

(define-public crate-graphql_depth_limit-0.1.1 (c (n "graphql_depth_limit") (v "0.1.1") (d (list (d (n "graphql-parser") (r "^0.2.3") (d #t) (k 0)))) (h "0y6a82vy5q43539ad4439wzjkq73d6bskpfgx84ffdrdrb0knga1")))

(define-public crate-graphql_depth_limit-0.1.2 (c (n "graphql_depth_limit") (v "0.1.2") (d (list (d (n "graphql-parser") (r "^0.2.3") (d #t) (k 0)))) (h "1v76xld1f56yn6dcb61yl945x385x8g62ngawbj07wv3mj30ifsi") (y #t)))

