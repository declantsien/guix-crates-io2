(define-module (crates-io gr ap graph-io-gml) #:use-module (crates-io))

(define-public crate-graph-io-gml-0.0.2 (c (n "graph-io-gml") (v "0.0.2") (d (list (d (n "asexp") (r "^0.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "1q1rv9sgpzb91ci9fdi5al35gz5lvwfhmsvd35x1cn6yjbjrhf74")))

(define-public crate-graph-io-gml-0.1.0 (c (n "graph-io-gml") (v "0.1.0") (d (list (d (n "asexp") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "05zz52zv28qdfyh9pv5afhdyikmh953in02riw954gjrkhpyyqrb")))

(define-public crate-graph-io-gml-0.1.1 (c (n "graph-io-gml") (v "0.1.1") (d (list (d (n "asexp") (r "^0.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.2.2") (d #t) (k 0)))) (h "11qmvwnnyzlnvidk2xnld8dyamnbr8krxrhq12183x8mhzxcd58r")))

(define-public crate-graph-io-gml-0.2.0 (c (n "graph-io-gml") (v "0.2.0") (d (list (d (n "asexp") (r "^0.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "0zadrfm4nif4wi5vzg94dz3wvvf7qrr5m86n89h0dzvg1ld7dq87")))

(define-public crate-graph-io-gml-0.3.0 (c (n "graph-io-gml") (v "0.3.0") (d (list (d (n "asexp") (r "^0.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "15dy4qvbbjb7i7z59gfn1ybmplazs1sipavcmpn0fhj60815vna6")))

