(define-module (crates-io gr ap graphql-id) #:use-module (crates-io))

(define-public crate-graphql-id-0.1.0 (c (n "graphql-id") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "10gy35gy84py1fxhvvrad2wain47m9xfn1hrqc1qljna3barmmn6")))

(define-public crate-graphql-id-0.1.1 (c (n "graphql-id") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0ldk9pfhqj48nb31r6vgczjfycvxliywmfkfa1pkk07lb5v8j9lh")))

(define-public crate-graphql-id-0.1.2 (c (n "graphql-id") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "graphql-parser") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1cilr3mdm8m299fwz39dvc6d47qp8f4lwcqk5f5qh2xbw86x0dax")))

