(define-module (crates-io gr ap graph_solver) #:use-module (crates-io))

(define-public crate-graph_solver-0.1.0 (c (n "graph_solver") (v "0.1.0") (d (list (d (n "quickbacktrack") (r "^0.5.0") (d #t) (k 0)))) (h "1cp3708hch4lm9gzlas6hkc42fwqhy0kxh0xnyik55zhnxddk0wc")))

(define-public crate-graph_solver-0.1.1 (c (n "graph_solver") (v "0.1.1") (d (list (d (n "quickbacktrack") (r "^0.5.0") (d #t) (k 0)))) (h "1rgf0ydpa0irv71hzpfzcqdzng50p21af8m2hfwvl9hk1dkxqgy6")))

(define-public crate-graph_solver-0.2.0 (c (n "graph_solver") (v "0.2.0") (d (list (d (n "quickbacktrack") (r "^0.6.0") (d #t) (k 0)))) (h "1dc5ylizm4ii2nzs4ml0vs6b010pk51rzq5dq31l568lbcpaf154")))

(define-public crate-graph_solver-0.3.0 (c (n "graph_solver") (v "0.3.0") (d (list (d (n "quickbacktrack") (r "^0.6.0") (d #t) (k 0)))) (h "0b81vi3r1x2awiamrwlp38d1cmxfgzf7x9cwajdyl6kgk57sf336")))

(define-public crate-graph_solver-0.4.0 (c (n "graph_solver") (v "0.4.0") (d (list (d (n "quickbacktrack") (r "^0.6.0") (d #t) (k 0)))) (h "1axrn0s62s0pmskwhkfrz07m22j83i04v994x15nd7xdpj32pnvh")))

