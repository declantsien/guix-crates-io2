(define-module (crates-io gr ap graphviz-rs) #:use-module (crates-io))

(define-public crate-graphviz-rs-0.1.0 (c (n "graphviz-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "09h8yc11d8wz2fblp85v24xbfcgsahjp8s2cmc94xcwjjiki0s3a")))

(define-public crate-graphviz-rs-0.2.0 (c (n "graphviz-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0041qf85w9kb0473fcbl3n7zjzzkp3kwrl3fr9v0g573dn50agx0")))

