(define-module (crates-io gr ap graph_event) #:use-module (crates-io))

(define-public crate-graph_event-1.0.0 (c (n "graph_event") (v "1.0.0") (d (list (d (n "armc") (r "^1.3.2") (d #t) (k 0)))) (h "1lwnl6mlh526vnlrigdizj98ir6fqcriwhbmkh6kcgwy9j52x5rq")))

(define-public crate-graph_event-1.0.1 (c (n "graph_event") (v "1.0.1") (d (list (d (n "armc") (r "^1.3.2") (d #t) (k 0)))) (h "14pxriv771hrpbdjbx58ism7j76qlc9ld10b7i6vsm36qg629jf5")))

(define-public crate-graph_event-1.1.1 (c (n "graph_event") (v "1.1.1") (d (list (d (n "armc") (r "^1.3.2") (d #t) (k 0)))) (h "1k4gvld32wsnlxnjqxy7vc6h8ar2fyl0i1sdx4rbqjw34dsnpv8b")))

