(define-module (crates-io gr ap graphite_minecraft) #:use-module (crates-io))

(define-public crate-graphite_minecraft-0.1.0 (c (n "graphite_minecraft") (v "0.1.0") (d (list (d (n "graphite_command") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_concierge") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_magma") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_mc_constants") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_mc_protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_net") (r "^0.1.0") (d #t) (k 0)) (d (n "graphite_server") (r "^0.1.0") (d #t) (k 0)))) (h "1m820q4ikly3a2g5ff1cisfcndnmjpa2v3ds1c9k4405asj4g7a3")))

