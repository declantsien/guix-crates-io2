(define-module (crates-io gr ap graph_traversal) #:use-module (crates-io))

(define-public crate-graph_traversal-0.0.0 (c (n "graph_traversal") (v "0.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bal6vvwjy52jkfms8pgj7nqxdacllklw1f6910lhv65qqsaxiva")))

