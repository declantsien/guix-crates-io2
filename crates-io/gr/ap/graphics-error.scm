(define-module (crates-io gr ap graphics-error) #:use-module (crates-io))

(define-public crate-graphics-error-0.0.0 (c (n "graphics-error") (v "0.0.0") (h "1gswx42a04ci3kn73nllqdabfpbz1nzblz8sfy6xc622vqfrgi4w") (f (quote (("default"))))))

(define-public crate-graphics-error-0.1.0 (c (n "graphics-error") (v "0.1.0") (h "158qb4cwwi1ziirm26pbmg8kign5fmd1126gb76lbd7vb388a2dx") (f (quote (("default"))))))

