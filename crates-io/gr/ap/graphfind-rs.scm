(define-module (crates-io gr ap graphfind-rs) #:use-module (crates-io))

(define-public crate-graphfind-rs-0.1.0 (c (n "graphfind-rs") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "graphviz-rust") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test_dir") (r "^0.2") (d #t) (k 2)))) (h "0ha5zchm06n3v2bg9w6pfvm2z16gwvispp4xnwi5b8pxiipm6l4x") (s 2) (e (quote (("svg" "dep:graphviz-rust"))))))

