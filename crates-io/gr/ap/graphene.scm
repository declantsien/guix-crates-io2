(define-module (crates-io gr ap graphene) #:use-module (crates-io))

(define-public crate-graphene-0.1.0 (c (n "graphene") (v "0.1.0") (h "10xf3982pf17ay4qksfirm0jndn4zpni3pa873a8vv35vm2rw9vs")))

(define-public crate-graphene-0.1.1 (c (n "graphene") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1989f49fhd5x0501xwyr5jcgb6lfyk06fvdxq7skkmlmzlzikdh0")))

(define-public crate-graphene-0.1.2 (c (n "graphene") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "1x59nd3giwpc0sjrvd5i4k1ycfv4naaiclksbg73mh6ggk56h1r0") (y #t)))

(define-public crate-graphene-0.1.3 (c (n "graphene") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0m668z7hdhi2bv37byd0ldxpr062il9fw0xmfqx6vhhixjbbzwb9")))

(define-public crate-graphene-0.1.4 (c (n "graphene") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "16l17xk58b9mkppzrrg8vh6nw188i5lp8v3cyq9d9l522vqjac88")))

(define-public crate-graphene-0.1.5 (c (n "graphene") (v "0.1.5") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1rq54zsyb7my43vnqk1cksh6ngiqqspdbhmqi17b82hwviqbsqd7")))

