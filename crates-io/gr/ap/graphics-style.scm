(define-module (crates-io gr ap graphics-style) #:use-module (crates-io))

(define-public crate-graphics-style-0.0.0 (c (n "graphics-style") (v "0.0.0") (h "1ghlvpdp0r8h1m3528ckfnlicrcbnj4cn3722gg4rk4xw1c52wl6") (f (quote (("default"))))))

(define-public crate-graphics-style-0.0.1 (c (n "graphics-style") (v "0.0.1") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "09a2hdggw8f3jfdfznznq3snbl6yqjlpld7hfpmqhpbi6qiz4a2b") (f (quote (("default"))))))

(define-public crate-graphics-style-0.0.2 (c (n "graphics-style") (v "0.0.2") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "17rdyadr86529hir4fv7shcc9kkh2axrv08wzzk75h8p3hzk0b4k") (f (quote (("default"))))))

(define-public crate-graphics-style-0.1.0 (c (n "graphics-style") (v "0.1.0") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "029rwl3hk7jl6sjra9i9ifnzvbmj9qn26xqxprlfqs8snsq2qbsy") (f (quote (("default"))))))

(define-public crate-graphics-style-0.1.1 (c (n "graphics-style") (v "0.1.1") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "0kj5gxv0i2m4n9ybsk5l0a1rh018nih84686nxlw3006mqaagrmy") (f (quote (("default"))))))

(define-public crate-graphics-style-0.1.2 (c (n "graphics-style") (v "0.1.2") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "0xwz5m3rjpcp1n70045v6dnfisf53gk17293r9f52s0ddzj2a248") (f (quote (("default"))))))

(define-public crate-graphics-style-0.1.3 (c (n "graphics-style") (v "0.1.3") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "0ckf18zq7s3jbnvqy2d7k87yf2m0k6qpd8ck114vxlr28sbmblbj") (f (quote (("default"))))))

(define-public crate-graphics-style-0.1.4 (c (n "graphics-style") (v "0.1.4") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "1d5ksklby8cswxwkjjp1djjx0fk5lcrn5qcqzm5ggjnz9d2wbz0c") (f (quote (("default"))))))

(define-public crate-graphics-style-0.1.5 (c (n "graphics-style") (v "0.1.5") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "1y72vh83qig9wi505bxrhqa8bblg8hzzypxaf406dq6caf5jrhyd") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.0 (c (n "graphics-style") (v "0.2.0") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "0mk1fqr5203yqmc4bf73lgviflzvjikj21496r4hkqj5xjc8v7fx") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.1 (c (n "graphics-style") (v "0.2.1") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "1q66g9gik0r31dsjr6gv6zw0ms06m905zrqkhfa74bz58kvr6fm4") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.3 (c (n "graphics-style") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "05417v5gi748mz3fspkk11xycwxslcqd5lhxh4dk25sv4d1ck830") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.4 (c (n "graphics-style") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q6nx6cb3phgllpgl89806zw9fi49lcdbh2whaci8ska0g76jg7w") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.5 (c (n "graphics-style") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1p2gk6bmxj1jpm5jcwsaj1vqllyz7c39dyknah8yf389f4xisgpc") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.6 (c (n "graphics-style") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0j7bl60f6sl6iww53816m896mkaibiy81rvf2wdavy15ccxhyaaw") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.7 (c (n "graphics-style") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0s6sxbda7zj9dn7mzk0cyw20hd749y61dsdngggsasbmwvw0pl5m") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.8 (c (n "graphics-style") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1jkz89i84vvpp8fy74wv32wk3qc5vsvhb803b393ajpvi253cr0x") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.9 (c (n "graphics-style") (v "0.2.9") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1w55kllzszz4m6nh3dik7p50q756b2fn8aha7nzgjpk72brlrfxd") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.10 (c (n "graphics-style") (v "0.2.10") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "1j6ik0g1pgf2am73y8sf7adaldlhqcp1rj5xsb48zi9i70m1kff4") (f (quote (("default"))))))

(define-public crate-graphics-style-0.2.11 (c (n "graphics-style") (v "0.2.11") (d (list (d (n "graphics-error") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "setting") (r "^0.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "wolfram-expr") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0vsm9ly76jm6ji87xmfbv5lhmnrv123jjqi6a4c1l48iwvp1qaba") (f (quote (("default"))))))

(define-public crate-graphics-style-0.3.0 (c (n "graphics-style") (v "0.3.0") (d (list (d (n "graphics-error") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0mmp9h19gvgdgpgd6d30iaa0nbqw45q5c6shk9qlkv7gc4wzm3lq") (f (quote (("default"))))))

