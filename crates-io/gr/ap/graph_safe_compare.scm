(define-module (crates-io gr ap graph_safe_compare) #:use-module (crates-io))

(define-public crate-graph_safe_compare-0.1.0 (c (n "graph_safe_compare") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11") (o #t) (d #t) (k 0)) (d (n "wyhash") (r "^0.5") (o #t) (d #t) (k 0)))) (h "03aaak8sqfzwbmr864i7l3cmrmnbr4mwkmnq0i6qbdb5bi7vqhkj") (f (quote (("wyrng" "wyhash") ("std" "alloc") ("default" "std" "wyrng") ("alloc")))) (r "1.57")))

(define-public crate-graph_safe_compare-0.1.1 (c (n "graph_safe_compare") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "fastrand") (r "^1") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11") (o #t) (d #t) (k 0)) (d (n "wyhash") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0n98g5jigfs0qz7dkc4nfjsjj3xsf44j1pj8qh8m7b8l2xsp3ghi") (f (quote (("wyrng" "wyhash") ("std" "alloc") ("default" "std" "wyrng") ("alloc")))) (r "1.57")))

(define-public crate-graph_safe_compare-0.2.0 (c (n "graph_safe_compare") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "cfg_rust_features") (r "^0.1") (d #t) (k 1)) (d (n "fastrand") (r "^1") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11") (o #t) (d #t) (k 0)) (d (n "wyhash") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1ncc1x00xa9a6l669d6cmyh85x6jaip4sz7swfn53m2cz7jgl6xr") (f (quote (("wyrng" "wyhash") ("std" "alloc") ("default" "std" "wyrng") ("anticipate") ("alloc")))) (r "1.57")))

(define-public crate-graph_safe_compare-0.2.1 (c (n "graph_safe_compare") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "cfg_rust_features") (r "^0.1") (d #t) (k 1)) (d (n "fastrand") (r "^1") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11") (o #t) (d #t) (k 0)) (d (n "wyhash") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1nrq43ixj9qxnyb782ka3dqanv5w3pfcxq1rnqiydjw1k1pwwcri") (f (quote (("wyrng" "wyhash") ("std" "alloc") ("default" "std" "wyrng") ("anticipate") ("alloc")))) (r "1.57")))

