(define-module (crates-io gr ap graphsearch) #:use-module (crates-io))

(define-public crate-graphsearch-0.2.0 (c (n "graphsearch") (v "0.2.0") (h "1g2cn5sc0fl9k37vyrbr23x6vbsq1kfamzbrrv46li0k9fhaikks")))

(define-public crate-graphsearch-0.2.1 (c (n "graphsearch") (v "0.2.1") (h "00w1y6bbrcb69l9rzh2j9zdgcydkb0b68i1pxirzbs804ygirjq0")))

(define-public crate-graphsearch-0.3.0 (c (n "graphsearch") (v "0.3.0") (h "1qnjslgnvvrz1azkxi73s6kjin1v1smljm7blhfz1zskkiqgbd6w")))

(define-public crate-graphsearch-0.4.0 (c (n "graphsearch") (v "0.4.0") (h "181vk9fyh17jbcf1jrcn4f82z5jhw56gh2f3bcvhmmdrwggqw0nh")))

(define-public crate-graphsearch-0.4.1 (c (n "graphsearch") (v "0.4.1") (h "0nm1fs8i6qwqvvzp1drwjn961w691fhif37s8igz0ca21dq9gyiq")))

(define-public crate-graphsearch-0.5.0 (c (n "graphsearch") (v "0.5.0") (h "1sn739x4q30rmf568m2lhn55fqkplbn712wzs4wf9wy5kdyv0qg3")))

(define-public crate-graphsearch-0.5.1 (c (n "graphsearch") (v "0.5.1") (h "1lj0lg9w8a4hjl217gi1id5l06d0j56g7gw4lvgwa89q8xh4k7jw")))

(define-public crate-graphsearch-0.6.0 (c (n "graphsearch") (v "0.6.0") (h "1gb3zbwa2rvxvsq0wci58k9a2kwzm8j7p2rrwzpw4bivzis5sqf4")))

