(define-module (crates-io gr r- grr-imgui) #:use-module (crates-io))

(define-public crate-grr-imgui-0.2.0 (c (n "grr-imgui") (v "0.2.0") (d (list (d (n "glutin") (r "^0.24") (d #t) (k 2)) (d (n "grr") (r "^0.8") (d #t) (k 0)) (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.4") (d #t) (k 2)))) (h "0091962j1i7dp4fga2f5ymxyywcwgkripnzn7lb2qxyinr4rkgib")))

