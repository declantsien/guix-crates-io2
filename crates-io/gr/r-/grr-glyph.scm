(define-module (crates-io gr r- grr-glyph) #:use-module (crates-io))

(define-public crate-grr-glyph-0.1.0 (c (n "grr-glyph") (v "0.1.0") (d (list (d (n "glutin") (r "^0.19") (d #t) (k 2)) (d (n "glyph_brush") (r "^0.2") (d #t) (k 0)) (d (n "grr") (r "^0.5") (d #t) (k 0)))) (h "1snhj312gpgs4xajjk6h7habrfplzaafk9p4ps7523w2br7b2cgf")))

