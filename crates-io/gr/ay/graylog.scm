(define-module (crates-io gr ay graylog) #:use-module (crates-io))

(define-public crate-graylog-0.1.0 (c (n "graylog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1faab6hzkqjhvnxhzi6l8xwb6l4h4hjkl16iw0xbnpqfiswgc6gd") (y #t)))

(define-public crate-graylog-0.1.1 (c (n "graylog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "193zdvr9qpf2lr5cdf74s85xxg9fbv73vni3k2z7ava9pqfzfiig") (y #t)))

(define-public crate-graylog-0.1.2 (c (n "graylog") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock" "serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "138w6x8iy1nh7k7dr2fhq5744hxfs3x1zza2ssi72zbd23k2qxwz")))

