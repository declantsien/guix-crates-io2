(define-module (crates-io gr ay graylog-to-grafana) #:use-module (crates-io))

(define-public crate-graylog-to-grafana-0.1.0 (c (n "graylog-to-grafana") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1qhnwvwrbzv3g4fcgcq80pyvvwsgjv17s2fndbd1iqbrqq63p28j")))

(define-public crate-graylog-to-grafana-0.1.1 (c (n "graylog-to-grafana") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1j17mylx6p5801vvd2sci3ybamd2877s6n2ssb8i9cyk05xh15vq")))

(define-public crate-graylog-to-grafana-0.2.0 (c (n "graylog-to-grafana") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0rhk8jkrcp74njqi9kvx6ibfqv4ch5p68p83h6lkvl3h7yvypy8p")))

(define-public crate-graylog-to-grafana-0.2.1 (c (n "graylog-to-grafana") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0sswvlmr8vg9kilqsy5qns74a3nmy611z38c05jq5723b75gn64g")))

