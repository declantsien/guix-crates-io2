(define-module (crates-io gr ay gray-codes) #:use-module (crates-io))

(define-public crate-gray-codes-0.1.0 (c (n "gray-codes") (v "0.1.0") (h "0dfvqs823r92s0iwjg9z49b6gsk9v4z3bf7v5v7hhax06m6kyww2")))

(define-public crate-gray-codes-0.1.1 (c (n "gray-codes") (v "0.1.1") (h "099apwvq8hskp8gzfh4p3ddc8d8i7dda3xpsk2nk0bkzivrm7sx8")))

