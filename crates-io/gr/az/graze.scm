(define-module (crates-io gr az graze) #:use-module (crates-io))

(define-public crate-graze-0.1.0 (c (n "graze") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 2)))) (h "0aam5alr19xpyy0w63ka66a79lcl80hrz04mh8mqb60w3gl58fx4")))

