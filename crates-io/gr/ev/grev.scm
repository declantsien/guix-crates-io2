(define-module (crates-io gr ev grev) #:use-module (crates-io))

(define-public crate-grev-0.1.0 (c (n "grev") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "08wak54c63h9i4359kp8mjfpgkvp7fc7i478j31qklgvy9cz9x2b")))

(define-public crate-grev-0.1.1 (c (n "grev") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "1hs2ykwz7qq77yvwiscvbsxv7np3qdcrpb81fbis8yb868ajkplf")))

(define-public crate-grev-0.1.2 (c (n "grev") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "0p7axi5cqlagp589rmrvil61jld4ki0gdqxdl46d1i8r5grg9zci")))

(define-public crate-grev-0.1.3 (c (n "grev") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "13glhhbvqp727isflfy1cdspxq8c4yzs0rbai80z72w2ldiz782f")))

