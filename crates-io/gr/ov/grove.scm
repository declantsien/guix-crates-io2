(define-module (crates-io gr ov grove) #:use-module (crates-io))

(define-public crate-grove-0.1.0 (c (n "grove") (v "0.1.0") (d (list (d (n "derive_destructure") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r ">=0.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "recursive_reference") (r "0.*") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0c03ayk8akw02w0kjnk6mvzjca71jnah0yl3gwq078pcwb02bcm0")))

(define-public crate-grove-0.2.0 (c (n "grove") (v "0.2.0") (d (list (d (n "derive_destructure") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r ">=0.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "recursive_reference") (r "0.*") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0cbz9i45gqg1n2m1jwiyqadj2491vm6pbxhwca118q6ydb7w6vgy")))

