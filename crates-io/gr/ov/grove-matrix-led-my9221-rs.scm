(define-module (crates-io gr ov grove-matrix-led-my9221-rs) #:use-module (crates-io))

(define-public crate-grove-matrix-led-my9221-rs-0.0.1 (c (n "grove-matrix-led-my9221-rs") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)))) (h "1vz16apsxvwhl44x2rjb3waf1x9xbnkaps7rpgasv0y9ljfxkgmh") (f (quote (("std") ("default" "std"))))))

(define-public crate-grove-matrix-led-my9221-rs-0.2.0 (c (n "grove-matrix-led-my9221-rs") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.15") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "panic-itm") (r "^0.4.2") (d #t) (k 2)) (d (n "stm32f3-discovery") (r "^0.7.2") (d #t) (k 2)))) (h "1lfl9sif2prgy0i779wbpgd390aizlrjlalcwc3aplc5w2adhpx6") (f (quote (("std") ("default"))))))

