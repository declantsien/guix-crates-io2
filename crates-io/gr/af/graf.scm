(define-module (crates-io gr af graf) #:use-module (crates-io))

(define-public crate-graf-0.0.1 (c (n "graf") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "06aj29l8dbccj9fq2jcd8lldd87ya2yxn7006ihffl0wdn3cp18p")))

