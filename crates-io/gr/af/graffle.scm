(define-module (crates-io gr af graffle) #:use-module (crates-io))

(define-public crate-graffle-0.1.0 (c (n "graffle") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)))) (h "0aj0p1rvjjhmxmk4miw3dd73869xw05c4q1ap3d5hgkxxc6r9l89") (s 2) (e (quote (("python-bindings" "dep:pyo3"))))))

