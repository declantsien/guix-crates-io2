(define-module (crates-io gr af grafana-dashboard) #:use-module (crates-io))

(define-public crate-grafana-dashboard-0.0.0 (c (n "grafana-dashboard") (v "0.0.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "00wx9j7yfs0yzq0anykp9d5ibnssjd8shldk3a53jxw4ppl8b1si")))

(define-public crate-grafana-dashboard-0.0.1 (c (n "grafana-dashboard") (v "0.0.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "1gh9cjd580y16dwhpzq15v1cx9hi0ksiaijxa18hivh1cgq0s657")))

(define-public crate-grafana-dashboard-0.0.2 (c (n "grafana-dashboard") (v "0.0.2") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0vkmz53cyr5c7lyk6b60csiw16wn400l77v72d9wqyvy3f6255zf")))

(define-public crate-grafana-dashboard-0.0.3 (c (n "grafana-dashboard") (v "0.0.3") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0fvq34hq38na4h454fv2lmaivspr12vdalyhm0gg6xjw2arwsakh")))

(define-public crate-grafana-dashboard-0.0.4 (c (n "grafana-dashboard") (v "0.0.4") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "0126z3j2sf6xq38df86vg4bizid3v8dvxiqzflms1agbmmqfwg34")))

(define-public crate-grafana-dashboard-0.0.5 (c (n "grafana-dashboard") (v "0.0.5") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "03wamgqndl93vj61vga65k04zdnd3nfg871a4jis3wwd8br63gd0")))

(define-public crate-grafana-dashboard-0.0.6 (c (n "grafana-dashboard") (v "0.0.6") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "06jbv0mjq289i6npxz65zqjq5b5rwn9clcy7raykj8ws4vcc6zsr")))

(define-public crate-grafana-dashboard-0.0.7 (c (n "grafana-dashboard") (v "0.0.7") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 2)))) (h "1hvb1qc2yvsvs76iq5a76spn799rdi6xnngaivnf0n1z6c7i6blc")))

