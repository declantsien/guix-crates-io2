(define-module (crates-io gr af grafbase-federated-graph) #:use-module (crates-io))

(define-public crate-grafbase-federated-graph-0.1.0 (c (n "grafbase-federated-graph") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hy4q70lp0yag39nxpsx5hy1bhvfryalmm8bhsgfpivz0bs2n0vn") (f (quote (("render_sdl") ("default" "render_sdl")))) (y #t)))

(define-public crate-grafbase-federated-graph-0.1.1 (c (n "grafbase-federated-graph") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ajv0gcxz5w4dhrwl7426fvdaffg4fsq97i0ikz3ag5h7r8qnjbd") (f (quote (("render_sdl") ("default" "render_sdl")))) (y #t)))

