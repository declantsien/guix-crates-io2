(define-module (crates-io gr af graftpress) #:use-module (crates-io))

(define-public crate-graftpress-0.1.0 (c (n "graftpress") (v "0.1.0") (d (list (d (n "futures") (r "~0.1.26") (d #t) (k 0)) (d (n "graftpress_core") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "~0.1.17") (d #t) (k 0)) (d (n "hyper") (r "~0.12.27") (d #t) (k 0)) (d (n "realm") (r "^0.1.6") (d #t) (k 0)))) (h "006rka21zxrz8h808nc6kzk0km8hzp5nfgi5vm4najs6xkfksrs0")))

(define-public crate-graftpress-0.1.1 (c (n "graftpress") (v "0.1.1") (d (list (d (n "futures") (r "~0.1.26") (d #t) (k 0)) (d (n "graftpress_core") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "~0.1.17") (d #t) (k 0)) (d (n "hyper") (r "~0.12.27") (d #t) (k 0)) (d (n "realm") (r "^0.1.7") (d #t) (k 0)))) (h "10gqfyr37x348wwng6myrqf3lazmwnlh3gax9kzrpa0cv8ds80z0")))

