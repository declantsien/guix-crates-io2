(define-module (crates-io gr af grafen) #:use-module (crates-io))

(define-public crate-grafen-0.3.1 (c (n "grafen") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)))) (h "0crilmicjfv1894j70yv1dg9s7l5qjqd1sb1x5hnag6ph3cr5wsp")))

(define-public crate-grafen-0.3.2 (c (n "grafen") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)))) (h "1c7bsip8hd2fbaw4a5z5kf55x6w8rfnczwp51zspidmi7z89z3gh")))

(define-public crate-grafen-0.3.3 (c (n "grafen") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)))) (h "0shchdhg517haabc4mqrcjxg0c5n0p8l2kpk05hm88a7ii8rd2l2")))

(define-public crate-grafen-0.4.0 (c (n "grafen") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)))) (h "13cdb9c7svynw2wx8n2xr5qf90fy9jbxlb9sb3rcybjdrhgsi8jw")))

(define-public crate-grafen-0.4.1 (c (n "grafen") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)))) (h "1cgx94c37ihl52hdccn8r09lm61vgdzzkw8sxc68drlj4wpbbn8d")))

(define-public crate-grafen-0.4.2 (c (n "grafen") (v "0.4.2") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)))) (h "1wr65nb9amx51s1j8bwbrq5q7sl45yi6rr75531zwwshd6s1b07h")))

(define-public crate-grafen-0.5.0 (c (n "grafen") (v "0.5.0") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)))) (h "1mhy9r1ba4zg3fv7pmggnbm7xpvrw8f9xa836fixclsfw3q5j8pz")))

(define-public crate-grafen-0.5.1 (c (n "grafen") (v "0.5.1") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)))) (h "0i7bmwab62z7858sq7f2w062niih7sv9lvc52qkw5x45gr41xm92")))

(define-public crate-grafen-0.5.2 (c (n "grafen") (v "0.5.2") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)))) (h "0qcfqfqn0naj069wqkks0qhnjxcjnbdx7ggyvym18lw2d140qpss")))

(define-public crate-grafen-0.6.0 (c (n "grafen") (v "0.6.0") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02gg41h3jmacx2wm4kkzxp9215il6bryyf8y7879vvpa8cjcq64r")))

(define-public crate-grafen-0.6.1 (c (n "grafen") (v "0.6.1") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rcwvzacc6fw2fyh5q05dxh752srjlvr0kwfi7k0y88432psd7zj")))

(define-public crate-grafen-0.7.0 (c (n "grafen") (v "0.7.0") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13pvc2l291a1q0dwm0lx7avvkwjgfm1knl94g966b87xbcmd1zj1")))

(define-public crate-grafen-0.7.1 (c (n "grafen") (v "0.7.1") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "068axrsambz27p0gdlzwhpvj1301h4n2ymgcd1d13gib4q4c6l0y")))

(define-public crate-grafen-0.8.0 (c (n "grafen") (v "0.8.0") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05hb06yidnisd6mhms9ynbi3pmrcg09dplmjf4cw6d6qa9k2ban8")))

(define-public crate-grafen-0.9.0 (c (n "grafen") (v "0.9.0") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "031vsqivxhndwcmqisgxgfhl1a7bwlzlmlanvdji9ihjznf9h89m")))

(define-public crate-grafen-0.9.1 (c (n "grafen") (v "0.9.1") (d (list (d (n "ansi_term") (r "~0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.23.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "~0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13qnykxhipjgv6zsakzchajm0xphar4mxsxmh9g6vhndm6g6licx")))

