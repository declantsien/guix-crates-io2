(define-module (crates-io gr af grafferous) #:use-module (crates-io))

(define-public crate-grafferous-0.1.0 (c (n "grafferous") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dv788hp1xyj5l47ci1nm8p199rs24l4vl6n7ldp86d1py3059b5")))

(define-public crate-grafferous-0.1.1 (c (n "grafferous") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02wi485s142bw8aysg93az160dr65i36w6al8acjc4g3cdcd074f")))

