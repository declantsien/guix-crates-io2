(define-module (crates-io gr af grafbase-local-common) #:use-module (crates-io))

(define-public crate-grafbase-local-common-0.1.0-pre.0 (c (n "grafbase-local-common") (v "0.1.0-pre.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01hchlh4wnc04lfqj0ikd76s806ffcd8hn5jzrm95cz657d4wyh2") (y #t)))

(define-public crate-grafbase-local-common-0.1.0-pre.1 (c (n "grafbase-local-common") (v "0.1.0-pre.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "041fj50bfyd9nvwgjh68a2mllggqscgmxddw6r01ayxb63v1yr6k")))

(define-public crate-grafbase-local-common-0.1.0-pre.2 (c (n "grafbase-local-common") (v "0.1.0-pre.2") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ygdmkynhbcshans191h295gqgb6m1f3cdsq5yzw3jadxk20qq0h")))

(define-public crate-grafbase-local-common-0.1.0-pre.4 (c (n "grafbase-local-common") (v "0.1.0-pre.4") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12nrmnm2hdn1ndvl2ys3r0dci3w8d6h0ipd9sgjd6z32zl5myzp7")))

(define-public crate-grafbase-local-common-0.2.0 (c (n "grafbase-local-common") (v "0.2.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0v19n1pxskq0rl1y3lfhd96lmizjxrkwfmz1wb0wc6qamb3hryzv")))

(define-public crate-grafbase-local-common-0.3.0 (c (n "grafbase-local-common") (v "0.3.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w59z3kxhx9dgxn8nh121k0bsgpzk97sryijpnr735h8js4aqr1v")))

(define-public crate-grafbase-local-common-0.4.0 (c (n "grafbase-local-common") (v "0.4.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nassqij161vdjjdq1laqy40yc3z58dxa02y5kaf20ja3x07cqzk")))

(define-public crate-grafbase-local-common-0.4.1 (c (n "grafbase-local-common") (v "0.4.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "120la32fa9cpwm52h2pnnr849m4cabz614l82z7f62fxl55rkbfv")))

(define-public crate-grafbase-local-common-0.5.0 (c (n "grafbase-local-common") (v "0.5.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h3rqac6ghwjiwmnj2rxx95agv0wkdn1kg0xaamffw606056ylsk")))

(define-public crate-grafbase-local-common-0.5.1 (c (n "grafbase-local-common") (v "0.5.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01q3aax86nsb2pg0vnz26c2mdgsg83vg5kxxq1ryfisdjpqchhk9")))

(define-public crate-grafbase-local-common-0.6.0 (c (n "grafbase-local-common") (v "0.6.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10m3lwr8pisksbaddp0wqkrifzzf0kwb709cg4sz6rpjas6c6f29")))

(define-public crate-grafbase-local-common-0.7.0 (c (n "grafbase-local-common") (v "0.7.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f2n3jzhnvg0y4p64qvd74qdc8cj3zi2iy8gm05ffgs7m710gqna")))

(define-public crate-grafbase-local-common-0.8.0 (c (n "grafbase-local-common") (v "0.8.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02ph5p5r9lw59q5bbkdhpzfxd3jfmg4jppq7y32na8hb2gabn04x")))

(define-public crate-grafbase-local-common-0.9.0 (c (n "grafbase-local-common") (v "0.9.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q9fnd04r5lnw9cfyyvzdahq8qp6pw15cis7k0q74s90kza58ki7") (y #t)))

(define-public crate-grafbase-local-common-0.9.1 (c (n "grafbase-local-common") (v "0.9.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0br3y2r66zhnndkj5k5ww7d7wn4bykdxljsarl7q51kl708b5sfj")))

(define-public crate-grafbase-local-common-0.10.0 (c (n "grafbase-local-common") (v "0.10.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0px1b89dl9hihykjpyzdvd66k0s133czn9r54h2vjcz3lgvfa223")))

(define-public crate-grafbase-local-common-0.11.0 (c (n "grafbase-local-common") (v "0.11.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15ipcsrhgpr3y0d1057d36586x05n278j5i1bi8l6b7gk8cvr3yf")))

(define-public crate-grafbase-local-common-0.11.1 (c (n "grafbase-local-common") (v "0.11.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1agwnzdswhsxa30xzal65qyxfp593396ymggfjzrn20ds23g46sc")))

(define-public crate-grafbase-local-common-0.11.2 (c (n "grafbase-local-common") (v "0.11.2") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08ckk6scnfig9jr4i108c6advgbcy04ywislkx1bi5hmj5d3lihs")))

(define-public crate-grafbase-local-common-0.12.0 (c (n "grafbase-local-common") (v "0.12.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kl1cxcqh7fa8v193vrkjs61pqd7091yj7glv89sih4zf0ha08sd")))

(define-public crate-grafbase-local-common-0.13.0 (c (n "grafbase-local-common") (v "0.13.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x4gvqz0qd3sanppjnwf7idsv16jznbvkwfnqhdibz0k7356nhsb")))

(define-public crate-grafbase-local-common-0.14.0 (c (n "grafbase-local-common") (v "0.14.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "104mjljmc0dfa5w0amcaagccj03s3rf8x7ff0pc2lvb6hcqgds58")))

(define-public crate-grafbase-local-common-0.14.1 (c (n "grafbase-local-common") (v "0.14.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16ns0jmxyavnlz07l76pn310yd3jq7fxrjd3bn57hd2bpjqgkh1b")))

(define-public crate-grafbase-local-common-0.15.0 (c (n "grafbase-local-common") (v "0.15.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01nay5aih3wk8pd46hxvn339jlwirw8zj3r870yrix4c02zsf1zl")))

(define-public crate-grafbase-local-common-0.16.0 (c (n "grafbase-local-common") (v "0.16.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i91rhqbjglk685l5pjrxv8ym53w2yh2555bf5wza98p74pi9v99")))

(define-public crate-grafbase-local-common-0.17.0 (c (n "grafbase-local-common") (v "0.17.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ng7f0m6yrc6lcm8zgnmkbk0y399000vd1i6zx5pmz5h7sismp38")))

(define-public crate-grafbase-local-common-0.18.0-rc.1 (c (n "grafbase-local-common") (v "0.18.0-rc.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r031y2c4rq2mldjq7sjyci20ihfrfbby342l4995aiqqzx19gma")))

(define-public crate-grafbase-local-common-0.18.0-rc.2 (c (n "grafbase-local-common") (v "0.18.0-rc.2") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19cc672vih8hs7n6xl9a9x0wkwr0plgplcikf1dplwlsy6vqai0i")))

(define-public crate-grafbase-local-common-0.18.0-rc.3 (c (n "grafbase-local-common") (v "0.18.0-rc.3") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xixd517c717xk09wcpnxs39ywmg5xv4175c9z58q4xrfqyzpz04")))

(define-public crate-grafbase-local-common-0.18.0-rc.4 (c (n "grafbase-local-common") (v "0.18.0-rc.4") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ahyph5c3yjf6rwk8z0fpjgf4bn4n2g28fnmn01mvbngzfnlw76r")))

(define-public crate-grafbase-local-common-0.18.0-rc.5 (c (n "grafbase-local-common") (v "0.18.0-rc.5") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06jjhai58k2rsi211484x9wphzl2qgs3pr7fj779dqil364gdls0")))

(define-public crate-grafbase-local-common-0.18.0-rc.6 (c (n "grafbase-local-common") (v "0.18.0-rc.6") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "195hy05794n7pi83vc49s1s8vb5njiffl728kbnd4vn56l3rdkk5")))

(define-public crate-grafbase-local-common-0.18.0-rc.7 (c (n "grafbase-local-common") (v "0.18.0-rc.7") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xjr271lvhpsmbn0zxk3fgczr6ry0rk3vkmr4zvd92sbhqa01iy9")))

(define-public crate-grafbase-local-common-0.18.0-rc.8 (c (n "grafbase-local-common") (v "0.18.0-rc.8") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0haypd33aqyvfyaja5bvlj43f4iiqwp53k2ibzjj2s75s62gariv")))

(define-public crate-grafbase-local-common-0.18.0 (c (n "grafbase-local-common") (v "0.18.0") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1096ymn7xnqz9knzwi5q59bpvff13vk0wd9bpj8l4xkm0mqz1z9i")))

(define-public crate-grafbase-local-common-0.18.1 (c (n "grafbase-local-common") (v "0.18.1") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08l1shz71dcix2fh2xd4r4ln8vwk0yr12j7mjkmjgc3iwgz6knmk")))

(define-public crate-grafbase-local-common-0.18.2 (c (n "grafbase-local-common") (v "0.18.2") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06fcwk1690nkxp8wn3jdziv6pjy159vni1a7z50vpdc4aqhmlcsr")))

(define-public crate-grafbase-local-common-0.18.3 (c (n "grafbase-local-common") (v "0.18.3") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qfk2s2ib8ih329r8syzyhd4z4gr28v1bavv77g69n1wxk4r6vgv")))

(define-public crate-grafbase-local-common-0.18.4 (c (n "grafbase-local-common") (v "0.18.4") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ffyxlis9dnkzislqwiiav8s7pkd7mk7rcah5rwb66rp8jgv4798")))

(define-public crate-grafbase-local-common-0.18.5 (c (n "grafbase-local-common") (v "0.18.5") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kj39n4as0s2ns76sb4xx6rdd7gw01if9lgdb6m10csn3qn0gmzr")))

(define-public crate-grafbase-local-common-0.18.6 (c (n "grafbase-local-common") (v "0.18.6") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05zx1hrfrx73i00qdb0l0hz4irpr0v0bm9y2rwapvh3amxfgfbzx")))

(define-public crate-grafbase-local-common-0.18.7 (c (n "grafbase-local-common") (v "0.18.7") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "090k6kwn06fqzymraw08by05ywvb5zgdfvma6vy8f2sc8pw03p6q")))

(define-public crate-grafbase-local-common-0.18.8 (c (n "grafbase-local-common") (v "0.18.8") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1khc2xrdkzs8w0vc6qr31prhif7l62pjffgx6z8ajbghs7qkvvxz")))

(define-public crate-grafbase-local-common-0.18.9 (c (n "grafbase-local-common") (v "0.18.9") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fqz09vvxaapw8nb1favnimyd459k05syb2crx0rr7ppa6n7kcaj")))

(define-public crate-grafbase-local-common-0.18.10 (c (n "grafbase-local-common") (v "0.18.10") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kfbvfl1s90kzskcnckbv3k5y32qggs68cll1qqfpjqcf5blvqgk")))

(define-public crate-grafbase-local-common-0.18.11 (c (n "grafbase-local-common") (v "0.18.11") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p4mkznjgq35vl7gf46jf28g3324i36546xffvg45nrlkv27mr3j")))

(define-public crate-grafbase-local-common-0.18.12 (c (n "grafbase-local-common") (v "0.18.12") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "011c9ypsq5cmfmcisynqjsnfvybpc2rj75rqlzn0rgj3yclh84qb")))

(define-public crate-grafbase-local-common-0.18.13 (c (n "grafbase-local-common") (v "0.18.13") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15day9gfvfy7grwhvv745520n1ilyv7jrgjkdj4kscl53ygdylzk")))

(define-public crate-grafbase-local-common-0.19.0 (c (n "grafbase-local-common") (v "0.19.0") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0kyihbzp73zwbp83yvrdk5jjybvdv09l3q1jcq773xj6jxs7n8rz")))

(define-public crate-grafbase-local-common-0.19.1 (c (n "grafbase-local-common") (v "0.19.1") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03zc43ylcllhphz5vhvanlcf7b3gr7p1lakcy9q1zdfljnj2j7z0")))

(define-public crate-grafbase-local-common-0.19.2 (c (n "grafbase-local-common") (v "0.19.2") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s8v5gp5dw3529vmji61ypsj8d84wxznrnbamfhqi2vyhcb3jhfw")))

(define-public crate-grafbase-local-common-0.20.0-rc.1 (c (n "grafbase-local-common") (v "0.20.0-rc.1") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i9f888fa52ix6qfl2kpi254mpmpkagip8dspymyd773ljy6hqki")))

(define-public crate-grafbase-local-common-0.20.0-rc.2 (c (n "grafbase-local-common") (v "0.20.0-rc.2") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zsif0zg8pxhqc2yfjjnj2j937pazqayzndc0n97vslry90b6iwb")))

(define-public crate-grafbase-local-common-0.20.0-rc.3 (c (n "grafbase-local-common") (v "0.20.0-rc.3") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "102vvr25yq2fx45s2bslb5mxcc77ds9h6db68lfd7zir1ycpmj36")))

