(define-module (crates-io gr af grafgrep) #:use-module (crates-io))

(define-public crate-grafgrep-0.1.0 (c (n "grafgrep") (v "0.1.0") (h "0mn7lpascrm2gj98ypm3pbcqk6ynrpncwzkigb3b46rf6yygh2mp")))

(define-public crate-grafgrep-0.1.1 (c (n "grafgrep") (v "0.1.1") (h "016xa89rd4m4czjgnrp7dswxv6sp0y1qj2irfa81b2gk31s90a4f")))

