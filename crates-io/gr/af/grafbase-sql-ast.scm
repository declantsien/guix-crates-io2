(define-module (crates-io gr af grafbase-sql-ast) #:use-module (crates-io))

(define-public crate-grafbase-sql-ast-0.1.0 (c (n "grafbase-sql-ast") (v "0.1.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pa64faf8gd6wxafg2w1rl5fd7agjx9c8m6sdqpc2hxpfz7i8ng8") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.1 (c (n "grafbase-sql-ast") (v "0.1.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1qr4mkkq4x2j3q35q3vn8klmcsd9ls95vcgpjhq97z2k6p4kkai9") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.2 (c (n "grafbase-sql-ast") (v "0.1.2") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cm9qwnd8685kha0fz7mif59a40h0drfs7pjzp1icmsfv50vhlfh") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.3 (c (n "grafbase-sql-ast") (v "0.1.3") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1z6qbzkrv0yhvrymv4mjabq691v9iq5m5jq6xxkdjvsqbfifvdvn") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.4 (c (n "grafbase-sql-ast") (v "0.1.4") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wws185krwzavhknshrcbxw7lmwnnw44a87bi3d8psq1zzmll5az") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.5 (c (n "grafbase-sql-ast") (v "0.1.5") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ip3cbzyq4l5j3a5dx8r237xinxgzsikhxg7cmmak9ch61jsqm6s") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.6 (c (n "grafbase-sql-ast") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0djc7ps6fqpa7jar9zssm87xh70f8w6r0fzai0lmr8xsm169n3zs") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.7 (c (n "grafbase-sql-ast") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g9dcg7qgky2fwvh4gmgarslr5rpm3g8dp48xm504365ifi3gfjg") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.8 (c (n "grafbase-sql-ast") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "165xf1aaxbzdvjnm8d48llfkgija75q7flhyr31cxhs4vmv9rlw0") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

(define-public crate-grafbase-sql-ast-0.1.9 (c (n "grafbase-sql-ast") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15ik8s1fxcjn4sz0xzmb5dmn32y0gprbgm0a3mjlh9wa9m2cgan3") (f (quote (("sqlite") ("postgresql") ("mysql") ("default" "postgresql" "mysql" "sqlite"))))))

