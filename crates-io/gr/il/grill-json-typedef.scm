(define-module (crates-io gr il grill-json-typedef) #:use-module (crates-io))

(define-public crate-grill-json-typedef-0.0.0 (c (n "grill-json-typedef") (v "0.0.0") (h "06vcrj4b3z2vz9wn4yqh00g8pvig4z9qrkl9q8phl4hh36sgn05d")))

(define-public crate-grill-json-typedef-0.0.1 (c (n "grill-json-typedef") (v "0.0.1") (h "150piknpbnrsrg05ph380y3d2pd3injy2izjnjyzhkglw5lclr7n")))

