(define-module (crates-io gr il grill-json-schema) #:use-module (crates-io))

(define-public crate-grill-json-schema-0.0.0 (c (n "grill-json-schema") (v "0.0.0") (h "1qvlh939npxv7xp8g0vm9zqj0kj829bwh6x2ypwgjwwv79vzy7p0")))

(define-public crate-grill-json-schema-0.0.1 (c (n "grill-json-schema") (v "0.0.1") (h "189jpfv1db7285nh5d8hlp9kdzazz506zdr7qw5blklcrdb48gdq")))

