(define-module (crates-io gr il grill-core) #:use-module (crates-io))

(define-public crate-grill-core-0.0.0 (c (n "grill-core") (v "0.0.0") (d (list (d (n "bigdecimal") (r "^0.3") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.13") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1991frqbw8sf29k5cw6lrpcwcnkhaa7v0v58bl5scii97hh1mb28") (f (quote (("big_num" "bigdecimal" "num-bigint" "num-integer" "num-traits"))))))

(define-public crate-grill-core-0.0.1 (c (n "grill-core") (v "0.0.1") (h "1wmglz4lr2b22bwwc9sk1g23jjmc6wm0rzbgi5d64nmyp5r6q51v")))

