(define-module (crates-io gr il grill-json-pointer) #:use-module (crates-io))

(define-public crate-grill-json-pointer-0.0.0 (c (n "grill-json-pointer") (v "0.0.0") (h "0f932lmsldmhfmbdlz9pn84scvwa5y2dj585g2p4y06pqf8f05si")))

(define-public crate-grill-json-pointer-0.0.1 (c (n "grill-json-pointer") (v "0.0.1") (h "013ka828gs9k68jiv2bha20avvb6h0cf7dz14hrkgbwzpfcq1zvs")))

