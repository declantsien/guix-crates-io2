(define-module (crates-io gr ah graham-number) #:use-module (crates-io))

(define-public crate-graham-number-0.0.0 (c (n "graham-number") (v "0.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "power-mod") (r "^0.1.0") (d #t) (k 0)))) (h "10ylhcl3n1623g8l8jjnz6l5wazdmia4n60qbilcvlwngiif7v2d") (f (quote (("default"))))))

(define-public crate-graham-number-1.0.0 (c (n "graham-number") (v "1.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "14v4c8ssj9mipy0prksvc462khs24v2crimd2g901kb2r77qjdp0") (f (quote (("default"))))))

(define-public crate-graham-number-0.1.0 (c (n "graham-number") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1qab0gbcyvrng85ipmlz6vi961fwp8spiyfn1y8mjl2kmndq3bc8") (f (quote (("default"))))))

