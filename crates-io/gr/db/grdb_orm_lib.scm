(define-module (crates-io gr db grdb_orm_lib) #:use-module (crates-io))

(define-public crate-grdb_orm_lib-0.1.0 (c (n "grdb_orm_lib") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13m08mxkg46zwf5762gqfzqmpc9s0jzs5k0s760cv8vhb7mfdzpw")))

(define-public crate-grdb_orm_lib-0.1.1 (c (n "grdb_orm_lib") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "018b7d3h8rk54v3j3ngk0jsv80m6ylrhi8rq7vvscmpkvhyllsfi")))

(define-public crate-grdb_orm_lib-0.1.3 (c (n "grdb_orm_lib") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gi9vcz49d2mak4pp7ni0i3wliy9ln33kxn105p0i5h7d7dcxm4l")))

(define-public crate-grdb_orm_lib-0.1.4 (c (n "grdb_orm_lib") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1sc3hdg0h7f18ymk1jhqhd8ingfmg1gkrl9prxay6ilc626mzcym")))

(define-public crate-grdb_orm_lib-0.1.5 (c (n "grdb_orm_lib") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1d8vrph08vbj983j7lnfdplkggb99s1s58qdq603vyirw096sdmp")))

(define-public crate-grdb_orm_lib-0.1.2 (c (n "grdb_orm_lib") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1rwr8zi3dz7sq04v2xynsdi7h8n67461nwygl043pj8l2xzhq6c8")))

(define-public crate-grdb_orm_lib-0.1.6 (c (n "grdb_orm_lib") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "144dmkx0qxbfiz0y7mgk0x2ir7fhs8mn9b8gzpv3lrmfsxgizj93")))

