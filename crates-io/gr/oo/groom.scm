(define-module (crates-io gr oo groom) #:use-module (crates-io))

(define-public crate-groom-0.1.0 (c (n "groom") (v "0.1.0") (d (list (d (n "accept-header") (r "^0.2.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (f (quote ("as_ref" "deref"))) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (k 0)) (d (n "utoipa") (r "^4.2.0") (d #t) (k 0)))) (h "1c7y5radlzvwyigxlh6k5k998vx9p4v6pzyi2hmai6r1hxc47xjj") (r "1.78")))

