(define-module (crates-io gr oo groop) #:use-module (crates-io))

(define-public crate-groop-0.1.0 (c (n "groop") (v "0.1.0") (d (list (d (n "bat") (r "^0.24.0") (f (quote ("regex-fancy"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)))) (h "0dd3c4rx2r0g5cy1nzw0a16ig586ib5h4z2pggr668kwvgy9yq15")))

(define-public crate-groop-0.1.2 (c (n "groop") (v "0.1.2") (d (list (d (n "bat") (r "^0.24.0") (f (quote ("regex-fancy"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)))) (h "0ilja0w7fkcxg46b0pdwjl8zhs7azd0p769nrrr18spb24gdq9v5")))

(define-public crate-groop-0.1.3 (c (n "groop") (v "0.1.3") (d (list (d (n "bat") (r "^0.24.0") (f (quote ("regex-fancy"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)))) (h "15w7cp00s1p605vnrvmf2950gp631xz2gzjz3gshdh8aw8y91gs8")))

