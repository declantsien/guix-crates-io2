(define-module (crates-io gr oo groonga-sys) #:use-module (crates-io))

(define-public crate-groonga-sys-0.1.0 (c (n "groonga-sys") (v "0.1.0") (h "085mbbbd57r65jpig0c0hm998bkgbc5gj9cba3vvrbw6ac6dkxkw")))

(define-public crate-groonga-sys-0.2.0 (c (n "groonga-sys") (v "0.2.0") (d (list (d (n "libc") (r "~0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "036dn2ykak6s6wzf9ydivcapvxl641qmh5k79yciq2dn1arzhsvi")))

(define-public crate-groonga-sys-0.3.0 (c (n "groonga-sys") (v "0.3.0") (d (list (d (n "libc") (r "~0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "16drpmd8q206w98h8c4j20hcmzqk7szxm5bhkznxvlc1jhadwx1z")))

(define-public crate-groonga-sys-0.3.1 (c (n "groonga-sys") (v "0.3.1") (d (list (d (n "libc") (r "~0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "0zjd574f1bv2c8pbsbmg8ci2c828mfvqag08kc8xqnq1kxqr4rci")))

