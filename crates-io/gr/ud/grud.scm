(define-module (crates-io gr ud grud) #:use-module (crates-io))

(define-public crate-grud-0.1.0 (c (n "grud") (v "0.1.0") (h "1r26bmvdkl83c62xz2c3ldy0vapljg6azivf8gn758plv4845vw5")))

(define-public crate-grud-0.1.1 (c (n "grud") (v "0.1.1") (h "0bnvg3ydl0isybripqkp48pgd4f9mavqjywz1mrhpck0rny9b6ns")))

