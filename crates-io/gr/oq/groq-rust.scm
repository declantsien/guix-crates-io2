(define-module (crates-io gr oq groq-rust) #:use-module (crates-io))

(define-public crate-groq-rust-0.1.0 (c (n "groq-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "06qh94yzgqwq5a37s0qldcl2hz1c2abc4drs55cha9ixlalkarwf")))

