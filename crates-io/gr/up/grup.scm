(define-module (crates-io gr up grup) #:use-module (crates-io))

(define-public crate-grup-0.2.0 (c (n "grup") (v "0.2.0") (d (list (d (n "comrak") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simple-server") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0f1avfcib8kcdlkds3xcqssm4marm8h6x1x565x1jj3wh5yz8rgp")))

(define-public crate-grup-0.2.1 (c (n "grup") (v "0.2.1") (d (list (d (n "comrak") (r "^0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0ydbmnylfbmqwbi2pvm4dlhlqvy8d3k9ywpixbz807nd38a528nd")))

(define-public crate-grup-0.2.2 (c (n "grup") (v "0.2.2") (d (list (d (n "comrak") (r "^0.13.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.19") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i13lki66qnah33hgqjr9m29m3sd5jf7ywk5871jidqhirvzcyrh")))

(define-public crate-grup-0.2.3 (c (n "grup") (v "0.2.3") (d (list (d (n "comrak") (r "^0.17.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.19") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.20.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0zl7819bg0ywsk4y2lnll3fhwxkhvzs8dd9brsh3kq8f3l0xqvsd")))

