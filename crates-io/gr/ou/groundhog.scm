(define-module (crates-io gr ou groundhog) #:use-module (crates-io))

(define-public crate-groundhog-0.0.1 (c (n "groundhog") (v "0.0.1") (h "108ixgxxya2gijwmdyfkhwzsfrgz56bjgc5xhyv0biaklvryp4hv")))

(define-public crate-groundhog-0.1.0 (c (n "groundhog") (v "0.1.0") (h "1by6k5rbssbfx2c6z2lpvj9xyakd343b39x319qpw00hr98221ka") (f (quote (("u128"))))))

(define-public crate-groundhog-0.2.0 (c (n "groundhog") (v "0.2.0") (h "0bpxpqf321kifckvq89mfanx3wwc1yc58ps4r9jclqdkk9v5j34h") (f (quote (("u128"))))))

(define-public crate-groundhog-0.2.1 (c (n "groundhog") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "04njz23yii689jg3765xv84g91g51f6il9lc3rk8258s4fnxjf4a") (f (quote (("u128") ("std") ("instant" "std" "once_cell") ("default"))))))

(define-public crate-groundhog-0.2.2 (c (n "groundhog") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1h2f4b2z2cdhdl3w2x0bh26xmwj96wmnslp5pxkprjr3zna4rbin") (f (quote (("u128") ("std") ("instant" "std" "once_cell") ("default"))))))

(define-public crate-groundhog-0.2.3 (c (n "groundhog") (v "0.2.3") (d (list (d (n "once_cell") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "0v2gkf2m8mqzd04b7dhrq8q8kw6g685mm0yvfsbi3qq0g5cdpcq8") (f (quote (("u128") ("std") ("instant" "std" "once_cell") ("default"))))))

(define-public crate-groundhog-0.2.4 (c (n "groundhog") (v "0.2.4") (d (list (d (n "once_cell") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1h2whbygj89y6spszcfyw7yk7lr8a4x9dyc8hv0qvsj5jsjav6aq") (f (quote (("u128") ("std") ("instant" "std" "once_cell") ("default"))))))

(define-public crate-groundhog-0.2.5 (c (n "groundhog") (v "0.2.5") (d (list (d (n "once_cell") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1b21cnllryikvhs23fqfm3agyn5h8p203b27fny0719nl017r9yh") (f (quote (("u128") ("std") ("instant" "std" "once_cell") ("default"))))))

