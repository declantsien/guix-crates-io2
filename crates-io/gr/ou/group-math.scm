(define-module (crates-io gr ou group-math) #:use-module (crates-io))

(define-public crate-group-math-0.1.0 (c (n "group-math") (v "0.1.0") (h "0gh77wp8chwkiarvi7d2m8k0wa5yflsv4jlxrk9ndq9vij2f9cy1") (f (quote (("int-be") ("int") ("impl" "byte" "int") ("byte"))))))

(define-public crate-group-math-0.2.0 (c (n "group-math") (v "0.2.0") (h "0l4b1v5aga7i59vxq5ngwp7bw546ml4ml4sjirllmnjngpjlw3nf") (f (quote (("int-be") ("int") ("impl" "byte" "int") ("byte"))))))

(define-public crate-group-math-0.2.1 (c (n "group-math") (v "0.2.1") (h "035rkq6rwv32pinx8bdxgfz0i30a1adhsxz7sk4n643165mvgnv2") (f (quote (("int-be") ("int") ("impl" "byte" "int") ("byte"))))))

(define-public crate-group-math-0.2.2 (c (n "group-math") (v "0.2.2") (h "12pc9kxhwia1b6hy2s5fzickmigk8kd5f7mziwgyjjax2jh5x7g4") (f (quote (("int-prime") ("int-be") ("int") ("impl" "byte" "int" "int-prime") ("default" "impl") ("byte"))))))

(define-public crate-group-math-0.2.3 (c (n "group-math") (v "0.2.3") (h "1i2h0lzh28amv2qap9frn5bmqg517q9y4c495ghwvaw02kxys475") (f (quote (("int-prime") ("int-be") ("int") ("impl" "byte" "int" "int-prime") ("default" "impl") ("byte"))))))

(define-public crate-group-math-0.2.4 (c (n "group-math") (v "0.2.4") (h "09vzhqb1dak17za1ms0svfrk2bpxd9353wplfma4vr12q04jrkrm") (f (quote (("int-prime") ("int-be") ("int") ("impl" "byte" "int" "int-prime") ("default" "impl") ("byte"))))))

