(define-module (crates-io gr ou groupy) #:use-module (crates-io))

(define-public crate-groupy-0.2.0 (c (n "groupy") (v "0.2.0") (d (list (d (n "ff") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "0365h10f9hzwy62vkdh4vgnsn2iv3mbxharj82i3cl14kgcs5zyh")))

(define-public crate-groupy-0.2.1 (c (n "groupy") (v "0.2.1") (d (list (d (n "ff") (r "= 0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "0w2g36rllxlajxq0j4qc645qy2g4a41j83d66mvibhzxd818x3i0")))

(define-public crate-groupy-0.3.0 (c (n "groupy") (v "0.3.0") (d (list (d (n "fff") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)))) (h "0j02g62kacwxjkfa0l2ai9agnihnlqiy094fpxpcahfl5cm5agab")))

(define-public crate-groupy-0.3.1 (c (n "groupy") (v "0.3.1") (d (list (d (n "fff") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "12sdq08j8qmx9z8ww8cha8kmkjz7nr6xfk3m1g1sfxbxq6zg2f1d")))

(define-public crate-groupy-0.4.0 (c (n "groupy") (v "0.4.0") (d (list (d (n "fff") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "1cnz5i8kjgih851cilhd0z5rvn6qp6s4mxgf3cgy5ism56ngm4xq") (y #t)))

(define-public crate-groupy-0.4.1 (c (n "groupy") (v "0.4.1") (d (list (d (n "fff") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "098q3y47if8i9rh3glxa6bmx4vb8jbq5pxjhfci2gm5vn89v4svz")))

