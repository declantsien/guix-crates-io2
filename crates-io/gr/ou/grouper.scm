(define-module (crates-io gr ou grouper) #:use-module (crates-io))

(define-public crate-grouper-0.0.0 (c (n "grouper") (v "0.0.0") (h "05av3yc51jfmbc08hzvlanzqwcdxij32cl446iqfd09ldy9z70py")))

(define-public crate-grouper-0.1.0 (c (n "grouper") (v "0.1.0") (h "14qvv7a1gjfbzs0zgqllvbnn06vplqs1ad2pv1icn9wrl48mjydf")))

(define-public crate-grouper-0.2.0 (c (n "grouper") (v "0.2.0") (h "0ypnnfz6xs7xfj34cymq4zkza9rzvi91fiiwdkavqbaammcr0n3b")))

(define-public crate-grouper-0.3.0 (c (n "grouper") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pcpj3vwyyspy0hwz6xwgzg2mjdlc5lrp6dv099nv2dx7hx6kc3q")))

(define-public crate-grouper-0.3.1 (c (n "grouper") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0yxfx2zhc08n424xxlyzk2vagc1mv2gb9v2mmfv8vdsybh1kizc2")))

(define-public crate-grouper-0.4.0 (c (n "grouper") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1hgbrxbpsffap3n3cy7vcswyn0rkb554kinb5hhxlzkm9wms449s")))

