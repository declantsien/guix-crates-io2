(define-module (crates-io gr ou groupme_bot) #:use-module (crates-io))

(define-public crate-groupme_bot-0.1.1 (c (n "groupme_bot") (v "0.1.1") (d (list (d (n "reqwest") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16ls06j7r5n9bx9imdp0a63gijq61wjy5vix066bfp884vyk3jg7")))

(define-public crate-groupme_bot-0.1.2 (c (n "groupme_bot") (v "0.1.2") (d (list (d (n "reqwest") (r "~0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j3n08j3157z7vk5lh679y1743bzxpq96lfacfv7l7jh2shs4vmp")))

