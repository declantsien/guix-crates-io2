(define-module (crates-io gr ou groundhog-stm32g031) #:use-module (crates-io))

(define-public crate-groundhog-stm32g031-0.3.1 (c (n "groundhog-stm32g031") (v "0.3.1") (d (list (d (n "cortex-m-rtic") (r "^0.5.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "groundhog") (r "^0.1") (d #t) (k 0)) (d (n "stm32g0xx-hal") (r "^0.1.0") (f (quote ("stm32g031"))) (d #t) (k 0)))) (h "0l9fj18y4i26i3zrsqynd6w9ihbdjjl4mxff1mip51qndz1ys6kr")))

