(define-module (crates-io gr ou group-varint-offset-encoding) #:use-module (crates-io))

(define-public crate-group-varint-offset-encoding-0.1.0 (c (n "group-varint-offset-encoding") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "1f99khsvyxydxk16334s281zdq1z6cawfd2lkr1gdmq278cjr0nc")))

(define-public crate-group-varint-offset-encoding-0.1.1 (c (n "group-varint-offset-encoding") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "08s3ndmd687gbi9cz93ll8j34ax1ldrlby3n487gifxvkpc80zy7")))

