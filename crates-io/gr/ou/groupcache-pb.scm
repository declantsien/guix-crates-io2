(define-module (crates-io gr ou groupcache-pb) #:use-module (crates-io))

(define-public crate-groupcache-pb-0.1.0 (c (n "groupcache-pb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (f (quote ("prost"))) (d #t) (k 0)))) (h "1bsrd8pqmbgna9ghi1bs9cmqx56b45rlz7ixvsm9ravws4xkbh4w")))

