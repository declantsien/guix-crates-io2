(define-module (crates-io gr ou groundhog-nrf52) #:use-module (crates-io))

(define-public crate-groundhog-nrf52-0.2.0 (c (n "groundhog-nrf52") (v "0.2.0") (d (list (d (n "cortex-m-rtic") (r "^0.5.5") (d #t) (k 0)) (d (n "groundhog") (r "^0.1") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.12.0") (d #t) (k 0)))) (h "1hjf5cqf0400zhijadhrk7n4l6la02jq917jr23324lasny12wwd")))

(define-public crate-groundhog-nrf52-0.2.1 (c (n "groundhog-nrf52") (v "0.2.1") (d (list (d (n "cortex-m-rtic") (r "^0.5.5") (d #t) (k 0)) (d (n "groundhog") (r "^0.1") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.12.0") (d #t) (k 0)))) (h "13l2q0ap2b47yvx104w0dl1s9w2n2aibz1dvkx1lg21v9mq2z9mk")))

(define-public crate-groundhog-nrf52-0.3.0 (c (n "groundhog-nrf52") (v "0.3.0") (d (list (d (n "cortex-m-rtic") (r "^0.5.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "groundhog") (r "^0.2.5") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.12.1") (d #t) (k 0)))) (h "0g7wrq47a1qmhacgskhklqsx98wg164g5870sha0vrqssqn2jbsm") (f (quote (("default" "52840") ("52840"))))))

(define-public crate-groundhog-nrf52-0.3.1 (c (n "groundhog-nrf52") (v "0.3.1") (d (list (d (n "cortex-m-rtic") (r "^0.5.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "groundhog") (r "^0.2.5") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.12.1") (d #t) (k 0)))) (h "0gskrf93xp1vs1j11nm2l9l0kmivav3cbi0p3lk436d9ss80zp1b") (f (quote (("default" "52840") ("52840"))))))

(define-public crate-groundhog-nrf52-0.4.0 (c (n "groundhog-nrf52") (v "0.4.0") (d (list (d (n "cortex-m-rtic") (r "^0.5.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "groundhog") (r "^0.2.5") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.14.0") (d #t) (k 0)))) (h "106q6qh5pi69m7kll4paim2bnx0capba517rl7xzvmiqdm7y5106") (f (quote (("default" "52840") ("52840"))))))

(define-public crate-groundhog-nrf52-0.4.1 (c (n "groundhog-nrf52") (v "0.4.1") (d (list (d (n "cortex-m-rtic") (r "^0.5.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "groundhog") (r "^0.2.5") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.14.0") (d #t) (k 0)))) (h "0nic3wcikwyqn68m4hz69yklvaxl6mfkj85mv8gb2i9za3rpz5dr") (f (quote (("default" "52840") ("52840"))))))

(define-public crate-groundhog-nrf52-0.5.0 (c (n "groundhog-nrf52") (v "0.5.0") (d (list (d (n "cortex-m-rtic") (r "^0.5.9") (f (quote ("cortex-m-7"))) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "groundhog") (r "^0.2.5") (d #t) (k 0)) (d (n "nrf52840-hal") (r "^0.14.0") (d #t) (k 0)))) (h "1mcgwn9xkhsyysnadjpbad3m36qxbmg2kakf1qh971mhi3j568wf") (f (quote (("default" "52840") ("52840"))))))

