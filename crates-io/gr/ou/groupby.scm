(define-module (crates-io gr ou groupby) #:use-module (crates-io))

(define-public crate-groupby-0.1.0 (c (n "groupby") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.9") (d #t) (k 0)))) (h "0kzxjxpcr1r1wh0kafsiri3gf7mmmmc9xvw7rdxsd4cx4xplv8py")))

(define-public crate-groupby-0.1.1 (c (n "groupby") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.9") (d #t) (k 0)))) (h "1g96rydzii85avk91cnpdrddy7c3r3by3x3n5cvm4lznzqxripsr")))

(define-public crate-groupby-0.1.2 (c (n "groupby") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.9") (d #t) (k 0)))) (h "1v293aax66gifwxn0v4ibf8j3n27ir9mj5bzkzh2r5b2ja03kmhl")))

(define-public crate-groupby-0.1.3 (c (n "groupby") (v "0.1.3") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.9") (d #t) (k 0)))) (h "0w7f3dpiy32sncaldnlr3z4gbzwqjb11k298r5n612l4aya6k2py")))

(define-public crate-groupby-0.1.4 (c (n "groupby") (v "0.1.4") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.9") (d #t) (k 0)))) (h "02d4vcakmis4p535v1npzdqy6canyqvj7ky18iah79sp33i2nvby")))

(define-public crate-groupby-0.1.5 (c (n "groupby") (v "0.1.5") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.9") (d #t) (k 0)))) (h "1hzxlmxmlayhq2fwjdhnmm1ivysfld9w52crhpn0r3sh3gia1lwn")))

