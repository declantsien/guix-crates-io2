(define-module (crates-io gr ou grouping_by) #:use-module (crates-io))

(define-public crate-grouping_by-0.1.0 (c (n "grouping_by") (v "0.1.0") (h "02xfhwsncfwdmqzdbhi1yiya8916d05qx83jay10sxyzy4nw2fsb")))

(define-public crate-grouping_by-0.1.1 (c (n "grouping_by") (v "0.1.1") (h "00c4ayq5pqhyynjx0skcprafky9cca872ygl5nks6jvda6k2spad")))

(define-public crate-grouping_by-0.1.2 (c (n "grouping_by") (v "0.1.2") (h "16aaqc20vf969y3plmhxmim8nnwadq3k6n6nvv245yi3cxri2f8h")))

(define-public crate-grouping_by-0.1.3 (c (n "grouping_by") (v "0.1.3") (h "1fkmbayq7jlnxzmc62m2ppwl25g50p5b1grn5lv4a8d89qfr5zz4")))

(define-public crate-grouping_by-0.1.5 (c (n "grouping_by") (v "0.1.5") (h "000fjlj9krhqgfglv99krhnhsga0bqw794rvi1q4kcll1ax04xa4")))

(define-public crate-grouping_by-0.1.6 (c (n "grouping_by") (v "0.1.6") (h "0rmzqnr85mipycrz1crvrf7nl3g1xw1yc1261xf5ajv1jw3kywsa")))

(define-public crate-grouping_by-0.1.7 (c (n "grouping_by") (v "0.1.7") (h "133bsf2lslwfkygvir71yx5a216wdrdfx8wpcbl6wz5wygw5cql2")))

(define-public crate-grouping_by-0.2.0 (c (n "grouping_by") (v "0.2.0") (h "17gzc2krj0lv27ibcgjy435ggmlbak1v6fzxvdzl8aplnhjm8kag")))

(define-public crate-grouping_by-0.2.1 (c (n "grouping_by") (v "0.2.1") (h "0iijc09xrcsi3f5fgzb2x8hf4a300wh5rsjg8z2lj8x0n1bj6lb7")))

(define-public crate-grouping_by-0.2.2 (c (n "grouping_by") (v "0.2.2") (h "1abh51lvnmpf0gnd91xjh41xi535zas7zska5zfr2chi42wdplwa")))

