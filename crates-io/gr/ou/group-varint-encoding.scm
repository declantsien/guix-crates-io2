(define-module (crates-io gr ou group-varint-encoding) #:use-module (crates-io))

(define-public crate-group-varint-encoding-0.1.0 (c (n "group-varint-encoding") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "08ijh9gbdxnsp5pb3q7g6qx4ymaa0hh5hvbcdynkl5fzkb4bz1hz")))

(define-public crate-group-varint-encoding-0.1.1 (c (n "group-varint-encoding") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "160gqkizvqc49j86c3dpvb4xkkf88fcajsafc9zgqv63xhiyyc2j")))

(define-public crate-group-varint-encoding-0.1.2 (c (n "group-varint-encoding") (v "0.1.2") (d (list (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "1p4dsl6787y8s3gbmgp1gwaihbxz6cg105l3gwy1j22kjnrzch6q")))

