(define-module (crates-io gr ou group-runner) #:use-module (crates-io))

(define-public crate-group-runner-0.1.0 (c (n "group-runner") (v "0.1.0") (d (list (d (n "snapbox") (r "^0.4") (d #t) (k 2)))) (h "0l4f45gckl8ncsqff41x0fblqlvvgaj58dz88a1g62c7k71fa818")))

(define-public crate-group-runner-0.1.1 (c (n "group-runner") (v "0.1.1") (d (list (d (n "snapbox") (r "^0.4") (d #t) (k 2)))) (h "1akg6rnr4b5w41lnrq745c0bbpzxnyriar04czmpwr6sh53w8m4q")))

(define-public crate-group-runner-0.1.2 (c (n "group-runner") (v "0.1.2") (d (list (d (n "snapbox") (r "^0.5") (d #t) (k 2)))) (h "1ajq9b84vwvmxw6cgkbzskrpwfx4c2jiph7x62x2pq6qdg26wwyg")))

