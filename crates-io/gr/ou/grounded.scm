(define-module (crates-io gr ou grounded) #:use-module (crates-io))

(define-public crate-grounded-0.1.0 (c (n "grounded") (v "0.1.0") (h "006spvlcdaclbpfxs7g15h7pyk6wvia0jhnlxmxg4avqrggf45dj")))

(define-public crate-grounded-0.1.1 (c (n "grounded") (v "0.1.1") (h "0k995jrqri88khimirldxzkr2w48s03skrcrflkpdx4wlq9qfyxs")))

(define-public crate-grounded-0.2.0 (c (n "grounded") (v "0.2.0") (d (list (d (n "portable-atomic") (r "^1.3") (k 0)))) (h "10307dlsy4ramjgk1fva790rxbhxf0bj3mc7vmgpbfby5i084zci") (f (quote (("default") ("critical-section" "portable-atomic/critical-section") ("cas" "portable-atomic/require-cas"))))))

