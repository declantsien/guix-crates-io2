(define-module (crates-io gr ou grouper-lib) #:use-module (crates-io))

(define-public crate-grouper-lib-0.1.0 (c (n "grouper-lib") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksjlvq03kwp34gm5qhai5byidbcf7kxg2bi4ddbnhxcrmv29361")))

(define-public crate-grouper-lib-0.1.11 (c (n "grouper-lib") (v "0.1.11") (d (list (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 0)))) (h "18kpvcgpcs3w4piaq53mzwf0aav5ja1swlzljvh33pki9bwzxraw")))

