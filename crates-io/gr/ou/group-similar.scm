(define-module (crates-io gr ou group-similar) #:use-module (crates-io))

(define-public crate-group-similar-0.1.0 (c (n "group-similar") (v "0.1.0") (d (list (d (n "kodama") (r "^0.2.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "04q7a9nlg7l392891mwjwcfrcmsrp2rq6q1529mx0cd8kx0k95ls")))

(define-public crate-group-similar-0.2.2 (c (n "group-similar") (v "0.2.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "jaro_winkler") (r "^0.1.0") (d #t) (k 0)) (d (n "kodama") (r "^0.2.3") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "113fjnfm72yknlnycna4drhyqbfa40crj89fh1rqxw0dlxli29n5")))

