(define-module (crates-io gr ou groupable) #:use-module (crates-io))

(define-public crate-groupable-0.1.0 (c (n "groupable") (v "0.1.0") (h "0z3n6wlnkyn74i6fsn1pq8kbpi3kzbxa4rypinqm28c1kady4n99")))

(define-public crate-groupable-0.2.0 (c (n "groupable") (v "0.2.0") (h "0msfvg8xw4bqi7y7xdkm8nvjjllznc10dnzkx8wnjr5yp119jq9j")))

