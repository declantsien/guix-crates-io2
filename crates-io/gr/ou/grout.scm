(define-module (crates-io gr ou grout) #:use-module (crates-io))

(define-public crate-grout-0.1.0 (c (n "grout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1d29s24rfl2ynqsbq58ly8mb31m1mrpry9rgfl95x34m1xdvhicg")))

(define-public crate-grout-0.2.0 (c (n "grout") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1rvybbspv375r0vcrd73qqm2nks0hynffqk3idxdidphw3skx6bl")))

