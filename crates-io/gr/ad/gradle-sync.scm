(define-module (crates-io gr ad gradle-sync) #:use-module (crates-io))

(define-public crate-gradle-sync-0.1.0 (c (n "gradle-sync") (v "0.1.0") (d (list (d (n "regex") (r "^1.0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0pd2n8xnq6n09pk238zvwc54s2p13v1mgri9b14j1q49l4zn7rkv")))

(define-public crate-gradle-sync-0.1.1 (c (n "gradle-sync") (v "0.1.1") (d (list (d (n "regex") (r "^1.0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0p28fhn1cfd9396vx9wmnj3vr5ymjdlj2gsg389brv4210h5x7hc")))

(define-public crate-gradle-sync-0.1.2 (c (n "gradle-sync") (v "0.1.2") (d (list (d (n "regex") (r "^1.0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1rpcii6vqhs0p596ni1hfgxq6q6n27i0zrs3pvazm6f6q738ilai")))

(define-public crate-gradle-sync-0.1.4 (c (n "gradle-sync") (v "0.1.4") (d (list (d (n "regex") (r "^1.0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1zykbag3pk3j37iq1pnv2i5xaf61khl4bcqqxx4w4kq9b7vz1xqb")))

(define-public crate-gradle-sync-0.2.0 (c (n "gradle-sync") (v "0.2.0") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "11xjdgz5iy9fs7xphz2w901id7pdmdijbx8pipinird3vjj5xd99")))

