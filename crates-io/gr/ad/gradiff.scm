(define-module (crates-io gr ad gradiff) #:use-module (crates-io))

(define-public crate-gradiff-0.1.0-rc1 (c (n "gradiff") (v "0.1.0-rc1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1s2b55nzzb91dg9dmn20i3r69jn8dm96xrxhfdryzh3c2vd2hi9j") (y #t)))

(define-public crate-gradiff-0.1.0-rc2 (c (n "gradiff") (v "0.1.0-rc2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1r1599k96jj1y3lg65xprarpq9478kq68ia4p17533as59my4d4n") (y #t)))

(define-public crate-gradiff-0.1.0-rc3 (c (n "gradiff") (v "0.1.0-rc3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "153h3cwpznyhgv6q78ms6xz96p3fg5c114qpsh4hncpzmm5vsd4m") (y #t)))

(define-public crate-gradiff-0.1.0-rc4 (c (n "gradiff") (v "0.1.0-rc4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1n966w1h6mzvwvzlgq2p8qf2qg8v210hybx8kklmfyj5lhxk10i0") (y #t)))

(define-public crate-gradiff-0.1.0-rc5 (c (n "gradiff") (v "0.1.0-rc5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1h8rs2lbkwxirqw5gvl215prfk5lc9a7flf5ccvaz1xvi5ilfyr7") (y #t)))

(define-public crate-gradiff-0.1.0-rc6 (c (n "gradiff") (v "0.1.0-rc6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0hm05lbhln0vw31vlyvz9akf32i852p4kk45lh3qqhqjhcmxs13a") (y #t)))

(define-public crate-gradiff-0.1.0-rc7 (c (n "gradiff") (v "0.1.0-rc7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "13zpwa057111jl0jmx79dx4g60x9qlpmafqql1a1bq6p997s4qx5") (y #t)))

(define-public crate-gradiff-0.1.0-rc8 (c (n "gradiff") (v "0.1.0-rc8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1bp1ncjxph7jpqnjxk8ysary01xg7w6798zp40pzqqdcw6iv2lzc") (y #t)))

(define-public crate-gradiff-0.1.0-rc9 (c (n "gradiff") (v "0.1.0-rc9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0dxgi2jkyik08l0wi318slf732zwb272h2r7qzxkqr5qbmy6vamk") (y #t)))

