(define-module (crates-io gr ad gradecalculators) #:use-module (crates-io))

(define-public crate-gradecalculators-0.1.0 (c (n "gradecalculators") (v "0.1.0") (h "1w8f8c6rbj9syplpn22gjvcbzk753kdrl9h4nb3s2p5mq8pshiyl")))

(define-public crate-gradecalculators-0.1.1 (c (n "gradecalculators") (v "0.1.1") (h "1mhkyjirjqiny9fj7zv1v36h5qcrhw8q4g6wdk2nn1c16mfmi1jl")))

(define-public crate-gradecalculators-0.1.2 (c (n "gradecalculators") (v "0.1.2") (h "0s0vs4p9xr50h0i53lkjriavsklr8mslyj4bwklzjs8ybhrmdjv3")))

