(define-module (crates-io gr ad grade) #:use-module (crates-io))

(define-public crate-grade-0.1.0 (c (n "grade") (v "0.1.0") (d (list (d (n "gio") (r "^0.7.0") (d #t) (k 2)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_22"))) (d #t) (k 2)) (d (n "pango") (r "^0.7.0") (d #t) (k 2)))) (h "1pkvpqdmxgq5kls1zcg631i4avcfx0mzcalkbxdzia7p1siv6nr7")))

(define-public crate-grade-0.1.1 (c (n "grade") (v "0.1.1") (d (list (d (n "gio") (r "^0.7.0") (d #t) (k 2)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_22"))) (d #t) (k 2)) (d (n "pango") (r "^0.7.0") (d #t) (k 2)))) (h "00c1av0f9v64675wi8pmz9aka316kx96sqfbssqgfdb1hr7yfqdk")))

(define-public crate-grade-0.2.0 (c (n "grade") (v "0.2.0") (d (list (d (n "gio") (r "^0.7.0") (d #t) (k 2)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_22"))) (d #t) (k 2)) (d (n "pango") (r "^0.7.0") (d #t) (k 2)))) (h "1jaarv550rxwddvms14p1zq34afysdhdg5g9hsvyna34w5bd9x18")))

(define-public crate-grade-0.3.0 (c (n "grade") (v "0.3.0") (d (list (d (n "gio") (r "^0.7.0") (d #t) (k 2)) (d (n "gtk") (r "^0.7.0") (f (quote ("v3_22"))) (d #t) (k 2)) (d (n "pango") (r "^0.7.0") (d #t) (k 2)))) (h "1917i7cchdvvmdcpk5mbp9wis6sghmdlvcfp6yrgm7nkvjyqw0g0")))

