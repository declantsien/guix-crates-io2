(define-module (crates-io gr ad grade_calculator) #:use-module (crates-io))

(define-public crate-grade_calculator-1.0.0 (c (n "grade_calculator") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "0j7q64hb4n82pi0r2lln3bl4naf2w6cs82hh5arn86iw80nj3q44")))

(define-public crate-grade_calculator-1.0.1 (c (n "grade_calculator") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "1jifb7drly58jcn8jvgmh8zf54f6ndmrlmzccg8rcwn931b4w1bw")))

(define-public crate-grade_calculator-1.1.0 (c (n "grade_calculator") (v "1.1.0") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)))) (h "0jpb7gnw2fdk6h8vfrz7hiqac60bvimw5lwwhpfb7cdjf3iyld22")))

(define-public crate-grade_calculator-1.1.1 (c (n "grade_calculator") (v "1.1.1") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)))) (h "0vg5af7sain6y8qf0f1mxfnk4mkh2nzwii95yyly74xwbd1d3a29")))

(define-public crate-grade_calculator-1.1.2 (c (n "grade_calculator") (v "1.1.2") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "cursive-aligned-view") (r "^0.6.0") (d #t) (k 0)))) (h "1mf94iyc7hiql3xhfdxjf23yjc9wmrzggia0g6j0wkg658yj44j0")))

