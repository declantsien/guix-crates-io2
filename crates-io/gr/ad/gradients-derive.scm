(define-module (crates-io gr ad gradients-derive) #:use-module (crates-io))

(define-public crate-gradients-derive-0.1.0 (c (n "gradients-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "111mhfsw8lknkzwh7qh6g2qicbi0lwljf4glsa0nfhvmsg3pagrp") (y #t)))

(define-public crate-gradients-derive-0.1.1 (c (n "gradients-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14pxl8lbh7pl4v27c8lgvhjh8v25qy8cqi8b4mdc5ls5hmcv0l6p")))

(define-public crate-gradients-derive-0.2.0 (c (n "gradients-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13kinbf7ymv3wf6d5dj93z4mvjilh90niljb60vsx3qvaj3a3phv")))

(define-public crate-gradients-derive-0.3.0 (c (n "gradients-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "163wy9s3d4xbsqlz05jq69gcf49mb6q45q01av3rpl1qxyry7fg9")))

(define-public crate-gradients-derive-0.3.3 (c (n "gradients-derive") (v "0.3.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19jl1acn9g6nf03nvgry4x2fr3m458cbwpk1n71p6rvlckx4ywjm")))

(define-public crate-gradients-derive-0.3.4 (c (n "gradients-derive") (v "0.3.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ava3z8hzvpp8y43qcbc4plzcv0ibmlpy7l6723hqyd534j1dgay")))

