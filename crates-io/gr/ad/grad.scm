(define-module (crates-io gr ad grad) #:use-module (crates-io))

(define-public crate-grad-0.1.0 (c (n "grad") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "warp") (r "^0.1") (d #t) (k 0)))) (h "0if4xgfncs8qk7k2jvj2438w8f9hif7zn9z72kh98kk29a0z1y1c")))

