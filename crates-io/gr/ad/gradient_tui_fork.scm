(define-module (crates-io gr ad gradient_tui_fork) #:use-module (crates-io))

(define-public crate-gradient_tui_fork-0.19.0 (c (n "gradient_tui_fork") (v "0.19.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cassowary") (r "^0.3") (d #t) (k 0)) (d (n "colorgrad") (r "^0.6.2") (d #t) (k 2)) (d (n "crossterm") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0kl7a0c8qpr6wyc9hi6pk677xwfn0rlnd6xpyki6fa8541cppdq0") (f (quote (("default" "crossterm")))) (r "1.56.1")))

