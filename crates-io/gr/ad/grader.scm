(define-module (crates-io gr ad grader) #:use-module (crates-io))

(define-public crate-grader-0.1.0 (c (n "grader") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "04s72n50k22n9hv3jbrqbb3xsafdxb71mzcch6lfdrv37wp5h1w5")))

(define-public crate-grader-0.2.0 (c (n "grader") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "05rflc92ib0blc233yn1ym0190aym3vgzkx8blhsl3gww0msk4w8")))

