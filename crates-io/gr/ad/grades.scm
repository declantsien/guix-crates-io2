(define-module (crates-io gr ad grades) #:use-module (crates-io))

(define-public crate-grades-0.1.0 (c (n "grades") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fraction") (r "^0.13.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "0s7bsmp2miy5cxjq07z9zpn98m8wh4s81szyxbwb7ybjvpa1m4ms") (y #t)))

(define-public crate-grades-0.1.1 (c (n "grades") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "1qs0m445w88sx9hsi359xk6xrx83lp7v1ilx3w4msm6x290x1f8w") (y #t)))

(define-public crate-grades-0.2.0 (c (n "grades") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fraction") (r "^0.13.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "xdg") (r "^2.5.0") (d #t) (k 0)))) (h "0m6by4kk19316y1b93bhxpjk3kcgd3vakvg778hj3p17mg7js17r")))

