(define-module (crates-io gr iz grizzly_scheduler) #:use-module (crates-io))

(define-public crate-grizzly_scheduler-0.1.0 (c (n "grizzly_scheduler") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "cron") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "11kkhl00gk8mlcnyzj8xsci0338gkw9lvffrr2xv0d639f949pph")))

