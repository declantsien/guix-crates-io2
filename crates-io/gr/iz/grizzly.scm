(define-module (crates-io gr iz grizzly) #:use-module (crates-io))

(define-public crate-grizzly-0.0.2 (c (n "grizzly") (v "0.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.24.0") (d #t) (k 0)))) (h "05f8vqks0yxxg4qdkn75bk2igxrjmhx3662p18lqn99vj00gl1zl")))

(define-public crate-grizzly-0.0.3 (c (n "grizzly") (v "0.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "naivebayes") (r "^0.1.2") (d #t) (k 0)) (d (n "sqlite") (r "^0.24.0") (d #t) (k 0)))) (h "1r4ydh70q2s1gr9vy89l9y118zm1vixk767713j4a3xikxjjiy32")))

(define-public crate-grizzly-0.0.4 (c (n "grizzly") (v "0.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "naivebayes") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "sqlite") (r "^0.24.0") (d #t) (k 0)))) (h "18f3b3gb5r212a23div8qjcsw2q4xs1lg5bn4kmnn1fdjwcl4jwl")))

(define-public crate-grizzly-0.0.5 (c (n "grizzly") (v "0.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "naivebayes") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "sqlite") (r "^0.24.0") (d #t) (k 0)))) (h "12ph2migwzkm629m4gg49mfx1mib4l8cbfh3b1mvqaagllcq2w0r")))

