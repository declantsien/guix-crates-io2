(define-module (crates-io gr on gronr) #:use-module (crates-io))

(define-public crate-gronr-0.1.0 (c (n "gronr") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "18zzg5q9wry5hsjffnszwxfl51sm52yypm7pa0knfrs0hz0344ki")))

