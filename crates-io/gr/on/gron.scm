(define-module (crates-io gr on gron) #:use-module (crates-io))

(define-public crate-gron-0.1.0 (c (n "gron") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1r0zq9ln2sig1rzl2zi0i974vlyhdv0s3zp4l4yaa2cj16p99gn5") (f (quote (("log_errors"))))))

(define-public crate-gron-0.2.0 (c (n "gron") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)))) (h "1ji90gn907kq5nl3x56gdvvcf65x33sfh3wzqc5jw2fl57n345dq") (f (quote (("log_errors"))))))

(define-public crate-gron-0.3.0 (c (n "gron") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0x3xim3fjpvvkif9q6c7x1v2lgkvaxyyx2yk84p32pacyb7dp4xn") (f (quote (("log_errors"))))))

(define-public crate-gron-0.4.0 (c (n "gron") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0yf41f5d1igl4mjfg2iv9pyh16i3p3cpcx767f3lbybnbbv74vlr") (f (quote (("log_errors"))))))

