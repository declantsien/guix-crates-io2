(define-module (crates-io gr mr grmr) #:use-module (crates-io))

(define-public crate-grmr-0.1.0 (c (n "grmr") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1nc9yn1pvnn770hb0ajj47a1dcbafizyym49znsbbsr827nr0078")))

(define-public crate-grmr-0.2.0 (c (n "grmr") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "0yvjndnk6yk7jqqxjn3ny51gba0mhjxszr625cr95g2qkr4316rn")))

