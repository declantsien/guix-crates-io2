(define-module (crates-io gr ee greenwasm-binary-format) #:use-module (crates-io))

(define-public crate-greenwasm-binary-format-0.2.0 (c (n "greenwasm-binary-format") (v "0.2.0") (d (list (d (n "greenwasm-structure") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "06sfq7bapj2q1fwmal3i1az9fv3m85921mp3gggzr7vdqrai0v3m")))

(define-public crate-greenwasm-binary-format-0.2.1 (c (n "greenwasm-binary-format") (v "0.2.1") (d (list (d (n "greenwasm-structure") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "15bb9c2rqz35y4r3a3r1vxy8b4jhk51kziya4jnw62c83k2fpc3f")))

(define-public crate-greenwasm-binary-format-0.3.0 (c (n "greenwasm-binary-format") (v "0.3.0") (d (list (d (n "greenwasm-structure") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0vzyj1xmzpw9cl35kfwqpslnn73fjks166v8jwy4bacm46iw9v7z")))

