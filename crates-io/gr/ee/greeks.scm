(define-module (crates-io gr ee greeks) #:use-module (crates-io))

(define-public crate-greeks-0.2.0 (c (n "greeks") (v "0.2.0") (h "1qjhhb71lwy6whl54m2klr5wz1i4qzxi3a9miidnwzq3dds6ljnl")))

(define-public crate-greeks-0.3.0 (c (n "greeks") (v "0.3.0") (h "1nwqfbcl964cnjjpjcwkc3bfswn1z2d0jrfhnbz8haz4h16x4b3d")))

(define-public crate-greeks-0.4.0 (c (n "greeks") (v "0.4.0") (h "1q8agk2bf5whq1b4ivkpf8kzaliv86pigg7b65nsci6sfdx7253g") (f (quote (("unstable"))))))

(define-public crate-greeks-0.5.0 (c (n "greeks") (v "0.5.0") (h "03wg7xmy609srnn746ddyl27i6lzi8fi99v63pc866bq4606755f") (f (quote (("unstable"))))))

