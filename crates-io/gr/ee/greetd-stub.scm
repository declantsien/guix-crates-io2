(define-module (crates-io gr ee greetd-stub) #:use-module (crates-io))

(define-public crate-greetd-stub-0.1.0 (c (n "greetd-stub") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "greetd_ipc") (r "^0.10.0") (f (quote ("tokio-codec"))) (d #t) (k 0)) (d (n "libfprint-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "net" "rt"))) (d #t) (k 0)))) (h "0747k2l7ikbn8xgmsm37fhnxa3622j924xkrkav65ff4ywvmrdjs")))

(define-public crate-greetd-stub-0.2.0 (c (n "greetd-stub") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "greetd_ipc") (r "^0.10.0") (f (quote ("tokio-codec"))) (d #t) (k 0)) (d (n "libfprint-rs") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "net" "rt"))) (d #t) (k 0)))) (h "0a4w8q8qp228agg7qsw1j6n9ayzpidfbzm4134hik5ajnj8wg83f") (f (quote (("fingerprint" "libfprint-rs"))))))

(define-public crate-greetd-stub-0.3.0 (c (n "greetd-stub") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "greetd_ipc") (r "^0.10.0") (f (quote ("tokio-codec"))) (d #t) (k 0)) (d (n "libfprint-rs") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "net" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0s1c3mhjm52ygdmiz2c5gqjv3vc3wg2hd8k6909nlgxr108b64sq") (f (quote (("fingerprint" "libfprint-rs"))))))

