(define-module (crates-io gr ee greentask) #:use-module (crates-io))

(define-public crate-greentask-0.1.0 (c (n "greentask") (v "0.1.0") (d (list (d (n "coroutine") (r "^0.6.0") (d #t) (k 0)))) (h "1r5j282lblvxc93xlvjj9p537w5siamypm64c420yimjd9pb4ls3")))

(define-public crate-greentask-0.2.0 (c (n "greentask") (v "0.2.0") (d (list (d (n "coroutine") (r "^0.8.0") (d #t) (k 0)))) (h "0ch49rf0kgpc0ipsdjqzgr27zw06h6a9bp768r4qff2ylyfbgqly")))

