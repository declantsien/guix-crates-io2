(define-module (crates-io gr ee greenie) #:use-module (crates-io))

(define-public crate-greenie-0.1.1 (c (n "greenie") (v "0.1.1") (d (list (d (n "greenie-proc") (r "^0.1") (d #t) (k 0)))) (h "1if3yqqkg743awprslgc9ab5wy1d3sb0scd2c0xmswxw54lmqcqv")))

(define-public crate-greenie-0.1.2 (c (n "greenie") (v "0.1.2") (d (list (d (n "greenie-proc") (r "^0.1") (d #t) (k 0)))) (h "0swrqp81dagidmi4xq7sn4f7qdljqs7hpk6ai7vn3ln2srj8s4z1")))

(define-public crate-greenie-0.2.0 (c (n "greenie") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "greenie-proc") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "13dvnc0smswsgwkh0ys2jdk3azapd3i8ws69qv9vjmqkl2w1c4kl") (f (quote (("preemptive") ("fifo"))))))

(define-public crate-greenie-0.3.0 (c (n "greenie") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "greenie-proc") (r "^0.2") (d #t) (k 0)) (d (n "hjul") (r "^0.2") (d #t) (k 0)) (d (n "intrusive-collections") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1q2nkb766x6ff3azrnj97mrl4z4cvysabbr69jcf9d3i3qrm97sp")))

(define-public crate-greenie-0.4.0 (c (n "greenie") (v "0.4.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "greenie-proc") (r "^0.2") (d #t) (k 0)) (d (n "hjul") (r "^0.2") (d #t) (k 0)) (d (n "intrusive-collections") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0sswqx3g8b1qy96n23il5qrap7my9dan736i3rzmkypmbnh2bg70")))

