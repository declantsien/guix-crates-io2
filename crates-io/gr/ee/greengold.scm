(define-module (crates-io gr ee greengold) #:use-module (crates-io))

(define-public crate-greengold-0.1.0 (c (n "greengold") (v "0.1.0") (h "173jg2hsz1rj96g4w54z3lnmm8kgsf8aa105i1dkiq4xsv4c4k24")))

(define-public crate-greengold-0.2.0 (c (n "greengold") (v "0.2.0") (h "1fa0zwvy7hcq088ccfbnfv6lg1v6cgcb41i65smj98yk2g7jixiy")))

(define-public crate-greengold-0.2.1 (c (n "greengold") (v "0.2.1") (h "01yzdayx2j5hdgb5z1g4aby755d0lyxvd7qnml64n00w6p4pysz1")))

