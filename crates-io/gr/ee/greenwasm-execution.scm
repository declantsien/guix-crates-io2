(define-module (crates-io gr ee greenwasm-execution) #:use-module (crates-io))

(define-public crate-greenwasm-execution-0.2.1 (c (n "greenwasm-execution") (v "0.2.1") (d (list (d (n "frounding") (r "^0.0.4") (d #t) (k 0)) (d (n "greenwasm-structure") (r "^0.2.0") (d #t) (k 0)) (d (n "greenwasm-validation") (r "^0.2.1") (d #t) (k 0)))) (h "0gspkkgnmhd70dr52xhwrrhljvsm8qzax10clhj2vhk46mm2ybff")))

(define-public crate-greenwasm-execution-0.3.0 (c (n "greenwasm-execution") (v "0.3.0") (d (list (d (n "frounding") (r "^0.0.4") (d #t) (k 0)) (d (n "greenwasm-structure") (r "^0.3.0") (d #t) (k 0)) (d (n "greenwasm-validation") (r "^0.3.0") (d #t) (k 0)))) (h "00ahf856ib4cj5kvspylaa3licp4gb0dwpjhb3kbssgnmmx79bhi")))

