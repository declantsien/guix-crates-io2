(define-module (crates-io gr ee greenglas) #:use-module (crates-io))

(define-public crate-greenglas-0.2.0 (c (n "greenglas") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.54") (o #t) (d #t) (k 0)) (d (n "coaster") (r "^0.1.0") (f (quote ("native"))) (k 0)) (d (n "image") (r "^0.14.0") (d #t) (k 0)) (d (n "modifier") (r "^0.1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)))) (h "08hiva8sydnswkgjai69a0v86w7r5infhvi7qfhhh7jjivlj283n") (f (quote (("travis") ("opencl" "coaster/opencl") ("native") ("lint" "clippy") ("dev") ("default" "native") ("cuda" "coaster/cuda"))))))

(define-public crate-greenglas-0.3.0 (c (n "greenglas") (v "0.3.0") (d (list (d (n "coaster") (r "^0.2") (f (quote ("native"))) (k 0)) (d (n "image") (r "=0.23.12") (d #t) (k 0)) (d (n "modifier") (r "^0.1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)))) (h "00j3v84j8ndhf8fms66iznrfcki5kf2121nhjhm0i97zsh8rq61y") (f (quote (("opencl" "coaster/opencl") ("native") ("default" "native" "cuda") ("cuda" "coaster/cuda"))))))

