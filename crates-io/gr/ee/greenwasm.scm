(define-module (crates-io gr ee greenwasm) #:use-module (crates-io))

(define-public crate-greenwasm-0.2.0 (c (n "greenwasm") (v "0.2.0") (d (list (d (n "binaryen") (r "^0.4.0") (d #t) (k 0)) (d (n "greenwasm-binary-format") (r "^0.2.1") (d #t) (k 0)) (d (n "greenwasm-execution") (r "^0.2.1") (d #t) (k 0)) (d (n "greenwasm-spectest") (r "^0.2.0") (d #t) (k 2)) (d (n "greenwasm-structure") (r "^0.2.0") (d #t) (k 0)) (d (n "greenwasm-validation") (r "^0.2.1") (d #t) (k 0)))) (h "1riwslva8f3sdprk51kil27gzrfcg1whwxbj1iaxdiiyjr6xhcs5")))

(define-public crate-greenwasm-0.2.1 (c (n "greenwasm") (v "0.2.1") (d (list (d (n "binaryen") (r "^0.4.0") (d #t) (k 0)) (d (n "greenwasm-binary-format") (r "^0.2.1") (d #t) (k 0)) (d (n "greenwasm-execution") (r "^0.2.1") (d #t) (k 0)) (d (n "greenwasm-spectest") (r "^0.3.0") (d #t) (k 2)) (d (n "greenwasm-structure") (r "^0.2.0") (d #t) (k 0)) (d (n "greenwasm-validation") (r "^0.2.1") (d #t) (k 0)))) (h "19hhkrncg5gkgpl92qq85hkqi294cymi414lzhiccfd3yipzlfng")))

(define-public crate-greenwasm-0.3.0 (c (n "greenwasm") (v "0.3.0") (d (list (d (n "binaryen") (r "^0.4.0") (d #t) (k 0)) (d (n "greenwasm-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "greenwasm-execution") (r "^0.3.0") (d #t) (k 0)) (d (n "greenwasm-spectest") (r "^0.3.0") (d #t) (k 2)) (d (n "greenwasm-structure") (r "^0.3.0") (d #t) (k 0)) (d (n "greenwasm-validation") (r "^0.3.0") (d #t) (k 0)))) (h "0713ax3lscacqawjqkmcplj11c3zkg3cbrcmd5lidw1rsbzja7hl")))

(define-public crate-greenwasm-0.3.1 (c (n "greenwasm") (v "0.3.1") (d (list (d (n "binaryen") (r "^0.4.0") (d #t) (k 0)) (d (n "greenwasm-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "greenwasm-execution") (r "^0.3.0") (d #t) (k 0)) (d (n "greenwasm-spectest") (r "^0.3.1") (d #t) (k 2)) (d (n "greenwasm-structure") (r "^0.3.0") (d #t) (k 0)) (d (n "greenwasm-validation") (r "^0.3.0") (d #t) (k 0)))) (h "0iwp6zfbhfqi5mlzm2qid0bamb6l7cf7mafxf30f8yszr5anmpav")))

