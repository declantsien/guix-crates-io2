(define-module (crates-io gr ee greenthread-future) #:use-module (crates-io))

(define-public crate-greenthread-future-0.1.0 (c (n "greenthread-future") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1abxwzzhr8nxxrwyjp7g9148j4m3ncnqdfviq1gi395yxi4100vi")))

