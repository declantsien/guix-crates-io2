(define-module (crates-io gr ee greenback) #:use-module (crates-io))

(define-public crate-greenback-0.0.1 (c (n "greenback") (v "0.0.1") (h "012x9s0pr8cgvp7pfvb5idc7biqkf6a2zzwvwvxb9a65h7k0jqmd")))

(define-public crate-greenback-0.0.2 (c (n "greenback") (v "0.0.2") (h "1i7sapnx21n3xdigxglsgd9x3mmyb7mv3zbrq5hhqn6yl4h4wxh3")))

(define-public crate-greenback-0.0.3 (c (n "greenback") (v "0.0.3") (h "158gdgbrs7f49pvgxpsph8wh9yvfj2254lzkhxp750494lj6qllb")))

