(define-module (crates-io gr ee greenwasm-spectest) #:use-module (crates-io))

(define-public crate-greenwasm-spectest-0.2.0 (c (n "greenwasm-spectest") (v "0.2.0") (d (list (d (n "wabt") (r "^0.5.0") (d #t) (k 0)))) (h "18b7wz3kgk7hprjshlhp52z41k8ra1r72wf3k2r625syv4757ak8")))

(define-public crate-greenwasm-spectest-0.3.0 (c (n "greenwasm-spectest") (v "0.3.0") (d (list (d (n "wabt") (r "^0.5.0") (d #t) (k 0)))) (h "0gd73pdl9084g09ch01jvrpnpjb3rk41c9ifwijydjq3vbqsdz22")))

(define-public crate-greenwasm-spectest-0.3.1 (c (n "greenwasm-spectest") (v "0.3.1") (d (list (d (n "wabt") (r "^0.5.0") (d #t) (k 0)))) (h "18azsagwx8v6llb55q1jf0pm2gh5zyhs6fiwlhkfhisgddwmx6l0")))

(define-public crate-greenwasm-spectest-0.3.2 (c (n "greenwasm-spectest") (v "0.3.2") (d (list (d (n "wabt") (r "^0.5.0") (d #t) (k 0)))) (h "0y3zwi9lh913mmlwgvzdv2sg831pqrsa9g04bcnjwgs47rmhaaz4")))

(define-public crate-greenwasm-spectest-0.3.3 (c (n "greenwasm-spectest") (v "0.3.3") (d (list (d (n "wabt") (r "^0.5.0") (d #t) (k 0)))) (h "16w4rr74mkhk10x1hsgiag8iangpzbndb37qbnda5823zwscc164")))

