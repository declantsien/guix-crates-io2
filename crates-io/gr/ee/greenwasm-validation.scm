(define-module (crates-io gr ee greenwasm-validation) #:use-module (crates-io))

(define-public crate-greenwasm-validation-0.2.0 (c (n "greenwasm-validation") (v "0.2.0") (d (list (d (n "greenwasm-structure") (r "^0.2.0") (d #t) (k 0)))) (h "1bb6xabhkid6pbp5swvd5z8659mha73prmmqj3aycpm8ky20x8fd")))

(define-public crate-greenwasm-validation-0.2.1 (c (n "greenwasm-validation") (v "0.2.1") (d (list (d (n "greenwasm-structure") (r "^0.2.0") (d #t) (k 0)))) (h "016jkclvfqjjfwxm64n3d6xylh5wp2jqgkyil07iyqi6j63xh7px")))

(define-public crate-greenwasm-validation-0.3.0 (c (n "greenwasm-validation") (v "0.3.0") (d (list (d (n "greenwasm-structure") (r "^0.3.0") (d #t) (k 0)))) (h "1shk9zi55gbwb15x6vjxi5wwm04r47lczrs637078hfbd7xfl4z7")))

