(define-module (crates-io gr ee greenriver) #:use-module (crates-io))

(define-public crate-greenriver-0.1.0 (c (n "greenriver") (v "0.1.0") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "greenfield") (r "^0.1.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (f (quote ("color"))) (d #t) (k 0)))) (h "0y7h5qxsjxf1m7qmppddrprsq2lp8r43ysw0h1q13g4naqx83ana")))

(define-public crate-greenriver-0.1.1 (c (n "greenriver") (v "0.1.1") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "greenfield") (r "^0.1.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (f (quote ("color"))) (d #t) (k 0)))) (h "18p91nwrqpv8r2x4x36mhxm6l307sr5w9318a6a9jj9hzgdcfxfl")))

(define-public crate-greenriver-0.1.2 (c (n "greenriver") (v "0.1.2") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "greenfield") (r "^0.1.3") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (f (quote ("color"))) (d #t) (k 0)))) (h "1qxyg514hp01hz6yhxbbbdf09nlk1ajz03bnz4fa1x9n0va0mbw5")))

(define-public crate-greenriver-0.1.3 (c (n "greenriver") (v "0.1.3") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "greenfield") (r "^0.1.4") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (f (quote ("color"))) (d #t) (k 0)))) (h "12d14dhnc2nq9yjr6hv7m5rbw8hhacv7qnpav69f5vg43x008pns")))

