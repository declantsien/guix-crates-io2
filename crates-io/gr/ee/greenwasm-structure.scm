(define-module (crates-io gr ee greenwasm-structure) #:use-module (crates-io))

(define-public crate-greenwasm-structure-0.2.0 (c (n "greenwasm-structure") (v "0.2.0") (d (list (d (n "smallvec") (r "^0.6.4") (f (quote ("union"))) (d #t) (k 0)))) (h "1r0sdkg1wk8dgl0a2p4liplmfl3xr6pcv3q0wxfimgsrrrc2jgm6")))

(define-public crate-greenwasm-structure-0.3.0 (c (n "greenwasm-structure") (v "0.3.0") (d (list (d (n "smallvec") (r "^0.6.4") (f (quote ("union"))) (d #t) (k 0)))) (h "0av5qfvh32rgwr0mp2ydm5x3skvyk00jvp3qcag9fbvc48c9ybjd")))

