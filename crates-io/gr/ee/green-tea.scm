(define-module (crates-io gr ee green-tea) #:use-module (crates-io))

(define-public crate-green-tea-0.1.0 (c (n "green-tea") (v "0.1.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)))) (h "07r6b6czgi9cp4wfxl82gg25njwyhz6r96qc13cflnaxaffw30hb")))

(define-public crate-green-tea-0.1.1 (c (n "green-tea") (v "0.1.1") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)))) (h "1xhryjhydnr64jvfsgms0li5pc0a0wx43a51vrglv1fg54rnaka5")))

(define-public crate-green-tea-0.1.2 (c (n "green-tea") (v "0.1.2") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)))) (h "1cj2ni9c3qqpmrghy6lcy563hknqsd443vlavsg2vi924x359gcq")))

