(define-module (crates-io gr ee greenie-proc) #:use-module (crates-io))

(define-public crate-greenie-proc-0.1.0 (c (n "greenie-proc") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0l1qbr119sayc7yj9k5al8mnxjxz08xp0mx6pqi3171nf9l24asw")))

(define-public crate-greenie-proc-0.2.0 (c (n "greenie-proc") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1y4zkw82l7xzq3clakxmin6ml3w92jqbzzlvsydjp1551nb6ylg6")))

