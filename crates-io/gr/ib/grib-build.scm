(define-module (crates-io gr ib grib-build) #:use-module (crates-io))

(define-public crate-grib-build-0.1.0 (c (n "grib-build") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.18") (d #t) (k 0)))) (h "0zj3jbmj1vqxj5spmkn00jh5vrvjwjkzi3xqvlsh7p6g3alnhpcn")))

(define-public crate-grib-build-0.2.0 (c (n "grib-build") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qsml7w40v0skkhy0ddnmdh36pgiw0w504q226frqs6ynm3wysqq")))

(define-public crate-grib-build-0.3.0 (c (n "grib-build") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i91hlpbfnjnqjidzrk0mh1a67dycw929izp0dizkr78lip15zy6")))

(define-public crate-grib-build-0.4.0 (c (n "grib-build") (v "0.4.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0312a99bakhdsw9bw8hghb5h5h7wsxav0rjk9rdjbplhn5px6h4b")))

(define-public crate-grib-build-0.4.1 (c (n "grib-build") (v "0.4.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b8484m1nmcp8r7li95fcjx9hrwhpdghmfw68w7jk0i4sg4k3zy8")))

(define-public crate-grib-build-0.4.2 (c (n "grib-build") (v "0.4.2") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hb542ylcrc4308rc3smw9m24swcczz3ih2jrnb5pnniyvw1a65s")))

(define-public crate-grib-build-0.4.3 (c (n "grib-build") (v "0.4.3") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "194mkifr43g6znrpd0mx7jc6fmamgsfp0zkgy99dnfacnn1mn2n8")))

