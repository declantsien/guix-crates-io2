(define-module (crates-io gr ib grib1_reader) #:use-module (crates-io))

(define-public crate-grib1_reader-0.1.0 (c (n "grib1_reader") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "fs" "test-util" "io-util"))) (d #t) (k 0)))) (h "0y1qlq7cdhiz45vr7acx2cda7v1idxkmhkl5mmc795zfba7g4xzm")))

(define-public crate-grib1_reader-0.2.0 (c (n "grib1_reader") (v "0.2.0") (d (list (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "fs" "test-util" "io-util"))) (d #t) (k 0)))) (h "1qmqw108axdrfahycjmrcxljmr2fxj2aka77yg2gx40pypcyn2as") (s 2) (e (quote (("json" "dep:serde"))))))

(define-public crate-grib1_reader-0.2.1 (c (n "grib1_reader") (v "0.2.1") (d (list (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "fs" "test-util" "io-util"))) (d #t) (k 0)))) (h "012x2lgvivgm7whpbp1xyrd2cdcwpyjzvqsmhyvqb775ch7v2llc") (s 2) (e (quote (("json" "dep:serde"))))))

