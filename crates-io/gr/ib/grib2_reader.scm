(define-module (crates-io gr ib grib2_reader) #:use-module (crates-io))

(define-public crate-grib2_reader-0.1.0 (c (n "grib2_reader") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "fs" "test-util" "io-util"))) (o #t) (d #t) (k 0)))) (h "03r2m9l30wnksbcysic81sr7r9h645qn9kw3dxg9prsfa7klfdq8") (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-grib2_reader-0.1.1 (c (n "grib2_reader") (v "0.1.1") (d (list (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "fs" "test-util" "io-util"))) (o #t) (d #t) (k 0)))) (h "11rpbix5f5r8rgbn87p0j23iwm8qglv64i253hblj9mg34s3xsx5") (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-grib2_reader-0.1.2 (c (n "grib2_reader") (v "0.1.2") (d (list (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("macros" "fs" "test-util" "io-util"))) (o #t) (d #t) (k 0)))) (h "0lr304pc36k4ribl3wxd011mwr3czdv8i68qwfznan5jzwkim36p") (s 2) (e (quote (("async" "dep:tokio"))))))

