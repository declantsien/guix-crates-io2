(define-module (crates-io gr ib griblib_rust) #:use-module (crates-io))

(define-public crate-griblib_rust-0.1.0 (c (n "griblib_rust") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "libloader") (r "^0.1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1rrrpa2xm4l8xgr34yrqdl20b51nphs9x3ifv5s9fh34a1c6nfxd") (y #t)))

(define-public crate-griblib_rust-0.1.1 (c (n "griblib_rust") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "libloader") (r "^0.1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "163x3k5mz9mdf93df0xza7yi8a4i0s3pqai1sm8d978j08mcqkyb")))

