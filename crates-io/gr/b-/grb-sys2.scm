(define-module (crates-io gr b- grb-sys2) #:use-module (crates-io))

(define-public crate-grb-sys2-0.1.0 (c (n "grb-sys2") (v "0.1.0") (h "0apy38phsx92nv1hkrm5iy3h06il7y7v0pybkshw26wmnywbr5fr")))

(define-public crate-grb-sys2-0.1.1 (c (n "grb-sys2") (v "0.1.1") (h "0i332w3qvb7mml0cgcw7cp23f8c8f5cnzmlzbhqh2q64d4x7m0g8")))

(define-public crate-grb-sys2-0.2.0 (c (n "grb-sys2") (v "0.2.0") (h "06d9j723fbw24pny46hvw9has9ijf78glnz7dfilzh5anay2g1lr")))

(define-public crate-grb-sys2-9.5.0 (c (n "grb-sys2") (v "9.5.0") (h "004l0m4b9dqy4v82196l4imq16lvs4k37yg571780rim901y8chk") (l "gurobi95")))

(define-public crate-grb-sys2-10.0.0 (c (n "grb-sys2") (v "10.0.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 1)))) (h "1d5cypmk7axf9srg14h8zmb4pnsp47pgzyaqjgsdl06d9mkv840l") (l "gurobi")))

