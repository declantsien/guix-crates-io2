(define-module (crates-io gr b- grb-macro) #:use-module (crates-io))

(define-public crate-grb-macro-0.1.0 (c (n "grb-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing" "derive" "proc-macro" "clone-impls"))) (k 0)))) (h "1l5habny21xpl8l8nccwny2qrap9xyh1kx0f53ra683bsf31w1vh")))

