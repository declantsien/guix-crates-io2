(define-module (crates-io gr b- grb-sys) #:use-module (crates-io))

(define-public crate-grb-sys-0.1.0 (c (n "grb-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "17s169dyfym19hy4f0iz6w7m3i67lgv0pd6pfns4qqxb5dpa17n3")))

(define-public crate-grb-sys-0.1.1 (c (n "grb-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "1iwqkm7nmhxcdss9gyhhdar04ywmms1hmkdm43wfa1n2g68b6v07")))

(define-public crate-grb-sys-0.1.2 (c (n "grb-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "01kvmnvmzgr62kd9ds3mvy09qw7bp165g1fph7gwq66p0cgmnspq")))

(define-public crate-grb-sys-0.1.3 (c (n "grb-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "1245b1889qg6byjs86p6ld3xasc9q0krzw0jrs0zzg9dr05646kh")))

(define-public crate-grb-sys-0.1.4 (c (n "grb-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "14kffh0wsk6xzy8gxmdq8w3vnzh5s5pazpp0pdy58fln866k54ll")))

(define-public crate-grb-sys-0.1.5 (c (n "grb-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "151vskclf8hlr409z3ly0jcncrj0ghwmd3m5qblh50mrdja9hb0p")))

(define-public crate-grb-sys-0.1.6 (c (n "grb-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "0y3w7z8gjynjwkiq1y1vynax03gzmzyzn24i1vig62bd6n8bk5zw")))

(define-public crate-grb-sys-0.1.7 (c (n "grb-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "1ljbm79sh9lkvi0sn71k4n1k31jni3nyw85cfr7f5j3vsa6w5jin")))

