(define-module (crates-io gr ux grux) #:use-module (crates-io))

(define-public crate-grux-0.1.0 (c (n "grux") (v "0.1.0") (h "1iiriamzihzq0zlx6vx413v41ls2svm778r9ygp0ly8fxl9lpylm")))

(define-public crate-grux-0.2.0 (c (n "grux") (v "0.2.0") (h "19aphvz2a927k1vw2485n5pwlf4q2lh6bpd6697q2m1jlgi0n9bn")))

