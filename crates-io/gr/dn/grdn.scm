(define-module (crates-io gr dn grdn) #:use-module (crates-io))

(define-public crate-grdn-0.0.1 (c (n "grdn") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (d #t) (k 0)))) (h "1mjcrrwrldfwrkmyshc2wmwjmfc1ihhfan46053ihyp10ishq7hv")))

