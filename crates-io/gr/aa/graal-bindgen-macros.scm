(define-module (crates-io gr aa graal-bindgen-macros) #:use-module (crates-io))

(define-public crate-graal-bindgen-macros-0.1.0 (c (n "graal-bindgen-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qasbrn83mzxkzxmk5vvdlkfsln10ffn0g81k0z0rr7hc1yi712g")))

