(define-module (crates-io gr #{3x}# gr3x) #:use-module (crates-io))

(define-public crate-gr3x-0.1.0 (c (n "gr3x") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap-verbosity-flag") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "18bmidplb9jalk37mh1gkhb6mcdp1zcx6dd53l831xqvaagczz7n")))

