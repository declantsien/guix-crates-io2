(define-module (crates-io gr x_ grx_macros) #:use-module (crates-io))

(define-public crate-grx_macros-0.1.0 (c (n "grx_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "17qcayzr2zizpxvl0lmck4xgdhpsvx83mr6bxcm5idcl0cx0w7xb")))

(define-public crate-grx_macros-0.1.1 (c (n "grx_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1f2v4rs66rjm8w0rchjarjf60qv5ghscv885wdc7094bxan7fywp")))

