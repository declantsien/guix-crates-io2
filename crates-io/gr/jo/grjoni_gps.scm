(define-module (crates-io gr jo grjoni_gps) #:use-module (crates-io))

(define-public crate-grjoni_gps-0.1.0 (c (n "grjoni_gps") (v "0.1.0") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0r0vxjr54xwv0vw7915a80bzy62gg1llf6yizz2c8a6adfdvvwq0")))

(define-public crate-grjoni_gps-0.1.1 (c (n "grjoni_gps") (v "0.1.1") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1z2d8r5d413fag8jjh50ixna7z1dp7br1nzqhg85lp9m06iknh79")))

