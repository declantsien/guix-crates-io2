(define-module (crates-io gr ok grokj2k-sys) #:use-module (crates-io))

(define-public crate-grokj2k-sys-0.1.0 (c (n "grokj2k-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "12nf15xlbvx02z9if8lxlin75blbnk9jm9x911vyjvjbvw50kvhq") (l "grokj2k")))

(define-public crate-grokj2k-sys-0.1.1 (c (n "grokj2k-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "10b6i9n5kr5a5pcp49px9b6mgy975qdc4v3f0i6ax4w47j1ia4dw") (l "grokj2k")))

