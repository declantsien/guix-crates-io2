(define-module (crates-io gr ok grok) #:use-module (crates-io))

(define-public crate-grok-0.1.0 (c (n "grok") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ml7qrdw0blljk0cw4zdhnlh8myqb8rqij18jvs63ypb0cr4p7i1")))

(define-public crate-grok-0.2.0 (c (n "grok") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1jcn7gzygd0k4hwfg2zg1cy0qz8fydy6j3hr84ddwkab2kl2bff3")))

(define-public crate-grok-0.3.0 (c (n "grok") (v "0.3.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "onig") (r "^1.6") (d #t) (k 0)))) (h "0bbrhivywb2vds0jr46pd2isay2jnjrpdrhggdd8s34c9yrrik9v")))

(define-public crate-grok-0.4.0 (c (n "grok") (v "0.4.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "onig") (r "^2.0") (d #t) (k 0)))) (h "0rjk2n218a4r9y4mk4l5p1js5svjxhwhf6azf28m3r5ypx0kxqy2")))

(define-public crate-grok-0.4.1 (c (n "grok") (v "0.4.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "onig") (r "^2.0") (d #t) (k 0)))) (h "0vqycgc6dgiinnvm0jwqf7clik83c11nbszz9rhg1dxbjgp295kk")))

(define-public crate-grok-0.5.0 (c (n "grok") (v "0.5.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "onig") (r "^3.1") (d #t) (k 0)))) (h "0nhb90ibwxg435jqhd11qx54psjqw1xjskgcgf7skg50ibwrmj16")))

(define-public crate-grok-1.0.0 (c (n "grok") (v "1.0.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 0)))) (h "080jh7sasab1ivjz05l65kbyr320bm6ymspbgbm0pf8vrp6pw7vf")))

(define-public crate-grok-1.1.0 (c (n "grok") (v "1.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "onig") (r "^5.0") (d #t) (k 0)))) (h "03kc5lv704sliypk740ians4271q65sphns20yrqqjd4b2y49ywh")))

(define-public crate-grok-1.0.1 (c (n "grok") (v "1.0.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "onig") (r "^4.3") (d #t) (k 0)))) (h "1fbia69yz6fzzkkn3i9cqyfraraiky6saivmci7xjdhms6gmicrc")))

(define-public crate-grok-1.2.0 (c (n "grok") (v "1.2.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "onig") (r "^6.1") (d #t) (k 0)))) (h "0w16c0l3jygipw8y3w117smjx7g7jfgdpkdw7ywi2g7j7dfbn3w4")))

(define-public crate-grok-2.0.0 (c (n "grok") (v "2.0.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "onig") (r "^6.3") (k 0)))) (h "1gfzgnaa3cm1jrnfyglgwqgvja5aqhzn28hh0xrha9v0h6b9fdr7") (r "1.56")))

