(define-module (crates-io gr df grdf) #:use-module (crates-io))

(define-public crate-grdf-0.1.0-beta (c (n "grdf") (v "0.1.0-beta") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "0b7qzksa8m5h9f2jpfxdxphjswjf6sqapvvy7fg90454r6wy48fz")))

(define-public crate-grdf-0.1.1-beta (c (n "grdf") (v "0.1.1-beta") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "1pm4q3qmk1ipqdp5s9rqmdly3v6a89p7yrr7is1qihkmm874g5p2")))

(define-public crate-grdf-0.2.0-beta (c (n "grdf") (v "0.2.0-beta") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "1q48yjzbdv68i28cp9d0nn4cl5xihi0l9fj1bw0rnbxd89jkg2lh")))

(define-public crate-grdf-0.2.1-beta (c (n "grdf") (v "0.2.1-beta") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "13y6jbfjxyp2yafqzgaqwgmvjh2pgwyd3105y7gviwwv2wv0xlbm")))

(define-public crate-grdf-0.3.0 (c (n "grdf") (v "0.3.0") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rdf-types") (r "^0.1") (d #t) (k 0)))) (h "0w379aflf2pmzmq3cfwvzx8gcm0sdd4l8z3vy4f4lbwp1bjxladi") (y #t)))

(define-public crate-grdf-0.3.1 (c (n "grdf") (v "0.3.1") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rdf-types") (r "^0.2") (d #t) (k 0)))) (h "183p7yj9rawnn7i2vq0p2rkbw1k5fv58pm0ykgjz0k3qm7zvzfs2")))

(define-public crate-grdf-0.3.2 (c (n "grdf") (v "0.3.2") (d (list (d (n "iref") (r "^1.1") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rdf-types") (r "^0.2") (d #t) (k 0)))) (h "1zsn57rsvwxdzwnywcv6jgy9kl3x2dkpjji0yvi07hlsacm0pj4c")))

(define-public crate-grdf-0.4.0 (c (n "grdf") (v "0.4.0") (d (list (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "rdf-types") (r "^0.2.4") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0fdbmhdp27xbwf2j9wclaxgwnvbbli12d9pk3g0vgz3yfz40p0xz") (y #t)))

(define-public crate-grdf-0.4.1 (c (n "grdf") (v "0.4.1") (d (list (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "rdf-types") (r "^0.2.8") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1ckq7r98zdhji0h3w6y050nl4zk5r7kd76sx6i62zjz6dr72bzmg") (y #t)))

(define-public crate-grdf-0.4.2 (c (n "grdf") (v "0.4.2") (d (list (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "rdf-types") (r "^0.2.8") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0zgfxjbbq7yap6yzj09a09k9zl95snd5cbzhqrnzvh0b5p5xkhxw")))

(define-public crate-grdf-0.5.0 (c (n "grdf") (v "0.5.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "rdf-types") (r "^0.3.2") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0x65khhwdc647z807m5zwx0h6xjialz08zm407985krixd3p2yqa")))

(define-public crate-grdf-0.6.0 (c (n "grdf") (v "0.6.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.3.2") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1b2sgmzaj75in05hacimz4bq6m2alfads4v5q3cvnq69dwmyh7ha") (f (quote (("loc" "locspan" "rdf-types/loc") ("default"))))))

(define-public crate-grdf-0.6.1 (c (n "grdf") (v "0.6.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.3.2") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1wq0j8wsr71lvbnfp5aygypqvx50215k58zncw1is70h0ylk1711") (f (quote (("loc" "locspan" "rdf-types/loc") ("default"))))))

(define-public crate-grdf-0.7.0 (c (n "grdf") (v "0.7.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.4.0") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0vbqvln0yp5yb5fv5f2dm01m8by5k89sylkllx5ckx5mxq6lx0bm") (f (quote (("loc" "locspan" "rdf-types/loc") ("default"))))))

(define-public crate-grdf-0.7.1 (c (n "grdf") (v "0.7.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.4.0") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0bd58f0kd1qzaksxiszccwlc8m39nmmnvi2pl4g4rxbmv8szww3h") (f (quote (("loc" "locspan" "rdf-types/loc") ("default"))))))

(define-public crate-grdf-0.7.2 (c (n "grdf") (v "0.7.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.4.0") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0wry09k2szfa76k2ikkwbqqxz5q540lwpzp45zrmv1l8s4zci7n3") (f (quote (("loc" "locspan" "rdf-types/loc") ("default"))))))

(define-public crate-grdf-0.7.3 (c (n "grdf") (v "0.7.3") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.4.0") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1mrbp82dgjy34hv5gj3dzk27d5whn735l6xjkhbhr8rvayfvbgai") (f (quote (("loc" "locspan" "rdf-types/loc") ("default"))))))

(define-public crate-grdf-0.8.0 (c (n "grdf") (v "0.8.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.6.1") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1iws2jimlc9p0lyyjxyzayfqh42gsjqq01bxg3sblhjj8ldwj6m6") (f (quote (("loc" "locspan" "rdf-types/loc") ("default"))))))

(define-public crate-grdf-0.9.0 (c (n "grdf") (v "0.9.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.7") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1fmncs7wy1igcrrs3gyivln8pm49hpb8wvz5swzkb0mvpaym4zfz") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.10.0 (c (n "grdf") (v "0.10.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.8") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1milji4ls8j46fsp61dwsz5zbgr8hiya5c3agmyi559i4pwb4azm") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.11.0 (c (n "grdf") (v "0.11.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.9") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "196dgqw2fghplmdm2isrsg6q6chmsxg8daz15n6drc095lmlk3yg") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.12.0 (c (n "grdf") (v "0.12.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.10") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "14cf31ca8h2kr5cjvcr4sn6kv7zz7g6drzx8m0zj24ar41vgq6ny") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.13.0 (c (n "grdf") (v "0.13.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.11") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "15hbacs606y3ld9sz1rjki01j61s58jp5jcrxq54yp3krdcj6fqn") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.14.0 (c (n "grdf") (v "0.14.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1n78417hs0b1k4v0wzjd96vfy7cvs5w9pkvxii9jjcfrmgwlv0m6") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.14.1 (c (n "grdf") (v "0.14.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "088xdqzw83r3xcmwd7gi292zzsyf76xmhlcx99w8r7i5pjdjyqa4") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.15.0 (c (n "grdf") (v "0.15.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0cf2z1sqw50zxn8fazhqangvmh98p3plj911csaixaqfahnl4zsy") (f (quote (("meta" "locspan" "rdf-types/meta") ("default")))) (y #t)))

(define-public crate-grdf-0.15.1 (c (n "grdf") (v "0.15.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "13myn4amxjv6h1gqnky59n5ay36nnpkhrxdan8fm33dp92763yck") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.15.2 (c (n "grdf") (v "0.15.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0yn5f1c2xs0wf60775glp62q69fp6a6xjc3yrcq3x4bihcg6c8hr") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.15.3 (c (n "grdf") (v "0.15.3") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0jhkb0iax30c9ji43fa18spvfmarlnkrjpsjp0i5cl64vwpnzz3w") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.16.0 (c (n "grdf") (v "0.16.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0c884byjv2gkjzkhda75695p6dsjl681l0k4ngfjq4bghiqyd3xb") (f (quote (("meta" "locspan" "rdf-types/meta") ("default")))) (y #t)))

(define-public crate-grdf-0.16.1 (c (n "grdf") (v "0.16.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0s58nmjldv0gm4n3kbikc9p88sp8az2hn9b1bm9skvpb5azdcvqm") (f (quote (("meta" "locspan" "rdf-types/meta") ("default")))) (y #t)))

(define-public crate-grdf-0.16.2 (c (n "grdf") (v "0.16.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0a7ix2rsbiy9vnwj3344zmxsw72ghmb96zwvk6yqbqysg1g29inf") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.16.3 (c (n "grdf") (v "0.16.3") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.12") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "000bi5wfa72b638zbdhxvq32qcgg8kqxqxmc5fd43y2xq0ifmfy9") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.17.0 (c (n "grdf") (v "0.17.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.13") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "0a9840iiph139srli8nryx6phqyjhaaj04fxkwgmd6q1iicim8wr") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.18.0 (c (n "grdf") (v "0.18.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.14.2") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "01v5cff61186pkh1mf1lbzprf1jxdwcsns0nwlxjb9vlrnldsywp") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.19.0 (c (n "grdf") (v "0.19.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^2.1.1") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.15") (d #t) (k 0)) (d (n "static-iref") (r "^1.0.0") (d #t) (k 2)))) (h "1gbrh8axgph194qpmp2miab1kl3f5z6lv8qvhv22xy3nlin4m6if") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.20.0 (c (n "grdf") (v "0.20.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^3.0") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.16") (d #t) (k 0)) (d (n "static-iref") (r "^3.0.0") (d #t) (k 2)))) (h "140mi5wq6zz6qn54zbz3ss73jr6ciym3xvd6kkqklcrfnbb2b8hp") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.21.0 (c (n "grdf") (v "0.21.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^3.0") (d #t) (k 0)) (d (n "locspan") (r "^0.7.9") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.17") (d #t) (k 0)) (d (n "static-iref") (r "^3.0.0") (d #t) (k 2)))) (h "1pqq3iw6ch27m9nz5n7n9qpkfff68x6if3kldm7hrw7v232fbn49") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.22.0 (c (n "grdf") (v "0.22.0") (d (list (d (n "educe") (r "^0.4.23") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^3.0") (d #t) (k 0)) (d (n "locspan") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.18") (d #t) (k 0)) (d (n "static-iref") (r "^3.0.0") (d #t) (k 2)))) (h "12v0rjwa7pqrcsb3zfb73z5fkxnain8lz3rpi0a3b3q7b9nfsr6s") (f (quote (("meta" "locspan" "rdf-types/meta") ("default"))))))

(define-public crate-grdf-0.22.1 (c (n "grdf") (v "0.22.1") (d (list (d (n "educe") (r "^0.4.23") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "iref") (r "^3.0") (d #t) (k 0)) (d (n "locspan") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rdf-types") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "static-iref") (r "^3.0.0") (d #t) (k 2)))) (h "1k7gl6mvlzm5b77mrvfs37jj44k42zim65nv7dw6qkgxdqrj279m") (f (quote (("meta" "locspan" "rdf-types/meta") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "rdf-types/serde"))))))

