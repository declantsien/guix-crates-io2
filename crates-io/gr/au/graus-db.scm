(define-module (crates-io gr au graus-db) #:use-module (crates-io))

(define-public crate-graus-db-0.1.0 (c (n "graus-db") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 2)))) (h "1hfph19f3hnmr7v0mmr64z3wwwplpv0mkd62njfz9awz13sv49dk")))

