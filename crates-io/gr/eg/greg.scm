(define-module (crates-io gr eg greg) #:use-module (crates-io))

(define-public crate-greg-0.1.0 (c (n "greg") (v "0.1.0") (h "1m9f785jf4iapmahwyqn5m2nr8rvb6s41v8vw8jpc0q9lybyfjzg")))

(define-public crate-greg-0.1.1 (c (n "greg") (v "0.1.1") (h "0slv67fb1hiyakqybkga2ygf8j4v3cipd3bcb1zhrwypyxysla9f")))

(define-public crate-greg-0.1.2 (c (n "greg") (v "0.1.2") (d (list (d (n "rusqlite") (r "^0.28") (o #t) (k 0)))) (h "0zrc8nq4kjnq0wbhxr1r3mqbabdqq5k9zgbqi713j2am74xiffsl") (s 2) (e (quote (("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2.0 (c (n "greg") (v "0.2.0") (d (list (d (n "rusqlite") (r "^0.28") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0x38sw51g7sb48cpbh6mw1bssn10483c8bfwclrja2c11b6ki1si") (s 2) (e (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2.1 (c (n "greg") (v "0.2.1") (d (list (d (n "rusqlite") (r "^0.29") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0kkrngl14043g2dzb2y03dwazykicfyjh9v500v7c5f1mhskf25n") (s 2) (e (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2.2 (c (n "greg") (v "0.2.2") (d (list (d (n "rusqlite") (r "^0.29") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0bm2zzgwd5ap3a2r7anc9hj2mmgibkcwqwcmxa1mlqxk7qm0xirr") (s 2) (e (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2.3 (c (n "greg") (v "0.2.3") (d (list (d (n "rusqlite") (r "^0.29") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1xxf359avp6ssh6njkrbld7qbq2rd8vawvk938hm1ic3lqq1bx7l") (s 2) (e (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

