(define-module (crates-io gr eg greg-tz) #:use-module (crates-io))

(define-public crate-greg-tz-0.1.0 (c (n "greg-tz") (v "0.1.0") (d (list (d (n "greg") (r "^0.1.1") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1h4w89xxkz4qny44irwi8g083dvzlgzxca67j2vl9p85yqnh8yhd")))

(define-public crate-greg-tz-0.1.1 (c (n "greg-tz") (v "0.1.1") (d (list (d (n "greg") (r "^0.1.2") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1w3bh1q19wqbcm6nz048453zp6vh5md53sz0qacxfs7ybjc0kvgn")))

(define-public crate-greg-tz-0.1.2 (c (n "greg-tz") (v "0.1.2") (d (list (d (n "greg") (r "^0.1.2") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0imjd6lpd0c3ppsxqnnr8kc537s02wzvgr2lyambr1hp0b6fa9cq")))

(define-public crate-greg-tz-0.2.0 (c (n "greg-tz") (v "0.2.0") (d (list (d (n "greg") (r "^0.2.0") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "07kz5d4pk28lsjnkim52ipc0711zz6pw8glja2dly7wxadx7y118")))

(define-public crate-greg-tz-0.2.1 (c (n "greg-tz") (v "0.2.1") (d (list (d (n "greg") (r "^0.2.1") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1ghcrjzms7hv6kks7hp8l7yx0sjn9hvwrq4d8s05izildywgad63")))

(define-public crate-greg-tz-0.2.2 (c (n "greg-tz") (v "0.2.2") (d (list (d (n "greg") (r "^0.2.3") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "1x3nwxz0ncfs6h0yrc2c1ndbvkl1xzf7jlc7zsvcsnhbf18m6ijs")))

(define-public crate-greg-tz-0.2.3 (c (n "greg-tz") (v "0.2.3") (d (list (d (n "greg") (r "^0.2.3") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3.0") (d #t) (k 1)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)))) (h "177581dkrsv7jxb008l45bizrcg0nfwbf2vzk4j4nb06jbp4cm09")))

