(define-module (crates-io gr eg gregex) #:use-module (crates-io))

(define-public crate-gregex-0.4.1 (c (n "gregex") (v "0.4.1") (h "1slmhaw02scn8nszk7rj93lcba3jpv76bqfy0ghgl4392njbxs20")))

(define-public crate-gregex-0.4.3 (c (n "gregex") (v "0.4.3") (h "18cjgjhfl6pc4k8kp8xz6mfdcf8128kys4y0sjphbmwxhxj535x1")))

(define-public crate-gregex-0.5.0 (c (n "gregex") (v "0.5.0") (h "1qhj6q4p3ys36drvw6z3gqp51qv29scw2gdiksbcksdxjsj2fwan")))

(define-public crate-gregex-0.5.1 (c (n "gregex") (v "0.5.1") (h "0izwvrd68gqdlhwgfyyhglpbr3nsxny21nkkf3armx8ga3yzyppw")))

(define-public crate-gregex-0.5.2 (c (n "gregex") (v "0.5.2") (h "020vlzkigl23rl3vbxx3hdly2gwbq3nhyyyf7ar75z01x3xzkxqg") (y #t)))

(define-public crate-gregex-0.5.3 (c (n "gregex") (v "0.5.3") (h "1rr5rcj41rkv22wr0qs3xf2nxsc86mslx5hqc7mh9v0dpdfbffd0")))

