(define-module (crates-io gr eg gregorian) #:use-module (crates-io))

(define-public crate-gregorian-0.1.0 (c (n "gregorian") (v "0.1.0") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)))) (h "1qdv2q3f7bcyn2iswiwx1w22k95qj6zspb748qlbrldj9j6xdxcp") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-gregorian-0.1.1 (c (n "gregorian") (v "0.1.1") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)))) (h "0175xfxxjx62wbzxz1y6y9c1b1pxg346hc008dw7569i1zdnvamp") (f (quote (("std") ("default" "std"))))))

(define-public crate-gregorian-0.2.0 (c (n "gregorian") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)))) (h "14gs790dr6ykw8c6gbgw95aa9g4z6zsf4zxahfbcwky8bf4h9drq") (f (quote (("std") ("default" "std"))))))

(define-public crate-gregorian-0.2.1 (c (n "gregorian") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (o #t) (d #t) (k 0)))) (h "0l4hybg8zmgyarkzwhh6b2h6590w9zhrzw44kgf3hp4r5hprflil") (f (quote (("std" "libc") ("default" "std"))))))

(define-public crate-gregorian-0.2.2 (c (n "gregorian") (v "0.2.2") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (o #t) (d #t) (k 0)))) (h "1b75q2mm0pxbybw2psvyba2qxs4kxr79s7d617yicxfqf8skhwy8") (f (quote (("std" "libc") ("default" "std")))) (y #t)))

(define-public crate-gregorian-0.2.3 (c (n "gregorian") (v "0.2.3") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (o #t) (d #t) (k 0)))) (h "19s5chpldr5gip9fb53g6x7969vb6cf11p4qxcbk24pds6q81vq9") (f (quote (("std" "libc") ("default" "std"))))))

(define-public crate-gregorian-0.2.4 (c (n "gregorian") (v "0.2.4") (d (list (d (n "assert2") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 2)))) (h "0m13n1zaq7rbcx346qyzn6crf6psbxgb0n8xk66l7a6d8bn8m20q") (f (quote (("std" "libc") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

