(define-module (crates-io gr eg gregor) #:use-module (crates-io))

(define-public crate-gregor-0.1.0 (c (n "gregor") (v "0.1.0") (h "1y2hjira714b519q7ggb8f93cz23mpyl9g9qxmmw8q0p914wqfar")))

(define-public crate-gregor-0.2.0 (c (n "gregor") (v "0.2.0") (h "0ms1gy739lq4hzvrip7ssr44x6ic3mhd73q9j078khww8icrqs02") (f (quote (("system_time"))))))

(define-public crate-gregor-0.2.1 (c (n "gregor") (v "0.2.1") (h "11f1z64kzv3dwjssw4843zzxiy58dx7lrh1c9pw4hngpr7s0vnbk") (f (quote (("system_time"))))))

(define-public crate-gregor-0.2.2 (c (n "gregor") (v "0.2.2") (h "1amqxyyp2bfvxpzpfn54nn7as7mnlysrxdg2fp0b5kiqlz9f5rmz") (f (quote (("system_time"))))))

(define-public crate-gregor-0.3.0 (c (n "gregor") (v "0.3.0") (h "0bylp9sii9c1grimbna9qpixlvci47zh9r5n9skq3gvpxl6dfq3g") (f (quote (("system_time"))))))

(define-public crate-gregor-0.3.1 (c (n "gregor") (v "0.3.1") (h "0myn83lp5998z0iv8jy2i05hsc4qm9h221mqn82mdz3byspbh4yw") (f (quote (("system_time"))))))

(define-public crate-gregor-0.3.2 (c (n "gregor") (v "0.3.2") (h "0yir48ypfxbvajpw3gi4962416y7bjll3kfbbvazfaljhyhf8kny") (f (quote (("system_time"))))))

(define-public crate-gregor-0.3.3 (c (n "gregor") (v "0.3.3") (h "0gjdn9mngnfrpynjiaqdfhr26iqbvvs5n8pw9a6cndycl7mjaxfc") (f (quote (("system_time"))))))

