(define-module (crates-io gr as grass-ir) #:use-module (crates-io))

(define-public crate-grass-ir-0.1.2 (c (n "grass-ir") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10r5j1q2c7xfv99723xwlxvmbv58l913gqchwa62j6vq5hr3wv0p")))

(define-public crate-grass-ir-0.1.3 (c (n "grass-ir") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xd1z0y5dl5gr83a4dqii89p01cfyj8y8ry2r5fvxnn2306vfa10")))

(define-public crate-grass-ir-0.1.4 (c (n "grass-ir") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i5h9pcpacc3807cd7v31f2xb6sk9vkv008m9lmg3n5r72bwikvx")))

