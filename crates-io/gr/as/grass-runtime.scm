(define-module (crates-io gr as grass-runtime) #:use-module (crates-io))

(define-public crate-grass-runtime-0.1.2 (c (n "grass-runtime") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)))) (h "04yb2rjixg65s45c0nydcggmnp4bx04bp6r3zmz8ppc6cxnp9ikj")))

(define-public crate-grass-runtime-0.1.3 (c (n "grass-runtime") (v "0.1.3") (d (list (d (n "d4-hts") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bb58f1qrd9wmpd2pbr3d1n8z9crxq98kxmpk5bbk4i8im3d3ki4") (f (quote (("htslib" "d4-hts"))))))

(define-public crate-grass-runtime-0.1.4 (c (n "grass-runtime") (v "0.1.4") (d (list (d (n "d4-hts") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1j6c6ryghi1i356wa0v7pkls1bi4zkq3y69c476x69kfybvi2v2n") (f (quote (("htslib" "d4-hts"))))))

