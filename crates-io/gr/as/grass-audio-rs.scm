(define-module (crates-io gr as grass-audio-rs) #:use-module (crates-io))

(define-public crate-grass-audio-rs-0.1.0 (c (n "grass-audio-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "0478s8a4rsh7ars21imx5nxspgbadhrlwpgwfa7qaggavpfsyx52") (y #t)))

(define-public crate-grass-audio-rs-0.1.1 (c (n "grass-audio-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "1mwnvzxvy6mwmibfpz2304rw2v36srbznz2kgyrkkn8pi9qybpbv") (y #t)))

(define-public crate-grass-audio-rs-0.1.2 (c (n "grass-audio-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "06bzin8mfrpk9c84ckwf4p7309cifxmcfilwsq4wn0vhjwf8hqzi") (y #t)))

(define-public crate-grass-audio-rs-0.1.3 (c (n "grass-audio-rs") (v "0.1.3") (d (list (d (n "grass-audio-sys") (r "^0.1.0") (d #t) (k 0)))) (h "01i97yrbqj98lcky4q7al5hw8i785fnj6a6zxxifbypnisij3dcz") (y #t)))

(define-public crate-grass-audio-rs-0.2.0 (c (n "grass-audio-rs") (v "0.2.0") (d (list (d (n "grass-audio-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0q8bx15drfh1z0v8fhp56acsxvircq54vnx8r9j6idx34jfxlpwx") (y #t)))

(define-public crate-grass-audio-rs-0.2.1 (c (n "grass-audio-rs") (v "0.2.1") (d (list (d (n "grass-audio-sys") (r "^0.2.0") (d #t) (k 0)))) (h "03kysiz3apjn4fsk33rggnisq1rhd7mc8vyzzch57s1nsgryqlk9") (y #t)))

(define-public crate-grass-audio-rs-0.3.0 (c (n "grass-audio-rs") (v "0.3.0") (d (list (d (n "grass-audio-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "09bfiy3gskwzgna07kbgvl84rnxln9z7b9ijlrf4d6zy8042m2a8") (y #t)))

(define-public crate-grass-audio-rs-0.4.0 (c (n "grass-audio-rs") (v "0.4.0") (d (list (d (n "grass-audio-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "12rdjnw612gg1yicns823h24bzdy0vlw8qprrvjhd3dv2p2rglrz") (y #t)))

