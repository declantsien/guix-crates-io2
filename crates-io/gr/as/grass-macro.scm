(define-module (crates-io gr as grass-macro) #:use-module (crates-io))

(define-public crate-grass-macro-0.1.2 (c (n "grass-macro") (v "0.1.2") (d (list (d (n "grass-ir") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "095dwfbdr6j81k1f2pl85ag7508zxqh099slabkwkv0aaswmys69")))

(define-public crate-grass-macro-0.1.3 (c (n "grass-macro") (v "0.1.3") (d (list (d (n "grass-ir") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "00b54j9j5rxsd9hwlr7w65rmnqhwqr51m5jj5mp3sk8bndd11l0s")))

(define-public crate-grass-macro-0.1.4 (c (n "grass-macro") (v "0.1.4") (d (list (d (n "grass-ir") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (k 0)))) (h "0hffjzsvxlhslcz2iwa1shfsrcz3s1dail5s3s0crl4qbyd488pc")))

