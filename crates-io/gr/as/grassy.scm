(define-module (crates-io gr as grassy) #:use-module (crates-io))

(define-public crate-grassy-0.1.0 (c (n "grassy") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "14lviwrw9x3mrybvy33dgr5h3jn9m6chas5cvfpzs3hxfci7wkpp") (y #t)))

