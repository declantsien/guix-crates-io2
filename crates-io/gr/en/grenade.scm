(define-module (crates-io gr en grenade) #:use-module (crates-io))

(define-public crate-grenade-0.1.0 (c (n "grenade") (v "0.1.0") (d (list (d (n "pond") (r "^0.3") (d #t) (k 0)))) (h "0ab5hd20xc94x5ayx9fx0pd6imxv3k9xcd6v9bihs54r21axvix0")))

(define-public crate-grenade-0.1.1 (c (n "grenade") (v "0.1.1") (d (list (d (n "pond") (r "^0.3") (d #t) (k 0)))) (h "119syg9yxs1zl89dc49pldl55si3k36az46718x7wvn0lyv0314w")))

(define-public crate-grenade-0.1.4 (c (n "grenade") (v "0.1.4") (d (list (d (n "pond") (r "^0.3") (d #t) (k 0)))) (h "0hwsxj0xj4qpxm8ffl27md7p7w5ia7wgiksbgza35mhzzhgwmiwf")))

(define-public crate-grenade-0.1.5 (c (n "grenade") (v "0.1.5") (d (list (d (n "pond") (r "^0.3") (d #t) (k 0)))) (h "0wqmckqcrm2jl1s95g6ghchpp2ybzbn2lr3rjqlvfhzkzsipjw5l")))

(define-public crate-grenade-0.1.6 (c (n "grenade") (v "0.1.6") (d (list (d (n "pond") (r "^0.3") (d #t) (k 0)))) (h "1cys1qdz5bvpjcs28vlhhmardh0kq3v351r1pywy55dg4p3653ga")))

