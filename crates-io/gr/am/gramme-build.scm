(define-module (crates-io gr am gramme-build) #:use-module (crates-io))

(define-public crate-gramme-build-0.1.0 (c (n "gramme-build") (v "0.1.0") (h "14pwxvylxzcwmpxi0axzs4nmic4h0d51a33ppypiay2rmmnxrqcy")))

(define-public crate-gramme-build-0.2.0 (c (n "gramme-build") (v "0.2.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)))) (h "0w76i8kf28zap8b2glpjlwq9n05r6llw3wyzig34al8rmrgl9a76")))

