(define-module (crates-io gr am grammarly) #:use-module (crates-io))

(define-public crate-grammarly-0.1.0 (c (n "grammarly") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ybbsb056gg6f27jngkfh1wl1976jhflb2idp4yg72l4l6x7psh2") (f (quote (("default" "client") ("client" "reqwest")))) (y #t)))

(define-public crate-grammarly-0.1.1 (c (n "grammarly") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jlp9pnrr632pivx4bv60fvbg7xabkppqm1di5jx4fx9y2v7j3j6") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-grammarly-0.2.0 (c (n "grammarly") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0s5cz1ksz0ffb8bbkvkz556rpmiksdf203c8gx3m698jgnk1xb4h") (f (quote (("default" "client") ("client" "reqwest")))) (y #t)))

(define-public crate-grammarly-0.2.1 (c (n "grammarly") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0x2fd83s805ycqqg1rmig1f5yj3sn71qrd86l8im6yls622j4gxx") (f (quote (("default" "client") ("client" "reqwest")))) (y #t)))

(define-public crate-grammarly-0.2.2 (c (n "grammarly") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dk6nsnyx1r2b8z052544wiy75w4vpqkpdl8677dc1dkskl70ylp") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-grammarly-0.3.0 (c (n "grammarly") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10ikz299j7kjxdq4z3hscc0dyvm5zgr30r5xgfhahqwyww33plvr") (f (quote (("default" "client") ("client" "reqwest")))) (y #t)))

(define-public crate-grammarly-0.3.1 (c (n "grammarly") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "164xgidsyqk1iljxm2m6lj9z93sfj4gb5qhk1sffhl0ivqbg7kfi") (f (quote (("default" "client") ("client" "reqwest"))))))

