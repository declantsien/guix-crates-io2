(define-module (crates-io gr am grammarbot-io) #:use-module (crates-io))

(define-public crate-grammarbot-io-1.0.0 (c (n "grammarbot-io") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ln4m524dm4jz2w41ks4hln2yb80y3bsbkya6na9n932521alyb0") (f (quote (("default" "client") ("client" "reqwest"))))))

(define-public crate-grammarbot-io-1.0.1 (c (n "grammarbot-io") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08nr8kyy4c2ni1461k49ah6zq58bz98civgihdn2czyn8fl6p8kk") (f (quote (("default" "client") ("client" "reqwest"))))))

