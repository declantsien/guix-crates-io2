(define-module (crates-io gr am gram-cli) #:use-module (crates-io))

(define-public crate-gram-cli-0.1.0 (c (n "gram-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.54") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "17apq4q794zpscxv1msfcr9281cdilj0py95zmkwpbj8ikwgrxzw")))

