(define-module (crates-io gr am grammar_tool) #:use-module (crates-io))

(define-public crate-grammar_tool-0.1.0 (c (n "grammar_tool") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14jy44bcni87sr5jslqwa5vn6771qfahgg496pbdyqpch18hm65s")))

