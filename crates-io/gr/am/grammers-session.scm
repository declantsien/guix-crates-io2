(define-module (crates-io gr am grammers-session) #:use-module (crates-io))

(define-public crate-grammers-session-0.1.0 (c (n "grammers-session") (v "0.1.0") (h "13rdx7kgs1haa73ycp2jbfzfa4j1plnnwdc1rmyd53szhwp7pqja")))

(define-public crate-grammers-session-0.2.0 (c (n "grammers-session") (v "0.2.0") (d (list (d (n "grammers-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "158j3bdjas9afmqc8d937v84w9ia812z8lz2psz77skmji9296h4")))

(define-public crate-grammers-session-0.3.0 (c (n "grammers-session") (v "0.3.0") (d (list (d (n "grammers-crypto") (r "^0.3.0") (d #t) (k 0)) (d (n "grammers-tl-gen") (r "^0.3.0") (d #t) (k 1)) (d (n "grammers-tl-parser") (r "^1.0.1") (d #t) (k 1)) (d (n "grammers-tl-types") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "12gdqgizjgw8nr3w37yki31gdw1i71kym84az66bk5cxyi9rb5rv")))

(define-public crate-grammers-session-0.4.0 (c (n "grammers-session") (v "0.4.0") (d (list (d (n "grammers-tl-gen") (r "^0.4.1") (d #t) (k 1)) (d (n "grammers-tl-parser") (r "^1.1.0") (d #t) (k 1)) (d (n "grammers-tl-types") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "15c1fqha4bwr0zcz26w6i3k5dnw3hnyb2rmx325bcsys6vmc9n9w")))

(define-public crate-grammers-session-0.5.1 (c (n "grammers-session") (v "0.5.1") (d (list (d (n "grammers-crypto") (r "^0.5.1") (d #t) (k 0)) (d (n "grammers-tl-gen") (r "^0.5.1") (d #t) (k 1)) (d (n "grammers-tl-parser") (r "^1.1.1") (d #t) (k 1)) (d (n "grammers-tl-types") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "1d0dcly478w5hfn7bl22lfn82i7k6vy2lqvrs9gxwyjvyhkqhv85")))

(define-public crate-grammers-session-0.5.2 (c (n "grammers-session") (v "0.5.2") (d (list (d (n "grammers-crypto") (r "^0.6.1") (d #t) (k 0)) (d (n "grammers-tl-gen") (r "^0.6.0") (d #t) (k 1)) (d (n "grammers-tl-parser") (r "^1.1.1") (d #t) (k 1)) (d (n "grammers-tl-types") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "1wbgzdwzjnmhsl7b0lw9s24mp9v0prl29xdixakl51v2jmqkcjmi")))

