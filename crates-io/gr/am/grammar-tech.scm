(define-module (crates-io gr am grammar-tech) #:use-module (crates-io))

(define-public crate-grammar-tech-0.1.0 (c (n "grammar-tech") (v "0.1.0") (h "0i79y5zi7xwxrfvyi0fajbgkdq96jkdzn9dzv1a2gzsg2wf8bkj9") (y #t)))

(define-public crate-grammar-tech-0.1.1 (c (n "grammar-tech") (v "0.1.1") (h "0vmp7yhsdr4ah0bk8mrrbdbnz46vcn8d87maja17njzsx8dvhw7a") (y #t)))

(define-public crate-grammar-tech-0.1.2 (c (n "grammar-tech") (v "0.1.2") (h "1kypq1wzszsq20r4g0z2km8zyqxxv0zi6ybpii3rna7lq73zv0qm") (y #t)))

(define-public crate-grammar-tech-0.1.3 (c (n "grammar-tech") (v "0.1.3") (h "048hkiif95mh9wwd04nrzsjy6bxpqmzc20qli6i48vv578fnpl4c") (y #t)))

