(define-module (crates-io gr am gramatica) #:use-module (crates-io))

(define-public crate-gramatica-0.1.0 (c (n "gramatica") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1khv4wjcazcm8hvqgzfkcf5x4xynjj9fwfg6yxv0qfc5n1c80mm4")))

(define-public crate-gramatica-0.1.1 (c (n "gramatica") (v "0.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ydsxpkgrdl0997dnpbg8ry0nv6ifiglsmgaxrcg0yc5mmbskff2")))

(define-public crate-gramatica-0.1.2 (c (n "gramatica") (v "0.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1jmkn836i1kld5ikfnzh66xvpaaxiijvw95kr3zdg461icvfzzi0")))

(define-public crate-gramatica-0.1.3 (c (n "gramatica") (v "0.1.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0r4igcy601vm117v3klf74xivra3d5s8i5pgfnbcd274lxrlkn5r")))

(define-public crate-gramatica-0.1.4 (c (n "gramatica") (v "0.1.4") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "029ibfpyiif7syl9q2i20yrkag4fdpmhm08rlk1qj9xkgfv5mfs0")))

(define-public crate-gramatica-0.1.5 (c (n "gramatica") (v "0.1.5") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1z9mfkpvkh39swaqyj9psk79aknn8pf8lh3vc2h07miy74db683z")))

(define-public crate-gramatica-0.2.0 (c (n "gramatica") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1civfamcfgxjqr82431f6xda00dyv8bc0ibb2ggndlm4sjcdd4zv")))

(define-public crate-gramatica-0.2.1 (c (n "gramatica") (v "0.2.1") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "0scngn8z5z7h36fz4prcizvzisjmsjny0av87fq9l7sc4hm76m8j")))

