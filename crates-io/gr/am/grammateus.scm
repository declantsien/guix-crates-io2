(define-module (crates-io gr am grammateus) #:use-module (crates-io))

(define-public crate-grammateus-0.1.0 (c (n "grammateus") (v "0.1.0") (h "134sy98012n16rqkgjl2hyfv2ld6w2pwd9cbkrk0fq2zajiqwrv8")))

(define-public crate-grammateus-0.2.0 (c (n "grammateus") (v "0.2.0") (h "0svmak8mfz52hs9ncip6df7xbkxfjqz1116anqbk60sqw4d6ivd1")))

(define-public crate-grammateus-0.2.1 (c (n "grammateus") (v "0.2.1") (h "07krvl3sg8kpfjkj1njb7rfmwhf08xl8gszpnfmjmh5ar9hj8l1v")))

(define-public crate-grammateus-0.2.2 (c (n "grammateus") (v "0.2.2") (h "1sckcgi28bpqvl7461yhp80p86ziiw8r35kaxv8745ss8prpgrkg")))

