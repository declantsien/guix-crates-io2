(define-module (crates-io gr am grammar-macro) #:use-module (crates-io))

(define-public crate-grammar-macro-0.1.0 (c (n "grammar-macro") (v "0.1.0") (d (list (d (n "grammar-tech") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0lic4wixxd3gpvz2fvn8i0axl09x19ryam3laf5z5ki9dcv3fm1s") (y #t)))

(define-public crate-grammar-macro-0.1.1 (c (n "grammar-macro") (v "0.1.1") (d (list (d (n "grammar-tech") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0gdwbz221aja4gh820adcbhmsdqapnxm8smchkvaqacd0l808sqk") (y #t)))

