(define-module (crates-io gr am grammar-tech-macro) #:use-module (crates-io))

(define-public crate-grammar-tech-macro-0.1.0 (c (n "grammar-tech-macro") (v "0.1.0") (h "006iiqa1x14ngbrfs10jzkgag8z7mf68zln95schg6r6grixf02s")))

(define-public crate-grammar-tech-macro-0.1.1 (c (n "grammar-tech-macro") (v "0.1.1") (d (list (d (n "grammar-tech") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1zxl84kcm3xka55bz2dpgj545bk580r56dkvb8jdzlcj9g2ss0pz")))

