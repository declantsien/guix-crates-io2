(define-module (crates-io gr am grammers-tl-parser) #:use-module (crates-io))

(define-public crate-grammers-tl-parser-1.0.0 (c (n "grammers-tl-parser") (v "1.0.0") (d (list (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)))) (h "0adwwpsqq6qh52ypk9hbjq1kyy9fjag6p6r0y3l9xx6l5vp35acb")))

(define-public crate-grammers-tl-parser-1.0.1 (c (n "grammers-tl-parser") (v "1.0.1") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)))) (h "0bin458aqgln1rcq1k8vc7lgy7ilbx9gablxzpbz5rjc8shwvpsq")))

(define-public crate-grammers-tl-parser-1.1.0 (c (n "grammers-tl-parser") (v "1.1.0") (d (list (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "140jfgmk2g57nlhdx757r5cdn9jhx43cyfwyqly80zaqfmlq1j18")))

(define-public crate-grammers-tl-parser-1.1.1 (c (n "grammers-tl-parser") (v "1.1.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "050x88gcjcmyk4bqb49lrs9kfrgya8qdxz2yl6rlygiqap07j6c4")))

