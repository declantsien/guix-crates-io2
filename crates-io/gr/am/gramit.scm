(define-module (crates-io gr am gramit) #:use-module (crates-io))

(define-public crate-gramit-0.1.1 (c (n "gramit") (v "0.1.1") (h "1597hr2gs0n7pqz0h4pgq30x1ki7hji08dfyikvzj3irf4mkd2zp")))

(define-public crate-gramit-0.2.0 (c (n "gramit") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vulkano") (r "^0.16") (o #t) (d #t) (k 0)))) (h "0as1nk8sqkw5ababg2ymggm62w3704i0aa58wkclk8h0npgm7c5l") (f (quote (("default"))))))

