(define-module (crates-io gr am gramschmidt) #:use-module (crates-io))

(define-public crate-gramschmidt-0.4.0 (c (n "gramschmidt") (v "0.4.0") (d (list (d (n "cblas") (r "^0.1.5") (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.7") (d #t) (k 2)) (d (n "openblas-src") (r "^0.5.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "19wb7782mjz52izbsd1s0mhz0dms54qwawx17hfc1fdigjrh5bi0")))

(define-public crate-gramschmidt-0.4.1 (c (n "gramschmidt") (v "0.4.1") (d (list (d (n "cblas") (r "^0.1.5") (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.7") (d #t) (k 2)) (d (n "openblas-src") (r "^0.5.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1a7d4603r65x1ghm2lklmirfh987zyzya3rc5rlj00lf3v375flz")))

(define-public crate-gramschmidt-0.5.0 (c (n "gramschmidt") (v "0.5.0") (d (list (d (n "cblas") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 2)) (d (n "openblas-src") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "00gj5qqfbw1sg1jj7dnzzbbbkva9la4aghrq3mmc98la18lsd22f")))

(define-public crate-gramschmidt-0.6.0 (c (n "gramschmidt") (v "0.6.0") (d (list (d (n "cblas") (r "^0.2.0") (d #t) (k 0)) (d (n "lapacke") (r "^0.2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 2)) (d (n "openblas-src") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1i3372bqima62v0fc84cannyf7lpp87p9wvgwbd42dizzz8d3c2j")))

