(define-module (crates-io gr am grammarbot) #:use-module (crates-io))

(define-public crate-grammarbot-0.1.0 (c (n "grammarbot") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.4.4") (d #t) (k 0)))) (h "01p4x0m3ikrmr8d3r30sm39cvch9x2334r0wn36iag9vzb5xp0mm")))

(define-public crate-grammarbot-0.1.1 (c (n "grammarbot") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.4.4") (d #t) (k 0)))) (h "1p3k90axwwnlkhakagl8rxl134zlln2cc7wb0ismnw7rn0hxzghs")))

