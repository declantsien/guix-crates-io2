(define-module (crates-io gr am grammers-tl-gen) #:use-module (crates-io))

(define-public crate-grammers-tl-gen-0.2.0 (c (n "grammers-tl-gen") (v "0.2.0") (d (list (d (n "grammers-tl-parser") (r "^1.0.0") (d #t) (k 0)))) (h "1v476v7r8ds4z6c7y0v9vq8aqmgwq2am7404kygshdz7zwgkknwb")))

(define-public crate-grammers-tl-gen-0.3.0 (c (n "grammers-tl-gen") (v "0.3.0") (d (list (d (n "grammers-tl-parser") (r "^1.0.1") (d #t) (k 0)))) (h "1ikwfw4xsb4sh6z87avsdfdixzf36c4r5vq7fnhyfidb48z12cnb")))

(define-public crate-grammers-tl-gen-0.4.1 (c (n "grammers-tl-gen") (v "0.4.1") (d (list (d (n "grammers-tl-parser") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 2)))) (h "0v747aijaj07izfapc8im0zmrf9xsy6d41m7l6m5ak853fa2rcqr")))

(define-public crate-grammers-tl-gen-0.5.0 (c (n "grammers-tl-gen") (v "0.5.0") (d (list (d (n "grammers-tl-parser") (r "^1.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "0xnhrdh0kzxycml36v4z4di539pnh2s9lz80yq47kg9qclnvcrl7")))

(define-public crate-grammers-tl-gen-0.5.1 (c (n "grammers-tl-gen") (v "0.5.1") (d (list (d (n "grammers-tl-parser") (r "^1.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "0yi7q05n90bamq114qj7jdn9bkp55g33ga827qaj1im2yfqx0qih")))

(define-public crate-grammers-tl-gen-0.6.0 (c (n "grammers-tl-gen") (v "0.6.0") (d (list (d (n "grammers-tl-parser") (r "^1.1.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "1awnn60vvkly68m75h9pkhwvy15z5qr7g57v3381v972pnjklrjy")))

