(define-module (crates-io gr p- grp-cli) #:use-module (crates-io))

(define-public crate-grp-cli-0.1.0 (c (n "grp-cli") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "14ziyndmcymlgz6b64smkjc7rd9v9rhiljv3xwnv3lb1yp9adarh")))

(define-public crate-grp-cli-0.2.0 (c (n "grp-cli") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "13syx3i1q4nc9cmp9i9mc773iksmcmj8psf552jzhz54pwn4a98n")))

