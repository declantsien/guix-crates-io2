(define-module (crates-io gr an grandeur_gdnative_macros) #:use-module (crates-io))

(define-public crate-grandeur_gdnative_macros-0.1.0 (c (n "grandeur_gdnative_macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8") (d #t) (k 0)))) (h "044fk6q9v040mrcawkyv6zfia6djll7gq96wkchccbpyfn903ylv")))

(define-public crate-grandeur_gdnative_macros-0.1.1 (c (n "grandeur_gdnative_macros") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "syn-rsx") (r "^0.9") (d #t) (k 0)))) (h "1ji6j6p545gmv8342n5rmkqmmfwf4z4gfrk5cvla1x7bpia37r8v")))

