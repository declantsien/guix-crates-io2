(define-module (crates-io gr an grandiorite) #:use-module (crates-io))

(define-public crate-grandiorite-1.0.3 (c (n "grandiorite") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)))) (h "0xh5qw9n0d76d0122c6gx70vbi0psc36fagqp6h2dpqjwjx9ab5y")))

(define-public crate-grandiorite-1.0.4 (c (n "grandiorite") (v "1.0.4") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)))) (h "040dlwfnsfi1i2rl3b61fw12mg5s8z3k548qdfyxqy5g95qhlrvm")))

