(define-module (crates-io gr an granular-id) #:use-module (crates-io))

(define-public crate-granular-id-0.1.0 (c (n "granular-id") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "03cf1iybx6ssw0g3ifnww80w4fgip09v6klh1hm3w12yhsiy3bbp")))

(define-public crate-granular-id-0.2.0 (c (n "granular-id") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "161i6bzaqcs4bkvhw0hzmfy1k37i37vy0rm4ya6v1qllbndbl1wj")))

(define-public crate-granular-id-0.2.1 (c (n "granular-id") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1bniqid5gbsxlc1b8prz7wbqzyl7dv1bi2mfmcfaf7yw5i64nas9")))

(define-public crate-granular-id-0.3.0 (c (n "granular-id") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1q7rf3jlk6arp04naxpvqpr878vks5ww3akswnij5bb9ldldq2qg")))

(define-public crate-granular-id-0.4.0 (c (n "granular-id") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1hhb10dvlfk2xbhbifmqzg6b1f9ckb9gl1jvir6f7cwwsla5q6ky")))

(define-public crate-granular-id-0.4.1 (c (n "granular-id") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1fihkd9pkqhawk4fp8v9fc20dijqmr6vb2i6clz6dpga5bnfi2j5")))

(define-public crate-granular-id-0.4.2 (c (n "granular-id") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0gvdm8cj45mkdfacicnw8l0iq00pibxlpwfr8814ry6vl2xkskjp")))

