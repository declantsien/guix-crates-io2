(define-module (crates-io gr an gran-turismo-query) #:use-module (crates-io))

(define-public crate-gran-turismo-query-0.1.0 (c (n "gran-turismo-query") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "salsa20") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1frlhjx8zn8vmi2rmld1a09nvv8y1z7lcrpws8spj0jj0cnlpy3q")))

(define-public crate-gran-turismo-query-0.1.1 (c (n "gran-turismo-query") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "salsa20") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0k27drrib1qmsc4h2an3x8ra0i97wgp8kw0laxnnfmrxq0pa4jzm") (f (quote (("json" "serde" "serde_json")))) (y #t)))

(define-public crate-gran-turismo-query-0.1.2 (c (n "gran-turismo-query") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "salsa20") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1gn8rq09393pqa2nhi58jzp6wbab7n7lsdlqwjlqphrj2j3pgwpz") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-gran-turismo-query-0.2.0 (c (n "gran-turismo-query") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "salsa20") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1szbcsr8g2z1i5p1sivnqmrjj3g5qqjq3hwlanjjivjq3ivxxyxs") (f (quote (("json" "serde" "serde_json"))))))

