(define-module (crates-io gr an grande-sonnerie) #:use-module (crates-io))

(define-public crate-grande-sonnerie-0.1.0 (c (n "grande-sonnerie") (v "0.1.0") (d (list (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "rusty_audio") (r "^1.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0kk8sjapzf1n2kv5sc9ddzash7ps0g9ylcncqp8gwd3h0r0iqjk9")))

