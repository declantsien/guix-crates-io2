(define-module (crates-io gr an granger) #:use-module (crates-io))

(define-public crate-granger-0.1.0 (c (n "granger") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "00i9dfliwy30baim7mxg8ic7f8vqrm1cdsnw6wfw958jkgaslc7r")))

