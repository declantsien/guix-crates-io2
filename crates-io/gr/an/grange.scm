(define-module (crates-io gr an grange) #:use-module (crates-io))

(define-public crate-grange-0.1.0 (c (n "grange") (v "0.1.0") (h "0qy5wgsig0rzp78c72673i2xvibl01iigg7v4dyxsjz7jl9cn7wq") (y #t)))

(define-public crate-grange-0.1.1 (c (n "grange") (v "0.1.1") (h "1cd5v9cz11a02xjjwpijpvmmch6ah5mh4d0h4vylssydnv2zyi4h") (y #t)))

(define-public crate-grange-0.2.0 (c (n "grange") (v "0.2.0") (h "18lv4wsspnijp8gpy8mc6vgf6nrs3rw3gn7hnwfzln353mn9d0jx") (y #t)))

