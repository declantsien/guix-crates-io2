(define-module (crates-io gr an granex) #:use-module (crates-io))

(define-public crate-granex-0.1.0 (c (n "granex") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0z93fjclyd4khq5vv2hg94zdhmwzqxrqla2sx2x5y0fi40ihqsbm")))

(define-public crate-granex-0.2.0 (c (n "granex") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "15kp7bxv5i3bavdc39dahai03xah51lpy1g6fgqrdlw0wmrc3a4s")))

(define-public crate-granex-0.2.1 (c (n "granex") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1dgijqwzksb0gcygiyhbamhdqw035ar23ykbrjgls0n0443jm4m3")))

(define-public crate-granex-0.2.2 (c (n "granex") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1nk594gmz8zvf67239j9l4bg0jrnip77x75841aw43fhm2j5732d")))

