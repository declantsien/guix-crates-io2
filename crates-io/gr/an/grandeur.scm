(define-module (crates-io gr an grandeur) #:use-module (crates-io))

(define-public crate-grandeur-0.0.1 (c (n "grandeur") (v "0.0.1") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)))) (h "0lamdab0i0l4ywbil8mfkd1bnyvxsrl2pzw6jxm6ba82iil1fwax") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-grandeur-0.1.0 (c (n "grandeur") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)))) (h "0ba7zkl94k3rnn43ksbwsw4gq60icf0kyrfawd6s3w7fqzls18ny") (f (quote (("std") ("nightly") ("default" "std"))))))

