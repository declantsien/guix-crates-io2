(define-module (crates-io gr an granny) #:use-module (crates-io))

(define-public crate-granny-0.0.1 (c (n "granny") (v "0.0.1") (h "0wd9dn41l70f9q10mlapk4b7vjxh1l7lhh8q2ghhkvwymaj4jqba")))

(define-public crate-granny-0.0.2 (c (n "granny") (v "0.0.2") (h "0yw9wwh0nf6hp5lzcc40p82z0byg22v1d8qh24b2gglppmdcnnc6")))

