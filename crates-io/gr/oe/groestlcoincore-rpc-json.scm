(define-module (crates-io gr oe groestlcoincore-rpc-json) #:use-module (crates-io))

(define-public crate-groestlcoincore-rpc-json-0.18.0 (c (n "groestlcoincore-rpc-json") (v "0.18.0") (d (list (d (n "groestlcoin") (r "^0.31.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0vwz03xxjpp1c5lv7gb3mz24jxryqqgs7s9ahx9ll64kls5rgadn")))

