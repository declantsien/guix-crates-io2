(define-module (crates-io gr oe groestl) #:use-module (crates-io))

(define-public crate-groestl-0.0.0 (c (n "groestl") (v "0.0.0") (h "1mrr7b3ml8gzga94amp6mw27pa9izrzg76004zq0q6qcqfb3nqp5")))

(define-public crate-groestl-0.1.0 (c (n "groestl") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1f8p7jicyfkc4ycd294028ymqkysz7xzyimhw212vm3sjz12ipyf")))

(define-public crate-groestl-0.2.0 (c (n "groestl") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.0") (d #t) (k 2)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0m4sc0xqkpn36f1pvq8zy6hbc84jk1sbmxipagww5xgn9bi1inr5")))

(define-public crate-groestl-0.2.1 (c (n "groestl") (v "0.2.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0zvrhh70a264yabmcmd5bl6dpya9i1662xdnybcv8vq6mm0cpqyj")))

(define-public crate-groestl-0.2.2 (c (n "groestl") (v "0.2.2") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "01svrvjw4rgg9c3yrz0xm2jlnz4l29pv2h71lnlbmp89jlg4f6jl")))

(define-public crate-groestl-0.3.0 (c (n "groestl") (v "0.3.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "0blajc02zaqk8n996g1r0736m60qsgvywgnpa0c0x7q4nds8m6zq")))

(define-public crate-groestl-0.7.0 (c (n "groestl") (v "0.7.0") (d (list (d (n "block-buffer") (r "= 0.2.0-tmp") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "00ysxppis5r45ybx5j8477ww5ll3m718sbm8hc44qq6kaw6rzyqx")))

(define-public crate-groestl-0.8.0 (c (n "groestl") (v "0.8.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1z0albp2i5ly2iik21r9wp8s9a4g5sysz296nw7yjmb895dpaq82") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-groestl-0.9.0 (c (n "groestl") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0iwy09jbhlknvhk23ylavj17b6s7k41j9qizlkf5h3cgkdx7hci4") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-groestl-0.10.0 (c (n "groestl") (v "0.10.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "19kqd7y40pqkp6x6s5r638nlqq867sz7pk5wh6hmg9v2g6z9xghx") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-groestl-0.10.1 (c (n "groestl") (v "0.10.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "0kbfczls871557ngpygg1dd2ldg4zl5plbr9c3yqiacjbwbgqg1l") (f (quote (("std" "digest/std") ("default" "std"))))))

