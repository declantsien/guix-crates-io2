(define-module (crates-io gr oe groestlcoinconsensus) #:use-module (crates-io))

(define-public crate-groestlcoinconsensus-2.19.1 (c (n "groestlcoinconsensus") (v "2.19.1") (d (list (d (n "cc") (r ">=1.0.36, <=1.0.41") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1m3r5wg7vbim1nghy9m6xfsy3vab7hsnf8cqav4gvs5pcl0ncayb") (f (quote (("external-secp"))))))

(define-public crate-groestlcoinconsensus-2.19.1-3 (c (n "groestlcoinconsensus") (v "2.19.1-3") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "13l4j2p0zaqj5sdznnks51jkmy7w0nfdqm732k3hfw38alvcar0i") (f (quote (("external-secp"))))))

(define-public crate-groestlcoinconsensus-2.20.1-0.5.0 (c (n "groestlcoinconsensus") (v "2.20.1-0.5.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "070v206ygs5rm6hg82c36s1jinikjk2wykj5g9vbv5llsyaad56r") (f (quote (("std") ("external-secp") ("default" "std"))))))

