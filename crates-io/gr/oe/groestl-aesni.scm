(define-module (crates-io gr oe groestl-aesni) #:use-module (crates-io))

(define-public crate-groestl-aesni-0.1.0 (c (n "groestl-aesni") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 0)))) (h "08d9c43qshvaw4mf9y6r46shqi7kl6s92xqrpn57jwypj2y0mbpn")))

(define-public crate-groestl-aesni-0.1.1 (c (n "groestl-aesni") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 0)))) (h "04fy2s16hr499y0rbffhhxms0v31n8y4zha8x58qai6dlmx52rsn")))

(define-public crate-groestl-aesni-0.1.2 (c (n "groestl-aesni") (v "0.1.2") (d (list (d (n "block-buffer") (r "^0.3.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 0)))) (h "0pigy2jfjf63bp9c5z3w1y985gknbjh2v8xlnh4ncnga2gibm6ny")))

(define-public crate-groestl-aesni-0.1.3 (c (n "groestl-aesni") (v "0.1.3") (d (list (d (n "block-buffer") (r "^0.3.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 0)))) (h "1mavphq5fy4ix3swb3yv5w9kg5y27ylg1c7ayzrqkq9a540fv30b")))

(define-public crate-groestl-aesni-0.1.4 (c (n "groestl-aesni") (v "0.1.4") (d (list (d (n "block-buffer") (r "^0.6") (d #t) (k 0)) (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "digest") (r "^0.7.1") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "0afznn540r17lva7yqlifvzsbxwffzn3z00yggwf6nyzd1dsr2mn")))

(define-public crate-groestl-aesni-0.2.0 (c (n "groestl-aesni") (v "0.2.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "digest") (r "^0.8") (d #t) (k 0)))) (h "0ilakkjfhkfdslsccbnsmwh3cmsafji2kcyc02ca36yaz7z6xpj5")))

(define-public crate-groestl-aesni-0.2.1 (c (n "groestl-aesni") (v "0.2.1") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0pwxvq52v7y88hcvg3galny9h3r0c0lb3ggjhzbl8201g3ykymlf") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-groestl-aesni-0.2.2 (c (n "groestl-aesni") (v "0.2.2") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (o #t) (d #t) (k 0)))) (h "06arq3513dy7smms2qzz5hilyqrxa0ghv72k2zqbfd5n7mijfl7y") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-groestl-aesni-0.3.0 (c (n "groestl-aesni") (v "0.3.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1dw0z8xipcmc11rk79p6p3fplzm80j6707vd7calx8hgrzcc7mbw") (f (quote (("std" "lazy_static") ("default" "std"))))))

