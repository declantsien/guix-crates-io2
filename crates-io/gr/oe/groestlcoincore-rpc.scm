(define-module (crates-io gr oe groestlcoincore-rpc) #:use-module (crates-io))

(define-public crate-groestlcoincore-rpc-0.18.0 (c (n "groestlcoincore-rpc") (v "0.18.0") (d (list (d (n "groestlcoincore-rpc-json") (r "^0.18.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "15l5rl9w4vsc149ndz13l7gz2i7w6gkri8p6whmpw5h53wllkwnl")))

