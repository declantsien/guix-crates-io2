(define-module (crates-io gr em gremlin-derive) #:use-module (crates-io))

(define-public crate-gremlin-derive-0.1.0 (c (n "gremlin-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "06925paqw25j5hnsp0k60894r1lvwi5rcva2w4zn407x5wq287fn")))

