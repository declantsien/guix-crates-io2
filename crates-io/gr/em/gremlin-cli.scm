(define-module (crates-io gr em gremlin-cli) #:use-module (crates-io))

(define-public crate-gremlin-cli-0.1.0 (c (n "gremlin-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "gremlin-client") (r "^0.6.0") (f (quote ("async-std-runtime"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rustyline") (r "^6.2.0") (d #t) (k 0)) (d (n "shellwords") (r "^1.1.0") (d #t) (k 0)) (d (n "smol") (r "^0.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1gm3apb4ybz449cdj1h6fjj3kpqqgij6vzc6k98sbcvxm7ndc105")))

