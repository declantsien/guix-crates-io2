(define-module (crates-io gr id grid_search_maze) #:use-module (crates-io))

(define-public crate-grid_search_maze-0.1.0 (c (n "grid_search_maze") (v "0.1.0") (d (list (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.2") (f (quote ("serde1"))) (d #t) (k 0)))) (h "0jbi23pq7s7vvrclmfinnv9p0dpzvf1rgbwqyfiab5r0ip4sgv0j")))

(define-public crate-grid_search_maze-0.2.0 (c (n "grid_search_maze") (v "0.2.0") (d (list (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3") (f (quote ("serde1"))) (d #t) (k 0)))) (h "107vn4743x25ajxwykmwaf83zvfzj1m64vl9kprskfbp8fsyhvzp")))

(define-public crate-grid_search_maze-0.2.1 (c (n "grid_search_maze") (v "0.2.1") (d (list (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3") (f (quote ("serde1"))) (d #t) (k 0)))) (h "0f952gryg4sg5nplawh31k9v2d8qswc3vx3a563bk3mqw0r4z2hi")))

