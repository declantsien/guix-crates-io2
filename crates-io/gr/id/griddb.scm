(define-module (crates-io gr id griddb) #:use-module (crates-io))

(define-public crate-griddb-0.6.0 (c (n "griddb") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "convert_case") (r "^0.3.0") (d #t) (k 0)) (d (n "griddbnet-sys") (r "^5.0.0") (d #t) (k 0)))) (h "01awqwrhipvlhmg9jswrvrbkl7p38xsdwskvpa4ak79m3i2ff1cg")))

