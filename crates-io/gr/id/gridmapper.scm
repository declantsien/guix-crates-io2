(define-module (crates-io gr id gridmapper) #:use-module (crates-io))

(define-public crate-gridmapper-0.1.0 (c (n "gridmapper") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "piston") (r "^0.53.1") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.81.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.69.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0dbdjinjf20gfy41d8glvv0vn7zbcnxm1j0jkvr961b9rpyd4vq3")))

