(define-module (crates-io gr id gridsim) #:use-module (crates-io))

(define-public crate-gridsim-0.1.0 (c (n "gridsim") (v "0.1.0") (h "1hwxl38d99g892vkkyid94y3nn0g7vzscf3rfbablkavs8kv105f")))

(define-public crate-gridsim-0.1.1 (c (n "gridsim") (v "0.1.1") (h "12r92xw5hk4dqdqxqawn0kq9hbaa9xv0p8x9lny1iipv9d4slgbb")))

(define-public crate-gridsim-0.1.2 (c (n "gridsim") (v "0.1.2") (h "07y8a4cchp35qgzgakm9z4haja5ch7jax4ddjq6pmbrx213k1van")))

(define-public crate-gridsim-0.1.3 (c (n "gridsim") (v "0.1.3") (d (list (d (n "rayon") (r "^1.0.1") (d #t) (k 0)))) (h "0vyz2lx8v1k952r7kx1qiyhjisc042459crwhx31zi8mkhys6kqr")))

(define-public crate-gridsim-0.2.0 (c (n "gridsim") (v "0.2.0") (d (list (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)))) (h "0wdn987bgfh8yrf30w1cbzscy5vgjs1qp7jmpf4yhsa1kzb8g5vk")))

(define-public crate-gridsim-0.2.1 (c (n "gridsim") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)))) (h "1ri8yrh4jj69ddncg687w8116whw2162rc26y3sg4viqhfzq3bg5") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.2.2 (c (n "gridsim") (v "0.2.2") (d (list (d (n "bincode") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)))) (h "0q7js1livmp6g22vvn81wa4ci7bn9ips0xvas8gskkwm2nycvj08") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.2.3 (c (n "gridsim") (v "0.2.3") (d (list (d (n "bincode") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)))) (h "08zc7wrrgf9wxxyf62wbgilchbqg8y56yn88d19cbcb8bfl66lb4") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.2.4 (c (n "gridsim") (v "0.2.4") (d (list (d (n "bincode") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)))) (h "1q2w8s8z15j46f0vdj2jcqh6j9brdcbigl557yrz2158xxg5n61r") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.3.0 (c (n "gridsim") (v "0.3.0") (d (list (d (n "bincode") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)))) (h "1xb7nskkgq34zal8h5yf7cgn0027chi2yilk9bnxrza1jbficyna") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.3.1 (c (n "gridsim") (v "0.3.1") (d (list (d (n "bincode") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)))) (h "0vhxc2yhw5w5pzv400pi0fjrk1nch8ghninlx6q9kfm4wi3r74gy") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.3.2 (c (n "gridsim") (v "0.3.2") (d (list (d (n "bincode") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "enum-iterator-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)))) (h "03g41a3qgavxmvhlygb1w5jjvhjhg0gvvqk12071l1jlnxly7pic") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.4.0 (c (n "gridsim") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (o #t) (d #t) (k 0)))) (h "0gg8z64016ac8z5kw1wl518qsm9f42hr6kbvgc93p8dzhr6hd0wd") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.5.0 (c (n "gridsim") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "enum-iterator") (r "^0.6.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (o #t) (d #t) (k 0)))) (h "0q762c52s7jfbwqx8v23bs1z0lw0yh3z9s6p0x8z31yynm0jdbd2") (f (quote (("multinode" "bincode" "serde" "itertools") ("default" "multinode"))))))

(define-public crate-gridsim-0.6.0 (c (n "gridsim") (v "0.6.0") (d (list (d (n "ndarray") (r "^0.14.0") (k 0)))) (h "111zvrs2l79520x0d3glxvn46vmkbq2lqc377y3i6yqpcy850zgs") (f (quote (("use-rayon" "ndarray/rayon") ("default" "use-rayon")))) (y #t)))

(define-public crate-gridsim-0.6.1 (c (n "gridsim") (v "0.6.1") (d (list (d (n "itertools") (r "^0.10.0") (k 0)) (d (n "ndarray") (r "^0.14.0") (k 0)))) (h "185k5hyd11hq618aq9kiylvjszckhsyx23vz20sm49m244nm4lf6") (f (quote (("use-rayon" "ndarray/rayon") ("default" "use-rayon"))))))

