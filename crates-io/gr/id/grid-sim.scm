(define-module (crates-io gr id grid-sim) #:use-module (crates-io))

(define-public crate-grid-sim-0.3.0 (c (n "grid-sim") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "ncurses") (r "^5.86.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0kfjwqxr1dl658k0yrdcbv3n17n6jmw87gq6p1fm32ydmhxss6gz") (f (quote (("binaries" "ncurses" "argparse"))))))

