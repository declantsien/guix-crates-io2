(define-module (crates-io gr id grid_plane) #:use-module (crates-io))

(define-public crate-grid_plane-0.1.0 (c (n "grid_plane") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_asset" "bevy_pbr" "bevy_core_pipeline" "bevy_winit"))) (k 0)))) (h "0pb6lv46x9fb6955dx2h3g42fv0bsk9vcg14dxg5qli2qyylbiqh")))

(define-public crate-grid_plane-0.1.1 (c (n "grid_plane") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_asset" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "1ylndkrg0l0scjwgpxm6f5sb58nzqggnlvdlz738bi1b2c8jjx25")))

(define-public crate-grid_plane-0.1.2 (c (n "grid_plane") (v "0.1.2") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_render" "bevy_asset" "bevy_pbr"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)))) (h "1m20fsn30akc6psv72jfg95477iq3y2x7il9g7rlcp78hgk6yprv")))

