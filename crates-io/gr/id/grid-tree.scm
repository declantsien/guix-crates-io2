(define-module (crates-io gr id grid-tree) #:use-module (crates-io))

(define-public crate-grid-tree-0.1.0 (c (n "grid-tree") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "ndshape") (r "^0.2") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "0dlzlcdr50fdckx6dkjv4camrvwj13g0jcjrc0scdqhjd5g9x6z3") (f (quote (("default" "glam"))))))

(define-public crate-grid-tree-0.2.0 (c (n "grid-tree") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "ndshape") (r "^0.3") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "0lki5mnhipc00cpig92zdaf8q24dhw2334mj5adgmapsf0bw50ns") (f (quote (("default" "glam"))))))

