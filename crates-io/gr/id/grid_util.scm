(define-module (crates-io gr id grid_util) #:use-module (crates-io))

(define-public crate-grid_util-0.1.0 (c (n "grid_util") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_derive2") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0pdcxvm7a7x89vc5qlgn91w7ly0a49gr5ys18pb84aj3958ya8jr")))

(define-public crate-grid_util-0.1.1 (c (n "grid_util") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_derive2") (r "^0.1") (d #t) (k 0)) (d (n "ron") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0qkn6i5a0ns7ihy6z65if23rrmjl7vvi162vpiihkxaz07z0jzrb")))

