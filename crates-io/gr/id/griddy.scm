(define-module (crates-io gr id griddy) #:use-module (crates-io))

(define-public crate-griddy-0.1.0 (c (n "griddy") (v "0.1.0") (h "0hp9fhm2nfnmhgbg9jfnwbnhwxhr6c0hi8fz06xjk2vhr5697xba")))

(define-public crate-griddy-0.1.1 (c (n "griddy") (v "0.1.1") (h "15zx3p96dj411h2y4rnrv379887wbxdz9l7a4028pbsscprfxlgc")))

(define-public crate-griddy-0.1.2 (c (n "griddy") (v "0.1.2") (h "1p48g27w12h8bjq169r3m5ysvczd521c5431xlndk99sc499p4bs")))

