(define-module (crates-io gr id grid_search_cardinal_common) #:use-module (crates-io))

(define-public crate-grid_search_cardinal_common-0.1.0 (c (n "grid_search_cardinal_common") (v "0.1.0") (d (list (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)))) (h "0rx4k3q3874rm8vzvmx84y3z4129c9syy6lfchfmpvp06bkrplvb")))

(define-public crate-grid_search_cardinal_common-0.1.1 (c (n "grid_search_cardinal_common") (v "0.1.1") (d (list (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "188340r36wmwzivz10bdfsqghdzn4pifab7lpm5pa99pj88z90b4") (f (quote (("serialize" "serde" "grid_2d/serialize" "direction/serialize"))))))

(define-public crate-grid_search_cardinal_common-0.1.2 (c (n "grid_search_cardinal_common") (v "0.1.2") (d (list (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "02a23shdrywcfpljc6675c994i28asx0n7hgwn3jwasd3lkm0dag") (f (quote (("serialize" "serde" "grid_2d/serialize" "direction/serialize"))))))

(define-public crate-grid_search_cardinal_common-0.1.3 (c (n "grid_search_cardinal_common") (v "0.1.3") (d (list (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "17piaqcsibss89rrg3wghsfl18kzf847q2knww815cxarlf4sfsr") (f (quote (("serialize" "serde" "grid_2d/serialize" "direction/serialize"))))))

(define-public crate-grid_search_cardinal_common-0.2.0 (c (n "grid_search_cardinal_common") (v "0.2.0") (d (list (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "17iqc86k1kn6szd9npmy81z0wdfjamzhdcx403g2q7p7s5wfg9dj") (f (quote (("serialize" "serde" "grid_2d/serialize" "direction/serialize"))))))

(define-public crate-grid_search_cardinal_common-0.3.0 (c (n "grid_search_cardinal_common") (v "0.3.0") (d (list (d (n "direction") (r "^0.18") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1fvab7jjyxs5gq8c235cdi84svvga7z957237php43rzci4wgcsv") (f (quote (("serialize" "serde" "grid_2d/serialize" "direction/serialize"))))))

(define-public crate-grid_search_cardinal_common-0.3.1 (c (n "grid_search_cardinal_common") (v "0.3.1") (d (list (d (n "direction") (r "^0.18") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0l10dsbpli3wz6jhksy8994ln7v50zb922livjgmwwwp4zicvcna") (f (quote (("serialize" "serde" "grid_2d/serialize" "direction/serialize"))))))

