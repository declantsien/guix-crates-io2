(define-module (crates-io gr id grid_search) #:use-module (crates-io))

(define-public crate-grid_search-0.1.0 (c (n "grid_search") (v "0.1.0") (d (list (d (n "direction") (r "^0.4.0") (d #t) (k 0)) (d (n "grid_2d") (r "^0.1.0") (d #t) (k 0)))) (h "0v7qp445nad0schd8lr19jfb87xw1d7g46g163zmg8rx577f0x2q")))

(define-public crate-grid_search-0.2.0 (c (n "grid_search") (v "0.2.0") (d (list (d (n "direction") (r "^0.8") (d #t) (k 0)) (d (n "grid_2d") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "066whvc0wd000rl112281gkh83nwa3ilywmgf7fm7814k535mjiz")))

(define-public crate-grid_search-0.3.0 (c (n "grid_search") (v "0.3.0") (d (list (d (n "direction") (r "^0.9") (d #t) (k 0)) (d (n "grid_2d") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1dpxlf6bnhzpd5b5g8nbb00j888csvcgk1bm9v9pj0z3853dcmsx")))

(define-public crate-grid_search-0.4.0 (c (n "grid_search") (v "0.4.0") (d (list (d (n "direction") (r "^0.10") (d #t) (k 0)) (d (n "grid_2d") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0iy3rv9k0sv8hkmhl9qcikkba6mkzzbw3z9g4rcwkm8669mf67ma")))

(define-public crate-grid_search-0.4.1 (c (n "grid_search") (v "0.4.1") (d (list (d (n "direction") (r "^0.10") (d #t) (k 0)) (d (n "grid_2d") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0h94m7ggk79iwlfy9v4byj10ln10nmxxci98255rpvpzjvvy0b3f")))

(define-public crate-grid_search-0.4.2 (c (n "grid_search") (v "0.4.2") (d (list (d (n "direction") (r "^0.10") (d #t) (k 0)) (d (n "grid_2d") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "06zfzgj2cbnns9mkh6i8aywyibbljc2h0wyssjnrl1hndhi2ibzy")))

(define-public crate-grid_search-0.5.0 (c (n "grid_search") (v "0.5.0") (d (list (d (n "direction") (r "^0.10") (d #t) (k 0)) (d (n "grid_2d") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "12s20v3vhyf831wk4bm4r8an3c9lm2q2y5j5p8xmmr8ddqif5mq5")))

(define-public crate-grid_search-0.6.0 (c (n "grid_search") (v "0.6.0") (d (list (d (n "direction") (r "^0.12") (d #t) (k 0)) (d (n "grid_2d") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1j34a0lh92hwgrql303vr5b2mf5a6r9zmjjghb4hh1y6crsavg10")))

(define-public crate-grid_search-0.7.0 (c (n "grid_search") (v "0.7.0") (d (list (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1f5xln58ivkdxizpmmxyx6q0swj1kpbrnzc5hgqz0rqcpgawg106")))

(define-public crate-grid_search-0.8.0 (c (n "grid_search") (v "0.8.0") (d (list (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ydymvpx9rsysf4k9pqk9gli7yzsawpjcprjz49ca9yrb8kadq5n")))

(define-public crate-grid_search-0.9.0 (c (n "grid_search") (v "0.9.0") (d (list (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "169lc03d6qc5c1yvpvk2ckfkxlp7f99lj1cd843qi6v66mx8g0nv")))

(define-public crate-grid_search-0.10.0 (c (n "grid_search") (v "0.10.0") (d (list (d (n "best") (r "^0.2") (d #t) (k 0)) (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08h7a37qgs5kapb1hx0dh9sxjsd333z3mcl95shb3q4z7b0jkl13")))

(define-public crate-grid_search-0.11.0 (c (n "grid_search") (v "0.11.0") (d (list (d (n "best") (r "^0.3") (d #t) (k 0)) (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hniapb608api5013hkvmrsgvvxrmba6m0qg3x25dh0fgf176h68")))

(define-public crate-grid_search-0.12.0 (c (n "grid_search") (v "0.12.0") (d (list (d (n "best") (r "^0.3") (d #t) (k 0)) (d (n "direction") (r "^0.14") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "036byz64bhcj4gk2hdbfmh0922rqh0m2vwdfgapvwx99bgkxlm92")))

(define-public crate-grid_search-0.13.0 (c (n "grid_search") (v "0.13.0") (d (list (d (n "best") (r "^0.3") (d #t) (k 0)) (d (n "direction") (r "^0.14") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0pn5y42m3ifri60db8c1sf4rybalbvrqnjv5vq7wf6bzbas3x0ak")))

(define-public crate-grid_search-0.14.0 (c (n "grid_search") (v "0.14.0") (d (list (d (n "best") (r "^0.6") (d #t) (k 0)) (d (n "direction") (r "^0.15") (d #t) (k 0)) (d (n "grid_2d") (r "^0.8") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "14w6j25smis8q08xpxzn8z3mxfp0z73lr3zy60gwczbn4lxspfyb")))

(define-public crate-grid_search-0.15.0 (c (n "grid_search") (v "0.15.0") (d (list (d (n "best") (r "^0.13") (d #t) (k 0)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "19cnchrkm4kgrpbizizgzafh1w41yww3icq5m6r6jp60fxfjy3pn") (f (quote (("serialize" "serde" "best/serialize" "direction/serialize" "grid_2d/serialize"))))))

(define-public crate-grid_search-0.15.1 (c (n "grid_search") (v "0.15.1") (d (list (d (n "best") (r "^0.13") (d #t) (k 0)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0zmsvmry7lw4m75c72n782knzmchy61id7bkjrdb26p1psiy1zjx") (f (quote (("serialize" "serde" "best/serialize" "direction/serialize" "grid_2d/serialize"))))))

