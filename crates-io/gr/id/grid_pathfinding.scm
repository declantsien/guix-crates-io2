(define-module (crates-io gr id grid_pathfinding) #:use-module (crates-io))

(define-public crate-grid_pathfinding-0.1.0 (c (n "grid_pathfinding") (v "0.1.0") (d (list (d (n "grid_util") (r "^0.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "1kkgn8pdyhl5gl38ffcv0w1am1m4f878m7l55pbcw417h4fssl74")))

(define-public crate-grid_pathfinding-0.1.1 (c (n "grid_pathfinding") (v "0.1.1") (d (list (d (n "grid_util") (r "^0.1.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)))) (h "05dkwzrhwk372r371802w23npzjshrfbps9c3k1hj9d7krv69yni")))

(define-public crate-grid_pathfinding-0.1.2 (c (n "grid_pathfinding") (v "0.1.2") (d (list (d (n "grid_util") (r "^0.1.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1vdq722zkhbf8krz5v4c94ivv509wppzvnb201m30idxj63xkw6h")))

