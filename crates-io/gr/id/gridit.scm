(define-module (crates-io gr id gridit) #:use-module (crates-io))

(define-public crate-gridit-0.1.0 (c (n "gridit") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 2)) (d (n "ggez") (r "^0.6.0-rc1") (d #t) (k 2)))) (h "0s64llwqzksmgmmc4qpk7199qnlw1n56ma4nfzdijcxh04f5v5cf")))

