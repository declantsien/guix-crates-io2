(define-module (crates-io gr id gridly_adapters) #:use-module (crates-io))

(define-public crate-gridly_adapters-0.1.0 (c (n "gridly_adapters") (v "0.1.0") (d (list (d (n "gridly") (r "^0.9.0") (d #t) (k 0)) (d (n "gridly_grids") (r "^0.4.0") (d #t) (k 2)))) (h "1y6v18j02ni7nx6g1w5ysz8fycgypq2wanim7y9cjb9zm9m3vmks")))

