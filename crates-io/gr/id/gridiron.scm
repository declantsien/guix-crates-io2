(define-module (crates-io gr id gridiron) #:use-module (crates-io))

(define-public crate-gridiron-0.1.0 (c (n "gridiron") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "03gw34d1172ylzmanb2d7bvry4il4ral6akda9m8imygnld0zqap") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.1.1 (c (n "gridiron") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "1bzhvvrkkyspwwdfkhjrwag5qsqxdxfdg2qs6r3kak9nh9qk2ivp") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.2.0 (c (n "gridiron") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "1jmwkw1bp12pcf4jgx5v796ww74ph534i5fd9q79vx4w38knmf80") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.3.0 (c (n "gridiron") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "07rmgmjmcrgqs9jfkql8r2w3sp3xiblqm6amhpnnac9skra3ihsy") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.4.0 (c (n "gridiron") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "139lyzw9xfw29p9lb18zgvvy5fjy5k30vmj8lg4wbc1zwwrv8rbp") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.4.1 (c (n "gridiron") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "1snskpjzz2qp42ch72b32p7qibwl1scdc46k74s9yj2bmz8v6p2x") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.5.0 (c (n "gridiron") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "1g4i7n6adr1dwnahx99n8shx60kqqf5gg9wf8hr76vijsdhi4y5v") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.5.1 (c (n "gridiron") (v "0.5.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "1676aindivhd4ah6hv1lxg28rncgfhfnz7l80llix877aiz8raqr") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.5.2 (c (n "gridiron") (v "0.5.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "1x9qjzyxwrnl8djqgzsqar7wvnbnrp0w8rxm5qs6q77jxwy7m4yr") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.6.0 (c (n "gridiron") (v "0.6.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "~0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "rand") (r "~0.5") (d #t) (k 2)))) (h "0m4wsdqj6ca13lmr916jyd4bkzfa9blq1hwsgi5qx2lxbg9jqnhm") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.7.0 (c (n "gridiron") (v "0.7.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "~0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "~0.2.11") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "1ic66v5s4c3a5iipnqa7igwj74spg71kz42hz5517qydmf7wkcxb") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.8.0 (c (n "gridiron") (v "0.8.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "~0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "~0.2.11") (d #t) (k 0)) (d (n "proptest") (r "~0.10") (d #t) (k 2)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0axs04awsrwb5f9fvfpcjb47a6l7sngl43lyqcv3xayshw7n2s9q") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.9.0 (c (n "gridiron") (v "0.9.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "criterion") (r "~0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "~0.2.11") (d #t) (k 0)) (d (n "proptest") (r "~1.0") (d #t) (k 2)) (d (n "rand") (r "~0.8.0") (d #t) (k 2)))) (h "111938732cs7kfhg6rh6lbxk75fp4pk5c16bfdb15wjzny4alicw") (f (quote (("unstable"))))))

(define-public crate-gridiron-0.10.0 (c (n "gridiron") (v "0.10.0") (d (list (d (n "criterion") (r "~0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "~0.2.11") (d #t) (k 0)) (d (n "proptest") (r "~1.0") (d #t) (k 2)) (d (n "rand") (r "~0.8.0") (d #t) (k 2)))) (h "000vlzw7z3c2067wcv45zqz2qiwaa5a29yi8biysqiars3l030zm") (f (quote (("unstable")))) (r "1.56.0")))

