(define-module (crates-io gr id grid-cell) #:use-module (crates-io))

(define-public crate-grid-cell-0.0.1 (c (n "grid-cell") (v "0.0.1") (h "0csgz933dkj022sfyaydaqzjcmndgcdn9klbd63ln79kf5ngisqc")))

(define-public crate-grid-cell-0.0.2 (c (n "grid-cell") (v "0.0.2") (h "0mi64i1rm9i6xjv3jninvckr8qhyy14arlnprrvk6xqy6nl3j4pk")))

