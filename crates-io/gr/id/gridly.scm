(define-module (crates-io gr id gridly) #:use-module (crates-io))

(define-public crate-gridly-0.1.0 (c (n "gridly") (v "0.1.0") (h "1cshbx0zv1j672jl5x6dspkkq71731hh8bhq114qx3k0nyf9v0bi")))

(define-public crate-gridly-0.2.0 (c (n "gridly") (v "0.2.0") (h "0g93059s25nh95b1dfyhk7ccdkwx28r10jw5chbb9yprmfdsys5f")))

(define-public crate-gridly-0.2.1 (c (n "gridly") (v "0.2.1") (h "103x3fvjjx334pa6vz4gg4gz5gm1bymi36yps6jrpcz3klqrb1sv")))

(define-public crate-gridly-0.5.0 (c (n "gridly") (v "0.5.0") (h "1hwnhxwiwqh4x5504x56d1dm4v8hiarf010rn6gyf2bjjxhdvs0z")))

(define-public crate-gridly-0.5.1 (c (n "gridly") (v "0.5.1") (d (list (d (n "gridly_grids") (r "^0.2.0") (d #t) (k 2)))) (h "1y4an751vywd1rrvjngx7p9l5knlgwnmx34pli9x7cmmig6ikzs2")))

(define-public crate-gridly-0.5.2 (c (n "gridly") (v "0.5.2") (d (list (d (n "gridly_grids") (r "^0.2.0") (d #t) (k 2)))) (h "0ksph3702msijyi0dd7w8jbznvgb783c11z7xl5xg40mq9klcpxf")))

(define-public crate-gridly-0.6.0 (c (n "gridly") (v "0.6.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)))) (h "0qppi912mwpnbyn4w1w4xpwzqd6g6245vxxny3n8x2qcc3ylsa8p")))

(define-public crate-gridly-0.7.0 (c (n "gridly") (v "0.7.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)))) (h "1wj28ins2ii8qm8gqjjnj9mfmky0711kgc3r8pgv9lg04ixls355")))

(define-public crate-gridly-0.7.1 (c (n "gridly") (v "0.7.1") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)))) (h "0zlyb5k5cq2vd7m7z1f5rbcbiwm3rmd42lr47r3ggcghl0q6dyk8")))

(define-public crate-gridly-0.8.0 (c (n "gridly") (v "0.8.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)))) (h "13p8y2l53ql4ywhd99kf5s4kfy35g3xyc8x4m188srab8v5d6d5m")))

(define-public crate-gridly-0.9.0 (c (n "gridly") (v "0.9.0") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)))) (h "1j85nkw13bklm60wrrq7ali83qi9w1433wqfzwwskvgiyxnzkdb9")))

(define-public crate-gridly-0.9.1 (c (n "gridly") (v "0.9.1") (d (list (d (n "cool_asserts") (r "^1.0.0") (d #t) (k 2)))) (h "17hdcig2pal52nqqhcqvah9pfbvighagw886wxnc39fj1hc31j9c")))

