(define-module (crates-io gr id grid_2d) #:use-module (crates-io))

(define-public crate-grid_2d-0.1.0 (c (n "grid_2d") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "137cvg1mk9gjspi7phk5gn7w5i5njm0vw1ghkw9lbh6aq4ld9a40")))

(define-public crate-grid_2d-0.2.0 (c (n "grid_2d") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0pf1wklj9ckhxc42mnb7ji1dczpn2rgnhq0548yidr33fk1siwgd")))

(define-public crate-grid_2d-0.3.0 (c (n "grid_2d") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fxvqydh2nnqxmpxp7iqhdkjz5bxiajf9bfg8rlzaww6cy2x73al")))

(define-public crate-grid_2d-0.4.0 (c (n "grid_2d") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1n1phckb1sk6wyck9maqxsyf5y8y1q0p4796fd8gzz8r3mnf1h5c")))

(define-public crate-grid_2d-0.5.0 (c (n "grid_2d") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zax0w56xskbzxwxahdrc2ghq8gnjwp01sdrkwnlmjs1705wq32v")))

(define-public crate-grid_2d-0.6.0 (c (n "grid_2d") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ddza7qibkwd2si7mbfb6gn2ds60b0srci8nqd0d4l1znpih25ms")))

(define-public crate-grid_2d-0.7.0 (c (n "grid_2d") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "10n8aqf9ahv2k5bmhgx4iv5k80y29xlpqrldw89ab45yj9hj8mz8")))

(define-public crate-grid_2d-0.8.0 (c (n "grid_2d") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "17c5kvcdfjwmppkh3cjqhfnfkm15849sdw9lfzl9v6yzr2x2d58z")))

(define-public crate-grid_2d-0.9.0 (c (n "grid_2d") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0nzd1773ibp3dfyna7gck9z1jdzdili83cv789dszjl23kadppjy")))

(define-public crate-grid_2d-0.9.1 (c (n "grid_2d") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0k8vqa8zwd9964bfcp3mqa3lxyx74j052rf22mc9cizkjpqfmw38")))

(define-public crate-grid_2d-0.10.0 (c (n "grid_2d") (v "0.10.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "19ypjzrxyfasai1wa2xkb5322hcyhi2wzkbd9mnbwjnp9a9b0j13")))

(define-public crate-grid_2d-0.11.0 (c (n "grid_2d") (v "0.11.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "139i7037mj7nms7cbhiyhkya9mk016njlyry7l6d50kk043kyls9")))

(define-public crate-grid_2d-0.11.1 (c (n "grid_2d") (v "0.11.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0qg72in8lvdamx5zd6imyvjw4bmd0b9146fbfs0li4pfckrrf71x")))

(define-public crate-grid_2d-0.11.2 (c (n "grid_2d") (v "0.11.2") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "00iqkycjn0p0ynwamvy1jag77lmmak4jikific25spwbkd2g71cs") (f (quote (("serialize" "serde" "coord_2d/serde"))))))

(define-public crate-grid_2d-0.11.3 (c (n "grid_2d") (v "0.11.3") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "10zdzkm0yy49d1246k0irvmnkya6skng22k89smjzd2i7f3afb7h") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.12.0 (c (n "grid_2d") (v "0.12.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0vgdwb6mpqlq056rhxnkjlmna9sp8jpa1mq508l75ll08iqg92d4") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.12.1 (c (n "grid_2d") (v "0.12.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "18db8dingj3118y24sqfjnhlw60v2q1mbiskwxp7ngqzwm3g6awx") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.12.2 (c (n "grid_2d") (v "0.12.2") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13s8nny26ip9ffnr4nykksvwdg8l6wbadla1g5w7bfb77hpqf35i") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.12.3 (c (n "grid_2d") (v "0.12.3") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "10yzykswhrrmky08186rsgabqmfpi25g6yk8nkrr0yw6jcg1cscm") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.12.4 (c (n "grid_2d") (v "0.12.4") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1n9iz92zwf1lmf785dbnjafpsbi882r3p7n9afz9hfarr7lfdpfa") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.12.5 (c (n "grid_2d") (v "0.12.5") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0p0bjf3b7a3nbj0q3j1a6najidyl886j2ihdpv8vrg188rgv7047") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.13.0 (c (n "grid_2d") (v "0.13.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "17w8pf00z33h8w9yfm8hy1y15jhwiwby1gnzr29v3gs61i10pfkg") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.13.1 (c (n "grid_2d") (v "0.13.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1s5z22dv68q0bgpvp09zfgm28nwak4kxx6nq9dlaxrl018krpy4v") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.13.2 (c (n "grid_2d") (v "0.13.2") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0jshj5l7vw1907y3prailllz3g5pik6vavdss5rn7n3q85p90jka") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.0 (c (n "grid_2d") (v "0.14.0") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1zjv1152r7inpfv8r2b6lv6ldfqwslgs27k1j5w6n6slwqnpv8vd") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.1 (c (n "grid_2d") (v "0.14.1") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0dmgwl6ncffmdvc04jdl1b81gwxhf4vjnfsfhlmhy4i3wkc5zlbr") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.2 (c (n "grid_2d") (v "0.14.2") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0gdmiqq9q3vhxkyg7nbpzjjrn50py3arf0fxwf0x4cdarg46yc56") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.3 (c (n "grid_2d") (v "0.14.3") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0d94yqikvb9p62s283rch21plcpaw72zb6jq0h5hn7r02cis19ix") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.4 (c (n "grid_2d") (v "0.14.4") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0m95i4gzmsjws0npfbmkhv899k0as7kq904lbyl1fipfa9p8f2a9") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.5 (c (n "grid_2d") (v "0.14.5") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0y4ax8yp05k6b5faclpc2d6813i75zj3ahdxq52a3mnin1vz2rx8") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.6 (c (n "grid_2d") (v "0.14.6") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0ycgbzd24p9qrznvs2f51wfjz3kfcg8j15h9z3qi8in8yk5f2dhv") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.14.7 (c (n "grid_2d") (v "0.14.7") (d (list (d (n "coord_2d") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "08yjixzgrhsa0fbgzz6hq8ix7r6bb4g1ikzp3mfm9l83jvafkvnl") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.15.0 (c (n "grid_2d") (v "0.15.0") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0rp04ib0x0vhl4g0qlrg89njdlmg14ph09n7pp9saqha5hgz9fik") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.15.1 (c (n "grid_2d") (v "0.15.1") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0r1sfr44irljf9yz4xfpnkana1rarpc5rzllvm6kdfbq077ak3ls") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.15.2 (c (n "grid_2d") (v "0.15.2") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1d4x4hzwn54yqh2vlrf27ypg64wi208aa7qd1xxsxj6zx2s6kshh") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

(define-public crate-grid_2d-0.15.3 (c (n "grid_2d") (v "0.15.3") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "16v84dm61jax3vz23g068s7id8b7lf27l17qilw3zzgdass27yvd") (f (quote (("serialize" "serde" "coord_2d/serialize"))))))

