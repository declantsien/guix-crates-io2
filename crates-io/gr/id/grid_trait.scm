(define-module (crates-io gr id grid_trait) #:use-module (crates-io))

(define-public crate-grid_trait-0.1.0 (c (n "grid_trait") (v "0.1.0") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "1xx4nplzzfmsfr3mwbzahvvq1k03wc63xgd90x9l53srhmsa7fy3") (y #t)))

(define-public crate-grid_trait-0.1.1 (c (n "grid_trait") (v "0.1.1") (d (list (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "1gg907b7cgki0f821pb6nsw5936dkgxx7c9qlai1znfic4v64kch")))

