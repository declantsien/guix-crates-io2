(define-module (crates-io gr id gridly_grids) #:use-module (crates-io))

(define-public crate-gridly_grids-0.1.0 (c (n "gridly_grids") (v "0.1.0") (d (list (d (n "gridly") (r "^0.2.0") (d #t) (k 0)))) (h "112cbf2wy6bdq6n03rw91w284zm3w77carlakhy0bls7ydp5kggx")))

(define-public crate-gridly_grids-0.2.0 (c (n "gridly_grids") (v "0.2.0") (d (list (d (n "generations") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "gridly") (r "^0.5.0") (d #t) (k 0)))) (h "10d1fg0qgjgm6zq9srw1in6q343swjl1l5l45jaw7ljr6wsskr14")))

(define-public crate-gridly_grids-0.3.0 (c (n "gridly_grids") (v "0.3.0") (d (list (d (n "generations") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "gridly") (r "^0.6.0") (d #t) (k 0)))) (h "0v3z5k4pkqzwy52r3zsly77jvhxzrlgaw1jda3887wn9ycirxdqd")))

(define-public crate-gridly_grids-0.3.1 (c (n "gridly_grids") (v "0.3.1") (d (list (d (n "gridly") (r "^0.7.0") (d #t) (k 0)))) (h "1q2amrqly111jl7b0fy1nr8wgzxwh67dgc18862mdg8afjas7b2w")))

(define-public crate-gridly_grids-0.3.2 (c (n "gridly_grids") (v "0.3.2") (d (list (d (n "gridly") (r "^0.7.0") (d #t) (k 0)))) (h "0x84sqkaxygmxq6dl80isndzizd300nck0qq6l8lcx7lhzh5jk33")))

(define-public crate-gridly_grids-0.3.3 (c (n "gridly_grids") (v "0.3.3") (d (list (d (n "gridly") (r "^0.7.0") (d #t) (k 0)))) (h "1hhaf2l1sp8jhvismah70kx9p8069jb9arz2h0xd08k4srxw237s")))

(define-public crate-gridly_grids-0.3.4 (c (n "gridly_grids") (v "0.3.4") (d (list (d (n "gridly") (r "^0.7.1") (d #t) (k 0)))) (h "1qpnq61gm2lam9fbyndn3h19wqy37q8pv8q26xrljqmqiccbfpyy")))

(define-public crate-gridly_grids-0.3.5 (c (n "gridly_grids") (v "0.3.5") (d (list (d (n "gridly") (r "^0.8.0") (d #t) (k 0)))) (h "02gsnm3mv366g2s9gg3ff3hdxx0gi439rhkdn2k3ayz9brgjcm3p")))

(define-public crate-gridly_grids-0.4.0 (c (n "gridly_grids") (v "0.4.0") (d (list (d (n "gridly") (r "^0.9.0") (d #t) (k 0)))) (h "0mlrbgcipiyqqr5ia4iri58hclx3hakja6gbx1i6n4vlk2bhcir5")))

(define-public crate-gridly_grids-0.5.0 (c (n "gridly_grids") (v "0.5.0") (d (list (d (n "brownstone") (r "^1.1.0") (d #t) (k 0)) (d (n "gridly") (r "^0.9.0") (d #t) (k 0)))) (h "1j8995mpc076fzrjv420wi5azmj14dhrxhk4q419nv803yn36x6j")))

