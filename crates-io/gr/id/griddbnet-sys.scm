(define-module (crates-io gr id griddbnet-sys) #:use-module (crates-io))

(define-public crate-griddbnet-sys-5.0.0 (c (n "griddbnet-sys") (v "5.0.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "clang-sys") (r "^1") (f (quote ("clang_6_0"))) (d #t) (k 0)))) (h "1ig42k2khl96snhlrr5aknps5zmp6v12476b42fs4z7jb6idgfa1") (f (quote (("static" "clang-sys/static") ("runtime" "clang-sys/runtime"))))))

