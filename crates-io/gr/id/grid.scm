(define-module (crates-io gr id grid) #:use-module (crates-io))

(define-public crate-grid-0.1.0 (c (n "grid") (v "0.1.0") (h "1b75dsrh00p5qbv5ixxd0r0hxsnsvcvdic0ay9im5j71hl018hnn")))

(define-public crate-grid-0.1.1 (c (n "grid") (v "0.1.1") (h "0gi8dpwfa2zk3rd3xsm0c7ykqg0nxn767y3yjrv4hrlrniwpy1mm")))

(define-public crate-grid-0.2.0 (c (n "grid") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0smm535jlljswvrqbvj397al638lpkgviwb4f1zds0akki5pihxy")))

(define-public crate-grid-0.2.1 (c (n "grid") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0r5m5hvz9f5wakvr7hl9d48i8w18pfnc6vfr7ld9x06xkhxf84zn")))

(define-public crate-grid-0.2.2 (c (n "grid") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0q9shv7qklgxaykz8fzl0s9l6ry67v3gqcfj5df27zfig7qyvrha")))

(define-public crate-grid-0.2.3 (c (n "grid") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "013kfdbpi6g1klghrkadrg3iijvjwjvfzifj8lbxj1dq3c79zqml")))

(define-public crate-grid-0.3.0 (c (n "grid") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "030szk5vmyf6qkjdq0y2rz22hcj8r8psc7j78n04mivpq76wwip6")))

(define-public crate-grid-0.4.0 (c (n "grid") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0a59wx578dqrlx6gmmfbg1r00bipx3xd8gds54jlggjn54fg7m13")))

(define-public crate-grid-0.5.0 (c (n "grid") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1x05d7lrxwkf3jdhi99vfnwhzzykgyjp2m7zyfnrzj40828rj4vn") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-grid-0.6.0 (c (n "grid") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1v4yl7ripxkazakpxmdbmwgniyfai41az5626rbhmywpw6g9kmxy") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-grid-0.7.0 (c (n "grid") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1lj12i96y0qnp9rlc5hlnl7as3jyzc7rd5yxs8b6x49nkdjd69lm") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-grid-0.8.0 (c (n "grid") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jnr0h3xw1ar5ppjqn2fl1yhqc7mkbm0i61kxgk1knskhgd97ns8") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-grid-0.8.1 (c (n "grid") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1aws5fr6iwdpld1x2zjaz017xs8wv0f1f11dhkf1f9yk8zq8pwcl") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-grid-0.9.0 (c (n "grid") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0iswdcxggyxp9m1rz0m7bfg4xacinvn78zp2fgfp0l0079x10d06") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-grid-0.10.0 (c (n "grid") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0g3j3knc7lgk4xh628dhxymjmrzi3kcff3fnw0g4b5yyn4gc1hgf") (f (quote (("std") ("default" "std"))))))

(define-public crate-grid-0.11.0 (c (n "grid") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 2)))) (h "1fgmg7354p4wjl7wy15mrhd6gc3jh1pf845yyqvvj38zipnhxw0x") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

(define-public crate-grid-0.12.0 (c (n "grid") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 2)))) (h "1k34qmjiv6gak053in9lms9gxqg34z827rcbxc739c7c85hzz10m") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

(define-public crate-grid-0.13.0 (c (n "grid") (v "0.13.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 2)))) (h "135mp5gji3r7xgmkr7vj9gc7hqa1z0mrn92r2d9qmcbxcb0zz5ni") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

(define-public crate-grid-0.14.0 (c (n "grid") (v "0.14.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 2)))) (h "10lg390k6ybbgiii84yvz10jdyqlnh1gkj5n1g3kr8f2mjfns4xy") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "std" "dep:serde"))))))

