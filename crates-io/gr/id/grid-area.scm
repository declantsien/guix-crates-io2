(define-module (crates-io gr id grid-area) #:use-module (crates-io))

(define-public crate-grid-area-1.0.0 (c (n "grid-area") (v "1.0.0") (h "0hi1i51jqbz20bskb36rzjj7w7yhyirkyfknn05q7gc23bhx24zc")))

(define-public crate-grid-area-1.0.1 (c (n "grid-area") (v "1.0.1") (h "1cxn29wdpd76nf1wmmy4nmzlbdr976v895m3y1a56szvkff82in3")))

