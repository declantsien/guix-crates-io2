(define-module (crates-io gr id gridish) #:use-module (crates-io))

(define-public crate-gridish-0.1.0 (c (n "gridish") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.7.13") (d #t) (k 0)))) (h "1xnqzsm5vjl590zcklky3nlj6jyysskcjns2ifxi2ma36fvd8npb")))

(define-public crate-gridish-0.1.1 (c (n "gridish") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.13") (d #t) (k 0)))) (h "1ckl0j6gzfm9q584ka0y49lffdyq0dbhf58m8jadq59r41dj0m29")))

(define-public crate-gridish-0.1.2 (c (n "gridish") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.13") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1f6chlvl5f328ypcvl5qb4g7vq06pvlx023l4jviwqc6n58f2ars") (f (quote (("tetrads"))))))

