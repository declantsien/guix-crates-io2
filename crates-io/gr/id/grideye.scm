(define-module (crates-io gr id grideye) #:use-module (crates-io))

(define-public crate-grideye-0.1.0 (c (n "grideye") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "0bv7dmw9pxi7fc370sc5gnfxzak1ws9zfxhjkaa5ahy2dr0mqlkd")))

(define-public crate-grideye-0.2.1 (c (n "grideye") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "1w3qa4nsgazjink47s2myhz8ns75mmaas45fk3sn8j4j8m2zrvf4")))

(define-public crate-grideye-0.2.2 (c (n "grideye") (v "0.2.2") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "10mls0nbb239m92jzw8y343gac33wwk98z1dr11bssj3nlprq713")))

(define-public crate-grideye-0.2.3 (c (n "grideye") (v "0.2.3") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "10nz7i7i01b1j8h5n888xmdwjb1cj213cbg7b30zph71bb1pwjpb")))

(define-public crate-grideye-0.2.4 (c (n "grideye") (v "0.2.4") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "libm") (r "^0.1.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "09w3m5bxqrhpff5gv8nwcfixsqvgywaxljf6wiri50xa35izfrrh")))

