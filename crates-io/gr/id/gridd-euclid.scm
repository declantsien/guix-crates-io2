(define-module (crates-io gr id gridd-euclid) #:use-module (crates-io))

(define-public crate-gridd-euclid-0.1.0 (c (n "gridd-euclid") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)))) (h "1kxzc22an1052m9wwqb260hrc5kr9n3cllx8dd0pzga8zx99z5xa")))

(define-public crate-gridd-euclid-0.1.2 (c (n "gridd-euclid") (v "0.1.2") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)))) (h "1m0rikx86cqnrnxdddxl2vp5dbci5jw37zx62vvfv9zjvj13pxjz")))

(define-public crate-gridd-euclid-0.1.3 (c (n "gridd-euclid") (v "0.1.3") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)))) (h "1fpzcb61i8p6xil03x7lx1anb2bs0qv8z7gggziqlpcn3n71qxps")))

