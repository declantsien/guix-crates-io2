(define-module (crates-io gr id grid_shapes) #:use-module (crates-io))

(define-public crate-grid_shapes-0.1.0 (c (n "grid_shapes") (v "0.1.0") (d (list (d (n "coord_2d") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1j5r9kkf478j5v8rx7n2vvziklwcfjg1qa5i4yg3i99cs7a14cyw") (f (quote (("default" "coord_2d" "grid_2d")))) (s 2) (e (quote (("serde" "dep:serde" "coord_2d/serialize" "grid_2d/serialize") ("grid_2d" "dep:grid_2d") ("coord_2d" "dep:coord_2d"))))))

(define-public crate-grid_shapes-0.1.1 (c (n "grid_shapes") (v "0.1.1") (d (list (d (n "coord_2d") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ingg9n8y8c59dw5svv07svxgn25xg1nz9h184sdrxj0dpdw2bj2") (f (quote (("default" "grid_2d")))) (s 2) (e (quote (("serde" "dep:serde" "coord_2d/serialize" "grid_2d/serialize") ("grid_2d" "coord_2d" "dep:grid_2d") ("coord_2d" "dep:coord_2d"))))))

(define-public crate-grid_shapes-0.1.2 (c (n "grid_shapes") (v "0.1.2") (d (list (d (n "coord_2d") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "142vj5ab59axwqrwsynl9x0bp34h7bpsmvhr11b2dya8igm8h6s8") (f (quote (("default" "grid_2d")))) (s 2) (e (quote (("serde" "dep:serde" "coord_2d/serialize" "grid_2d/serialize") ("grid_2d" "coord_2d" "dep:grid_2d") ("coord_2d" "dep:coord_2d"))))))

(define-public crate-grid_shapes-0.1.3 (c (n "grid_shapes") (v "0.1.3") (d (list (d (n "coord_2d") (r "^0.3") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ziqn4ss5g270hz8j0mc3nwpiyvi428hsgzabp7cr0wcs9grh17c") (f (quote (("extentions") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "coord_2d/serialize" "grid_2d/serialize"))))))

