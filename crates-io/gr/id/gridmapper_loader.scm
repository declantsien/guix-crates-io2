(define-module (crates-io gr id gridmapper_loader) #:use-module (crates-io))

(define-public crate-gridmapper_loader-0.1.0 (c (n "gridmapper_loader") (v "0.1.0") (d (list (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.81.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1wg5y9xjg3rwnplhv2crnvfln6kqvk5lg6mfyhczcajx33xh4937")))

(define-public crate-gridmapper_loader-0.1.1 (c (n "gridmapper_loader") (v "0.1.1") (d (list (d (n "piston2d-graphics") (r "^0.42.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.81.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1b77qx2pfh1fm6ya75xxgi73v88vdsnxhwic4x6cz44apikzr3kc")))

