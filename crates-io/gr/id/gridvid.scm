(define-module (crates-io gr id gridvid) #:use-module (crates-io))

(define-public crate-gridvid-0.1.0 (c (n "gridvid") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minimp4-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "openh264") (r "^0.3") (f (quote ("encoder"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)))) (h "0iw7hjz8q2g0gnvmzk9lhmvn8wg2j80ajyml95c65cyfgc9xky0m")))

(define-public crate-gridvid-0.1.1 (c (n "gridvid") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minimp4-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "openh264") (r "^0.3") (f (quote ("encoder"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)))) (h "159r2qfzqpsqinjmp4m445awbhjahqp2jy07pp922q5s5jj2xp6f")))

(define-public crate-gridvid-0.2.0 (c (n "gridvid") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openh264") (r "^0.3") (f (quote ("encoder"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1w89785bn4qlcd2f1ffxw019vy498aam8vcsrgk8hxqn67cmshy3")))

(define-public crate-gridvid-0.3.0 (c (n "gridvid") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openh264") (r "^0.3") (f (quote ("encoder"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ciqdr2bhahhw8bk0pwbab23ka6zjj9yvxvwjc5d54k9kzilfxa0")))

