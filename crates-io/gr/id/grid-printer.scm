(define-module (crates-io gr id grid-printer) #:use-module (crates-io))

(define-public crate-grid-printer-0.1.0 (c (n "grid-printer") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "01sng5xw26p7zv238z0z0i5xi6v3cwrcg3nlrk20br58vkzxhldw")))

(define-public crate-grid-printer-0.1.1 (c (n "grid-printer") (v "0.1.1") (h "06sb1bi0w4hhbxacnmbvzz4xyaz3harbgwfdxkjib9ld1kx27p4i")))

