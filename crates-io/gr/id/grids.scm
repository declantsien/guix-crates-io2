(define-module (crates-io gr id grids) #:use-module (crates-io))

(define-public crate-grids-0.1.2 (c (n "grids") (v "0.1.2") (d (list (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1l466q5l2vrlwjwc2wjimlb1ff2z4r1h9rmnh8i2dgshmyqzdizf") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-grids-0.1.3 (c (n "grids") (v "0.1.3") (d (list (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ssp4pf29xfrfhrw2yb96c5kd3r0y23qndrbay1i96jblplzqfd5") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-grids-0.1.4 (c (n "grids") (v "0.1.4") (d (list (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ipxw1jbwhn1pp5l2sscp8pjbn4ny9ab7vc63b8midxq1dvxf9rw") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-grids-0.1.5 (c (n "grids") (v "0.1.5") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nh6nrp1jmf088hczg6iz6armb7rhx849x36p88j9zrszjrgsv7k") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-grids-0.2.0 (c (n "grids") (v "0.2.0") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qcf3m4i2lhbyhidh8cl2y8w48ryiky0pz2wg72zcfnh8drwhajf") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-grids-0.2.1 (c (n "grids") (v "0.2.1") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1813s44cwycqxc79ziv83j36nj0h1kk65p0pw8hnzk252l8wxfvb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-grids-0.2.2 (c (n "grids") (v "0.2.2") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13379c197bgckx934byqalycx3bzpmhdkl3iggvzpqm2i19svkkx") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-grids-0.2.3 (c (n "grids") (v "0.2.3") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0xr55vrkf6vgixm65qk962015awbcfq91splk0x11va3rr68xn2x") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

