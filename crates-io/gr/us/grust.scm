(define-module (crates-io gr us grust) #:use-module (crates-io))

(define-public crate-grust-0.0.2 (c (n "grust") (v "0.0.2") (d (list (d (n "glib-2-0-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0ij97lvmn66ips6g34vxn762y6ihs6gbwvcscbfd6hrvp5mw16xs")))

(define-public crate-grust-0.0.3 (c (n "grust") (v "0.0.3") (d (list (d (n "glib-2-0-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.3") (d #t) (k 0)))) (h "1srzaszzmcmzgm58lvn2rgdqlrllpmayi4vdbmwdwnwj01633kgi")))

(define-public crate-grust-0.0.4 (c (n "grust") (v "0.0.4") (d (list (d (n "glib-2-0-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0cz1lm0nl5ksi6b1pgbk2mgf5dkhpk70x0ybk8v9dk7265k00w2h")))

(define-public crate-grust-0.0.5 (c (n "grust") (v "0.0.5") (d (list (d (n "glib-2-0-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.5") (d #t) (k 0)))) (h "0ydj3sypgj5amm9d6pxyw4xjb8jnr9fvmq4f74qy4bd409lz2xnq")))

(define-public crate-grust-0.0.6 (c (n "grust") (v "0.0.6") (d (list (d (n "glib-2-0-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.6") (d #t) (k 0)))) (h "15nnja9r198ck6vdj7d8kib2l4l3c10a3chl6gbk9rg1wix6ar7i")))

(define-public crate-grust-0.0.7 (c (n "grust") (v "0.0.7") (d (list (d (n "glib-2-0-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.6") (d #t) (k 0)))) (h "1d52876mb0w6pijls8mzdzdb20xdfdbgdvxdlxj5j1kwksdrs0y8")))

(define-public crate-grust-0.0.8 (c (n "grust") (v "0.0.8") (d (list (d (n "glib-2-0-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.7") (d #t) (k 0)))) (h "0ncmr5rgqaf06pgwiikbib9xc02wqx3ra94flwkfqpjciy4c2fzr")))

(define-public crate-grust-0.0.9 (c (n "grust") (v "0.0.9") (d (list (d (n "glib-2-0-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.7") (d #t) (k 0)))) (h "0hp9cj8md1ck8c4awzbj8zbxcnzkq5dfxxznn12d1xr9x2z0xwfv")))

(define-public crate-grust-0.0.10 (c (n "grust") (v "0.0.10") (d (list (d (n "glib-2-0-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.0.9") (d #t) (k 0)))) (h "11d5zrmj457ipv6livsb0zrx9972c2axi21skrqddnnvd6y33hpk")))

(define-public crate-grust-0.1.0 (c (n "grust") (v "0.1.0") (d (list (d (n "glib-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1s8qwlabrh4ap1m9pc45g89kwiyjmscja6l1w0n7zn9cd7m3g0r0")))

(define-public crate-grust-0.1.1 (c (n "grust") (v "0.1.1") (d (list (d (n "glib-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1j09pk1kxz3vm1l2pvcqnkrycpa4jxz05hk5d6kbkx2dz3kd07hq")))

(define-public crate-grust-0.1.2 (c (n "grust") (v "0.1.2") (d (list (d (n "glib-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ja8isbm8z8n0dms796iijqx4gs3ry6hx4hb7kdkx60d31sn83vx")))

(define-public crate-grust-0.1.3 (c (n "grust") (v "0.1.3") (d (list (d (n "glib-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ws7q018920ny17q80y0i7vxa5l8d2i80wzbi8wnzcdp9y4gxxqv")))

(define-public crate-grust-0.2.0 (c (n "grust") (v "0.2.0") (d (list (d (n "glib-2-0-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "126ig6h01rrlrf2ln084d5garwsamkgzac9l11wibz0ycp9m1y2f")))

(define-public crate-grust-0.3.0 (c (n "grust") (v "0.3.0") (d (list (d (n "glib-2-0-sys") (r "^0.2") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.2") (d #t) (k 0)) (d (n "gtypes") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "03f1ziyqwfxaipkkdmz19cvbbjhkyzigi1x9mww4d999mvyzhmw4")))

(define-public crate-grust-0.3.1 (c (n "grust") (v "0.3.1") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0hi0wfljaqnr98hyfjnkpv5zzi0lx7dkfmfj4nhbmx0ld8p5fzqz")))

(define-public crate-grust-0.3.2 (c (n "grust") (v "0.3.2") (d (list (d (n "glib-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.46") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00xd399yhzr2alrz16iw3lydxj5if3lrmar7w9g1lxy1nk766wn0")))

