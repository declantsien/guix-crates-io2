(define-module (crates-io gr us grus-lib) #:use-module (crates-io))

(define-public crate-grus-lib-0.1.0 (c (n "grus-lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "interim") (r "^0.1.0") (f (quote ("chrono"))) (o #t) (d #t) (k 0)) (d (n "sanakirja") (r "^1.2.16") (d #t) (k 0)))) (h "01gdig20d5cygdv5sfp4mgbmfhygifsx1pb1xcywpsl73zqz98z0")))

(define-public crate-grus-lib-0.1.1 (c (n "grus-lib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "interim") (r "^0.1.1") (f (quote ("chrono"))) (o #t) (d #t) (k 0)) (d (n "sanakirja") (r "^1.4.1") (d #t) (k 0)))) (h "043sg5f8vd4bzv3mvycri3pr882bq6wyc9dsa5k003vi0v4dabxs")))

