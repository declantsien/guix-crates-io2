(define-module (crates-io gr us grus) #:use-module (crates-io))

(define-public crate-grus-0.1.0 (c (n "grus") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fuzzydate") (r "^0.2.0") (d #t) (k 0)) (d (n "sanakirja") (r "^1.2.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0sb9qn4s1hlfdkf673csh6q9nmp6bkrpvw7kwhb6kp04g32fi9g6")))

