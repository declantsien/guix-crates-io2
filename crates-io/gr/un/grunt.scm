(define-module (crates-io gr un grunt) #:use-module (crates-io))

(define-public crate-grunt-0.0.1 (c (n "grunt") (v "0.0.1") (d (list (d (n "serenity") (r "^0.6") (d #t) (k 0)))) (h "1qp4z7z1w6kmsz8sl7aba4ws96a0yp9cr37jggs9pnjj088n1dp8") (y #t)))

(define-public crate-grunt-0.0.5 (c (n "grunt") (v "0.0.5") (h "1pia1i2ns3780nv8gfjr0vf5kkbl0rhxcp623agj91lhwh24w2rc") (y #t)))

(define-public crate-grunt-0.0.7 (c (n "grunt") (v "0.0.7") (h "06815m05kb7phcy2fjm1pflryff44jql18bdyzyd4wvrgi423n15") (y #t)))

(define-public crate-grunt-0.1.1 (c (n "grunt") (v "0.1.1") (h "1wqw8xkrx28h938p5syp7z5ipdf5lphyjw8m1277wi51q5n2d9b7") (y #t)))

(define-public crate-grunt-0.1.3 (c (n "grunt") (v "0.1.3") (h "0hj1l2r241v0jhkzii00g78zid436dz11w6r8zbam93mavmxbql9") (y #t)))

(define-public crate-grunt-0.1.4 (c (n "grunt") (v "0.1.4") (h "0a849a4bqa9bbpbwbzwhzqn1zxn61b5znskdirwgs47syacixrzs") (y #t)))

(define-public crate-grunt-0.1.77 (c (n "grunt") (v "0.1.77") (h "1vwyyp0mfww9zv31hz78sj0b99hh1wmyqvggxg97rnlsvgnpyi03") (y #t)))

(define-public crate-grunt-0.77.0 (c (n "grunt") (v "0.77.0") (h "0ar1wipi7bw0kps7bvyzh2x6syhz7ic9mg9svllvz153992h7mv4") (y #t)))

(define-public crate-grunt-7.7.18 (c (n "grunt") (v "7.7.18") (h "1scqssyn5jaxscnv8xkivgzw0yp494a9gb50hx08z2nlkm3di6h1") (y #t)))

(define-public crate-grunt-2018.7.7 (c (n "grunt") (v "2018.7.7") (h "0wg0z65kfq8kirhf06880xnar5dgc1a83qmf5s3v6bqbi2yv972a") (y #t)))

(define-public crate-grunt-2019.12.13 (c (n "grunt") (v "2019.12.13") (h "0xmrxrdw6124mf6ayx5psd85g9m6mi665bm989khfafal393h32j") (y #t)))

(define-public crate-grunt-9999.999.99 (c (n "grunt") (v "9999.999.99") (h "1vz2ii1c0pczyshn5g5mfmziil0r02gc8f6iwz5pjmxwbfl0d4np") (y #t)))

(define-public crate-grunt-9.9.9 (c (n "grunt") (v "9.9.9") (h "0p2lh5qwc7ghmprz72cx14vzkmz8bz4k2l9rdpdnqnfshy36vp0l") (y #t)))

(define-public crate-grunt-99999.99999.99999 (c (n "grunt") (v "99999.99999.99999") (h "1hxjs01yjhbmfwnjch5z8bjksw4nrivf7sf3rpn0vxvxi4869fg6") (y #t)))

(define-public crate-grunt-9999999.9999999.9999999 (c (n "grunt") (v "9999999.9999999.9999999") (h "0zy07fslmjz2gscvj07ywcv4xhhr0945wywzafw03di1p8z6n4wa") (y #t)))

(define-public crate-grunt-999999999.999999999.999999999 (c (n "grunt") (v "999999999.999999999.999999999") (h "028d919zf5rkz6kn5f3f79m9w1irm0zbl9zr5fdcxs8vh3chgiw1")))

