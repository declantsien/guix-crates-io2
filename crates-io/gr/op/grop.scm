(define-module (crates-io gr op grop) #:use-module (crates-io))

(define-public crate-grop-0.1.0 (c (n "grop") (v "0.1.0") (d (list (d (n "fgrok") (r "^1.1.0") (d #t) (k 0)) (d (n "grok") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0vxs7izfsfq722lm2lfrdl2kcxwa6k2mpsml1w7asb3xs24nq272")))

(define-public crate-grop-0.2.0 (c (n "grop") (v "0.2.0") (d (list (d (n "fgrok") (r "^1.1.0") (d #t) (k 0)) (d (n "grok") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1dlrxbzg1vc7vr0w98b7nnfj51d8k9cmsnwnjmza8ncp2n8hzhpc")))

