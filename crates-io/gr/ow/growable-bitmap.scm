(define-module (crates-io gr ow growable-bitmap) #:use-module (crates-io))

(define-public crate-growable-bitmap-0.1.0 (c (n "growable-bitmap") (v "0.1.0") (h "02la66aslha2nqv35704z8a92disha66y8agjnkkxifzpn511dkv")))

(define-public crate-growable-bitmap-0.2.0 (c (n "growable-bitmap") (v "0.2.0") (h "1d8fzslack1p3p6c5g3w4abajrmc49z8apqsmya2bcwjwyi2f9c8")))

