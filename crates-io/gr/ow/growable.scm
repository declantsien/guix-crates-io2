(define-module (crates-io gr ow growable) #:use-module (crates-io))

(define-public crate-growable-0.3.0 (c (n "growable") (v "0.3.0") (h "1dzd0856v3s3klia7a3fr58k5vpzbpwhpi63f9jx7dsc6y711wwc")))

(define-public crate-growable-0.3.1 (c (n "growable") (v "0.3.1") (h "0g159w26fbkrd0wn54v3fbvl15qczr0g5lhwphjf5zp2699fbqaz")))

(define-public crate-growable-0.3.2 (c (n "growable") (v "0.3.2") (h "11p2pf5qs6s43wpsn2lmzp6sxq6jiwxqp9fp2001dp1r51cfap9n")))

(define-public crate-growable-0.3.3 (c (n "growable") (v "0.3.3") (h "1cnf3fjdmlaninp031wiwpl9w0rsd99hcqc4ma8klach98431f3a")))

(define-public crate-growable-0.4.0 (c (n "growable") (v "0.4.0") (h "0ai1v2ndn9xfj1w2qic4aa87w464k4a5vf86kmwbc1jy7k1nwk1l")))

(define-public crate-growable-0.4.1 (c (n "growable") (v "0.4.1") (h "0dg172f5wffj1801s8yvs8n635nafb1c5bhx2c0a8d4l9hnvl32b")))

(define-public crate-growable-0.5.0 (c (n "growable") (v "0.5.0") (h "0pj779j0fvi8wvqi182j20g40pcm9p5zq22m9k704kpln20z8wzz")))

(define-public crate-growable-0.6.0 (c (n "growable") (v "0.6.0") (h "1x25lijq27qwrp0q7p5swz414dhk3lc3bfpqmy078102wg8w0qqx")))

(define-public crate-growable-0.7.0 (c (n "growable") (v "0.7.0") (h "0aavymnx2b59mk4syddqz0m0mlz9bp1b06qx11ragm73zqp3sk73")))

