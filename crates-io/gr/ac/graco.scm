(define-module (crates-io gr ac graco) #:use-module (crates-io))

(define-public crate-graco-0.1.3 (c (n "graco") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "0csqiwgj4gsb3s5lzb703gl2w3ngfgar51z68im87ja9lpnzn4n2")))

