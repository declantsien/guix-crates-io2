(define-module (crates-io gr ac grace-cli) #:use-module (crates-io))

(define-public crate-grace-cli-0.1.0 (c (n "grace-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recase") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serial_test") (r "^2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "04a67dm48wfv2sqgci3ipjm6ahkcgsgzqhmlccympqj156y08j43")))

(define-public crate-grace-cli-0.1.1 (c (n "grace-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recase") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serial_test") (r "^2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0vg3ixlq48sngyiivk6wwvwcz58pmjjvksmm5rqqkabrwq6z6zwg")))

