(define-module (crates-io gr ac grace) #:use-module (crates-io))

(define-public crate-grace-0.1.0 (c (n "grace") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "signal-stack") (r "^0.1.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0glg469f1lq4n2lbx2ak6p1jrnf9l35xs2f68w8s9lhng4h344vn")))

