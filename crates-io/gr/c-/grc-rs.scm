(define-module (crates-io gr c- grc-rs) #:use-module (crates-io))

(define-public crate-grc-rs-0.1.1 (c (n "grc-rs") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "1r6fwmj50kwd44hdh54hsgg4mc4zpfw8z839l0z55qbys66xs3qg")))

(define-public crate-grc-rs-0.1.2 (c (n "grc-rs") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "0d5nc6k3wc5hfjw6limdap3nwk3s54f7kafdp49awz9zcyd6jr95")))

(define-public crate-grc-rs-0.2.0 (c (n "grc-rs") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "08sk5s6lam20cgal2ag6jppmv0bx4yk37iaidlf0gq4r0nh6fzfg")))

(define-public crate-grc-rs-0.3.0 (c (n "grc-rs") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "0cdkicz2sri33245bdz3zfy9hcjghla5cl17yc372qn0sfr3dr76")))

(define-public crate-grc-rs-0.3.1 (c (n "grc-rs") (v "0.3.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "04v82gyld3g9ldzz3zawpghgbanigfvxsqvw0dwfsl7s9b0pbl49")))

(define-public crate-grc-rs-0.3.2 (c (n "grc-rs") (v "0.3.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "1yk706553c5g99fw90crhi30jqcc7nb4qsr4jzmmjzfm3ci1gi5k")))

