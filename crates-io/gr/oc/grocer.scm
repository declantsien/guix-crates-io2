(define-module (crates-io gr oc grocer) #:use-module (crates-io))

(define-public crate-grocer-0.1.0 (c (n "grocer") (v "0.1.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "0l610h24wcr0rvhqr7k57q8y4kzzgqsnm32kh1nl2r3xzgjfcivs")))

(define-public crate-grocer-0.1.1 (c (n "grocer") (v "0.1.1") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "0i6mxb1whgn1jcw727gpsg9fagakcrgm0aagrr7rv95rkr5iipky")))

