(define-module (crates-io gr ey greyhound) #:use-module (crates-io))

(define-public crate-greyhound-0.0.0 (c (n "greyhound") (v "0.0.0") (d (list (d (n "async-h1") (r "^2.3.3") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "http-types") (r "^2.12") (d #t) (k 0)))) (h "090rin322si707m3b8gqzf4gk460cjy18q9c63f7x8sq6dhfxghm")))

(define-public crate-greyhound-0.0.1 (c (n "greyhound") (v "0.0.1") (d (list (d (n "async-h1") (r "^2.3.3") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "http-types") (r "^2.12") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "routefinder") (r "^0.5.3") (d #t) (k 0)))) (h "0vrqf018wqjhgf2s2a6s5xdkp3cdbfcj2v3i89qvq0lnz2nrxkbf") (f (quote (("h1-server" "async-h1") ("default" "h1-server"))))))

