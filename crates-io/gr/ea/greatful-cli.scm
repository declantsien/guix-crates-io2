(define-module (crates-io gr ea greatful-cli) #:use-module (crates-io))

(define-public crate-greatful-cli-0.0.1 (c (n "greatful-cli") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "088ryj1wwlnq7i33hlhkslrc99jw8ndknvs0aay9jmkal6fz40mm") (y #t)))

(define-public crate-greatful-cli-0.0.2 (c (n "greatful-cli") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0yzd3rprkswj9w6d4nzyadx6z55pibd4w130ax6az7rk5q1vx8q2") (y #t)))

(define-public crate-greatful-cli-0.0.3 (c (n "greatful-cli") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "107dr586b004hnqxq85d2v33pjwkn564h7nrmb7bkhsr52ibbpli") (y #t)))

