(define-module (crates-io gr ea greap) #:use-module (crates-io))

(define-public crate-greap-0.1.0 (c (n "greap") (v "0.1.0") (h "1hbpxc0xzlgkm6dsfnv3dz6lcn842lafgf2smhjx4nb11k6w8mif")))

(define-public crate-greap-0.2.0 (c (n "greap") (v "0.2.0") (h "1kqksng6qymb6cgddhi3rcfzmb2aza0vixsjhbg70hvag4mwyif1")))

