(define-module (crates-io u2 #{56}# u256-literal) #:use-module (crates-io))

(define-public crate-u256-literal-0.1.0 (c (n "u256-literal") (v "0.1.0") (d (list (d (n "primitive-types") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04ipfj78bc1vl08zm4s8hqi5lafpdjg21l0ld1hv24qw1wc1br7p")))

(define-public crate-u256-literal-1.0.0 (c (n "u256-literal") (v "1.0.0") (d (list (d (n "primitive-types") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xbaapz41wnr6k4mvi9kwx6q40y4jamzhmdwlfzlw3fp2cp917a5")))

(define-public crate-u256-literal-1.1.0 (c (n "u256-literal") (v "1.1.0") (d (list (d (n "primitive-types") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02z49mibjnx3j2k276ycbczi2rn2j6nljyx3iv9igjxsk9wpgnzq")))

(define-public crate-u256-literal-1.0.1 (c (n "u256-literal") (v "1.0.1") (d (list (d (n "primitive-types") (r "^0.11") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bzfki0mrddxnhisvfm7vyp5hbwks88c0vhg5jbpbhp415layzr8")))

(define-public crate-u256-literal-1.0.2 (c (n "u256-literal") (v "1.0.2") (d (list (d (n "primitive-types") (r "^0.11") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17r31zagy296kdc2mi5vs0gvl3nhfxc28wrqmwzqjc3m7lgn3xvh")))

(define-public crate-u256-literal-1.1.2 (c (n "u256-literal") (v "1.1.2") (d (list (d (n "primitive-types") (r "^0.11") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v5g9vh55cqvpz3jzqg7xs80vzl1q5n3bvanhbizw88p2646vn4w")))

(define-public crate-u256-literal-1.2.0 (c (n "u256-literal") (v "1.2.0") (d (list (d (n "primitive-types") (r "^0.11") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x9bzpvyb3ya5w5dykl4asb3jc9calsy4wm4j6kxadm8hd20m9z1")))

(define-public crate-u256-literal-1.2.1 (c (n "u256-literal") (v "1.2.1") (d (list (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y27xjnlmxibazjj2g3m2f5wzcqzp9nihfj1rzdpqp448bc1x6n3")))

