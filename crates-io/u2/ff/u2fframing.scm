(define-module (crates-io u2 ff u2fframing) #:use-module (crates-io))

(define-public crate-u2fframing-0.1.0 (c (n "u2fframing") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1ggijdmxi05ivjb9hxrv2kzla4901vsxjxc2hqvfwsnzafd6i0fx")))

