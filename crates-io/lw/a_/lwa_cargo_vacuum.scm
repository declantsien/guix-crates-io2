(define-module (crates-io lw a_ lwa_cargo_vacuum) #:use-module (crates-io))

(define-public crate-lwa_cargo_vacuum-0.1.0 (c (n "lwa_cargo_vacuum") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dpc-pariter") (r "^0.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "1q7cbz98k7il7mp1wzr1gvdp6ahfzvaq9jgikc1ia1rqjjyrmwxq")))

(define-public crate-lwa_cargo_vacuum-0.1.1 (c (n "lwa_cargo_vacuum") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dpc-pariter") (r "^0.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "0igxjrfpz0kshp57c44qav4d02in2v8shhcd4cqkklhdamp323mh")))

(define-public crate-lwa_cargo_vacuum-0.2.0 (c (n "lwa_cargo_vacuum") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dpc-pariter") (r "^0.5") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "056mgzi2f59lnrjp38f1l04qh1d9w5ngc51dcp3pmh2g7nmxs5iy")))

