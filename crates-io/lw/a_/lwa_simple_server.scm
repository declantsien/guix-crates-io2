(define-module (crates-io lw a_ lwa_simple_server) #:use-module (crates-io))

(define-public crate-lwa_simple_server-0.1.0 (c (n "lwa_simple_server") (v "0.1.0") (d (list (d (n "actix-cors") (r "^0.6") (d #t) (k 0)) (d (n "actix-files") (r "^0.6") (d #t) (k 0)) (d (n "actix-web") (r "^4.4") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0xa6g0yip3dr8vdgdhkz944xbgw1pq034r1wkqakn4487r3z78x4")))

