(define-module (crates-io lw a_ lwa_unity_unpack) #:use-module (crates-io))

(define-public crate-lwa_unity_unpack-0.1.0 (c (n "lwa_unity_unpack") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "168hksag07ahzk9c1l921ssbl1hn774bpk1ndxzn9280hrahjxr8")))

(define-public crate-lwa_unity_unpack-0.2.0 (c (n "lwa_unity_unpack") (v "0.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "19n2v493ri4zjwvll12rmfvm9b2kvd07pn0gsb3dqghhwvyimj3z")))

(define-public crate-lwa_unity_unpack-0.2.1 (c (n "lwa_unity_unpack") (v "0.2.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "01vjmq68r7m6jxms8lhrb66bxn78n4pdylhrfa628w7irw55gcp2")))

(define-public crate-lwa_unity_unpack-0.3.0 (c (n "lwa_unity_unpack") (v "0.3.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash" "allocator-api2" "inline-more" "rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "02pch5l7y1jwbbgngg1n23a06mxg7ww2ax7g18aanwplxx43hdh6")))

(define-public crate-lwa_unity_unpack-0.4.0 (c (n "lwa_unity_unpack") (v "0.4.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "gltf") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "008plkz92869l00fr51cngkqy44hw3s1dmf3nh1l3rqakdyl5q1f")))

(define-public crate-lwa_unity_unpack-0.4.1 (c (n "lwa_unity_unpack") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "gltf") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0ys4qy0pc4bmgcgslkh94cy9x9ddz44skaianj5z3lbs0qgjs0m5")))

