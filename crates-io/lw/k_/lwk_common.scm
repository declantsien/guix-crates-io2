(define-module (crates-io lw k_ lwk_common) #:use-module (crates-io))

(define-public crate-lwk_common-0.1.0 (c (n "lwk_common") (v "0.1.0") (h "02qjrd2j8ckbybaylz6csl5b8ghqjy0cljilacv6cvlfjck5cjrm")))

(define-public crate-lwk_common-0.2.0 (c (n "lwk_common") (v "0.2.0") (d (list (d (n "elements") (r "^0.24.0") (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1m6ra4h4bjmi51rzyyq6r1258wx8fxh0ma0g1q7yccjc2rr8cqfi")))

(define-public crate-lwk_common-0.3.0 (c (n "lwk_common") (v "0.3.0") (d (list (d (n "elements") (r "^0.24.0") (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0hv3ay4m6ag4y3c99yaxdfi3kzpzz7w7imz75rlmxc90clbxffx9")))

(define-public crate-lwk_common-0.4.0 (c (n "lwk_common") (v "0.4.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "elements") (r "^0.24.0") (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "qr_code") (r "^2.0.0") (f (quote ("bmp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1pfskk6fm4b4wch6y3jj861nca55czi57fz1vxyrlq4fqas37pg6")))

(define-public crate-lwk_common-0.5.0 (c (n "lwk_common") (v "0.5.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "elements") (r "^0.24.0") (f (quote ("base64"))) (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "qr_code") (r "^2.0.0") (f (quote ("bmp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1aw233k5hxkb5ixi3zmiwl3dq3pp04dcfsj9dgrjhnvp5wxygmzn")))

(define-public crate-lwk_common-0.5.1 (c (n "lwk_common") (v "0.5.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "elements") (r "^0.24.0") (f (quote ("base64"))) (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "qr_code") (r "^2.0.0") (f (quote ("bmp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0d7n3hpcqac5ww8c36fiif393y47lqwn1hqhm7b7dfxia1mx07j5")))

(define-public crate-lwk_common-0.5.2 (c (n "lwk_common") (v "0.5.2") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "elements") (r "^0.24.0") (f (quote ("base64"))) (d #t) (k 0)) (d (n "elements-miniscript") (r "^0.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "qr_code") (r "^2.0.0") (f (quote ("bmp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1609va60wb0rwmmryj4ipi6h67k178fqzkjgv6zvc90xh5a47hxj")))

