(define-module (crates-io lw k_ lwk_ledger) #:use-module (crates-io))

(define-public crate-lwk_ledger-0.1.0 (c (n "lwk_ledger") (v "0.1.0") (h "1y4liq7nj3kxfnhwxz6fzv0vamqln1fciln4rf2hg957z5i65kbk")))

(define-public crate-lwk_ledger-0.3.0 (c (n "lwk_ledger") (v "0.3.0") (d (list (d (n "elements-miniscript") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ledger-apdu") (r "^0.10") (d #t) (k 0)) (d (n "lwk_common") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a9jxp496ia29373dpbgp441mph61zbczck9hj4zcnxawy4v7kzl")))

(define-public crate-lwk_ledger-0.3.1 (c (n "lwk_ledger") (v "0.3.1") (d (list (d (n "elements-miniscript") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ledger-apdu") (r "^0.10") (d #t) (k 0)) (d (n "lwk_common") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16ja88zl8npmm16s99g1gl79hszrqrg6l07mcpg6cy9ppz15dhng")))

