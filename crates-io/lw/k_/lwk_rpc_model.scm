(define-module (crates-io lw k_ lwk_rpc_model) #:use-module (crates-io))

(define-public crate-lwk_rpc_model-0.1.0 (c (n "lwk_rpc_model") (v "0.1.0") (h "0z6li9cmd66i2gmg0rhdgll9gn52cs7pcjpyvd11rs11pai3wkbx")))

(define-public crate-lwk_rpc_model-0.2.0 (c (n "lwk_rpc_model") (v "0.2.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "17gdgql7p6fk1rblk9nbc0m05m387nfavqqkn2v9w999a6181i6a")))

(define-public crate-lwk_rpc_model-0.3.0 (c (n "lwk_rpc_model") (v "0.3.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1ana8bfdn3mhdkyr2s3m4z76z5h6kvdj2f8z3jabsq6840fgpj1x")))

(define-public crate-lwk_rpc_model-0.4.0 (c (n "lwk_rpc_model") (v "0.4.0") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "116g50g2hxn3gvd1h3nk8y5dkz063pvvr4iw5jjjc9idwacx30z7")))

(define-public crate-lwk_rpc_model-0.4.1 (c (n "lwk_rpc_model") (v "0.4.1") (d (list (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1mvl6vbk1vi86vrx764p43p0ks9xmhxrdb235bk93kdy9cdcha4p")))

