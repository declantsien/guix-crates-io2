(define-module (crates-io lw mu lwmud) #:use-module (crates-io))

(define-public crate-lwmud-0.1.0 (c (n "lwmud") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00l1pny5za0r0lw6w3h1avr6m9k4zvabw2fr8p8zbj5nc1pi06g2")))

