(define-module (crates-io lw ex lwext4-sys) #:use-module (crates-io))

(define-public crate-lwext4-sys-0.1.0 (c (n "lwext4-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0m3wm2yym6gjjg7y1nsyqg30wqhcjdv5vmw90zw7ij8gszvn8n7y") (l "lwext4")))

