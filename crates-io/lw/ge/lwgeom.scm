(define-module (crates-io lw ge lwgeom) #:use-module (crates-io))

(define-public crate-lwgeom-0.0.0 (c (n "lwgeom") (v "0.0.0") (d (list (d (n "lwgeom-sys") (r "^0.0.0") (d #t) (k 0)))) (h "0xckp2kf81d098yv91a02ymfcin4paw0m1889jp7sai5cbfhabxm") (y #t)))

(define-public crate-lwgeom-0.0.1 (c (n "lwgeom") (v "0.0.1") (d (list (d (n "lwgeom-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0am4352kpa9kxcrhj3iwh2fplkaq0l0zhzk701i6h9drs8s8y81w") (y #t)))

(define-public crate-lwgeom-0.0.2 (c (n "lwgeom") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lwgeom-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "004sb83v0v9k9wfw96f1kndqikvxk1wxf4ims9ddizla0yy4fv3g") (y #t)))

(define-public crate-lwgeom-0.0.5 (c (n "lwgeom") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "lwgeom-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k1k1fbzkw5aygfwfqfic7dwxjkkcl9xa25d14p30r9sv1yl1ikh") (y #t)))

(define-public crate-lwgeom-0.0.6 (c (n "lwgeom") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "lwgeom-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fx7rz4nf3wm0y4cjdc3h5gyb7d64sbpnsr5054i7w9vax0ppmg8") (y #t)))

(define-public crate-lwgeom-0.0.7 (c (n "lwgeom") (v "0.0.7") (d (list (d (n "foreign-types") (r "^0.5") (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "lwgeom-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nqgpvwrzw7nl954ffjsypwgwvxjgp8hyq7ad8p44j878gd6qnlv")))

