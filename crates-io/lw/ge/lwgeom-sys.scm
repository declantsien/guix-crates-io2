(define-module (crates-io lw ge lwgeom-sys) #:use-module (crates-io))

(define-public crate-lwgeom-sys-0.0.0 (c (n "lwgeom-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0wgx7aafq8v1n81v2y3zqprgrfkgi4x8ivh5rriq0f7zqms5nxc2") (y #t)))

(define-public crate-lwgeom-sys-0.0.1 (c (n "lwgeom-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)))) (h "1wgb5jsljdjc663c98rjfnir6bzj1sg931n40v0njjdzgxsrgsfq") (y #t)))

(define-public crate-lwgeom-sys-0.0.2 (c (n "lwgeom-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qh4da5a1nmlrns499q3jk7lw6fl0vb7q0wmpgj3a704khlbmby3") (y #t)))

(define-public crate-lwgeom-sys-0.0.4 (c (n "lwgeom-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "080xsawl4crrrc8nycl269r9v4sgnrkb65y27z8pvc1qh1b1j0b7") (y #t) (l "lwgeom")))

(define-public crate-lwgeom-sys-0.0.5 (c (n "lwgeom-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0qvgyaz00jmy351imz8r44ad31cpamdkdcrlamglxv2xhv9vkbnl") (y #t) (l "lwgeom")))

(define-public crate-lwgeom-sys-0.0.7 (c (n "lwgeom-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0r2a8n2knnvid5zzli7lpxdfzqr92mzk4dzycf472nxayfdicp92") (l "lwgeom")))

