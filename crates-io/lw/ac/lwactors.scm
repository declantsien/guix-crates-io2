(define-module (crates-io lw ac lwactors) #:use-module (crates-io))

(define-public crate-lwactors-0.0.1 (c (n "lwactors") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)))) (h "1y9wvjcls0zbvz2q13zk4hsmzg78bpnvqa5xy24yy0fj8mb8q19i")))

(define-public crate-lwactors-0.0.2 (c (n "lwactors") (v "0.0.2") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)))) (h "0gc2g29fnn48xl82pjz1mjizgfh796wqgb6vzxxsdwqs3967axp6")))

(define-public crate-lwactors-0.0.3 (c (n "lwactors") (v "0.0.3") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 2)))) (h "0492kwwc9m21blfimn971mv4n4f38i0w3kashc4dar2vqwqjmzy2")))

(define-public crate-lwactors-0.0.4 (c (n "lwactors") (v "0.0.4") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 2)))) (h "12j0fscncg842pff0rns4c1mhpx7fjkgx6h789pnl2vaky2avflj")))

(define-public crate-lwactors-0.0.5 (c (n "lwactors") (v "0.0.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 2)))) (h "1ml5lpg2dpb40wvhz0000j8l0s86vpj3139ayyygkr7w6iw2nvsn")))

(define-public crate-lwactors-0.0.6 (c (n "lwactors") (v "0.0.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 2)))) (h "0dy2qvzx4mgwqsc1pqxqbkbzf6kpvqpk64kqrf9jyck2kn79rrqv")))

(define-public crate-lwactors-0.1.0 (c (n "lwactors") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "02631q2xf4kd05lp5pv69clrrdh7g93q59wccql4n5lf56vwqbrd")))

(define-public crate-lwactors-0.1.1 (c (n "lwactors") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core"))) (o #t) (d #t) (k 0)))) (h "14a72qmq1hv2c5sr82b9vidpsan1rq6msa9smxb5qxywbzjimpyw")))

(define-public crate-lwactors-0.2.0 (c (n "lwactors") (v "0.2.0") (d (list (d (n "async-global-executor14") (r "^1.4") (o #t) (d #t) (k 0) (p "async-global-executor")) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio02") (r "^0.2") (f (quote ("rt-core"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio10") (r "^1.0") (f (quote ("rt"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "151gz5fpx9p6h0qcplid3ja71901p6jfpfpr5brnbwms4j2zpa4f") (f (quote (("with_tokio10" "tokio10" "__tokio" "__global_executor") ("with_tokio02" "tokio02" "__tokio" "__global_executor") ("with_async_global_executor14" "async-global-executor14" "__global_executor") ("__tokio") ("__global_executor"))))))

