(define-module (crates-io y8 #{6-}# y86-lib) #:use-module (crates-io))

(define-public crate-y86-lib-0.1.0 (c (n "y86-lib") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0b4dy1gp7vmd2za9wpfnfg838n6mlzpgm5mi5wvsjxcvjrlw71mw")))

