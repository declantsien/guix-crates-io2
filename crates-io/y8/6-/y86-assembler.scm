(define-module (crates-io y8 #{6-}# y86-assembler) #:use-module (crates-io))

(define-public crate-y86-assembler-0.1.0 (c (n "y86-assembler") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hxgqizyfgavavni2raysjq8d3r2k85mz6az7ivn3i2fkn6k8c16")))

(define-public crate-y86-assembler-0.2.0 (c (n "y86-assembler") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1j15yr4x0870f1knn7gv3azckdgy4izkxjhb7cvapi5n6xlc1dxh")))

(define-public crate-y86-assembler-0.3.0 (c (n "y86-assembler") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "191fsp8yf0n6anz5fhm0z31zmni96d1cmhbymm30wa40b2z598w0")))

