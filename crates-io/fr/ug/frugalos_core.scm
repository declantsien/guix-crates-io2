(define-module (crates-io fr ug frugalos_core) #:use-module (crates-io))

(define-public crate-frugalos_core-0.1.0 (c (n "frugalos_core") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "trackable") (r "^0.2.21") (d #t) (k 0)))) (h "0s7p45qg3a5dmi00xrfvd0jv97784zq0p3lzrvnxccg7cf6nrc43")))

(define-public crate-frugalos_core-0.1.1 (c (n "frugalos_core") (v "0.1.1") (d (list (d (n "rustracing") (r "^0.1") (d #t) (k 0)) (d (n "rustracing_jaeger") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "trackable") (r "^0.2.21") (d #t) (k 0)))) (h "10bfb344af0cnxpnp3g5lrac26dsfjsv8w3anlmhjjxgi67dcvmm")))

(define-public crate-frugalos_core-1.0.0 (c (n "frugalos_core") (v "1.0.0") (d (list (d (n "rustracing") (r "^0.1") (d #t) (k 0)) (d (n "rustracing_jaeger") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "trackable") (r "^0.2.21") (d #t) (k 0)))) (h "0flilxnnkawf57anh78krh074xbln2kqxjqsbr4i5cvfzcc4za86")))

(define-public crate-frugalos_core-1.2.0 (c (n "frugalos_core") (v "1.2.0") (d (list (d (n "rustracing") (r "^0.1") (d #t) (k 0)) (d (n "rustracing_jaeger") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "trackable") (r "^0.2.21") (d #t) (k 0)))) (h "1lplcmb7ff4qpxkab6cpq99vywqs5l7nqlsck1zb63la224s6cgd")))

