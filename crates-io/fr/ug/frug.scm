(define-module (crates-io fr ug frug) #:use-module (crates-io))

(define-public crate-frug-0.1.0 (c (n "frug") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.14") (d #t) (k 0)))) (h "0zcgnwj8c8nnqhlq7dyci386nklsy2d5qjwix0b02rx12gibhhf8")))

