(define-module (crates-io fr ns frnsc-liveregistry-rs) #:use-module (crates-io))

(define-public crate-frnsc-liveregistry-rs-0.1.0 (c (n "frnsc-liveregistry-rs") (v "0.1.0") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03d9l8d1pm3r65zrfrqvshvlfswdmckmrh7gkqzqw2n2d2ajx9bn")))

(define-public crate-frnsc-liveregistry-rs-0.1.2 (c (n "frnsc-liveregistry-rs") (v "0.1.2") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "16zb0r5ihx9g00mqm0hzfbcgyv6gwbyy395jxmyb9cnm5j00i9fd")))

(define-public crate-frnsc-liveregistry-rs-0.2.0 (c (n "frnsc-liveregistry-rs") (v "0.2.0") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0iqnrlqv4jv6l00fc6ycnszgj4zhz8ygwqrp9z2cwzhjxkd79qzx")))

(define-public crate-frnsc-liveregistry-rs-0.2.1 (c (n "frnsc-liveregistry-rs") (v "0.2.1") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "13w5z4m7wc8kbavrrb9qbiapn0gdz30zgyynkfyymvjmb3ma6kyb")))

(define-public crate-frnsc-liveregistry-rs-0.2.2 (c (n "frnsc-liveregistry-rs") (v "0.2.2") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "02v0nksmxsihdksm2a68hcn85sy83bxws7f4dxljc5yizgb5k918")))

(define-public crate-frnsc-liveregistry-rs-0.7.0 (c (n "frnsc-liveregistry-rs") (v "0.7.0") (d (list (d (n "forensic-rs") (r "^0.7") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1gqrcj389q0a1w1rc06gsc6wdwf4r8jr18kiiyxii0909nz6wd00")))

(define-public crate-frnsc-liveregistry-rs-0.8.0 (c (n "frnsc-liveregistry-rs") (v "0.8.0") (d (list (d (n "forensic-rs") (r "^0.8") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0r0grdccxn0dfaq0cdaxq5136aal0pjai77m37sciwag2fi3vrnx")))

(define-public crate-frnsc-liveregistry-rs-0.9.0 (c (n "frnsc-liveregistry-rs") (v "0.9.0") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0cr9rikc9abk4v9sz4r90lcl2xp6hg11hhvh1rvy83w8rac7ds52")))

(define-public crate-frnsc-liveregistry-rs-0.9.1 (c (n "frnsc-liveregistry-rs") (v "0.9.1") (d (list (d (n "forensic-rs") (r "^0.9") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1lilkq7wqaylwv4zslgwn2g95vb1290k4km135qm7ywzmv2a2gbm")))

(define-public crate-frnsc-liveregistry-rs-0.12.1 (c (n "frnsc-liveregistry-rs") (v "0.12.1") (d (list (d (n "forensic-rs") (r "^0.12") (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "02lk488r4mkhj435m6m6jjd1by7925hk3d32qgphvbvg0qjhpqwh")))

(define-public crate-frnsc-liveregistry-rs-0.13.0 (c (n "frnsc-liveregistry-rs") (v "0.13.0") (d (list (d (n "forensic-rs") (r "^0.13") (d #t) (k 0)) (d (n "windows") (r "^0.53") (f (quote ("Win32_Foundation" "Win32_System_Registry"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "01yfhd2dxz7aspmr14553y1hx56jck982nqvbaggpbjq2504sy62")))

