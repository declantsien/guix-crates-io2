(define-module (crates-io fr ns frnsc-sqlite) #:use-module (crates-io))

(define-public crate-frnsc-sqlite-0.1.0 (c (n "frnsc-sqlite") (v "0.1.0") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)))) (h "1q83qjawj4vq9jrwlnzycshc8wg3qcim5sxq5plghadcm88sz8sa")))

