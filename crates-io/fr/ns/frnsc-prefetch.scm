(define-module (crates-io fr ns frnsc-prefetch) #:use-module (crates-io))

(define-public crate-frnsc-prefetch-0.9.0 (c (n "frnsc-prefetch") (v "0.9.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "forensic-rs") (r "^0.9.0") (d #t) (k 0)))) (h "0bram9nyi3fr6xy85xr79lvxgdibdk4vrnmjhrs2cwqj14f17lif")))

(define-public crate-frnsc-prefetch-0.9.1 (c (n "frnsc-prefetch") (v "0.9.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "forensic-rs") (r "^0.9") (d #t) (k 0)))) (h "1nbydms9k20sn2l479wh1a0sjmqmf1lqm64ldcmy6hji5724ik68")))

(define-public crate-frnsc-prefetch-0.9.2 (c (n "frnsc-prefetch") (v "0.9.2") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "forensic-rs") (r ">=0.9") (d #t) (k 0)))) (h "0xsj85q5mj7pdch4sjqlvh07g4xfjfwyq3rlphp9lqd1494p5g1c")))

(define-public crate-frnsc-prefetch-0.13.0 (c (n "frnsc-prefetch") (v "0.13.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "forensic-rs") (r "^0.13") (d #t) (k 0)))) (h "1ppphnk82qwp8jalxd1a7pll0ifzakgvkr2y7vbl6yc1mllw1s7v")))

