(define-module (crates-io fr ns frnsc-hive) #:use-module (crates-io))

(define-public crate-frnsc-hive-0.8.0 (c (n "frnsc-hive") (v "0.8.0") (d (list (d (n "forensic-rs") (r "^0.8") (d #t) (k 0)))) (h "1bdl3zz1vsq7vg09vz7a544iy23cpldgswnayc1608mrjx2y828f") (f (quote (("default"))))))

(define-public crate-frnsc-hive-0.9.0 (c (n "frnsc-hive") (v "0.9.0") (d (list (d (n "forensic-rs") (r "^0.9") (d #t) (k 0)))) (h "0dl3696l344k5y9xw3y7p419mvdcgjgixmns3ydq8l5ki8jhgkh6") (f (quote (("default"))))))

(define-public crate-frnsc-hive-0.13.0 (c (n "frnsc-hive") (v "0.13.0") (d (list (d (n "forensic-rs") (r "^0.13") (d #t) (k 0)))) (h "1l9zkchn6lnwbpinpw1arrskwsyh6lf5f8lhwmfsvxz7m2mn6nvr") (f (quote (("default"))))))

