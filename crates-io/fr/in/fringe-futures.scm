(define-module (crates-io fr in fringe-futures) #:use-module (crates-io))

(define-public crate-fringe-futures-0.1.0 (c (n "fringe-futures") (v "0.1.0") (d (list (d (n "fringe") (r "^1.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)))) (h "00incm1qjm2bym9410lhw3avzdzci7h9yd7k2brf9xdkvab2dbii") (y #t)))

