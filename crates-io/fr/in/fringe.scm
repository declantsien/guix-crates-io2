(define-module (crates-io fr in fringe) #:use-module (crates-io))

(define-public crate-fringe-1.0.0 (c (n "fringe") (v "1.0.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (t "cfg(windows)") (k 0)))) (h "0417wm7c8km6bgmnda2j0723bw27ib4i3pdnlch417kf6zzcpq4q") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.0.1 (c (n "fringe") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0wgc6nl83wddb787yifn144shc6aycva6bb5fx9f9nybqj52sxk3") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.0.2 (c (n "fringe") (v "1.0.2") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "06ma8g44ybrw6whny78jq3sgxsfhbxx9xqji55cj4wzqmalgw4kr") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.0.3 (c (n "fringe") (v "1.0.3") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1vb9cw6kdxvp6lh7c9lxrsvrz4nn150r3ppaqx8w6vyqds8maihp") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.0.4 (c (n "fringe") (v "1.0.4") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0a1n37yrfjsc34afcx154ymi2ydipmxv44zg68hggzmy48mlj7rz") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.0.5 (c (n "fringe") (v "1.0.5") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0mbrq52rk8dbanp4fkn7ba8vl9yzbh9q58w53a7zrpvf0y2pc9f5") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.1.0 (c (n "fringe") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "12np8ykc8l63pxk2kmc6zdikwj7c85b8cxxlfs6ywpnqzbf8jxlq") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.2.0 (c (n "fringe") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1mbr4bzbvlfzac79babd1647ms7474ylhhaf9j51ccdx8v5bv16i") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1.2.1 (c (n "fringe") (v "1.2.1") (d (list (d (n "libc") (r "^0.2.14") (d #t) (t "cfg(unix)") (k 0)) (d (n "simd") (r "^0.1") (d #t) (k 2)) (d (n "valgrind_request") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0wbj58qvr849aml31qx59vhib9dry037qf8gl34jk77h2n2svb0y") (f (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

