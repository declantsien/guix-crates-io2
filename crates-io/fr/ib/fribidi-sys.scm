(define-module (crates-io fr ib fribidi-sys) #:use-module (crates-io))

(define-public crate-fribidi-sys-0.1.0 (c (n "fribidi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "06qlyqsckibrlynimkfck4cjr29i2jyz4z6931dcj0sh6mgk66bn") (f (quote (("static") ("default" "pkg-config")))) (l "fribidi")))

