(define-module (crates-io fr ib fribidi) #:use-module (crates-io))

(define-public crate-fribidi-0.1.0 (c (n "fribidi") (v "0.1.0") (d (list (d (n "fribidi-sys") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0i961kfqbkskyis2258pr1a5cljjxhzwrmrai5ajh99mxgw8y879") (f (quote (("static" "fribidi-sys/static") ("default" "pkg-config"))))))

(define-public crate-fribidi-0.1.1 (c (n "fribidi") (v "0.1.1") (d (list (d (n "fribidi-sys") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1wq7ywrvjxfpz3pxk34was0yk5246v9mfljnr7ksfff15yjgk7qg") (f (quote (("static" "fribidi-sys/static") ("default" "pkg-config"))))))

(define-public crate-fribidi-0.1.2 (c (n "fribidi") (v "0.1.2") (d (list (d (n "fribidi-sys") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1wrys6cy8gyyl48gp530vhf9h9ngljrycn0xbpsjrw5cah66i815") (f (quote (("static" "fribidi-sys/static") ("default" "pkg-config"))))))

