(define-module (crates-io fr ic fricgan) #:use-module (crates-io))

(define-public crate-fricgan-0.1.0 (c (n "fricgan") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "05n05k0snxvhw9dgwhj3qpmx325x862cgkvwby0cyjbyhs3zaizh") (f (quote (("vlq-string" "std" "vlq" "num-traits" "num-traits/std") ("vlq-64" "vlq") ("vlq-32" "vlq") ("vlq") ("unsafe") ("std") ("safety-checks") ("io-u8") ("io-u64") ("io-u32") ("io-u16") ("io-string" "std" "num-traits" "num-traits/std") ("io-i8") ("io-i64") ("io-i32") ("io-i16") ("io-f64") ("io-f32") ("default"))))))

