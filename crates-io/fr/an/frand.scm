(define-module (crates-io fr an frand) #:use-module (crates-io))

(define-public crate-frand-0.7.0 (c (n "frand") (v "0.7.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "13q726rxp6byivkip76j0fx0br277d17kws1vsx6h85inlgr8nhi")))

(define-public crate-frand-0.7.1 (c (n "frand") (v "0.7.1") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1x6j54gzp4i8dzgpyqnpwi9cmvb59bj7zgky7iw90x8djvhvigcc")))

(define-public crate-frand-0.7.2 (c (n "frand") (v "0.7.2") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1anac322jsrcyz24ks1h0idpp14vaz1jnxvwfc79jcmzvklf49aa") (y #t)))

(define-public crate-frand-0.7.3 (c (n "frand") (v "0.7.3") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1n2cn8asf55h829v5acrmw5nfjmqck7zvhl667isic75hmgbcwpb")))

(define-public crate-frand-0.8.0 (c (n "frand") (v "0.8.0") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "glam") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "wyhash") (r "^0.5.0") (d #t) (k 2)))) (h "13cvy44v49fsgxah04s54jzdgn2jidmhkz39cvr2jqij0si6pac3") (f (quote (("default" "glam")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-frand-0.8.1 (c (n "frand") (v "0.8.1") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "glam") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "wyhash") (r "^0.5.0") (d #t) (k 2)))) (h "1qay4y15gpgkr1103plnavpdcc7bzm50vgyqbrzs3l367xl9p21j") (f (quote (("std" "alloc") ("default" "glam" "std" "alloc") ("alloc")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-frand-0.9.0 (c (n "frand") (v "0.9.0") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "glam") (r "^0") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.5") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1hr8l2z1bddsq3zqrdmb7d9vhx8w3gsjcjmb1bxdnfslp4qmpgq8") (f (quote (("std") ("default" "glam" "std")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-frand-0.10.0 (c (n "frand") (v "0.10.0") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "glam") (r "^0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "14p6pyz1rav52r5g40lmmhwbha9znxrsqyqmhnsma7fqfc83bjs1") (f (quote (("std") ("default" "glam" "std" "impl_rng_core")))) (s 2) (e (quote (("impl_rng_core" "dep:rand_core") ("glam" "dep:glam"))))))

