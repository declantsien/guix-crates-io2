(define-module (crates-io fr an frank) #:use-module (crates-io))

(define-public crate-frank-0.1.0 (c (n "frank") (v "0.1.0") (h "1077n5pbmhhfa6l451pr90m9rj83facdm86in6gwh5pd7lp633a2")))

(define-public crate-frank-0.1.1 (c (n "frank") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "077m1grgpjhjd16lj6yzmsk2sw2v0rlf7gj5n238qw364l8x510p")))

(define-public crate-frank-0.1.2 (c (n "frank") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1wpr77kn9mymn6w7jxj4fzbq16vg0pylxk7bqm3pnqq3fqhlifx2")))

(define-public crate-frank-0.1.3 (c (n "frank") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "17m8alq0bfmyn2z7q9cp9fcp7l3p05nrvhbksliwai4kfk277ih6")))

