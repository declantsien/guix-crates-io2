(define-module (crates-io fr an frankenweenie) #:use-module (crates-io))

(define-public crate-frankenweenie-0.1.0 (c (n "frankenweenie") (v "0.1.0") (d (list (d (n "frankenstein") (r "^0.24.1") (f (quote ("async-http-client"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pjpplwcgkcgbhsc9ijsqx4jkf1w2cydd21f0yfm04qbhlwxg2ar")))

(define-public crate-frankenweenie-0.1.1 (c (n "frankenweenie") (v "0.1.1") (d (list (d (n "frankenstein") (r "^0.24.1") (f (quote ("async-http-client"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "18m9sm864g1gaicrsxam109c4az6v927nkdc4sr9q7vjvz2zwpwd")))

