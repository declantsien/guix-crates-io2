(define-module (crates-io fr et fretboard_cli) #:use-module (crates-io))

(define-public crate-fretboard_cli-1.0.0 (c (n "fretboard_cli") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1vscx2zmjcrykrp35zpim8yfs6ln9m6cfisgxsn913hyd01xgn23")))

