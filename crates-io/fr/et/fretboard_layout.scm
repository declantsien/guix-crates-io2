(define-module (crates-io fr et fretboard_layout) #:use-module (crates-io))

(define-public crate-fretboard_layout-0.1.1 (c (n "fretboard_layout") (v "0.1.1") (d (list (d (n "rug") (r "1.12.*") (f (quote ("float"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "0.10.*") (d #t) (k 0)))) (h "19fvfg8p1zdr4s0srx30130wjlhgyihsfcg23s01qybdsvlnk1zi")))

(define-public crate-fretboard_layout-0.1.2 (c (n "fretboard_layout") (v "0.1.2") (d (list (d (n "rug") (r "1.12.*") (f (quote ("float"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "0.10.*") (d #t) (k 0)))) (h "1f9rm230xghq6bqg5ifb12zj0lq234pi9h6a60hf22mjq7ylqni0")))

(define-public crate-fretboard_layout-0.1.8 (c (n "fretboard_layout") (v "0.1.8") (d (list (d (n "hex") (r "~0.4") (f (quote ("std"))) (k 0)) (d (n "rgba_simple") (r "~0.3") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "~0.10") (d #t) (k 0)))) (h "1am7zxr1pmdrdrb58865gfn986asjmvcb7r9ix4jkknzn7zn2n2x")))

(define-public crate-fretboard_layout-0.1.9 (c (n "fretboard_layout") (v "0.1.9") (d (list (d (n "rgba_simple") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "~0.10") (d #t) (k 0)))) (h "1cvmvlfwglw1n3h82cb12zr1pggl7jrzmmd6da7nnfk764pck6b0")))

(define-public crate-fretboard_layout-0.2.0 (c (n "fretboard_layout") (v "0.2.0") (d (list (d (n "rgba_simple") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "~0.10") (d #t) (k 0)))) (h "1ggbfp94vrc9k2m4v76dqympixrnx8pcxdzclfjapjk2xa7fg31m")))

(define-public crate-fretboard_layout-0.3.0 (c (n "fretboard_layout") (v "0.3.0") (d (list (d (n "rgba_simple") (r "~0.5") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "~0.10") (d #t) (k 0)))) (h "0fjm0xqdhpjkn0z8bv10gcz9cp6rlh0fr3zfcs4d0s42ys2c2gp5")))

(define-public crate-fretboard_layout-0.3.1 (c (n "fretboard_layout") (v "0.3.1") (d (list (d (n "rgba_simple") (r "~0.5") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "~0.10") (d #t) (k 0)))) (h "0svx8c6p2gmyg5hv4i1f2y80cpi7h568j6q7yqdizm32vkl6ghzd")))

(define-public crate-fretboard_layout-0.5.0 (c (n "fretboard_layout") (v "0.5.0") (d (list (d (n "pango") (r "^0.16") (o #t) (d #t) (k 0) (p "pango")) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rgba_simple") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "svg") (r "^0.10") (d #t) (k 0)))) (h "0is8889v44g75z8v2a1drvwkm4x464gbw0fnhdxpqpcsv9y7hi16") (f (quote (("gdk" "rgba_simple/gdk")))) (s 2) (e (quote (("serde" "rgba_simple/serde" "dep:serde"))))))

(define-public crate-fretboard_layout-0.5.1 (c (n "fretboard_layout") (v "0.5.1") (d (list (d (n "pango") (r "^0.16") (o #t) (d #t) (k 0) (p "pango")) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rgba_simple") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "svg") (r "^0.12") (d #t) (k 0)))) (h "1ldf0yphvh9f65lf451ac3r51fvj0qh98bw7aw2laqjxhk2cbsaq") (f (quote (("gdk" "rgba_simple/gdk")))) (s 2) (e (quote (("serde" "rgba_simple/serde" "dep:serde"))))))

(define-public crate-fretboard_layout-0.6.0 (c (n "fretboard_layout") (v "0.6.0") (d (list (d (n "pango") (r "^0.17") (o #t) (d #t) (k 0) (p "pango")) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rgba_simple") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "0ba8n1rcw8386b537igg1f1715zk82px8qbz7dhwgg47wsk3jhqy") (f (quote (("gdk" "rgba_simple/gdk")))) (s 2) (e (quote (("serde" "rgba_simple/serde" "dep:serde"))))))

(define-public crate-fretboard_layout-0.7.0 (c (n "fretboard_layout") (v "0.7.0") (d (list (d (n "pango") (r "^0.18") (o #t) (d #t) (k 0) (p "pango")) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rgba_simple") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "0h3jxiwksyv795fxz3a3pqj86n3zpd5ygy407vxr1vvwxh89p2qj") (f (quote (("gdk" "rgba_simple/gdk")))) (s 2) (e (quote (("serde" "rgba_simple/serde" "dep:serde"))))))

(define-public crate-fretboard_layout-0.8.0 (c (n "fretboard_layout") (v "0.8.0") (d (list (d (n "pango") (r "^0.18") (o #t) (d #t) (k 0) (p "pango")) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rgba_simple") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "svg") (r "^0.14") (d #t) (k 0)))) (h "0g1bd6pxq6np7dllnwqv3by27fqmr8s0nps7wyyb8cfd96a20w0n") (f (quote (("gdk" "rgba_simple/gdk")))) (s 2) (e (quote (("serde" "rgba_simple/serde" "dep:serde"))))))

(define-public crate-fretboard_layout-0.7.1 (c (n "fretboard_layout") (v "0.7.1") (d (list (d (n "pango") (r "^0.18") (o #t) (d #t) (k 0) (p "pango")) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "rgba_simple") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "svg") (r "^0.14") (d #t) (k 0)))) (h "11sbjhx67sfwa53l3c82xbggiai6a43x3lsdvja56lgq3p0gnnf0") (f (quote (("gdk" "rgba_simple/gdk")))) (s 2) (e (quote (("serde" "rgba_simple/serde" "dep:serde"))))))

