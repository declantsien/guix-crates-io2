(define-module (crates-io fr ob frob-volume) #:use-module (crates-io))

(define-public crate-frob-volume-0.1.0 (c (n "frob-volume") (v "0.1.0") (d (list (d (n "fauxpas") (r "^1.0.51") (d #t) (k 0)) (d (n "libpulse-binding") (r "^2.25.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "19aai1g50jwip1fqlzbdzdx1pvp1zjl1hdz462im8y4xa42biic5")))

(define-public crate-frob-volume-0.1.1 (c (n "frob-volume") (v "0.1.1") (d (list (d (n "fauxpas") (r "^1.0.51") (d #t) (k 0)) (d (n "libpulse-binding") (r "^2.25.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0i15jl8hyaafm0qrvabshfv4ip190mndrfyjmmgdi0kxnccd593a")))

(define-public crate-frob-volume-0.1.2 (c (n "frob-volume") (v "0.1.2") (d (list (d (n "fauxpas") (r "^1.0.51") (d #t) (k 0)) (d (n "libpulse-binding") (r "^2.25.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "11p05nia2k9jb8xk115sddjfd42zcms8q612x4inrapnzzc102bj")))

