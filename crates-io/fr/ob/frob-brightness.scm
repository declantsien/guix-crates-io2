(define-module (crates-io fr ob frob-brightness) #:use-module (crates-io))

(define-public crate-frob-brightness-0.1.0 (c (n "frob-brightness") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "fauxpas") (r "^1.0.51") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0zf0wxnmd3b21adrw3srzypy674r4lglk6rxfvh6cm6q5hpai445")))

(define-public crate-frob-brightness-0.1.1 (c (n "frob-brightness") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "fauxpas") (r "^1.0.51") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0kbkss0gfsd3snn3gxc991s8craw74rh87h6ydk0z7vz1656gjcd")))

(define-public crate-frob-brightness-0.1.2 (c (n "frob-brightness") (v "0.1.2") (d (list (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "fauxpas") (r "^1.0.51") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "01n154csixrbp6xy3q4kb5jlfnmdby9w935gbrdvn4id2p05isa0")))

