(define-module (crates-io fr ob frob) #:use-module (crates-io))

(define-public crate-frob-0.1.0 (c (n "frob") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "frob-brightness") (r "^0.1.0") (d #t) (k 0)) (d (n "frob-monitor") (r "^0.1.0") (d #t) (k 0)) (d (n "frob-volume") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1x01pvaz8ghf41fpfqqk3d6966rlydghk187iiyjndxk0q62g6s1")))

(define-public crate-frob-0.1.1 (c (n "frob") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "frob-brightness") (r "^0.1.0") (d #t) (k 0)) (d (n "frob-monitor") (r "^0.1.0") (d #t) (k 0)) (d (n "frob-volume") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1g744pv5g91sfcfk9nbc0ywq1dr48g98cwpm34wfl7k7ikc0v10g")))

(define-public crate-frob-0.1.2 (c (n "frob") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "frob-brightness") (r "^0.1.0") (d #t) (k 0)) (d (n "frob-monitor") (r "^0.1.0") (d #t) (k 0)) (d (n "frob-volume") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0yk14985327jw60fsny341574sa0qzfj5qn6396bly33rcizmarr")))

