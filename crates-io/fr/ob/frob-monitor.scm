(define-module (crates-io fr ob frob-monitor) #:use-module (crates-io))

(define-public crate-frob-monitor-0.1.0 (c (n "frob-monitor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "06j7x13f01iii9gkcdjiizpvy0jmvqfm7gv9agyp40nv4lq2rrfh")))

(define-public crate-frob-monitor-0.1.1 (c (n "frob-monitor") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "19cyp9rnzi13541knr6ginjpj0x5lhaw8mb3d5l8qxm65fs33jgl")))

(define-public crate-frob-monitor-0.1.2 (c (n "frob-monitor") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0l7v88yg9vrcc0y8948ws6srirjwzf9g8yb7alwg9klfx2rkpzv0")))

