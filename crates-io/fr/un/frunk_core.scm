(define-module (crates-io fr un frunk_core) #:use-module (crates-io))

(define-public crate-frunk_core-0.0.1 (c (n "frunk_core") (v "0.0.1") (h "03qkgq662a5k1swxzvdvp043v86rnvw41n6hvf8j69pc2rlq0a4l")))

(define-public crate-frunk_core-0.0.2 (c (n "frunk_core") (v "0.0.2") (h "0y706nb80sjff17wwsxibmwg4gll9pkkfjwcmpbpqy6z010j2rpr")))

(define-public crate-frunk_core-0.0.3 (c (n "frunk_core") (v "0.0.3") (h "06w45inqgcia0mqbm9x75f3ggs8fgcxzy94dbddl3q7xbq0nwl5s")))

(define-public crate-frunk_core-0.0.4 (c (n "frunk_core") (v "0.0.4") (h "1v926nwr6fxfvga5sglzjp4scavyvbn7dgmv6sljzdgvqg9wcbqg")))

(define-public crate-frunk_core-0.0.5 (c (n "frunk_core") (v "0.0.5") (h "1c50rhsivr8a46dwdidl9lnvrqh4v0s5yxmr6gb1cf9n4yymz39q")))

(define-public crate-frunk_core-0.0.6 (c (n "frunk_core") (v "0.0.6") (h "07xq0nvlmb7mp0826p0anj2i6jgz4npsycfw8ic7v9vzr0kk83xp")))

(define-public crate-frunk_core-0.0.7 (c (n "frunk_core") (v "0.0.7") (h "0lh7cg2gp6n04zh9lih8r4kr55g4zxiqbbw7kwgl4545s96fgx7q")))

(define-public crate-frunk_core-0.0.8 (c (n "frunk_core") (v "0.0.8") (h "13qx3bf7nla7ps27mvnrbf6hfsbia0ql9dlyz2bw79gj5rwmh9xh")))

(define-public crate-frunk_core-0.0.9 (c (n "frunk_core") (v "0.0.9") (h "11yfz9bqbqsb7a598hl10x6yp2ivxiw76bfgcmsr4bhsfs8vcq6x")))

(define-public crate-frunk_core-0.0.10 (c (n "frunk_core") (v "0.0.10") (d (list (d (n "frunk_derives") (r "^0.0.10") (d #t) (k 2)))) (h "10x2ic47k6fxx4829wn55v2scb4hgks9qn7k8v7cv2yv6rr3nlg7")))

(define-public crate-frunk_core-0.0.11 (c (n "frunk_core") (v "0.0.11") (d (list (d (n "frunk_derives") (r "^0.0.11") (d #t) (k 2)))) (h "0qdfw1lg4a99i1knjnvi2mxgjgjw5qkbk40dvr2iv89001274q6d")))

(define-public crate-frunk_core-0.0.12 (c (n "frunk_core") (v "0.0.12") (d (list (d (n "frunk_derives") (r "^0.0.12") (d #t) (k 2)))) (h "0mmsp8bn2mvlb0cp97rnffcd4akxxi45n5fwcsk881m2d063h5db")))

(define-public crate-frunk_core-0.0.13 (c (n "frunk_core") (v "0.0.13") (d (list (d (n "frunk_derives") (r "^0.0.13") (d #t) (k 2)))) (h "040mb15nhdcdxnhcy7pkzkz3i91gjc15syvmykhcnv8dh7wcq194")))

(define-public crate-frunk_core-0.0.14 (c (n "frunk_core") (v "0.0.14") (d (list (d (n "frunk_derives") (r "^0.0.14") (d #t) (k 2)))) (h "1n02kpmh8nvlwiy9ylfpqrkdv3kaf065b6ddh6nn2q3yqp27yfxb")))

(define-public crate-frunk_core-0.0.15 (c (n "frunk_core") (v "0.0.15") (d (list (d (n "frunk_derives") (r "^0.0.15") (d #t) (k 2)))) (h "0kyl698241f5yci3dlhpby41fxvmljabnmcnlwd3zc1vv1kywcaa")))

(define-public crate-frunk_core-0.0.16 (c (n "frunk_core") (v "0.0.16") (d (list (d (n "frunk_derives") (r "^0.0.16") (d #t) (k 2)))) (h "1bhzhfdbh2vmygq75yzcp4gvwhmdi0sji0w39ahcxb09fhsi2q65")))

(define-public crate-frunk_core-0.0.17 (c (n "frunk_core") (v "0.0.17") (d (list (d (n "frunk_derives") (r "^0.0.17") (d #t) (k 2)))) (h "0lwf61i5rx3av61dc45rmqqnib5j5glk5pyrxq3ckkjd24v60krz")))

(define-public crate-frunk_core-0.0.18 (c (n "frunk_core") (v "0.0.18") (d (list (d (n "frunk_derives") (r "^0.0.18") (d #t) (k 2)))) (h "1cz2xiybz1wbwx7rmb65zp14iwhhcb5qizn0srr85n13phpa8gdm")))

(define-public crate-frunk_core-0.0.19 (c (n "frunk_core") (v "0.0.19") (d (list (d (n "frunk_derives") (r "^0.0.19") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "0f17841s6gjcbfxfw3fxrgb8h23fc82l7bgf8z8pg77rlkyaabi5") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.20 (c (n "frunk_core") (v "0.0.20") (d (list (d (n "frunk_derives") (r "^0.0.20") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "0gcfnfss2iwcq9gnpbary611kjky1q0ac1np8gwd7216wq6sjb3x") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.21 (c (n "frunk_core") (v "0.0.21") (d (list (d (n "frunk_derives") (r "^0.0.21") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "0qqlchp3vs5bn0xh9cn9xsiyd03hxsjyg5g4pj0v1588ndyz7h75") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.22 (c (n "frunk_core") (v "0.0.22") (d (list (d (n "frunk_derives") (r "^0.0.22") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "015jyz7w4v5ijp5kn3gfvsn8b17pil7kz4wvj2yqnkxiddbmaymf") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.23 (c (n "frunk_core") (v "0.0.23") (d (list (d (n "frunk_derives") (r "^0.0.23") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "12s4n0hm6wkqgp1vb8nhh969kryi0sjg97s6hg99spnsdjnlhxay") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.2.0 (c (n "frunk_core") (v "0.2.0") (d (list (d (n "frunk") (r "^0.1.36") (d #t) (k 2)) (d (n "frunk_derives") (r "^0.0.24") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0v2ym7nw8xpk95mxwb52q827xn9kjxzzzp8wz9qn0129vl89zlr0")))

(define-public crate-frunk_core-0.2.1 (c (n "frunk_core") (v "0.2.1") (d (list (d (n "frunk") (r "^0.2.0") (d #t) (k 2)) (d (n "frunk_derives") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10h3srqm6gjmpm0ji5mh7c10mb2cm50ngs9wv8v72jmgl19a5j90")))

(define-public crate-frunk_core-0.2.2 (c (n "frunk_core") (v "0.2.2") (d (list (d (n "frunk") (r "^0.2.1") (d #t) (k 2)) (d (n "frunk_derives") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02pfyjijjh2d376aknmpn5gdvhzvf6zlr782c0y01y265whfas97")))

(define-public crate-frunk_core-0.2.3 (c (n "frunk_core") (v "0.2.3") (d (list (d (n "frunk") (r "^0.2.2") (d #t) (k 2)) (d (n "frunk_derives") (r "^0.2.2") (d #t) (k 2)) (d (n "frunk_proc_macros") (r "^0.0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fh6b121ff3f1jk7lyaiwpj4q5afjny4vswx40wzin75cqj186wg")))

(define-public crate-frunk_core-0.2.4 (c (n "frunk_core") (v "0.2.4") (d (list (d (n "frunk") (r "^0.2.2") (d #t) (k 2)) (d (n "frunk_derives") (r "^0.2.3") (d #t) (k 2)) (d (n "frunk_proc_macros") (r "^0.0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d94qddmlzgjrgqr722r77swvz3r6jxw07f3dadhs8pg4m5rph16")))

(define-public crate-frunk_core-0.3.0 (c (n "frunk_core") (v "0.3.0") (d (list (d (n "frunk") (r "^0.2.4") (k 2)) (d (n "frunk_derives") (r "^0.2.4") (k 2)) (d (n "frunk_proc_macros") (r "^0.0.2") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yn90khzlblnfjdlhxfb0d8xazjsx004yp90bgbbm3bcl80jnifw") (f (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.3.1 (c (n "frunk_core") (v "0.3.1") (d (list (d (n "frunk") (r "^0.3.0") (k 2)) (d (n "frunk_derives") (r "^0.3.0") (k 2)) (d (n "frunk_proc_macros") (r "^0.0.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gj11jby49f2kc516wxpfxyqjn9hw657gplh54ffg56xbajcs10f") (f (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.3.2 (c (n "frunk_core") (v "0.3.2") (d (list (d (n "frunk") (r "^0.3.1") (k 2)) (d (n "frunk_derives") (r "^0.3.0") (k 2)) (d (n "frunk_proc_macros") (r "^0.0.4") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n2qklizmzzzrkp0k7qkj9ygnkiycsbbh044l0i6lcps8pb1936x") (f (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.4.0 (c (n "frunk_core") (v "0.4.0") (d (list (d (n "frunk") (r "^0.3.2") (k 2)) (d (n "frunk_derives") (r "^0.3.2") (k 2)) (d (n "frunk_proc_macros") (r "^0.0.5") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10jlqb5xan2mv02bdnqpppd9g74rzfw61hxm0ljqpgw0xi1wyihj") (f (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.4.1 (c (n "frunk_core") (v "0.4.1") (d (list (d (n "frunk") (r "^0.4.0") (k 2)) (d (n "frunk_derives") (r "^0.4.0") (k 2)) (d (n "frunk_proc_macros") (r "^0.1.0") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0drmkzndbmfmd7vp04z7lbcjpym9chl24hzg5351sc2qll0nsi1a") (f (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.4.2 (c (n "frunk_core") (v "0.4.2") (d (list (d (n "frunk") (r "^0.4.1") (k 2)) (d (n "frunk_derives") (r "^0.4.0") (k 2)) (d (n "frunk_proc_macros") (r "^0.1.1") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mjqnn7dclwn8d5g0mrfkg360cgn70a7mm8arx6fc1xxn3x6j95g") (f (quote (("std") ("default" "std"))))))

