(define-module (crates-io fr un frunk_laws) #:use-module (crates-io))

(define-public crate-frunk_laws-0.0.1 (c (n "frunk_laws") (v "0.0.1") (d (list (d (n "frunk") (r "^0.1.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "12gvzwmmjxh6sw5x7yw6xc5as6y447c01j9m2kxh34xswwg9dnsm")))

(define-public crate-frunk_laws-0.0.2 (c (n "frunk_laws") (v "0.0.2") (d (list (d (n "frunk") (r "^0.1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "13kjasic35g6c05n5m8vgbfrcxwlwqb9kina33ciw1m0m0afanfi")))

(define-public crate-frunk_laws-0.0.3 (c (n "frunk_laws") (v "0.0.3") (d (list (d (n "frunk") (r "^0.1.25") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "05shzmkp23zb46qpcpv1d06lv2qpbw174fdrmf5c6waq4x6jm5vm")))

(define-public crate-frunk_laws-0.0.4 (c (n "frunk_laws") (v "0.0.4") (d (list (d (n "frunk") (r "^0.1.26") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "0br12d6xx9n54pqscbrp1s6cxcxynqfm8l3cfdg1kmk01wj4gxih")))

(define-public crate-frunk_laws-0.0.5 (c (n "frunk_laws") (v "0.0.5") (d (list (d (n "frunk") (r "^0.1.27") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "058zmm3mcnh6xf56yczb7sd9l1xhsga6dk58b4amwb7k7qwazp8d")))

(define-public crate-frunk_laws-0.0.6 (c (n "frunk_laws") (v "0.0.6") (d (list (d (n "frunk") (r "^0.1.28") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 0)))) (h "1y1dnmjk72cylvv72wbm7gfkgrlsfiwmrmskvfbw1mvc12x6xg4g")))

(define-public crate-frunk_laws-0.0.7 (c (n "frunk_laws") (v "0.0.7") (d (list (d (n "frunk") (r "^0.1.29") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "1zvgnyp4s8fg0m2jd8x9bfwkp4z28n02ws246a393ra75n24ivad")))

(define-public crate-frunk_laws-0.0.8 (c (n "frunk_laws") (v "0.0.8") (d (list (d (n "frunk") (r "^0.1.30") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "1c4wcxbqh4sr3c2ggibn4s70b8np54z35bzamarhj46xgabqw40r")))

(define-public crate-frunk_laws-0.0.9 (c (n "frunk_laws") (v "0.0.9") (d (list (d (n "frunk") (r "^0.1.31") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "1jb3d92ffjbky2qm4gmd7sff28hbkbqb09f79s7ysdmk9qyawx2h")))

(define-public crate-frunk_laws-0.0.10 (c (n "frunk_laws") (v "0.0.10") (d (list (d (n "frunk") (r "^0.1.32") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "19n37s80w54mjfargm8h9qlds2q8hspg9vdpv6qdjij3a4778aqj")))

(define-public crate-frunk_laws-0.0.11 (c (n "frunk_laws") (v "0.0.11") (d (list (d (n "frunk") (r "^0.1.33") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "1vckfpw17x6m45x629byggasb6ajq2wa0c57xy58fai3s09xc4n7")))

(define-public crate-frunk_laws-0.0.12 (c (n "frunk_laws") (v "0.0.12") (d (list (d (n "frunk") (r "^0.1.34") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 0)))) (h "0py1vcl621qd1yv6hrc80i0ayg8xx0ylidvxkwqzx6q6a61salpq")))

(define-public crate-frunk_laws-0.0.13 (c (n "frunk_laws") (v "0.0.13") (d (list (d (n "frunk") (r "^0.1.35") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "0w3mz3fj7hy5li0005ghs8zp98vylbpk36ydanwwxfp05rnly96r")))

(define-public crate-frunk_laws-0.0.14 (c (n "frunk_laws") (v "0.0.14") (d (list (d (n "frunk") (r "^0.1.36") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "17g04171wzabnmzzcy213cqpariplvzd7l3x70vfgnxxs0cd99ds")))

(define-public crate-frunk_laws-0.2.0 (c (n "frunk_laws") (v "0.2.0") (d (list (d (n "frunk") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "19yqv0d023ddad65lbd9jvn3k1g6rwcc6xx1pgqlxxgf1v3r2j8q")))

(define-public crate-frunk_laws-0.2.1 (c (n "frunk_laws") (v "0.2.1") (d (list (d (n "frunk") (r "^0.2.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "1mjhmsxj1y8i04n6ryxkjj9g6zq1hvm01l53zry9466bn0mv1gsr")))

(define-public crate-frunk_laws-0.2.2 (c (n "frunk_laws") (v "0.2.2") (d (list (d (n "frunk") (r "^0.2.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "0dynj22c9yqcrvz8x2q8xn9m65fm8fxldfzzymixfrfbdwhhgwi5")))

(define-public crate-frunk_laws-0.2.4 (c (n "frunk_laws") (v "0.2.4") (d (list (d (n "frunk") (r "^0.2.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "127wi590lf9ikbilyjv8cmckd2alc022ivnjh36cy33ilw79iczg")))

(define-public crate-frunk_laws-0.3.0 (c (n "frunk_laws") (v "0.3.0") (d (list (d (n "frunk") (r "^0.3.0") (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "0dmnsjfrcxnkqb1719cdjzicp2v0bbq5v7yxckkdh9lyc0m97ww9")))

(define-public crate-frunk_laws-0.3.1 (c (n "frunk_laws") (v "0.3.1") (d (list (d (n "frunk") (r "^0.3.1") (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "0v31zazjnqvac1qg1x5c6yfdxa5i6yjrvqra1hh8jq98qims7840")))

(define-public crate-frunk_laws-0.3.2 (c (n "frunk_laws") (v "0.3.2") (d (list (d (n "frunk") (r "^0.3.2") (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "0nc2cvfmn51kklnhvspf4rxp2n0xmv44npkn83xck8azgrnwp1h0")))

(define-public crate-frunk_laws-0.4.0 (c (n "frunk_laws") (v "0.4.0") (d (list (d (n "frunk") (r "^0.4.0") (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "0ihl0klb5h49lkrag22ndpd6d0dks2l7ds91h55a54l8ldl5wkwm")))

(define-public crate-frunk_laws-0.4.1 (c (n "frunk_laws") (v "0.4.1") (d (list (d (n "frunk") (r "^0.4.1") (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "1nvm78czqlxyxj3ic6k5s5l85bx7n2r75592aiyilhdnfjaga4a7")))

(define-public crate-frunk_laws-0.4.2 (c (n "frunk_laws") (v "0.4.2") (d (list (d (n "frunk") (r "^0.4.2") (k 0)) (d (n "quickcheck") (r "^0.6.1") (d #t) (k 0)))) (h "0fp8sxlszfwvgnw0fy9nw0wp8qqlh8wk2pai0xapx4h865kiyj83")))

(define-public crate-frunk_laws-0.5.0 (c (n "frunk_laws") (v "0.5.0") (d (list (d (n "frunk") (r "^0.4.2") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 0)))) (h "09kj8czxgczg85ni0d4s9r99g08npra124517yg87dadr3iw0pz0")))

