(define-module (crates-io fr un frunk_utils) #:use-module (crates-io))

(define-public crate-frunk_utils-0.1.0 (c (n "frunk_utils") (v "0.1.0") (d (list (d (n "frunk") (r "^0.4") (d #t) (k 0)))) (h "1f50jmqgcj52wg6fn2iqsnqjyysparyqcnb63ppwnifg9gqv6hl4")))

(define-public crate-frunk_utils-0.2.0 (c (n "frunk_utils") (v "0.2.0") (d (list (d (n "frunk") (r "^0.4") (d #t) (k 0)))) (h "1cc2jxl7qzyk2s8xh3ybf2465hzc0kakjpm4p9rj5cjpcahc1nxb")))

(define-public crate-frunk_utils-0.2.1 (c (n "frunk_utils") (v "0.2.1") (d (list (d (n "frunk") (r "^0.4") (d #t) (k 0)))) (h "0w3c7pdj1w3ldamqy0y2881pllwirp8d7rp4paz6q0p4kgkzji50")))

(define-public crate-frunk_utils-0.2.2 (c (n "frunk_utils") (v "0.2.2") (d (list (d (n "frunk") (r "^0.4") (d #t) (k 0)))) (h "0fs5rwwdqgi5zy8nydvzqsbk3a1aj4098bknk7cy8g7hi5f90nmb")))

