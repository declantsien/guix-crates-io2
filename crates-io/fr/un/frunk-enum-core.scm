(define-module (crates-io fr un frunk-enum-core) #:use-module (crates-io))

(define-public crate-frunk-enum-core-0.2.0 (c (n "frunk-enum-core") (v "0.2.0") (d (list (d (n "frunk") (r "^0.3.0") (d #t) (k 0)))) (h "0l1a893jrwhymm3w4jws6pl3ggz2r68wy0wmd5axw877570f0z7a")))

(define-public crate-frunk-enum-core-0.2.1 (c (n "frunk-enum-core") (v "0.2.1") (d (list (d (n "frunk") (r "^0.3.1") (d #t) (k 0)))) (h "0ig8ja3kl2pqw2nfq3lw2ircb6378mm209wz631ckk153xpn4lmp")))

(define-public crate-frunk-enum-core-0.3.0 (c (n "frunk-enum-core") (v "0.3.0") (d (list (d (n "frunk") (r "^0.4") (d #t) (k 0)))) (h "1a5vha5s9vhsvjkbd6wz2yir2zw87cjdfcz95ryzfhli73lmbld2")))

