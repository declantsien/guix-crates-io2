(define-module (crates-io fr un frunk_utils_derives) #:use-module (crates-io))

(define-public crate-frunk_utils_derives-0.1.0 (c (n "frunk_utils_derives") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0j1ldwlnd763ws7nj3j8wr2rlpvg6rcwnwb5wcgmnq8cbk3qng63")))

(define-public crate-frunk_utils_derives-0.1.1 (c (n "frunk_utils_derives") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "13a9palfnj72hdc751ahj11fvl1wnn457wywhakd6590nndj5i37")))

