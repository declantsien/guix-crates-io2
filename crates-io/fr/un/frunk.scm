(define-module (crates-io fr un frunk) #:use-module (crates-io))

(define-public crate-frunk-0.1.0 (c (n "frunk") (v "0.1.0") (h "090ib9v2623837bkgqkxhjh8xaw7bhc518almgmg6p2dmlcizskw")))

(define-public crate-frunk-0.1.1 (c (n "frunk") (v "0.1.1") (h "1shpzp086600yclq086qcm5j6w1ymfi7nrd0vzvpk9lwjwx6m392")))

(define-public crate-frunk-0.1.2 (c (n "frunk") (v "0.1.2") (h "02gghyiy8k1zgwx67sadq2m5yyh8jicl56w8jyb3fqlcpqcwapr0")))

(define-public crate-frunk-0.1.3 (c (n "frunk") (v "0.1.3") (h "0315na7700qdni68ya2p4ymrv3q1hriql9cziwww0i43p01ha0ls")))

(define-public crate-frunk-0.1.4 (c (n "frunk") (v "0.1.4") (h "1i5w3m1lqllyay5nzbdml1n4k7c1i4ijpb1f3l3zbi1fkdgggrcp")))

(define-public crate-frunk-0.1.5 (c (n "frunk") (v "0.1.5") (h "08zy3qxvq1ziy1qw63714w7i2nfql1p5vh9warqbrmnmz6x9pmd8")))

(define-public crate-frunk-0.1.6 (c (n "frunk") (v "0.1.6") (h "04yyyh4pbprivwsq0rghjkf3fn63nban9vfklpx3bws3g4pd6r5r")))

(define-public crate-frunk-0.1.7 (c (n "frunk") (v "0.1.7") (h "1700rb3sba52dqzwa5969bqkwmvg7072fszn8c9ffy3lrvmavyhh")))

(define-public crate-frunk-0.1.8 (c (n "frunk") (v "0.1.8") (d (list (d (n "frunk_core") (r "^0.0.1") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.1") (d #t) (k 0)))) (h "0178kf5z5bp1v5v9b8klrjchyadg1ggfbygzkbfgsq5yxlwii4zg")))

(define-public crate-frunk-0.1.9 (c (n "frunk") (v "0.1.9") (d (list (d (n "frunk_core") (r "^0.0.1") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.1") (d #t) (k 0)))) (h "14wdc731spxx453mbk9arqkn4v9pn1lp30jdixj7yfvgaqybz56y")))

(define-public crate-frunk-0.1.10 (c (n "frunk") (v "0.1.10") (d (list (d (n "frunk_core") (r "^0.0.2") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.1") (d #t) (k 0)))) (h "1s23a5yy1jy4bzy63z00rr5xwwn3v0jk5rrcyxlqj2f71swgmw3b")))

(define-public crate-frunk-0.1.11 (c (n "frunk") (v "0.1.11") (d (list (d (n "frunk_core") (r "^0.0.3") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.2") (d #t) (k 0)))) (h "1h1z2clvdrjq4nn7mq6yllsss3xjvlkd5fgi7h9nnhbb5s9m35jm")))

(define-public crate-frunk-0.1.12 (c (n "frunk") (v "0.1.12") (d (list (d (n "frunk_core") (r "^0.0.4") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.3") (d #t) (k 0)))) (h "0qlpszfq6v14iw0lv9qhwaawpja54xrww9lg5fvd9m7wbkgzphln")))

(define-public crate-frunk-0.1.13 (c (n "frunk") (v "0.1.13") (d (list (d (n "frunk_core") (r "^0.0.5") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.4") (d #t) (k 0)))) (h "1z7yw1zwn6f522fdxh6yfbwfhigxsp3n0b88fj2q63cg9cvjkyf7")))

(define-public crate-frunk-0.1.14 (c (n "frunk") (v "0.1.14") (d (list (d (n "frunk_core") (r "^0.0.6") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.5") (d #t) (k 0)))) (h "0wi8ggh6j929662ml4x1y24bzhggffik41p1zjn4mk9y4c0330f1")))

(define-public crate-frunk-0.1.15 (c (n "frunk") (v "0.1.15") (d (list (d (n "frunk_core") (r "^0.0.7") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.6") (d #t) (k 0)))) (h "1p7pardfv5b1pqihhhlg6r14y6wm4pbs33p9v4wb6xshmsxgxlh1")))

(define-public crate-frunk-0.1.16 (c (n "frunk") (v "0.1.16") (d (list (d (n "frunk_core") (r "^0.0.7") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.7") (d #t) (k 0)))) (h "12nkgms6j1hhfybps076c1jnkb5fylffh907l1fyfxjsb3hjpf7n")))

(define-public crate-frunk-0.1.17 (c (n "frunk") (v "0.1.17") (d (list (d (n "frunk_core") (r "^0.0.7") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.8") (d #t) (k 0)))) (h "0s12bgkab7pbnnk18dw6qg9xdhi8x39i1l1rbmx2p73rybhr0mqa")))

(define-public crate-frunk-0.1.18 (c (n "frunk") (v "0.1.18") (d (list (d (n "frunk_core") (r "^0.0.8") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.9") (d #t) (k 0)))) (h "08sbgabqfcj8rzvx7r5zs12xpzaiya3zh90x17agxg8v4lgm0gg5")))

(define-public crate-frunk-0.1.19 (c (n "frunk") (v "0.1.19") (d (list (d (n "frunk_core") (r "^0.0.9") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.10") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "14rhhfc75hv6jgcz31dakda07f7mgqfcp7y53m020f8igs52qjmf")))

(define-public crate-frunk-0.1.20 (c (n "frunk") (v "0.1.20") (d (list (d (n "frunk_core") (r "^0.0.10") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.11") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "14z4qx4banaw63m6pai1jfl7ky38np1dxd87jnzcjkwd78lhcsli")))

(define-public crate-frunk-0.1.21 (c (n "frunk") (v "0.1.21") (d (list (d (n "frunk_core") (r "^0.0.11") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.11") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0m2ldcsnrf7c9844jra8cy6g718vdfj2pvvnlmj8fd0n6nmjfyn4")))

(define-public crate-frunk-0.1.22 (c (n "frunk") (v "0.1.22") (d (list (d (n "frunk_core") (r "^0.0.11") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.12") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0r8fwq80spy1sd9c8msbglhbi8gm242m35rh8yjsppr5j4jvvcj2")))

(define-public crate-frunk-0.1.23 (c (n "frunk") (v "0.1.23") (d (list (d (n "frunk_core") (r "^0.0.11") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.12") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.1") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0ph0d9hsjpd7iwm5sdws3lz24q7knwfifz2yf23m18l0gqj6vfrk")))

(define-public crate-frunk-0.1.24 (c (n "frunk") (v "0.1.24") (d (list (d (n "frunk_core") (r "^0.0.12") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.13") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.2") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1d2mqbhwmkg6wgk5gd0m86hzs5fa00kjciwsa353vp45ckvd2475")))

(define-public crate-frunk-0.1.25 (c (n "frunk") (v "0.1.25") (d (list (d (n "frunk_core") (r "^0.0.13") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.14") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.2") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0lay20xwap6nwl5l6rp3h55hyk0bmg48618rdlgpldfkihj3vr8z")))

(define-public crate-frunk-0.1.26 (c (n "frunk") (v "0.1.26") (d (list (d (n "frunk_core") (r "^0.0.14") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.15") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.3") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "10hz42cl65ij0gixl5nqflxhfi2xfaam8z09xb4p9n1dm7xbz8rx")))

(define-public crate-frunk-0.1.27 (c (n "frunk") (v "0.1.27") (d (list (d (n "frunk_core") (r "^0.0.15") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.16") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.4") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "038v13dbswdfmb9232pfxb16lpgv2ywczak37rkr5bmlw076za3a")))

(define-public crate-frunk-0.1.28 (c (n "frunk") (v "0.1.28") (d (list (d (n "frunk_core") (r "^0.0.15") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.16") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.5") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1ycpiir3mv8gwrxsqi8sxjkgv0nxzryqprnazp4rfydbam7jfhmc")))

(define-public crate-frunk-0.1.29 (c (n "frunk") (v "0.1.29") (d (list (d (n "frunk_core") (r "^0.0.16") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.17") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.6") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1nnmn0vh943sx5fqnwkxk3hs70chxjjmcnz30xbgwmb54rwsqsai")))

(define-public crate-frunk-0.1.30 (c (n "frunk") (v "0.1.30") (d (list (d (n "frunk_core") (r "^0.0.17") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.18") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.7") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1r60mw21pi830z57nrbc28qybxjaplw9v23m8q2lcf2gh4v7842s")))

(define-public crate-frunk-0.1.31 (c (n "frunk") (v "0.1.31") (d (list (d (n "frunk_core") (r "^0.0.18") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.19") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.8") (d #t) (k 2)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0mldj2nwmhn9mvsl23j3y26dck7cmsb5dhaxa4qpkdg1715qaadd")))

(define-public crate-frunk-0.1.32 (c (n "frunk") (v "0.1.32") (d (list (d (n "frunk_core") (r "^0.0.19") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.20") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0hcp7pqhg0b23dvxdqv9gcn41fs4rkc4z60nrbimf1qlymh836k1") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1.33 (c (n "frunk") (v "0.1.33") (d (list (d (n "frunk_core") (r "^0.0.20") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.21") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1rxbih99a6kn0lamhzdj7r9cy1r8r1a79fm193y1y9db3dswrzsd") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1.34 (c (n "frunk") (v "0.1.34") (d (list (d (n "frunk_core") (r "^0.0.21") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.22") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1w5la7nv427dkk4ahm25blvbi27i6qgm0lw1wi653r5h6dakddkq") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1.35 (c (n "frunk") (v "0.1.35") (d (list (d (n "frunk_core") (r "^0.0.22") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.23") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0vfxwj6n3jdmas6bcjirm4lim8jpr816g1v4mnn9707zahba9a31") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1.36 (c (n "frunk") (v "0.1.36") (d (list (d (n "frunk_core") (r "^0.0.23") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.0.24") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1cixr73yjnjf0kwd38wb08pkn1fdz0fk048j7rr5pxsgwp5i9p0i") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.2.0 (c (n "frunk") (v "0.2.0") (d (list (d (n "frunk_core") (r "^0.2.0") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.2.0") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0kf42i1k3nk1f1hcbyhwwvp2h9nbg5w6vz0y85i6ida7991ng35v")))

(define-public crate-frunk-0.2.1 (c (n "frunk") (v "0.2.1") (d (list (d (n "frunk_core") (r "^0.2.1") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.2.1") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0i7clqa1kv0fbmjzj0hyjlyhr343277xwqslvnm3k3gghg06ldpf")))

(define-public crate-frunk-0.2.2 (c (n "frunk") (v "0.2.2") (d (list (d (n "frunk_core") (r "^0.2.2") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.2.2") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1y6ggsfpl2zddd8m972iz3dgjh9j0brmnmpslb2n10gbl1r6r02x")))

(define-public crate-frunk-0.2.4 (c (n "frunk") (v "0.2.4") (d (list (d (n "frunk_core") (r "^0.2.4") (d #t) (k 0)) (d (n "frunk_derives") (r "^0.2.4") (d #t) (k 0)) (d (n "frunk_laws") (r "^0.2.2") (d #t) (k 2)) (d (n "frunk_proc_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "07n4zp9klqa2j8kxhi2lcda0c9z3gngmpg9ga25yijzxr5d5a4w3")))

(define-public crate-frunk-0.3.0 (c (n "frunk") (v "0.3.0") (d (list (d (n "frunk_core") (r "^0.3.0") (k 0)) (d (n "frunk_derives") (r "^0.3.0") (k 0)) (d (n "frunk_laws") (r "^0.2.4") (k 2)) (d (n "frunk_proc_macros") (r "^0.0.3") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0pxnvpg1bhz4vjcmsr2h537mfk80qg19css5fbzbqh9cqaik3cdj") (f (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.3.1 (c (n "frunk") (v "0.3.1") (d (list (d (n "frunk_core") (r "^0.3.1") (k 0)) (d (n "frunk_derives") (r "^0.3.1") (k 0)) (d (n "frunk_laws") (r "^0.3.0") (k 2)) (d (n "frunk_proc_macros") (r "^0.0.4") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "04asy0bmv6vwr01ydrm6d4gmbc3zdhsnkq85zm6jvnnmrap4hhjj") (f (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.3.2 (c (n "frunk") (v "0.3.2") (d (list (d (n "frunk_core") (r "^0.3.2") (k 0)) (d (n "frunk_derives") (r "^0.3.2") (k 0)) (d (n "frunk_laws") (r "^0.3.1") (k 2)) (d (n "frunk_proc_macros") (r "^0.0.5") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1jjnlwx037wgmrsk3339kxg85iqrg2cpqlizw9afz0ip2f6800fx") (f (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.4.0 (c (n "frunk") (v "0.4.0") (d (list (d (n "frunk_core") (r "^0.4.0") (k 0)) (d (n "frunk_derives") (r "^0.4.0") (k 0)) (d (n "frunk_laws") (r "^0.3.1") (k 2)) (d (n "frunk_proc_macros") (r "^0.1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0pirlnkkp45pjgcql8g08gp4mms76xf9iwvnxb874zjbspvprmhc") (f (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.4.1 (c (n "frunk") (v "0.4.1") (d (list (d (n "frunk_core") (r "^0.4.1") (k 0)) (d (n "frunk_derives") (r "^0.4.1") (k 0)) (d (n "frunk_laws") (r "^0.4.0") (k 2)) (d (n "frunk_proc_macros") (r "^0.1.1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1q71yryms0gyvpz3dy1mqmgsj064ghslaf47l21z6280ylxp1758") (f (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.4.2 (c (n "frunk") (v "0.4.2") (d (list (d (n "frunk_core") (r "^0.4.2") (k 0)) (d (n "frunk_derives") (r "^0.4.2") (k 0)) (d (n "frunk_laws") (r "^0.4.1") (k 2)) (d (n "frunk_proc_macros") (r "^0.1.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "11v242h7zjka0lckxcffn5pjgr3jzxyljy7ffr0ppy8jkssm38qi") (f (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

