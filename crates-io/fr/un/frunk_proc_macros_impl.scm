(define-module (crates-io fr un frunk_proc_macros_impl) #:use-module (crates-io))

(define-public crate-frunk_proc_macros_impl-0.0.1+pre (c (n "frunk_proc_macros_impl") (v "0.0.1+pre") (d (list (d (n "frunk_core") (r "^0.2.2") (d #t) (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "110r9fwbzxi6w9amhxl1sgx9x9hn4r5x2zylqdv4wqr9kkxhm3dz") (y #t)))

(define-public crate-frunk_proc_macros_impl-0.0.1 (c (n "frunk_proc_macros_impl") (v "0.0.1") (d (list (d (n "frunk_core") (r "^0.2.3") (d #t) (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "01030nmvga2ra6lfih3jalk55a52qmr5pdgdgmmlcgf5b3i0mhp3")))

(define-public crate-frunk_proc_macros_impl-0.0.2 (c (n "frunk_proc_macros_impl") (v "0.0.2") (d (list (d (n "frunk_core") (r "^0.2.4") (d #t) (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1l739w64b1jcd033f0c3jhm9hzaj0dnsjypihl9m149pby277q4q")))

(define-public crate-frunk_proc_macros_impl-0.0.3 (c (n "frunk_proc_macros_impl") (v "0.0.3") (d (list (d (n "frunk_core") (r "^0.3.0") (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.3") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "03dfjjfqw3yr27b4s2a6xklfnpcwgvlg3s0w846gjvvxvd5k7d21")))

(define-public crate-frunk_proc_macros_impl-0.0.4 (c (n "frunk_proc_macros_impl") (v "0.0.4") (d (list (d (n "frunk_core") (r "^0.3.1") (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.4") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0z6d0l1j26m77ljmfjm4mm3jhaynqy3sadbaz2ldz59f17gvmkjs")))

(define-public crate-frunk_proc_macros_impl-0.0.5 (c (n "frunk_proc_macros_impl") (v "0.0.5") (d (list (d (n "frunk_core") (r "^0.3.2") (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.5") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1jyiy7pfcsqhd86nfv1qn9fd9bjqbvypc4pyfgkj792dycv6gpg0")))

(define-public crate-frunk_proc_macros_impl-0.1.0 (c (n "frunk_proc_macros_impl") (v "0.1.0") (d (list (d (n "frunk_core") (r "^0.4.0") (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.1.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ckcvzapkmgyf1akq7r6nk13p1nal2wzp23kym17xxd41ygskyvg")))

(define-public crate-frunk_proc_macros_impl-0.1.1 (c (n "frunk_proc_macros_impl") (v "0.1.1") (d (list (d (n "frunk_core") (r "^0.4.1") (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.1.1") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0lr65bll9jsfls6jz9kbv57jk1hhwffgqs3q3bzfg3n19jbjv00a")))

