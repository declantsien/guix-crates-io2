(define-module (crates-io fr un frunk_proc_macros) #:use-module (crates-io))

(define-public crate-frunk_proc_macros-0.0.1+pre (c (n "frunk_proc_macros") (v "0.0.1+pre") (d (list (d (n "frunk") (r "^0.2.2") (d #t) (k 2)) (d (n "frunk_core") (r "^0.2.2") (d #t) (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "06ijf2i1qi7gsznjk5hlgi66pllhrzffp1p8hg3b4qiwgxfxnvgl") (y #t)))

(define-public crate-frunk_proc_macros-0.0.1 (c (n "frunk_proc_macros") (v "0.0.1") (d (list (d (n "frunk") (r "^0.2.2") (d #t) (k 2)) (d (n "frunk_core") (r "^0.2.3") (d #t) (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0f9ghnw8p6yw2rpi5j57gnq01nzihj7d3b1ss9kdc08051wz6yii")))

(define-public crate-frunk_proc_macros-0.0.2 (c (n "frunk_proc_macros") (v "0.0.2") (d (list (d (n "frunk") (r "^0.2.2") (d #t) (k 2)) (d (n "frunk_core") (r "^0.2.4") (d #t) (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1yz4rz98fcdxs91dy11bkkx16ns2fgrwscw2yzqg16g2zjkjxa26")))

(define-public crate-frunk_proc_macros-0.0.3 (c (n "frunk_proc_macros") (v "0.0.3") (d (list (d (n "frunk") (r "^0.2.2") (k 2)) (d (n "frunk_core") (r "^0.3.0") (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.0.3") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1w508x5habfsj9i9nbskcc9znagggvjyp9w16apgbhq7xs3akk0y")))

(define-public crate-frunk_proc_macros-0.0.4 (c (n "frunk_proc_macros") (v "0.0.4") (d (list (d (n "frunk") (r "^0.3.0") (k 2)) (d (n "frunk_core") (r "^0.3.1") (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.0.4") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0vq74dj5yh43plmaqbv1sgb9n20npncjhxy10ixcabg5w96cl1xh")))

(define-public crate-frunk_proc_macros-0.0.5 (c (n "frunk_proc_macros") (v "0.0.5") (d (list (d (n "frunk") (r "^0.3.1") (k 2)) (d (n "frunk_core") (r "^0.3.2") (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.0.5") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1fdy5s8w8lakchd8a253l56dzickik4jq7ag4r119mjywvxzfda7")))

(define-public crate-frunk_proc_macros-0.1.0 (c (n "frunk_proc_macros") (v "0.1.0") (d (list (d (n "frunk") (r "^0.3.2") (k 2)) (d (n "frunk_core") (r "^0.4.0") (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.1.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1k6va5ikn71pkcskziggm5r2lxl5cmsqyyq01dgbijzcb62bsy50")))

(define-public crate-frunk_proc_macros-0.1.1 (c (n "frunk_proc_macros") (v "0.1.1") (d (list (d (n "frunk") (r "^0.4.0") (k 2)) (d (n "frunk_core") (r "^0.4.1") (k 0)) (d (n "frunk_proc_macros_impl") (r "^0.1.1") (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1q7szgh805skmhhihgww25xnavhqgywl5czzzy5b9sjx517m40ga")))

(define-public crate-frunk_proc_macros-0.1.2 (c (n "frunk_proc_macros") (v "0.1.2") (d (list (d (n "frunk_core") (r "^0.4.2") (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.1.2") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "14rsw5znll59xhlpy4il0cza1v1gxw9qwpn0845k0sws98fmmf3i")))

