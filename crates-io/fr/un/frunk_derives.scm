(define-module (crates-io fr un frunk_derives) #:use-module (crates-io))

(define-public crate-frunk_derives-0.0.1 (c (n "frunk_derives") (v "0.0.1") (d (list (d (n "frunk_core") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "160pm8xlq3nkkk4z47sp350i73ynnysgfx5gcq67mwsq91gi1ssd")))

(define-public crate-frunk_derives-0.0.2 (c (n "frunk_derives") (v "0.0.2") (d (list (d (n "frunk_core") (r "^0.0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0gdmbblpb49gpa7iyhlna9zqc1h0b4nw9bk3jd0z1jn66lg7zi8q")))

(define-public crate-frunk_derives-0.0.3 (c (n "frunk_derives") (v "0.0.3") (d (list (d (n "frunk_core") (r "^0.0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "127hlv60wfmxv68l8p2s0gnw7bny55m8zi4c0f896y2qdi4x4jfi")))

(define-public crate-frunk_derives-0.0.4 (c (n "frunk_derives") (v "0.0.4") (d (list (d (n "frunk_core") (r "^0.0.5") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "1bk7307nj36n74pc23mx8hf6qy3g2wgkj2yanngix5apzvzx746f")))

(define-public crate-frunk_derives-0.0.5 (c (n "frunk_derives") (v "0.0.5") (d (list (d (n "frunk_core") (r "^0.0.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "04npydbzbqrrd3i7pq91v2wks3iwghr0p4bdv25gn2ji0bkg7apm")))

(define-public crate-frunk_derives-0.0.6 (c (n "frunk_derives") (v "0.0.6") (d (list (d (n "frunk_core") (r "^0.0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "14rh25pjbz8cmhkw0fv0ahm045mw2bzmlysv6hvrv1jay789p085")))

(define-public crate-frunk_derives-0.0.7 (c (n "frunk_derives") (v "0.0.7") (d (list (d (n "frunk_core") (r "^0.0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0kvc48xdlq0jlc404wl3lf095q8yn9k96h1m9cx54q56iij96kd6")))

(define-public crate-frunk_derives-0.0.8 (c (n "frunk_derives") (v "0.0.8") (d (list (d (n "frunk_core") (r "^0.0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0gh1dd2zrm409hfix7jycj3a0gsidkwns8nj0d52m25kmlwx6a9k")))

(define-public crate-frunk_derives-0.0.9 (c (n "frunk_derives") (v "0.0.9") (d (list (d (n "frunk_core") (r "^0.0.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (d #t) (k 0)))) (h "0f9450f17i0j7p9wnxyhg4il4kmsl2p4wk4ffss5cy63cfnaq8kb")))

(define-public crate-frunk_derives-0.0.10 (c (n "frunk_derives") (v "0.0.10") (d (list (d (n "frunk_core") (r "^0.0.9") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "15q0j406wr3gdhy2zrnn0a2xr1rdzfyqi72rfjzzvz46kvgcrlvh")))

(define-public crate-frunk_derives-0.0.11 (c (n "frunk_derives") (v "0.0.11") (d (list (d (n "frunk_core") (r "^0.0.10") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0g8gvsb986fhi9937msmrmj0008xsxs8blvw6bk2vh1lzi74ifmz")))

(define-public crate-frunk_derives-0.0.12 (c (n "frunk_derives") (v "0.0.12") (d (list (d (n "frunk_core") (r "^0.0.11") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1v89y69aghgvacp2r7d5pzrgqydn033jnpfp5sacxydagzmqw33y")))

(define-public crate-frunk_derives-0.0.13 (c (n "frunk_derives") (v "0.0.13") (d (list (d (n "frunk_core") (r "^0.0.12") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0l8plvr3jmy6ncsm5nhc8hbxdk9ga7x4576hphmd15f22z44fsha")))

(define-public crate-frunk_derives-0.0.14 (c (n "frunk_derives") (v "0.0.14") (d (list (d (n "frunk_core") (r "^0.0.13") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "17nk3mlk2i5rs0dz71ffbra532l5gkgfrlzzilaclfd35c4h7w0f")))

(define-public crate-frunk_derives-0.0.15 (c (n "frunk_derives") (v "0.0.15") (d (list (d (n "frunk_core") (r "^0.0.14") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0l27hk7mcmwvx9ib50qy8c47bm0d0gn58537d0wvsqj2r7q26qx0")))

(define-public crate-frunk_derives-0.0.16 (c (n "frunk_derives") (v "0.0.16") (d (list (d (n "frunk_core") (r "^0.0.15") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0r2w73a2s5hqhh602xr11rg3wqs53nipxr49c3yqhz8i3mkvxnrl")))

(define-public crate-frunk_derives-0.0.17 (c (n "frunk_derives") (v "0.0.17") (d (list (d (n "frunk_core") (r "^0.0.16") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0a37mixzcamslx9hmy89v55qwgr4068x76wkpgim9pwlyf0fnax1")))

(define-public crate-frunk_derives-0.0.18 (c (n "frunk_derives") (v "0.0.18") (d (list (d (n "frunk_core") (r "^0.0.17") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1k5ff6wk8yx1ywvlf9p5jj998pfjckcjkhjf03y1hc19rkpfw0gw")))

(define-public crate-frunk_derives-0.0.19 (c (n "frunk_derives") (v "0.0.19") (d (list (d (n "frunk_core") (r "^0.0.18") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "18cls1c42qaz34zyqkf59bmnkj4nkwwbxwlpvkfi7j74yl6wqfh3")))

(define-public crate-frunk_derives-0.0.20 (c (n "frunk_derives") (v "0.0.20") (d (list (d (n "frunk_core") (r "^0.0.19") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "17qyga9dp8dj2bd3mnvhiary0p22a31z6q8g1z4syiwl0gr6n8yl")))

(define-public crate-frunk_derives-0.0.21 (c (n "frunk_derives") (v "0.0.21") (d (list (d (n "frunk_core") (r "^0.0.20") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "14065wc5af834y5nacxw49map11p1bnl5sv31mmv1qar1b27gi7p")))

(define-public crate-frunk_derives-0.0.22 (c (n "frunk_derives") (v "0.0.22") (d (list (d (n "frunk_core") (r "^0.0.21") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "01izk653lsh4vv036dzishkmxcvmcmm7wl51izdqlssph17rscv7")))

(define-public crate-frunk_derives-0.0.23 (c (n "frunk_derives") (v "0.0.23") (d (list (d (n "frunk_core") (r "^0.0.22") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0mkzh5780v2al9qkf4v5hccis9pmcc04dpfirnk86j1qhwgiv975")))

(define-public crate-frunk_derives-0.0.24 (c (n "frunk_derives") (v "0.0.24") (d (list (d (n "frunk_core") (r "^0.0.23") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0n5c478kwdmpsvn6rp9v8m23yrz9zz598f4nl4bm9flk5kb3knb5")))

(define-public crate-frunk_derives-0.2.0 (c (n "frunk_derives") (v "0.2.0") (d (list (d (n "frunk_core") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1l2fb84sziqmymzhqczmp0wwlr53mf5hj39cj1m9mqb8jmb4hs1q")))

(define-public crate-frunk_derives-0.2.1 (c (n "frunk_derives") (v "0.2.1") (d (list (d (n "frunk_core") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "07fi7907qivinb1fg6zvnp057p1a94kahfrj1bff3lghrbgfdqp7")))

(define-public crate-frunk_derives-0.2.2 (c (n "frunk_derives") (v "0.2.2") (d (list (d (n "frunk_core") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "10w2jkcx5wkb03a3dadbky5g1pbpdkagi7gk6a0gjss2ili7pcll")))

(define-public crate-frunk_derives-0.2.3 (c (n "frunk_derives") (v "0.2.3") (d (list (d (n "frunk_core") (r "^0.2.3") (d #t) (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "10vkmrsb0bw6p2zd061h885mq54avsm00s5wn8wmbw3ww4my450c")))

(define-public crate-frunk_derives-0.2.4 (c (n "frunk_derives") (v "0.2.4") (d (list (d (n "frunk_core") (r "^0.2.4") (d #t) (k 0)) (d (n "frunk_proc_macro_helpers") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0gqhbnwm7c53gahshq1s2f84q8zncyyhqmjygbxjx1wp1cw1ga84")))

(define-public crate-frunk_derives-0.3.0 (c (n "frunk_derives") (v "0.3.0") (d (list (d (n "frunk_proc_macro_helpers") (r "^0.0.3") (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0248cmyffdshyxcxpjw66x7k52qpzsi75nqhm2g8d46kb0zd48ld")))

(define-public crate-frunk_derives-0.3.1 (c (n "frunk_derives") (v "0.3.1") (d (list (d (n "frunk_proc_macro_helpers") (r "^0.0.4") (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "093pd27kwpda9a8nqy5xi7in4yqqjhw4ksvx48k1w7267dskd3ai")))

(define-public crate-frunk_derives-0.3.2 (c (n "frunk_derives") (v "0.3.2") (d (list (d (n "frunk_proc_macro_helpers") (r "^0.0.5") (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0rhrmirysfd09k1rvq9jxwmqri3r1hi27kiydhj1dkm73fm3whaj")))

(define-public crate-frunk_derives-0.4.0 (c (n "frunk_derives") (v "0.4.0") (d (list (d (n "frunk_proc_macro_helpers") (r "^0.1.0") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17m0bryq6wwhi1qd1f1k5yd1ihxjgawdxkjcs8qz18y59q44zg1x")))

(define-public crate-frunk_derives-0.4.1 (c (n "frunk_derives") (v "0.4.1") (d (list (d (n "frunk_proc_macro_helpers") (r "^0.1.1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0wzir38b6dasnrpn7m5n1khf4ziqyxx3r4bj0zz7rjdl5f8n8cdq")))

(define-public crate-frunk_derives-0.4.2 (c (n "frunk_derives") (v "0.4.2") (d (list (d (n "frunk_proc_macro_helpers") (r "^0.1.2") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0blsy6aq6rbvxcc0337g15083w24s8539fmv8rwp1qan2qprkymh")))

