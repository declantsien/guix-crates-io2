(define-module (crates-io fr un frunk_proc_macro_helpers) #:use-module (crates-io))

(define-public crate-frunk_proc_macro_helpers-0.0.1+pre (c (n "frunk_proc_macro_helpers") (v "0.0.1+pre") (d (list (d (n "frunk_core") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0fxb4ywr2rprix9rlv1qi50v1w1rxw1yw1mh257j7k56n0gb1b64") (y #t)))

(define-public crate-frunk_proc_macro_helpers-0.0.1 (c (n "frunk_proc_macro_helpers") (v "0.0.1") (d (list (d (n "frunk_core") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1x8z1p9zvjnh5b0s12p805g7sfxdv51fscq3467ffdnc77p10x6c")))

(define-public crate-frunk_proc_macro_helpers-0.0.2 (c (n "frunk_proc_macro_helpers") (v "0.0.2") (d (list (d (n "frunk_core") (r "^0.2.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1xh87x7wn06nk9k622h74hjirw5n0syr8wdqpl8r0skdr79pcw8j")))

(define-public crate-frunk_proc_macro_helpers-0.0.3 (c (n "frunk_proc_macro_helpers") (v "0.0.3") (d (list (d (n "frunk_core") (r "^0.3.0") (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1w6cmay4p4kvm48z0brws75yx35lak0a07z726py0mzn484qy30r")))

(define-public crate-frunk_proc_macro_helpers-0.0.4 (c (n "frunk_proc_macro_helpers") (v "0.0.4") (d (list (d (n "frunk_core") (r "^0.3.1") (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1gjd4pv2ja67yqwnz9s6mm5l0whnj7zlw5317vgy0jwpq7d4cig2")))

(define-public crate-frunk_proc_macro_helpers-0.0.5 (c (n "frunk_proc_macro_helpers") (v "0.0.5") (d (list (d (n "frunk_core") (r "^0.3.2") (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0p7fvnk7jwm9phw6kn4s1l6rfhr39248f489dkk8vb6jdpmvf3fn")))

(define-public crate-frunk_proc_macro_helpers-0.1.0 (c (n "frunk_proc_macro_helpers") (v "0.1.0") (d (list (d (n "frunk_core") (r "^0.4.0") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gpn2pznnlhcnxaslyh1i2h91hrrzc0ydf7wbwzpbih6y5bi5wcr")))

(define-public crate-frunk_proc_macro_helpers-0.1.1 (c (n "frunk_proc_macro_helpers") (v "0.1.1") (d (list (d (n "frunk_core") (r "^0.4.1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0l7gczq73i606qg4yyl8ky745bw92w7k94smlywgbc5y3dcjam01")))

(define-public crate-frunk_proc_macro_helpers-0.1.2 (c (n "frunk_proc_macro_helpers") (v "0.1.2") (d (list (d (n "frunk_core") (r "^0.4.2") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0b1xl4cfrfai7qi5cb4h9x0967miv3dvwvnsmr1vg4ljhgflmd9m")))

