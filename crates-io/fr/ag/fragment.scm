(define-module (crates-io fr ag fragment) #:use-module (crates-io))

(define-public crate-fragment-0.1.0 (c (n "fragment") (v "0.1.0") (h "1xhrn7r5h34qa8wb4ph26n9mxy3fgk0wnj53ky0614vvpm89379s")))

(define-public crate-fragment-0.1.1 (c (n "fragment") (v "0.1.1") (h "0i683df9ln5ar4379sxw13rn5lidzjgm4n560x6qj4l7f3c212jv")))

(define-public crate-fragment-0.1.2 (c (n "fragment") (v "0.1.2") (h "0s895frzlsgjwg444dzqsaivxr3qylzk9dc0yfygkndxj5yq77ky")))

(define-public crate-fragment-0.2.0 (c (n "fragment") (v "0.2.0") (h "13wirfc8x9q1yhs6h1d60xsis77n92f9njcygx61hhcnbqm4s2lj")))

(define-public crate-fragment-0.3.0 (c (n "fragment") (v "0.3.0") (h "1ssq25rndnmhzc7xf9ihk6li3mr7zhdmq6m1yn4g15xhsgmxbb7h")))

(define-public crate-fragment-0.3.1 (c (n "fragment") (v "0.3.1") (h "1cscfpc5jnildyx21afc5p7dz3y04wq1b300glfzhassaf9jbvch")))

