(define-module (crates-io fr ag frag) #:use-module (crates-io))

(define-public crate-frag-0.1.0 (c (n "frag") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1y58d9qa7ymkiqjqwkbp1p4g4z5q9lzb5p1i8whqifvh2lb7g2ql") (y #t)))

(define-public crate-frag-0.1.1 (c (n "frag") (v "0.1.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "19i330x4gp7n8xkrnnwayassbzp3xpc81dx6kxf1dsjlj3vai303") (y #t)))

(define-public crate-frag-0.1.2 (c (n "frag") (v "0.1.2") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1b9szrh8hxwwn572k5nvp76whfd92pw2a1bnr7yb6j2fmyhz2qm3") (y #t)))

(define-public crate-frag-0.1.3 (c (n "frag") (v "0.1.3") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1wjg7k681v6rb1d2s8divgpnlfx8zgdcl0rdd7qkcbggh9mbvs37") (y #t)))

(define-public crate-frag-0.1.4 (c (n "frag") (v "0.1.4") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1bdwb3y9yxfmr3ssjqxv5h0lls41vsj06cqgkp4dvzjhar8wndhf") (y #t)))

(define-public crate-frag-0.1.5 (c (n "frag") (v "0.1.5") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4.5") (d #t) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1wnvyshaa6hyajvq1a40iad0ih6avnl69i9mfqhqm27j6yn5hkmv")))

(define-public crate-frag-0.1.6 (c (n "frag") (v "0.1.6") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.5.0") (d #t) (k 0)) (d (n "png") (r "^0.17.13") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (d #t) (k 0)))) (h "1m28y4c55g7f5z96g42gbdcblfpcsx7izbgzwynhb0zwxqlvwwdl")))

