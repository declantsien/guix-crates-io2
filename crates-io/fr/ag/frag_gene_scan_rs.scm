(define-module (crates-io fr ag frag_gene_scan_rs) #:use-module (crates-io))

(define-public crate-frag_gene_scan_rs-1.0.1 (c (n "frag_gene_scan_rs") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0ldkjxysk777rszzg3n84c3kfmyadpzy2m7xgj5gg0isblvd8z4g")))

(define-public crate-frag_gene_scan_rs-1.1.0 (c (n "frag_gene_scan_rs") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0mzwkyk7zn7gbynggkdklg8vjxv3rgd2vw8lihza0mywb154lkls")))

