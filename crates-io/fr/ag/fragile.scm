(define-module (crates-io fr ag fragile) #:use-module (crates-io))

(define-public crate-fragile-0.1.0 (c (n "fragile") (v "0.1.0") (h "06gmhc0vcip8zx3wjrx9663m1rk2cga3iw682cfpnnkkc9jfp74f") (y #t)))

(define-public crate-fragile-0.2.0 (c (n "fragile") (v "0.2.0") (h "12m1k6byr0kp7vv1pwix2l6aalzll1l8aih66cha430542whrkwr") (y #t)))

(define-public crate-fragile-0.2.1 (c (n "fragile") (v "0.2.1") (h "1z5mkjxavv0fcgdh6cy7hhkr33cjpism06n5c9fd3wiv1a1pgxrg") (y #t)))

(define-public crate-fragile-0.3.0 (c (n "fragile") (v "0.3.0") (h "1yf2hmkw52x2dva3c9km1x8c2z5kwby7qqn8kz5ms3gs480i9y05") (y #t)))

(define-public crate-fragile-1.0.0 (c (n "fragile") (v "1.0.0") (h "1wlihmkjyhvl5rckal32p010piy1l15s6l81h7z31jcd971kk839") (y #t)))

(define-public crate-fragile-1.1.0 (c (n "fragile") (v "1.1.0") (h "0vaa2qzvmfs2sm67mdgh37nyb24mpgqgqp20b5z5nnjvkkwbi8cd") (y #t)))

(define-public crate-fragile-1.2.0 (c (n "fragile") (v "1.2.0") (d (list (d (n "slab") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1c9am2kxsimmk292nbm1dbw51rdfv2d3h6wwr14xgs251gk5imz9") (y #t)))

(define-public crate-fragile-1.2.1 (c (n "fragile") (v "1.2.1") (d (list (d (n "slab") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1az1dkdfxi08nb5a9xx28sxrvkjr370xivz27l9zdi8h5ffvip45") (y #t)))

(define-public crate-fragile-2.0.0 (c (n "fragile") (v "2.0.0") (d (list (d (n "slab") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1ajfdnwdn921bhjlzyvsqvdgci8ab40ln6w9ly422lf8svb428bc")))

(define-public crate-fragile-1.2.2 (c (n "fragile") (v "1.2.2") (d (list (d (n "new_fragile") (r "^2.0.0") (d #t) (k 0) (p "fragile")))) (h "1s2rz4cmmba5zi0gf2h6hprrcrf0wm83c1y45sdls09z99f4qimp") (f (quote (("slab" "new_fragile/slab"))))))

