(define-module (crates-io fr ag fragments) #:use-module (crates-io))

(define-public crate-fragments-0.0.0 (c (n "fragments") (v "0.0.0") (h "17qjxykancl54il917y092lvfa7viqw80hcrlhnxpxql3zz4r6g6")))

(define-public crate-fragments-0.1.0 (c (n "fragments") (v "0.1.0") (h "0bjzi35999cv99n9injwh73fym73z28fv80pfj78wilsmny1mqsq")))

