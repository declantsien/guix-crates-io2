(define-module (crates-io fr ip friperms) #:use-module (crates-io))

(define-public crate-friperms-0.1.0 (c (n "friperms") (v "0.1.0") (d (list (d (n "friperms_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ckndfmih7ws33jh8pqhk962mv24m2k5s4cjcwl47gzbpmx0119w") (f (quote (("derive" "friperms_derive") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-friperms-0.1.1 (c (n "friperms") (v "0.1.1") (d (list (d (n "friperms_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n3d41jnjgmp62zlcn6ygmvfq5dp4m9bbf7fhcj3aw5y4s2271yy") (f (quote (("derive" "friperms_derive") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-friperms-0.2.0 (c (n "friperms") (v "0.2.0") (d (list (d (n "friperms_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n6cf2yp3gn54zgfb0qwnjwczgf6q3ccjgklf1i198fra46j6p2g") (f (quote (("derive" "friperms_derive") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-friperms-0.2.1 (c (n "friperms") (v "0.2.1") (d (list (d (n "friperms_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 2)))) (h "138p59ngf55nylhcfmvfkami57cy6rg66y4fxww3qnnhk26fxfwx") (f (quote (("derive" "friperms_derive") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

