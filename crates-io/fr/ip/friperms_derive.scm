(define-module (crates-io fr ip friperms_derive) #:use-module (crates-io))

(define-public crate-friperms_derive-0.1.0 (c (n "friperms_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "0lkf3f6lh69dfm3rhsv2kn102cxx3hqymivvzwj2zcadxy0dcsrq")))

(define-public crate-friperms_derive-0.1.1 (c (n "friperms_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "0axy3qb6iiqhni3f4as631lsdzr9cjl3dqzrj7v4b7rnbdgj5xk9")))

