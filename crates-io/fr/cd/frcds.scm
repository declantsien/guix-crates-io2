(define-module (crates-io fr cd frcds) #:use-module (crates-io))

(define-public crate-frcds-0.1.0 (c (n "frcds") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "01kpxnaapnd8br0gmr9d1l8q14yghd232lg6c524ka2dfbk6gvqn")))

