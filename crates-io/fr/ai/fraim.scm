(define-module (crates-io fr ai fraim) #:use-module (crates-io))

(define-public crate-fraim-0.0.0 (c (n "fraim") (v "0.0.0") (h "0vrjlbsp6xf9ac1ykbxhcs581k67h2zlh2frlz74fz4ydyy2kl7v")))

(define-public crate-fraim-0.0.1 (c (n "fraim") (v "0.0.1") (h "0v90y8h8zwsbmyx71rli7pqv3qh9d6w3cmn84hn6xnj1c40n24gn")))

