(define-module (crates-io fr cv frcv) #:use-module (crates-io))

(define-public crate-frcv-0.1.0 (c (n "frcv") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0mzvdhs211b52cc39d41m19skmfvwlz91iwpnmp15s737a1wj787") (y #t)))

(define-public crate-frcv-0.1.1 (c (n "frcv") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1nnmyka72425fqpbd3ihhyibv9zl3am5893b06dy969wl2h1bwsi") (y #t)))

