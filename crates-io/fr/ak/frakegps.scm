(define-module (crates-io fr ak frakegps) #:use-module (crates-io))

(define-public crate-frakegps-0.1.2 (c (n "frakegps") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "web-view") (r "^0.6.0") (d #t) (k 0)))) (h "1q3zl2pig0gjkpiy0b2yqw3in4cpiygnf94xppmphlf43bfzznyk")))

(define-public crate-frakegps-0.1.3 (c (n "frakegps") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "0ian07d7nnlj6pwqprm6qk6mzj3ab71f5s0xc80bx9plzar7v24h")))

(define-public crate-frakegps-0.2.0 (c (n "frakegps") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "wry") (r "^0.29.0") (d #t) (k 0)))) (h "03aw6y5zlpbs5bfa8m1jgqkcznymsf9hfwy3lh7nm7qjgjc3labl")))

