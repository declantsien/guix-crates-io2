(define-module (crates-io fr ot frotate) #:use-module (crates-io))

(define-public crate-frotate-0.1.1 (c (n "frotate") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f8avplwkvaf24vp6mwkqrhh6d9iw3sr7jk3ms620rgq33fag1qn")))

