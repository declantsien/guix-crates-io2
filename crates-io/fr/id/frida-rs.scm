(define-module (crates-io fr id frida-rs) #:use-module (crates-io))

(define-public crate-frida-rs-0.1.0 (c (n "frida-rs") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.37") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_with") (r "^1.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "1q7cxa1v3w17zi8a607s6yhvxxnkcjinxjjjwj55mfybggsk60hd") (f (quote (("default" "console_error_panic_hook"))))))

