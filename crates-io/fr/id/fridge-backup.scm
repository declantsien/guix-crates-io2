(define-module (crates-io fr id fridge-backup) #:use-module (crates-io))

(define-public crate-fridge-backup-0.1.0 (c (n "fridge-backup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0scf6ilsxs1yikm42ank0fy9f0f7b0hrxh9axfvz3ljj2ndsf567")))

(define-public crate-fridge-backup-0.2.0 (c (n "fridge-backup") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1z15sg2drnvn35kfw4frbbqaspylfijq042lmqii9wcfad38i4gc")))

(define-public crate-fridge-backup-0.2.1 (c (n "fridge-backup") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1p6hf41gwbi9k4wv1pjj4yyqy1458mzir30nzxa43x6dnaj5bb8c")))

(define-public crate-fridge-backup-0.2.2 (c (n "fridge-backup") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1v85x0g1bp2k2vn8554hh6c0pi3arf5b1hdada73c6gfbswavnn9")))

(define-public crate-fridge-backup-0.2.3 (c (n "fridge-backup") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1dk5cb5xg7pbjgfk0m8561akjg6z2rj479g553y3j4fh5qavhwn7")))

