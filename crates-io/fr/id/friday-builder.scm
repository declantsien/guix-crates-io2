(define-module (crates-io fr id friday-builder) #:use-module (crates-io))

(define-public crate-friday-builder-1.0.0 (c (n "friday-builder") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colored") (r "^1.9.4") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "job_scheduler") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0a5kdrna632ms8ws66jy0fcfaiqrn15d027dnl39vnl56y67vss9")))

