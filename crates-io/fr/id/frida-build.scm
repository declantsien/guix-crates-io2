(define-module (crates-io fr id frida-build) #:use-module (crates-io))

(define-public crate-frida-build-0.1.0 (c (n "frida-build") (v "0.1.0") (h "1g7sp088bqi0ayk9d27136aqs90w9s7zz51ifwjwdmd6gyq5pnyl")))

(define-public crate-frida-build-0.2.0 (c (n "frida-build") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.33") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)))) (h "02hsw1axad5qhy15cd19jnmwx3qbxs537574x9vaxsncxmg5skbc")))

(define-public crate-frida-build-0.2.1 (c (n "frida-build") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.33") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)))) (h "0mq26sy5f24xypjm6yp0m5im4hk28p15czni6grkhvx6k1f55cfj")))

(define-public crate-frida-build-0.13.6 (c (n "frida-build") (v "0.13.6") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)))) (h "0hjkmmih5in7qygr2qsf8pw4y5xqfvjfqcl5488pmsaja4qah3n3")))

