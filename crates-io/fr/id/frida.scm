(define-module (crates-io fr id frida) #:use-module (crates-io))

(define-public crate-frida-0.1.0 (c (n "frida") (v "0.1.0") (d (list (d (n "frida-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0md1vcl0j167x8m1j4nycag11dbl149cs0h4z7hw8v5jlqlcc128")))

(define-public crate-frida-0.1.1 (c (n "frida") (v "0.1.1") (d (list (d (n "frida-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1a8v8y61jq6kcylyd2w6zhja58ygxdjydj4dnphygpcw2hmhwgm1") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.1.2 (c (n "frida") (v "0.1.2") (d (list (d (n "frida-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "15hakqzz778lrndfdvz6969z9ayssj94n70l03xiz6rkjkgld806") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.1.3 (c (n "frida") (v "0.1.3") (d (list (d (n "frida-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "138hv6q3sgd83q3k6znl2nxfykrydrvkn3fvnfcdcg1wj17z3snc") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.2.0 (c (n "frida") (v "0.2.0") (d (list (d (n "frida-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0fc1malz6nz6hdsrjfzxr4kdsgrmgyxbggn76kp3h4r9bh1l7f6y") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.2.1 (c (n "frida") (v "0.2.1") (d (list (d (n "frida-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0izymff8cfhpilqgz3f7rkzvn0yc4p1pzi5fd003zvldf2pk0xhz") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.2.2 (c (n "frida") (v "0.2.2") (d (list (d (n "frida-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "17md64z86gd8h0izd4kznm35w61fn1n4hzq25z11k5id26zr60jq") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.2.3 (c (n "frida") (v "0.2.3") (d (list (d (n "frida-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vr3cdafczbfh8j9affmhw8j282g0b4z5dy328198dh2p0l21zys") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.3.0 (c (n "frida") (v "0.3.0") (d (list (d (n "frida-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0l9sm1c1d8bf4fsqg9aqdkmx3qg7blia2gs5niivydkckg8105ng") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.4.0 (c (n "frida") (v "0.4.0") (d (list (d (n "frida-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c3qp81j4yl977rwz8jnhfd3i5w9sczcjnx2lhy4q110746gaw4i") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.4.1 (c (n "frida") (v "0.4.1") (d (list (d (n "frida-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mmrbxlv9a513x71w106flmf7g5kgp76y30k5555k1miliwdd1pr") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.4.2 (c (n "frida") (v "0.4.2") (d (list (d (n "frida-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "061r171l7qdml8xkf0i7flqxldj1g9k11193kpk94jqf611z6xvd") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.4.4 (c (n "frida") (v "0.4.4") (d (list (d (n "frida-sys") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "169xfw32449065kp4qmp2hzxf51n7m67x9rq27627ha5bs39knha") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.4.5 (c (n "frida") (v "0.4.5") (d (list (d (n "frida-sys") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "142gbv4yc1qw9snxbcdcsnrkxvrj0igpl2ga1w8y6b95mg4wcyaw") (f (quote (("auto-download" "frida-sys/auto-download"))))))

(define-public crate-frida-0.13.6 (c (n "frida") (v "0.13.6") (d (list (d (n "frida-sys") (r "^0.13.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1czy03pjzrkqqvksmln6j6m1sbwlkh98dvhapb6aa2g3h26pfizx") (f (quote (("auto-download" "frida-sys/auto-download"))))))

