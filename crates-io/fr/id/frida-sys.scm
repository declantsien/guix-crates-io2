(define-module (crates-io fr id frida-sys) #:use-module (crates-io))

(define-public crate-frida-sys-0.1.0 (c (n "frida-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "125ij743rf2lpz4r65gik6rp70x8c3v4qvb8gcpgl2ddai64cczh")))

(define-public crate-frida-sys-0.1.1 (c (n "frida-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "1c1xa8jk6ac6hxk6nn1rq9yhhjkzxy0ik3a2b53vj5hfr4y4b02q") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.1.2 (c (n "frida-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "1z6kall5jrk9qdpvr1nhiibfnlk115sb1l972qlli1n96gm0grby") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.1.3 (c (n "frida-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "0f14nn3l5jjrda4mgs98aagw7z1qn7q2cg649kyy7x6kn0fy5s0b") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.2.0 (c (n "frida-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.0") (o #t) (d #t) (k 1)))) (h "1i6y4wamcmcxi06h7dxz6k7zhvpvmmb9kr4pdyvlp2pxd35i8w3i") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.2.1 (c (n "frida-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.0") (o #t) (d #t) (k 1)))) (h "021x027mrkkkiqlpdr6j3hslz41x42hvd8wg7n0qh01j1bvplg3l") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.2.2 (c (n "frida-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.0") (o #t) (d #t) (k 1)))) (h "1vy9zh84xacx3ij2w7jdq0l6p6abz0simjjy38j7cj9ryvf2ci60") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.2.3 (c (n "frida-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.0") (o #t) (d #t) (k 1)))) (h "0nx35i28k060siwq544f6h3ppvkg55lr5xbf91j88p1hf95gspx9") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.3.0 (c (n "frida-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.1") (o #t) (d #t) (k 1)))) (h "10xkyb5i40b2zip59hhivkr1qyam0axfhh1dizjcqizif048qb9n") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.4.0 (c (n "frida-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.1") (o #t) (d #t) (k 1)))) (h "11fglpwv6ns8yckpwblqhcz4g8dm5k15gyi7kvfmbw28ipl2zwwv") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.4.1 (c (n "frida-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.1") (o #t) (d #t) (k 1)))) (h "0v29gw817bxp9644grcsfwiazgcilq54pnd9q6jib24ppfqzvkii") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.4.2 (c (n "frida-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.1") (o #t) (d #t) (k 1)))) (h "1kjrpznzsasjd4mwgrpd1niqcz2qgbryh0y31b5j4kjwlm2sv3pb") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.4.3 (c (n "frida-sys") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.1") (o #t) (d #t) (k 1)))) (h "1ng67xqmjxvrkv15bgy8si5a3g19apzx562qb2jckvx2njq0wgjk") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.4.4 (c (n "frida-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.1") (o #t) (d #t) (k 1)))) (h "1l2g452fvfqz4glj5vmp431gs433fqfcph2xflk18ip7i46bljs9") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.4.5 (c (n "frida-sys") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "frida-build") (r "^0.2.1") (o #t) (d #t) (k 1)))) (h "00sirjq3lzn329kq2kfc9mp6samnqj9dcawl3iway6z3dxcjdzsl") (f (quote (("auto-download" "frida-build"))))))

(define-public crate-frida-sys-0.13.6 (c (n "frida-sys") (v "0.13.6") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "frida-build") (r "^0.13.6") (o #t) (d #t) (k 1)))) (h "00y31zimwj7579mfdiv3jf6hbiz4ksc5fdbigl0fsqc9wcpq3v1y") (f (quote (("auto-download" "frida-build"))))))

