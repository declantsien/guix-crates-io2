(define-module (crates-io fr id fridge-r0-verifier) #:use-module (crates-io))

(define-public crate-fridge-r0-verifier-0.1.0 (c (n "fridge-r0-verifier") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-markdown") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qf9fx2zvmnk033vkngq1cvqp77828y2cp4k5wx6amg8z1gpn3nv") (y #t)))

(define-public crate-fridge-r0-verifier-0.0.32 (c (n "fridge-r0-verifier") (v "0.0.32") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-markdown") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rljsigd0an5967kk6as3vdaqkkmb0442h97wi7qdvckfd4yg336")))

