(define-module (crates-io fr oo froop) #:use-module (crates-io))

(define-public crate-froop-0.1.0 (c (n "froop") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0pkm1yk5pqvmsapijxfsjj1rr1p505k1wnc93vgz3p9axkg49cn5")))

(define-public crate-froop-0.1.1 (c (n "froop") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1bfdcyvy8pgs0ryjxp9g6myr7g2m5za8mrw0v56y37z1s6kfirpm")))

