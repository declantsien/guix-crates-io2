(define-module (crates-io fr ac fractal-utils) #:use-module (crates-io))

(define-public crate-fractal-utils-0.1.0 (c (n "fractal-utils") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1w2qsac96vqn84db2335xaklmip868f47wk56q5y9npq6g9wxj34")))

(define-public crate-fractal-utils-0.1.1 (c (n "fractal-utils") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0z4k2vpi59c3461akjvmf4539m9r9wsrh4zl834dydpllx0fclfs")))

(define-public crate-fractal-utils-0.1.2 (c (n "fractal-utils") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17b4rbsfshi4mdzpd3izhzsb4d6py9dyym5qragir4g7bfb0ikzq")))

(define-public crate-fractal-utils-0.1.3 (c (n "fractal-utils") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1kn4ywprgmdba859va1hlrwfjk0s0wpa86dzhf7hvp3grpirfd51")))

(define-public crate-fractal-utils-0.1.4 (c (n "fractal-utils") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1l4gxzkdrpxs4x1w2im9c3c8zv1sivw01grzh2z6k4rm4pjzr5kz")))

(define-public crate-fractal-utils-0.1.5 (c (n "fractal-utils") (v "0.1.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "00i41msx2dlj6gan8z61g28iwxrrkk91a35xzmam8dzmh1nr5417")))

(define-public crate-fractal-utils-0.1.6 (c (n "fractal-utils") (v "0.1.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0153z4hyhrbyai0snk7pfq3npf1gs3icpz3mihvg8glqw63aajnf")))

(define-public crate-fractal-utils-0.1.7 (c (n "fractal-utils") (v "0.1.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1wzn7nazj6s0ahkd3d9cpmgbgja51miam61iccsky1l5m147qp46")))

(define-public crate-fractal-utils-0.1.8 (c (n "fractal-utils") (v "0.1.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0v033xvf6jr31rbxnldcvpyp2kdj3znj8jlgpsmrw4lxl8m7w7s8")))

(define-public crate-fractal-utils-0.1.9 (c (n "fractal-utils") (v "0.1.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13jhm6zvzj0qx5pkxpzb372kk6d2vslkjb95nagr2m72a02yz32p")))

(define-public crate-fractal-utils-0.2.0 (c (n "fractal-utils") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17pp8689z2y76k7sq4p6ad4sx2g46fi8njswy7pyhh0ffcmfbknb")))

(define-public crate-fractal-utils-0.2.1 (c (n "fractal-utils") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f0f6mbf8waslc50mjrg9430zlv25745i2w6ilacg0gihvq7s7h4") (f (quote (("json-types") ("default")))) (y #t)))

(define-public crate-fractal-utils-0.2.2 (c (n "fractal-utils") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gd9lvxvl3g241pgiv9d1007868cx6502sv0hrmdqlh6pfqrmny2") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.2.3 (c (n "fractal-utils") (v "0.2.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1lfpxbhkfwrx8h63sk4sm2s85l12ly6gxajp4jq25jy3js10zm97") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.2.4 (c (n "fractal-utils") (v "0.2.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0n6hrm8pf0ya2grr6s46dy3rlgc3zklpxgivlcsvnzbpizbv0da1") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.2.5 (c (n "fractal-utils") (v "0.2.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1v7ik1x483sryy581q5krfjsqglr2lfd95rw2ybzkb75pwg2fkzh") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.2.6 (c (n "fractal-utils") (v "0.2.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "18sgbzj7p7vjzrxy4ih375c3p35ic6ag5s9nhvl757sfbv4vnc82") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.3.0 (c (n "fractal-utils") (v "0.3.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ds0lccskmf4pcz0dibl93w6qsrnk6js9cx3nnfggwxiyb1pvpsn") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.3.1 (c (n "fractal-utils") (v "0.3.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06vv2f54k8cy1ip2av2f06b0whymkkpqf44z27dq03savs29fbph") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.3.2 (c (n "fractal-utils") (v "0.3.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p0l6lyfpfbpqfhmd2zsdabq0n9wsqr7981w63wjycbfrhm6kg9a") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.3.3 (c (n "fractal-utils") (v "0.3.3") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ls61d7aggfdrdl43i5i0az43mbn70cqa9x3v0ihkdkwacgcf9rw") (f (quote (("json-types") ("default"))))))

(define-public crate-fractal-utils-0.3.4 (c (n "fractal-utils") (v "0.3.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1lxckvfj59mylkah84cnmn9l737jl12sdlidwndihavqqm2i4k9j") (f (quote (("json-types") ("default"))))))

