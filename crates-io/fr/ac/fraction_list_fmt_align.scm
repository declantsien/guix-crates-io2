(define-module (crates-io fr ac fraction_list_fmt_align) #:use-module (crates-io))

(define-public crate-fraction_list_fmt_align-0.1.0 (c (n "fraction_list_fmt_align") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "09ms5chm8bg2a5pll7jb2g2g6nghp5hd0m74ijy9mfjgnjg6d4pw")))

(define-public crate-fraction_list_fmt_align-0.2.0 (c (n "fraction_list_fmt_align") (v "0.2.0") (h "0fgwzqph8b1vznbg4s9fbs8jj7vmqfkb0v16slk3l2j7m8r4gnrc")))

(define-public crate-fraction_list_fmt_align-0.2.1 (c (n "fraction_list_fmt_align") (v "0.2.1") (h "0hk85mzyjnkxic3ilr8xx1zjl9lkhicb8y32ihvdzz6fl331caqh")))

(define-public crate-fraction_list_fmt_align-0.2.2 (c (n "fraction_list_fmt_align") (v "0.2.2") (h "1nxrz0ljvcfnn7bivyzs2ximx962jv1ilwgw7kwdr2mic3c25ddr")))

(define-public crate-fraction_list_fmt_align-0.2.3 (c (n "fraction_list_fmt_align") (v "0.2.3") (h "16hkgf7xl01p5f7dvk13l40l8wqw4nypinz5y5lbj5mszbscr31l")))

(define-public crate-fraction_list_fmt_align-0.3.1 (c (n "fraction_list_fmt_align") (v "0.3.1") (h "1ldjjr9gs2ljqh217gkj0h0n6609fvzq86nwqs5v626bwim5wbrx")))

