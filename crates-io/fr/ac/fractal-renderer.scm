(define-module (crates-io fr ac fractal-renderer) #:use-module (crates-io))

(define-public crate-fractal-renderer-0.1.0 (c (n "fractal-renderer") (v "0.1.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ravif") (r "^0.8.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1j82zz0fxndz1hx3n7c8n7pbsb86c8y4k429ffnryb76caxw32cb") (r "1.56")))

