(define-module (crates-io fr ac fractions-rs) #:use-module (crates-io))

(define-public crate-fractions-rs-0.1.0 (c (n "fractions-rs") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "02p3d5fpykzh0h37vclgz128shdl1gnwfklfrxpzsv87nhj0g0zb")))

(define-public crate-fractions-rs-0.1.1 (c (n "fractions-rs") (v "0.1.1") (d (list (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0cpb5bjw3bjmwnx0amjy1sx87pkskxb0jbf03d5x5b2xfhnbhsni")))

(define-public crate-fractions-rs-0.1.2 (c (n "fractions-rs") (v "0.1.2") (d (list (d (n "autocfg") (r "^1.0.0") (d #t) (k 1)) (d (n "num-bigint") (r "^0.4.0") (o #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (f (quote ("i128"))) (k 0)) (d (n "num-traits") (r "^0.2.18") (f (quote ("i128"))) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0jss3s6gbaqzzdfzi5dm0ws0k8dkc3qpnfa1jvrwka66r1hprmnf") (f (quote (("std" "num-integer/std" "num-traits/std") ("num-bigint-std" "num-bigint/std") ("default" "num-bigint-std" "std"))))))

