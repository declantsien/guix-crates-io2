(define-module (crates-io fr ac fracter) #:use-module (crates-io))

(define-public crate-fracter-0.1.0 (c (n "fracter") (v "0.1.0") (d (list (d (n "gloo-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "seoul") (r "^0.1.0") (d #t) (k 0)) (d (n "sycamore") (r "^0.9.0-beta.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (f (quote ("Location" "PopStateEvent"))) (d #t) (k 0)))) (h "0zf92dnjs65bkfzn344m3h3sc8w15qsjz4b770qfq066kva1c92y")))

