(define-module (crates-io fr ac fractal-dto) #:use-module (crates-io))

(define-public crate-fractal-dto-0.0.1 (c (n "fractal-dto") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jf1xm1nsg0q7ln49sfbmh6h8va89d9x5gd3hjwqisn3k6724q8a")))

(define-public crate-fractal-dto-0.0.2 (c (n "fractal-dto") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0bnk8qii6bj8pf49hcgnyzgw32m9ap11dqs2wcqlpvldgnwrnyfa")))

(define-public crate-fractal-dto-0.0.3 (c (n "fractal-dto") (v "0.0.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "15l2ngpfkr16xw093pqymhf4vf5gzavjn2ia49qywswl1jk75iyz")))

(define-public crate-fractal-dto-0.0.4 (c (n "fractal-dto") (v "0.0.4") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "002lgy13flwkga8ixk5hsr03zrvsl7hmnvkb69wraydpx4vwbhlq")))

(define-public crate-fractal-dto-0.0.5 (c (n "fractal-dto") (v "0.0.5") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0k9jdzlslnckj1gvn0pxv06b8g3wl0isnhywmxc4x9k2z0c2snna")))

(define-public crate-fractal-dto-0.0.8 (c (n "fractal-dto") (v "0.0.8") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0kxg0pr1jvam8a71y83svjmwk1d6h9jsxyjra7mwc1vnphkxpb2s")))

(define-public crate-fractal-dto-0.0.9 (c (n "fractal-dto") (v "0.0.9") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07ajpnnyri1dbfv1zymj41zdqwkq86dvxbdnaqv2plygn4ard1k7")))

(define-public crate-fractal-dto-0.0.10 (c (n "fractal-dto") (v "0.0.10") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1am9rb9clvzs4v1r30m1vl25hwyh5r1kj025570vg8nlq7z6dvvx")))

(define-public crate-fractal-dto-0.0.11 (c (n "fractal-dto") (v "0.0.11") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1y4kx4jii2rafwkwslk4wj0kqpwnsag4h0v88zhdkhdl162n9hzy")))

(define-public crate-fractal-dto-0.0.12 (c (n "fractal-dto") (v "0.0.12") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0hv52ai02albj3x6vqvq7b8qsaj8i9fqrrvrhfhqqvsz1968azar")))

(define-public crate-fractal-dto-0.0.13 (c (n "fractal-dto") (v "0.0.13") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13bgykcn3nn4pp8vd4bbq3w4bdr8fxp5sm913nh76fkl4xr74kd3")))

(define-public crate-fractal-dto-0.0.14 (c (n "fractal-dto") (v "0.0.14") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0q90vs5mk6f8mwgnb7sk654p0krkcs9zlp31cmc7zqad7jsviixh")))

(define-public crate-fractal-dto-0.0.15 (c (n "fractal-dto") (v "0.0.15") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1xcffwp30wbay76yqgand547xmn97smby6d2f59cgw3ij0gjn9l5")))

(define-public crate-fractal-dto-0.0.16 (c (n "fractal-dto") (v "0.0.16") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1a27s6a2iy1afkkw5gpvgp8v3igx0glsxlik1qh7nwhqz49wsb7g")))

(define-public crate-fractal-dto-0.0.17 (c (n "fractal-dto") (v "0.0.17") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0877x0jvrwq0dc9w5wxspy5q74xx6c6gc3kr6iaaxrx0k1y191di")))

(define-public crate-fractal-dto-0.0.18 (c (n "fractal-dto") (v "0.0.18") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cs84whqjpd6viyzvpqclh9vi9rmd3x478l7736nzxycpw971dd7")))

(define-public crate-fractal-dto-0.0.19 (c (n "fractal-dto") (v "0.0.19") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0sjvgw6yxsna62k4dfmbpvqi48w9h4pczyan4v4h6v5bhygsx37f")))

(define-public crate-fractal-dto-0.0.20 (c (n "fractal-dto") (v "0.0.20") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1bakyp5h6in9zx48ixkhqp10l95xmg2dwk2jk9vbhpprdxcv06li")))

(define-public crate-fractal-dto-0.0.21 (c (n "fractal-dto") (v "0.0.21") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0bis7gws8z22iybdjyfzw549avb8qldh7kalzaf7c9y8n64z74j6")))

(define-public crate-fractal-dto-0.0.22 (c (n "fractal-dto") (v "0.0.22") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1gbgbkvfihba6q66pzlszrc5dcg3p0i48qbghjv0dch4j8hrjidd")))

(define-public crate-fractal-dto-0.0.23 (c (n "fractal-dto") (v "0.0.23") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0i2qcakihvfxmfmhpl4fa7rlvj3kz7dnrmjrixgki8czfr2209z8")))

(define-public crate-fractal-dto-0.0.24 (c (n "fractal-dto") (v "0.0.24") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cyfjzbnnnxnwa3h7ccnrq0a6ky0j3zb33q2rd1xa7wrs7vxrykr")))

(define-public crate-fractal-dto-0.0.25 (c (n "fractal-dto") (v "0.0.25") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0hn3arw82isx6jsqj73bg7jicl84m7innvdv04kxvnximrkh6aad")))

(define-public crate-fractal-dto-0.0.26 (c (n "fractal-dto") (v "0.0.26") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1dbbvfjq5h4n7g3hxlzpfpyv22qngh4g38yppqlz4f6rgl70ya6g")))

(define-public crate-fractal-dto-0.0.27 (c (n "fractal-dto") (v "0.0.27") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f7qafwzb72qfq2ajj3qb0h1x8xfgm67xi3k1k3qj2dh4c4bxp0y")))

(define-public crate-fractal-dto-0.0.28 (c (n "fractal-dto") (v "0.0.28") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0v1zzqmil45cm9n03ya0avg6258x3a9n7m7r162sn7yk34d3ccd8")))

(define-public crate-fractal-dto-0.0.29 (c (n "fractal-dto") (v "0.0.29") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1cm2lpasww7bsi3fywg6j5ax22vsikzkck21f1s29s03rz1aj7dn")))

(define-public crate-fractal-dto-0.0.30 (c (n "fractal-dto") (v "0.0.30") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0aj27cyy5l4fl30s3i1qy0gbx47a188dk3aa02aq6j6969y17hs1")))

(define-public crate-fractal-dto-0.1.0 (c (n "fractal-dto") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1kjhaw4xpymbfhk9zwj2bk51h5404s37vpb5xxalf7wjdfw0q2c8")))

(define-public crate-fractal-dto-0.2.0 (c (n "fractal-dto") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jjikv0w4d8x63javjf1d2gv7g7iyxmdm88cywfrrcnyg4xwmk3n") (y #t)))

(define-public crate-fractal-dto-0.2.1 (c (n "fractal-dto") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0q3d4asi1px9z0pc9pvcdqq9vizqkaxmzdi259rj2ykx56imp2xc")))

(define-public crate-fractal-dto-0.3.0 (c (n "fractal-dto") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1234ma5ipzv4fwybqv44d1lyy346dlp9a1v8mg9zw2ln3x0fypbf") (y #t)))

(define-public crate-fractal-dto-0.3.1 (c (n "fractal-dto") (v "0.3.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1hywjvrqmfd53n7kzdfl5wnpkm5h4x6ariip8xwyv8bn9rf7ddaq") (y #t)))

(define-public crate-fractal-dto-0.3.2 (c (n "fractal-dto") (v "0.3.2") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "195d4g1n8cl5jl3s03c3fflj9dywyywrln0l02898bvrvqwhdr90")))

(define-public crate-fractal-dto-0.3.5 (c (n "fractal-dto") (v "0.3.5") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gcz2pwk41qqx3ihyy3948svij8hljdzn1pamqlaghl22a6wyk2i") (y #t)))

(define-public crate-fractal-dto-0.3.6 (c (n "fractal-dto") (v "0.3.6") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1cfdlz9ka4kc02abws2wzdcyapzivp9pnkm1ssd8d11f8wl7vfy8")))

(define-public crate-fractal-dto-0.3.7 (c (n "fractal-dto") (v "0.3.7") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0aqbpdd2c2rl4cxfnsdc4rl4lchf1avai1wvpp9bvwbpak99sddv")))

(define-public crate-fractal-dto-0.3.8 (c (n "fractal-dto") (v "0.3.8") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17vzl2nca4ifsqlfp0ny2qzl92dv159ghawbppzwjmbm1pqds2zg")))

(define-public crate-fractal-dto-0.3.9 (c (n "fractal-dto") (v "0.3.9") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1c860xk61m4rz467cvl5iqq51prb7mbwq1cazfh8knpdnhgakklh")))

(define-public crate-fractal-dto-0.4.0 (c (n "fractal-dto") (v "0.4.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0sm5y5rqav5dyd9x5rfhia22vlbpjhq37asiqlmm2bc3ly0bxq2x")))

(define-public crate-fractal-dto-0.4.1 (c (n "fractal-dto") (v "0.4.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02pq5wp4xyc7q4g98iy3rlsrhqiaijraay6cb00qb23injf5iy7g")))

(define-public crate-fractal-dto-0.5.0 (c (n "fractal-dto") (v "0.5.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0zhi4q4pzxzigidnh3y60jamaw17mgar6741nijhivlj4in9wv46")))

(define-public crate-fractal-dto-0.6.0 (c (n "fractal-dto") (v "0.6.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1vb6wjjnc4wj0p4yvk65wfksyarqqg6kqgyi943a1x3ziyvgbszr") (y #t)))

(define-public crate-fractal-dto-0.6.1 (c (n "fractal-dto") (v "0.6.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1z4vks3i2iv100f79wjl9191di1syi6i5w7z21c5gkbanrsmx0zx")))

(define-public crate-fractal-dto-0.6.2 (c (n "fractal-dto") (v "0.6.2") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0j3hbsn1qicqdwhl92xbznh9mkl34ihyx7c7k7d1ff82ipr50ci2")))

(define-public crate-fractal-dto-0.7.0 (c (n "fractal-dto") (v "0.7.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0kqnpla7s1lr2znq484l6lrn7zp3bgxihx8nrmy6cikjhycj7f2b")))

(define-public crate-fractal-dto-0.7.1 (c (n "fractal-dto") (v "0.7.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1p18l7bl0gkz2c0qqz55m5c63vsrl3lgw3ps2j80k98cbwiqlx2f")))

(define-public crate-fractal-dto-0.8.0 (c (n "fractal-dto") (v "0.8.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "160jzwji7s35mw5sy6mhlphr8lkk0x8p05vkry4cara2sl3scpd7")))

(define-public crate-fractal-dto-0.8.1 (c (n "fractal-dto") (v "0.8.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1in1rj0bxzbm38llyhjskq61b32w28fnj8ij93l39zci387iwv3g")))

(define-public crate-fractal-dto-0.8.2 (c (n "fractal-dto") (v "0.8.2") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0mrrp39pb0m4hlfrh9wag2qc2diwr2zazldsrgzj9q7lsd5ksnn8")))

(define-public crate-fractal-dto-0.8.3 (c (n "fractal-dto") (v "0.8.3") (d (list (d (n "chrono") (r "^0.2") (f (quote ("rustc-serialize"))) (d #t) (k 0)) (d (n "fractal-utils") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1kmy15f05rr30r0szzx7qa7sv4il30if1r047mw9sr1c7lsrzjnz")))

