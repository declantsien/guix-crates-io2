(define-module (crates-io fr ac fractal) #:use-module (crates-io))

(define-public crate-fractal-0.1.0 (c (n "fractal") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "dwt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)) (d (n "statistics") (r "*") (d #t) (k 0)))) (h "17jmcvfnzn6n8jjiqklmayiyhcb28f5acinxp81ci7az9vh8d8rn")))

(define-public crate-fractal-0.1.1 (c (n "fractal") (v "0.1.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "dwt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)) (d (n "statistics") (r "*") (d #t) (k 0)))) (h "1gsn2fwk71ds6s88n384f2lgvbylvv4cf1fi248fn5clhxgg175f")))

(define-public crate-fractal-0.2.0 (c (n "fractal") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "dwt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)) (d (n "statistics") (r "*") (d #t) (k 0)))) (h "05flf0is7p7a6d4wprpb0rqfkk47m7z602378paamys66k3harqs")))

(define-public crate-fractal-0.3.0 (c (n "fractal") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "dwt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 0)) (d (n "statistics") (r "*") (d #t) (k 0)))) (h "18cnkj4qm9s1lpl1mhp7bhggrsfxwz5rg3k81i1y24g345zqlc0p")))

(define-public crate-fractal-0.4.0 (c (n "fractal") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "dwt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 0)) (d (n "statistics") (r "*") (d #t) (k 0)))) (h "0h5m8z4mh164mvyvi2zklvi02r98rf4r2sky9dn7my6fafyh3a21")))

(define-public crate-fractal-0.5.0 (c (n "fractal") (v "0.5.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "dwt") (r "*") (d #t) (k 0)) (d (n "probability") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 0)) (d (n "statistics") (r "*") (d #t) (k 0)))) (h "03ipzzwpl6hzminwa8ddmlc7cndc6yp6zyknv855h8axf1prjgp7")))

(define-public crate-fractal-0.5.1 (c (n "fractal") (v "0.5.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "dwt") (r "*") (d #t) (k 0)) (d (n "probability") (r "^0.12") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 0)) (d (n "statistics") (r "*") (d #t) (k 0)))) (h "0a5n9a1nh64jph4zxpnj5szj4rg8qgch225pgl5p69kkljw6808v")))

(define-public crate-fractal-0.5.2 (c (n "fractal") (v "0.5.2") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "dwt") (r "^0.3") (d #t) (k 0)) (d (n "probability") (r "^0.14") (d #t) (k 0)) (d (n "statistics") (r "^0.3") (d #t) (k 0)))) (h "0cn4x631mabbybz8vm7gj1i83b8q37mxd9l10n9vf81hs0jwh7c4")))

(define-public crate-fractal-0.5.3 (c (n "fractal") (v "0.5.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "dwt") (r "^0.3") (d #t) (k 0)) (d (n "probability") (r "^0.14") (d #t) (k 0)) (d (n "statistics") (r "^0.3") (d #t) (k 0)))) (h "000q8sni0j2d0wy8fm2908k3sx6havmjc4ss3hcj7vkiabm2mlhd")))

(define-public crate-fractal-0.6.0 (c (n "fractal") (v "0.6.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "dwt") (r "^0.3") (d #t) (k 0)) (d (n "probability") (r "^0.14") (d #t) (k 0)) (d (n "statistics") (r "^0.3") (d #t) (k 0)))) (h "000d2dhz25q239wnkhb4829rm5vscxnagi495v1hwcz3a2d9l0j1")))

(define-public crate-fractal-0.6.1 (c (n "fractal") (v "0.6.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "dwt") (r "^0.3") (d #t) (k 0)) (d (n "probability") (r "^0.15") (d #t) (k 0)) (d (n "statistics") (r "^0.3") (d #t) (k 0)))) (h "02gphf98hs2pxj2zc39qwi2kzxz1alk8mnyi1g68g2b7r12zmvf4")))

(define-public crate-fractal-0.6.2 (c (n "fractal") (v "0.6.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "dwt") (r "^0.4") (d #t) (k 0)) (d (n "probability") (r "^0.15") (d #t) (k 0)) (d (n "statistics") (r "^0.4") (d #t) (k 0)))) (h "1g1sk66gxhlij5s4scrxifg9ixvw5kxz3l4sk6gg5pal321xw4la")))

(define-public crate-fractal-0.6.3 (c (n "fractal") (v "0.6.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "dwt") (r "^0.5") (d #t) (k 0)) (d (n "probability") (r "^0.15") (d #t) (k 0)) (d (n "statistics") (r "^0.4") (d #t) (k 0)))) (h "1944ajbcb9759p5sxx61fh7dqnajvhnpx2as8nkd6ip165vvl161")))

(define-public crate-fractal-0.7.0 (c (n "fractal") (v "0.7.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "dwt") (r "^0.5") (d #t) (k 0)) (d (n "probability") (r "^0.17") (d #t) (k 0)) (d (n "statistics") (r "^0.4") (d #t) (k 0)))) (h "1c9ldjf5wijnx1jalx3wrxj43bl2qi971a0cdcylrdpaxsj1wfp6")))

(define-public crate-fractal-0.7.1 (c (n "fractal") (v "0.7.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "dwt") (r "^0.5") (d #t) (k 0)) (d (n "probability") (r "^0.17") (d #t) (k 0)) (d (n "statistics") (r "^0.4") (d #t) (k 0)))) (h "1sqh4nsn9nh5488yrsz9sgmsx7ldd8p5qmjyjx2sj60vxbb1789x")))

