(define-module (crates-io fr ac fractal-gen) #:use-module (crates-io))

(define-public crate-fractal-gen-0.1.0 (c (n "fractal-gen") (v "0.1.0") (h "16yz4q01xjjqbpjc31sy1laqdv7vvrmhmncdsidgqxm2wasaj83i")))

(define-public crate-fractal-gen-0.1.1 (c (n "fractal-gen") (v "0.1.1") (h "17gmv1c8rr3ihisq5l5d7bwnyd1q9kmagd0jlil7j6vqghb7ixhf")))

(define-public crate-fractal-gen-0.1.2 (c (n "fractal-gen") (v "0.1.2") (h "0jxzspzz9pq6pdgvgq3g21f0f1ypvh3xmmxmam8spka5kmw79bkb")))

(define-public crate-fractal-gen-0.1.3 (c (n "fractal-gen") (v "0.1.3") (h "1jp6bsx28lfm2jjj2cnr5j34p1041zdpbdjjyc7zhqs4dkca7cjd")))

(define-public crate-fractal-gen-0.1.4 (c (n "fractal-gen") (v "0.1.4") (h "1pby8wm4sz03klrm7hj95y0gzb9wx3ssnpl300k5vngdcnj44cp5") (y #t)))

(define-public crate-fractal-gen-0.1.5 (c (n "fractal-gen") (v "0.1.5") (h "16117i3dhh3rkr6mq96y0r8zp3h6nadprpnkng876lfa1vnlidlq")))

