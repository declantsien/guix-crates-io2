(define-module (crates-io fr ac fracpack) #:use-module (crates-io))

(define-public crate-fracpack-0.0.0 (c (n "fracpack") (v "0.0.0") (h "0zdwdsaa9nqb4qjcpv2i6g5apgbzjskmdi5kpqj4ayfhs69v8n4k")))

(define-public crate-fracpack-0.1.0 (c (n "fracpack") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0d63bs87gp7lvqad9z5nzrmp0nkndyabyhbwaz3n2yh264ysv09n") (r "1.64")))

(define-public crate-fracpack-0.1.1 (c (n "fracpack") (v "0.1.1") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0zy86zd2yrwjxl3ail6lh8pqyvfay3ka38ps9fk0s4cvjnx2ylmf") (r "1.64")))

(define-public crate-fracpack-0.1.2 (c (n "fracpack") (v "0.1.2") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1fchis1d9y168zjs91d22qp1w4mqvxq78dasp82lw5lq8p5g7iqy") (r "1.64")))

(define-public crate-fracpack-0.1.3 (c (n "fracpack") (v "0.1.3") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.3") (d #t) (k 0)))) (h "0y71db21r98892w8mh2x29laghsrzqc56fg6q1nxd3kky948d4gj") (r "1.64")))

(define-public crate-fracpack-0.1.4 (c (n "fracpack") (v "0.1.4") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.4") (d #t) (k 0)))) (h "11jl7z0z0cqf612pys5rx5pbqxwfpl2wj0y5ncpywp59issyhirm") (r "1.64")))

(define-public crate-fracpack-0.1.5 (c (n "fracpack") (v "0.1.5") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.4") (d #t) (k 0)))) (h "1r4i320ap5m3d9fmb9ykj0vdgbr0w1zlp54jvnfqbhb3ir4xi8hb") (r "1.64")))

(define-public crate-fracpack-0.1.6 (c (n "fracpack") (v "0.1.6") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.5") (d #t) (k 0)))) (h "1aqbbg5lny499zfs7v2w3wfwc5m20x7p8fksifvx7rh71sy3qv00") (r "1.64")))

(define-public crate-fracpack-0.1.7 (c (n "fracpack") (v "0.1.7") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.1.6") (d #t) (k 0)))) (h "028dq0vrqcmgcw2q5yardvynmifyizv6bch2pp8nx0rpjq17vvxk") (r "1.64")))

(define-public crate-fracpack-0.2.0 (c (n "fracpack") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.2.0") (d #t) (k 0)))) (h "02yn17kjm6asxim9ayxy6kmv73k7aghb1jakrcmpw4h4mcb4bhb2") (r "1.64")))

(define-public crate-fracpack-0.3.0 (c (n "fracpack") (v "0.3.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0mq1px8k3mzfydcih2yjp4gxipd75qyarcwbqxjnk1rk8cf1qqqd") (r "1.64")))

(define-public crate-fracpack-0.6.0 (c (n "fracpack") (v "0.6.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.6.0") (d #t) (k 0)))) (h "0xhnz23m96axafcbf5vzk030bkiapyvd0nx6vngrpi0ga7gla06z") (r "1.64")))

(define-public crate-fracpack-0.8.0 (c (n "fracpack") (v "0.8.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.8.0") (d #t) (k 0)))) (h "111wrlc0sdw1s2han4m4j1z4lf9wz7cb997xaxj6n3jvwdz7vl1l") (r "1.64")))

(define-public crate-fracpack-0.9.0 (c (n "fracpack") (v "0.9.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "psibase_macros") (r "^0.9.0") (d #t) (k 0)))) (h "04y3kqv68zkmqd1yx163am7f4cwhdk1q9h4ysdbd73c8yp9jg4jj") (r "1.64")))

