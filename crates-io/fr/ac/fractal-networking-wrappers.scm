(define-module (crates-io fr ac fractal-networking-wrappers) #:use-module (crates-io))

(define-public crate-fractal-networking-wrappers-0.1.0 (c (n "fractal-networking-wrappers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "ipnet") (r "^2.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("process" "io-util" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "wireguard-keys") (r "^0.1.0") (d #t) (k 0)))) (h "0z06s9arkgk15wjkn7idjjc3ypkw03iajxbr10sz5k7vsvd6b73f")))

