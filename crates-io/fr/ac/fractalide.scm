(define-module (crates-io fr ac fractalide) #:use-module (crates-io))

(define-public crate-fractalide-0.1.0 (c (n "fractalide") (v "0.1.0") (h "10p2jfp4y7nq5xkdhfg6kaj8zs27pjqz6cysy6vk2hdvggg7pb71") (y #t)))

(define-public crate-fractalide-0.1.1 (c (n "fractalide") (v "0.1.1") (h "025nqsipnrbd53yahp71dsp9x3vqrb66v3jsr04w7h7qhbw425b5")))

(define-public crate-fractalide-0.1.2 (c (n "fractalide") (v "0.1.2") (h "10nbdg0k0r4rhzha71qwkc3p3jqj0yqys6h5mz2vqvbjmw84nm6g")))

(define-public crate-fractalide-0.2.0 (c (n "fractalide") (v "0.2.0") (h "1aw8lgk4w3y11ip62401r35llwsl3yp8x6s8lzbcivmza0x53d5g")))

(define-public crate-fractalide-0.2.1 (c (n "fractalide") (v "0.2.1") (h "18j3kcwjq76aal7azkd7sw7bjcxc3zsz3r8hkxhddv20x03dkh0f")))

