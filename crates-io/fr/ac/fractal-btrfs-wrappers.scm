(define-module (crates-io fr ac fractal-btrfs-wrappers) #:use-module (crates-io))

(define-public crate-fractal-btrfs-wrappers-0.1.0 (c (n "fractal-btrfs-wrappers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("process"))) (d #t) (k 0)))) (h "166b3a3qh10b7f95vbab7nm0g12rwfzxrrw5af4xjnw4qrdcicnd")))

(define-public crate-fractal-btrfs-wrappers-0.1.1 (c (n "fractal-btrfs-wrappers") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("process"))) (d #t) (k 0)))) (h "1kv54wkxxbd1jxrf8qqw6jysr5h1wfmgphcbqaxywaibbqjapcsg")))

