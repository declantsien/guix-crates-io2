(define-module (crates-io fr ac fractran) #:use-module (crates-io))

(define-public crate-fractran-0.1.0 (c (n "fractran") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mny6xlja8h53fkk19c9gqzwxmm076d56fli0qv47shh6q9nfhav")))

