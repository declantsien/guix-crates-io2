(define-module (crates-io fr ac fraco_point) #:use-module (crates-io))

(define-public crate-fraco_point-0.1.0 (c (n "fraco_point") (v "0.1.0") (d (list (d (n "fraco_point_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "16v8v0ajwccfvz645wgjcw384mxmh575lrqmjhjl1rxhxg9bz8pf") (f (quote (("derive" "fraco_point_derive") ("default")))) (y #t)))

(define-public crate-fraco_point-0.1.1 (c (n "fraco_point") (v "0.1.1") (d (list (d (n "fraco_point_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jcl2i8kdf2a5kbyganawsmigj3ii0vlcwrcra929vkqa7ikjyw0") (f (quote (("derive" "fraco_point_derive") ("default"))))))

