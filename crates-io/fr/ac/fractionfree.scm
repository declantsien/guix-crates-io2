(define-module (crates-io fr ac fractionfree) #:use-module (crates-io))

(define-public crate-fractionfree-0.1.0 (c (n "fractionfree") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "0m5a25fr627a94pps6fpic36xijb3i2ygyvsdhkr5n8z8lyifa2b")))

(define-public crate-fractionfree-0.1.1 (c (n "fractionfree") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "0rxw35k6jgxijzdijhlv4gznc3z1vhz0byx3qwrvvg02f30l04xc")))

(define-public crate-fractionfree-0.1.2 (c (n "fractionfree") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "15kzl2nh8b3xk3p95h3jgfacxhyq8f94v69gcykk6wxckw5v0pi1")))

(define-public crate-fractionfree-0.1.3 (c (n "fractionfree") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.46") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1p17q21j9c4jhybal9cgjxjkqw5kf18ldwhskaxljii5s1wg0cj3")))

