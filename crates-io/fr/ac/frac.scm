(define-module (crates-io fr ac frac) #:use-module (crates-io))

(define-public crate-frac-0.1.0 (c (n "frac") (v "0.1.0") (h "1ix2q58bnxvnqknzgp0n82bs9q0ddb3lhwnzl31bigjh5k9xqaij")))

(define-public crate-frac-1.0.0 (c (n "frac") (v "1.0.0") (h "0pc031j4qs0asjci80742dd0g5cbx3az6nk5nzrnp68fnccc8fgv")))

(define-public crate-frac-1.0.1 (c (n "frac") (v "1.0.1") (h "0gnq92ixcy0grid8vnk2wbp2mmaz9lfg32jgpq82rwkr5id1a17h")))

(define-public crate-frac-1.0.2 (c (n "frac") (v "1.0.2") (h "0qw1p6s8vkrfra614zjhprkr4z0idil988b3wlaw12xaqshr8z35")))

(define-public crate-frac-1.0.3 (c (n "frac") (v "1.0.3") (h "1s16zl0vghqh41dfp8zwqqwpgxw7vvd2l2sbgb44ldgfv1vxb3iw")))

(define-public crate-frac-1.0.4 (c (n "frac") (v "1.0.4") (h "03mvxxg4yjdpns03c215mc7fv6973hbdfm0zmyixvvqmxvpz6gar")))

(define-public crate-frac-1.0.5 (c (n "frac") (v "1.0.5") (h "0q135s9s5p6w7hip2d3pi4l1f0s6h7c728czvmicpcpkgn7nvnah")))

(define-public crate-frac-1.0.6 (c (n "frac") (v "1.0.6") (h "1v7kdj1ps58dy4yz4f4h6i5b3qgznyjq7jl46dyhm6bx833xslwa")))

(define-public crate-frac-1.0.7 (c (n "frac") (v "1.0.7") (h "1sspyr6b3896j127w9k8g8fa2qkn3zc0zndwvwnsv6zvcb65nf8p")))

(define-public crate-frac-1.1.0 (c (n "frac") (v "1.1.0") (h "1ikf4nhgbk0g9c2l7f2nn8qkbs7ck4p6lx4yr8rhxjc528s02qyc")))

(define-public crate-frac-1.1.1 (c (n "frac") (v "1.1.1") (h "1km8xqsmwg8sdvzy2s3fiz91ad8r2bj0q0s1rcigxika9fjiz42m")))

(define-public crate-frac-1.1.2 (c (n "frac") (v "1.1.2") (h "0ry0vjnmjyi4rs3c89q3jc5p7aiw751lzp67ykh2k132zclzam71")))

(define-public crate-frac-1.2.0 (c (n "frac") (v "1.2.0") (h "0pz3gb7cqpgmgpisns1236mcr2d820q5q1hqdrfs7w67lwrayk9h")))

(define-public crate-frac-1.2.1 (c (n "frac") (v "1.2.1") (h "1pb3031zc637gci9c0c10fbp3rwsws99wifj8l7w2l1r6b56m1sp")))

(define-public crate-frac-1.2.2 (c (n "frac") (v "1.2.2") (h "126fj08nw09v2y630aljwq0yh5kd1ls6hibl7y9zak2yv3xs4gn3")))

(define-public crate-frac-1.2.3 (c (n "frac") (v "1.2.3") (h "0m5qy99wm0sf6jwjvlzzgy22g1yablmbavppyvgjd0mxkgp7b0sy")))

(define-public crate-frac-1.2.4 (c (n "frac") (v "1.2.4") (h "0l57srr5d66mk9by9jrgzcww1ki9hij019pfmaql8sz2h4w4gizh")))

(define-public crate-frac-1.2.5 (c (n "frac") (v "1.2.5") (h "1fcxm745vrz1dv21an9fmvh7pzz8z0abknyqjzgyyvd0g2yq3z7i")))

(define-public crate-frac-1.3.0 (c (n "frac") (v "1.3.0") (h "0wxx0g9jdvfn3vf5qfhy5cri0kkd11yd9f9ll239radxywfri23h")))

(define-public crate-frac-2.0.0 (c (n "frac") (v "2.0.0") (h "136pv31gl45571pr112s973s7xscs9wq5spvawbrd6hbjmjhg9vc")))

(define-public crate-frac-2.1.0 (c (n "frac") (v "2.1.0") (h "0yhq825v2piz5984qf2rlaf09akq1jk00dws0cl3r8bcf3xvv978")))

