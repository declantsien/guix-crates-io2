(define-module (crates-io fr ac fractus) #:use-module (crates-io))

(define-public crate-fractus-0.1.0 (c (n "fractus") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.20.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0hj2bygsvi2d9kw7iaywx84kk3xdkwznymfzjr63p5g8nckpsg3a") (f (quote (("python" "pyo3/extension-module"))))))

(define-public crate-fractus-0.1.6 (c (n "fractus") (v "0.1.6") (d (list (d (n "pyo3") (r "^0.20.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0d9m7cmygkk1ahbhml8gajzkcr19bwamb958d2dpdc0jri9yq4wk") (f (quote (("python" "pyo3/extension-module"))))))

(define-public crate-fractus-0.1.7 (c (n "fractus") (v "0.1.7") (d (list (d (n "pyo3") (r "^0.20.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0bd94ifwlshq2j69n9647qzn0r8lga1adk04bvkg3gdbj55gawrv")))

(define-public crate-fractus-0.1.8 (c (n "fractus") (v "0.1.8") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "18ppdn7kcgpm9skq6a100wgzgipfgsc2ndp16v8vfb0mpfd739r4")))

