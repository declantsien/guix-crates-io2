(define-module (crates-io fr ac fractional_index) #:use-module (crates-io))

(define-public crate-fractional_index-0.1.0 (c (n "fractional_index") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0nq8rn4z87szy8qqxk9s3b2m2lha59mh6s5n03xkw0f25rpmx3z5") (f (quote (("default" "serde"))))))

(define-public crate-fractional_index-0.1.1 (c (n "fractional_index") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02qvv9zx7klyfjsypld2wyzjjzhj92pvm6m12riwm28c9qkvzksy") (f (quote (("default" "serde"))))))

(define-public crate-fractional_index-1.0.0 (c (n "fractional_index") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sls7a1kwz3pix459sn9rkvz3nyhvd0p7626y49hrfl3s3rjarbx") (f (quote (("default" "serde"))))))

(define-public crate-fractional_index-1.0.1 (c (n "fractional_index") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "094w376zwvybaq0jbh3ib476hkvsixndhinq0pyml65kjl9447s3") (f (quote (("default" "serde"))))))

(define-public crate-fractional_index-2.0.0 (c (n "fractional_index") (v "2.0.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "0lkqn8cwyq2pa9n1zawz7fn7sv6f5kzvnz8klpiwc6r3z474np1q") (f (quote (("default" "serde"))))))

(define-public crate-fractional_index-2.0.1 (c (n "fractional_index") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1i6bkmwbldjn5zxn6k7g4js2jb2kaypmppcrz3fibwf07sgil5d1") (f (quote (("default" "serde"))))))

