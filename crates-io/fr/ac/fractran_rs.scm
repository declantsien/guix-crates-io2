(define-module (crates-io fr ac fractran_rs) #:use-module (crates-io))

(define-public crate-fractran_rs-0.6.0 (c (n "fractran_rs") (v "0.6.0") (h "055fdq91pkn09zj116ci9pca359fr8smxh7iz6q7k954c6vzwsfs")))

(define-public crate-fractran_rs-0.6.1 (c (n "fractran_rs") (v "0.6.1") (h "1vdv3061n9i941iqc9xhaqswic1969rdwyyrzlrsyr8svz1bs950")))

