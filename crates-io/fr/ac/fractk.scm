(define-module (crates-io fr ac fractk) #:use-module (crates-io))

(define-public crate-fractk-0.0.1 (c (n "fractk") (v "0.0.1") (d (list (d (n "fractk_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "gtk4") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1v6325b97xr4ncbds6fjvi309kphhbcwxjfbll75i54rpr9sd4lj") (f (quote (("gtk4-backend" "gtk4") ("default" "gtk4-backend"))))))

(define-public crate-fractk-0.0.2 (c (n "fractk") (v "0.0.2") (d (list (d (n "embed-manifest") (r "^1.3.1") (d #t) (k 1)) (d (n "fractk_macro") (r "^0.0.2") (d #t) (k 0)) (d (n "gtk4") (r "^0.4.8") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bqm2ky0fipisd9aww4m9afda8n2argb1a41f9hi3qkq4qfgikxf")))

(define-public crate-fractk-0.0.3 (c (n "fractk") (v "0.0.3") (d (list (d (n "embed-manifest") (r "^1.3.1") (d #t) (k 1)) (d (n "fractk_macro") (r "^0.0.3") (d #t) (k 0)) (d (n "gtk4") (r "^0.4.8") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b5lb9w2dz0iznffjrjmsdbppzih2dp0wipss3nj7df55222ar0v")))

