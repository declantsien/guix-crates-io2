(define-module (crates-io fr ac fractran_macros) #:use-module (crates-io))

(define-public crate-fractran_macros-0.1.0 (c (n "fractran_macros") (v "0.1.0") (d (list (d (n "num") (r ">= 0.0.0") (d #t) (k 0)) (d (n "slow_primes") (r "~0.1.2") (d #t) (k 0)))) (h "02k20rdbisks5hmkjwba48pf33db6n3nlapsbxp12pmrvfwwq9nj")))

(define-public crate-fractran_macros-0.1.1 (c (n "fractran_macros") (v "0.1.1") (d (list (d (n "num") (r ">= 0.0.0") (d #t) (k 0)) (d (n "slow_primes") (r "~0.1.2") (d #t) (k 0)))) (h "05qzqfd7ahwy4dskljx1ppyiq56316q176ysdm365q9x9rn2m93v")))

(define-public crate-fractran_macros-0.1.3 (c (n "fractran_macros") (v "0.1.3") (d (list (d (n "num") (r "~0") (d #t) (k 0)) (d (n "slow_primes") (r "~0.1.4") (d #t) (k 0)))) (h "0mwifx44gjwkavxnb652gnr3nsfad35cqqzr3ag8m0bb89w8rrjy")))

(define-public crate-fractran_macros-0.1.4 (c (n "fractran_macros") (v "0.1.4") (d (list (d (n "num") (r "~0") (d #t) (k 0)) (d (n "slow_primes") (r "~0.1.4") (d #t) (k 0)))) (h "12cz10bndqmxcsclsl04sfkfjxnd0sqyx83f10nzk1jhq8gpqs8c")))

(define-public crate-fractran_macros-0.1.5 (c (n "fractran_macros") (v "0.1.5") (d (list (d (n "num") (r "~0") (d #t) (k 0)) (d (n "slow_primes") (r "~0.1.4") (d #t) (k 0)))) (h "1yxbr5zf1rc31qqrda7wrypkvn5ccc1bj50ln5kpij6vfrmghirr")))

