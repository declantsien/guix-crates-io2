(define-module (crates-io fr ac fractious) #:use-module (crates-io))

(define-public crate-fractious-0.1.0 (c (n "fractious") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "00jl1790mg9n9m1qkn5p1czbn40hv5w1xgzyf8cq48h51rpnbqdi")))

(define-public crate-fractious-0.1.1 (c (n "fractious") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "04acp7dax72mfc3gisnddlsnpmg3mz5svcalbghd65mdwz86xrs4")))

