(define-module (crates-io fr om from_bytes) #:use-module (crates-io))

(define-public crate-from_bytes-0.1.0 (c (n "from_bytes") (v "0.1.0") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)))) (h "1againwavrms5wkmi0glf7v807r3y0insr1si925qjr7bg58xc9a")))

(define-public crate-from_bytes-0.1.1 (c (n "from_bytes") (v "0.1.1") (d (list (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)))) (h "0jaky2k99afmiaa55vpvlkqyvf1b3q5j7ycrnzbz4pwbc1dy32s4")))

(define-public crate-from_bytes-0.2.0 (c (n "from_bytes") (v "0.2.0") (d (list (d (n "duplicate") (r "^0.2") (d #t) (k 0)) (d (n "from_bytes_derive") (r "=0.2.0") (d #t) (k 0)))) (h "10jja2qxd25i636qh6acpxlmhkjhpnchcgpakpcanmgg50rw5xha") (f (quote (("u8_arrays") ("u64_arrays") ("u32_arrays") ("u16_arrays") ("i8_arrays") ("i64_arrays") ("i32_arrays") ("i16_arrays"))))))

(define-public crate-from_bytes-0.2.1 (c (n "from_bytes") (v "0.2.1") (d (list (d (n "duplicate") (r "^0.2") (d #t) (k 0)) (d (n "from_bytes_derive") (r "=0.2.1") (d #t) (k 0)))) (h "1ggf312wl0gvh9fri2xg3109pmgxyryg7xnr959f03b885l1dyf9") (f (quote (("u8_arrays") ("u64_arrays") ("u32_arrays") ("u16_arrays") ("i8_arrays") ("i64_arrays") ("i32_arrays") ("i16_arrays"))))))

(define-public crate-from_bytes-0.2.2 (c (n "from_bytes") (v "0.2.2") (d (list (d (n "duplicate") (r "^0.2") (d #t) (k 0)) (d (n "from_bytes_derive") (r "^0.2.2") (d #t) (k 0)))) (h "1sn8gsc26kmc8rf0nsf29m2p373x0vffch48zzakjv36hdm92jag") (f (quote (("u8_arrays") ("u64_arrays") ("u32_arrays") ("u16_arrays") ("i8_arrays") ("i64_arrays") ("i32_arrays") ("i16_arrays"))))))

