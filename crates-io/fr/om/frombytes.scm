(define-module (crates-io fr om frombytes) #:use-module (crates-io))

(define-public crate-frombytes-0.1.0 (c (n "frombytes") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1phzdbiqdpq2ra75yll2dizrfmkqyf4sfi7hs8iw3hb75z6n8zmf")))

(define-public crate-frombytes-0.1.1 (c (n "frombytes") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "0wnhwb77ddrirwxqyx3vqd53grr1xq74ayb8kjpc3adw4nrxgfd1") (f (quote (("std" "thiserror") ("default" "std"))))))

