(define-module (crates-io fr om from_variants_impl) #:use-module (crates-io))

(define-public crate-from_variants_impl-0.2.0 (c (n "from_variants_impl") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1ik17r25r5j127m3311l27ngc4wwv0l6p9iqc77hd6iwjxl22jac")))

(define-public crate-from_variants_impl-0.2.1 (c (n "from_variants_impl") (v "0.2.1") (d (list (d (n "darling") (r "^0.1.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0dy1q0jnix6lsv976z1fsf43mcfwxaj2j0jdqzfl4q22qp4j3gyk")))

(define-public crate-from_variants_impl-0.2.2 (c (n "from_variants_impl") (v "0.2.2") (d (list (d (n "darling") (r "= 0.1.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1ayzr3gz4s633almnpx4frdn4npwnvcn7ml4nfl6i23fln5jwa04")))

(define-public crate-from_variants_impl-0.2.3 (c (n "from_variants_impl") (v "0.2.3") (d (list (d (n "darling") (r "^0.2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "01nyicq20hpxpnzxdr0w9cyf14apgkpr11kx5zkdaq6vp18pf1xi")))

(define-public crate-from_variants_impl-0.2.4 (c (n "from_variants_impl") (v "0.2.4") (d (list (d (n "darling") (r "^0.3.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^0.12.10") (d #t) (k 0)))) (h "0pvh636mq2h0kkk9f0gnrkm1y8nyih3bll50yakdhblk7bdwzpb1")))

(define-public crate-from_variants_impl-0.3.0 (c (n "from_variants_impl") (v "0.3.0") (d (list (d (n "darling") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1kgv0h0qjkgk6pkd6wam25hs4sl2hmn3lljrfx2nz02wkydlwkzw")))

(define-public crate-from_variants_impl-0.4.0 (c (n "from_variants_impl") (v "0.4.0") (d (list (d (n "darling") (r "^0.5.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.10") (d #t) (k 0)))) (h "1h5rwnqyrank18xgdp05ralm1ihb4wd3h977zqv7axg98100hkfm")))

(define-public crate-from_variants_impl-0.5.0 (c (n "from_variants_impl") (v "0.5.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hz42d8yin4xnh42nmswjg3szy8wqm4rwlqdvs74f86ayb8h7shd")))

(define-public crate-from_variants_impl-0.6.0 (c (n "from_variants_impl") (v "0.6.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06i4bjjxbq6c4hlx2ly04s64d1972zkskshc2v4xx7n8lfghf23y")))

(define-public crate-from_variants_impl-1.0.0 (c (n "from_variants_impl") (v "1.0.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bg7wvjn3iymhrbiwr8pmbadirnzjbphid6lm18v1arysjazvaqk")))

(define-public crate-from_variants_impl-1.0.1 (c (n "from_variants_impl") (v "1.0.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0732zvmvbv9mi7zq9611pjppxwfkz63h3wzp4a5c4jjf9rjahg94")))

(define-public crate-from_variants_impl-1.0.2 (c (n "from_variants_impl") (v "1.0.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)))) (h "17p6djij0ar0c9dlfnq4dj9bgmq16fcsf3winjr9cv8fm12fd9am")))

