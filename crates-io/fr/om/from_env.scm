(define-module (crates-io fr om from_env) #:use-module (crates-io))

(define-public crate-from_env-0.1.0 (c (n "from_env") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "137k7wrc769pkq8vj4fsxx66sqbm8hig3nnnk1m5rmf1i6s5hs0p")))

(define-public crate-from_env-0.1.1 (c (n "from_env") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0xcdp1p7mc8p3d8v2wvj0gvi9rk0vi4k6mczfrsr68l829w5rghv")))

