(define-module (crates-io fr om from-env-derive) #:use-module (crates-io))

(define-public crate-from-env-derive-0.1.0 (c (n "from-env-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1glkn8v0ppxga8fz40q2p99nyn01gvacwx3cwc58m2xr0676i0xy")))

(define-public crate-from-env-derive-0.2.0 (c (n "from-env-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "032aqqsx5yihgm641an2bl28nxr2izrs78blqb78hqip93kqb63d")))

(define-public crate-from-env-derive-0.2.1 (c (n "from-env-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0i008y82nhf5jvjqpcl2gqiwgsyzlfrwhfyam5hq9bq4vjb3qv5i")))

(define-public crate-from-env-derive-0.2.2 (c (n "from-env-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "19h343zf7ji4fxzyyai80cnsfgqhkh9wdm6ws5rb15jswv7glmzy")))

