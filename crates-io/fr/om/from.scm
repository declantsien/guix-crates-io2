(define-module (crates-io fr om from) #:use-module (crates-io))

(define-public crate-from-0.0.1 (c (n "from") (v "0.0.1") (h "0ji075qarwqc4d5fkkr2bbli97qnywbc9ya5l068jamm2hlnyp9v")))

(define-public crate-from-0.0.2 (c (n "from") (v "0.0.2") (h "02wxvs4aaqgw4yq2dqlpsw7ynn46fgr7ay7r3401g11d0mnp2nq6")))

