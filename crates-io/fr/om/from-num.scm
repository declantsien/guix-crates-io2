(define-module (crates-io fr om from-num) #:use-module (crates-io))

(define-public crate-from-num-0.1.0 (c (n "from-num") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1a0g02hj2afl7hdlzrjkiv5r7w6qqxc31hkksnc63fnjzvgazjq7")))

(define-public crate-from-num-1.1.0 (c (n "from-num") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1vmmw505c41spfd24v1hf3spq650ml67f17h5gip0as8q5djbs51")))

(define-public crate-from-num-1.1.1 (c (n "from-num") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0kv1jv1il2zg1083y1x850agisd40mhr70fz9c46ml78ad2pbn3g")))

