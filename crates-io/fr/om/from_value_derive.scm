(define-module (crates-io fr om from_value_derive) #:use-module (crates-io))

(define-public crate-from_value_derive-0.1.0 (c (n "from_value_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jbh5frwnwin4l5p5sl1c0ly6zbr8y18y52h5m9wvj6cs9q8w488")))

(define-public crate-from_value_derive-0.1.1 (c (n "from_value_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bx58q09v6kj38zw3vz8bwmlfb8m9whgwr52zrv2qw1c195hpy3l")))

