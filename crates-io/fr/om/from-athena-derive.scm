(define-module (crates-io fr om from-athena-derive) #:use-module (crates-io))

(define-public crate-from-athena-derive-0.1.0 (c (n "from-athena-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1i3dj62gpndcszq2578qgv1dhhicv1apb4ryr0iqrvdnxnd414nn") (y #t)))

(define-public crate-from-athena-derive-0.1.1 (c (n "from-athena-derive") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "aws-sdk-athena") (r "^1.19.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1zgsdszdrvm76bywiwswsxljvkdyllf653ibyjlknzaf2lllr7ks")))

