(define-module (crates-io fr om from_variants) #:use-module (crates-io))

(define-public crate-from_variants-0.1.0 (c (n "from_variants") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1q3k218bzq11474n6w5x6dkp8wbkl5qrxvlx8mp6kn49ayqnd6zm")))

(define-public crate-from_variants-0.1.1 (c (n "from_variants") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0sp8p20pba8qy0wmbcsqbsd4yvz26ljql45v3f2gjph4g2cwycxc")))

(define-public crate-from_variants-0.1.2 (c (n "from_variants") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (k 0)) (d (n "pretty_assertions") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1pwf8i14bk1yxb7rkhdh1k4fxaf3wnqkhv9r0pg1s8dnydsggsav")))

(define-public crate-from_variants-0.2.0 (c (n "from_variants") (v "0.2.0") (d (list (d (n "from_variants_impl") (r "^0.2") (d #t) (k 0)))) (h "08qqwylcr7c0v8s6h5lg7rjbr9z30zi923ri9l9w6ll595fzqms6")))

(define-public crate-from_variants-0.2.1 (c (n "from_variants") (v "0.2.1") (d (list (d (n "from_variants_impl") (r "= 0.2.1") (d #t) (k 0)))) (h "1yz41zs86dl6sx45blwm09dyxxv5fw11jggfz163ci12lybswca8")))

(define-public crate-from_variants-0.2.2 (c (n "from_variants") (v "0.2.2") (d (list (d (n "from_variants_impl") (r "= 0.2.2") (d #t) (k 0)))) (h "1i57klv3v1cqkbc70gyqsni3i5bfcpxh3pix1xy6hm012bgvy67f")))

(define-public crate-from_variants-0.2.3 (c (n "from_variants") (v "0.2.3") (d (list (d (n "from_variants_impl") (r "= 0.2.3") (d #t) (k 0)))) (h "1v39nz69wjxql8cd635grgcsirxsdcsx339p425bzcr8yi53464g")))

(define-public crate-from_variants-0.2.4 (c (n "from_variants") (v "0.2.4") (d (list (d (n "from_variants_impl") (r "= 0.2.4") (d #t) (k 0)))) (h "14l84bf7yg7rw0pfn30s8aw040vw4qjckr9lrm2mkvwgzfp711q5")))

(define-public crate-from_variants-0.3.0 (c (n "from_variants") (v "0.3.0") (d (list (d (n "from_variants_impl") (r "= 0.3.0") (d #t) (k 0)))) (h "0gqb76s752lcmp4i7crlmv42s28hir887kjsajnyrj3mc42jfyjm")))

(define-public crate-from_variants-0.4.0 (c (n "from_variants") (v "0.4.0") (d (list (d (n "from_variants_impl") (r "= 0.4.0") (d #t) (k 0)))) (h "0clyx2vdhfykmayffhr1qmr8pa3nvdmb9jr46bil03dxsyakfyrb")))

(define-public crate-from_variants-0.5.0 (c (n "from_variants") (v "0.5.0") (d (list (d (n "from_variants_impl") (r "=0.5.0") (d #t) (k 0)))) (h "0rd6aixrih0w2c05mp4li1xz09l44l04gkm0wmjirybxl5v48r98")))

(define-public crate-from_variants-0.6.0 (c (n "from_variants") (v "0.6.0") (d (list (d (n "from_variants_impl") (r "=0.6.0") (d #t) (k 0)))) (h "1qx4jmwljwmcdfc998ndf7iz8wyg7lmlc3vl3fy812f9lfqiw6i2")))

(define-public crate-from_variants-1.0.0 (c (n "from_variants") (v "1.0.0") (d (list (d (n "from_variants_impl") (r "=1.0.0") (d #t) (k 0)))) (h "0a8n5ly3xy7myr7kaj7a1kfwpx8fcxmy355ij4g04g3gra063wvw")))

(define-public crate-from_variants-1.0.1 (c (n "from_variants") (v "1.0.1") (d (list (d (n "from_variants_impl") (r "=1.0.1") (d #t) (k 0)))) (h "14s4yjapm4mqznzj4w276a64j5d40p17zpnj2gr118ij9lqhqvmx")))

(define-public crate-from_variants-1.0.2 (c (n "from_variants") (v "1.0.2") (d (list (d (n "from_variants_impl") (r "=1.0.2") (d #t) (k 0)))) (h "1wmv85523z261vwmx1iqjykf0dp8fvy9kgjxj0c7cs2p427rr1af")))

