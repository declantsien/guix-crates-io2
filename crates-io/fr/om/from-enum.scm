(define-module (crates-io fr om from-enum) #:use-module (crates-io))

(define-public crate-from-enum-0.1.0 (c (n "from-enum") (v "0.1.0") (d (list (d (n "from-enum-derive") (r "^0.1") (d #t) (k 0)))) (h "1s3yfxm0m7nmdla7826l35py2l2yyk19z1dcz281xyjsw5vwvc4l")))

(define-public crate-from-enum-0.1.1 (c (n "from-enum") (v "0.1.1") (d (list (d (n "from-enum-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0n8d9wcm1pnrgll7ibjj1h1zhxc04hdkiy4yp1ib7m3j4bzqdfb9")))

