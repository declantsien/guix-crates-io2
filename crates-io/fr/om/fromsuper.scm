(define-module (crates-io fr om fromsuper) #:use-module (crates-io))

(define-public crate-fromsuper-0.1.0 (c (n "fromsuper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fromsuper_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1gkin34zjrdxhlvs641v65ng5hf673wyc6sjwa20d5acq4axp8p7")))

(define-public crate-fromsuper-0.1.1 (c (n "fromsuper") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fromsuper_macros") (r "^0.1.0") (d #t) (k 0)))) (h "149v8c9z39lyx3nzjv4mz2j4b7by8lvx34ypzv7784n7gijf2cjq")))

(define-public crate-fromsuper-0.1.2 (c (n "fromsuper") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fromsuper_macros") (r "^0.1.2") (d #t) (k 0)))) (h "09s0v2p0i0j429d6a7842s975144kjd4rs9ka41ryz21n05hz16a")))

(define-public crate-fromsuper-0.1.3 (c (n "fromsuper") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fromsuper_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1l1qarb2bija40mf1sna2397l9mwzgnhnm6sv8vpnl3npacc83cp")))

(define-public crate-fromsuper-0.2.0 (c (n "fromsuper") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fromsuper_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0161zdcwcnqvgkhnxljvpdvny5vp8cb7igij45fjxq4j1wcjxrjb")))

(define-public crate-fromsuper-0.2.1 (c (n "fromsuper") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fromsuper_macros") (r "^0.2.0") (d #t) (k 0)))) (h "1wwd1r1cxg353flnwrcwwkisn2hm7vyxc88m85c7xg8cqr2d563v")))

