(define-module (crates-io fr om from_bytes_derive) #:use-module (crates-io))

(define-public crate-from_bytes_derive-0.1.0 (c (n "from_bytes_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (d #t) (k 0)))) (h "0cd5p2g8f12s8v6iq4pzh593vqq0gl6x0sb9vlz4xqhapa4s3pn9")))

(define-public crate-from_bytes_derive-0.2.0 (c (n "from_bytes_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d5ph6rckcsair994brdswmpa5yymrc42mf087pbkjynh8k8ii9n")))

(define-public crate-from_bytes_derive-0.2.1 (c (n "from_bytes_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wrwhqkq4bxknakvsiww8i0k9pqlsi1hlc55zf2b6x2v4n09m3vi")))

(define-public crate-from_bytes_derive-0.2.2 (c (n "from_bytes_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qlxc3dz2i4c7a2gfd42ligw92zqzspvhrmjhqpdx262yd6xb912")))

