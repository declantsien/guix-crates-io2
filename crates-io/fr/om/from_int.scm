(define-module (crates-io fr om from_int) #:use-module (crates-io))

(define-public crate-from_int-0.1.0 (c (n "from_int") (v "0.1.0") (d (list (d (n "from_int_derive") (r "^0.1.0") (d #t) (k 0)))) (h "01yqjjaim3dmx4ad5y5y1qwzjx3rbm5fkwf8qgldmqsmv8sgk1lp")))

(define-public crate-from_int-0.1.1 (c (n "from_int") (v "0.1.1") (d (list (d (n "from_int_derive") (r "^0.1.0") (d #t) (k 0)))) (h "138rga65i1rf1ad72z0jakjgf5v9k4pqn6a1yxdl28i2qzdvlkq9")))

(define-public crate-from_int-0.1.2 (c (n "from_int") (v "0.1.2") (d (list (d (n "from_int_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0s7qfki8506nlx2pfd00nwgfhhfj6pp6s4k6l3p5slyrzbxh213p")))

