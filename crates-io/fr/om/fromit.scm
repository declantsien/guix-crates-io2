(define-module (crates-io fr om fromit) #:use-module (crates-io))

(define-public crate-fromit-0.0.0 (c (n "fromit") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01ff9f1fjr5hs338i7pmmv03wrx2fn54dzhh4r4mf928ar0lwkp1")))

(define-public crate-fromit-0.0.1 (c (n "fromit") (v "0.0.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0d1pamim26q5yik131m56sgpsljgilv6y96ng5y5kfrbamvcfjxn")))

(define-public crate-fromit-0.0.2 (c (n "fromit") (v "0.0.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "08p166dg6x6jxg0imkvdb2q8qgflbcqj7mz51nkw8fnp22k735bj")))

(define-public crate-fromit-0.0.3 (c (n "fromit") (v "0.0.3") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "12yaxig63h45x20ihaxiayaxmwcii017286fsj1iqislskfkpdgc")))

(define-public crate-fromit-0.1.1 (c (n "fromit") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0ngampmsgg4nnv003lsavk7fqkqjxjr23px9f6rs85zg3lax78l9")))

(define-public crate-fromit-0.1.2 (c (n "fromit") (v "0.1.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "derivit-core") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0apjv9ppzw9yj11fyj4w7393b9krs6nbhznx56ggmbqp92rvxfwk")))

