(define-module (crates-io fr om from_variant) #:use-module (crates-io))

(define-public crate-from_variant-0.1.0 (c (n "from_variant") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "071i1jg5hh61hgkr9g8q9s8hw5kknqxzwmkikagyw4pnamqglhk4")))

(define-public crate-from_variant-0.1.1 (c (n "from_variant") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "08qpn0bha7fcbfa7ca04pc955mrwya9img0rn06zk70rifbnmxmm")))

(define-public crate-from_variant-0.1.2 (c (n "from_variant") (v "0.1.2") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "1fabcp6w9mn8zq97pc7pdd4xlc65x369dxldmna6pa3rcnnqb603")))

(define-public crate-from_variant-0.1.3 (c (n "from_variant") (v "0.1.3") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "0gf6gh6b9b01f7j0gwv3km1k6lily3banx47izlpniya4x866l89")))

(define-public crate-from_variant-0.1.4 (c (n "from_variant") (v "0.1.4") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "0v6zj4i0vqi0r3ys7dpx74611sjj357xa89rvx1zdf9a1m3ix67h")))

(define-public crate-from_variant-0.1.5 (c (n "from_variant") (v "0.1.5") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "01v37vx2kvv6b8mg30j2pyppfm8x5zgkffa40x2kh8jk0xv9ji0x")))

(define-public crate-from_variant-0.1.6 (c (n "from_variant") (v "0.1.6") (d (list (d (n "pmutil") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "0kz5nmqcf202zxkfwv67651zky8z310iqav99bc7i471iv1mvv03")))

(define-public crate-from_variant-0.1.7 (c (n "from_variant") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "0yydk58irds3lbq3yslanzms44w8wm13v55x9vw55kkkn7p122rs")))

(define-public crate-from_variant-0.1.8 (c (n "from_variant") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.11") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold" "parsing" "printing"))) (d #t) (k 0)))) (h "1nq71sc0fx3x93jgpiknf9a7x16na0jcx6rv6mki014vcdswrjgx")))

