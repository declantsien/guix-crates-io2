(define-module (crates-io fr om from-mapper-derive) #:use-module (crates-io))

(define-public crate-from-mapper-derive-0.1.0 (c (n "from-mapper-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0nx639gax03hpxhsjv9z2h93jv80d0zmckl2xd03x7zb0bxx9kgd") (f (quote (("unstable") ("nightly") ("doc") ("default") ("debug"))))))

