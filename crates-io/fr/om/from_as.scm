(define-module (crates-io fr om from_as) #:use-module (crates-io))

(define-public crate-from_as-0.1.0 (c (n "from_as") (v "0.1.0") (d (list (d (n "derive_from_as") (r "^0.1.0") (d #t) (k 0)) (d (n "from_as_file") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0diwwsc52fjldy3m4s32ps5i6d29jaywz9p6vlij3y3yhk3pan2w")))

(define-public crate-from_as-0.1.1 (c (n "from_as") (v "0.1.1") (d (list (d (n "derive_from_as") (r "^0.1.1") (d #t) (k 0)) (d (n "from_as_file") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0g8rfy6qcjn9vnhg6pl5j4c25xlrbjhf1qbisf7p0kwlc0yv271z")))

(define-public crate-from_as-0.2.0 (c (n "from_as") (v "0.2.0") (d (list (d (n "derive_from_as") (r "^0.2.0") (d #t) (k 0)) (d (n "from_as_file") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.20") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "15nq7bj3ga3iqfc6349136dbff9hsk4mxn4kpnh07a8h437n4z2w")))

