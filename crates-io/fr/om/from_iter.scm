(define-module (crates-io fr om from_iter) #:use-module (crates-io))

(define-public crate-from_iter-1.0.0 (c (n "from_iter") (v "1.0.0") (h "11zgqj49svx78l1isf2yxwpldr2169ir5nlwvs0j3m43xp8qciyc")))

(define-public crate-from_iter-1.1.0 (c (n "from_iter") (v "1.1.0") (h "1hshs0jgvasz90jsamyxxjxw2wam1qma0splwkz0qz5lsnhpvra2")))

