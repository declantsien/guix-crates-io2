(define-module (crates-io fr om from-enum-derive) #:use-module (crates-io))

(define-public crate-from-enum-derive-0.1.0 (c (n "from-enum-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sn67ni1kzr4mqlyw7qjk17aji4y9963iz91aklw2p0dnkb80slh")))

(define-public crate-from-enum-derive-0.1.1 (c (n "from-enum-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h9la1fpr1dnkh99pad726ada98cfvj7ksrk3zhvq3xdg7dixpax")))

