(define-module (crates-io fr om from_file_derive) #:use-module (crates-io))

(define-public crate-from_file_derive-0.1.0 (c (n "from_file_derive") (v "0.1.0") (d (list (d (n "from_file") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "0hk1z05mx68a5i4zhcwfnpj3d4kj38x5iw67bck9ch6n3m8ikh8z")))

(define-public crate-from_file_derive-0.1.1 (c (n "from_file_derive") (v "0.1.1") (d (list (d (n "from_file") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "10vlccfjqshiqb234d3igv91fa62zv855y8c50pyj0745v4sy70m")))

(define-public crate-from_file_derive-0.1.2 (c (n "from_file_derive") (v "0.1.2") (d (list (d (n "from_file") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "1pisi4rq6nv4j3ip038a21mbxxhva0ab1g9jbjy0nwmvzv4p32mz")))

(define-public crate-from_file_derive-0.1.3 (c (n "from_file_derive") (v "0.1.3") (d (list (d (n "from_file") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)))) (h "07bgmxxif9igpj2l5j9pa4z5hhl47nkbg1kga808vcrndxlmcpz1")))

