(define-module (crates-io fr om from-pest) #:use-module (crates-io))

(define-public crate-from-pest-0.3.0 (c (n "from-pest") (v "0.3.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest-ast") (r "^0.3") (d #t) (k 2)) (d (n "pest_derive") (r "^2.0") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1d3m4iljgydfasa62yhlvxpambbg0lj4w9pxdfjhlbz5s9ggqk5i")))

(define-public crate-from-pest-0.3.1 (c (n "from-pest") (v "0.3.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest-ast") (r "^0.3") (d #t) (k 2)) (d (n "pest_derive") (r "^2.0") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "03y67d8j8yij2x5621h1iv76nq7jhksh1vd258xgp8fbxnf3iadb")))

(define-public crate-from-pest-0.3.2 (c (n "from-pest") (v "0.3.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest-ast") (r "^0.3") (d #t) (k 2)) (d (n "pest_derive") (r "^2.5") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "1nv4c15w8idmv5hr1ghnacfyfcik752600q4b6rkp7j59y5hsf6k")))

