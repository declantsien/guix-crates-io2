(define-module (crates-io fr om from_bytes_or_zeroed) #:use-module (crates-io))

(define-public crate-from_bytes_or_zeroed-0.1.0 (c (n "from_bytes_or_zeroed") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0ahl5rwqf1gi07lplyif65d0qabaqc7pnp2p7qi8jn23g159699x")))

