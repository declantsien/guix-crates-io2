(define-module (crates-io fr om from-to-repr) #:use-module (crates-io))

(define-public crate-from-to-repr-0.1.1 (c (n "from-to-repr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1kj3l6lv8l084lxclrng36kasg6wz3spa3jy06n456n6w2n91vb8")))

(define-public crate-from-to-repr-0.1.2 (c (n "from-to-repr") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0zag7xf1m0n5gdf59hr12362dw9m55vrgc0k65gf5gfqpv9wgmbx")))

(define-public crate-from-to-repr-0.1.3 (c (n "from-to-repr") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "01h1b3x88x5i7wkspdz44xz6bbi3vwf0sb8yz86i6s3aw4dfz3m1") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.1.4 (c (n "from-to-repr") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1zgip0aq9y0sdc1jcgfh2nixj52hg9vl7cx1ll75k6jny8820cqw") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.1.5 (c (n "from-to-repr") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "12mq4mqnw841dpx90sjxyz0w37y0wc0vp4wgnnw7siv3h5vm7hd0") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.1.6 (c (n "from-to-repr") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "06w0ndqg5l30j1bqzmj2mfd8xm3ckl7drm2lrbkglr3xwkf2n97s") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.1.7 (c (n "from-to-repr") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0lqzcfpfap79jlq4c2xv51nlshj3wxid71x9iq926bvzczsbgm51") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.1.8 (c (n "from-to-repr") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1alcahplzvym9apkc2d32p9sn3587lf3jngcvbhgkyijhwm51r2v") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.1.9 (c (n "from-to-repr") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1kgipd0smwzd2kr4xj7a1nir16pc9a6lm68x5z6ffcrw3bjvj7w0") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.1.10 (c (n "from-to-repr") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0ysa6l4w5iq505j8l97kvm99cjqr24bin5dgnqzx977236hzprw8") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.2.0 (c (n "from-to-repr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1pyx1xvp9iqkxf3ki5jyzg4na305sj754x4gsyjmc0xp9m784ana") (f (quote (("from_to_other" "syn/full"))))))

(define-public crate-from-to-repr-0.2.1 (c (n "from-to-repr") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1gaxq4hf05ihdci5lw873kmi68vx7q2kpj0yw8qb9mi7q8rbckf5") (f (quote (("from_to_other" "syn/full"))))))

