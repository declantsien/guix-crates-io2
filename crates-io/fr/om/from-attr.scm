(define-module (crates-io fr om from-attr) #:use-module (crates-io))

(define-public crate-from-attr-0.1.0 (c (n "from-attr") (v "0.1.0") (d (list (d (n "from-attr-core") (r "^0.1.0") (k 0)) (d (n "from-attr-macro") (r "^0.1.0") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 2)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "full" "extra-traits"))) (k 2)))) (h "0hm5avwf04qvmrar1k5yy8ks8zx0pv7pwj3fbv8knh6k19q482j3") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-from-attr-0.1.1 (c (n "from-attr") (v "0.1.1") (d (list (d (n "from-attr-core") (r "^0.1.1") (k 0)) (d (n "from-attr-macro") (r "^0.1.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 2)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "full" "extra-traits"))) (k 2)))) (h "0xz1cisjy2bps122k260v5cgp3lwhpjs64jwyfafm6pr1pr7qiry") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-from-attr-0.1.2 (c (n "from-attr") (v "0.1.2") (d (list (d (n "from-attr-core") (r "^0.1.2") (k 0)) (d (n "from-attr-macro") (r "^0.1.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 2)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "full" "extra-traits"))) (k 2)))) (h "1dj2d7hwdq3619jq3wvra6x00bhnz5qkbvyxr5y6sjqm8jrm4cs9") (f (quote (("syn-full" "syn/full"))))))

