(define-module (crates-io fr om fromage_converter) #:use-module (crates-io))

(define-public crate-fromage_converter-0.1.0 (c (n "fromage_converter") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "130n9kb066swabj9b7p0lpk4wxzwia40x44vvygxwb247xrhdx23")))

