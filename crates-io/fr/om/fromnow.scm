(define-module (crates-io fr om fromnow) #:use-module (crates-io))

(define-public crate-fromnow-0.1.0 (c (n "fromnow") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0741jhcpf8wv6m378irwz0zk21rjc0raafrzvc1vdl3ns0icq1b8")))

