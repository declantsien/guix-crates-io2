(define-module (crates-io fr om from_tuple) #:use-module (crates-io))

(define-public crate-from_tuple-0.1.0 (c (n "from_tuple") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vdinm1rpcj735k75n2f8kzdr4psrzl2717bsd6n2ps2bsy0cnak")))

(define-public crate-from_tuple-0.1.1 (c (n "from_tuple") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04622jzi2zj0imzia7bx5908jg422py66yhgrx8fgkpd79s8r6kj")))

(define-public crate-from_tuple-0.1.2 (c (n "from_tuple") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1csyghnjs4q8f73gbhgc0km4fdlkjim1b52q6gm0pfg01llq3sv4")))

