(define-module (crates-io fr om from_file) #:use-module (crates-io))

(define-public crate-from_file-0.1.0 (c (n "from_file") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0wawrh9f2bb400zdyy28x3hxn9w4zg7z3409wyy5q22hzfsvb2l2")))

(define-public crate-from_file-0.1.1 (c (n "from_file") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1dys65n9a3x9lk5ydj3c6b0sn9w30zb8vvwgmd9zm0s19x8la1iy")))

(define-public crate-from_file-0.1.2 (c (n "from_file") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "06xmcxvi620gsi53x4y41and0995080lf0ybarqap3arxl1xxjqk")))

(define-public crate-from_file-0.1.3 (c (n "from_file") (v "0.1.3") (d (list (d (n "from_file_derive") (r "^0.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ysb3n9l7c9mb886r9dblvslm6p15m4s1r0fp286cjxcjic6h311")))

