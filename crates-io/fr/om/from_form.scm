(define-module (crates-io fr om from_form) #:use-module (crates-io))

(define-public crate-from_form-0.1.0 (c (n "from_form") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "08fkiiafx40mjmgzyip1ah7xkk3q6pvgcppwvn8qz82q0sx1jb6k")))

(define-public crate-from_form-0.1.1 (c (n "from_form") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "04ha1v2px6f3gd8gn051j1xx0i2x0lvvmzvl1xzrnzvjq3yi7k8a")))

