(define-module (crates-io fr om fromxml) #:use-module (crates-io))

(define-public crate-fromxml-1.0.0 (c (n "fromxml") (v "1.0.0") (d (list (d (n "xml-rs") (r "^0.1.4") (d #t) (k 0)))) (h "1vg1yijrq20v03wgmmd9959hs5ij4i2qsdbap7g9lda6swrzzjgq")))

(define-public crate-fromxml-1.1.0 (c (n "fromxml") (v "1.1.0") (d (list (d (n "xml-rs") (r "^0.1.5") (d #t) (k 0)))) (h "0rkjdyraf4jbh1byaa9k71nmi5xhyv1l4c8m3ax8g15rprmc929h")))

(define-public crate-fromxml-1.1.1 (c (n "fromxml") (v "1.1.1") (d (list (d (n "xml-rs") (r "^0.1.6") (d #t) (k 0)))) (h "12frrj116v8pkxqxlnmdb5jj8knnxip5cccvdvz2qhyv5m1al1rj")))

(define-public crate-fromxml-1.1.2 (c (n "fromxml") (v "1.1.2") (d (list (d (n "xml-rs") (r "= 0.1.6") (d #t) (k 0)))) (h "01wgqj6nhggbf7v9bq744h2c7vfi9grfi85szh2ls3484rxz0kfb")))

(define-public crate-fromxml-1.2.0 (c (n "fromxml") (v "1.2.0") (d (list (d (n "xml-rs") (r "^0.1.9") (d #t) (k 0)))) (h "0cllfd011nbrk7ciaf16wrjkfcsabmy5dgs0id5pfl1qan3200bi")))

