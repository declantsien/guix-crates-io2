(define-module (crates-io fr om from_remote_derive) #:use-module (crates-io))

(define-public crate-from_remote_derive-0.1.0 (c (n "from_remote_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1n7phi1w63m5jljwsbdda8kxmlk1wziazpazkx7yp3l801pqpp38")))

(define-public crate-from_remote_derive-0.1.1 (c (n "from_remote_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zs1fq0sca8iwhp6d7iswppvqa0jb1lh5mbgc81w391iskddb0fh")))

(define-public crate-from_remote_derive-0.1.2 (c (n "from_remote_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qh3vmy61v5slirzb5xv4ycfng168ibgj5120dzqjjmdnnl2dd4j")))

(define-public crate-from_remote_derive-0.1.3 (c (n "from_remote_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0h8jxkl8jb13vafn8qvyddznqwf6dlxrvj769yj2c9k5bc49f3dc")))

(define-public crate-from_remote_derive-0.1.4 (c (n "from_remote_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0bxlpn36a7yirmngmbwn6mj2vavh15gjzjwlwakjbs84xdz9xy56")))

(define-public crate-from_remote_derive-0.2.0 (c (n "from_remote_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "13k10xg3xwzzp0q48m5l46xl06sa6l2rjjzpcqdrmvfsyd1wf7qw")))

(define-public crate-from_remote_derive-0.2.1 (c (n "from_remote_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0bddw4g6ab8602kzwlqyybgpxcv0bzx2l3lygnnbznv3mdm5crjm")))

(define-public crate-from_remote_derive-0.2.2 (c (n "from_remote_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0s53970nz8jbffvr1z4b1h2xp8i9llswksb8160r9i52c4vr59yz")))

