(define-module (crates-io fr om from_int_derive) #:use-module (crates-io))

(define-public crate-from_int_derive-0.1.0 (c (n "from_int_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "046ixrgr5adczvzcdfd1a1vckb9d18r5f8vijbrfr7jgnbf3y7cf")))

(define-public crate-from_int_derive-0.1.2 (c (n "from_int_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0c34h1bphz50zc190lcv64dprljrjg81p1dia2zsbmkm3ag6s5xv")))

