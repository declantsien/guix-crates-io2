(define-module (crates-io fr om from_as_file) #:use-module (crates-io))

(define-public crate-from_as_file-0.1.0 (c (n "from_as_file") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "157ayvs4lmf1my0lbxpw9llv7yrj53bpnsf489vnkvqv90zafxfs")))

(define-public crate-from_as_file-0.1.1 (c (n "from_as_file") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1f61k8h9n26czrzm073i5imd7zb995xj6niw7jwwxpyfj2c73z8y")))

(define-public crate-from_as_file-0.2.0 (c (n "from_as_file") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.20") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1krw459f1lsfak8n7j7sk81p7pdd9fnf9nx3a3nsdzzwsrlplpgn")))

