(define-module (crates-io fr om from-attr-core) #:use-module (crates-io))

(define-public crate-from-attr-core-0.1.0 (c (n "from-attr-core") (v "0.1.0") (d (list (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "1sl20gyjmhc64bmzlcc4pc1vily81h5vjwanvkxjswbm8azxwyz6")))

(define-public crate-from-attr-core-0.1.1 (c (n "from-attr-core") (v "0.1.1") (d (list (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "00wi43bcb5zmlirgslddvw74bkfhd47qzwnpisyfqifrrqq4zhqk")))

(define-public crate-from-attr-core-0.1.2 (c (n "from-attr-core") (v "0.1.2") (d (list (d (n "syn") (r "^2") (f (quote ("parsing"))) (k 0)))) (h "0kzxgy9v6d61d4fd3nqx13wg8d991niz51wsswya10n9xdrdjp7d")))

