(define-module (crates-io fr om from-attr-macro) #:use-module (crates-io))

(define-public crate-from-attr-macro-0.1.0 (c (n "from-attr-macro") (v "0.1.0") (d (list (d (n "from-attr-core") (r "^0.1.0") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "quote-use") (r "^0.8") (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro"))) (k 0)))) (h "0dfhspd7wvm00wbbl8w32fyajjdwysx5219jmcq5fww58p00lnpr")))

(define-public crate-from-attr-macro-0.1.1 (c (n "from-attr-macro") (v "0.1.1") (d (list (d (n "from-attr-core") (r "^0.1.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "quote-use") (r "^0.8") (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro"))) (k 0)))) (h "1frpkz3xsa9vvw5qi4rrk5m6l3vm0ywx2qif2jlmf6fsbzfkvi85")))

(define-public crate-from-attr-macro-0.1.2 (c (n "from-attr-macro") (v "0.1.2") (d (list (d (n "from-attr-core") (r "^0.1.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "quote-use") (r "^0.8") (k 0)) (d (n "syn") (r "^2") (f (quote ("proc-macro"))) (k 0)))) (h "108xi63a918q6yr7skwfqj2bi7zxrqv97cn07awrmxx05k99s66r")))

