(define-module (crates-io fr om from-regex) #:use-module (crates-io))

(define-public crate-from-regex-0.1.0 (c (n "from-regex") (v "0.1.0") (d (list (d (n "from-regex-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vkxwgr1kl9f9zbybyip6phfhdlnz0c24cb8jns4zymsvaxx04zn")))

(define-public crate-from-regex-0.2.1 (c (n "from-regex") (v "0.2.1") (d (list (d (n "from-regex-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "segmap") (r "^0.1") (d #t) (k 0)))) (h "0bsd22lb4rbfn0nsqwsz0riqp97fjnrrl2ajh4nrb3149gwg9a5j")))

