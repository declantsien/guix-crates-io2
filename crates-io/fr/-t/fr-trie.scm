(define-module (crates-io fr -t fr-trie) #:use-module (crates-io))

(define-public crate-fr-trie-0.0.1 (c (n "fr-trie") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 2)))) (h "09ynghq4gnz23hhr6q6gh1vr5cpby0w6p7x1384pkjlmikq0im8f") (y #t)))

(define-public crate-fr-trie-0.0.2 (c (n "fr-trie") (v "0.0.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 2)))) (h "0nnxrfpg9kc5c5niyya6alskcal44phsd5c3sqkilic2ia9n0ra9")))

(define-public crate-fr-trie-0.0.3 (c (n "fr-trie") (v "0.0.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 2)))) (h "1yh2xc4jgybdxpaawaamfwip1zwijrs4qszydpiylz983jy8721i")))

(define-public crate-fr-trie-0.0.4 (c (n "fr-trie") (v "0.0.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 2)))) (h "0qadh6y6sasczncwz8cf0mxnmjz6vzzshjdm39jjaffna2z2mqj4")))

