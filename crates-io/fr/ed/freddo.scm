(define-module (crates-io fr ed freddo) #:use-module (crates-io))

(define-public crate-freddo-0.1.0 (c (n "freddo") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sy04k30vm9l2aq0wfs7vczsy9d71p3qg3fz9mi26i2g93hdvill")))

