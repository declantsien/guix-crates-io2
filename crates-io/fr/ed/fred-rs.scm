(define-module (crates-io fr ed fred-rs) #:use-module (crates-io))

(define-public crate-fred-rs-0.1.0 (c (n "fred-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.1") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.47") (d #t) (k 0)))) (h "0dz182xnm7nnnb1xq5l9vy8bh9bfn0fqjp1ngclp7qn4avln54s1")))

(define-public crate-fred-rs-0.1.1 (c (n "fred-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10.1") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.47") (d #t) (k 0)))) (h "0m5gh4fpw109phmgir8y396ybad51cpwraq0wb0ai6l99kfpf4rn")))

