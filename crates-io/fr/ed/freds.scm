(define-module (crates-io fr ed freds) #:use-module (crates-io))

(define-public crate-freds-0.0.1 (c (n "freds") (v "0.0.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)))) (h "145qhscsbikz48yxblwvdyppby2165r46a2m057mbk06ygnhqvfq") (f (quote (("write" "tokio") ("read") ("default" "read" "write" "serde" "serde_json"))))))

(define-public crate-freds-0.0.2 (c (n "freds") (v "0.0.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)))) (h "08a3c4yhp3z24yhvp8n59jaqjvp5pf9zd7r382rk3dzciqivqmwl") (f (quote (("write" "tokio") ("read") ("default" "read" "write" "serde" "serde_json"))))))

(define-public crate-freds-0.1.0 (c (n "freds") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1rc7sq5ci7w728y1fn2imf4s9w8yczgh0z9mkvp87y6h4mfx1g9d") (f (quote (("write" "tokio") ("read") ("default" "read" "write" "serde" "serde_json"))))))

(define-public crate-freds-0.1.1 (c (n "freds") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0ivz75d35x0nhnj43hcyjgrdz1syx2j5qnmqf0b0c3rk19ya2d6x") (f (quote (("write" "tokio") ("read") ("default" "read" "write" "serde" "serde_json"))))))

(define-public crate-freds-0.1.2 (c (n "freds") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0zx45xlsy5lxnis5gk8hklcsclrmpz89frx7da2pyykhv4dwibm9") (f (quote (("write" "tokio") ("read") ("default" "read" "write" "serde" "serde_json"))))))

