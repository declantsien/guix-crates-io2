(define-module (crates-io fr it fritzlogger) #:use-module (crates-io))

(define-public crate-fritzlogger-0.1.0 (c (n "fritzlogger") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "roxmltree") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0q4h49ch4jd96hsd44654mlxvdg78piajz7a3lnv8612mnf7r4h7")))

