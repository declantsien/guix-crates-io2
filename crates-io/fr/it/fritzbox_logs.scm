(define-module (crates-io fr it fritzbox_logs) #:use-module (crates-io))

(define-public crate-fritzbox_logs-0.1.0 (c (n "fritzbox_logs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1qmkzpmmda01qqfm709j9qghxmrah8mgp03hcxbcid091zxnlyqp")))

(define-public crate-fritzbox_logs-0.2.0 (c (n "fritzbox_logs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0n4qp285j3bwjfdjrbvqsms2iyjl113n40958p3gknwfimch3k9j")))

