(define-module (crates-io fr it fritzbox_logs_analyzer) #:use-module (crates-io))

(define-public crate-fritzbox_logs_analyzer-0.1.0 (c (n "fritzbox_logs_analyzer") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "fritzbox_logs") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "1b3x0fl91wsayrfr4xvkqpvv2j25fvhdhqclzhxzpqh9xmlagv81")))

