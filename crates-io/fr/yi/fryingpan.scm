(define-module (crates-io fr yi fryingpan) #:use-module (crates-io))

(define-public crate-fryingpan-0.1.0 (c (n "fryingpan") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "grid") (r "^0.9.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0nprvbgmyclbijc5mj8j7rcz6rfr9j2x7zshnsgjr1xykbvg8z4a")))

(define-public crate-fryingpan-0.1.1 (c (n "fryingpan") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "grid") (r "^0.9.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "13vkb1m85d08x1d7xsg1g8x54f1fz5zbqp7qgkrd0fl4hhanx7k6")))

(define-public crate-fryingpan-0.1.2 (c (n "fryingpan") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "grid") (r "^0.9.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1mwy5dflm6f8d2ajm6fwz6lwfz8wnznnl5hrnfb4rywmfysh504p")))

