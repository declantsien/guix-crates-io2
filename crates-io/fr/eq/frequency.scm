(define-module (crates-io fr eq frequency) #:use-module (crates-io))

(define-public crate-frequency-1.0.0 (c (n "frequency") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "01nv0081y4gk6x2yq339lpmc2ppi3fa83gb43j8manp0h3dzrrw0")))

(define-public crate-frequency-1.0.1 (c (n "frequency") (v "1.0.1") (d (list (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "05v762chl28s1vczsnmwsv2sgax9ii8dz1fbj2r14r183528gb5h")))

