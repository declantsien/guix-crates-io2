(define-module (crates-io fr eq freqiterator) #:use-module (crates-io))

(define-public crate-freqiterator-0.1.0 (c (n "freqiterator") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1cn7amv9sqj2arha8rpqimcpd6q276w7yj9xppz2r3qvhh3bkh0y")))

(define-public crate-freqiterator-1.0.0 (c (n "freqiterator") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0acv32d61zlqyjmam5ar8p1axqvh60g1f45l00f4awx79vhva94r")))

(define-public crate-freqiterator-2.0.0 (c (n "freqiterator") (v "2.0.0") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0h29bascg1lnfs9567498cmggl6l0k589qb470ssd801i32ppyfz")))

