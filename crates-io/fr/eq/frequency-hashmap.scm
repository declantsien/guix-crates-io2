(define-module (crates-io fr eq frequency-hashmap) #:use-module (crates-io))

(define-public crate-frequency-hashmap-1.0.0 (c (n "frequency-hashmap") (v "1.0.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "frequency") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "171kd3hs2k53jqjzz09adsab4ipvvchzbmh1dhxlin3xnidcrqzd") (y #t)))

(define-public crate-frequency-hashmap-1.1.0 (c (n "frequency-hashmap") (v "1.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "frequency") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "13qbdyswa2g61xqb7394s2hq92hspk6p3h7kf04qmvbjiazd54dq")))

