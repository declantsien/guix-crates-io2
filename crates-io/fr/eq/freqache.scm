(define-module (crates-io fr eq freqache) #:use-module (crates-io))

(define-public crate-freqache-0.1.0 (c (n "freqache") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uplock") (r "^0.1") (d #t) (k 0)))) (h "008w21j9fkgzmhzc1k8a8g0hnygr60ydnxmzzhzbz7pw9nwmdfk5")))

(define-public crate-freqache-0.2.0 (c (n "freqache") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uplock") (r "^0.1") (d #t) (k 0)))) (h "01j703z088xzir8hsn4zwwlijl6asdzng3ag7y48y94n4s9xjkg9") (y #t)))

(define-public crate-freqache-0.2.1 (c (n "freqache") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uplock") (r "^0.1") (d #t) (k 0)))) (h "151c4blbm770xwfm1m3l0qi606p915s858g2cdcclgr82sgkrw6v") (y #t)))

(define-public crate-freqache-0.2.2 (c (n "freqache") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uplock") (r "^0.1") (d #t) (k 0)))) (h "14ab34z6g6j56ysr0igdf8vcx1inlqbx1mpgr6sg4f8hishmhv4w")))

(define-public crate-freqache-0.2.3 (c (n "freqache") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.8") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uplock") (r "~0.1.2") (d #t) (k 0)))) (h "1akcqxpyigyjg5izaa1s4x2jvp39r1m946i562vnf2hazra20hba")))

(define-public crate-freqache-0.2.4 (c (n "freqache") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.8") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uplock") (r "~0.1.2") (d #t) (k 0)))) (h "1rgbvyb62xk3k03wm3f34fx9nall6cs81f9xmdqhm4iibafnzs6g")))

(define-public crate-freqache-0.2.5 (c (n "freqache") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.11") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.11") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0ml1ps22ys7fz7c2kj1w271iy1423q1rqbjyjk365a72yrd09h2v")))

(define-public crate-freqache-0.3.0 (c (n "freqache") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.11") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0vqvaj0v609ki9a8w5wcxqnns7b9d97j6ddjgr258q9faba62wm2")))

(define-public crate-freqache-0.4.0 (c (n "freqache") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xdvygcsq7gnwrswrfja9n1kvqykqs79wzlfm22xwwm650lyva0g") (y #t)))

(define-public crate-freqache-0.4.1 (c (n "freqache") (v "0.4.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0d1w0n9cd64xiihkj6rpmqvk44a160n9cfnrbh9ndkgw60vm4jsa")))

(define-public crate-freqache-0.5.0 (c (n "freqache") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0cs9dk2vjhgg2k4r9k7qxjrm8lwxdjky141p8ik5biq5nal9a174")))

(define-public crate-freqache-0.6.0 (c (n "freqache") (v "0.6.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10lbv1qqjmacy7p6mnfiqb3kj7r6hxbckbwdm6dr4hxk5x8p5fc9")))

