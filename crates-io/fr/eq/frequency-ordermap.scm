(define-module (crates-io fr eq frequency-ordermap) #:use-module (crates-io))

(define-public crate-frequency-ordermap-1.0.0 (c (n "frequency-ordermap") (v "1.0.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "frequency") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "ordermap") (r "^0.2.8") (d #t) (k 0)))) (h "0rfkrvcqiic0sdz2rp56v0a7prgxrzvkgnzw15wrrydvpj26pkp2")))

