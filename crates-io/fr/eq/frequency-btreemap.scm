(define-module (crates-io fr eq frequency-btreemap) #:use-module (crates-io))

(define-public crate-frequency-btreemap-1.0.0 (c (n "frequency-btreemap") (v "1.0.0") (d (list (d (n "frequency") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "19ml6l706a86wajvgiqpp0rjqkarj6d4q51h40fgzs535r0rhy84")))

