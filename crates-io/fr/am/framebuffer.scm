(define-module (crates-io fr am framebuffer) #:use-module (crates-io))

(define-public crate-framebuffer-0.1.0 (c (n "framebuffer") (v "0.1.0") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.2.2") (d #t) (k 0)))) (h "074kmzklxwjyvhngjl73rl5xp7w1crls5q13pa7r3ipjlsn32k4p")))

(define-public crate-framebuffer-0.1.1 (c (n "framebuffer") (v "0.1.1") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.2.2") (d #t) (k 0)))) (h "0sn1nfk1c5kiwjfg3m18dfdwfcy7by9jcmz2nck4yipnj3z2nq0s")))

(define-public crate-framebuffer-0.1.2 (c (n "framebuffer") (v "0.1.2") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.2.2") (d #t) (k 0)))) (h "1y7k7yaa2v5d7qa2dy6zrwdd2bi9m9ldcyvpbzgnf8hbpj9d6p8z")))

(define-public crate-framebuffer-0.1.3 (c (n "framebuffer") (v "0.1.3") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "0p4gsqrv6yv42fr70ycrnsh4g7fbwiw3k67amc19ns1rh27pyqkh")))

(define-public crate-framebuffer-0.1.4 (c (n "framebuffer") (v "0.1.4") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "0jjy3pabnz4zzsp2875a8crdbwym3qw57gjjh8vw1rgby7a1x62k")))

(define-public crate-framebuffer-0.1.5 (c (n "framebuffer") (v "0.1.5") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "1vm590q2k3ss5620pmfcgiakf7y1bbf9hbyq5y71kp4dq7l673b3")))

(define-public crate-framebuffer-0.1.6 (c (n "framebuffer") (v "0.1.6") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "0vx6qny31i8f2124r399pgb71mgpsld2bs0l3yi0ncvn50xgyy2k")))

(define-public crate-framebuffer-0.1.7 (c (n "framebuffer") (v "0.1.7") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 2)))) (h "0qgxc2969gf515xfrxvy7dih7c5n1byf8yl1vbrjsm8c1k8ff5g5")))

(define-public crate-framebuffer-0.1.8 (c (n "framebuffer") (v "0.1.8") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1mi0rg3j9iapv5vb6wdv50hka6wbffkwnjqza6cn5glnam72jqiq")))

(define-public crate-framebuffer-0.2.0 (c (n "framebuffer") (v "0.2.0") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0zydvv3srb6wz6bc3vkzrm92yh6wwxfclcf70xn1g06zqqfbvkcr")))

(define-public crate-framebuffer-0.2.1 (c (n "framebuffer") (v "0.2.1") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0idpv3r9jikppnvzplpmvfi3hgn68ddlwmdbwpiz9cf3j8zk5y4b")))

(define-public crate-framebuffer-0.2.2 (c (n "framebuffer") (v "0.2.2") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "12mbd74nivxs43z32lg1pm6nkyryhmbg2h27f96xj8h64yc21s6z")))

(define-public crate-framebuffer-0.3.0 (c (n "framebuffer") (v "0.3.0") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1rn5x23dm62pimb3brf6421pxhqh9xdn8gcjvzyfn3abwf6r59x7")))

(define-public crate-framebuffer-0.3.1 (c (n "framebuffer") (v "0.3.1") (d (list (d (n "bmp") (r "^0.1.4") (d #t) (k 2)) (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0jgi9slvmajy2hgf3f2bmhyr2zp06c4zkvn6gmq9yb5r3fpsm347")))

