(define-module (crates-io fr am framing_sv2) #:use-module (crates-io))

(define-public crate-framing_sv2-0.1.0 (c (n "framing_sv2") (v "0.1.0") (d (list (d (n "binary_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "156afzpxxpvbbhwsrm0prvps75l0vk3fffxcliz0xp1df10dv73f") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

(define-public crate-framing_sv2-0.1.3 (c (n "framing_sv2") (v "0.1.3") (d (list (d (n "binary_sv2") (r "^0.1.3") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "05n60i0f224afcymn0c9i4ykc2bhc1r8lxh7mprfcxv2aj6bja63") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

(define-public crate-framing_sv2-0.1.4 (c (n "framing_sv2") (v "0.1.4") (d (list (d (n "binary_sv2") (r "^0.1.5") (d #t) (k 0)) (d (n "buffer_sv2") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1d8wdb78qlz6dkd1k2vzv5gpkgvzwxpxzi0zzhvr08pr5rpmr98d") (f (quote (("with_serde" "binary_sv2/with_serde" "serde" "buffer_sv2/with_serde") ("with_buffer_pool" "binary_sv2/with_buffer_pool" "buffer_sv2"))))))

(define-public crate-framing_sv2-0.1.6 (c (n "framing_sv2") (v "0.1.6") (d (list (d (n "binary_sv2") (r "^0.1.6") (d #t) (k 0)) (d (n "buffer_sv2") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "0d1k22a6ihr2pd1bkmqfd0q1n3qmlh4r3pia2cqpg280ff95wm2a") (f (quote (("with_serde" "binary_sv2/with_serde" "serde" "buffer_sv2/with_serde") ("with_buffer_pool" "binary_sv2/with_buffer_pool" "buffer_sv2"))))))

(define-public crate-framing_sv2-1.0.0 (c (n "framing_sv2") (v "1.0.0") (d (list (d (n "binary_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "buffer_sv2") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "const_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1irc14misn0i1b8xy3lr1rmfkvc5m5xg8mmg1k1wa3ss8xkxcanj") (f (quote (("with_serde" "binary_sv2/with_serde" "serde" "buffer_sv2/with_serde") ("with_buffer_pool" "binary_sv2/with_buffer_pool" "buffer_sv2"))))))

(define-public crate-framing_sv2-1.1.0 (c (n "framing_sv2") (v "1.1.0") (d (list (d (n "binary_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "buffer_sv2") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "const_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "0rzwkwviyas5249g3yzcjnsvrly2jh4l2xlvd4hv8qwqj80ccsgn") (f (quote (("with_serde" "binary_sv2/with_serde" "serde" "buffer_sv2/with_serde") ("with_buffer_pool" "binary_sv2/with_buffer_pool" "buffer_sv2"))))))

