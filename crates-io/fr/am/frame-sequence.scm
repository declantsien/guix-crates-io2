(define-module (crates-io fr am frame-sequence) #:use-module (crates-io))

(define-public crate-frame-sequence-0.1.0 (c (n "frame-sequence") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1r2zknqg0262b7g2yc3cingxnfkky9aa74fdzx7xjxfcwwncm7jz")))

(define-public crate-frame-sequence-0.1.1 (c (n "frame-sequence") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)))) (h "0az9d9hk71rgir395wrn3kwwyymsv4cf4xkjpvgqi4pr4473ik2z")))

