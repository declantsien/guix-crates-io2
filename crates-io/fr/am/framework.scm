(define-module (crates-io fr am framework) #:use-module (crates-io))

(define-public crate-framework-0.1.0 (c (n "framework") (v "0.1.0") (h "0c1hk5zz2ycnwsx3dsvfcc9iirmy6vmxhzwv9y8wnx2njkqvax6b")))

(define-public crate-framework-0.2.0 (c (n "framework") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "11prrmv8924a3rm93r64r65j5hwv391qb85ikx86fzc29f52cisw")))

(define-public crate-framework-0.2.1 (c (n "framework") (v "0.2.1") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1wcpxr647sjwdcpxdlh3in4sc2fy5qhcrkd3x52xjnd4dq7z2awl")))

(define-public crate-framework-0.2.2 (c (n "framework") (v "0.2.2") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0gha0w9vvpb5xv1kgw9ad3wp1a1f4dhljhs4wn2v1c339vhs8vd3")))

(define-public crate-framework-0.2.3 (c (n "framework") (v "0.2.3") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "15zdcnkbijcgfjpan3pv89l3f6laz4d3l9pyg667v302sq2alcav")))

(define-public crate-framework-0.2.4 (c (n "framework") (v "0.2.4") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mlvkxviipsr2wl14vjys0pb4jb8zsvg3i8vbk90s7500g28cjwy")))

