(define-module (crates-io fr am framing) #:use-module (crates-io))

(define-public crate-framing-0.1.0 (c (n "framing") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0fqzls7dk590r7kmhw5gmvxfi831ahrpsx86wdrri6mf69fc8fnq")))

(define-public crate-framing-0.1.1 (c (n "framing") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1igd3m3z5k2pgdzmhds578x06nwcni4n926wxfxmdg273s4iw782")))

(define-public crate-framing-0.2.0 (c (n "framing") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0n38s6cmlr16pgblqzbkkrbk9rb828wpx3vvig6cxrx85w48qqd5")))

(define-public crate-framing-0.2.1 (c (n "framing") (v "0.2.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1cbxiga5l7k5pr3g36j3d2724519jcbavv5aysy3y49snn4qyhxw")))

(define-public crate-framing-0.3.0 (c (n "framing") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1qhskf37hwg9sjnkh2dak3x5si3d3n4ywpw84acbndxjh2vllnsp")))

(define-public crate-framing-0.3.1 (c (n "framing") (v "0.3.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "05krg941mvsb10y3gm5ikkhza09grccakdns1az336ny4n10hzff")))

(define-public crate-framing-0.3.2 (c (n "framing") (v "0.3.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0avqkhwvsdbig4zc48hvsvldc4j3hxi0kb8xmvxzchlwl304li5a")))

(define-public crate-framing-0.3.3 (c (n "framing") (v "0.3.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0wki8w9dfsxvq4jnc9xlgj2q7cm2ims8qzy8qn8kmmdlihm3gqi7")))

(define-public crate-framing-0.3.4 (c (n "framing") (v "0.3.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "08x0xwwz6jd16l0pca9vczzdv4l48d6yzk8r6v4l16s849k7p6cq")))

(define-public crate-framing-0.3.5 (c (n "framing") (v "0.3.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "15i3kdnjyqy15m74l4h6gxdfn7kzz6hrlr1zd7qsrarjq7bb6xbv")))

(define-public crate-framing-0.4.0 (c (n "framing") (v "0.4.0") (d (list (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)))) (h "062fzsn73bvgg0pavqw2552jhvnn4d8whfw0jj7m3iyhwal7xjv9")))

(define-public crate-framing-0.4.1 (c (n "framing") (v "0.4.1") (d (list (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)))) (h "182djg6a0jj9cl3xav6589qn0vf08rhlkhxkqiwjh6b7vlriss81")))

(define-public crate-framing-0.5.0 (c (n "framing") (v "0.5.0") (d (list (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)))) (h "0bc6ly1j0akjym34kxwqc0q890rx7z1skkq0ari2kp6b140a5crm")))

(define-public crate-framing-0.6.0 (c (n "framing") (v "0.6.0") (d (list (d (n "clamp") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)))) (h "11qaacgw44vdx2gblgxabrjdf5h4kqrgki8856k9k5fzcjrmz6pv")))

