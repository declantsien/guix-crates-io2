(define-module (crates-io fr am framestream) #:use-module (crates-io))

(define-public crate-framestream-0.1.2 (c (n "framestream") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.1") (d #t) (k 0)) (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)))) (h "0na9dfjxp6n7k6g4chhmaw1kqpjn8vrr4x2hq11lgfwxzw190c9c")))

(define-public crate-framestream-0.1.3 (c (n "framestream") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "07v77b66ligv6z9bfbggjkz27f39z1nvb1fyb1iwl0zzwsvfsbi5")))

(define-public crate-framestream-0.1.4 (c (n "framestream") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1vd7f8qblfv12m0n6a31aa5sswys0c9pc3rz2zlj39m32v1q3c3q")))

(define-public crate-framestream-0.2.4 (c (n "framestream") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^0.6") (d #t) (k 0)))) (h "0w46lw77n3grln46p44mmqln2sypry0jiiby5nlsdxrzpyg6azfq")))

(define-public crate-framestream-0.2.5 (c (n "framestream") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "0x6bml2zm7w666pf70sdlp58ya3h7z42h036ppsnr79iic3h5iy0")))

