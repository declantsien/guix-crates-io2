(define-module (crates-io fr am frame-system-rpc-runtime-api) #:use-module (crates-io))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-alpha.3 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-alpha.3") (d (list (d (n "codec") (r "^1.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-alpha.2") (k 0)))) (h "0sra6yl6xlfzb6dicjpx2lp5mnmrvr1ksxn0qxjj1l02yfhyp0d5") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-alpha.5 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-alpha.5") (d (list (d (n "codec") (r "^1.2.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-alpha.5") (k 0)))) (h "0w67pswq2zar0xh8mjlrj959s5c4g80dmlsadr6dn2n75bz33av3") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-alpha.6 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-alpha.6") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-alpha.6") (k 0)))) (h "0mknwq1idhigl6x7nva45h7a4c09m3zxsi6cs07w8inhi77rxg3m") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-alpha.7 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-alpha.7") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-alpha.7") (k 0)))) (h "1491zlcm0jp9jl6qhlpkz753vd4mrqy5spxcggf2j6iyvm2g2ind") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-alpha.8 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-alpha.8") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-alpha.8") (k 0)))) (h "13prsxwm2a8ggjs2g0msx492bxxbzw2hl009h7sqdqkx9l2pba3h") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-rc1 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-rc1") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-rc1") (k 0)))) (h "1pb2mnycj41d5gld3yygca0kaaij7dxjiybmv0mmhxm3wva0vxb6") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-rc2 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-rc2") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-rc2") (k 0)))) (h "1z6m2hls2ds99rx34dnx0x2g99qyajm2l64mzrm19935p84dnpfd") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-rc3 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-rc3") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-rc3") (k 0)))) (h "0wjph3wl6lrfm9r0i18k8qr5rxjkbpwnr8scpxbhh1cvcwzadg14") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-rc4 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-rc4") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-rc4") (k 0)))) (h "0kc3nxb3871kk18zmn1k0bz4wy1ja9q0zvs1bv6g3jx64bf8jlcz") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-rc5 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-rc5") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-rc5") (k 0)))) (h "1qfpnyc21961jaal63cwgm538hq079zr2380x7b1jr8kvsc49zr3") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0-rc6 (c (n "frame-system-rpc-runtime-api") (v "2.0.0-rc6") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0-rc6") (k 0)))) (h "06dsxkqijnn3maqyr2if3jky5y5s6ng5gwxxsjrcm6bb581hy81r") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.0 (c (n "frame-system-rpc-runtime-api") (v "2.0.0") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0") (k 0)))) (h "0wz3rgyrsv73ssnm85v6wa02s7la999bx0g8lz1rgm6rkxl8y4lv") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-2.0.1 (c (n "frame-system-rpc-runtime-api") (v "2.0.1") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^2.0.0") (k 0)))) (h "1apvk7p6qh8mmp0ji7mmrzgkfzizfcjr5ab3bnb1lmrm075jbdny") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-3.0.0 (c (n "frame-system-rpc-runtime-api") (v "3.0.0") (d (list (d (n "codec") (r "^2.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^3.0.0") (k 0)))) (h "0780y3lm5shl79b07cdfx37479v9grv0qrixrbjdwgwhdbwd5faw") (f (quote (("std" "sp-api/std" "codec/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-4.0.0 (c (n "frame-system-rpc-runtime-api") (v "4.0.0") (d (list (d (n "codec") (r "^3.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^4.0.0") (k 0)))) (h "0scqmax8snhii1cmmbdv8bgrmdd57k3mpf2qkk2755nd03mjraiw") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-5.0.0 (c (n "frame-system-rpc-runtime-api") (v "5.0.0") (d (list (d (n "codec") (r "^3.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^5.0.0") (k 0)))) (h "1b4zikgk3rn426kswyfyv9q5x7akbjl1xhrbr25frv2idzv26zjp") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-6.0.0 (c (n "frame-system-rpc-runtime-api") (v "6.0.0") (d (list (d (n "codec") (r "^3.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^6.0.0") (k 0)))) (h "1marbxpzxlvrmi6rn7234zj67hzmkcd50lm9lr4l23hdpr95rwgj") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-7.0.0 (c (n "frame-system-rpc-runtime-api") (v "7.0.0") (d (list (d (n "codec") (r "^3.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^7.0.0") (k 0)))) (h "1srskkyh7m0dfi2zpaxll9m82bq3dl5mwjbqsqw5rbwxppfmv1xj") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-8.0.0 (c (n "frame-system-rpc-runtime-api") (v "8.0.0") (d (list (d (n "codec") (r "^3.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^8.0.0") (k 0)))) (h "0h98z4z3ga919q4bdg8l5y50h4s01zzxxpc9hzy99d2kh4xzs2lb") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-9.0.0 (c (n "frame-system-rpc-runtime-api") (v "9.0.0") (d (list (d (n "codec") (r "^3.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^9.0.0") (k 0)))) (h "0lvh7fyndidkxryxds704llsdkd0dwmwbwya5yslslijl3p8d4wd") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-10.0.0 (c (n "frame-system-rpc-runtime-api") (v "10.0.0") (d (list (d (n "codec") (r "^3.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^10.0.0") (k 0)))) (h "05nhk8xapncimpm5zl8xs178jsns04349g9g3f0cj2l8hhq11ghf") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-11.0.0 (c (n "frame-system-rpc-runtime-api") (v "11.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^11.0.0") (k 0)))) (h "17kihpvrz0498kplaam57zflyjg6v1lkcf00vsjpi20i6rs7j745") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-12.0.0 (c (n "frame-system-rpc-runtime-api") (v "12.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^12.0.0") (k 0)))) (h "0gs2hprjwm8wssryck5rnfh1s4asxbjrv2i11292dln4fpf5c789") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-13.0.0 (c (n "frame-system-rpc-runtime-api") (v "13.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^13.0.0") (k 0)))) (h "1kx24as7l7zn3jf8n0kxvy1zyvf0yi9nl73ia6f8hvzvdzn7khy1") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-14.0.0 (c (n "frame-system-rpc-runtime-api") (v "14.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^14.0.0") (k 0)))) (h "09dld6b2nzzsj6kjjg6p7f8y64ynjcg059r24h60zmzxv4pzhpv9") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-15.0.0 (c (n "frame-system-rpc-runtime-api") (v "15.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^15.0.0") (k 0)))) (h "0zmk068dnj2hbjzqkw2hcwc23vvasfvc8lfm48mwq3ydrzvx9754") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-16.0.0 (c (n "frame-system-rpc-runtime-api") (v "16.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^16.0.0") (k 0)))) (h "1na3lmxppxzqqiwb0l0pgvspfl69skj98w7g5zlj9mkd7mr6n5z8") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-17.0.0 (c (n "frame-system-rpc-runtime-api") (v "17.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^17.0.0") (k 0)))) (h "1z1x5al8gl429mw51ix87gli687xvafahw1r5d4mimyyvd8l6rk0") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-18.0.0 (c (n "frame-system-rpc-runtime-api") (v "18.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^18.0.0") (k 0)))) (h "02dcbap9h03y163bgd4qb8vy27bc6xxj6kmzhfkqwdkv47rhmlab") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-19.0.0 (c (n "frame-system-rpc-runtime-api") (v "19.0.0") (d (list (d (n "codec") (r "^3.2.2") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^19.0.0") (k 0)))) (h "018ddxmvi3q6skyd1jscv7c36b4136r01drv60gnhnkajzpf9mx8") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-19.1.0-dev.2 (c (n "frame-system-rpc-runtime-api") (v "19.1.0-dev.2") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^19.1.0-dev.2") (k 0)))) (h "0wfdjycvbz8q5m9fywxa1qailip1wyfzx4zbkjz03q7z6d2l1sbl") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-19.1.0-dev.3 (c (n "frame-system-rpc-runtime-api") (v "19.1.0-dev.3") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^19.1.0-dev.3") (k 0)))) (h "1j8kvjwq5m1wym7ahljgn3yihznlq5x64h4p7pkrzs74yj4ja8y4") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-19.1.0-dev.5 (c (n "frame-system-rpc-runtime-api") (v "19.1.0-dev.5") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "=19.1.0-dev.5") (k 0)))) (h "1ln0cd3hkdfajzvwa2b9nqwb5l2s8fvgkvmmi448smwh9jrs4lj1") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-19.1.0-dev.6 (c (n "frame-system-rpc-runtime-api") (v "19.1.0-dev.6") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "=19.1.0-dev.6") (k 0)))) (h "1n2q2m8x75kgv3sdqyvbjp9s367ly6x305f5kamiwf5pb93cj5a4") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-20.0.0 (c (n "frame-system-rpc-runtime-api") (v "20.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^20.0.0") (k 0)))) (h "1n338dl948skdy1hlwas3xz7g5crcgmb9z4yw3hap2i27a8h7cn1") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-21.0.0 (c (n "frame-system-rpc-runtime-api") (v "21.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^21.0.0") (k 0)))) (h "0qh5b7sd3n9zsppxb5v75wqzib3yj2p8fdzrp0w2k5hyh72z9817") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-22.0.0-dev.1 (c (n "frame-system-rpc-runtime-api") (v "22.0.0-dev.1") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "=22.0.0-dev.1") (k 0)))) (h "03gz31fk40lbdfnkhcfr52a4br71pm20ji3szak4kvxavfldyds4") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-22.0.0 (c (n "frame-system-rpc-runtime-api") (v "22.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^22.0.0") (k 0)))) (h "1l6hq8j7jjawjhr6i9301gm2yfsxb6a43f8n1laczljagszd4gm1") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-23.0.0 (c (n "frame-system-rpc-runtime-api") (v "23.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^23.0.0") (k 0)))) (h "1lyn09q3y4791bh6sj5kk6jf0ic6ncq2qylpnv5dcsl6zls2lmqp") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-24.0.0 (c (n "frame-system-rpc-runtime-api") (v "24.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^24.0.0") (k 0)))) (h "0cfp7n6dg4a8iqg6qdicxngn8q5kxxlii7w8mr2lm6jq4s65sy8l") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-25.0.0 (c (n "frame-system-rpc-runtime-api") (v "25.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^25.0.0") (k 0)))) (h "1q8rb3ps6df5i3bz43fs7xx4bkwg6sxlnx61p873k6mcnhgxvqrw") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-26.0.0 (c (n "frame-system-rpc-runtime-api") (v "26.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^26.0.0") (k 0)))) (h "0yl3q18sff9330c5rnw7kni29v3bmqiy87ynmj4ivpx7ks0q7l98") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-27.0.0 (c (n "frame-system-rpc-runtime-api") (v "27.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^27.0.0") (k 0)))) (h "1j0czian9r5gj3jj0v6gckx3937c64b9gv85x5nbaxiw7i1j06sc") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-28.0.0 (c (n "frame-system-rpc-runtime-api") (v "28.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^28.0.0") (k 0)))) (h "0xdlq20nvjaxcr16d7gxgz4vgl0dc0lnqm6kjzkvfyx0761v4j3v") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-29.0.0 (c (n "frame-system-rpc-runtime-api") (v "29.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^29.0.0") (k 0)))) (h "0h8c2l4ikxm9ya09bxb8dkc8cfdcshg2v4g865kysc0nrzgfir3s") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-30.0.0 (c (n "frame-system-rpc-runtime-api") (v "30.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^30.0.0") (k 0)))) (h "0rg1a3vwc92d2441gfqj9yvrwyrwpcbdvakpyl5wad8cxw7iqi94") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-31.0.0 (c (n "frame-system-rpc-runtime-api") (v "31.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^31.0.0") (k 0)))) (h "17kqfggxqj5h3zdscnabgjggsghn4cv1p2z29mfq426xh7j1lxsl") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

(define-public crate-frame-system-rpc-runtime-api-32.0.0 (c (n "frame-system-rpc-runtime-api") (v "32.0.0") (d (list (d (n "codec") (r "^3.6.1") (k 0) (p "parity-scale-codec")) (d (n "sp-api") (r "^32.0.0") (k 0)))) (h "19gbfa97v6yvg69mizmgnvjhnwhx183rigdbzmp4agdr33zf74mx") (f (quote (("std" "codec/std" "sp-api/std") ("default" "std"))))))

