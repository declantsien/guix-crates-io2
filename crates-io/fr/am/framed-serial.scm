(define-module (crates-io fr am framed-serial) #:use-module (crates-io))

(define-public crate-framed-serial-0.1.0 (c (n "framed-serial") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-serial") (r "^0.3") (d #t) (k 0)) (d (n "serial") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1yq2mfgkc4kfh2i19jm9cr7gd3wxsx91hpz009crl9m57z1d5l65") (f (quote (("std" "serial") ("device_connected" "std") ("default" "std") ("collections"))))))

(define-public crate-framed-serial-0.2.0 (c (n "framed-serial") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-serial") (r "^0.4") (d #t) (k 0)) (d (n "serial") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0bfzbjxggqh1rws5nx2kb2p7rhgv2ihnxgpbs9k4pzhphn4lbad4") (f (quote (("std" "serial") ("device_connected" "std") ("default" "std") ("collections"))))))

(define-public crate-framed-serial-0.3.0 (c (n "framed-serial") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-serial") (r "^0.4") (d #t) (k 0)) (d (n "serial") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1qn78vvw4ikk3zzkbdrmykyhr5wpv7yy3wsz8a677cv7zbvlc3i2") (f (quote (("std" "serial") ("device_connected" "std") ("default" "std") ("collections"))))))

(define-public crate-framed-serial-0.3.1 (c (n "framed-serial") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-serial") (r "^0.4") (d #t) (k 0)) (d (n "serial") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0v7hj6x5zbnk1zyc65kp1fazxr5500n4wsa3aclsjb8qi6c7xqhr") (f (quote (("std" "serial") ("device_connected" "std") ("default" "std") ("collections"))))))

(define-public crate-framed-serial-0.4.0 (c (n "framed-serial") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-serial") (r "^0.5") (d #t) (k 0)) (d (n "serial") (r "^0.3") (o #t) (d #t) (k 0)))) (h "13a44vp12acmvh9m17qgb9q4fcy18f8q013qaglcz9cfvdikmrma") (f (quote (("std" "serial") ("device_connected" "std") ("default" "std") ("collections"))))))

