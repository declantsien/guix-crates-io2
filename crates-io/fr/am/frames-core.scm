(define-module (crates-io fr am frames-core) #:use-module (crates-io))

(define-public crate-frames-core-0.1.0 (c (n "frames-core") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full" "test-util"))) (d #t) (k 2)))) (h "1x0mjhinrf8gb88540g76ij9pa6j9df5jzrg7wwp59z2hah1dpg2")))

