(define-module (crates-io fr am frame-rate) #:use-module (crates-io))

(define-public crate-frame-rate-0.1.0 (c (n "frame-rate") (v "0.1.0") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1m3cylaxnx8bijimnhmprz16p8slkx0fr5sjnv850rjwr3vbpvw2")))

(define-public crate-frame-rate-0.1.1 (c (n "frame-rate") (v "0.1.1") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11165jp6nv6f9rhj85cdaxv35yqwkclai11rgyj1bcyz6rskm3f3")))

(define-public crate-frame-rate-0.2.0 (c (n "frame-rate") (v "0.2.0") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bpd09hi327scr577qyvl4kprqf2v048harcnpwjvra76jqni3nn")))

(define-public crate-frame-rate-0.2.1 (c (n "frame-rate") (v "0.2.1") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09gl0rlq3cmqc1fvfhz6c4inpygsq5ah33w419273hfgzcfjf097")))

