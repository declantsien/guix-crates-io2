(define-module (crates-io fr am framed) #:use-module (crates-io))

(define-public crate-framed-0.1.0 (c (n "framed") (v "0.1.0") (d (list (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "08fck71c29cwspx62f7fr341nbv5bkjb723az457kqwpw5hxir51") (f (quote (("use_std") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.1.1 (c (n "framed") (v "0.1.1") (d (list (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "1j97md0md3mx3kri6ydh43l05qbpbka01k1xminvlvgr87r53vpq") (f (quote (("use_std") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.1.2 (c (n "framed") (v "0.1.2") (d (list (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "02gv7m86sj6ky29x8g6jci88b6ha4r6wqk89dxzas8l2v1als0v6") (f (quote (("use_std") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.1.3 (c (n "framed") (v "0.1.3") (d (list (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)))) (h "14vh415zk4saxcbmr77gz4zlidw6qmshjkkcwvz6psc1pfll8zqm") (f (quote (("use_std") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.1.4 (c (n "framed") (v "0.1.4") (d (list (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "ssmarshal") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17fk7aav42disffnfh1zqr8lzq154h97z959inyskyvg9njmj12b") (f (quote (("use_std") ("typed" "serde" "ssmarshal") ("trace") ("default" "use_std" "typed"))))))

(define-public crate-framed-0.2.0 (c (n "framed") (v "0.2.0") (d (list (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "ssmarshal") (r "^1.0") (d #t) (k 0)))) (h "1vrj2jvgnbgb58ik1w7d4nlzjbvsg1lifznx30pq430qv1zpc7lz") (f (quote (("use_std") ("use_nightly") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.3.0 (c (n "framed") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "crc16") (r "^0.3.4") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "ssmarshal") (r "^1.0") (d #t) (k 0)))) (h "0d72ba2ldsij8q1nd5ywv2wdkisnh70jm878mp8g99cw5snlq963") (f (quote (("use_std") ("use_nightly") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.4.0 (c (n "framed") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "crc16") (r "^0.3.4") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "ssmarshal") (r "^1.0") (d #t) (k 0)))) (h "1wg6sw5bxv4y67gvlwsg13a75bh02iafnd8jacyq3ayas65lrwja") (f (quote (("use_std") ("use_nightly") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.4.1 (c (n "framed") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cobs") (r "^0.1.3") (d #t) (k 0)) (d (n "crc16") (r "^0.3.4") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "ssmarshal") (r "^1.0") (d #t) (k 0)))) (h "18hckardvg92m89l71iv392bj7w0hd7lsavav8a5851rjrmn22y4") (f (quote (("use_std") ("use_nightly") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.4.2 (c (n "framed") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cobs") (r "^0.1.4") (k 0)) (d (n "crc16") (r "^0.3.4") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "ssmarshal") (r "^1.0") (k 0)))) (h "0xcgv89j0c1mqmli348xw5s9r0if70928qcign7xcjkq44jf246a") (f (quote (("use_std" "serde/std" "ssmarshal/std") ("use_nightly") ("trace") ("default" "use_std"))))))

(define-public crate-framed-0.4.3 (c (n "framed") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cobs") (r "^0.1.4") (k 0)) (d (n "crc16") (r "^0.3.4") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "ssmarshal") (r "^1.0") (k 0)))) (h "04l6bdn1ggapz3z4c1ydg1zhl3ynz2cy7qkkshf7bs1cm1b9v4sx") (f (quote (("use_std" "serde/std" "ssmarshal/std") ("use_nightly") ("trace") ("default" "use_std"))))))

