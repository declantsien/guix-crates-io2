(define-module (crates-io fr am frame_sequence_parser) #:use-module (crates-io))

(define-public crate-frame_sequence_parser-0.1.0 (c (n "frame_sequence_parser") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "113804i39pjp4m76iq2yxbckardd9v5s1dykci65qs96yj09vysx") (y #t)))

