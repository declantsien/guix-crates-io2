(define-module (crates-io fr am framed_pipe) #:use-module (crates-io))

(define-public crate-framed_pipe-0.1.0 (c (n "framed_pipe") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports" "async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "rt-multi-thread" "macros" "sync"))) (d #t) (k 0)))) (h "0icrzs2ksmqjwm35bhiymi8injvfn7vmlb80mndj1igq2ran9hni")))

