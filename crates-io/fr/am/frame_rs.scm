(define-module (crates-io fr am frame_rs) #:use-module (crates-io))

(define-public crate-frame_rs-0.1.0 (c (n "frame_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "ethers") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hfyhz7jnaxb7rkpdi65grlykl5bh2amfd49sb1x64v2kjg9l9rk")))

(define-public crate-frame_rs-0.1.1 (c (n "frame_rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "ethers") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d16p0f8pfv2wr7kmn03l1c69d5v7f4chmhcgyhd68wm1k1klcmm")))

(define-public crate-frame_rs-0.1.2 (c (n "frame_rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "ethers") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gvghxsnlsj0avsj091xibcswz8i8n72ql1hdrn3csda6s3s65l1")))

