(define-module (crates-io fr am frame_counter) #:use-module (crates-io))

(define-public crate-frame_counter-0.1.0 (c (n "frame_counter") (v "0.1.0") (h "1m4m84j7m75aaglwsgq2g1vg8468zjqdvs259xw1slmd340rcvcb")))

(define-public crate-frame_counter-0.1.1 (c (n "frame_counter") (v "0.1.1") (h "003n1833fzihwz1x0pzqp20qzqmlgmy32y19llz8l98apfpx4200")))

(define-public crate-frame_counter-0.1.2 (c (n "frame_counter") (v "0.1.2") (h "0h78sy57x6ykzpmmk6ny9r853cxpz6wzxrq0k4lgy2k3jz25g2wp")))

