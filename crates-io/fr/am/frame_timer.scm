(define-module (crates-io fr am frame_timer) #:use-module (crates-io))

(define-public crate-frame_timer-0.1.0 (c (n "frame_timer") (v "0.1.0") (h "0b54hap0vbn2i6v4gfynnwrwqs4ppkqrzcs853lxim73jv254k33")))

(define-public crate-frame_timer-0.1.1 (c (n "frame_timer") (v "0.1.1") (h "10pfxw7r2f4xjl7b0ksqikpbj38vmngsaxy53pm07p3nqlg2wcv2")))

(define-public crate-frame_timer-0.1.2 (c (n "frame_timer") (v "0.1.2") (h "1d29x5isnfqvv0w4sbfky4ybj2mhmp5bbhwaqx51ynrr9cd467px")))

