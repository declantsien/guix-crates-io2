(define-module (crates-io fr am frames) #:use-module (crates-io))

(define-public crate-frames-0.0.0 (c (n "frames") (v "0.0.0") (h "0a1avrk9v2k1k5l1lc5nlpack1b9vikni3mmc7szyn7cid13wdcs")))

(define-public crate-frames-0.1.0 (c (n "frames") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0wnq7k36sg097lm43nd3hgqm2zfyrvvfkrwcg0zvh2s53iacdrgp")))

