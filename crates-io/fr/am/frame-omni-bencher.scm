(define-module (crates-io fr am frame-omni-bencher) #:use-module (crates-io))

(define-public crate-frame-omni-bencher-0.0.0 (c (n "frame-omni-bencher") (v "0.0.0") (h "0i5vn58ja2mijz18jimla0gwff4i42bi8gbnnz26z7h9d4w0kanb")))

(define-public crate-frame-omni-bencher-0.1.0 (c (n "frame-omni-bencher") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cumulus-primitives-proof-size-hostfunction") (r "^0.7.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "frame-benchmarking-cli") (r "^37.0.0") (k 0)) (d (n "log") (r "^0.4.21") (k 0)) (d (n "sc-cli") (r "^0.41.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^36.0.0") (d #t) (k 0)) (d (n "sp-statement-store") (r "^15.0.0") (d #t) (k 0)))) (h "0v3qqqz78w8mqrjbdi699bl82inaq0ywzliz5zqlz0ziyv64g90h")))

(define-public crate-frame-omni-bencher-0.2.0 (c (n "frame-omni-bencher") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cumulus-primitives-proof-size-hostfunction") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "frame-benchmarking-cli") (r "^38.0.0") (k 0)) (d (n "log") (r "^0.4.21") (k 0)) (d (n "sc-cli") (r "^0.42.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^37.0.0") (d #t) (k 0)) (d (n "sp-statement-store") (r "^16.0.0") (d #t) (k 0)))) (h "1ip9z0z4ai36wvjjp8aqhm39hp1cq5r8rx68mdrv5vixfxinmphr")))

