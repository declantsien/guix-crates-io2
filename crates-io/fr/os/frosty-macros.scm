(define-module (crates-io fr os frosty-macros) #:use-module (crates-io))

(define-public crate-frosty-macros-0.0.1 (c (n "frosty-macros") (v "0.0.1") (h "1pgwvjm7wvmz5ih1kbqyfy0vlia9m3n1d5bnj3g6zdf9cmim7a3g") (y #t)))

(define-public crate-frosty-macros-0.0.3 (c (n "frosty-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "147i4v3yq0ap18mjr0qwlk6m0qgzwrmj3lilh2bw58pyx9y1yr24") (y #t)))

