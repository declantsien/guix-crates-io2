(define-module (crates-io fr os frosty) #:use-module (crates-io))

(define-public crate-frosty-0.0.1 (c (n "frosty") (v "0.0.1") (h "185cbcwar5hhkr3vk2w1viax0b4qc0imjf5bfqnd235flj11bix5") (y #t)))

(define-public crate-frosty-0.0.2 (c (n "frosty") (v "0.0.2") (h "0d2qgzfc1x8y3mh0hx5ik264qx27hsbrj1iazvq4bm7mpnj3384f") (y #t)))

(define-public crate-frosty-0.0.3 (c (n "frosty") (v "0.0.3") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "flume") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "frosty-macros") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "io-uring") (r "^0.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "os_socketaddr") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "scoped-tls") (r "^1") (d #t) (k 0)) (d (n "socket2") (r "^0.4") (f (quote ("all"))) (d #t) (k 0)))) (h "06ahxark27ah8spfb2bhgd8caqilfja836g9cqdvjgagljcqr460") (f (quote (("utils" "nix") ("sync" "lazy_static" "flume") ("macros" "frosty-macros") ("default" "async-cancel" "bytes" "macros" "utils") ("debug") ("async-cancel")))) (y #t)))

