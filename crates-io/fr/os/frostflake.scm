(define-module (crates-io fr os frostflake) #:use-module (crates-io))

(define-public crate-frostflake-0.1.0 (c (n "frostflake") (v "0.1.0") (h "1s966r55s4ilvz7j02qnj5fsv2fl4knd67gskxiq8yvz86yjgijv")))

(define-public crate-frostflake-0.2.0 (c (n "frostflake") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)))) (h "0ckn2vzq98h73ssgaa8anmagidfh4nw2cqwfbw6i073vs6n1245p")))

(define-public crate-frostflake-0.2.1 (c (n "frostflake") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)))) (h "05584q0ivipsww9wakksvh0pf5ph23g2911a3np8clf5bjjgardl")))

(define-public crate-frostflake-0.3.0 (c (n "frostflake") (v "0.3.0") (d (list (d (n "crossbeam") (r "~0.8") (d #t) (k 0)))) (h "1hsw8ck91x3ccgg4x7iw0kbbmk70126w63d5j0xfn1bhsbiwc1cp")))

(define-public crate-frostflake-0.4.0 (c (n "frostflake") (v "0.4.0") (d (list (d (n "anyhow") (r "~1.0.57") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "~1.18.1") (f (quote ("rt" "sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "~1.18.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0sqg1rh1c07radg3jy73c9k0by8kaz4hc3q2v58pks814qrsrp6p") (f (quote (("default") ("all" "tokio" "std-thread")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:anyhow") ("std-thread" "dep:crossbeam"))))))

