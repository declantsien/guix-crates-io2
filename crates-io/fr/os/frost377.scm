(define-module (crates-io fr os frost377) #:use-module (crates-io))

(define-public crate-frost377-0.1.0 (c (n "frost377") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3") (k 0)) (d (n "ark-poly") (r "^0.3") (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "decaf377") (r "^0.3") (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "192p2i68v588n8ngnp615g72g1x65grzxv85vhcvspzhgpqygbp0") (f (quote (("std" "ark-ff/std" "ark-poly/std") ("parallel" "ark-ff/parallel" "decaf377/parallel" "ark-poly/std") ("default" "std"))))))

(define-public crate-frost377-0.2.0 (c (n "frost377") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4") (k 0)) (d (n "ark-poly") (r "^0.4") (k 0)) (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "decaf377") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0jkb7wh909k9hfhnvn8prgxgkf2phw4rvfbaimny6mibjbfm1jql") (f (quote (("std" "ark-ff/std" "ark-poly/std") ("parallel" "ark-ff/parallel" "decaf377/parallel" "ark-poly/std") ("default" "std"))))))

