(define-module (crates-io fr os frosty_grass) #:use-module (crates-io))

(define-public crate-frosty_grass-0.0.1 (c (n "frosty_grass") (v "0.0.1") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_asset" "bevy_pbr" "bevy_gltf"))) (k 0)) (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_winit" "x11" "ktx2" "zstd" "tonemapping_luts"))) (k 2)) (d (n "bytemuck") (r "^1.14.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "noise") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "060hpjzzgzwm3nvb799whhgmfwq990agy6931faxqaiqvb855l3g")))

