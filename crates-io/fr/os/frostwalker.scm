(define-module (crates-io fr os frostwalker) #:use-module (crates-io))

(define-public crate-frostwalker-0.1.0 (c (n "frostwalker") (v "0.1.0") (h "1wysiy4ys7r755b4zd6080lpsbs0z4pvdanh36rhx3irmji4cysg")))

(define-public crate-frostwalker-0.1.0-a (c (n "frostwalker") (v "0.1.0-a") (h "11wzlbk3fvzp42vg8kjw2aiwq44yfrjpkm4naid3n0jkxf3c49nw")))

(define-public crate-frostwalker-0.1.1 (c (n "frostwalker") (v "0.1.1") (h "0syl8f63y9x4hvb3vmm22mg3g58qni35mjf4v24a1ggj367gbjqp")))

