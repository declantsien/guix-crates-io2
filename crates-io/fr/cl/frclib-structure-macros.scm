(define-module (crates-io fr cl frclib-structure-macros) #:use-module (crates-io))

(define-public crate-frclib-structure-macros-0.1.0 (c (n "frclib-structure-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0k4aaijlqlp6g6ph97psry07d20akql4092iz04jcnj410ijlqcd") (r "1.75.0")))

(define-public crate-frclib-structure-macros-0.1.1 (c (n "frclib-structure-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1cwnc5fwyc17r8nk7nrckk72nsf8qrgy124692zdmjn33032y98l") (r "1.75.0")))

(define-public crate-frclib-structure-macros-0.1.2 (c (n "frclib-structure-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1af0fxnvry97qdq42j2732z2fbvlnh4whrr9ga61if3fqshic3mw") (r "1.75.0")))

