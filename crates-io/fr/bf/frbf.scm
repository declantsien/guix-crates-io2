(define-module (crates-io fr bf frbf) #:use-module (crates-io))

(define-public crate-frbf-0.1.0 (c (n "frbf") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "0rqaqg8pg6hy2nk4gpfji5s3a90xsh4nxgz0hkq71jrkb3y4vryw")))

