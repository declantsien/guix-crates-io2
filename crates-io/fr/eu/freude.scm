(define-module (crates-io fr eu freude) #:use-module (crates-io))

(define-public crate-freude-0.1.1 (c (n "freude") (v "0.1.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)))) (h "03v0m1zhijsd32jrxxc5wa499y3ay80nrqbqqr3drd757zppvbh0")))

(define-public crate-freude-0.1.2 (c (n "freude") (v "0.1.2") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)))) (h "1hrz49gmfg335i3cpb1b5wvg40mmbcrwgvsq9m1q5xh1gqkj6wmx")))

(define-public crate-freude-0.2.0 (c (n "freude") (v "0.2.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.7") (d #t) (k 0)))) (h "1by3mls6plk1fksq8ri372n47pq8hg1gzrkx134nrz6vc8b070ys") (y #t)))

(define-public crate-freude-0.2.1 (c (n "freude") (v "0.2.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.8") (d #t) (k 0)))) (h "0cnabi9b8b9wdvj9970j8cq5dmxl8faxlv17icx6hly73kmk6r8r")))

(define-public crate-freude-0.3.0 (c (n "freude") (v "0.3.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.15") (d #t) (k 2)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0bni89hix1idb5dn80mgff4a1b5195ssyhyjy59a11j20g405s7d")))

(define-public crate-freude-0.3.1 (c (n "freude") (v "0.3.1") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.15") (d #t) (k 2)) (d (n "ndarray") (r "^0.9") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tuple") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0kxb3w0s7izx1gyj4p6jw4fmsg7058qc2dip5wsg5v1mhand21q5")))

(define-public crate-freude-0.4.0 (c (n "freude") (v "0.4.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.15") (d #t) (k 2)) (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tuple") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "0y43wlyk17w4r665vpb9bch9k4vkxpkgbdv5fiijsk15adk15fm8") (f (quote (("nightly_test" "tuple"))))))

(define-public crate-freude-0.5.0 (c (n "freude") (v "0.5.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "blas") (r "^0.17") (d #t) (k 2)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "tuple") (r "^0.3.5") (o #t) (d #t) (k 0)))) (h "17xph0iwdvvckwlc9xdp5pqi76bb2p0cc1kfkh4j7id8dmvbq8q8") (f (quote (("nightly_test" "tuple"))))))

(define-public crate-freude-0.6.0 (c (n "freude") (v "0.6.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "blas-src") (r "^0.2.1") (f (quote ("openblas"))) (d #t) (k 2)) (d (n "cblas") (r "^0.2.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "tuple") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "043bj4r05zr8jhrd83pjz6lp19dhfvd4ydbzz3ayk5z61zj2rims") (f (quote (("nightly_test" "tuple"))))))

(define-public crate-freude-0.7.0 (c (n "freude") (v "0.7.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "blas-src") (r "^0.2.1") (f (quote ("openblas"))) (d #t) (k 2)) (d (n "cblas") (r "^0.2.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "tuple") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0fwmj99pz9zagfmiycxsri6gmz31a0gxm6j6cm988rb7g82r22p2") (f (quote (("nightly_test" "tuple"))))))

