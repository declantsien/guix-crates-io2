(define-module (crates-io fr ey freyskeyd) #:use-module (crates-io))

(define-public crate-freyskeyd-0.1.0 (c (n "freyskeyd") (v "0.1.0") (h "003vn30jx36860k9nscljcm18zck76r7c4ybdcvgm9zw3zbqcs9c")))

(define-public crate-freyskeyd-0.1.1 (c (n "freyskeyd") (v "0.1.1") (d (list (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.7") (d #t) (k 0)))) (h "0c4yxd2j23nzlw41ki6x90wlzgl3jy94i9cjx6mcv1z7la3yfgcs")))

