(define-module (crates-io fr ey freya-native-core-macro) #:use-module (crates-io))

(define-public crate-freya-native-core-macro-0.2.0 (c (n "freya-native-core-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ijkyb332ksrmmr1gh2iv09cza0yzsc7hz636ii5xmvpxsv0vjns")))

