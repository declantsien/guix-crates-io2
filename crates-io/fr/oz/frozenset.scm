(define-module (crates-io fr oz frozenset) #:use-module (crates-io))

(define-public crate-frozenset-0.1.0 (c (n "frozenset") (v "0.1.0") (h "0ksrrl7n29sawi3kdl7yfmrlz01p1mxdh5ymlxah7lc8icmvh99v")))

(define-public crate-frozenset-0.1.1 (c (n "frozenset") (v "0.1.1") (h "12kvq2qqjiizmb8370mp350lwnnhkzw6qf9425xij9k43681vlfb")))

(define-public crate-frozenset-0.2.0 (c (n "frozenset") (v "0.2.0") (h "175b1py8cypcn77qjv0acjc9yck68x10fw3haci91zbmp2iwj5da")))

(define-public crate-frozenset-0.2.1 (c (n "frozenset") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.164") (o #t) (d #t) (k 0)))) (h "0gn312ifqiwkjf3l513qpilnx0hsr5p9wpjy2121y8ymlvdkj848")))

(define-public crate-frozenset-0.2.2 (c (n "frozenset") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.164") (o #t) (d #t) (k 0)))) (h "12ys2xbsy0rwq5mnf9yw6mh8zg8p1dwab8bd0xliwqj6iwr11cg5")))

