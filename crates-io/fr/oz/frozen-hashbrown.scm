(define-module (crates-io fr oz frozen-hashbrown) #:use-module (crates-io))

(define-public crate-frozen-hashbrown-0.1.0 (c (n "frozen-hashbrown") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "03l0p3snv7v2lvbyb7jljdqcakwva4kfwxd7y94jhc4i5f3jyjnp") (r "1.72")))

