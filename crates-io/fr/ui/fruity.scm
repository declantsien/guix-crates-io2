(define-module (crates-io fr ui fruity) #:use-module (crates-io))

(define-public crate-fruity-0.0.0 (c (n "fruity") (v "0.0.0") (h "0j6dmrkqydrhvhwz570rqhx7h8jxn81yn59fj59bap33iviamxdd")))

(define-public crate-fruity-0.1.0 (c (n "fruity") (v "0.1.0") (h "0lpdfnjid6kkcz6py7gvx6fggwn828h2wxfsdnnmf438ianc24vc") (f (quote (("link") ("default" "link"))))))

(define-public crate-fruity-0.2.0 (c (n "fruity") (v "0.2.0") (h "13kqrc8x3ppqvfw79mjnm3ia5fxbz0a0wgvyx8wppwc4fn32jdr7") (f (quote (("objc") ("foundation" "objc") ("core_foundation"))))))

(define-public crate-fruity-0.3.0 (c (n "fruity") (v "0.3.0") (d (list (d (n "malloced") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0zfh9ipsb3jp8sb36s73mjd1q13dm66cvh9sbhy9l6c3npjvn794") (f (quote (("ui_kit" "foundation") ("system_configuration" "core_foundation") ("objc") ("io_kit" "core_foundation") ("foundation" "objc" "core_graphics") ("dispatch") ("default" "malloced") ("core_video" "foundation") ("core_text" "core_foundation") ("core_services" "core_foundation") ("core_image" "foundation") ("core_graphics") ("core_foundation" "objc") ("core_audio") ("core_animation" "foundation") ("cf_network" "core_foundation") ("app_kit" "foundation"))))))

