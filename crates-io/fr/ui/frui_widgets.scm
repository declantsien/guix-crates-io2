(define-module (crates-io fr ui frui_widgets) #:use-module (crates-io))

(define-public crate-frui_widgets-0.0.1 (c (n "frui_widgets") (v "0.0.1") (d (list (d (n "druid-shell") (r "^0.7.0") (d #t) (k 0)) (d (n "frui") (r "^0.0.1") (d #t) (k 0) (p "frui_core")) (d (n "frui_macros") (r "^0.0.1") (d #t) (k 0) (p "frui_macros")) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0nhb2i957qaz9a4kkjwrxlrqfh9vmlaw9qw1285gvpd49p2awwmn") (f (quote (("miri"))))))

