(define-module (crates-io fr ui frui_core) #:use-module (crates-io))

(define-public crate-frui_core-0.0.1 (c (n "frui_core") (v "0.0.1") (d (list (d (n "druid-shell") (r "^0.7.0") (d #t) (k 0)) (d (n "frui_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "1l5rr06vxd366vyfdyabc59azbpjil0nv6p601s2083rfjf6wlgi") (f (quote (("miri"))))))

