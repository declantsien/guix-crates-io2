(define-module (crates-io fr ui frui_macros) #:use-module (crates-io))

(define-public crate-frui_macros-0.0.1 (c (n "frui_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.33") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "1srqc0xl0x9kqv311vall0ikjykkx9xqfskwpzr91bqvlgpxfw7s")))

