(define-module (crates-io fr ui fruity__bbqsrc) #:use-module (crates-io))

(define-public crate-fruity__bbqsrc-0.2.0 (c (n "fruity__bbqsrc") (v "0.2.0") (d (list (d (n "malloced") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1033wqlzlg7hr0dc535xrw4q4yc5w6wd1cq7j2a4j1l69kqdi0qj") (f (quote (("ui_kit" "foundation") ("system_configuration" "core_foundation") ("objc") ("io_kit" "core_foundation") ("foundation" "objc" "core_graphics") ("dispatch") ("default" "malloced") ("core_video" "foundation") ("core_text" "core_foundation") ("core_services" "core_foundation") ("core_image" "foundation") ("core_graphics") ("core_foundation" "objc") ("core_audio") ("core_animation" "foundation") ("cf_network" "core_foundation") ("app_kit" "foundation"))))))

