(define-module (crates-io fr ui frui) #:use-module (crates-io))

(define-public crate-frui-0.0.1 (c (n "frui") (v "0.0.1") (d (list (d (n "frui_core") (r "^0.0.1") (d #t) (k 0)) (d (n "frui_widgets") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1icxzj6dm3r5zsz22qj8f3z3k9vk71hk2s8fd92s56a78kp4h2l2") (f (quote (("miri" "frui_core/miri" "frui_widgets/miri"))))))

