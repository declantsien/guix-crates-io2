(define-module (crates-io fr c4 frc42_hasher) #:use-module (crates-io))

(define-public crate-frc42_hasher-0.1.0 (c (n "frc42_hasher") (v "0.1.0") (d (list (d (n "fvm_sdk") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1nphfk1ab3wzmbb1633vy44b17x2ff7y01d66p4qw2gwwrbp58j8") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-0.2.0 (c (n "frc42_hasher") (v "0.2.0") (d (list (d (n "fvm_sdk") (r "^2.0.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "^2.0.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "150kdbsk5zi2qyv1d0bdlgp8ds9v1pn1n2i9p33ppw4hsrfa6xih") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.0.0 (c (n "frc42_hasher") (v "1.0.0") (d (list (d (n "fvm_sdk") (r "^2.0.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "^2.0.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0xrwgf8ik08q3as5dcimjym21iwyml7xv3myi28irlkssmbk3m0k") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.1.0 (c (n "frc42_hasher") (v "1.1.0") (d (list (d (n "fvm_sdk") (r "=2.0.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0yvc22in6hfklcy1ic5bqi9nnyqbqwnrcrjgcq4nkwxilii49khc") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.2.0 (c (n "frc42_hasher") (v "1.2.0") (d (list (d (n "fvm_sdk") (r "=2.1.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1rmrdg3arvj9mjz0lqpfpg5gg7ibvn03dx0hy1diym2bscl5nxqq") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.3.0 (c (n "frc42_hasher") (v "1.3.0") (d (list (d (n "fvm_sdk") (r "=2.2.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1dwmrv3vzgfzv3ph7dnv2m079csswk4gi7lhsifd36kylrh2xx8c") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.3.1 (c (n "frc42_hasher") (v "1.3.1") (d (list (d (n "fvm_sdk") (r "~3.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09lhkd8rd5gcvg6wwn34bi6a00z7ca2g0lx2wfkdyl98ymgk94db") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.4.0 (c (n "frc42_hasher") (v "1.4.0") (d (list (d (n "fvm_sdk") (r "~3.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nglj0qx4yyhjhm5pck6fgzjc2fns4xsd57zn48nslr863xggjnn") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.5.0 (c (n "frc42_hasher") (v "1.5.0") (d (list (d (n "fvm_sdk") (r "~3.2") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1l1mqwspzh5l7sc1n4qlcvbkqqsavs619cd8df83jz65pp7gf75d") (f (quote (("no_sdk")))) (s 2) (e (quote (("default" "dep:fvm_sdk"))))))

(define-public crate-frc42_hasher-1.6.0 (c (n "frc42_hasher") (v "1.6.0") (d (list (d (n "fvm_sdk") (r "~3.3.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~3.4.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0l6kjh39vl2xgwq8y7kgradbg1glwnnib7cg2f43b2wm3gzanbgp") (f (quote (("default" "use_sdk")))) (s 2) (e (quote (("use_sdk" "dep:fvm_sdk" "dep:fvm_shared"))))))

(define-public crate-frc42_hasher-2.0.0 (c (n "frc42_hasher") (v "2.0.0") (d (list (d (n "fvm_sdk") (r "~3.3.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~3.6.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0iw25agxia1jakyllq13lq745xca30mx9z5dnvn9r4kqvf52b1qh") (f (quote (("default" "use_sdk")))) (s 2) (e (quote (("use_sdk" "dep:fvm_sdk" "dep:fvm_shared"))))))

(define-public crate-frc42_hasher-3.0.0-alpha.1 (c (n "frc42_hasher") (v "3.0.0-alpha.1") (d (list (d (n "fvm_sdk") (r "~4.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~4.0.0-alpha.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09m9ys6qav3skvn3814wncm0s20kk0nxwx8wg4sxbkg1x7y7wwhr") (f (quote (("default" "use_sdk")))) (s 2) (e (quote (("use_sdk" "dep:fvm_sdk" "dep:fvm_shared"))))))

(define-public crate-frc42_hasher-3.0.0 (c (n "frc42_hasher") (v "3.0.0") (d (list (d (n "fvm_sdk") (r "~4.0") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~4.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "192qny5m5j6v2a5924zrza2g6y8jw1mldc0pzk7833qh2ir5x8q8") (f (quote (("default" "use_sdk")))) (s 2) (e (quote (("use_sdk" "dep:fvm_sdk" "dep:fvm_shared"))))))

(define-public crate-frc42_hasher-4.0.0 (c (n "frc42_hasher") (v "4.0.0") (d (list (d (n "fvm_sdk") (r "~4.1.1") (o #t) (d #t) (k 0)) (d (n "fvm_shared") (r "~4.1.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "17a7hysphz1dqiyl39ldmcajn6pdw7b930zf1xfhzn9n1nsqpxb3") (f (quote (("default" "use_sdk")))) (s 2) (e (quote (("use_sdk" "dep:fvm_sdk" "dep:fvm_shared"))))))

