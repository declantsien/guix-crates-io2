(define-module (crates-io fr is frisbee) #:use-module (crates-io))

(define-public crate-frisbee-0.2.0 (c (n "frisbee") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "059igwyhr64pwb0lnshi3bqb6qaxb8bnnd005s73dxabry3r3inn")))

(define-public crate-frisbee-0.3.0 (c (n "frisbee") (v "0.3.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)))) (h "0grwdv9lav455m4hla0wp41z4vwhp3q7s8ygczwrq23mn7gq2k4f")))

