(define-module (crates-io fr is frisk) #:use-module (crates-io))

(define-public crate-frisk-0.1.0 (c (n "frisk") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0aif5zsm7x8vw466bk87ck1hbl0psz4gvwkfhb0s15kgr0zgvfqr")))

(define-public crate-frisk-0.1.1 (c (n "frisk") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "17czkm53m42d5isxkdayb5bjm21dx31ym1848jxbp7j5iawcsyqd")))

