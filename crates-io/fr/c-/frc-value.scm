(define-module (crates-io fr c- frc-value) #:use-module (crates-io))

(define-public crate-frc-value-0.1.0 (c (n "frc-value") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1lc8ivdmpckwx0mq5ki7s7zmx023bj11mbpvlwpv2imdryblzmdp")))

(define-public crate-frc-value-0.1.1 (c (n "frc-value") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0gc6v4c2pyzfrzhmfi7qhbg82yf6s4ng591pgadhy51p743k1zwm")))

(define-public crate-frc-value-0.1.2 (c (n "frc-value") (v "0.1.2") (d (list (d (n "rmpv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0di4037rwbrf6cssrv8vfz98dja6lah60zz7y1grhglhydjncqpf") (f (quote (("rmpv-casting" "rmpv"))))))

(define-public crate-frc-value-0.1.3 (c (n "frc-value") (v "0.1.3") (d (list (d (n "rmpv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "18kbci9s53zjx1flqc9iiaw2rab3dk511glsrkkhkdfbix2vrs39") (f (quote (("rmpv-casting" "rmpv"))))))

(define-public crate-frc-value-0.1.4 (c (n "frc-value") (v "0.1.4") (d (list (d (n "bytes") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "protobuf") (r "^3.3.0") (f (quote ("bytes" "with-bytes"))) (d #t) (k 0)) (d (n "rmpv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "variantly") (r "^0.3.0") (d #t) (k 0)))) (h "1nwl0xqhjhqwj27cl70gyy4k62abfpng85psxwqzrh230jnyj7fz") (f (quote (("rmpv-casting" "rmpv"))))))

