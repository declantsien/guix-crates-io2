(define-module (crates-io fr b_ frb_plugin_tool) #:use-module (crates-io))

(define-public crate-frb_plugin_tool-0.1.0 (c (n "frb_plugin_tool") (v "0.1.0") (d (list (d (n "duct") (r "^0.13.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tera") (r "1.*") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "06lb24bbgxlxzxvgy7q9i7dgx1g888axhjl3xb2jj2wwnk0199dx")))

(define-public crate-frb_plugin_tool-0.1.1 (c (n "frb_plugin_tool") (v "0.1.1") (d (list (d (n "duct") (r "^0.13.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tera") (r "1.*") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0747c4kdn5r7wc61grl4d4dhziwdkbchkdrf43m9i6f58phaq2vq")))

(define-public crate-frb_plugin_tool-0.1.2 (c (n "frb_plugin_tool") (v "0.1.2") (d (list (d (n "duct") (r "^0.13.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.26") (d #t) (k 0)) (d (n "tera") (r "1.*") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rx28lwv6797nn75xwp833ca66lwmdyigy2l2skclxr7x0si0d0y")))

