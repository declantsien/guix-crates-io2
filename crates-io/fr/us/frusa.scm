(define-module (crates-io fr us frusa) #:use-module (crates-io))

(define-public crate-frusa-0.1.0 (c (n "frusa") (v "0.1.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1jcfdxgcww0a3j5zx7h426nylgjbdpb7yb71d1h542wzi8qpk8ll") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-frusa-0.1.1 (c (n "frusa") (v "0.1.1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04dlrdkxfsh6ly1vrmhhkan8bbrrzn61m787cy1rk9ih1kqq11nz") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-frusa-0.1.2 (c (n "frusa") (v "0.1.2") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (d #t) (k 2)) (d (n "talc") (r "^4.2.0") (d #t) (k 2)))) (h "188hil80h0xficirky8hpwgi35wk8rdvqdl0zpvhl5fslq0291r0") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

