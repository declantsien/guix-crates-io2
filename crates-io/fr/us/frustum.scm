(define-module (crates-io fr us frustum) #:use-module (crates-io))

(define-public crate-frustum-0.1.0 (c (n "frustum") (v "0.1.0") (d (list (d (n "euclid") (r "^0.20.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k462m6i8hyn88c9s8ivpmqywz7jwrxdv4yw0hgpdck0v3jv29l6")))

(define-public crate-frustum-0.1.1 (c (n "frustum") (v "0.1.1") (d (list (d (n "euclid") (r "^0.20.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (k 0)))) (h "19xfmcspnwvkp5821mfxj5cmlmyq6h66viaq15rbhikjzbi7m6dn")))

(define-public crate-frustum-0.2.1 (c (n "frustum") (v "0.2.1") (d (list (d (n "euclid") (r "^0.20.7") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "palette") (r "^0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gz3b7qrjg70i8z06w9kaynklvqlpp2fkfw2af9a2q8ci6dif3j8") (f (quote (("serialization" "serde" "euclid/serde"))))))

