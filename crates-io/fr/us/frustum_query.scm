(define-module (crates-io fr us frustum_query) #:use-module (crates-io))

(define-public crate-frustum_query-0.1.0 (c (n "frustum_query") (v "0.1.0") (d (list (d (n "rayon") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "074zwdn2s5b4f8m6nvp1l0xjhr3ij7xd5g83wswqmnib6i512znn") (f (quote (("multithreaded_rayon" "rayon" "time") ("default"))))))

(define-public crate-frustum_query-0.1.1 (c (n "frustum_query") (v "0.1.1") (d (list (d (n "rayon") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "0mzym5yx070ybah6zj6mcmzcwnhjyz6pbw06ix6sharrg4z9r472") (f (quote (("multithreaded_rayon" "rayon" "time") ("default"))))))

(define-public crate-frustum_query-0.1.2 (c (n "frustum_query") (v "0.1.2") (d (list (d (n "rayon") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.35") (o #t) (d #t) (k 0)))) (h "01asxw39rwmb91kq9x2v41nvini7zpzl51w8gm9b49pdmck1qxz1") (f (quote (("multithreaded_rayon" "rayon" "time") ("default"))))))

