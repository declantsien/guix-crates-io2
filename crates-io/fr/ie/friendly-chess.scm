(define-module (crates-io fr ie friendly-chess) #:use-module (crates-io))

(define-public crate-friendly-chess-0.1.0 (c (n "friendly-chess") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0r0xg8056jgn4k1b75yrpyvahy75dm2a0vpd2snxlcwsrvl6dz4a")))

(define-public crate-friendly-chess-0.2.0 (c (n "friendly-chess") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "04ll9570s9rqr2lfx8swp19nncmpxrlvdx4qxp7zs8hn019gz7lg")))

(define-public crate-friendly-chess-0.3.0 (c (n "friendly-chess") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "02lhrxwmsqajlp79f0gbsdcvvx720izcpxk75dzzk92v19ifzwqc")))

(define-public crate-friendly-chess-0.4.0 (c (n "friendly-chess") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1vli3xcx4xk8xr29jwfli9b0jmpnf17w5wjnnrlr1ym4jrrk6c3f")))

(define-public crate-friendly-chess-0.5.0 (c (n "friendly-chess") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "05ial3099zp65qlis3lqarn48n760nxj9fdra8daqxv5ypq39ric")))

(define-public crate-friendly-chess-0.6.0 (c (n "friendly-chess") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0a3zcwq11dh0riw88c9zykjppccyrcaxyx202ixs8kvjbb3vksa3")))

