(define-module (crates-io fr ie friendlyid) #:use-module (crates-io))

(define-public crate-friendlyid-0.2.0 (c (n "friendlyid") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "friendly_id") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "190mniagwsngazhsjy4n89vzakk44q4952iyzlq5nkb5rn724lqk")))

(define-public crate-friendlyid-0.3.0 (c (n "friendlyid") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "friendly_id") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nhy59q2w82sgq7dmzwyls20d9lynm3wg4sbz14f3h066mjyqxkp")))

