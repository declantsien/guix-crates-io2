(define-module (crates-io fr ie friedrich) #:use-module (crates-io))

(define-public crate-friedrich-0.1.0 (c (n "friedrich") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0h3hcnbfmbjngv7fzf3ncc0qdyk5hi4d67kn19d0dih26vnshfm0")))

(define-public crate-friedrich-0.1.1 (c (n "friedrich") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0dm96inw4kwbikv5vzrsr99pnzsglb8kik8nxhhpwxd9ziw523y1")))

(define-public crate-friedrich-0.2.0 (c (n "friedrich") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1r7r02zjfwcf2f3ab9b21yif260bz1gwpql7q4aa232b9d8v4bkk")))

(define-public crate-friedrich-0.2.1 (c (n "friedrich") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "112s7lz80qg5rqxalxykh8jfw960vrs3qhwvir856dpzxn39h3wk")))

(define-public crate-friedrich-0.2.2 (c (n "friedrich") (v "0.2.2") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0ac0nsf46npgycb39fnnwcxp5i421lpshkdpjhzflhlhr893p42y")))

(define-public crate-friedrich-0.3.0 (c (n "friedrich") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0nwq3llgcwkb425175nh9i1rxmblcjhl971kv6n97nbkd0vk3gx7")))

(define-public crate-friedrich-0.3.1 (c (n "friedrich") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1fwv4mdhn4h9lb0ci1c1zcfy4ai0f3qn5y5klvmpka8g4sapy7a9") (f (quote (("friedrich_ndarray" "ndarray"))))))

(define-public crate-friedrich-0.3.2 (c (n "friedrich") (v "0.3.2") (d (list (d (n "nalgebra") (r "^0.22.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "1schf12qh6nhp3cnqchl5abs505lmzsckwwk9piw28v3h7wrybcg") (f (quote (("friedrich_ndarray" "ndarray"))))))

(define-public crate-friedrich-0.3.3 (c (n "friedrich") (v "0.3.3") (d (list (d (n "nalgebra") (r "^0.23.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "1scpc52i0rvcgfnx24x18wvh2q5yjrk3a92rvhrqcjxz8q1r53kq") (f (quote (("friedrich_ndarray" "ndarray"))))))

(define-public crate-friedrich-0.4.0 (c (n "friedrich") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1f7ls57kh75wk1cr5ciix1i1inhwij8naqgr29z1v8182x7lvvbf") (f (quote (("friedrich_serde" "serde" "nalgebra/serde-serialize") ("friedrich_ndarray" "ndarray") ("default" "friedrich_serde"))))))

(define-public crate-friedrich-0.4.1 (c (n "friedrich") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.29") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18p2yxbcf2bf6wn1ml5kcqipb924mnvrl7a1gn5m59jdb6kgg8s9") (f (quote (("friedrich_serde" "serde" "nalgebra/serde-serialize") ("friedrich_ndarray" "ndarray") ("default" "friedrich_serde"))))))

(define-public crate-friedrich-0.5.0 (c (n "friedrich") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d7v3923yviaayazprgla83hdgs3c02q183siym2navn0kgyb62h") (f (quote (("friedrich_serde" "serde" "nalgebra/serde-serialize") ("friedrich_ndarray" "ndarray") ("default" "friedrich_serde"))))))

