(define-module (crates-io fr ie friendly-zoo) #:use-module (crates-io))

(define-public crate-friendly-zoo-0.1.0 (c (n "friendly-zoo") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0x9yd8a2kjbzbfr91d0bm3bid70j1vfg3ys5cl4dmbic1srk3yg8")))

(define-public crate-friendly-zoo-0.1.1 (c (n "friendly-zoo") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1frsh6b0csicrg8f35sni2drkkh50im6a1wiyvda1y39s8wcyyw8")))

(define-public crate-friendly-zoo-0.2.0 (c (n "friendly-zoo") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0q0m2v8ak8dggip4mx0865xipg3sczpjh482x6gcwikdq8pg7xgb")))

(define-public crate-friendly-zoo-0.2.1 (c (n "friendly-zoo") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xnfn0g8gsi8chqsykb66nib55lpwb558lxsbimmnq5ldk62ql76")))

(define-public crate-friendly-zoo-0.3.0 (c (n "friendly-zoo") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0grcz6mla51z9cf9rhcis9j01acd2jyxjwj1pvm6y1hv94s1jn1m")))

(define-public crate-friendly-zoo-1.0.0 (c (n "friendly-zoo") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x5aq0vrlp7606prm7fdxwz0rkg5jldify9ab7f8ln4qn0n89rkn")))

(define-public crate-friendly-zoo-1.1.0 (c (n "friendly-zoo") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16g6z4q8kgyvq7a666jczsa6qj6b5vjsxcfpljcnd1sg1gqpjfx8")))

