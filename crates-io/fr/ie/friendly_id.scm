(define-module (crates-io fr ie friendly_id) #:use-module (crates-io))

(define-public crate-friendly_id-0.1.0 (c (n "friendly_id") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pc3nn7fvyqilmwl6lh0k3scan3djibjpz741i61zyda98mb873c")))

(define-public crate-friendly_id-0.1.1 (c (n "friendly_id") (v "0.1.1") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "17nb5iccgb1bl7css60b6fhhh8pmdhqii1kyrbnz4ni2a76i429m")))

(define-public crate-friendly_id-0.1.2 (c (n "friendly_id") (v "0.1.2") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "125xrjhysl4b8z0bbn7sh6k8rzpz5giwy83f4jsrl6xxhm56b1jv")))

(define-public crate-friendly_id-0.2.0 (c (n "friendly_id") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0k54h0g0x87ky3061p4yfmbllaqx355v2gqx6g6y3dj96v3wwn12")))

(define-public crate-friendly_id-0.3.0 (c (n "friendly_id") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "10fs0cqjcqcfgzxbsghfizp6s2g34j9l6fsklayb1vj30kkwqmab")))

