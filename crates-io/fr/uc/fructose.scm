(define-module (crates-io fr uc fructose) #:use-module (crates-io))

(define-public crate-fructose-0.1.0 (c (n "fructose") (v "0.1.0") (h "0v3gc6slhrnlqkl9hwpi4xvwz1mn0nrs7xm8b0msbf3b3c1g3pj7")))

(define-public crate-fructose-0.2.0 (c (n "fructose") (v "0.2.0") (h "139m3ish3f2rhdk6bjcqj36289z3hxfq4qfyqkywkzgb3zl21bwm")))

(define-public crate-fructose-0.3.0 (c (n "fructose") (v "0.3.0") (h "13lax1z98d102fqvcasjh2dcwjngp1idxgdnjk2hs0bdi2ykfq7s")))

(define-public crate-fructose-0.3.1 (c (n "fructose") (v "0.3.1") (h "0x1vgqvrjmr5xclc9g0rrfllfg73065rmdra2hf2lk652l4hmnhl")))

(define-public crate-fructose-0.3.2 (c (n "fructose") (v "0.3.2") (h "1pcnnj2i15hg2w3krs6x2xd7d2x41qz3wch852r2qfjdd3v4bm12") (f (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3.3 (c (n "fructose") (v "0.3.3") (h "115snz63x7fwzgzh55f0zg6v7f68sp2hac2h4mahkx7dwd2wlqqj") (f (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3.4 (c (n "fructose") (v "0.3.4") (h "05zs0cg5kkbirmafwcrgwpzp2jlbhdhz3f7mgdjfq0a77wpjm5z8") (f (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3.5 (c (n "fructose") (v "0.3.5") (h "1b1j7r8f5araqxr4fqs2j8a6arky9bqg4xgfv1j0gc2pjv9lfw98") (f (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3.6 (c (n "fructose") (v "0.3.6") (h "0h5ylgavng3cn89v2a34a0q7h3dl3alz71z5dkag5kgss3dx3f1v") (f (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3.7 (c (n "fructose") (v "0.3.7") (h "1x5aqm6a68n7j6b1clcjqn5s3h4zxpxfh85cq6a0bbn61qkagl5i") (f (quote (("specifics") ("full" "specifics") ("default" "full"))))))

(define-public crate-fructose-0.3.8 (c (n "fructose") (v "0.3.8") (h "0cpks7mg7x3crzyc3s5dvdcg74h54xkbg26llmpb8kbbf4nrjjfw") (f (quote (("specifics") ("full" "specifics") ("default" "full"))))))

(define-public crate-fructose-0.3.9 (c (n "fructose") (v "0.3.9") (h "1cm14qqzrf0y0khl7zb6f572h121pii3alwcsb0kqdzalqsnf62c") (f (quote (("specifics") ("full" "specifics") ("default" "full"))))))

