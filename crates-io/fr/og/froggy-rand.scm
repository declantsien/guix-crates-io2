(define-module (crates-io fr og froggy-rand) #:use-module (crates-io))

(define-public crate-froggy-rand-0.1.0 (c (n "froggy-rand") (v "0.1.0") (d (list (d (n "deterministic-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)))) (h "1yfsqrx88by8lp37bi38knbr9h2jrgdcgkwsdr2xf0y13i3kvavm")))

(define-public crate-froggy-rand-0.2.0 (c (n "froggy-rand") (v "0.2.0") (d (list (d (n "deterministic-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)))) (h "022m6v2dnzpcl37fqa084mnjzf33lgfv6d73vx1rby8h6sps26ya")))

(define-public crate-froggy-rand-0.2.1 (c (n "froggy-rand") (v "0.2.1") (d (list (d (n "deterministic-hash") (r "^1.0.1") (d #t) (k 0)) (d (n "hashers") (r "^1.0.1") (d #t) (k 0)))) (h "10rjh1kzwv1xc8xbzfhvn6np5h5vik5f2qlxbg29fwfpdfp7k81m")))

