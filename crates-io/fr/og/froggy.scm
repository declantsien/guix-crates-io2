(define-module (crates-io fr og froggy) #:use-module (crates-io))

(define-public crate-froggy-0.1.0 (c (n "froggy") (v "0.1.0") (h "1bs1x42gz8lfccmlx786kfbf2f0czjvyiws8dyx4b544fzmq8byw")))

(define-public crate-froggy-0.1.1 (c (n "froggy") (v "0.1.1") (h "17fm7qq6mq22f8lgqbnmwkhfqi8pxhnakan8ckfc6d63jcdjbxqq")))

(define-public crate-froggy-0.2.0 (c (n "froggy") (v "0.2.0") (d (list (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1950p5apg7xnxj0k0d084n5ml1hbv8vd482l2g0qvmkpnmmkk7dy")))

(define-public crate-froggy-0.2.1 (c (n "froggy") (v "0.2.1") (h "0zh3ysx36f5npjcz3cryap98cvarp1h3jw9bcasvqjij9syabivi")))

(define-public crate-froggy-0.3.0 (c (n "froggy") (v "0.3.0") (h "154a3krxycw202h8cyym3lygivl93gxkhhw0cjjsajjra4rhzrzr")))

(define-public crate-froggy-0.4.0 (c (n "froggy") (v "0.4.0") (d (list (d (n "spin") (r "^0.4") (k 0)))) (h "1yjr6hn5467l917ia1g6wi46l1if8dfg5914ln26zzzqybnbyif2")))

(define-public crate-froggy-0.4.1 (c (n "froggy") (v "0.4.1") (d (list (d (n "spin") (r "^0.4") (k 0)))) (h "159jk8x4ki666hz8pw8cngmj5rd33r2r2ar8467zmdwczk324y9m")))

(define-public crate-froggy-0.4.4 (c (n "froggy") (v "0.4.4") (d (list (d (n "spin") (r "^0.4") (k 0)))) (h "0nrsinkyi19ikb794j5rhikhnvl10vn12nxzzaa10mxcq224vnbh")))

(define-public crate-froggy-0.4.2 (c (n "froggy") (v "0.4.2") (d (list (d (n "spin") (r "^0.4") (k 0)))) (h "1h1k7a4px6q5hfc8gdkfcjfkdv9l8nqxcmx48gcrc6r5f9k1d5qs")))

