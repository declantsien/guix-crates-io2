(define-module (crates-io fr od frodobuf-derive) #:use-module (crates-io))

(define-public crate-frodobuf-derive-0.1.0 (c (n "frodobuf-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03pvy677i7vjqjk9cqp0xgv1hc738ylr99m7ajjvcz6n81hr3ffq")))

