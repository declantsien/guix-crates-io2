(define-module (crates-io fr od frodobuf-schema) #:use-module (crates-io))

(define-public crate-frodobuf-schema-0.1.0 (c (n "frodobuf-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lsykw1as7hvbkpjc0ar97ik7q1n4j9rpfjbvhyzsgvbm3mdpxcf")))

(define-public crate-frodobuf-schema-0.1.1 (c (n "frodobuf-schema") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ahmy2qgzpvm0c4sq1qg844bv1xd8mf2gankwn8qz9npvn6dnx5d")))

