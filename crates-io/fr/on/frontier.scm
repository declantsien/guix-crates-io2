(define-module (crates-io fr on frontier) #:use-module (crates-io))

(define-public crate-frontier-0.0.0 (c (n "frontier") (v "0.0.0") (h "1gh68dgc95kvlzx5dily2ljigc4dg40jav9cg9pjc2ggply9hn2q")))

(define-public crate-frontier-0.0.1 (c (n "frontier") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("phf_macros" "macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "18s5a2a4dsky054lxcv1z1c6zc3mjwf7bsmfnjy8zkwwbnd2q7ng")))

(define-public crate-frontier-0.0.2 (c (n "frontier") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("phf_macros" "macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "059gzaqw57sqabcdgqz1nxpafssjr34izraxd128xg99p0v6k73w")))

(define-public crate-frontier-0.0.3 (c (n "frontier") (v "0.0.3") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("phf_macros" "macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "01fvd7pfmz1rbvpqjl3fg4xl1i6jcsb3fbgfjc435lzr10ika48b")))

(define-public crate-frontier-0.0.4 (c (n "frontier") (v "0.0.4") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("phf_macros" "macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "18wsnmn6pwlh6rcd8pk1cwpdxnlg0rawklg7arlpb8qvvmb26jy2")))

