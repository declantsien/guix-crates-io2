(define-module (crates-io fr on frontend-environment) #:use-module (crates-io))

(define-public crate-frontend-environment-0.1.0 (c (n "frontend-environment") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.18") (f (quote ("headers"))) (o #t) (d #t) (k 0)) (d (n "http-body") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "lol_html") (r "^1.0.1") (d #t) (k 0)) (d (n "tower-http") (r "^0.4.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)))) (h "08dmydlr0pbpf8xm096r5zslzh62zg13zzs8d2xyf7cdzxgbkgfp") (f (quote (("default" "axum")))) (s 2) (e (quote (("axum" "dep:axum" "dep:http-body" "dep:tower-http"))))))

