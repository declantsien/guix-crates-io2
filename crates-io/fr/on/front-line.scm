(define-module (crates-io fr on front-line) #:use-module (crates-io))

(define-public crate-front-line-0.1.0 (c (n "front-line") (v "0.1.0") (d (list (d (n "front-line-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "front-line-router") (r "^0.1.0") (d #t) (k 0)))) (h "1ih5gxwaa545vl2hfdc95y36iaqh0q6px9jlgp9zvn1z0rjmyhl5")))

(define-public crate-front-line-0.2.0 (c (n "front-line") (v "0.2.0") (d (list (d (n "front-line-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "front-line-router") (r "^0.2.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0sm7nx452iazrwpkq9x27r4d3kk9mwpvhrzc3z61pllb8szjq48q")))

