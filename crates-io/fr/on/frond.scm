(define-module (crates-io fr on frond) #:use-module (crates-io))

(define-public crate-frond-0.1.0 (c (n "frond") (v "0.1.0") (h "0cbrjz9wjwyc7751b8czf9b345n2n6wih1id63nm2r5d4y31inpm")))

(define-public crate-frond-0.1.1 (c (n "frond") (v "0.1.1") (h "1vcw6960zsyxkn8yzwc62y3x5l6m4cl077f4lcvnv7gakzchhvz9")))

