(define-module (crates-io fr on front-line-derive) #:use-module (crates-io))

(define-public crate-front-line-derive-0.1.0 (c (n "front-line-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1xwhdw2icp87a93wdm03s5rxvglp7s3cvgiijvhcw733mzbbnz0g")))

(define-public crate-front-line-derive-0.2.0 (c (n "front-line-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1ydws12yrv2jzckhk064irayg5zr56ddaj55nhia3crq70xzs7vz")))

