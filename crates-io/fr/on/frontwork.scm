(define-module (crates-io fr on frontwork) #:use-module (crates-io))

(define-public crate-frontwork-0.1.0 (c (n "frontwork") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rsass") (r "^0.28.8") (d #t) (k 0)) (d (n "rusync") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "shell") (r "^0.3.2") (d #t) (k 0)) (d (n "time") (r "^0.3.36") (d #t) (k 0)) (d (n "zip") (r "^1.3.0") (d #t) (k 0)) (d (n "zip-extensions") (r "^0.6") (d #t) (k 0)))) (h "0rwkyj78ahp5p6s8kw4rrkhkmqqfk2jm6b6ljbxc7j8q29lb90jx")))

