(define-module (crates-io fr on frontmatter) #:use-module (crates-io))

(define-public crate-frontmatter-0.1.0 (c (n "frontmatter") (v "0.1.0") (d (list (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0crnnglh402x1cim82d27i9px0zbnmbcbf0f5p7081x48m6drp8p")))

(define-public crate-frontmatter-0.1.1 (c (n "frontmatter") (v "0.1.1") (d (list (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0p1qpgsjivsf2n80bfinjaicrbcf32rzpw39x9zba852hp5kcbna")))

(define-public crate-frontmatter-0.2.0 (c (n "frontmatter") (v "0.2.0") (d (list (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "050z4kalv8g12v0v8vr85g9888xw5ml8gpamnhn1vgv7kmn41czz")))

(define-public crate-frontmatter-0.3.0 (c (n "frontmatter") (v "0.3.0") (d (list (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "0ldj6hvj9vp5jxzd93jz4h0q8r1q9r02zvdq41267x7g2vhklnr5")))

(define-public crate-frontmatter-0.3.1 (c (n "frontmatter") (v "0.3.1") (d (list (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "00sh7jjffgmiq05cqk5mfymp701lsvdzl338nj5ph8b1mylha9lx")))

(define-public crate-frontmatter-0.4.0 (c (n "frontmatter") (v "0.4.0") (d (list (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1yl4djwzzyln2l4zzx3izzmsivyp3zy8mp1vz9b6v763dyk9kiwg")))

