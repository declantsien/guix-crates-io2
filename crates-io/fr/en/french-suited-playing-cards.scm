(define-module (crates-io fr en french-suited-playing-cards) #:use-module (crates-io))

(define-public crate-french-suited-playing-cards-0.1.0 (c (n "french-suited-playing-cards") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b36bhhqifgli35939pdxxi7nzl6w2cwwl5gd3mqbyzw9676ghnv")))

(define-public crate-french-suited-playing-cards-0.1.1 (c (n "french-suited-playing-cards") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1iq8sk83cfq35m7vr50f2ss5gas21nyacdnblcsz07qkn5wqj5bx")))

(define-public crate-french-suited-playing-cards-0.2.0 (c (n "french-suited-playing-cards") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1k69c82bsrilp0qa75c0n967xy0wp5h2zwp8kshkgw4zvs517q8q")))

(define-public crate-french-suited-playing-cards-0.3.0 (c (n "french-suited-playing-cards") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qgp300f53v1jr7kyfi7rg86rsxx1iw08j23zi5mvn0c8lrj5d4z")))

