(define-module (crates-io fr en frender-macros) #:use-module (crates-io))

(define-public crate-frender-macros-1.0.0-alpha.2 (c (n "frender-macros") (v "1.0.0-alpha.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wv2shbpad1pqnpcx24nbgfkmc6ikfnk3q0p32b25rz8zhwqw1kg")))

(define-public crate-frender-macros-1.0.0-alpha.3 (c (n "frender-macros") (v "1.0.0-alpha.3") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13cymjmv9ai3zgr76jksnwp5cjn85qxmm9q3a9kdqqavwj06rc37")))

(define-public crate-frender-macros-1.0.0-alpha.4 (c (n "frender-macros") (v "1.0.0-alpha.4") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0aasq73rqjlkggl88f2i4apl80i0xxgfki94ca34sjjmlmzj38hh")))

(define-public crate-frender-macros-1.0.0-alpha.5 (c (n "frender-macros") (v "1.0.0-alpha.5") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "15sxm7azgs8vk7kbgmi83wgihrd70vkkyw972gxz4x0zk7s9p23k")))

(define-public crate-frender-macros-1.0.0-alpha.6 (c (n "frender-macros") (v "1.0.0-alpha.6") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1dg8wnc7bry11gmxx1h2aj0ylcydvqidm9w3jp3rink09za99jc9")))

(define-public crate-frender-macros-1.0.0-alpha.7 (c (n "frender-macros") (v "1.0.0-alpha.7") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0w3fj8nggcymkiygs32nfi32q3s529ajp8sd18gdzhnhyin3illj")))

(define-public crate-frender-macros-1.0.0-alpha.8 (c (n "frender-macros") (v "1.0.0-alpha.8") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0dcaxvid06ii5yvxk053z2w8m2h9aik70zbiaf6l3c9274wf7nkk")))

