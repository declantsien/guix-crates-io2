(define-module (crates-io fr en french-numbers) #:use-module (crates-io))

(define-public crate-french-numbers-0.1.0 (c (n "french-numbers") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0gs4wnyxpga6kvb2qb4b071x087cxm2v82x8vr83s7snp30pvhf3")))

(define-public crate-french-numbers-0.1.1 (c (n "french-numbers") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1j3xrg745s2fn9wfh5j1527r7rjpm8ziadqk2q707m5rjz1848zr")))

(define-public crate-french-numbers-0.1.2 (c (n "french-numbers") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1iar8aldhsrjhrddyk96wvixj2v4aqsajb0yfz6pik0l1wv2iswk")))

(define-public crate-french-numbers-0.1.3 (c (n "french-numbers") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0i2w58iaq9sa5pk1n2ylc0s7gb7djnnq76z6lhaqn8ac03hls0wi")))

(define-public crate-french-numbers-0.1.4 (c (n "french-numbers") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1zc2zbnxrh1zrs9g4xz0srdgvxq553hfj4xr6wyqdby1qxqc4sna")))

(define-public crate-french-numbers-0.1.5 (c (n "french-numbers") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0012nmmv2bya9sz5p6bvj0n5p7zyqhwpirr4s01c8wsm536lpdhr")))

(define-public crate-french-numbers-0.1.6 (c (n "french-numbers") (v "0.1.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "112z4c8i455qrckxkjvdw0p5f7sdhnmbkw8jkl8l33yzxvy63yhy")))

(define-public crate-french-numbers-0.1.7 (c (n "french-numbers") (v "0.1.7") (d (list (d (n "clap") (r "^2.24") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0jpwp35qqmh9sdfkfck1mfd7k5rg7dm381fzrxrh7r3c7faqbjr2")))

(define-public crate-french-numbers-0.1.8 (c (n "french-numbers") (v "0.1.8") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "10q6s395y4065zvb4zshq39d696zhbys5gyvva3h565vlydz726w")))

(define-public crate-french-numbers-0.1.9 (c (n "french-numbers") (v "0.1.9") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zzca2qwipxihr87sxvz4lcp7747m1w7hi3ladb47247zmzxnm7x")))

(define-public crate-french-numbers-0.1.10 (c (n "french-numbers") (v "0.1.10") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gvjph5p0nb18di7k842n88rvgh18k8xdgn1zvj9db7kfbb98aw2")))

(define-public crate-french-numbers-0.1.11 (c (n "french-numbers") (v "0.1.11") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cz3k6xzk4zl7p9cslww1hi264f4qg2gffxzi9l7pvys980xm7ac")))

(define-public crate-french-numbers-0.1.12 (c (n "french-numbers") (v "0.1.12") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "01vyssp086prpdizsq5nycdav8bs7pvp4ignshvn9grczn427jiq")))

(define-public crate-french-numbers-1.0.0 (c (n "french-numbers") (v "1.0.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0q9cmjmsc5qjq1zwgbjp0i71h606g98srqqczz6l4y09fx16xb6d")))

(define-public crate-french-numbers-1.0.1 (c (n "french-numbers") (v "1.0.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1yfypkj1k4sn19njykawsakhfa5x5mihag04ysgfvxi7i84nx60w")))

(define-public crate-french-numbers-1.1.0 (c (n "french-numbers") (v "1.1.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "03hhpbqqapzswwraqray97gds86jcz3ri8n65rhhb9gl247m13jx")))

(define-public crate-french-numbers-1.1.1 (c (n "french-numbers") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.41") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "04lpz7sndqi0kvxbbw2h8k1xa47ajck94brqdzx18kzil65a9ric")))

(define-public crate-french-numbers-1.1.2 (c (n "french-numbers") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.41") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0v8wikaxywvlblx583qzmrs93qm0n35fp8mi1f0mdq874s6g5b94")))

(define-public crate-french-numbers-1.1.3 (c (n "french-numbers") (v "1.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.41") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1zks0kj4af796cq8k70ay5yjdlwvm0b3lr8rw84z6x9w17a6n5yq")))

(define-public crate-french-numbers-1.1.4 (c (n "french-numbers") (v "1.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.41") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "10cbl6mc2ah57lrp15rsbk5dy4vqls1s2an1000ydzay9xyn4ymz")))

(define-public crate-french-numbers-1.2.0 (c (n "french-numbers") (v "1.2.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 2)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)))) (h "1y8q00zw35w2y02qamig9b0blglzg8lwaravm6l9dv684cwry83i") (f (quote (("default")))) (s 2) (e (quote (("cli" "dep:num-bigint" "dep:clap" "dep:proc-macro2"))))))

