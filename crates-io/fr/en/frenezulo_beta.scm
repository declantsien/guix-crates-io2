(define-module (crates-io fr en frenezulo_beta) #:use-module (crates-io))

(define-public crate-frenezulo_beta-0.1.0 (c (n "frenezulo_beta") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "compiler_builtins") (r "^0.1.79") (f (quote ("c"))) (d #t) (k 0)) (d (n "frenezulo-macros-beta") (r "^0.1.0") (d #t) (k 0)) (d (n "lunatic") (r "^0.11.1") (f (quote ("msgpack_serializer"))) (d #t) (k 0)) (d (n "multimap") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.7") (d #t) (k 0)) (d (n "submillisecond") (r "^0.2.0-alpha0") (f (quote ("json"))) (d #t) (k 0)))) (h "1p1s8nxpvpqb30izqpiwn7b6b0q0xb04x69i8qj28gv8k7v4mmrb") (y #t)))

