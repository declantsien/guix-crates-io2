(define-module (crates-io fr en frenzy) #:use-module (crates-io))

(define-public crate-frenzy-0.1.0 (c (n "frenzy") (v "0.1.0") (h "0vmvjhl4s38w9wi8sngnr8jrq7ssqcpdi5vdfz7xgvsd0zdk9q2n") (y #t)))

(define-public crate-frenzy-0.1.1 (c (n "frenzy") (v "0.1.1") (h "0b0h6qbxvwlp04k1q2jckv9xlh4il1cys7har2fddydw9skkig4l")))

