(define-module (crates-io fr es fresco) #:use-module (crates-io))

(define-public crate-fresco-0.1.0 (c (n "fresco") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "embed_plist") (r "^1.2") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (d #t) (k 0)))) (h "0wcm2npqb2q83dnnf1qxln3xcyigjcv98gcx2xcbvkw04yx1air9")))

