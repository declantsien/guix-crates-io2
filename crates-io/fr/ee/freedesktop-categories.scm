(define-module (crates-io fr ee freedesktop-categories) #:use-module (crates-io))

(define-public crate-freedesktop-categories-0.1.0 (c (n "freedesktop-categories") (v "0.1.0") (d (list (d (n "freedesktop-categories-codegen") (r "^0.1.0") (o #t) (d #t) (k 1)) (d (n "phf") (r "^0.7.22") (d #t) (k 0)))) (h "0y8pj4lcf7hhkapwkgslanhr5sbh19q7sqjfah8dzrl72r585zps") (f (quote (("generate-map" "freedesktop-categories-codegen") ("default"))))))

(define-public crate-freedesktop-categories-0.2.0 (c (n "freedesktop-categories") (v "0.2.0") (d (list (d (n "freedesktop-categories-codegen") (r "^0.2.0") (o #t) (d #t) (k 1)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)))) (h "1iqgl53w5vfvrv138h0qpd01a3vhwbhkdz4wipbnxpb43xdsa7kv") (f (quote (("generate-map" "freedesktop-categories-codegen") ("default"))))))

