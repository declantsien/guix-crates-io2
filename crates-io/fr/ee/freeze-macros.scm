(define-module (crates-io fr ee freeze-macros) #:use-module (crates-io))

(define-public crate-freeze-macros-0.1.0 (c (n "freeze-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bnraclm0f1nc758y2d8zahik8gkskvvpcbblhl9jcw3x9jakrl7") (f (quote (("nightly") ("default"))))))

