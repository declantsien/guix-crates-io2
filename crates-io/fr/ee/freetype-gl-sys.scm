(define-module (crates-io fr ee freetype-gl-sys) #:use-module (crates-io))

(define-public crate-freetype-gl-sys-0.1.0 (c (n "freetype-gl-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "freetype-src-sys") (r "^0.1") (d #t) (k 0)))) (h "1sk106zn3fdsgdrzlc0w1kxvclg7xaj4yhv89hmiwjpd54sv8gjl")))

(define-public crate-freetype-gl-sys-0.1.1 (c (n "freetype-gl-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "freetype-src-sys") (r "^0.1") (d #t) (k 0)))) (h "0h13z9x6x02fg0154rzzc1xznhvj2gixxd9skimwbqp732sf6km9") (l "freetype-sys")))

(define-public crate-freetype-gl-sys-0.1.2 (c (n "freetype-gl-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "freetype-src-sys") (r "^0.1") (d #t) (k 0)))) (h "19fqkk96mkiiwkqnp1rsryh1rigid65p5w33m2fb176pbxcs1bh7") (l "freetype-sys")))

(define-public crate-freetype-gl-sys-0.1.3 (c (n "freetype-gl-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "freetype-src-sys") (r "^0.1") (d #t) (k 0)))) (h "0d4hir29j4ni01xxlp5yx0ggxajbrzbxhs2axpnk6sbs8i6gjaba") (l "freetype-sys")))

(define-public crate-freetype-gl-sys-0.1.4 (c (n "freetype-gl-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "freetype-src-sys") (r "^0.1") (d #t) (k 0)))) (h "1mmdkj5q1nckvsx56rxif6rlmpm3cy2xikrhgqxqvzkdw37ba557") (l "freetype-sys")))

(define-public crate-freetype-gl-sys-0.2.0 (c (n "freetype-gl-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "freetype-src-sys") (r "^0.1") (d #t) (k 0)))) (h "0m98gsd54aayr7lj0zj0ibmi64094637bksi6d9v9inpc6w91sqj") (l "freetype-sys")))

