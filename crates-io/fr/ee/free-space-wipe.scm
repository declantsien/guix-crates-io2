(define-module (crates-io fr ee free-space-wipe) #:use-module (crates-io))

(define-public crate-free-space-wipe-1.0.0 (c (n "free-space-wipe") (v "1.0.0") (d (list (d (n "clap") (r "^2.17.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1gl50y0jh4nm69kw09602r32xgv369mfzql032rn7k26d9phb2x2")))

