(define-module (crates-io fr ee freertos-rust) #:use-module (crates-io))

(define-public crate-freertos-rust-0.1.0 (c (n "freertos-rust") (v "0.1.0") (h "169ivxp6zwrdr4g5bsh2cxmi5rj9j8x40d1i2c09m3dmc1ykycch") (l "freertos")))

(define-public crate-freertos-rust-0.1.1 (c (n "freertos-rust") (v "0.1.1") (h "0xphpjllg2pry8zrdwg07cdfcc64pah1p72sxs5n5563kkrjs5ic") (l "freertos")))

(define-public crate-freertos-rust-0.1.2 (c (n "freertos-rust") (v "0.1.2") (h "016mjf58cgbmsgm1zazgn6bfv31m8768ix0401jcshk56pvx3m4f") (l "freertos")))

