(define-module (crates-io fr ee freecut) #:use-module (crates-io))

(define-public crate-freecut-0.1.9 (c (n "freecut") (v "0.1.9") (d (list (d (n "comfy-table") (r "^2.1.0") (d #t) (k 0)) (d (n "cut-optimizer-2d") (r "^0.1.1") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "pdf-canvas") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "090njd69if7484a95aqzl83flr06iavfl88r49ddhssxjghb91jj")))

(define-public crate-freecut-0.1.10 (c (n "freecut") (v "0.1.10") (d (list (d (n "comfy-table") (r "^2.1.0") (d #t) (k 0)) (d (n "cut-optimizer-2d") (r "^0.1.1") (d #t) (k 0)) (d (n "fltk") (r "^0.15") (d #t) (k 0)) (d (n "pdf-canvas") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0vvyx8mbx84lh8zvb96zp3hhydyrgs57z48cgw360mi8f2wjh4n5")))

