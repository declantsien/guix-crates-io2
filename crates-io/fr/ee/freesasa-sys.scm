(define-module (crates-io fr ee freesasa-sys) #:use-module (crates-io))

(define-public crate-freesasa-sys-0.1.0 (c (n "freesasa-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "073wv42wbfgc8s12lmmqg9ib4l0m44rqsn32jaiwaljzfdg591zy") (y #t)))

(define-public crate-freesasa-sys-0.1.1 (c (n "freesasa-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "1bvna0dr13vc266qwrhk953pjb6x0mr8c4m7fai752ykvx29d0vx") (y #t)))

(define-public crate-freesasa-sys-0.1.2 (c (n "freesasa-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0c88ybiynn434wy6xdjrra1rrfbg5jmlf7sjambss2lsvwbyh89q") (y #t)))

(define-public crate-freesasa-sys-0.1.3 (c (n "freesasa-sys") (v "0.1.3") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "1w3sz94wy7n9b47ykfqp6g8bk4476phfjz89ka86jd0pd74ys88b") (y #t)))

(define-public crate-freesasa-sys-0.1.4 (c (n "freesasa-sys") (v "0.1.4") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "1lf6z9l0qxcm3smz84kfji57srpylbzk4fqv052v2dlkp523rdya") (y #t)))

(define-public crate-freesasa-sys-0.1.5 (c (n "freesasa-sys") (v "0.1.5") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "06rsn6s1s0zka2darc6ckjwp53smaan5pjba65ahl5vd1iv0ljns") (y #t)))

(define-public crate-freesasa-sys-0.1.6 (c (n "freesasa-sys") (v "0.1.6") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "016c7ar96g742yfbssax6ixg08d5p999mr00s6a8qbkalkjmhfnz") (y #t)))

(define-public crate-freesasa-sys-0.1.7 (c (n "freesasa-sys") (v "0.1.7") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)))) (h "0x5y3brm3lp9hvk4h1jnq1dffgk31x7f345vcgvq8xmng4nc9gh9") (y #t)))

(define-public crate-freesasa-sys-0.1.8 (c (n "freesasa-sys") (v "0.1.8") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (d #t) (k 0)))) (h "0q94ck6anihb5dmp3xf5n92kyz62pf4pw6zalcxcjxcq4prhdazh") (y #t)))

(define-public crate-freesasa-sys-0.1.9 (c (n "freesasa-sys") (v "0.1.9") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (d #t) (k 0)))) (h "0qxlpfsj8dgh9hqz775b13zq62rymg0rmg0bmpr9hibb0rs2p2mx") (y #t)))

(define-public crate-freesasa-sys-0.1.10 (c (n "freesasa-sys") (v "0.1.10") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (d #t) (k 0)))) (h "0d1hdrb6kiv9mmfbw5m1j9rqfwymwr36lclmz7abnqdpsw21487a") (y #t)))

(define-public crate-freesasa-sys-0.1.11 (c (n "freesasa-sys") (v "0.1.11") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (d #t) (k 0)))) (h "06rqkb84xnqs4x2444v8fxa1jmw6dffm6s4la11fvj0swawq2a3r") (y #t)))

(define-public crate-freesasa-sys-0.1.12 (c (n "freesasa-sys") (v "0.1.12") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.8") (d #t) (k 0)))) (h "16nx1khrskhihd63bg4lh4w6ydg5pqw2j84axwcym7rp5mjlapha")))

