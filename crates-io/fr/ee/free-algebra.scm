(define-module (crates-io fr ee free-algebra) #:use-module (crates-io))

(define-public crate-free-algebra-0.1.0 (c (n "free-algebra") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "maths-traits") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1z7bs0dwy2vimsna1mbw0cif372sjrhnpcvv8d4kvwds3hxs818g")))

