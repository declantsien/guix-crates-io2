(define-module (crates-io fr ee freechains) #:use-module (crates-io))

(define-public crate-freechains-0.3.0 (c (n "freechains") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gchadb471ddl229bhsgpgdi3p24rfi6djybjgxwv11dmskvlfz7")))

(define-public crate-freechains-0.3.1 (c (n "freechains") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mn4rnaybr95nv82kksp8dvymjd8k9q0lhrqmdrnn3k8z300qq4j")))

