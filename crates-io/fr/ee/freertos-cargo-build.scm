(define-module (crates-io fr ee freertos-cargo-build) #:use-module (crates-io))

(define-public crate-freertos-cargo-build-0.1.0 (c (n "freertos-cargo-build") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.52") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "09hi8g60zqq28d66w4ih12pgc15wslprq6p99s428hwkv985w7s1")))

(define-public crate-freertos-cargo-build-0.1.1 (c (n "freertos-cargo-build") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.52") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0cgrq4x533fbcr344yqrgibh9lns4g3s2a7f208qn9pk70hdccxm")))

