(define-module (crates-io fr ee freebsd-libgeom) #:use-module (crates-io))

(define-public crate-freebsd-libgeom-0.1.0 (c (n "freebsd-libgeom") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 2)))) (h "0v581wz50s2fhym1ka4p6a827vqwjszys04rfh5z8i56px1pibdb")))

(define-public crate-freebsd-libgeom-0.1.1 (c (n "freebsd-libgeom") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 2)))) (h "1hf2laxqhpm5pkc1nvj4vikrkf44y52lb3qlpqxih4k0yxcm2q1f")))

(define-public crate-freebsd-libgeom-0.2.0 (c (n "freebsd-libgeom") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 2)))) (h "1pyqbqyc3l7jifbkcx2c269ykx9708268hy1b0qqblwrnwbsf1br")))

(define-public crate-freebsd-libgeom-0.2.1 (c (n "freebsd-libgeom") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (f (quote ("time"))) (k 2)))) (h "14yh2zhvd8b2frblz9kdmf0vhjklg4xazmbikzin7ls9l8zqx70x")))

(define-public crate-freebsd-libgeom-0.2.2 (c (n "freebsd-libgeom") (v "0.2.2") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (f (quote ("time"))) (k 2)))) (h "1lk6xqqjj98wv1nwa9hh4firz53v6la1labz1wqcqqxqim5gglk3") (r "1.64")))

(define-public crate-freebsd-libgeom-0.2.3 (c (n "freebsd-libgeom") (v "0.2.3") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (f (quote ("time"))) (k 2)))) (h "0gbqvvhbsq9d4cg18553d52swvjh4rlp72xg4pqda5hhvya8azcb") (r "1.64")))

(define-public crate-freebsd-libgeom-0.2.4 (c (n "freebsd-libgeom") (v "0.2.4") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.27.0") (f (quote ("time"))) (k 2)))) (h "0cl53f6xppxlk4q02705cr942aqrqa04si7i75mszn3fwn6vrf8l") (r "1.64")))

(define-public crate-freebsd-libgeom-0.3.0 (c (n "freebsd-libgeom") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "freebsd-libgeom-sys") (r "=0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nix") (r "^0.27.0") (f (quote ("time"))) (k 2)))) (h "0lfj3hvynfdqg5ifjy7c5ndyln70l0cgb19vs6pkdl3fby6yllbl") (r "1.70")))

