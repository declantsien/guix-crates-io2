(define-module (crates-io fr ee freee) #:use-module (crates-io))

(define-public crate-freee-0.1.0 (c (n "freee") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1kyjjpxd0y86652bgqvqvd2cfs7xd05csa52ql7msxglyjwkp1ry")))

(define-public crate-freee-0.1.1 (c (n "freee") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0c4959n4zpc0pisf580wbjqm8pm060g2yj6a0lgwva77apxklha7")))

(define-public crate-freee-0.1.2 (c (n "freee") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1g53pdmxkw7lcz6gs8ybh5r2ccmiy7r9dlxci2x7xn4mkszd1gp1")))

(define-public crate-freee-0.2.0 (c (n "freee") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1s3qnrc2dq28gi9ryk5l4ic94flyfjsmqpcxix9y71l1q6rsvb74")))

(define-public crate-freee-0.3.0 (c (n "freee") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0i7p57lygn0ccmlc3shnja5w4hfb274k6zy68radfsaz2mjiyl87")))

(define-public crate-freee-0.3.1 (c (n "freee") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "06dj3w1rc81gig0y3f25ng00q8m9lagzmqp9y2fbrh6i1xxkc0g2")))

(define-public crate-freee-0.4.0 (c (n "freee") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "02ajb5isfhg4q4xpgh7l2075dxiyhq86xylpczw7n8jijys2939m")))

(define-public crate-freee-0.4.1 (c (n "freee") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "00cmdips90308knsif668kjn5i29jadcsn0xdj2vlnz236l7j1yf")))

(define-public crate-freee-0.5.0 (c (n "freee") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "05gzsh1c600dyq2jwpl94rj78rz4vl1p6z665fy4nms5vyfxc2i5")))

(define-public crate-freee-0.6.0 (c (n "freee") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1rpjqnqs1k604xr2w3488agnzmqbgvcw49pylva8x84q5kkbnd50")))

(define-public crate-freee-0.7.0 (c (n "freee") (v "0.7.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0j769ay4hgfsv8s2rsx2wdx7mwgn23jm0m9aiyhbf6rn69dyrp1v")))

