(define-module (crates-io fr ee freenectrs) #:use-module (crates-io))

(define-public crate-freenectrs-0.1.0 (c (n "freenectrs") (v "0.1.0") (d (list (d (n "genmesh") (r "^0.4") (d #t) (k 2)) (d (n "glium") (r "^0.16") (d #t) (k 2)) (d (n "image") (r "^0.12") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "obj") (r "^0.5") (f (quote ("usegenmesh"))) (d #t) (k 2)))) (h "1d232f349d2iknvpcgkkksvsz84pk2fsbsvsyqr2690wvdw8gc8r")))

