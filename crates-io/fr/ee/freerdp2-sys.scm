(define-module (crates-io fr ee freerdp2-sys) #:use-module (crates-io))

(define-public crate-freerdp2-sys-0.1.0 (c (n "freerdp2-sys") (v "0.1.0") (d (list (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "1smvibi0yz6sdp9gvkg09c6fsl5lav3177j8imc1dpldml55bpz4") (l "freerdp-client2")))

(define-public crate-freerdp2-sys-0.2.0 (c (n "freerdp2-sys") (v "0.2.0") (d (list (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "03g1pwnqhvgwrajx0cj9m31jwbl4f17jqjh1kxass574s2faicsi") (l "freerdp-client2")))

