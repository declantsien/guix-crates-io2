(define-module (crates-io fr ee freefare) #:use-module (crates-io))

(define-public crate-freefare-0.1.0 (c (n "freefare") (v "0.1.0") (d (list (d (n "freefare-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1iya41qivbk7jq2hidma6r2qnp7sqbk9yaskdsnq7vgwanr0bks5")))

(define-public crate-freefare-0.1.1 (c (n "freefare") (v "0.1.1") (d (list (d (n "freefare-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0jcnh3gc14kqgkcva7yikxwp0jwskbh1n6slvjlqhg5kbdvjb6ln")))

(define-public crate-freefare-0.1.2 (c (n "freefare") (v "0.1.2") (d (list (d (n "freefare-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)))) (h "131pylf4r19zc3mwys7yd4rbw51gpmk44mvz0b3g60y7b7fwh9bm")))

(define-public crate-freefare-0.2.0 (c (n "freefare") (v "0.2.0") (d (list (d (n "freefare-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0pplksyby0h61kxav6d72wdrr8ch1rv1hlxfm4d0vc1yjgn5n905")))

