(define-module (crates-io fr ee freedesktop_entry_parser) #:use-module (crates-io))

(define-public crate-freedesktop_entry_parser-0.1.0 (c (n "freedesktop_entry_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)))) (h "046pkxk7wz4sy0dzd5iskwq4affn1979h8gc1h6nf94r1fxjvsj8")))

(define-public crate-freedesktop_entry_parser-0.2.0 (c (n "freedesktop_entry_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)))) (h "0zplf4bwfl0hp95l3pva0l5vv1xxgyi4xkzf5gvf832k2q7gvmq9")))

(define-public crate-freedesktop_entry_parser-0.2.2 (c (n "freedesktop_entry_parser") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)))) (h "1qzk277bdklhnra9sa8gnfq1zbidywwk6kjzik5sy04yvq6f8w8f")))

(define-public crate-freedesktop_entry_parser-0.4.0 (c (n "freedesktop_entry_parser") (v "0.4.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fmimypb0cj23j816r1p6s0a2ld5h6zxd9p80ngwf1xhws35jyq8")))

(define-public crate-freedesktop_entry_parser-0.4.1 (c (n "freedesktop_entry_parser") (v "0.4.1") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13s0i1m8zmlxxbxbkv6r1f27df1h2f79dks3bd3d8g41xdn1pybx")))

(define-public crate-freedesktop_entry_parser-1.0.0 (c (n "freedesktop_entry_parser") (v "1.0.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03f1jqhr35y240p4h01zfy7sqavxfads2kr9fjnh65x8bpdwwpqx")))

(define-public crate-freedesktop_entry_parser-1.1.0 (c (n "freedesktop_entry_parser") (v "1.1.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yylfghbkmnxhl4f0hjq8dky1l25v2fav66qanzs1fb2qjm20hbg")))

(define-public crate-freedesktop_entry_parser-1.1.1 (c (n "freedesktop_entry_parser") (v "1.1.1") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l5yfgqs2yck6mdsqcsf48hsm84g1xnzg56rjj1rih2sdb0njp6v")))

(define-public crate-freedesktop_entry_parser-1.1.2 (c (n "freedesktop_entry_parser") (v "1.1.2") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fiz8n2qflx6gj5lksqw5znbmzsxrpsn4sqw9sy2hp2ggakkr21y")))

(define-public crate-freedesktop_entry_parser-1.2.0 (c (n "freedesktop_entry_parser") (v "1.2.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hbfys7nj7pil2v9bjjyczm777dq3rk8n4f39l50m3042cds4yil")))

(define-public crate-freedesktop_entry_parser-1.3.0 (c (n "freedesktop_entry_parser") (v "1.3.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r630rrn55znzvd6zrbn6c0k3vz44qnrxa4cby4rma8r5yvjg76v")))

