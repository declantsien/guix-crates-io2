(define-module (crates-io fr ee freesasa-rs) #:use-module (crates-io))

(define-public crate-freesasa-rs-0.1.3 (c (n "freesasa-rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "freesasa-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.3.2") (d #t) (k 0)))) (h "0pk2qd99anp94mys2mfz1wiw95pfc7vzqnjamd8zjz989ij504kl") (f (quote (("unsafe-ops"))))))

