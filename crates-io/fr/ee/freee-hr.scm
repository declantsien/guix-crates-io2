(define-module (crates-io fr ee freee-hr) #:use-module (crates-io))

(define-public crate-freee-hr-0.3.1 (c (n "freee-hr") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "164ym5nvj5ak7dwqbmivv0yd91vnpkfp95g2rv1b7i6cr4l2gpyg")))

(define-public crate-freee-hr-0.4.0 (c (n "freee-hr") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1n16g68nnyrarfqv0m8cr9mw5rg7jlhgkwxkrwb4k8a3s2aampf3")))

(define-public crate-freee-hr-0.4.1 (c (n "freee-hr") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0dzgj2w965mh561mzlhs55nsrkcqm06mhxddbazd4802p7hm30i6")))

(define-public crate-freee-hr-0.5.0 (c (n "freee-hr") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0c000z96yghkc3xrh3hkd7888w64prkpnh21d02zmb4vr53v7yh6")))

(define-public crate-freee-hr-0.6.0 (c (n "freee-hr") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1sj7h23c5s6pflx2hd01jjm7yl07x5gbsfx6h60n3z7azcgwl1kj")))

(define-public crate-freee-hr-0.7.0 (c (n "freee-hr") (v "0.7.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.2") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1ma7q8g67zzq6jx7pps40aqd8l1mkvvh6f6cymdivqj8r5y4dwld")))

