(define-module (crates-io fr ee freerdp2) #:use-module (crates-io))

(define-public crate-freerdp2-0.1.0 (c (n "freerdp2") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "freerdp2-sys") (r "^0.1") (d #t) (k 0)))) (h "03znxzfgka2ilvb86f59kxicc1q2zjhky77y2z53icqkjbzy46lq")))

(define-public crate-freerdp2-0.2.0 (c (n "freerdp2") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "freerdp2-sys") (r "^0.2") (d #t) (k 0)))) (h "16lxnkcy7nmamykf3ard7fk0f1hx35gqmkwzw0nci5ajfrz04jvl")))

