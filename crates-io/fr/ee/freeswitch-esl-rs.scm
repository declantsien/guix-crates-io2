(define-module (crates-io fr ee freeswitch-esl-rs) #:use-module (crates-io))

(define-public crate-freeswitch-esl-rs-0.1.0 (c (n "freeswitch-esl-rs") (v "0.1.0") (d (list (d (n "queues") (r "^1.0.2") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "04w7apyf9j734b6d1zj2vn785agkpa51zhd39gxb73pgiqpbj4rl")))

(define-public crate-freeswitch-esl-rs-0.1.1 (c (n "freeswitch-esl-rs") (v "0.1.1") (d (list (d (n "queues") (r "^1.0.2") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "10m1b6940wmkyyhlglnai5pda8lm4z3ga43v70ddllrmdfs69qcp")))

(define-public crate-freeswitch-esl-rs-0.2.0 (c (n "freeswitch-esl-rs") (v "0.2.0") (d (list (d (n "queues") (r "^1.0.2") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0k22l3fv3g4rdv2r4jh8r57i0k81w0jaxgj4x6mk035s5pzb8zs4")))

(define-public crate-freeswitch-esl-rs-0.3.0 (c (n "freeswitch-esl-rs") (v "0.3.0") (d (list (d (n "queues") (r "^1.0.2") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0898241zabij7b94x2s29cf3n2ah84vdsihs12g4amksqrd0mgfl")))

(define-public crate-freeswitch-esl-rs-0.3.1 (c (n "freeswitch-esl-rs") (v "0.3.1") (d (list (d (n "queues") (r "^1.0.2") (d #t) (k 0)) (d (n "urldecode") (r "^0.1.1") (d #t) (k 0)))) (h "0xc8vw9kx4rc95r42h6j7fmdsrcr8kny8ilkiqwfgxdm3wgxsl11")))

