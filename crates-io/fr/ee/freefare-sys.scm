(define-module (crates-io fr ee freefare-sys) #:use-module (crates-io))

(define-public crate-freefare-sys-0.1.0 (c (n "freefare-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1sqzlan2mmzdrhjavxl86j0hq376k004x8hy43mnj7zid77yz259")))

(define-public crate-freefare-sys-0.1.1 (c (n "freefare-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0d26ix3337qlmi9mf1hw2vhf5pvzx5ibk514i963phg46c3k1viz")))

(define-public crate-freefare-sys-0.1.2 (c (n "freefare-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)))) (h "176k2w6jwpwm54qkc4zq49n8q7bf5n7mhxdv2r66cjm8mx7vgvqm")))

(define-public crate-freefare-sys-0.2.0 (c (n "freefare-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0nvkfwxgi75vq2p75adp4b96iyh970jkdq6m2jddha8cif1sxrql")))

