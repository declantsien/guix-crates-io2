(define-module (crates-io fr ee free-list) #:use-module (crates-io))

(define-public crate-free-list-0.1.1 (c (n "free-list") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "10jy899g51qz2pfyzx2ab514l4ki6wxcwqd1hbz0yvz6jh49xd12") (y #t)))

(define-public crate-free-list-0.1.2 (c (n "free-list") (v "0.1.2") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0z61c44wfi878cw98qsri1x9bff4ysliq6lgswhvkfgflslpaxpy") (y #t)))

(define-public crate-free-list-0.1.3 (c (n "free-list") (v "0.1.3") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0030wp2x0ipnhxcws29wl92yn6ashcfmajrc01h4vqdz32s7134d") (y #t)))

(define-public crate-free-list-0.1.4 (c (n "free-list") (v "0.1.4") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1m60qri923ns2vkrlpl1nq2xlj83k6k00jl9hfl40jas99d4kzq7") (y #t)))

(define-public crate-free-list-0.2.0 (c (n "free-list") (v "0.2.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "16v5xf92jasvzz5z25x0nax6mwy0sn1p6l4zrvy4iydi23kd8jib") (y #t)))

(define-public crate-free-list-0.2.1 (c (n "free-list") (v "0.2.1") (h "0y97zmmqa2ni0bmp634d6ri3g076vwb0xw64a1q61r7aki9basrs") (y #t)))

(define-public crate-free-list-0.3.0 (c (n "free-list") (v "0.3.0") (d (list (d (n "align-address") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("const_new" "const_generics" "union"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.15") (o #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0avgp4fzrvaj534qcjyrrn4j9ihkazn30saxphxziq92rkfvyh6i") (s 2) (e (quote (("x86_64" "dep:x86_64"))))))

(define-public crate-free-list-0.3.1 (c (n "free-list") (v "0.3.1") (d (list (d (n "align-address") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("const_new" "const_generics" "union"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.15") (o #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "1axgz7z4rjfwizqsi9ic0336dhvybprrk4cw4yd333zr9s5363w1") (s 2) (e (quote (("x86_64" "dep:x86_64"))))))

