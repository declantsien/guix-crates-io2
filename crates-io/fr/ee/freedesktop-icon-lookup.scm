(define-module (crates-io fr ee freedesktop-icon-lookup) #:use-module (crates-io))

(define-public crate-freedesktop-icon-lookup-0.1.0 (c (n "freedesktop-icon-lookup") (v "0.1.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tini") (r "^1") (d #t) (k 0)))) (h "1vvlzh1krwj928xxjk2g5y63phxgs2474jslmhzjqlr3wk579qd2") (f (quote (("default") ("debug"))))))

(define-public crate-freedesktop-icon-lookup-0.1.1 (c (n "freedesktop-icon-lookup") (v "0.1.1") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tini") (r "^1") (d #t) (k 0)))) (h "1cmqwbzv5df678rfrl8wc2b20bwxi3c1lwrzhyam9asgdlcaa06w") (f (quote (("default") ("debug")))) (y #t)))

(define-public crate-freedesktop-icon-lookup-0.1.2 (c (n "freedesktop-icon-lookup") (v "0.1.2") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tini") (r "^1") (d #t) (k 0)))) (h "0xhdiwb7f920zqwiv7d5id9v8kl4mkvq6ggdanbd5ixl33ziadcg") (f (quote (("default") ("debug"))))))

(define-public crate-freedesktop-icon-lookup-0.1.3 (c (n "freedesktop-icon-lookup") (v "0.1.3") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tini") (r "^1") (d #t) (k 0)))) (h "0cxzav1wmvbw54vql04d14g8gf9b943wc9akllrngxgfzmd5gnpk") (f (quote (("default") ("debug"))))))

