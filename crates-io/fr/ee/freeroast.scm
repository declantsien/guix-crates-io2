(define-module (crates-io fr ee freeroast) #:use-module (crates-io))

(define-public crate-freeroast-0.1.0 (c (n "freeroast") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1kbwsvzwyflr7srxv0nmv9xsrsc22awjd5xcpi19hb7yi0pwc9kk") (y #t)))

(define-public crate-freeroast-0.1.1 (c (n "freeroast") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ppyrhn4swv8z67il24v85yiy9paqr8073bcv7gdzyfg9sic4whz") (y #t)))

(define-public crate-freeroast-0.1.2 (c (n "freeroast") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0nynlmp649nycblwg1dn9r9p03hfra6415pnayrln5yxgjvlgh08") (y #t)))

(define-public crate-freeroast-0.1.3 (c (n "freeroast") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0l76kpx9gg0awgn2kin5rjbpic1y6vvhibn53vgdy9bh4sl2y3s2") (y #t)))

(define-public crate-freeroast-0.1.4 (c (n "freeroast") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ds06krzamkw6jbmqzg7s066iycrl0wmaakqc69496l922xajr84") (y #t)))

(define-public crate-freeroast-0.1.5 (c (n "freeroast") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0gj62g5f3pmr3hs9ah163bda54va3a6b8clc0b80sx8ppg59g5jw") (y #t)))

(define-public crate-freeroast-0.1.6 (c (n "freeroast") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1f9qmxcv9kd7f9lki7mc4701h4pgk48rzpam87a34z8phbjsyjyq") (y #t)))

(define-public crate-freeroast-0.1.7 (c (n "freeroast") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "09gfy097f6vnyy8k4ii3bk08hw30v76d4h77y17lb5dydh658q4s") (y #t)))

(define-public crate-freeroast-0.1.8 (c (n "freeroast") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0q0msb9h7azd56x6jh8kg8cfz7f4gfg2hgqx8w5p7bmgvxiyblk4")))

(define-public crate-freeroast-0.1.9 (c (n "freeroast") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.26.3") (f (quote ("chrono" "bundled"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0d1x3gcmby5ss8vl5nyv5cfhp3gas00lq1h7wf9xjb815n5541bg")))

