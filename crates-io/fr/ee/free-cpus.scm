(define-module (crates-io fr ee free-cpus) #:use-module (crates-io))

(define-public crate-free-cpus-1.0.0 (c (n "free-cpus") (v "1.0.0") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "18bji7gzzjnz1yh0439616zhdca1njscbrg3pkwji65jgmmf3pk4")))

(define-public crate-free-cpus-2.0.0 (c (n "free-cpus") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)))) (h "0ffj58cz0g98zh9pnnmi8wf954ql5zv7a8468qq36ffd1fjglxfh")))

