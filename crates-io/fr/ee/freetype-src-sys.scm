(define-module (crates-io fr ee freetype-src-sys) #:use-module (crates-io))

(define-public crate-freetype-src-sys-0.0.1 (c (n "freetype-src-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "zlib-src-sys") (r "*") (d #t) (k 0)))) (h "1xa92r5zci4h8j9xwq5hm7sa7d821kxbvq2ifj67am89kpd5q5v1")))

(define-public crate-freetype-src-sys-0.0.2 (c (n "freetype-src-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0i1rw04sjpr63a0jwsg2v2rw5if0fwwixblqkn716lg8var62q11")))

(define-public crate-freetype-src-sys-0.0.3 (c (n "freetype-src-sys") (v "0.0.3") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0r3cvr44nlr4z5d1jw6n8k1x4alapnv68vfvhnl3xzkmfmk0002k")))

(define-public crate-freetype-src-sys-0.0.4 (c (n "freetype-src-sys") (v "0.0.4") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0qaxigsp17la69hfnbslg9syan5jfrr1m8m0ir8b5515v15cfi5r")))

(define-public crate-freetype-src-sys-0.1.0 (c (n "freetype-src-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0msi9gzgkvjny6vpfmbhp1f3r0q1mbb3wbjz7s1qk6h36d4wvghj") (l "freetype2")))

(define-public crate-freetype-src-sys-0.1.1 (c (n "freetype-src-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0vh78v5122x231m0f1y31c14043rdr7kcmb31ywf8a6j0jbnsv2g") (l "freetype2")))

(define-public crate-freetype-src-sys-0.1.2 (c (n "freetype-src-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.1.1") (d #t) (k 0)))) (h "12w45irpxzvcqr2cq089gvfcaxglaf9v3x8bkx0kbpdz6s15584i") (l "freetype2")))

(define-public crate-freetype-src-sys-0.1.3 (c (n "freetype-src-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0qarkdl2d6da4pbp8hhczmmapy9jkggzvwg4iv60slwdlww9lszn") (l "freetype2")))

(define-public crate-freetype-src-sys-0.1.4 (c (n "freetype-src-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.1.1") (d #t) (k 0)))) (h "11wpwywnq792ds6ib72g59byb7pzy6r4dkgik36ry39sx9b5jsb3") (l "freetype2")))

(define-public crate-freetype-src-sys-0.1.5 (c (n "freetype-src-sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "zlib-src-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1md0v04z40w9xslg14pf7vn3lhhd7xzq38z3lcrnilgvnmwq16rh") (l "freetype2")))

