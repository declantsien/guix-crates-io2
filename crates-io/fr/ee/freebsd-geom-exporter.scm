(define-module (crates-io fr ee freebsd-geom-exporter) #:use-module (crates-io))

(define-public crate-freebsd-geom-exporter-0.1.0 (c (n "freebsd-geom-exporter") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "freebsd-libgeom") (r "^0.3.0") (d #t) (k 0)) (d (n "prometheus_exporter") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("perf" "std"))) (d #t) (k 0)))) (h "0v98x286hqr137aiwkf6yjhhhv39a1jn7ckhlijrp85hkpidj21w") (r "1.70")))

(define-public crate-freebsd-geom-exporter-0.1.1 (c (n "freebsd-geom-exporter") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "freebsd-libgeom") (r "^0.3.0") (d #t) (k 0)) (d (n "prometheus_exporter") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("perf" "std"))) (d #t) (k 0)))) (h "0q74x4zbljvvhfhrbaw5qzyjlszbc2wi9icm7s43av2chrc2b540") (r "1.70")))

