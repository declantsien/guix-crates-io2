(define-module (crates-io fr ee freetype-gen-rs) #:use-module (crates-io))

(define-public crate-freetype-gen-rs-0.2.13 (c (n "freetype-gen-rs") (v "0.2.13") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "06hymnlim4yy13caaay2m6n6850whl13h5y3grpcrix2shy34maz")))

(define-public crate-freetype-gen-rs-0.213.1 (c (n "freetype-gen-rs") (v "0.213.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1cq6h779q1qxnmfvqvc6fzzrb4iw42gvqz0njkkavm0yw431a464")))

