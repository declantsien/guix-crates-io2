(define-module (crates-io fr ee freetype-sys) #:use-module (crates-io))

(define-public crate-freetype-sys-0.0.1 (c (n "freetype-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0y4k2c1yhm5mhvdxh89z486xjdla281dlb5laf10z4xaf2n2x5kr")))

(define-public crate-freetype-sys-0.0.2 (c (n "freetype-sys") (v "0.0.2") (d (list (d (n "libz-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "161svi2j6kqa8yv8k0i4rn38z38npgykdl165wx1xi6bpqixn72x")))

(define-public crate-freetype-sys-0.0.4 (c (n "freetype-sys") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libz-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1kgg65746qxklgm71mq10b8dmsdjndd6ghfsjfds439v1xpnncfx")))

(define-public crate-freetype-sys-0.0.5 (c (n "freetype-sys") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libz-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1iy0zchhk05v0abl1nfn2rr71sc2rzkkrbjlamdb307pnw3z4gbi")))

(define-public crate-freetype-sys-0.1.0 (c (n "freetype-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "libz-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.4") (d #t) (k 1)))) (h "093fhgqr3mp13j5dxj5f47llgl8c9skzssy5pgrcg93armgwqsya")))

(define-public crate-freetype-sys-0.1.1 (c (n "freetype-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "libz-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "0hgbj5lmjwsr69vq4ijnf664q168s6ak11l0jzhbdv4v9xn1lkd0")))

(define-public crate-freetype-sys-0.1.2 (c (n "freetype-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libz-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "1s73ii5sal7ibiz0i44vj5pllbi8x4y0xhrvlymimc25pf9zfh42")))

(define-public crate-freetype-sys-0.2.0 (c (n "freetype-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "0f35ikk355hd65bd98clbv84q9qm4zf1spmqfsmirfchv19wrism")))

(define-public crate-freetype-sys-0.2.1 (c (n "freetype-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "17pq2nxjrgm82xp2mq0j20db6dlbc3rj5dz58ybqxqa4zbpr0gk4")))

(define-public crate-freetype-sys-0.3.0 (c (n "freetype-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "0cvdmvh7355jyznwx7iih8nbip63nwi6mhf31cf096blmfgkpqjp")))

(define-public crate-freetype-sys-0.3.1 (c (n "freetype-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "0kpcyvm6vd1yfwzjnkrc2zqi3svz4hdw9mckpwydhqqz9bscs3yn")))

(define-public crate-freetype-sys-0.4.0 (c (n "freetype-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "09w1y5sqg835gpzbsi9cia31khpnclbsjhi11hgr56dcdkcvdkzc")))

(define-public crate-freetype-sys-0.5.0 (c (n "freetype-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "1b3v2pqq1v21na4dklahsrwr2h12r3fmlbajymfbiwzwh664j7j8")))

(define-public crate-freetype-sys-0.6.0 (c (n "freetype-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)))) (h "0aw33d9hixvl7i2n1vvxb3xp5avsix1xvhh8f2gxa0r1b5a8r1ag")))

(define-public crate-freetype-sys-0.7.0 (c (n "freetype-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.18") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1bz53zsk66x77gs27mi74f7mmnwhbcrm2vly2gsrghamd2ayk1cl") (l "freetype")))

(define-public crate-freetype-sys-0.7.1 (c (n "freetype-sys") (v "0.7.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.18") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0f2x5cqlv7fbjpsf1hxlh0jbkfmzzr3kcqlhl98nxkz7rik8d70g") (l "freetype")))

(define-public crate-freetype-sys-0.8.0 (c (n "freetype-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.18") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1i5q9dj7m7qkagd9ddh4j7qnbw8zqlsdd30n2wkz6g6y72881j8j") (l "freetype")))

(define-public crate-freetype-sys-0.9.0 (c (n "freetype-sys") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.18") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1i309xc6gcsgdfiim3j5f0sk08imr4frlzfa185iaxqciysqgikx") (l "freetype")))

(define-public crate-freetype-sys-0.10.0 (c (n "freetype-sys") (v "0.10.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0.18") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1jbjj6phbkdsixdfa07069gk8vp9lsx4zpvif15fn79p0abax70g") (l "freetype")))

(define-public crate-freetype-sys-0.11.0 (c (n "freetype-sys") (v "0.11.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1spk6igzacbblm9mxv9dqm7rfjhvqpbrirf8bq8pmm3d6v7c12nl") (l "freetype")))

(define-public crate-freetype-sys-0.12.0 (c (n "freetype-sys") (v "0.12.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0wgfp46bsa0rzb78kc2w4gmdl2hb6kaivy5my5qm5nvnirx36c3m") (l "freetype")))

(define-public crate-freetype-sys-0.13.0 (c (n "freetype-sys") (v "0.13.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1x65lpy8lcpw7iwwhqx1vaxhp351795a984zhz7by9yn12ga10w0") (l "freetype")))

(define-public crate-freetype-sys-0.13.1 (c (n "freetype-sys") (v "0.13.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "06kkds31s6b1i39dyanwmzbnic7laia1kk3gfvx8sqncq08l0zd3") (l "freetype")))

(define-public crate-freetype-sys-0.14.0 (c (n "freetype-sys") (v "0.14.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1iqgjbgzsgnkp6b903sajd0r9w3lncir7fv9hz9qzlgkzkpysz5n") (l "freetype")))

(define-public crate-freetype-sys-0.15.0 (c (n "freetype-sys") (v "0.15.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0a0hic3s6793sjjij5xf2wx7dlpkxl5ha7n0hs72mkpz81hap5dc") (l "freetype")))

(define-public crate-freetype-sys-0.16.0 (c (n "freetype-sys") (v "0.16.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1clijnjv0gl4a3a1cnrlx3276jk07gp3wlgkhp2wclbxmmp6jz51") (l "freetype")))

(define-public crate-freetype-sys-0.17.0 (c (n "freetype-sys") (v "0.17.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)))) (h "127z6hbsfhsw0fg110zy9s65fzald0cvwbxmhk1vxmmsdk54hcb4") (l "freetype")))

(define-public crate-freetype-sys-0.18.0 (c (n "freetype-sys") (v "0.18.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0as8is2gxwgmybvsm2lqfspinf2b4r4k0sh9kj70974xcvssrn4q") (l "freetype")))

(define-public crate-freetype-sys-0.19.0 (c (n "freetype-sys") (v "0.19.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1mgci2x18xn24s9izy2s5gyjqwjnpas8yydlv2z9zn23kb1jivk6") (l "freetype")))

(define-public crate-freetype-sys-0.20.0 (c (n "freetype-sys") (v "0.20.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "1q6vg4rh7r4r7h5v3f8mmkvz88bj1rv2dgw24bk94ys4p3sbnkcb") (l "freetype")))

(define-public crate-freetype-sys-0.20.1 (c (n "freetype-sys") (v "0.20.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0d5iiv95ap3lwy7b0hxbc8caa9ng1fg3wlwrvb7rld39jrdxqzhf") (l "freetype")))

