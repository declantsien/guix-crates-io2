(define-module (crates-io fr ee freedesktop-categories-codegen) #:use-module (crates-io))

(define-public crate-freedesktop-categories-codegen-0.1.0 (c (n "freedesktop-categories-codegen") (v "0.1.0") (d (list (d (n "amxml") (r "^0.5.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "curl") (r "^0.4.12") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.22") (d #t) (k 0)))) (h "1k5wlr97mg3hzhiiq9pc9hv45gk586sg2k1b5qzs71sd61020igy")))

(define-public crate-freedesktop-categories-codegen-0.2.0 (c (n "freedesktop-categories-codegen") (v "0.2.0") (d (list (d (n "amxml") (r "^0.5.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "curl") (r "^0.4.19") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 0)))) (h "02vf12zzgg79h7ghqas1sswisbc63g8avammnqpvkl3682iny7x8")))

