(define-module (crates-io fr ee freedesktop-desktop-entry) #:use-module (crates-io))

(define-public crate-freedesktop-desktop-entry-0.1.0 (c (n "freedesktop-desktop-entry") (v "0.1.0") (d (list (d (n "markup") (r "^0.3.1") (d #t) (k 0)))) (h "0pl1r2p27k7v81ax2vgf2slkh038k9064wn8xh9da98sjrpsgdws") (y #t)))

(define-public crate-freedesktop-desktop-entry-0.1.1 (c (n "freedesktop-desktop-entry") (v "0.1.1") (d (list (d (n "markup") (r "^0.3.1") (d #t) (k 0)))) (h "1gz40543csj17z3f6hiyv68g4j705jsjlhzj6isg15ddpr0c7xnq")))

(define-public crate-freedesktop-desktop-entry-0.2.0 (c (n "freedesktop-desktop-entry") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "012krvh22flgl78z713knm7nj7g4mvsivr52gnv04h0i8mw93253") (y #t)))

(define-public crate-freedesktop-desktop-entry-0.2.1 (c (n "freedesktop-desktop-entry") (v "0.2.1") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "0jc7i8q6y5bd3xq0n08rgna4pqbyc66h1dcy580hsrjv32xr1ys9") (y #t)))

(define-public crate-freedesktop-desktop-entry-0.2.2 (c (n "freedesktop-desktop-entry") (v "0.2.2") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "1j1xj0ffrdbbvmnw7xx511ml1422ah3m01dg6gzff37rfaw3lb4z") (y #t)))

(define-public crate-freedesktop-desktop-entry-0.2.3 (c (n "freedesktop-desktop-entry") (v "0.2.3") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "0pwdd40wnb5p6jid7w55klhrv768m4izwh6057ggwdxhi8prwrq7") (y #t)))

(define-public crate-freedesktop-desktop-entry-0.2.4 (c (n "freedesktop-desktop-entry") (v "0.2.4") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (d #t) (k 0)))) (h "0r9bpfnpg8jv0rnv6rypq655sgbpvm3a3ny00lzj09nnn87f02vd")))

(define-public crate-freedesktop-desktop-entry-0.3.0 (c (n "freedesktop-desktop-entry") (v "0.3.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "1fnj4nzxgl917a692nj93gnlm7nn4lifx2gvdbp197pcr06i1q2k")))

(define-public crate-freedesktop-desktop-entry-0.3.1 (c (n "freedesktop-desktop-entry") (v "0.3.1") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r9krcp3mgkc8ry4pdwza6nylx4kwvqbvyspzq97cjrk1fkl6hzl")))

(define-public crate-freedesktop-desktop-entry-0.3.2 (c (n "freedesktop-desktop-entry") (v "0.3.2") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qk6k5wiiwmp2ri0g1ldb37p01lfmqrrmqs1q6h0dvvjj4md69bq")))

(define-public crate-freedesktop-desktop-entry-0.3.3 (c (n "freedesktop-desktop-entry") (v "0.3.3") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pdg613glh3j7v433q9m29gbws2z90kf37j4r2qvh6a2bsagx07h")))

(define-public crate-freedesktop-desktop-entry-0.4.0 (c (n "freedesktop-desktop-entry") (v "0.4.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "gettext-rs") (r "^0.7") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1klarkyjm1g3p3pbmkfkwjmnjwf4pbbjx379lshjrwgz7gvb5v31")))

(define-public crate-freedesktop-desktop-entry-0.4.1 (c (n "freedesktop-desktop-entry") (v "0.4.1") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "gettext-rs") (r "^0.7") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bmi7d8rxliq0h0sfam9648i415w98hbnk5mbjbzmi3xba7s2xxz")))

(define-public crate-freedesktop-desktop-entry-0.5.0 (c (n "freedesktop-desktop-entry") (v "0.5.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "gettext-rs") (r "^0.7") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xdg") (r "^2.4.0") (d #t) (k 0)))) (h "0abdzj05yy7hjhdxb6yvfa7mzbvqdc5l70j3b4zizs15lxsp25a5")))

(define-public crate-freedesktop-desktop-entry-0.5.1 (c (n "freedesktop-desktop-entry") (v "0.5.1") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "gettext-rs") (r "^0.7") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xdg") (r "^2.4.0") (d #t) (k 0)))) (h "05sbsm4yxi1rc12kddypvsyfz0f3ycwyrpv55d6x13fqlfqqjzr8")))

(define-public crate-freedesktop-desktop-entry-0.5.2 (c (n "freedesktop-desktop-entry") (v "0.5.2") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "gettext-rs") (r "^0.7") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xdg") (r "^2.4.0") (d #t) (k 0)))) (h "1l7a3860g822kvwhw2dpwcbldzv42r14hnr6hpz0cmgvv96l80f2")))

