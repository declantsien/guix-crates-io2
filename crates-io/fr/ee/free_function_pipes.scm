(define-module (crates-io fr ee free_function_pipes) #:use-module (crates-io))

(define-public crate-free_function_pipes-1.0.0 (c (n "free_function_pipes") (v "1.0.0") (h "0j4sbjrd2fakxpwm51csl1p5pk27cjgai7ikyfb1zkysxsq3rn87")))

(define-public crate-free_function_pipes-1.0.1 (c (n "free_function_pipes") (v "1.0.1") (h "02nlgzs6jsyna1jvm3fg5c9wdcwfk9xwlbva988i8ikl8jzmg28y")))

