(define-module (crates-io fr ee freebsd-embedded-hal) #:use-module (crates-io))

(define-public crate-freebsd-embedded-hal-0.1.0 (c (n "freebsd-embedded-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.6") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "1x8g5z754ikncwjj8rw8avk1wrvw7sfnlfprfz9n2kaavjdlsrbf")))

(define-public crate-freebsd-embedded-hal-0.1.1 (c (n "freebsd-embedded-hal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.6") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "0k0y8mhrzc3j1rzrymwqgshk023zvv7hax3ja29lcsw6yqa3iv6m")))

(define-public crate-freebsd-embedded-hal-0.1.2 (c (n "freebsd-embedded-hal") (v "0.1.2") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.6") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "1h99ifcws5n21qxjpnh99lc1acxhdjwz6vpqqy2k26frrwj7wj4l")))

