(define-module (crates-io fr ee freebsd-errno) #:use-module (crates-io))

(define-public crate-freebsd-errno-1.0.0 (c (n "freebsd-errno") (v "1.0.0") (d (list (d (n "posix-errno") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1h22q2hc7c7d3d8a0jirqc5kqw2kf1aqy9s9lwvkfgrfl93wl5s6") (f (quote (("default" "posix-traits")))) (s 2) (e (quote (("posix-traits" "dep:posix-errno"))))))

