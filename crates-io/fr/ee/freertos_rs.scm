(define-module (crates-io fr ee freertos_rs) #:use-module (crates-io))

(define-public crate-freertos_rs-0.1.0 (c (n "freertos_rs") (v "0.1.0") (h "1n2cvg6cpag0ph4pv7chim7f7vrq2y6ddm0127g32f4v5yhybjlk")))

(define-public crate-freertos_rs-0.2.0 (c (n "freertos_rs") (v "0.2.0") (h "0s5cblsl215sas57hyaa9dw3phxxzmdqigxbs4syl37h11hgv1fi")))

(define-public crate-freertos_rs-0.3.0 (c (n "freertos_rs") (v "0.3.0") (h "07wlj4hk3x7rhsr50f6yq9b1m964z92ivk51ng17kickzrqy4vff")))

