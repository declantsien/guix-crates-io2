(define-module (crates-io fr ee freestuffapi) #:use-module (crates-io))

(define-public crate-freestuffapi-0.1.1 (c (n "freestuffapi") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full" "net"))) (d #t) (k 2)))) (h "1r77frgaarf4nsh0w0bn4gwnhr6m700iwb290w81j3dsngdycw1p")))

(define-public crate-freestuffapi-0.1.2 (c (n "freestuffapi") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full" "net"))) (d #t) (k 2)))) (h "1g95adsb5ryyjlxn6m2vp7a2q8nsaqfcx65zmiqwyh13fl9g3m89")))

(define-public crate-freestuffapi-0.1.3 (c (n "freestuffapi") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full" "net"))) (d #t) (k 2)))) (h "0vygc4c2x02vfpxm5qmfhs5ki8sm76kh3lab3v93phv2ik4cg3rm") (f (quote (("default" "client")))) (s 2) (e (quote (("client" "dep:reqwest"))))))

