(define-module (crates-io fr ee freeimage-sys) #:use-module (crates-io))

(define-public crate-freeimage-sys-0.0.1 (c (n "freeimage-sys") (v "0.0.1") (h "0d0xdrd2zc6q4b76ys9qvwmhdv2ww01qx1skmczczsfdjm89vyn3")))

(define-public crate-freeimage-sys-3.18.0 (c (n "freeimage-sys") (v "3.18.0") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)) (d (n "fs_extra") (r "^1") (d #t) (k 1)))) (h "0zswnpqs9dar2jww3ib02bm14fnlngs9hzij5qs5gg4g3z678ds6") (l "freeimage")))

(define-public crate-freeimage-sys-3.18.1 (c (n "freeimage-sys") (v "3.18.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)) (d (n "fs_extra") (r "^1") (d #t) (k 1)))) (h "1xwb0pg6clcvskn88lpbm9kq3f8rvzdi8i4dlk2f41s3rc4vyzkm") (l "freeimage")))

(define-public crate-freeimage-sys-3.18.2 (c (n "freeimage-sys") (v "3.18.2") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)) (d (n "fs_extra") (r "^1") (d #t) (k 1)))) (h "1qwq3ncrw0224sim9rc427b85py84h6sgh7rs396wsw616n95c2v") (l "freeimage")))

(define-public crate-freeimage-sys-3.18.3 (c (n "freeimage-sys") (v "3.18.3") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(windows)") (k 1)) (d (n "fs_extra") (r "^1") (d #t) (k 1)))) (h "1imzjaci99c8lhzhz9m0bblf0mzv10k300g7vrnbci6wga93qn43") (l "freeimage")))

