(define-module (crates-io fr ee free-flight-stabilization) #:use-module (crates-io))

(define-public crate-free-flight-stabilization-0.1.0 (c (n "free-flight-stabilization") (v "0.1.0") (d (list (d (n "fixed") (r "^1.27.0") (f (quote ("num-traits"))) (d #t) (k 2)) (d (n "libc") (r "^0.2.154") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "piddiy") (r "^0.1.1") (d #t) (k 0)))) (h "10ciz7nx99wdcdihz1qc154shcnpw3am9fd7bzxy0mi6pm1g4mp2")))

