(define-module (crates-io fr ee free-ranges) #:use-module (crates-io))

(define-public crate-free-ranges-0.1.0 (c (n "free-ranges") (v "0.1.0") (h "18b1s4y63ql95b5c650isik10q7haapjydcnlcly65zgskci9yv5")))

(define-public crate-free-ranges-0.1.1 (c (n "free-ranges") (v "0.1.1") (h "10c98s0n2g2s8vgahfrb24laal2wn30ig3kwgq3nbwws22bck1pw")))

(define-public crate-free-ranges-0.1.2 (c (n "free-ranges") (v "0.1.2") (h "1rbaqjy11xggryv6fprf8ri6cbdjnz09z3dzkmv0z6xanmis4gsp")))

(define-public crate-free-ranges-0.1.3 (c (n "free-ranges") (v "0.1.3") (h "188c24gm2zhgxgfp2n11varfmvn8zwkx19wsb1rvc1m4k14jaz3q")))

(define-public crate-free-ranges-1.0.0 (c (n "free-ranges") (v "1.0.0") (h "04xnxr18wqrlnaxblhbmlqs8vvxn7dvnmndm9ld7awn298z2pcxh")))

(define-public crate-free-ranges-1.0.1 (c (n "free-ranges") (v "1.0.1") (h "1cg8xnkalx7n5y3pf8xpr78cbh7jb4x3hk0hw9p4r6sy4h8ar68y")))

(define-public crate-free-ranges-1.0.2 (c (n "free-ranges") (v "1.0.2") (h "0vxsdnz6xn7i781xv2s3ipd2ib3y7na67w43vibys6681xg6bxwg")))

(define-public crate-free-ranges-1.0.3 (c (n "free-ranges") (v "1.0.3") (h "0f685na07knjijw50j6l3jyqjw4vafac4c6m68nvbcnzi4dsgcgl")))

(define-public crate-free-ranges-1.0.4 (c (n "free-ranges") (v "1.0.4") (h "1hyk32dh9j4nh2dmnmq72hc43mg1fngw2sxrhr0px1r7p6kz7c0j")))

(define-public crate-free-ranges-1.0.5 (c (n "free-ranges") (v "1.0.5") (h "1w2k1zn59avkwvc07kbsfkhc1rs2qgd8y1vpn9ikk6w2r3sv6hl7")))

(define-public crate-free-ranges-1.0.6 (c (n "free-ranges") (v "1.0.6") (h "0vdg558k5d01injyjx0h1fwh1bjwv0c5gx53bm9wba008n7f7gsl")))

(define-public crate-free-ranges-1.0.7 (c (n "free-ranges") (v "1.0.7") (h "00lc6iws1svas7p1ar8sxck64lirrvffrd1gqyc4mv6l7sq4jkg5")))

