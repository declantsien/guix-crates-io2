(define-module (crates-io fr ee freeform) #:use-module (crates-io))

(define-public crate-freeform-0.1.0 (c (n "freeform") (v "0.1.0") (d (list (d (n "bevy_reflect") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "typed_key") (r "^0.1.1") (d #t) (k 0)))) (h "1cwwy122as54rcyzw5h205b5x9z7wwpcgzp3fzl485hmmpwkjg25")))

(define-public crate-freeform-0.2.0 (c (n "freeform") (v "0.2.0") (d (list (d (n "bevy_reflect") (r "^0.13.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "typed_key") (r "^0.1.1") (d #t) (k 0)))) (h "1g2bp1gfxblwcb62l6pydjlbk35w8p0qap7lwqdyiapkb5x4lwsa")))

(define-public crate-freeform-0.3.0 (c (n "freeform") (v "0.3.0") (d (list (d (n "bevy_reflect") (r "^0.13.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "typed_key") (r "^0.1.1") (d #t) (k 0)))) (h "0q7hc6z8ygkh4dw7d303cz98rdk1c8xc352b842a0jjjb0m87r6v") (f (quote (("default" "json")))) (s 2) (e (quote (("toml" "dep:toml") ("ron" "dep:ron") ("json" "dep:serde_json"))))))

