(define-module (crates-io fr ee freetype-rs) #:use-module (crates-io))

(define-public crate-freetype-rs-0.0.0 (c (n "freetype-rs") (v "0.0.0") (d (list (d (n "freetype-sys") (r "^0.0.1") (d #t) (k 0)))) (h "034py0dn6iqfd4lyf9ab6zp980p3mqmawj86hlpyqpj5jdrgig3y")))

(define-public crate-freetype-rs-0.0.3 (c (n "freetype-rs") (v "0.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0wg92knlmflf2nv3j1hzvkjbmkf4hd5daxwlmgq8p959nkpx0bk9")))

(define-public crate-freetype-rs-0.0.4 (c (n "freetype-rs") (v "0.0.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0lplqmpgs64n9vzb1npbn5m0dzbj4gxp7fab1jmn1pbydsc867n1")))

(define-public crate-freetype-rs-0.0.5 (c (n "freetype-rs") (v "0.0.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.0.2") (d #t) (k 0)))) (h "08b9sxx4bl57a15hspg5x2qv5nlmqxp1nhxap4jwv60zavsgbl9y")))

(define-public crate-freetype-rs-0.0.6 (c (n "freetype-rs") (v "0.0.6") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1zwry1x5q5pfck8y35dwx8h17vvp65cv22nzvhmzcmv1kqm10xyj")))

(define-public crate-freetype-rs-0.0.7 (c (n "freetype-rs") (v "0.0.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "piston") (r "^0.0.7") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.0.23") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.0.5") (d #t) (k 2)))) (h "048788spl309zyyylmramyfcclbsnjsyjkq2yyymj8xiyfh8kqf1")))

(define-public crate-freetype-rs-0.0.8 (c (n "freetype-rs") (v "0.0.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "12jnkwn3k2mchr0vb098w9k7igxcflimrjqwcsxzbjdpxfnrcckr")))

(define-public crate-freetype-rs-0.0.9 (c (n "freetype-rs") (v "0.0.9") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "freetype-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "149sdzv37xs2xfph4zvbm2jjg61nycym4jc0x15z28h7ggb8wnrk")))

(define-public crate-freetype-rs-0.0.10 (c (n "freetype-rs") (v "0.0.10") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "1bj5m2nc7naw7mjffmdxqragxak4jd3dsradslcyc0z2vv5n445x")))

(define-public crate-freetype-rs-0.1.0 (c (n "freetype-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "00z6s8ljnmj3mp94rwgv0zkzcbj2hiyjwr2fp7h2a23ibsj9dcxq")))

(define-public crate-freetype-rs-0.1.1 (c (n "freetype-rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0d83g8a6p7wvpdmld4707zw18r2ka28873dfh16j85p71knws79d")))

(define-public crate-freetype-rs-0.1.2 (c (n "freetype-rs") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "02p6cdn2np874hq4mxzvd1swrwwadl2168xy7dhxnrsqhcz84k9a")))

(define-public crate-freetype-rs-0.1.3 (c (n "freetype-rs") (v "0.1.3") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0snwmgdhz3qn7xn1wimb8jmd2f8n01466n0cmpspkb99fp3is5aw")))

(define-public crate-freetype-rs-0.2.0 (c (n "freetype-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1fq45kymiqjvgv52z9znrk71mgm4hf6lwl832gw43bjx5q7yspk4")))

(define-public crate-freetype-rs-0.2.1 (c (n "freetype-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1zic822b88ilrj582n105ci3sgn87wnd6gfyga3fvg7l5zz3iv14")))

(define-public crate-freetype-rs-0.3.0 (c (n "freetype-rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1jlkaihygw1cmgfia89iiq1s4a7k9kcxkxw1sb1sj8c1jc7rs1vw")))

(define-public crate-freetype-rs-0.3.1 (c (n "freetype-rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "15rzwsagvg3772jxasl1j38nc0abqwkc6a6fbwha9sy5qh7a0ybr")))

(define-public crate-freetype-rs-0.4.0 (c (n "freetype-rs") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0ka0zrgs7rizp47na687wmnc924cs3w4vzj17d0zqycx26xk11hn")))

(define-public crate-freetype-rs-0.4.1 (c (n "freetype-rs") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0bafkl9imkd8ihy8apjgybfnam8qc6654j4qd535iclch13xs016")))

(define-public crate-freetype-rs-0.4.2 (c (n "freetype-rs") (v "0.4.2") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1ylhyla30qq06d0x8i8fj1h9sy800if6qafjzmxi0s4fpb1hj6hr")))

(define-public crate-freetype-rs-0.5.0 (c (n "freetype-rs") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "17qvc4pld3a6gd2zb0lbdmwc43bbjcldy4911czq52zlk53ysaiq")))

(define-public crate-freetype-rs-0.6.0 (c (n "freetype-rs") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1qfz6bfj82w4987vmvzzzs6n7ksgy7n8fkwjcqr7ixy176r7xa9w")))

(define-public crate-freetype-rs-0.6.1 (c (n "freetype-rs") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0bvbdwrxyhvchdp301jqzryn0za3dp41wzqqapq6dmd2aa906cdq")))

(define-public crate-freetype-rs-0.7.0 (c (n "freetype-rs") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1zw9m4210svscazyvk64jryml4qyxbm0vvl8qsmm7p0s5n57slzd")))

(define-public crate-freetype-rs-0.8.0 (c (n "freetype-rs") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "14j1l8qzp3bd07zzhindj5ji5frbbvcjf3bjy7s3xmwhc9cncd4n")))

(define-public crate-freetype-rs-0.9.0 (c (n "freetype-rs") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.6.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0hma56079aqp00kfdhw6kj4hvcm98kx0c0ldnlm8cmmi86hr730y")))

(define-public crate-freetype-rs-0.10.0 (c (n "freetype-rs") (v "0.10.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0qyr0ncahmssddpnb3dhylf371fas456hi9bfam0rhn2n5dl7kjk")))

(define-public crate-freetype-rs-0.11.0 (c (n "freetype-rs") (v "0.11.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1xjfbl4nbbm3g3acx4ykbqf0ihnb5nlva8vpivc5m6mh8gyckq2j")))

(define-public crate-freetype-rs-0.12.0 (c (n "freetype-rs") (v "0.12.0") (d (list (d (n "bitflags") (r "^0.8.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1r225fwga21s1jq811b1bwja365brnadywhd9lb6ky9c5wbi7wwz")))

(define-public crate-freetype-rs-0.13.0 (c (n "freetype-rs") (v "0.13.0") (d (list (d (n "bitflags") (r "^0.8.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1ii0r4xlwv12f56dsc1qb2g68pngn9a0maf133z8xv2z0lm8whd1")))

(define-public crate-freetype-rs-0.14.0 (c (n "freetype-rs") (v "0.14.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1m94j6f9cy06y4r8xarlmpswp3nscrq3gxg8gf60w4vq8xlkpa45")))

(define-public crate-freetype-rs-0.15.0 (c (n "freetype-rs") (v "0.15.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "17ry9db71m3c3vypwg5apvzpjmkdcr85ad6sxyf8q2sg5x00rdfz")))

(define-public crate-freetype-rs-0.16.0 (c (n "freetype-rs") (v "0.16.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0nmk90kzxn1xvsx20bm982hs17ifql7xaqhs343b3hacfnw3rs68")))

(define-public crate-freetype-rs-0.17.0 (c (n "freetype-rs") (v "0.17.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1zzax3l0p1slm13m946cdkkckaarfadjfkyam86vp37vj4nnjigm")))

(define-public crate-freetype-rs-0.18.0 (c (n "freetype-rs") (v "0.18.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0hhxj46gv84fxf2673p40dllfmz0cp9kn6vamg6bclnpasgj0faf")))

(define-public crate-freetype-rs-0.19.0 (c (n "freetype-rs") (v "0.19.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "15y23d731rr9c4a9l1h1dbmzmx5n4r9a3j1lpbpmgik367krkyy3")))

(define-public crate-freetype-rs-0.19.1 (c (n "freetype-rs") (v "0.19.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "01g78ib2n1sdrbc6hp08nqgdrbl3k8zjd7i7whqvdrqf0jkr5k18")))

(define-public crate-freetype-rs-0.20.0 (c (n "freetype-rs") (v "0.20.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0ryn5icawjjxmpi9l7qg90wa5hkdabzx8bap0qrcywmjfnynlrn3")))

(define-public crate-freetype-rs-0.21.0 (c (n "freetype-rs") (v "0.21.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1hc0wz79vz6r0d3jybyzpqds9yv0147sj13dnxwmqp8kfk8q1bm8")))

(define-public crate-freetype-rs-0.22.0 (c (n "freetype-rs") (v "0.22.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1iwsba7g5j7mlrl55zq8val75d3dypzgp4i23j32dakx0cwdmhwf")))

(define-public crate-freetype-rs-0.23.0 (c (n "freetype-rs") (v "0.23.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "06yn6l44wad0h0i4nzs5jfq64zgf89xr01fy1w22i90j22ilnkmd")))

(define-public crate-freetype-rs-0.24.0 (c (n "freetype-rs") (v "0.24.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0ps47himb8ygi5hzxbhlqcni1ins68pa0h1qgg1433ywghi1j19l")))

(define-public crate-freetype-rs-0.25.0 (c (n "freetype-rs") (v "0.25.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1bpixg61l8hq35xmzd6rlynj3smicgyg7fj28zljn1fpagwpqs2y")))

(define-public crate-freetype-rs-0.26.0 (c (n "freetype-rs") (v "0.26.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.13.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1yzmbd73hlblbns0dqkcwfj54l97hx3yb0lqpda8rhm5s34xxskl")))

(define-public crate-freetype-rs-0.27.0 (c (n "freetype-rs") (v "0.27.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0lyc501s94pz4nbpy5fv23va9vzlm84zp5yds6qg2ix86iabg4wm")))

(define-public crate-freetype-rs-0.28.0 (c (n "freetype-rs") (v "0.28.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0m5wqd50xwcaj8xqm2g6aripjmdkcji0jxlck2cf3cg3i85wm5la")))

(define-public crate-freetype-rs-0.29.0 (c (n "freetype-rs") (v "0.29.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0v1zz7cfv0w5f37za07rgpppbfkf3s60fy2s81w8mnh19ig6zqkk")))

(define-public crate-freetype-rs-0.29.1 (c (n "freetype-rs") (v "0.29.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1gmgl8w7f29ryvmjjn63vqp9n5s5vdpslhdrx1asy5gdbfr8j1fb")))

(define-public crate-freetype-rs-0.30.0 (c (n "freetype-rs") (v "0.30.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0dhc4rd96lvli91r85gsr5sqiz0ph3jnj27dw34n7vwrx09smr51")))

(define-public crate-freetype-rs-0.30.1 (c (n "freetype-rs") (v "0.30.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1lbrxyaqvj3ddby51clvqsxdidpsldzv36rlx6wjgrmddcrlw2a3")))

(define-public crate-freetype-rs-0.31.0 (c (n "freetype-rs") (v "0.31.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1fk01dza1i6lzripvg931xgdm1lzgxsc364hzynbbm16vb3da022")))

(define-public crate-freetype-rs-0.32.0 (c (n "freetype-rs") (v "0.32.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1q54jimjgzwdb3xsp7rsvdmp6w54cak7bvc379mdabc2ciz3776m")))

(define-public crate-freetype-rs-0.33.0 (c (n "freetype-rs") (v "0.33.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.18.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1flnyh541j28vdl8rnncm0j0n7dx94kjzblqxrmyp5h6ycbrvg77")))

(define-public crate-freetype-rs-0.34.0 (c (n "freetype-rs") (v "0.34.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.19.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0liids9vdbwmypp0lp300wlnr673fv76vky128vzidvz75d53pc5")))

(define-public crate-freetype-rs-0.34.1 (c (n "freetype-rs") (v "0.34.1") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.19.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0daxcbcxnmraph6v5mzwvy22r78ck6wi6wd2f93xba7swikjafdi")))

(define-public crate-freetype-rs-0.35.0 (c (n "freetype-rs") (v "0.35.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.20.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1gzfb9fax3d3s691ys99nfihpzwl7hacvxnwvlxg4sph1fzd5ymi")))

(define-public crate-freetype-rs-0.36.0 (c (n "freetype-rs") (v "0.36.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.20.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "05rbdl18ks2rb0kz7giwpcv7k1hfg19hbp406l9h95m0dkixwhjl")))

