(define-module (crates-io fr ee freertos-sys2) #:use-module (crates-io))

(define-public crate-freertos-sys2-0.1.0 (c (n "freertos-sys2") (v "0.1.0") (h "1fcz0s4c3dknaqfz6jyczaa15y4v994rggq2laz03sd2iw39spwq")))

(define-public crate-freertos-sys2-0.2.0 (c (n "freertos-sys2") (v "0.2.0") (h "13is073k5jr36fwnn0qq1n65dw33193k7hscqdpw3ybigx8cdw86")))

(define-public crate-freertos-sys2-0.2.1 (c (n "freertos-sys2") (v "0.2.1") (h "1y9aqc2gvd020m6jc3bdibk8qx5984cm1whkppva6j6mzl0zsl8r")))

