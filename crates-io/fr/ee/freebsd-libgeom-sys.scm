(define-module (crates-io fr ee freebsd-libgeom-sys) #:use-module (crates-io))

(define-public crate-freebsd-libgeom-sys-0.1.0 (c (n "freebsd-libgeom-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1c25ky8bcdkambdkp7ngvjicblajcl29iq49ng9n9cq7z4w50im5")))

(define-public crate-freebsd-libgeom-sys-0.1.1 (c (n "freebsd-libgeom-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "147hgv3ymhqmai13zcsshrn3kgp6kgxwni5a73zmnj8h0q354n6l")))

(define-public crate-freebsd-libgeom-sys-0.1.2 (c (n "freebsd-libgeom-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.0") (d #t) (k 1)))) (h "1jm72hfy8zw17gyz7bjpn0hyyb0yfhpbsqvx5dlkr3vw7j6yah9f")))

(define-public crate-freebsd-libgeom-sys-0.1.3 (c (n "freebsd-libgeom-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "regex") (r "^1.5.1") (d #t) (k 1)))) (h "0939cw5xc2qndv8pishkxf8nvlwz9pc21qqiysxshx66c7j27l25")))

(define-public crate-freebsd-libgeom-sys-0.1.4 (c (n "freebsd-libgeom-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0a1lzjcmiz5q4fs2w591yhg4jgsnwrirdy9dm9avkhb7ncwrq18f")))

(define-public crate-freebsd-libgeom-sys-0.1.5 (c (n "freebsd-libgeom-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0njdvmb2ix7im5jk3j1hhm88zjzgjnjfv082wzz7nlaaqmqm8fwi")))

(define-public crate-freebsd-libgeom-sys-0.1.6 (c (n "freebsd-libgeom-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0vb1lrirx7fbrn5swa384xbb46lw1y67ba960bc5rr81w66svlyk")))

