(define-module (crates-io fr ee freetype) #:use-module (crates-io))

(define-public crate-freetype-0.1.0 (c (n "freetype") (v "0.1.0") (h "1a6l0a6zcvzz2whcx9x9lhcvzlhv1ny5ninqn4jm0jm06q8ivyq6")))

(define-public crate-freetype-0.1.1 (c (n "freetype") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^4.0.2") (d #t) (k 0)))) (h "195k2hinkmpsnq3rh3pwnhay9ds3av95k4gi4n9qp2swmnyz86ms")))

(define-public crate-freetype-0.1.2 (c (n "freetype") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^4.0.2") (o #t) (d #t) (k 0)))) (h "1ldc6gx2k14f6f9hs95ilymd4jbm4wbcnmjw977n4mqqmwz5d2ba") (f (quote (("default" "servo-freetype-sys"))))))

(define-public crate-freetype-0.1.3 (c (n "freetype") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^4.0.2") (o #t) (d #t) (k 0)))) (h "1r99lsm9y2zdhckbjybqdvfz50xwx5jqsnk3l3hx2b2aqfmj6aw1") (f (quote (("default" "servo-freetype-sys"))))))

(define-public crate-freetype-0.2.0 (c (n "freetype") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^4.0.2") (o #t) (d #t) (k 0)))) (h "1bszcdfnd8kvwz2flpbj9clgbh2f2xqwn1pcxw55gr47qrr35qpx") (f (quote (("default" "servo-freetype-sys"))))))

(define-public crate-freetype-0.3.0 (c (n "freetype") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^4.0.2") (o #t) (d #t) (k 0)))) (h "1rxh77p39y468nxhq3l61s7zcg1b01pq1adcam6ii628i08qm2rr") (f (quote (("default" "servo-freetype-sys"))))))

(define-public crate-freetype-0.4.0 (c (n "freetype") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^4.0.2") (o #t) (d #t) (k 0)))) (h "0gchiqyz2l1fp9abwvpvrx2pi4vi5gy0kyfpmxszwf3kg9dyfndn") (f (quote (("default" "servo-freetype-sys"))))))

(define-public crate-freetype-0.4.1 (c (n "freetype") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "servo-freetype-sys") (r "^4.0.2") (o #t) (d #t) (k 0)))) (h "0a70x03n68997f08bi3n47q9wyi3pv5s9v4rjc79sihb84mnp4hi") (f (quote (("default" "servo-freetype-sys"))))))

(define-public crate-freetype-0.5.0 (c (n "freetype") (v "0.5.0") (d (list (d (n "freetype-sys") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dy5kgzjb36qi0jdd6ma3mxypr06rph7gkz7h5b4mh5zfxd3x1vp") (f (quote (("default" "freetype-sys"))))))

(define-public crate-freetype-0.5.1 (c (n "freetype") (v "0.5.1") (d (list (d (n "freetype-sys") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1masbh60c85mksq95klzy5yd8kv1ipd5hchw1vz6bbfr6amj4cmp") (f (quote (("default" "freetype-sys"))))))

(define-public crate-freetype-0.6.0 (c (n "freetype") (v "0.6.0") (d (list (d (n "freetype-sys") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19xyjvan5d1n3d08nrva4nd7plgmcbcyzmaq24xk95r0g8yzy1wg") (f (quote (("default" "freetype-sys"))))))

(define-public crate-freetype-0.7.0 (c (n "freetype") (v "0.7.0") (d (list (d (n "freetype-sys") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ijxyd8isq0w7qkbhp7p1y536xg3d8b8vy5ljg31rnz3m5w87qxy") (f (quote (("default" "freetype-sys"))))))

(define-public crate-freetype-0.7.1 (c (n "freetype") (v "0.7.1") (d (list (d (n "freetype-sys") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10sx1zwq2q3mcyl0ds1irbsqmn57iygixiw6xgnzibbq62d5kj7g") (f (quote (("default" "freetype-sys"))))))

(define-public crate-freetype-0.7.2 (c (n "freetype") (v "0.7.2") (d (list (d (n "freetype-sys") (r "^0.20.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vl0lyrz3wswch06i34sxadyzb443qapgf6fjd48wyb3w140fi2s") (f (quote (("default" "freetype-sys"))))))

