(define-module (crates-io fr ee freezable) #:use-module (crates-io))

(define-public crate-freezable-0.1.0 (c (n "freezable") (v "0.1.0") (h "0qss51rrsa5xw44caahw9k30q6dg4dabvll43cl9sq6dklwc7k6s") (f (quote (("std") ("default" "std"))))))

(define-public crate-freezable-0.1.1 (c (n "freezable") (v "0.1.1") (h "0q1m153a75cm94v3ssrb1x3s9gjkallh6x00m4n7z969w6n56q82") (f (quote (("std") ("default" "std"))))))

(define-public crate-freezable-0.1.2 (c (n "freezable") (v "0.1.2") (h "0klnhdd1q14zx6sfcbdxjsxslycjlkf27jqib3jcja4jkxm37f60") (f (quote (("std") ("default" "std"))))))

(define-public crate-freezable-0.1.3 (c (n "freezable") (v "0.1.3") (h "0g6ag697vizlq6nlhjp21mql62f0i4zcg5xdslhkbxd5vv72hd8f") (f (quote (("std") ("default" "std"))))))

(define-public crate-freezable-0.1.4 (c (n "freezable") (v "0.1.4") (h "0anvlb6hzzal36c8x6j7sqh23qkv85n2ylqrwxxqif1n11r10fmk") (f (quote (("std") ("default" "std"))))))

(define-public crate-freezable-0.1.5 (c (n "freezable") (v "0.1.5") (h "19nnxc03zh3dcxax8mc3w6c6lk83w5wzdg7gaya05y48gcxwks8i") (f (quote (("std") ("default" "std"))))))

(define-public crate-freezable-0.1.6 (c (n "freezable") (v "0.1.6") (h "0gdq9004ljpc4dqh5vqbv7vyswag54jp6kmqnv9raiwc0zsi4b1n") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-freezable-0.1.7 (c (n "freezable") (v "0.1.7") (h "1i33w2lzr4ir37wk9bah7wg6z8jaks3hp4vfcariyn4cgg5dfk4f") (f (quote (("std") ("default" "std"))))))

(define-public crate-freezable-0.1.8 (c (n "freezable") (v "0.1.8") (h "09ssqxq7dsml6zcv77yan0df42cllw6scxqyggvbia6hblzgx3m6") (f (quote (("std") ("default" "std"))))))

