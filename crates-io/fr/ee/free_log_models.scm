(define-module (crates-io fr ee free_log_models) #:use-module (crates-io))

(define-public crate-free_log_models-0.1.0 (c (n "free_log_models") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_trace"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1xp73s9i6n7q87dkaqi73w02ir6dky4qr1xxs7n651nkka43d4ra") (f (quote (("fail-on-warnings"))))))

(define-public crate-free_log_models-0.1.1 (c (n "free_log_models") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("release_max_level_trace"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1adbd07g0vjmzypah3zarxhx3cgbkrdvr0zwasks3xvpg2xiibkc") (f (quote (("fail-on-warnings"))))))

