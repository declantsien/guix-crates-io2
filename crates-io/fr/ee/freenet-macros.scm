(define-module (crates-io fr ee freenet-macros) #:use-module (crates-io))

(define-public crate-freenet-macros-0.0.4 (c (n "freenet-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ya2id7k0d55wivb3z0yb2yzivbfwalnqdwswip7psdy8ddsr7rf") (r "1.71.1")))

