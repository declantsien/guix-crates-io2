(define-module (crates-io fr ee freertos-sys) #:use-module (crates-io))

(define-public crate-freertos-sys-0.1.1 (c (n "freertos-sys") (v "0.1.1") (d (list (d (n "cmsis-rtos2") (r "^0.1.1") (d #t) (k 0)))) (h "0r862dw2kcfm047bxh7blr2b3wwkj1r6mgpbd0nrdbs5p6zn5rnq") (f (quote (("stm32h7x" "family-selected") ("stm32f7x" "family-selected") ("stm32f4x" "family-selected") ("stm32f3x" "family-selected") ("family-selected")))) (l "cmsis_rtos2")))

(define-public crate-freertos-sys-0.1.2 (c (n "freertos-sys") (v "0.1.2") (d (list (d (n "cmsis-rtos2") (r "^0.1.1") (d #t) (k 0)))) (h "0w2hw29aqcy2raafwm4m3wm2khs6n1q4ygwfan304mlqi7bvcf80") (f (quote (("stm32h7x" "family-selected") ("stm32f7x" "family-selected") ("stm32f4x" "family-selected") ("stm32f3x" "family-selected") ("family-selected") ("dbgsym")))) (l "cmsis_rtos2")))

(define-public crate-freertos-sys-0.1.3 (c (n "freertos-sys") (v "0.1.3") (d (list (d (n "cmsis-rtos2") (r "^0.1.1") (d #t) (k 0)))) (h "1c5w1fxkjx20jx9gh8drr2qdi1z3p1brfvms7k4q36mvjhgjmhn2") (f (quote (("stm32h7x" "family-selected") ("stm32f7x" "family-selected") ("stm32f4x" "family-selected") ("stm32f3x" "family-selected") ("family-selected") ("dbgsym")))) (l "cmsis_rtos2")))

