(define-module (crates-io i_ sh i_shape) #:use-module (crates-io))

(define-public crate-i_shape-0.1.0 (c (n "i_shape") (v "0.1.0") (d (list (d (n "i_float") (r "^0.1.0") (d #t) (k 0)))) (h "099fls47yjzw99akvkgqc0bf08jnih25rngq34la95j8ajdmid4g")))

(define-public crate-i_shape-0.1.1 (c (n "i_shape") (v "0.1.1") (d (list (d (n "i_float") (r "^0.1.0") (d #t) (k 0)))) (h "0v6r07kckyq301mbs5kmblaxqvl6hcf402gidmqy0phng54c1h77")))

(define-public crate-i_shape-0.1.2 (c (n "i_shape") (v "0.1.2") (d (list (d (n "i_float") (r "^0.1.0") (d #t) (k 0)))) (h "0fl3w70w2mba55ip3ckxd5qrd67lacgmhb0q2g0j0dvv7wg2qgz8")))

(define-public crate-i_shape-0.1.3 (c (n "i_shape") (v "0.1.3") (d (list (d (n "i_float") (r "^0.1.1") (d #t) (k 0)))) (h "1lhlazk6qbz59xvssvvq21gx89w9jaxca8k1zmrbkafwsl0drc8i")))

(define-public crate-i_shape-0.2.0 (c (n "i_shape") (v "0.2.0") (d (list (d (n "i_float") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ilw5027kwv8fxdl292q0ri9nirhi7pyhldb8v69fwr8hi4zaxwg")))

(define-public crate-i_shape-0.3.0 (c (n "i_shape") (v "0.3.0") (d (list (d (n "i_float") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11a4331wwrlprrpzkx2m7h0zhj3k7v70hy1r6rih6cxrba7cxnwf")))

(define-public crate-i_shape-0.4.0 (c (n "i_shape") (v "0.4.0") (d (list (d (n "i_float") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xr1c2g135nygc92rypy96pin5s5n5pna9xd66ww8kq85avg66im")))

(define-public crate-i_shape-0.5.0 (c (n "i_shape") (v "0.5.0") (d (list (d (n "i_float") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yq94xq7pvn424ikdl2l2am1m3w2hxkbni07p8wfnj30rfcrif56")))

(define-public crate-i_shape-0.6.0 (c (n "i_shape") (v "0.6.0") (d (list (d (n "i_float") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pv6v9zghrc0f8bvxvhmm23kk9ayjy0j9dkbc8xgi26hddc9mgnv")))

(define-public crate-i_shape-0.7.0 (c (n "i_shape") (v "0.7.0") (d (list (d (n "i_float") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p1x212giwpk98gw606vbn8aryabfz1s4610qlg3dssn44wk9cd9")))

(define-public crate-i_shape-0.8.0 (c (n "i_shape") (v "0.8.0") (d (list (d (n "i_float") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g4p12inxsk1dwf8dd7wj1nqlafjjsj392shk3c0nhphyqx3sj6a")))

(define-public crate-i_shape-0.9.0 (c (n "i_shape") (v "0.9.0") (d (list (d (n "i_float") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "082qq2ifbpql23zrjp9dmx3nxr0sj5pk9v1bk45li4yn51jilbk3")))

(define-public crate-i_shape-0.10.0 (c (n "i_shape") (v "0.10.0") (d (list (d (n "i_float") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sfh2m3p5vlm0alasg28x4g4haqk3gc1xx3n57jwb0szm73incg6")))

(define-public crate-i_shape-0.11.0 (c (n "i_shape") (v "0.11.0") (d (list (d (n "i_float") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d39w3qmv38fmycbsakxh4j4pac2ih16xkwn9v79mmymvkvfdwa4")))

(define-public crate-i_shape-0.12.0 (c (n "i_shape") (v "0.12.0") (d (list (d (n "i_float") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mpzbql6b2hrfgwfdxsvf27p0yihlffsmchvjfpvhspg6xgcs2hq")))

(define-public crate-i_shape-0.13.0 (c (n "i_shape") (v "0.13.0") (d (list (d (n "i_float") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wp7azyy70hqhxrpc51z1bfzz0q112la404y5560ah06f2i7jmbs")))

(define-public crate-i_shape-0.14.0 (c (n "i_shape") (v "0.14.0") (d (list (d (n "i_float") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14ng7lxaiy64nhbx774pc7mhhs0ijmlmnaz77qml28dm0jdnj2n2")))

(define-public crate-i_shape-0.15.0 (c (n "i_shape") (v "0.15.0") (d (list (d (n "i_float") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kay18i95x856d0p8wgjxv1mdqim3s4sja74cirgla7mgq7hqs92")))

(define-public crate-i_shape-0.16.0 (c (n "i_shape") (v "0.16.0") (d (list (d (n "i_float") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02psclb2pcr63cvjwxzprfqm8ysaphdi7ivrhslyfhw0dd5ydcs7")))

(define-public crate-i_shape-1.0.0 (c (n "i_shape") (v "1.0.0") (d (list (d (n "i_float") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14g8hy9zvph3iwc94anivbfqji85fhq3nmazsiy2g4ca3lr81i3v")))

(define-public crate-i_shape-1.0.1 (c (n "i_shape") (v "1.0.1") (d (list (d (n "i_float") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xmzfyb4wwccyac18id0jcmlaz20vb8dgyj5mvcff90yawwlhvii")))

(define-public crate-i_shape-1.0.2 (c (n "i_shape") (v "1.0.2") (d (list (d (n "i_float") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ci6blwp9v4hr5kxv8fncmi2g8vgkfwkvjdldiy2py9pli8bxrh1")))

(define-public crate-i_shape-1.0.3 (c (n "i_shape") (v "1.0.3") (d (list (d (n "i_float") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sjdaa6j5x7lg896l8j1rbqmf6jaxfbwn5vd28xn68sbj6p5lcgl")))

