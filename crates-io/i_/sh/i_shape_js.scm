(define-module (crates-io i_ sh i_shape_js) #:use-module (crates-io))

(define-public crate-i_shape_js-0.1.0 (c (n "i_shape_js") (v "0.1.0") (d (list (d (n "i_float") (r "^0.1.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.2.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "119nbvc5fbfp8mbyl7as033z7clwck9x1lbbppkhsqz12av1pyrh")))

(define-public crate-i_shape_js-0.7.1 (c (n "i_shape_js") (v "0.7.1") (d (list (d (n "i_float") (r "^1.0.1") (d #t) (k 0)) (d (n "i_overlay") (r "^1.0.1") (d #t) (k 0)) (d (n "i_shape") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0ap1pc833cvjdm2y1mmixhkhrvzvj6hfr776yadpjhir9kd6n9ch")))

