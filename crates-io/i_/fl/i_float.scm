(define-module (crates-io i_ fl i_float) #:use-module (crates-io))

(define-public crate-i_float-0.1.0 (c (n "i_float") (v "0.1.0") (h "1bz9r8vhjc3jclqlpbgnxkc2axx4gpaihdgwz65zif8s075rr2j1")))

(define-public crate-i_float-0.1.1 (c (n "i_float") (v "0.1.1") (h "1l8plll5vplxvmcfkcim1wslfcpyinhpxf6kh6ccvxyn896a5np0")))

(define-public crate-i_float-0.2.0 (c (n "i_float") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ydda53qnsf44rjjwg3jd4va9hll5yp7nv15li6n99248wr8vcl6")))

(define-public crate-i_float-0.3.0 (c (n "i_float") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x2723yw8y43js2cs8c7qnzl7m33dd6c44zfvd1xavxz86i5ba85")))

(define-public crate-i_float-0.4.0 (c (n "i_float") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rr7gy3di837y9xa0bd536ly791vapmz9z7aqjmsddp06fy5mpla")))

(define-public crate-i_float-0.5.0 (c (n "i_float") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wdz3wm07x3k86cmcjfnhiigd7myvr6pqg4yp6ffv0lnxkrrgwrn")))

(define-public crate-i_float-0.6.0 (c (n "i_float") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1520idr7yvsml07gci4fgmd8bnx9x1bj1ywwh6cw40lq65jaqfib")))

(define-public crate-i_float-0.7.0 (c (n "i_float") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wld6xzmyb4vfqa72jav8j52zj8fpg0n1w4k4lbp3vkspwxvhf7b")))

(define-public crate-i_float-0.8.0 (c (n "i_float") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lrldlqfw3gxp3nbbpy94n6rk43xkl714qsj9glkali115ybzh85")))

(define-public crate-i_float-0.9.0 (c (n "i_float") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l2yxs267gwqwgjqaygybjgk011chj5hnivgman3vdks8rb0axg2")))

(define-public crate-i_float-0.10.0 (c (n "i_float") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gx3rq9f9kyvhzy1rqwbhmn5ljahmmpbcym4lfafqbhyz8i83ay8")))

(define-public crate-i_float-1.0.0 (c (n "i_float") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16a7sl4n1iqc1kvgxic36q24ry96wdxgklkyy5m0b1j8lxpfc7fr")))

(define-public crate-i_float-1.0.1 (c (n "i_float") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r7qanp7vjdjn2q8b1i5xsdlz5cnwjwm8a3g1c2f3xr5nga1660x")))

(define-public crate-i_float-1.0.3 (c (n "i_float") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cg704lqb507r9d7azy5r0z6cjihsjmwm6c1yh9iyr9r052v10my")))

(define-public crate-i_float-1.1.0 (c (n "i_float") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ja5x6d5zq7drwz1sx0ahhh2l5h88sriw65d28qkv2i16xzmizxn")))

