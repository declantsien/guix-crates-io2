(define-module (crates-io i_ tr i_triangle) #:use-module (crates-io))

(define-public crate-i_triangle-0.1.2 (c (n "i_triangle") (v "0.1.2") (d (list (d (n "i_float") (r "^0.1.1") (d #t) (k 0)) (d (n "i_overlay") (r "^0.4.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1gklqq29skfgf5wcqcad4nhfs44sf7fwq3m5yp2vckl1q1zd8bdq")))

(define-public crate-i_triangle-0.2.0 (c (n "i_triangle") (v "0.2.0") (d (list (d (n "i_float") (r "^0.2.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.5.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "099w0s9xj9p18lz9slafc5634dj26q6rl7mxdcr1akrk8k48mhk7")))

(define-public crate-i_triangle-0.3.0 (c (n "i_triangle") (v "0.3.0") (d (list (d (n "i_float") (r "^0.2.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.6.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13jzyvnrr1ipgcg15iaj53rdw7klr46qx0c36d0qzflq91wmmaq1")))

(define-public crate-i_triangle-0.4.0 (c (n "i_triangle") (v "0.4.0") (d (list (d (n "i_float") (r "^0.3.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.7.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sbxbwas0yaw7g1mvddhfsd4g2x4z84czvkgf2w5pzshfigk76jy")))

(define-public crate-i_triangle-0.5.0 (c (n "i_triangle") (v "0.5.0") (d (list (d (n "i_float") (r "^0.3.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.8.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pz3w4ba7v9lsfjrlws82w3jzfk4kbihzwqcwidfxlml6ifvs55h")))

(define-public crate-i_triangle-0.5.1 (c (n "i_triangle") (v "0.5.1") (d (list (d (n "i_float") (r "^0.3.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.8.1") (d #t) (k 0)) (d (n "i_shape") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07hqdy8xjmpvjx33lgcpxa3bp49rnszkn044pz83dp0qcwwchjlw")))

(define-public crate-i_triangle-0.6.0 (c (n "i_triangle") (v "0.6.0") (d (list (d (n "i_float") (r "^0.4.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.9.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z69a095x7jmf3wcj4qx4rp5fr7gmbqzsmn6qjxjzirgrl1x0ll5")))

(define-public crate-i_triangle-0.7.0 (c (n "i_triangle") (v "0.7.0") (d (list (d (n "i_float") (r "^0.4.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.10.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lk5i04mkhsr47dr5ysrab5qdv8pym37ik074jizabpb32k4dgyq")))

(define-public crate-i_triangle-0.7.1 (c (n "i_triangle") (v "0.7.1") (d (list (d (n "i_float") (r "^0.4.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.10.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dpzg4ycj3mv8nn8gbvyqkm115idq8sg1lj47a8x7j0ypa3xjw93")))

(define-public crate-i_triangle-0.8.0 (c (n "i_triangle") (v "0.8.0") (d (list (d (n "i_float") (r "^0.4.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.11.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0f1h048j93ala7i1l6p01f7bv4qijwrr6v5d7093q622z2n3jxgs")))

(define-public crate-i_triangle-0.9.0 (c (n "i_triangle") (v "0.9.0") (d (list (d (n "i_float") (r "^0.5.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.12.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17wimms5nm7bprp44pvrys0703mskm2925yzk1syyiizlq3xvqjx")))

(define-public crate-i_triangle-0.10.0 (c (n "i_triangle") (v "0.10.0") (d (list (d (n "i_float") (r "^0.6.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.13.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mk275i02y5cy7yd17lak10jszi38ynssxqdn4a6bwgvkz45byzv")))

(define-public crate-i_triangle-0.11.0 (c (n "i_triangle") (v "0.11.0") (d (list (d (n "i_float") (r "^0.6.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.13.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0k078x0n6knqkl794a687shziklgv2cll33jr9jzkwdv8b559084")))

(define-public crate-i_triangle-0.12.0 (c (n "i_triangle") (v "0.12.0") (d (list (d (n "i_float") (r "^0.6.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.15.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nxqpbgzq8fyrswjl3l9vbhh3z6qmr5n6irk1b7r8qr241kwy3s5")))

(define-public crate-i_triangle-0.13.0 (c (n "i_triangle") (v "0.13.0") (d (list (d (n "i_float") (r "^0") (d #t) (k 0)) (d (n "i_overlay") (r "^0") (d #t) (k 0)) (d (n "i_shape") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cz3z5zcc3n5j6c5c1qnj0mdywcv7bgdqn0zx4ql0xs4n1nkr3rx")))

(define-public crate-i_triangle-0.14.0 (c (n "i_triangle") (v "0.14.0") (d (list (d (n "i_float") (r "^0.6.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.17.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1aw310iknxgdmwavcz14naj008mvpi0yvidv43xiaship3l2k1s1")))

(define-public crate-i_triangle-0.15.0 (c (n "i_triangle") (v "0.15.0") (d (list (d (n "i_float") (r "^0.8") (d #t) (k 0)) (d (n "i_overlay") (r "^0.19") (d #t) (k 0)) (d (n "i_shape") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "03lwwwa1y5rjnpykih1f7mljikmj819bjravcz8x212yqxhqawfv")))

(define-public crate-i_triangle-0.16.0 (c (n "i_triangle") (v "0.16.0") (d (list (d (n "i_float") (r "^0.8") (d #t) (k 0)) (d (n "i_overlay") (r "^0.20") (d #t) (k 0)) (d (n "i_shape") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "087a6l6cwgqxvhd91r0qq5kdi8ipkpzvm38k3acn8k3sfinrm5bg")))

(define-public crate-i_triangle-0.17.0 (c (n "i_triangle") (v "0.17.0") (d (list (d (n "i_float") (r "^0.8") (d #t) (k 0)) (d (n "i_overlay") (r "^0.21") (d #t) (k 0)) (d (n "i_shape") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18i117xx66gqpiwib612hlycirci4y3wrs3g5dkm965gg34if3v9")))

(define-public crate-i_triangle-0.18.0 (c (n "i_triangle") (v "0.18.0") (d (list (d (n "i_float") (r "^0.8") (d #t) (k 0)) (d (n "i_overlay") (r "^0.23.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mvz8rvjj8nipl4nnmf2lfrrd42i3s2r0byfkj8shqvz88i43syz")))

(define-public crate-i_triangle-0.19.0 (c (n "i_triangle") (v "0.19.0") (d (list (d (n "i_float") (r "^0.9.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.24.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jddg8jf22b7khc8rgyd40dzxja2cryzvay1g752mgzlk6gwywq5")))

(define-public crate-i_triangle-0.19.1 (c (n "i_triangle") (v "0.19.1") (d (list (d (n "i_float") (r "^0.9.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.24.2") (d #t) (k 0)) (d (n "i_shape") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "194fy3rpjxyyh80swwjjwabjma2kbs81c88rf6ar9098f5pih7yf")))

(define-public crate-i_triangle-0.20.0 (c (n "i_triangle") (v "0.20.0") (d (list (d (n "i_float") (r "^0.10.0") (d #t) (k 0)) (d (n "i_overlay") (r "^0.25.0") (d #t) (k 0)) (d (n "i_shape") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0739wlncgbampnyx28wijv1p8rqcy8ahjbc3l0sj3jl8kzqjhd73")))

(define-public crate-i_triangle-0.21.0 (c (n "i_triangle") (v "0.21.0") (d (list (d (n "i_float") (r "^1.0.0") (d #t) (k 0)) (d (n "i_overlay") (r "^1.0.0") (d #t) (k 0)) (d (n "i_shape") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dqdq8xnhnh4l2fjrkh92w5m501jabs7vyifkxs36r6ych9da5nk")))

(define-public crate-i_triangle-0.22.0 (c (n "i_triangle") (v "0.22.0") (d (list (d (n "i_float") (r "^1.0.1") (d #t) (k 0)) (d (n "i_overlay") (r "^1.0.1") (d #t) (k 0)) (d (n "i_shape") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pkfmv3k63awbxz7gv84s57qdb6j3lqxln50kp3gwhx3z8hcbhyp")))

(define-public crate-i_triangle-0.22.1 (c (n "i_triangle") (v "0.22.1") (d (list (d (n "i_float") (r "^1.0.1") (d #t) (k 0)) (d (n "i_overlay") (r "^1.0.2") (d #t) (k 0)) (d (n "i_shape") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1844spmq8kmx9sva9mh5n1iiag3hphm1fkfng8nywa2kbd3wj0dn")))

(define-public crate-i_triangle-0.22.2 (c (n "i_triangle") (v "0.22.2") (d (list (d (n "i_float") (r "^1.0.3") (d #t) (k 0)) (d (n "i_overlay") (r "^1.0.3") (d #t) (k 0)) (d (n "i_shape") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lmv5qrw0khzglf8lmgc1kgmx1fz6k6xw1n9vcsywvhawcnk4xnw")))

(define-public crate-i_triangle-0.23.0 (c (n "i_triangle") (v "0.23.0") (d (list (d (n "i_float") (r "^1.0.3") (d #t) (k 0)) (d (n "i_overlay") (r "^1.1.0") (d #t) (k 0)) (d (n "i_shape") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z5dmgjgpx0fzczc7radqxccjck3p2pd240x1j4gh6n25vgw62qa")))

(define-public crate-i_triangle-0.24.0 (c (n "i_triangle") (v "0.24.0") (d (list (d (n "i_float") (r "^1.1.0") (d #t) (k 0)) (d (n "i_overlay") (r "^1.2.0") (d #t) (k 0)) (d (n "i_shape") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18jin3lf9zfnyrf3g50qsqfyysvb5y3czldisxz98xj8zk9yjkgp")))

