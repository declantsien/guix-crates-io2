(define-module (crates-io i_ tr i_tree) #:use-module (crates-io))

(define-public crate-i_tree-0.1.0 (c (n "i_tree") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hwa7rh62lhvjw4ilm2qr96vbfwac90m3nnmmjb83aif3chimg7y")))

(define-public crate-i_tree-0.2.0 (c (n "i_tree") (v "0.2.0") (d (list (d (n "i_float") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0l9a4wvjw14yjbf0d6ma8yk76ldlqri9nj1ick489260w4736kny")))

(define-public crate-i_tree-0.3.0 (c (n "i_tree") (v "0.3.0") (d (list (d (n "i_float") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03x2h69gv1gj0lrfb7s7cmc4fj7cb8zpp1ym72bdqsgf3m0vjsd7")))

(define-public crate-i_tree-0.4.0 (c (n "i_tree") (v "0.4.0") (d (list (d (n "i_float") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04q4qrpfhgyx67sb40ps8ps4hjf51x0cvkkkd6lvx3fj9x2jrs5r")))

(define-public crate-i_tree-0.5.0 (c (n "i_tree") (v "0.5.0") (d (list (d (n "i_float") (r "^1.0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jd1jm5nibnjjp4k9nkbcl3nz4xinhz59i303limm8l0kjhxi1kv")))

(define-public crate-i_tree-0.6.0 (c (n "i_tree") (v "0.6.0") (d (list (d (n "i_float") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1saiilcy0h8likcr2ardhnqawkxxlgs1hnwknv4h6sphl97abybl")))

(define-public crate-i_tree-0.7.0 (c (n "i_tree") (v "0.7.0") (d (list (d (n "i_float") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13jf5z3vgrjb1j1if75x8gzdx60bxx2rij6wpg4j9fx8h4bv0zkz")))

(define-public crate-i_tree-0.8.0 (c (n "i_tree") (v "0.8.0") (d (list (d (n "i_float") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "067grfnyc9696kx8mx8117l7zpz6hjfkr625nsgmm3y4xw9k7f5y")))

