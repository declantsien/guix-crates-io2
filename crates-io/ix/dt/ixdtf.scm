(define-module (crates-io ix dt ixdtf) #:use-module (crates-io))

(define-public crate-ixdtf-0.0.0 (c (n "ixdtf") (v "0.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde-json-core") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)))) (h "0x6jkihqacziqwvnl7gi3yn710gyxp1kx6fxbrdlgvg79jlss9sz")))

(define-public crate-ixdtf-0.1.0 (c (n "ixdtf") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde-json-core") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)))) (h "0rvk1dzy83xlxnyqjz6vwjrzvizkrvg7w0wld5nd0h7djwkhyzsr")))

(define-public crate-ixdtf-0.2.0 (c (n "ixdtf") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "serde-json-core") (r "^0.4.0") (f (quote ("std"))) (k 2)))) (h "1wzvd8s50g3bxi2ynl2wr4kh3nc00a036p605x8ajlklj9gk08r1") (f (quote (("duration") ("default" "duration")))) (r "1.67")))

