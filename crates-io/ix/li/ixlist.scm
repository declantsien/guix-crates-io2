(define-module (crates-io ix li ixlist) #:use-module (crates-io))

(define-public crate-ixlist-0.1.0 (c (n "ixlist") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.6.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0y7hfidgr4azwi97q3gb75aaki7rppnk0pglg6yk57y524c5jmc2")))

