(define-module (crates-io ix wi ixwindow) #:use-module (crates-io))

(define-public crate-ixwindow-0.1.0 (c (n "ixwindow") (v "0.1.0") (d (list (d (n "bspc-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("randr"))) (d #t) (k 0)))) (h "0wpy990lvxz4c0j8888yv1skwsfpv2rqba0v3ns9zihdhsbk5rxb")))

(define-public crate-ixwindow-0.1.1 (c (n "ixwindow") (v "0.1.1") (d (list (d (n "bspc-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("randr"))) (d #t) (k 0)))) (h "1vr112b8gjzrgrb34qwwsbz3b1r9pzskw5qdzjxmiqw0vg79kx6w")))

(define-public crate-ixwindow-0.1.2 (c (n "ixwindow") (v "0.1.2") (d (list (d (n "bspc-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("randr"))) (d #t) (k 0)))) (h "02h5d1b2g3k2k5kwzj761bawhyiij9b6i3rcqiyhw7bk4lzip08k")))

