(define-module (crates-io sv gb svgbob_server) #:use-module (crates-io))

(define-public crate-svgbob_server-0.6.4 (c (n "svgbob_server") (v "0.6.4") (d (list (d (n "axum") (r "^0.4.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g597m3gxci0x3s5i9dcs70kxs4f3scndzb0avgnsf2q4ilifls1")))

