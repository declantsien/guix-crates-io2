(define-module (crates-io sv gb svgbob_cli) #:use-module (crates-io))

(define-public crate-svgbob_cli-0.1.8 (c (n "svgbob_cli") (v "0.1.8") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.1") (d #t) (k 0)))) (h "0az9swdgjq4lpp900rfhhh35yv2wvz7dyfjfv3syamnx8w3qs27d")))

(define-public crate-svgbob_cli-0.1.9 (c (n "svgbob_cli") (v "0.1.9") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.1") (d #t) (k 0)))) (h "0pgbw1p16aikybvmwcrspshhcjlsq92yk7xvd0whim0ifarplym1") (y #t)))

(define-public crate-svgbob_cli-0.1.10 (c (n "svgbob_cli") (v "0.1.10") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.1") (d #t) (k 0)))) (h "1d3dzdff66r043j54752c3x42b62k3m1p7ll8c7bdhpam55yysq4")))

(define-public crate-svgbob_cli-0.1.11 (c (n "svgbob_cli") (v "0.1.11") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.1") (d #t) (k 0)))) (h "00d5d028i7s8r78kx5zasa4ji9ja5fncadd0zximhgd7ccjhvikz")))

(define-public crate-svgbob_cli-0.1.14 (c (n "svgbob_cli") (v "0.1.14") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.1") (d #t) (k 0)))) (h "1b62yz433z58ran51vimhgj4g974d78inqwrbqw0b1s3dxnnf0nm")))

(define-public crate-svgbob_cli-0.2.0 (c (n "svgbob_cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "19xhrakz98kdaicqhhhdwna0h97cxb98cmm0964zll45d0zxr1p9")))

(define-public crate-svgbob_cli-0.2.1 (c (n "svgbob_cli") (v "0.2.1") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "0gbmx6ig6r7xvk9c20j1y27s6ag8dkswgjk831dhpghyq2639chc")))

(define-public crate-svgbob_cli-0.2.2 (c (n "svgbob_cli") (v "0.2.2") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "1c304nzpmb1idxhhlq3m8q7mrxav7fx64lcmx9bng60vny57rf39")))

(define-public crate-svgbob_cli-0.2.3 (c (n "svgbob_cli") (v "0.2.3") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "14q3324vmnfvvhhl21wr9v01hyspdhsiq034y47a0lls7qay86vg")))

(define-public crate-svgbob_cli-0.2.5 (c (n "svgbob_cli") (v "0.2.5") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "0b11yjhvpii73x9vcar0jc88yg8bka7giip423vd1vwl1kmyqzag")))

(define-public crate-svgbob_cli-0.2.6 (c (n "svgbob_cli") (v "0.2.6") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "09bgjx3nnyiiai1bik6l2gqd3yhzvsk26bqrr521h478zrsn4pr7")))

(define-public crate-svgbob_cli-0.2.7 (c (n "svgbob_cli") (v "0.2.7") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "0k26c8hr72yrbfcq0a32hz6b8ckn0wfivvda4z2nb80jjpj83b8b")))

(define-public crate-svgbob_cli-0.2.8 (c (n "svgbob_cli") (v "0.2.8") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.2") (d #t) (k 0)))) (h "15xq4h26ifaw5vhih9m4a404v4kvmfnkmn8mxxn9br6jyqh8g9qh")))

(define-public crate-svgbob_cli-0.3.0 (c (n "svgbob_cli") (v "0.3.0") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.3") (d #t) (k 0)))) (h "07aabnpv60a0wixmf3716zjffwv8ckiz6kdj1rm9pr4mxnfpzwvb")))

(define-public crate-svgbob_cli-0.3.1 (c (n "svgbob_cli") (v "0.3.1") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.3") (d #t) (k 0)))) (h "0qaxnaz4l137d0z2xqpmnq46cmkakpy0r7cz0yd05la3cm3zqi5x")))

(define-public crate-svgbob_cli-0.3.2 (c (n "svgbob_cli") (v "0.3.2") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.3.2") (d #t) (k 0)))) (h "1yqm7y54imrxzypg3bb0nybmglp7l69pyqpc0vcn8anrp7k5m825")))

(define-public crate-svgbob_cli-0.4.2 (c (n "svgbob_cli") (v "0.4.2") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.4.1") (d #t) (k 0)))) (h "0ww902gx3wjrrkj975hnjg85r84m86jym4xkw12nm21zwpdwh1pb")))

(define-public crate-svgbob_cli-0.5.0-alpha.5 (c (n "svgbob_cli") (v "0.5.0-alpha.5") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.5.0-alpha.4") (d #t) (k 0)))) (h "1wb8947fx28kaq8xjkg98snsvm63ad4nk2zl65yl31yw0ib1bgsn")))

(define-public crate-svgbob_cli-0.5.0-alpha.6 (c (n "svgbob_cli") (v "0.5.0-alpha.6") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.5.0-alpha.6") (d #t) (k 0)))) (h "0svj0lw4qhwjbffjal0iyi6xbjf8aklxd72ngmbq2b5n67xhm3xf")))

(define-public crate-svgbob_cli-0.5.0-alpha.8 (c (n "svgbob_cli") (v "0.5.0-alpha.8") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.5.0-alpha.8") (d #t) (k 0)))) (h "10qpx2jymjdchcv76vsw5r88s37maka78nhy0av3dxbb51qm71j3")))

(define-public crate-svgbob_cli-0.5.0 (c (n "svgbob_cli") (v "0.5.0") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.5.0-alpha.8") (d #t) (k 0)))) (h "0980y5jhjcqsfjiv88m6g2rils04xzlbxkp640q73a3za7yxdlxg")))

(define-public crate-svgbob_cli-0.5.2 (c (n "svgbob_cli") (v "0.5.2") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.5") (d #t) (k 0)))) (h "01r9h23jy1wsb0fbyla8hqlqadq6wc59hhz4q9wk1lsgf3fkshp5")))

(define-public crate-svgbob_cli-0.5.3 (c (n "svgbob_cli") (v "0.5.3") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.5") (d #t) (k 0)))) (h "134i6qq3zly7p3lhp1wdzyih68mpyp8yhbmsa6czd7zshzll9y1j")))

(define-public crate-svgbob_cli-0.5.4 (c (n "svgbob_cli") (v "0.5.4") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.5.4") (d #t) (k 0)))) (h "03lymjfvv3fzksgpl1b32m5wwwnhhh4z5ci7ap1lsn0fwp25aj46")))

(define-public crate-svgbob_cli-0.6.0 (c (n "svgbob_cli") (v "0.6.0") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)))) (h "1ckfz3c71d0ngldk04ac702pgbvs95pwckfwl0cx3pr3makj058a")))

(define-public crate-svgbob_cli-0.6.1 (c (n "svgbob_cli") (v "0.6.1") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)))) (h "1lir8ghwh7fwd181vxfms6m5ns9n2n3c0sg22whafn8scb5nrfpd")))

(define-public crate-svgbob_cli-0.6.2 (c (n "svgbob_cli") (v "0.6.2") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)))) (h "0chhjkgp41vqzx5if5brfq4aic6rzvpr37nj4z7cgp5hz32qrbvi")))

(define-public crate-svgbob_cli-0.6.3 (c (n "svgbob_cli") (v "0.6.3") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)))) (h "0sy2dsaya89ix3kw2wb4xi125m94w35gd083kz16829sy2mmsd0j")))

(define-public crate-svgbob_cli-0.6.4 (c (n "svgbob_cli") (v "0.6.4") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)))) (h "1lmyzjysbyqy2jynrgjh7cb4rm5hzg6hywcsivsws3m31c9qkv25")))

(define-public crate-svgbob_cli-0.6.5 (c (n "svgbob_cli") (v "0.6.5") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)))) (h "11hdv378mcs4znzxfgj21gl4g33q01w32yhih0f7qfbnndc7ddhc")))

(define-public crate-svgbob_cli-0.6.6 (c (n "svgbob_cli") (v "0.6.6") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.6") (d #t) (k 0)))) (h "1d6v0iypm4lcir6g5rkppjv8jgmpsr7hmy9vwdii3xv8vx264gy0")))

(define-public crate-svgbob_cli-0.7.0 (c (n "svgbob_cli") (v "0.7.0") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.7") (d #t) (k 0)))) (h "00zcs36c8hsjwcmgcw8b982x1makg8kdxi2cjjgv191ixfvcvwn3")))

(define-public crate-svgbob_cli-0.7.2 (c (n "svgbob_cli") (v "0.7.2") (d (list (d (n "clap") (r "^2.16") (d #t) (k 0)) (d (n "svgbob") (r "^0.7") (d #t) (k 0)))) (h "1nbygl82k1427jn4lyhms67dq761hqqg6rk34gg926mvf4fan7pg")))

