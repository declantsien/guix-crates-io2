(define-module (crates-io sv gx svgx) #:use-module (crates-io))

(define-public crate-svgx-0.1.0 (c (n "svgx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "10ga04k685j68j7zwbyc6v7l63a2sydx1bikq6jwllyshc0h8qjq")))

