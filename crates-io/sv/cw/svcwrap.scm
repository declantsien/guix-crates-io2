(define-module (crates-io sv cw svcwrap) #:use-module (crates-io))

(define-public crate-svcwrap-0.1.0 (c (n "svcwrap") (v "0.1.0") (d (list (d (n "dbgtools-win") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "eventlog") (r "^0.1") (d #t) (k 0)) (d (n "figment") (r "^0.10") (d #t) (k 0)) (d (n "figment-winreg") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qargparser") (r "^0.5.6") (d #t) (k 0)) (d (n "registry") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows-service") (r "^0.4") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (k 0)))) (h "0vdl66fksl94km9kdca38jy1jlir24f8bdz36bhylfpdd71sh5qi")))

