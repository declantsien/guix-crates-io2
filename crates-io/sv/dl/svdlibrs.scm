(define-module (crates-io sv dl svdlibrs) #:use-module (crates-io))

(define-public crate-svdlibrs-0.1.0 (c (n "svdlibrs") (v "0.1.0") (d (list (d (n "nalgebra-sparse") (r "^0.5.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0awzp1cb8rfp27jvad813h595znwj52b71k690vb6nw1bv49rwzn")))

(define-public crate-svdlibrs-0.2.0 (c (n "svdlibrs") (v "0.2.0") (d (list (d (n "nalgebra-sparse") (r "^0.5.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1rhi0d8xbq9gdnqai518cw809pw44g0qg29zvfzz00ip4hbjal13")))

(define-public crate-svdlibrs-0.3.0 (c (n "svdlibrs") (v "0.3.0") (d (list (d (n "nalgebra-sparse") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zsn93i4xsm0iixyg2fkjwwp8i0v24a0lrxd7fg0q74jafxzm3y2")))

(define-public crate-svdlibrs-0.4.0 (c (n "svdlibrs") (v "0.4.0") (d (list (d (n "nalgebra-sparse") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0r78pasr2vh2h2pidm0av294yjah8c7kilmybjzls25c6ibjzc2w")))

(define-public crate-svdlibrs-0.4.2 (c (n "svdlibrs") (v "0.4.2") (d (list (d (n "nalgebra-sparse") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ssajx3wwr7gzcays2qfzj2d3h79a82l0gqs87xv86nnj1wwcs2k")))

(define-public crate-svdlibrs-0.4.3 (c (n "svdlibrs") (v "0.4.3") (d (list (d (n "nalgebra-sparse") (r "^0.7.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0c3bbnwi8a11anwkaqj8w9zwhb13ml7wmgh7a7z177iqi51bbah3")))

(define-public crate-svdlibrs-0.5.0 (c (n "svdlibrs") (v "0.5.0") (d (list (d (n "nalgebra-sparse") (r "^0.8.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1yawm97fdmg0cxs65wkm11rd5370d2kzabs75vkpxjiq6wfw2jph")))

(define-public crate-svdlibrs-0.5.1 (c (n "svdlibrs") (v "0.5.1") (d (list (d (n "nalgebra-sparse") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0aabq4d2cbczw5pirhn0ms06d6fxskcy7m0ji5v06g0y4j9y9wy3")))

