(define-module (crates-io sv gw svgwriter) #:use-module (crates-io))

(define-public crate-svgwriter-0.1.0 (c (n "svgwriter") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0998gcp1yi2sqjfs03hgh97rygywdnyncdd0rxbqqn66vfcjsxv6")))

