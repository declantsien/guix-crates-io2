(define-module (crates-io sv gf svgfilters) #:use-module (crates-io))

(define-public crate-svgfilters-0.1.0 (c (n "svgfilters") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.5") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "115d67yjxhd9zcf9zp1020ccysjyc44r23s7wpfwijsir8a5l0q8")))

(define-public crate-svgfilters-0.2.0 (c (n "svgfilters") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.5") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "03h94m5vs5p7nij4xvsjdjjcqsic48q30xrvrnvkpsl2gchv65ih")))

(define-public crate-svgfilters-0.3.0 (c (n "svgfilters") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.5") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "0km39dbwjh3s0k7fkzr3bqkzm9gpbxb4ifmg3p141b3rxqpww3gv")))

(define-public crate-svgfilters-0.4.0 (c (n "svgfilters") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1kjbl0khhq548ciw2lnmkk3w2q6ncda6yzgkg7qjvp2zq7mvr6k3")))

