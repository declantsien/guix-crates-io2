(define-module (crates-io sv el svelters) #:use-module (crates-io))

(define-public crate-svelters-0.0.1 (c (n "svelters") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "muncher") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "swc_common") (r "^0.31") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.103") (f (quote ("serde-impl"))) (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.133") (f (quote ("debug"))) (d #t) (k 0)))) (h "17dak2pl0lwkni0l7r82gllimwxxrpvbhjycvk16sba7lxlhavcb")))

