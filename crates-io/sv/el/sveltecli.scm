(define-module (crates-io sv el sveltecli) #:use-module (crates-io))

(define-public crate-sveltecli-0.1.0 (c (n "sveltecli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "174yd2kmr5s1zy8496j5kjm24qyydmd6scxvny3vww0y8ryq768x")))

(define-public crate-sveltecli-0.1.1 (c (n "sveltecli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rc27fhfwzr8l8dfjd4bx6w99gh4i81bdqa83la34hg6l74qfgm0")))

(define-public crate-sveltecli-0.2.0 (c (n "sveltecli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0pgngy03202d4xgkg30w6ydy8rskqghi7srx5snkqxcg2223a9xd")))

(define-public crate-sveltecli-0.2.1 (c (n "sveltecli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1d4773vp5xzgzs3p5gyp6d339mj70jigsdqwh3lv95mchm9gsw18")))

(define-public crate-sveltecli-0.3.0 (c (n "sveltecli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "16vr00c8cfwk97m13dxg3h43hwhivbzg8mwih6jm8951flqjyc08")))

