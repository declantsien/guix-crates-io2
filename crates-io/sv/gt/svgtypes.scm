(define-module (crates-io sv gt svgtypes) #:use-module (crates-io))

(define-public crate-svgtypes-0.1.0 (c (n "svgtypes") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "xmlparser") (r "^0.4") (d #t) (k 0)))) (h "0scd0ddfi4ksb9r2qhslaxhzzgzqfy382gah95kz5xqr97j0kicp")))

(define-public crate-svgtypes-0.1.1 (c (n "svgtypes") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "xmlparser") (r "^0.4.1") (d #t) (k 0)))) (h "13qhcfawky3161xnhi8lp49ffcf0n9zl47riidzhhp7262sg4vpw")))

(define-public crate-svgtypes-0.2.0 (c (n "svgtypes") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "xmlparser") (r "^0.6") (d #t) (k 0)))) (h "1j87k9vnz65pkqrmy8rskmidjys6s0kf36c37w10gnxflcba2zbd")))

(define-public crate-svgtypes-0.3.0 (c (n "svgtypes") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)))) (h "0fs5c6v29lxvk409sbhzynnbb7h38b1v07imbr5h4r3jvvk2ag64")))

(define-public crate-svgtypes-0.4.0 (c (n "svgtypes") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)))) (h "006aqa8z6n396lahbkd5i7jhbfqhiq1sb6qlnpg5kxnxii6dzbnm")))

(define-public crate-svgtypes-0.4.1 (c (n "svgtypes") (v "0.4.1") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)))) (h "0z54y4nxxqwc2z9937ad7l3irl6pm4jkl053dmjavcd8yy9j20cs")))

(define-public crate-svgtypes-0.4.2 (c (n "svgtypes") (v "0.4.2") (d (list (d (n "float-cmp") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)))) (h "0ay5ch709nbhmgnnb4ji6z42qm951b26iw4pshrbxmn61b40qa06")))

(define-public crate-svgtypes-0.4.3 (c (n "svgtypes") (v "0.4.3") (d (list (d (n "float-cmp") (r "^0.4.1") (k 0)) (d (n "phf") (r "^0.7.24") (o #t) (d #t) (k 0)))) (h "04bhfmjl3a7wq3zivxg415ci5s8yr25pxmrsl3kn3zfc3w3ix938") (f (quote (("element-id" "phf") ("default" "color" "attribute-id" "element-id") ("color" "phf") ("attribute-id" "phf"))))))

(define-public crate-svgtypes-0.4.4 (c (n "svgtypes") (v "0.4.4") (d (list (d (n "float-cmp") (r "^0.5") (k 0)) (d (n "phf") (r "^0.7.24") (o #t) (d #t) (k 0)))) (h "0n12s36r7pk06mss9v3ccj49amjik47sja7jbmcf0nlj50n8hk24") (f (quote (("element-id" "phf") ("default" "color" "attribute-id" "element-id") ("color" "phf") ("attribute-id" "phf"))))))

(define-public crate-svgtypes-0.5.0 (c (n "svgtypes") (v "0.5.0") (d (list (d (n "float-cmp") (r "^0.5") (k 0)) (d (n "siphasher") (r "^0.2.3") (d #t) (k 0)))) (h "1zv0yb4nfyz78y8k7fmyjqgdh9vf7xc44c9pzry8640szym6ylww")))

(define-public crate-svgtypes-0.6.0 (c (n "svgtypes") (v "0.6.0") (d (list (d (n "float-cmp") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "12zs6rkrazvw2awkh5l1nbmdxj5cpbf6wp3677fajjkxmasd36c1")))

(define-public crate-svgtypes-0.7.0 (c (n "svgtypes") (v "0.7.0") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1x47kr9w2wmjbsvawvdn5xyphg3by9hlzisdvxdqb21va11zm7mj")))

(define-public crate-svgtypes-0.8.0 (c (n "svgtypes") (v "0.8.0") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1cwlxxkp56nmp2dj7bzn84mkik7215b59d1a53amcz25kaskxfys")))

(define-public crate-svgtypes-0.8.1 (c (n "svgtypes") (v "0.8.1") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1hc5ijhyh1cww8vnbr7x4z33gs33g2d1yc11zzcg9ka4n5l2z06c")))

(define-public crate-svgtypes-0.8.2 (c (n "svgtypes") (v "0.8.2") (d (list (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0r2mjyrsyrczd05hycw0ww03nqv4hyqsd67qajxpcsmc5f55x5r2")))

(define-public crate-svgtypes-0.9.0 (c (n "svgtypes") (v "0.9.0") (d (list (d (n "kurbo") (r "^0.8") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0d4pn6c218y1lq4w42by80a3paqvq4mchsjzzv61hnvs830jkvn9")))

(define-public crate-svgtypes-0.10.0 (c (n "svgtypes") (v "0.10.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0r97kic0l9zdylkz5fwzx6y2yjf5yfs7jwlhkibsc7fgrpnsrzwq")))

(define-public crate-svgtypes-0.11.0 (c (n "svgtypes") (v "0.11.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "19q2xxy8d3sl7406jy8kd4inklp2v62y667sq1l7y9zkww8hcjzd")))

(define-public crate-svgtypes-0.12.0 (c (n "svgtypes") (v "0.12.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0llyghxgvcg207qc1svr3x193rpd12inj4xjxlk9vxa25pzrj56p")))

(define-public crate-svgtypes-0.13.0 (c (n "svgtypes") (v "0.13.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0w4xknlff1np8l9if7y8ig6bx44bjr006m5xgj8ih0wnrn4f4i3f")))

(define-public crate-svgtypes-0.14.0 (c (n "svgtypes") (v "0.14.0") (d (list (d (n "kurbo") (r "^0.10") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0haj29qzd73lkbwavry4ayy9rxwkg8gdmzbw38byh6xm2a7n3msr")))

(define-public crate-svgtypes-0.15.0 (c (n "svgtypes") (v "0.15.0") (d (list (d (n "kurbo") (r "^0.11") (d #t) (k 0)) (d (n "siphasher") (r "^1.0") (d #t) (k 0)))) (h "0zz8hcf5daflfp6yql5v47x118inxbldijcs2fl0viy9j6lajz6r")))

(define-public crate-svgtypes-0.15.1 (c (n "svgtypes") (v "0.15.1") (d (list (d (n "kurbo") (r "^0.11") (d #t) (k 0)) (d (n "siphasher") (r "^1.0") (d #t) (k 0)))) (h "0g54d2cc6wlyf0254i98bjffwjhjknk2a13alz4r34xqz56hdqzs")))

