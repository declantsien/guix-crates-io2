(define-module (crates-io sv ma svmap) #:use-module (crates-io))

(define-public crate-svmap-0.1.0 (c (n "svmap") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "04872d202kvb2icf4h636l818p2lb16l87f4bgwy071isfgap7ik")))

(define-public crate-svmap-0.1.1 (c (n "svmap") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1w8x36n8yr8cza23nnndxxshkqkp9hacsfp8wgpbv9smifg7wbwp")))

(define-public crate-svmap-0.2.0 (c (n "svmap") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0mkmsvxlf8y50hjw0s607vq8w5rds8myfrbslpi4s23xc1b6psq3")))

(define-public crate-svmap-0.2.1 (c (n "svmap") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1kg35hn5dkcpqvrdwx4gs9z8ip7mfwrjj0a0z10k3b1kxzmf19x2")))

(define-public crate-svmap-0.3.0 (c (n "svmap") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0yfaw58qqp48rdf3n9zskczf29fbbxmki1qaji0w9zc8318y590i")))

