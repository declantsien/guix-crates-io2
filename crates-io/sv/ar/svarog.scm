(define-module (crates-io sv ar svarog) #:use-module (crates-io))

(define-public crate-svarog-0.5.0 (c (n "svarog") (v "0.5.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0pvcqhgfx3w6hrqlnifs10lapnfsvpq6xj0a4wfs0qbpk7g05xak")))

(define-public crate-svarog-0.6.0 (c (n "svarog") (v "0.6.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "08gv4cxzmk40x50741sk0wsvbqn2aiz17vnrcrinphi54i8jjway")))

(define-public crate-svarog-0.6.1 (c (n "svarog") (v "0.6.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1fjr8a1rdykjm1mhqxa4r982ngkwwivlnwx3wzc46rzrjkakqp1a")))

