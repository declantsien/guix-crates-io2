(define-module (crates-io sv _m sv_menu_scraper) #:use-module (crates-io))

(define-public crate-sv_menu_scraper-0.1.0 (c (n "sv_menu_scraper") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0w96nh3awmhg80az3gdlrxwr6fa901fi3bn3wf33lcbyhny4pnag")))

(define-public crate-sv_menu_scraper-0.2.0 (c (n "sv_menu_scraper") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0l98z1vq8x23gp8qp62rjz3711q0byqjxvblvwxafbg0hg1fa698")))

(define-public crate-sv_menu_scraper-0.2.1 (c (n "sv_menu_scraper") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1m0m4ll7ph73palf67s2xn3xjy18rwl7yf2af219qq42krxwcjm6")))

