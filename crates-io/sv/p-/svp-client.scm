(define-module (crates-io sv p- svp-client) #:use-module (crates-io))

(define-public crate-svp-client-0.1.0 (c (n "svp-client") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0m3x2skgbgjgssq6sr9qi7dw3f6s7gbshbpn41hacw0hmm205kih")))

