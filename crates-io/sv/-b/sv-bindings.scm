(define-module (crates-io sv -b sv-bindings) #:use-module (crates-io))

(define-public crate-sv-bindings-0.1.0 (c (n "sv-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "1gxb953z3daclbaysgzh3vp58yw7ldh1273cvl31rn2jfxcrr0j8") (f (quote (("vpi_user") ("vpi_compatibility") ("svdpi") ("sv_vpi_user") ("default" "svdpi" "vpi_user" "vpi_compatibility" "sv_vpi_user"))))))

(define-public crate-sv-bindings-0.1.1 (c (n "sv-bindings") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "1sc8j9mjj1nkd01cykwy6x2p9d6w739sb8z568lg26cp2ysnb7fs") (f (quote (("default"))))))

(define-public crate-sv-bindings-0.1.2 (c (n "sv-bindings") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "1awmsc6xiqm5w0rfr6d2fj13i52c061hra4v4dix7dxds685x7w5") (f (quote (("default"))))))

