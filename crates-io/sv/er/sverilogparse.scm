(define-module (crates-io sv er sverilogparse) #:use-module (crates-io))

(define-public crate-sverilogparse-0.1.0 (c (n "sverilogparse") (v "0.1.0") (d (list (d (n "arcstr") (r "^1.1.3") (f (quote ("serde" "substr-usize-indices"))) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "01q6i2cswp7c249aha47zdv18ivnsx3hxgywfv1w1mq0b0hlvqyy")))

(define-public crate-sverilogparse-0.2.0 (c (n "sverilogparse") (v "0.2.0") (d (list (d (n "arcstr") (r "^1.1.3") (f (quote ("serde" "substr-usize-indices"))) (d #t) (k 0)) (d (n "indenter") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1nf6v2gwaqvbfzhbzh6z5vwyph3yhwsa4ai86nmziw1fgcpkik9m")))

(define-public crate-sverilogparse-0.3.0 (c (n "sverilogparse") (v "0.3.0") (d (list (d (n "arcstr") (r "^1.1.3") (f (quote ("serde" "substr-usize-indices"))) (d #t) (k 0)) (d (n "indenter") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1a5gh9qclf7sjfh6jsvnayllnjp7jq6w3c9fz7hhq3bv5337w9rv")))

(define-public crate-sverilogparse-0.3.1 (c (n "sverilogparse") (v "0.3.1") (d (list (d (n "arcstr") (r "^1.1.3") (f (quote ("serde" "substr-usize-indices"))) (d #t) (k 0)) (d (n "indenter") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "09m6q521gqz9rd0f2cw6s77m19cvabjpmmj1brrwxpmcicwknq5a")))

(define-public crate-sverilogparse-0.3.2 (c (n "sverilogparse") (v "0.3.2") (d (list (d (n "arcstr") (r "^1.1.3") (f (quote ("serde" "substr-usize-indices"))) (d #t) (k 0)) (d (n "indenter") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1l5fd37ms5ksl3m9i5wwli9l9091iwalm8l49y0hcy5lh4xdqm2z")))

(define-public crate-sverilogparse-0.3.3 (c (n "sverilogparse") (v "0.3.3") (d (list (d (n "arcstr") (r "^1.1.3") (f (quote ("serde" "substr-usize-indices"))) (d #t) (k 0)) (d (n "indenter") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0bj36cy9dj4qiigb33abr5npcygm2ms983ir9zpd1dm9nfk8r8ip")))

