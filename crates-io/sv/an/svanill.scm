(define-module (crates-io sv an svanill) #:use-module (crates-io))

(define-public crate-svanill-0.2.0 (c (n "svanill") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.13") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.17") (d #t) (k 0)))) (h "0iywnx5iw2kdvgdn4hr171wgixn1psjb92li7kvpgqq48afrwz90")))

