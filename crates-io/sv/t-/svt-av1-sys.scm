(define-module (crates-io sv t- svt-av1-sys) #:use-module (crates-io))

(define-public crate-svt-av1-sys-0.1.0 (c (n "svt-av1-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "02irkaqhsm3j8zwk8dxpshcayqbvi1cp9v8h00zl1hzcl4pbxlma") (f (quote (("build_sources"))))))

(define-public crate-svt-av1-sys-0.1.1 (c (n "svt-av1-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1.2") (d #t) (k 1)))) (h "0lijhaikbl0fksvvs8346jhn2gcd6mnrcw0dd6yrcqn4wx5ss7g7") (f (quote (("build_sources"))))))

