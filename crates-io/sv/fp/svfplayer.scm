(define-module (crates-io sv fp svfplayer) #:use-module (crates-io))

(define-public crate-svfplayer-0.1.0 (c (n "svfplayer") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jtag-taps") (r "^0.2") (d #t) (k 0)) (d (n "svf") (r "^0.3") (d #t) (k 0)))) (h "1qmkh7na4ps9hxiwrm33wjyq920q62mymclz3jxvizc3i89ww1cj")))

