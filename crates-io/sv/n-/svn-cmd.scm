(define-module (crates-io sv n- svn-cmd) #:use-module (crates-io))

(define-public crate-svn-cmd-0.1.0 (c (n "svn-cmd") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0f2c8aw5bqb017d0ah8469fwf9bx9w4z3jng262gl7lm6rmglpdg")))

