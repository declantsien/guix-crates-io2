(define-module (crates-io sv gp svgplot) #:use-module (crates-io))

(define-public crate-svgplot-2022.0.40 (c (n "svgplot") (v "2022.0.40") (h "1nzymwlv14iby2m1mph5n0qxfh96g01km2mi6ppxwrsq2a4mn9v1") (r "1.66")))

(define-public crate-svgplot-2022.0.43 (c (n "svgplot") (v "2022.0.43") (h "1w1q0igzd6nks50j5mzxlm15hgbz8dmmkcj85gm1qyhh4l2vag2j") (r "1.66")))

(define-public crate-svgplot-2022.0.44 (c (n "svgplot") (v "2022.0.44") (h "0mv9yj87zcw3nb2f8wgfkjx8la956gcrfj57g804dzdbsdaxh2zk") (r "1.66")))

(define-public crate-svgplot-2022.0.45 (c (n "svgplot") (v "2022.0.45") (h "0ggyjsv56a7dfglsap76bzi04bb38kcs157zlwbvx75xwwnzlc7w") (r "1.66")))

(define-public crate-svgplot-2022.0.46 (c (n "svgplot") (v "2022.0.46") (h "0qvf3zrq27rrn99gr753471in4ks4anjnyn6v0bjdhbl7jkz8hah") (r "1.66")))

(define-public crate-svgplot-2022.0.49 (c (n "svgplot") (v "2022.0.49") (h "01gr8r6gbzvin3zmwmh395n2d5fv5slsfydv8s435bdq74cy93zl") (r "1.66")))

(define-public crate-svgplot-2022.0.50 (c (n "svgplot") (v "2022.0.50") (h "17k9vr03fvp87jzvcqpkh949api00xqpwwn2y4760mfavbnsh620") (r "1.66")))

(define-public crate-svgplot-2022.0.52 (c (n "svgplot") (v "2022.0.52") (h "1f4snhgkw960cyhaaicdzqdfbk54py7gd29x5ri2ls5jnqiirria") (r "1.66")))

(define-public crate-svgplot-2022.0.53 (c (n "svgplot") (v "2022.0.53") (h "14fn7iywrv314jb7cfv2ik2pnr3yaj8ppizamm55hqc4sc1py4sx") (r "1.66")))

(define-public crate-svgplot-2022.0.54 (c (n "svgplot") (v "2022.0.54") (h "1xnwxd83w63jhipwm4z2zl98rck6bi0qdbs664qhb63gp28nkl8v") (r "1.66")))

(define-public crate-svgplot-2022.0.55 (c (n "svgplot") (v "2022.0.55") (h "1qym2bgw1waw0g007bnvgxx6h0vb2zv7jx7qx6x14iy906aqxwnp") (r "1.66")))

(define-public crate-svgplot-2022.0.56 (c (n "svgplot") (v "2022.0.56") (h "09v67cv0dkzi2krk9n34vz71i310h5b2f5pr6yiqr0wz6gy99cyi") (r "1.66")))

(define-public crate-svgplot-2022.0.57 (c (n "svgplot") (v "2022.0.57") (h "0fpqf01r3ai72hnfmr801l94dng7gx1vvlidgjqp43wp68v6fdlp") (r "1.66")))

(define-public crate-svgplot-2022.0.58 (c (n "svgplot") (v "2022.0.58") (h "0q31qm8j5sc0gpcw9w5pyh4yxlr902jfy1aifq15v9g65ys5br9k") (r "1.66")))

(define-public crate-svgplot-2022.0.59 (c (n "svgplot") (v "2022.0.59") (h "18zw36iwn1iq8i70dplim3mcmr3g696izll822ahachlmycarbb2") (r "1.74")))

(define-public crate-svgplot-2022.0.60 (c (n "svgplot") (v "2022.0.60") (h "11w2vsd2ki40f97jxdrlsnkmmr3qcvwakrdhf326ax3251048hdy") (r "1.74")))

(define-public crate-svgplot-2022.0.61 (c (n "svgplot") (v "2022.0.61") (h "1rjg6laap2b47a74sqm9fds7khlsq2rhmn7gw4glhh4znbid6lh6") (r "1.74")))

(define-public crate-svgplot-2022.0.62 (c (n "svgplot") (v "2022.0.62") (h "0ss12ww49nq9gv5kk263s7y3b792p8rj0nykp503917v25h985ni") (r "1.74")))

(define-public crate-svgplot-2022.0.63 (c (n "svgplot") (v "2022.0.63") (h "0v9g5xb60xg2rlvypkbak6nv2qfn5v77hdl240a3qlssjxq14y1x") (r "1.74")))

(define-public crate-svgplot-2022.0.64 (c (n "svgplot") (v "2022.0.64") (h "1fpi5dnhvgk2l8n2y435ppkyr9cf4gk6cx3ljb7s359hj5lh1iz4") (r "1.75")))

(define-public crate-svgplot-2022.0.65 (c (n "svgplot") (v "2022.0.65") (h "1m6vhmbxzh39ffxvxgigmqpmml6c8n0jrj7n8fjryw934ssc99wj") (r "1.76")))

(define-public crate-svgplot-2022.0.66 (c (n "svgplot") (v "2022.0.66") (h "1ikrf0dp561zycs89p5af8ymrqn6qvi7vx63phpzpbzp1ilb0dg6") (r "1.76")))

