(define-module (crates-io sv gp svgparser) #:use-module (crates-io))

(define-public crate-svgparser-0.0.1 (c (n "svgparser") (v "0.0.1") (d (list (d (n "phf") (r "0.7.*") (d #t) (k 0)))) (h "159mkhvw17rh5i7dn34i9fhhdg7gblqwzmwglpd5cay4lq198vy1")))

(define-public crate-svgparser-0.0.2 (c (n "svgparser") (v "0.0.2") (d (list (d (n "phf") (r "0.7.*") (d #t) (k 0)))) (h "1qa8qrj9wikgl71rh7k57c38ldxclwz52a8hg39p7w9gidfhz500")))

(define-public crate-svgparser-0.0.3 (c (n "svgparser") (v "0.0.3") (d (list (d (n "phf") (r "0.7.*") (d #t) (k 0)))) (h "1y49240qz401smvj2h6p8zfxrx31418mjmf5b7fni4v6zxcbzicj")))

(define-public crate-svgparser-0.1.0 (c (n "svgparser") (v "0.1.0") (d (list (d (n "phf") (r "0.7.*") (d #t) (k 0)))) (h "0lfpfimgn2hzkjlw1zp2dd7jsjlcmrjrg72frbipw7fj6w440fxj")))

(define-public crate-svgparser-0.2.0 (c (n "svgparser") (v "0.2.0") (d (list (d (n "phf") (r "0.7.*") (d #t) (k 0)))) (h "1nvwqlyvh7rvhnzw85n9qdny1f1mkqbribw84lbzk3cb29x613h0")))

(define-public crate-svgparser-0.2.1 (c (n "svgparser") (v "0.2.1") (d (list (d (n "phf") (r "= 0.7.20") (d #t) (k 0)))) (h "0h4bga8fp5cihkp8gyf4y719n5bsk5x10xrp89mbl0y6z15a40gy")))

(define-public crate-svgparser-0.3.0 (c (n "svgparser") (v "0.3.0") (d (list (d (n "phf") (r "= 0.7.21") (d #t) (k 0)))) (h "1b75bz1wi8dm309sm0gkahdd0lvx9gl0csy23afq4h9drv56i9kb")))

(define-public crate-svgparser-0.3.1 (c (n "svgparser") (v "0.3.1") (d (list (d (n "phf") (r "= 0.7.21") (d #t) (k 0)))) (h "1h0i8kqnvjzjjvq3bbzc06nb8fp27w6a7dbckpbljc3w3cksw642")))

(define-public crate-svgparser-0.4.0 (c (n "svgparser") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)))) (h "0xx3j30irdf4p6i94dr4gm2njknarjcwgj1llf5mfz7s3p7wczqi")))

(define-public crate-svgparser-0.4.1 (c (n "svgparser") (v "0.4.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)))) (h "0syjy933w57rv6ik1x5xj8dxyazilnj8qn0zp63pi7mgc7azm2xq")))

(define-public crate-svgparser-0.4.2 (c (n "svgparser") (v "0.4.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)))) (h "0k39zihiv26zbryggvh0vxk8lc6rr5980qa79x83rnkgvpzfcxri")))

(define-public crate-svgparser-0.4.3 (c (n "svgparser") (v "0.4.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)))) (h "1llx80gii5nl92szv823brk2qkj8sfgm3g62rlc1276bqk8sz5li")))

(define-public crate-svgparser-0.5.0 (c (n "svgparser") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)))) (h "1mm8jgc3xqkaaqmlbhvdhmn4pkrnhkjj0rk4mm297zfj3arjg760")))

(define-public crate-svgparser-0.6.0 (c (n "svgparser") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)) (d (n "xmlparser") (r "^0.1") (d #t) (k 0)))) (h "0c2i4ngryq8mqlfvq8bqm675a2gx00wjj02f2fy8xa71wsmfqhxh")))

(define-public crate-svgparser-0.6.1 (c (n "svgparser") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)) (d (n "xmlparser") (r "^0.1") (d #t) (k 0)))) (h "18r8n29qm46sp3g3vak12b96gsnq0xlck8kqcllh3p0p2787d2hq") (y #t)))

(define-public crate-svgparser-0.6.2 (c (n "svgparser") (v "0.6.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "= 0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)) (d (n "xmlparser") (r "^0.1") (d #t) (k 0)))) (h "05y7a9rf96al0qybdx7zh2jglyjy6jpzn03pihdkbw77283h7090")))

(define-public crate-svgparser-0.6.3 (c (n "svgparser") (v "0.6.3") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)) (d (n "xmlparser") (r "^0.1") (d #t) (k 0)))) (h "1544l9h0c62maylk0xf3iffqrjl16296d44x8y6lh28h026if1xb")))

(define-public crate-svgparser-0.6.4 (c (n "svgparser") (v "0.6.4") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.2") (d #t) (k 2)) (d (n "xmlparser") (r "^0.1") (d #t) (k 0)))) (h "1jx30w022dqva6zia5aq6agmf1jf49dgndp4a33jcmfjyzas9clh")))

(define-public crate-svgparser-0.7.0 (c (n "svgparser") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.2") (d #t) (k 0)))) (h "1ry0wqpahdrxn3aspli9wa21nc3mj15nylxr5c6ifq95gdpgnxqy")))

(define-public crate-svgparser-0.8.0 (c (n "svgparser") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.3") (d #t) (k 0)))) (h "0734vfgvjd6ryw82x1dmi4agaixdsc1ykakxgvg31lc9hb7jlj3y")))

(define-public crate-svgparser-0.8.1 (c (n "svgparser") (v "0.8.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.3") (d #t) (k 2)) (d (n "xmlparser") (r "^0.3") (d #t) (k 0)))) (h "1wc5xj895in01n9j7yz30kb3gpwv6agk0bz00r71pz02n59g6iic")))

