(define-module (crates-io sv g- svg-tikz) #:use-module (crates-io))

(define-public crate-svg-tikz-0.1.0 (c (n "svg-tikz") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 0)))) (h "0r9i4gzsxakbsqzhr4d6siklw1ki1wv4xkvcpcp1i7sd3qd0ybry")))

(define-public crate-svg-tikz-0.2.0 (c (n "svg-tikz") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 0)))) (h "0la45kb2y9kkvsg8qvhgxympgw6zl672qilw8ga5ibbfvr951mgk")))

(define-public crate-svg-tikz-0.2.1 (c (n "svg-tikz") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 0)))) (h "15cm54vv5c1wdjlih8pzxmhqb16f56q08mxq93zx8lq9173qwv8a")))

