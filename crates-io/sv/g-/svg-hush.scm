(define-module (crates-io sv g- svg-hush) #:use-module (crates-io))

(define-public crate-svg-hush-0.9.0 (c (n "svg-hush") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.12.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0w8sinsr5p7qzcvbkgs4q690923ssnq0i32241iqaz00b3n4509d")))

(define-public crate-svg-hush-0.9.1 (c (n "svg-hush") (v "0.9.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "data-url") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0inirv5pc2imwkyj6hx66sw1374ccqy30fci50rfrh4kf4z4vqnb") (r "1.57")))

(define-public crate-svg-hush-0.9.2 (c (n "svg-hush") (v "0.9.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "data-url") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.10") (d #t) (k 0)))) (h "127qg0gbacc5mcp8x5vczr064aasyzy094fyawiw61m67pq0ba4c") (r "1.63")))

(define-public crate-svg-hush-0.9.3 (c (n "svg-hush") (v "0.9.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "data-url") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.16") (d #t) (k 0)))) (h "0a2lz0pxq2xqqi1ab6inih81adl1f6kra6z1y0ilaipv3gqfz8kj") (r "1.57")))

(define-public crate-svg-hush-0.9.4 (c (n "svg-hush") (v "0.9.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "data-url") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.16") (d #t) (k 0)))) (h "0k9g1hhamacsi76bi1x2kcx3r583k1plfx122gp0fqiljacmnxcg") (r "1.57")))

