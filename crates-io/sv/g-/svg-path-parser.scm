(define-module (crates-io sv g- svg-path-parser) #:use-module (crates-io))

(define-public crate-svg-path-parser-0.1.0 (c (n "svg-path-parser") (v "0.1.0") (h "177az95s7zsk95lbf9yffgf1bhxj5fgb7vggswm48yzqjc460px8")))

(define-public crate-svg-path-parser-0.1.1 (c (n "svg-path-parser") (v "0.1.1") (h "00lfwc1sgzgmw8lzi7yl0rjck8gvrvhg53chzmqmfdvl3zc1970b")))

