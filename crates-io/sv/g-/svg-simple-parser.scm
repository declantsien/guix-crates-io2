(define-module (crates-io sv g- svg-simple-parser) #:use-module (crates-io))

(define-public crate-svg-simple-parser-0.0.1 (c (n "svg-simple-parser") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0dgdbd601h2idkkb7dv3lc8jgzr2kczfg861ghv67x13v3jaii75")))

(define-public crate-svg-simple-parser-0.0.2 (c (n "svg-simple-parser") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0v46jgb4j1lwqa4qrb63gj2jsd7hzwq82y5xkdpqk37alzb4zvbd")))

(define-public crate-svg-simple-parser-0.0.3 (c (n "svg-simple-parser") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0y7jwwxdfx1180blvnbhxgkc6pwdyws2672nfrgkhrn97i9llc8y")))

(define-public crate-svg-simple-parser-0.0.4 (c (n "svg-simple-parser") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1lpzsqc964xvlal4bb355ndc60ibp9n65a1nbwnrijbm9di6i8fn")))

(define-public crate-svg-simple-parser-0.0.5 (c (n "svg-simple-parser") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1ww25gl8pn77mbaw0yzxsqlsc5hvhl2hczw0zx34hwvryrrakjs1")))

(define-public crate-svg-simple-parser-0.0.6 (c (n "svg-simple-parser") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1830bg6r1mp9a3k1haaf6y56fqr2xr4zycc9ddjzh2d8bzjbk4iv")))

