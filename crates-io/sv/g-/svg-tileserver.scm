(define-module (crates-io sv g- svg-tileserver) #:use-module (crates-io))

(define-public crate-svg-tileserver-0.1.0 (c (n "svg-tileserver") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "resvg") (r "^0.41.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.4") (d #t) (k 0)) (d (n "usvg") (r "^0.41.0") (d #t) (k 0)))) (h "1zkrcs2yk17s03i3dhm0r4pq2a42m2absw364qjbn84d64j554lx")))

(define-public crate-svg-tileserver-0.1.1 (c (n "svg-tileserver") (v "0.1.1") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "resvg") (r "^0.41.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.4") (d #t) (k 0)) (d (n "usvg") (r "^0.41.0") (d #t) (k 0)))) (h "1fva2yvz73y74qfhkv1hn5c9wyjnbwl12bdgky4mi6pvywzhdp1x")))

(define-public crate-svg-tileserver-0.1.2 (c (n "svg-tileserver") (v "0.1.2") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "resvg") (r "^0.41.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.4") (d #t) (k 0)) (d (n "usvg") (r "^0.41.0") (d #t) (k 0)))) (h "0lvikaljhnnbm6gl8525557vf1yrqnfqp7ar4flgf4xi239dmx6n")))

