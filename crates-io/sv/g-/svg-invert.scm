(define-module (crates-io sv g- svg-invert) #:use-module (crates-io))

(define-public crate-svg-invert-0.0.1 (c (n "svg-invert") (v "0.0.1") (d (list (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.20") (d #t) (k 0)))) (h "17jjwf1qi3s3ff0g60yqvnhsl5sipqz2i5h8yhwxgf9rgfvcf02w")))

(define-public crate-svg-invert-0.0.2 (c (n "svg-invert") (v "0.0.2") (d (list (d (n "csscolorparser") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.20") (d #t) (k 0)))) (h "0m5nyfg65qqcq8j1fx1qadbdzljpk7w1xkmw3l1dh50xa2z4s5is")))

