(define-module (crates-io sv g- svg-nd) #:use-module (crates-io))

(define-public crate-svg-nd-0.1.0 (c (n "svg-nd") (v "0.1.0") (d (list (d (n "bezier-nd") (r "^0.1.4") (d #t) (k 0)) (d (n "geo-nd") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1049s6qikzmyj5w3gzbifi53ba5yb5kq6cz79dflmf0yzrmiaq5q")))

(define-public crate-svg-nd-0.1.1 (c (n "svg-nd") (v "0.1.1") (d (list (d (n "bezier-nd") (r "^0.5") (d #t) (k 0)) (d (n "geo-nd") (r "^0.5") (d #t) (k 0)) (d (n "indent-display") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xx6klp5jfx6l6pmzr78gcy61v0jb5i6why60q1my88jv0h80q9q")))

