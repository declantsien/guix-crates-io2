(define-module (crates-io sv g- svg-path-cst) #:use-module (crates-io))

(define-public crate-svg-path-cst-0.0.1 (c (n "svg-path-cst") (v "0.0.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "04zaaa61nfkaz8ixmawglvdcdm1y2r6cvhl4hdwmhxlq45pns88z")))

(define-public crate-svg-path-cst-0.0.2 (c (n "svg-path-cst") (v "0.0.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "1s5jys019lk4nqs81ad9z8px7vsbsdw4m1hqcp3p3j8xpjm4cx41")))

(define-public crate-svg-path-cst-0.0.3 (c (n "svg-path-cst") (v "0.0.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "1pg53qavcmirzbxr8f78d91fx10plzfyjvsmkpyga582kxl0k9p4")))

(define-public crate-svg-path-cst-0.0.4 (c (n "svg-path-cst") (v "0.0.4") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "0jmm11rwmsjsx7fff5fb53q2ph08k2d2pjfms2fkp6k8jwwi0qs4")))

(define-public crate-svg-path-cst-0.0.5 (c (n "svg-path-cst") (v "0.0.5") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "0y1inhvwd2m2a98vif0h6ca8xkppjzmy29rwf4x2l8hwhwmgvmfr")))

(define-public crate-svg-path-cst-0.0.6 (c (n "svg-path-cst") (v "0.0.6") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)))) (h "1vbai563wv4x0ayazzcgazk0zyw8b0zgrb1hmlbc4n2sxc1r9hja")))

(define-public crate-svg-path-cst-0.0.7 (c (n "svg-path-cst") (v "0.0.7") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.7.5") (k 0)))) (h "1cv8b061w4clsfrx3a7fzs27s153csc6m5c3454j3ppihkc2diyg")))

(define-public crate-svg-path-cst-0.1.0 (c (n "svg-path-cst") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "snafu") (r "^0.8.0") (k 0)))) (h "1b9z52ld5qiw0ilbjzy7zii41d95bnaz5xd1idq1bb4j2v0xn7fj")))

