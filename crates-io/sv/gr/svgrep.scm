(define-module (crates-io sv gr svgrep) #:use-module (crates-io))

(define-public crate-svgrep-1.0.0 (c (n "svgrep") (v "1.0.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0vfgjn8va4hqm6jij2aahf8rwzxvf3536ky3m1bggbx0yvn4d5xr")))

(define-public crate-svgrep-1.1.0 (c (n "svgrep") (v "1.1.0") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "05xx44fv22i2mch4dba3gq3c1rkrhvipsag5lp36q3ch6fd5384q")))

(define-public crate-svgrep-1.1.1 (c (n "svgrep") (v "1.1.1") (d (list (d (n "clap") (r "^2.29.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "196988pyl0wbrmh547w8qp859j8si3ccjp9grmr162y9irgywdf5")))

(define-public crate-svgrep-2.0.0 (c (n "svgrep") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0b15500lm3x6q9z70s3m6bh2b7p44871nh8q7r0b5l2a3r3bmz4g")))

(define-public crate-svgrep-2.0.1 (c (n "svgrep") (v "2.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1zhgfim6zhixb7a8c17nvgsy4q5chg8md14lvaxc9hwwm394g19w")))

(define-public crate-svgrep-2.0.2 (c (n "svgrep") (v "2.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0nyjd0q8lfa993ir30v8vcslj5196gg7azx2zy5dbrhwh0rzzmhq")))

(define-public crate-svgrep-2.1.0 (c (n "svgrep") (v "2.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1k0qfiwjc1nnzi92887s9a259w7y2lhciys5d2qshqb9al4ca6lv")))

(define-public crate-svgrep-2.1.1 (c (n "svgrep") (v "2.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "05m7kxg6xd11r3g7r1rlwkp097svd2dadgqw5r0jvv12k67kxp9h")))

(define-public crate-svgrep-2.1.2 (c (n "svgrep") (v "2.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "00m2gjzfgbz2wnyjshw46jlanx82lv0igx8p1zfkfc9i0llfyxlp")))

