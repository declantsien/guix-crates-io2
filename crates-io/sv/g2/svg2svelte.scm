(define-module (crates-io sv g2 svg2svelte) #:use-module (crates-io))

(define-public crate-svg2svelte-0.1.0 (c (n "svg2svelte") (v "0.1.0") (d (list (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.5") (d #t) (k 0)))) (h "13j9vn7g47mbyzj8p4marldmvc9vk2akvphacl3fllccz8iz1w0r")))

(define-public crate-svg2svelte-0.1.1 (c (n "svg2svelte") (v "0.1.1") (d (list (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.5") (d #t) (k 0)))) (h "0qhnc4npmvsafd2l3ryii14viazgjjs7rb6qj22j29vqs0ifamxx")))

(define-public crate-svg2svelte-0.2.0 (c (n "svg2svelte") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.5") (d #t) (k 0)))) (h "0jm5fsz96wbgjwlphawf2a1cfv08cnnjdjq3cnxshxxv68lx6nbc")))

(define-public crate-svg2svelte-0.2.1 (c (n "svg2svelte") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.5") (d #t) (k 0)))) (h "061l315dkmpqy5c2jwln61qsx4jkkml9y4jyz1v0vl6cnlfjmlsy")))

(define-public crate-svg2svelte-0.2.2 (c (n "svg2svelte") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.5") (d #t) (k 0)))) (h "0paqj8qq7zs1b8km56fs0vmmqnx0grbdd4qya1fqfcjv1521d2wf")))

(define-public crate-svg2svelte-0.2.3 (c (n "svg2svelte") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "sedregex") (r "^0.2.5") (d #t) (k 0)))) (h "0wm5l7zv7kr6xpy563nf0z9f56k8g8f5xwc7d12q4ic8g8jcviid")))

