(define-module (crates-io sv g2 svg2webp) #:use-module (crates-io))

(define-public crate-svg2webp-0.1.0 (c (n "svg2webp") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.2") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "1ga0m4mn5p5cdvkzypsxckz9v29jqd8vhpqgazjr1l6zzm2bdyrm")))

(define-public crate-svg2webp-0.1.1 (c (n "svg2webp") (v "0.1.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.2") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "1wkv4jld68dkf9gg760iv6ajvk81383f62dc8bd53mhfd304idlb")))

(define-public crate-svg2webp-0.1.2 (c (n "svg2webp") (v "0.1.2") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.2") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "1jsczrh7acbj5kbx0fsd0al88xd15vfrkcr4mj426ha6labmq9x7")))

(define-public crate-svg2webp-0.1.3 (c (n "svg2webp") (v "0.1.3") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "resvg") (r "^0.36.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.2") (d #t) (k 0)) (d (n "usvg") (r "^0.36.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "03azmf12fd3pzm93k1im9x0awaz5p3fzncwakj71lwl42nybwx83")))

(define-public crate-svg2webp-0.1.4 (c (n "svg2webp") (v "0.1.4") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "resvg") (r "^0.38.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.3") (d #t) (k 0)) (d (n "usvg") (r "^0.38.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "1dgprsvp3zd23g9jasb189dy65bm4lfpxcxl9rqidarj878sbf83")))

(define-public crate-svg2webp-0.1.5 (c (n "svg2webp") (v "0.1.5") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "resvg") (r "^0.39.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.4") (d #t) (k 0)) (d (n "usvg") (r "^0.39.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "0p2fg9j2xzmmjkl69bxsj2fzpl0k424z0p0gg1snkns191jwdl9k")))

(define-public crate-svg2webp-0.1.6 (c (n "svg2webp") (v "0.1.6") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "resvg") (r "^0.40.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.4") (d #t) (k 0)) (d (n "usvg") (r "^0.40.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "1b27hzyi7psakkl5vnh539r2xyzz5cgc8i0fwfi7m6aw5kvjnxxx")))

(define-public crate-svg2webp-0.1.7 (c (n "svg2webp") (v "0.1.7") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "resvg") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.4") (d #t) (k 0)) (d (n "usvg") (r "^0.41.0") (d #t) (k 0)) (d (n "webp") (r "^0.3.0") (d #t) (k 0)))) (h "1yzmy8w746phls58pial2579kiy38spchb71jlnf46argrkxi1xj")))

