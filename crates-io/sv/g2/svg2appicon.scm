(define-module (crates-io sv g2 svg2appicon) #:use-module (crates-io))

(define-public crate-svg2appicon-0.1.0 (c (n "svg2appicon") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "resvg") (r "^0.15.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5.1") (d #t) (k 0)) (d (n "usvg") (r "^0.15.0") (d #t) (k 0)))) (h "1xff3bv4r266bqpxrbvxh7x5ysibzkv5imbzcz294hl7fd584v60")))

(define-public crate-svg2appicon-0.1.1 (c (n "svg2appicon") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "resvg") (r "^0.15.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5.1") (d #t) (k 0)) (d (n "usvg") (r "^0.15.0") (d #t) (k 0)))) (h "100dmhslkry8dxv7nn59zlnng9ckh7h4h4k417ad1ix3nvn2ajar")))

(define-public crate-svg2appicon-0.1.2 (c (n "svg2appicon") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "resvg") (r "^0.15.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.5.1") (d #t) (k 0)) (d (n "usvg") (r "^0.15.0") (d #t) (k 0)))) (h "1790lm7gzc21dkxscnayzp087qjsni3y38bfxhcqlmryfjj09hzi")))

