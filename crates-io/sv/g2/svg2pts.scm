(define-module (crates-io sv g2 svg2pts) #:use-module (crates-io))

(define-public crate-svg2pts-0.1.0 (c (n "svg2pts") (v "0.1.0") (d (list (d (n "lyon_geom") (r "^0.15") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "usvg") (r "^0.9") (d #t) (k 0)))) (h "0j6srx0xbd1sxyv5llblfx0jzv35ls6sqxppdl3rk48yy67di18v")))

(define-public crate-svg2pts-0.1.1 (c (n "svg2pts") (v "0.1.1") (d (list (d (n "lyon_geom") (r "^0.15") (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (d #t) (k 0)))) (h "02fs76i7bxmsqm2sy1vxh1rvb9hylk1dxpxp7h68i2qaa86x6p8x")))

(define-public crate-svg2pts-0.1.2 (c (n "svg2pts") (v "0.1.2") (d (list (d (n "kurbo") (r "^0.5") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.15") (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (d #t) (k 0)))) (h "1kvkcddix9p9pli6wdif5m81mkl63x9xs0zrzx9k10apffvka98c")))

(define-public crate-svg2pts-0.1.3 (c (n "svg2pts") (v "0.1.3") (d (list (d (n "kurbo") (r "^0.5") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.15") (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (d #t) (k 0)))) (h "0pcr265q3rn38m1nyrcg7xs9zrspyia1qwfj8n3z6krkblls7cf2")))

(define-public crate-svg2pts-0.1.3-1 (c (n "svg2pts") (v "0.1.3-1") (d (list (d (n "kurbo") (r "^0.5") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.15") (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (k 0)))) (h "047212kqsnhms0x242ys2bf816br6zvpxg8m4fqjar2dn5lgikp1") (f (quote (("text" "usvg/text") ("default")))) (y #t)))

(define-public crate-svg2pts-0.1.4 (c (n "svg2pts") (v "0.1.4") (d (list (d (n "kurbo") (r "^0.5") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.15") (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (k 0)))) (h "0j2icarz7yfkiyzdd988wngsb6kvf1kv3w1w4frzqafdz88lmc1b") (f (quote (("text" "usvg/text") ("default"))))))

(define-public crate-svg2pts-0.1.5 (c (n "svg2pts") (v "0.1.5") (d (list (d (n "kurbo") (r "^0.5") (d #t) (k 0)) (d (n "lyon_geom") (r "^0.15") (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (k 0)))) (h "1kvdvcz0xp8kp7kfqg7cfcfdvyv0m4ms3xysknml1qnww0insxax") (f (quote (("text" "usvg/text") ("default"))))))

