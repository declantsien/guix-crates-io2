(define-module (crates-io sv g_ svg_fmt) #:use-module (crates-io))

(define-public crate-svg_fmt-0.1.0 (c (n "svg_fmt") (v "0.1.0") (h "0b85k4jmy0mqgq04iskajgjmy6cvdid2iw1gbl048qksfc4ljp88")))

(define-public crate-svg_fmt-0.2.0 (c (n "svg_fmt") (v "0.2.0") (h "1w314j1jngkj4gbj431kyks7810wgr0lywyw6nx6fgbsnff63b0f")))

(define-public crate-svg_fmt-0.2.1 (c (n "svg_fmt") (v "0.2.1") (h "1z06f8nzjk65l2jzpvfgcb0iqajwm3wrm2prs46g6dypi5ggkr90")))

(define-public crate-svg_fmt-0.3.0 (c (n "svg_fmt") (v "0.3.0") (h "1nyr91y3n7jpmf3zgrszp9gr575zhj556jki0acbywvx7x7nl3q9")))

(define-public crate-svg_fmt-0.4.0 (c (n "svg_fmt") (v "0.4.0") (h "0ayksvzxhpa7q4vqayqm6dd3sbypfy8ayw7pg82hxqp1v3zg0rn6")))

(define-public crate-svg_fmt-0.4.1 (c (n "svg_fmt") (v "0.4.1") (h "1qjhciyls66jw9p4m7zy20ziqp39z9h44l0wzjfjxvhjyhaxzccg")))

(define-public crate-svg_fmt-0.4.2 (c (n "svg_fmt") (v "0.4.2") (h "16dl8bd7sm2bgvilqp81flk6zbc2fwphm6xqxxvgnpi6lc1aafzq")))

(define-public crate-svg_fmt-0.4.3 (c (n "svg_fmt") (v "0.4.3") (h "1jpjmac2pvf37k5pn0ngdcvg481yx0k4zx9yarsxcpyg8q7nmq90")))

