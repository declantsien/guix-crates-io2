(define-module (crates-io sv g_ svg_definitions) #:use-module (crates-io))

(define-public crate-svg_definitions-0.1.0 (c (n "svg_definitions") (v "0.1.0") (h "1dpdcyyqxr9ni0q57hghd4plsm4gqdd1jwb7596bw0y9lihx8gx1")))

(define-public crate-svg_definitions-0.2.0 (c (n "svg_definitions") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0k74m6v30na1gplmwx5g5q9ll4nbfg13n22gx7mx2j5r6r0mc1nj")))

(define-public crate-svg_definitions-0.3.0 (c (n "svg_definitions") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0q4qdq8w8qj063a8gg2l2y6qw1m7sh5fgqdaly5l76vk8fxwjm5c") (f (quote (("parsing" "roxmltree"))))))

(define-public crate-svg_definitions-0.3.1 (c (n "svg_definitions") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1hyg3vglvcj1rs5xhcgbc9y8h672qcq7fdq9ip1g2fg2yhjkldrv") (f (quote (("parsing" "roxmltree"))))))

(define-public crate-svg_definitions-0.3.2 (c (n "svg_definitions") (v "0.3.2") (d (list (d (n "roxmltree") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1x9dmnh9y32yzss0zrkvlgw8llvz2w4r0caag52rhl0ijxlc11rb") (f (quote (("parsing" "roxmltree"))))))

