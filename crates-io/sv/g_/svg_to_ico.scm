(define-module (crates-io sv g_ svg_to_ico) #:use-module (crates-io))

(define-public crate-svg_to_ico-0.1.0 (c (n "svg_to_ico") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0xx6n6q443ksfc6bzffhcqx2dxz0v4qfpz4gz5w4b6850g5mp0fj")))

(define-public crate-svg_to_ico-1.0.0 (c (n "svg_to_ico") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "nsvg") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1nljicmja5f9g4bfz6qxa5f9xnrkyllq4195qhw56ij3vadv3jb5")))

(define-public crate-svg_to_ico-1.2.0 (c (n "svg_to_ico") (v "1.2.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "ico") (r "^0.1.0") (d #t) (k 0)) (d (n "resvg") (r "^0.23") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tiny-skia") (r "^0.6") (d #t) (k 0)) (d (n "usvg") (r "^0.23") (d #t) (k 0)))) (h "06w28pyj36i9pdrisxwzmvjgy2c9a9gfzdanvddd735v6ls0pf7i")))

