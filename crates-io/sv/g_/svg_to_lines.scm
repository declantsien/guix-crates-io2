(define-module (crates-io sv g_ svg_to_lines) #:use-module (crates-io))

(define-public crate-svg_to_lines-0.1.0 (c (n "svg_to_lines") (v "0.1.0") (d (list (d (n "ash") (r "^0.29") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)) (d (n "skia-safe") (r "^0.21") (f (quote ("vulkan"))) (d #t) (k 2)) (d (n "skulpin") (r "^0.4.0") (d #t) (k 2)) (d (n "svgtypes") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha4") (d #t) (k 2)))) (h "029bgl2a4p58vpv1mhbg675jp18n4gqs0nxd05m05hp2v4gmf7sa")))

