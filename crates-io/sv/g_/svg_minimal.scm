(define-module (crates-io sv g_ svg_minimal) #:use-module (crates-io))

(define-public crate-svg_minimal-0.1.0 (c (n "svg_minimal") (v "0.1.0") (h "1ghq8rns9hc2k5acsclan0g0i79qbxminj2kc51xffapaxbw5hp9")))

(define-public crate-svg_minimal-0.1.1 (c (n "svg_minimal") (v "0.1.1") (h "0klklaan7shzlix98lay1cq73lar03cb07acvb6b0lcqrlgnhs91")))

