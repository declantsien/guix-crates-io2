(define-module (crates-io sv g_ svg_to_png) #:use-module (crates-io))

(define-public crate-svg_to_png-0.1.0 (c (n "svg_to_png") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.4.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rsvg") (r "^0.4.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.6") (d #t) (k 0)))) (h "1pim0hxkc5zd5lhx14sbsndmf6k2kfmbsla8dimhhp2rbka6na4y")))

