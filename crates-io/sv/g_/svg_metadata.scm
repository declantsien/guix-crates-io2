(define-module (crates-io sv g_ svg_metadata) #:use-module (crates-io))

(define-public crate-svg_metadata-0.1.0 (c (n "svg_metadata") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "16lk0236ay3q6lkra43n9jpcz335ngqh1j1a9m3sw8kyl5qricc0")))

(define-public crate-svg_metadata-0.2.0 (c (n "svg_metadata") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "08vsyd7xy0ngnf5hbkjk03yjrsax5jynaf9az97wznlmyiw1baq6")))

(define-public crate-svg_metadata-0.2.1 (c (n "svg_metadata") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0k8pz48g9jd5qs8ssi8pp3w4k7v5w1a773ya36i41jsg6yvbkrkk")))

(define-public crate-svg_metadata-0.3.0 (c (n "svg_metadata") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1gh4a918bfxz21z0nvj8xdl4jfgz439j8w8ycmginn6v9k6d8x8j")))

(define-public crate-svg_metadata-0.4.0 (c (n "svg_metadata") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "roxmltree") (r "^0.11.0") (d #t) (k 0)))) (h "03wvsjvbf384aq0w7nm0qivkr2h3l9kfh0zmfc4nrkx4igaqbw5i")))

(define-public crate-svg_metadata-0.4.1 (c (n "svg_metadata") (v "0.4.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "roxmltree") (r "^0.11.0") (d #t) (k 0)))) (h "0mqwhbd6698dvvv1ggkakw5lcz3m5spxqla9f1az0f93svxv3raz")))

(define-public crate-svg_metadata-0.4.2 (c (n "svg_metadata") (v "0.4.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "roxmltree") (r "^0.13") (d #t) (k 0)))) (h "1c4fjclk84pz7jmp3yn30sc1ifa41q5i8hma2xm0jqgy5qk3d7yk")))

(define-public crate-svg_metadata-0.4.3 (c (n "svg_metadata") (v "0.4.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.15.0") (d #t) (k 0)))) (h "1k8cwmki1lyn01q33kyad92ngwz0q2r11d4df48dali5bjfxzkpa")))

(define-public crate-svg_metadata-0.4.4 (c (n "svg_metadata") (v "0.4.4") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "002j0na1kfz4pgi43hdcz5baygzk6irnjd5lrmbqqfjldwn3sbx4")))

(define-public crate-svg_metadata-0.5.0 (c (n "svg_metadata") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "0d3wz430xgpqhjysgkfmbh5wzb0p5zmvcbs3wljpps93lhznm8xx")))

(define-public crate-svg_metadata-0.5.1 (c (n "svg_metadata") (v "0.5.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "roxmltree") (r "^0.20.0") (d #t) (k 0)))) (h "0717wb4zkj5qdypg58zs7m1x45i49pdrhzga0k0wvqn2g2y7qk17")))

