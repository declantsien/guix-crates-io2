(define-module (crates-io sv g_ svg_face) #:use-module (crates-io))

(define-public crate-svg_face-0.1.0 (c (n "svg_face") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "1jjjs4kg207x19yhj9jbrdg4ybzharc3yl366zl5dgmp9sr8madn")))

(define-public crate-svg_face-0.1.1 (c (n "svg_face") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "0hwylqhvh07dsxkikvrc859bdlc0c4r9n01c8akd02cl60pj44jg")))

(define-public crate-svg_face-0.1.2 (c (n "svg_face") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)))) (h "13rb2ckdlkvy9zhli5953qimr73l884b4pgygx23a4zamiphl994")))

(define-public crate-svg_face-0.1.3 (c (n "svg_face") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)))) (h "0br2pv3ca295439iqp46c2m8rq73b8knanx442a3wmry7nrjy6h4")))

