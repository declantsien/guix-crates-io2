(define-module (crates-io sv g_ svg_panelize) #:use-module (crates-io))

(define-public crate-svg_panelize-0.1.0 (c (n "svg_panelize") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "089y6nl4svsgs7kws4hij6knjhds7k6sxi09fxmqs9pfrk6kv7b9")))

