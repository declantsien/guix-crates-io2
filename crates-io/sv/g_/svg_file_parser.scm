(define-module (crates-io sv g_ svg_file_parser) #:use-module (crates-io))

(define-public crate-svg_file_parser-0.1.0 (c (n "svg_file_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0d382qijpcaqia76105qf6gmgxhrad3cq0sk7x8q3l2nq648r0da")))

