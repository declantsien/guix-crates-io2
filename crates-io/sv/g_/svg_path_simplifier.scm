(define-module (crates-io sv g_ svg_path_simplifier) #:use-module (crates-io))

(define-public crate-svg_path_simplifier-0.2.1 (c (n "svg_path_simplifier") (v "0.2.1") (d (list (d (n "clap") (r "~4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kurbo") (r "~0.9") (d #t) (k 0)) (d (n "usvg") (r "~0.28") (d #t) (k 0)))) (h "0wkg9javqmbzcbb0q8wghk0msdiz8jbyqyv37fv4zbs9gzah6b75")))

