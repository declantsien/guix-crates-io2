(define-module (crates-io sv g_ svg_cleaner) #:use-module (crates-io))

(define-public crate-svg_cleaner-0.1.0 (c (n "svg_cleaner") (v "0.1.0") (d (list (d (n "clap") (r "^2") (o #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "fern") (r "=0.5.8") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "svgdom") (r "^0.10.5") (d #t) (k 0)))) (h "1fy4q3i3sqnj7sisdpsinwklcn00ci62varipckd9k3mpypwn0ys") (f (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

