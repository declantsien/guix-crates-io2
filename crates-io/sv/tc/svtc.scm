(define-module (crates-io sv tc svtc) #:use-module (crates-io))

(define-public crate-svtc-0.1.0 (c (n "svtc") (v "0.1.0") (h "1dgb28fys7mk514ig24h5182sva9xx30wlg6gdxklaz6x7qd0dsq")))

(define-public crate-svtc-0.1.1 (c (n "svtc") (v "0.1.1") (h "1n9cyza6jrxmkrwwixrd6lwwa1zhv7md9nb88qi39ag1smaylw3a")))

