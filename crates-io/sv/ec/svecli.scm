(define-module (crates-io sv ec svecli) #:use-module (crates-io))

(define-public crate-svecli-0.2.1 (c (n "svecli") (v "0.2.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1x9hb9hrpaw3znyjk55gzsav4531qcwl536h8dljz1ac4d4l7qzc")))

(define-public crate-svecli-0.2.2 (c (n "svecli") (v "0.2.2") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1y377jykxxzkmk4xfza53zwcf00a8pqqcsqf7311zh31m8x526j1")))

(define-public crate-svecli-0.2.4 (c (n "svecli") (v "0.2.4") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "0g1cgi9nrjl8cg5axnbl1qcqhbw2vdqb4fgxpinzz0sw1hmwzxzx")))

(define-public crate-svecli-0.2.5 (c (n "svecli") (v "0.2.5") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1vc0n43yqfz8ym2x595g60wnksvvnkyglrd6s8df5pb0x8fl26r4")))

(define-public crate-svecli-0.2.6 (c (n "svecli") (v "0.2.6") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "17h517m3hgb4gr2b5sznxdh7dxjazngn20kig7nlk1crqr0m1vip")))

(define-public crate-svecli-0.2.7 (c (n "svecli") (v "0.2.7") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "036fn4bm3q6rqx7p0sq4bazd91cdvx2xm3crfz76q6n73s7yqddk")))

(define-public crate-svecli-0.2.8 (c (n "svecli") (v "0.2.8") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1m7n0f392kxlma140vyjbj079xiiavfp28wfn080j78w6sqzkvl6")))

(define-public crate-svecli-0.2.9 (c (n "svecli") (v "0.2.9") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1z24gialg357pcfmwh0qx0wq1dcaggsir65i0zcjxcm1xwv91ilf")))

(define-public crate-svecli-0.2.10 (c (n "svecli") (v "0.2.10") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "0sv4b9wrj8vm9gmvcf3c2jfjk8vz3qcx4vvi0apb5jmqx7p5c9nd")))

(define-public crate-svecli-0.2.11 (c (n "svecli") (v "0.2.11") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1s655g4x43r6x3hg43nl9sdg8h10v1qajcil8cgv3mdzqnq6kh7d")))

(define-public crate-svecli-0.3.0 (c (n "svecli") (v "0.3.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "116m6ff5jki6947apz8gsczplav6n4ad8bj80s1vpaq0wknybipc")))

(define-public crate-svecli-0.3.1 (c (n "svecli") (v "0.3.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1xgq1q412fdxnp135m3ngvm1wmsa872wrk99svrr72mjp8826zgg")))

(define-public crate-svecli-0.3.2 (c (n "svecli") (v "0.3.2") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1x71a0cjmm99lvlrfrva1smjjk91mcsnqfg6lx24sd8v9ymmq1yk")))

(define-public crate-svecli-0.3.3 (c (n "svecli") (v "0.3.3") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1jk1pdd2n67p66bm7qdzwjhk58w4yc04v897ghidqn4mjvbal7fp")))

(define-public crate-svecli-0.4.0 (c (n "svecli") (v "0.4.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "clap") (r "^3.1.0") (f (quote ("unicode" "cargo"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "0sqfrqqj2snwfnxf4mk95frvigqz93qz609711kjpdknv9sdpli3")))

