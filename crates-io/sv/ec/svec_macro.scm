(define-module (crates-io sv ec svec_macro) #:use-module (crates-io))

(define-public crate-svec_macro-0.1.0 (c (n "svec_macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "0znpg14rndb8q5qx9nj5wikk3ix1p1zrs0b1hhyc7n57kmp5cpnd")))

