(define-module (crates-io sv d2 svd2utra) #:use-module (crates-io))

(define-public crate-svd2utra-0.1.0 (c (n "svd2utra") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "1sk8p5fj002rwxrqxg6k33zsai84igsqh7y3qj6y05g4cwqbhhqx")))

(define-public crate-svd2utra-0.1.1 (c (n "svd2utra") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "029fqqdx4lns3gfvlf9mcgmnifam3fym2rsrsgb5v97wvvzfbg7y")))

(define-public crate-svd2utra-0.1.2 (c (n "svd2utra") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "0mqzyf0v8n2171sbp85ixkv03qxqy01xhs1dpnzbhkf98dis3z6j")))

(define-public crate-svd2utra-0.1.3 (c (n "svd2utra") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "158q5nk47dixd7i7h0ag5cvf243f4yfqjvabcig3n100hsr2ygyp")))

(define-public crate-svd2utra-0.1.4 (c (n "svd2utra") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "0dr6yd3m0r99carcr8xnbzmwgngmf37cq56j4y98h1x4pjf2xdg7")))

(define-public crate-svd2utra-0.1.5 (c (n "svd2utra") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "1kzp2k715ircvw066mcrw7q5bzfw45p9kq1skjiv4hvn3cmi5cr1")))

(define-public crate-svd2utra-0.1.6 (c (n "svd2utra") (v "0.1.6") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "05vl12vr1bpsmz7vx0z27lnzpdwngxnm6dgr40z95g6cjgvnr024")))

(define-public crate-svd2utra-0.1.7 (c (n "svd2utra") (v "0.1.7") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "1p86l0fcbllb2kg3f80wzlx49ivd70j84ih385bavjcyzwfghp56")))

(define-public crate-svd2utra-0.1.8 (c (n "svd2utra") (v "0.1.8") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "0hw7i91cxj4aqwsw6y9vr3k26ds9622rp3m5k4xsn0pr06v92zpb")))

(define-public crate-svd2utra-0.1.9 (c (n "svd2utra") (v "0.1.9") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "0ih0n80yncl01nxgykm0zdp37ns4iam1s44gng6izgha2yqydzby")))

(define-public crate-svd2utra-0.1.10 (c (n "svd2utra") (v "0.1.10") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "0f3nzhx1fcwi22cprgp7qcf24dkzha1yd9sifkf8f3q5smj9q6bg")))

(define-public crate-svd2utra-0.1.11 (c (n "svd2utra") (v "0.1.11") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "1hn2wrhns4w9nqbq3wy26z55cnkdnngsxr8xqpwl8w97rqlzadin")))

(define-public crate-svd2utra-0.1.12 (c (n "svd2utra") (v "0.1.12") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "06ia44kv3khzwidh07ikinj8sj53xx67c27g89ql926zjgl93jmq")))

(define-public crate-svd2utra-0.1.13 (c (n "svd2utra") (v "0.1.13") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.19.0") (d #t) (k 0)))) (h "1mnd4r94q5v236w807shr82v22p41v0zm2bjcwskai81fzp9qk4g")))

(define-public crate-svd2utra-0.1.14 (c (n "svd2utra") (v "0.1.14") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "1r0k7nsmgvmkzqim51n6dfp3c9isdqx4fccrjw5bq392p22wmqyc")))

(define-public crate-svd2utra-0.1.15 (c (n "svd2utra") (v "0.1.15") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "190c3hf066b3ld2p4bw17r18xac6id79mn97966yhj3zkcymgfrg")))

(define-public crate-svd2utra-0.1.16 (c (n "svd2utra") (v "0.1.16") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "03lzjpz0pb9k1diwbivkg14dwg8fchhrbifkf8h8vsmik74lbwz1")))

(define-public crate-svd2utra-0.1.17 (c (n "svd2utra") (v "0.1.17") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "1d53fiqd5nkjib4bpajjjbv3wh4kl1g4jns0jvn9i4r9hpf1fw51")))

(define-public crate-svd2utra-0.1.18 (c (n "svd2utra") (v "0.1.18") (d (list (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "0qasq3bf8i4b7zh9daznlj1ib7d2d9kkm684b7n579c1vs0ppa1b")))

(define-public crate-svd2utra-0.1.19 (c (n "svd2utra") (v "0.1.19") (d (list (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "0z5pdh9yn5xn74ya1wfnzqcdji2c3qbk4ys7zq1cx7bh2i5rhvjq")))

(define-public crate-svd2utra-0.1.20 (c (n "svd2utra") (v "0.1.20") (d (list (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "0cwbc0wc0slnf430d4b9v96cq7ph5z6hhvh5qz33i1014av45m44")))

(define-public crate-svd2utra-0.1.21 (c (n "svd2utra") (v "0.1.21") (d (list (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "03f4yqdv1yr9fimq07hkpgnw3vzkpsjci9vqbn34n62n9m4w8l5w")))

(define-public crate-svd2utra-0.1.22 (c (n "svd2utra") (v "0.1.22") (d (list (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)))) (h "0l6hqf2dcw5p4xybjdz2cx4av44b4blmsjhlx7f2j93j5fqahnn2")))

