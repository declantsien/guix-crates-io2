(define-module (crates-io sv m4 svm40) #:use-module (crates-io))

(define-public crate-svm40-0.0.1 (c (n "svm40") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "0vg6jq3ywycn15byzp39hs198y6590yqamgg74g0mx8q5y1v6ixh")))

(define-public crate-svm40-0.0.2 (c (n "svm40") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "172kjzkm3f74f8dbmqppi0f07r0agw2klkg61i5rcw4g97nb8c1h")))

(define-public crate-svm40-1.0.0 (c (n "svm40") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "sensirion-i2c") (r "^0.1") (d #t) (k 0)))) (h "0xnxnn8vqc9jvfj7pp66mc4szgkals6hgrpqhb3xi0av97l9x4pp")))

