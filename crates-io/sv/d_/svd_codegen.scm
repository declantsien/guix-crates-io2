(define-module (crates-io sv d_ svd_codegen) #:use-module (crates-io))

(define-public crate-svd_codegen-0.1.0 (c (n "svd_codegen") (v "0.1.0") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "11rwy5q6rvhp9an19lkyixivgg9diak9jiyhmiqrfcdmlkfgw68v")))

(define-public crate-svd_codegen-0.1.1 (c (n "svd_codegen") (v "0.1.1") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "0mhj3i48xmg9qhs5nf8imcml5l6yf571i4kray62yz8ffvb1vfs9")))

(define-public crate-svd_codegen-0.2.0 (c (n "svd_codegen") (v "0.2.0") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1gymrhsnqc9f3iflg4yqw5b5dgpq6jvvqm1agqvifzggmdn7ivmj")))

(define-public crate-svd_codegen-0.2.1 (c (n "svd_codegen") (v "0.2.1") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "03whf5kx7knsad4zszwh5knmbrffvymzbnyfq35k897jdkkmsp2p") (y #t)))

(define-public crate-svd_codegen-0.3.0 (c (n "svd_codegen") (v "0.3.0") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "1236786zkam533z197ydvsfq9sf8f4q81vdlbz8h5m4y7b0h2pg7")))

(define-public crate-svd_codegen-0.4.0 (c (n "svd_codegen") (v "0.4.0") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)))) (h "024llbrc5grfxlslqvgi5y7z0lmz4yxdmxwy23ynnc5nxgbky7zg")))

