(define-module (crates-io sv d_ svd_board) #:use-module (crates-io))

(define-public crate-svd_board-0.1.0 (c (n "svd_board") (v "0.1.0") (d (list (d (n "inflections") (r "^1.0.0") (d #t) (k 1)) (d (n "once") (r "^0.3.2") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 1)) (d (n "svd_codegen") (r "^0.1.1") (d #t) (k 1)) (d (n "volatile") (r "^0.2.0") (d #t) (k 0)))) (h "1cxy4gfz6fjpb8fz5h6vvlkgxn8x1iy8xn6aprx19n2zfvmp6ia4") (f (quote (("stm32f7") ("stm32f46"))))))

(define-public crate-svd_board-0.2.0 (c (n "svd_board") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.6.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 1)) (d (n "once") (r "^0.3.2") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 1)) (d (n "svd_codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "volatile") (r "^0.2.0") (d #t) (k 0)))) (h "03g7z5vr3v8194c51ycdl04nibl1gr105airn6v3qhrks72gdqns") (f (quote (("stm32f7") ("stm32f46"))))))

(define-public crate-svd_board-0.2.1 (c (n "svd_board") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.6.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 1)) (d (n "once") (r "^0.3.2") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 1)) (d (n "svd_codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "volatile") (r "^0.2.0") (d #t) (k 0)))) (h "15manpj51lzmk1a0cqbdwick9v6px7k6ikb9w18x22q87vwinilv") (f (quote (("stm32f7") ("stm32f46"))))))

(define-public crate-svd_board-0.2.2 (c (n "svd_board") (v "0.2.2") (d (list (d (n "bit_field") (r "^0.6.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 1)) (d (n "once") (r "^0.3.2") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 1)) (d (n "svd_codegen") (r "^0.3.0") (d #t) (k 1)) (d (n "volatile") (r "^0.2.0") (d #t) (k 0)))) (h "0qgrh9ahbmnvdh5ds04imhpkhkpmc1d54gxkfk1pafwzknysw80r") (f (quote (("stm32f7") ("stm32f46"))))))

(define-public crate-svd_board-0.3.0 (c (n "svd_board") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.6.0") (d #t) (k 0)) (d (n "inflections") (r "^1.0.0") (d #t) (k 1)) (d (n "once") (r "^0.3.2") (d #t) (k 0)) (d (n "svd-parser") (r "^0.1.1") (d #t) (k 1)) (d (n "svd_codegen") (r "^0.4.0") (d #t) (k 1)) (d (n "volatile") (r "^0.2.0") (d #t) (k 0)))) (h "03zfc6im65ynx2yw8pkgqj3syans5fb9ln77qhv4j6r0j5c0qp1j") (f (quote (("stm32f7") ("stm32f46"))))))

