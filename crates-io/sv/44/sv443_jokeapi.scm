(define-module (crates-io sv #{44}# sv443_jokeapi) #:use-module (crates-io))

(define-public crate-sv443_jokeapi-0.1.0 (c (n "sv443_jokeapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c6r41jgblka31c7mklaqb702sjw6k8z6jcwmy4np1i5kfmklqdr")))

(define-public crate-sv443_jokeapi-0.1.1 (c (n "sv443_jokeapi") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hdclq8bv1by0fn8j2zjvqx08f651v940378a09dqwpxxpk158xf")))

(define-public crate-sv443_jokeapi-0.2.0 (c (n "sv443_jokeapi") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "16kpbr1dzinrgdf0gg5vx7f49jxr2jkv4qfkh22x4q8jx5zlprg9")))

