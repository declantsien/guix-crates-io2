(define-module (crates-io sv -p sv-parser-macros) #:use-module (crates-io))

(define-public crate-sv-parser-macros-0.1.0 (c (n "sv-parser-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1v35fmg2c7hg0ibmbh653y95hywgpdvq9sf2vn33kf03ji96cgrv")))

(define-public crate-sv-parser-macros-0.1.1 (c (n "sv-parser-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1w1jjl862ybnmhv8660fmb95qlx4xlv36mvbmgs5i4n6y4zqlwpj")))

(define-public crate-sv-parser-macros-0.1.2 (c (n "sv-parser-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0qlvz23r46xlv46ka4vjx5wbhscjkpf54q1gj997v1a4zzcikxfa")))

(define-public crate-sv-parser-macros-0.1.4 (c (n "sv-parser-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1klr370vwl3jlk0zmpksq0yk7iqvzjhjyp0bf19xm4wh1r3cpgjh")))

(define-public crate-sv-parser-macros-0.1.5 (c (n "sv-parser-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0l62v2xns7jqj7lzmr3rw06injinx5wnd87miq5x5ddsv9i5c55r")))

(define-public crate-sv-parser-macros-0.1.6 (c (n "sv-parser-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1kd3kq0lsz8dx1i648hvxslqc8vf7gvnakjflds471n5zqi3pxkn")))

(define-public crate-sv-parser-macros-0.1.7 (c (n "sv-parser-macros") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "03fny33s3bpx0s5mhfkv0w1p974l3wyvnlbj2n3imsycrscpcq9v")))

(define-public crate-sv-parser-macros-0.2.0 (c (n "sv-parser-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1n4chfl4c2q6fj7hbm547vgfrn6hlk9ndrww6x1lpznj4r7chn7d")))

(define-public crate-sv-parser-macros-0.2.1 (c (n "sv-parser-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0ikhjsrnzvlzlj99g66x277ndx45qv9lszxs82fddsb0nsjpn595")))

(define-public crate-sv-parser-macros-0.3.0 (c (n "sv-parser-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0d5yh017fnjsvfkjysvy70bpwgp7c5lzm59m1d95daxm1mp54056")))

(define-public crate-sv-parser-macros-0.3.1 (c (n "sv-parser-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1g13dpm68kg3kgw87h22hdkrg9xwx5l9q17nwzidmn03vyc7d369")))

(define-public crate-sv-parser-macros-0.3.2 (c (n "sv-parser-macros") (v "0.3.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "13s530mi4f4l9fzypfh3hqwxynk1bi742n05jp44cciikc965pps")))

(define-public crate-sv-parser-macros-0.3.3 (c (n "sv-parser-macros") (v "0.3.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1d9ymdsqjgdkd9mb202lsq8f8kn58xdwa8hi67i4vk437yy64xmp")))

(define-public crate-sv-parser-macros-0.3.4 (c (n "sv-parser-macros") (v "0.3.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0ydhd3m0gvmmkb85zwdgnl4frp9iadjpzxbvv2vmyvxbx6gj4fa4")))

(define-public crate-sv-parser-macros-0.3.5 (c (n "sv-parser-macros") (v "0.3.5") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0lyiy40xcgily0jix9sz28zb8x2v73vz39i8kprj6776msifs76m")))

(define-public crate-sv-parser-macros-0.3.6 (c (n "sv-parser-macros") (v "0.3.6") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1hc8y9ns1b4d3xzl68swlycnw2hddvlyhrdi4wd37rh7hqx4pqk9")))

(define-public crate-sv-parser-macros-0.3.7 (c (n "sv-parser-macros") (v "0.3.7") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1x9jv4blc7x3s70yrh53fjydmzrzvddjaai866xak8xn7ykv12js")))

(define-public crate-sv-parser-macros-0.4.0 (c (n "sv-parser-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0wfjdlgf9xxy5b3lncqr7h5ksdnyx9qw347a25lmywby2ii5m5cc")))

(define-public crate-sv-parser-macros-0.4.1 (c (n "sv-parser-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0kv59bk8a1ygvpdsnhjdx5cs1r3bmxw6i3f3iw0l59qy7apkdhnq")))

(define-public crate-sv-parser-macros-0.4.2 (c (n "sv-parser-macros") (v "0.4.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0m5vs8lg80v63x0755yzl46jych8vrqj7hz4ynsvcsrj6fyp6vl8")))

(define-public crate-sv-parser-macros-0.4.3 (c (n "sv-parser-macros") (v "0.4.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "151asnbqi4kzkf9zmk86qw2fhiych77mk4mlhl12mngkwz1216gx")))

(define-public crate-sv-parser-macros-0.4.4 (c (n "sv-parser-macros") (v "0.4.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "05bprj5lhrlgd5n15d0c67pfrz7vqvc26ylx1rd708bhyb5chzbh")))

(define-public crate-sv-parser-macros-0.4.5 (c (n "sv-parser-macros") (v "0.4.5") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "17rnn6zp4zpvhcg4zy02plzpak25jl7976ja3ircx5xvnrwfql3a")))

(define-public crate-sv-parser-macros-0.4.6 (c (n "sv-parser-macros") (v "0.4.6") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0h1rnsbnmmshswyki4yriq8yq8mnnv21yd557ncwl0ai7h8f7cj1")))

(define-public crate-sv-parser-macros-0.4.7 (c (n "sv-parser-macros") (v "0.4.7") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0y2rrnnv5pxqmklj5kpf9hajhgxjpfwia3kwjwk9xcxxvnhb7akl")))

(define-public crate-sv-parser-macros-0.4.8 (c (n "sv-parser-macros") (v "0.4.8") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1il4bgdmsivvzqjkh7660ns1j90dkq9n0955kw33zbb2xaslmqwb")))

(define-public crate-sv-parser-macros-0.4.9 (c (n "sv-parser-macros") (v "0.4.9") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1i7xg2psnw359ka88drl5fkns0g78blfnclrf7l5nwmy4csf2fii")))

(define-public crate-sv-parser-macros-0.4.10 (c (n "sv-parser-macros") (v "0.4.10") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1ryf8vsdi4pphdwxsm1y7rwy1jlbw3n7rm98d43akcy37cf2dg1s")))

(define-public crate-sv-parser-macros-0.4.11 (c (n "sv-parser-macros") (v "0.4.11") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "19cdasmrhxrr1v47zzm0qbqnpd52jnd0c1n17id32kynryshbr0n")))

(define-public crate-sv-parser-macros-0.4.12 (c (n "sv-parser-macros") (v "0.4.12") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "060l2apaqa0j9c0v9xxz56akrglln2rjrk9pzxa327qqa9v969a0")))

(define-public crate-sv-parser-macros-0.4.13 (c (n "sv-parser-macros") (v "0.4.13") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0kkmvn8gk0xq58wzi3iryq6ip9ayfpa256wpwxrh87k4ncf2fdj4")))

(define-public crate-sv-parser-macros-0.4.14 (c (n "sv-parser-macros") (v "0.4.14") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1kfs539xcn5nd08g5820zfdcbk6jc2chp162af3d4nkrra918ki9")))

(define-public crate-sv-parser-macros-0.4.16 (c (n "sv-parser-macros") (v "0.4.16") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "05qlnz1m94pml0fg3bgnhnylpwyb6ls01ifi0pgjfbisiqcgyh8g")))

(define-public crate-sv-parser-macros-0.4.17 (c (n "sv-parser-macros") (v "0.4.17") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1bivi25dp5l7ih1zcv5gkgfpryv3rgkfx8xmlsy48ssm2wm0520k")))

(define-public crate-sv-parser-macros-0.4.18 (c (n "sv-parser-macros") (v "0.4.18") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1h8vcvpr48n7qy6afdva9wpr2yc1smdcba1p4h5z4njpf2g5kjvg")))

(define-public crate-sv-parser-macros-0.4.19 (c (n "sv-parser-macros") (v "0.4.19") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "10x332c5dvyax62v035j8j4ywakgaids11f9akkavqr6i8v36gcc")))

(define-public crate-sv-parser-macros-0.4.20 (c (n "sv-parser-macros") (v "0.4.20") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0qkdkg36f7v5hk631i55z8dz60j1jf98fk343y5hc7xk9fnkhipf")))

(define-public crate-sv-parser-macros-0.5.0 (c (n "sv-parser-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1414rk1hdnnhrnyv7r2km6xdrrgzf5qimvnjvg5l179nrxh7ipwj")))

(define-public crate-sv-parser-macros-0.6.0 (c (n "sv-parser-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0r8l0ir1vcd8sbqqxzcsbmdd74q6p4y01krcgffnzrymgvn05dxl")))

(define-public crate-sv-parser-macros-0.6.1 (c (n "sv-parser-macros") (v "0.6.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1vkqhviwdsqjmci87xg4incvk42pa1px3i4wxaqdl7w9icxzhyn2")))

(define-public crate-sv-parser-macros-0.6.3 (c (n "sv-parser-macros") (v "0.6.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1xvmqrg8ks5zn9mcragys4b1jq5x3vs1yrlq70dinyzqqnmc437m")))

(define-public crate-sv-parser-macros-0.6.4 (c (n "sv-parser-macros") (v "0.6.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1yfxmc72hydy1cgpczi3lh1dh7rx1r2frh11cavxan6f5408z9yx")))

(define-public crate-sv-parser-macros-0.6.5 (c (n "sv-parser-macros") (v "0.6.5") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0y5lm9s1ypvv9y13pp3428m9cq4zx6fchnsfp04p3pp5xgy80qva")))

(define-public crate-sv-parser-macros-0.7.0 (c (n "sv-parser-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0qv8wd2h2lh3f539srx4z49jram6nsl8x4yc05swyxy6z6cqcjrr")))

(define-public crate-sv-parser-macros-0.8.0 (c (n "sv-parser-macros") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0yqzndl69rygkpnla68knhjyha8pqjwphbqy41ai2irsx7pcxn5j")))

(define-public crate-sv-parser-macros-0.8.1 (c (n "sv-parser-macros") (v "0.8.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0g87cwc18i653g1fs4pfwydji9f2y7p9maq9fwx1xi9qzz52wwdb")))

(define-public crate-sv-parser-macros-0.8.2 (c (n "sv-parser-macros") (v "0.8.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1rmfjldhdkkrrr0acd9cs4bxpxy60msfxj8xfpyam6fm7y2hc6ji")))

(define-public crate-sv-parser-macros-0.8.3 (c (n "sv-parser-macros") (v "0.8.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0lqpqfw38pkscygwlanxzyshwa6zasqcbyg9fmxg6wllypm1hp8w")))

(define-public crate-sv-parser-macros-0.9.0 (c (n "sv-parser-macros") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1my1xbjrx45mdsi0hlx3l27nlarsmj4ra1rhy1v9lx0bdzg61cw5")))

(define-public crate-sv-parser-macros-0.10.0 (c (n "sv-parser-macros") (v "0.10.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0srv7wdgvyv1cs15v9cbsygd7gwfj0wp9cqmqf05hgd10w45v973")))

(define-public crate-sv-parser-macros-0.10.1 (c (n "sv-parser-macros") (v "0.10.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "17adrxpgw7i3hj32b9h6gng8vqbzgm8dwks2cc4md46pwq1lg3vq")))

(define-public crate-sv-parser-macros-0.10.2 (c (n "sv-parser-macros") (v "0.10.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "160ab5l6h392px2v240zz608ip0lfqyp7p10a6z1cir09j1vkvfi")))

(define-public crate-sv-parser-macros-0.10.3 (c (n "sv-parser-macros") (v "0.10.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0gp0v4pff7fvkabdw08x5pwamhp72ncfprdk8xgjqv4y728kp1br")))

(define-public crate-sv-parser-macros-0.10.4 (c (n "sv-parser-macros") (v "0.10.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "07jdp54pmshx3rsaw6nzfzwf6j5bcmczyq0wz0qf4apcsdf3y3gr")))

(define-public crate-sv-parser-macros-0.10.5 (c (n "sv-parser-macros") (v "0.10.5") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "044sr64fk042b3jhmazr47y527349xjnr035pcx8sb2zn13h25ci")))

(define-public crate-sv-parser-macros-0.10.6 (c (n "sv-parser-macros") (v "0.10.6") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0ndb1334yjxl08d98nlc7a1q528jsb9c2mpnl8gwv6j537myv4bk")))

(define-public crate-sv-parser-macros-0.10.7 (c (n "sv-parser-macros") (v "0.10.7") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0k3gg71pncfjdkglgp96m9wkp04rc1bgdglmwvxjpljqxka1jj8z")))

(define-public crate-sv-parser-macros-0.10.8 (c (n "sv-parser-macros") (v "0.10.8") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0cvzp2s2jld5v9lkrvcnqkcp1rj15dvxs3qqjl4hqfcqqx89swd1")))

(define-public crate-sv-parser-macros-0.11.0 (c (n "sv-parser-macros") (v "0.11.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0la48422gfn3fvnr62wl9wik8z5j0695rsh48kj6q7rj2haf29ra")))

(define-public crate-sv-parser-macros-0.11.1 (c (n "sv-parser-macros") (v "0.11.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1j7sacnr10aaaxg7xwvb3mbhfxq05c6gb8ldp2fndbg0fdqm9wk0")))

(define-public crate-sv-parser-macros-0.11.2 (c (n "sv-parser-macros") (v "0.11.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "09z1612anmlcrv96d10083nzq25dpynd8d1a5axjkk4d40icj7wf")))

(define-public crate-sv-parser-macros-0.11.3 (c (n "sv-parser-macros") (v "0.11.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1k0qyhzg5n0xxn1h930g1hx6pa7ya3y5ws6x0y7y66qndxymgqgh")))

(define-public crate-sv-parser-macros-0.12.0 (c (n "sv-parser-macros") (v "0.12.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0fg5a4vaww5450f2fd5s3qlvdv4z662fgkgp5j26qyy1dw9v1z7m")))

(define-public crate-sv-parser-macros-0.12.1 (c (n "sv-parser-macros") (v "0.12.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0rahh7ga9aa7jz5c5cl6qm788hd9x783lqkzmbsa5790ybdv3mgy")))

(define-public crate-sv-parser-macros-0.12.2 (c (n "sv-parser-macros") (v "0.12.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0sms91qrk13k6hix3q5608jch6lcfnpckxcp5silfikav1jr4nb0")))

(define-public crate-sv-parser-macros-0.12.3 (c (n "sv-parser-macros") (v "0.12.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "10wrnfl0sca8bl16pa8smjga96yimcq88pi6hgxi8jx3vycj2f47") (y #t)))

(define-public crate-sv-parser-macros-0.13.0 (c (n "sv-parser-macros") (v "0.13.0") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0rm4aql80qb8dza90zhd48bq1zivsc5mzwjl4856mmr87pglssaf")))

(define-public crate-sv-parser-macros-0.13.1 (c (n "sv-parser-macros") (v "0.13.1") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.6") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0jvxq597zg3r9v8h0i272gx6hlqj2pma7gzrc573kssz07pif13v")))

(define-public crate-sv-parser-macros-0.13.2 (c (n "sv-parser-macros") (v "0.13.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.6") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0iqrmyz0crifsfmymy1agd44dqc4lkzw94avdnfffycc163fgbn0")))

(define-public crate-sv-parser-macros-0.13.3 (c (n "sv-parser-macros") (v "0.13.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.6") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "004v9rmnd0p1993hj8d3280172di3ixg19knxrnj49lbq62vlbka")))

