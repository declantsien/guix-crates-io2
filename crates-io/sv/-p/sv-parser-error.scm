(define-module (crates-io sv -p sv-parser-error) #:use-module (crates-io))

(define-public crate-sv-parser-error-0.1.0 (c (n "sv-parser-error") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "166ii0yk4yq966lvs4s7qw2snvadaqw5kxnkl1rpidrqwbjhb0hr")))

(define-public crate-sv-parser-error-0.1.1 (c (n "sv-parser-error") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0yjkbmn4alf5qa4x6p4makvmbckpd5zqp03xbj25lnn8xlqng0nh")))

(define-public crate-sv-parser-error-0.1.2 (c (n "sv-parser-error") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1yaa3i90lz0wk5bxcfcj8kz7463a1hbyh3snrgdsrdxkn616px0x")))

(define-public crate-sv-parser-error-0.1.4 (c (n "sv-parser-error") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0lm33dsa6yb4jw987f07wlgqjz0xlnccn875b1l3r95m39f395s9")))

(define-public crate-sv-parser-error-0.1.5 (c (n "sv-parser-error") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "15c89wwklhy6wlliw61b2im08q0bjz49vlsfpf8kfjgr9836xqsh")))

(define-public crate-sv-parser-error-0.1.6 (c (n "sv-parser-error") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "16nwj98v90mshqm8rfyn2m2vv414aqpdh8acipfmkk6bw489541c")))

(define-public crate-sv-parser-error-0.1.7 (c (n "sv-parser-error") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "116df7wm5ikaydf5vv02raikhi1d3sn1q8j48pp275x6l77a2ry1")))

(define-public crate-sv-parser-error-0.2.0 (c (n "sv-parser-error") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "136lmrql681cia8rqvaw2wh2jqqmwfklaz9ndfff7p3sgi59jbnh")))

(define-public crate-sv-parser-error-0.2.1 (c (n "sv-parser-error") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "12ghr3i1sjr4h6byx4is2jjj2nw8jm57s6ffybqkn41ykc3rd815")))

(define-public crate-sv-parser-error-0.3.0 (c (n "sv-parser-error") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "04913533zgfigvzcik35hy7k5nq5x7ybph41fhqajbir3las0928")))

(define-public crate-sv-parser-error-0.3.1 (c (n "sv-parser-error") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0046hsg3h1illpv5qqbj1fm5rqqp9xxhnxmj8mpx8d5kjmqfjjky")))

(define-public crate-sv-parser-error-0.3.2 (c (n "sv-parser-error") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1a0ac89p122bdj6d1z48jway7b56qj89gl2qbx8idvnyk6np9l3j")))

(define-public crate-sv-parser-error-0.3.3 (c (n "sv-parser-error") (v "0.3.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "07j52imln04w3lvjw0ggav37fl1fb3nwckrsv5wyd11spibm1jps")))

(define-public crate-sv-parser-error-0.3.4 (c (n "sv-parser-error") (v "0.3.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1ynrw7m1np59w07k3c3pic5i29fqnp5dpmdhz89bhd490pvy6d9b")))

(define-public crate-sv-parser-error-0.3.5 (c (n "sv-parser-error") (v "0.3.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1v9aapkma9krmf0a7d4b9hzlxjmbq5awxs5fx832lqihdvs7gqbi")))

(define-public crate-sv-parser-error-0.3.6 (c (n "sv-parser-error") (v "0.3.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1gyi4qnm46wvibdm1463bk90kvy14f4z7bbicyh3wz83x4ssm60x")))

(define-public crate-sv-parser-error-0.3.7 (c (n "sv-parser-error") (v "0.3.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "03mzcgkcjzba6hgnbx1r37hqywrd263lm0601g9ycf20bacmgr8v")))

(define-public crate-sv-parser-error-0.4.0 (c (n "sv-parser-error") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0dk0jsgyk3r2vfpa9lhxpisxf7n41wgwnammzlda08zra60js88s")))

(define-public crate-sv-parser-error-0.4.1 (c (n "sv-parser-error") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1a0qgc8d23smpjz3016cailnkj77b6v7q3dfhllrdjx85blcjkm6")))

(define-public crate-sv-parser-error-0.4.2 (c (n "sv-parser-error") (v "0.4.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0n3xknibj6s7xdbmj12cv0zdb4yp432ssf37daynwpzpbj9nabz1")))

(define-public crate-sv-parser-error-0.4.3 (c (n "sv-parser-error") (v "0.4.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "14w77qmnpra9x0z72cndda63g6gs0kvvky6yz0ijf3krpncsjw07")))

(define-public crate-sv-parser-error-0.4.4 (c (n "sv-parser-error") (v "0.4.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0im68xc12rxq9rl4wq49a8flpsfwm6xvz575r63niplakygnhiws")))

(define-public crate-sv-parser-error-0.4.5 (c (n "sv-parser-error") (v "0.4.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0rdy036rh99ivy3ibd62h6amvf4hdz7amvm9pd3mgb54q19x86yh")))

(define-public crate-sv-parser-error-0.4.6 (c (n "sv-parser-error") (v "0.4.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1klcvxzj6bdnbi51bxzhdw9haj69xiv346p9l3lvm3kjkczd6vip")))

(define-public crate-sv-parser-error-0.4.7 (c (n "sv-parser-error") (v "0.4.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1rm5ps4nkx59ccmky61mjmhw6g2i35fbg3ql05asdnfmmd0f67h9")))

(define-public crate-sv-parser-error-0.4.8 (c (n "sv-parser-error") (v "0.4.8") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "141rblca9sy5qr139is0rpsf60jyrjgvr1danyb3hdrrlsds6y1p")))

(define-public crate-sv-parser-error-0.4.9 (c (n "sv-parser-error") (v "0.4.9") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "11jqhb9ysb5y4x0zw7zk0hl8hdhg54vdgvdy15l872lfxl4yklam")))

(define-public crate-sv-parser-error-0.4.10 (c (n "sv-parser-error") (v "0.4.10") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "15w5463f4s4hymf8i95z4b7dpyyf8rnr7a6m52x5fh6ph4gxmlkx")))

(define-public crate-sv-parser-error-0.4.11 (c (n "sv-parser-error") (v "0.4.11") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "064535xb8ggwn3884f06xnri8xw5xj1bbiqqsfcvls61svzyrxgf")))

(define-public crate-sv-parser-error-0.4.12 (c (n "sv-parser-error") (v "0.4.12") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0as5ska5a77dxyvs5qs5lrak5lf28wpzdw8d256f3mk9f77fs52w")))

(define-public crate-sv-parser-error-0.4.13 (c (n "sv-parser-error") (v "0.4.13") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "01jwbdsra7yp0108ywzgls3n419k20qnj8g1djd8vabzkdbhyvc1")))

(define-public crate-sv-parser-error-0.4.14 (c (n "sv-parser-error") (v "0.4.14") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0yxvbh38xhq2c8h2yrz1pjg9s6f78cc0xq8c6fn1n8r214zlmw7i")))

(define-public crate-sv-parser-error-0.4.15 (c (n "sv-parser-error") (v "0.4.15") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1fhjl40qrdka4gyw7ki6lrwdgw4as4m49h38pfpafrhvhcg1xkfw")))

(define-public crate-sv-parser-error-0.4.16 (c (n "sv-parser-error") (v "0.4.16") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1pd6skrd2d1kczx9axdi081nhdn4j4jf06r962gynmzwsfr1ccpb")))

(define-public crate-sv-parser-error-0.4.17 (c (n "sv-parser-error") (v "0.4.17") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1vnik2lh2csxbbps9pj7y6mb9zrbj50rryz59azy3pl295sj2pbd")))

(define-public crate-sv-parser-error-0.4.18 (c (n "sv-parser-error") (v "0.4.18") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1h0y3i0dcfk2k6bmhvi0c98yhxv6n8nn38vx18grk24d4wy2qp98")))

(define-public crate-sv-parser-error-0.4.19 (c (n "sv-parser-error") (v "0.4.19") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1130wwpqdz10zzw199nanzg4v26dwiwgmladxy3xil5wkfzzp9aj")))

(define-public crate-sv-parser-error-0.4.20 (c (n "sv-parser-error") (v "0.4.20") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1mr5f0jvnpndvzkwg18svnf1r3z4vs9jayi2w8wlf6437vslb0p8")))

(define-public crate-sv-parser-error-0.5.0 (c (n "sv-parser-error") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "165mqvhr1rdfckdrm4fh0y5d0qs6l8fd2zy4r6p6iia0h8skwkza")))

(define-public crate-sv-parser-error-0.6.0 (c (n "sv-parser-error") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pbxbipsayxwil24gvlkkxaml5dc45g12k7ncbczaqlr2c1rc4pk")))

(define-public crate-sv-parser-error-0.6.1 (c (n "sv-parser-error") (v "0.6.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14gnx280dn98i7c2ahyajc088d5db3awvwxa2jxzv8g1wxsdclqw")))

(define-public crate-sv-parser-error-0.6.2 (c (n "sv-parser-error") (v "0.6.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gpaav1gbhbh3ba1yrvrkbylzb20977pqmgfmdhm3v0dk7j30b6r")))

(define-public crate-sv-parser-error-0.6.3 (c (n "sv-parser-error") (v "0.6.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1029kf1havhpnc02f3fy56gci9xdfj7i3q3y3qc86hygzzmvwg0c")))

(define-public crate-sv-parser-error-0.6.4 (c (n "sv-parser-error") (v "0.6.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b0say5nnmv5bn38lrsc50bz8a0nmfbyjjj1djl3qngsz5a820dj")))

(define-public crate-sv-parser-error-0.6.5 (c (n "sv-parser-error") (v "0.6.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kp9szjyzg6gm6qkwkkhvj1simngghr0xm560xfbn5n1s5k9kl2b")))

(define-public crate-sv-parser-error-0.7.0 (c (n "sv-parser-error") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "038ifkq3pqccgmf6ljq028xjrxnsn2q5m87ch1z3abpbcarrb7h0")))

(define-public crate-sv-parser-error-0.8.0 (c (n "sv-parser-error") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0spca1hjabwwwgm47j8w61g930m0rr8s2sv5zcpn6sb3fk7x3672")))

(define-public crate-sv-parser-error-0.8.1 (c (n "sv-parser-error") (v "0.8.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1civ57pc9pkbqaqn7qy26ikkf33nlr4zj06af508arxs11lsbxvx")))

(define-public crate-sv-parser-error-0.8.2 (c (n "sv-parser-error") (v "0.8.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jjrgw0bmvgs8aqkmw0xzg29vfdvh9fs8a4n97qg0sm1zs87s04f")))

(define-public crate-sv-parser-error-0.8.3 (c (n "sv-parser-error") (v "0.8.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c6gv68nyl6dz3ghjn4w0s36lds3qywxj0rh7wmgp07cavrq6iwd")))

(define-public crate-sv-parser-error-0.9.0 (c (n "sv-parser-error") (v "0.9.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15pn14xlggq59lprw45rfb54dfz66bmw5fvfmg98g39nvnsxzfym")))

(define-public crate-sv-parser-error-0.10.0 (c (n "sv-parser-error") (v "0.10.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y5izvj0n8664rql5ajhp4vdsjs1nfvlr14b5aa99h97x13c6w4w")))

(define-public crate-sv-parser-error-0.10.1 (c (n "sv-parser-error") (v "0.10.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pkv59pw34qdfm8knj89bq1z37h9812bq8srqsg9550fy772g3pg")))

(define-public crate-sv-parser-error-0.10.2 (c (n "sv-parser-error") (v "0.10.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ff1gn9h1rf1njx383hy6v2h96sgsa5vkx5j2xdf85in6d6zlixn")))

(define-public crate-sv-parser-error-0.10.3 (c (n "sv-parser-error") (v "0.10.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0325fjwjz8wzy1v0176r6zg8bl6ivi9xld5agfqhqz2j41rdcskw")))

(define-public crate-sv-parser-error-0.10.4 (c (n "sv-parser-error") (v "0.10.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iyf6wf759n77xbqvbk6d9v7knpg45w25936jlyhmnqgwc0zzf9i")))

(define-public crate-sv-parser-error-0.10.5 (c (n "sv-parser-error") (v "0.10.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07hfalqjx538g1pi0q0ncvm5dzhcw413j3cf0lgsn5sqrg2akfcn")))

(define-public crate-sv-parser-error-0.10.6 (c (n "sv-parser-error") (v "0.10.6") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pa6saih1hlfghcyiz88fjjdbqx4y7p4cjr3pdahgmhpy32mjjn8")))

(define-public crate-sv-parser-error-0.10.7 (c (n "sv-parser-error") (v "0.10.7") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1845p6sw62l1z09jw4qqm6zm50xbxcxvimp385p30fy7xxxywz6g")))

(define-public crate-sv-parser-error-0.10.8 (c (n "sv-parser-error") (v "0.10.8") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h44j64w9gzrxs5lg59gcc8jyqdmmaa76qy1l86aj9ziw8nzwfbr")))

(define-public crate-sv-parser-error-0.11.0 (c (n "sv-parser-error") (v "0.11.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0adbczrmfzw79dy0jsdn2cvglcp12cvapwfb9gq1j00iilqdsnmq")))

(define-public crate-sv-parser-error-0.11.1 (c (n "sv-parser-error") (v "0.11.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06w940d7nqdj8jw3sb5idncnrfdk3hi4yyk3izxq6wwba3cgfyqc")))

(define-public crate-sv-parser-error-0.11.2 (c (n "sv-parser-error") (v "0.11.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aw9c8d8nh65xvc6bqd8n6w39c5p2hwz7fpv9g1x0bq7jvglzqy1")))

(define-public crate-sv-parser-error-0.11.3 (c (n "sv-parser-error") (v "0.11.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a374g3n83scp25rncb9xcxwsj42gjbnkw7xr55cdiys87rxmdw6")))

(define-public crate-sv-parser-error-0.12.0 (c (n "sv-parser-error") (v "0.12.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12i9bpd9yhmna3w1xb20ys1172aaicr62s7pkb59d2yhnanx4scr")))

(define-public crate-sv-parser-error-0.12.1 (c (n "sv-parser-error") (v "0.12.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17qvqab2phwjxfa6cwhv737hp7icxxknqgvn890b3shpayn41n9i")))

(define-public crate-sv-parser-error-0.12.2 (c (n "sv-parser-error") (v "0.12.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hxi4zh6v6yra2jqnh3xwl7ls1dspp2hkivyl930nixgyr09iayq")))

(define-public crate-sv-parser-error-0.12.3 (c (n "sv-parser-error") (v "0.12.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14s06dk4dj6v9wqclg3xk82i259y22npbxqbm9k9r6jc9317dynr") (y #t)))

(define-public crate-sv-parser-error-0.13.0 (c (n "sv-parser-error") (v "0.13.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xl48bra8xgndvqh6zqpsgiywk367dwi4fcyhdngm3m7slrsahj7")))

(define-public crate-sv-parser-error-0.13.1 (c (n "sv-parser-error") (v "0.13.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wcv4hx590afpphvhqg3h0yk23lxxhcqzcpmrpi0jj0vyzs02jkn")))

(define-public crate-sv-parser-error-0.13.2 (c (n "sv-parser-error") (v "0.13.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "192xb2clws9hxvkxx8bp8as7zw1y0y5pba9z473m6rh4jh9yhinx")))

(define-public crate-sv-parser-error-0.13.3 (c (n "sv-parser-error") (v "0.13.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11hsxwlpjqb10ig2pyyyz7pj8af77xa9mhsppnf3rnqiaqhyy0np")))

