(define-module (crates-io sv al sval_test) #:use-module (crates-io))

(define-public crate-sval_test-2.0.1 (c (n "sval_test") (v "2.0.1") (d (list (d (n "sval") (r "^2.0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.0.1") (d #t) (k 2)))) (h "12b97vb4gd0g1zxdbj9k39kc4b2d31s1n0zjgmk8zd17i7b8mraz")))

(define-public crate-sval_test-2.0.2 (c (n "sval_test") (v "2.0.2") (d (list (d (n "sval") (r "^2.0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.0.2") (d #t) (k 2)))) (h "0cv7j9am1857kp9mv09639fjcsr4a128kalp9srzsj9g3dii3rf4")))

(define-public crate-sval_test-2.0.3 (c (n "sval_test") (v "2.0.3") (d (list (d (n "sval") (r "^2.0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.0.3") (d #t) (k 2)))) (h "1n9jbcwf6qcjmz9086p3andak0ggwrdc6wm92kynzq6cyl2h1w8f")))

(define-public crate-sval_test-2.1.0 (c (n "sval_test") (v "2.1.0") (d (list (d (n "sval") (r "^2.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.1.0") (d #t) (k 2)))) (h "0sg06zz8qwfd6a2jg9yn97ibgs9zc8kskpzkx4yhyjkkyp89skl6")))

(define-public crate-sval_test-2.1.1 (c (n "sval_test") (v "2.1.1") (d (list (d (n "sval") (r "^2.1.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.1.1") (d #t) (k 2)))) (h "01lar1q991v5489rfj2k8wq014qgwdlmcbkqqckqbpyx1ippgdp0")))

(define-public crate-sval_test-2.2.0 (c (n "sval_test") (v "2.2.0") (d (list (d (n "sval") (r "^2.2.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.2.0") (d #t) (k 2)))) (h "06izza5cbd1dn9lfrl2ydvmlws5d9j71wq4mzx3nkg2ardh1imwx")))

(define-public crate-sval_test-2.2.1 (c (n "sval_test") (v "2.2.1") (d (list (d (n "sval") (r "^2.2.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.2.1") (d #t) (k 2)))) (h "1bz3ggqkjyvspbgq45swbh9nm4r662msqddcrrn7lb8lkdvqddbl")))

(define-public crate-sval_test-2.3.0 (c (n "sval_test") (v "2.3.0") (d (list (d (n "sval") (r "^2.3.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.3.0") (d #t) (k 2)))) (h "1i0s3f6r1drfkxbx7drg8fx4rr9kvl84r8b2xxmlpk572dxn7w6w")))

(define-public crate-sval_test-2.3.1 (c (n "sval_test") (v "2.3.1") (d (list (d (n "sval") (r "^2.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.3.1") (d #t) (k 2)))) (h "1zp3mzjw3caacizgqq2ifjfkypqxrkv6cfv5bw1w5b58b57rmc0x")))

(define-public crate-sval_test-2.4.0 (c (n "sval_test") (v "2.4.0") (d (list (d (n "sval") (r "^2.4.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.4.0") (d #t) (k 2)))) (h "1jnnlj9dmmpfiisgg0kpx1m9j8580vjz28b3rj6rbyjlxfg2s4hb")))

(define-public crate-sval_test-2.5.0 (c (n "sval_test") (v "2.5.0") (d (list (d (n "sval") (r "^2.5.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.5.0") (d #t) (k 2)))) (h "0arn9f7w9f5rka9mkh42nis29z7qrca2gxfw2hhk86x2jnjk0sdv")))

(define-public crate-sval_test-2.6.0 (c (n "sval_test") (v "2.6.0") (d (list (d (n "sval") (r "^2.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.6.0") (d #t) (k 2)))) (h "0fgaigg3f36s3zm8gcsfgv5gigpd2zs6dafslg4xgwz4zrwczpap")))

(define-public crate-sval_test-2.6.1 (c (n "sval_test") (v "2.6.1") (d (list (d (n "sval") (r "^2.6.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.6.1") (d #t) (k 2)))) (h "10kmxysanpa0kvll716s7a4bqsfd8c0w0b4g1a1g3mv2mm8cbip2")))

(define-public crate-sval_test-2.7.0 (c (n "sval_test") (v "2.7.0") (d (list (d (n "sval") (r "^2.7.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.7.0") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.7.0") (f (quote ("std"))) (d #t) (k 0)))) (h "1jwnyj8x01373snafxs3hv1v60an9xyaswg0j7dmjkjaynpynbmy")))

(define-public crate-sval_test-2.7.1 (c (n "sval_test") (v "2.7.1") (d (list (d (n "sval") (r "^2.7.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.7.1") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.7.1") (f (quote ("std"))) (d #t) (k 0)))) (h "15psfnnp92ds6gjhk2xdl08lx8845rk50r8y619v74fhi88i95xf")))

(define-public crate-sval_test-2.8.0 (c (n "sval_test") (v "2.8.0") (d (list (d (n "sval") (r "^2.8.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.8.0") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.8.0") (f (quote ("std"))) (d #t) (k 0)))) (h "0ncxlxp64rvj0mw97jj70xss24y6c5jibfi0wvpkmbswg7mg5yw6")))

(define-public crate-sval_test-2.9.0 (c (n "sval_test") (v "2.9.0") (d (list (d (n "sval") (r "^2.9.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.9.0") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.9.0") (f (quote ("std"))) (d #t) (k 0)))) (h "1p9ikj2d0zszk3x48c77rk6ggkgdl1mj4rwz4hhi1c8q34b1gxir")))

(define-public crate-sval_test-2.9.1 (c (n "sval_test") (v "2.9.1") (d (list (d (n "sval") (r "^2.9.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.9.1") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.9.1") (f (quote ("std"))) (d #t) (k 0)))) (h "1y0bxck7xy59jxccgz58c63gvyx1zppkxax91y04fpwz7890ckl0")))

(define-public crate-sval_test-2.9.2 (c (n "sval_test") (v "2.9.2") (d (list (d (n "sval") (r "^2.9.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.9.2") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.9.2") (f (quote ("std"))) (d #t) (k 0)))) (h "0x3596nw7xhpds32q3lzr5xgj7nz8syq3v38f9xxzs3gqzg7cp3n")))

(define-public crate-sval_test-2.10.0 (c (n "sval_test") (v "2.10.0") (d (list (d (n "sval") (r "^2.10.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.10.0") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.10.0") (f (quote ("std"))) (d #t) (k 0)))) (h "0r6sj3qlcrgn3ngd4mj8fn0xkgxn5ikr995m738rvaf1kqy9mh27")))

(define-public crate-sval_test-2.10.1 (c (n "sval_test") (v "2.10.1") (d (list (d (n "sval") (r "^2.10.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.10.1") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.10.1") (f (quote ("std"))) (d #t) (k 0)))) (h "0adnn14k80n3y2iazkwnb1rdz808n3gk44sxqa1fhgmiwkw2vhbg")))

(define-public crate-sval_test-2.10.2 (c (n "sval_test") (v "2.10.2") (d (list (d (n "sval") (r "^2.10.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.10.2") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.10.2") (f (quote ("std"))) (d #t) (k 0)))) (h "0bgbcdl7vniil0xiyvxscmzcwymhz2w2iywqgxjmc5c0krzg0hd4")))

(define-public crate-sval_test-2.11.0 (c (n "sval_test") (v "2.11.0") (d (list (d (n "sval") (r "^2.11.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.11.0") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.11.0") (f (quote ("std"))) (d #t) (k 0)))) (h "1wpdpzbplvby9gpy3xslajnr1akk90ajiyyj7jhzfjzbr6gfahpk")))

(define-public crate-sval_test-2.11.1 (c (n "sval_test") (v "2.11.1") (d (list (d (n "sval") (r "^2.11.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.11.1") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.11.1") (f (quote ("std"))) (d #t) (k 0)))) (h "15yappkdiisknwpgqn2gq9jkdzrfdlyqhxvra1r3rik93765g05s")))

(define-public crate-sval_test-2.12.0 (c (n "sval_test") (v "2.12.0") (d (list (d (n "sval") (r "^2.12.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.12.0") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.12.0") (f (quote ("std"))) (d #t) (k 0)))) (h "17i533nfb82idlv5jz7vlxxhq1wxabh2nsvgp7a9vxnqp6hr8659")))

(define-public crate-sval_test-2.13.0 (c (n "sval_test") (v "2.13.0") (d (list (d (n "sval") (r "^2.13.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "sval_dynamic") (r "^2.13.0") (d #t) (k 2)) (d (n "sval_fmt") (r "^2.13.0") (f (quote ("std"))) (d #t) (k 0)))) (h "11lx3na7j0wkyxpw2wvz6jnhx32wc3iykd7cb76ibfywknnim8x5")))

