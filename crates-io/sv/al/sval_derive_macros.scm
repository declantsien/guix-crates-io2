(define-module (crates-io sv al sval_derive_macros) #:use-module (crates-io))

(define-public crate-sval_derive_macros-2.7.0 (c (n "sval_derive_macros") (v "2.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1vclbf35vil9zll3mmj77p5n943h9xzkq4p1bn6x14splvjy7lcj") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.7.1 (c (n "sval_derive_macros") (v "2.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "18gv72hsj8fc0r0qik54qdpkn4w5mfghvcdqxf1wl6rfjv4jyil4") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.8.0 (c (n "sval_derive_macros") (v "2.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "11dr20kgl56rm31sg17lhw5j6sbv1i9whwmyk0ysg21hpqv1mvxd") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.9.0 (c (n "sval_derive_macros") (v "2.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0c8q89h6kxzxzp8cqfydcnqlcakybpxhjkylx6icmhac3wqr7xwz") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.9.1 (c (n "sval_derive_macros") (v "2.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0jrgv47xs4wqv5lidic9rwpp7a9imai0r6hdm7lfp96nacv85vb5") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.9.2 (c (n "sval_derive_macros") (v "2.9.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0sqp05r2yd8asp0ww3jzibxj8qi18ya5g3mx2rqb88k914majmia") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.10.0 (c (n "sval_derive_macros") (v "2.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "060arl9y2bhpqwllzd9yjzpxrpgiabq1dr52av45njann8qijalx") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.10.1 (c (n "sval_derive_macros") (v "2.10.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0vrna3xzy8hq1zj1dnkr5ggqqxbhrspnz5jfz0cshv67v2scjx41") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.10.2 (c (n "sval_derive_macros") (v "2.10.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "11vmfpr028n4z9x0nlaqa1v7p4fij6fz7wxbr3mmizp9v6267nry") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.11.0 (c (n "sval_derive_macros") (v "2.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "02jd9xp4z108n7dvl4h0bha0i8pasx207wm94yhbyph5bc6izd9a") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.11.1 (c (n "sval_derive_macros") (v "2.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "14glg6z00wdjjn9b93kbd6kzjqlfxr73am1mamqd0jpkap0zlwkv") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.12.0 (c (n "sval_derive_macros") (v "2.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1s130gn3a9c2fnnb7yf42n7151y7gk0h0cgk1v74xj2pgfrvyyig") (f (quote (("flatten"))))))

(define-public crate-sval_derive_macros-2.13.0 (c (n "sval_derive_macros") (v "2.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0668clp7whxqm3rp5b5f0w61i3fad8wcq8lv9rvcg2cars2j7d1q") (f (quote (("flatten"))))))

