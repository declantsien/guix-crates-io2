(define-module (crates-io sv al sval) #:use-module (crates-io))

(define-public crate-sval-0.0.0 (c (n "sval") (v "0.0.0") (d (list (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "sval_derive") (r "^0.0.0") (o #t) (d #t) (k 0)))) (h "093s162yk8ywbd4qc6kk7swbjhglz4vmv63s523w20b4p95rv19j") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive"))))))

(define-public crate-sval-0.0.1 (c (n "sval") (v "0.0.1") (d (list (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "sval_derive") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0ijwaz1zfvjrl32jl13s7yp7k1ak596nb9cyiqmjrwld85sr543k") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive"))))))

(define-public crate-sval-0.0.2 (c (n "sval") (v "0.0.2") (d (list (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "sval_derive") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1svmzbbhf9dz342bz0409fs0g5mlcn4rs66nsz2yqyac2sy0a4y0") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive"))))))

(define-public crate-sval-0.0.3 (c (n "sval") (v "0.0.3") (d (list (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "sval_derive") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "0s20hpn17h78zrhvp1kw9iznlss52l5jbv7imb1bp5vgjm1glmhw") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive"))))))

(define-public crate-sval-0.0.4 (c (n "sval") (v "0.0.4") (d (list (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "sval_derive") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "0lrfdllifzivl7s85wcxpjg7cb8s7r32cpgij0jcpawdkzym305z") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive"))))))

(define-public crate-sval-0.0.5 (c (n "sval") (v "0.0.5") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.0.5") (o #t) (d #t) (k 0)))) (h "0120wi68hxf788z1s6w065i0i61spk2blxc68jy04fhza93yzzbf") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.1.0 (c (n "sval") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1pccqkc61sxcjgqkwfb3qji8i5b6yrq788ib26vicx2qp8cjh3wb") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.1.1 (c (n "sval") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "09pv4yzp30dxry6cb3gzj01hqpgih0nhkkpj8542qig2cxlw4cxz") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.1.2 (c (n "sval") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0mmvxs6kgwigp4nyddksl9wdkp6fljmgkvlndjffr2pmymvyarkj") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.1.3 (c (n "sval") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "18j8i1vr0w1cqhl7vag2r0bqvkssf6l62aycfkqcbf4cxwryf360") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.1.4 (c (n "sval") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "102a3xm6arzzmz7sfnw7r9rdlmzl8prxlxqcni9c2vjhjnv4mr7r") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.1.5 (c (n "sval") (v "0.1.5") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0fi7i5cy7zyap30vbrsh67q53liciiai4chfw6pm8z382pbp780p") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.2.0 (c (n "sval") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "087nrik31csm3dkmbknggvwilmmbz5b4237ld0s25nq7f2hbvq8d") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.3.0 (c (n "sval") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1qmvf525f2c3pn39s5jyrkjdb096m4xrqrf2395yi77wh6c98xlq") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.3.1 (c (n "sval") (v "0.3.1") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0b1xxkdq8fsb5w09sbmqbnybfjlpapcaj13qisnfgsbdiqp27jjv") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.0 (c (n "sval") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0hsfcgwn0hbh2djwn8qd3rsaffdmz2al8ryi04jilgcwjlg4wlzx") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.1 (c (n "sval") (v "0.4.1") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0wnsl300r6r8z02mm42pz7kqp96glfdm3lmifdqpgpbgi794wwb1") (f (quote (("test" "std") ("std") ("serde" "std" "serde_lib/std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.2 (c (n "sval") (v "0.4.2") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0685zk40wh7fq9m87aydf2dqx82zv28awxnkj9wqqgnbkga5vc53") (f (quote (("test" "std") ("std") ("serde_std" "std" "serde_lib/std") ("serde_no_std" "serde_lib") ("serde" "serde_std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.3 (c (n "sval") (v "0.4.3") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "1nkciqf0zsd6g453msblg1759z14g3yskmiqhsqsm936faq6q8c3") (f (quote (("test" "std") ("std") ("serde_std" "std" "serde_lib/std") ("serde_no_std" "serde_lib") ("serde" "serde_std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.4 (c (n "sval") (v "0.4.4") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "0xaj6zik6vljfb5s4z9sfcwavxqbw98943z66jcm1j3w9pj65gzw") (f (quote (("test" "std") ("std") ("serde_std" "std" "serde_lib/std") ("serde_no_std" "serde_lib") ("serde" "serde_std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.5 (c (n "sval") (v "0.4.5") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1pwlnlq7ay9ycmpcrqsq2rmqyfnpbhsmqsf3w5bn193jk8shr1rb") (f (quote (("test" "std") ("std") ("serde_std" "std" "serde_lib/std") ("serde_no_std" "serde_lib") ("serde" "serde_std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.6 (c (n "sval") (v "0.4.6") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "042xpm78yhq1qyh1b9h23q93kk0qiipk3k9dnavbghz8ggl83ki7") (f (quote (("test" "std") ("std") ("serde_std" "std" "serde_lib/std") ("serde_no_std" "serde_lib") ("serde" "serde_std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.4.7 (c (n "sval") (v "0.4.7") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde_lib") (r "^1") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sval_derive") (r "^0.4.7") (o #t) (d #t) (k 0)))) (h "1aljggx64481q4wp3wx9hxsfh2bs7d64nqsrwbb2zxcpmdnbn6yk") (f (quote (("test" "std") ("std") ("serde_std" "std" "serde_lib/std") ("serde_no_std" "serde_lib") ("serde" "serde_std") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "std" "smallvec"))))))

(define-public crate-sval-0.5.0 (c (n "sval") (v "0.5.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "09abjn621rh1ldkc60qzgmy56h82f5ny9q2sb4h429klqlrbkx0s") (f (quote (("test" "std") ("std" "alloc") ("serde" "serde_lib") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-0.5.1 (c (n "sval") (v "0.5.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "16qrhp9f9y7gy4phg5xrfqbjskmmclvvv3i18pwbmgwvpr3b5gyn") (f (quote (("test" "std") ("std" "alloc") ("serde" "serde_lib") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-0.5.2 (c (n "sval") (v "0.5.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "052j9ipwpb1zh02gw2ys8c4wpjqdf35991k0zkwljnalx37i79qj") (f (quote (("test" "std") ("std" "alloc") ("serde" "serde_lib") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-1.0.0-alpha.1 (c (n "sval") (v "1.0.0-alpha.1") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde1_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^1.0.0-alpha.1") (o #t) (d #t) (k 0)))) (h "0jn5bgw26h5sjfkjx9fjh4wv0srgylvysq4raacnhib0k0gbabfx") (f (quote (("test" "std") ("std" "alloc") ("serde1" "serde1_lib") ("serde" "serde1") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-1.0.0-alpha.2 (c (n "sval") (v "1.0.0-alpha.2") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde1_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^1.0.0-alpha.2") (o #t) (d #t) (k 0)))) (h "1fqq488jcydnqr1jyx667nlfcbb9fa4dyxn686qybh374rjjxb0a") (f (quote (("test" "std") ("std" "alloc") ("serde1" "serde1_lib") ("serde" "serde1") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-1.0.0-alpha.3 (c (n "sval") (v "1.0.0-alpha.3") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde1_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^1.0.0-alpha.3") (o #t) (d #t) (k 0)))) (h "0irrkx2yf8dkxcfkpy37cy6yvfbhnwvvj2csrd7sw12f7mvf683j") (f (quote (("test" "std") ("std" "alloc") ("serde1" "serde1_lib") ("serde" "serde1") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-1.0.0-alpha.4 (c (n "sval") (v "1.0.0-alpha.4") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde1_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^1.0.0-alpha.4") (o #t) (d #t) (k 0)))) (h "0z95qd4ijvr2gaw78gs7g920gblif9ylag0z8mw0qw2320kbm4d8") (f (quote (("test" "std") ("std" "alloc") ("serde1" "serde1_lib") ("serde" "serde1") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-1.0.0-alpha.5 (c (n "sval") (v "1.0.0-alpha.5") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde1_lib") (r "^1.0.104") (o #t) (k 0) (p "serde")) (d (n "smallvec") (r "^1") (o #t) (k 0)) (d (n "sval_derive") (r "^1.0.0-alpha.5") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "025sy290xnn56nl15qkrkq0whxcwlvb4bzp996azbjl7gdyfxxj5") (f (quote (("test" "std") ("std" "alloc") ("serde1" "serde1_lib") ("serde" "serde1") ("fmt") ("derive" "sval_derive") ("arbitrary-depth" "alloc" "smallvec") ("alloc"))))))

(define-public crate-sval-2.0.0 (c (n "sval") (v "2.0.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)))) (h "0gcjc81vga46dw17rn7jaavmjg1rlplj6gp432ic0sb1py7kiz2s") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-sval-2.0.1 (c (n "sval") (v "2.0.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)))) (h "1c04r4x1whv0g1kpx0lqqsh6qgwad9ni4lz5dabcflf5kp484gxh") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-sval-2.0.2 (c (n "sval") (v "2.0.2") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)))) (h "0bh9wxb92dysgg7s3kr0lfr2wns4fs915352a6jzbi054pccg17h") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-sval-2.0.3 (c (n "sval") (v "2.0.3") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)))) (h "15kq87h0lrlpyx29s8wccck5kp9r9jwlcca87kwikhgfczwzhzbp") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-sval-2.1.0 (c (n "sval") (v "2.1.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)))) (h "1hxh201a88816yx4ix05n459fylqsshpjc9qmq94zm8yrc9lyl24") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-sval-2.1.1 (c (n "sval") (v "2.1.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)))) (h "05dq580hpccikdm7s8vm8wxxc5mq7ys9vkrzz07qrivk6clgb14d") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-sval-2.2.0 (c (n "sval") (v "2.2.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.2.0") (o #t) (d #t) (k 0)))) (h "08ac10hyj3wnzcinb744kd8p7iyl2vd5qhqyhrksfkbifw3zlr3i") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.2.1 (c (n "sval") (v "2.2.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.2.1") (o #t) (d #t) (k 0)))) (h "0d6r00a87462n01hccv81wz217jamjp2qk4pr5wbawmniq7x60an") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.3.0 (c (n "sval") (v "2.3.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.3.0") (o #t) (d #t) (k 0)))) (h "014lfr4dn4lg7zf7j5zyj6h781whr4kv5mm03fvnpns9rwv4qndb") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.3.1 (c (n "sval") (v "2.3.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "0dh9mggj75klnkp7r0wvgdvnnhm4vm9iwx35vwfk0fvn6isbackq") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.4.0 (c (n "sval") (v "2.4.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "1xhaa2i20pb48jmbzmiadwksrvyar59lxf8wxlck2qfr88qbn58m") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.5.0 (c (n "sval") (v "2.5.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.5.0") (o #t) (d #t) (k 0)))) (h "045l5360qmcvgvng4s1r1wcljsm7fp4552jj3bi75r6rx1na2shf") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.6.0 (c (n "sval") (v "2.6.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.6.0") (o #t) (d #t) (k 0)))) (h "026gc3vl889x6i0886gpp6wiy6fr2mxxhq6igkn4w13nj9hvmyp2") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.6.1 (c (n "sval") (v "2.6.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive") (r "^2.6.1") (o #t) (d #t) (k 0)))) (h "1cbq3cx67dw0my4cn8ldgf9p4hkxsmb5g6yggi3yklrllhh160wb") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive"))))))

(define-public crate-sval-2.7.0 (c (n "sval") (v "2.7.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.7.0") (o #t) (d #t) (k 0)))) (h "0jlh1kvi2kn885hgam9cnij3rcq29i8whvia455qx4w1slkj110d") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.7.1 (c (n "sval") (v "2.7.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.7.1") (o #t) (d #t) (k 0)))) (h "176nz2klbriq28p53f48gfmnx5g14bikq0jlx1m80lyavdzxhf9m") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.8.0 (c (n "sval") (v "2.8.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.8.0") (o #t) (d #t) (k 0)))) (h "01mdh1w5n9vl4lq9zyv1v6b9mnrlfl79yd77f6ychaxykzn1xl85") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.9.0 (c (n "sval") (v "2.9.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.9.0") (o #t) (d #t) (k 0)))) (h "0m863hzgvixzk30cgpi7c97ba38ix0vhb2s6d6d886n89z8116g3") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.9.1 (c (n "sval") (v "2.9.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.9.1") (o #t) (d #t) (k 0)))) (h "00znq0n2i9mw7igr8xmq8kqsiya2haxdh3bd7vav3nvsh51rl8ak") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.9.2 (c (n "sval") (v "2.9.2") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.9.2") (o #t) (d #t) (k 0)))) (h "0va0xfii8jimgsj8v2bjsnlzp83vh598vya4kcgdl2nfmgnm14cz") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.10.0 (c (n "sval") (v "2.10.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.10.0") (o #t) (d #t) (k 0)))) (h "0pds6pq7jlyj5vyxcparhfkqqc0c2f4wasjdlkda694v4vgrh320") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.10.1 (c (n "sval") (v "2.10.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.10.1") (o #t) (d #t) (k 0)))) (h "1fw8phl65851wfp13bmzjk52ql8ma9wzhddnnqg2x0ps7nvqjl75") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.10.2 (c (n "sval") (v "2.10.2") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.10.2") (o #t) (d #t) (k 0)))) (h "0wq8dpcwkxf9i5ivaqgi736kalqdsn88yhsb9fh1dhmpilmg2pdi") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.11.0 (c (n "sval") (v "2.11.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.11.0") (o #t) (d #t) (k 0)))) (h "1l33bjx8j1fjl6aydky7sk7acgzj41nqr1njcay0aj3ga2myj10n") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.11.1 (c (n "sval") (v "2.11.1") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.11.1") (o #t) (d #t) (k 0)))) (h "0ncal904dy3vi69ipwcs76v7h46h3mmk0l14wzj23893x9mki8l2") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.12.0 (c (n "sval") (v "2.12.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.12.0") (o #t) (d #t) (k 0)))) (h "1s86ww2qi57qf8h6ydl1gfiz83ccg7zm1b9ifqg791jfvk1b78iq") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

(define-public crate-sval-2.13.0 (c (n "sval") (v "2.13.0") (d (list (d (n "humantime") (r "^2") (d #t) (k 2)) (d (n "sval_derive_macros") (r "^2.13.0") (o #t) (d #t) (k 0)))) (h "1zlbfing7vh90zd1qjc10s0c6yv2ygd8fpfjsl3579brpizrbssk") (f (quote (("std" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:sval_derive_macros"))))))

