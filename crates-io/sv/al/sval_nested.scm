(define-module (crates-io sv al sval_nested) #:use-module (crates-io))

(define-public crate-sval_nested-2.11.0 (c (n "sval_nested") (v "2.11.0") (d (list (d (n "sval") (r "^2.11.0") (d #t) (k 0)) (d (n "sval_buffer") (r "^2.11.0") (k 0)) (d (n "sval_derive_macros") (r "^2.11.0") (d #t) (k 2)) (d (n "sval_ref") (r "^2.11.0") (d #t) (k 0)))) (h "17cnwn6dg6vqvd3b0115rs50hfcaibs7g22igkf9jphypvvc6nb6") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("no_debug_assertions") ("default" "alloc") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_nested-2.11.1 (c (n "sval_nested") (v "2.11.1") (d (list (d (n "sval") (r "^2.11.1") (d #t) (k 0)) (d (n "sval_buffer") (r "^2.11.1") (k 0)) (d (n "sval_derive_macros") (r "^2.11.1") (d #t) (k 2)) (d (n "sval_ref") (r "^2.11.1") (d #t) (k 0)))) (h "1hw4y95lxyj2kkb05kxah2hm8k9z9yv8h1q9wcw1ylwwnysgzz33") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("no_debug_assertions") ("default" "alloc") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_nested-2.12.0 (c (n "sval_nested") (v "2.12.0") (d (list (d (n "sval") (r "^2.12.0") (d #t) (k 0)) (d (n "sval_buffer") (r "^2.12.0") (k 0)) (d (n "sval_derive_macros") (r "^2.12.0") (d #t) (k 2)) (d (n "sval_ref") (r "^2.12.0") (d #t) (k 0)))) (h "1y184i07d2vjb36y9lgis2zyx1vjn7i5img2gm0jf2fgrdgx7hbi") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("no_debug_assertions") ("default" "alloc") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_nested-2.13.0 (c (n "sval_nested") (v "2.13.0") (d (list (d (n "sval") (r "^2.13.0") (d #t) (k 0)) (d (n "sval_buffer") (r "^2.13.0") (k 0)) (d (n "sval_derive_macros") (r "^2.13.0") (d #t) (k 2)) (d (n "sval_ref") (r "^2.13.0") (d #t) (k 0)))) (h "1yx6s20nszzbgdx9shvym8y1cl0scw8arydvmsx7c14zf0jfnvw8") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("no_debug_assertions") ("default" "alloc") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

