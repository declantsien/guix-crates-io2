(define-module (crates-io sv al sval_flatten) #:use-module (crates-io))

(define-public crate-sval_flatten-2.7.1 (c (n "sval_flatten") (v "2.7.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.7.1") (k 0)) (d (n "sval_buffer") (r "^2.7.1") (k 0)))) (h "0d2y3csl6b94yqjsipsjr7xdw4n6v83h950sc50j1br09x636p51") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.8.0 (c (n "sval_flatten") (v "2.8.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.8.0") (k 0)) (d (n "sval_buffer") (r "^2.8.0") (k 0)))) (h "0x5w4labv996bbk2y62687jgqk101g8wz62hn76c9ybkkjs68lsx") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.9.0 (c (n "sval_flatten") (v "2.9.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.0") (k 0)) (d (n "sval_buffer") (r "^2.9.0") (k 0)))) (h "0ggbfazr3b186vc1b45qbrjy4q7yjc3dfda0q09wbmx188kx0lvy") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.9.1 (c (n "sval_flatten") (v "2.9.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.1") (k 0)) (d (n "sval_buffer") (r "^2.9.1") (k 0)))) (h "0yphbkx7fasgs62j5ka262l2nvf637zjbw6irm889z3b73w825dm") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.9.2 (c (n "sval_flatten") (v "2.9.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.2") (k 0)) (d (n "sval_buffer") (r "^2.9.2") (k 0)))) (h "16p1nn08dxbhmsjs95k2p839c5lvafy9c4vkpzn7zf6li3pbgqa2") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.10.0 (c (n "sval_flatten") (v "2.10.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.0") (k 0)) (d (n "sval_buffer") (r "^2.10.0") (k 0)))) (h "16zj3d5kylsl7cr2f4sj5j5pjaj2inkpaf5i75hdsh4cscz71v4a") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.10.1 (c (n "sval_flatten") (v "2.10.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.1") (k 0)) (d (n "sval_buffer") (r "^2.10.1") (k 0)))) (h "0ha4a375zk27cg109kr8a5xzzrqbhl1wb6wyfrd533f5qvyd7dh3") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.10.2 (c (n "sval_flatten") (v "2.10.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.2") (k 0)) (d (n "sval_buffer") (r "^2.10.2") (k 0)))) (h "121ac1pn2b113rgkf98n65kpwn2j80rikjzdwn5yaknxp9yqqcr3") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.11.0 (c (n "sval_flatten") (v "2.11.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.11.0") (k 0)) (d (n "sval_buffer") (r "^2.11.0") (k 0)))) (h "03bl5gzv5qbx6ci1j5wf299xsfhg1mwlh9mi858pkyfpahw1bywg") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.11.1 (c (n "sval_flatten") (v "2.11.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.11.1") (k 0)) (d (n "sval_buffer") (r "^2.11.1") (k 0)))) (h "1j2lzbn0l67fq5zyvv8rkbpwf5502b8hh87hwsvsycz9lvgq6pm1") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.12.0 (c (n "sval_flatten") (v "2.12.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.12.0") (k 0)) (d (n "sval_buffer") (r "^2.12.0") (k 0)))) (h "1l4gscmwyvpkld5plyrgiqvr2vjzdz2axvmq8jpk4ivx8j24nsz2") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

(define-public crate-sval_flatten-2.13.0 (c (n "sval_flatten") (v "2.13.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.13.0") (k 0)) (d (n "sval_buffer") (r "^2.13.0") (k 0)))) (h "1gdh7dfix44f8r8zwzsld7h9wcnqks6x7yl3y2f2r9g2mbfk9fz9") (f (quote (("std" "alloc" "sval/std" "sval_buffer/std") ("alloc" "sval/alloc" "sval_buffer/alloc"))))))

