(define-module (crates-io sv al sval_fmt) #:use-module (crates-io))

(define-public crate-sval_fmt-2.0.0 (c (n "sval_fmt") (v "2.0.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.0") (d #t) (k 0)))) (h "13vzbrsyricy23ga0ljhj97bnkqxlhznz69j9vi0a6vw3parvbiq") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.0.1 (c (n "sval_fmt") (v "2.0.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.1") (d #t) (k 0)))) (h "1rwapcp8f98gpzvh6agf3i70msspqbflmizqm3ibr0yv55h44yxg") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.0.2 (c (n "sval_fmt") (v "2.0.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.2") (d #t) (k 0)))) (h "1fzwarabrc8h6yd1y50gps8fvn49p3mjlxy6ysjcknpxr24dsh0n") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.0.3 (c (n "sval_fmt") (v "2.0.3") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.3") (d #t) (k 0)))) (h "0h7nyx5lzdgnp6vzqrscky9r33wflyxv6g9y4vglkkd7k83z9k6z") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.1.0 (c (n "sval_fmt") (v "2.1.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.1.0") (d #t) (k 0)))) (h "0knnhajj4jjvkl99g3dhalipcajjpnksh0hhh8jyhbv7qhfl7g6p") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.1.1 (c (n "sval_fmt") (v "2.1.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.1.1") (d #t) (k 0)))) (h "188pb62bpansrhi207whhn3x3y7igmljg9ylwj6q83lik16dvlfq") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.2.0 (c (n "sval_fmt") (v "2.2.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.2.0") (d #t) (k 0)))) (h "0bh2ib74qmgfn733kswi44g7kgy185bp8vdcandcwqb5v6c09axz") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.2.1 (c (n "sval_fmt") (v "2.2.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.2.1") (d #t) (k 0)))) (h "0wyyr99n6xim1vlrapmx7fisdvd1z9vl6zhj99rxzbs3ab68s93a") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.3.0 (c (n "sval_fmt") (v "2.3.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.3.0") (d #t) (k 0)))) (h "0n27mbyyixps7k974djwn427w6q9yhc0x2m5drw8fp19r79jr4yf") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.3.1 (c (n "sval_fmt") (v "2.3.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.3.1") (d #t) (k 0)))) (h "1wvg5axjn1glps0yx35qhpc6i04glfsyx8lyj3dxs5r26kyrgzgz") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.4.0 (c (n "sval_fmt") (v "2.4.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.4.0") (d #t) (k 0)))) (h "0wimyw7004fsh44nj8n7zrszrjwsh0jr80s94d917rrdardfj3g4") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.5.0 (c (n "sval_fmt") (v "2.5.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.5.0") (d #t) (k 0)))) (h "1f5bzg8al8yd672lr87v3d1k93mkff2mxdwpplpmr4kg6h8csg4x") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.6.0 (c (n "sval_fmt") (v "2.6.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.6.0") (d #t) (k 0)))) (h "1cgr81y1ihr42kjnq223z5mw9mskm6dbf8qa7wnbs1v0blfvb3q9") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.6.1 (c (n "sval_fmt") (v "2.6.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.6.1") (d #t) (k 0)))) (h "09j3ss5kayp6pb23wasc1k9mjcvv0x6v1xrqzasml162cj9rxh3x") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.7.0 (c (n "sval_fmt") (v "2.7.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.7.0") (d #t) (k 0)))) (h "0zmrhj5gm18jn8gbcg88n8537jr8s7z0sngymz5qrfygm024c3xm") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.7.1 (c (n "sval_fmt") (v "2.7.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.7.1") (d #t) (k 0)))) (h "0kyhclzl2m0ivfwx774x4alcw30smkzlzb359q8xjpp8wjkz2fa8") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.8.0 (c (n "sval_fmt") (v "2.8.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.8.0") (d #t) (k 0)))) (h "0y35i69br1mw23pglz4wmmacakicjpr5fndk01nj87il04iqnmrg") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.9.0 (c (n "sval_fmt") (v "2.9.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.0") (d #t) (k 0)))) (h "1mxlnm5lyirals4sbvbsqkr1s6x7s9s4s1imb8c66lbm772dfm4r") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.9.1 (c (n "sval_fmt") (v "2.9.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.1") (d #t) (k 0)))) (h "17fqlhacy3qvqkmjchdr4q00yd9g3lq16b539fzai2in5ar82vl5") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.9.2 (c (n "sval_fmt") (v "2.9.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.2") (d #t) (k 0)))) (h "0l0n77nz7psw5994dzpv5x15sm4gj04g9h0a9lsa1xxhb6n9ia4p") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.10.0 (c (n "sval_fmt") (v "2.10.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.0") (d #t) (k 0)))) (h "0iqxxnrk6xkilp849r5x5zdqmkfmgwz772nq36z3z5fn2wlp0df1") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.10.1 (c (n "sval_fmt") (v "2.10.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.1") (d #t) (k 0)))) (h "1hljndfm8i8vm1pf1gwfhsclc1136wpaw0d0zr8k02iba3mzhlxd") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.10.2 (c (n "sval_fmt") (v "2.10.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.2") (d #t) (k 0)))) (h "0hqkjb7blcdqjlawnffmw0bq5gxf98i52lbgcnjabxr64a47ybsk") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.11.0 (c (n "sval_fmt") (v "2.11.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.11.0") (d #t) (k 0)))) (h "0khcv9xpd1p82w7397287zrlv54fg6bjb8g8mv3mmb0v8di4hiy8") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.11.1 (c (n "sval_fmt") (v "2.11.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.11.1") (d #t) (k 0)))) (h "1f7ip3dzlyvpijgfqwijsav0nvcgw223gh5in9qz5kpc1x7pp0qh") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.12.0 (c (n "sval_fmt") (v "2.12.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.12.0") (d #t) (k 0)))) (h "1vxn5blc3z9k8fhwhn8ir8myb83ky2bycf8xhb451lmh61di5ayh") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_fmt-2.13.0 (c (n "sval_fmt") (v "2.13.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.13.0") (d #t) (k 0)))) (h "0kgnmg0hj2lgmjd7548d0zj6safjbfhvvi372d52gplpfikqiqf4") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

