(define-module (crates-io sv al sval_json) #:use-module (crates-io))

(define-public crate-sval_json-0.0.0 (c (n "sval_json") (v "0.0.0") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.0.0") (d #t) (k 0)))) (h "1idwygqcfpbs74sm5004m9b9bbzm0r3dy9fnsl313awzmg8h9krr") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.0.1 (c (n "sval_json") (v "0.0.1") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.0.1") (d #t) (k 0)))) (h "1bmn3mmn6sn8jk5s5l4y027a35qzch4g75fm8f6x1i1mcg5cnqp9") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.0.2 (c (n "sval_json") (v "0.0.2") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.0.2") (d #t) (k 0)))) (h "1ivnkiv4gklmx177g8bdkqsm409bsgvri229p4yihbsr0v7n4yvv") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.0.3 (c (n "sval_json") (v "0.0.3") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.0.3") (d #t) (k 0)))) (h "142sl1sqifms75rzqvmmqvjgmf6dslz8q2jpk8pp9adigq6np5m4") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.0.4 (c (n "sval_json") (v "0.0.4") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.0.4") (d #t) (k 0)))) (h "17k1zawwczi52w2rr7hckspd8zd9l6p35rws0lhd52vhcl5n6123") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.0.5 (c (n "sval_json") (v "0.0.5") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.0.5") (d #t) (k 0)))) (h "0zhm4970vzzajgjwi9ljasg9cm89ddj8yk4pq72zxlmpsa0jwrh3") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.1.0 (c (n "sval_json") (v "0.1.0") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.1.0") (d #t) (k 0)))) (h "1ds69c1vj8w4q1669zwgi4sklrlr9jfwr56yjhk2slfqgk5b4dng") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.1.1 (c (n "sval_json") (v "0.1.1") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.1.1") (d #t) (k 0)))) (h "1h18srmxf7qcgdpg797ygr25yymcbx87qxkdidd0xwky68ax1v80") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.1.2 (c (n "sval_json") (v "0.1.2") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.1.2") (d #t) (k 0)))) (h "1wdzgniqkmpcah030gpiy0pa45axy51b818d0mk7qa20cwqgwriq") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.1.3 (c (n "sval_json") (v "0.1.3") (d (list (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.1.3") (d #t) (k 0)))) (h "0qg20dlhdimdszcqb64jac2qxkfg1ina3wh7j8wis72lckv107sc") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.1.4 (c (n "sval_json") (v "0.1.4") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.1.4") (d #t) (k 0)))) (h "17p5hzhabq4ddlgxpj78syicrmanrwjkgbm4c9yg1gcykbran08l") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.1.5 (c (n "sval_json") (v "0.1.5") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.1.5") (d #t) (k 0)))) (h "0nl7g32mdx3rnd4b2m3js1kvrwfm2x4wd2rd9gslqg0i86l35sjb") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.2.0 (c (n "sval_json") (v "0.2.0") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.2.0") (d #t) (k 0)))) (h "1bgnkaj90bgqnvizcg7xxny6fgh8zziw6x2ysdvsa55pjx506hqh") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.3.0 (c (n "sval_json") (v "0.3.0") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.3.0") (d #t) (k 0)))) (h "0dxff3riw1k6g9i2dy7jgzcfl9d1nml1cdhqa4bwg90k89xhij1i") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.3.1 (c (n "sval_json") (v "0.3.1") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.3.1") (d #t) (k 0)))) (h "1frcmnarzs8a4da4ij5x0sd41jb57dgrrgdqkgq8lg6q5pj3sj8x") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.0 (c (n "sval_json") (v "0.4.0") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.4.0") (d #t) (k 0)))) (h "02rv6kbrxyvn535xaq3c0rzf27w58fxjzrfbmj19gqvpzl8aqs8f") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.1 (c (n "sval_json") (v "0.4.1") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "sval") (r "^0.4.1") (d #t) (k 0)))) (h "02kijqmvx6lki18zghk2c3i1fiw65z80gk1skcbh5ihh0lvxa9a9") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.2 (c (n "sval_json") (v "0.4.2") (d (list (d (n "itoa") (r "^0.4") (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.4.2") (d #t) (k 0)))) (h "1xxvm41b9agpi456dygdzira6vwpdxj3snrg5jkpxdzfj38vxjly") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.3 (c (n "sval_json") (v "0.4.3") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.4.3") (d #t) (k 0)))) (h "1a7q76lqwb17i0y8g9wsz39p7i51n884bz5bcdwbngvzqk3pnbcb") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.4 (c (n "sval_json") (v "0.4.4") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.4.4") (d #t) (k 0)))) (h "0b2pkdjg86jya7zzsn7ba4q1x1p7hw6zqmn3i749r1ynknndnr89") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.5 (c (n "sval_json") (v "0.4.5") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.4.5") (d #t) (k 0)))) (h "17bz27x1mvy786yjfyk1gb2w6j2jf7rag8l0jk3s81iwcqqwfahx") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.6 (c (n "sval_json") (v "0.4.6") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.4.6") (d #t) (k 0)))) (h "1h564bxdmj7g5w7604lmbay6kmlyylr8l5x5bh7j2cb7nwkkdkp2") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.4.7 (c (n "sval_json") (v "0.4.7") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.4.7") (d #t) (k 0)))) (h "1gb340lqm26ch86k4b38kf1rql5qwazwvc29ksz7yq9la1rm5wgw") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.5.0 (c (n "sval_json") (v "0.5.0") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.5.0") (d #t) (k 0)))) (h "08wrm694s82xy8drrplx7b76qfigz18vdfcg1mf6rxmm942sgjdz") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.5.1 (c (n "sval_json") (v "0.5.1") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.5.1") (d #t) (k 0)))) (h "11zb6bismxlffl8q2gzcnqx5s7vbi8nv6m714knc3jpwkz3csklw") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-0.5.2 (c (n "sval_json") (v "0.5.2") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^0.5.2") (d #t) (k 0)))) (h "0bw6jzjsc4w9wvijifdfxizisam2qv45f8sxnh5gks0jyr0an912") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-1.0.0-alpha.1 (c (n "sval_json") (v "1.0.0-alpha.1") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "087br1x9g8aivah99zwvl8rylxvqxxk4y29i0bpjys5kbkh1cl5v") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-1.0.0-alpha.2 (c (n "sval_json") (v "1.0.0-alpha.2") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^1.0.0-alpha.2") (d #t) (k 0)))) (h "1sw51wrsjzr7khw494x0l64kj0abmj0h7kr8s94iaakpy2ghjrka") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-1.0.0-alpha.3 (c (n "sval_json") (v "1.0.0-alpha.3") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^1.0.0-alpha.3") (d #t) (k 0)))) (h "1lcpsgc85a2cjqkyivc0v0iv9yllvgd4i7b24i66qi6yfgwjwisx") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-1.0.0-alpha.4 (c (n "sval_json") (v "1.0.0-alpha.4") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^1.0.0-alpha.4") (d #t) (k 0)))) (h "1gdfciq6q4qz6fzr81d6iyxvwkp047jc56risi39b0mdzhli7xyi") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-1.0.0-alpha.5 (c (n "sval_json") (v "1.0.0-alpha.5") (d (list (d (n "itoa") (r "^0.4") (f (quote ("i128"))) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^1.0.0-alpha.5") (d #t) (k 0)))) (h "0kdn0c6p25g6155dvcwx82wgcsdsf18b1axkwi2qs4lcx60mfl3c") (f (quote (("std" "sval/std"))))))

(define-public crate-sval_json-2.0.0 (c (n "sval_json") (v "2.0.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.0") (d #t) (k 0)))) (h "1slf96988crxiw2jarq0r983i4nj1nvjxn0w5g81q9mm0qp3bbi9") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.0.1 (c (n "sval_json") (v "2.0.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.1") (d #t) (k 0)))) (h "15cp96ba0cg3y01f7m2hb535y8i7wa6l82d4cylr3fx4zaxbn8sl") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.0.2 (c (n "sval_json") (v "2.0.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.2") (d #t) (k 0)))) (h "17vdmxa5b4shhzzn1kvgvdk1vzizg7cvqfdg4p5vcjb9j3lxkm38") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.0.3 (c (n "sval_json") (v "2.0.3") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.0.3") (d #t) (k 0)))) (h "0nxd1sygk2vpc9d29fayxn0wpq6q20wx3v581vjzzrapd6qp9mnm") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.1.0 (c (n "sval_json") (v "2.1.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.1.0") (d #t) (k 0)))) (h "0svmlc355w49sy2rmfvx2992p5p4j81vxi32j6srkq7h3bv0h900") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.1.1 (c (n "sval_json") (v "2.1.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.1.1") (d #t) (k 0)))) (h "1vv0cz4fc6839inc7pzha6p6ylx5dsjk5lvn3b25hi9r5rpb0k1p") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.2.0 (c (n "sval_json") (v "2.2.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.2.0") (d #t) (k 0)))) (h "1r7qa6wbm25i02rh9316h4vizrr36prxs7lqnbaiv3f8px355qcg") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.2.1 (c (n "sval_json") (v "2.2.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.2.1") (d #t) (k 0)))) (h "0ka4gbzsgkhw6hbb1qyl6x46bcdxc85wdq24pidd91zqhgcf9jjh") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.3.0 (c (n "sval_json") (v "2.3.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.3.0") (d #t) (k 0)))) (h "0b6yaxi43zir57bi3cyyqj5q557iq2kscmlfqmhdh3c745jv2gqy") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.3.1 (c (n "sval_json") (v "2.3.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.3.1") (d #t) (k 0)))) (h "0dpiq1nq0wnxqn9i351cv6ki7c8w1d5smxxah612fh02v13l07nh") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.4.0 (c (n "sval_json") (v "2.4.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.4.0") (d #t) (k 0)))) (h "1czsz8nncpx3si87yv5jxv65rfvcpm5ykymrn0cgghjkafai8yv3") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.5.0 (c (n "sval_json") (v "2.5.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.5.0") (d #t) (k 0)))) (h "1mg2pphgswqk5259wx38g5a5q60v9bm1fg144d739mpgim4f24ka") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.6.0 (c (n "sval_json") (v "2.6.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.6.0") (d #t) (k 0)))) (h "1iyi4fkmcaf6dnr8ndj3ccgqi1r5mckay4wgy6b78xixg2i2c4gh") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.6.1 (c (n "sval_json") (v "2.6.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.6.1") (d #t) (k 0)))) (h "13c8hna2n7ykiz3l3ahx2iqmfjmwhyfm0j9n51h8ifwcwckzd9md") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.7.0 (c (n "sval_json") (v "2.7.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.7.0") (d #t) (k 0)))) (h "1an1li3zy23fdba3agx5w3q6c0j0j9l56qmwmb9sqxzldazx25lj") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.7.1 (c (n "sval_json") (v "2.7.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.7.1") (d #t) (k 0)))) (h "1l5p1al46yav05s8ikgl1abqc62ynghnlrzzhga1gh240mxi69ah") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.8.0 (c (n "sval_json") (v "2.8.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.8.0") (d #t) (k 0)))) (h "0jxw9kvw05xcawvbyd1lh0h2n4agrbvw24wdxa5y3vy6bl7w0il3") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.9.0 (c (n "sval_json") (v "2.9.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.0") (d #t) (k 0)))) (h "148bxpy55fny7qvg42g9f1v9kn2rf3qfwnar7s7b55nxqv9dflnb") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.9.1 (c (n "sval_json") (v "2.9.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.1") (d #t) (k 0)))) (h "1rgpjsx1px4hz32da05k4wfxh4j8ynmbzn2fj9zmpcm8fg92zzis") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.9.2 (c (n "sval_json") (v "2.9.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.9.2") (d #t) (k 0)))) (h "136crk8k7r15ps8aj02886zyh78cas1mk069y1c3vpx4d17xlgn6") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.10.0 (c (n "sval_json") (v "2.10.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.0") (d #t) (k 0)))) (h "15bs1fsryi3av4f15nhad7p27xj0ldpqjv100igrlm4d16kx9fcn") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.10.1 (c (n "sval_json") (v "2.10.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.1") (d #t) (k 0)))) (h "0qi8ilb7iw9fnij3r0z0x8w46pcv0f46f2wsp92pdlkckwy2a4zr") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.10.2 (c (n "sval_json") (v "2.10.2") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.10.2") (d #t) (k 0)))) (h "0wp0yyaldqr6kgqsblav86j8fxjp2jbmrsbsiw0yxrhambc3pq3n") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.11.0 (c (n "sval_json") (v "2.11.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.11.0") (d #t) (k 0)))) (h "07n22hzqilpcxy1hgv7rdgqhq20zc005c51lnlga3ln31h1liwyq") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.11.1 (c (n "sval_json") (v "2.11.1") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.11.1") (d #t) (k 0)))) (h "1a8jhcnrshap4xqddnn4zq57nd03qgyrq3yina51mrjcyg6mw7w9") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.12.0 (c (n "sval_json") (v "2.12.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.12.0") (d #t) (k 0)))) (h "1qhd7ihm4sjsbn22nzz0hgqy6s2ni2dqm8fm8b6qndpq4s3kjdrr") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_json-2.13.0 (c (n "sval_json") (v "2.13.0") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "sval") (r "^2.13.0") (d #t) (k 0)))) (h "10jjrvdxn5gb26n901yppag6m3mpwgala8hzg122frdfgp05dx1j") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

