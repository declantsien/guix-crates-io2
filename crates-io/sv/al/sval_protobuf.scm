(define-module (crates-io sv al sval_protobuf) #:use-module (crates-io))

(define-public crate-sval_protobuf-0.1.0 (c (n "sval_protobuf") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "sval") (r "^2.8") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "sval_derive") (r "^2.8") (f (quote ("alloc" "flatten"))) (d #t) (k 2)))) (h "1b4ip4nlfv8bkjkl6lnny1ipjhsc69spx434dczb7p3bs1zwzrh7")))

(define-public crate-sval_protobuf-0.1.1 (c (n "sval_protobuf") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "sval") (r "^2.8") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "sval_derive") (r "^2.8") (f (quote ("alloc" "flatten"))) (d #t) (k 2)))) (h "1ghcxd7g8im25vqjxyndfmm5yibi2kq18glyw76sm6cs51m1xa6l")))

(define-public crate-sval_protobuf-0.1.2 (c (n "sval_protobuf") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "sval") (r "^2.8") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "sval_derive") (r "^2.8") (f (quote ("alloc" "flatten"))) (d #t) (k 2)))) (h "0w1wsbmdks3ys1ilc9hk9cnd5ild7idzmf3329gbbjynmq5vs4h9")))

