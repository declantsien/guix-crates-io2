(define-module (crates-io sv al sval_ref) #:use-module (crates-io))

(define-public crate-sval_ref-2.1.1 (c (n "sval_ref") (v "2.1.1") (d (list (d (n "sval") (r "^2.1.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.1.1") (d #t) (k 2)))) (h "04fbhk2rljhvpaczw4j5rfz22112symkw9v3x4nrp587pnwsmd5y") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.2.0 (c (n "sval_ref") (v "2.2.0") (d (list (d (n "sval") (r "^2.2.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.2.0") (d #t) (k 2)))) (h "1arid4c2xxy9gji0gr4qrcdl677yliaf4hc1brrzia4zd65ilvqs") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.2.1 (c (n "sval_ref") (v "2.2.1") (d (list (d (n "sval") (r "^2.2.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.2.1") (d #t) (k 2)))) (h "0mvhpp1zv4ppn00621g2inkf6a6vvzqbjfxwcla5lcrv42x7fph3") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.3.0 (c (n "sval_ref") (v "2.3.0") (d (list (d (n "sval") (r "^2.3.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.3.0") (d #t) (k 2)))) (h "1jmmr8drqp39685zym5yzkglivflbs7f7sprl4n687rizpkkkhpp") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.3.1 (c (n "sval_ref") (v "2.3.1") (d (list (d (n "sval") (r "^2.3.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.3.1") (d #t) (k 2)))) (h "1qxssib07fadhhnh59jzyawic10bq5wgfrf6gxhiklbjyxik42fs") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.4.0 (c (n "sval_ref") (v "2.4.0") (d (list (d (n "sval") (r "^2.4.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.4.0") (d #t) (k 2)))) (h "1n0xvi5w46c74yki82y435qhj9xwnwzyfwhg2qlp3z5m5w0aqv5d") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.5.0 (c (n "sval_ref") (v "2.5.0") (d (list (d (n "sval") (r "^2.5.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.5.0") (d #t) (k 2)))) (h "0f4rcj2pc362jr7fz8cin6dvx9afc2g4f964grgy9l2d515zr5xp") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.6.0 (c (n "sval_ref") (v "2.6.0") (d (list (d (n "sval") (r "^2.6.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.6.0") (d #t) (k 2)))) (h "1khj6fbfzml937pfr9n957mlgdf5b4f5j8h3hl588cdxlzmdjm2q") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.6.1 (c (n "sval_ref") (v "2.6.1") (d (list (d (n "sval") (r "^2.6.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.6.1") (d #t) (k 2)))) (h "0z1dy9mvqw9w63mz82h8il1r9dxlp1dadh54npcx0jwq5aaa2g3h") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.7.0 (c (n "sval_ref") (v "2.7.0") (d (list (d (n "sval") (r "^2.7.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.7.0") (d #t) (k 2)))) (h "0rq72rq58zwmpjv6wq38amhsj5giy2jj2vq18z26g9sl6hns5clm") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.7.1 (c (n "sval_ref") (v "2.7.1") (d (list (d (n "sval") (r "^2.7.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.7.1") (d #t) (k 2)))) (h "1i2a5rhm24bi4hc4bmijpn81xmz3zv2sc5c0sp6a1jf2am8yc6xr") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.8.0 (c (n "sval_ref") (v "2.8.0") (d (list (d (n "sval") (r "^2.8.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.8.0") (d #t) (k 2)))) (h "01nxqg28jyaw459bv4f4305wg00yqmmsg8glq3mjly9gjn4wq5v6") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.9.0 (c (n "sval_ref") (v "2.9.0") (d (list (d (n "sval") (r "^2.9.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.9.0") (d #t) (k 2)))) (h "0m69zrizbdxfqzspgsbkvsbg40yd0zcm33rz30paiqama1cfmkl9") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.9.1 (c (n "sval_ref") (v "2.9.1") (d (list (d (n "sval") (r "^2.9.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.9.1") (d #t) (k 2)))) (h "1al6gm6cilhbdj31cdji8wj0i8gp30majrfxpalgxr2bmbs08sj1") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.9.2 (c (n "sval_ref") (v "2.9.2") (d (list (d (n "sval") (r "^2.9.2") (d #t) (k 0)) (d (n "sval_test") (r "^2.9.2") (d #t) (k 2)))) (h "1zggqfipx5hqlgg7b5q15p6bk6jv39c556y7hxmlrby5v9lrvrc7") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.10.0 (c (n "sval_ref") (v "2.10.0") (d (list (d (n "sval") (r "^2.10.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.10.0") (d #t) (k 2)))) (h "00ihbcb50mf40kf3wzr79f4rb66ncj1knfhh8y3p8b2qxrd354pm") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.10.1 (c (n "sval_ref") (v "2.10.1") (d (list (d (n "sval") (r "^2.10.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.10.1") (d #t) (k 2)))) (h "1dwc21qfdh1jj8wz91bnxdh088nrncksa4hmx3ml0ryh28a6dab6") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.10.2 (c (n "sval_ref") (v "2.10.2") (d (list (d (n "sval") (r "^2.10.2") (d #t) (k 0)) (d (n "sval_test") (r "^2.10.2") (d #t) (k 2)))) (h "1qd9w4iqp8z7v0mf7icz1409g48jnibyrh9nbnms1hmq5x7hbvbm") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.11.0 (c (n "sval_ref") (v "2.11.0") (d (list (d (n "sval") (r "^2.11.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.11.0") (d #t) (k 2)))) (h "0a5xf9wriykc8vmvvvpq08slav384dv58gbgprvxml42plcx76l2") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.11.1 (c (n "sval_ref") (v "2.11.1") (d (list (d (n "sval") (r "^2.11.1") (d #t) (k 0)) (d (n "sval_test") (r "^2.11.1") (d #t) (k 2)))) (h "17c0ymcm8yn7cr60jdx57wnwii10174jrnj7kk5c9km75af5ywmg") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.12.0 (c (n "sval_ref") (v "2.12.0") (d (list (d (n "sval") (r "^2.12.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.12.0") (d #t) (k 2)))) (h "1iblrpa2r2v4vvw39hxsgdj8n505k9qz37qyzrfpdrskgcd4ysc2") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

(define-public crate-sval_ref-2.13.0 (c (n "sval_ref") (v "2.13.0") (d (list (d (n "sval") (r "^2.13.0") (d #t) (k 0)) (d (n "sval_test") (r "^2.13.0") (d #t) (k 2)))) (h "1kk9yd2lvrjmbyc4i3ajcnarj430lfpl0hdnii7z8y6p2z2pybmy") (f (quote (("std" "alloc" "sval/std") ("alloc" "sval/alloc"))))))

