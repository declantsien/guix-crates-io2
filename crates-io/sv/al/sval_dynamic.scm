(define-module (crates-io sv al sval_dynamic) #:use-module (crates-io))

(define-public crate-sval_dynamic-2.0.0 (c (n "sval_dynamic") (v "2.0.0") (d (list (d (n "sval") (r "^2.0.0") (d #t) (k 0)))) (h "1b5f0ah9jx1x060ziwqv6ymi2kzbc7d8kccw9mbysq0vg7jqkp2m")))

(define-public crate-sval_dynamic-2.0.1 (c (n "sval_dynamic") (v "2.0.1") (d (list (d (n "sval") (r "^2.0.1") (d #t) (k 0)))) (h "0wfw1d643m86n18mra197clcrik0f5gc80hnf4vjw0jzhrpmq2aq")))

(define-public crate-sval_dynamic-2.0.2 (c (n "sval_dynamic") (v "2.0.2") (d (list (d (n "sval") (r "^2.0.2") (d #t) (k 0)))) (h "1dgfz5dzdf5c3xxs9pg40gfd5wa1xwr97hdjgx1rg4ba86lc4zql")))

(define-public crate-sval_dynamic-2.0.3 (c (n "sval_dynamic") (v "2.0.3") (d (list (d (n "sval") (r "^2.0.3") (d #t) (k 0)))) (h "0817pgvxg17finf7qzmaas1ldm27ffbrp3mvx014l1ndwq1khri6")))

(define-public crate-sval_dynamic-2.1.0 (c (n "sval_dynamic") (v "2.1.0") (d (list (d (n "sval") (r "^2.1.0") (d #t) (k 0)))) (h "1g6hzh9v4lyi90gafp2gzd8awj3s5i6cf4ad2s8rixd6217297av")))

(define-public crate-sval_dynamic-2.1.1 (c (n "sval_dynamic") (v "2.1.1") (d (list (d (n "sval") (r "^2.1.1") (d #t) (k 0)))) (h "1kjj7ka6m5pg6b9nrn95wr011wqlzqlbjjsalqqd1laav73b6qx3")))

(define-public crate-sval_dynamic-2.2.0 (c (n "sval_dynamic") (v "2.2.0") (d (list (d (n "sval") (r "^2.2.0") (d #t) (k 0)))) (h "0njs20yh3c1p6in9zz1kq1acvw53r57f9siajicbnpbgb9pccs98")))

(define-public crate-sval_dynamic-2.2.1 (c (n "sval_dynamic") (v "2.2.1") (d (list (d (n "sval") (r "^2.2.1") (d #t) (k 0)))) (h "0ayc870077vmhyr96ayfwlplmd628nl0ahdqw5kfn6n2d0wwlns0")))

(define-public crate-sval_dynamic-2.3.0 (c (n "sval_dynamic") (v "2.3.0") (d (list (d (n "sval") (r "^2.3.0") (d #t) (k 0)))) (h "0xdhsgbhfsf730z47wqpnvj0a429y5cr6wcsnkz2z1vd8izbz21m")))

(define-public crate-sval_dynamic-2.3.1 (c (n "sval_dynamic") (v "2.3.1") (d (list (d (n "sval") (r "^2.3.1") (d #t) (k 0)))) (h "00c7y9z65wgbk51mkgw1zblqdlqyidg08k1qjxqabgfgcc8db3lc")))

(define-public crate-sval_dynamic-2.4.0 (c (n "sval_dynamic") (v "2.4.0") (d (list (d (n "sval") (r "^2.4.0") (d #t) (k 0)))) (h "0ih8nypdc5vyzsmh881hdpqv93bkwvmj6ngfcad648cr9bdgqjnf")))

(define-public crate-sval_dynamic-2.5.0 (c (n "sval_dynamic") (v "2.5.0") (d (list (d (n "sval") (r "^2.5.0") (d #t) (k 0)))) (h "12xvjzwkadyqaznzijhl2p83vaj92bx4jxkaka2j4prc6a44jsxd")))

(define-public crate-sval_dynamic-2.6.0 (c (n "sval_dynamic") (v "2.6.0") (d (list (d (n "sval") (r "^2.6.0") (d #t) (k 0)))) (h "0fi760m2j6fsjgnxrifc15r2sgxdcs7q2ih0jnmv4v4zkqscfpzf")))

(define-public crate-sval_dynamic-2.6.1 (c (n "sval_dynamic") (v "2.6.1") (d (list (d (n "sval") (r "^2.6.1") (d #t) (k 0)))) (h "1kq3shbdfz74hhq4aaf27h99ax7piaqd3f4d6g9nx93pia765vx0")))

(define-public crate-sval_dynamic-2.7.0 (c (n "sval_dynamic") (v "2.7.0") (d (list (d (n "sval") (r "^2.7.0") (d #t) (k 0)))) (h "0446ghqndcdpfi6b233fqimpis1cj6hcj9mgqcz8a7gdykffi392")))

(define-public crate-sval_dynamic-2.7.1 (c (n "sval_dynamic") (v "2.7.1") (d (list (d (n "sval") (r "^2.7.1") (d #t) (k 0)))) (h "0f819kpdd7fv9wfaja4fyb9qwkwh6kqlvihh1227kwgyp27agdy2")))

(define-public crate-sval_dynamic-2.8.0 (c (n "sval_dynamic") (v "2.8.0") (d (list (d (n "sval") (r "^2.8.0") (d #t) (k 0)))) (h "1xb840qdkg6wf7n4rs7sqcya4x5iv7gyxmarzfns4k9bq4n5lky3")))

(define-public crate-sval_dynamic-2.9.0 (c (n "sval_dynamic") (v "2.9.0") (d (list (d (n "sval") (r "^2.9.0") (d #t) (k 0)))) (h "1kd820g86pb95c0w3qgbhqyp5m05d7vr1nzih315mfk3zk7id43z")))

(define-public crate-sval_dynamic-2.9.1 (c (n "sval_dynamic") (v "2.9.1") (d (list (d (n "sval") (r "^2.9.1") (d #t) (k 0)))) (h "16ii6ayvk9yvxbccs9bwhw6b9xp0q0dxmlr4p5d3p5v9wyzzk2zv")))

(define-public crate-sval_dynamic-2.9.2 (c (n "sval_dynamic") (v "2.9.2") (d (list (d (n "sval") (r "^2.9.2") (d #t) (k 0)))) (h "118sqlv2iyg0sh7di0plsncy1xky88l9d0f01k6h6i7a28w7yhdf")))

(define-public crate-sval_dynamic-2.10.0 (c (n "sval_dynamic") (v "2.10.0") (d (list (d (n "sval") (r "^2.10.0") (d #t) (k 0)))) (h "1gs47q57mx3bi59fy3c81k7dmibw538vpqggr4csn57p5si3hw0v")))

(define-public crate-sval_dynamic-2.10.1 (c (n "sval_dynamic") (v "2.10.1") (d (list (d (n "sval") (r "^2.10.1") (d #t) (k 0)))) (h "0dvg0gbl4wga1raz8z9b64fh678qk0yvphk6zz5x6a6hsv2f9y75")))

(define-public crate-sval_dynamic-2.10.2 (c (n "sval_dynamic") (v "2.10.2") (d (list (d (n "sval") (r "^2.10.2") (d #t) (k 0)))) (h "1f2p3xvq5qyg0w721as7dxrgqgrfqsc0m7qp2r1pn7fvkqjx54wx")))

(define-public crate-sval_dynamic-2.11.0 (c (n "sval_dynamic") (v "2.11.0") (d (list (d (n "sval") (r "^2.11.0") (d #t) (k 0)))) (h "1z2rw15hg4d5iislrdl18npvjj32grpyc8px7x0rl2935a1wb2i3")))

(define-public crate-sval_dynamic-2.11.1 (c (n "sval_dynamic") (v "2.11.1") (d (list (d (n "sval") (r "^2.11.1") (d #t) (k 0)))) (h "0362m8776cjlr5xc821jdih51prl8kpa52hhn20b5p7cw8bb2kx7")))

(define-public crate-sval_dynamic-2.12.0 (c (n "sval_dynamic") (v "2.12.0") (d (list (d (n "sval") (r "^2.12.0") (d #t) (k 0)))) (h "18cw4wcg1524hm25cr423bd8faim6hdjnwqgbshr2q9w9hbj8hsq")))

(define-public crate-sval_dynamic-2.13.0 (c (n "sval_dynamic") (v "2.13.0") (d (list (d (n "sval") (r "^2.13.0") (d #t) (k 0)))) (h "0vsmxlbw7nirj476h9sp4h41ff29a28b6z7d0k92f4csj83jngza")))

