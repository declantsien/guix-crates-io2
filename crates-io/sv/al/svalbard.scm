(define-module (crates-io sv al svalbard) #:use-module (crates-io))

(define-public crate-svalbard-1.0.0 (c (n "svalbard") (v "1.0.0") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0qri06jnigapnx9fl5jjkyvpfn5x04bxq86fv65hk5ngbmrnyc8c")))

(define-public crate-svalbard-1.0.1 (c (n "svalbard") (v "1.0.1") (d (list (d (n "cursive") (r "^0.13") (f (quote ("termion-backend"))) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "ferris_print") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0ma6zxvlzl1lv946aq633vmcndzwr3jhyr4izfh8s6s878gwj9iq")))

