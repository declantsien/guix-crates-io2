(define-module (crates-io sv se svsep) #:use-module (crates-io))

(define-public crate-svsep-1.0.0 (c (n "svsep") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1x2vxbp4vmsf2ggpn58lqxl2wagjr588lhhjyx6vfi3mnkdbs9lb")))

