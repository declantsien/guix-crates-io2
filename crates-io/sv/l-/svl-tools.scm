(define-module (crates-io sv l- svl-tools) #:use-module (crates-io))

(define-public crate-svl-tools-0.1.0 (c (n "svl-tools") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "crc") (r "^3.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "1mmih2kb28jfv33m20chnpwfx0fzplaxrphy1d1g0fqzf25dpka4")))

