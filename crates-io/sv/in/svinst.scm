(define-module (crates-io sv in svinst) #:use-module (crates-io))

(define-public crate-svinst-0.0.4 (c (n "svinst") (v "0.0.4") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "sv-parser") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.6.3") (d #t) (k 0)))) (h "0kxhyrraczgrz4gylwgd0ynw5dp9ig0n99qhcax91ykp1pkd3byr")))

(define-public crate-svinst-0.0.5 (c (n "svinst") (v "0.0.5") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "sv-parser") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.6.3") (d #t) (k 0)))) (h "0bx3g3irhhcwakhamindkhva8va3m0vgbv198miib3kzkqk5irz8")))

(define-public crate-svinst-0.0.7 (c (n "svinst") (v "0.0.7") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "sv-parser") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.6.3") (d #t) (k 0)))) (h "1nh2gjk0ix1mlj34qn12lprpw66cclabkbm18v23wn18r9kbd28k")))

(define-public crate-svinst-0.0.8 (c (n "svinst") (v "0.0.8") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "sv-parser") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.6.3") (d #t) (k 0)))) (h "0wyadv789xsh3q9kzhiczw8yscmvxc68ql2w11jmpv623hff5hz9")))

(define-public crate-svinst-0.0.9 (c (n "svinst") (v "0.0.9") (d (list (d (n "enquote") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "sv-parser") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.6.3") (d #t) (k 0)))) (h "0cc6l0fg2k6xqhn2c4xc4has4gbivyv91cd61mmnap6zz110wlbi")))

(define-public crate-svinst-0.1.0 (c (n "svinst") (v "0.1.0") (d (list (d (n "enquote") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "sv-parser") (r "^0.7.0") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.7.0") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.7.0") (d #t) (k 0)))) (h "1maf5dvs836jj5j7l2m6x2j4g9zbsdyhywx425mdqaxmpfk2qy8q")))

(define-public crate-svinst-0.1.1 (c (n "svinst") (v "0.1.1") (d (list (d (n "enquote") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "sv-parser") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.6.3") (d #t) (k 0)))) (h "09n4nky86v6qb7zn8wl5qwpqa2k61b1z3s20jg8rw81khp0881k9")))

(define-public crate-svinst-0.1.3 (c (n "svinst") (v "0.1.3") (d (list (d (n "enquote") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "sv-parser") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.6.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.6.3") (d #t) (k 0)))) (h "0rz0cz90923hb0010ph7p2ljmiawmrx61rbcm3ssd0vgfydb14hn")))

(define-public crate-svinst-0.1.4 (c (n "svinst") (v "0.1.4") (d (list (d (n "enquote") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "sv-parser") (r "^0.8.2") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.8.2") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.8.2") (d #t) (k 0)))) (h "0bs7agfyr1xiggjd94xbakfr9mi99hg72nzlq0ng2x30aq5qfk3z")))

(define-public crate-svinst-0.1.5 (c (n "svinst") (v "0.1.5") (d (list (d (n "enquote") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "sv-parser") (r "^0.8.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.8.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.8.3") (d #t) (k 0)))) (h "0n63rq4hiprdqgsvp4kliygfbb7m4clq3p0x189kd2hdh8d4xwgk")))

(define-public crate-svinst-0.1.6 (c (n "svinst") (v "0.1.6") (d (list (d (n "enquote") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "sv-parser") (r "^0.8.3") (d #t) (k 0)) (d (n "sv-parser-error") (r "^0.8.3") (d #t) (k 0)) (d (n "sv-parser-syntaxtree") (r "^0.8.3") (d #t) (k 0)))) (h "0558kri1yzp7vqm01hig0l4qn56phijzwj51c3pcsxayvdkw83nv")))

