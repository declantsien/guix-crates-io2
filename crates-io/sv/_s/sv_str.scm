(define-module (crates-io sv _s sv_str) #:use-module (crates-io))

(define-public crate-sv_str-1.0.0 (c (n "sv_str") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)))) (h "0i8fp30h04zy001ril15p8py06f1zihcqhfp5n4zkqw6qziqjqks")))

(define-public crate-sv_str-1.0.1 (c (n "sv_str") (v "1.0.1") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)))) (h "0gqci50qi12nywg6kvyhwlpy8r5mwasjkwjzz9ncrcggqbw3s74s")))

(define-public crate-sv_str-1.0.2 (c (n "sv_str") (v "1.0.2") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)))) (h "1vw1gashxk9gsqvi96jy634d0y8vgvwviaar7jqyvqw9q150w0rj")))

(define-public crate-sv_str-1.0.3 (c (n "sv_str") (v "1.0.3") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)))) (h "05z5q2lvzgw6pchkikzzp7nwyxmg0hna6f56cflaznabrhi118bd")))

(define-public crate-sv_str-1.0.4 (c (n "sv_str") (v "1.0.4") (d (list (d (n "lazy-regex") (r "^2.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0sak39f6b21lril0rfi5pj0vqcixc5zs2ds9r1h71fc5ygl150wi")))

