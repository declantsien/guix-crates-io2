(define-module (crates-io sv gg svggen) #:use-module (crates-io))

(define-public crate-svggen-0.1.0 (c (n "svggen") (v "0.1.0") (h "1bqcwk69xcs7isif3d2pixma7ixy6y0i1q6pixynlhh96dw3kg1x")))

(define-public crate-svggen-1.0.0 (c (n "svggen") (v "1.0.0") (h "0s4gh2ds2qfqapypryrycphcw94hyh7b3f5h0apdvcakyb322x5k")))

(define-public crate-svggen-2.0.0 (c (n "svggen") (v "2.0.0") (d (list (d (n "rutil") (r "^0.1.0") (d #t) (k 0)))) (h "0i7r4hw1mvv7ndigwhrghlxx5mfbp0g44m8lr9s5i3p1wq3l4q49")))

