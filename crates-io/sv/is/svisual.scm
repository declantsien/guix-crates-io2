(define-module (crates-io sv is svisual) #:use-module (crates-io))

(define-public crate-svisual-0.2.3 (c (n "svisual") (v "0.2.3") (d (list (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)) (d (n "heapless") (r "^0.4.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1r254b9a1dq771xgl4bzif8dxsc33jmpv5ng88wjj8imkgq5qn2r")))

(define-public crate-svisual-0.2.4 (c (n "svisual") (v "0.2.4") (d (list (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)) (d (n "heapless") (r "^0.5.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "10gqz1q5a73m7mdirr01nw18p46hgi9p9dz5i54nvr2kxrga450a")))

(define-public crate-svisual-0.3.0 (c (n "svisual") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "0vjvsxvf5p7pgiqbyjj025jc4ax9x04mln7ad38627jppxjpz946")))

(define-public crate-svisual-0.4.0 (c (n "svisual") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "0c38sypnjy2a6nwfravn1a050yph3f2qq6jb5qg8bn95ml2pja40")))

(define-public crate-svisual-0.4.1 (c (n "svisual") (v "0.4.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "1cxgj0pw4fpg2l0qxbd19zdsd0wqq3j4qyi1hf072b40ra46cyy3")))

