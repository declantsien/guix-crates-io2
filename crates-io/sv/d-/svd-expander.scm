(define-module (crates-io sv d- svd-expander) #:use-module (crates-io))

(define-public crate-svd-expander-0.1.0 (c (n "svd-expander") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "0k44zgzpki4d10ca3by6cykkj53br5n67kwpfi4a22hjr27lb8sm") (y #t)))

(define-public crate-svd-expander-0.1.1 (c (n "svd-expander") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "1zp76wf1ggzpmh8507cn5lbxnf0nq0lrpz8y4lqmhbshy6d8s7s5") (y #t)))

(define-public crate-svd-expander-0.1.2 (c (n "svd-expander") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "1h28hay9x7zxx8vl5w6cr5v11s2c2jkhv10xrsw016axp3av48c3") (y #t)))

(define-public crate-svd-expander-0.2.0 (c (n "svd-expander") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "0ypjcbbqh41c0mcr2bp8l4yg58rfbvmkfcn3s2skr57i0v4yip31")))

(define-public crate-svd-expander-0.2.1 (c (n "svd-expander") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "0r652f26n6m1yjl8369dw3acy63r44pml38fk6zks2zmmybnl18d")))

(define-public crate-svd-expander-0.2.2 (c (n "svd-expander") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "10lm20l237xnc3s9di2vsjbz35f4cc1fy66l4gidwwqi7yjw2j61")))

(define-public crate-svd-expander-0.2.3 (c (n "svd-expander") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "0gdy3alw0vpvra9fz8f913zlsqd7cz10zyd16367w5z0qy25wyxh")))

(define-public crate-svd-expander-0.2.4 (c (n "svd-expander") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "1bdhzzmxi3nzfszx7wlpp7wv4s1snxd3rbazl5mxvklscn5bcmci") (y #t)))

(define-public crate-svd-expander-0.2.5 (c (n "svd-expander") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "076ng8vs37w0xq3mb6j6mkcigh63yhw4n0zsgy66di92hmgwci4j")))

(define-public crate-svd-expander-0.3.0 (c (n "svd-expander") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "00xp6m8949bh5325f1hgx8g7iz16d594ld5k2wg5lwg8vvkjbpmg")))

(define-public crate-svd-expander-0.3.1 (c (n "svd-expander") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "19r40rby4llq4zdcnv2wdni168psgrm75fwbixj8sl50z5zghdvp")))

(define-public crate-svd-expander-0.3.2 (c (n "svd-expander") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "0jxppwbr9gnd936dqwgy7lvpypqk6v4pban3ykqwbbd33h88ypc8")))

(define-public crate-svd-expander-0.3.3 (c (n "svd-expander") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "0k7bphs80cfm94lgc3r3i36zkiry0mbgwwhw51vs26fa26wcw8b3")))

(define-public crate-svd-expander-0.4.0 (c (n "svd-expander") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "svd-parser") (r "^0.9.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "1c9bwjy9dxdmibfh9s5mjza41ivmgmxv2z3c5ifslaffakpzmg6l")))

