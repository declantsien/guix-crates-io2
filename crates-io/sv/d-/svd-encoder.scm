(define-module (crates-io sv d- svd-encoder) #:use-module (crates-io))

(define-public crate-svd-encoder-0.11.0 (c (n "svd-encoder") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "svd-rs") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "10crnrfigcxdp0z74wgmjqhbbyam5dqyn5s70lry04999mgin1mp")))

(define-public crate-svd-encoder-0.12.0 (c (n "svd-encoder") (v "0.12.0") (d (list (d (n "svd-rs") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "0fwrnpi0sahjxzlp4726dw1hpqyq1crgp6h7hid1lq0kdv95rp97")))

(define-public crate-svd-encoder-0.13.0 (c (n "svd-encoder") (v "0.13.0") (d (list (d (n "svd-rs") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "1sz2rh080iy3ycxfmdj0fk3fp8gchhz8lqbm60dnbhx3713rwl2w")))

(define-public crate-svd-encoder-0.13.1 (c (n "svd-encoder") (v "0.13.1") (d (list (d (n "svd-rs") (r "^0.13.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "039ix0arh546cdvh3bjfp60dyw6dk5sgibxf716q884yknkg9dsm")))

(define-public crate-svd-encoder-0.14.0 (c (n "svd-encoder") (v "0.14.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "svd-rs") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "1pcn4qc1inhps72s6dshdz4nmcgi6xq8vp9xqc7fw050yq768hwl") (r "1.56.0")))

(define-public crate-svd-encoder-0.14.1 (c (n "svd-encoder") (v "0.14.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "svd-rs") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "1n408pgdk0z6x5k5h2nvdi66nmnfrakbqrg9ncdhqci41ckaqb0s") (r "1.56.0")))

(define-public crate-svd-encoder-0.14.2 (c (n "svd-encoder") (v "0.14.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "svd-rs") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "15agc22m4kzwfb3q838dfa7ff73n2rphkijmp7f6kpfxwy9m870m") (r "1.56.0")))

(define-public crate-svd-encoder-0.14.3 (c (n "svd-encoder") (v "0.14.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "svd-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "1p9icp7gk37iiaxm8fl48p6f35niqg81mr9krkr2xvxh5k7y9bwg") (r "1.56.0")))

(define-public crate-svd-encoder-0.14.4 (c (n "svd-encoder") (v "0.14.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "svd-rs") (r "^0.14.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (f (quote ("attribute-order"))) (d #t) (k 0)))) (h "0pvnjyh00sjs0h5scjzak59gxzpz4ypmqgdhwxhp3clgc6ir9caj") (r "1.58.0")))

