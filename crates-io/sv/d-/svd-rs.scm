(define-module (crates-io sv d- svd-rs) #:use-module (crates-io))

(define-public crate-svd-rs-0.11.0 (c (n "svd-rs") (v "0.11.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "0r5687n8nppz4xy41fa72icncc4nwmivx2zjvp9smzyqiryx32dw") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.11.1 (c (n "svd-rs") (v "0.11.1") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "1a063ylflik9vllfrq4lsmib0skqaf0fgsd288ph87ccjwb9023c") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.11.2 (c (n "svd-rs") (v "0.11.2") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "1kmsfg637bh3zc87xqa0cgkja0aki6nhf0ry83pr0v7sfvkncz2b") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.12.0 (c (n "svd-rs") (v "0.12.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "0ga24cnxa2gfd3h8ixi0mn90znnm3ibizjbvrwcivyqqzd4khhdx") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.12.1 (c (n "svd-rs") (v "0.12.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "1vi5xb4wv8x9dhnswkccih9h0mv3i2k8dmbvrir84qvir9476p4l") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.13.0 (c (n "svd-rs") (v "0.13.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "0mmq2ygjahix48dphysz6nxc48gdkxvg4d5b3m9x4l8i35nd77gh") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.13.1 (c (n "svd-rs") (v "0.13.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "0b4749fb99l2c728vkykj698f2pz4wxgkhc3sm6jhx7wb30if0bf") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.13.2 (c (n "svd-rs") (v "0.13.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "056ybb1lk8zdf8zzaja0im74xlymh7a5rljn6cz282kcdz29zzqd") (f (quote (("derive-from"))))))

(define-public crate-svd-rs-0.14.0 (c (n "svd-rs") (v "0.14.0") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16d6p745mrwc3gg4fa3ml4fz2yjmga61yqjhsi8xd5s5wrk2yfx6") (f (quote (("derive-from")))) (r "1.56.0")))

(define-public crate-svd-rs-0.14.1 (c (n "svd-rs") (v "0.14.1") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hsvq2mb9ip8lji15zlgilg45c999i217lzzahfbkj1rg46746xp") (f (quote (("derive-from")))) (r "1.56.0")))

(define-public crate-svd-rs-0.14.2 (c (n "svd-rs") (v "0.14.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0g24lnqj03250z7inf663p3al1m3m3pn0fchk4i66sx69jy770nh") (f (quote (("derive-from")))) (r "1.56.0")))

(define-public crate-svd-rs-0.14.3 (c (n "svd-rs") (v "0.14.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i3ps57y4mcfxlwyz9nr1f7vw2plk4f8hj8bn8hqf3yxg6a4ww8q") (f (quote (("derive-from")))) (r "1.58.0")))

(define-public crate-svd-rs-0.14.4 (c (n "svd-rs") (v "0.14.4") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0cwbg4i2awwwpjlqfap787qzp08pl4dhjaa4czmjir8fgli0z0rm") (f (quote (("derive-from")))) (r "1.58.0")))

(define-public crate-svd-rs-0.14.5 (c (n "svd-rs") (v "0.14.5") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1x1563spf6997cz5xnz57a3ap63gsrrq44s8ixqvys9n597g6mnw") (f (quote (("derive-from")))) (r "1.58.0")))

(define-public crate-svd-rs-0.14.6 (c (n "svd-rs") (v "0.14.6") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ndmw9bf98ayqlzlfwis4a0nf0plrm8d6xyradics3i9k9269ml8") (f (quote (("derive-from")))) (r "1.58.0")))

(define-public crate-svd-rs-0.14.7 (c (n "svd-rs") (v "0.14.7") (d (list (d (n "once_cell") (r "^1.17.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1m3p9a8g8ix2vnyf9v3w94yky6jf1vx9ik76cxa19l51xxsz70hw") (f (quote (("derive-from")))) (r "1.61.0")))

(define-public crate-svd-rs-0.14.8 (c (n "svd-rs") (v "0.14.8") (d (list (d (n "once_cell") (r "^1.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0y421ad4afyq38rg0m31nmkc0a12dz3a17am1d4wqms166881ska") (f (quote (("derive-from")))) (r "1.65.0")))

