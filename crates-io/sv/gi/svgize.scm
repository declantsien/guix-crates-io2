(define-module (crates-io sv gi svgize) #:use-module (crates-io))

(define-public crate-svgize-0.0.1 (c (n "svgize") (v "0.0.1") (d (list (d (n "quick-xml") (r "^0.31") (k 0)))) (h "0jsm4mz3ypdsgmn9q41qj0gxhg8gww55hlvvwjpka1jhjkg0cyi7") (f (quote (("exp") ("default" "attr-core" "attr-styling" "attr-presentation" "attr-event") ("crossorigin") ("attr-styling") ("attr-presentation") ("attr-event") ("attr-core") ("attr-cond_proc")))) (y #t)))

(define-public crate-svgize-0.0.2 (c (n "svgize") (v "0.0.2") (d (list (d (n "quick-xml") (r "^0.31") (k 0)))) (h "0pr23hn0i1wp4f8z9k7vzsp1m27y81awy4ix05n9ngh4aiwlgrg5") (f (quote (("exp") ("default" "attr-core" "attr-styling" "attr-presentation" "attr-event") ("crossorigin") ("attr-styling") ("attr-presentation") ("attr-event") ("attr-core") ("attr-cond_proc")))) (y #t)))

(define-public crate-svgize-0.0.3 (c (n "svgize") (v "0.0.3") (d (list (d (n "quick-xml") (r "^0.31") (k 0)))) (h "1wx4im7y6z7mi19b3x897fvw8925wzibnxc9jh4dsb2gc63xck39") (f (quote (("exp") ("default" "attr-core" "attr-styling" "attr-presentation" "attr-event") ("crossorigin") ("attr-styling") ("attr-presentation") ("attr-event") ("attr-core") ("attr-cond_proc"))))))

