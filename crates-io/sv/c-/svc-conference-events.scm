(define-module (crates-io sv c- svc-conference-events) #:use-module (crates-io))

(define-public crate-svc-conference-events-0.1.0 (c (n "svc-conference-events") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1acr2kbpwkj6np60qbmj74p3zlshs6nwccq830k9yn8g5x0n7bwg")))

(define-public crate-svc-conference-events-0.2.0 (c (n "svc-conference-events") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10x7c8n4rcal9770d5ap39753k75x09kmq36bpgrwvs8l5r5w679")))

