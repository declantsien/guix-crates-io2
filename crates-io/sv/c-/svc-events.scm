(define-module (crates-io sv c- svc-events) #:use-module (crates-io))

(define-public crate-svc-events-0.3.0 (c (n "svc-events") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z1kk42gc2cli2sqvv5y6f1q0ivy5wr4md1cwbdiyxm18nz4vm54")))

(define-public crate-svc-events-0.4.0 (c (n "svc-events") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j18qsk7bdqgp9w12hh3kjw00gm9dg0skwcbn1y4g5b5zcbp3rkb")))

(define-public crate-svc-events-0.5.0 (c (n "svc-events") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.20") (d #t) (k 0)) (d (n "svc-authn") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1nw1n4861ggsp150ggi15y1kpphahh8nkyicwzizrdg6wqin60ah")))

(define-public crate-svc-events-0.6.0 (c (n "svc-events") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.20") (d #t) (k 0)) (d (n "svc-authn") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0zqdka9f8qzcsg53ayg5r675r8ga5i4symwfkm2x7islz5f3jpgy")))

(define-public crate-svc-events-0.7.0 (c (n "svc-events") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.20") (d #t) (k 0)) (d (n "svc-authn") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1mdp6042013qw6dq99wldkpj1mrvawmkw9gf6r1absm7lp72yhqm")))

(define-public crate-svc-events-0.8.0 (c (n "svc-events") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.20") (d #t) (k 0)) (d (n "svc-authn") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1yy4b3j6v5xjj6wp5j4q4ywvcia2369h80hhcff41s13v16l52i2")))

(define-public crate-svc-events-0.9.0 (c (n "svc-events") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.20") (d #t) (k 0)) (d (n "svc-authn") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0x3r8i24m2zxbx4lsky0dicgynx4hnxgjhvmkrd1cw126hzrw2x3")))

(define-public crate-svc-events-0.10.0 (c (n "svc-events") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.21") (d #t) (k 0)) (d (n "svc-authn") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0dp6cas7y4i8ddkwkb63zby2wcjbjiplmw32l4z5x7ygig2zz5gx")))

(define-public crate-svc-events-0.11.0 (c (n "svc-events") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svc-agent") (r "^0.21") (d #t) (k 0)) (d (n "svc-authn") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0dcpcrj74m7zg7s39wa7d2fxa51l5j1wl26xywynk2srbb8lpn1s")))

