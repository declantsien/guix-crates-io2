(define-module (crates-io sv c- svc-storage-client-grpc) #:use-module (crates-io))

(define-public crate-svc-storage-client-grpc-0.2.0 (c (n "svc-storage-client-grpc") (v "0.2.0") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.3") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0z92yxfykfjb7wp3mi8zfz4z87fm5y99lkmssy6sbbwxjixgxcnh")))

