(define-module (crates-io sv #{4s}# sv4state) #:use-module (crates-io))

(define-public crate-sv4state-0.1.0 (c (n "sv4state") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0bdqbsjmhn99dp45f9azanhain2hf0jcfmqq8812f16gz5qzyi7f")))

(define-public crate-sv4state-0.2.0 (c (n "sv4state") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1iqqx44js7n3x21vd8xv5c4nn82yajbzdy937xridfr9ka2aw14b")))

