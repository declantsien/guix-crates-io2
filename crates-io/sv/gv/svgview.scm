(define-module (crates-io sv gv svgview) #:use-module (crates-io))

(define-public crate-svgview-0.1.0 (c (n "svgview") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "pixels") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "resvg") (r "^0.20.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.6.3") (d #t) (k 0)) (d (n "usvg") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11") (d #t) (k 0)))) (h "1cm5l6vz2rc8fgxsqj5m1yjj7ibjx1zkmsqjrjqzv8d2ikzjynyi") (f (quote (("optimize" "log/release_max_level_warn") ("default" "optimize"))))))

