(define-module (crates-io sv by svbyte) #:use-module (crates-io))

(define-public crate-svbyte-0.1.0 (c (n "svbyte") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0z6cigrpfyfsz1k4by66vbsds6m8k8p00vbyw9arihknvdrbqk6p")))

(define-public crate-svbyte-0.1.1 (c (n "svbyte") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0a36w8szqd9m67nd9dnb7zdh538r9bzlf1na6x9shr0kxpxciriw")))

