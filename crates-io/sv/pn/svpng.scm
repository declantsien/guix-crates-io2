(define-module (crates-io sv pn svpng) #:use-module (crates-io))

(define-public crate-svpng-0.1.0 (c (n "svpng") (v "0.1.0") (h "04fjw791widcgll5is2vggx4ysqp202sycwcxr46jnzs8jyzkxcq")))

(define-public crate-svpng-0.1.1 (c (n "svpng") (v "0.1.1") (h "0dy5dxl148x4qkvisp1cccd0xszcdhix6445d81g4x5z7bawd3w0")))

