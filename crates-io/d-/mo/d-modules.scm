(define-module (crates-io d- mo d-modules) #:use-module (crates-io))

(define-public crate-d-modules-0.1.2 (c (n "d-modules") (v "0.1.2") (d (list (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "0p49y3iyygr6hkcjkj6s6gj62wc3qzkq0igfhhqdadxli8px2avf")))

(define-public crate-d-modules-0.1.3 (c (n "d-modules") (v "0.1.3") (d (list (d (n "elrond-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "0pdxqw9dh1ixmrh796i6wc6a9c004v5cfbldsh8g8ncczxbybhy7")))

