(define-module (crates-io d- td d-td) #:use-module (crates-io))

(define-public crate-d-td-0.1.0 (c (n "d-td") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)))) (h "0fpyr9i06s1gl6hwnm23rk2agg4lyz4ss72h8wpjy1dhak3xxql3")))

(define-public crate-d-td-0.2.0 (c (n "d-td") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)))) (h "1vgzpq4mjgcmr8qv84zvkdabb0nr9sj15f4findphcfml3mskmms")))

(define-public crate-d-td-0.3.0 (c (n "d-td") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)))) (h "1fa01kicp0jkkp7k7vxg9caqqj5hy4pkcsfi7gyqxfw435qpglk5")))

(define-public crate-d-td-1.0.0 (c (n "d-td") (v "1.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)))) (h "141yml1gfa34j6vy96r7facd19kbqczl7dxj6kjk90sf4np7qjzz")))

