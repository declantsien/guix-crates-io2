(define-module (crates-io d- st d-stu) #:use-module (crates-io))

(define-public crate-d-stu-0.1.0 (c (n "d-stu") (v "0.1.0") (d (list (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "1b8y72ciamcg1g6bx0jgz9km5yyxykjgc2frsxd6c45m56nljmbs")))

(define-public crate-d-stu-0.1.1 (c (n "d-stu") (v "0.1.1") (d (list (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "1fzza2alzr4r240hcf71vfyyar0ikinvvh0h21mry3ffi833yv0r")))

(define-public crate-d-stu-0.1.2 (c (n "d-stu") (v "0.1.2") (d (list (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "19d32bxkfmqn52p3i9w2vznpmaffsaax2p8gkff7kc7c84422bh2")))

(define-public crate-d-stu-0.1.3 (c (n "d-stu") (v "0.1.3") (d (list (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "1l3kqwwkp5yhv36ljzs0vlwl3j9kafzgc107rqff9x2jv92fn3dz")))

(define-public crate-d-stu-0.1.4 (c (n "d-stu") (v "0.1.4") (d (list (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "0h78vp45v81gkhk6781qflkg9ycx399k71lwa28gr1y5f073h5l9")))

(define-public crate-d-stu-0.1.5 (c (n "d-stu") (v "0.1.5") (d (list (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "18nmfcyqv4rs06mlxdwscnhfddqgi4l15if5s1yyyfz4sc2iz6kd")))

