(define-module (crates-io d- rs d-rs) #:use-module (crates-io))

(define-public crate-d-rs-0.1.0 (c (n "d-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("cargo" "unstable-multicall"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1dy8k5w6852zppgdc9xan87hk6xmy85wgq42s0yxk0kp4l9vily8")))

(define-public crate-d-rs-0.2.0 (c (n "d-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("cargo" "unstable-multicall"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)))) (h "1yzdkfrzw9h6nh3wbvm9pi3k8dhdfm9671a7zl6x7b35xfk4aqr3")))

