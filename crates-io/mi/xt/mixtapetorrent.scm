(define-module (crates-io mi xt mixtapetorrent) #:use-module (crates-io))

(define-public crate-mixtapetorrent-0.1.0 (c (n "mixtapetorrent") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "02r9gik4427i788jfzvzcbdrlw8s9qzxl0vmmfa1x4mb0j9zakyv")))

(define-public crate-mixtapetorrent-0.1.1 (c (n "mixtapetorrent") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored_json") (r "^2.1.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1prsk61z9lxxq41cc1vrccldgvp44dgph4p54njpq63149mhmsaj")))

