(define-module (crates-io mi le mile39) #:use-module (crates-io))

(define-public crate-mile39-0.1.0 (c (n "mile39") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lmdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1zwpclzscllg4yc86sb0ryb8f1wkhgwcr9gp427snkp6szkj59g7")))

