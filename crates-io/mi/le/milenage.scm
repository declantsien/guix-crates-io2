(define-module (crates-io mi le milenage) #:use-module (crates-io))

(define-public crate-milenage-0.1.0 (c (n "milenage") (v "0.1.0") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "aes-soft") (r "^0.3.3") (d #t) (k 0)) (d (n "block-modes") (r "^0.3.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 0)))) (h "0rgsv9bam4dzm2ln8b5ndwchp9kz24qgxh2khm7c54cn4khf1qar")))

(define-public crate-milenage-0.1.1 (c (n "milenage") (v "0.1.1") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 0)) (d (n "aes-soft") (r "^0.3.3") (d #t) (k 0)) (d (n "block-modes") (r "^0.3.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 0)))) (h "02ks4r8hz670jrhcx56r9j9xp2m5dwg3kp2x6yh2pvhb17v275lf")))

(define-public crate-milenage-0.1.2 (c (n "milenage") (v "0.1.2") (d (list (d (n "aes") (r "^0.6.0") (d #t) (k 0)) (d (n "aes-soft") (r "^0.6.4") (d #t) (k 0)) (d (n "block-modes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)))) (h "095f0as89d55f3nj4804jraczafgrsn2b0bcx8pvf43bvky1g9br")))

(define-public crate-milenage-0.1.3 (c (n "milenage") (v "0.1.3") (d (list (d (n "aes") (r "^0.6.0") (d #t) (k 0)) (d (n "aes-soft") (r "^0.6.4") (d #t) (k 0)) (d (n "block-modes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1isdcp3mf2ypd96gxfv8ra5w1nnwy8dcckf921zlnpmyfpynvv7r")))

(define-public crate-milenage-0.1.4 (c (n "milenage") (v "0.1.4") (d (list (d (n "aes") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "aes-soft") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "block-modes") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0p9znfsg3da40afmz5rrgnilnl00yranam3rh4f2gr1wzj6xvvgw") (f (quote (("default" "aes")))) (s 2) (e (quote (("openssl" "dep:openssl") ("aes" "dep:aes" "dep:aes-soft" "dep:block-modes"))))))

