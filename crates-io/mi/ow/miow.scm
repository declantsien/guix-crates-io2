(define-module (crates-io mi ow miow) #:use-module (crates-io))

(define-public crate-miow-0.1.0 (c (n "miow") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.1") (d #t) (k 0)))) (h "0sdc3n6v0raikh3csxbcbn57533mggnaan0ccimy1bbid79gbxwj") (f (quote (("unstable"))))))

(define-public crate-miow-0.1.1 (c (n "miow") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "07d6j2fzlwgfxj72z4a3w7nm97y74fa7x4bnh91pds74r752mp8r") (f (quote (("unstable"))))))

(define-public crate-miow-0.1.2 (c (n "miow") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "16p8k6kl3i4zk89xci18mk3sbrq3jza6fvan4idgd3sbscrxd4sf")))

(define-public crate-miow-0.1.3 (c (n "miow") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "16jvfjsp6fr4mbd2sw5hcdmi4dsa0m0aa45gjz78mb1h4mwcdgym")))

(define-public crate-miow-0.1.4 (c (n "miow") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "0hk9pjd4d0kaz5w5x754a88szd6nbznavi4039280f52xa841df6")))

(define-public crate-miow-0.1.5 (c (n "miow") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "1lcwlmkv5pkkyls8vz32allzhzw23sc7hqym8p6hmxmjyrfhqs9y")))

(define-public crate-miow-0.2.0 (c (n "miow") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "0ycm0illzwnizj9ayz4ma868jahadgcvhi7r1k0h4wxrbrhd4y1s") (y #t)))

(define-public crate-miow-0.2.1 (c (n "miow") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.5") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "06g9b8sqlh5gxakwqq4rrib07afwanfnxgxajrldwcgk3hxjy7wc") (y #t)))

(define-public crate-miow-0.3.0 (c (n "miow") (v "0.3.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.2") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "1m7lsad12h34lfc4pfcnjmw87cjgqp45a488chsjq12njzsd9bs3") (y #t)))

(define-public crate-miow-0.3.1 (c (n "miow") (v "0.3.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.2") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "0zcq7iznk2nzzkrdawnjqny8z26n42mdyy6g7pspri5kh8gwj94j") (y #t)))

(define-public crate-miow-0.3.2 (c (n "miow") (v "0.3.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "0wg4b4637j9gfdzxkk87y0r41ki6yv9iay5pshizaxvpm6gw6gn5") (y #t)))

(define-public crate-miow-0.3.3 (c (n "miow") (v "0.3.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "synchapi" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "09ljvx6wg30f2xlv7b7hhpkw7k312n3hjgmrbhwzhz9x03ra0sir") (y #t)))

(define-public crate-miow-0.3.4 (c (n "miow") (v "0.3.4") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "synchapi" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "1iy142jh045gsz8a3cd3lavmzw1sbh00gv8ppmd9lqxja4fxvpr2") (y #t)))

(define-public crate-miow-0.3.5 (c (n "miow") (v "0.3.5") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "synchapi" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "17lpb2754vg6vflk2vgka4kz2p4gkbsgnb815bb3ckaxg6wqzf07") (y #t)))

(define-public crate-miow-0.3.6 (c (n "miow") (v "0.3.6") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "socket2") (r "^0.3.16") (d #t) (k 0)) (d (n "winapi") (r "^0.3.3") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "synchapi" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "15sqdhh29dqgw5xh59clwv6scbsbvdkbmdc16hbfvyq7b2sw2css")))

(define-public crate-miow-0.2.2 (c (n "miow") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "net2") (r "^0.2.36") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (k 0)))) (h "0kcl8rnv0bhiarcdakik670w8fnxzlxhi1ys7152sck68510in7b")))

(define-public crate-miow-0.3.7 (c (n "miow") (v "0.3.7") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "socket2") (r "^0.4.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.3") (f (quote ("std" "fileapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "ntdef" "synchapi" "winerror" "winsock2" "ws2def" "ws2ipdef"))) (d #t) (k 0)))) (h "08afp2xfpxmdw003111lxz6g9jgbj4zi2fpldvv7da6d4nqcbwdr")))

(define-public crate-miow-0.3.8 (c (n "miow") (v "0.3.8") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "socket2") (r "^0.4.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.28.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_NetworkManagement_IpHelper" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "1i2cj5ngdls2i2631pslgrfnph6gh0qs2v1zpmahvmmrslj9b6wc") (y #t)))

(define-public crate-miow-0.4.0 (c (n "miow") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "socket2") (r "^0.4.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.28.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_NetworkManagement_IpHelper" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "03p0dgbahlysgxk0sihhf09k5h13r9aam3d6rfivdbxkj9vpydx7")))

(define-public crate-miow-0.5.0 (c (n "miow") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "socket2") (r "^0.4.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (k 0)))) (h "08qi8xm2zf8dqacdbnrp19aqk2xiwmw75n1mpq43rqsmysibrzsj")))

(define-public crate-miow-0.6.0 (c (n "miow") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "socket2") (r "^0.5.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading"))) (d #t) (k 0)))) (h "0i307jyhxnhgzj148cdb9zq59rhlhr1b65g142g9z9r01d1pd7rm")))

