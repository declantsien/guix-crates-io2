(define-module (crates-io mi ld mildew) #:use-module (crates-io))

(define-public crate-mildew-0.1.0 (c (n "mildew") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)))) (h "1fx9ilylqwy9brw2fmlgw27yyp3lnip6lyylnchnmk7zg7dm87m7")))

(define-public crate-mildew-0.1.1 (c (n "mildew") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1wb4mp4kdz9sv2n50zzj9gs3p029rmy2lziv9qbml7ayjjnfkvd3")))

(define-public crate-mildew-0.1.2 (c (n "mildew") (v "0.1.2") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0r0d8narsm3by6aspndfjmf8f9q9v6mrwh5ajpimx8r2pmvjgi6w")))

