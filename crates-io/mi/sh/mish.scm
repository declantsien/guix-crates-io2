(define-module (crates-io mi sh mish) #:use-module (crates-io))

(define-public crate-mish-0.1.0 (c (n "mish") (v "0.1.0") (h "1axr8d3aa1gbqlsa2f9lc9dzx3yjl3xl6frv08l41blv5bdjrx2w")))

(define-public crate-mish-0.1.1 (c (n "mish") (v "0.1.1") (h "127zv08wr4cpjcmz85rjgyrzs6nzbjr27a72jy2w54zdz3314gsj")))

(define-public crate-mish-0.1.2 (c (n "mish") (v "0.1.2") (h "002b6g8sz6kxjq86gqdrhwl8l7qwg4jdksw2m7337y6q5z6cwhnw")))

(define-public crate-mish-0.1.4 (c (n "mish") (v "0.1.4") (h "1zjph9wj1nf03fq96zdi2dlhk6hikns845cr3wn338p8ycvqmd1b")))

(define-public crate-mish-0.2.0 (c (n "mish") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)))) (h "19zq29jzlhyqy3l56l2n2sjmkr331kmfkc6ik4b2x2mycjj6hwyp")))

(define-public crate-mish-0.2.1 (c (n "mish") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0ycab6mskdbjwvyfdqc3g4vkwv88dpgdjy8aj59rigcnizx140x6")))

