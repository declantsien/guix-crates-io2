(define-module (crates-io mi ca mica) #:use-module (crates-io))

(define-public crate-mica-0.1.0 (c (n "mica") (v "0.1.0") (d (list (d (n "mica-hl") (r "^0.1.0") (d #t) (k 0)) (d (n "mica-std") (r "^0.1.0") (d #t) (k 0)))) (h "0q77d14q25kmdxq4dspvrdawi1dxxpwjr136as2i50divgshhjql")))

(define-public crate-mica-0.2.0 (c (n "mica") (v "0.2.0") (d (list (d (n "mica-hl") (r "^0.2.0") (d #t) (k 0)) (d (n "mica-std") (r "^0.2.0") (d #t) (k 0)))) (h "17vpifbb0i95hp4hi9cgjscv8ayjrd0lx3zbyybs6xwzqiwm370x") (f (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.3.0 (c (n "mica") (v "0.3.0") (d (list (d (n "mica-hl") (r "^0.3.0") (d #t) (k 0)) (d (n "mica-std") (r "^0.3.0") (d #t) (k 0)))) (h "0y8k3pljvscms4hsy6xyzlj7wg9fm47v7lbbnybykdy6nn0hwhnb") (f (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.4.0 (c (n "mica") (v "0.4.0") (d (list (d (n "mica-hl") (r "^0.4.0") (d #t) (k 0)) (d (n "mica-std") (r "^0.4.0") (d #t) (k 0)))) (h "1frwf5xd8l7xgq561psgyj3a16x281jw6spx1ggg65v4qxr5ss20") (f (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.5.0 (c (n "mica") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 2)) (d (n "mica-hl") (r "^0.5.0") (d #t) (k 0)) (d (n "mica-std") (r "^0.5.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "128k7bnfz133csdn5rkgwli6k9sghdgrf6ls2bgchb7csg5kdh7j") (f (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.6.0 (c (n "mica") (v "0.6.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("raw"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "0lakbz91dgs1lc7ay548frwlq0pqf7a8firvi63g9j656rpw10mv") (f (quote (("default"))))))

(define-public crate-mica-0.7.0 (c (n "mica") (v "0.7.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.1") (f (quote ("raw"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "18m15nnl7mhnl1dbsm7rhzljrappay6qcbk2qln5vj920030gxwd") (f (quote (("default"))))))

(define-public crate-mica-0.7.1 (c (n "mica") (v "0.7.1") (d (list (d (n "hashbrown") (r "^0.12.1") (f (quote ("raw"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 2)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "1hda600x23dhamy7zjj0kznw7anim54fa3707n93dh2nfp9mql6l") (f (quote (("default"))))))

