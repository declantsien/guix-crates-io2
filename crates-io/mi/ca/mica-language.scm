(define-module (crates-io mi ca mica-language) #:use-module (crates-io))

(define-public crate-mica-language-0.1.0 (c (n "mica-language") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.8.0") (d #t) (k 0)))) (h "05bs82cah86s8ggvd1dhwc1xsgkg3i2yqfpgxfmfbzr4j497l2nb")))

(define-public crate-mica-language-0.2.0 (c (n "mica-language") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.8.0") (d #t) (k 0)))) (h "0ahaambrqv068748ld9ipsj14sx759w3iaczkyh2f631867p3jzi") (f (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("default"))))))

(define-public crate-mica-language-0.3.0 (c (n "mica-language") (v "0.3.0") (h "1nmb9qdnjbfwhvvzl8v8lbdw8wvfa89pli5c2fwpdv88dnwnjxm2") (f (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("trace-vm-calls") ("trace-gc") ("default"))))))

(define-public crate-mica-language-0.4.0 (c (n "mica-language") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.12.1") (f (quote ("raw"))) (d #t) (k 0)))) (h "09gmf9qz870626jd955kmfh2fkmp3in9fglk4xn08kgc7p5805vm") (f (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("trace-vm-calls") ("trace-gc") ("default"))))))

(define-public crate-mica-language-0.5.0 (c (n "mica-language") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.12.1") (f (quote ("raw"))) (d #t) (k 0)))) (h "1mcmjax2xxafvgbb9w3jvaphfbzfickiz53awjk4ypjyqj3c6jm9") (f (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("trace-vm-calls") ("trace-gc") ("default"))))))

