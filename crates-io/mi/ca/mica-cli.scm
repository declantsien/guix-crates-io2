(define-module (crates-io mi ca mica-cli) #:use-module (crates-io))

(define-public crate-mica-cli-0.1.0 (c (n "mica-cli") (v "0.1.0") (d (list (d (n "mica") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0qbdlpsa72m5bfqd5d2gszd2gfk86rdf0b524rx6rh909l2xyc40")))

(define-public crate-mica-cli-0.2.0 (c (n "mica-cli") (v "0.2.0") (d (list (d (n "mica") (r "^0.2.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "19n60arhkcvmvr7fjx3sb9crzijk1724xym6wh0zapiy9z2f0ric")))

(define-public crate-mica-cli-0.3.0 (c (n "mica-cli") (v "0.3.0") (d (list (d (n "mica") (r "^0.3.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "03yvzq854n1gb5r34j890lq936764bwwb9ljqm3x8296v70mycs2")))

(define-public crate-mica-cli-0.4.0 (c (n "mica-cli") (v "0.4.0") (d (list (d (n "mica") (r "^0.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "00y7d229d3xcxx0814bm85cj383nlfplnjxh5qi47nkv6kxfc9h1")))

(define-public crate-mica-cli-0.5.0 (c (n "mica-cli") (v "0.5.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mica") (r "^0.5.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "01lzg2xx0l002gk44jqk9x790dwdn8w15j2adwg0s6xw793awna6")))

(define-public crate-mica-cli-0.6.0 (c (n "mica-cli") (v "0.6.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mica") (r "^0.6.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1awdf8h2x7hdl6l56jqfvgsji1c5r9c8yscvswzrnmx2fvx7kzr8")))

(define-public crate-mica-cli-0.7.0 (c (n "mica-cli") (v "0.7.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mica") (r "^0.7.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0ndhwzyxz63r9p1kqn3lqmhpm8x9z2865ikl7qvp7qzvk4zxx1vi")))

(define-public crate-mica-cli-0.7.1 (c (n "mica-cli") (v "0.7.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mica") (r "^0.7.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "06hr3nkary5gcxx1fli9wl23cqbyzypmdnm0s3sjajakvi51781f")))

