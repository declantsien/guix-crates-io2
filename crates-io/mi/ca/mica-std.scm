(define-module (crates-io mi ca mica-std) #:use-module (crates-io))

(define-public crate-mica-std-0.1.0 (c (n "mica-std") (v "0.1.0") (d (list (d (n "mica-hl") (r "^0.1.0") (d #t) (k 0)))) (h "0zg04wymz5fzasgp508v8j035w80b01zzi8p5l4famngb2j4wal6")))

(define-public crate-mica-std-0.2.0 (c (n "mica-std") (v "0.2.0") (d (list (d (n "mica-hl") (r "^0.2.0") (d #t) (k 0)))) (h "1dddlf1sysl6sqg7fa8riazxixks63xj8wfhgajr4m6m0gifv79y") (f (quote (("io"))))))

(define-public crate-mica-std-0.3.0 (c (n "mica-std") (v "0.3.0") (d (list (d (n "mica-hl") (r "^0.3.0") (d #t) (k 0)))) (h "0y48nfkck82f9775cj7ca5kykcfjzbqqvxxm8lr4w4nmasy5m8hv") (f (quote (("io"))))))

(define-public crate-mica-std-0.4.0 (c (n "mica-std") (v "0.4.0") (d (list (d (n "mica-hl") (r "^0.4.0") (d #t) (k 0)))) (h "06crpm3zi2hdzbaghggffv780lxa1vn493zhzhwrl40i7wsckcic") (f (quote (("io"))))))

(define-public crate-mica-std-0.5.0 (c (n "mica-std") (v "0.5.0") (d (list (d (n "mica-hl") (r "^0.5.0") (d #t) (k 0)))) (h "10hy9n8snr5gyw4k752cfgmad3r8mcg7irvqk3syd0fprsp8izpn") (f (quote (("io"))))))

