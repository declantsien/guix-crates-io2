(define-module (crates-io mi ca mica-hl) #:use-module (crates-io))

(define-public crate-mica-hl-0.1.0 (c (n "mica-hl") (v "0.1.0") (d (list (d (n "mica-language") (r "^0.1.0") (d #t) (k 0)))) (h "0l9nsnmy75v7405whwxs3492ff9k6lbnpkja6k7xvm2gdjfnd170")))

(define-public crate-mica-hl-0.2.0 (c (n "mica-hl") (v "0.2.0") (d (list (d (n "mica-language") (r "^0.2.0") (d #t) (k 0)))) (h "1clr61k61kl6wggrd1i5mwd7l8xfbrc7kfx1h69c9rxgakmlsrls")))

(define-public crate-mica-hl-0.3.0 (c (n "mica-hl") (v "0.3.0") (d (list (d (n "mica-language") (r "^0.3.0") (d #t) (k 0)))) (h "0w1xqmigk27caida8p0zdr4xajd2qahv4ssn7n33yyqjddxxrlrl")))

(define-public crate-mica-hl-0.4.0 (c (n "mica-hl") (v "0.4.0") (d (list (d (n "mica-language") (r "^0.4.0") (d #t) (k 0)))) (h "07i8i03h7n8wzw6xhm4xd739n2hnp2clnf8a2p9n7x8n3qlyfvmw")))

(define-public crate-mica-hl-0.5.0 (c (n "mica-hl") (v "0.5.0") (d (list (d (n "mica-language") (r "^0.5.0") (d #t) (k 0)))) (h "14kqgcv52r8m2s9sp6n0hb4q1cpn0aj19mwj897z43iniw7bbj89")))

