(define-module (crates-io mi da midas_python) #:use-module (crates-io))

(define-public crate-midas_python-0.1.0 (c (n "midas_python") (v "0.1.0") (d (list (d (n "cpython") (r "^0.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "midas_rs") (r "^0.1") (d #t) (k 0)))) (h "1g8v9jwdwblfxbxxm7fkyipzrprid3izlqnmv6cj93x380xzyih8")))

(define-public crate-midas_python-0.1.1 (c (n "midas_python") (v "0.1.1") (d (list (d (n "cpython") (r "^0.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "midas_rs") (r "^0.1") (d #t) (k 0)))) (h "1phqmgp0c52zrvb2rfxqdj50blqaik8swvi44m59946nyg9hqxl6")))

(define-public crate-midas_python-0.2.0 (c (n "midas_python") (v "0.2.0") (d (list (d (n "cpython") (r "^0.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "midas_rs") (r "^0.2") (d #t) (k 0)))) (h "0gfcvx4sk6im6gdf0nvqhmlf6167qxwqbs430c2qvpidyjncapyj")))

(define-public crate-midas_python-0.2.1 (c (n "midas_python") (v "0.2.1") (d (list (d (n "cpython") (r "^0.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "midas_rs") (r "^0.2") (d #t) (k 0)))) (h "1bxv296k002im268zrykcxa89r6l7hzsvrhshp5ay3d1wx58j2hf")))

