(define-module (crates-io mi da midas_rs) #:use-module (crates-io))

(define-public crate-midas_rs-0.1.0 (c (n "midas_rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "134qzbhp7j2lqmmw3c6l99r233w4qknfkvxb61lgdacx2xk9q2w3")))

(define-public crate-midas_rs-0.1.1 (c (n "midas_rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "12kdfvgxhqz5y09pmn150r2bnfykzsq1xcrr0d8rg685dg1gnv4i")))

(define-public crate-midas_rs-0.1.2 (c (n "midas_rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "0mq6l1gr2bx3jrvj93y773fzpzdkhfigajz3r27d503mi6nmh7xk")))

(define-public crate-midas_rs-0.1.3 (c (n "midas_rs") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "1h1dwpc07vj7gzp65wmmmp1s17zpyz5nv0x4m6r55ijv1zc45yk4")))

(define-public crate-midas_rs-0.2.0 (c (n "midas_rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "12pcmcjimms4i0110c066i8nx1xw0009zn84mdyd7swbggx045jp")))

(define-public crate-midas_rs-0.2.1 (c (n "midas_rs") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (k 0)))) (h "1bij1crkswighl44ffx8jskvg6axbf4kl05q46vb5fgp7jfvrbhn")))

