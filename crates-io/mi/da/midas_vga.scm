(define-module (crates-io mi da midas_vga) #:use-module (crates-io))

(define-public crate-midas_vga-0.1.0 (c (n "midas_vga") (v "0.1.0") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "082dqna012zays1lr978lj4pfa5gh6vx1m415y6i5wfcifmg9qwg")))

(define-public crate-midas_vga-0.1.1 (c (n "midas_vga") (v "0.1.1") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "014fnm38lva1cjhw8p10ssj1whigx9c9arxhzlqja82vfjwlnhxd")))

(define-public crate-midas_vga-0.1.2 (c (n "midas_vga") (v "0.1.2") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "18fmxzwp5931qm8iyzx20b0rw26sd3v5kzk3157zc3i6dc3c2q6p")))

(define-public crate-midas_vga-0.1.3 (c (n "midas_vga") (v "0.1.3") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "0mvc1pzxzac1f9n0y10d7cl61ijwhbhfnhqlxhii0407k6zn074c")))

(define-public crate-midas_vga-0.1.4 (c (n "midas_vga") (v "0.1.4") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "07j6963nf0v4spvlnsks4jip9jk083sv9y04hqvzy89n852q5hil")))

(define-public crate-midas_vga-0.1.5 (c (n "midas_vga") (v "0.1.5") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "1gb41i1izr8s734hjpl743r8j22m8xhv3pz930ngy6nycj8xwq48")))

(define-public crate-midas_vga-0.1.6 (c (n "midas_vga") (v "0.1.6") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "19lbhfly5z9q7v2r756lclxr9wihs4l8m1dw8kb6ni8gx1zy9bg2")))

(define-public crate-midas_vga-0.1.7 (c (n "midas_vga") (v "0.1.7") (d (list (d (n "vga") (r "^0.2.7") (d #t) (k 0)))) (h "1g4dllj1xxivhkl280ak9qgbwyz8jkh2kk7cqa44nkg6mcsgwf6q")))

