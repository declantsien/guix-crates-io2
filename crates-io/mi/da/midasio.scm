(define-module (crates-io mi da midasio) #:use-module (crates-io))

(define-public crate-midasio-0.1.0 (c (n "midasio") (v "0.1.0") (h "1mxlarm1b3qqd69kjximk0qs57mkm4j7l8ajgwb8k2c9f9f07fvq")))

(define-public crate-midasio-0.1.1 (c (n "midasio") (v "0.1.1") (h "0c92prnza11ahwv15r5cfjcdzphp3jp856kivpa6vx6h6gh6df8w")))

(define-public crate-midasio-0.2.0 (c (n "midasio") (v "0.2.0") (h "0zx6j4jsd35z4dxy65jsrihzb6dpvh58014lgqc1b0r68116ryr6")))

(define-public crate-midasio-0.3.0 (c (n "midasio") (v "0.3.0") (h "0w6lwllg02gnk98z7fmvm9md3py2aidgllb9lsdfj74mckgf9wv6")))

(define-public crate-midasio-0.4.0 (c (n "midasio") (v "0.4.0") (h "0mbz59jv34403h9wrsiz2mb560ib8k61cc8d6mymn0xyjd026s85")))

(define-public crate-midasio-0.4.1 (c (n "midasio") (v "0.4.1") (h "045n0rh8zp46bjyyzl4xc10zkn2h0d11vw40nkbhrq8h7q0xbh2y")))

(define-public crate-midasio-0.5.0 (c (n "midasio") (v "0.5.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.30") (d #t) (k 0)))) (h "0in2qkz0glhjyln6mgijmkgy4sf800rgzzkl6mcp1svwkgkgg1qx")))

(define-public crate-midasio-0.5.1 (c (n "midasio") (v "0.5.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.30") (d #t) (k 0)))) (h "0v5xcbb1ksnxfw3a93fk5m76wv7p1dcpmcca6p0lrkaqa39zqa87")))

(define-public crate-midasio-0.5.2 (c (n "midasio") (v "0.5.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.30") (d #t) (k 0)))) (h "0k79anxpfisn13l3n5ac69rqh0mzdzk4c8jxk3zmgg7agiwvl4hw")))

(define-public crate-midasio-0.5.3 (c (n "midasio") (v "0.5.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.5.30") (d #t) (k 0)))) (h "1yxrbpj1yl6zzfsl6z267bzlhkvcxz2wj0yh6b46scldnv9w6l9j")))

(define-public crate-midasio-0.5.4 (c (n "midasio") (v "0.5.4") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "winnow") (r "^0.6.1") (d #t) (k 0)))) (h "1cqp1hjglnjz8fiscxpv1wlww5hvx64qvnavrpxcmym4jffs0l9z")))

(define-public crate-midasio-0.6.0 (c (n "midasio") (v "0.6.0") (d (list (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "winnow") (r "^0.6.1") (d #t) (k 0)))) (h "12hp4vsypwaj31il2dd4cb3q24nkx4csw6fjklfxc4391zzkxqxh")))

