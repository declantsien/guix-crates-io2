(define-module (crates-io mi da midas_cli) #:use-module (crates-io))

(define-public crate-midas_cli-0.1.0 (c (n "midas_cli") (v "0.1.0") (d (list (d (n "midas_rs") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "08aacwcq4pzvglglfvz2xk99ywnj4w9yq2565hyfrq39i93bj2bf")))

(define-public crate-midas_cli-0.1.1 (c (n "midas_cli") (v "0.1.1") (d (list (d (n "midas_rs") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1h8cxydmnajvsnjwz1jxp6fkzqqabyav6sglrb015bn209x71zlx")))

(define-public crate-midas_cli-0.2.0 (c (n "midas_cli") (v "0.2.0") (d (list (d (n "midas_rs") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0zd6fpc5ic16r3050m76nylhc17sd6azvda2jx6p1l7z7fgmziz4")))

(define-public crate-midas_cli-0.2.1 (c (n "midas_cli") (v "0.2.1") (d (list (d (n "midas_rs") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zz3r2mr7cdz0id0p2lbphg6c5p4sam60vrr225k2985gcw79vvh")))

