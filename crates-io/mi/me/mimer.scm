(define-module (crates-io mi me mimer) #:use-module (crates-io))

(define-public crate-mimer-0.1.0 (c (n "mimer") (v "0.1.0") (d (list (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0j7gij6ifs516ak815cpj72hmsq8dsf6rjxq2j40pqa15qcww7n7")))

