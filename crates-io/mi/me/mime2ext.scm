(define-module (crates-io mi me mime2ext) #:use-module (crates-io))

(define-public crate-mime2ext-0.1.0 (c (n "mime2ext") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1igij1c2g02kza8im7whmm8n99zwdcjml2jmj4zjzvmg1d8frwk8")))

(define-public crate-mime2ext-0.1.1 (c (n "mime2ext") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0sqx3rvz2pyc2352ic072ra3q1cd98lkx248xnviwa0x2jj97vhy")))

(define-public crate-mime2ext-0.1.2 (c (n "mime2ext") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0bigizlpzx68ccni69qgwdgwm1887c048ak0z097wsaq29hlg2dq")))

(define-public crate-mime2ext-0.1.3 (c (n "mime2ext") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "10z22j6c68p6gw5ib8fymbva7mx564w855l0q39ximf1nyh3gcyq")))

(define-public crate-mime2ext-0.1.4 (c (n "mime2ext") (v "0.1.4") (h "1qrihf8kx9dlrr4j7p97qhpha41g8axxj5q29sfz6z34s68vyyw1")))

(define-public crate-mime2ext-0.1.49 (c (n "mime2ext") (v "0.1.49") (h "0mqwrdincrhv3rn3iai6m8wzshszf5g1qa8cxcrma2lr036y0s33")))

(define-public crate-mime2ext-0.1.50 (c (n "mime2ext") (v "0.1.50") (h "0hc1pdlh5f6r4wkwmng8bxm1lmpsn6f8wc59fdi2qmcbkvr5f8k8")))

(define-public crate-mime2ext-0.1.51 (c (n "mime2ext") (v "0.1.51") (h "1gcvywzszlpmfkizqgld20qjrg5afaj6m0ksshaa1qx08mbggxrq")))

(define-public crate-mime2ext-0.1.52 (c (n "mime2ext") (v "0.1.52") (h "0w2cb2nmfnj7npkn1kvy1pcyknxdh7676mwqn5j0xm7bd585ma51")))

