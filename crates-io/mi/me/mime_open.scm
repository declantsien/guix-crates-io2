(define-module (crates-io mi me mime_open) #:use-module (crates-io))

(define-public crate-mime_open-0.1.0 (c (n "mime_open") (v "0.1.0") (h "0s1vhn6sazjr3725km215raca7dbc6hlj17nsnjfq666z44afida")))

(define-public crate-mime_open-0.2.0 (c (n "mime_open") (v "0.2.0") (h "0szgbcgpr27s6d2bg468z59bcnxi8nk12123xzynw9ns0d8fcsq2")))

