(define-module (crates-io mi me mimeograph_router_helpers) #:use-module (crates-io))

(define-public crate-mimeograph_router_helpers-0.1.0 (c (n "mimeograph_router_helpers") (v "0.1.0") (d (list (d (n "mimeograph_request") (r "^0.1.0") (d #t) (k 0)))) (h "1hiq77pwvh47dizx752i8mw6nyvqk2i4q4rs0063sh3w19z7k7mi")))

(define-public crate-mimeograph_router_helpers-0.2.0 (c (n "mimeograph_router_helpers") (v "0.2.0") (d (list (d (n "mimeograph_request") (r "^0.1.0") (d #t) (k 0)))) (h "0mrkjn2i907dzx262byb7pxd0xfjk1xm9cp9zn9pm8x01d3cfn8l")))

(define-public crate-mimeograph_router_helpers-0.3.0 (c (n "mimeograph_router_helpers") (v "0.3.0") (d (list (d (n "mimeograph_request") (r "^0.1.0") (d #t) (k 0)))) (h "0h5jwd3bhcg908gnk8lf8m2xw5dgi1cfkhrqy16vm64is4ax16hf")))

(define-public crate-mimeograph_router_helpers-0.4.0 (c (n "mimeograph_router_helpers") (v "0.4.0") (d (list (d (n "mimeograph_request") (r "^0.1.0") (d #t) (k 0)))) (h "0wy0rr2ybqla4cvl05wy5xnzvlpw3w3c4h25m6j2jsq0sqr3p637")))

(define-public crate-mimeograph_router_helpers-0.4.1 (c (n "mimeograph_router_helpers") (v "0.4.1") (d (list (d (n "mimeograph_request") (r "^0.1.0") (d #t) (k 0)))) (h "0nan2m3lzjq1sffcwzc7zjnkwvqysi2szdv9bjy3ai7y816373xz")))

(define-public crate-mimeograph_router_helpers-0.5.0 (c (n "mimeograph_router_helpers") (v "0.5.0") (d (list (d (n "mimeograph_request") (r "^0.1.1") (d #t) (k 0)))) (h "0kg2k925jrld35jy9qrmjmgci54km9sbc21ay3v4nwm1phh9l40r")))

(define-public crate-mimeograph_router_helpers-0.5.1 (c (n "mimeograph_router_helpers") (v "0.5.1") (d (list (d (n "mimeograph_request") (r "^0.1.1") (d #t) (k 0)))) (h "12fgv65ahi23rrmfjwxr1iimp9aknmgwdb0fkdpj3jh0f7n05f8q")))

