(define-module (crates-io mi me mime_typed) #:use-module (crates-io))

(define-public crate-mime_typed-0.1.0 (c (n "mime_typed") (v "0.1.0") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "05xb3vpk6yx0l00ppvpsawmdsm76k5470cf1jjbk2ay54372j0z0") (f (quote (("mime_support" "mime")))) (r "1.56.1")))

(define-public crate-mime_typed-0.1.1 (c (n "mime_typed") (v "0.1.1") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "18c8vy510j3dnf3nl7c44qvb2z7bvghmxg0g96b24bx4zqj7c8i7") (f (quote (("mime_support" "mime")))) (r "1.56.1")))

(define-public crate-mime_typed-0.1.2 (c (n "mime_typed") (v "0.1.2") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "1lvhi8dpyvdppa50hphnp2gw69yy1vl3cam5pfln8fcqpdchx14d") (f (quote (("mime_support" "mime")))) (r "1.56.1")))

(define-public crate-mime_typed-0.1.3 (c (n "mime_typed") (v "0.1.3") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "0qlnsznixvlsxhcv7i0kcw47k12kjxr8a6fqi1bs95hjri9q2pki") (f (quote (("mime_support" "mime") ("evcxr_support")))) (r "1.56.1")))

(define-public crate-mime_typed-0.1.4 (c (n "mime_typed") (v "0.1.4") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "0isdn9xgci7im0jhkn47jmkxwy7rdggk3lc7238n13dycphbfmly") (f (quote (("mime_support" "mime") ("evcxr_support")))) (r "1.56.1")))

(define-public crate-mime_typed-0.1.5 (c (n "mime_typed") (v "0.1.5") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "0115jf0cn3nyyx56w2cmw10zwlz558szh04iw3a8rfjdhbsim5qf") (f (quote (("mime_support" "mime") ("evcxr_support")))) (r "1.56.1")))

(define-public crate-mime_typed-0.1.6 (c (n "mime_typed") (v "0.1.6") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "000dk10p1k1r1j0csknd6v7jv9dmn0cpy0d8jkw2p6ibdd9vgafn") (f (quote (("mime_support" "mime") ("evcxr_support")))) (r "1.56.1")))

(define-public crate-mime_typed-0.1.7 (c (n "mime_typed") (v "0.1.7") (d (list (d (n "mime") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "0vl2x1hr4zrx5y2qzc6wbihry4mcihccpvj53d0zzwwl9gwvk5h4") (f (quote (("mime_support" "mime") ("evcxr_support")))) (r "1.56.1")))

