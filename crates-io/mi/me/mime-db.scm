(define-module (crates-io mi me mime-db) #:use-module (crates-io))

(define-public crate-mime-db-0.1.0 (c (n "mime-db") (v "0.1.0") (d (list (d (n "async-std") (r "^1.2") (f (quote ("attributes"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "surf") (r "^1.0") (d #t) (k 1)))) (h "08fla7dhxpv8x3vj0xv8gfb7bslcxylfl1qgglbw63bp3paada9x")))

(define-public crate-mime-db-0.1.1 (c (n "mime-db") (v "0.1.1") (d (list (d (n "async-std") (r "^1.2") (f (quote ("attributes"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "surf") (r "^1.0") (d #t) (k 1)))) (h "1xpykhj58cb4s823vgj7v81f28cypz3yipm3v8yl2ya977k8kn63")))

(define-public crate-mime-db-0.1.2 (c (n "mime-db") (v "0.1.2") (d (list (d (n "async-std") (r "^1.2") (f (quote ("attributes"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "surf") (r "^1.0") (d #t) (k 1)))) (h "11rpzhivjhssgjilxmk1xmkd6x6rmy978n8mphv22jdkpg49jsv6")))

(define-public crate-mime-db-0.1.3 (c (n "mime-db") (v "0.1.3") (d (list (d (n "async-std") (r "^1.2") (f (quote ("attributes"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "surf") (r "^1.0") (d #t) (k 1)))) (h "1rxw7dbhyayvskknfbqpjfsdiba6b2i090c7n7c8arnd46d7f5d5")))

(define-public crate-mime-db-0.1.4 (c (n "mime-db") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 1)))) (h "0k57pg7h5hj2cyks5j2d732cp4jbgxfgy0igbqspqxxnkrzpvycr")))

(define-public crate-mime-db-0.1.5 (c (n "mime-db") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 1)))) (h "15hfhkcfr9wpcirq89yj3kj949yskr5cfxszvsszgp8y67qyrmgy")))

(define-public crate-mime-db-1.0.0 (c (n "mime-db") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 1)))) (h "14scyr2l2yp9ws4r0pckf33ds97llwc692273mlwkx9dijha2qz9")))

(define-public crate-mime-db-1.1.0 (c (n "mime-db") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 1)))) (h "1grnj06a5r37x3jjqcslfia0g44hk7di0hz66ddsisr76sz0vakn")))

(define-public crate-mime-db-1.2.0 (c (n "mime-db") (v "1.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 1)))) (h "05f3p4bh1qd0z0iwm4i8sp1xrb6d57kmvi0la6irky1dpjdbc974")))

(define-public crate-mime-db-1.3.0 (c (n "mime-db") (v "1.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 1)))) (h "074kc404d64hxk05w48vk5lp5ra8qmf8fvfing36q7gk4s51a6c0")))

(define-public crate-mime-db-1.4.0 (c (n "mime-db") (v "1.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 1)))) (h "1bf972w8aw2mq4a5ddfj207p2mhz21f7jnrjkfbx96yvh5s1nc3p")))

(define-public crate-mime-db-1.5.0 (c (n "mime-db") (v "1.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 1)))) (h "08kjlis6civg954m330w5hh8c60mxn6szzhgkyhzs1il80f5hf50")))

(define-public crate-mime-db-1.6.0 (c (n "mime-db") (v "1.6.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 1)))) (h "0zaynxc8dzs4f918lilyc9b8gmv14am6kagfw5rzhh8cqdp82z3d")))

(define-public crate-mime-db-1.7.0 (c (n "mime-db") (v "1.7.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 1)))) (h "061lxrliarc7l4zj32lnjsj1wxqf8vn08671yddywclapk262yhw")))

