(define-module (crates-io mi me mimeograph_crumble) #:use-module (crates-io))

(define-public crate-mimeograph_crumble-0.1.0 (c (n "mimeograph_crumble") (v "0.1.0") (h "0v6xkcrjj86gxg0xjz4pr23jlw2ak5kr0wg0p702vvqxbf4lzv89")))

(define-public crate-mimeograph_crumble-0.1.1 (c (n "mimeograph_crumble") (v "0.1.1") (h "0nwyy12wpi010228v39r9fakch2hv1mjjwxkrmjsdrp186dvmbzy")))

(define-public crate-mimeograph_crumble-0.1.2 (c (n "mimeograph_crumble") (v "0.1.2") (h "1cl008bf6fk3ckgwyy91s39qrayb4j8n4qkai61ryw4sm13xp78z") (f (quote (("std") ("default" "std"))))))

