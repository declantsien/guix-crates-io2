(define-module (crates-io mi me mime-detective) #:use-module (crates-io))

(define-public crate-mime-detective-0.1.2 (c (n "mime-detective") (v "0.1.2") (d (list (d (n "magic") (r "^0.12.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.5") (d #t) (k 0)) (d (n "rocket") (r "^0.3.5") (o #t) (d #t) (k 0)))) (h "08v12zxvqqnib8b3wgixvscd45hzcqxw66qiwzh6sqqkgipfqvnf") (f (quote (("rocket_data" "rocket"))))))

(define-public crate-mime-detective-0.2.0 (c (n "mime-detective") (v "0.2.0") (d (list (d (n "magic") (r "^0.12.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.5") (d #t) (k 0)))) (h "11fv0myz1ahcjyx521jcr2ky4gm3ng15hd98kw30qgl3zvjahvhk")))

(define-public crate-mime-detective-0.2.1 (c (n "mime-detective") (v "0.2.1") (d (list (d (n "magic") (r "^0.12.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.5") (d #t) (k 0)))) (h "049426yjklsxg81h3vc42pfqxl18fkch4spvn4xxh3aizfcms8nl")))

(define-public crate-mime-detective-0.2.2 (c (n "mime-detective") (v "0.2.2") (d (list (d (n "magic") (r "^0.12.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.13") (d #t) (k 0)))) (h "12bc89gbbi1p26xr5jmx9i7448lz958c3bg4a8sjlnyvdifnf4vw")))

(define-public crate-mime-detective-0.2.3 (c (n "mime-detective") (v "0.2.3") (d (list (d (n "magic") (r "^0.12.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)))) (h "0bxczj1gkrv92dfijaapp34411a6zvjf7b2q10sd7x7qw363safa")))

(define-public crate-mime-detective-1.0.0 (c (n "mime-detective") (v "1.0.0") (d (list (d (n "magic") (r "^0.12.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.14") (d #t) (k 0)))) (h "1sdlr4lc8272jfpsb8vkj2ji7y2wccl68lwzpn87qycn0a8la7v9")))

