(define-module (crates-io mi me mime_classifier) #:use-module (crates-io))

(define-public crate-mime_classifier-0.0.1 (c (n "mime_classifier") (v "0.0.1") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m5v7hdr4mabz68h3f6szk4w2r60q7j8hklapg7py54yr0scn5z8")))

