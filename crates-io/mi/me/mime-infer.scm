(define-module (crates-io mi me mime-infer) #:use-module (crates-io))

(define-public crate-mime-infer-2.0.5 (c (n "mime-infer") (v "2.0.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.7") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "0n9mgsh4k88zbm87q9mlqcidr4pj6igzlzdzgpkvzaf49wf1hnzn") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

(define-public crate-mime-infer-3.0.0 (c (n "mime-infer") (v "3.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.7") (d #t) (k 0)) (d (n "unicase") (r "^2.7.0") (d #t) (k 1)))) (h "0bpf5crl4zzk9j7r3881jga2jd8m33gp2d86rn5whas7vlcyvjli") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

