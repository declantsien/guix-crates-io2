(define-module (crates-io mi me mime_serde_shim) #:use-module (crates-io))

(define-public crate-mime_serde_shim-0.2.0 (c (n "mime_serde_shim") (v "0.2.0") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01r8y2mk0ayy8a5jhcjz2qznah7qgil5x8lqks561hqhq7zflry4")))

(define-public crate-mime_serde_shim-0.2.1 (c (n "mime_serde_shim") (v "0.2.1") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "142bhm0fsn5kwn5pghydwyyk8cghvrg3ccvqvwi3hmj8i53i1rm7")))

(define-public crate-mime_serde_shim-0.2.2 (c (n "mime_serde_shim") (v "0.2.2") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sjwbndzh2lc79gdg6d21ds312v8iikpzvswcymlmqrmn4rmp75b")))

