(define-module (crates-io mi me mimeograph_request) #:use-module (crates-io))

(define-public crate-mimeograph_request-0.1.0 (c (n "mimeograph_request") (v "0.1.0") (d (list (d (n "http") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1aa3iybr3vrccw0368wxrpm41prs2s318863k9dyngmxyc8za377") (f (quote (("with_http" "http") ("default"))))))

(define-public crate-mimeograph_request-0.1.1 (c (n "mimeograph_request") (v "0.1.1") (d (list (d (n "http") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "05zqx2n1na6b4sq5rxmlgwmxx6hjnlffn4rq768b1lm36ggjrs21") (f (quote (("with_http" "http") ("default"))))))

(define-public crate-mimeograph_request-0.1.2 (c (n "mimeograph_request") (v "0.1.2") (d (list (d (n "http") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1f9zf3xfcl8f567k5mbmfybyw1asgrx70b6yjabvgwbdi3cxv447") (f (quote (("with_http" "http") ("default"))))))

