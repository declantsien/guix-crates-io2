(define-module (crates-io mi me mimesniff) #:use-module (crates-io))

(define-public crate-mimesniff-0.1.0 (c (n "mimesniff") (v "0.1.0") (h "0dbjcjj4rk2sq3idlj321qmhb2avqld0d8j99g7nvmns96ra6b5n")))

(define-public crate-mimesniff-0.2.0 (c (n "mimesniff") (v "0.2.0") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)))) (h "18df4vx8vany0cq91b1g4kqdqx0xi4hh2r23hirm9glaa8gv4c6y")))

(define-public crate-mimesniff-0.2.1 (c (n "mimesniff") (v "0.2.1") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)))) (h "0igbp8ldpx2mjsapxnf4ngxiqyihksm5ljml0nkq6khx089qk4ng")))

(define-public crate-mimesniff-0.3.0 (c (n "mimesniff") (v "0.3.0") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)))) (h "1fzq9lqaksrh2inf9zrwgddnxq6q80y2brq5cik4rygadgnx61vv")))

