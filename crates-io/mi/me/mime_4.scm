(define-module (crates-io mi me mime_4) #:use-module (crates-io))

(define-public crate-mime_4-0.4.0-a.0 (c (n "mime_4") (v "0.4.0-a.0") (d (list (d (n "mime-macro") (r "^0.0.0") (o #t) (d #t) (k 0) (p "mime-macro-4")) (d (n "mime-parse") (r "^0.0.0") (d #t) (k 0) (p "mime-parse-4")) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "quoted-string") (r "^0.2.2") (d #t) (k 0)) (d (n "serde1") (r "^1") (o #t) (d #t) (k 0) (p "serde")))) (h "1jn1sh2dzaa9vg466qp2bhx20jlw6bav3kchfjn8nlk6blllbr0j") (f (quote (("macro" "mime-macro" "proc-macro-hack"))))))

