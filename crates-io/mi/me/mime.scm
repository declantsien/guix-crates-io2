(define-module (crates-io mi me mime) #:use-module (crates-io))

(define-public crate-mime-0.0.1 (c (n "mime") (v "0.0.1") (h "0m5d1bvvjjjqk8l7fvq6vxzzmvx7rlgbj89ir6kf0xkajx27wb0r")))

(define-public crate-mime-0.0.2 (c (n "mime") (v "0.0.2") (h "1sj2l78l31nyvycfqv1cqkxwjqcrwjdnxani1d2lzx2dwix4v72d")))

(define-public crate-mime-0.0.3 (c (n "mime") (v "0.0.3") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0ch59d7asmjz075xp8lbrp271aizn2wv385n1mz98v0svn4bdzmz")))

(define-public crate-mime-0.0.4 (c (n "mime") (v "0.0.4") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "14v5yca1yqbrsn9hry1hhwmls2iv3q6asb8xm2k8riyz2nqy77xz")))

(define-public crate-mime-0.0.5 (c (n "mime") (v "0.0.5") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0900n7qymcbx61mrm7kqd28m4wr2ll2i3n9ch2s99055m3lailjf")))

(define-public crate-mime-0.0.6 (c (n "mime") (v "0.0.6") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1c5klnp3j1rqhmgaq4iwnidz0qlfv5x49xnfkfbrpis34f0z0bl2")))

(define-public crate-mime-0.0.7 (c (n "mime") (v "0.0.7") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1l0h4mkawcj0pn5zqmgjk3mkx1lz1l6bwrjcj5c2b30w9ibx95lh")))

(define-public crate-mime-0.0.8 (c (n "mime") (v "0.0.8") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "07x4gn0qd91ss6j349z0mr1274wrpypmsn2crislk991qgj9v8x5")))

(define-public crate-mime-0.0.9 (c (n "mime") (v "0.0.9") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0mk40axkslz1rwalj4386088mprd96sj8jgdyqgm1d53i9hz15fq")))

(define-public crate-mime-0.0.10 (c (n "mime") (v "0.0.10") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1rmlk3r18ldcx9dyphkj6ksfnz1wjgiwascmh0d6p1hw9wxczphs")))

(define-public crate-mime-0.0.11 (c (n "mime") (v "0.0.11") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1qbzhs9pirfipd02bfw067x15s4pzflayipmzx1c20d63phmg9bm") (f (quote (("nightly"))))))

(define-public crate-mime-0.0.12 (c (n "mime") (v "0.0.12") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1lg9z63liw664b3q0llizgyygs43qdxbcwl27p7d28wn2ywxbbgk") (f (quote (("nightly"))))))

(define-public crate-mime-0.1.0 (c (n "mime") (v "0.1.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1zw4vzlv8zl9hdsi4aij5cidy4417crg5983ypb3sp2xm1jr7jw2") (f (quote (("nightly"))))))

(define-public crate-mime-0.1.1 (c (n "mime") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "1q01cr1vsx9l90i51awb9wpvgp9lf1cdfnd7pzaraw7llpc3avbd") (f (quote (("nightly"))))))

(define-public crate-mime-0.1.2 (c (n "mime") (v "0.1.2") (d (list (d (n "heapsize") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "0jz04mn1gxf4cfy4xiq1yfn01vvcjlfvzwhs7vl31hf6rw9wkq82") (f (quote (("nightly") ("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-mime-0.1.3 (c (n "mime") (v "0.1.3") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 2)))) (h "11x71aa43rqxzkg4kra32b2ijfxff2sv8h1a36id9w8vj16jy37c") (f (quote (("nightly") ("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-mime-0.2.0 (c (n "mime") (v "0.2.0") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 2)))) (h "1lkvfb6vv7q4kcfvarivlwn91wxbsrh2idmsyprljz7rgdcc4k57") (f (quote (("nightly") ("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-mime-0.2.1 (c (n "mime") (v "0.2.1") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "heapsize_plugin") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 2)))) (h "0r2zz3749na98p1bl4lk0wy3fj49gbrg1966gqigi78cf2fag4yg") (f (quote (("nightly") ("heap_size" "heapsize" "heapsize_plugin"))))))

(define-public crate-mime-0.2.2 (c (n "mime") (v "0.2.2") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.7, < 0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">= 0.7, < 0.9") (d #t) (k 2)))) (h "0rjfz6vny8dq3svxjxixhssxx0q8lmrrnl9whgkwdpc7sx5kmjdm") (f (quote (("nightly") ("heap_size" "heapsize"))))))

(define-public crate-mime-0.2.3 (c (n "mime") (v "0.2.3") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.7, < 0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">= 0.7, < 0.9") (d #t) (k 2)))) (h "0zkklqxyxya6i82xz7n27g4p010yxzj2jlgrwlgd0hik28wg052m") (f (quote (("nightly") ("heap_size" "heapsize"))))))

(define-public crate-mime-0.2.4 (c (n "mime") (v "0.2.4") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.7, < 0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">= 0.7, < 0.9") (d #t) (k 2)))) (h "1j1n5axqk9dwwxrdhgf29ibdrhy4fvl4qlbln5bfsdk3vjf8hscx") (f (quote (("nightly") ("heap_size" "heapsize"))))))

(define-public crate-mime-0.2.5 (c (n "mime") (v "0.2.5") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.7, < 0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">= 0.7, < 0.9") (d #t) (k 2)))) (h "0gqfqhys29izm1m2naa76yfgi13pgshlbhig6jnvxb7bkkyrk6ay") (f (quote (("nightly") ("heap_size" "heapsize"))))))

(define-public crate-mime-0.2.6 (c (n "mime") (v "0.2.6") (d (list (d (n "heapsize") (r ">= 0.2.0, < 0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r ">= 0.7, < 0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">= 0.7, < 0.9") (d #t) (k 2)))) (h "1q1s1ax1gaz8ld3513nvhidfwnik5asbs1ma3hp6inp5dn56nqms") (f (quote (("nightly") ("heap_size" "heapsize"))))))

(define-public crate-mime-0.3.0 (c (n "mime") (v "0.3.0") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0qam0l7dja5y3g8p3vl994fymi0wf6rfiqzvdsgxhnyrs4fbgh5w")))

(define-public crate-mime-0.3.1 (c (n "mime") (v "0.3.1") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "10h0kq7nqry3mb0vml2qdyn4ylyrp3xxqv97hvvc16wdwjfg64xs")))

(define-public crate-mime-0.3.2 (c (n "mime") (v "0.3.2") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0bfbaw6xckng875wfwb5qbsyy730dsidqs7xh9w6ih91l3c9kjn5")))

(define-public crate-mime-0.3.3 (c (n "mime") (v "0.3.3") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0d94j47c9kzaj1jxclps7aqvmq8a83p7hm4y0z7dwddiwbfrhgqm")))

(define-public crate-mime-0.3.4 (c (n "mime") (v "0.3.4") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0q61kc43zp05sc6822vl3ww4mbd5b2dkqaigvik1a3ikpgzhkmz3")))

(define-public crate-mime-0.3.5 (c (n "mime") (v "0.3.5") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1g9jwwhpfrqpj25z9iy8kl2i2wxic0bb1vxz3nli040qpqbhxq72")))

(define-public crate-mime-0.3.6 (c (n "mime") (v "0.3.6") (d (list (d (n "quoted-string") (r "^0.2.2") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0k2wab5ijs5aakdmk64maxvz9bvvj3d3j97ndr0vdza5c6ny5adb") (y #t)))

(define-public crate-mime-0.3.7 (c (n "mime") (v "0.3.7") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1yl5drcnavizx84fxzzwdwsv3vsl93rgdcy9w45w5fq91cynha0b")))

(define-public crate-mime-0.3.8 (c (n "mime") (v "0.3.8") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1nack64w2l47678dgpxwakxm3lck47m2dghyijzj5i9dkmlwhlgy")))

(define-public crate-mime-0.3.9 (c (n "mime") (v "0.3.9") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0ydx71p722pqvx0j9fqw87215hnzwff86wxgafs43kznsf92c22b")))

(define-public crate-mime-0.3.10 (c (n "mime") (v "0.3.10") (d (list (d (n "quoted-string") (r "^0.2.2") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1qrl93qybz2yw2w131ilx7j5yljbddsgniyqrlkzshq6b5jgf2qr") (y #t)))

(define-public crate-mime-0.3.11 (c (n "mime") (v "0.3.11") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "15fz7sqzrb249305vdid5mp7kbqkgr8k5c076jl4xq228qmw68px") (y #t)))

(define-public crate-mime-0.3.12 (c (n "mime") (v "0.3.12") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0c7njfkq09frn6p2lp1dr6pxv70ihzhqg8rr4h1qgsdrwy1pp40a")))

(define-public crate-mime-0.3.13 (c (n "mime") (v "0.3.13") (d (list (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "09clbyvdkwflp8anwjhqdib0sw8191gphcchdp80nc8ayhhwl9ry")))

(define-public crate-mime-0.3.14 (c (n "mime") (v "0.3.14") (h "1kxg2iqf55xbpkc5xxnb7ad9ppbmhj80amij1k60715ps6n667fx")))

(define-public crate-mime-0.3.15 (c (n "mime") (v "0.3.15") (h "1l85hhyappxkyyqbjcy8l1slp408link19g6q4nb5xr61xlppav1")))

(define-public crate-mime-0.3.16 (c (n "mime") (v "0.3.16") (h "13dcm9lh01hdwfjcg74ppljyjfj1c6w3a3cwkhxf0w8wa37cfq1a")))

(define-public crate-mime-0.3.17 (c (n "mime") (v "0.3.17") (h "16hkibgvb9klh0w0jk5crr5xv90l3wlf77ggymzjmvl1818vnxv8")))

