(define-module (crates-io mi me mime-macro-4) #:use-module (crates-io))

(define-public crate-mime-macro-4-0.0.0 (c (n "mime-macro-4") (v "0.0.0") (d (list (d (n "mime-parse-4") (r "^0.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "18d1jgw814wgkdvf7rmn06lsm66l7hhllqcrb549hxl5yr51shw3")))

