(define-module (crates-io mi me mime_guess2) #:use-module (crates-io))

(define-public crate-mime_guess2-2.0.4 (c (n "mime_guess2") (v "2.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "0g9jmqx6q821pldpmd7hjccd163xb04iixl2nsb34ciwcx1523mh") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

(define-public crate-mime_guess2-2.0.5 (c (n "mime_guess2") (v "2.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "0h8c0bf58s469lph49nflis2hxy1nhwnlxnw3rh015b0n4xk78r5") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

