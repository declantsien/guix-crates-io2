(define-module (crates-io mi me mime-sniffer) #:use-module (crates-io))

(define-public crate-mime-sniffer-0.1.0 (c (n "mime-sniffer") (v "0.1.0") (d (list (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1ij11cin4p8h9imnnfhr7w0kncjpznwb739mhy04a081iqdiqh9s")))

(define-public crate-mime-sniffer-0.1.1 (c (n "mime-sniffer") (v "0.1.1") (d (list (d (n "mime") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0dkh4b7flfj6qp4yw8kp3ilg5g37f1xlv4kdvp9fcpy0gn1mynqn")))

(define-public crate-mime-sniffer-0.1.2 (c (n "mime-sniffer") (v "0.1.2") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "142lm43w3w6wf3pma98v9n7dxqvmglr3bap24i36fr5gpg7zg61f")))

