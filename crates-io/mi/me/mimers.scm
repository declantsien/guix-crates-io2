(define-module (crates-io mi me mimers) #:use-module (crates-io))

(define-public crate-mimers-0.1.0 (c (n "mimers") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "1dhv5jsglmfc5019hy2war04pq9v18xl2iw4igc9562b7y90pd92")))

(define-public crate-mimers-0.1.1 (c (n "mimers") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4.1") (d #t) (k 0)))) (h "08wgs9kip4ryw6f77h80pk4xv9gnvxf41frifjk32rq18q655hsx")))

