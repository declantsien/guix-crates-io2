(define-module (crates-io mi me mime_guess) #:use-module (crates-io))

(define-public crate-mime_guess-1.0.0 (c (n "mime_guess") (v "1.0.0") (d (list (d (n "mime") (r "*") (d #t) (k 0)))) (h "1fpprsmagvp888b14v0w6gwd9j2yksm98f6msimzqmyffivmk6xc")))

(define-public crate-mime_guess-1.1.0 (c (n "mime_guess") (v "1.1.0") (d (list (d (n "mime") (r "*") (d #t) (k 0)))) (h "09cbzlbkg0k47srlsjhng4l3b1nah455q884f1vzvhp83k7ay5da")))

(define-public crate-mime_guess-1.1.1 (c (n "mime_guess") (v "1.1.1") (d (list (d (n "mime") (r "*") (d #t) (k 0)))) (h "0gmp5xw4x6p50y2d2352sdz2lp9644xn1gs8ph8r93zidzhnk5b9")))

(define-public crate-mime_guess-1.2.0 (c (n "mime_guess") (v "1.2.0") (d (list (d (n "mime") (r "^0.1") (d #t) (k 0)))) (h "168h48jghg1dyzpgssx77r3hhxkcb21my9lc89xgprp71lbwx816")))

(define-public crate-mime_guess-1.3.0 (c (n "mime_guess") (v "1.3.0") (d (list (d (n "mime") (r "^0.1") (d #t) (k 0)))) (h "0k5bryv0iprak47bm1iv50ph8fafm4c3qplx83hry482xhq4appf") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.4.0 (c (n "mime_guess") (v "1.4.0") (d (list (d (n "mime") (r "^0.1") (d #t) (k 0)))) (h "154zm58v2y1idy7j05hlm1idhrj354sdpij932788z6s2nmjpcls") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.4.1 (c (n "mime_guess") (v "1.4.1") (d (list (d (n "mime") (r "^0.1") (d #t) (k 0)))) (h "042d0s243d4miz29mkh8c6n8ivirhh9pn4qqsaa6vjrpsxgcdm91") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.5.0 (c (n "mime_guess") (v "1.5.0") (d (list (d (n "mime") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "16499kbc332lgk59ha5b3vmr5h6q8145283aqv4ah2a2cj0csaj2") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.5.1 (c (n "mime_guess") (v "1.5.1") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "13i3aqiyipl12frdl4r65mpwxm7l7016hk5iqqi2cja8wd1qmhd3") (f (quote (("bench")))) (y #t)))

(define-public crate-mime_guess-1.6.0 (c (n "mime_guess") (v "1.6.0") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0kp9h3v09wksvlwag3r1w1dmjgga5cdsgfar6nhs3zyvzzcgdp4f") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.6.1 (c (n "mime_guess") (v "1.6.1") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1dp7fjgsij4b7j8b95za15y9z0d38rlqbsjwkvv4wxc15xabyl2y") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.7.0 (c (n "mime_guess") (v "1.7.0") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "01mzr7zqq358phdkgj2gwbmlsd2j5h3200ihsfd0sirqzkfhqmhn") (f (quote (("bench")))) (y #t)))

(define-public crate-mime_guess-1.8.0 (c (n "mime_guess") (v "1.8.0") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "0860z9mw7g9fh9scs7da7wz3wkh4idca0f831mdi9sdwnffdi9z9") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.8.1 (c (n "mime_guess") (v "1.8.1") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "0rfxi6d6akrp1bfgxj4mrkhjq0bk23mlhgx5vw78rbs7a3w6vnkn") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.0-alpha.1 (c (n "mime_guess") (v "2.0.0-alpha.1") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1c6zc3yllsx366km5wira8lnhanlzyhmvmdcyfp3pwx0k9rrsc6x") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.0-alpha.2 (c (n "mime_guess") (v "2.0.0-alpha.2") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1blr4rlcl30s0fr9qvasfqmn6hafm15l7ihlvidf4506k9kyd997") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.8.2 (c (n "mime_guess") (v "1.8.2") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "0gik1ckzh5h2fkxqlrvizx9h65fk5fzfggsr9afw6jildy1imvmv") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.0-alpha.3 (c (n "mime_guess") (v "2.0.0-alpha.3") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "15g4m5ycv0jh4c620f5kn9rjcpr9c6a2fyqcf4a9wa33axwp4d81") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.8.3 (c (n "mime_guess") (v "1.8.3") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1a0d8c11r8p2p3nj1y1hw28vx0hk7prmndkjkv6lxfr9ashq4znw") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.0-alpha.4 (c (n "mime_guess") (v "2.0.0-alpha.4") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1ggbdwryfzqq9qk5y4sdqa2raji3b6ndk95mba8blpdnq74s63hk") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.8.4 (c (n "mime_guess") (v "1.8.4") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "0yfyywqi9p21v6vg76py8dss22cihm6kmlc2zgh88gri12fv1qmp") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.0-alpha.5 (c (n "mime_guess") (v "2.0.0-alpha.5") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "0h8khfd8mkhnpqlvn2frrmn4zqsg3cbid0m6idd0s24349g8p9yi") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.8.5 (c (n "mime_guess") (v "1.8.5") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1jz8ms4v5nhhqqhxzxsj6x3x8jayxda6wmkli5nq44qq0f9vm1vj") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.0-alpha.6 (c (n "mime_guess") (v "2.0.0-alpha.6") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1vf3k57mmgjr6f82m692q68215bn80s3y4yqcgna3jzg2d32xpih") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.8.6 (c (n "mime_guess") (v "1.8.6") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1ial941gi43c9d2nb0b699m486v0h4rfcflwlb7zv3iv2ihhjk1d") (f (quote (("bench"))))))

(define-public crate-mime_guess-1.8.7 (c (n "mime_guess") (v "1.8.7") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "1xshrkchpmcpph8ri1w365085npkq0arb5rjx4b0n6l5xvlpv5qd") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.0 (c (n "mime_guess") (v "2.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "179ahap1z3cm02yrdskj5bpb6yhqj10z0fhlwwgq75prx92wyafp") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

(define-public crate-mime_guess-2.0.1 (c (n "mime_guess") (v "2.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "16c5ssgali30db6jh1cndy77dd1qgcykhshiyfyjvxxf94wx03hs") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

(define-public crate-mime_guess-1.8.8 (c (n "mime_guess") (v "1.8.8") (d (list (d (n "mime") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("unicase"))) (d #t) (k 1)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "unicase") (r "^1.1") (d #t) (k 0)) (d (n "unicase") (r "^1.1") (d #t) (k 1)))) (h "18qcd5aa3363mb742y7lf39j7ha88pkzbv9ff2qidlsdxsjjjs91") (f (quote (("bench"))))))

(define-public crate-mime_guess-2.0.2 (c (n "mime_guess") (v "2.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "12c0rd048rn1in1j5y81zkh3yglz9zwvkaimygjrqqkdvdy528pl") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

(define-public crate-mime_guess-2.0.3 (c (n "mime_guess") (v "2.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "04pjpbl90z4yn0cmifvwgf4mqznciw6b095k626q96bxx71d9116") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

(define-public crate-mime_guess-2.0.4 (c (n "mime_guess") (v "2.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 0)) (d (n "unicase") (r "^2.4.0") (d #t) (k 1)))) (h "1vs28rxnbfwil6f48hh58lfcx90klcvg68gxdc60spwa4cy2d4j1") (f (quote (("rev-mappings") ("default" "rev-mappings"))))))

