(define-module (crates-io mi pu mipush_rs) #:use-module (crates-io))

(define-public crate-mipush_rs-0.1.0 (c (n "mipush_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)))) (h "1mjfipfz05v5icrwn7vvbpl6dj745a9xwybzzwszvrmmrsn7p5j2")))

(define-public crate-mipush_rs-0.1.1 (c (n "mipush_rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)))) (h "114yrc4m6jgcgw8n6jgdmifm6q5ickq63m6516cag2b278pwvkca")))

(define-public crate-mipush_rs-0.1.2 (c (n "mipush_rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.15") (d #t) (k 0)))) (h "1baiw06ll1j4v4vdlddmm05wzdqmzkgqlmhzd1rvhzk2ahdfa0h9")))

